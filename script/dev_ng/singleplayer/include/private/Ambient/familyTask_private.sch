///private header for family task control scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "timer_public.sch"
USING "script_ped.sch"
USING "help_at_location.sch"
USING "context_control_public.sch"

USING "familyAnim_Private.sch"

USING "familyTask_animArray.sch"
USING "familyTask_animDialogue.sch"


// *******************************************************************************************
//	FAMILY TASKS PRIVATE FUNCTIONS
// *******************************************************************************************
FUNC BOOL PRIVATE_Update_Family_Anim(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	TEXT_LABEL_63 tFamilyAnimDict
	TEXT_LABEL_63 tFamilyAnimClip
	
	ANIMATION_FLAGS eFamilyAnimFlag
	enumFamilyAnimProgress eFamilyAnimProgress
	
	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
			tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag, eFamilyAnimProgress)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS animHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		REQUEST_ANIM_DICT(tFamilyAnimDict)
		IF NOT HAS_ANIM_DICT_LOADED(tFamilyAnimDict)
			REQUEST_ANIM_DICT(tFamilyAnimDict)
			
			#IF IS_DEBUG_BUILD
			str  = ("request: ")
			str += ("\"")
			str += (tFamilyAnimDict)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, animHudColour)
			#ENDIF
			
			RETURN FALSE
		ELSE
			IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimClip)
				TASK_PLAY_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimClip,
						NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
						eFamilyAnimFlag)
				SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
				
				#IF IS_DEBUG_BUILD
				str  = ("TASK_PLAY_ANIM \"")
//				str += (tFamilyAnimDict)
//				str += ("\", \"")
				str += (tFamilyAnimClip)
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, animHudColour)
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("anim \"")
//			str += (tFamilyAnimDict)
//			str += ("\", \"")
			str += (tFamilyAnimClip)
			str += ("\" ")
			str += GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimClip))
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, animHudColour)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Update_Family_AnimAdvanced(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		FLOAT blendInDelta = NORMAL_BLEND_IN,
		FLOAT blendOutDelta = NORMAL_BLEND_OUT)
	
	TEXT_LABEL_63 tFamilyAnimAdvancedDict
	TEXT_LABEL_63 tFamilyAnimAdvancedClip
	
	ANIMATION_FLAGS eFamilyAnimAdvancedFlag
	enumFamilyAnimProgress eFamilyAnimAdvancedProgress
	
	VECTOR vAnimAdvancedOffset
	FLOAT fAnimAdvancedHead
	
	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
			tFamilyAnimAdvancedDict, tFamilyAnimAdvancedClip, eFamilyAnimAdvancedFlag, eFamilyAnimAdvancedProgress)
	AND PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vAnimAdvancedOffset, fAnimAdvancedHead)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS AnimAdvancedHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		REQUEST_ANIM_DICT(tFamilyAnimAdvancedDict)
		IF NOT HAS_ANIM_DICT_LOADED(tFamilyAnimAdvancedDict)
			REQUEST_ANIM_DICT(tFamilyAnimAdvancedDict)
			
			#IF IS_DEBUG_BUILD
			str  = ("request: ")
			str += ("\"")
			str += (tFamilyAnimAdvancedDict)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, AnimAdvancedHudColour)
			#ENDIF
			
			RETURN FALSE
		ELSE
			IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimAdvancedDict, tFamilyAnimAdvancedClip)
				
				IK_CONTROL_FLAGS ikFlags
				PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimAdvancedFlag, AF_USE_KINEMATIC_PHYSICS)
					SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
					SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
				ENDIF
				
				TASK_PLAY_ANIM_ADVANCED(PedIndex, tFamilyAnimAdvancedDict, tFamilyAnimAdvancedClip,
						vFamilyScenePos+vAnimAdvancedOffset,
						<<0,0,fFamilySceneHead+fAnimAdvancedHead>>,
						blendInDelta, blendOutDelta, -1,
						eFamilyAnimAdvancedFlag, 0, DEFAULT, ikFlags)
				SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
				
				#IF IS_DEBUG_BUILD
				str  = ("TASK_PLAY_AnimAdvanced \"")
				str += (tFamilyAnimAdvancedClip)
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, AnimAdvancedHudColour)
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("AnimAdvanced \"")
			str += (tFamilyAnimAdvancedClip)
			str += ("\" ")
			str += GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimAdvancedDict, tFamilyAnimAdvancedClip))
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, AnimAdvancedHudColour)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Update_Family_SynchScene(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		INT &iScene, bool loop, bool holdLastFrame,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		FLOAT blendInDelta = NORMAL_BLEND_IN,
		FLOAT blendOutDelta = NORMAL_BLEND_OUT)
	TEXT_LABEL_63 tFamilySynchSceneDict
	TEXT_LABEL_63 tFamilySynchSceneClip
	
	ANIMATION_FLAGS eFamilySynchSceneFlag
	enumFamilyAnimProgress eFamilySynchSceneProgress
	
	VECTOR vSynchSceneOffset
	FLOAT fSynchSceneHead
	
	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
			tFamilySynchSceneDict, tFamilySynchSceneClip, eFamilySynchSceneFlag, eFamilySynchSceneProgress)
	AND PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS SynchSceneHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		REQUEST_ANIM_DICT(tFamilySynchSceneDict)
		IF NOT HAS_ANIM_DICT_LOADED(tFamilySynchSceneDict)
			REQUEST_ANIM_DICT(tFamilySynchSceneDict)
			
			#IF IS_DEBUG_BUILD
			str  = ("request: ")
			str += ("\"")
			
			INT iFamilySynchSceneDictLength
			iFamilySynchSceneDictLength = GET_LENGTH_OF_LITERAL_STRING(tFamilySynchSceneDict)
			IF iFamilySynchSceneDictLength >= 10
				str += GET_STRING_FROM_STRING(tFamilySynchSceneDict,
						iFamilySynchSceneDictLength - 10,
						iFamilySynchSceneDictLength)
			ELSE
				str += tFamilySynchSceneDict
			ENDIF
			
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			tAnimDict = ""
			tAnimClip = ""
			
			RETURN FALSE
		ELSE
			SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT
			RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT
			FLOAT moverBlendInDelta = INSTANT_BLEND_IN
			IK_CONTROL_FLAGS ikFlags = AIK_NONE
			
			
			
				PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
				
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				iScene = CREATE_SYNCHRONIZED_SCENE(vFamilyScenePos+vSynchSceneOffset, <<0,0,fFamilySceneHead+fSynchSceneHead>>)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iScene, loop)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, holdLastFrame)
				SET_SYNCHRONIZED_SCENE_RATE(iScene, 1.0)
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_USE_PHYSICS)
					SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
					SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
				ENDIF
				
				TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
						tFamilySynchSceneDict, tFamilySynchSceneClip,
						blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
				SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
				
				#IF IS_DEBUG_BUILD
				str  = ("creating SynchScene \"")
				INT iFamilySynchSceneDictLength
				iFamilySynchSceneDictLength = GET_LENGTH_OF_LITERAL_STRING(tFamilySynchSceneClip)
				IF iFamilySynchSceneDictLength >= 10
					str += GET_STRING_FROM_STRING(tFamilySynchSceneClip,
							iFamilySynchSceneDictLength - 10,
							iFamilySynchSceneDictLength)
				ELSE
					str += tFamilySynchSceneClip
				ENDIF
				str += ("\" ")
				str += (iScene)
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
				#ENDIF
				
				tAnimDict = tFamilySynchSceneDict
				tAnimClip = tFamilySynchSceneClip
				
				RETURN FALSE
			ENDIF
			
			IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilySynchSceneDict, tFamilySynchSceneClip, ANIM_SYNCED_SCENE)
				IF IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_USE_PHYSICS)
					SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
					SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
				ENDIF
				
				TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
						tFamilySynchSceneDict, tFamilySynchSceneClip,
						blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta, ikFlags)
				SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iScene, loop)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, holdLastFrame)
				
				#IF IS_DEBUG_BUILD
				str  = ("tasking SynchScene \"")
				
				INT iTaunterAnimOutLength
				iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tFamilySynchSceneDict)
				
				IF iTaunterAnimOutLength >= 10
					str += GET_STRING_FROM_STRING(tFamilySynchSceneDict,
							iTaunterAnimOutLength - 10,
							iTaunterAnimOutLength)
				ELSE
					str += tFamilySynchSceneDict
				ENDIF
				
				str += ("/")
				iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(tFamilySynchSceneClip)
				
				IF iTaunterAnimOutLength >= 15
					str += GET_STRING_FROM_STRING(tFamilySynchSceneClip,
							iTaunterAnimOutLength - 15,
							iTaunterAnimOutLength)
				ELSE
					str += tFamilySynchSceneClip
				ENDIF
				
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
				#ENDIF
				
				tAnimDict = tFamilySynchSceneDict
				tAnimClip = tFamilySynchSceneClip
				
				RETURN FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("SynchScene \"")
			str += (tFamilySynchSceneDict)
			str += ("\" id:")
			str += iScene
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			
			str  = (tFamilySynchSceneClip)
			str += (":")
			str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, SynchSceneHudColour)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	tAnimDict = ""
	tAnimClip = ""
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_SynchSceneMatch(PED_INDEX PedIndex, //enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
//		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		INT &iScene, TEXT_LABEL_63 tAnimDict, TEXT_LABEL_63 tAnimClip,
		STRING sAnimTail, STRING sMatchTail)
		
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	HUD_COLOURS SynchSceneHudColour = HUD_COLOUR_PINK
	#ENDIF
	
	REQUEST_ANIM_DICT(tAnimDict)
	IF NOT HAS_ANIM_DICT_LOADED(tAnimDict)
		REQUEST_ANIM_DICT(tAnimDict)
		
		#IF IS_DEBUG_BUILD
		str  = ("request: ")
		str += ("\"")
		str += (tAnimDict)
		str += ("\"")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
		#ENDIF
		
		RETURN FALSE
	ELSE
		FLOAT blendInDelta = NORMAL_BLEND_IN
		FLOAT blendOutDelta = NORMAL_BLEND_OUT
		SYNCED_SCENE_PLAYBACK_FLAGS flags = SYNCED_SCENE_USE_PHYSICS
		RAGDOLL_BLOCKING_FLAGS ragdollFlags = RBF_PLAYER_IMPACT
		FLOAT moverBlendInDelta = INSTANT_BLEND_IN
		
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
		
			#IF IS_DEBUG_BUILD
			str  = ("synch scene not running...")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF IS_STRING_NULL_OR_EMPTY(sAnimTail)
		
			#IF IS_DEBUG_BUILD
			str  = ("sAnimTail is null or empty")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			RETURN FALSE
		ENDIF
			
		INT iMatchClip_length = GET_LENGTH_OF_LITERAL_STRING(tAnimClip)
		INT iMatchTail_length = GET_LENGTH_OF_LITERAL_STRING(sAnimTail)
		
		INT iMatchBody_length = iMatchClip_length - iMatchTail_length
		TEXT_LABEL_63 tMatchBody = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tAnimClip, 0, iMatchBody_length)
		TEXT_LABEL_63 tMatchClip = tMatchBody
		tMatchClip += sMatchTail
		
		IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tMatchClip, ANIM_SYNCED_SCENE)
		OR GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.001
			IF IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_USE_PHYSICS)
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
				SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			ENDIF
			
			TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
					tAnimDict, tMatchClip,
					blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			
			#IF IS_DEBUG_BUILD
			str  = ("tasking SynchSceneMatch \"")
			str += (tMatchClip)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
			#ENDIF
			
			CPRINTLN(DEBUG_FAMILY, "matchup synch scene: \"", tAnimDict, "/", tAnimClip, "\", \"", sAnimTail, "\" - \"", tMatchBody, "\" + \"", sMatchTail, "\" = \"", tMatchClip, "\"")
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str  = ("SynchScene \"")
		str += (tAnimDict)
		str += ("\" id:")
		str += iScene
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, SynchSceneHudColour)
		
		str  = (tMatchClip)
		str += (":")
		str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, SynchSceneHudColour)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Update_Family_Scenario(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent, INT &iScenarioCount)
	
	TEXT_LABEL_63 tFamilyScenarioDictNULL
	TEXT_LABEL_63 tFamilyScenarioClip
	
	ANIMATION_FLAGS eFamilyScenarioFlag
	enumFamilyAnimProgress eFamilyScenarioProgress
	
	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
			tFamilyScenarioDictNULL, tFamilyScenarioClip, eFamilyScenarioFlag, eFamilyScenarioProgress)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS scenarioHudColour = HUD_COLOUR_PINK
		#ENDIF
		
		IF NOT IS_SCENARIO_TYPE_ENABLED(tFamilyScenarioClip)
			SET_SCENARIO_TYPE_ENABLED(tFamilyScenarioClip, TRUE)
			
			#IF IS_DEBUG_BUILD
			str  = ("enable: ")
			str += ("\"")
			str += (tFamilyScenarioClip)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, scenarioHudColour)
			#ENDIF
			
			RETURN FALSE
		ELSE
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_START_SCENARIO_IN_PLACE) <> PERFORMING_TASK)
				
				IF (iScenarioCount > 0)
					iScenarioCount = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
					RETURN FALSE
				ENDIF
				
				TASK_START_SCENARIO_IN_PLACE(PedIndex, tFamilyScenarioClip)
				iScenarioCount++
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyScenarioFlag, AF_USE_KINEMATIC_PHYSICS)
					SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
					SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				str  = ("TASK_START_SCENARIO_IN_PLACE \"")
//				str += (tFamilyScenarioDict)
//				str += ("\", \"")
				str += (tFamilyScenarioClip)
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, scenarioHudColour)
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("scenario \"")
//			str += (tFamilyScenarioDict)
//			str += ("\", \"")
			str += (tFamilyScenarioClip)
			str += ("\" count:")
			str += (iScenarioCount)
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, scenarioHudColour)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Update_Family_ON_PHONE(PED_INDEX PedIndex)	//, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
//	TEXT_LABEL_63 tFamilyON_PHONEDictNULL
//	TEXT_LABEL_63 tFamilyON_PHONEClip
//	
//	ANIMATION_FLAGS eFamilyON_PHONEFlag
//	enumFamilyAnimProgress eFamilyON_PHONEProgress
//	
//	IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
//			tFamilyON_PHONEDictNULL, tFamilyON_PHONEClip, eFamilyON_PHONEFlag, eFamilyON_PHONEProgress)
//		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		HUD_COLOURS ON_PHONEHudColour = HUD_COLOUR_PINK
		#ENDIF
//		
//		IF NOT IS_ON_PHONE_TYPE_ENABLED(tFamilyON_PHONEClip)
//			SET_ON_PHONE_TYPE_ENABLED(tFamilyON_PHONEClip, TRUE)
//			
//			#IF IS_DEBUG_BUILD
//			str  = ("enable: ")
//			str += ("\"")
//			str += (tFamilyON_PHONEClip)
//			str += ("\"")
//			
//			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, ON_PHONEHudColour)
//			#ENDIF
//			
//			RETURN FALSE
//		ELSE
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_USE_MOBILE_PHONE) <> PERFORMING_TASK)
				TASK_USE_MOBILE_PHONE(PedIndex, TRUE)
				
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
				SET_PED_CONFIG_FLAG(PedIndex, PCF_UseKinematicModeWhenStationary, TRUE)
				
				SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
				
				#IF IS_DEBUG_BUILD
				str  = ("TASK_USE_MOBILE_PHONE \"")
//				str += (tFamilyON_PHONEDict)
//				str += ("\", \"")
//				str += (tFamilyON_PHONEClip)
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, ON_PHONEHudColour)
				#ENDIF
				
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("ON_PHONE \"")
//			str += (tFamilyON_PHONEDict)
//			str += ("\", \"")
//			str += (tFamilyON_PHONEClip)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, ON_PHONEHudColour)
			#ENDIF
			
			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_Family_Member_Ready_For_Driving(PED_INDEX PedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VEHICLE_INDEX vehicleIndex, AUTOMATIC_DOOR_ENUM eDoor,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		VEHICLE_SEAT seat = VS_DRIVER)
	
	
	IF NOT IS_BIT_SET(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_ALWAYS_OPEN)
		FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(eDoor, TRUE)
		CLEAR_BIT(g_sAutoDoorData[ENUM_TO_INT(eDoor)].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
		RETURN FALSE
	ELSE
		
	ENDIF
	
	//entering vehicle, must be ready for driving!
	IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_VEHICLE_SEAT_FREE(vehicleIndex, seat)
		IF (GET_PED_IN_VEHICLE_SEAT(vehicleIndex, seat) <> PedIndex)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (eFamilyMember = FM_MICHAEL_WIFE)
		
		VECTOR vInitOffset
		FLOAT fInitHead
		IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vInitOffset, fInitHead)
			
			VECTOR VecCentre = vFamilyScenePos+vInitOffset
			VECTOR VecOffset = <<0,0,0>>
			VECTOR VecBounds = <<12,12,2>>
			FLOAT fAngle = fFamilySceneHead+fInitHead
			FLOAT fOffset = 9.0
			
				
			IF eFamilyEvent = FE_M_WIFE_leaving_in_car
				
				VecOffset = <<-0.4,-2.7,0.5>>
				VecBounds = <<3.0,5.0,2.0>>+<<0.5,0.5,0>>
				fOffset = 80.000
			ENDIF
			
	//		START_WIDGET_GROUP("PRIVATE_Is_Family_Member_Ready_For_Driving")
	//			ADD_WIDGET_VECTOR_SLIDER("VecCentre", VecCentre, -4000.0, 4000.0, 0.0)
	//			ADD_WIDGET_VECTOR_SLIDER("VecOffset", VecOffset, -10.0, 10.0, 0.1)
	//			ADD_WIDGET_VECTOR_SLIDER("VecBounds", VecBounds, 0.0, 20.0, 0.5)
	//			ADD_WIDGET_FLOAT_SLIDER("fAngle", fAngle, 0.0, 360.0, 0.0)
	//			ADD_WIDGET_FLOAT_SLIDER("fOffset", fOffset, 0.0, 360.0, 1.0)
	//		STOP_WIDGET_GROUP()
	//		
	//		WHILE TRUE
	//			IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
	//					VecCentre+VecOffset, VecBounds, fAngle+fOffset)
	//			
	//			IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_C)
	//			
	//				SAVE_NEWLINE_TO_DEBUG_FILE()
	//				SAVE_STRING_TO_DEBUG_FILE("		IF eFamilyEvent = FE_X_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))SAVE_NEWLINE_TO_DEBUG_FILE()
	//				SAVE_STRING_TO_DEBUG_FILE("			VecOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(VecOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
	//				SAVE_STRING_TO_DEBUG_FILE("			VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
	//				SAVE_STRING_TO_DEBUG_FILE("			fOffset = ")SAVE_FLOAT_TO_DEBUG_FILE(fOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
	//				SAVE_NEWLINE_TO_DEBUG_FILE()
	//
	//			ENDIF
	//			
	//			WAIT(0)
	//		ENDWHILE
			
			//
			IF NOT IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					VecCentre+VecOffset, VecBounds, fAngle+fOffset)
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE
	ELIF ((eFamilyMember = FM_FRANKLIN_LAMAR) OR (eFamilyMember = FM_FRANKLIN_STRETCH))
		RETURN TRUE
	ELSE
		
		
		IF eFamilyEvent = FE_M7_SON_coming_back_from_a_bike_ride
			
			VECTOR vInitOffset
			FLOAT fInitHead
			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vInitOffset, fInitHead)
				
				VECTOR VecCentre = vFamilyScenePos+vInitOffset
				VECTOR VecOffset1 = <<30.0, 25.0, 6.9>>, VecOffset2 = <<5.0, 0.0, 0.0>>
				VECTOR VecBounds1 = <<24.0, 30.0, 10.0>>, VecBounds2 = <<45.0,36.0,0.0>>
				FLOAT fAngle = fFamilySceneHead+fInitHead
				FLOAT fOffset = 6.0000
				
			/*	START_WIDGET_GROUP("PRIVATE_Is_Family_Member_Ready_For_Driving")
					ADD_WIDGET_VECTOR_SLIDER("VecCentre", VecCentre, -4000.0, 4000.0, 0.0)
					
					ADD_WIDGET_STRING("Locate 1")
					ADD_WIDGET_VECTOR_SLIDER("VecOffset1", VecOffset1, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("VecBounds1", VecBounds1, 0.0, 50.0, 0.5)
					
					ADD_WIDGET_STRING("Locate 2")
					ADD_WIDGET_VECTOR_SLIDER("VecOffset2", VecOffset2, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("VecBounds2", VecBounds2, 0.0, 50.0, 0.5)
					
					ADD_WIDGET_STRING("Angle")
					ADD_WIDGET_FLOAT_SLIDER("fAngle", fAngle, 0.0, 360.0, 0.0)
					ADD_WIDGET_FLOAT_SLIDER("fOffset", fOffset, 0.0, 360.0, 1.0)
				STOP_WIDGET_GROUP()
				
				WHILE TRUE
					IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
							VecCentre+VecOffset1, VecBounds1, fAngle+fOffset)
					IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
							VecCentre+VecOffset2, VecBounds2, fAngle+fOffset)
					
					IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_C)
					
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("		IF eFamilyEvent = FE_X_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("			VecOffset1 = ")SAVE_VECTOR_TO_DEBUG_FILE(VecOffset1)SAVE_STRING_TO_DEBUG_FILE(", VecOffset2 = ")SAVE_VECTOR_TO_DEBUG_FILE(VecOffset2)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("			VecBounds1 = ")SAVE_VECTOR_TO_DEBUG_FILE(VecBounds1)SAVE_STRING_TO_DEBUG_FILE(", VecBounds2 = ")SAVE_VECTOR_TO_DEBUG_FILE(VecBounds2)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("			fOffset = ")SAVE_FLOAT_TO_DEBUG_FILE(fOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
		
					ENDIF
					
					WAIT(0)
				ENDWHILE*/
				
				//
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						VecCentre+VecOffset1, VecBounds1, fAngle+fOffset)
				OR IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						VecCentre+VecOffset2, VecBounds2, fAngle+fOffset)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			
			IF NOT IS_ENTITY_ON_SCREEN(PedIndex)
				RETURN FALSE
			ENDIF
		ENDIF
		
		
		
		//
		FLOAT fDistFromPlayer = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex))
		IF fDistFromPlayer > (15.0*15.0)
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
ENDFUNC
FUNC BOOL PRIVATE_Is_Family_Member_Ready_For_DriveWander(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		VECTOR VehCoors, VECTOR ExitCoors,
		TEXT_LABEL_31 &tRecordingName)
	
	//already wandering, must be ready for a wander!
	IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) = PERFORMING_TASK)
		RETURN TRUE
	ENDIF
	
	CONST_FLOAT fReachedExitRadius	2.0
	FLOAT fExitMag, fExitRad
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_WIFE
			fExitMag = VMAG(ExitCoors-VehCoors)
			fExitRad = fExitMag-fReachedExitRadius
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(VehCoors, fExitRad, HUD_COLOUR_BLUELIGHT, 0.25)
			DrawDebugFamilySphere(ExitCoors, fReachedExitRadius, HUD_COLOUR_RED, 1.0)
			#ENDIF
			
			IF VDIST2(VehCoors, GET_ENTITY_COORDS(PedIndex)) > ((fExitRad)*(fExitRad))
				RETURN TRUE
			ENDIF
			
			tRecordingName = "family_m_wife_drive"
			RETURN FALSE
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			fExitMag = VMAG(ExitCoors-VehCoors)
			fExitRad = fExitMag-fReachedExitRadius
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(VehCoors, fExitRad, HUD_COLOUR_BLUELIGHT, 0.25)
			DrawDebugFamilySphere(ExitCoors, fReachedExitRadius, HUD_COLOUR_RED, 1.0)
			#ENDIF
			
			IF VDIST2(VehCoors, GET_ENTITY_COORDS(PedIndex)) > ((fExitRad)*(fExitRad))
				RETURN TRUE
			ENDIF
			
			tRecordingName = "family_m_daughter_drive"
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC BOOL PRIVATE_Update_Family_DrivingAway(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		VEHICLE_INDEX &vehicleIndex, enumFamilyMember eFamilyMembersVeh,
		enumFamilyEvents eFamilyEvent, INT &iEnterVehStage,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead, INT &iScene,
		AUTOMATIC_DOOR_ENUM eDoor,
		
		STRING strTextBlock, STRING strBase, STRING strExit, TEXT_LABEL &tCreatedConvLabels[],
		FLOAT fExitSpeechPhase,
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer,
		STRING sDismissContext,
		#IF USE_TU_CHANGES
		INTERIOR_INSTANCE_INDEX &iInteriorForThisPlayer,
		#ENDIF
		PED_INDEX PassengerPedIndex = NULL)
	
	enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH
	
	#IF IS_DEBUG_BUILD
	CONST_INT iFamTextOffset 2
	#ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehicleIndex)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		str  = ("cant find ")
		str += (Get_String_From_FamilyMember(eFamilyMembersVeh))
		str += ("'s vehicle")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_PURE_WHITE)
		#ENDIF
		
		iEnterVehStage = 0
	ELSE
		//
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyLine(GET_ENTITY_COORDS(PedIndex, FALSE), GET_ENTITY_COORDS(vehicleIndex, FALSE), HUD_COLOUR_RED)
		
		TEXT_LABEL_63 str
		str  = (Get_String_From_FamilyMember(eFamilyMember))
		str += (", driving ")
		str += (Get_String_From_FamilyMember(eFamilyMembersVeh))
		str += ("'s vehicle")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset, HUD_COLOUR_RED)
		#ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF NOT IS_PED_SITTING_IN_VEHICLE(PedIndex, vehicleIndex)
				
				VEHICLE_SEAT eSeat = VS_DRIVER
				FLOAT MoveSpeed = PEDMOVEBLENDRATIO_WALK
				ENTER_EXIT_VEHICLE_FLAGS eFlags = ECF_RESUME_IF_INTERRUPTED
				INT iTime = DEFAULT_TIME_NEVER_WARP		//DEFAULT_TIME_BEFORE_WARP
			
				TEXT_LABEL_63 tFamilyAnimDict = ""
				TEXT_LABEL_63 tFamilyAnimLoopClip = "", tFamilyAnimExitClip = ""
				
				ANIMATION_FLAGS eFamilyAnimFlag
				enumFamilyAnimProgress eFamilyAnimProgress
				
				CONST_INT iDRIVING_0_waitToEnter		0
				
//				CONST_INT iDRIVING_1_exitAnim			1
				CONST_INT iDRIVING_2_exitSynch			2
				CONST_INT iDRIVING_4_exitDialogue		4
				CONST_INT iDRIVING_5_exitAnim			5
				CONST_INT iDRIVING_6_exitDialogue		6
				
				CONST_INT iDRIVING_3_enterVeh			3
				CONST_INT iDRIVING_33_enterVehDismiss	33
				
				SWITCH iEnterVehStage
					CASE iDRIVING_0_waitToEnter		//idle anim while waiting for the player to approach
						
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("0. wait to enter, idle anim", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
						#ENDIF
						
						IF NOT PRIVATE_Is_Family_Member_Ready_For_Driving(PedIndex, eFamilyMember, eFamilyEvent, vehicleIndex, eDoor, vFamilyScenePos, fFamilySceneHead, eSeat)
							//
							
							
							IF (eFamilyEvent = FE_M_WIFE_leaving_in_car)
								TEXT_LABEL_63 tAnimDict, tAnimClip
								IF PRIVATE_Update_Family_SynchScene(PedIndex, eFamilyMember, eFamilyEvent,
										vFamilyScenePos, fFamilySceneHead,
										iScene, TRUE, FALSE, tAnimDict, tAnimClip)
									
								ENDIF
							ELSE
								IF PRIVATE_Update_Family_ON_PHONE(PedIndex)
								ENDIF
							ENDIF
							
						ELSE
							//
							
							CLEAR_PED_TASKS(PedIndex)
							
							FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(eDoor, TRUE)
							CLEAR_BIT(g_sAutoDoorData[ENUM_TO_INT(eDoor)].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
							
							IF (eFamilyEvent = FE_M_WIFE_leaving_in_car)
								iEnterVehStage = iDRIVING_2_exitSynch
							ELSE
								IF NOT IS_STRING_NULL_OR_EMPTY(strBase)
									IF Is_Ped_Playing_Family_Speech(strTextBlock, strBase,
												inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
										REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex))
										iEnterVehStage = iDRIVING_3_enterVeh
									ENDIF
								ELSE
									REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex))
									iEnterVehStage = iDRIVING_3_enterVeh
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE iDRIVING_2_exitSynch		//
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("2. start playing exit synch", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
						#ENDIF
						
						IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
								tFamilyAnimDict, tFamilyAnimLoopClip, eFamilyAnimFlag, eFamilyAnimProgress)
							REQUEST_ANIM_DICT(tFamilyAnimDict)
							IF HAS_ANIM_DICT_LOADED(tFamilyAnimDict)
								//play "base" dialogue
								IF Is_Ped_Playing_Family_Speech(strTextBlock, strBase,
											inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
											
									IF (eFamilyEvent <> FE_M_WIFE_leaving_in_car)
										tFamilyAnimExitClip = "exit"
									ELSE
										tFamilyAnimExitClip = "exit_demo"
									ENDIF
									
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
										VECTOR vSynchSceneOffset
										FLOAT fSynchSceneHead
										vSynchSceneOffset = <<0,0,0>>
										fSynchSceneHead = 0
										IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
											iScene = CREATE_SYNCHRONIZED_SCENE(vFamilyScenePos+vSynchSceneOffset, <<0,0,fFamilySceneHead+fSynchSceneHead>>)
										ENDIF
									ENDIF
									
									SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
									
									TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
											tFamilyAnimDict, tFamilyAnimExitClip,
											NORMAL_BLEND_IN, SLOW_BLEND_OUT,
											SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
									SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
									
									RESTART_TIMER_NOW(speechTimer)
									iEnterVehStage = iDRIVING_4_exitDialogue
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE iDRIVING_4_exitDialogue		//
						#IF IS_DEBUG_BUILD
						str  = "4. exiting, starting dialogue \""
						str += strExit
						str += "\" "
						str += GET_STRING_FROM_FLOAT(fExitSpeechPhase)
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
						#ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(strExit)
							iEnterVehStage = iDRIVING_5_exitAnim
							RETURN TRUE
						ENDIF
						IF (fExitSpeechPhase <= 0.0)
							iEnterVehStage = iDRIVING_5_exitAnim
							RETURN TRUE
						ENDIF
						
						IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
								tFamilyAnimDict, tFamilyAnimLoopClip, eFamilyAnimFlag, eFamilyAnimProgress)
								
							IF (eFamilyEvent <> FE_M_WIFE_leaving_in_car)
								tFamilyAnimExitClip = "exit"
							ELSE
								tFamilyAnimExitClip = "exit_demo"
							ENDIF
							
							IF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
								//play "exit" dialogue
								IF Is_Ped_Playing_Family_Speech(strTextBlock, strExit,
											inSpeechStruct, enSpeechPriority, tCreatedConvLabels, "face")
									iEnterVehStage = iDRIVING_6_exitDialogue
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE iDRIVING_6_exitDialogue		//
						#IF IS_DEBUG_BUILD
						str  = "6. exiting, wait for dialogue \""
						str += strExit
						str += "\" "
						str += GET_STRING_FROM_FLOAT(fExitSpeechPhase)
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
						#ENDIF
						
						IF IS_STRING_NULL_OR_EMPTY(strExit)
							iEnterVehStage = iDRIVING_5_exitAnim
							RETURN TRUE
						ENDIF
						IF (fExitSpeechPhase <= 0.0)
							iEnterVehStage = iDRIVING_5_exitAnim
							RETURN TRUE
						ENDIF
						
						IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
								tFamilyAnimDict, tFamilyAnimLoopClip, eFamilyAnimFlag, eFamilyAnimProgress)
								
								
							IF (eFamilyEvent <> FE_M_WIFE_leaving_in_car)
								tFamilyAnimExitClip = "exit"
							ELSE
								tFamilyAnimExitClip = "exit_demo"
							ENDIF
							
							FLOAT fAnimClipCurrentTime
							fAnimClipCurrentTime = -1
							IF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip, ANIM_SCRIPT)
								fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
							ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip, ANIM_SYNCED_SCENE)
								fAnimClipCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
							ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
								fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
							ENDIF
							
							
							#IF IS_DEBUG_BUILD
							str  = "\""
							str += tFamilyAnimExitClip
							str += "\": "
							str += GET_STRING_FROM_FLOAT(fAnimClipCurrentTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
							#ENDIF
							
							
							IF (fAnimClipCurrentTime >= 0)
								IF fAnimClipCurrentTime >= fExitSpeechPhase
									//unpause "exit" dialogue
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_FAMILY, "TASK_PLAY_ANIM(\"", tFamilyAnimDict, "\", \"", "face", "\", AF_SECONDARY)")
									#ENDIF
									
									RESTART_TIMER_NOW(speechTimer)
									iEnterVehStage = iDRIVING_5_exitAnim
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE iDRIVING_5_exitAnim		//
						
						CONST_FLOAT fANIM_CLIP_MAX_TIME 0.88
						
						#IF IS_DEBUG_BUILD
						str  = "5. playing exit anim \""
						str += strExit
						str += "\" "
						str += GET_STRING_FROM_FLOAT(fANIM_CLIP_MAX_TIME)
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_GREENLIGHT)
						#ENDIF
						
						IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
								tFamilyAnimDict, tFamilyAnimLoopClip, eFamilyAnimFlag, eFamilyAnimProgress)
							
							IF (eFamilyEvent <> FE_M_WIFE_leaving_in_car)
								tFamilyAnimExitClip = "exit"
							ELSE
								tFamilyAnimExitClip = "exit_demo"
							ENDIF
							
							FLOAT fAnimClipCurrentTime
							fAnimClipCurrentTime = -1
							IF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip, ANIM_SCRIPT)
								fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
							ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip, ANIM_SYNCED_SCENE)
								fAnimClipCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
							ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
								fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimExitClip)
							ENDIF
							
							
							#IF IS_DEBUG_BUILD
							str  = "\""
							str += tFamilyAnimExitClip
							str += "\": "
							str += GET_STRING_FROM_FLOAT(fAnimClipCurrentTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREENLIGHT)
							#ENDIF
							
							IF (fAnimClipCurrentTime >= 0)
							
								IF fAnimClipCurrentTime >= fANIM_CLIP_MAX_TIME
									FORCE_PED_MOTION_STATE(PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
									CLEAR_PED_TASKS(PedIndex)
									
									TASK_ENTER_VEHICLE(PedIndex, vehicleIndex, iTime, eSeat, MoveSpeed, eFlags)
									
									FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(eDoor, TRUE)
									CLEAR_BIT(g_sAutoDoorData[ENUM_TO_INT(eDoor)].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
									
									REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex))
									iEnterVehStage = iDRIVING_3_enterVeh
									RETURN TRUE
								ENDIF
							ELSE
								FORCE_PED_MOTION_STATE(PedIndex, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
								CLEAR_PED_TASKS(PedIndex)
								
								IF IS_VEHICLE_SEAT_FREE(vehicleIndex, eSeat)
									TASK_ENTER_VEHICLE(PedIndex, vehicleIndex, iTime, eSeat, MoveSpeed, eFlags)
								ELSE
									//
									
								ENDIF
								
								FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(eDoor, TRUE)
								CLEAR_BIT(g_sAutoDoorData[ENUM_TO_INT(eDoor)].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
								
								REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex))
								iEnterVehStage = iDRIVING_3_enterVeh
								RETURN TRUE
							ENDIF
						ENDIF
					BREAK

					CASE iDRIVING_3_enterVeh				//tasked to enter the vehicle
					CASE iDRIVING_33_enterVehDismiss
						
						IF NOT PRIVATE_Is_Family_Member_Ready_For_Driving(PedIndex, eFamilyMember, eFamilyEvent, vehicleIndex, eDoor, vFamilyScenePos, fFamilySceneHead, eSeat)
							//
							
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset("3. trying to enter vehicle!", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_GREEN)
							#ENDIF
							
							IF NOT IS_VEHICLE_SEAT_FREE(vehicleIndex, eSeat)
								#IF IS_DEBUG_BUILD
								IF iEnterVehStage = iDRIVING_3_enterVeh
									DrawDebugFamilyTextWithOffset("3. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ELIF iEnterVehStage = iDRIVING_33_enterVehDismiss
									DrawDebugFamilyTextWithOffset("33. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ELSE
									DrawDebugFamilyTextWithOffset("XX. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ENDIF
								#ENDIF
								
								IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) <> PERFORMING_TASK)
								AND (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
//									TASK_WANDER_IN_AREA(PedIndex,  GET_ENTITY_COORDS(vehicleIndex), VDIST(GET_ENTITY_COORDS(vehicleIndex), vFamilyScenePos))
									TASK_WANDER_STANDARD(PedIndex)
								ENDIF
								
								IF (iEnterVehStage = iDRIVING_3_enterVeh)
									IF NOT IS_STRING_NULL_OR_EMPTY(sDismissContext)
										IF (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(PedIndex, FALSE)) < 5)
										AND NOT (IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID()) OR IS_ANY_SPEECH_PLAYING(PedIndex) OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR IS_CALLING_ANY_CONTACT())
										
											STRING VoiceName
											SWITCH eFamilyMember
												CASE FM_MICHAEL_WIFE		VoiceName = "AMANDA_NORMAL" BREAK
												CASE FM_MICHAEL_SON			VoiceName = "JIMMY_NORMAL" BREAK
												CASE FM_MICHAEL_DAUGHTER	VoiceName = "TRACY_NORMAL" BREAK
											ENDSWITCH
											
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PedIndex, sDismissContext, VoiceName)
											iEnterVehStage = iDRIVING_33_enterVehDismiss
											
											#IF IS_DEBUG_BUILD
											SAVE_STRING_TO_DEBUG_FILE("sDismissContext - two")SAVE_NEWLINE_TO_DEBUG_FILE()
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								RETURN FALSE
							ENDIF
						ELSE
							
							IF NOT IS_VEHICLE_SEAT_FREE(vehicleIndex, eSeat)
								#IF IS_DEBUG_BUILD
								IF iEnterVehStage = iDRIVING_3_enterVeh
									DrawDebugFamilyTextWithOffset("3. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ELIF iEnterVehStage = iDRIVING_33_enterVehDismiss
									DrawDebugFamilyTextWithOffset("33. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ELSE
									DrawDebugFamilyTextWithOffset("33. entering vehicle (seat not free)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								ENDIF
								#ENDIF
								
								IF GET_PED_IN_VEHICLE_SEAT(vehicleIndex, eSeat) != PedIndex
									IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) <> PERFORMING_TASK)
									AND (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
	//									TASK_WANDER_IN_AREA(PedIndex,  GET_ENTITY_COORDS(vehicleIndex), VDIST(GET_ENTITY_COORDS(vehicleIndex), vFamilyScenePos))
										TASK_WANDER_STANDARD(PedIndex)
									ENDIF
								ENDIF
								
								IF (iEnterVehStage = iDRIVING_3_enterVeh)
									IF NOT IS_STRING_NULL_OR_EMPTY(sDismissContext)
										IF (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(PedIndex, FALSE)) < 5)
										AND NOT (IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID()) OR IS_ANY_SPEECH_PLAYING(PedIndex) OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR IS_CALLING_ANY_CONTACT())
										
											STRING VoiceName
											SWITCH eFamilyMember
												CASE FM_MICHAEL_WIFE		VoiceName = "AMANDA_NORMAL" BREAK
												CASE FM_MICHAEL_SON			VoiceName = "JIMMY_NORMAL" BREAK
												CASE FM_MICHAEL_DAUGHTER	VoiceName = "TRACY_NORMAL" BREAK
											ENDSWITCH
											
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PedIndex, sDismissContext, VoiceName)
											iEnterVehStage = iDRIVING_33_enterVehDismiss
											
											#IF IS_DEBUG_BUILD
											SAVE_STRING_TO_DEBUG_FILE("sDismissContext - two")SAVE_NEWLINE_TO_DEBUG_FILE()
											CPRINTLN(DEBUG_FAMILY, "sDismissContext - two")
											#ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								RETURN FALSE
							ENDIF
							
							IF VDIST2(GET_ENTITY_COORDS(vehicleIndex), GET_ENTITY_COORDS(PedIndex)) > (50*50)
								#IF IS_DEBUG_BUILD
								DrawDebugFamilyTextWithOffset("3. entering vehicle (too far, navmesh)", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_GREEN)
								#ENDIF
								
								IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
									TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, GET_ENTITY_COORDS(vehicleIndex), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
								ENDIF
								
								RETURN FALSE
							ENDIF
							
							
							IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
								
								IF NOT HAS_VEHICLE_ASSET_LOADED(GET_ENTITY_MODEL(vehicleIndex))
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] enter assets havent loaded... ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle)		", Get_String_From_FamilyEvent(eFamilyEvent))
									#ENDIF
								ELSE
									TASK_ENTER_VEHICLE(PedIndex, vehicleIndex, iTime, eSeat, MoveSpeed, eFlags)
									
									#IF IS_DEBUG_BUILD
									CDEBUG3LN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] entering ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle)		", Get_String_From_FamilyEvent(eFamilyEvent))
									#ENDIF
								
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								DrawDebugFamilyTextWithOffset("3. entering vehicle", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
								#ENDIF
								
								IF GET_PED_CONFIG_FLAG(pedIndex, PCF_OpenDoorArmIK)
									SET_PED_RESET_FLAG(pedIndex, PRF_SearchForClosestDoor, TRUE)
								ENDIF
							ENDIF
							
							IF NOT IS_PED_INJURED(PassengerPedIndex)
								IF NOT IS_PED_SITTING_IN_VEHICLE(PassengerPedIndex, vehicleIndex)
									IF (GET_SCRIPT_TASK_STATUS(PassengerPedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
										TASK_ENTER_VEHICLE(PassengerPedIndex, vehicleIndex, iTime, VS_ANY_PASSENGER, MoveSpeed, eFlags)
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					BREAK
					
					DEFAULT
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "invalid iEnterVehStage ", iEnterVehStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DrivingAway(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
						CASSERTLN(DEBUG_FAMILY, "invalid iEnterVehStage ", iEnterVehStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DrivingAway(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
						#ENDIF
					BREAK
				ENDSWITCH
				
			ELSE
				iEnterVehStage = 0
				
				FLOAT CruiseSpeed	= 20
				DRIVINGMODE Mode	= DRIVINGMODE_STOPFORCARS
				VECTOR VehCoors		= << -825.7757, 177.2016, 71.9871 >>
				VECTOR VecExitCoors	= << -844.4757, 156.9072, 67.0048 >>	//<< -849.4479, 158.3810, 65.6464 >>
				
				TEXT_LABEL_31 tRecordingName
				IF NOT PRIVATE_Is_Family_Member_Ready_For_DriveWander(PedIndex, eFamilyMembersVeh,
						VehCoors, VecExitCoors,
						tRecordingName)
					
					#IF USE_TU_CHANGES
					IF IS_VALID_INTERIOR(iInteriorForThisPlayer)
					AND IS_INTERIOR_READY(iInteriorForThisPlayer)
						IF NOT IS_ENTITY_ON_SCREEN(vehicleIndex)
							SET_ENTITY_COORDS(vehicleIndex, VehCoors)
							SET_ENTITY_HEADING(vehicleIndex, GET_HEADING_FROM_VECTOR_2D(VecExitCoors.x-VehCoors.x, VecExitCoors.y-VehCoors.y))
							
							CWARNINGLN(DEBUG_FAMILY, "bug #1766708 - move driver because the player is inside and cant see them")
						ENDIF
					ENDIF
					#ENDIF
					
					IF IS_STRING_NULL_OR_EMPTY(tRecordingName)
						IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK)
							
							DRIVINGSTYLE Style = DRIVINGSTYLE_ACCURATE
							FLOAT TargetRadius = 3.0
							FLOAT StraightLineDist = -1.0 
							
							TASK_VEHICLE_DRIVE_TO_COORD(PedIndex, vehicleIndex,
									VecExitCoors, CruiseSpeed * 0.5,
									Style, DUMMY_MODEL_FOR_SCRIPT,
									DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,
									TargetRadius, StraightLineDist)
							
							IF (eDoor <> AUTODOOR_MAX)
								IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
									REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
								ENDIF
							ENDIF
							
							#IF IS_DEBUG_BUILD
							CDEBUG3LN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] is in ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle, task to drive to coord)		", Get_String_From_FamilyEvent(eFamilyEvent))
							#ENDIF
							
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset("in vehicle... drive to coord", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
							#ENDIF
						ENDIF
					ELSE
						IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) <> PERFORMING_TASK)
							
							REQUEST_WAYPOINT_RECORDING(tRecordingName)
							IF GET_IS_WAYPOINT_RECORDING_LOADED(tRecordingName)
								
								
								CPRINTLN(DEBUG_FAMILY, "DOOR_SYSTEM_GET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(eDoor)].doorID): ", DOOR_SYSTEM_GET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(eDoor)].doorID))
								
								IF (eDoor <> AUTODOOR_MAX)
									FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(eDoor, TRUE)
									CLEAR_BIT(g_sAutoDoorData[ENUM_TO_INT(eDoor)].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
									
									IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
										REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
									ENDIF
									
									IF DOOR_SYSTEM_GET_OPEN_RATIO(g_sAutoDoorData[ENUM_TO_INT(eDoor)].doorID) > -1.0
										RETURN TRUE
									ENDIF
								ENDIF
								INT iStartingProgress
								iStartingProgress = 0
								
								IF VDIST(GET_ENTITY_COORDS(PedIndex), <<-827.1387, 176.3679, 69.9464>>) > 2.5
									iStartingProgress = 8
								ENDIF
								
								#IF IS_DEBUG_BUILD
								SAVE_STRING_TO_DEBUG_FILE("dist: ")
								SAVE_FLOAT_TO_DEBUG_FILE(VDIST(GET_ENTITY_COORDS(PedIndex), <<-827.1387, 176.3679, 69.9464>>))
								SAVE_NEWLINE_TO_DEBUG_FILE()
								#ENDIF
								
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(PedIndex, vehicleIndex,
										tRecordingName, Mode, iStartingProgress,	//
										EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN)			//EWAYPOINT_FOLLOW_FLAGS iFlags = EWAYPOINT_DEFAULT)
								
								#IF IS_DEBUG_BUILD
								CDEBUG3LN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] is in ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle, task to waypoint)		", Get_String_From_FamilyEvent(eFamilyEvent))
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset("in vehicle... follow waypoint", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					BOOL bPassengerNotInVeh
					bPassengerNotInVeh = FALSE
					IF NOT IS_PED_INJURED(PassengerPedIndex)
						IF NOT IS_PED_SITTING_IN_VEHICLE(PassengerPedIndex, vehicleIndex)
							bPassengerNotInVeh = TRUE
							IF (GET_SCRIPT_TASK_STATUS(PassengerPedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
								TASK_ENTER_VEHICLE(PassengerPedIndex, vehicleIndex, DEFAULT_TIME_NEVER_WARP, VS_ANY_PASSENGER, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_VEHICLE(PedIndex, vehicleIndex, FALSE)
						IF bPassengerNotInVeh
							IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_TEMP_ACTION) <> PERFORMING_TASK)
								TASK_VEHICLE_TEMP_ACTION(PedIndex, vehicleIndex, TEMPACT_BRAKE, 500)
								
	//							IF (eDoor <> AUTODOOR_MAX)
	//								IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
	//									REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
	//								ENDIF
	//							ENDIF
								
								#IF IS_DEBUG_BUILD
								CDEBUG3LN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] is in ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle, task to break)		", Get_String_From_FamilyEvent(eFamilyEvent))
								#ENDIF
								
							ELSE
								#IF IS_DEBUG_BUILD
								DrawDebugFamilyTextWithOffset("in vehicle... drive break", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
								#ENDIF
							ENDIF
						ELSE
							IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK)
								TASK_VEHICLE_DRIVE_WANDER(PedIndex, vehicleIndex, CruiseSpeed, Mode)
								
								IF (eDoor <> AUTODOOR_MAX)
									IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
										REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								CDEBUG3LN(DEBUG_FAMILY, "PRIVATE_Update_Family_DrivingAway(family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] is in ", Get_String_From_FamilyMember(eFamilyMembersVeh), "'s vehicle, task to WANDER)		", Get_String_From_FamilyEvent(eFamilyEvent))
								#ENDIF
								
							ELSE
								#IF IS_DEBUG_BUILD
								DrawDebugFamilyTextWithOffset("in vehicle... drive wander", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("vehicle NOT DRIVABLE", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sDismissContext)
				STRING VoiceName
				SWITCH eFamilyMember
					CASE FM_MICHAEL_WIFE		VoiceName = "AMANDA_NORMAL" BREAK
					CASE FM_MICHAEL_SON			VoiceName = "JIMMY_NORMAL" BREAK
					CASE FM_MICHAEL_DAUGHTER	VoiceName = "TRACY_NORMAL" BREAK
				ENDSWITCH
				
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PedIndex, sDismissContext, VoiceName)
				
				#IF IS_DEBUG_BUILD
				SAVE_STRING_TO_DEBUG_FILE("sDismissContext - two")SAVE_NEWLINE_TO_DEBUG_FILE()
				CPRINTLN(DEBUG_FAMILY, "sDismissContext - two")
				#ENDIF
			ENDIF
			
			iEnterVehStage = 0
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Update_Family_DrivingHome(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		VEHICLE_INDEX &vehicleIndex, enumFamilyMember eFamilyMembersVeh,
		enumFamilyEvents eFamilyEvent, INT &iEnterVehStage,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead, VECTOR VecExitCoors,
		AUTOMATIC_DOOR_ENUM eDoor, enumFamilyEvents eDesiredFamilyEvent,
		
		STRING strTextBlock, STRING strBase, TEXT_LABEL &tCreatedConvLabels[],
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer)
	
	enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH
	
	#IF IS_DEBUG_BUILD
	CONST_INT iFamTextOffset 2
	#ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehicleIndex)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		str  = ("cant find ")
		str += (Get_String_From_FamilyMember(eFamilyMembersVeh))
		str += ("'s vehicle")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_PURE_WHITE)
		#ENDIF
		
		eFamilyMembersVeh = eFamilyMembersVeh
		iEnterVehStage = 0
	ELSE
		//
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyLine(GET_ENTITY_COORDS(PedIndex, FALSE), GET_ENTITY_COORDS(vehicleIndex, FALSE), HUD_COLOUR_RED)
		
		TEXT_LABEL_63 str
		str  = (Get_String_From_FamilyMember(eFamilyMember))
		str += (", driving ")
		str += (Get_String_From_FamilyMember(eFamilyMembersVeh))
		str += ("'s vehicle")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset, HUD_COLOUR_RED)
		#ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
		
			VEHICLE_SEAT eSeat = VS_DRIVER
			FLOAT MoveSpeed = PEDMOVEBLENDRATIO_WALK
			ENTER_EXIT_VEHICLE_FLAGS eFlags = ECF_RESUME_IF_INTERRUPTED
			INT iTime = DEFAULT_TIME_NEVER_WARP		//DEFAULT_TIME_BEFORE_WARP
			
			CONST_INT iDRIVING_0_waitToEnter	0
			CONST_INT iDRIVING_1_waitToEnter	1
			
			SWITCH iEnterVehStage
				CASE iDRIVING_0_waitToEnter
					#IF IS_DEBUG_BUILD
					DrawDebugFamilyTextWithOffset("0. waitToEnter", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
					#ENDIF
					
					
					IF NOT PRIVATE_Is_Family_Member_Ready_For_Driving(PedIndex, eFamilyMember, eFamilyEvent, vehicleIndex, eDoor, vFamilyScenePos, fFamilySceneHead, VS_DRIVER)
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("NOT PRIVATE_Is_Family_Member_Ready_For_Driving", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
						#ENDIF
						
						RETURN FALSE
					ENDIF
					
					FLOAT fExitCoorsDist2
					fExitCoorsDist2 = VDIST2(GET_ENTITY_COORDS(PedIndex, FALSE), VecExitCoors)
					
					IF NOT IS_PED_SITTING_IN_VEHICLE(PedIndex, vehicleIndex)
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("NOT IS_PED_SITTING_IN_VEHICLE", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
						#ENDIF
						
						IF NOT IS_ENTITY_ON_SCREEN(PedIndex)
							SET_PED_INTO_VEHICLE(PedIndex, vehicleIndex, eSeat)
							RETURN FALSE
						ENDIF
						
						IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
							TASK_ENTER_VEHICLE(PedIndex, vehicleIndex, iTime, eSeat, MoveSpeed, eFlags)
						ENDIF

					ELSE
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("IS_PED_SITTING_IN_VEHICLE", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
						
						str  =("dist: ")
						str +=GET_STRING_FROM_FLOAT(SQRT(fExitCoorsDist2))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+3, HUD_COLOUR_RED)
						#ENDIF
						
						IF (eDoor <> AUTODOOR_MAX)
							IF NOT IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
								REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, PedIndex)
							ENDIF
						ENDIF
						
						
						IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) <> PERFORMING_TASK)
							
							FLOAT CruiseSpeed
							DRIVINGMODE Mode
							CruiseSpeed	= 7.5
							Mode	= DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS
							
							DRIVINGSTYLE Style
							FLOAT TargetRadius, StraightLineDist
							Style = DRIVINGSTYLE_ACCURATE
							TargetRadius = 3.0
							StraightLineDist = -1.0 
							
							TASK_VEHICLE_DRIVE_TO_COORD(PedIndex, vehicleIndex,
										VecExitCoors, CruiseSpeed,
										Style, DUMMY_MODEL_FOR_SCRIPT, Mode,
										TargetRadius, StraightLineDist)
						ENDIF
					ENDIF
					
					IF fExitCoorsDist2 <= (5*5)
						TASK_LEAVE_VEHICLE(PedIndex, vehicleIndex)
						iEnterVehStage = iDRIVING_1_waitToEnter
					ENDIF
				BREAK
				CASE iDRIVING_1_waitToEnter
					#IF IS_DEBUG_BUILD
					DrawDebugFamilyTextWithOffset("1. waitToEnter", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
					#ENDIF
					
					IF Is_Ped_Playing_Family_Speech(strTextBlock, strBase,
							inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
						RESTART_TIMER_NOW(speechTimer)
						
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
						RETURN TRUE
					ENDIF
				BREAK

				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid iEnterVehStage ", iEnterVehStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DrivingAway(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					CASSERTLN(DEBUG_FAMILY, "invalid iEnterVehStage ", iEnterVehStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DrivingAway(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
				BREAK
			ENDSWITCH
				
				
			
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("vehicle NOT DRIVABLE", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) <> PERFORMING_TASK)
			AND (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) <> PERFORMING_TASK)
//				TASK_WANDER_IN_AREA(PedIndex,  GET_ENTITY_COORDS(vehicleIndex), VDIST(GET_ENTITY_COORDS(vehicleIndex), vFamilyScenePos))
				TASK_WANDER_STANDARD(PedIndex)
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Get_Family_Walking_Offsets(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR &vWalkOffsets[], FLOAT &fWalkOffsets[])
	
	vWalkOffsets[0] = <<0,0,0>>
	
	SWITCH eFamilyEvent
		CASE FE_M_DAUGHTER_walks_to_room_music
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
																	fWalkOffsets[0] = 0.0
					vWalkOffsets[1] = <<-4.5000, 9.6000, -1.2000>>	fWalkOffsets[1] = 0.0
					vWalkOffsets[2] = <<-6.3000, 7.8000, -4.5000>>	fWalkOffsets[2] = 0.0
					vWalkOffsets[3] = <<-0.5000, 0.2000, -3.8000>>	fWalkOffsets[3] = 0.0
					vWalkOffsets[4] = <<-0.9000, 12.9000, -4.1000>>	fWalkOffsets[4] = 0.0
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			vWalkOffsets[0] = <<-6.3800, -3.7100, 1.0000>>	fWalkOffsets[0] = 82.5000
			vWalkOffsets[1] = <<-1.8580, -4.6230, 1.1920>>	fWalkOffsets[1] = 162.0000
			vWalkOffsets[2] = <<-8.3066, -15.8344, 1.1816>>	fWalkOffsets[2] = 162.0000
			vWalkOffsets[3] = <<-15.2080, -4.1770, 0.5000>>	fWalkOffsets[3] = 71.3000
			vWalkOffsets[4] = <<-7.4216, -9.6486, 5.1000>>	fWalkOffsets[4] = -110.7000
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Get_Family_Walking_Offsets(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Get_Family_Walking_Offsets(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Get_Family_Walking_AnimSet(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		INT iWalkingStage,
		TEXT_LABEL_63 &tCleaningEnter_animDict, TEXT_LABEL_63 &tCleaningEnter_animClip,
		TEXT_LABEL_63 &tCleaningBase_animDict, TEXT_LABEL_63 &tCleaningBase_animClip,
		TEXT_LABEL_63 &tCleaningExit_animDict, TEXT_LABEL_63 &tCleaningExit_animClip,
		ANIMATION_FLAGS &eCleaning_animFlag)
	
	IF NOT DOES_ENTITY_EXIST(PedIndex)
		RETURN FALSE
	ENDIF
	
	SWITCH eFamilyEvent
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			
			SWITCH iWalkingStage 
				CASE 0
					tCleaningEnter_animDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	tCleaningEnter_animClip = "IG_9_ENTER"
					tCleaningBase_animDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	tCleaningBase_animClip = "IG_9_BASE"
					tCleaningExit_animDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	tCleaningExit_animClip = "IG_9_EXIT"
//					tCleaningEnter_animDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	tCleaningEnter_animClip = "IG_9_IDLE_A"
//					tCleaningEnter_animDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	tCleaningEnter_animClip = "IG_9_IDLE_B"
					
					#IF IS_DEBUG_BUILD
					Draw_Family_Member_Cleaning_Debug_Surface_Plane(PedIndex, 0.8, 0.15-0.02, HUD_COLOUR_BLUELIGHT)
					#ENDIF
					
					RETURN TRUE
				BREAK
				
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					TEXT_LABEL_63 tCleaning_animDict, tCleaning_animClip
					enumFamilyAnimProgress eFamilyAnimProgress
					IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
							tCleaning_animDict, tCleaning_animClip, eCleaning_animFlag, eFamilyAnimProgress)
						tCleaningEnter_animDict  = tCleaning_animDict	tCleaningEnter_animDict += "ENTER"
						tCleaningEnter_animClip  = tCleaning_animClip	tCleaningEnter_animClip += "enter"
						
						tCleaningBase_animDict  = tCleaning_animDict	tCleaningBase_animDict += "BASE"
						tCleaningBase_animClip  = tCleaning_animClip	tCleaningBase_animClip += "base"
						
						tCleaningExit_animDict  = tCleaning_animDict	tCleaningExit_animDict += "EXIT"
						tCleaningExit_animClip  = tCleaning_animClip	tCleaningExit_animClip += "exit"
					
						#IF IS_DEBUG_BUILD
						Draw_Family_Member_Cleaning_Debug_Window_Plane(PedIndex, 0.8, fDebugWindowPlaneOffset, fDebugWindowPlaneDiff, HUD_COLOUR_BLUELIGHT)
						#ENDIF
						
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Get_Family_Walking_Offsets(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Get_Family_Walking_Offsets(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Walking(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vSequencePos, FLOAT fSequenceHead, INT &iWalkingStage)
	
	VECTOR vWalkOffset
	FLOAT fWalkHead
	
	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vWalkOffset, fWalkHead)
		VECTOR vWalk_offsets[5]
		FLOAT fWalk_offsets[5]
		
		IF PRIVATE_Get_Family_Walking_Offsets(eFamilyMember, eFamilyEvent, vWalk_offsets, fWalk_offsets)
			
			VECTOR vWalk_Pos_x = vSequencePos+vWalkOffset
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE
			FLOAT fAlphaMult = 1.0
			
			INT iWalk
			REPEAT COUNT_OF(vWalk_offsets) iWalk
				INT iNextWalk = iWalk+1
				IF iNextWalk >= COUNT_OF(vWalk_offsets)
					iNextWalk = 0
				ENDIF
				
				str  =("vWalk_offsets[")
				str +=(iWalk)
				str +=("]")
				
				DrawDebugFamilyTextWithOffset(str, vWalk_Pos_x+vWalk_offsets[iWalk], 0, eColour, fAlphaMult)
				DrawDebugFamilyLine(vWalk_Pos_x+vWalk_offsets[iWalk], vWalk_Pos_x+vWalk_offsets[iNextWalk], eColour, fAlphaMult)
			ENDREPEAT
			
			str  =("iWalkingStage: ")
			str +=(iWalkingStage)
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, eColour)
			
			str  =("dist: ")
			str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vWalk_Pos_x+vWalk_offsets[iWalkingStage]))
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PedIndex),vWalk_Pos_x+vWalk_offsets[iWalkingStage]) >= (1.0*1.0)
				#IF IS_DEBUG_BUILD
				DrawDebugFamilySphere(vWalk_Pos_x+vWalk_offsets[iWalkingStage], 1.0, eColour, 0.15)
				#ENDIF
				
				IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, vWalk_Pos_x+vWalk_offsets[iWalkingStage], PEDMOVE_WALK)
				ENDIF
			ELSE
				CLEAR_PED_TASKS(PedIndex)
				
				iWalkingStage++
				IF iWalkingStage >= COUNT_OF(vWalk_offsets)
					iWalkingStage = 0
				ENDIF
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	fSequenceHead = fSequenceHead
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_GoToCoordCleaning(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vSequencePos, FLOAT fSequenceHead, INT &iScene,
		OBJECT_INDEX &cleaning_prop, MODEL_NAMES eCleaningPropModel, MODEL_NAMES &eStoredCleaningPropModel, PED_BONETAG eCleaningPropAttachBonetag,
		INT &iCleaningStage, INT &iCleaningCount,
		BOOL bMovePeds)
	
	VECTOR vCleanOffset
	FLOAT fCleanHead
	
	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vCleanOffset, fCleanHead)
		VECTOR vClean_offsets[5]
		FLOAT fClean_offsets[5]
		
		IF PRIVATE_Get_Family_Walking_Offsets(eFamilyMember, eFamilyEvent, vClean_offsets, fClean_offsets)
			
			INT Time					= DEFAULT_TIME_BEFORE_WARP	//DEFAULT_TIME_NEVER_WARP	//
			FLOAT Radius				= 0.01						//0.15						//DEFAULT_NAVMESH_RADIUS
			ENAV_SCRIPT_FLAGS NavFlags	= ENAV_DONT_ADJUST_TARGET_POSITION
			
			VECTOR vClean_Pos_x = vSequencePos+vCleanOffset
			FLOAT fClean_Head_x = fSequenceHead+fCleanHead
			
			#IF IS_DEBUG_BUILD
			IF bMovePeds
				
				TEXT_LABEL_63 tCleaningEnter_animDict, tCleaningEnter_animClip
				TEXT_LABEL_63 tCleaningBase_animDict, tCleaningBase_animClip
				TEXT_LABEL_63 tCleaningExit_animDict, tCleaningExit_animClip
				ANIMATION_FLAGS eCleaning_animFlag
				
				FLOAT fCleaningPhase = 0.0
				
				INT iWidget
				TEXT_LABEL_63 tWidget
				START_WIDGET_GROUP("PRIVATE_Update_Family_GoToCoordCleaning")
					ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
					ADD_WIDGET_INT_SLIDER("iCleaningCount", iCleaningCount, 0, COUNT_OF(vClean_offsets)-1, 1)
					
					ADD_WIDGET_FLOAT_SLIDER("fCleaningPhase", fCleaningPhase, 0, 1, 0.001)
					
					REPEAT COUNT_OF(vClean_offsets) iWidget
						
						IF PRIVATE_Get_Family_Walking_AnimSet(PedIndex, eFamilyMember, eFamilyEvent,
								iWidget,
								tCleaningEnter_animDict, tCleaningEnter_animClip,
								tCleaningBase_animDict, tCleaningBase_animClip,
								tCleaningExit_animDict, tCleaningExit_animClip,
								eCleaning_animFlag)
						ENDIF
						
						tWidget  ="["
						tWidget += iWidget
						tWidget +="] \""
						tWidget += GET_STRING_FROM_STRING(tCleaningBase_animDict,
								GET_LENGTH_OF_LITERAL_STRING("TIMETABLE@"),
								GET_LENGTH_OF_LITERAL_STRING(tCleaningBase_animDict))
						tWidget +="\""
						
						START_WIDGET_GROUP(tWidget)
							ADD_WIDGET_FLOAT_SLIDER("vClean_offsets x", vClean_offsets[iWidget].x, vClean_offsets[iWidget].x-5.0, vClean_offsets[iWidget].x+5.0, 0.01)
						    ADD_WIDGET_FLOAT_SLIDER("vClean_offsets y", vClean_offsets[iWidget].y, vClean_offsets[iWidget].y-5.0, vClean_offsets[iWidget].y+5.0, 0.01)
						    ADD_WIDGET_FLOAT_SLIDER("vClean_offsets z", vClean_offsets[iWidget].z, vClean_offsets[iWidget].z-2.0, vClean_offsets[iWidget].z+2.0, 0.01)
							
							ADD_WIDGET_FLOAT_SLIDER("fClean_offsets", fClean_offsets[iWidget], -180, 180, 0.1)
						STOP_WIDGET_GROUP()
					ENDREPEAT
					
				STOP_WIDGET_GROUP()
				WHILE bMovePeds
					
					IF IS_PED_INJURED(PedIndex)
						bMovePeds = FALSE
						RETURN FALSE
					ENDIF
					
					SET_ENTITY_COORDS(PedIndex, vClean_Pos_x+vClean_offsets[iCleaningCount]+<<0,0,-1>>)
					SET_ENTITY_HEADING(PedIndex, fClean_Head_x+fClean_offsets[iCleaningCount])
					
					IF PRIVATE_Get_Family_Walking_AnimSet(PedIndex, eFamilyMember, eFamilyEvent,
							iCleaningCount,
							tCleaningEnter_animDict, tCleaningEnter_animClip,
							tCleaningBase_animDict, tCleaningBase_animClip,
							tCleaningExit_animDict, tCleaningExit_animClip,
							eCleaning_animFlag)
						
						REQUEST_ANIM_DICT(tCleaningEnter_animDict)
						IF HAS_ANIM_DICT_LOADED(tCleaningEnter_animDict)
							IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tCleaningEnter_animDict, tCleaningEnter_animClip)
								IF ARE_STRINGS_EQUAL( "TIMETABLE@MAID@CLEANING_SURFACE_1@", tCleaningEnter_animDict)
									TASK_PLAY_ANIM_ADVANCED(PedIndex, tCleaningEnter_animDict, tCleaningEnter_animClip,
											vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
											NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, eCleaning_animFlag, 0.0,
											EULER_YXZ, AIK_NONE)
								ELSE
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
										iScene = CREATE_SYNCHRONIZED_SCENE(vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>)
									ENDIF
									TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
											tCleaningEnter_animDict, tCleaningEnter_animClip,
											INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
											SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					VECTOR vFaceOffset
					vFaceOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vClean_Pos_x+vClean_offsets[iCleaningCount], fClean_Head_x+fClean_offsets[iCleaningCount], <<0,1,0>>)
					DrawDebugFamilyLine(vClean_Pos_x+vClean_offsets[iCleaningCount], vFaceOffset, HUD_COLOUR_RED, 1.0)
					DrawDebugFamilySphere(vFaceOffset, 0.05, HUD_COLOUR_RED, 1.0)
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<0,-1.5,-1>>))
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
						iScene = CREATE_SYNCHRONIZED_SCENE(vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>)
					ELSE
						SET_SYNCHRONIZED_SCENE_ORIGIN(iScene, vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>)
						SET_SYNCHRONIZED_SCENE_PHASE(iScene, fCleaningPhase)
					ENDIF
					
					WAIT(0)
				ENDWHILE
				
				REPEAT COUNT_OF(vClean_offsets) iWidget
					SAVE_STRING_TO_DEBUG_FILE("		vWalkOffsets[")
					SAVE_INT_TO_DEBUG_FILE(iWidget)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_VECTOR_TO_DEBUG_FILE(vClean_offsets[iWidget])
					SAVE_STRING_TO_DEBUG_FILE("	fWalkOffsets[")
					SAVE_INT_TO_DEBUG_FILE(iWidget)
					SAVE_STRING_TO_DEBUG_FILE("] = ")
					SAVE_FLOAT_TO_DEBUG_FILE(fClean_offsets[iWidget])
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				iCleaningStage = 0
				
				RETURN FALSE
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE
			FLOAT fAlphaMult = 1.0
			
			INT iClean
			REPEAT COUNT_OF(vClean_offsets) iClean
				INT iNextClean = iClean+1
				IF iNextClean >= COUNT_OF(vClean_offsets)
					iNextClean = 0
				ENDIF
				
				IF (iClean = iCleaningCount)
					eColour = HUD_COLOUR_RED
				ENDIF
				
				str  =("vClean_offsets[")
				str +=(iClean)
				str +=("]")
				DrawDebugFamilyTextWithOffset(str, vClean_Pos_x+vClean_offsets[iClean], 0, eColour, fAlphaMult)
				
				FLOAT fPlayerCoord_groundZ = 0.0
				FLOAT  fAnimPos_heightDiff
				
				str = "  height: "
				IF GET_GROUND_Z_FOR_3D_COORD(vClean_Pos_x+vClean_offsets[iClean] ,fPlayerCoord_groundZ)
					fAnimPos_heightDiff = ((vClean_Pos_x.z+vClean_offsets[iClean].z) - 1.0) - fPlayerCoord_groundZ
					
					str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
					DrawDebugFamilyTextWithOffset(str, vClean_Pos_x+vClean_offsets[iClean], 1, eColour, fAlphaMult)
				ELSE
					fAnimPos_heightDiff = ((vClean_Pos_x.z+vClean_offsets[iClean].z) - 1.0) - fPlayerCoord_groundZ
					
					str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
					DrawDebugFamilyTextWithOffset(str, vClean_Pos_x+vClean_offsets[iClean], 1, eColour, fAlphaMult)
				ENDIF
				
				IF (iClean = iCleaningCount)
					VECTOR vFaceOffset
					vFaceOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vClean_Pos_x+vClean_offsets[iClean], fClean_Head_x+fClean_offsets[iClean], <<0,1,0>>)
					DrawDebugFamilyLine(vClean_Pos_x+vClean_offsets[iClean], vFaceOffset, eColour, fAlphaMult)
					DrawDebugFamilySphere(vFaceOffset, 0.05, eColour, fAlphaMult)
				ENDIF
				
				
				eColour = HUD_COLOUR_PURE_WHITE
				DrawDebugFamilyLine(vClean_Pos_x+vClean_offsets[iClean], vClean_Pos_x+vClean_offsets[iNextClean], eColour, fAlphaMult)
				
				
			ENDREPEAT
			
			str  =("iCleaningStage: ")
			str +=(iCleaningStage)
			str +=(", Count: ")
			str +=(iCleaningCount)
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, eColour)
			#ENDIF
			
			TEXT_LABEL_63 tCleaningEnter_animDict, tCleaningEnter_animClip
			TEXT_LABEL_63 tCleaningBase_animDict, tCleaningBase_animClip
			TEXT_LABEL_63 tCleaningExit_animDict, tCleaningExit_animClip
			ANIMATION_FLAGS eCleaning_animFlag
			
			IF PRIVATE_Get_Family_Walking_AnimSet(PedIndex, eFamilyMember, eFamilyEvent,
					iCleaningCount,
					tCleaningEnter_animDict, tCleaningEnter_animClip,
					tCleaningBase_animDict, tCleaningBase_animClip,
					tCleaningExit_animDict, tCleaningExit_animClip,
					eCleaning_animFlag)
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eCleaning_animFlag, AF_LOOPING)
					CLEAR_BITMASK_ENUM_AS_ENUM(eCleaning_animFlag, AF_LOOPING)
				ELSE
					//
				ENDIF
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eCleaning_animFlag, AF_USE_KINEMATIC_PHYSICS)
					
				ELSE
					SET_BITMASK_ENUM_AS_ENUM(eCleaning_animFlag, AF_USE_KINEMATIC_PHYSICS)
				ENDIF
				
				CONST_INT iCLEANING_0_start	0
				CONST_INT iCLEANING_1_wait	1
				CONST_INT iCLEANING_2_find	2
				CONST_INT iCLEANING_3_enter	3
				CONST_INT iCLEANING_4_base	4
				CONST_INT iCLEANING_5_exit	5
				
				SWITCH iCleaningStage
					CASE iCLEANING_0_start
						#IF IS_DEBUG_BUILD
						str  =("dist_0_start: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						#ENDIF
						
						IF DOES_ENTITY_EXIST(cleaning_prop)
							DELETE_OBJECT(cleaning_prop)
							eStoredCleaningPropModel = DUMMY_MODEL_FOR_SCRIPT
						ENDIF
						
						IF VDIST2(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]) >= (1.0*1.0)
							#IF IS_DEBUG_BUILD
							DrawDebugFamilySphere(vClean_Pos_x+vClean_offsets[iCleaningCount], 1.0, eColour, 0.15)
							#ENDIF
							
							IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK)
								IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)

									NAVDATA sNavdata
									sNavdata.m_fSlideToCoordHeading = fClean_Head_x+fClean_offsets[iCleaningCount]
									
									
									SEQUENCE_INDEX siseq
									OPEN_SEQUENCE_TASK(siseq)
										TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, vClean_Pos_x+vClean_offsets[iCleaningCount], PEDMOVE_WALK,
												Time, Radius, NavFlags, sNavdata,
												fClean_Head_x+fClean_offsets[iCleaningCount])
									CLOSE_SEQUENCE_TASK(siseq)
									TASK_PERFORM_SEQUENCE(PedIndex, siseq)
									CLEAR_SEQUENCE_TASK(siseq)
									
								ENDIF
							ENDIF
						ELSE
							REQUEST_MODEL(eCleaningPropModel)
							REQUEST_ANIM_DICT(tCleaningEnter_animDict)
							REQUEST_ANIM_DICT(tCleaningBase_animDict)
							REQUEST_ANIM_DICT(tCleaningExit_animDict)
							
							iCleaningStage = iCLEANING_1_wait
						ENDIF
					BREAK
					CASE iCLEANING_1_wait
						#IF IS_DEBUG_BUILD
						str  =("dist_1_wait: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						
						INT iWaitForRequest
						iWaitForRequest = 0
						IF NOT HAS_MODEL_LOADED(eCleaningPropModel)
							str  =("request \"")
							str +=GET_MODEL_NAME_FOR_DEBUG(eCleaningPropModel)
							str +=("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4+TO_FLOAT(iWaitForRequest), eColour)
							iWaitForRequest++
						ENDIF
						IF NOT HAS_ANIM_DICT_LOADED(tCleaningEnter_animDict)
							str  =("request \"")
							str +=tCleaningEnter_animDict
							str +=("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4+TO_FLOAT(iWaitForRequest), eColour)
							iWaitForRequest++
						ENDIF
						IF NOT HAS_ANIM_DICT_LOADED(tCleaningBase_animDict)
							str  =("request \"")
							str +=tCleaningBase_animDict
							str +=("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4+TO_FLOAT(iWaitForRequest), eColour)
							iWaitForRequest++
						ENDIF
						IF NOT HAS_ANIM_DICT_LOADED(tCleaningExit_animDict)
							str  =("request \"")
							str +=tCleaningExit_animDict
							str +=("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4+TO_FLOAT(iWaitForRequest), eColour)
							iWaitForRequest++
						ENDIF
						
						IF iWaitForRequest = 0
							str  =("request \"")
							str +="none"
							str +=("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4+TO_FLOAT(iWaitForRequest), eColour)
						ENDIF
						
						#ENDIF
						
						IF HAS_MODEL_LOADED(eCleaningPropModel)
						AND HAS_ANIM_DICT_LOADED(tCleaningEnter_animDict)
						AND HAS_ANIM_DICT_LOADED(tCleaningBase_animDict)
						AND HAS_ANIM_DICT_LOADED(tCleaningExit_animDict)

							IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK)
								
								NAVDATA sNavdata
								sNavdata.m_fSlideToCoordHeading = fClean_Head_x+fClean_offsets[iCleaningCount]
								
								SEQUENCE_INDEX siseq
								OPEN_SEQUENCE_TASK(siseq)
									TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, vClean_Pos_x+vClean_offsets[iCleaningCount], PEDMOVE_WALK,
											Time, Radius, NavFlags, sNavdata,
											fClean_Head_x+fClean_offsets[iCleaningCount])
								CLOSE_SEQUENCE_TASK(siseq)
								TASK_PERFORM_SEQUENCE(PedIndex, siseq)
								CLEAR_SEQUENCE_TASK(siseq)
							ENDIF
							
							iCleaningStage = iCLEANING_2_find
						ENDIF
					BREAK
					CASE iCLEANING_2_find
						#IF IS_DEBUG_BUILD
						str  =("dist_2_find: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						#ENDIF
						
						IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PedIndex, FALSE), vClean_Pos_x+vClean_offsets[iCleaningCount]) <= 0.5
								
								SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
								
								IF HAS_ANIM_DICT_LOADED(tCleaningEnter_animDict)
									IF ARE_STRINGS_EQUAL(tCleaningEnter_animDict, "TIMETABLE@MAID@CLEANING_SURFACE_1@")
										TASK_PLAY_ANIM(PedIndex, tCleaningEnter_animDict, tCleaningEnter_animClip,
		//										vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
												WALK_BLEND_IN, INSTANT_BLEND_OUT, -1, eCleaning_animFlag | AF_HOLD_LAST_FRAME,
												0.0, DEFAULT, AIK_NONE)
									ELSE
										iScene = CREATE_SYNCHRONIZED_SCENE(vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>)
										TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
												tCleaningEnter_animDict, tCleaningEnter_animClip,
												WALK_BLEND_IN, WALK_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT,
												default, WALK_BLEND_IN)
									ENDIF
								ELSE
									CASSERTLN(DEBUG_FAMILY, "missing enter anim dict?")
								ENDIF
								
								iCleaningStage = iCLEANING_3_enter
							ENDIF
						ENDIF
					BREAK
					CASE iCLEANING_3_enter
						#IF IS_DEBUG_BUILD
						str  =("dist_3_enter: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						#ENDIF
						
						BOOL bFinishedEnterAnim
						bFinishedEnterAnim = FALSE
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							FLOAT fEnterAnimPhaseTime
							fEnterAnimPhaseTime = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
							
							#IF IS_DEBUG_BUILD
							str  =("enterSync: ")
							str +=GET_STRING_FROM_FLOAT(fEnterAnimPhaseTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, eColour)
							#ENDIF
							
							IF fEnterAnimPhaseTime >= 0.3
								IF NOT DOES_ENTITY_EXIST(cleaning_prop)
									eStoredCleaningPropModel = eCleaningPropModel
									
									cleaning_prop = CREATE_OBJECT(eCleaningPropModel, GET_PED_BONE_COORDS(pedIndex, eCleaningPropAttachBonetag, <<0,0,0>>))
									ATTACH_ENTITY_TO_ENTITY(cleaning_prop, pedIndex,
											GET_PED_BONE_INDEX(pedIndex, eCleaningPropAttachBonetag),
											<<0,0,0>>, <<0,0,0>>,
											TRUE, DEFAULT, TRUE)
								ENDIF
							ENDIF
							
							IF fEnterAnimPhaseTime >= 0.99
								bFinishedEnterAnim = TRUE
							ENDIF
						ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tCleaningEnter_animDict, tCleaningEnter_animClip)
							FLOAT fEnterAnimPhaseTime
							fEnterAnimPhaseTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tCleaningEnter_animDict, tCleaningEnter_animClip)
							
							#IF IS_DEBUG_BUILD
							str  =("enterAnim: ")
							str +=GET_STRING_FROM_FLOAT(fEnterAnimPhaseTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, eColour)
							#ENDIF
							
							IF fEnterAnimPhaseTime >= 0.3
								IF NOT DOES_ENTITY_EXIST(cleaning_prop)
									eStoredCleaningPropModel = eCleaningPropModel
									
									cleaning_prop = CREATE_OBJECT(eCleaningPropModel, GET_PED_BONE_COORDS(pedIndex, eCleaningPropAttachBonetag, <<0,0,0>>))
									ATTACH_ENTITY_TO_ENTITY(cleaning_prop, pedIndex,
											GET_PED_BONE_INDEX(pedIndex, eCleaningPropAttachBonetag),
											<<0,0,0>>, <<0,0,0>>,
											TRUE, DEFAULT, TRUE)
								ENDIF
							ENDIF
							
							IF fEnterAnimPhaseTime >= 0.99
								bFinishedEnterAnim = TRUE
							ENDIF
						ELSE
							bFinishedEnterAnim = TRUE
						ENDIF
						
						IF bFinishedEnterAnim
							
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
								DETACH_SYNCHRONIZED_SCENE(iScene)
							ENDIF
							iScene = -1
							
							IF HAS_ANIM_DICT_LOADED(tCleaningBase_animDict)
	//							TASK_PLAY_ANIM_ADVANCED(PedIndex, tCleaningBase_animDict, tCleaningBase_animClip,
	//									vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
	//									INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eCleaning_animFlag | AF_HOLD_LAST_FRAME,
	//									0.0, EULER_YXZ, AIK_NONE)
								TASK_PLAY_ANIM(PedIndex, tCleaningBase_animDict, tCleaningBase_animClip,
	//									vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, eCleaning_animFlag | AF_HOLD_LAST_FRAME,
										0.0, DEFAULT, AIK_NONE)
								
							ELSE
								CASSERTLN(DEBUG_FAMILY, "missing base anim dict?")
							ENDIF
							
							iCleaningStage = iCLEANING_4_base
						ENDIF
					BREAK
					CASE iCLEANING_4_base
						#IF IS_DEBUG_BUILD
						str  =("dist_4_base: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						#ENDIF
						
						BOOL bFinishedBaseAnim
						bFinishedBaseAnim = FALSE
						IF IS_ENTITY_PLAYING_ANIM(PedIndex, tCleaningBase_animDict, tCleaningBase_animClip)
							FLOAT fBaseAnimPhaseTime
							fBaseAnimPhaseTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tCleaningBase_animDict, tCleaningBase_animClip)
							
							#IF IS_DEBUG_BUILD
							str  =("baseAnim: ")
							str +=GET_STRING_FROM_FLOAT(fBaseAnimPhaseTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, eColour)
							#ENDIF
							
							IF fBaseAnimPhaseTime >= 0.99
								bFinishedBaseAnim = TRUE
							ENDIF
						ELSE
							bFinishedBaseAnim = TRUE
						ENDIF
						
						IF bFinishedBaseAnim
							IF HAS_ANIM_DICT_LOADED(tCleaningExit_animDict)
	//							TASK_PLAY_ANIM_ADVANCED(PedIndex, tCleaningExit_animDict, tCleaningExit_animClip,
	//									vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
	//									INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, eCleaning_animFlag | AF_HOLD_LAST_FRAME,
	//									0.0, EULER_YXZ, AIK_NONE)
								TASK_PLAY_ANIM(PedIndex, tCleaningExit_animDict, tCleaningExit_animClip,
	//									vClean_Pos_x+vClean_offsets[iCleaningCount], <<0,0,fClean_Head_x+fClean_offsets[iCleaningCount]>>,
										INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, eCleaning_animFlag,
										0.0, DEFAULT, AIK_NONE)
								
							ELSE
								CASSERTLN(DEBUG_FAMILY, "missing exit anim dict?")
							ENDIF
							iCleaningStage = iCLEANING_5_exit
						ENDIF
					BREAK
					CASE iCLEANING_5_exit
						#IF IS_DEBUG_BUILD
						str  =("dist_5_exit: ")
						str +=GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(PedIndex),vClean_Pos_x+vClean_offsets[iCleaningCount]))
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, eColour)
						#ENDIF
						
						BOOL bFinishedExitAnim
						bFinishedExitAnim = FALSE
						IF IS_ENTITY_PLAYING_ANIM(PedIndex, tCleaningExit_animDict, tCleaningExit_animClip)
							FLOAT fExitAnimPhaseTime
							fExitAnimPhaseTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tCleaningExit_animDict, tCleaningExit_animClip)
							
							#IF IS_DEBUG_BUILD
							str  =("exitAnim: ")
							str +=GET_STRING_FROM_FLOAT(fExitAnimPhaseTime)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, eColour)
							#ENDIF
							
							IF fExitAnimPhaseTime >= 0.7
								IF DOES_ENTITY_EXIST(cleaning_prop)
									DELETE_OBJECT(cleaning_prop)
									eStoredCleaningPropModel = DUMMY_MODEL_FOR_SCRIPT
								ENDIF
							ENDIF
						ELSE
							bFinishedExitAnim = TRUE
						ENDIF
						
						IF bFinishedExitAnim
							SET_MODEL_AS_NO_LONGER_NEEDED(eCleaningPropModel)
							REMOVE_ANIM_DICT(tCleaningEnter_animDict)
							REMOVE_ANIM_DICT(tCleaningBase_animDict)
							REMOVE_ANIM_DICT(tCleaningExit_animDict)
							
							iCleaningCount++
							IF iCleaningCount >= COUNT_OF(vClean_offsets)
								iCleaningCount = 0
							ENDIF
							
							CLEAR_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
							
							iCleaningStage = iCLEANING_0_start
						ENDIF
					BREAK
				ENDSWITCH
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	bMovePeds = bMovePeds
	RETURN FALSE
ENDFUNC

//#IF IS_DEBUG_BUILD
//structTimer sDebugTimerForUpdatingFamilyEvents
//#ENDIF

FUNC BOOL PRIVATE_Update_Family_Event_For_Anim(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eUpdatedFamilyEvent)
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] <> eUpdatedFamilyEvent)
		
		
		TEXT_LABEL_63 tFamilyAnimDict, tFamilyAnimClip
		ANIMATION_FLAGS eFamilyAnimFlag
		enumFamilyAnimProgress eFamilyAnimProgress
		
		IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember],
				tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag,
				eFamilyAnimProgress)
			IF IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilyAnimDict, tFamilyAnimClip)
				FLOAT fFamilyAnimClip
				fFamilyAnimClip = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilyAnimDict, tFamilyAnimClip)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, " * family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] playing anim \"", tFamilyAnimDict, "\", \"", tFamilyAnimClip, "\" ", fFamilyAnimClip)
				#ENDIF
				
				IF fFamilyAnimClip >= 0.95
					g_eCurrentFamilyEvent[eFamilyMember] = eUpdatedFamilyEvent
					RETURN TRUE
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, " * family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] NOT playing anim \"", tFamilyAnimDict, "\", \"", tFamilyAnimClip, "\"")
				#ENDIF
		
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, " * family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] has no anim")
		#ENDIF
		
		RETURN FALSE
	ELSE
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, " * family_ped[", Get_String_From_FamilyMember(eFamilyMember), "] current event = ", Get_String_From_FamilyEvent(eUpdatedFamilyEvent))
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Wander(PED_INDEX PedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vSequencePos, FLOAT fSequenceHead)
	
	VECTOR vWanderOffset
	FLOAT fWanderHead
	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vWanderOffset, fWanderHead)
		
		//TASK_WANDER_STANDARD(PedIndex)
		
		IF NOT (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) = PERFORMING_TASK)
			
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_Wander")
			
			TASK_WANDER_IN_AREA(PedIndex, 
					vSequencePos+vWanderOffset,
					25.0)
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(vSequencePos+vWanderOffset,
					25.0, HUD_COLOUR_GREEN, 0.5)
			#ENDIF
			
		ENDIF
		
		fSequenceHead = fSequenceHead
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Facial(PED_INDEX PedIndex, TEXT_LABEL_63 tFamilySleepingDict, TEXT_LABEL_63 tFamilySleepingClip, ANIMATION_FLAGS AF_extraFacial = AF_DEFAULT)
	
	IF IS_STRING_NULL_OR_EMPTY(tFamilySleepingDict)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	HUD_COLOURS animHudColour = HUD_COLOUR_PINK
	#ENDIF
	
	REQUEST_ANIM_DICT(tFamilySleepingDict)
	IF NOT HAS_ANIM_DICT_LOADED(tFamilySleepingDict)
		REQUEST_ANIM_DICT(tFamilySleepingDict)
		
		#IF IS_DEBUG_BUILD
		str  = ("request: ")
		str += ("\"")
		str += (tFamilySleepingDict)
		str += ("\"")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, animHudColour)
		#ENDIF
		
		RETURN FALSE
	ELSE
		IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tFamilySleepingDict, tFamilySleepingClip)
			TASK_PLAY_ANIM(PedIndex, tFamilySleepingDict, tFamilySleepingClip,
					NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
					AF_SECONDARY | AF_extraFacial)
			
			#IF IS_DEBUG_BUILD
			str  = ("TASK_PLAY_ANIM \"")
//			str += (tFamilySleepingDict)
//			str += ("\", \"")
			str += (tFamilySleepingClip)
			str += ("\"")
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, animHudColour)
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str  = ("anim \"")
//		str += (tFamilySleepingDict)
//		str += ("\", \"")
		str += (tFamilySleepingClip)
		str += ("\" ")
		str += GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tFamilySleepingDict, tFamilySleepingClip))
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, animHudColour)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Sleeping(PED_INDEX PedIndex)
	/*
	Added to DICTIONARY: - facials@generic ANIM: - mood_Sleeping
	NOTE: This will be moved to a new dictionary structure at some point in the next month or so.
	*/

	TEXT_LABEL_63 tFamilySleepingDict	= "facials@gen_male@base"		//"facials@generic"
	IF NOT IS_PED_MALE(PedIndex)
		tFamilySleepingDict				= "facials@gen_female@base"
	ENDIF
	
	TEXT_LABEL_63 tFamilySleepingClip	= "mood_sleeping_1"			//"mood_Sleeping"
	
	RETURN PRIVATE_Update_Family_Facial(PedIndex, tFamilySleepingDict, tFamilySleepingClip, AF_LOOPING)
ENDFUNC


FUNC BOOL PRIVATE_SetFamilyMemberPedCapsule(enumFamilyMember eFamilyMember, FLOAT &fCapsuleRadius)
	//
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
		CASE FE_M_SON_on_ecstasy_AND_friendly
			fCapsuleRadius = (0.4) RETURN TRUE
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M_SON_Fighting_with_sister_B
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M_SON_Fighting_with_sister_C
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M_SON_Fighting_with_sister_D
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M7_SON_jumping_jacks
			fCapsuleRadius = (0.5) RETURN TRUE
		BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			fCapsuleRadius = (1.25) RETURN TRUE
		BREAK
		CASE FE_M_DAUGHTER_dancing_practice
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M2_MEXMAID_clean_window
		CASE FE_M7_MEXMAID_clean_window
		CASE FE_M_MEXMAID_MIC4_clean_window
			fCapsuleRadius = (0.5) RETURN TRUE
		BREAK
		CASE FE_M_MEXMAID_does_the_dishes
			fCapsuleRadius = (0.5) RETURN TRUE
		BREAK
		CASE FE_M2_WIFE_doing_yoga
		CASE FE_M7_WIFE_doing_yoga
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_M_GARDENER_mowing_lawn
			fCapsuleRadius = (1.0) RETURN TRUE
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			fCapsuleRadius = (0.50) RETURN TRUE
//		BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands
			fCapsuleRadius = (0.50) RETURN TRUE
		BREAK
		CASE FE_T0_MICHAEL_sunbathing
			fCapsuleRadius = (0.80) RETURN TRUE
		BREAK
		CASE FE_T0_RONEX_doing_target_practice
			fCapsuleRadius = (0.75) RETURN TRUE
		BREAK
		CASE FE_T0_TREVOR_smoking_crystal
			fCapsuleRadius = (1.5) RETURN TRUE
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			fCapsuleRadius = (1.25) RETURN TRUE
		BREAK
		#ENDIF
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			fCapsuleRadius = (0.8) RETURN TRUE
		BREAK
		
		CASE FE_T1_FLOYD_cleaning
			fCapsuleRadius = (0.50) RETURN TRUE
		BREAK
		
		CASE FE_ANY_find_family_event
		CASE FE_ANY_wander_family_event
			IF eFamilyMember = FM_TREVOR_1_FLOYD
			OR eFamilyMember = FM_TREVOR_1_FLOYD
				fCapsuleRadius = (0.30) RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	fCapsuleRadius = 0.0
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Is_Family_Sleeping(enumFamilyEvents eFamilyEvent)
	SWITCH eFamilyEvent
		CASE FE_M_SON_sleeping
		CASE FE_M_DAUGHTER_sleeping
		CASE FE_M_DAUGHTER_couchsleep
		CASE FE_M2_WIFE_sleeping
		CASE FE_M7_WIFE_sleeping
		CASE FE_T1_FLOYD_is_sleeping
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_GotoCoord(PED_INDEX PedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vSequencePos, FLOAT fSequenceHead, 
		TEXT_LABEL_63 &tSynchDict, TEXT_LABEL_63 &tSynchClip, INT &iScene,
		OBJECT_INDEX leftObj, OBJECT_INDEX rightObj,
		BOOL bMovePeds)
	
	#IF IS_DEBUG_BUILD
	IF bMovePeds
		RETURN TRUE
	ENDIF
	#ENDIF
	
	VECTOR vWanderOffset
	FLOAT fWanderHead
	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vWanderOffset, fWanderHead)
		
		CONST_FLOAT fFAMILY_GOTO_DIST_CHECK_RADIUS 4.0		//1.0
		CONST_FLOAT fFAMILY_GOTO_NAVMESH_RADIUS 			DEFAULT_NAVMESH_RADIUS
		
		IF VDIST2(GET_ENTITY_COORDS(PedIndex),vSequencePos+vWanderOffset) > (fFAMILY_GOTO_DIST_CHECK_RADIUS*fFAMILY_GOTO_DIST_CHECK_RADIUS)
			IF NOT (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)
				
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord")
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex,
						vSequencePos+vWanderOffset, PEDMOVEBLENDRATIO_WALK,
						DEFAULT_TIME_NEVER_WARP,
						fFAMILY_GOTO_NAVMESH_RADIUS,
						ENAV_DEFAULT,
						fSequenceHead+fWanderHead)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tSynchDict)
					
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - dict \"", tSynchDict, "\" is nulled")
					
					tSynchDict = ""
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(tSynchClip)
					
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - clip \"", tSynchClip, "\" is nulled")
					
					tSynchClip = ""
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
					DETACH_SYNCHRONIZED_SCENE(iScene)
				ENDIF
				iScene = -1
			ELSE
				#IF IS_DEBUG_BUILD
				DrawDebugFamilySphere(vSequencePos+vWanderOffset,
						fFAMILY_GOTO_DIST_CHECK_RADIUS, HUD_COLOUR_GREEN, 0.25)
				DrawDebugFamilyLine(vSequencePos+vWanderOffset,
						GET_ENTITY_COORDS(PedIndex),
						HUD_COLOUR_GREEN)
				DrawDebugFamilyTextWithOffset("go to coord", GET_ENTITY_COORDS(PedIndex), 4, HUD_COLOUR_GREEN)
				#ENDIF
				
			ENDIF
		ELSE
			IF NOT (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)

				IF DOES_ENTITY_EXIST(leftObj)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(leftObj, PedIndex)
						IF NOT IS_ENTITY_VISIBLE(leftObj)
							CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - SET_ENTITY_VISIBLE(leftObj)")
							SET_ENTITY_VISIBLE(leftObj, TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(rightObj)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(rightObj, PedIndex)
						IF NOT IS_ENTITY_VISIBLE(rightObj)
							CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - SET_ENTITY_VISIBLE(rightObj)")
							SET_ENTITY_VISIBLE(rightObj, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				FLOAT fCapsuleRadius
				IF PRIVATE_SetFamilyMemberPedCapsule(eFamilyMember, fCapsuleRadius)
				
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" SET_PED_CAPSULE ", fCapsuleRadius)
					#ENDIF
					
					SET_PED_CAPSULE(PedIndex, fCapsuleRadius)
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(leftObj)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(leftObj, PedIndex)
				IF IS_ENTITY_VISIBLE(leftObj)
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - SET_ENTITY_INVISIBLE(leftObj)")
					SET_ENTITY_VISIBLE(leftObj, FALSE)
				ENDIF
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(rightObj)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(rightObj, PedIndex)
				IF IS_ENTITY_VISIBLE(rightObj)
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_GotoCoord - SET_ENTITY_INVISIBLE(rightObj)")
					SET_ENTITY_VISIBLE(rightObj, FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		SET_PED_CAPSULE(PedIndex, 0.3)
		
		RETURN FALSE
	ENDIF
	
	bMovePeds = bMovePeds
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Hangup(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eUpdatedFamilyEvent,
		STRING strTextBlock, STRING strLabel, TEXT_LABEL &tCreatedConvLabels[],
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer, enumFamilyEvents &ePrevFamilyEvent,
		enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ""
	CONST_FLOAT Offset_y	3.0
	HUD_COLOURS animHudColour = HUD_COLOUR_GREEN
	#ENDIF
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] <> eUpdatedFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugFamilyStuff
		str  =("updated event <> ")
		str +=(Get_String_From_FamilyEvent(eUpdatedFamilyEvent))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y, animHudColour)
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_bUpdatedFamilyEvents
		OR (IS_KEYBOARD_KEY_PRESSED(KEY_LEFT) OR IS_KEYBOARD_KEY_PRESSED(KEY_RIGHT))
			str  =(" ## restart timer reset at ")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
			RETURN FALSE
		ENDIF
		#ENDIF
		
//		IF IS_ENTITY_OCCLUDED(PedIndex)
//			#IF IS_DEBUG_BUILD
//			IF g_bDrawDebug""FamilyStuff
//			str  =("PRIVATE_Update_Family_Hangup.IS_ENTITY_OCCLUDED()")
//			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
//			ENDIF
//			#ENDIF
//			
//			RETURN FALSE
//		ENDIF
		
		IF NOT IS_ENTITY_ON_SCREEN(PedIndex)

			#IF IS_DEBUG_BUILD
			IF g_bDrawDebugFamilyStuff
			str  =("PRIVATE_Update_Family_Hangup.IS_ENTITY_ON_SCREEN()")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		CONST_FLOAT	fCONST_min_hangup_dist	7.0
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) > (fCONST_min_hangup_dist*fCONST_min_hangup_dist)

			#IF IS_DEBUG_BUILD
			IF g_bDrawDebugFamilyStuff
			str  =("PRIVATE_Update_Family_Hangup.VDIST()")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex, FALSE), fCONST_min_hangup_dist, animHudColour, 0.1)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF IS_ANY_SPEECH_PLAYING(PedIndex)

			#IF IS_DEBUG_BUILD
			IF g_bDrawDebugFamilyStuff
			str  =("PRIVATE_Update_Family_Hangup.IS_ANY_SPEECH_PLAYING()")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strLabel)
			//play "hang up" dialogue
			
			INT iRandCount, iSpeechBit, iPlayerBitset
			Get_SpeechLabel_From_FamilyEvent(eUpdatedFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
			
			IF NOT IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
				CPRINTLN(DEBUG_FAMILY, "hangup - invalid IS_BITMASK_SET")
				RETURN FALSE
			ENDIF
			
			//is the speech a single line or is it a random conv
			IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(strLabel)
					IF Is_Ped_Playing_Family_Speech(strTextBlock, strLabel,
								inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
						RESTART_TIMER_NOW(speechTimer)
						g_eCurrentFamilyEvent[eFamilyMember] = eUpdatedFamilyEvent
						RETURN TRUE
					ENDIF
				ELSE
					CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Hangup(", Get_String_From_FamilyMember(eFamilyMember), ", current:", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), ", updated:", Get_String_From_FamilyEvent(eUpdatedFamilyEvent), ") - attempting to play an empty single random line??")
				ENDIF
			ELSE
				
				TEXT_LABEL returnedStrLabel = strLabel
				IF GetRandomConversationFromLabel(strTextBlock, returnedStrLabel, tCreatedConvLabels)
					
					IF NOT IS_STRING_NULL_OR_EMPTY(returnedStrLabel)
						IF Is_Ped_Playing_Family_Speech(strTextBlock, returnedStrLabel,
									inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
							RESTART_TIMER_NOW(speechTimer)
							g_eCurrentFamilyEvent[eFamilyMember] = eUpdatedFamilyEvent
							RETURN TRUE
						ENDIF
					ELSE
						CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Hangup(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), ", updated:", Get_String_From_FamilyEvent(eUpdatedFamilyEvent), ") - attempting to play an empty multiple random line??")
					ENDIF
				ENDIF
				
			ENDIF
		
			RETURN FALSE
		ENDIF
		
		g_eCurrentFamilyEvent[eFamilyMember] = eUpdatedFamilyEvent
		ePrevFamilyEvent = g_eCurrentFamilyEvent[eFamilyMember]
		RETURN TRUE
	ELSE
	
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugFamilyStuff
		str  =(" * family_ped[")
		str +=(Get_String_From_FamilyMember(eFamilyMember))
		str +=("] current event = ")
		str +=(Get_String_From_FamilyEvent(eUpdatedFamilyEvent))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y, animHudColour)
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Hangup_And_Wander(PED_INDEX PedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		INT &iHangupStage, OBJECT_INDEX &phoneProp, MODEL_NAMES &ePhonePropModel, BOOL bDeletePhoneProp)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ""
	CONST_FLOAT Offset_y	2.0
	HUD_COLOURS animHudColour = HUD_COLOUR_GREEN
	#ENDIF
	
	SWITCH iHangupStage
		CASE 0
		
			#IF IS_DEBUG_BUILD
			str  =("play hangup anim...")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+0.0, animHudColour)
			#ENDIF
			
			tAnimDict = ""
			tAnimDict = ""

			TEXT_LABEL_63 sHangup_animDict, sHangup_animClip
			ANIMATION_FLAGS eHangup_animFlag
			enumFamilyAnimProgress eFamilyAnimProgress
			
			IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
					sHangup_animDict, sHangup_animClip, eHangup_animFlag, eFamilyAnimProgress)
			ENDIF
			
			IF (eFamilyAnimProgress <> FAP_2_dialogue)
				REQUEST_ANIM_DICT(sHangup_animDict)
				IF NOT HAS_ANIM_DICT_LOADED(sHangup_animDict)
					#IF IS_DEBUG_BUILD
					str  =("requesting \"")
					str +=(sHangup_animDict)
					str +=("\"")
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
					#ENDIF
					
					RETURN FALSE
				ENDIF
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eHangup_animFlag, AF_LOOPING)
					CLEAR_BITMASK_ENUM_AS_ENUM(eHangup_animFlag, AF_LOOPING)
				ELSE
					//
				ENDIF
				
				TASK_PLAY_ANIM(PedIndex, sHangup_animDict, sHangup_animClip,
							NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, eHangup_animFlag)
				tAnimDict = sHangup_animDict
				tAnimClip = sHangup_animClip
				
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_FamilyMember_Anim(\"", tAnimDict, "\", \"", tAnimClip, "\")")
				
				iHangupStage = 1
			ELSE
				IF PRIVATE_Update_Family_DialogueAnim(PedIndex, eFamilyMember, eFamilyEvent,
						tAnimDict, tAnimClip,
						vFamilyScenePos, fFamilySceneHead)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_DialogueAnim(\"", tAnimDict, "\", \"", tAnimClip, ")	//", Get_String_From_FamilyEvent(eFamilyEvent))
					#ENDIF
					
					iHangupStage = 1
				ELSE
					#IF IS_DEBUG_BUILD
					str  =("waiting on dialogue anim...")
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
		
			#IF IS_DEBUG_BUILD
			str  =("playing hangup anim \"")
			str +=(tAnimClip)
			str +=("\" ")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+0.0, animHudColour)
			#ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip)
				FLOAT fHangupAnimCurrentTime
				fHangupAnimCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tAnimDict, tAnimClip)
				
				#IF IS_DEBUG_BUILD
				str  =("anim time:")
				str += GET_STRING_FROM_FLOAT(fHangupAnimCurrentTime)
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+1.0, animHudColour)
				#ENDIF
				
				IF fHangupAnimCurrentTime >= 1.0
					CLEAR_PED_TASKS(PedIndex)
				ENDIF
				
				IF DOES_ENTITY_EXIST(phoneProp)
					
					STRING pEventName
					pEventName = "hangup"
					FLOAT ReturnStartPhase, ReturnEndPhase
					IF FIND_ANIM_EVENT_PHASE(tAnimDict, tAnimClip, pEventName,
							ReturnStartPhase, ReturnEndPhase)
					ELSE
						ReturnStartPhase = -1
						
						IF ARE_STRINGS_EQUAL(tAnimDict, "TIMETABLE@MAID@IG_8@")
							IF ARE_STRINGS_EQUAL(tAnimClip, "IG_8_IDLE_A")
								ReturnStartPhase = 0.85
							ENDIF
						ENDIF
						
						IF ReturnStartPhase < 0
							#IF IS_DEBUG_BUILD
							CASSERTLN(DEBUG_FAMILY, "\"", pEventName, "\" missing from \"", tAnimDict, "/", tAnimClip, "\"")
							#ENDIF
							
							ReturnStartPhase = 0.5
						ENDIF
					ENDIF
					
					IF fHangupAnimCurrentTime < ReturnStartPhase
						#IF IS_DEBUG_BUILD
						IF bDeletePhoneProp
							str  =("delete time:")
						ELSE
							str  =("detach time:")
						ENDIF
						str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+2.0, animHudColour)
						#ENDIF
					ELSE
					
						#IF IS_DEBUG_BUILD
						IF bDeletePhoneProp
							str  =("deleting:")
						ELSE
							str  =("detaching:")
						ENDIF
						str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+2.0, animHudColour)
						#ENDIF
						
						IF IS_ENTITY_ATTACHED(phoneProp)
							DETACH_ENTITY(phoneProp)
						ENDIF
					
						IF bDeletePhoneProp
							ePhonePropModel = DUMMY_MODEL_FOR_SCRIPT
							DELETE_OBJECT(phoneProp)
						ENDIF
					ENDIF
					
					
				ELSE
					#IF IS_DEBUG_BUILD
					IF bDeletePhoneProp
						str  =("deleted (no prop)")
					ELSE
						str  =("detached (no prop)")
					ENDIF
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+2.0, animHudColour)
					#ENDIF
				ENDIF
				
			ELSE
				
				IF DOES_ENTITY_EXIST(phoneProp)
					IF IS_ENTITY_ATTACHED(phoneProp)
						DETACH_ENTITY(phoneProp)
					ENDIF
				
					IF bDeletePhoneProp
						ePhonePropModel = DUMMY_MODEL_FOR_SCRIPT
						DELETE_OBJECT(phoneProp)
					ENDIF
				ENDIF
				
				tAnimDict = ""
				tAnimClip = ""
				iHangupStage = 2
			ENDIF
			
		BREAK
		CASE 2
		
			#IF IS_DEBUG_BUILD
			str  =("..wander")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+0.0, animHudColour)
			#ENDIF
			
			IF bDeletePhoneProp
				IF DOES_ENTITY_EXIST(phoneProp)
					CASSERTLN(DEBUG_FAMILY, "phoneProp exists???")
				ENDIF
			ENDIF
			
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			iHangupStage = 0
			RETURN TRUE
		BREAK
		
		
		DEFAULT
		
			#IF IS_DEBUG_BUILD
			str  =("DEFAULT ")
			str +=iHangupStage
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), Offset_y+0.0, animHudColour)
			CASSERTLN(DEBUG_FAMILY, str)
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Trigger_Family_Argue(PED_INDEX PedIndex, enumFamilyEvents eFamilyEvent, VECTOR vSequencePos, FLOAT fSequenceHead)
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		RETURN TRUE
	ENDIF
	HUD_COLOURS eHudColour = HUD_COLOUR_ORANGELIGHT
	#ENDIF
	CONST_FLOAT fDEFAULT_ArgueRad	4.0
	
	SWITCH eFamilyEvent
		CASE FE_M2_SON_gaming_loop
//			CONST_FLOAT fSON_gaming_ArgueRad	fDEFAULT_ArgueRad
//			
//			#IF IS_DEBUG_BUILD
//			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fSON_gaming_ArgueRad, eHudColour, 0.25)
//			#ENDIF
//			
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fSON_gaming_ArgueRad)*(fSON_gaming_ArgueRad))
//				RETURN TRUE
//			ENDIF
			
		/*	IF IS_THIS_TV_ON(TV_LOC_JIMMY_BEDROOM)
				IF NOT IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
					CASSERTLN(DEBUG_FAMILY, "NOT IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)")
					RETURN TRUE
				ENDIF
				
				//#1445897
				IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
					CASSERTLN(DEBUG_FAMILY, "IS_TVSHOW_CURRENTLY_PLAYING")
					RETURN TRUE
				ENDIF
			ENDIF*/
			
			RETURN FALSE
		BREAK
		CASE FE_M7_SON_gaming
			CONST_FLOAT fSON_gaming_ArgueRad	fDEFAULT_ArgueRad
			
		/*	#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fSON_gaming_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fSON_gaming_ArgueRad)*(fSON_gaming_ArgueRad))
				RETURN TRUE
			ENDIF
			
			IF IS_THIS_TV_ON(TV_LOC_JIMMY_BEDROOM)
				IF NOT IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
					CASSERTLN(DEBUG_FAMILY, "NOT IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)")
					RETURN TRUE
				ENDIF
			ENDIF	*/
			
			RETURN FALSE
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_B	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_C	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_D	
			CONST_FLOAT fSON_Fighting_with_sister_ArgueRad			7.5
			CONST_FLOAT fSON_Fighting_with_sister_ArgueRad_VEHICLE	15.0
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fSON_Fighting_with_sister_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fSON_Fighting_with_sister_ArgueRad_VEHICLE)*(fSON_Fighting_with_sister_ArgueRad_VEHICLE))
					RETURN TRUE
				ENDIF
			ELSE
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fSON_Fighting_with_sister_ArgueRad)*(fSON_Fighting_with_sister_ArgueRad))
					RETURN TRUE
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE FE_M_DAUGHTER_screaming_at_dad		FALLTHRU	
			CONST_FLOAT fDAUGHTER_screaming_at_dad_ArgueRad		8.5
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fDAUGHTER_screaming_at_dad_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fDAUGHTER_screaming_at_dad_ArgueRad)*(fDAUGHTER_screaming_at_dad_ArgueRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
//		CASE FE_M_WIFE_screaming_at_son_P1		FALLTHRU
		CASE FE_M_WIFE_screaming_at_son_P2		FALLTHRU
		CASE FE_M_WIFE_screaming_at_son_P3		FALLTHRU	
			CONST_FLOAT fWIFE_screaming_at_son_ArgueRad			5.0
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fWIFE_screaming_at_son_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fWIFE_screaming_at_son_ArgueRad)*(fWIFE_screaming_at_son_ArgueRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_M_WIFE_screaming_at_daughter	
			CONST_FLOAT fWIFE_screaming_at_daughter_ArgueRad	5.0
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fWIFE_screaming_at_daughter_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fWIFE_screaming_at_daughter_ArgueRad)*(fWIFE_screaming_at_daughter_ArgueRad))
				RETURN TRUE
			ENDIF
			
			VECTOR WIFE_screaming_at_daughter_VecCentre, WIFE_screaming_at_daughter_VecBounds
			FLOAT WIFE_screaming_at_daughter_fAngle
			WIFE_screaming_at_daughter_VecCentre = <<-0.4200, 0.6800, 1.0000>>
			WIFE_screaming_at_daughter_VecBounds = <<4.5000, 2.7500, 1.0000>>
			WIFE_screaming_at_daughter_fAngle = -22.0000
			
		/*	START_WIDGET_GROUP("FE_M_WIFE_screaming_at_daughter")
				ADD_WIDGET_VECTOR_SLIDER("VecCentre", WIFE_screaming_at_daughter_VecCentre, -20.0, 20.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("VecBounds", WIFE_screaming_at_daughter_VecBounds, 0.5, 20.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("fAngle", WIFE_screaming_at_daughter_fAngle, -360.0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			WHILE TRUE
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						vSequencePos+WIFE_screaming_at_daughter_VecCentre, WIFE_screaming_at_daughter_VecBounds, fSequenceHead+WIFE_screaming_at_daughter_fAngle)
				ENDIF
				IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screaming_at_daughter_VecCentre = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_screaming_at_daughter_VecCentre)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screaming_at_daughter_VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_screaming_at_daughter_VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screaming_at_daughter_fAngle = ")SAVE_FLOAT_TO_DEBUG_FILE(WIFE_screaming_at_daughter_fAngle)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					RETURN TRUE
				ENDIF
				WAIT(0)
			ENDWHILE */
			
			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					vSequencePos+WIFE_screaming_at_daughter_VecCentre, WIFE_screaming_at_daughter_VecBounds, fSequenceHead+WIFE_screaming_at_daughter_fAngle)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_M_WIFE_screams_at_mexmaid	
			CONST_FLOAT fWIFE_screams_at_mexmaid_ArgueRad	3.0
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fWIFE_screams_at_mexmaid_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fWIFE_screams_at_mexmaid_ArgueRad)*(fWIFE_screams_at_mexmaid_ArgueRad))
				RETURN TRUE
			ENDIF
			
			VECTOR WIFE_screams_at_mexmaid_VecCentre, WIFE_screams_at_mexmaid_VecBounds
			FLOAT WIFE_screams_at_mexmaid_fAngle
			WIFE_screams_at_mexmaid_VecCentre = <<10.8751, 2.6168, 1.4700>>
			WIFE_screams_at_mexmaid_VecBounds = <<7.5000, 3.0000, 1.5000>>
			WIFE_screams_at_mexmaid_fAngle = -21.0000
			
		/*	START_WIDGET_GROUP("FE_M_WIFE_screams_at_mexmaid")
				ADD_WIDGET_VECTOR_SLIDER("VecCentre", WIFE_screams_at_mexmaid_VecCentre, -20.0, 20.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("VecBounds", WIFE_screams_at_mexmaid_VecBounds, 0.5, 20.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("fAngle", WIFE_screams_at_mexmaid_fAngle, -360.0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			WHILE TRUE
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						vSequencePos+WIFE_screams_at_mexmaid_VecCentre, WIFE_screams_at_mexmaid_VecBounds, fSequenceHead+WIFE_screams_at_mexmaid_fAngle)
				ENDIF
				IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screams_at_mexmaid_VecCentre = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_screams_at_mexmaid_VecCentre)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screams_at_mexmaid_VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_screams_at_mexmaid_VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_screams_at_mexmaid_fAngle = ")SAVE_FLOAT_TO_DEBUG_FILE(WIFE_screams_at_mexmaid_fAngle)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					RETURN TRUE
				ENDIF
				WAIT(0)
			ENDWHILE */
			
			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					vSequencePos+WIFE_screams_at_mexmaid_VecCentre, WIFE_screams_at_mexmaid_VecBounds, fSequenceHead+WIFE_screams_at_mexmaid_fAngle)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE FE_M2_WIFE_with_shopping_bags_enter	FALLTHRU
		CASE FE_M7_WIFE_with_shopping_bags_enter	FALLTHRU
		CASE FE_M7_WIFE_shopping_with_daughter		//FALLTHRU
//		CASE FE_M7_WIFE_shopping_with_son
			DOOR_DATA_STRUCT sData_M_Mansion_F_L, sData_M_Mansion_F_R
			sData_M_Mansion_F_L = GET_DOOR_DATA(DOORNAME_M_MANSION_F_L)
			sData_M_Mansion_F_R = GET_DOOR_DATA(DOORNAME_M_MANSION_F_R)
			
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData_M_Mansion_F_L.doorHash)
			OR NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData_M_Mansion_F_R.doorHash)
				//ADD_DOOR_TO_SYSTEM(sData_M_Mansion_F_L.doorHash, ObjectModel, vecPos)
				
				#IF IS_DEBUG_BUILD
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData_M_Mansion_F_L.doorHash)
					DrawDebugFamilyTextWithOffset("sData_M_Mansion_F_L not in system", sData_M_Mansion_F_L.coords, 0, HUD_COLOUR_RED)
				ENDIF
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sData_M_Mansion_F_R.doorHash)
					DrawDebugFamilyTextWithOffset("sData_M_Mansion_F_R not in system", sData_M_Mansion_F_R.coords, 0, HUD_COLOUR_RED)
				ENDIF
				#ENDIF
				
				RETURN FALSE
			ELSE
				//
				
				FLOAT fDoorLeftOpenRatio, fDoorRightOpenRatio
				fDoorLeftOpenRatio = ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(sData_M_Mansion_F_L.doorHash))
				fDoorRightOpenRatio = ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(sData_M_Mansion_F_R.doorHash))
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str
				str  = "Mansion_F_L open:"
				str += GET_STRING_FROM_FLOAT(fDoorLeftOpenRatio)
				DrawDebugFamilyTextWithOffset(str, sData_M_Mansion_F_L.coords, 0, HUD_COLOUR_RED)
				
				str  = "Mansion_F_R open:"
				str += GET_STRING_FROM_FLOAT(fDoorRightOpenRatio)
				DrawDebugFamilyTextWithOffset(str, sData_M_Mansion_F_R.coords, 0, HUD_COLOUR_RED)
				#ENDIF
				
				IF fDoorLeftOpenRatio >= 0.1
				OR fDoorRightOpenRatio >= 0.1
					RETURN FALSE
				ENDIF
			ENDIF
			
			VECTOR WIFE_with_shopping_VecCentre, WIFE_with_shopping_VecBounds
			FLOAT WIFE_with_shopping_fAngle
			WIFE_with_shopping_VecCentre = <<-0.72, 0.52, 1.0>>
			WIFE_with_shopping_VecBounds = <<3.0, 3.0, 1.0>>
			WIFE_with_shopping_fAngle = -22.00
			
		/*	START_WIDGET_GROUP("FE_M_WIFE_with_shopping")
				ADD_WIDGET_VECTOR_SLIDER("VecCentre", WIFE_with_shopping_VecCentre, -20.0, 20.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("VecBounds", WIFE_with_shopping_VecBounds, 0.5, 20.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("fAngle", WIFE_with_shopping_fAngle, -360.0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			WHILE TRUE
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						vSequencePos+WIFE_with_shopping_VecCentre, WIFE_with_shopping_VecBounds, fSequenceHead+WIFE_with_shopping_fAngle)
				ENDIF
				IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_with_shopping_VecCentre = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_with_shopping_VecCentre)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_with_shopping_VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(WIFE_with_shopping_VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("WIFE_with_shopping_fAngle = ")SAVE_FLOAT_TO_DEBUG_FILE(WIFE_with_shopping_fAngle)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					RETURN TRUE
				ENDIF
				WAIT(0)
			ENDWHILE */
			
			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					vSequencePos+WIFE_with_shopping_VecCentre, WIFE_with_shopping_VecBounds, fSequenceHead+WIFE_with_shopping_fAngle)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE FE_M7_WIFE_Making_juice
			CONST_FLOAT fWIFE_Making_juiceRad	fDEFAULT_ArgueRad
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fWIFE_Making_juiceRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fWIFE_Making_juiceRad)*(fWIFE_Making_juiceRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_T1_FLOYD_pineapple
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fDEFAULT_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fDEFAULT_ArgueRad)*(fDEFAULT_ArgueRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK

		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator	FALLTHRU
		CASE FE_M7_WIFE_using_vibrator
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fDEFAULT_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fDEFAULT_ArgueRad)*(fDEFAULT_ArgueRad))
				RETURN TRUE
			ENDIF
			
			MODEL_NAMES ObjectModel
			VECTOR vecPos
			DOOR_NAME_ENUM eDoorName
			DOOR_HASH_ENUM eDoorHash
			TEXT_LABEL_63 roomName
			BOOL bIgnoreInRoomCheck
			
			PRIVATE_Get_Family_Door_Attributes(FD_7_MICHAEL_BEDROOM,
					ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
			
			IF (eDoorName <> DUMMY_DOORNAME)
				//do nothing...
				CPRINTLN(DEBUG_FAMILY, "do nothing...")
			ELSE
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(eDoorHash), ObjectModel, vecPos)
					
					CPRINTLN(DEBUG_FAMILY, "ADD_DOOR_TO_SYSTEM()")
				ELSE
					//
					
					FLOAT fDoorOpenRatio
					fDoorOpenRatio = ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(eDoorHash)))
					
					CPRINTLN(DEBUG_FAMILY, "fDoorOpenRatio: ", fDoorOpenRatio)
					
					IF fDoorOpenRatio >= 0.5
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		#ENDIF
		CASE FE_T0_RON_ranting_about_government_LAYING
		CASE FE_T0_RONEX_working_a_moonshine_sill
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fDEFAULT_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fDEFAULT_ArgueRad)*(fDEFAULT_ArgueRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(GET_ENTITY_COORDS(PedIndex), fDEFAULT_ArgueRad, eHudColour, 0.25)
			#ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedIndex)) < ((fDEFAULT_ArgueRad)*(fDEFAULT_ArgueRad))
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
			VECTOR FLOYD_hiding_from_Trevor_VecCentre, FLOYD_hiding_from_Trevor_VecBounds
			FLOAT FLOYD_hiding_from_Trevor_fAngle
			FLOYD_hiding_from_Trevor_VecCentre = <<-0.0, 0.0, 1.0>>
			FLOYD_hiding_from_Trevor_VecBounds = <<2.0, 2.0, 1.0>>
			FLOYD_hiding_from_Trevor_fAngle = -0.00
			
		/*	START_WIDGET_GROUP("FE_M_FLOYD_hiding_from_Trevor")
				ADD_WIDGET_VECTOR_SLIDER("VecCentre", FLOYD_hiding_from_Trevor_VecCentre, -20.0, 20.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("VecBounds", FLOYD_hiding_from_Trevor_VecBounds, 0.5, 20.0, 0.5)
				ADD_WIDGET_FLOAT_SLIDER("fAngle", FLOYD_hiding_from_Trevor_fAngle, -360.0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			WHILE TRUE
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						vSequencePos+FLOYD_hiding_from_Trevor_VecCentre, FLOYD_hiding_from_Trevor_VecBounds, fSequenceHead+FLOYD_hiding_from_Trevor_fAngle)
				ENDIF
				IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("FLOYD_hiding_from_Trevor_VecCentre = ")SAVE_VECTOR_TO_DEBUG_FILE(FLOYD_hiding_from_Trevor_VecCentre)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("FLOYD_hiding_from_Trevor_VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(FLOYD_hiding_from_Trevor_VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("FLOYD_hiding_from_Trevor_fAngle = ")SAVE_FLOAT_TO_DEBUG_FILE(FLOYD_hiding_from_Trevor_fAngle)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					RETURN TRUE
				ENDIF
				WAIT(0)
			ENDWHILE */
			
			IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
					vSequencePos+FLOYD_hiding_from_Trevor_VecCentre, FLOYD_hiding_from_Trevor_VecBounds, fSequenceHead+FLOYD_hiding_from_Trevor_fAngle)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Trigger_Family_Argue()")
	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Trigger_Family_Argue()")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING Get_Family_Argue_Exit_Clip(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	SWITCH eFamilyEvent
		CASE FE_M2_SON_gaming_loop
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					RETURN "JIM_IG_2_P2_Lagging"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_gaming
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					RETURN "IG_2_Exit"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_SON_Fighting_with_sister_A
		CASE FE_M_SON_Fighting_with_sister_B
		CASE FE_M_SON_Fighting_with_sister_C
		CASE FE_M_SON_Fighting_with_sister_D
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					RETURN "EXIT_JIMMY"
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					RETURN "EXIT_TRACY"
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_M_WIFE_screaming_at_son_P1
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE 
//					RETURN "IG_2_YouKnowWhat_AMANDA"
//				BREAK
//				CASE FM_MICHAEL_SON
//					RETURN "IG_2_YouKnowWhat_JIMMY"
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG_2_P2_ItsNotWastingTime_AMANDA"
				BREAK
				CASE FM_MICHAEL_SON
					RETURN "IG_2_P2_ItsNotWastingTime_JIMMY"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_screaming_at_son_P3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG2_P3_ItsNotABigDeal_AMANDA"
				BREAK
				CASE FM_MICHAEL_SON
					RETURN "IG2_P3_ItsNotABigDeal_JIMMY"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG_3_WhenURPregnant_AMANDA"
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					RETURN "IG_3_WhenURPregnant_TRACY"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG_9_IAmOnToYou_AMANDA"
				BREAK
				CASE FM_MICHAEL_MEXMAID
					RETURN "IG_9_IAmOnToYou_MAID"
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG_7_EXIT"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_Making_juice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "Ugh_kale_Amanda"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_pineapple
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					RETURN "Pineapple_EXIT_LOOP_FLOYD"
				BREAK
			ENDSWITCH
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M7_WIFE_using_vibrator
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					RETURN "IG_6_EXIT"
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		CASE FE_M_DAUGHTER_screaming_at_dad
			IF (eFamilyEvent = FE_M_DAUGHTER_screaming_at_dad)
				MODEL_NAMES PedModel
				PedModel = GET_ENTITY_MODEL(PedIndex)
				
				IF (PedModel = GET_NPC_PED_MODEL(CHAR_TRACEY))
					RETURN "EXIT_TRACY"
				ELIF (PedModel = A_M_Y_HIPSTER_01)
					RETURN "EXIT_BOY"
				ENDIF
			ENDIF
		BREAK
		
		CASE FE_T0_RON_ranting_about_government_LAYING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					RETURN "ENTER"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					RETURN "IG_3_IDLE_A"
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					RETURN "base"
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT
			//
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " for Get_Family_Argue_Exit_Clip(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " for Get_Family_Argue_Exit_Clip(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
	#ENDIF
	
	eFamilyMember = eFamilyMember
	eFamilyEvent = eFamilyEvent
	
	RETURN "null"
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Argue(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		enumFamilyEvents eFamilyEvent, VECTOR vSequencePos, FLOAT fSequenceHead,
		INT &iArgueStage, INT &iScene,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		enumFamilyEvents eDesiredFamilyEvent,
		STRING strTextBlock, STRING strLoop, STRING strExit, TEXT_LABEL &tCreatedConvLabels[],
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer, FLOAT fExitDialogueTime = 0.0,
		enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH)
	
	#IF IS_DEBUG_BUILD
	
	IF (eDesiredFamilyEvent = INT_TO_ENUM(enumFamilyEvents, 0))
		eDesiredFamilyEvent = NO_FAMILY_EVENTS
	ENDIF
	
	CONST_INT iFamTextOffset 3
	TEXT_LABEL_63 str
	#ENDIF
	
	CONST_INT iARGUE_0_argue					0
	CONST_INT iARGUE_1_playExit					1
	CONST_INT iARGUE_2_performingSynch			2
	CONST_INT iARGUE_3_playExit					3
	CONST_INT iARGUE_33_playExitWaitForSpeech	33
	CONST_INT iARGUE_4_wanderArea				4
	CONST_INT iARGUE_5_updateEvent				5
	
	SWITCH iArgueStage
		CASE iARGUE_0_argue		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("0. wait for argue", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				IF PRIVATE_Trigger_Family_Argue(PedIndex, eFamilyEvent, vSequencePos, fSequenceHead)
					
					IF IS_STRING_NULL_OR_EMPTY(tAnimDict)
						CWARNINGLN(DEBUG_FAMILY, "PRIVATE_Update_Family_Argue(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ") tAnimDict has not been initialised")
						
						PRIVATE_Update_Family_SynchScene(PedIndex, eFamilyMember, eFamilyEvent, vSequencePos, fSequenceHead, iScene, TRUE, FALSE, tAnimDict, tAnimClip)
						
						RETURN FALSE
					ENDIF
					
					
					IF IS_STRING_NULL_OR_EMPTY(strTextBlock)
					AND IS_STRING_NULL_OR_EMPTY(strExit)
						SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
						
						CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_1_playExit - ARGUE (silent)!")
						
						iArgueStage = iARGUE_1_playExit
						RETURN FALSE
					ENDIF
					
					INT iRandCount, iSpeechBit, iPlayerBitset
					Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
					
					IF NOT IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
						CPRINTLN(DEBUG_FAMILY, "argue - invalid IS_BITMASK_SET: ", GET_CURRENT_PLAYER_PED_STRING())
					ELSE
						
						//is the speech a single line or is it a random conv
						IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
							
							BOOL bReadyForPlayExit
							bReadyForPlayExit = FALSE
							IF NOT IS_STRING_NULL_OR_EMPTY(strExit)
								IF Is_Ped_Playing_Family_Speech(strTextBlock, strExit,
										inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
									RESTART_TIMER_NOW(speechTimer)
									bReadyForPlayExit = TRUE
								ENDIF
							ELSE
								IF (eFamilyEvent = FE_T1_FLOYD_pineapple)
									CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Argue(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ") - attempting to play an empty single random line...")
									
									bReadyForPlayExit = TRUE
								ELSE
									CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Argue(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ") - attempting to play an empty single random line??")
								ENDIF
							ENDIF
							
							IF (fExitDialogueTime <> 0)
								bReadyForPlayExit = TRUE
							ENDIF
							
							IF bReadyForPlayExit
								SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
								
								CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_1_playExit - ARGUE (random)!")
								
								iArgueStage = iARGUE_1_playExit
								RETURN FALSE
							ENDIF
						ELSE
							TEXT_LABEL returnedStrLabel
							returnedStrLabel = strExit
							IF GetRandomConversationFromLabel(strTextBlock, returnedStrLabel, tCreatedConvLabels)
								
								IF NOT IS_STRING_NULL_OR_EMPTY(returnedStrLabel)
									IF Is_Ped_Playing_Family_Speech(strTextBlock, returnedStrLabel,
											inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
										RESTART_TIMER_NOW(speechTimer)
										
										SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
										
										CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_1_playExit - ARGUE (non-random)!")
										
										iArgueStage = iARGUE_1_playExit
										RETURN FALSE
									ENDIF
								ELSE
									CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Argue(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ") - attempting to play an empty multiple random line??")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iScene)
					
					CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_1_playExit - non-looped...")
					
					iArgueStage = iARGUE_1_playExit
					RETURN FALSE
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(strLoop)
					//
					
					CPRINTLN(DEBUG_FAMILY, "play \"", strLoop, "\"...")
					
					IF NOT IS_TIMER_STARTED(speechTimer)
						START_TIMER_NOW(speechTimer)
					ENDIF
					
					TEXT_LABEL returnedStrLabel
					returnedStrLabel = strLoop
					IF GetRandomConversationFromLabel(strTextBlock, returnedStrLabel, tCreatedConvLabels)
						INT iCurrentRandCount
						Play_This_Family_Speech(PedIndex, eFamilyEvent,
								inSpeechStruct, strTextBlock, tCreatedConvLabels,
								speechTimer, iCurrentRandCount,
								10, returnedStrLabel)
					ENDIF
				ENDIF
			ENDIF
			
			FLOAT LoopblendInDelta, exitblendOutDelta
			LoopblendInDelta	= NORMAL_BLEND_IN
			exitblendOutDelta	= NORMAL_BLEND_OUT
			
			IF (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_a)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_b)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_c)
				LoopblendInDelta	= NORMAL_BLEND_IN
				exitblendOutDelta	= SLOW_BLEND_OUT
			ENDIF
			
			IF NOT PRIVATE_Update_Family_SynchScene(PedIndex, eFamilyMember, eFamilyEvent,
					vSequencePos, fSequenceHead,
					iScene, TRUE, FALSE, tAnimDict, tAnimClip, 
					LoopblendInDelta, exitblendOutDelta)
				RETURN FALSE
			ENDIF
		BREAK
		CASE iARGUE_1_playExit
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("1. play exit, wait for speech", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF IS_STRING_NULL_OR_EMPTY(tAnimDict)
				TEXT_LABEL_63 tTempFamilyAnimClip
				
				ANIMATION_FLAGS eFamilyAnimFlag
				enumFamilyAnimProgress eFamilyAnimProgress
				
				PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
						tAnimDict, tTempFamilyAnimClip,
						eFamilyAnimFlag, eFamilyAnimProgress)
				
				REQUEST_ANIM_DICT(tAnimDict)
				IF NOT HAS_ANIM_DICT_LOADED(tAnimDict)
					CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_1_playExit - waiting for \"", tAnimDict, "\"")
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				VECTOR vSynchSceneOffset
				FLOAT fSynchSceneHead
				vSynchSceneOffset = <<0,0,0>>
				fSynchSceneHead = 0
				IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
					iScene = CREATE_SYNCHRONIZED_SCENE(vSequencePos+vSynchSceneOffset, <<0,0,fSequenceHead+fSynchSceneHead>>)
				ENDIF
			ENDIF
			
			STRING sFamilyArgueExitClip
			sFamilyArgueExitClip = Get_Family_Argue_Exit_Clip(PedIndex, eFamilyMember, eFamilyEvent)
			
			FLOAT blendInDelta, blendOutDelta
			SYNCED_SCENE_PLAYBACK_FLAGS flags 
			RAGDOLL_BLOCKING_FLAGS ragdollFlags
			
			blendInDelta	= NORMAL_BLEND_IN
			blendOutDelta	= WALK_BLEND_OUT
			flags 			= SYNCED_SCENE_USE_PHYSICS
			ragdollFlags	= RBF_PLAYER_IMPACT
			
			IF (eFamilyEvent = FE_M_WIFE_screams_at_mexmaid)
				blendInDelta	= INSTANT_BLEND_IN
			ENDIF
			
			IF (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_a)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_b)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_c)
				blendInDelta	= SLOW_BLEND_IN
			ENDIF
			
			IF (eDesiredFamilyEvent = NO_FAMILY_EVENTS)
				flags 			|= SYNCED_SCENE_TAG_SYNC_OUT
			ELIF (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_a)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_b)
			OR (eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_c)
			OR (eFamilyEvent = FE_M7_WIFE_Making_juice)		//#1588065
			OR (eFamilyEvent = FE_T0_RONEX_working_a_moonshine_sill)
			OR (eFamilyEvent = FE_T1_FLOYD_pineapple)		//#1590017
				blendOutDelta	= WALK_BLEND_OUT
			ELSE
				blendOutDelta	= INSTANT_BLEND_OUT
			ENDIF
			
			IF eFamilyEvent = FE_M_WIFE_screaming_at_son_P2
				IF eFamilyMember = FM_MICHAEL_WIFE
					blendOutDelta	= WALK_BLEND_OUT
				ENDIF
			ELIF eFamilyEvent = FE_M_WIFE_screaming_at_son_P3
				IF eFamilyMember = FM_MICHAEL_WIFE
					blendOutDelta	= WALK_BLEND_OUT
				ENDIF
			ELIF eFamilyEvent = FE_M_WIFE_screaming_at_daughter
				IF eFamilyMember = FM_MICHAEL_WIFE
					blendOutDelta	= WALK_BLEND_OUT
				ENDIF
//			ELIF eFamilyEvent = FE_M_WIFE_screaming_at_son_P1
//				IF eFamilyMember = FM_MICHAEL_WIFE
//					blendOutDelta	= WALK_BLEND_OUT
//				ENDIF
			ENDIF
			
			TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
					tAnimDict, sFamilyArgueExitClip,
					blendInDelta, blendOutDelta, flags, ragdollFlags)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			
			SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
			IF eFamilyEvent = FE_M7_WIFE_Making_juice
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, FALSE)
			ELSE
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
			ENDIF
			tAnimClip = sFamilyArgueExitClip
			
			SET_PED_CONFIG_FLAG(PedIndex, PCF_UseKinematicModeWhenStationary, TRUE)
			
			CPRINTLN(DEBUG_FAMILY, "iArgueStage = iARGUE_2_performingSynch \"", tAnimDict, "/", sFamilyArgueExitClip, "\"")
			
			iArgueStage = iARGUE_2_performingSynch
			RETURN FALSE
		BREAK
		CASE iARGUE_2_performingSynch		//
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				str  = ("creating SynchScene")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_REDLIGHT)
			ELSE
				str  = ("SynchScene phase: ")
				str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_BLUELIGHT)
			ENDIF
			DrawDebugFamilyTextWithOffset("2. wait to start playing argue", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK)
				iArgueStage = iARGUE_3_playExit
				
				IF (fExitDialogueTime <> 0)
					iArgueStage = iARGUE_33_playExitWaitForSpeech
				ENDIF
			ENDIF
		BREAK
		CASE iARGUE_3_playExit		//
		CASE iARGUE_33_playExitWaitForSpeech
			
			
			TEXT_LABEL_63 tFamilyDebugAnimDict, tFamilyDebugAnimClip
			ANIMATION_FLAGS eFamilyDebugAnimFlag
			enumFamilyAnimProgress eFamilyDebugAnimProgress
			
			IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
					tFamilyDebugAnimDict, tFamilyDebugAnimClip,
					eFamilyDebugAnimFlag, eFamilyDebugAnimProgress)
				
				tFamilyDebugAnimClip = Get_Family_Argue_Exit_Clip(PedIndex, eFamilyMember, eFamilyEvent)
				
				#IF IS_DEBUG_BUILD
				str  = ("tExitClip: \"")
				str += (tFamilyDebugAnimClip)
				str += ("\"")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_REDLIGHT)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				str  = ("tExitClip: \"")
				str += (tFamilyDebugAnimClip)
				str += ("\"")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_REDLIGHT)
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str  = ("3. playing argue, synch scene [")
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				str += "null"
			ELSE
				str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
			ENDIF
			str += ("]")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF (fExitDialogueTime <> 0)
			AND (iArgueStage <> iARGUE_3_playExit)
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				AND (GET_SYNCHRONIZED_SCENE_PHASE(iScene) >= fExitDialogueTime))
					IF NOT IS_STRING_NULL_OR_EMPTY(strExit)
						IF Is_Ped_Playing_Family_Speech(strTextBlock, strExit,
								inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
							RESTART_TIMER_NOW(speechTimer)
							iArgueStage = iARGUE_3_playExit
						ENDIF
					ELSE
						CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": PRIVATE_Update_Family_Argue(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ") - attempting to play an empty exit line??")
					ENDIF
				ENDIF
			ENDIF
			
			FLOAT ReturnStartPhase, ReturnEndPhase
			IF FIND_ANIM_EVENT_PHASE(tFamilyDebugAnimDict, tFamilyDebugAnimClip,
					"WalkInterruptible",
					ReturnStartPhase, ReturnEndPhase)
				CASSERTLN(DEBUG_FAMILY, "walk interruptible in argue anim")
			ELSE
				ReturnStartPhase = 0.95
				ReturnEndPhase = 1.0
				
				IF eFamilyEvent = FE_M7_SON_gaming
					ReturnStartPhase = 0.44
					ReturnEndPhase = 1.0
				ENDIF
				
				IF eFamilyEvent = FE_M_WIFE_screaming_at_son_P2
					IF eFamilyMember = FM_MICHAEL_WIFE
						ReturnStartPhase = 0.78		ReturnEndPhase = 1.00	//#1577498 & #1585993
					ENDIF
				ELIF eFamilyEvent = FE_M_WIFE_screaming_at_son_P3
					IF eFamilyMember = FM_MICHAEL_WIFE
						ReturnStartPhase = 0.73		ReturnEndPhase = 1.00	//#1577498 & #1585993
					ENDIF
				ELIF eFamilyEvent = FE_M_WIFE_screaming_at_daughter
					IF eFamilyMember = FM_MICHAEL_WIFE
						ReturnStartPhase = 0.80		ReturnEndPhase = 1.00	//#1577498 & #1585993
					ENDIF
//				ELIF eFamilyEvent = FE_M_WIFE_screaming_at_son_P1
//					IF eFamilyMember = FM_MICHAEL_WIFE
//						ReturnStartPhase = 0.71		ReturnEndPhase = 1.00	//#1577498 & #1585993
//					ENDIF
				ENDIF
				
				IF eFamilyEvent = FE_T0_RONEX_working_a_moonshine_sill
					ReturnStartPhase = 0.80		ReturnEndPhase = 1.00
				ENDIF
				
				IF eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_a
				OR eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_b
				OR eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_c
					ReturnStartPhase = 0.75		ReturnEndPhase = 1.00
				ENDIF
				
				IF eFamilyEvent = FE_M7_WIFE_Making_juice
					ReturnStartPhase = 5.0		ReturnEndPhase = 1.00
				ENDIF
			ENDIF
			
			
			#IF IS_DEBUG_BUILD
			str  = ("  WalkInterruptible phases: ")
			str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
			str += (", ")
			str += GET_STRING_FROM_FLOAT(ReturnEndPhase)
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
			#ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			OR (IS_SYNCHRONIZED_SCENE_RUNNING(iScene) AND (GET_SYNCHRONIZED_SCENE_PHASE(iScene) >= ReturnStartPhase))
				RESTART_TIMER_NOW(speechTimer)
				
				IF (eDesiredFamilyEvent = NO_FAMILY_EVENTS)
				
					TASK_GO_STRAIGHT_TO_COORD(PedIndex,
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<0,1.5,0>>),
							PEDMOVE_WALK)
					
					FORCE_PED_MOTION_STATE(PedIndex, MS_ON_FOOT_WALK, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PedIndex)
					
					iArgueStage = iARGUE_4_wanderArea
//				ELIF (eFamilyEvent = FE_M_WIFE_screaming_at_son_P2 AND eFamilyMember = FM_MICHAEL_WIFE)
//				OR (eFamilyEvent = FE_M_WIFE_screaming_at_son_P3 AND eFamilyMember = FM_MICHAEL_WIFE)
//				
//					TASK_GO_STRAIGHT_TO_COORD(PedIndex,
//							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<0,1.5,0>>),
//							PEDMOVE_WALK)
//					
//					FORCE_PED_MOTION_STATE(PedIndex, MS_ON_FOOT_WALK, FALSE)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(PedIndex)
//					
//					RESTART_TIMER_NOW(speechTimer)
//					iArgueStage = 0
//					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
//					RETURN FALSE
				ELSE
					 IF (eDesiredFamilyEvent = FE_ANY_find_family_event)
					 OR (eDesiredFamilyEvent = FE_ANY_wander_family_event)
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							DETACH_SYNCHRONIZED_SCENE(iScene)
						ENDIF
						iScene = -1
					ENDIF
					
					RESTART_TIMER_NOW(speechTimer)
					iArgueStage = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE iARGUE_4_wanderArea		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("4. finished argue - wander area", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			iArgueStage = 0
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
		BREAK
		CASE iARGUE_5_updateEvent		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("5. finished argue - update event", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			 IF (eDesiredFamilyEvent = FE_ANY_find_family_event)
			 OR (eDesiredFamilyEvent = FE_ANY_wander_family_event)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
					DETACH_SYNCHRONIZED_SCENE(iScene)
				ENDIF
				iScene = -1
			ENDIF
			
			RESTART_TIMER_NOW(speechTimer)
			iArgueStage = 0
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
			RETURN FALSE
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid iArgueStage ", iArgueStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_Argue(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			CASSERTLN(DEBUG_FAMILY, "invalid iArgueStage ", iArgueStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_Argue(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_Set_Entity_Visible_And_Collision(ENTITY_INDEX EntityIndex, BOOL VisibleFlag)
	
	IF NOT DOES_ENTITY_EXIST(EntityIndex)
		EXIT
	ENDIF
	
	IF NOT VisibleFlag
		IF IS_ENTITY_VISIBLE(EntityIndex)
			SET_ENTITY_VISIBLE(EntityIndex, FALSE)
			SET_ENTITY_COLLISION(EntityIndex, FALSE)
		ENDIF
	ELSE
		IF NOT IS_ENTITY_VISIBLE(EntityIndex)
			SET_ENTITY_VISIBLE(EntityIndex, TRUE)
			SET_ENTITY_COLLISION(EntityIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PRIVATE_Update_Family_ComingHome(PED_INDEX &PedIndex, enumFamilyMember eFamilyMember,
		enumFamilyEvents eFamilyEvent, VECTOR vSequencePos, FLOAT fSequenceHead,
		INT &iShoppingStage, OBJECT_INDEX oLeftBag, OBJECT_INDEX oRightBag,
		INT &iScene, TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		enumFamilyEvents eDesiredFamilyEvent,
		INTERIOR_INSTANCE_INDEX InteriorInstanceIndex, STRING RoomKeyName,
		STRING strTextBlock, STRING strEnter, STRING strIdle, TEXT_LABEL &tCreatedConvLabels[],
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer, INT &iCurrentRandCount,
		enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH)
	
	CDEBUG3LN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), " PRIVATE_Update_Family_ComingHome.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
	
	#IF IS_DEBUG_BUILD
	
	IF (eDesiredFamilyEvent = INT_TO_ENUM(enumFamilyEvents, 0))
		eDesiredFamilyEvent = NO_FAMILY_EVENTS
	ENDIF
	
	CONST_INT iFamTextOffset 3
	TEXT_LABEL_63 str
	#ENDIF
	
	CONST_INT iSHOPPING_0_shopping				0
	CONST_INT iSHOPPING_1_waitForSpeech			1
	CONST_INT iSHOPPING_2_performingSynch		2
	CONST_INT iSHOPPING_3_playIdle				3
	CONST_INT iSHOPPING_4_exitShop				4
	CONST_INT iSHOPPING_5_updateEvent			5
	
	SWITCH iShoppingStage
		CASE iSHOPPING_0_shopping		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("0. wait for shopping", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF NOT PRIVATE_Trigger_Family_Argue(PedIndex, eFamilyEvent, vSequencePos, fSequenceHead)
				
				PRIVATE_Set_Entity_Visible_And_Collision(PedIndex, FALSE)
				PRIVATE_Set_Entity_Visible_And_Collision(oLeftBag, FALSE)
				PRIVATE_Set_Entity_Visible_And_Collision(oRightBag, FALSE)
				
				VECTOR Family_ComingHome_VecCentre, Family_ComingHome_VecBounds
				FLOAT Family_ComingHome_fAngle
				Family_ComingHome_VecCentre = <<-7.5000, -2.9500, 0.5700>>
				Family_ComingHome_VecBounds = <<3.0000, 3.0000, 1.5000>>
				Family_ComingHome_fAngle = -22.0000

				
			/*	START_WIDGET_GROUP("Family_ComingHome")
					ADD_WIDGET_VECTOR_SLIDER("VecCentre", Family_ComingHome_VecCentre, -20.0, 20.0, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("VecBounds", Family_ComingHome_VecBounds, 0.5, 20.0, 0.5)
					ADD_WIDGET_FLOAT_SLIDER("fAngle", Family_ComingHome_fAngle, -360.0, 360.0, 1.0)
				STOP_WIDGET_GROUP()
				WHILE TRUE
					IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
							vSequencePos+Family_ComingHome_VecCentre, Family_ComingHome_VecBounds, fSequenceHead+Family_ComingHome_fAngle)
					ENDIF
					IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
						SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("Family_ComingHome_VecCentre = ")SAVE_VECTOR_TO_DEBUG_FILE(Family_ComingHome_VecCentre)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("Family_ComingHome_VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(Family_ComingHome_VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_STRING_TO_DEBUG_FILE("Family_ComingHome_fAngle = ")SAVE_FLOAT_TO_DEBUG_FILE(Family_ComingHome_fAngle)SAVE_NEWLINE_TO_DEBUG_FILE()
						SAVE_NEWLINE_TO_DEBUG_FILE()
						RETURN TRUE
					ENDIF
					WAIT(0)
				ENDWHILE */
				
				IF IS_PED_AT_ANGLED_COORD(PLAYER_PED_ID(),
						vSequencePos+Family_ComingHome_VecCentre, Family_ComingHome_VecBounds, fSequenceHead+Family_ComingHome_fAngle)
					
					Set_Current_Event_For_FamilyMember(eFamilyMember, FAMILY_MEMBER_BUSY)
					DELETE_PED(PedIndex)
					RETURN FALSE
				ENDIF
				
				
				
			ELSE
				PRIVATE_Set_Entity_Visible_And_Collision(PedIndex, TRUE)
				PRIVATE_Set_Entity_Visible_And_Collision(oLeftBag, TRUE)
				PRIVATE_Set_Entity_Visible_And_Collision(oRightBag, TRUE)
				
				INT iRandCount, iSpeechBit, iPlayerBitset
				Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
				
				IF NOT IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
					CASSERTLN(DEBUG_FAMILY, "coming home - invalid IS_BITMASK_SET")
					RETURN FALSE
				ENDIF
				
				//is the speech a single line or is it a random conv
				IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
					
					IF Is_Ped_Playing_Family_Speech(strTextBlock, strEnter,
							inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
						RESTART_TIMER_NOW(speechTimer)
						IF ARE_STRINGS_EQUAL(strEnter, strIdle)
							iCurrentRandCount++
						ENDIF
						
						CPRINTLN(DEBUG_FAMILY, "iShoppingStage = iSHOPPING_1_waitForSpeech - SHOPPING (random)!")
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							SET_SYNCHRONIZED_SCENE_RATE(iScene, 1.0)
						ENDIF
						
						iShoppingStage = iSHOPPING_1_waitForSpeech
						RETURN FALSE
					ENDIF
				ELSE
					TEXT_LABEL returnedStrLabel
					returnedStrLabel = strEnter
					IF GetRandomConversationFromLabel(strTextBlock, returnedStrLabel, tCreatedConvLabels)
						
						IF Is_Ped_Playing_Family_Speech(strTextBlock, returnedStrLabel,
								inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
							RESTART_TIMER_NOW(speechTimer)
							IF ARE_STRINGS_EQUAL(strEnter, strIdle)
								iCurrentRandCount++
							ENDIF
							
							CPRINTLN(DEBUG_FAMILY, "iShoppingStage = iSHOPPING_1_waitForSpeech - SHOPPING (non-random)!")
							
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
								SET_SYNCHRONIZED_SCENE_RATE(iScene, 1.0)
							ENDIF
							
							iShoppingStage = iSHOPPING_1_waitForSpeech
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF PRIVATE_Update_Family_SynchScene(PedIndex, eFamilyMember, eFamilyEvent,
					vSequencePos, fSequenceHead,
					iScene, FALSE, FALSE, tAnimDict, tAnimClip)
					
				IF IS_VALID_INTERIOR(InteriorInstanceIndex)
				AND IS_INTERIOR_READY(InteriorInstanceIndex)
					FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					
					IF DOES_ENTITY_EXIST(oLeftBag)
						FORCE_ROOM_FOR_ENTITY(oLeftBag, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					ENDIF
					IF DOES_ENTITY_EXIST(oRightBag)
						FORCE_ROOM_FOR_ENTITY(oRightBag, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					ENDIF
				ENDIF
					
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
					SET_SYNCHRONIZED_SCENE_RATE(iScene, 0.0)
				ENDIF
			ENDIF
			
		BREAK
		CASE iSHOPPING_1_waitForSpeech
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("1. wait for speech", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF PRIVATE_Update_Family_SynchScene(PedIndex, eFamilyMember, eFamilyEvent,
					vSequencePos, fSequenceHead,
					iScene, FALSE, TRUE, tAnimDict, tAnimClip,
					NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
				
				PRIVATE_Set_Entity_Visible_And_Collision(PedIndex, TRUE)
				PRIVATE_Set_Entity_Visible_And_Collision(oLeftBag, TRUE)
				PRIVATE_Set_Entity_Visible_And_Collision(oRightBag, TRUE)
				
				IF IS_VALID_INTERIOR(InteriorInstanceIndex)
				AND IS_INTERIOR_READY(InteriorInstanceIndex)
					FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					
					IF DOES_ENTITY_EXIST(oLeftBag)
						FORCE_ROOM_FOR_ENTITY(oLeftBag, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					ENDIF
					IF DOES_ENTITY_EXIST(oRightBag)
						FORCE_ROOM_FOR_ENTITY(oRightBag, InteriorInstanceIndex, GET_HASH_KEY(RoomKeyName))
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
					SET_SYNCHRONIZED_SCENE_RATE(iScene, 1.0)
				ENDIF
				
				iShoppingStage = iSHOPPING_2_performingSynch
				RETURN FALSE
			ENDIF
		BREAK
		CASE iSHOPPING_2_performingSynch		//
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				str  = ("creating SynchScene")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_REDLIGHT)
			ELSE
				str  = ("SynchScene phase: ")
				str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_BLUELIGHT)
			ENDIF
			DrawDebugFamilyTextWithOffset("2. play shoppingEnter", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			OR (GET_SYNCHRONIZED_SCENE_PHASE(iScene) >= 0.95)
				IF DOES_ENTITY_EXIST(oLeftBag)
					IF IS_ENTITY_ATTACHED(oLeftBag)
						DETACH_ENTITY(oLeftBag)
						CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(oLeftBag)")
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
						STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftBag, INSTANT_BLEND_OUT, TRUE)
						CPRINTLN(DEBUG_FAMILY, "STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftBag)")
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(oRightBag)
					IF IS_ENTITY_ATTACHED(oRightBag)
						DETACH_ENTITY(oRightBag)
						CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(oRightBag)")
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
						STOP_SYNCHRONIZED_ENTITY_ANIM(oRightBag, INSTANT_BLEND_OUT, TRUE)
						CPRINTLN(DEBUG_FAMILY, "STOP_SYNCHRONIZED_ENTITY_ANIM(oLeftBag)")
					ENDIF
				ENDIF
				
				IF (eFamilyMember = FM_MICHAEL_WIFE)
					iShoppingStage = iSHOPPING_3_playIdle
				ELSE
					iShoppingStage = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE iSHOPPING_3_playIdle		//
			INT iRandCount, iSpeechBit, iPlayerBitset
			Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
			
			#IF IS_DEBUG_BUILD
			str  = ("3. play shoppingIdle")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			
			str  = ("iRandCount [")
			str += (iCurrentRandCount)
			str += (" >= ")
			str += (iRandCount)
			str += ("]")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+2, HUD_COLOUR_RED)
			#ENDIF
			
			PRIVATE_Update_Family_SynchSceneArray(PedIndex,
					eFamilyMember, eFamilyEvent,
				 	tAnimDict, tAnimClip,
				 	vSequencePos, fSequenceHead,
					iScene, TRUE, TRUE)
	
			IF Play_This_Family_Speech(PedIndex, eFamilyEvent,
					inSpeechStruct,
					strTextBlock, tCreatedConvLabels, speechTimer, iCurrentRandCount,
					16, strIdle)
			ENDIF
			
//			IF iCurrentRandCount >= iRandCount
//				iShoppingStage = iSHOPPING_4_exitShop
//			ENDIF
			
		BREAK
		
		CASE iSHOPPING_4_exitShop		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("4. exit shop", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_RED)
			#ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				VECTOR vSynchSceneOffset
				FLOAT fSynchSceneHead
				vSynchSceneOffset = <<0,0,0>>
				fSynchSceneHead = 0
				IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
					iScene = CREATE_SYNCHRONIZED_SCENE(vSequencePos+vSynchSceneOffset, <<0,0,fSequenceHead+fSynchSceneHead>>)
				ENDIF
			ENDIF
			
			STRING sFamilyArgueExitClip
			sFamilyArgueExitClip = Get_Family_Argue_Exit_Clip(PedIndex, eFamilyMember, eFamilyEvent)
			
			TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
						tAnimDict, sFamilyArgueExitClip,
						NORMAL_BLEND_IN, WALK_BLEND_OUT,
						SYNCED_SCENE_USE_PHYSICS)
			SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
			
			SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, FALSE)
			
			tAnimClip = sFamilyArgueExitClip
			
			CPRINTLN(DEBUG_FAMILY, "iArgueStage = iSHOPPING_5_updateEvent \"", tAnimDict, "/", sFamilyArgueExitClip, "\"")
			
			iShoppingStage = iSHOPPING_5_updateEvent
			RETURN FALSE
		BREAK
		CASE iSHOPPING_5_updateEvent		//
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("5. play shoppingExit", GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+0, HUD_COLOUR_RED)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				str  = ("creating SynchScene")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_REDLIGHT)
			ELSE
				str  = ("SynchScene phase: ")
				str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), iFamTextOffset+1, HUD_COLOUR_BLUELIGHT)
			ENDIF
			#ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				iShoppingStage = 0
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
				RETURN FALSE
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid iShoppingStage ", iShoppingStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_Shopping(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			CASSERTLN(DEBUG_FAMILY, "invalid iShoppingStage ", iShoppingStage, " for ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_Shopping(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_AskForCash(enumCharacterList charSheetID, PED_INDEX PedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent, INT iPlayerBitset,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead,
		INT &iCompletedGivingCash,
		TEXT_LABEL_63 &tDialogueAnimDict, TEXT_LABEL_63 &tDialogueAnimClip,
		INT &cash_intention_index, STRING strTextBlock, STRING strTextRoot, TEXT_LABEL &tCreatedConvLabels[],
		INT &iScene,
		structPedsForConversation &inSpeechStruct, structTimer &speechTimer,
		enumConversationPriority enSpeechPriority = CONV_PRIORITY_AMBIENT_HIGH,
		MODEL_NAMES ePropModel = DUMMY_MODEL_FOR_SCRIPT)
	
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	
	CONST_INT iCONST_0_requestAssets	0
	CONST_INT iCONST_1_waitForLoad		1
	
	CONST_INT iCONST_2_animStart		2
	CONST_INT iCONST_3_animFinish		3
	
	CONST_INT iCONST_2_synchStart		20
	CONST_INT iCONST_3_synchFinishing	30
	CONST_INT iCONST_4_synchFinished	40
	
	CONST_INT iCONST_done				-1
	
	IF NOT IS_BITMASK_SET(iPlayerBitset, GET_PLAYER_PED_BIT(charSheetID))
		#IF IS_DEBUG_BUILD
		str  = ("ask for cash, player not iPlayerBitset")
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, HUD_COLOUR_GREENDARK)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (iCompletedGivingCash = iCONST_done)
		#IF IS_DEBUG_BUILD
		str  = ("ask for cash, giving cash is done")
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, HUD_COLOUR_PURE_WHITE)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	STRING		strCASH			= NULL
	INT			iCashAskedFor	= -1
	
	VECTOR		vPedCoord		= GET_ENTITY_COORDS(PedIndex)
	VECTOR		vLocateOffset
	
	VECTOR		vScenePosOffset
	FLOAT		fSceneRotOffset
	
	TEXT_LABEL_63 tAnimDict		= ""
	TEXT_LABEL_63 tAnimPlayer	= "", tAnimFamMember = "", tAnimFamProp = "", tAnimFamCam = ""
	
	INT			iNextStage		= -99
	INT			iRandomAnimSet	= -1
	
	SWITCH eFamilyEvent
		CASE FE_M_DAUGHTER_Coming_home_drunk
			strCASH				= "PAY_M_DAUG"	//Press ~INPUT_CONTEXT~ to give $~1~ to Tracey.
			iCashAskedFor		= 50
			vLocateOffset		= <<0,0,-0.75>>
			
			tAnimDict			= "TIMETABLE@TRACY@IG_14@"
			
			tAnimPlayer			= "IG_14_IWishAll_A_PLAYER"
			tAnimFamMember		= "IG_14_IWishAll_A_TRACY"
			tAnimFamProp		= "IG_14_IWishAll_A_NOTE"
			tAnimFamCam			= "IG_14_IWishAll_A_CAM"
			
			//tAnimPlayer		= "IG_14_IWishAll_A_PLAYER"
			//tAnimFamMember	= "IG_14_IWishAll_A_TRACY"
			//tAnimFamProp		= "IG_14_IWishAll_A_NOTE"
			//tAnimFamCam		= "IG_14_IWishAll_A_CAM"
			
			//tAnimPlayer		= "IG_14_IWishAll_B_PLAYER"
			//tAnimFamMember	= "IG_14_IWishAll_B_TRACY"
			//tAnimFamProp		= "IG_14_IWishAll_B_NOTE"
			//tAnimFamCam		= "IG_14_IWishAll_B_CAM"
			
			//tAnimPlayer		= "IG_14_ParentingAtItsFinest_PLAYER"
			//tAnimFamMember	= "IG_14_ParentingAtItsFinest_TRACY"
			//tAnimFamProp		= "IG_14_ParentingAtItsFinest_NOTE"
			//tAnimFamCam		= "IG_14_ParentingAtItsFinest_CAM"
			
			vScenePosOffset		= <<0,0,0>>
			fSceneRotOffset		= 0
			
			iRandomAnimSet		= 99
			iNextStage			= iCONST_2_synchStart
		BREAK
		CASE FE_M_SON_watching_porn
			strCASH				= "PAY_M_SON"	//Press ~INPUT_CONTEXT~ to check on Jimmy.
			iCashAskedFor		= 0
			vLocateOffset		= <<0,6,0>>
			
			tAnimDict			= "TIMETABLE@JIMMY@DOORKNOCK@"
			tAnimPlayer			= "KNOCKDOOR_IDLE"
			tAnimFamMember		= ""
			tAnimFamProp		= ""
			tAnimFamCam			= "KNOCKDOOR_IDLE_CAM"
			
			vScenePosOffset		= <<0.5010, 6.6020, 0.0>>
			fSceneRotOffset		= 242.1000
			
			iRandomAnimSet		= -1
			iNextStage			= iCONST_2_synchStart
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_AskForCash()")
			CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_AskForCash()")
			#ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPedCoord+vLocateOffset, <<2,2,1>>, FALSE, TRUE, TM_ON_FOOT)
	AND NOT IS_PHONE_ONSCREEN()
		REQUEST_ANIM_DICT(tAnimDict)
		IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(ePropModel)
		ENDIF
		
		IF (iCashAskedFor > 0)
			#IF IS_DEBUG_BUILD
			str  = ("(GET_TOTAL_CASH(")
			str +=  (GET_PLAYER_PED_STRING(charSheetID))
			str += ("):")
			str += (GET_TOTAL_CASH(charSheetID))
			str += (" < ")
			str += (iCashAskedFor)
			str += (")")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -1, HUD_COLOUR_GREENDARK)
			#ENDIF
			
			IF (GET_TOTAL_CASH(charSheetID) < iCashAskedFor)
				IF cash_intention_index != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(cash_intention_index)
					CLEAR_THIS_FLOATING_HELP_WITH_NUMBER(strCASH, iCashAskedFor)
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		BOOL eHasPropModelLoaded = TRUE
		IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
			IF NOT HAS_MODEL_LOADED(ePropModel)
				eHasPropModelLoaded = FALSE
			ENDIF
		ENDIF
		
		IF NOT HAS_ANIM_DICT_LOADED(tAnimDict)
		OR NOT Is_Family_Speech_Loaded_And_Setup(strTextBlock)
		OR NOT eHasPropModelLoaded
			#IF IS_DEBUG_BUILD
			str  = ("request assets: ")
			
			IF NOT HAS_ANIM_DICT_LOADED(tAnimDict)
				str += (GET_PLAYER_PED_STRING(charSheetID))
				str += " "
			ENDIF
			IF NOT eHasPropModelLoaded
				str += (GET_MODEL_NAME_FOR_DEBUG(ePropModel))
				str += " "
			ENDIF
			IF NOT Is_Family_Speech_Loaded_And_Setup(strTextBlock)
				str += (strTextBlock)
			ENDIF
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), -2, HUD_COLOUR_RED)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		IF cash_intention_index = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(cash_intention_index, CP_MEDIUM_PRIORITY, strCASH, TRUE)
		ELSE
			
			IF IS_CONTEXT_INTENTION_TOP(cash_intention_index)
				IF NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(strCASH, iCashAskedFor)
					PRINT_HELP_FOREVER_WITH_NUMBER(strCASH, iCashAskedFor)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_CONTEXT_BUTTON_TRIGGERED(cash_intention_index)
			RETURN FALSE
		ELSE
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			iCompletedGivingCash = iCONST_0_requestAssets
			OBJECT_INDEX PropIndex
			CAMERA_INDEX CamIndex
			WHILE iCompletedGivingCash > iCONST_done
			AND IS_PLAYER_PLAYING(PLAYER_ID())
			AND NOT IS_PED_INJURED(PedIndex)
			
				#IF IS_DEBUG_BUILD
				str  = ("iCompletedGivingCash:")
				str += iCompletedGivingCash
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENDARK)
				#ENDIF
				
				SWITCH iCompletedGivingCash
					CASE iCONST_0_requestAssets
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("requestAssets", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
						#ENDIF
						
						IF cash_intention_index != NEW_CONTEXT_INTENTION
							RELEASE_CONTEXT_INTENTION(cash_intention_index)
						ENDIF
						IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(strCASH, iCashAskedFor)
							CLEAR_HELP()
						ENDIF
							
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
						ENDIF
						
						REQUEST_ANIM_DICT(tAnimDict)
						IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
							REQUEST_MODEL(ePropModel)
						ENDIF
						
						KILL_ANY_CONVERSATION()
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DISPLAY_RADAR(FALSE)
						
						iCompletedGivingCash = iCONST_1_waitForLoad
					BREAK
					CASE iCONST_1_waitForLoad
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("waitForLoad", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
						#ENDIF
						
						BOOL eFinishedWaitingForPropModelToLoad 
						eFinishedWaitingForPropModelToLoad = TRUE
						IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
							IF NOT HAS_MODEL_LOADED(ePropModel)
								eFinishedWaitingForPropModelToLoad = FALSE
							ENDIF
						ENDIF
						
						IF HAS_ANIM_DICT_LOADED(tAnimDict)
						AND eFinishedWaitingForPropModelToLoad
							iCompletedGivingCash = iNextStage
						ENDIF
					BREAK
					
					CASE iCONST_2_animStart
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("animStart", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
						#ENDIF
						
						IF Is_Ped_Playing_Family_Speech(strTextBlock, strTextRoot,
								inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
							RESTART_TIMER_NOW(speechTimer)
							
							TASK_PLAY_ANIM(PLAYER_PED_ID(), tAnimDict, tAnimPlayer)
							TASK_PLAY_ANIM(PedIndex, tAnimDict, tAnimFamMember)
							
							iCompletedGivingCash = iCONST_3_animFinish
						ENDIF
					BREAK
					CASE iCONST_3_animFinish
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("animFinish", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
						#ENDIF
						
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), tAnimDict, tAnimPlayer)
						AND NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimFamMember)
							iCompletedGivingCash = iCONST_done
						ENDIF
					BREAK
					
					CASE iCONST_2_synchStart
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("synchStart", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
						#ENDIF
						
//						IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK)
							
							IF NOT Is_Ped_Playing_Family_Speech(strTextBlock, strTextRoot,
										inSpeechStruct, enSpeechPriority, tCreatedConvLabels)
								#IF IS_DEBUG_BUILD
								DrawDebugFamilyTextWithOffset("wait for speech to start...", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_GREENDARK)
								#ENDIF
							ELSE
								VECTOR vSynchSceneOffset
								FLOAT fSynchSceneHead
								vSynchSceneOffset = <<0,0,0>>
								fSynchSceneHead = 0
								IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
										DETACH_SYNCHRONIZED_SCENE(iScene)
									ENDIF
									iScene = -1
									
									IF iRandomAnimSet = 99
										
										TEXT_LABEL rootLabel
										INT iRootFilenameNum
										rootLabel  = g_ConversationData.ConversationSegmentToGrab
										rootLabel += "A"
										iRootFilenameNum = GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(rootLabel)
										
										#IF IS_DEBUG_BUILD 
										SAVE_STRING_TO_DEBUG_FILE("PRIVATE_GetTextLabelFromConv(\"")
										SAVE_STRING_TO_DEBUG_FILE(rootLabel)
										SAVE_STRING_TO_DEBUG_FILE("\", \"")
										SAVE_INT_TO_DEBUG_FILE(iRootFilenameNum)
										SAVE_STRING_TO_DEBUG_FILE("\")")
										SAVE_NEWLINE_TO_DEBUG_FILE()
										#ENDIF
										
										IF iRootFilenameNum < 0
											iRootFilenameNum = GET_RANDOM_INT_IN_RANGE(0,6)
											
											#IF IS_DEBUG_BUILD
											SAVE_STRING_TO_DEBUG_FILE("invalid iRootFilenameNum, randomized to ")
											SAVE_INT_TO_DEBUG_FILE(iRootFilenameNum)
											SAVE_NEWLINE_TO_DEBUG_FILE()
											#ENDIF 
										ENDIF
										
										SWITCH iRootFilenameNum
											CASE 0
											CASE 1
												tAnimPlayer		= "IG_14_IWishAll_A_PLAYER"
												tAnimFamMember	= "IG_14_IWishAll_A_TRACY"
												tAnimFamProp	= "IG_14_IWishAll_A_NOTE"
												tAnimFamCam		= "IG_14_IWishAll_A_CAM"
											BREAK
											
											CASE 2
											CASE 3
												tAnimPlayer		= "IG_14_IWishAll_B_PLAYER"
												tAnimFamMember	= "IG_14_IWishAll_B_TRACY"
												tAnimFamProp	= "IG_14_IWishAll_B_NOTE"
												tAnimFamCam		= "IG_14_IWishAll_B_CAM"
											BREAK
											
											CASE 4
											CASE 5
												tAnimPlayer		= "IG_14_ParentingAtItsFinest_PLAYER"
												tAnimFamMember	= "IG_14_ParentingAtItsFinest_TRACY"
												tAnimFamProp	= "IG_14_ParentingAtItsFinest_NOTE"
												tAnimFamCam		= "IG_14_ParentingAtItsFinest_CAM"
											BREAK
										ENDSWITCH
										
										#IF IS_DEBUG_BUILD
										SAVE_STRING_TO_DEBUG_FILE("iRootFilenameNum: ")
										SAVE_INT_TO_DEBUG_FILE(iRootFilenameNum)
										SAVE_STRING_TO_DEBUG_FILE(" \"")
										SAVE_STRING_TO_DEBUG_FILE(tAnimPlayer)
										SAVE_STRING_TO_DEBUG_FILE("\"")
										SAVE_NEWLINE_TO_DEBUG_FILE()
										#ENDIF 
									ENDIF
									
									iScene = CREATE_SYNCHRONIZED_SCENE(vFamilyScenePos+vSynchSceneOffset+vScenePosOffset, <<0,0,fFamilySceneHead+fSynchSceneHead+fSceneRotOffset>>)
									
									SET_SYNCHRONIZED_SCENE_LOOPED(iScene, FALSE)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, TRUE)
									
									IF NOT IS_STRING_NULL_OR_EMPTY(tAnimPlayer)
										TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iScene,
												tAnimDict, tAnimPlayer,
												INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_PLAYER_IMPACT, INSTANT_BLEND_IN)
									ENDIF
									IF NOT IS_STRING_NULL_OR_EMPTY(tAnimFamMember)
										TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
												tAnimDict, tAnimFamMember,
												INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_NONE, RBF_PLAYER_IMPACT, INSTANT_BLEND_IN)
									ENDIF
									IF NOT IS_STRING_NULL_OR_EMPTY(tAnimFamProp)
										PropIndex = CREATE_OBJECT(ePropModel, vFamilyScenePos+vSynchSceneOffset)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(PropIndex, iScene,
												tAnimFamProp, tAnimDict,
												INSTANT_BLEND_IN)
									ENDIF
									IF NOT IS_STRING_NULL_OR_EMPTY(tAnimFamCam)
										CamIndex = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
										PLAY_SYNCHRONIZED_CAM_ANIM(CamIndex, iScene,
												tAnimFamCam, tAnimDict)
										RENDER_SCRIPT_CAMS(TRUE, FALSE)
									ENDIF
									
									RESTART_TIMER_NOW(speechTimer)
									iCompletedGivingCash = iCONST_3_synchFinishing
								ENDIF
								
							ENDIF
//						ENDIF
					BREAK
					CASE iCONST_3_synchFinishing
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							FLOAT fScenePhase
							fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
							
							#IF IS_DEBUG_BUILD
							str  = ("synchFinishing:phase ")
							str += GET_STRING_FROM_FLOAT(fScenePhase)
							IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene)
								str += (" hold last frame")
							ENDIF
							IF IS_SYNCHRONIZED_SCENE_LOOPED(iScene)
								str += (" looped")
							ENDIF
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
							
							str  = ("tAnimFamMember: \"")
							str += (tAnimFamMember)
							str += ("\"")
							
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_GREENDARK)
							
							
							
							IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
								VECTOR vSynchSceneOffset
								FLOAT fSynchSceneHead
								vSynchSceneOffset = <<0,0,0>>
								fSynchSceneHead = 0
								IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)

								
									START_WIDGET_GROUP("PRIVATE_Update_Family_AskForCash")
										ADD_WIDGET_STRING("scene phase")
										ADD_WIDGET_FLOAT_SLIDER("fScenePhase", fScenePhase, 0.0, 1.0, 0.001)
										ADD_WIDGET_STRING("scene position")
										ADD_WIDGET_VECTOR_SLIDER("vFamilyScenePos", vFamilyScenePos, -4000.0, 4000.0, 0.0)
										ADD_WIDGET_VECTOR_SLIDER("vSynchSceneOffset", vSynchSceneOffset, -4000.0, 4000.0, 0.0)
										ADD_WIDGET_VECTOR_SLIDER("vScenePosOffset", vScenePosOffset, -20.0, 20.0, 0.001)
										ADD_WIDGET_STRING("scene orientation")
										ADD_WIDGET_FLOAT_SLIDER("fFamilySceneHead", fFamilySceneHead, -4000.0, 4000.0, 0.0)
										ADD_WIDGET_FLOAT_SLIDER("fSynchSceneHead", fSynchSceneHead, -4000.0, 4000.0, 0.0)
										ADD_WIDGET_FLOAT_SLIDER("fSceneRotOffset", fSceneRotOffset, 0.0, 360.0, 0.1)
									STOP_WIDGET_GROUP()
									
									WHILE NOT IS_KEYBOARD_KEY_JUST_RELEASED(KEY_C)
										SET_SYNCHRONIZED_SCENE_ORIGIN(iScene, 
												vFamilyScenePos+vSynchSceneOffset+vScenePosOffset,
												<<0,0,fFamilySceneHead+fSynchSceneHead+fSceneRotOffset>>)
										SET_SYNCHRONIZED_SCENE_PHASE(iScene, fScenePhase)
										
										WAIT(0)
									ENDWHILE
									
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("fScenePhase = ")SAVE_FLOAT_TO_DEBUG_FILE(fScenePhase)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("vFamilyScenePos = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamilyScenePos)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("vSynchSceneOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vSynchSceneOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("vScenePosOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vScenePosOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("fFamilySceneHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamilySceneHead)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("fSynchSceneHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fSynchSceneHead)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("fSceneRotOffset = ")SAVE_FLOAT_TO_DEBUG_FILE(fSceneRotOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_NEWLINE_TO_DEBUG_FILE()
								ENDIF
							ENDIF
							
							#ENDIF
							
							IF (fScenePhase >= 0.95)
								iCompletedGivingCash = iCONST_4_synchFinished
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset("synchFinishing:DONE", GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
							#ENDIF
							
							iCompletedGivingCash = iCONST_4_synchFinished
						ENDIF
					BREAK
					CASE iCONST_4_synchFinished
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
							#IF IS_DEBUG_BUILD
							FLOAT fScenePhase
							fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
							
							str  = ("synchFinished:phase ")
							str += GET_STRING_FROM_FLOAT(fScenePhase)
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENDARK)
							
							str  = ("tDialogueAnimClip: \"")
							str += (tDialogueAnimClip)
							str += ("\"")
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_GREENDARK)
							#ENDIF
						ENDIF
						
						TEXT_LABEL_63 tFamilySynchSceneDict
						TEXT_LABEL_63 tFamilySynchSceneClip
						tFamilySynchSceneDict = ""
						tFamilySynchSceneClip = ""
	
						ANIMATION_FLAGS eFamilySynchSceneFlag
						enumFamilyAnimProgress eFamilySynchSceneProgress
						eFamilySynchSceneFlag = AF_DEFAULT
						eFamilySynchSceneProgress = FAP_0_default
						
						FLOAT blendInDelta
						FLOAT blendOutDelta
						blendInDelta = INSTANT_BLEND_IN
						blendOutDelta = NORMAL_BLEND_OUT
						
						SYNCED_SCENE_PLAYBACK_FLAGS flags
						RAGDOLL_BLOCKING_FLAGS ragdollFlags
						FLOAT moverBlendInDelta
						flags = SYNCED_SCENE_USE_PHYSICS
						ragdollFlags = RBF_PLAYER_IMPACT
						moverBlendInDelta = INSTANT_BLEND_IN
						
						IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
								tFamilySynchSceneDict, tFamilySynchSceneClip, eFamilySynchSceneFlag, eFamilySynchSceneProgress)
							
							IF NOT IS_STRING_NULL_OR_EMPTY(tAnimFamMember)
								IF IS_BITMASK_ENUM_AS_ENUM_SET(ragdollFlags, SYNCED_SCENE_USE_PHYSICS)
									SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
									SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
									
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
								ENDIF
								
								SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
								
								SET_SYNCHRONIZED_SCENE_LOOPED(iScene, TRUE)
								SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene, FALSE)
								
								TASK_SYNCHRONIZED_SCENE(PedIndex, iScene,
										tFamilySynchSceneDict, tFamilySynchSceneClip,
										blendInDelta, blendOutDelta, flags, ragdollFlags, moverBlendInDelta)
								SET_FORCE_FOOTSTEP_UPDATE(PedIndex, TRUE)
								
								tDialogueAnimDict = tFamilySynchSceneDict
								tDialogueAnimClip = tFamilySynchSceneClip
							
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "PRIVATE_TaskSynchSceneDone(PedIndex, \"", tDialogueAnimDict, "\", \"", tDialogueAnimClip, "\": ", iScene, ")")
								#ENDIF
							ENDIF
							
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
							
							iCompletedGivingCash = iCONST_done
						ENDIF
					BREAK
					
					DEFAULT
						CPRINTLN(DEBUG_FAMILY, "unknown iCompletedGivingCash: ", iCompletedGivingCash)
						CASSERTLN(DEBUG_FAMILY, "unknown iCompletedGivingCash: ", iCompletedGivingCash)
					BREAK
				ENDSWITCH
				
				WAIT(0)
			ENDWHILE
			
			
			IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
				SET_MODEL_AS_NO_LONGER_NEEDED(ePropModel)
			ENDIF
			IF DOES_ENTITY_EXIST(PropIndex)
				DELETE_OBJECT(PropIndex)
			ENDIF
			IF DOES_CAM_EXIST(CamIndex)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				DESTROY_CAM(camIndex)
			ENDIF
			
			IF NOT IS_PED_INJURED(PedIndex)
			AND (iCashAskedFor > 0)
				DEBIT_BANK_ACCOUNT(charSheetID, BAAC_LESTER, iCashAskedFor)
			ENDIF
			
			DISPLAY_RADAR(TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			RESTART_TIMER_NOW(speechTimer)
			
			IF IS_PED_INJURED(PedIndex)
				RETURN FALSE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str  = ("asking for cash:")
		str += iScene
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
		#ENDIF
		
		RETURN TRUE
	ELSE
		IF cash_intention_index != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(cash_intention_index)
			IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED(strCASH, iCashAskedFor)
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF (ePropModel <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(ePropModel)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Find_Event(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		VECTOR vSequencePos, FLOAT fExitRad = 4.0, BOOL bSetCurrentFamilyMemberEvent = TRUE)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	IF VDIST2(vSequencePos, GET_ENTITY_COORDS(PedIndex)) > ((fExitRad)*(fExitRad))
		
		#IF IS_DEBUG_BUILD
		str  = ("Update_Family_Find_Event(")
		str += (Get_String_From_FamilyMember(eFamilyMember))
		str += (")")
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
		
		str  = ("VDIST ")
		str += GET_STRING_FROM_FLOAT(VDIST(vSequencePos, GET_ENTITY_COORDS(PedIndex)))
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENLIGHT)
		#ENDIF
		
		IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
			INT Time = DEFAULT_TIME_NEVER_WARP		//DEFAULT_TIME_BEFORE_WARP
			FLOAT Radius = DEFAULT_NAVMESH_RADIUS
			
			TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, vSequencePos, PEDMOVE_WALK, Time, Radius)
		ELSE
			#IF IS_DEBUG_BUILD
			str += ("	FOLLOW_NAV_MESH_TO_COORD != performing")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
			#ENDIF
		ENDIF
	ELSE
		
		IF IS_ENTITY_IN_WATER(PedIndex)
			#IF IS_DEBUG_BUILD
			str += ("PED IN WATER (Family_Find_Event)!!!")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
			CPRINTLN(DEBUG_FAMILY, str)
			#ENDIF
				
			RETURN FALSE
		ENDIF
		
		enumFamilyEvents eDesiredFamilyEvent
		IF PRIVATE_Get_Desired_FamilyMember_Event(eFamilyMember, eDesiredFamilyEvent, TRUE)
			IF PRIVATE_Is_Valid_Find_FamilyMember_Event(eFamilyMember, eDesiredFamilyEvent)
				#IF IS_DEBUG_BUILD
				str  = ("desired event ")
				str += (Get_String_From_FamilyEvent(eDesiredFamilyEvent))
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
				#ENDIF
				
				
				
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
				RETURN TRUE
			ELSE
				
				#IF IS_DEBUG_BUILD
				str  = ("Update_Family_Find_Event(")
				str += (Get_String_From_FamilyMember(eFamilyMember))
				str += (") not valid? ")
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENLIGHT)
				
				CPRINTLN(DEBUG_FAMILY, str)
				#ENDIF
			ENDIF
		ELSE
			
			#IF IS_DEBUG_BUILD
			str  = ("Update_Family_Find_Event(")
			str += (Get_String_From_FamilyMember(eFamilyMember))
			str += (")")
			
			TEXT_LABEL_63 str2
			str2  = ("nothing desired? ")
			str2 += (Get_String_From_FamilyEvent(eDesiredFamilyEvent))
			
			IF ARE_STRINGS_EQUAL(GET_THIS_SCRIPT_NAME(), "family_scene_m")
				SWITCH GetMichaelScheduleStage()
					CASE MSS_M2_WithFamily
						str2 += (" - M2_WithFam")
					BREAK
					CASE MSS_M4_WithoutFamily
						str2 += (" - M4_WithoutFam")
					BREAK
					CASE MSS_M6_Exiled
						str2 += (" - M6_Exiled")
					BREAK
					CASE MSS_M7_ReunitedWithFamily
						str2 += (" - M7_Reunited")
					BREAK
					
					DEFAULT
						str2 += (" - MSS_Mxxx")
					BREAK
				ENDSWITCH
			ELSE
				//
			ENDIF
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
			DrawDebugFamilyTextWithOffset(str2, GET_ENTITY_COORDS(PedIndex, FALSE), 3, HUD_COLOUR_GREENLIGHT)
			CPRINTLN(DEBUG_FAMILY, str, str2)
			#ENDIF
		ENDIF
	ENDIF
	
	IF bSetCurrentFamilyMemberEvent
		IF IS_ENTITY_IN_WATER(PedIndex)
			#IF IS_DEBUG_BUILD
			str += ("PED IN WATER (Family_Find_Event)!!!")
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENLIGHT)
			CPRINTLN(DEBUG_FAMILY, str)
			#ENDIF
				
			RETURN FALSE
		ENDIF
		
		PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR PRIVATE_get_random_family_point(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, BOOL bClosest)
	VECTOR vEmergencyGotoCoords[10]
	
	VECTOR vOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<GET_RANDOM_FLOAT_IN_RANGE(-5, 5),GET_RANDOM_FLOAT_IN_RANGE(10, 50),0>>)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
		CASE FM_MICHAEL_DAUGHTER
		CASE FM_MICHAEL_WIFE
		CASE FM_MICHAEL_MEXMAID
		#IF USE_TU_CHANGES
		CASE FM_MICHAEL_GARDENER
		#ENDIF
			//EXTERIOR
			vEmergencyGotoCoords[0] = <<-821.1343, 155.6063, 69.2348>>
			vEmergencyGotoCoords[1] = <<-793.1925, 165.5698, 70.3529>>
			vEmergencyGotoCoords[2] = <<-802.1196, 191.1686, 71.8350>>
			vEmergencyGotoCoords[3] = <<-779.3063, 184.1423, 71.8354>>
			vEmergencyGotoCoords[4] = <<-781.6624, 155.8212, 66.4745>>
			
			//INTERIOR
			vEmergencyGotoCoords[5] = <<-815.5585, 179.7955, 71.1531>>
			vEmergencyGotoCoords[6] = <<-806.1222, 169.9548, 71.8347>>
			vEmergencyGotoCoords[7] = <<-794.8763, 179.0604, 71.8347>>
			vEmergencyGotoCoords[8] = <<-798.2634, 185.7100, 71.6055>>
			
			IF eFamilyMember = FM_MICHAEL_SON
				vEmergencyGotoCoords[9] = <<-808.7040, 169.7271, 75.7504>>
			ELIF eFamilyMember = FM_MICHAEL_DAUGHTER
				vEmergencyGotoCoords[9] = <<-800.1566, 172.8743, 75.7504>>
			ELIF eFamilyMember = FM_MICHAEL_WIFE
				vEmergencyGotoCoords[9] = <<-815.9270, 179.6694, 75.7407>>
			ELSE
				vEmergencyGotoCoords[9] = <<-802.9651, 178.5574, 75.7483>>
			ENDIF

		BREAK
		#IF USE_TU_CHANGES
		CASE FM_FRANKLIN_AUNT
		#ENDIF
		CASE FM_FRANKLIN_LAMAR
		CASE FM_FRANKLIN_STRETCH
			//EXTERIOR
			vEmergencyGotoCoords[0] = <<-38.2037, -1459.5112, 30.3994>>
			vEmergencyGotoCoords[1] = <<-86.7261, -1471.7407, 31.5351>>
			
			//INTERIOR
			
		BREAK
		CASE FM_TREVOR_0_RON
		CASE FM_TREVOR_0_WIFE
		
		#IF USE_TU_CHANGES
		CASE FM_TREVOR_0_MICHAEL
		CASE FM_TREVOR_0_TREVOR
		#ENDIF
			//EXTERIOR
			vEmergencyGotoCoords[0] = <<1980.7178, 3819.2358, 31.4324>>
			vEmergencyGotoCoords[1] = <<1974.1460, 3825.4822, 31.3671>>
			vEmergencyGotoCoords[2] = <<1989.2611, 3820.5364, 31.3665>>
			vEmergencyGotoCoords[3] = <<1971.3353, 3837.5620, 30.9977>>
			
			//INTERIOR
			vEmergencyGotoCoords[4] = <<1975.6251, 3820.1807, 32.4501>>
			vEmergencyGotoCoords[5] = <<1969.5767, 3815.7659, 32.4287>>
			
		BREAK
		
		CASE FM_TREVOR_1_FLOYD
			//EXTERIOR
			vEmergencyGotoCoords[0] = <<-1148.7909, -1522.9229, 9.6330>>
			vEmergencyGotoCoords[1] = <<-1151.6345, -1521.5306, 3.3567>>
			vEmergencyGotoCoords[2] = <<-1145.8826, -1542.1782, 3.3772>>
			vEmergencyGotoCoords[3] = <<-1167.1464, -1505.1429, 3.3794>>
			vEmergencyGotoCoords[4] = <<-1187.0579, -1532.1716, 3.3795>>
			
			//INTERIOR
			vEmergencyGotoCoords[5] = <<-1156.8148, -1517.5059, 9.6327>>
			vEmergencyGotoCoords[6] = <<-1157.3087, -1525.3698, 9.6321>>
			vEmergencyGotoCoords[7] = <<-1148.6206, -1519.6217, 9.6327>>
			vEmergencyGotoCoords[8] = <<-1149.4954, -1513.1079, 9.6327>>
			
			IF (g_eCurrentSafehouseActivity = SA_TREVOR_BEER)
			OR (g_eCurrentSafehouseActivity = SA_TREVOR_SHOTS)
				vEmergencyGotoCoords[8] = <<0,0,0>>
			ELSE
				vEmergencyGotoCoords[8] = <<-1153.3973, -1522.3252, 9.6475>>
			ENDIF
			
			//RANDOM COORD
			vOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PedIndex, <<GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5),GET_RANDOM_FLOAT_IN_RANGE(5, 10),0>>)
			
		BREAK
		
	ENDSWITCH
	
//	VECTOR vRandomCoord = GET_RANDOM_POINT_IN_SPHERE(GET_ENTITY_COORDS(PedIndex, FALSE), 20.0)
//	
//	INT iRandom = GET_RANDOM_INT_IN_RANGE(0,15)
//	IF iRandom < COUNT_OF(vEmergencyGotoCoords)
//		IF NOT ARE_VECTORS_ALMOST_EQUAL(vEmergencyGotoCoords[iRandom], <<0,0,0>>)
//			vRandomCoord = vEmergencyGotoCoords[iRandom]
//		ENDIF
//	ENDIF
	
	INT iCount
	
	FLOAT fShortDist = 9999999.99
	INT iClosest = 99, iTotalAmount = 0
	
	REPEAT COUNT_OF(vEmergencyGotoCoords) iCount
		IF NOT ARE_VECTORS_EQUAL(vEmergencyGotoCoords[iCount], <<0,0,0>>)
			FLOAT fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vOffset, vEmergencyGotoCoords[iCount])
			IF fCurrentDist < fShortDist
				fShortDist = fCurrentDist
				iClosest = iCount
			ENDIF
			
			iTotalAmount++
		ENDIF
	ENDREPEAT
	
	INT iSecondClosest = iClosest
	IF bClosest
		fShortDist = 9999999.99
		REPEAT COUNT_OF(vEmergencyGotoCoords) iCount
			IF NOT ARE_VECTORS_EQUAL(vEmergencyGotoCoords[iCount], <<0,0,0>>)
				FLOAT fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vOffset, vEmergencyGotoCoords[iCount])
				IF fCurrentDist < fShortDist
				AND iCount != iClosest
					fShortDist = fCurrentDist
					iSecondClosest = iCount
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		
		#IF USE_TU_CHANGES
		IF iTotalAmount = 0
			CASSERTLN(DEBUG_FAMILY, "iSecondClosest is somehow greater than COUNT_OF(vEmergencyGotoCoords)??")
			
			VECTOR vRandomCoord = <<GET_RANDOM_INT_IN_RANGE(-40, 40), GET_RANDOM_INT_IN_RANGE(-40, 40), 0.0>>
			
			VECTOR vSafeCoord
			IF GET_SAFE_COORD_FOR_PED(vRandomCoord, FALSE, vSafeCoord)
				vRandomCoord = vSafeCoord
			ENDIF
			
			CPRINTLN(DEBUG_FAMILY, "vRandomCoord[", iSecondClosest, "]: ", vRandomCoord, "	//emergency bail")
			
			RETURN vRandomCoord
		ENDIF
		#ENDIF
	
		iSecondClosest = GET_RANDOM_INT_IN_RANGE(0, iTotalAmount-1)
	ENDIF
	
	VECTOR vRandomCoord = vEmergencyGotoCoords[iSecondClosest]
	
	VECTOR vSafeCoord
	IF GET_SAFE_COORD_FOR_PED(vEmergencyGotoCoords[iSecondClosest], FALSE, vSafeCoord)
		vRandomCoord = vSafeCoord
	ENDIF
	
	CPRINTLN(DEBUG_FAMILY, "vRandomCoord[", iSecondClosest, "]: ", vRandomCoord, " aka ", vEmergencyGotoCoords[iSecondClosest])
	
	IF ARE_VECTORS_EQUAL(vRandomCoord, <<0,0,0>>)
		CASSERTLN(DEBUG_FAMILY, "PRIVATE_get_random_family_point(", Get_String_From_FamilyMember(eFamilyMember), ") is zero??? #1718002")
		
		RETURN vOffset
	ELSE
		RETURN vRandomCoord
	ENDIF
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Wander_Event(PED_INDEX PedIndex, enumFamilyMember eFamilyMember,
		INT &iWanderStage, 
		OBJECT_INDEX &objLeftHand, MODEL_NAMES &ObjLeftHandModel,
		OBJECT_INDEX &objRightHand, MODEL_NAMES &eObjRightHandModel,
		OBJECT_INDEX &objChair,
		INT &iScene)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
		DETACH_SYNCHRONIZED_SCENE(iScene)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "detach synch scene ", Get_String_From_FamilyMember(eFamilyMember))
		#ENDIF
	ENDIF
	IF (iScene <> -1)
		iScene = -1
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "invalidate synch scene ", Get_String_From_FamilyMember(eFamilyMember))
		#ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		
		#IF IS_DEBUG_BUILD
		str  = "wanted: "
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_BLUEDARK)
		#ENDIF
		
		IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_SMART_FLEE_PED) <> PERFORMING_TASK)
			
			PED_INDEX nearbyPeds[10]
			INT iNearbyPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), nearbyPeds)
			INT iPed
			REPEAT iNearbyPeds iPed
				IF DOES_ENTITY_EXIST(nearbyPeds[iPed])
				AND NOT IS_PED_INJURED(nearbyPeds[iPed])
					IF GET_PED_TYPE(nearbyPeds[iPed]) = PEDTYPE_COP
						TASK_SMART_FLEE_PED(PedIndex, nearbyPeds[iPed], 75, -1, TRUE)
			
						iWanderStage = 0
						RETURN FALSE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_SMART_FLEE_POINT) <> PERFORMING_TASK)
				TASK_SMART_FLEE_COORD(PedIndex, GET_ENTITY_COORDS(PLAYER_PED_ID()), 75, -1, TRUE)
				iWanderStage = 0
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objLeftHand)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(objLeftHand, PedIndex)
				CPRINTLN(DEBUG_FAMILY, "detach left hand prop ", Get_String_From_FamilyMember(eFamilyMember))
				
				DETACH_ENTITY(objLeftHand)
				
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objLeftHand, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "left hand prop detach failed")
					CPRINTLN(DEBUG_FAMILY, "running inverse detach ped ", Get_String_From_FamilyMember(eFamilyMember), " from left hand prop")
					
					DETACH_ENTITY(PedIndex)
				ENDIF
				
				ObjLeftHandModel = DUMMY_MODEL_FOR_SCRIPT
				objLeftHand = NULL
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(objRightHand)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(objRightHand, PedIndex)
				CPRINTLN(DEBUG_FAMILY, "detach right hand prop ", Get_String_From_FamilyMember(eFamilyMember))
				
				DETACH_ENTITY(objRightHand)
				
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objRightHand, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "right hand prop detach failed")
					CPRINTLN(DEBUG_FAMILY, "running inverse detach ped ", Get_String_From_FamilyMember(eFamilyMember), " from right hand prop")
					
					DETACH_ENTITY(PedIndex)
				ENDIF
				
				eObjRightHandModel = DUMMY_MODEL_FOR_SCRIPT
				objRightHand = NULL
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objChair)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(objChair, PedIndex)
				CPRINTLN(DEBUG_FAMILY, "detach chair prop from ped ", Get_String_From_FamilyMember(eFamilyMember))
				
				DETACH_ENTITY(objChair)
				
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objChair, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "chair detach failed")
					CPRINTLN(DEBUG_FAMILY, "running inverse detach ped ", Get_String_From_FamilyMember(eFamilyMember), " from chair prop")
					
					DETACH_ENTITY(PedIndex)
				ENDIF

				objChair = NULL
			ENDIF
			
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	CONST_INT 	Time 	 DEFAULT_TIME_NEVER_WARP		//DEFAULT_TIME_BEFORE_WARP
	CONST_FLOAT Radius 	 DEFAULT_NAVMESH_RADIUS
	
	CONST_INT	iCONST_WANDER_0_navmesh			0
	CONST_INT	iCONST_WANDER_1_wander			1
	CONST_INT	iCONST_WANDER_2_scenario		2
	
	CONST_FLOAT	fDIST_SCENARIO					5.0	//10.0
	
	SWITCH iWanderStage
		CASE iCONST_WANDER_0_navmesh
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("0. iWander - Navmesh", GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_GREENDARK)
			#ENDIF
			
			IF PRIVATE_Update_Family_Find_Event(PedIndex, eFamilyMember, GET_ENTITY_COORDS(PedIndex, FALSE), 40.0)
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_Find_Event (wander)!!!")
				
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
				
				iWanderStage = 0		//reset
				RETURN TRUE
			ENDIF
			
			IF DOES_ENTITY_EXIST(objLeftHand)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objLeftHand, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "detach left hand prop")
					
					DETACH_ENTITY(objLeftHand)
					objLeftHand = NULL
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(objRightHand)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objRightHand, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "detach right hand prop")
					
					DETACH_ENTITY(objRightHand)
					objLeftHand = NULL
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(objChair)
				IF IS_ENTITY_ATTACHED_TO_ENTITY(objChair, PedIndex)
					CPRINTLN(DEBUG_FAMILY, "detach chair prop")
					
					DETACH_ENTITY(objChair)
					
					objChair = NULL
				ENDIF
			ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK)
				IF NOT IS_PED_IN_ANY_VEHICLE(PedIndex, TRUE)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, PRIVATE_get_random_family_point(PedIndex, eFamilyMember, TRUE),
							PEDMOVE_WALK, Time, Radius)
							
					RETURN FALSE
				ENDIF
			ELSE
			
				WAIT(5)
				IF IS_PED_INJURED(PedIndex)
					RETURN FALSE
				ENDIF
				
				NAVMESH_ROUTE_RESULT enavmesh_route_result
				FLOAT fOut_DistanceRemaining
				INT iOut_ThisIsLastRouteSection
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("navmesh...", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_GREENDARK)
				
				str  = "navtask["
				SWITCH enavmesh_route_result
					CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
					DEFAULT
						str += ENUM_TO_INT(enavmesh_route_result)
					BREAK
				ENDSWITCH
				str += ", "
				str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
				str += "m, s:"
				str += (iOut_ThisIsLastRouteSection)
				str += "]"
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_BLUE)
				#ENDIF
			
				IF enavmesh_route_result != NAVMESHROUTE_ROUTE_NOT_FOUND
					iWanderStage = iCONST_WANDER_1_wander
					RETURN FALSE
				ELSE
					CLEAR_PED_TASKS(PedIndex)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, PRIVATE_get_random_family_point(PedIndex, eFamilyMember, FALSE),
							PEDMOVE_WALK, Time, Radius)
					
					WAIT(0)
					
					IF NOT IS_PED_INJURED(PedIndex)
					AND (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)
						
						enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
					
						IF enavmesh_route_result != NAVMESHROUTE_ROUTE_NOT_FOUND
							iWanderStage = iCONST_WANDER_1_wander
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// //
			STRING scenarioTypeName
			scenarioTypeName = "PROP_HUMAN_SEAT_CHAIR"
			BOOL bEnableScenarioType
			bEnableScenarioType = TRUE
			
			enumFamilyMember eThisFamMember
			PED_COMPONENT ePedCompID
			INT iDrawableId, iTextureID
			IF GetComponentForFamilyMember(FC_AMANDA_LEGS_5, eThisFamMember, ePedCompID, iDrawableId, iTextureID)
			
				IF (eFamilyMember = eThisFamMember)
					IF GET_PED_DRAWABLE_VARIATION(PedIndex, ePedCompID) = iDrawableId
					AND GET_PED_TEXTURE_VARIATION(PedIndex, ePedCompID) = iTextureID
						
						IF IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
							CPRINTLN(DEBUG_FAMILY, "disable \"", scenarioTypeName, "\" for heels...")
							
							SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, FALSE)
							bEnableScenarioType = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PedIndex, FALSE))
				IF NOT IS_PED_IN_ANY_VEHICLE(PedIndex, TRUE)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_BUMP)
					
				//	TASK_WANDER_IN_AREA(PedIndex, GET_ENTITY_COORDS(PedIndex, FALSE), 40.0)
					TASK_WANDER_STANDARD(PedIndex)
					
					iWanderStage = iCONST_WANDER_1_wander
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE iCONST_WANDER_1_wander
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("1. iWander - Wander", GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_PINK)
//			DrawDebugFamilySphere(vSequencePos, fWanderRad, HUD_COLOUR_PINK, 0.25)
			#ENDIF
			
			IF PRIVATE_Update_Family_Find_Event(PedIndex, eFamilyMember, GET_ENTITY_COORDS(PedIndex, FALSE), 40.0)
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_Find_Event (wander)!!!")
				
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
				
				iWanderStage = 0
				RETURN TRUE
			ENDIF
			
			IF NOT DOES_SCENARIO_EXIST_IN_AREA(GET_ENTITY_COORDS(PedIndex), fDIST_SCENARIO, TRUE)
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("NO scenario in area", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_PINK)
				#ENDIF
				
			ELIF IS_SCENARIO_OCCUPIED(GET_ENTITY_COORDS(PedIndex), fDIST_SCENARIO, TRUE)
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("scenario OCCUPIED in area", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_PINK)
				#ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("scenario in area", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_PINK)
				#ENDIF
				
				
//				IF eFamilyMember = FM_MICHAEL_SON
//					IF IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
//						CPRINTLN(DEBUG_FAMILY, "disable \"")
//						CPRINTLN(DEBUG_FAMILY, scenarioTypeName)
//						CPRINTLN(DEBUG_FAMILY, "\" for jimmy...")
//						CprintNL()
//						
//						SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, FALSE)
//						bEnableScenarioType = FALSE
//					ENDIF
//				ENDIF
			
				IF bEnableScenarioType
//					IF NOT IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
//						CPRINTLN(DEBUG_FAMILY, "reenable \"")
//						CPRINTLN(DEBUG_FAMILY, scenarioTypeName)
//						CPRINTLN(DEBUG_FAMILY, "\" (not heels, not jimmy)...")
//						CprintNL()
//						
//						SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, TRUE)
//					ENDIF
				ENDIF
				
				IF (g_eCurrentSafehouseActivity = SA_MICHAEL_SOFA)
					IF NOT IS_STRING_NULL_OR_EMPTY(scenarioTypeName)
					AND IS_SCENARIO_TYPE_ENABLED(scenarioTypeName)
						CPRINTLN(DEBUG_FAMILY, "disable \"", scenarioTypeName, "\" for sofa activity...")
						
						SET_SCENARIO_TYPE_ENABLED(scenarioTypeName, FALSE)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF NOT (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK)
					TASK_USE_NEAREST_SCENARIO_TO_COORD(PedIndex,
							GET_ENTITY_COORDS(PedIndex), fDIST_SCENARIO, 0)
				ELSE
					SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
					
					iWanderStage = iCONST_WANDER_2_scenario
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)
			
				NAVMESH_ROUTE_RESULT enavmesh_route_result
				FLOAT fOut_DistanceRemaining
				INT iOut_ThisIsLastRouteSection
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("navmesh...", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_PINK)
				
				str  = "navtask["
				SWITCH enavmesh_route_result
					CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
					DEFAULT
						str += ENUM_TO_INT(enavmesh_route_result)
					BREAK
				ENDSWITCH
				str += ", "
				str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
				str += "m, s:"
				str += (iOut_ThisIsLastRouteSection)
				str += "]"
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 5, HUD_COLOUR_BLUE)
				#ENDIF
				
				IF enavmesh_route_result = NAVMESHROUTE_ROUTE_NOT_FOUND
					VECTOR vRandomCoord
					vRandomCoord = PRIVATE_get_random_family_point(PedIndex, eFamilyMember, FALSE)
					
					CLEAR_PED_TASKS(PedIndex)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, vRandomCoord, PEDMOVE_WALK,
							Time, Radius)
					
					CPRINTLN(DEBUG_FAMILY, "vRandomCoord: ", vRandomCoord)
					
					iWanderStage = iCONST_WANDER_0_navmesh
					RETURN FALSE
				ENDIF
				
				RETURN FALSE
			ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) = PERFORMING_TASK)
			OR (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) = PERFORMING_TASK)
				NAVMESH_ROUTE_RESULT enavmesh_route_result
				FLOAT fOut_DistanceRemaining
				INT iOut_ThisIsLastRouteSection
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				#IF IS_DEBUG_BUILD
				str  = "wandtask["
				SWITCH enavmesh_route_result
					CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
					DEFAULT
						str += ENUM_TO_INT(enavmesh_route_result)
					BREAK
				ENDSWITCH
				str += ", "
				str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
				str += "m, s:"
				str += (iOut_ThisIsLastRouteSection)
				str += "]"
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 5, HUD_COLOUR_BLUE)
				#ENDIF
				
				IF enavmesh_route_result = NAVMESHROUTE_ROUTE_NOT_FOUND
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, PRIVATE_get_random_family_point(PedIndex, eFamilyMember, TRUE),
							PEDMOVE_WALK, Time, Radius)
					iWanderStage = iCONST_WANDER_0_navmesh
						
					CPRINTLN(DEBUG_FAMILY, "NAVMESHROUTE_ROUTE_NOT_FOUND (wandmiss)")
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF IS_PED_RAGDOLL(PedIndex)
				SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT|RBF_PLAYER_BUMP)
				
				iWanderStage = iCONST_WANDER_0_navmesh
				RETURN FALSE
			ENDIF
			
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_IN_AREA) = PERFORMING_TASK)
			OR (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) = PERFORMING_TASK)
			OR (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK)
			OR (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_WANDER_STANDARD) = PERFORMING_TASK)
			
			ELSE
				iWanderStage = iCONST_WANDER_0_navmesh
				RETURN FALSE
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE iCONST_WANDER_2_scenario
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("2. iWander - Scenario", GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_BLUE)
			#ENDIF
			
			IF (GET_SCRIPT_TASK_STATUS(PedIndex, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK)
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("not performing...", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_BLUE)
				#ENDIF
				
				IF NOT (g_eCurrentSafehouseActivity = SA_MICHAEL_SOFA)
					IF DOES_SCENARIO_EXIST_IN_AREA(GET_ENTITY_COORDS(PedIndex), fDIST_SCENARIO, TRUE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(PedIndex,
								GET_ENTITY_COORDS(PedIndex), fDIST_SCENARIO, 0)
						
						NAVMESH_ROUTE_RESULT enavmesh_route_result
						FLOAT fOut_DistanceRemaining
						INT iOut_ThisIsLastRouteSection
						enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
						
						#IF IS_DEBUG_BUILD
						str  = "wandtask["
						SWITCH enavmesh_route_result
							CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
							CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
							CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
							CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
							DEFAULT
								str += ENUM_TO_INT(enavmesh_route_result)
							BREAK
						ENDSWITCH
						str += ", "
						str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
						str += "m, s:"
						str += (iOut_ThisIsLastRouteSection)
						str += "]"
						DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 5, HUD_COLOUR_BLUE)
						#ENDIF
						
						IF enavmesh_route_result = NAVMESHROUTE_ROUTE_NOT_FOUND
						or enavmesh_route_result = NAVMESHROUTE_TASK_NOT_FOUND
							VECTOR vRandomCoord
							vRandomCoord = PRIVATE_get_random_family_point(PedIndex, eFamilyMember, TRUE)
							
							TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, vRandomCoord,
									PEDMOVE_WALK, Time, Radius)
							iWanderStage = iCONST_WANDER_0_navmesh
							
							CPRINTLN(DEBUG_FAMILY, "NAVMESHROUTE_ROUTE_NOT_FOUND (wandmiss)")
							RETURN FALSE
						ENDIF
						
					ELSE
						iWanderStage = iCONST_WANDER_1_wander
						RETURN FALSE
					ENDIF
				ELSE
					VECTOR vRandomCoord
					vRandomCoord = PRIVATE_get_random_family_point(PedIndex, eFamilyMember, TRUE)
					
					TASK_FOLLOW_NAV_MESH_TO_COORD(PedIndex, vRandomCoord,
							PEDMOVE_WALK, Time, Radius)
					iWanderStage = iCONST_WANDER_0_navmesh
					
					CPRINTLN(DEBUG_FAMILY, "NAVMESHROUTE_ROUTE_NOT_FOUND (wandmiss)")
					RETURN FALSE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("performing...", GET_ENTITY_COORDS(PedIndex, FALSE), 4, HUD_COLOUR_BLUE)
				#ENDIF
				
			ENDIF
			
			IF NOT PED_HAS_USE_SCENARIO_TASK(PedIndex)
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("not has task...", GET_ENTITY_COORDS(PedIndex, FALSE), 5, HUD_COLOUR_BLUE)
				#ENDIF
				
			ELSE
				NAVMESH_ROUTE_RESULT enavmesh_route_result
				FLOAT fOut_DistanceRemaining
				INT iOut_ThisIsLastRouteSection
				enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(PedIndex, fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
				
				#IF IS_DEBUG_BUILD
				str  = "scenario["
				SWITCH enavmesh_route_result
					CASE NAVMESHROUTE_TASK_NOT_FOUND		str += "TASK_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_YET_TRIED	str += "ROUTE_NOT_YET_TRIED" BREAK
					CASE NAVMESHROUTE_ROUTE_NOT_FOUND		str += "ROUTE_NOT_FOUND" BREAK
					CASE NAVMESHROUTE_ROUTE_FOUND			str += "ROUTE_FOUND" BREAK
					DEFAULT
						str += ENUM_TO_INT(enavmesh_route_result)
					BREAK
				ENDSWITCH
				str += ", "
				str += GET_STRING_FROM_FLOAT(fOut_DistanceRemaining)
				str += "m, s:"
				str += (iOut_ThisIsLastRouteSection)
				str += "]"
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 5, HUD_COLOUR_BLUE)
				#ENDIF
				
				IF PRIVATE_Update_Family_Find_Event(PedIndex, eFamilyMember, GET_ENTITY_COORDS(PedIndex, FALSE), 40.0)
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Update_Family_Find_Event (scenario)!!!")
					RETURN TRUE
				ENDIF
				
				IF (iOut_ThisIsLastRouteSection > 0)
					IF IS_SCENARIO_OCCUPIED(GET_ENTITY_COORDS(PedIndex), fOut_DistanceRemaining+0.5, TRUE)
						
						enavmesh_route_result = enavmesh_route_result
						
						CLEAR_PED_TASKS(PedIndex)
						iWanderStage = iCONST_WANDER_1_wander
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			str  = "iWanderStage: "
			str +=  iWanderStage
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_ORANGELIGHT)
			#ENDIF
		
		BREAK
	ENDSWITCH
	
	eFamilyMember = eFamilyMember
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Cop(PED_INDEX CrookPedIndex, enumFamilyMember eFamilyMember, PED_INDEX &CopPedIndex)
	IF NOT DOES_ENTITY_EXIST(CopPedIndex)
//		CLEAR_PED_TASKS(CopPedIndex)
		RETURN FALSE	//TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(CopPedIndex)
		IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			//
			WEAPON_TYPE TypeOfWeapon = WEAPONTYPE_PISTOL
			IF NOT HAS_PED_GOT_WEAPON(CrookPedIndex, TypeOfWeapon)
				GIVE_WEAPON_TO_PED(CrookPedIndex, TypeOfWeapon, INFINITE_AMMO, FALSE)
			ENDIF
			IF NOT HAS_PED_GOT_WEAPON(CopPedIndex, TypeOfWeapon)
				GIVE_WEAPON_TO_PED(CopPedIndex, TypeOfWeapon, INFINITE_AMMO, FALSE)
			ENDIF
			
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), CopPedIndex)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				
				RETURN FALSE
			ENDIF
			
			IF NOT IS_PED_FACING_PED(CopPedIndex, CrookPedIndex, 15.0)
				TASK_TURN_PED_TO_FACE_ENTITY(CopPedIndex, CrookPedIndex)
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyLine(GET_ENTITY_COORDS(CopPedIndex), GET_ENTITY_COORDS(CrookPedIndex), HUD_COLOUR_REDDARK)
				#ENDIF
				
			ELSE
				IF NOT PED_HAS_USE_SCENARIO_TASK(CopPedIndex)
					TASK_START_SCENARIO_IN_PLACE(CopPedIndex, "WORLD_VEHICLE_POLICE_NEXT_TO_CAR")
				ENDIF
			ENDIF
//			IF NOT IS_PED_FACING_PED(CrookPedIndex, CopPedIndex, 30.0)
//				TASK_TURN_PED_TO_FACE_ENTITY(CrookPedIndex, CopPedIndex)
//			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyLine(GET_ENTITY_COORDS(CopPedIndex), GET_ENTITY_COORDS(CrookPedIndex), HUD_COLOUR_BLUEDARK)
			#ENDIF
		ELSE
			//
			TASK_COMBAT_PED(CopPedIndex, PLAYER_PED_ID())
			SET_PED_KEEP_TASK(CopPedIndex, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(CopPedIndex)
			
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_find_family_event)
			
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
