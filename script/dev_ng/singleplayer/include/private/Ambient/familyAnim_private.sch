///private header for family anim scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "commands_task.sch"

// *******************************************************************************************
//	FAMILY ANIM PRIVATE FUNCTIONS
// *******************************************************************************************

ENUM enumFamilyAnimProgress
	FAP_0_default = 0,
	FAP_1_placeholder,
	FAP_2_dialogue,
	FAP_3_array,
	FAP_4_scenario,
	
	MAX_FAMILY_ANIM_PROGRESS
ENDENUM

FUNC BOOL PRIVATE_Placeholder_FamilyMember_Anim(enumFamilyEvents eFamilyEvent, TEXT_LABEL_63 &tFamilyAnimDict, STRING sFamilyAnimDict,
		TEXT_LABEL_63 &tFamilyAnimClip, STRING sFamilyAnimClip,
		enumFamilyAnimProgress &eFamilyAnimProgress)
	
	IF (eFamilyEvent <> NO_FAMILY_EVENTS)
	ENDIF
	
	tFamilyAnimDict = sFamilyAnimDict
	tFamilyAnimClip = sFamilyAnimClip
	
	eFamilyAnimProgress = FAP_1_placeholder
	RETURN TRUE
ENDFUNC
FUNC BOOL PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(enumFamilyEvents eFamilyEvent, TEXT_LABEL_63 &tFamilyAnimDict, TEXT_LABEL_63 &tFamilyAnimClip, enumFamilyAnimProgress &eFamilyAnimProgress)
	RETURN PRIVATE_Placeholder_FamilyMember_Anim(eFamilyEvent,
			tFamilyAnimDict,
			"AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE",	//"AMB@STAND_SMOKE",
			tFamilyAnimClip,
			"BASE",										//"StandSmoking",
			eFamilyAnimProgress)
ENDFUNC

FUNC BOOL PRIVATE_Preload_FamilyMember_Anim(enumFamilyEvents eFamilyEvent, TEXT_LABEL_63 &tFamilyAnimDict)
	
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_12"	RETURN TRUE	BREAK
		
		CASE FE_M7_FAMILY_finished_breakfast		tFamilyAnimDict = "TIMETABLE@REUNITED@IG_9"	RETURN TRUE	BREAK
		CASE FE_M7_FAMILY_finished_pizza			tFamilyAnimDict = "TIMETABLE@REUNITED@IG_9"	RETURN TRUE	BREAK
		CASE FE_M7_FAMILY_watching_TV				tFamilyAnimDict = "TIMETABLE@REUNITED@IG_10" RETURN TRUE BREAK
		
		CASE FE_M_SON_sleeping						tFamilyAnimDict = "TIMETABLE@JIMMY@IG_3@BASE" RETURN TRUE BREAK
		CASE FE_M2_SON_gaming_loop					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_2@IG_2_P2" RETURN TRUE BREAK
		CASE FE_M7_SON_gaming						tFamilyAnimDict = "TIMETABLE@JIMMY@IG_2@IG_2_P2" RETURN TRUE BREAK
		CASE FE_M_SON_in_room_asks_for_munchies		tFamilyAnimDict = "TIMETABLE@JIMMY@IG_3@SLEEPING" RETURN TRUE BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly		tFamilyAnimDict = "TIMETABLE@JIMMY@IG_5@BASE" RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_A		tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_2@" RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_B		tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_7@" RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_C		tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_8@" RETURN TRUE BREAK
		CASE FE_M_SON_Fighting_with_sister_D		tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_11@" RETURN TRUE BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong		tFamilyAnimDict = "TIMETABLE@JIMMY@IG_1@BASE" RETURN TRUE BREAK
		CASE FE_M_SON_raids_fridge_for_food			tFamilyAnimDict = "TIMETABLE@JIMMY@IG_4@BASE" RETURN TRUE BREAK
		CASE FE_M7_SON_jumping_jacks				tFamilyAnimDict = "TIMETABLE@REUNITED@IG_2" RETURN TRUE BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs	tFamilyAnimDict = "SWITCH@MICHAEL@AROUND_THE_TABLE_SELFISH" RETURN TRUE BREAK
		CASE FE_M2_SON_watching_TV					tFamilyAnimDict = "SWITCH@MICHAEL@ON_SOFA" RETURN TRUE BREAK
		CASE FE_M7_SON_watching_TV_with_tracey		tFamilyAnimDict = "TIMETABLE@JIMMY@MICS3_IG_15@" RETURN TRUE BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing				tFamilyAnimDict = "TIMETABLE@TRACY@IG_3@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3			tFamilyAnimDict = "TIMETABLE@TRACY@IG_5@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_dancing_practice			tFamilyAnimDict = "TIMETABLE@TRACY@IG_8@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom	tFamilyAnimDict = "TIMETABLE@TRACY@IG_7@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober		tFamilyAnimDict = "TIMETABLE@TRACY@IG_2@IDLE_A" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk		tFamilyAnimDict = "TIMETABLE@TRACY@IG_15@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad			tFamilyAnimDict = "TIMETABLE@TRACY@IG_4@" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy		tFamilyAnimDict = "TIMETABLE@TRACY@IG_1@BASE" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk		tFamilyAnimDict = "TIMETABLE@TRACY@IG_14@" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_couchsleep				tFamilyAnimDict = "TIMETABLE@TRACY@SLEEP@" RETURN TRUE	BREAK
		CASE FE_M7_DAUGHTER_studying_on_phone		tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_4" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends		tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_4" RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_on_phone_LOCKED			tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_4" RETURN TRUE	BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails		tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_5" RETURN TRUE	BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_9" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_in_face_mask				tFamilyAnimDict = "TIMETABLE@AMANDA@FACEMASK@BASE" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_in_face_mask				tFamilyAnimDict = "TIMETABLE@AMANDA@FACEMASK@BASE" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_doing_yoga					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_4" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_doing_yoga					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_4" RETURN TRUE	BREAK
		CASE FE_M_WIFE_leaving_in_car				tFamilyAnimDict = "TIMETABLE@AMANDA@MAGDEMO_IG_2_SYNCED" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter	tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_with_shopping_bags_enter	tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7" RETURN TRUE	BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen		tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_sunbathing					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_5" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_sunbathing					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_5" RETURN TRUE	BREAK
		CASE FE_M_WIFE_passed_out_BED				tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK@BASE" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_passed_out_SOFA				tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK@BASE" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_passed_out_SOFA				tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK@BASE" RETURN TRUE	BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2" RETURN TRUE	BREAK
		CASE FE_M_WIFE_screaming_at_son_P2			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2_P2" RETURN TRUE	BREAK
		CASE FE_M_WIFE_screaming_at_son_P3			tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK@BASE" RETURN TRUE	BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter		tFamilyAnimDict = "TIMETABLE@AMANDA@IG_3" RETURN TRUE	BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist		tFamilyAnimDict = "TIMETABLE@AMANDA@IG_11"		RETURN TRUE	BREAK
		CASE FE_M7_WIFE_phones_man_OR_therapist		tFamilyAnimDict = "TIMETABLE@AMANDA@IG_11"		RETURN TRUE	BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_11"		RETURN TRUE	BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator				tFamilyAnimDict = "TIMETABLE@AMANDA@IG_6" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_using_vibrator				tFamilyAnimDict = "TIMETABLE@AMANDA@IG_6" RETURN TRUE	BREAK
		CASE FE_M_WIFE_using_vibrator_END			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_6" RETURN TRUE	BREAK
		#ENDIF
		CASE FE_M2_WIFE_sleeping					tFamilyAnimDict = "SWITCH@MICHAEL@BEDROOM"		RETURN TRUE	BREAK
		CASE FE_M7_WIFE_sleeping					tFamilyAnimDict = "SWITCH@MICHAEL@GETS_READY" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_Making_juice				tFamilyAnimDict = "TIMETABLE@REUNITED@IG_6" RETURN TRUE	BREAK
		CASE FE_M7_WIFE_shopping_with_daughter		tFamilyAnimDict = "TIMETABLE@REUNITED@IG_7" RETURN TRUE	BREAK
//		CASE FE_M7_WIFE_shopping_with_son			tFamilyAnimDict = "TIMETABLE@REUNITED@IG_8" RETURN TRUE	BREAK
		
		CASE FE_M2_MEXMAID_clean_surface_a			tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE@BASE"	RETURN TRUE
		CASE FE_M2_MEXMAID_clean_surface_c			tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE@BASE"	RETURN TRUE
		CASE FE_M7_MEXMAID_clean_surface			tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE@BASE"	RETURN TRUE
		CASE FE_M2_MEXMAID_clean_surface_b			tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"	RETURN TRUE
		CASE FE_M2_MEXMAID_clean_window				tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_WINDOW@BASE"	RETURN TRUE	BREAK
		CASE FE_M7_MEXMAID_clean_window				tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_WINDOW@BASE"	RETURN TRUE	BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window			tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_WINDOW@BASE"	RETURN TRUE	BREAK

		CASE FE_M_MEXMAID_does_the_dishes			tFamilyAnimDict = "TIMETABLE@MAID@IG_2@"	RETURN TRUE	BREAK
		CASE FE_M_MEXMAID_stealing_stuff			tFamilyAnimDict = "TIMETABLE@MAID@IG_8@"	RETURN TRUE	BREAK
		CASE FE_M_MEXMAID_stealing_stuff_caught		tFamilyAnimDict = "TIMETABLE@MAID@IG_8@"	RETURN TRUE	BREAK
		
		CASE FE_M_GARDENER_cleaning_pool			tFamilyAnimDict = "TIMETABLE@GARDENER@CLEAN_POOL@"	RETURN TRUE	BREAK
		CASE FE_M_GARDENER_mowing_lawn				tFamilyAnimDict = "TIMETABLE@GARDENER@LAWNMOW@"	RETURN TRUE	BREAK
		CASE FE_M_GARDENER_watering_flowers			tFamilyAnimDict = "TIMETABLE@GARDENER@FILLING_CAN"	RETURN TRUE	BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises		tFamilyAnimDict = "TIMETABLE@DENICE@IG_1"	RETURN TRUE BREAK
		CASE FE_F_AUNT_in_face_mask					tFamilyAnimDict = "TIMETABLE@DENICE@IG_2"	RETURN TRUE	BREAK
		CASE FE_F_AUNT_watching_TV					tFamilyAnimDict = "TIMETABLE@DENICE@IG_3"	RETURN TRUE	BREAK
		CASE FE_F_AUNT_returned_to_aunts			tFamilyAnimDict = "TIMETABLE@DENICE@IG_3"	RETURN TRUE	BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x	tFamilyAnimDict = "TIMETABLE@DENICE@IG_4"	BREAK
		
		CASE FE_T0_RON_monitoring_police_frequency	tFamilyAnimDict = "TIMETABLE@RON@HAND_RADIO_IG_1"	RETURN TRUE	BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast	tFamilyAnimDict = "TIMETABLE@RON@IG_2"	RETURN TRUE	BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals	tFamilyAnimDict = "TIMETABLE@RON@IG_2"	RETURN TRUE		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING		tFamilyAnimDict = "TIMETABLE@RON@IG_3_COUCH"		RETURN TRUE	BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING		tFamilyAnimDict = "TIMETABLE@RON@IG_3_COUCH"		RETURN TRUE	BREAK
		CASE FE_T0_RON_smoking_crystal						tFamilyAnimDict = "TIMETABLE@RON@IG_4_SMOKING_METH"	RETURN TRUE	BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar			tFamilyAnimDict = "TIMETABLE@RON@MOONSHINE_IG_5"	RETURN TRUE	BREAK
		CASE FE_T0_RON_stares_through_binoculars			tFamilyAnimDict = "TIMETABLE@RON@IG_6"				RETURN TRUE	BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands	tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_CHAIRIDLE_A"	RETURN TRUE	BREAK
		CASE FE_T0_MICHAEL_sunbathing				tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_CLUBCHAIRBASE"	RETURN TRUE	BREAK
		CASE FE_T0_MICHAEL_drinking_beer			tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_SOFABASE"	RETURN TRUE	BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist	tFamilyAnimDict = "TIMETABLE@MICHAEL@TALK_PHONEbase"	RETURN TRUE	BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders		tFamilyAnimDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT_A"	RETURN TRUE	BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk	tFamilyAnimDict = "TIMETABLE@TREVOR@IG_1"	RETURN TRUE	BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare	tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_2"	RETURN TRUE	BREAK
		CASE FE_T0_TREVOR_smoking_crystal			tFamilyAnimDict = "TIMETABLE@TREVOR@SMOKING_METH@BASE"	RETURN TRUE	BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit				tFamilyAnimDict = "TIMETABLE@TREVOR@ON_THE_TOILET"	RETURN TRUE	BREAK
		#ENDIF
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh	tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_5BASE"	RETURN TRUE	BREAK
		CASE FE_T0_TREVOR_blowing_shit_up			tFamilyAnimDict = "TIMETABLE@TREVOR@GRENADE_THROWING"	RETURN TRUE	BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk	tFamilyAnimDict = "SWITCH@TREVOR@BED"	RETURN TRUE	BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely		tFamilyAnimDict = "TIMETABLE@RON@IG_1"	RETURN TRUE	BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill	tFamilyAnimDict = "TIMETABLE@RON@IG_3"	RETURN TRUE	BREAK
		CASE FE_T0_RONEX_doing_target_practice		tFamilyAnimDict = "TIMETABLE@RON@IG_4"	RETURN TRUE	BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning			tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_1"	RETURN TRUE	BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work	tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_2@BASE"	RETURN TRUE	BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael	tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_3@"	RETURN TRUE	BREAK
		
		CASE FE_T1_FLOYD_cleaning					tFamilyAnimDict = "TIMETABLE@FLOYD@CLEAN_KITCHEN@BASE"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position	tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED@BASE"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend		tFamilyAnimDict = "TIMETABLE@FLOYD@CALLING"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders		tFamilyAnimDict = "TIMETABLE@FLOYD@ENDING_CALL"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a		tFamilyAnimDict = "TIMETABLE@FLOYD@HIDING_BEHIND_COUCH"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b		tFamilyAnimDict = "TIMETABLE@FLOYD@HIDING_BEHIND_COUCH"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_c		tFamilyAnimDict = "TIMETABLE@FLOYD@HIDING_BEHIND_COUCH"	RETURN TRUE	BREAK
		CASE FE_T1_FLOYD_is_sleeping				tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED_IG_5@"	RETURN TRUE	BREAK
	ENDSWITCH
	
	tFamilyAnimDict = ""
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Get_FamilyMember_Anim(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tFamilyAnimDict, TEXT_LABEL_63 &tFamilyAnimClip, ANIMATION_FLAGS &eFamilyAnimFlag,
		enumFamilyAnimProgress &eFamilyAnimProgress)
	eFamilyAnimFlag = AF_LOOPING | AF_NOT_INTERRUPTABLE
	eFamilyAnimProgress = FAP_0_default
	
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops
			tFamilyAnimDict = "TIMETABLE@AMANDA@IG_12"
			
			/**/
			eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
			eFamilyAnimFlag -= AF_LOOPING
			/**/
			
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				//	TIMETABLE@AMANDA@IG_12/jimmy_base
				//	TIMETABLE@AMANDA@IG_12/jimmy_idle_a
				//	TIMETABLE@AMANDA@IG_12/jimmy_idle_b
					
					tFamilyAnimClip = "jimmy_"
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
				//	TIMETABLE@AMANDA@IG_12/tracy_base
				//	TIMETABLE@AMANDA@IG_12/tracy_idle_a
				//	TIMETABLE@AMANDA@IG_12/tracy_idle_b
					
					tFamilyAnimClip = "tracy_"
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_WIFE
				//	TIMETABLE@AMANDA@IG_12/base_amanda
				//	TIMETABLE@AMANDA@IG_12/idle_a_amanda
				//	TIMETABLE@AMANDA@IG_12/idle_b_amanda
				
					tFamilyAnimClip = "_amanda"
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_FAMILY_MIC4_locked_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_finished_breakfast
		CASE FE_M7_FAMILY_finished_pizza
					
			/*
			eat_takeout_Prop_Laptop_01a.anim
			eating_disorder_Prop_Laptop_01a.anim
			getting_fit_Prop_Laptop_01a.anim
			your_boyfriend_Prop_Laptop_01a.anim
			*/
			
			/**/
			eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
			/**/
			
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					/*
					TIMETABLE@REUNITED@IG_9
					
					eat_takeout_Amanda.anim
					eating_disorder_Amanda.anim
					getting_fit_Amanda.anim
					your_boyfriend_Amanda.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_9"
					tFamilyAnimClip = "base_Amanda"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_SON
					/*
					TIMETABLE@REUNITED@IG_2
					
					eat_takeout_Jimmy.anim
					eating_disorder_Jimmy.anim
					getting_fit_Jimmy.anim
					your_boyfriend_Jimmy.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_9"
					tFamilyAnimClip = "base_Jimmy"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					/*
					TIMETABLE@REUNITED@IG_2
					
					eat_takeout_Tracy.anim
					eating_disorder_Tracy.anim
					getting_fit_Tracy.anim
					your_boyfriend_Tracy.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_9"
					tFamilyAnimClip = "base_Tracy"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					/*
					TIMETABLE@REUNITED@IG_10
					
					base_Amanda.anim
				//	Amanda_IsThisTheBest.anim
					IsThisTheBest_Amanda.anim
					ShouldntYouGuys_Amanda.anim
					watching_this_Amanda.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_10"
					tFamilyAnimClip = "base_Amanda"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_SON
					/*
					TIMETABLE@REUNITED@IG_2
					
					base_Jimmy.anim
				//	Jimmy_IsThisTheBest.anim
					IsThisTheBest_Jimmy.anim
					ShouldntYouGuys_Jimmy.anim
					watching_this_Jimmy.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_10"
					tFamilyAnimClip = "base_Jimmy"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					/*
					TIMETABLE@REUNITED@IG_2
					
					base_Tracy.anim
				//	Tracy_IsThisTheBest.anim
					IsThisTheBest_Tracy.anim
					ShouldntYouGuys_Tracy.anim
					watching_this_Tracy.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_10"
					tFamilyAnimClip = "base_Tracy"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_SON_gaming_loop
		CASE FE_M7_SON_gaming
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_2@IG_2_P2"
					tFamilyAnimClip = "IG_2_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_SON_gaming_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_SON
//					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_2"
//					tFamilyAnimClip = "Exit"
//					
//					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M7_SON_gaming_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_SON
//					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_2"
//					tFamilyAnimClip = "Exit"
//					
//					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
//					eFamilyAnimFlag -= AF_LOOPING
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_SON_rapping_in_the_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					eFamilyAnimFlag |= AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Borrows_sisters_car
		CASE FE_M7_SON_going_for_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_watching_porn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_3"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_DISABLE_FORCED_PHYSICS_UPDATE
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_in_room_asks_for_munchies
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_3@SLEEPING"
					tFamilyAnimClip = "base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					eFamilyAnimProgress = FAP_3_array	//2_dialogue
					
					RETURN TRUE
				
				//TIMETABLE@JIMMY@IG_3@Base.anim
				//TIMETABLE@JIMMY@IG_3@Body_High2.anim
				//TIMETABLE@JIMMY@IG_3@Bring_Me_Some_Food.anim
				//TIMETABLE@JIMMY@IG_3@Grow_This_Strain.anim
				//TIMETABLE@JIMMY@IG_3@Helps_My_Glands.anim
					
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_phone_calls_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "null"
					tFamilyAnimClip = "WORLD_HUMAN_STAND_MOBILE"	//WORLD_HUMAN_STAND_MOBILE_CALL_MALE
					
					eFamilyAnimProgress = FAP_4_scenario
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_5@BASE"
					tFamilyAnimClip = "base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				
				//TIMETABLE@JIMMY@IG_5@BASE/Base.anim
				//TIMETABLE@JIMMY@IG_5@IDLE_A/Good_To_See_You.anim
				//TIMETABLE@JIMMY@IG_5@IDLE_A/I_Dont_Tell_You.anim
				//TIMETABLE@JIMMY@IG_5@IDLE_A/Its_The_Big_Man.anim


				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A	//653018
		//	TIMETABLE@TRACY@IG_9_2/BASE_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_2/BASE_TRACY.anim
		//	TIMETABLE@TRACY@IG_9_2/EXIT_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_2/EXIT_TRACY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_2@"
					tFamilyAnimClip = "BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_2@"
					tFamilyAnimClip = "BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_B	//653025
		//	TIMETABLE@TRACY@IG_9_7@/BASE_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_7@/BASE_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_7@/BASE_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_7@/BASE_TRACY.anim
		
		//	TIMETABLE@TRACY@IG_9_7@/EXIT_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_7@/EXIT_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_7@/EXIT_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_7@/EXIT_TRACY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_7@"
					tFamilyAnimClip = "BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_7@"
					tFamilyAnimClip = "BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_C	//653028
		//	TIMETABLE@TRACY@IG_9_8@/BASE/BASE_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_8@/BASE/BASE_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_8@/BASE/BASE_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_8@/BASE/BASE_TRACY.anim
		
		//	TIMETABLE@TRACY@IG_9_8@/EXIT/EXIT_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_8@/EXIT/EXIT_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_8@/EXIT/EXIT_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_8@/EXIT/EXIT_TRACY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_8@"
					tFamilyAnimClip = "BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_8@"
					tFamilyAnimClip = "BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_D	//653030
		//	TIMETABLE@TRACY@IG_9_11@/BASE/BASE_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_11@/BASE/BASE_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_11@/BASE/BASE_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_11@/BASE/BASE_TRACY.anim
		
		//	TIMETABLE@TRACY@IG_9_11@/EXIT/EXIT_DOOR1.anim
		//	TIMETABLE@TRACY@IG_9_11@/EXIT/EXIT_DOOR2.anim
		//	TIMETABLE@TRACY@IG_9_11@/EXIT/EXIT_JIMMY.anim
		//	TIMETABLE@TRACY@IG_9_11@/EXIT/EXIT_TRACY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_11@"
					tFamilyAnimClip = "BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_9_11@"
					tFamilyAnimClip = "BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_1@BASE"
					tFamilyAnimClip = "base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_raids_fridge_for_food
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					
					tFamilyAnimDict = "TIMETABLE@JIMMY@IG_4"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_jumping_jacks
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					//TIMETABLE@REUNITED@IG_2/Jimmy_BASE.anim
					//TIMETABLE@REUNITED@IG_2/Jimmy_2_bed_Endorphins.anim
					//TIMETABLE@REUNITED@IG_2/Jimmy_GetKnocked.anim
					//TIMETABLE@REUNITED@IG_2/Jimmy_Masterbation.anim
					//TIMETABLE@REUNITED@IG_2/Jimmy_RollsFlapping.anim
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_2"
					tFamilyAnimClip = "Jimmy_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
//					RETURN PRIVATE_Get_FamilyMember_Anim(eFamilyMember, FE_M_FAMILY_on_laptops,
//							tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag,
//							eFamilyAnimProgress)
							

					
					tFamilyAnimDict = "SWITCH@MICHAEL@AROUND_THE_TABLE_SELFISH"
					tFamilyAnimClip = "AROUND_THE_TABLE_SELFISH_BASE_Jimmy"
					
					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_SON_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "SWITCH@MICHAEL@ON_SOFA"
					tFamilyAnimClip = "BASE_Jimmy"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@JIMMY@MICS3_IG_15@"
					tFamilyAnimClip = "MICS3_15_BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@JIMMY@MICS3_IG_15@"
					tFamilyAnimClip = "MICS3_15_BASE_TRACY"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_3@BASE"
					tFamilyAnimClip = "base"
					
					eFamilyAnimProgress = FAP_2_dialogue
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_5"
					tFamilyAnimClip = ""
					
					/*
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_5@BASE"
					tFamilyAnimClip = "base"
					
					TIMETABLE@TRACY@IG_5@IDLE_A/
					Idle_a.anim
					Idle_b.anim
					Idle_c.anim
					
					TIMETABLE@TRACY@IG_5@IDLE_B/
					Idle_d.anim
					Idle_e.anim
					
					*/
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_walks_to_room_music
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_dancing_practice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_8"
					tFamilyAnimClip = ""
					
					/*
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_8@BASE"
					tFamilyAnimClip = "base"
					
					TIMETABLE@TRACY@IG_8@IDLE_A/
					Idle_a.anim
					Idle_b.anim
					Idle_c.anim
					
					TIMETABLE@TRACY@IG_8@IDLE_B/
					Idle_d.anim
					Idle_e.anim
					
					*/
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					//TIMETABLE@TRACY@IG_7@BASE/BASE.anim
					//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_A.anim
					//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_B.anim
					//TIMETABLE@TRACY@IG_7@IDLE_A/IDLE_C.anim
					//TIMETABLE@TRACY@IG_7@IDLE_B/IDLE_D.anim
					
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_7@"
					tFamilyAnimClip = ""
					
//					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					eFamilyAnimFlag |= AF_IGNORE_GRAVITY | AF_TURN_OFF_COLLISION
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_2"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_15"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
				
//TIMETABLE@TRACY@IG_4@/BASE/BASE_BOY.anim
//TIMETABLE@TRACY@IG_4@/BASE/BASE_DOOR.anim
//TIMETABLE@TRACY@IG_4@/BASE/BASE_TRACY.anim
					
//TIMETABLE@TRACY@IG_4@/EXIT/EXIT_BOY.anim
//TIMETABLE@TRACY@IG_4@/EXIT/EXIT_DOOR.anim
//TIMETABLE@TRACY@IG_4@/EXIT/EXIT_TRACY.anim
					
		
		
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_4@"
					tFamilyAnimClip = "BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_1@BASE"
					tFamilyAnimClip = "base"
					
//					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_DISABLE_FORCED_PHYSICS_UPDATE
					
					eFamilyAnimProgress = FAP_2_dialogue
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					
					/*
					
					//TIMETABLE@TRACY@IG_14@/IG_14_BASE_TRACY.anim
					
					//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_A_WhatRUDoingHere_TRACY.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_B_ReallyLoveUDad_TRACY.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IDLE_C_YouGotAnyBlowDad_TRACY.anim
					
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_NOTE.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_PLAYER.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_A_TRACY.anim
					
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_NOTE.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_PLAYER.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_IWishAll_B_TRACY.anim
					
					//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_NOTE.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_PLAYER.anim
					//TIMETABLE@TRACY@IG_14@/IG_14_ParentingAtItsFinest_TRACY.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@TRACY@IG_14@"
					tFamilyAnimClip = "IG_14_BASE_TRACY"
					
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sleeping
			RETURN PRIVATE_Get_FamilyMember_Anim(eFamilyMember, FE_M_DAUGHTER_couchsleep, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag, eFamilyAnimProgress)
		BREAK
		CASE FE_M_DAUGHTER_couchsleep
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					
				//	TIMETABLE@TRACY@SLEEP@BASE
				//	TIMETABLE@TRACY@SLEEP@IDLE_A
				//	TIMETABLE@TRACY@SLEEP@IDLE_B
				//	TIMETABLE@TRACY@SLEEP@IDLE_C
				//	TIMETABLE@TRACY@SLEEP@IDLE_D
				//	TIMETABLE@TRACY@SLEEP@IDLE_E
				//	TIMETABLE@TRACY@SLEEP@IDLE_F
					
					tFamilyAnimDict = "TIMETABLE@TRACY@SLEEP@"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_studying_on_phone
		CASE FE_M_DAUGHTER_on_phone_to_friends
		CASE FE_M_DAUGHTER_on_phone_LOCKED
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					//TIMETABLE@TRACY@FAMR_IG_4
					//BASE.anim
					//IDLE_A.anim
					//IDLE_B.anim
					//IDLE_C.anim
					
					tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_4"
					tFamilyAnimClip = "base"
					
//					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					//TIMETABLE@TRACY@FAMR_IG_5
					//BASE.anim
					//FAMR_IG_5_IAmAboutToCrackThis.anim
					//FAMR_IG_5_IAmTotallyOnTop.anim
					//FAMR_IG_5_IAmTryingtoConcentrate.anim
					//FAMR_IG_5_ThisCollegeStuff.anim
					
					tFamilyAnimDict = "TIMETABLE@TRACY@FAMR_IG_5"
					tFamilyAnimClip = ""
					
//					eFamilyAnimFlag |= /*AF_OVERRIDE_PHYSICS |*/ AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_sunbathing
			RETURN PRIVATE_Get_FamilyMember_Anim(eFamilyMember, FE_M2_DAUGHTER_sunbathing, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag, eFamilyAnimProgress)
		BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid
		//	TIMETABLE@AMANDA@IG_9/IG_9_BASE_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_9/IG_9_BASE_MAID.anim
		//	TIMETABLE@AMANDA@IG_9/IG_9_IAmOnToYou_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_9/IG_9_IAmOnToYou_MAID.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_9"
					tFamilyAnimClip = "IG_9_BASE_AMANDA"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_MEXMAID
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_9"
					tFamilyAnimClip = "IG_9_BASE_MAID"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@AMANDA@FACEMASK@BASE"
					tFamilyAnimClip = "base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_playing_tennis
			RETURN PRIVATE_Get_FamilyMember_Anim(eFamilyMember, FAMILY_MEMBER_BUSY, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag, eFamilyAnimProgress)
		BREAK
		CASE FE_M2_WIFE_doing_yoga
		CASE FE_M7_WIFE_doing_yoga
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_4"
					tFamilyAnimClip = ""
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_nails_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					PRIVATE_Placeholder_FamilyMember_SitAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_leaving_in_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					tFamilyAnimDict = "TIMETABLE@AMANDA@MAGDEMO_IG_2_SYNCED"
					tFamilyAnimClip = "base_demo"
					
					/*
				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/base
				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/exit
					*/
					
					eFamilyAnimFlag |= AF_LOOPING
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE 
//					tFamilyAnimDict = "TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT"
//					tFamilyAnimClip = "base"
//					
//					/*
//				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/base
//				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/exit
//					*/
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE 
//					tFamilyAnimDict = "TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT_STATIC_MOVER"
//					tFamilyAnimClip = "base_demo"
//					
//					/*
//				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/base
//				//	TIMETABLE@AMANDA@MAGDEMO_IG_2_RESHOT/exit
//					*/
//					
//					eFamilyAnimFlag |= AF_LOOPING
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					
				//	TIMETABLE@AMANDA@IG_7/IG_7_ENTER
					
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7"
					tFamilyAnimClip = "IG_7_ENTER"
					
					eFamilyAnimFlag -= AF_LOOPING
					
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle
//		CASE FE_M7_WIFE_with_shopping_bags_idle
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					
//				//	TIMETABLE@AMANDA@IG_7/BASE
//				//	TIMETABLE@AMANDA@IG_7/IDLE_A
//				//	TIMETABLE@AMANDA@IG_7/IDLE_B
//				//	TIMETABLE@AMANDA@IG_7/IDLE_C
//				//	TIMETABLE@AMANDA@IG_7/IDLE_D
//				//	TIMETABLE@AMANDA@IG_7/IDLE_E
//					
//					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7"
//					tFamilyAnimClip = ""
//					
//					eFamilyAnimProgress = FAP_3_array
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit
//		CASE FE_M7_WIFE_with_shopping_bags_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					
//				//	TIMETABLE@AMANDA@IG_7/IG_7_EXIT
//					
//					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_7"
//					tFamilyAnimClip = "IG_7_EXIT"
//					
//					eFamilyAnimFlag -= AF_LOOPING
//					
//					eFamilyAnimProgress = FAP_0_default
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop1.anim
				//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop2.anim
				//	TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@amanda_gets_drunk_loop3.anim
					
					tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK_IN_KITCHEN@"
					tFamilyAnimClip = "amanda_gets_drunk_"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_5"
					tFamilyAnimClip = "IG_5_BASE"
					
					eFamilyAnimProgress = FAP_2_dialogue
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_botox_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					PRIVATE_Placeholder_FamilyMember_SitAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_passed_out_BED
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@AMANDA@DRUNK@BASE"
					tFamilyAnimClip = "base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION
					eFamilyAnimProgress = FAP_2_dialogue
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
//		//	TIMETABLE@AMANDA@IG_2/IG_2_BASE_AMANDA.anim
//		//	TIMETABLE@AMANDA@IG_2/IG_2_BASE_JIMMY.anim
//		//	TIMETABLE@AMANDA@IG_2/IG_2_YouKnowWhat_AMANDA.anim
//		//	TIMETABLE@AMANDA@IG_2/IG_2_YouKnowWhat_JIMMY.anim
//		
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE 
//					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2"
//					tFamilyAnimClip = "IG_2_BASE_AMANDA"
//					
//					eFamilyAnimProgress = FAP_0_default
//					RETURN TRUE
//				BREAK
//				CASE FM_MICHAEL_SON
//					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2"
//					tFamilyAnimClip = "IG_2_BASE_JIMMY"
//					
//					eFamilyAnimProgress = FAP_0_default
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
		//	TIMETABLE@AMANDA@IG_2_P2/IG_2_P2_BASE_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_2_P2/IG_2_P2_BASE_JIMMY.anim
		//	TIMETABLE@AMANDA@IG_2_P2/IG_2_P2_ItsNotWastingTime_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_2_P2/IG_2_P2_ItsNotWastingTime_JIMMY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2_P2"
					tFamilyAnimClip = "IG_2_P2_BASE_AMANDA"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2_P2"
					tFamilyAnimClip = "IG_2_P2_BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screaming_at_son_P3
		//	TIMETABLE@AMANDA@IG_2_P3/IG2_P3_BASE_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_2_P3/IG2_P3_BASE_JIMMY.anim
		//	TIMETABLE@AMANDA@IG_2_P3/IG2_P3_ItsNotABigDeal_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_2_P3/IG2_P3_ItsNotABigDeal_JIMMY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2_P3"
					tFamilyAnimClip = "IG2_P3_BASE_AMANDA"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_SON
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_2_P3"
					tFamilyAnimClip = "IG2_P3_BASE_JIMMY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter
		//	TIMETABLE@AMANDA@IG_3/IG_3_BASE_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_3/IG_3_BASE_TRACY.anim
		//	TIMETABLE@AMANDA@IG_3/IG_3_WhenURPregnant_AMANDA.anim
		//	TIMETABLE@AMANDA@IG_3/IG_3_WhenURPregnant_TRACY.anim
		
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_3"
					tFamilyAnimClip = "IG_3_BASE_AMANDA"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_3"
					tFamilyAnimClip = "IG_3_BASE_TRACY"
					
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist
		CASE FE_M7_WIFE_phones_man_OR_therapist
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_11"
					tFamilyAnimClip = "IG_11_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					//TIMETABLE@AMANDA@IG_11/IG_11_IAmAFastLearner.anim
					//TIMETABLE@AMANDA@IG_11/IG_11_IKnowHeIsInTherapy.anim
					//TIMETABLE@AMANDA@IG_11/IG_11_UhHuhYesOk.anim
				
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_11"
					tFamilyAnimClip = "IG_11_IAmAFastLearner"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET
					eFamilyAnimFlag -= AF_LOOPING
					eFamilyAnimProgress = FAP_2_dialogue
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M7_WIFE_using_vibrator
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					//TIMETABLE@AMANDA@IG_6/IG_6_BASE.anim
					//TIMETABLE@AMANDA@IG_6/IG_6_EXIT.anim
					//TIMETABLE@AMANDA@IG_6/IG_6_END_BASE.anim
					
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_6"
					tFamilyAnimClip = "IG_6_BASE"
					
					eFamilyAnimFlag -= AF_LOOPING
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_using_vibrator_END
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					//TIMETABLE@AMANDA@IG_6/IG_6_BASE.anim
					//TIMETABLE@AMANDA@IG_6/IG_6_EXIT.anim
					//TIMETABLE@AMANDA@IG_6/IG_6_END_BASE.anim
					
					tFamilyAnimDict = "TIMETABLE@AMANDA@IG_6"
					tFamilyAnimClip = "IG_6_END_BASE"
					
					eFamilyAnimFlag -= AF_LOOPING
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		CASE FE_M2_WIFE_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "SWITCH@MICHAEL@BEDROOM"
					tFamilyAnimClip = "BED_LOOP_02_Amanda"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "SWITCH@MICHAEL@GETS_READY"
					tFamilyAnimClip = "001520_02_MICS3_14_GETS_READY_IDLE_AMA"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_Making_juice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_6"
					tFamilyAnimClip = "BASE_Amanda"
					
					eFamilyAnimFlag -= AF_LOOPING
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_shopping_with_daughter
					
			//TIMETABLE@REUNITED@\IG_7\AmandaBase_Amanda.anim
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Amanda.anim
			
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Bag_01.anim
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Bag_02.anim
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Bag_03.anim
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Bag_04.anim
			
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Door_L.anim
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Door_R.anim
			
			//TIMETABLE@REUNITED@\IG_7\ThanksDad_Tracy.anim
			
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_7"
					tFamilyAnimClip = "ThanksDad_Amanda"
					
					eFamilyAnimFlag -= AF_LOOPING
					
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_7"
					tFamilyAnimClip = "ThanksDad_Tracy"
					
					eFamilyAnimFlag -= AF_LOOPING
					
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M7_WIFE_shopping_with_son
//					
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Amanda.anim
//			
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Bag_01.anim
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Bag_02.anim
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Bag_03.anim
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Bag_04.anim
//			
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Door_L.anim
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Door_R.anim
//			
//			//TIMETABLE@REUNITED@\IG_8\totallyprepared_Jimmy.anim
//			
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					
//					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_8"
//					tFamilyAnimClip = "totallyprepared_Amanda"
//					
//					eFamilyAnimFlag -= AF_LOOPING
//					
//					eFamilyAnimProgress = FAP_3_array
//					
//					RETURN TRUE
//				BREAK
//				CASE FM_MICHAEL_SON
//					tFamilyAnimDict = "TIMETABLE@REUNITED@IG_8"
//					tFamilyAnimClip = "totallyprepared_Jimmy"
//					
//					eFamilyAnimFlag -= AF_LOOPING
//					
//					eFamilyAnimProgress = FAP_3_array
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M7_WIFE_on_phone
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
//		CASE FE_M_MEXMAID_cooking_for_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID	PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)	RETURN TRUE	BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_WINDOW@"
					tFamilyAnimClip = ""
					

					
					/*
					
					TIMETABLE@MAID@CLEANING_WINDOW@BASE
					base.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@ENTER
					enter.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@EXIT
					exit.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@IDLE_A
					idle_a.anim
					idle_b.anim
					idle_c.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@IDLE_B
					idle_d.anim
					idle_e.anim
					idle_f.anim
					
					*/
					
					
					eFamilyAnimProgress = FAP_1_placeholder
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_MEXMAID_clean_surface_a
		CASE FE_M2_MEXMAID_clean_surface_c
		CASE FE_M7_MEXMAID_clean_surface
//		CASE FE_M_MEXMAID_MIC4_clean_surface
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					
					tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE"
					tFamilyAnimClip = ""
					
					/*
					
					//TIMETABLE@MAID@CLEANING_SURFACE@BASE/base"
					//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_a"
					//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_b"
					//TIMETABLE@MAID@CLEANING_SURFACE@IDLE_A/idle_c"
					
					*/
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_MEXMAID_clean_surface_b
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					
					tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_SURFACE_1@"
					tFamilyAnimClip = ""
					
					/*
					
					//TIMETABLE@MAID@CLEANING_SURFACE_1@BASE/base"
					//TIMETABLE@MAID@CLEANING_SURFACE_1@IDLE_A/idle_a"
					//TIMETABLE@MAID@CLEANING_SURFACE_1@IDLE_A/idle_b"
					//TIMETABLE@MAID@CLEANING_SURFACE_1@IDLE_A/idle_c"
					
					*/
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_MEXMAID_clean_window
		CASE FE_M7_MEXMAID_clean_window
		CASE FE_M_MEXMAID_MIC4_clean_window
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					
					tFamilyAnimDict = "TIMETABLE@MAID@CLEANING_WINDOW"
					tFamilyAnimClip = ""
					
					/*
					
					TIMETABLE@MAID@CLEANING_WINDOW@BASE
					base.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@ENTER
					enter.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@EXIT
					exit.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@IDLE_A
					idle_a.anim
					idle_b.anim
					idle_c.anim
					
					TIMETABLE@MAID@CLEANING_WINDOW@IDLE_B
					idle_d.anim
					idle_e.anim
					idle_f.anim
					
					*/
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK

		CASE FE_M_MEXMAID_does_the_dishes
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					/*
					TIMETABLE@MAID@IG_2@
					IG_2_BASE.anim
					IG_2_IDLE_A.anim
					IG_2_IDLE_B.anim
					IG_2_IDLE_C.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@MAID@IG_2@"
					tFamilyAnimClip = "IG_2_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_MEXMAID_watching_TV
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID
//					/*
//					TIMETABLE@MAID@COUCH@
//					base.anim
//					idle_a.anim
//					idle_b.anim
//					idle_c.anim
//					idle_d.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@MAID@COUCH@"
//					tFamilyAnimClip = ""
//					
//					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_MEXMAID_stealing_stuff
		CASE FE_M_MEXMAID_stealing_stuff_caught
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					/*
					TIMETABLE@/MAID@/IG_8@/IG_8_BASE.anim
					TIMETABLE@/MAID@/IG_8@/IG_8_IDLE_A.anim
					TIMETABLE@/MAID@/IG_8@/IG_8_P2_IBringThisToday.anim
					TIMETABLE@/MAID@/IG_8@/IG_8_P3_ItLooksBroken.anim
					TIMETABLE@/MAID@/IG_8@/IG_8_P4_ITakeHomeToPolish.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@MAID@IG_8@"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					
					IF (eFamilyEvent = FE_M_MEXMAID_stealing_stuff)
						eFamilyAnimProgress = FAP_3_array
					ENDIF
					IF (eFamilyEvent = FE_M_MEXMAID_stealing_stuff_caught)
						eFamilyAnimProgress = FAP_2_dialogue
					ENDIF
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "null"
					tFamilyAnimClip = "WORLD_HUMAN_GARDENER_LEAF_BLOWER"
					
					eFamilyAnimProgress = FAP_4_scenario
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_planting_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "null"
					tFamilyAnimClip = "WORLD_HUMAN_GARDENER_PLANT"
					
					eFamilyAnimProgress = FAP_4_scenario
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_trimming_hedges
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER	PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)					RETURN TRUE				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_cleaning_pool
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "TIMETABLE@GARDENER@CLEAN_POOL@"
					tFamilyAnimClip = "Base_gardener"
					
					//TIMETABLE@GARDENER@CLEAN_POOL@BASE
					//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_A
					//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_B
					//TIMETABLE@GARDENER@CLEAN_POOL@IDLE_C
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					eFamilyAnimFlag -= AF_LOOPING
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_mowing_lawn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "TIMETABLE@GARDENER@LAWNMOW@"
					tFamilyAnimClip = ""
					
					//TIMETABLE@GARDENER@LAWNMOW@BASE
					//TIMETABLE@GARDENER@LAWNMOW@IDLE_A
					//TIMETABLE@GARDENER@LAWNMOW@IDLE_B
					//TIMETABLE@GARDENER@LAWNMOW@IDLE_C
					
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_watering_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "TIMETABLE@GARDENER@FILLING_CAN"
					tFamilyAnimClip = "GAR_IG_5_Filling_Can"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_0_default
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)					RETURN TRUE				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					tFamilyAnimDict = "null"
					tFamilyAnimClip = "WORLD_HUMAN_STAND_MOBILE"	//WORLD_HUMAN_STAND_MOBILE_CALL_MALE
					
					eFamilyAnimProgress = FAP_4_scenario
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_smoking_weed
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
//					tFamilyAnimDict = "TIMETABLE@GARDENER@SMOKING_JOINT"
//					tFamilyAnimClip = ""
//					
//					eFamilyAnimProgress = FAP_3_array
//					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
//					eFamilyAnimFlag -= AF_LOOPING
					tFamilyAnimDict = "null"
					tFamilyAnimClip = "WORLD_HUMAN_SMOKING"
					
					eFamilyAnimProgress = FAP_4_scenario
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_M_MICHAEL_MIC2_washing_face
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MICHAEL
//					tFamilyAnimDict = "missfam5_washing_face"
//					tFamilyAnimClip = "michael_washing_face"
//					
//					eFamilyAnimProgress = FAP_0_default
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					tFamilyAnimDict = "TIMETABLE@DENICE@IG_1"
					tFamilyAnimClip = ""
					
					//TIMETABLE@DENICE@IG_1/base.anim
					//TIMETABLE@DENICE@IG_1/idle_A.anim
					//TIMETABLE@DENICE@IG_1/idle_B.anim
					//TIMETABLE@DENICE@IG_1/idle_C.anim
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					eFamilyAnimFlag -= AF_LOOPING
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_in_face_mask
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					tFamilyAnimDict = "TIMETABLE@DENICE@IG_2"
					tFamilyAnimClip = "base"
					
					//TIMETABLE@DENICE@IG_2/base.anim
					//TIMETABLE@DENICE@IG_2/idle_A_WhenWasYourLastTime.anim
					//TIMETABLE@DENICE@IG_2/idle_B_BoyU.anim
					//TIMETABLE@DENICE@IG_2/idle_C_ImSoStressedOut.anim
					
					eFamilyAnimProgress = FAP_2_dialogue
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS    | AF_HOLD_LAST_FRAME
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_watching_TV
		CASE FE_F_AUNT_returned_to_aunts
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					tFamilyAnimDict = "TIMETABLE@DENICE@IG_3"
					tFamilyAnimClip = ""
					
					//TIMETABLE@DENICE@IG_3/base.anim
					//TIMETABLE@DENICE@IG_3/idle_A.anim
					//TIMETABLE@DENICE@IG_3/idle_B.anim
					//TIMETABLE@DENICE@IG_3/idle_C.anim
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					tFamilyAnimDict = "TIMETABLE@DENICE@IG_4"
					tFamilyAnimClip = ""
					
					//TIMETABLE@DENICE@IG_4/base.anim
					//TIMETABLE@DENICE@IG_4/idle_A.anim
					//TIMETABLE@DENICE@IG_4/idle_B.anim
					//TIMETABLE@DENICE@IG_4/idle_C.anim
					
					eFamilyAnimProgress = FAP_3_array
//					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					/*
//					TIMETABLE@LAMAR@IG_3
//					003131_01_GC_LAS_IG_3_P3_Traffic_Jam_Lamar.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_1"
//					tFamilyAnimClip = "GC_LAS_IG_1_Whats_Up_Lamar"
//					
//					eFamilyAnimProgress = FAP_0_default
//					RETURN TRUE
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					/*
//					TIMETABLE@STRETCH@IG_3
//					003131_01_GC_LAS_IG_3_P3_Traffic_Jam_Stretch.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_1"
//					tFamilyAnimClip = "GC_LAS_IG_1_Whats_Up_Stretch"
//					
//					eFamilyAnimProgress = FAP_0_default
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					/*
//					TIMETABLE@LAMAR@IG_2
//					
//					Bomb_As_A_Mother_Fucker_Lamar.anim
//					Grill_Protien_Like_Clockwork_Lamar.anim
//					Keep_On_Walkin_Lamar.anim
//					Nothing_ToSee_Here_Lamar.anim
//					Smokin_Is_My_Specialty_Lamar.anim
//					Swine_Lamar.anim
//					You_Want_Some_of_That_Lamar.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_2"
//					tFamilyAnimClip = "Base_Lamar"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					/*
//					TIMETABLE@STRETCH@IG_2
//					
//					Bomb_As_A_Mother_Fucker_Stretch.anim
//					Grill_Protien_Like_Clockwork_Stretch.anim
//					Keep_On_Walkin_Stretch.anim
//					Nothing_ToSee_Here_Stretch.anim
//					Smokin_Is_My_Specialty_Stretch.anim
//					Swine_Stretch.anim
//					You_Want_Some_of_That_Stretch.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_2"
//					tFamilyAnimClip = "Base_Stretch"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					/*
//					TIMETABLE@LAMAR@IG_3
//					
//					003131_01_GC_LAS_IG_3_P3_Traffic_Jam_Lamar.anim
//					FAMILES_IS_FAMILES_Lamar.anim						//LAS_IG_3b
//					//LAS_IG_3_PT1_Captain_Crunch_Lamar.anim
//					LAS_IG_3_PT1_Soul_Pole_Lamar.anim					//LAS_IG_3a
//					//STEVE_JOBS_DIED_Lamar.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_3"
//					tFamilyAnimClip = "Base_Lamar"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					/*
//					TIMETABLE@STRETCH@IG_3
//					
//					003131_01_GC_LAS_IG_3_P3_Traffic_Jam_Stretch.anim
//					FAMILES_IS_FAMILES_Stretch.anim						//LAS_IG_3b
//					//LAS_IG_3_PT1_Captain_Crunch_Stretch.anim
//					LAS_IG_3_PT1_Soul_Pole_Stretch.anim					//LAS_IG_3a
//					//STEVE_JOBS_DIED_Stretch.anim
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_3"
//					tFamilyAnimClip = "Base_Stretch"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					/*
//					TIMETABLE@LAMAR@IG_4
//					
//					TIMETABLE@LAMAR@IG_4/Hey_One_Time_Lamar
//					TIMETABLE@LAMAR@IG_4/Keep_Moving_Lamar
//					TIMETABLE@LAMAR@IG_4/Looking_For_Someone_Frame_Up_Lamar
//					TIMETABLE@LAMAR@IG_4/Nothing_To_See_Here_Lamar
//					TIMETABLE@LAMAR@IG_4/Ride_On_Through_Lamar
//					
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_4"
//					tFamilyAnimClip = "Base_Lamar"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					/*
//					TIMETABLE@STRETCH@IG_3
//					
//					TIMETABLE@LAMAR@IG_4/Base_Stretch
//					TIMETABLE@LAMAR@IG_4/Hey_One_Time_Stretch
//					TIMETABLE@LAMAR@IG_4/Keep_Moving_Stretch
//					TIMETABLE@LAMAR@IG_4/Looking_For_Someone_Frame_Up_Stretch
//					TIMETABLE@LAMAR@IG_4/Nothing_To_See_Here_Stretch
//					TIMETABLE@LAMAR@IG_4/Ride_On_Through_Stretch
//					
//					*/
//					
//					tFamilyAnimDict = "TIMETABLE@LAMAR@IG_4"
//					tFamilyAnimClip = "Base_Stretch"
//					
//					eFamilyAnimProgress = FAP_3_array
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_LAMAR
					//no anim
					PRIVATE_Placeholder_FamilyMember_Anim(eFamilyEvent, tFamilyAnimDict, "", tFamilyAnimClip, "", eFamilyAnimProgress)
					RETURN FALSE
				BREAK
				CASE FM_FRANKLIN_STRETCH
					//no anim
					PRIVATE_Placeholder_FamilyMember_Anim(eFamilyEvent, tFamilyAnimDict, "", tFamilyAnimClip, "", eFamilyAnimProgress)
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RON_monitoring_police_frequency
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@HAND_RADIO_IG_1"
					tFamilyAnimClip = "IG_1_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast
		CASE FE_T0_RONEX_trying_to_pick_up_signals
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@IG_2"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					
					/*
					Ron starts off with (LAYING) anim on couch.
					Then trigger (ENTER) go from (LAYING) ->(talking) or (BASE) anims
					(talking) anims loop – beg/end in Base pose.
					*/
					
					tFamilyAnimDict = "TIMETABLE@RON@IG_3_COUCH"
					tFamilyAnimClip = "LAYING"
					
					//TIMETABLE@RON@IG_3_COUCH/BASE.anim

					//TIMETABLE@RON@IG_3_COUCH/LAYING.anim
					//TIMETABLE@RON@IG_3_COUCH/ENTER.anim
					
//					eFamilyAnimFlag	= AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					
					/*
					Ron starts off with (LAYING) anim on couch.
					Then trigger (ENTER) go from (LAYING) ->(talking) or (BASE) anims
					(talking) anims loop – beg/end in Base pose.
					*/
					
					tFamilyAnimDict = "TIMETABLE@RON@IG_3_COUCH"
					tFamilyAnimClip = "BASE"
					
					//TIMETABLE@RON@IG_3_COUCH/BASE.anim

					//TIMETABLE@RON@IG_3_COUCH/ENTER.anim
					//TIMETABLE@RON@IG_3_COUCH/LAYING.anim
					
//					eFamilyAnimFlag	= AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@IG_4_SMOKING_METH"
					tFamilyAnimClip = "BASE"
					
					eFamilyAnimFlag	= AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_USE_KINEMATIC_PHYSICS					eFamilyAnimProgress = FAP_2_dialogue
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@MOONSHINE_IG_5"
					tFamilyAnimClip = "IG_5_"
					
					eFamilyAnimFlag	|= AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_stares_through_binoculars
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@IG_6"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag	|= AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL 
					tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_CHAIR"
					tFamilyAnimClip = ""
					
					/*

					TIMETABLE@MICHAEL@ON_CHAIR/IDLE_A/
					ON_CHAIR_A.anim
					ON_CHAIR_B.anim

					TIMETABLE@MICHAEL@ON_CHAIR/IDLE_B/
					ON_CHAIR_C.anim
					ON_CHAIR_D.anim

					TIMETABLE@MICHAEL@ON_CHAIR/IDLE_C/
					ON_CHAIR_E.anim
					ON_CHAIR_F.anim

					*/
					
//					eFamilyAnimFlag = AF_NOT_INTERRUPTABLE    | AF_OVERRIDE_PHYSICS | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_sunbathing
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL 
					tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_CLUBCHAIR"
					tFamilyAnimClip = "ON_CLUBCHAIR"
					
					/*
					
					TIMETABLE@MICHAEL@ON_CLUBCHAIR/BASE/
					ON_CLUBCHAIR_base.anim
					
					
					TIMETABLE@MICHAEL@ON_CLUBCHAIR/IDLE_A/
					ON_CLUBCHAIR_A.anim
					ON_CLUBCHAIR_B.anim

					TIMETABLE@MICHAEL@ON_CLUBCHAIR/IDLE_B/
					ON_CLUBCHAIR_C.anim
					ON_CLUBCHAIR_D.anim

					TIMETABLE@MICHAEL@ON_CLUBCHAIR/IDLE_C/
					ON_CLUBCHAIR_E.anim
					ON_CLUBCHAIR_F.anim
					ON_CLUBCHAIR_G.anim
					
					*/
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS   
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_drinking_beer
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					
					//TIMETABLE@MICHAEL@ON_SOFABASE/SIT_Sofa_base.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_A.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_B.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_A/SIT_Sofa_C.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_D.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_E.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_B/SIT_Sofa_F.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_G.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_H.anim
					//TIMETABLE@MICHAEL@ON_SOFAIDLE_C/SIT_Sofa_I.anim
				
					tFamilyAnimDict = "TIMETABLE@MICHAEL@ON_SOFA"
					tFamilyAnimClip = "SIT_Sofa"
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_DISABLE_FORCED_PHYSICS_UPDATE
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					tFamilyAnimDict = "TIMETABLE@MICHAEL@TALK_PHONEbase"
					tFamilyAnimClip = "TALK_PHONE_Base"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					tFamilyAnimDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT"
					tFamilyAnimClip = "TALK_PHONE_Exit"
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET
					eFamilyAnimProgress = FAP_2_dialogue
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
			//TIMETABLE@TREVOR@IG_1/IG_1_BASE_PATRICIA.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_BASE_TREVOR.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_TheDesertIsSoBeautiful_PATRICIA.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_TheDesertIsSoBeautiful_TREVOR.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_TheDontKnowWhy_PATRICIA.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_TheDontKnowWhy_TREVOR.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_ThereAreJustSomeMoments_PATRICIA.anim
			//TIMETABLE@TREVOR@IG_1/IG_1_ThereAreJustSomeMoments_TREVOR.anim

			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "TIMETABLE@TREVOR@IG_1"
					tFamilyAnimClip = "IG_1_"
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@TREVOR@IG_1"
					tFamilyAnimClip = "IG_1_"
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			//TIMETABLE@TREVOR@TRV_IG_2/Base_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_2/Base_Trevor.anim
			//TIMETABLE@TREVOR@TRV_IG_2/Making_Me_Blush_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_2/Making_Me_Blush_Trevor.anim
			//TIMETABLE@TREVOR@TRV_IG_2/Met_You_20_Years_Ago_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_2/Met_You_20_Years_Ago_Trevor.anim
			//TIMETABLE@TREVOR@TRV_IG_2/You_Blinked_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_2/You_Blinked_Trevor.anim

			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_2"
					tFamilyAnimClip = ""
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_2"
					tFamilyAnimClip = ""
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR 
					tFamilyAnimDict = "TIMETABLE@TREVOR@SMOKING_METH"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag	= AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS | AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "TIMETABLE@TREVOR@ON_THE_TOILET"
					tFamilyAnimClip = "TrevOnLav_"
//					tFamilyAnimClip = "TrevOnLav_BaseLoop"			//(Basic short loop)
//					tFamilyAnimClip = "TrevOnLav_StruggleLoop"		//(Longer Loop of Trevor Struggling
//					
//					tFamilyAnimClip = "TrevOnLav_Midwife"			//('I need a fuckin midwife for this thing')
//					tFamilyAnimClip = "TrevOnLav_ComeHere"			//('Come Here!!  I wanna talk to you')
//					tFamilyAnimClip = "TrevOnLav_BackedUp"			//('ARrgghh,  I'm backed up,  gonna need a C-Section')
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		/*
		CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
			
			//TIMETABLE@TREVOR@TRV_IG_5BASE\BASE_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_5BASE\BASE_Trevor.anim
			//
			//TIMETABLE@TREVOR@TRV_IG_5IDLE_A\IDLE_A_Patricia.anim
			//TIMETABLE@TREVOR@TRV_IG_5IDLE_A\IDLE_A_Trevor.anim
			
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_5"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@TREVOR@TRV_IG_5"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		*/
		CASE FE_T0_TREVOR_blowing_shit_up
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "TIMETABLE@TREVOR@GRENADE_THROWING"
					tFamilyAnimClip = "GRENADE_THROWING_trev"
					
					//TIMETABLE@TREVOR@GRENADE_THROWING/GRENADE_THROWING_grenade
					//TIMETABLE@TREVOR@GRENADE_THROWING/GRENADE_THROWING_trev
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					tFamilyAnimDict = "SWITCH@TREVOR@BED"
					tFamilyAnimClip = "GC_TRV_IG_7"
					
					/*
					
					Anims submitted at 11:40 AM EST Mar/26/12:
					x:\gta5\art\anim\export_mb\SWITCH@\TREVOR@\BED\GC_TRV_IG_7_base.anim
					x:\gta5\art\anim\export_mb\SWITCH@\TREVOR@\BED\GC_TRV_IG_7_IDLE_01.anim
					x:\gta5\art\anim\export_mb\SWITCH@\TREVOR@\BED\GC_TRV_IG_7_IDLE_02.anim
					x:\gta5\art\anim\export_mb\SWITCH@\TREVOR@\BED\GC_TRV_IG_7_IDLE_03.anim
					
					fbx here:x:\gta5\art\anim\source_fbx\SWITCH@\TREVOR@\BED\TREV_PASSED_OUT.fbx
					
					*/
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON 
					tFamilyAnimDict = "TIMETABLE@RON@IG_1"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON 
					tFamilyAnimDict = "TIMETABLE@RON@IG_3"
					tFamilyAnimClip = "IG_3_BASE"
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_doing_target_practice
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					tFamilyAnimDict = "TIMETABLE@RON@IG_4"
					tFamilyAnimClip = "IG_4_BASE"
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_RONEX_conspiracies_boring_Michael
//			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Michael.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Patricia.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Michael.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Patricia.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Michael.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Patricia.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Michael.anim
//			//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Patricia.anim
//
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_WIFE
//					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_3@"
//					tFamilyAnimClip = ""
//					RETURN TRUE
//				BREAK
//				CASE FM_TREVOR_0_MICHAEL
//					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_3@"
//					tFamilyAnimClip = ""
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_1"
					tFamilyAnimClip = ""
					
					//TIMETABLE@PATRICIA@PAT_IG_1/Base.anim
					//TIMETABLE@PATRICIA@PAT_IG_1/Clean_Up.anim
					
					eFamilyAnimProgress = FAP_3_array
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME | AF_USE_MOVER_EXTRACTION
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_2@"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Michael.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_1/ALT_1_Patricia.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Michael.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/ALT_2/ALT_2_Patricia.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Michael.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/BASE/BASE_Patricia.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Michael.anim
			//TIMETABLE@PATRICIA@PAT_IG_3@/PAT_IG_3_Patricia.anim

			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_3@"
					tFamilyAnimClip = ""
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
				CASE FM_TREVOR_0_MICHAEL
					tFamilyAnimDict = "TIMETABLE@PATRICIA@PAT_IG_3@"
					tFamilyAnimClip = ""
					eFamilyAnimProgress = FAP_3_array
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MOTHER
					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_MOTHER_something_b
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MOTHER
//					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_T0_MOTHER_something_c
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MOTHER
//					PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress)
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T1_FLOYD_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "TIMETABLE@FLOYD@CLEAN_KITCHEN"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_HOLD_LAST_FRAME
					eFamilyAnimFlag -= AF_LOOPING
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_on_sofa
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "SWITCH@TREVOR@FLOYD_CRYING"
					tFamilyAnimClip = "Console_end_LOOP_FLOYD"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_pineapple
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "SWITCH@TREVOR@PINEAPPLE"
					tFamilyAnimClip = "Pineapple_EXIT_LOOP_FLOYD"
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
//					tFamilyAnimDict = "TIMETABLE@FLOYD@ENDING_CALL"
//					tFamilyAnimClip = "idle"
					
					/*
					TIMETABLE@FLOYD@CALLING
					base.anim
					FL_IG_3_IAmNotBeingJealous.anim
					FL_IG_3_NoTheCondoIsFine.anim
					FL_IG_3_ThatsJustTheTv.anim
					*/
					
					tFamilyAnimDict = "TIMETABLE@FLOYD@CALLING"
					tFamilyAnimClip = "base"
					
					eFamilyAnimProgress = FAP_2_dialogue
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "TIMETABLE@FLOYD@ENDING_CALL"
					tFamilyAnimClip = "base"
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "TIMETABLE@FLOYD@HIDING_BEHIND_COUCH"
					tFamilyAnimClip = "idle"
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_is_sleeping
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					tFamilyAnimDict = "TIMETABLE@FLOYD@CRYINGONBED_IG_5@"
					tFamilyAnimClip = ""
					
					eFamilyAnimFlag |= AF_USE_KINEMATIC_PHYSICS
					eFamilyAnimProgress = FAP_3_array
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress) RETURN TRUE BREAK
				CASE FM_TREVOR_1_WADE PRIVATE_Placeholder_FamilyMember_StandSmokeAnim(eFamilyEvent, tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimProgress) RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_ANY_wander_family_event
		CASE FE_ANY_find_family_event
			//
			
			tFamilyAnimDict = ""
			tFamilyAnimClip = ""
			
			eFamilyAnimFlag = INT_TO_ENUM(ANIMATION_FLAGS, -1)
			eFamilyAnimProgress = MAX_FAMILY_ANIM_PROGRESS
			
			RETURN FALSE
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			//
			
			tFamilyAnimDict = ""
			tFamilyAnimClip = ""
			
			eFamilyAnimFlag = INT_TO_ENUM(ANIMATION_FLAGS, -1)
			eFamilyAnimProgress = MAX_FAMILY_ANIM_PROGRESS
			
			RETURN FALSE
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in PRIVATE_Get_FamilyMember_Anim()")
			PRINTNL()
			
			SCRIPT_ASSERT("invalid eFamilyEvent in PRIVATE_Get_FamilyMember_Anim()")
			#ENDIF
			
			tFamilyAnimDict = ""
			tFamilyAnimClip = ""
			
			eFamilyAnimFlag = INT_TO_ENUM(ANIMATION_FLAGS, -1)
			eFamilyAnimProgress = MAX_FAMILY_ANIM_PROGRESS
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("invalid eFamilyMember ")
	PRINTSTRING(Get_String_From_FamilyMember(eFamilyMember))
	PRINTSTRING(" in PRIVATE_Get_FamilyMember_Anim()")
	PRINTNL()
	
	SCRIPT_ASSERT("invalid eFamilyMember PRIVATE_Get_FamilyMember_Anim()")
	#ENDIF
	
	tFamilyAnimDict = ""
	tFamilyAnimClip = ""
	
	eFamilyAnimFlag = INT_TO_ENUM(ANIMATION_FLAGS, -1)
	eFamilyAnimProgress = MAX_FAMILY_ANIM_PROGRESS
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent, IK_CONTROL_FLAGS &ikFlags)
	SWITCH eFamilyEvent
		CASE FE_M_SON_sleeping
		CASE FE_M_SON_smoking_weed_in_a_bong
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_dancing_practice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	ikFlags = AIK_DISABLE_LEG_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom
		CASE FE_M_DAUGHTER_on_phone_to_friends
		CASE FE_M_DAUGHTER_on_phone_LOCKED
		CASE FE_M7_DAUGHTER_studying_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_M_WIFE_screaming_at_son_P1
		CASE FE_M_WIFE_screaming_at_son_P2
		CASE FE_M_WIFE_screaming_at_son_P3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_WIFE		ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_WIFE_phones_man_OR_therapist
		CASE FE_M7_WIFE_phones_man_OR_therapist
		CASE FE_M_WIFE_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_F_AUNT_watching_TV	//1406394 
		CASE FE_F_AUNT_returned_to_aunts
		CASE FE_F_AUNT_in_face_mask
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT		ikFlags = AIK_DISABLE_LEG_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT		ikFlags = AIK_USE_ARM_BLOCK_TAGS	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE		ikFlags = AIK_DISABLE_LEG_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR		ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
				CASE FM_TREVOR_0_WIFE		ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
		//	SWITCH eFamilyMember
		//		CASE FM_TREVOR_0_TREVOR		ikFlags = AIK_DISABLE_LEG_IK	RETURN TRUE BREAK
		//		CASE FM_TREVOR_0_WIFE		ikFlags = AIK_DISABLE_LEG_IK	RETURN TRUE BREAK
		//	ENDSWITCH
		//BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL	ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_on_sofa
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD		ikFlags = AIK_DISABLE_HEAD_IK	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	ikFlags = AIK_NONE
	RETURN FALSE
ENDFUNC


PROC PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	FLOAT pitch = 0
	FLOAT heading = 0
	VECTOR vecOffset = <<0,0,0>>
	
	FLOAT M7_FAMILY_watching_TVpitch_son = 0.938
	FLOAT M7_FAMILY_watching_TVheading_son = -0.240
	VECTOR M7_FAMILY_watching_TVvecOffset_son = <<-0.090,-0.450,-0.069>>

	FLOAT M7_FAMILY_watching_TVpitch_dau = 0.998
	FLOAT M7_FAMILY_watching_TVheading_dau = -1.000
	VECTOR M7_FAMILY_watching_TVvecOffset_dau = <<-0.330,-0.540,-0.090>>

	FLOAT M7_FAMILY_watching_TVpitch_wife = 0.818
	FLOAT M7_FAMILY_watching_TVheading_wife = -0.030
	VECTOR M7_FAMILY_watching_TVvecOffset_wife = <<-0.180, -0.420, -0.229>>


	SWITCH eFamilyEvent
		CASE FE_M7_FAMILY_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					pitch = M7_FAMILY_watching_TVpitch_son
					heading = M7_FAMILY_watching_TVheading_son
					vecOffset = M7_FAMILY_watching_TVvecOffset_son
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					pitch = M7_FAMILY_watching_TVpitch_dau
					heading = M7_FAMILY_watching_TVheading_dau
					vecOffset = M7_FAMILY_watching_TVvecOffset_dau
				BREAK
				CASE FM_MICHAEL_WIFE
					pitch = M7_FAMILY_watching_TVpitch_wife
					heading = M7_FAMILY_watching_TVheading_wife
					vecOffset = M7_FAMILY_watching_TVvecOffset_wife
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					pitch = 1.118
					heading = 0.3900
					vecOffset = <<0.425, -0.2800, 0.2000>>
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					pitch = CONST_PI * 0.3464		//1.0883
					heading = -0.1500
					vecOffset = <<-0.0600, -0.6000, 0.2000>>
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_SON_sleeping
			pitch = CONST_PI * 0.5
			heading = 0
			vecOffset = <<0.0, -0.0, 0.25>>
		BREAK
		CASE FE_M7_SON_jumping_jacks
			pitch = 0.0
			heading = 0.1
			vecOffset = <<0.1,0.1,0>>
		BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing
		CASE FE_M7_DAUGHTER_sunbathing
			pitch = CONST_PI * -0.4163	//-1.3080
			heading = 0
			vecOffset = <<0.0, -0.6, 0.7>>
		BREAK
		CASE FE_M_DAUGHTER_sleeping
		CASE FE_M_DAUGHTER_couchsleep
			pitch = CONST_PI * 0.5
			heading = CONST_PI * -0.5
			vecOffset = <<0.390, 0.0, -0.750>>
		BREAK
		
		CASE FE_M_DAUGHTER_on_phone_to_friends
		CASE FE_M_DAUGHTER_on_phone_LOCKED
		CASE FE_M7_DAUGHTER_studying_on_phone
			pitch = CONST_PI * 0.5		//1.5708
			heading = 0.0
			vecOffset = <<0.0, 0.15, 0.0>>
		BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			pitch = CONST_PI * 0.4332		//1.3608
			heading = 0
			vecOffset = <<-0.0, -1.0, -0.3>>
		BREAK
		
		CASE FE_M_WIFE_passed_out_BED
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			pitch = CONST_PI * 0.4132		//1.2981
			heading = 0
			vecOffset = <<0,0,0>>
		BREAK
		
//		CASE FE_M_MEXMAID_watching_TV
//			pitch = CONST_PI * 0.4037		//1.2681
//			heading = -0.0300
//			vecOffset = <<-0.120,-0.330,-0.409>>
//		BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises
			pitch = CONST_PI * 0.5
			heading = -0.0
			vecOffset = <<0.0, -0.0, -0.6>>
		BREAK
		CASE FE_F_AUNT_in_face_mask
//			pitch = CONST_PI * 0.2882		//0.9054
//			heading = -0.6800
//			vecOffset = <<0.0900, -0.3900, -0.3000 - 31.2078 + 31.5729>>
			
			pitch = 0
			heading = 0
			vecOffset = <<0,0,0>>
			
		BREAK
		
		CASE FE_T0_TREVOR_smoking_crystal
			pitch = 0.0
			heading = -0.0
			vecOffset = <<-0.0,-0.4, -0.0>>
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			pitch = 0.0
			heading = -0.0
			vecOffset = <<-0.25,-0.5, -0.0>>
		BREAK
		#ENDIF
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame()")
			PRINTNL()
			
			SCRIPT_ASSERT("invalid eFamilyEvent PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame()")
			#ENDIF
			
			pitch = 0
			heading = 0
			vecOffset = <<0,0,0>>
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_O)
		WIDGET_GROUP_ID wBoundsWidget = START_WIDGET_GROUP("ForceFamilyMemberBoundsOrientationThisFrame")
			ADD_WIDGET_FLOAT_SLIDER("pitch", pitch, -CONST_PI, CONST_PI, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("heading", heading, -CONST_PI, CONST_PI, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("vecOffset", vecOffset, -10, 10, 0.01)
		STOP_WIDGET_GROUP()
		WAIT(0)
		SET_PED_KEEP_TASK(PedIndex, TRUE)
		
		WHILE NOT IS_PED_INJURED(PedIndex)
			SET_PED_BOUNDS_ORIENTATION(PedIndex, pitch, heading, vecOffset) 
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_O)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("			pitch = CONST_PI * ")SAVE_FLOAT_TO_DEBUG_FILE(pitch / CONST_PI)SAVE_STRING_TO_DEBUG_FILE("		//")SAVE_FLOAT_TO_DEBUG_FILE(pitch)SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("			heading = ")SAVE_FLOAT_TO_DEBUG_FILE(heading)SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("			vecOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vecOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				DELETE_WIDGET_GROUP(wBoundsWidget)
				
				EXIT
			ENDIF
			
			WAIT(0)
		ENDWHILE
	ENDIF
	#ENDIF
	
	SET_PED_BOUNDS_ORIENTATION(PedIndex, pitch, heading, vecOffset) 
ENDPROC
