
USING "selector_public.sch"


#if USE_CLF_DLC
	USING "familySchedule_privateCLF.sch"	
#endif
#if USE_NRM_DLC
	USING "familySchedule_privateNRM.sch"	
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	USING "familySchedule_private.sch"	
#endif
#endif
USING "family_private.sch"


USING "familytask_private.sch"
USING "friendutil_private.sch"

#IF IS_DEBUG_BUILD
	USING "family_debug.sch"
#ENDIF

///private header for family scene scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY SCENE PRIVATE CLEANUP FUNCTIONS
// *******************************************************************************************

#IF IS_DEBUG_BUILD
FUNC STRING cheap_GET_MODEL_NAME_FOR_DEBUG(MODEL_NAMES ModelHashKey)
	
	IF ModelHashKey = DUMMY_MODEL_FOR_SCRIPT
		RETURN "dummy_model_for_script"
	ENDIF
	
//	IF (ModelHashKey <> V_RET_GC_KNIFE01)
		RETURN GET_MODEL_NAME_FOR_DEBUG(ModelHashKey)
//	ELSE
//		RETURN "V_RET_GC_KNIFE01"
//	ENDIF
ENDFUNC
#ENDIF

PROC SetFamilyPedAsNoLongerNeeded(PED_INDEX &pedIndex)
	IF NOT IS_PED_INJURED(pedIndex)
		IF (GET_SCRIPT_TASK_STATUS(pedIndex, SCRIPT_TASK_PLAY_ANIM) <> WAITING_TO_START_TASK)
			SET_PED_KEEP_TASK(pedIndex, TRUE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedIndex)
		IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(pedIndex)		//asserts otherwise, doesnt it
			SET_PED_AS_NO_LONGER_NEEDED(pedIndex)
		ENDIF
	ELSE
		SET_PED_AS_NO_LONGER_NEEDED(pedIndex)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PrintDeleteFamilyEntity(ENTITY_INDEX entityIndex, BOOL b, STRING s1, STRING s2, INT i2)
	IF NOT DOES_ENTITY_EXIST(entityIndex)
		IF NOT b
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> DONT delete family entity \"", "null", "\" [", s1, "]    //", s2, " ", i2)
		ELSE
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> delete family entity \"", "null", "\" [", s1, "]    //", s2, " ", i2)
		ENDIF
	ELSE
	
		IF IS_ENTITY_A_PED(entityIndex)
			IF NOT b
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> DONT delete family ", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ELSE
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> delete family ", "ped", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ENDIF
		ELIF IS_ENTITY_A_VEHICLE(entityIndex)
			IF NOT b
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> DONT delete family ", "vehicle", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ELSE
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> delete family ", "vehicle", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ENDIF
		ELIF IS_ENTITY_AN_OBJECT(entityIndex)
			IF NOT b
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> DONT delete family ", "object", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ELSE
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> delete family ", "object", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ENDIF
		ELSE
			IF NOT b
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> DONT delete family ", "entity", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ELSE
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> delete family ", "entity", " \"", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), "\" [", s1, "]    //", s2, " ", i2)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF ARE_STRINGS_EQUAL(s2, "familyPed")
		IF (i2 = 2)
			CPRINTLN(DEBUG_FAMILY, "")
			CPRINTLN(DEBUG_FAMILY, "g_eDebugSelectedMember = ", Get_String_From_FamilyMember(g_eDebugSelectedMember))
			
			CPRINTLN(DEBUG_FAMILY, "g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(g_eDebugSelectedMember), "] = ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[g_eDebugSelectedMember]))
			
			CPRINTLN(DEBUG_FAMILY, "g_bUpdatedFamilyEvents = ", g_bUpdatedFamilyEvents)
			CPRINTLN(DEBUG_FAMILY, "g_bDebugForceCreateFamily = ", g_bDebugForceCreateFamily)
			CPRINTLN(DEBUG_FAMILY, "")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

FUNC BOOL ShouldDeleteFamilyEntity(ENTITY_INDEX entityIndex, STRING s2, INT i2)
	
	IF NOT DOES_ENTITY_EXIST(entityIndex)
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, FALSE, "not does_entity_exist", s2, i2)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_REPEAT_PLAY)
	AND GET_ENTITY_MODEL(entityIndex) != prop_bong_01
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, TRUE, "force_cleanup_flag_repeat_play", s2, i2)
		#ENDIF
		
		IF NOT IS_ENTITY_DEAD(entityIndex)
		AND NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(entityIndex)
			CPRINTLN(DEBUG_FAMILY, "SET_ENTITY_AS_MISSION_ENTITY(", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)), ")")
			SET_ENTITY_AS_MISSION_ENTITY(entityIndex)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(entityIndex)
		IF IS_ENTITY_A_VEHICLE(entityIndex)
		AND IS_PLAYER_PLAYING(PLAYER_ID())
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityIndex))
			
				#IF IS_DEBUG_BUILD
				PrintDeleteFamilyEntity(entityIndex, FALSE, "player is sat in vehicle", s2, i2)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugForceCreateFamily
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, TRUE, "g_bDebugForceCreateFamily", s2, i2)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_ENTITY_DEAD(entityIndex)
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(entityIndex)
			
			#IF IS_DEBUG_BUILD
			PrintDeleteFamilyEntity(entityIndex, FALSE, "not does_entity_belong_to_this_script", s2, i2)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		//1641866
		IF GET_ENTITY_MODEL(entityIndex) = prop_bong_01
			
			#IF IS_DEBUG_BUILD
			PrintDeleteFamilyEntity(entityIndex, FALSE, "GET_ENTITY_MODEL(entityIndex) = prop_bong_01", s2, i2)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_bSavebedCompleteSuccessfully
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, TRUE, "g_bSavebedCompleteSuccessfully", s2, i2)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, TRUE, "is_screen_faded_out", s2, i2)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF (IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() OR GET_MISSION_FLAG())
		
		IF IS_ENTITY_ON_SCREEN(entityIndex)
			
			#IF IS_DEBUG_BUILD
			PrintDeleteFamilyEntity(entityIndex, FALSE, "IS_ENTITY_ON_SCREEN", s2, i2)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PrintDeleteFamilyEntity(entityIndex, TRUE, "IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE", s2, i2)
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PrintDeleteFamilyEntity(entityIndex, FALSE, "fallen through???", s2, i2)
	#ENDIF
	
	s2 = s2
	i2 = i2
	RETURN FALSE
ENDFUNC

PROC PRIVATE_GenericFamilySceneCleanup(INT &iFamilySynchScene[],
		VEHICLE_INDEX &familyVeh[], PED_INDEX &familyPed[],
		OBJECT_INDEX &familyProp[], MODEL_NAMES &familyPropModel[],
		PED_INDEX &familyExtraPed[], MODEL_NAMES &familyExtraPedModel[],
		INT &iSfxStage[], INT &iSfxID[], TEXT_LABEL_63 &RequestedBankName[])
	INT iCleanup
	
	REPEAT COUNT_OF(iFamilySynchScene) iCleanup
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamilySynchScene[iCleanup])
			DETACH_SYNCHRONIZED_SCENE(iFamilySynchScene[iCleanup])
		ENDIF
		iFamilySynchScene[iCleanup] = -1
	ENDREPEAT
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	REPEAT COUNT_OF(familyVeh) iCleanup
		IF ShouldDeleteFamilyEntity(familyVeh[iCleanup], "familyVeh", iCleanup)
			DELETE_VEHICLE(familyVeh[iCleanup])
		ENDIF
		IF DOES_ENTITY_EXIST(familyVeh[iCleanup])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(familyVeh[iCleanup])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(familyVeh[iCleanup])
		ENDIF
	ENDREPEAT
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	REPEAT COUNT_OF(familyPed) iCleanup
		IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, familyPed[iCleanup])
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, familyPed[iCleanup])
		ENDIF
		
		IF (familyPed[iCleanup] <> PLAYER_PED_ID())
			IF ShouldDeleteFamilyEntity(familyPed[iCleanup], "familyPed", iCleanup)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "# DELETE_PED(familyPed[", GET_THIS_SCRIPT_NAME(), " ", iCleanup, "])")
				#ENDIF
				
				DELETE_PED(familyPed[iCleanup])
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "# dont_delete_ped(familyPed[", GET_THIS_SCRIPT_NAME(), " ", iCleanup, "])")
				#ENDIF
			ENDIF
			SetFamilyPedAsNoLongerNeeded(familyPed[iCleanup])
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "# dont_delete_ped(familyPed[", GET_THIS_SCRIPT_NAME(), " ", iCleanup, "]) - player ped!!!")
			#ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(familyProp[iCleanup])
			IF IS_ENTITY_ATTACHED(familyProp[iCleanup])
				DETACH_ENTITY(familyProp[iCleanup])
			ENDIF
		ENDIF
		IF ShouldDeleteFamilyEntity(familyProp[iCleanup], "familyProp", iCleanup)
			DELETE_OBJECT(familyProp[iCleanup])
		ENDIF
		IF DOES_ENTITY_EXIST(familyProp[iCleanup])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(familyProp[iCleanup])
			SET_OBJECT_AS_NO_LONGER_NEEDED(familyProp[iCleanup])
		ENDIF
		
		IF ShouldDeleteFamilyEntity(familyExtraPed[iCleanup], "familyExtraPed", iCleanup)
			DELETE_PED(familyExtraPed[iCleanup])
		ENDIF
		IF DOES_ENTITY_EXIST(familyExtraPed[iCleanup])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(familyExtraPed[iCleanup])
			SET_PED_AS_NO_LONGER_NEEDED(familyExtraPed[iCleanup])
		ENDIF
		
		IF (familyPropModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "SET_MODEL_AS_NO_LONGER_NEEDED(familyPropModel[iCleanup]", cheap_GET_MODEL_NAME_FOR_DEBUG(familyPropModel[iCleanup]), ")")
			#ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(familyPropModel[iCleanup])
		ENDIF
		IF (familyExtraPedModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "SET_MODEL_AS_NO_LONGER_NEEDED(familyExtraPedModel[iCleanup]", cheap_GET_MODEL_NAME_FOR_DEBUG(familyExtraPedModel[iCleanup]), ")")
			#ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(familyExtraPedModel[iCleanup])
		ENDIF
		
		PRIVATE_Cleanup_Family_Stream_And_Sfx(iSfxStage[iCleanup], iSfxID[iCleanup], RequestedBankName[iCleanup])
	ENDREPEAT
	
	//- remove anims from the memory -//
	TEXT_LABEL_63 tFamilyAnimDict
	enumFamilyEvents eFamilyEvent
	REPEAT MAX_FAMILY_EVENTS eFamilyEvent
		tFamilyAnimDict = ""
		IF PRIVATE_Preload_FamilyMember_Anim(eFamilyEvent, tFamilyAnimDict)
			REMOVE_ANIM_DICT(tFamilyAnimDict)
		ENDIF
	ENDREPEAT
	
	//- remove vehicle recordings from the memory -//
	PRIVATE_Cleanup_Family_Doors()
	
	//- clear sequences -//
	//- destroy all scripted cams -//
	PRIVATE_Cleanup_Tv_Globals()
	
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	CANCEL_COMMUNICATION(TEXT_FAM_GRIEF_MICHAEL)
	CANCEL_COMMUNICATION(TEXT_FAM_GRIEF_FRANKLIN)
	CANCEL_COMMUNICATION(TEXT_FAM_GRIEF_TREVOR)
	
	CANCEL_COMMUNICATION(CALL_FAM_M_GRIEF_GENERAL)
	CANCEL_COMMUNICATION(CALL_FAM_M_GRIEF_FIRING_ROCKETS)
	CANCEL_COMMUNICATION(CALL_FAM_M_GRIEF_STOLE_CAR)
	CANCEL_COMMUNICATION(CALL_FAM_M_GRIEF_SHOOTING_HOUSE)
	
	CANCEL_COMMUNICATION(CALL_FAM_F_GRIEF_GENERAL)
	CANCEL_COMMUNICATION(CALL_FAM_F_GRIEF_FIRING_ROCKETS)
	CANCEL_COMMUNICATION(CALL_FAM_F_GRIEF_STOLE_CAR_c)
	CANCEL_COMMUNICATION(CALL_FAM_F_GRIEF_STOLE_CAR_b)
	CANCEL_COMMUNICATION(CALL_FAM_F_GRIEF_SHOOTING_HOUSE)
	
	CANCEL_COMMUNICATION(CALL_FAM_T_GRIEF_GENERAL)
	CANCEL_COMMUNICATION(CALL_FAM_T_GRIEF_FIRING_ROCKETS)
	CANCEL_COMMUNICATION(CALL_FAM_T_GRIEF_STOLE_CAR)
	CANCEL_COMMUNICATION(CALL_FAM_T_GRIEF_SHOOTING_HOUSE)
	
	g_ePreviousFamilyGriefing = NUM_MONITER_PLAYER_GRIEFING
	g_iFamilyGriefingTimer = -1
	
	//- kill mission text link -//
	
	
ENDPROC

PROC SetFamilyMemberModelAsNoLongerNeeded(enumFamilyMember eFamilyMember)
	
	// ped
	MODEL_NAMES eMemberModel = DUMMY_MODEL_FOR_SCRIPT
	enumCharacterList eCharID = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eMemberModel)
	IF (eCharID <> NO_CHARACTER)
		IF IS_PLAYER_PED_PLAYABLE(eCharID)
			eMemberModel = GET_PLAYER_PED_MODEL(eCharID)
		ELSE
			eMemberModel = GET_NPC_PED_MODEL(eCharID)
		ENDIF
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(eMemberModel)
	
ENDPROC



PROC PRIVATE_SetDefaultFamilyMemberAttributes(PED_INDEX &pedIndex, REL_GROUP_HASH eRelGroup)
	CPRINTLN(DEBUG_FAMILY, "SetDefaultFamilyMemberAttributes(", Get_String_From_FamilyMember(GET_enumFamilyMember_from_ped(pedIndex)), ")")
	
	SET_PED_CAN_BE_TARGETTED(pedIndex, FALSE)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_AllowMedicsToAttend, TRUE)
//	SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(pedIndex, FALSE, RELGROUPHASH_PLAYER)
	
	SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)			//1445300
	
	IF (eRelGroup <> RELGROUPHASH_NO_RELATIONSHIP)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedIndex, eRelGroup)	//RELGROUPHASH_PLAYER)
	ENDIF
	
ENDPROC

// *******************************************************************************************
//	FAMILY SCENE PRIVATE CHECK FUNCTIONS
// *******************************************************************************************

FUNC BOOL IsPlayerVehicleInterfering(VECTOR vSceneMemberCoord, FLOAT fInterferingRadius)
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugForceCreateFamily
		RETURN FALSE
	ENDIF
	#ENDIF
	
	VEHICLE_INDEX playersLastVeh = GET_PLAYERS_LAST_VEHICLE()
	IF NOT IS_ENTITY_DEAD(playersLastVeh)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = ("interfering[")
		#ENDIF
		
		IF IS_ENTITY_ON_SCREEN(playersLastVeh)
			#IF IS_DEBUG_BUILD
			str += ("IS_ENTITY_ON_SCREEN]")
			DrawFamilyLiteralString(str, 15)
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		VECTOR vPlayersLastVehCoord = GET_ENTITY_COORDS(playersLastVeh)
		FLOAT fDist2FromSceneToLastVeh = VDIST2(vSceneMemberCoord, vPlayersLastVehCoord)
		
		IF fDist2FromSceneToLastVeh < (fInterferingRadius*fInterferingRadius)
			
			INT NodeNumber = GET_RANDOM_INT_IN_RANGE(1, 10)
			VECTOR VecReturnNearestNodeCoors = <<0,0,0>>
			FLOAT ReturnHeading = 0
			INT ReturnNumLanes = 0
			NODE_FLAGS nodeFlags = NF_INCLUDE_SWITCHED_OFF_NODES
			IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vSceneMemberCoord, NodeNumber,
					VecReturnNearestNodeCoors, ReturnHeading,
					ReturnNumLanes, nodeFlags)
				
				VECTOR vInterferingBounds = <<2.0,2.0,2.0>>		//<<fInterferingRadius,fInterferingRadius,fInterferingRadius>>
				
				IF IS_SPHERE_VISIBLE(VecReturnNearestNodeCoors, VMAG(vInterferingBounds))
					#IF IS_DEBUG_BUILD
					str += ("is_sphere_visible]")
					DrawFamilyLiteralString(str, 15)
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
				IF IS_AREA_OCCUPIED(VecReturnNearestNodeCoors-vInterferingBounds,
						VecReturnNearestNodeCoors+vInterferingBounds,
						FALSE, TRUE, TRUE, FALSE, FALSE)
					#IF IS_DEBUG_BUILD
					str += ("IS_AREA_OCCUPIED]")
					DrawFamilyLiteralString(str, 15)
					#ENDIF
					
					RETURN TRUE
				ENDIF
				
				SET_ENTITY_COORDS(playersLastVeh, VecReturnNearestNodeCoors)
				SET_ENTITY_HEADING(playersLastVeh, ReturnHeading)
				
				#IF IS_DEBUG_BUILD
				str += ("SET_ENTITY_COORDS]")
				DrawFamilyLiteralString(str, 15)
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str += ("GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING]")
			DrawFamilyLiteralString(str, 15)
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	FAMILY SCENE PRIVATE SETUP FUNCTIONS
// *******************************************************************************************
FUNC BOOL DOES_VEHICLE_MATCH_VEH_DATA(VEHICLE_INDEX vehicleIndex, PED_VEH_DATA_STRUCT sVehData)
	IF NOT DOES_ENTITY_EXIST(vehicleIndex)
		RETURN FALSE
	ENDIF
	
	/*
	MODEL_NAMES model_a, model_t
	FLOAT fDirtLevel
	INT iColourCombo
	INT iColour1, iColour2
	INT iColourExtra1, iColourExtra2
	BOOL bColourCombo, bColourExtra	//	going to remove these soon
	BOOL bExtraOn[9]
	INT iRadioIndex
	INT iPlateBack
	TEXT_LABEL_15 tlNumberPlate
	INT iModSlots[MAX_VEHICLE_MOD_SLOT_INDEXES]// array pos = mod index
	INT iTyreR, iTyreG, iTyreB
	INT iWindowTintColour
	BOOL bTyresCanBurst
	*/
	
	IF GET_ENTITY_MODEL(vehicleIndex) <> sVehData.model
		RETURN FALSE
	ENDIF
	
	/*
	// CarApp Data
	BOOL bDisplayCarAppFeedUpdate
	BOOL bFailedToUpdateCarApp
	BOOL bUpdateCarApp
	BOOL bDataSentToApp
	BOOL bDataReceivedFromApp
	INT iModCountEngine
	INT iModCountBrakes
	INT iModCountExhaust
	INT iModCountWheels
	INT iModCountHorn
	INT iModCountArmour
	INT iModCountSuspension
	INT iModColoursThatCanBeSet
	INT iHornHash[5]
	MOD_KIT_TYPE eModKitType
	
	VEHICLE_CREATE_TYPE_ENUM eType
	*/
	
	RETURN TRUE
ENDFUNC
FUNC BOOL DOES_NPC_VEHICLE_EXIT_NEARBY(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed,
		VECTOR vCoords, VEHICLE_CREATE_TYPE_ENUM eTypePreference)
	
	PED_VEH_DATA_STRUCT sVehData
	GET_NPC_VEH_DATA(ePed, sVehData, eTypePreference)
	
	// //
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX playerInVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF DOES_VEHICLE_MATCH_VEH_DATA(playerInVeh, sVehData)
			ReturnVeh = playerInVeh
			RETURN TRUE
		ENDIF
	ENDIF
	
	// //
	VEHICLE_INDEX playerLastVeh = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_VEHICLE_MATCH_VEH_DATA(playerLastVeh, sVehData)
		ReturnVeh = playerLastVeh
		RETURN TRUE
	ENDIF
	
	// //
	VEHICLE_INDEX randomSphereVeh = GET_RANDOM_VEHICLE_IN_SPHERE(vCoords, 20.0, sVehData.model, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF DOES_VEHICLE_MATCH_VEH_DATA(randomSphereVeh, sVehData)
		ReturnVeh = randomSphereVeh
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CreateThisFamilyVehicle(VEHICLE_INDEX &vehicleIndex,
		enumFamilyMember eFamilyMember,
		VECTOR vFamilySceneCoord, FLOAT fFamilySceneHead, BOOL bParkedWithoutEvent
		
		#IF IS_DEBUG_BUILD
		, WIDGET_GROUP_ID family_scene_widget, BOOL &bMoveVeh
		#ENDIF
		
		)
	
	IF NOT DOES_ENTITY_EXIST(vehicleIndex)
		
		enumFamilyEvents eFamilyEvent = g_eCurrentFamilyEvent[eFamilyMember]
		VECTOR vVehOffset
		FLOAT fVehHead
		enumCharacterList eVehPed
		VEHICLE_CREATE_TYPE_ENUM eVehCreateType
		
		IF PRIVATE_Get_FamilyMember_Vehicle_Offset(eFamilyMember, eFamilyEvent, vVehOffset, fVehHead, eVehPed, eVehCreateType, bParkedWithoutEvent)
			
			IF NOT g_bMagDemoActive
			
				CONST_FLOAT fCreateChrck_a	15.0
				CONST_FLOAT fCreateChrck_b	35.0
				
				FLOAT fPlayerSquareDistFromVehCreation = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vFamilySceneCoord+vVehOffset)
				
				IF fPlayerSquareDistFromVehCreation < (fCreateChrck_a*fCreateChrck_a)
				AND NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] =  ", GET_PLAYER_PED_STRING(eVehPed), " VDIST < ", ROUND(fCreateChrck_a), " [", SQRT(fPlayerSquareDistFromVehCreation), "]")
					
					DrawDebugFamilySphere(vFamilySceneCoord+vVehOffset, fCreateChrck_a, HUD_COLOUR_GREYLIGHT, 0.25)
					#ENDIF
					
					EXIT
				ENDIF
				
				PED_VEH_DATA_STRUCT sVehData
				IF (eVehPed <> CHAR_MULTIPLAYER)
					GET_NPC_VEH_DATA(eVehPed, sVehData, VEHICLE_TYPE_CAR)
				ELSE
//					SWITCH eFamilyEvent
//						CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//							sVehData.model = POLICE
//						BREAK
//					ENDSWITCH
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX PlayerVehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					IF GET_ENTITY_MODEL(PlayerVehIndex) = sVehData.model
						vehicleIndex= PlayerVehIndex
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleIndex, TRUE)
						
						#IF IS_DEBUG_BUILD
						SET_CURRENT_WIDGET_GROUP(family_scene_widget)
							ADD_WIDGET_BOOL("bMoveVeh", bMoveVeh)
						CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
						#ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = players current veh!!!")
						#ENDIF
						
						EXIT
					ENDIF
				ENDIF
				
				IF fPlayerSquareDistFromVehCreation < (fCreateChrck_b*fCreateChrck_b)
					IF IS_SPHERE_VISIBLE(vFamilySceneCoord+vVehOffset, 10.0)
						IF WOULD_ENTITY_BE_OCCLUDED(sVehData.model, vFamilySceneCoord+vVehOffset, FALSE)
							IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] =  ", GET_PLAYER_PED_STRING(eVehPed), " VDIST < ", ROUND(fCreateChrck_b), " [", SQRT(fPlayerSquareDistFromVehCreation), "]\n")
								DrawDebugFamilySphere(vFamilySceneCoord+vVehOffset, fCreateChrck_b, HUD_COLOUR_GREYDARK, 0.1)
								#ENDIF
								
								EXIT
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_SPHERE_VISIBLE(vFamilySceneCoord+vVehOffset, 4.0)
						IF WOULD_ENTITY_BE_OCCLUDED(sVehData.model, vFamilySceneCoord+vVehOffset, FALSE)IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] =  ", GET_PLAYER_PED_STRING(eVehPed), " VDIST >= ", ROUND(fCreateChrck_b), " [", SQRT(fPlayerSquareDistFromVehCreation), "]\n")
								DrawDebugFamilySphere(vFamilySceneCoord+vVehOffset, 4, HUD_COLOUR_GREYDARK, 0.25)
								#ENDIF
								
								EXIT
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// 
				IF IsPlayerVehicleInterfering(vFamilySceneCoord+vVehOffset, 6.0)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = IsPlayerVehicleInterfering()")
					#ENDIF
					
					EXIT
				ENDIF
				
				IF IS_AREA_OCCUPIED(vFamilySceneCoord+vVehOffset-<<4,4,4>>,
						vFamilySceneCoord+vVehOffset+<<4,4,4>>,
						FALSE, TRUE, FALSE, FALSE, FALSE)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = IS_AREA_OCCUPIED()")
					#ENDIF
					
					EXIT
				ENDIF
			
			ENDIF
			
			IF (eVehPed <> NO_CHARACTER)
				IF NOT DOES_NPC_VEHICLE_EXIT_NEARBY(vehicleIndex, eVehPed,
						vFamilySceneCoord+vVehOffset, eVehCreateType)
					IF CREATE_NPC_VEHICLE(vehicleIndex, eVehPed, 
							vFamilySceneCoord+vVehOffset,
							WRAP(fFamilySceneHead+fVehHead, 0, 360), TRUE, eVehCreateType)
						
						REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex), ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleIndex, TRUE)
						
						IF (eFamilyMember = FM_MICHAEL_WIFE)
							LOWER_CONVERTIBLE_ROOF(vehicleIndex, TRUE)
						ENDIF
						
						IF g_bMagDemoActive
							SET_VEHICLE_DOORS_LOCKED(vehicleIndex, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						ENDIF
						
						#IF IS_DEBUG_BUILD
						SET_CURRENT_WIDGET_GROUP(family_scene_widget)
							ADD_WIDGET_BOOL("bMoveVeh", bMoveVeh)
						CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
						#ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = CREATE_NPC_VEHICLE()")
						#ENDIF
						
					ENDIF
				ELSE
					REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex), ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleIndex, TRUE)
					
					IF (eFamilyMember = FM_MICHAEL_WIFE)
						LOWER_CONVERTIBLE_ROOF(vehicleIndex, TRUE)
					ENDIF
					
					IF g_bMagDemoActive
						SET_VEHICLE_DOORS_LOCKED(vehicleIndex, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					SET_CURRENT_WIDGET_GROUP(family_scene_widget)
						ADD_WIDGET_BOOL("bMoveVeh", bMoveVeh)
					CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
					#ENDIF
				ENDIF
			ELSE
				MODEL_NAMES vehModel = DUMMY_MODEL_FOR_SCRIPT
//				SWITCH eFamilyEvent
//					CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//						vehModel = POLICE
//					BREAK
//				ENDSWITCH
				
				IF (vehModel <> DUMMY_MODEL_FOR_SCRIPT)
					REQUEST_MODEL(vehModel)
			        IF HAS_MODEL_LOADED(vehModel)
						
						// 
						IF IS_SPHERE_VISIBLE(vFamilySceneCoord+vVehOffset, 2.5)
							EXIT
						ENDIF
						
						// 
						IF IsPlayerVehicleInterfering(vFamilySceneCoord+vVehOffset, 6.0)
					
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = IsPlayerVehicleInterfering()")
							#ENDIF
							
							EXIT
						ENDIF
						
						
						IF DOES_ENTITY_EXIST(vehicleIndex)
			                DELETE_VEHICLE(vehicleIndex)
			            ENDIF
			            
			            vehicleIndex = CREATE_VEHICLE(vehModel,
								vFamilySceneCoord+vVehOffset,
								WRAP(fFamilySceneHead+fVehHead, 0, 360), FALSE, FALSE)
			            SET_VEHICLE_ON_GROUND_PROPERLY(vehicleIndex)
			            
	//		            // Colours
	//		            IF sVehData.bColourCombo
	//		                SET_VEHICLE_COLOUR_COMBINATION(vehicleIndex, sVehData.iColourCombo)
	//		            ELSE
	//		                SET_VEHICLE_COLOURS(vehicleIndex, sVehData.iColour1, sVehData.iColour2)
	//		            ENDIF
	//		            IF sVehData.bColourExtra
	//		                SET_VEHICLE_EXTRA_COLOURS(vehicleIndex, sVehData.iColourExtra1, sVehData.iColourExtra2)
	//		            ENDIF
			            
	//		            // Dirt
	//		            SET_VEHICLE_DIRT_LEVEL(vehicleIndex, sVehData.fDirtLevel)
			            
	//		            // Extras
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 1, (NOT sVehData.bExtraOn[1]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 2, (NOT sVehData.bExtraOn[2]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 3, (NOT sVehData.bExtraOn[3]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 4, (NOT sVehData.bExtraOn[4]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 5, (NOT sVehData.bExtraOn[5]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 6, (NOT sVehData.bExtraOn[6]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 7, (NOT sVehData.bExtraOn[7]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 8, (NOT sVehData.bExtraOn[8]))
	//		            SET_VEHICLE_EXTRA(vehicleIndex, 9, (NOT sVehData.bExtraOn[9]))
			        	
						REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehicleIndex), ENUM_TO_INT(VRF_REQUEST_ALL_ANIMS))
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleIndex, TRUE)
						
			            IF (vehModel = POLICE)
							SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(vehicleIndex, FALSE)
							SET_VEHICLE_NEEDS_TO_BE_HOTWIRED(vehicleIndex, TRUE)
							
							SET_VEHICLE_DISABLE_TOWING(vehicleIndex, TRUE)
						ENDIF
						
						SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
			            
//						SET_VEHICLE_DOORS_LOCKED(vehicleIndex, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
						
						
						#IF IS_DEBUG_BUILD
						SET_CURRENT_WIDGET_GROUP(family_scene_widget)
							ADD_WIDGET_BOOL("bMoveVeh", bMoveVeh)
						CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
						#ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "CreateThisFamilyVehicle[", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), "] = CREATE_NPC_VEHICLE()")
						#ENDIF
						
			        ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		IF bMoveVeh
			enumFamilyEvents eFamilyEvent = g_eCurrentFamilyEvent[eFamilyMember]
			VECTOR vVehOffset
			FLOAT fVehHead
			enumCharacterList eVehPed
			VEHICLE_CREATE_TYPE_ENUM eVehCreateType
			
			IF PRIVATE_Get_FamilyMember_Vehicle_Offset(eFamilyMember, eFamilyEvent, vVehOffset, fVehHead, eVehPed, eVehCreateType, bParkedWithoutEvent)

			
//				VECTOR vVehOffset = GET_ENTITY_COORDS(vehicleIndex) - vFamilySceneCoord
//				FLOAT fVehHead = GET_ENTITY_HEADING(vehicleIndex) - fFamilySceneHead
							
				PED_VEH_DATA_STRUCT sTempData
				GET_NPC_VEH_DATA(eVehPed, sTempData)


				START_WIDGET_GROUP("CreateThisFamilyVehicle()")
					ADD_WIDGET_BOOL("bMoveVeh", bMoveVeh)
					ADD_WIDGET_VECTOR_SLIDER("vVehOffset", vVehOffset, -100, 100, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("fVehHead", fVehHead, -180, 180, 1.0)
					ADD_WIDGET_INT_SLIDER("iColour2", sTempData.iColour2, 0, 500, 1)
					ADD_WIDGET_BOOL("bColourCombo", sTempData.bColourCombo)
					ADD_WIDGET_INT_SLIDER("iColourCombo", sTempData.iColourCombo, 0, GET_NUMBER_OF_VEHICLE_COLOURS(vehicleIndex), 1)
					ADD_WIDGET_INT_SLIDER("iColour1", sTempData.iColour1, 0, 500, 1)
					ADD_WIDGET_INT_SLIDER("iColour2", sTempData.iColour2, 0, 500, 1)
					
					ADD_WIDGET_BOOL("bColourExtra", sTempData.bColourExtra)
					ADD_WIDGET_INT_SLIDER("iColourExtra1", sTempData.iColourExtra1, 0, 134, 1)
					ADD_WIDGET_INT_SLIDER("iColourExtra2", sTempData.iColourExtra2, 0, 134, 1)
					
					ADD_WIDGET_FLOAT_SLIDER("fDirtLevel", sTempData.fDirtLevel, 0.0, 15.9, 1.0)
					ADD_WIDGET_INT_SLIDER("fHealth", sTempData.fHealth, 0, 10000, 1)
					
				STOP_WIDGET_GROUP()
				
				WHILE bMoveVeh
				AND NOT IS_ENTITY_DEAD(vehicleIndex)
					
					SET_ENTITY_COORDS(vehicleIndex, vFamilySceneCoord+vVehOffset)
					SET_ENTITY_HEADING(vehicleIndex, fFamilySceneHead+fVehHead)
					
					// Colours
				    IF sTempData.bColourCombo
				        SET_VEHICLE_COLOUR_COMBINATION(vehicleIndex, sTempData.iColourCombo)
				    ELSE
				        SET_VEHICLE_COLOURS(vehicleIndex, sTempData.iColour1, sTempData.iColour2)
				    ENDIF
				    IF sTempData.bColourExtra
				        SET_VEHICLE_EXTRA_COLOURS(vehicleIndex, sTempData.iColourExtra1, sTempData.iColourExtra2)
				    ENDIF
				    
				    // Dirt
				    SET_VEHICLE_DIRT_LEVEL(vehicleIndex, sTempData.fDirtLevel)
				    SET_ENTITY_HEALTH(vehicleIndex, sTempData.fHealth)
					
					WAIT(0)
				ENDWHILE

				SAVE_STRING_TO_DEBUG_FILE("")
				
				bMoveVeh = FALSE
			ENDIF
		ENDIF
		#ENDIF
		
	ENDIF
ENDPROC


PROC CreateThisFamilyProp(OBJECT_INDEX &propIndex, PED_INDEX pedIndex,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vFamilySceneCoord, FLOAT fFamilySceneHead,
		MODEL_NAMES propModel, PED_BONETAG pedBonetag,
		VECTOR &vPropAttachOffset, VECTOR &vPropAttachRotation
		
		#IF IS_DEBUG_BUILD
		, WIDGET_GROUP_ID family_scene_widget, BOOL &bMoveProp
		#ENDIF
		
		)
	
	IF (propModel <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT DOES_ENTITY_EXIST(propIndex)
			VECTOR vPedIndexCoord = GET_ENTITY_COORDS(pedIndex)
			
			REQUEST_MODEL(propModel)
			IF HAS_MODEL_LOADED(propModel)
				
				IF (propModel = PROP_BONG_01)
					propIndex = GET_CLOSEST_OBJECT_OF_TYPE(vPedIndexCoord, 2.5, propModel)
				ELSE
				
				
					IF IS_PED_INJURED(pedIndex)
						EXIT
					ENDIF
					
					IF (propModel = PROP_BONG_01)
					OR (propModel = P_CS_LIGHTER_01)
						CREATE_MODEL_HIDE(vPedIndexCoord, 1.5, propModel, FALSE)
					ENDIF
					propIndex = CREATE_OBJECT(propModel, vPedIndexCoord)
					SET_CAN_CLIMB_ON_ENTITY(propIndex, FALSE)
				ENDIF
				
				
				IF NOT DOES_ENTITY_EXIST(propIndex)
					EXIT
				ENDIF
				
				IF (pedBonetag <> BONETAG_NULL)
					IF NOT IS_PED_INJURED(pedIndex)
						IF (propModel <> GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL))
							ATTACH_ENTITY_TO_ENTITY(propIndex, pedIndex,
									GET_PED_BONE_INDEX(pedIndex, pedBonetag),
									vPropAttachOffset, vPropAttachRotation,
									TRUE, DEFAULT, TRUE)
						ELSE
							ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(propIndex, pedIndex,			//1110814
									GET_PED_BONE_INDEX(pedIndex, pedBonetag),
									GET_ENTITY_BONE_INDEX_BY_NAME(propIndex, "Gun_GripR"),
									vPropAttachOffset, <<0,0,0>>,
									vPropAttachRotation,
									-1,		//FLOAT PhysicalStrength,
									TRUE,	//BOOL ConstrainRotation,
									TRUE,	//BOOL DoInitialWarp = TRUE,
									FALSE,	//bool CollideWithEntity = FALSE,
									TRUE)	//BOOL AddInitialSeperation = TRUE
						ENDIF
					ENDIF
				ELSE
					VECTOR vInitOffset
					FLOAT fInitHead
					IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
							vInitOffset, fInitHead)
						SET_ENTITY_COORDS(propIndex, vInitOffset+vFamilySceneCoord+vPropAttachOffset)
						SET_ENTITY_ROTATION(propIndex, <<0,0,fInitHead+fFamilySceneHead>>+vPropAttachRotation)
					ENDIF
					
					FREEZE_ENTITY_POSITION(propIndex, TRUE)
				ENDIF
				
				#IF IS_DEBUG_BUILD
				SET_CURRENT_WIDGET_GROUP(family_scene_widget)
					START_WIDGET_GROUP(cheap_GET_MODEL_NAME_FOR_DEBUG(propModel))
						ADD_WIDGET_VECTOR_SLIDER("vPropAttachOffset", vPropAttachOffset, -10.0, 10.0, 0.001)
						ADD_WIDGET_VECTOR_SLIDER("vPropAttachRotation", vPropAttachRotation, -180.0, 180.0, 1.0)
						
						ADD_WIDGET_BOOL("bMoveProp", bMoveProp)
					STOP_WIDGET_GROUP()
				CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
				#ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(propModel)
			ENDIF
		ELSE
			//
			
			#IF IS_DEBUG_BUILD
			IF bMoveProp
				IF (pedBonetag <> BONETAG_NULL)
					IF NOT IS_PED_INJURED(pedIndex)
						IF (propModel <> GET_WEAPONTYPE_MODEL(WEAPONTYPE_PISTOL))
							ATTACH_ENTITY_TO_ENTITY(propIndex, pedIndex,
									GET_PED_BONE_INDEX(pedIndex, pedBonetag),
									vPropAttachOffset, vPropAttachRotation,
									TRUE, DEFAULT, TRUE)
						ELSE
							ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(propIndex, pedIndex,			//1110814
									GET_PED_BONE_INDEX(pedIndex, pedBonetag),
									GET_ENTITY_BONE_INDEX_BY_NAME(propIndex, "Gun_GripR"),
									vPropAttachOffset, <<0,0,0>>,
									vPropAttachRotation,
									-1,		//FLOAT PhysicalStrength,
									TRUE,	//BOOL ConstrainRotation,
									TRUE,	//BOOL DoInitialWarp = TRUE,
									FALSE,	//bool CollideWithEntity = FALSE,
									TRUE)	//BOOL AddInitialSeperation = TRUE
						ENDIF
					ENDIF
				ELSE
					VECTOR vInitOffset
					FLOAT fInitHead
					IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
							vInitOffset, fInitHead)
						SET_ENTITY_COORDS(propIndex, vInitOffset+vFamilySceneCoord+vPropAttachOffset)
						SET_ENTITY_ROTATION(propIndex, <<0,0,fInitHead+fFamilySceneHead>>+vPropAttachRotation)
					ENDIF
				ENDIF
			ENDIF
			
			IF (GET_ENTITY_MODEL(propIndex) <> propModel)
				IF NOT IS_ENTITY_DEAD(propIndex)
					IF IS_ENTITY_ATTACHED(propIndex)
						DETACH_ENTITY(propIndex)
					ENDIF
				ENDIF
				DELETE_OBJECT(propIndex)
				
				IF propModel = DUMMY_MODEL_FOR_SCRIPT
					CWARNINGLN(DEBUG_FAMILY, "delete family prop for ", Get_String_From_FamilyEvent(eFamilyEvent)," model doesn't match \"DUMMY\"")
				ELSE
					CWARNINGLN(DEBUG_FAMILY, "delete family prop for ", Get_String_From_FamilyEvent(eFamilyEvent)," model doesn't match \"", GET_MODEL_NAME_FOR_DEBUG(propModel), "\"")
				ENDIF
			ENDIF
			#ENDIF
			
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(propIndex)
			IF NOT IS_ENTITY_DEAD(propIndex)
				IF IS_ENTITY_ATTACHED(propIndex)
					DETACH_ENTITY(propIndex)
				ENDIF
			ENDIF
			DELETE_OBJECT(propIndex)
		ENDIF
	ENDIF
ENDPROC

PROC CreateThisFamilyExtraPed(PED_INDEX &extraPedIndex, PED_INDEX pedIndex, MODEL_NAMES extraPedModel,
		VECTOR &vExtraPedOffset, FLOAT &fExtraPedHeading
		
		#IF IS_DEBUG_BUILD
		, WIDGET_GROUP_ID family_scene_widget, BOOL &bMovePed,
		enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent
		#ENDIF
		
		)
		


	IF (extraPedModel <> DUMMY_MODEL_FOR_SCRIPT)
		IF NOT IS_ENTITY_DEAD(pedIndex)
			IF NOT DOES_ENTITY_EXIST(extraPedIndex)
				
				VECTOR vPedIndexCoord = GET_ENTITY_COORDS(pedIndex)
				
				REQUEST_MODEL(extraPedModel)
				IF HAS_MODEL_LOADED(extraPedModel)
				
					// 
//					IF IS_SPHERE_VISIBLE(vPedIndexCoord+vExtraPedOffset, 2.5)
//						EXIT
//					ENDIF
//				
//					// 
//					IF IsPlayerVehicleInterfering(vPedIndexCoord+vExtraPedOffset, 4.0)
//						EXIT
//					ENDIF
					
					extraPedIndex = CREATE_PED(PEDTYPE_CIVMALE,
							extraPedModel,
							vPedIndexCoord+vExtraPedOffset,
							GET_ENTITY_HEADING(pedIndex)+fExtraPedHeading)
					
					PRIVATE_SetDefaultFamilyMemberAttributes(extraPedIndex, RELGROUPHASH_NO_RELATIONSHIP)
//					SetFamilyMemberConfigFlag(extraPedIndex, eFamilyMember)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)

					
					IF (extraPedModel = S_M_Y_COP_01)
						SET_PED_HIGHLY_PERCEPTIVE(extraPedIndex, TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
					SET_CURRENT_WIDGET_GROUP(family_scene_widget)
						START_WIDGET_GROUP(cheap_GET_MODEL_NAME_FOR_DEBUG(extraPedModel))
							ADD_WIDGET_VECTOR_SLIDER("vFamilyExtraPedCoordOffset", vExtraPedOffset, -10.0, 10.0, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fFamilyExtraPedeadOffset", fExtraPedHeading, -180.0, 180.0, 1.0)
						STOP_WIDGET_GROUP()
						
						ADD_WIDGET_BOOL("bMovePed", bMovePed)
					CLEAR_CURRENT_WIDGET_GROUP(family_scene_widget)
					#ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(extraPedModel)
				ENDIF
			ELSE
				//
				
				#IF IS_DEBUG_BUILD
				IF bMovePed
					IF NOT IS_PED_INJURED(extraPedIndex)
					AND NOT IS_PED_INJURED(pedIndex)
						DrawDebugFamilyLine(GET_ENTITY_COORDS(extraPedIndex), GET_ENTITY_COORDS(pedIndex), HUD_COLOUR_GREEN)
						
						SET_ENTITY_COORDS(extraPedIndex,
								GET_ENTITY_COORDS(pedIndex)+vExtraPedOffset)
						SET_ENTITY_HEADING(extraPedIndex,
								GET_ENTITY_HEADING(pedIndex)+fExtraPedHeading)
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
							
							OPEN_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(eFamilyEvent))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	SWITCH eMember")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			eFamilyExtraPedModel[eMember]		= ")SAVE_STRING_TO_DEBUG_FILE(cheap_GET_MODEL_NAME_FOR_DEBUG(extraPedModel))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			vFamilyExtraPedCoordOffset[eMember]	= ")SAVE_VECTOR_TO_DEBUG_FILE(vExtraPedOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("			fFamilyExtraPedHeadOffset[eMember]	= ")SAVE_FLOAT_TO_DEBUG_FILE(fExtraPedHeading)SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	ENDSWITCH")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
							CLOSE_DEBUG_FILE()
							
						ENDIF
					ELSE
						DrawDebugFamilyLine(GET_ENTITY_COORDS(extraPedIndex, FALSE), GET_ENTITY_COORDS(pedIndex, FALSE), HUD_COLOUR_RED,172.0/255.0)
					ENDIF
				ENDIF
				#ENDIF
				
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(extraPedIndex)
			IF NOT IS_ENTITY_DEAD(extraPedIndex)
				IF IS_ENTITY_ATTACHED(extraPedIndex)
					DETACH_ENTITY(extraPedIndex)
				ENDIF
			ENDIF
			DELETE_PED(extraPedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC UpdateInteriorForPlayer(BOOL &bUpdatePlayerInterior,
		structTimer &sTimerUpdatePlayerInterior,
		INTERIOR_INSTANCE_INDEX &iPlayerInterior, INTERIOR_INSTANCE_INDEX &iSafehouseInterior,
		VECTOR VecInCoors, STRING Name)
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT bUpdatePlayerInterior
		IF NOT IS_TIMER_STARTED(sTimerUpdatePlayerInterior)
			bUpdatePlayerInterior = TRUE
		ELSE
			CONST_FLOAT fCONST_updatePlayerInteriorTimer 1.0
			IF TIMER_DO_WHEN_READY(sTimerUpdatePlayerInterior, fCONST_updatePlayerInteriorTimer)
				bUpdatePlayerInterior = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			iPlayerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			
			IF NOT IS_STRING_NULL_OR_EMPTY(Name)
				IF NOT IS_VALID_INTERIOR(iSafehouseInterior)
					iSafehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(VecInCoors, Name)
				ENDIF
			ENDIF
			
			RESTART_TIMER_NOW(sTimerUpdatePlayerInterior)
			bUpdatePlayerInterior = FALSE
		ENDIF
	ENDIF
	
	IF IS_VALID_INTERIOR(iPlayerInterior)
	AND IS_INTERIOR_READY(iPlayerInterior)
		IF iPlayerInterior = iSafehouseInterior
			
			
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)

				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("IS_PLAYER_WANTED_LEVEL_GREATER",
						GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),
						-1.0, HUD_COLOUR_BLUE)
				#ENDIF
				
				EXIT
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PICKUP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)		//717963
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)		//717963
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)				//717963
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)	//1432687
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)				//2058789
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
			
			//1402049
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)				//889978
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
			
			IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
//			OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//			OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
				IF NOT g_savedGlobals.sFamilyData.bSeenFamWeaponDisplay
					PRINT_HELP("FAM_WEAPDIS")
					g_savedGlobals.sFamilyData.bSeenFamWeaponDisplay = TRUE
				ENDIF
				
				iPlayerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
				
				IF NOT IS_STRING_NULL_OR_EMPTY(Name)
					IF NOT IS_VALID_INTERIOR(iSafehouseInterior)
						iSafehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(VecInCoors, Name)
					ENDIF
				ENDIF
				
				RESTART_TIMER_NOW(sTimerUpdatePlayerInterior)
				bUpdatePlayerInterior = FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("DISABLE_CONTROL_ACTION(INPUT_SELECT_WEAPON)",
					GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),
					-1.0, HUD_COLOUR_RED)
			
			#ENDIF
			
			WEAPON_TYPE currentWeaponType
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeaponType)
				IF (currentWeaponType <> WEAPONTYPE_UNARMED)
				AND (currentWeaponType <> WEAPONTYPE_OBJECT)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> Is_Scene_Member_Active - player set to unarmed ")
				ENDIF
			ENDIF
			IF GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), currentWeaponType)
				IF (currentWeaponType <> WEAPONTYPE_UNARMED)
				AND (currentWeaponType <> WEAPONTYPE_OBJECT)
					SET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> Is_Scene_Member_Active - player set to unarmed (veh) ")
				ENDIF
			ENDIF
			
//			//
//			CPRINTLN(DEBUG_FAMILY, "DISABLE_CONTROL_ACTION")
//			cprintNL()
		ELSE
			
			//#1527519
			IF NOT IS_STRING_NULL_OR_EMPTY(Name)
				VECTOR iPlayerInteriorCoord
				iPlayerInteriorCoord = GET_OFFSET_FROM_INTERIOR_IN_WORLD_COORDS(iPlayerInterior, <<0,0,0>>)
				
				IF VDIST2(VecInCoors, iPlayerInteriorCoord) < (5.0*5.0)
					IF NOT IS_VALID_INTERIOR(iSafehouseInterior)
						iSafehouseInterior = iPlayerInterior
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ShouldCreateInteriorFamilyMember(enumFamilyMember eFamilyMember, VECTOR vSceneMemberCoord,
		INTERIOR_INSTANCE_INDEX iInteriorForThisPlayer, INTERIOR_INSTANCE_INDEX &iInteriorForThisMember
		
		#IF IS_DEBUG_BUILD	, HUD_COLOURS &hudColour	#ENDIF
		)
	
	IF NOT IS_COLLISION_MARKED_OUTSIDE(vSceneMemberCoord)
		IF NOT IS_VALID_INTERIOR(iInteriorForThisMember)
			#IF IS_DEBUG_BUILD
			hudColour = HUD_COLOUR_PINK
			DrawFamilyLiteralStringInt(Get_String_From_FamilyMember(eFamilyMember),
					NATIVE_TO_INT(iInteriorForThisMember), (ENUM_TO_INT(eFamilyMember)*2),
					hudColour)
			#ENDIF
			
			iInteriorForThisMember = GET_INTERIOR_FROM_COLLISION(vSceneMemberCoord)
			RETURN FALSE
			
		ELSE
			
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				IF (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
					IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF (eFamilyMember = FM_TREVOR_0_MOTHER)
		IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF (iInteriorForThisMember <> iInteriorForThisPlayer)
		IF (iInteriorForThisMember = NULL)
			#IF IS_DEBUG_BUILD
			hudColour = HUD_COLOUR_GREEN
			#ENDIF
		ELSE
			IF IS_VALID_INTERIOR(iInteriorForThisMember)
				IF IS_INTERIOR_READY(iInteriorForThisMember)
					#IF IS_DEBUG_BUILD
					hudColour = HUD_COLOUR_RED
					
					DrawDebugFamilyLine(vSceneMemberCoord, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), hudColour)
					DrawDebugFamilySphere(vSceneMemberCoord, 0.5, hudColour)
					#ENDIF
					
					
					VECTOR vMemberInteriorCoord = GET_OFFSET_FROM_INTERIOR_IN_WORLD_COORDS(iInteriorForThisMember, <<0,0,0>>)
					FLOAT fPlayerInteriorDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMemberInteriorCoord)
					FLOAT fSceneInteriorDist2 = VDIST2(vSceneMemberCoord, vMemberInteriorCoord)
					
					
					IF (fPlayerInteriorDist2 > fSceneInteriorDist2)
						#IF IS_DEBUG_BUILD
						hudColour = HUD_COLOUR_GREEN
						DrawFamilyLiteralStringInt(Get_String_From_FamilyMember(eFamilyMember),
								NATIVE_TO_INT(iInteriorForThisMember), (ENUM_TO_INT(eFamilyMember)*2),
								hudColour)
						DrawFamilyLiteralStringInt("player int: ",
								NATIVE_TO_INT(iInteriorForThisPlayer), (ENUM_TO_INT(eFamilyMember)*2)+1,
								hudColour)
						#ENDIF
						
						RETURN TRUE
					ENDIF
					
					IF NOT IS_SPHERE_VISIBLE(vSceneMemberCoord, 1.0)
						MODEL_NAMES eMemberModel = DUMMY_MODEL_FOR_SCRIPT
						enumCharacterList eCharID = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eMemberModel)
						
						IF (eCharID <> NO_CHARACTER)
							IF IS_PLAYER_PED_PLAYABLE(eCharID)
								eMemberModel = GET_PLAYER_PED_MODEL(eCharID)
							ELSE
								eMemberModel = GET_NPC_PED_MODEL(eCharID)
							ENDIF
						ENDIF
						
						IF NOT WOULD_ENTITY_BE_OCCLUDED(eMemberModel, vSceneMemberCoord, FALSE)
							#IF IS_DEBUG_BUILD
							hudColour = HUD_COLOUR_GREEN
							DrawFamilyLiteralStringInt(Get_String_From_FamilyMember(eFamilyMember),
									NATIVE_TO_INT(iInteriorForThisMember), (ENUM_TO_INT(eFamilyMember)*2),
									hudColour)
							DrawFamilyLiteralStringInt("player int: ",
									NATIVE_TO_INT(iInteriorForThisPlayer), (ENUM_TO_INT(eFamilyMember)*2)+1,
									hudColour)
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					hudColour = HUD_COLOUR_BLUE
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				hudColour = HUD_COLOUR_BLUE
				#ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		DrawFamilyLiteralStringInt(Get_String_From_FamilyMember(eFamilyMember),
				NATIVE_TO_INT(iInteriorForThisMember), (ENUM_TO_INT(eFamilyMember)*2),
				hudColour)
		DrawFamilyLiteralStringInt("player int: ",
				NATIVE_TO_INT(iInteriorForThisPlayer), (ENUM_TO_INT(eFamilyMember)*2)+1,
				hudColour)
		#ENDIF
		
		IF VDIST2(vSceneMemberCoord, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
			RETURN TRUE
		ENDIF
		
		//dont create family member if player is not in the same interior
		RETURN FALSE
	ELSE
		IF (iInteriorForThisMember = NULL)
			#IF IS_DEBUG_BUILD
			hudColour = HUD_COLOUR_BLACK
			#ENDIF
		ELSE
			IF IS_VALID_INTERIOR(iInteriorForThisMember)
				IF IS_INTERIOR_READY(iInteriorForThisMember)
					#IF IS_DEBUG_BUILD
					hudColour = HUD_COLOUR_PURE_WHITE
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					hudColour = HUD_COLOUR_GREY
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				hudColour = HUD_COLOUR_GREY
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ShouldCreateFamilyMember(enumFamilyMember eFamilyMember, VECTOR vSceneMemberCoord,
		INTERIOR_INSTANCE_INDEX iInteriorForThisPlayer, INTERIOR_INSTANCE_INDEX &iInteriorForThisMember)
		
	#IF IS_DEBUG_BUILD
	HUD_COLOURS hudColour = HUD_COLOUR_PURE_WHITE
	
	IF g_bUpdatedFamilyEvents
	OR g_bDebugForceCreateFamily
		DrawDebugFamilyLine(vSceneMemberCoord, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), hudColour)
		DrawDebugFamilySphere(vSceneMemberCoord, 0.5, hudColour)
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	FLOAT fSceneMemberCoord_groundZ
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vSceneMemberCoord, fSceneMemberCoord_groundZ)
		IF NOT GET_GROUND_Z_FOR_3D_COORD(vSceneMemberCoord+<<0,0,1>>, fSceneMemberCoord_groundZ)
			#IF IS_DEBUG_BUILD
			hudColour = HUD_COLOUR_YELLOW
			
			TEXT_LABEL_63 str = Get_String_From_FamilyMember(eFamilyMember)
			str += ".z:"
			IF (fSceneMemberCoord_groundZ <> 0)
				str += GET_STRING_FROM_FLOAT(fSceneMemberCoord_groundZ)
			ELSE
				str += "null"
			ENDIF
			
			DrawFamilyLiteralString(str, (ENUM_TO_INT(eFamilyMember)*2), hudColour)
			DrawDebugFamilyLine(vSceneMemberCoord, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), hudColour)
			DrawDebugFamilySphere(vSceneMemberCoord, 0.5, hudColour)
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	//force family members to be created with the mag demo...
	IF g_bMagDemoActive
		IF (eFamilyMember = FM_MICHAEL_DAUGHTER)
		OR (eFamilyMember = FM_MICHAEL_WIFE)
		OR (eFamilyMember = FM_MICHAEL_MEXMAID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT ShouldCreateInteriorFamilyMember(eFamilyMember, vSceneMemberCoord,
			iInteriorForThisPlayer, iInteriorForThisMember
			#IF IS_DEBUG_BUILD	, hudColour		#ENDIF
			)
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eMemberModel = DUMMY_MODEL_FOR_SCRIPT
	enumCharacterList eCharID = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eMemberModel)
	
	//blocking family member creation if communications are queued from them.
	IF DOES_CHAR_HAVE_COMMUNICATION_QUEUED(eCharID)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = Get_String_From_FamilyMember(eFamilyMember)
		str += " queued comms"
		DrawFamilyLiteralString(str, (ENUM_TO_INT(eFamilyMember)*2), hudColour)
		#ENDIF
		
		Set_Current_Event_For_FamilyMember(eFamilyMember, FAMILY_MEMBER_BUSY)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		IF (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
			IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_ESTABLISHING_SHOT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF (eFamilyMember = FM_TREVOR_0_MOTHER)
		IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	// player ped is potentially on screen when created, so dont create!
	IF (eCharID <> NO_CHARACTER)
		IF IS_PLAYER_PED_PLAYABLE(eCharID)
			eMemberModel = GET_PLAYER_PED_MODEL(eCharID)
		ELSE
			eMemberModel = GET_NPC_PED_MODEL(eCharID)
		ENDIF
	ENDIF
	
	//#1390895
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	IF (g_eLastMissionPassed = SP_MISSION_ARMENIAN_1)
		IF ((eCharID = CHAR_DENISE) AND (g_eCurrentFamilyEvent[FM_FRANKLIN_AUNT] = FE_F_AUNT_watching_TV))
			RETURN TRUE
		ENDIF
	ENDIF
	
	//#1517574
	IF (g_eLastMissionPassed = SP_HEIST_DOCKS_1)
		IF ((eCharID = CHAR_FLOYD) AND (g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD] = FE_T1_FLOYD_with_wade_post_docks1))
		OR ((eCharID = CHAR_WADE) AND (g_eCurrentFamilyEvent[FM_TREVOR_1_WADE] = FE_T1_FLOYD_with_wade_post_docks1))
			RETURN TRUE
		ENDIF
	ENDIF
	
	#ENDIF
	#ENDIF
	
	BOOL bMemberWouldBeHidden = TRUE
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ""
	#ENDIF
	
	FLOAT fPlayerDistFromScene2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vSceneMemberCoord)
	
	IF WOULD_ENTITY_BE_OCCLUDED(eMemberModel, vSceneMemberCoord, FALSE)
		#IF IS_DEBUG_BUILD
		str += ("OCCLUDED, ")
		#ENDIF
		
		IF IS_SPHERE_VISIBLE(vSceneMemberCoord, 4.0)
			IF (fPlayerDistFromScene2 < (10*10))
				bMemberWouldBeHidden = FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str += ("VISIBLE ")
		ELSE
			str += ("hidden ")
			#ENDIF
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		str += ("not occluded, ")
		#ENDIF
		
		IF IS_SPHERE_VISIBLE(vSceneMemberCoord, 1.0)
			IF (fPlayerDistFromScene2 < (10*10))
				bMemberWouldBeHidden = FALSE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str += ("VISIBLE ")
		ELSE
			str += ("hidden ")
			#ENDIF
		ENDIF
		
	ENDIF
	
	IF ((eCharID = CHAR_JIMMY) AND (g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M7_SON_coming_back_from_a_bike_ride))
	AND NOT IS_VALID_INTERIOR(iInteriorForThisPlayer)
		IF WOULD_ENTITY_BE_OCCLUDED(eMemberModel, vSceneMemberCoord, FALSE)
			#IF IS_DEBUG_BUILD
			str += ("OCCLUDED, ")
			#ENDIF
			
			IF IS_SPHERE_VISIBLE(vSceneMemberCoord, 8.0)
				IF (fPlayerDistFromScene2 < (30*30))
					bMemberWouldBeHidden = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				str += ("VISIBLE ")
			ELSE
				str += ("hidden ")
				#ENDIF
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
			str += ("not occluded, ")
			#ENDIF
			
			IF IS_SPHERE_VISIBLE(vSceneMemberCoord, 2.0)
				IF (fPlayerDistFromScene2 < (30*30))
					bMemberWouldBeHidden = FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				str += ("VISIBLE ")
			ELSE
				str += ("hidden ")
				#ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (hudColour = HUD_COLOUR_BLACK)
		IF bMemberWouldBeHidden
			hudColour = HUD_COLOUR_BLUELIGHT
		ELSE
			hudColour = HUD_COLOUR_BLUEDARK
		ENDIF
	ENDIF
	
	DrawFamilyLiteralStringInt(Get_String_From_FamilyMember(eFamilyMember),
			NATIVE_TO_INT(iInteriorForThisMember), (ENUM_TO_INT(eFamilyMember)*2),
			hudColour)
	DrawFamilyLiteralString(str,
			(ENUM_TO_INT(eFamilyMember)*2)+1,
			hudColour)
	
	DrawDebugFamilyLine(vSceneMemberCoord, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), hudColour)
	DrawDebugFamilySphere(vSceneMemberCoord, 0.5, hudColour)
	
	#ENDIF
	
	RETURN bMemberWouldBeHidden
ENDFUNC

FUNC BOOL SetFamilyMemberConfigFlag(PED_INDEX pedIndex, enumFamilyMember eFamilyMember, REL_GROUP_HASH eRelGroup, BOOL bIgnoreForceRoom)
	
	IF IS_ENTITY_DEAD(pedIndex)
		RETURN FALSE
	ENDIF
	
	IF (pedIndex = PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_FAMILY, "SetFamilyMemberConfigFlag(", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedIndex)), ", ", Get_String_From_FamilyMember(eFamilyMember), ", ", ENUM_TO_INT(eRelGroup), ")")
	
	IF (eFamilyMember <> FM_MICHAEL_MEXMAID) AND (g_eCurrentFamilyEvent[eFamilyMember] <> FE_M_WIFE_screams_at_mexmaid)
	AND (g_eCurrentFamilyEvent[eFamilyMember] <> FE_ANY_find_family_event) AND (g_eCurrentFamilyEvent[eFamilyMember] <> FE_ANY_wander_family_event)
		CLEAR_PED_TASKS(pedIndex)								//#1569188
	ENDIF
	
	BOOL bUpdatedPedConfigFlag = FALSE
	BOOL bUpdatedRagdollBlockingFlags = FALSE
	BOOL bForcedRoomForPed = FALSE
	
	
	SET_PED_RESET_FLAG(pedIndex, PRF_SearchForClosestDoor, TRUE)	//1456149
	SET_PED_CONFIG_FLAG(pedIndex, PCF_OpenDoorArmIK, TRUE)
	SET_PED_PATH_CAN_USE_CLIMBOVERS(pedIndex, FALSE)				//1452401
	bUpdatedPedConfigFlag = TRUE
	
	SET_PED_RESET_FLAG(pedIndex, PRF_SupressGunfireEvents, TRUE)
//	SET_PED_CONFIG_FLAG(pedIndex, PCF_DisableExplosionReactions, TRUE)
	SET_PED_CONFIG_FLAG(pedIndex, PCF_DisableShockingEvents, TRUE)
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_RONEX_doing_target_practice)
	OR (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_RONEX_outside_looking_lonely)
		
	ELSE
		SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_VEHICLE_IMPACT)		//1445300
		bUpdatedPedConfigFlag = TRUE
	ENDIF
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_MICHAEL_sunbathing)
		SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_IMPACT_OBJECT)
		bUpdatedPedConfigFlag = TRUE
	ENDIF
	
	FLOAT fCapsuleRadius
	IF PRIVATE_SetFamilyMemberPedCapsule(eFamilyMember, fCapsuleRadius)
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" SET_PED_CAPSULE ", fCapsuleRadius)
		#ENDIF
		
		SET_PED_CAPSULE(PedIndex, fCapsuleRadius)
		bUpdatedRagdollBlockingFlags = TRUE
	ENDIF
	
	IF NOT bIgnoreForceRoom
		IF PRIVATE_ForceRoomForFamilyEntity(PedIndex, eFamilyMember)
			bForcedRoomForPed = TRUE
		ENDIF
	ENDIF
	
	PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
	
	
	//1503578
	IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_TREVOR_and_kidnapped_wife_walk)
		SET_FORCE_STEP_TYPE(pedIndex,TRUE,20,0)			//1503578
		bUpdatedPedConfigFlag = TRUE
	ELIF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_DAUGHTER_walks_to_room_music)
		//												//1498526
	ELSE
		SET_FORCE_STEP_TYPE(pedIndex, TRUE, 0,1)		//421373
		bUpdatedPedConfigFlag = TRUE
	ENDIF
	
	IF PRIVATE_Is_Family_Sleeping(g_eCurrentFamilyEvent[eFamilyMember])
		SET_FACIAL_IDLE_ANIM_OVERRIDE(pedIndex, "mood_sleeping_1")
		bUpdatedPedConfigFlag = TRUE
	ELSE
		CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(pedIndex)
	ENDIF
	
	IF bUpdatedPedConfigFlag
	OR bUpdatedRagdollBlockingFlags
	OR bForcedRoomForPed
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GetFamilyMemberFromScene(PED_INDEX &pedIndex, enumFamilyMember eFamilyMember,
		structPedsForConversation &YourLocalPedStruct, INT YourNumberID, REL_GROUP_HASH eRelGroup)
	
	IF (g_eCurrentFamilyEvent[eFamilyMember]= FAMILY_MEMBER_BUSY)
	AND (g_eSceneBuddyEvents <> NO_FAMILY_EVENTS)
		
		#IF IS_DEBUG_BUILD
		IF NOT DOES_ENTITY_EXIST(g_pScene_buddy)
			CPRINTLN(DEBUG_FAMILY, "<", Get_String_From_FamilyMember(eFamilyMember), "> test1 - g_eSceneBuddyEvents(", Get_String_From_FamilyEvent(g_eSceneBuddyEvents), "), g_pScene_buddy: ", NATIVE_TO_INT(g_pScene_buddy))
		ELSE
			CPRINTLN(DEBUG_FAMILY, "<", Get_String_From_FamilyMember(eFamilyMember), "> test1 - g_eSceneBuddyEvents(", Get_String_From_FamilyEvent(g_eSceneBuddyEvents), "), g_pScene_buddy: ", cheap_GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(g_pScene_buddy)))
		ENDIF
		#ENDIF
		
		IF DOES_ENTITY_EXIST(g_pScene_buddy)
			MODEL_NAMES eMemberModel = DUMMY_MODEL_FOR_SCRIPT
			enumCharacterList eCharID = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eMemberModel)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", Get_String_From_FamilyMember(eFamilyMember), "> test2 - eMemberModel: ", cheap_GET_MODEL_NAME_FOR_DEBUG(eMemberModel), ", eCharID, ", GET_PLAYER_PED_STRING(eCharID))
			#ENDIF
			
			IF (eCharID <> NO_CHARACTER)
				IF IS_PLAYER_PED_PLAYABLE(eCharID)
					eMemberModel = GET_PLAYER_PED_MODEL(eCharID)
				ELSE
					eMemberModel = GET_NPC_PED_MODEL(eCharID)
				ENDIF
				
				IF (GET_ENTITY_MODEL(g_pScene_buddy) = eMemberModel)
				
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "g_pScene_buddy could be family member ", cheap_GET_MODEL_NAME_FOR_DEBUG(eMemberModel), "!")
					#ENDIF
					
					pedIndex = g_pScene_buddy
					g_pScene_buddy = NULL
					SET_ENTITY_AS_MISSION_ENTITY(pedIndex, TRUE, TRUE)
					
					Update_Previous_Event_For_FamilyMember(eFamilyMember)
					g_eCurrentFamilyEvent[eFamilyMember] = g_eSceneBuddyEvents
					
					IF DOES_ENTITY_EXIST(g_pScene_extra_buddy)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "g_pScene_buddy = g_pScene_extra_buddy")
						#ENDIF
						
						g_pScene_buddy = g_pScene_extra_buddy
						g_pScene_extra_buddy = NULL
					ELSE
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "g_eSceneBuddyEvents = NO_FAMILY_EVENTS")
						#ENDIF
						
						g_eSceneBuddyEvents = NO_FAMILY_EVENTS
					ENDIF
					
					PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
//						SetFamilyMemberComponentVariation(pedIndex, eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
//						SetFamilyMemberConfigFlag(pedIndex, eFamilyMember)
					
					ADD_PED_FOR_DIALOGUE(YourLocalPedStruct, YourNumberID,
							pedIndex, Get_VoiceID_From_FamilyMember(eFamilyMember))
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG3LN(DEBUG_FAMILY, "<", Get_String_From_FamilyMember(eFamilyMember), "> GetFamilyMemberFromScene - g_eSceneBuddyEvents:", Get_String_From_FamilyEvent(g_eSceneBuddyEvents), ", g_eCurrentFamilyEvent: ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
		#ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Is_Scene_Member_Active(PED_INDEX &pedIndex, enumFamilyMember eFamilyMember,
		VECTOR vSceneMemberCoord, FLOAT fSceneMemberHeading,
		INTERIOR_INSTANCE_INDEX &iInteriorForThisPlayer, INTERIOR_INSTANCE_INDEX &iInteriorForThisMember,
		structPedsForConversation &YourLocalPedStruct, INT YourNumberID,
		REL_GROUP_HASH eRelGroup)
		
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY) OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY_PREP))
	OR (GET_RANDOM_EVENT_FLAG() AND g_bRandomEventActive)
	OR IS_MISSION_LEADIN_ACTIVE()
		IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
			
			Update_Previous_Event_For_FamilyMember(eFamilyMember)
			g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
			
			#IF IS_DEBUG_BUILD
			IF (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY))
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS for ", "mission")
			ENDIF
			IF (GET_RANDOM_EVENT_FLAG())
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS for ", "random event")
			ENDIF
			#ENDIF
			
		ENDIF
	ENDIF
	
	MODEL_NAMES eMemberModel = DUMMY_MODEL_FOR_SCRIPT
	enumCharacterList eCharID = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eMemberModel)
	
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		
		//dont create the family member if current family event is NO_FAMILY_EVENTS
		IF (g_eCurrentFamilyEvent[eFamilyMember]= NO_FAMILY_EVENTS)
		OR (g_eCurrentFamilyEvent[eFamilyMember]= FAMILY_MEMBER_BUSY)
			IF GetFamilyMemberFromScene(pedIndex, eFamilyMember,
					YourLocalPedStruct, YourNumberID, eRelGroup)
				RETURN FALSE
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		BOOL bWaitingForSceneAnims = FALSE
		//dont create family member if for some reason you shouldnt (e.g. in wrong room)
		IF (g_eCurrentFamilyEvent[eFamilyMember] <> FE_T0_KIDNAPPED_WIFE_does_garden_work)
			IF NOT ShouldCreateFamilyMember(eFamilyMember, vSceneMemberCoord,
					iInteriorForThisPlayer, iInteriorForThisMember)
				WAIT(0)
					
				RETURN FALSE
			ENDIF
		ENDIF
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		
		IF (g_eLastMissionPassed = SP_MISSION_ARMENIAN_1)
		AND ((eCharID = CHAR_DENISE) AND (g_eCurrentFamilyEvent[FM_FRANKLIN_AUNT] = FE_F_AUNT_watching_TV))
			//
		ELSE
			
			IF IsPlayerVehicleInterfering(vSceneMemberCoord, 4.0)
				WAIT(0)
					
				RETURN FALSE
			ENDIF
		ENDIF
		
		#ENDIF
		#ENDIF
	
	
//		IF g_eCurrentFamilyEvent[eFamilyMember]= FE_M_WIFE_MD_leaving_in_car_v3
//			eCharID = NO_CHARACTER
//			eMemberModel = S_F_Y_HOOKER_01
//		ENDIF
		
		TEXT_LABEL_63 tFamilyAnimDict
		IF PRIVATE_Preload_FamilyMember_Anim(g_eCurrentFamilyEvent[eFamilyMember], tFamilyAnimDict)
			REQUEST_ANIM_DICT(tFamilyAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(tFamilyAnimDict)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> scene ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), " is waiting on anim dict \"", tFamilyAnimDict, "\"")
				#ENDIF
				
				bWaitingForSceneAnims = TRUE
			ENDIF
		ENDIF
		
		IF (eCharID <> NO_CHARACTER)
			
			BOOL bWaitingForPedModels = FALSE
			IF IS_PLAYER_PED_PLAYABLE(eCharID)
				REQUEST_PLAYER_PED_MODEL(eCharID)
				IF NOT HAS_PLAYER_PED_MODEL_LOADED(eCharID)
					bWaitingForPedModels = TRUE
				ENDIF
			ELSE
				REQUEST_NPC_PED_MODEL(eCharID)
				IF NOT HAS_NPC_PED_MODEL_LOADED(eCharID)
					bWaitingForPedModels = TRUE
				ENDIF
			ENDIF
				
			IF NOT bWaitingForSceneAnims
			AND NOT bWaitingForPedModels
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				IF IS_PLAYER_PED_PLAYABLE(eCharID)
					IF CREATE_PLAYER_PED_ON_FOOT(pedIndex, eCharID,
							vSceneMemberCoord, fSceneMemberHeading)
						PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
						SetFamilyMemberComponentVariation(pedIndex, eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
						SetFamilyMemberConfigFlag(pedIndex, eFamilyMember, eRelGroup, FALSE)
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)
						
						ADD_PED_FOR_DIALOGUE(YourLocalPedStruct, YourNumberID,
								pedIndex, Get_VoiceID_From_FamilyMember(eFamilyMember))
						
						SELECTOR_SLOTS_ENUM eSceneSlot = GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(eCharID)
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> pedID[", Get_String_From_FamilyMember(eFamilyMember), "] DOES NOT exist")
						#ENDIF
						
						g_sPlayerPedRequest.sSelectorPeds.pedID[eSceneSlot] = pedIndex
						STORE_PLAYER_PED_INFO(pedIndex)
						g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[eCharID] = GET_ENTITY_COORDS(pedIndex)
						g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[eCharID] = GET_ENTITY_HEADING(pedIndex)
						
						iInteriorForThisMember = GET_INTERIOR_FROM_ENTITY(pedIndex)
					ENDIF
				ELSE
					IF CREATE_NPC_PED_ON_FOOT(pedIndex, eCharID,
							vSceneMemberCoord, fSceneMemberHeading)
						PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
						SetFamilyMemberComponentVariation(pedIndex, eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
						SetFamilyMemberConfigFlag(pedIndex, eFamilyMember, eRelGroup, FALSE)
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)
						
						ADD_PED_FOR_DIALOGUE(YourLocalPedStruct, YourNumberID,
								pedIndex, Get_VoiceID_From_FamilyMember(eFamilyMember))
						
						iInteriorForThisMember = GET_INTERIOR_FROM_ENTITY(pedIndex)
					ENDIF
				ENDIF
			ENDIF
				
		ELSE
			IF (eMemberModel <> DUMMY_MODEL_FOR_SCRIPT)
				REQUEST_MODEL(eMemberModel)
				IF HAS_MODEL_LOADED(eMemberModel)
				AND NOT bWaitingForSceneAnims
					pedIndex = CREATE_PED(PEDTYPE_MISSION, eMemberModel, vSceneMemberCoord,
							fSceneMemberHeading,
							FALSE, FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(eMemberModel)
					
					PRIVATE_SetDefaultFamilyMemberAttributes(pedIndex, eRelGroup)
					SetFamilyMemberComponentVariation(pedIndex, eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
					SetFamilyMemberConfigFlag(pedIndex, eFamilyMember, eRelGroup, FALSE)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedIndex, TRUE)
					
					ADD_PED_FOR_DIALOGUE(YourLocalPedStruct, YourNumberID,
							pedIndex, Get_VoiceID_From_FamilyMember(eFamilyMember))
					
					
					#IF IS_DEBUG_BUILD
					SAVE_STRING_TO_DEBUG_FILE("<")
					SAVE_STRING_TO_DEBUG_FILE(GET_THIS_SCRIPT_NAME())
					SAVE_STRING_TO_DEBUG_FILE("> set pedID[")
					SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
					SAVE_STRING_TO_DEBUG_FILE("] iInteriorForThisMember: ")
					SAVE_INT_TO_DEBUG_FILE(NATIVE_TO_INT(iInteriorForThisMember))
					
					iInteriorForThisMember = GET_INTERIOR_FROM_ENTITY(pedIndex)
					
					SAVE_STRING_TO_DEBUG_FILE(", iInteriorForThisMember: ")
					SAVE_INT_TO_DEBUG_FILE(NATIVE_TO_INT(iInteriorForThisMember))
					SAVE_NEWLINE_TO_DEBUG_FILE()
					#ENDIF
					
				ENDIF
			ENDIF
				
		ENDIF
	ELSE
		
		IF NOT IS_PED_INJURED(pedIndex)
			FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 5.0
			
			SWITCH g_eCurrentFamilyEvent[eFamilyMember]
				CASE FE_M_SON_rapping_in_the_shower
				CASE FE_M_SON_watching_porn
				CASE FE_M_DAUGHTER_purges_in_the_bathroom
				CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
				CASE FE_M_DAUGHTER_sex_sounds_from_room
				CASE FE_M_DAUGHTER_on_phone_LOCKED
				CASE FE_M_FAMILY_MIC4_locked_in_room
				
				CASE FE_T1_FLOYD_with_wade_post_docks1
				
				CASE FE_T0_MOTHER_duringRandomChar
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedIndex)) < (fReplayBlockRange*fReplayBlockRange)	//30.0f radius on replay recording
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2184277
						CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> disable cam movement for pedID[", Get_String_From_FamilyMember(eFamilyMember), "]")
					ELSE
						#IF IS_DEBUG_BUILD
						IF g_bDrawDebugFamilyStuff
						CDEBUG3LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> allow cam movement for pedID[", Get_String_From_FamilyMember(eFamilyMember), "]")
						ENDIF
						#ENDIF
					ENDIF
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					IF g_bDrawDebugFamilyStuff
					CDEBUG3LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> ignore cam movement for pedID[", Get_String_From_FamilyMember(eFamilyMember), "]")
					ENDIF
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		IF (eFamilyMember = FM_TREVOR_0_MOTHER)
			IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				SET_ENTITY_VISIBLE(pedIndex, FALSE)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(pedIndex)
			IF NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedIndex)
			
				#IF IS_DEBUG_BUILD
				SAVE_STRING_TO_DEBUG_FILE("<")
				SAVE_STRING_TO_DEBUG_FILE(GET_THIS_SCRIPT_NAME())
				SAVE_STRING_TO_DEBUG_FILE("> pedID[")
				SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
				SAVE_STRING_TO_DEBUG_FILE("] not yet preloaded...")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				#ENDIF
			
				RETURN FALSE
			ENDIF
		ELSE
			IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
				
				
				CC_TextPart ePart1 = TPART_NONE
				SWITCH eFamilyMember
					CASE FM_MICHAEL_SON 		ePart1 = TPART_FHOS_M_SON BREAK
					CASE FM_MICHAEL_DAUGHTER 	ePart1 = TPART_FHOS_M_DAU BREAK
					CASE FM_MICHAEL_WIFE 		ePart1 = TPART_FHOS_M_WIF BREAK
//					CASE FM_MICHAEL_MEXMAID 	ePart1 = TPART_FHOS_M_MAI BREAK
//					CASE FM_MICHAEL_GARDENER 	ePart1 = TPART_FHOS_M_GAR BREAK
					
					CASE FM_FRANKLIN_AUNT 		ePart1 = TPART_FHOS_F_AUN BREAK
					
					CASE FM_TREVOR_0_RON 		ePart1 = TPART_FHOS_T_RON BREAK
					CASE FM_TREVOR_0_WIFE 		ePart1 = TPART_FHOS_T_WIF BREAK
					CASE FM_TREVOR_1_FLOYD 		ePart1 = TPART_FHOS_T_FLO BREAK

				ENDSWITCH
				
				IF (eCharID != NO_CHARACTER)	//#1572458
				AND (ePart1 != TPART_NONE)		//#1574398
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedIndex, PLAYER_PED_ID())
					
					CC_CodeID eHospitalCID = Private_GetHospitalChargeCID(pedIndex)
					IF eHospitalCID != CID_BLANK
						
						
//						IF REGISTERED_FRIEND_TEXT_MESSAGE_TO_PLAYER(
//								GET_CURRENT_PLAYER_PED_ENUM(),
//								eCharID, FTM_FRIEND_HOSPITAL,
//								FALSE, eHospitalCID)
//						ENDIF
						
						IF NOT IS_TEXT_MESSAGE_REGISTERED(TEXT_FAM_HOSPITALBILL)
							Store_Vector_ID_Data(VID_DYNAMIC_FRIEND_HOSPITAL, GET_ENTITY_COORDS(pedIndex, FALSE), 250.0)
							IF REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(TEXT_FAM_HOSPITALBILL,
									ePart1, TPART_NONE,
									CT_AMBIENT, GET_CURRENT_PLAYER_PED_BIT(), eCharID, 60000,
									DEFAULT, VID_DYNAMIC_FRIEND_HOSPITAL, eHospitalCID, DEFAULT, COMM_FLAG_DONT_SAVE| COMM_FLAG_TXTMSG_NO_SILENT)
							
								Update_Previous_Event_For_FamilyMember(eFamilyMember)
								g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
								
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS injured [player]!!!")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ENDIF
						
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] is injured, wait for comms...")
					#ENDIF
				ELSE
					
					Update_Previous_Event_For_FamilyMember(eFamilyMember)
					g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS injured [not player]???")
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF (eCharID <> NO_CHARACTER)
			g_iCharWaitTime[eCharID] = GET_GAME_TIMER() + CC_GLOBAL_DELAY_BETWEEN_COMMS
		ENDIF
		
//		fireTimer = fireTimer
//		IF IS_ENTITY_ON_FIRE(pedIndex)
//			STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(pedIndex, FALSE), 4.0)
//			RESTART_TIMER_NOW(fireTimer)
//		ENDIF
		
		FLOAT		fDISTANCE_TO_CLEAR_FAMILY 	= (fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT*1.25)
		
		FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedIndex, FALSE))
		IF fDist2 > (fDISTANCE_TO_CLEAR_FAMILY*fDISTANCE_TO_CLEAR_FAMILY)
			IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
				Update_Previous_Event_For_FamilyMember(eFamilyMember)
				g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS distance")
				#ENDIF
			ENDIF
		ENDIF
		IF fDist2 < (10*10)
		AND NOT ARE_PEDS_IN_THE_SAME_VEHICLE(pedIndex, PLAYER_PED_ID())
			IF (g_eCurrentFamilyEvent[eFamilyMember] <> FE_ANY_wander_family_event)
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 1)
					
					
					IF eCharID = CHAR_TREVOR
						INT iLegsDrawable = GET_PED_DRAWABLE_VARIATION(pedIndex, PED_COMP_LEG)
						IF (iLegsDrawable = 25)
							CASSERTLN(DEBUG_FAMILY, "start changing Trev out of toilet outfit!!!")
							
							WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_LEGS, LEGS_P2_SWEAT_PANTS, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_FEET, FEET_P2_REDWINGS, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_SPECIAL, SPECIAL_P2_DUMMY, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedIndex, COMP_TYPE_SPECIAL2, SPECIAL2_P2_NONE, FALSE)
								WAIT(0)
							ENDWHILE
							
							CASSERTLN(DEBUG_FAMILY, "finished changing Trev out of toilet outfit!!!")
						ENDIF
					ENDIF
					
					Update_Previous_Event_For_FamilyMember(eFamilyMember)
					g_eCurrentFamilyEvent[eFamilyMember] = FE_ANY_wander_family_event
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = FE_ANY_wander_family_event wanted level")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(pedIndex)
			SET_PED_RESET_FLAG(pedIndex, PRF_CannotBeTargetedByAI, TRUE)
			SET_PED_RESET_FLAG(pedIndex, PRF_SupressGunfireEvents, TRUE)
			
			IF (GET_SCRIPT_TASK_STATUS(pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK)
			OR (GET_SCRIPT_TASK_STATUS(pedIndex, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
				FLOAT fCapsuleRadius
				IF PRIVATE_SetFamilyMemberPedCapsule(eFamilyMember, fCapsuleRadius)
					SET_PED_CAPSULE(PedIndex, fCapsuleRadius)
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> disable SET_PED_CAPSULE for [", Get_String_From_FamilyMember(eFamilyMember), "] as SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD = WAITING_TO_START_TASK")
				#ENDIF
			ENDIF
			
			iInteriorForThisMember = GET_INTERIOR_FROM_ENTITY(pedIndex)
			
			
			IF g_pScene_buddy = pedIndex
				IF (g_eCurrentFamilyEvent[eFamilyMember] <> FAMILY_MEMBER_BUSY)
					IF NOT Is_Player_Timetable_Scene_In_Progress()
						g_pScene_buddy = NULL
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> remove scene buddy for this family member [", Get_String_From_FamilyMember(eFamilyMember), "]")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IsPlayerCurrentWeaponTypeThrown()
	
	WEAPON_TYPE ePlayerWeaponType = GET_SELECTED_PED_WEAPON(PLAYER_PED_ID())
	IF (ePlayerWeaponType = WEAPONTYPE_INVALID)
		RETURN FALSE
	ENDIF
	
	WEAPON_GROUP ePlayerWeaponGroup = GET_WEAPONTYPE_GROUP(ePlayerWeaponType)
	IF (ePlayerWeaponGroup = WEAPONGROUP_THROWN)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL TimedStopFamilyFires(PED_INDEX &familyPeds[], structTimer &fireTimer)
	RETURN FALSE
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TIMER_STARTED(fireTimer)
		START_TIMER_NOW(fireTimer)	
		RETURN FALSE
	ENDIF
	
	IF TIMER_DO_ONCE_WHEN_READY(fireTimer, 1.000)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
	OR IsPlayerCurrentWeaponTypeThrown()
		INT iFires
		REPEAT COUNT_OF(familyPeds) iFires
			IF DOES_ENTITY_EXIST(familyPeds[iFires])
				
				VECTOR VecPos = GET_ENTITY_COORDS(familyPeds[iFires], FALSE)
				FLOAT fRange = 5.0
				
				STOP_FIRE_IN_RANGE(VecPos, fRange)
				CLEAR_AREA_OF_PROJECTILES(VecPos, fRange)
				
				WEAPON_TYPE WeaponType = WEAPONTYPE_STICKYBOMB
				IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(VecPos, WeaponType, fRange)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		RESTART_TIMER_NOW(fireTimer)	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC Moniter_Player_Griefing_Firing_Rockets(VECTOR VecMinCoors, VECTOR VecMaxCoors)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "Griefing_Firing_Rockets"
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	#ENDIF
	
	WEAPON_TYPE ReturnWeaponType
	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), ReturnWeaponType)
		IF (ReturnWeaponType = WEAPONTYPE_RPG)
			IF IS_PROJECTILE_TYPE_IN_AREA(VecMinCoors, VecMaxCoors, ReturnWeaponType, TRUE)
				
				#IF IS_DEBUG_BUILD
				str += (" - rpg in box")
				eHudColour = HUD_COLOUR_RED
				#ENDIF
				g_ePreviousFamilyGriefing = MPG_Firing_Rockets
//				CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Firing_Rockets")
			ELSE
				#IF IS_DEBUG_BUILD
				str += (" - rpg NOT in box")
				eHudColour = HUD_COLOUR_BLUE
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			str += (" - player doesnt have RPG equiped")
			eHudColour = HUD_COLOUR_GREEN
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str += (" - player has no weapon")
		eHudColour = HUD_COLOUR_GREEN
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(MPG_Firing_Rockets-NUM_MONITER_PLAYER_GRIEFING)), eHudColour)
	#ENDIF
ENDPROC
PROC Moniter_Player_Griefing_Creep(VECTOR VecMinCoors, VECTOR VecMaxCoors, INT &iCreep)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "Griefing_Creep"
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	#ENDIF
	
	IF IS_PED_DUCKING(PLAYER_PED_ID())
	OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), VecMinCoors, VecMaxCoors)
			
			
			IF (iCreep < 0)
				iCreep = GET_GAME_TIMER()
			ELSE
				IF (GET_GAME_TIMER() > (iCreep+10000))
					g_ePreviousFamilyGriefing = MPG_Creep
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			str += (" - bullet in box [")
			str += (GET_GAME_TIMER() - iCreep)
			str += ("]")
			eHudColour = HUD_COLOUR_RED
			#ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
			str += (" - player not in box")
			eHudColour = HUD_COLOUR_BLUE
			#ENDIF
			
			iCreep = -1
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str += (" - player not ducking")
		eHudColour = HUD_COLOUR_GREEN
		#ENDIF
		
		iCreep = -1
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(MPG_Creep-NUM_MONITER_PLAYER_GRIEFING)), eHudColour)
	#ENDIF
ENDPROC
PROC Moniter_Player_Griefing_Stole_Car(INT iPlayerBitset, VEHICLE_INDEX &familyVeh[])
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "Griefing_Stole_Car"
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	#ENDIF
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		enumCharacterList eCharID = GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehicleIndex)
		
		IF (eCharID = NO_CHARACTER)
			#IF IS_DEBUG_BUILD
			str += (" - no vehicle owner")
			eHudColour = HUD_COLOUR_BLUE
			#ENDIF
		ELIF (eCharID = GET_CURRENT_PLAYER_PED_ENUM())
			#IF IS_DEBUG_BUILD
			str += (" - current ped is owner")
			eHudColour = HUD_COLOUR_BLUE
			#ENDIF
		ELIF NOT IS_BITMASK_SET(iPlayerBitset, GET_PLAYER_PED_BIT(eCharID))
			#IF IS_DEBUG_BUILD
			str += (" - some other owner")
			eHudColour = HUD_COLOUR_BLUE
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			str += (" - stolen players car")
			eHudColour = HUD_COLOUR_RED
			#ENDIF
			
			IF GET_ENTITY_MODEL(vehicleIndex) = BAGGER
				g_ePreviousFamilyGriefing = MPG_Stole_Car_wife
	//			CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Stole_Car - franklins bike")
			ELSE
				g_ePreviousFamilyGriefing = MPG_Stole_Car_player
	//			CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Stole_Car - players")
			ENDIF
		ENDIF
		
		IF (g_ePreviousFamilyGriefing <> MPG_Stole_Car_player)
			INT iVeh
			REPEAT COUNT_OF(familyVeh) iVeh
				IF (vehicleIndex = familyVeh[iVeh])
					#IF IS_DEBUG_BUILD
					str += (" - stolen car[")
					str += (iVeh)
					str += ("]")
					eHudColour = HUD_COLOUR_RED
					#ENDIF
					
					MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleIndex)
					
					IF vehicleModel = ISSI2
						g_ePreviousFamilyGriefing = MPG_Stole_Car_daughter
//						CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Stole_Car - daughter")
					ENDIF
					IF vehicleModel = SENTINEL2
						g_ePreviousFamilyGriefing = MPG_Stole_Car_wife
//						CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Stole_Car - wife")
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str += (" - player not in vehicle")
		eHudColour = HUD_COLOUR_GREEN
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(MPG_Stole_Car_player-NUM_MONITER_PLAYER_GRIEFING)), eHudColour)
	#ENDIF
ENDPROC
PROC Moniter_Player_Griefing_Set_Fire(VECTOR VecMinCoors, VECTOR VecMaxCoors)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "Griefing_Set_Fire"
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	#ENDIF
	
	WEAPON_TYPE ReturnWeaponType
	IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), ReturnWeaponType)
		IF (ReturnWeaponType = WEAPONTYPE_MOLOTOV)
		OR (ReturnWeaponType = WEAPONTYPE_PETROLCAN)
			IF IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, VecMinCoors, VecMaxCoors)
				#IF IS_DEBUG_BUILD
				str += (" - rpg in box")
				eHudColour = HUD_COLOUR_RED
				#ENDIF
				
				g_ePreviousFamilyGriefing = MPG_Set_Fire
//				CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing_Set_Fire")
			ELSE
				#IF IS_DEBUG_BUILD
				str += (" - rpg NOT in box")
				eHudColour = HUD_COLOUR_BLUE
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			str += (" - player doesnt have RPG equiped")
			eHudColour = HUD_COLOUR_GREEN
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str += (" - player has no weapon")
		eHudColour = HUD_COLOUR_GREEN
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(MPG_Set_Fire-NUM_MONITER_PLAYER_GRIEFING)), eHudColour)
	#ENDIF
ENDPROC
PROC Moniter_Player_Griefing_Shooting_House(VECTOR VecMinCoors, VECTOR VecMaxCoors, INT &iShooting_House)
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "Griefing_Shooting_House"
	HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
	#ENDIF
	
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
		IF HAS_BULLET_IMPACTED_IN_BOX(VecMinCoors, VecMaxCoors, TRUE, TRUE)
			#IF IS_DEBUG_BUILD
			str += (" - bullet in box [")
			str += (iShooting_House)
			str += ("]")
			eHudColour = HUD_COLOUR_RED
			#ENDIF
			iShooting_House++
			
			IF (iShooting_House > 6)
				g_ePreviousFamilyGriefing = MPG_Shooting_House
			ENDIF
		ELIF IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, VecMinCoors, VecMaxCoors)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, VecMinCoors, VecMaxCoors)
		OR IS_PROJECTILE_TYPE_IN_AREA(VecMinCoors, VecMaxCoors, WEAPONTYPE_GRENADELAUNCHER, TRUE)
			#IF IS_DEBUG_BUILD
			str += (" - explosion in box [")
			str += (iShooting_House)
			str += ("]")
			eHudColour = HUD_COLOUR_RED
			#ENDIF
			
			g_ePreviousFamilyGriefing = MPG_Grenade_House
		ELSE
			#IF IS_DEBUG_BUILD
			str += (" - bullet NOT in box [")
			str += (iShooting_House)
			str += ("]")
			eHudColour = HUD_COLOUR_BLUE
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str += (" - player not shooting [")
		str += (iShooting_House)
		str += ("]")
		eHudColour = HUD_COLOUR_GREEN
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(MPG_Shooting_House-NUM_MONITER_PLAYER_GRIEFING)), eHudColour)
	#ENDIF
ENDPROC
FUNC BOOL Moniter_Player_Griefing(INT iPlayerBitset, structFamilyGriefing &sFamilyGriefing, VECTOR VecCentre, VECTOR VecBounds, VEHICLE_INDEX &familyVeh[])
	IF IS_PED_INJURED(PLAYER_PED_ID())
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - player injured", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
//	IF (g_ePreviousFamilyGriefing <> NUM_MONITER_PLAYER_GRIEFING)
//		
//		#IF IS_DEBUG_BUILD
//		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - g_ePreviousFamilyGriefing", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
//		#ENDIF
//		
//		RETURN FALSE
//	ENDIF
	
	IF (g_iFamilyGriefingTimer <> -1)
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - g_iFamilyGriefingTimer", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - player is bitset", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	SELECTOR_SLOTS_ENUM eSelectorChar
	REPEAT NUM_OF_PLAYABLE_PEDS eSelectorChar
		enumCharacterList eCharList = GET_PLAYER_PED_ENUM_FROM_SELECTOR_SLOT(eSelectorChar)
		IF NOT IS_PED_THE_CURRENT_PLAYER_PED(eCharList)
			IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorChar])
				IF IS_BITMASK_SET(iPlayerBitset, GET_PLAYER_PED_BIT(eCharList))
					#IF IS_DEBUG_BUILD
					DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - pedID[eSelectorChar] is bitset", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_PURE_WHITE, 0.25)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	enumCharacterList ePed, eFamTxt = NO_CHARACTER
	REPEAT NUM_OF_PLAYABLE_PEDS ePed
		IF IS_BITMASK_SET(iPlayerBitset, GET_PLAYER_PED_BIT(ePed))
			eFamTxt = ePed
		ENDIF
	ENDREPEAT
	
	IF NOT IS_PLAYER_PED_PLAYABLE(eFamTxt)
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - ped isnt player char", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_PED_AVAILABLE(eFamTxt)
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - ped isnt available", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	SWITCH eFamTxt
		CASE CHAR_MICHAEL
			IF g_bMichaelHasBeenDisrupted
		
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - michael disrupted!!", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
//			IF g_bFranklinHasBeenDisrupted
//			ENDIF
		BREAK
		CASE CHAR_TREVOR
//			IF g_bTrevorHasBeenDisrupted
//			ENDIF
		BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [g_bMichaelHasBeenDisrupted]")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF NOT ARE_CHARS_FRIENDS(eFamTxt, GET_CURRENT_PLAYER_PED_ENUM())
		#IF IS_DEBUG_BUILD
		IF g_bRestoredSaveThisSession
		#ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - ped isnt a friend", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
			#ENDIF
			
			RETURN FALSE
		
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
	
	
	enumPhoneBookPresence WhichPhonebook
	SWITCH eFamTxt
		CASE CHAR_MICHAEL	WhichPhonebook = MICHAEL_BOOK BREAK
		CASE CHAR_FRANKLIN	WhichPhonebook = FRANKLIN_BOOK BREAK
		CASE CHAR_TREVOR	WhichPhonebook = TREVOR_BOOK BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [WhichPhonebook]")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF (GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(eFamTxt, ENUM_TO_INT(WhichPhonebook)) = LISTED)
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyTextWithOffset("Moniter_Player_Griefing - ped not in phonebook", GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 0-5, HUD_COLOUR_BLACK, 0.25)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF ARE_VECTORS_EQUAL(VecBounds, <<0,0,0>>)
		VecBounds = <<50,50,50>>
		#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_G)
			
			VECTOR VecMinCoors = VecCentre-VecBounds
			VECTOR VecMaxCoors = VecCentre+VecBounds
			
			#IF IS_DEBUG_BUILD
			IS_ENTITY_IN_AREA(PLAYER_PED_ID(), VecMinCoors, VecMaxCoors)
			#ENDIF
			
			WIDGET_GROUP_ID wBoundsWidget = START_WIDGET_GROUP("Moniter_Player_Griefing")
				ADD_WIDGET_VECTOR_SLIDER("VecCentre", VecCentre, -5000, 5000, 0.0)
				ADD_WIDGET_VECTOR_SLIDER("VecBounds", VecBounds, 0, 500, 0.5)
				ADD_WIDGET_VECTOR_SLIDER("VecMinCoors", VecMinCoors, -5000, 5000, 0.0)
				ADD_WIDGET_VECTOR_SLIDER("VecMaxCoors", VecMaxCoors, -5000, 5000, 0.0)
			STOP_WIDGET_GROUP()
			WAIT(0)
			
			WHILE NOT IS_PED_INJURED(PLAYER_PED_ID())
				VecMinCoors = VecCentre-VecBounds
				VecMaxCoors = VecCentre+VecBounds
				
				#IF IS_DEBUG_BUILD
				IS_ENTITY_IN_AREA(PLAYER_PED_ID(), VecMinCoors, VecMaxCoors)
				#ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_G)
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE("VecBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(VecBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					DELETE_WIDGET_GROUP(wBoundsWidget)
					
					RETURN FALSE
				ENDIF
				
				WAIT(0)
			ENDWHILE
		ENDIF
		#ENDIF
	ENDIF
	
	VECTOR VecMinCoors = VecCentre-VecBounds
	VECTOR VecMaxCoors = VecCentre+VecBounds
	
	#IF IS_DEBUG_BUILD
	IS_ENTITY_IN_AREA(PLAYER_PED_ID(), VecMinCoors, VecMaxCoors)
	#ENDIF
	
	Moniter_Player_Griefing_Firing_Rockets(VecMinCoors, VecMaxCoors)
	Moniter_Player_Griefing_Creep(VecMinCoors, VecMaxCoors, sFamilyGriefing.iCreep)
	Moniter_Player_Griefing_Stole_Car(iPlayerBitset, familyVeh)
	Moniter_Player_Griefing_Set_Fire(VecMinCoors, VecMaxCoors)
	Moniter_Player_Griefing_Shooting_House(VecMinCoors, VecMaxCoors, sFamilyGriefing.iShooting_House)
	
	IF (g_ePreviousFamilyGriefing <> NUM_MONITER_PLAYER_GRIEFING)
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = "ePreviousFamilyGriefing"
		str += ENUM_TO_INT(g_ePreviousFamilyGriefing)
		HUD_COLOURS eHudColour = HUD_COLOUR_WHITE
		
		IF (g_iFamilyGriefingTimer = -1)
			str += " NULL"
		ELSE
			str += " "
			str += (GET_GAME_TIMER() - g_iFamilyGriefingTimer)
		ENDIF
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), TO_FLOAT(ENUM_TO_INT(NUM_MONITER_PLAYER_GRIEFING)-1), eHudColour)
		#ENDIF
		
		IF (g_iFamilyGriefingTimer = -1)
			
			CC_CommID eID = COMM_NONE
			
			IF IS_BITMASK_SET(iPlayerBitset, GET_CURRENT_PLAYER_PED_BIT())
			ENDIF
			
			INT iQueueTime = 5000
			IF GET_RANDOM_BOOL()
				
				SWITCH eFamTxt
					CASE CHAR_MICHAEL	eID = TEXT_FAM_GRIEF_MICHAEL BREAK
					CASE CHAR_FRANKLIN	eID = TEXT_FAM_GRIEF_FRANKLIN BREAK
					CASE CHAR_TREVOR	eID = TEXT_FAM_GRIEF_TREVOR BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [TEXT_FAM_GRIEF]")
						RETURN FALSE
					BREAK
				ENDSWITCH
				
				INT iNumberOfRandomConvs = -1
				CC_TextPart ePart1, ePart2
				SWITCH g_ePreviousFamilyGriefing
					//																	   "A"
					CASE MPG_Firing_Rockets		ePart1 = TPART_GRIEF_FIRING_ROCKETS	iNumberOfRandomConvs = 3 BREAK
					CASE MPG_Creep				ePart1 = TPART_GRIEF_CREEP			iNumberOfRandomConvs = 3 BREAK					
					CASE MPG_Stole_Car_player	ePart1 = TPART_GRIEF_STOLE_CAR_P	iQueueTime = 10000	iNumberOfRandomConvs = 3 BREAK
					CASE MPG_Stole_Car_wife		ePart1 = TPART_GRIEF_STOLE_CAR_W	iQueueTime = 10000	iNumberOfRandomConvs = 3 BREAK
					CASE MPG_Stole_Car_daughter	ePart1 = TPART_GRIEF_STOLE_CAR_D	iQueueTime = 10000	iNumberOfRandomConvs = 3 BREAK
					CASE MPG_Set_Fire			ePart1 = TPART_GRIEF_SET_FIRE		iNumberOfRandomConvs = 3 BREAK
					//																	   "F"
					CASE MPG_Shooting_House		ePart1 = TPART_GRIEF_SHOOTING_HOUSE	iNumberOfRandomConvs = 3 BREAK
					CASE MPG_Grenade_House		ePart1 = TPART_GRIEF_GRENADE_HOUSE	iNumberOfRandomConvs = 3 BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_FAMILY, "invalid g_iFamilyGriefingTimer")
						RETURN FALSE
					BREAK
				ENDSWITCH
				
				//B* 1818748: If it's a stolen car and the griefed character isn't Michael, make sure it's only Stole_car_player that gets returned
				IF (ePart1 = TPART_GRIEF_STOLE_CAR_W OR ePart1 = TPART_GRIEF_STOLE_CAR_D) AND NOT (eID = TEXT_FAM_GRIEF_MICHAEL)
					ePart1 = TPART_GRIEF_STOLE_CAR_P
				ENDIF
								
				INT iRandomConv = GET_RANDOM_INT_IN_RANGE(0, iNumberOfRandomConvs)
				SWITCH iRandomConv
					CASE 0	ePart2 = TPART_GRIEF_ZERO	BREAK
					CASE 1	ePart2 = TPART_GRIEF_ONE	BREAK
					CASE 2	ePart2 = TPART_GRIEF_TWO	BREAK
					CASE 3	ePart2 = TPART_GRIEF_THREE	BREAK
					CASE 4	ePart2 = TPART_GRIEF_FOUR	BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_FAMILY, "invalid g_iFamilyGriefingTimer")
						RETURN FALSE
					BREAK
				ENDSWITCH
				
				IF REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(eID, ePart1, ePart2,
						CT_AMBIENT, GET_CURRENT_PLAYER_PED_BIT(), eFamTxt, iQueueTime)
					g_iFamilyGriefingTimer = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ELSE
			
				SWITCH g_ePreviousFamilyGriefing
					CASE MPG_Firing_Rockets
						SWITCH eFamTxt
							CASE CHAR_MICHAEL	eID = CALL_FAM_M_GRIEF_FIRING_ROCKETS BREAK
							CASE CHAR_FRANKLIN	eID = CALL_FAM_F_GRIEF_FIRING_ROCKETS BREAK
							CASE CHAR_TREVOR	eID = CALL_FAM_T_GRIEF_FIRING_ROCKETS BREAK
							
							DEFAULT
								CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [g_ePreviousFamilyGriefing]")
								RETURN FALSE
							BREAK
						ENDSWITCH
						
					BREAK
					CASE MPG_Stole_Car_player
						SWITCH eFamTxt
							CASE CHAR_MICHAEL	eID = CALL_FAM_M_GRIEF_STOLE_CAR BREAK
							CASE CHAR_FRANKLIN	eID = CALL_FAM_F_GRIEF_STOLE_CAR_c BREAK
							CASE CHAR_TREVOR	eID = CALL_FAM_T_GRIEF_STOLE_CAR BREAK
							
							DEFAULT
								CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [MPG_Stole_Car_player]")
								RETURN FALSE
							BREAK
						ENDSWITCH
						iQueueTime = 10000
					BREAK
					CASE MPG_Stole_Car_wife
						SWITCH eFamTxt
							CASE CHAR_FRANKLIN	eID = CALL_FAM_F_GRIEF_STOLE_CAR_b BREAK
							
							DEFAULT		RETURN FALSE	BREAK
						ENDSWITCH
					BREAK
					CASE MPG_Shooting_House
						SWITCH eFamTxt
							CASE CHAR_MICHAEL	eID = CALL_FAM_M_GRIEF_SHOOTING_HOUSE BREAK
							CASE CHAR_FRANKLIN	eID = CALL_FAM_F_GRIEF_SHOOTING_HOUSE BREAK
							CASE CHAR_TREVOR	eID = CALL_FAM_T_GRIEF_SHOOTING_HOUSE BREAK
							
							DEFAULT
								CASSERTLN(DEBUG_FAMILY, "Moniter_Player_Griefing:invalid eFamTxt - [MPG_Shooting_House]")
								RETURN FALSE
							BREAK
						ENDSWITCH
					BREAK
					
					CASE MPG_Creep
					CASE MPG_Set_Fire
					CASE MPG_Grenade_House
				//	CASE MPG_Stole_Car_wife
					CASE MPG_Stole_Car_daughter
						RETURN FALSE
					BREAK
					
					DEFAULT
						CASSERTLN(DEBUG_FAMILY, "invalid g_iFamilyGriefingTimer")
						RETURN FALSE
					BREAK
				ENDSWITCH
				
//				GetRandomTextMessageFromLabel(sFamTxt)
				
				IF REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(eID, CT_AMBIENT, 
						GET_CURRENT_PLAYER_PED_BIT(), eFamTxt, ENUM_TO_INT(eFamTxt), iQueueTime, 10000)
					g_iFamilyGriefingTimer = GET_GAME_TIMER()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC WIDGET_GROUP_ID Create_Family_Scene_Widget_Group(ENUM_TO_INT MAX_SCENE_MEMBERS, STRING &sFamilyMemberName[],
//		INT &currentFamilyEvent[],
		TEXT_WIDGET_ID &familyAnimWidget[], TEXT_WIDGET_ID &familyAnimFlagWidget[], 
		VECTOR &vecFamilyCoordOffset[], FLOAT &fFamilyHeadOffset[],
		BOOL &jumpToFamilyMember[],
		FLOAT &fDebugJumpAngle[],FLOAT &fDebugJumpDistance[],FLOAT &fFamilySynchScenePhase[],
		BOOL &bMovePeds, BOOL &bDrawPeds, BOOL &bSavePeds,
		BOOL &bEditPedSpeechBounds)
	WIDGET_GROUP_ID family_scene_widget = START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())
		ADD_WIDGET_BOOL("g_bDrawDebugFamilyStuff", g_bDrawDebugFamilyStuff)
		
		INT iWidget
		REPEAT MAX_SCENE_MEMBERS iWidget
			START_WIDGET_GROUP(sFamilyMemberName[iWidget])
//				START_NEW_WIDGET_COMBO()
//					INT iCombo
//					REPEAT COUNT_OF(enumFamilyEvents) iCombo
//						ADD_TO_WIDGET_COMBO(Get_String_From_FamilyEvent(INT_TO_ENUM(enumFamilyEvents, iCombo)))
//					ENDREPEAT
//				STOP_WIDGET_COMBO("currentFamilyEvent", currentFamilyEvent[iWidget])
				
				familyAnimWidget[iWidget] = ADD_TEXT_WIDGET("anim")
				TEXT_LABEL_63 sAnimWidget = "tFamilyAnimDict"
				sAnimWidget += ", "
				sAnimWidget += "tFamilyAnim"
				SET_CONTENTS_OF_TEXT_WIDGET(familyAnimWidget[iWidget], sAnimWidget)
				
				familyAnimFlagWidget[iWidget] = ADD_TEXT_WIDGET("animFlag")
				TEXT_LABEL_63 sAnimFlagWidget = "eFamilyAnimFlag"
				SET_CONTENTS_OF_TEXT_WIDGET(familyAnimFlagWidget[iWidget], sAnimFlagWidget)
				
				IF fFamilyHeadOffset[iWidget] < 0.0
					fFamilyHeadOffset[iWidget] += 360.0
				ENDIF
				
				IF NOT g_bMagDemoActive
					ADD_WIDGET_FLOAT_SLIDER("vecFamilyCoordOffset x", vecFamilyCoordOffset[iWidget].x, -50.0, 50.0, 0.001)
				    ADD_WIDGET_FLOAT_SLIDER("vecFamilyCoordOffset y", vecFamilyCoordOffset[iWidget].y, -50.0, 50.0, 0.001)
				    ADD_WIDGET_FLOAT_SLIDER("vecFamilyCoordOffset z", vecFamilyCoordOffset[iWidget].z, -50.0, 50.0, 0.0001)
					
					ADD_WIDGET_FLOAT_SLIDER("fFamilyHeadOffset", fFamilyHeadOffset[iWidget], 0.0, 360.0, 1.0)
				ELSE
					ADD_WIDGET_VECTOR_SLIDER("vecFamilyCoordOffset",
							vecFamilyCoordOffset[iWidget],
							-20.0, 20.0,
							0.001)
					ADD_WIDGET_FLOAT_SLIDER("fFamilyHeadOffset",
							fFamilyHeadOffset[iWidget],
							fFamilyHeadOffset[iWidget] - 10.0,
							fFamilyHeadOffset[iWidget] + 10.0,
							0.01)
				ENDIF
				
				ADD_WIDGET_BOOL("jumpToFamilyMember", jumpToFamilyMember[iWidget])
				ADD_WIDGET_FLOAT_SLIDER("fDebugJumpAngle", fDebugJumpAngle[iWidget], 0.0, 360.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("fDebugJumpDistance", fDebugJumpDistance[iWidget], -60.0, 60.0, 0.1)
				
				
				ADD_WIDGET_STRING("synch scene")
				ADD_WIDGET_FLOAT_SLIDER("fFamilySynchScenePhase", fFamilySynchScenePhase[iWidget], -0.0, 1.0, 0.01)
				
				
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
		ADD_WIDGET_BOOL("bMovePeds", bMovePeds)
		ADD_WIDGET_BOOL("bDrawPeds", bDrawPeds)
		ADD_WIDGET_BOOL("bSavePeds", bSavePeds)
		ADD_WIDGET_BOOL("bEditPedSpeechBounds", bEditPedSpeechBounds)
		
	STOP_WIDGET_GROUP()
	
	RETURN family_scene_widget
ENDFUNC

PROC Update_Family_Member_Widget(enumFamilyMember eFamilyMember,
		TEXT_WIDGET_ID &familyAnimWidget, TEXT_WIDGET_ID &familyAnimFlagWidget,
		VECTOR &vecFamilyCoordOffset, FLOAT &fFamilyHeadOffset,
		FLOAT &fDebugJumpAngle,FLOAT &fDebugJumpDistance,
		PTFX_ID &ptfxID, INT &iSfxStage, INT &iSfxID, TEXT_LABEL_63 &RequestedBankName, structTimer &speechTimer, INT &iSpeechTimer,
		OBJECT_INDEX &familyProp)
	
	IF DOES_ENTITY_EXIST(familyProp)
		IF (GET_ENTITY_MODEL(familyProp) <> PROP_BONG_01)
			REMOVE_MODEL_HIDE(GET_ENTITY_COORDS(familyProp), 1.5, GET_ENTITY_MODEL(familyProp), FALSE)
		ENDIF
		
		DELETE_OBJECT(familyProp)
	ENDIF
	SET_OBJECT_AS_NO_LONGER_NEEDED(familyProp)
	
	PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember],
			vecFamilyCoordOffset, fFamilyHeadOffset)
	
	TEXT_LABEL_63 tFamilyAnimDict, tFamilyAnim
	ANIMATION_FLAGS eFamilyAnimFlag
	enumFamilyAnimProgress eFamilyAnimProgress
	PRIVATE_Get_FamilyMember_Anim(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember],
			tFamilyAnimDict, tFamilyAnim, eFamilyAnimFlag, eFamilyAnimProgress)
	
//	IF NOT IS_ENTITY_DEAD(family_veh[MAX_SCENE_VEHICLES])
//		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), family_veh[MAX_SCENE_VEHICLES])
//			VECTOR vfamily_veh[MAX_SCENE_VEHICLES]_coord = GET_ENTITY_COORDS(family_veh[MAX_SCENE_VEHICLES])
//			FLOAT ffamily_veh[MAX_SCENE_VEHICLES]_head = GET_ENTITY_HEADING(family_veh[MAX_SCENE_VEHICLES])
//			
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), vfamily_veh[MAX_SCENE_VEHICLES]_coord)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), ffamily_veh[MAX_SCENE_VEHICLES]_head)
//		ENDIF
//		
//		DELETE_VEHICLE(family_veh[MAX_SCENE_VEHICLES])
//	ENDIF
	
	IF NOT (IS_STRING_NULL(tFamilyAnimDict) OR ARE_STRINGS_EQUAL(tFamilyAnimDict, ""))
		
		TEXT_LABEL_63 sAnimWidget
		CONST_INT iANIM_WIDGET_LENGTH	63
		
		INT iFamilyAnimDict = GET_LENGTH_OF_LITERAL_STRING(tFamilyAnimDict)
		INT iFamilyAnimSep = GET_LENGTH_OF_LITERAL_STRING(", ")
		INT iFamilyAnim = GET_LENGTH_OF_LITERAL_STRING(tFamilyAnim)
		
		CPRINTLN(DEBUG_FAMILY, "length of \"", tFamilyAnimDict, "\": ", iFamilyAnimDict)
		CPRINTLN(DEBUG_FAMILY, "length of \"", ", ", "\": ", iFamilyAnimSep)
		CPRINTLN(DEBUG_FAMILY, "length of \"", tFamilyAnim, "\": ", iFamilyAnim)
		CPRINTLN(DEBUG_FAMILY, "length of \"", tFamilyAnimDict, ", ", tFamilyAnim, "\": ", iFamilyAnimDict+iFamilyAnimSep+iFamilyAnim)
		CPRINTLN(DEBUG_FAMILY, "")
		
		IF (iFamilyAnimDict+iFamilyAnimSep+iFamilyAnim) <= iANIM_WIDGET_LENGTH
			sAnimWidget  = tFamilyAnimDict
		ELSE
			sAnimWidget  = GET_STRING_FROM_STRING(tFamilyAnimDict,
					(iFamilyAnimDict+iFamilyAnimSep+iFamilyAnim) - iANIM_WIDGET_LENGTH,
					GET_LENGTH_OF_LITERAL_STRING(tFamilyAnimDict))
		ENDIF
		
		sAnimWidget += ", "
		sAnimWidget += tFamilyAnim
		SET_CONTENTS_OF_TEXT_WIDGET(familyAnimWidget, sAnimWidget)
	ELSE
		SET_CONTENTS_OF_TEXT_WIDGET(familyAnimWidget, "NULL")
	ENDIF
	
	IF (eFamilyAnimFlag >= AF_DEFAULT)
		TEXT_LABEL_63 sAnimFlagWidget = ""
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_DEFAULT)
			sAnimFlagWidget += "default, "
		ENDIF
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_LOOPING)
			sAnimFlagWidget += "looping, "
		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_NOT_INTERRUPTABLE)
			sAnimFlagWidget += "nonint, "
		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_OVERRIDE_PHYSICS)
			sAnimFlagWidget += "ov-phys, "
		ENDIF
//		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_DISABLE_LEG_IK)
//			sAnimFlagWidget += "leg ik, "
//		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_EXTRACT_INITIAL_OFFSET)
			sAnimFlagWidget += "ext_off, "
		ENDIF 
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_TURN_OFF_COLLISION)
			sAnimFlagWidget += "coll_off, "
		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_USE_KINEMATIC_PHYSICS)
			sAnimFlagWidget += "kine_phys, "
		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_HOLD_LAST_FRAME)
			sAnimFlagWidget += "hold_last, "
		ENDIF
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_IGNORE_GRAVITY)
			sAnimFlagWidget += "no_grav, "
		ENDIF
		
		SET_CONTENTS_OF_TEXT_WIDGET(familyAnimFlagWidget, sAnimFlagWidget)
	ELSE
		SET_CONTENTS_OF_TEXT_WIDGET(familyAnimFlagWidget, "NULL")
	ENDIF
	
	VECTOR vFM_TREVOR_1_FLOYD
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember],	vFM_TREVOR_1_FLOYD)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_1_FLOYD, fDebugJumpAngle, fDebugJumpDistance)
	
	IF (ptfxID <> NULL)
		REMOVE_PARTICLE_FX(ptfxID)
		ptfxID = NULL
	ENDIF
	
	PRIVATE_Cleanup_Family_Stream_And_Sfx(iSfxStage, iSfxID, RequestedBankName)
	PRIVATE_Cleanup_Family_Doors()
	PRIVATE_Cleanup_Tv_Globals()
	
	KILL_ANY_CONVERSATION()
	CANCEL_TIMER(speechTimer)
	iSpeechTimer = 0
	
	WAIT(0)
	
ENDPROC

#ENDIF
