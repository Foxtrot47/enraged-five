///private header for family sfx control scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "commands_audio.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"

// *******************************************************************************************
//	FAMILY CONST_VALUES
// *******************************************************************************************
CONST_INT	iCONST_FAMILY_SFX_INVALID		-1

ENUM enumFamilySfxEntityType
	FSE_0_ped = 0,
	FSE_1_coord,
	MAX_FAMILY_SFX_ENTITY_TYPES
ENDENUM

// *******************************************************************************************
//	FAMILY STREAM PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC BOOL PRIVATE_Play_Family_Stream(INT &iSfxStage, STRING streamName, VECTOR position)
	
	SWITCH iSfxStage
		CASE 0		// load and play stream...
			IF LOAD_STREAM(streamName)
				
				IF ARE_STRINGS_EQUAL(streamName, "AFT_SON_PORN")
					SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
				ENDIF
				
				PLAY_STREAM_FROM_POSITION(position)
				iSfxStage = 1
			ENDIF
		BREAK
		CASE 1		// playing stream at position...
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str = "sfx \""
			str += streamName
			str += "\""
			DrawDebugFamilyTextWithOffset(str, position, 0, HUD_COLOUR_BLUEDARK)
			DrawDebugFamilySphere(position, 0.1, HUD_COLOUR_BLUEDARK)
			#ENDIF
			
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_FAMILY, "invalid iSfxStage ", iSfxStage, " in PRIVATE_Play_Family_Stream()")
			
			CASSERTLN(DEBUG_FAMILY, "invalid iSfxStage in PRIVATE_Play_Family_Stream()")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//FUNC BOOL PRIVATE_Update_Family_Stream(enumFamilyEvents eFamilyEvent, INT &iSfxStage,
//		STRING streamName, VECTOR position)
//	SWITCH eFamilyEvent
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside		
//			IF PRIVATE_Play_Family_Stream(iSfxStage, streamName, position)
//				RETURN TRUE
//			ENDIF
//		BREAK
//		
//	ENDSWITCH
//	
//	#IF IS_DEBUG_BUILD
//	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_Stream()")
//	#ENDIF
//	
//	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent PRIVATE_Update_Family_Stream()")
//	RETURN FALSE
//ENDFUNC


// *******************************************************************************************
//	FAMILY SFX PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC BOOL PRIVATE_Play_Family_SFX_On_Entity(INT &iSfxStage, INT &iSfxID,
		STRING BankName, STRING SFXName, ENTITY_INDEX entityID,
		enumFamilySfxEntityType eFamilySfxEntityType, TEXT_LABEL_63 &RequestedBankName)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str, strSFXName
	INT iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(SFXName)
	
	IF iTaunterAnimOutLength >= 8
		strSFXName = GET_STRING_FROM_STRING(SFXName,
				iTaunterAnimOutLength - 8,
				iTaunterAnimOutLength)
	ELSE
		strSFXName = SFXName
	ENDIF

	
	#ENDIF
	
	CONST_INT iSFX_0_loadStream			0
	CONST_INT iSFX_1_playSound			1
	CONST_INT iSFX_2_playingSound		2
	CONST_INT iSFX_3_pausedSound		3
	CONST_INT iSFX_4_bankLoaded			4
	
	SWITCH iSfxStage
		CASE iSFX_0_loadStream		// load stream...
			
			//url:bugstar:677528
			IF (iSfxID <> iCONST_FAMILY_SFX_INVALID)
				STOP_SOUND(iSfxID)
				RELEASE_SOUND_ID(iSfxID)
				
				iSfxID = iCONST_FAMILY_SFX_INVALID
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(BankName)
				IF REQUEST_AMBIENT_AUDIO_BANK(BankName)
					RequestedBankName = BankName
					iSfxStage = iSFX_1_playSound
				ENDIF
			ELSE
				iSfxStage = iSFX_1_playSound
			ENDIF
		BREAK
		CASE iSFX_1_playSound		// play stream...
			IF NOT DOES_ENTITY_EXIST(entityID)
				
				IF IS_STRING_NULL_OR_EMPTY(SFXName)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Play_Family_SFX_On_Entity - ", "entity doesnt exist \"", SFXName, "\" and SFXName is empty")
					#ENDIF
					
					iSfxStage = iSFX_4_bankLoaded
					RETURN FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Play_Family_SFX_On_Entity - ", "entity doesnt exist \"", SFXName, "\"")
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			SWITCH eFamilySfxEntityType
				CASE FSE_0_ped
					iSfxID = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iSfxID, SFXName, entityID)
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "PLAY_SOUND_FROM_ENTITY(", iSfxID, ", \"", SFXName, "\", ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityID)), ")", "	//BankName:\"", BankName, "\"")
					#ENDIF
					
				BREAK
				CASE FSE_1_coord
					iSfxID = GET_SOUND_ID()
					PLAY_SOUND_FROM_COORD(iSfxID, SFXName, GET_ENTITY_COORDS(entityID))
					
					#IF IS_DEBUG_BUILD
					VECTOR vEntityCoord
					vEntityCoord = GET_ENTITY_COORDS(entityID)
					CPRINTLN(DEBUG_FAMILY, "PLAY_SOUND_FROM_COORD(", iSfxID, ", \"", SFXName, "\", ", vEntityCoord, ")", "	//BankName:\"", BankName, "\"")
					#ENDIF
					
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "PRIVATE_Play_Family_SFX_On_Entity - ", " invalid eFamilySfxEntityType FSE_", ENUM_TO_INT(eFamilySfxEntityType), "_unknown")
					#ENDIF
					
					RETURN FALSE
				BREAK
					

			ENDSWITCH
			
			iSfxStage = iSFX_2_playingSound
		BREAK
		CASE iSFX_2_playingSound		// play stream at entity...
			
			#IF IS_DEBUG_BUILD
			str = "sfx \""
			str += strSFXName
			str += "\""
			#ENDIF
			
			IF (iSfxID <> iCONST_FAMILY_SFX_INVALID)
				IF NOT HAS_SOUND_FINISHED(iSfxID)
					#IF IS_DEBUG_BUILD
					str += " playing"
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str += " FINISHED"
					#ENDIF
					
					iSfxStage = iSFX_0_loadStream
					RETURN TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				str += " INVALID?"
				#ENDIF
				
				iSfxStage = iSFX_0_loadStream
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityID), 3, HUD_COLOUR_BLUEDARK)
			#ENDIF
			
			IF (eFamilySfxEntityType <> FSE_1_coord)
				IF IS_ENTITY_A_PED(entityID)
					PED_INDEX pedID
					pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(entityID)
					IF IS_ANY_SPEECH_PLAYING(pedID)
						iSfxStage = iSFX_3_pausedSound
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE iSFX_3_pausedSound		// ped is playing a line of dialogie, stop speech until ped has finished talking...
			
			#IF IS_DEBUG_BUILD
			str = "sfx \""
			str += strSFXName
			str += "\""
			str += " paused"
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityID), 3, HUD_COLOUR_BLUELIGHT)
			#ENDIF
			
			IF IS_ENTITY_A_PED(entityID)
				PED_INDEX pedID
				pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(entityID)
				IF NOT IS_ANY_SPEECH_PLAYING(pedID)
					iSfxStage = iSFX_0_loadStream
				ENDIF
			ENDIF
		BREAK
		CASE iSFX_4_bankLoaded
			
//			#IF IS_DEBUG_BUILD
//			str = "bank \""
//			str += BankName
//			str += "\""
//			str += " loaded"
//			
//			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityID), 3, HUD_COLOUR_BLUELIGHT)
//			#ENDIF
			
//			IF IS_ENTITY_A_PED(entityID)
//				PED_INDEX pedID
//				pedID = GET_PED_INDEX_FROM_ENTITY_INDEX(entityID)
//				IF NOT IS_ANY_SPEECH_PLAYING(pedID)
//					iSfxStage = iSFX_0_loadStream
//				ENDIF
//			ENDIF
		BREAK
		
		
		DEFAULT
			CPRINTLN(DEBUG_FAMILY, "invalid iSfxStage ", iSfxStage, " in PRIVATE_Play_Family_SFX_On_Entity()")
			CASSERTLN(DEBUG_FAMILY, "invalid iSfxStage ", iSfxStage, " in PRIVATE_Play_Family_SFX_On_Entity()")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

//
FUNC FLOAT PRIVATE_Get_Variable_For_Player_In_Room(enumFamilyEvents eFamilyEvent,
		INTERIOR_INSTANCE_INDEX iInteriorForThisPlayer,
		INTERIOR_INSTANCE_INDEX iInteriorForThisFamilyMember)
	
	SWITCH eFamilyEvent
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M_WIFE_using_vibrator_END
		CASE FE_M7_WIFE_using_vibrator
		#ENDIF
		#IF IS_JAPANESE_BUILD
		CASE NO_FAMILY_EVENTS
		#ENDIF
			// I've added a variable to this sound - if you pass a value to MichaelInRoom
			// (0 when he's not, 1 when he is) to the sound using
			
			// SET_VARIABLE_ON_SOUND(SoundID,"MichaelInRoom", FloatValue)
			
			// it will make it sound appropriately occluded when necessary.
			// It defaults to 0.
			
			IF (iInteriorForThisPlayer = iInteriorForThisFamilyMember)
				RETURN 1.0
			ENDIF
			
			RETURN 0.0
		BREAK
	ENDSWITCH
	
	RETURN -1.0
ENDFUNC

FUNC BOOL PRIVATE_Set_Variable_On_Family_Sound(enumFamilyEvents eFamilyEvent, INT iSfxID, FLOAT VariableValue = 0.0)
	
	IF NOT HAS_SOUND_FINISHED(iSfxID)
	
		SWITCH eFamilyEvent
			#IF NOT IS_JAPANESE_BUILD
			CASE FE_M2_WIFE_using_vibrator
//			CASE FE_M_WIFE_using_vibrator_END
			CASE FE_M7_WIFE_using_vibrator
				
				// I've added a variable to this sound - if you pass a value to MichaelInRoom
				// (0 when he's not, 1 when he is) to the sound using
				
				// SET_VARIABLE_ON_SOUND(SoundID,"MichaelInRoom", FloatValue)
				
				// it will make it sound appropriately occluded when necessary.
				// It defaults to 0.
				
				SET_VARIABLE_ON_SOUND(iSfxID, "MichaelInRoom", VariableValue)
				RETURN TRUE
			BREAK
			#ENDIF
			CASE FE_M_GARDENER_with_leaf_blower
				
				//	I made sounds for a bit back - if you load the bank:
				//	 GARDEN_LEAF_BLOWER
				//	you can then trigger the sound
				//	 Leaf_Blower
				//	from the SoundSet
				//	 MICHAELS_GARDEN_SOUNDS
				//
				//	To control the blowing set a variable called "Blow" on the sound
				//	- values range from "0" (idling) to "-1" (blowing).
				
				SET_VARIABLE_ON_SOUND(iSfxID, "Blow", VariableValue)
				RETURN TRUE
			BREAK
		ENDSWITCH
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_Sfx(enumFamilyEvents eFamilyEvent, INT &iSfxStage, INT &iSfxID,
		STRING BankName, STRING SFXName, ENTITY_INDEX entityID, TEXT_LABEL_63 &RequestedBankName, FLOAT VariableValue = 0.0)
	SWITCH eFamilyEvent
		
		CASE FE_M_FAMILY_on_laptops					FALLTHRU
		
		CASE FE_M2_SON_gaming_loop					FALLTHRU
//		CASE FE_M2_SON_gaming_exit					FALLTHRU
		CASE FE_M7_SON_gaming						FALLTHRU
		CASE FE_M7_SON_on_laptop_looking_for_jobs	FALLTHRU
		CASE FE_M_SON_smoking_weed_in_a_bong		FALLTHRU
		
		CASE FE_M_DAUGHTER_purges_in_the_bathroom	FALLTHRU
		CASE FE_M_DAUGHTER_sex_sounds_from_room		FALLTHRU
		CASE FE_M_DAUGHTER_crying_over_a_guy		FALLTHRU
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet	FALLTHRU
		CASE FE_M_DAUGHTER_dancing_practice			FALLTHRU
		CASE FE_M7_DAUGHTER_studying_does_nails		FALLTHRU
		
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator				FALLTHRU
//		CASE FE_M_WIFE_using_vibrator_END			FALLTHRU
		CASE FE_M7_WIFE_using_vibrator				FALLTHRU
		#ENDIF
		CASE FE_M_WIFE_gets_drink_in_kitchen		FALLTHRU
		CASE FE_M2_WIFE_with_shopping_bags_enter	FALLTHRU
		CASE FE_M7_WIFE_with_shopping_bags_enter	FALLTHRU
		
		CASE FE_M_MEXMAID_does_the_dishes			FALLTHRU
		CASE FE_M2_MEXMAID_clean_surface_a			FALLTHRU
		CASE FE_M2_MEXMAID_clean_surface_b			FALLTHRU
		CASE FE_M2_MEXMAID_clean_surface_c			FALLTHRU
		CASE FE_M7_MEXMAID_clean_surface			FALLTHRU
		
		CASE FE_M2_MEXMAID_clean_window				FALLTHRU
		CASE FE_M7_MEXMAID_clean_window				FALLTHRU
		CASE FE_M_MEXMAID_MIC4_clean_window			FALLTHRU
		
		CASE FE_M_GARDENER_with_leaf_blower			FALLTHRU
//		CASE FE_M_GARDENER_trimming_hedges			FALLTHRU
		CASE FE_M_GARDENER_cleaning_pool			FALLTHRU
		CASE FE_M_GARDENER_mowing_lawn				FALLTHRU
		CASE FE_M_GARDENER_watering_flowers			FALLTHRU
//		CASE FE_M_GARDENER_spraying_for_weeds		FALLTHRU
		
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside		FALLTHRU
		
		CASE FE_T0_RON_smoking_crystal				FALLTHRU
		CASE FE_T0_RON_drinks_moonshine_from_a_jar	FALLTHRU
		CASE FE_T0_RON_listens_to_radio_broadcast	FALLTHRU
		CASE FE_T0_RON_monitoring_police_frequency	FALLTHRU
		CASE FE_T0_RONEX_trying_to_pick_up_signals	FALLTHRU
		CASE FE_T0_RONEX_working_a_moonshine_sill	FALLTHRU
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit				FALLTHRU
		#ENDIF
		CASE FE_T0_TREVOR_blowing_shit_up			FALLTHRU
		CASE FE_T0_TREVOR_smoking_crystal			FALLTHRU
		CASE FE_T0_MICHAEL_drinking_beer			FALLTHRU
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work	FALLTHRU
		CASE FE_T0_KIDNAPPED_WIFE_cleaning			FALLTHRU
		
		CASE FE_T1_FLOYD_cleaning					FALLTHRU
		CASE FE_T1_FLOYD_with_wade_post_docks1
			IF PRIVATE_Play_Family_SFX_On_Entity(iSfxStage, iSfxID, BankName, SFXName, entityID, FSE_0_ped, RequestedBankName)
				PRIVATE_Set_Variable_On_Family_Sound(eFamilyEvent, iSfxID, VariableValue)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
		
		CASE FE_M_SON_rapping_in_the_shower			FALLTHRU
		CASE FE_M_DAUGHTER_shower
			IF PRIVATE_Play_Family_SFX_On_Entity(iSfxStage, iSfxID, BankName, SFXName, entityID, FSE_1_coord, RequestedBankName)
				PRIVATE_Set_Variable_On_Family_Sound(eFamilyEvent, iSfxID, VariableValue)
				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	
	TEXT_LABEL_63 str
	str += ("invalid eFamilyEvent ")
	INT iTaunterAnimOutLength
	iTaunterAnimOutLength = GET_LENGTH_OF_LITERAL_STRING(Get_String_From_FamilyEvent(eFamilyEvent))
	
	IF iTaunterAnimOutLength >= 12
		str += GET_STRING_FROM_STRING(Get_String_From_FamilyEvent(eFamilyEvent),
				iTaunterAnimOutLength - 12,
				iTaunterAnimOutLength)
	ELSE
		str += Get_String_From_FamilyEvent(eFamilyEvent)
	ENDIF
	str += (" in Update_Family_Sfx()")
	
	CASSERTLN(DEBUG_FAMILY, str)
	#ENDIF
	
	RETURN PRIVATE_Play_Family_SFX_On_Entity(iSfxStage, iSfxID, BankName, SFXName, entityID, FSE_0_ped, RequestedBankName)
ENDFUNC

// *******************************************************************************************
//	FAMILY STREAM & SFX CLEANUP PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC BOOL PRIVATE_Cleanup_Family_Stream_And_Sfx(INT &iSfxStage, INT &iSfxID, TEXT_LABEL_63 &RequestedBankName)
	STOP_STREAM()
	
	IF (iSfxStage = 0)
	AND (iSfxID = iCONST_FAMILY_SFX_INVALID)
	AND IS_STRING_NULL_OR_EMPTY(RequestedBankName)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		

	IF (iSfxID <> iCONST_FAMILY_SFX_INVALID)
		IF (iSfxStage <> 0)
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Family_Stream_And_Sfx(", " iSfxStage = ", iSfxStage, " iSfxID = ", iSfxID, ")")
		ELSE
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Family_Stream_And_Sfx(", " iSfxID = ", iSfxID, ")")
		ENDIF
	ELSE
		IF (iSfxStage <> 0)
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Family_Stream_And_Sfx(", " iSfxStage = ", iSfxStage, ")")
		ELSE
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Family_Stream_And_Sfx(", ")")
		ENDIF
	ENDIF
	#ENDIF
	
	IF (iSfxStage <> 0)
		STOP_STREAM()		//cleanup PRIVATE_Play_Family_Stream()
		iSfxStage = 0
	ENDIF
	IF (iSfxID <> iCONST_FAMILY_SFX_INVALID)
		STOP_SOUND(iSfxID)
		iSfxID = iCONST_FAMILY_SFX_INVALID
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(RequestedBankName)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "RELEASE_NAMED_SCRIPT_AUDIO_BANK(\"", RequestedBankName, "\")")
		#ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(RequestedBankName)
		
		IF ARE_STRINGS_EQUAL(RequestedBankName, "AFT_SON_PORN")
			SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
		ENDIF
		
	ELSE
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "bul(\"", RequestedBankName, "\")")
		#ENDIF
		
		
	ENDIF
	RequestedBankName = ""
	
	RETURN TRUE
ENDFUNC
