
USING "pb_prostitute_ai.sch"

#IF IS_PIMP_AVAILABLE


FUNC MODEL_NAMES GET_PIMP_PILE_OF_CASH_MODEL()
	RETURN Prop_Anim_Cash_Pile_01
ENDFUNC

PROC PROSTITUTE_CREATE_PIMP_SYNC_SCENE(INT &iSceneID, VECTOR scenePosition, VECTOR sceneOrientation, BOOL bHoldLastFrame)
	iSceneID = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneOrientation)
	SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSceneID, bHoldLastFrame)
ENDPROC


FUNC BOOL FIND_A_PIMP()
	
	IF IS_ENTITY_DEAD(prostitute_ped)
		RETURN FALSE
	ENDIF
		
	IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), prostitute_ped, <<25, 25, 25>>)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iPimpFlags, PIMP_SHOULD_CREATE)
		IF NOT IS_TIMER_STARTED(tPimpPlayerInVehicle)
			START_TIMER_NOW(tPimpPlayerInVehicle)
		ENDIF
		IF NOT IS_TIMER_STARTED(tPimpPlayerOutVehicle)
			START_TIMER_NOW(tPimpPlayerOutVehicle)
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RESTART_TIMER_NOW(tPimpPlayerInVehicle)
		ELSE
			RESTART_TIMER_NOW(tPimpPlayerOutVehicle)
		ENDIF
		
	
		
		IF (GET_TIMER_IN_SECONDS(tPimpPlayerInVehicle) > 60.0) //give player 60 seconds to flag hooker
		OR (GET_TIMER_IN_SECONDS(tPimpPlayerOutVehicle) > 15.0) //player has no vehicle, make something happen			
			SET_BITMASK_AS_ENUM(iPimpFlags, PIMP_SHOULD_CREATE)
			PROST_PB_DEBUG_MESSAGE("triggered pimp!!!")
			CDEBUG1LN(DEBUG_PROSTITUTE, "[PIMP] Creation Triggered!")	
		ENDIF
	ELSE
		IF NOT IS_BITMASK_AS_ENUM_SET(iPimpFlags, PIMP_WAS_REQUESTED)	
			REQUEST_MODEL(GET_PIMP_MODEL())
			SET_BITMASK_AS_ENUM(iPimpFlags, PIMP_WAS_REQUESTED)
			PROST_PB_DEBUG_MESSAGE("requested pimp!!!")
		ELIF HAS_MODEL_LOADED(GET_PIMP_MODEL())
			IF IS_ENTITY_DEAD(pimp_ped)
				VECTOR vPlayersPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				VECTOR vSpawnPos
				IF GET_CLOSEST_OFFSCREEN_PED_SPAWNPOINT(vPlayersPos, vSpawnPos)			
					FLOAT fSpawnHeading = GET_HEADING_BETWEEN_VECTORS(vSpawnPos, GET_ENTITY_COORDS(prostitute_ped))
					pimp_ped = CREATE_PED(PEDTYPE_CIVMALE, GET_PIMP_MODEL(), vSpawnPos, fSpawnHeading)
					ADD_PED_FOR_DIALOGUE(cultresConversation, CONST_iDialoguePimpSlot, pimp_ped, "PIMP")
					
					SET_ENTITY_AS_MISSION_ENTITY(pimp_ped)
					GIVE_WEAPON_TO_PED(pimp_ped, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE, FALSE)
					CDEBUG1LN(DEBUG_PROSTITUTE, "[PIMP] Created @", vSpawnPos)
				ELSE
					PROST_PB_DEBUG_MESSAGE("can't find offscreen spawnpoint!!!")
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD	
				IF IS_PROSTITUTE_DEBUG_TEXT_ON()
					pimp_blip = ADD_BLIP_FOR_ENTITY(pimp_ped)
					SET_BLIP_AS_FRIENDLY (pimp_blip, TRUE)
					SET_PROSITUTE_BLIP_COLOUR(BLIP_COLOUR_BLUE)				
				ENDIF
				#ENDIF
				PROST_PB_DEBUG_MESSAGE("found a pimp!!!")
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
		
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_CUSTOMER_A_PIMP()
	
	IF g_bIsProstitutePimpActive
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_CHECK_CUSTOMER_TYPE()
		RETURN FALSE
	ENDIF
	
	IF IS_BITMASK_AS_ENUM_SET(iPimpFlags, PIMP_WAS_VISITED)	
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT IS_PROSTITUTE_FORCE_TO_USE_PLAYER()	
	AND NOT IS_PROSTITUTE_FORCE_TO_USE_RANDOM()
	#ENDIF
		IF FIND_A_PIMP()
			customer_type = CUSTOMER_IS_PIMP
			RETURN TRUE
		ENDIF	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC
	
PROC MANAGE_CONVERSATION_DURING_PIMP_PROS_CONFRONTATION()
	
	
	IF IS_PED_INJURED(prostitute_ped)
	OR IS_PED_INJURED(pimp_ped)
		EXIT
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
			CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
			CDEBUG1LN(DEBUG_PROSTITUTE,"Clearing bIsPimpTalkingToPro flag to: ",  IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro), " at ", GET_GAME_TIMER() )
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_PROSTITUTE,"Pro is arguing - conversation. Seq = ",iPimpConv, "for ", GET_GAME_TIMER() )
		SWITCH iPimpConv
		
			CASE 0 //stand still 3000
				IF NOT IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					CREATE_CONVERSATION(cultresConversation, "pbproau", "pbpro_Pimp1", CONV_PRIORITY_AMBIENT_HIGH)
					CDEBUG1LN(DEBUG_PROSTITUTE,"Conversation pbpro_Pimp1 triggered at", GET_GAME_TIMER() )
					SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					iPimpConv++
				ENDIF
			BREAK
			CASE 1 //street_argue_a
				IF NOT IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					CREATE_CONVERSATION(cultresConversation, "pbproau", "pbpro_Pimp2", CONV_PRIORITY_AMBIENT_HIGH)
					CDEBUG1LN(DEBUG_PROSTITUTE,"Conversation pbpro_Pimp2 triggered at", GET_GAME_TIMER() )
					SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					iPimpConv++
				ENDIF
			BREAK
			
			CASE 2 //street_argue_b
				IF NOT IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					CREATE_CONVERSATION(cultresConversation, "pbproau", "pbpro_Pimp3", CONV_PRIORITY_AMBIENT_HIGH)
					CDEBUG1LN(DEBUG_PROSTITUTE,"Conversation pbpro_Pimp3 triggered at", GET_GAME_TIMER() )
					SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsPimpTalkingToPro)
					iPimpConv++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
		
ENDPROC

///////////////////////////////////
// *** PROS PIMP INTERACTION *** //
///////////////////////////////////

PROC PROSTITUTE_PIMP_ARGUEMENT()
	IF IS_ENTITY_DEAD(pimp_ped)			
		REMOVE_NON_PLAYER_CUSTOMER()			
	ENDIF
					
	SWITCH eCurrentProstituteState
		CASE PROSTITUTE_IDLE
			/// determine if player is the customer or use a nearby ped
			IF NOT (customer_type = CUSTOMER_NO_CUSTOMER)				
				eCurrentProstituteState = PROSTITUTE_SPOTTED_CUSTOMER
				PROST_PB_DEBUG_MESSAGE("prostitute SPOTTED PIMP")						
			ENDIF
		BREAK
		
		CASE PROSTITUTE_SPOTTED_CUSTOMER
			/// pimp controller goes here
			SWITCH current_pimp_state
				CASE PIMP_STATE_STARTING
				
					VECTOR vPimpSceneStartPos
					VECTOR vPimpSceneStartRot			
					
					vPimpSceneStartPos = GET_ANIM_INITIAL_OFFSET_POSITION(GET_PROSTITUTE_PIMP_ANIM_DICT(), GET_PROSTITUTE_PIMP_ANIM_TO_PLAY(PROSTITUTE_PIMP_MAIN), GET_ENTITY_COORDS(prostitute_ped), GET_ENTITY_ROTATION(prostitute_ped))
					vPimpSceneStartRot = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_PROSTITUTE_PIMP_ANIM_DICT(), GET_PROSTITUTE_PIMP_ANIM_TO_PLAY(PROSTITUTE_PIMP_MAIN), GET_ENTITY_COORDS(prostitute_ped), GET_ENTITY_ROTATION(prostitute_ped))
				
					// have the pimp approach the pros		
					TASK_FOLLOW_NAV_MESH_TO_COORD( pimp_ped, vPimpSceneStartPos , PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_STOP_EXACTLY,vPimpSceneStartRot.z)					
					TASK_LOOK_AT_ENTITY(pimp_ped, prostitute_ped, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						
					current_pimp_state = PIMP_MOVING_TO_PRO
				BREAK
				
				CASE PIMP_MOVING_TO_PRO
					// if finished going to pros
					IF (GET_SCRIPT_TASK_STATUS(pimp_ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK)
						
						IF HAS_ANIM_DICT_LOADED(GET_PROSTITUTE_PIMP_ANIM_DICT())  
							
							PROSTITUTE_CREATE_PIMP_SYNC_SCENE(iPimpSyncSceneID, GET_ENTITY_COORDS(prostitute_ped),GET_ENTITY_ROTATION(prostitute_ped), FALSE)
							TASK_SYNCHRONIZED_SCENE(prostitute_ped, iPimpSyncSceneID, GET_PROSTITUTE_PIMP_ANIM_DICT(), GET_PROSTITUTE_PIMP_ANIM_TO_PLAY(PROSTITUTE_PIMP_HOOKER), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							TASK_SYNCHRONIZED_SCENE(pimp_ped, iPimpSyncSceneID, GET_PROSTITUTE_PIMP_ANIM_DICT(), GET_PROSTITUTE_PIMP_ANIM_TO_PLAY(PROSTITUTE_PIMP_MAIN), SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_TAG_SYNC_OUT)
//							JDDO - Get the cash hooked in
//							IF NOT IS_ENTITY_DEAD(oCashObject)
//								PLAY_SYNCHRONIZED_ENTITY_ANIM(oCashObject, iPimpSyncSceneID, GET_PROSTITUTE_PIMP_ANIM_DICT(), GET_PROSTITUTE_PIMP_ANIM_TO_PLAY(PROSTITUTE_PIMP_MAIN), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
//							ENDIF
							CDEBUG1LN(DEBUG_PROSTITUTE, "[PIMP] Sync Scene Starting")
							current_pimp_state = PIMP_AND_PRO_ARGUING
						ENDIF
					ENDIF
				BREAK
				
				CASE PIMP_AND_PRO_ARGUING
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iPimpSyncSceneID)
						MANAGE_CONVERSATION_DURING_PIMP_PROS_CONFRONTATION()
					ELSE
					
						FORCE_PED_MOTION_STATE(pimp_ped, MS_ON_FOOT_WALK)

						CLEAR_PED_TASKS(pimp_ped)

						FORCE_PED_AI_AND_ANIMATION_UPDATE(pimp_ped)
						TASK_WANDER_STANDARD(pimp_ped)
						
						// have the pimp leave the area	
						INT iPimpMoney, iProsMoney
						iPimpMoney = GET_PED_MONEY(pimp_ped)
						iProsMoney = GET_PED_MONEY(prostitute_ped)
						IF iProsMoney > GET_PROSTITUTE_PRICE_FOR_SEX_ACT(SEX_TYPE_CHOSEN_BLOWJOB)
							iPimpMoney += GET_PED_MONEY(prostitute_ped)
						ELSE 
							iPimpMoney += GET_PROSTITUTE_PRICE_FOR_SEX_ACT(SEX_TYPE_CHOSEN_BLOWJOB)
						ENDIF
						SET_PED_MONEY(pimp_ped, iPimpMoney)			
						SET_PED_MONEY(prostitute_ped, 0)			
						
						CDEBUG1LN(DEBUG_PROSTITUTE, "prostitute & PIMP FINISHED")									
						current_pimp_state = PIMP_LEAVING_AREA
					ENDIF
				BREAK
				
				CASE PIMP_LEAVING_AREA
					REMOVE_NON_PLAYER_CUSTOMER()
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PROSTITUTE_UNDER_ATTACK
			IF NOT IS_PED_INJURED (pimp_ped)		
				SEQUENCE_INDEX seqIDPimp
				OPEN_SEQUENCE_TASK (seqIDPimp)							
					TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(250, 500))				
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())		
				CLOSE_SEQUENCE_TASK(seqIDPimp)
			
				TASK_PERFORM_SEQUENCE(pimp_ped, seqIDPimp)
				CLEAR_SEQUENCE_TASK(seqIDPimp)		
				SET_PED_KEEP_TASK(pimp_ped, TRUE)										
				PROST_PB_DEBUG_MESSAGE("pimp told to attack player")					
			ENDIF
			
			pimp_ped = NULL
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
		BREAK
		
		CASE PROSTITUTE_WAITING_TO_END			
			/// if player far enough away
			IF NOT IS_PED_INJURED (customer_ped)
				IF NOT IS_ENTITY_AT_ENTITY(prostitute_ped, customer_ped, <<12.0, 12.0, 5.0>>, FALSE)							
					/// end the script				
					SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	
ENDPROC

#ENDIF

//EOF
