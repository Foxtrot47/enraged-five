//File: pb_prostitute_unique.sch
//Original Author: John Diaz

//Purpose - Support file for unique prostitutes
//B*1276913 - UNIQUE PROSTITUTES-------------------------------------------------------


USING "pb_prostitute_common.sch"


/// PURPOSE:
///    Convert the unique prostitute enum into a string, mostly for debug info
/// PARAMS:
///    eUniquePro - 
/// RETURNS:
///    
FUNC STRING GET_UNIQUE_PROSTITUTE_NAME_VIA_ENUM(PROSTITUTE_UNIQUE_ENUM eUniqueProEnum)
	
	STRING sProstituteID
	
	SWITCH eUniqueProEnum
				
		CASE PUN_BABS
			sProstituteID = "UPRO BABS"
		BREAK

		CASE PUN_DANA		
			sProstituteID = "UPRO DANA"
		BREAK

		CASE PUN_LIZZIE		
			sProstituteID = "UPRO LIZZIE"
		BREAK

		CASE PUN_AMANDA		
			sProstituteID = "UPRO AMANDA"
		BREAK

		CASE PUN_ASHLEY		
			sProstituteID = "UPRO ASHLEY"
		BREAK
	
		CASE PUN_KRISTEN	
			sProstituteID = "UPRO KRISTEN"
		BREAK
		
		CASE PUN_SASHA		
			sProstituteID = "UPRO SASHA"
		BREAK
		
		CASE PUN_JACQUELINE
			sProstituteID = "UPRO JACQUELINE"
		BREAK
		
		DEFAULT
			sProstituteID = "UPRO N/A"
	ENDSWITCH
	
//	CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Name is ",sProstituteID)
	

	RETURN sProstituteID

ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    iUPIndex - 
/// RETURNS:
///    
FUNC STRING GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(INT iUniqueProIndex)
	
	RETURN GET_UNIQUE_PROSTITUTE_NAME_VIA_ENUM(INT_TO_ENUM(PROSTITUTE_UNIQUE_ENUM, iUniqueProIndex))
	
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
FUNC INT GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(INT iUniqueProIndex)
	INT iTimeSleptWithPlayer = 0
	
	IF iUniqueProIndex = -1 	
		#IF IS_DEBUG_BUILD
		#IF SHOW_UNIQUE_INFO_IN_DEBUG

			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Index for GET_NUM_TIMES_SLEPT_WITH_PLAYER is INVALID ")

		#ENDIF
		#ENDIF
		
		RETURN 0
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		iTimeSleptWithPlayer = g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_MICHAEL]
		
			#IF IS_DEBUG_BUILD
			#IF SHOW_UNIQUE_INFO_IN_DEBUG
		
				CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Index for ", GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex),  " GET_NUM_TIMES_SLEPT_WITH MICHAEL =  ", iTimeSleptWithPlayer)
		
			#ENDIF
			#ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		iTimeSleptWithPlayer = g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_FRANKLIN]
		#IF IS_DEBUG_BUILD
		#IF SHOW_UNIQUE_INFO_IN_DEBUG
	
		CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Index for ", GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex),  " GET_NUM_TIMES_SLEPT_WITH FRANKLIN =  ", iTimeSleptWithPlayer)
	
		#ENDIF
		#ENDIF
	
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		iTimeSleptWithPlayer = g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_TREVOR]
		
		#IF IS_DEBUG_BUILD
		#IF SHOW_UNIQUE_INFO_IN_DEBUG
	
		CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Index for ", GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex),  " GET_NUM_TIMES_SLEPT_WITH TREVOR =  ", iTimeSleptWithPlayer)
	
		#ENDIF
		#ENDIF
		

	ENDIF
	
	
	RETURN iTimeSleptWithPlayer

ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
/// RETURNS:
///    
FUNC BOOL HAS_UNIQUE_PROSTITUTE_MET_PLAYER(INT iUniqueProIndex)
	IF GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProIndex) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
///    whichComponent - 
/// RETURNS:
///    
FUNC INT GET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION_FOR_COMPONENT(INT iUniqueProIndex ,UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS whichComponent)

	IF HAS_UNIQUE_PROSTITUTE_MET_PLAYER(iUniqueProIndex)
		
		RETURN g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iComponentDrawableVariation[whichComponent]
	
	ENDIF
	
	RETURN -1 
ENDFUNC
/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
///    whichComponent - 
/// RETURNS:
///    
FUNC INT GET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION_FOR_COMPONENT(INT iUniqueProIndex ,UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS whichComponent)

	IF HAS_UNIQUE_PROSTITUTE_MET_PLAYER(iUniqueProIndex)
		
		RETURN g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iComponentTextureVariation[whichComponent]
	
	ENDIF
	
	RETURN -1 
ENDFUNC

PROC SET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION_FOR_COMPONENT(INT iUniqueProIndex ,UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS whichComponent, INT iNewComponent)
	IF iUniqueProIndex > -1 
		g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iComponentDrawableVariation[whichComponent] = iNewComponent
	ENDIF
ENDPROC

PROC SET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION_FOR_COMPONENT(INT iUniqueProIndex ,UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS whichComponent, INT iNewComponent)
	
	IF iUniqueProIndex > -1 
		g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iComponentTextureVariation[whichComponent] = iNewComponent
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Use this to find the first unused unique prostitute slot
/// RETURNS:
///    Which enum is available
///    

FUNC INT GET_UNUSED_UNIQUE_PROSTITUTE_INDEX()
	
	INT iIterator
	
	REPEAT MAX_UNIQUE_PROSTITUTES iIterator
		IF GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iIterator) =  0		
			RETURN iIterator
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC
/// PURPOSE:
///    
/// RETURNS:
///    
FUNC PROSTITUTE_UNIQUE_ENUM GET_UNUSED_UNIQUE_PROSTITUTE_ENUM()
	
	INT iUnusedUniqueProIndex
	iUnusedUniqueProIndex = GET_UNUSED_UNIQUE_PROSTITUTE_INDEX()
	
	CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] First slot is : ", GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUnusedUniqueProIndex) )
	RETURN INT_TO_ENUM(PROSTITUTE_UNIQUE_ENUM,iUnusedUniqueProIndex)

ENDFUNC
/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
PROC UPDATE_UNIQUE_PROSTITUTE_SLEPT_WITH_PLAYER(INT iUniqueProIndex)
	IF iUniqueProIndex > -1
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			
			g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_MICHAEL]++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) , " serviced MICHAEL #", GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProIndex) )

		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			
			g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_FRANKLIN]++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) , " serviced  FRANKLIN #", GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProIndex) )

		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		
			g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iNumTimesEncounteredByPlayer[CHAR_TREVOR]++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) , " serviced TREVOR #", GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProIndex) )
		
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
/// RETURNS:
///    
FUNC STRING GET_UNIQUE_PROSTITUTE_DIALOGUE_NAME(INT iUniqueProIndex)

	
	STRING sProstituteID
	
	SWITCH INT_TO_ENUM(PROSTITUTE_UNIQUE_ENUM, iUniqueProIndex)
				
		CASE PUN_BABS
			sProstituteID = "Hooker1"
		BREAK

		CASE PUN_DANA		
			sProstituteID = "Hooker2"
		BREAK

		CASE PUN_LIZZIE		
			sProstituteID = "Hooker3"
		BREAK

		CASE PUN_AMANDA		
			sProstituteID = "Hooker4"
		BREAK

		CASE PUN_ASHLEY		
			sProstituteID = "Hooker5"
		BREAK
	
		CASE PUN_KRISTEN	
			sProstituteID = "Hooker6"
		BREAK
		
		CASE PUN_SASHA		
			sProstituteID = "Hooker7"
		BREAK
		
		CASE PUN_JACQUELINE
			sProstituteID = "Hooker8"
		BREAK
		
		DEFAULT
			sProstituteID = "UPRO N/A"
	ENDSWITCH
	
	RETURN sProstituteID
ENDFUNC

/// PURPOSE: Converts a UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS into a PED_COMPENET
///   
FUNC PED_COMPONENT GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_DRAWABLE_COMPONENT(UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS whichTextureVariation)
	
	PED_COMPONENT tPedComponentToReturn
	
	SWITCH whichTextureVariation
		
		CASE PRO_COMP_HEAD
			tPedComponentToReturn = PED_COMP_HEAD
		BREAK
		
		CASE PRO_COMP_HAIR
			tPedComponentToReturn = PED_COMP_HAIR
		BREAK
		
//		CASE PRO_COMP_TORSO
//			tPedComponentToReturn = PED_COMP_TORSO
//		BREAK
//		
//		CASE PRO_COMP_LEG
//			tPedComponentToReturn = PED_COMP_LEG
//		BREAK
	ENDSWITCH
	
	RETURN tPedComponentToReturn
ENDFUNC

/// PURPOSE: Converts a UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS into a PED_COMPENET
///    

FUNC PED_COMPONENT GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_TEXTURE_VARIATION(UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS whichTextureVariation)
	
	PED_COMPONENT tPedComponentToReturn
	
	SWITCH whichTextureVariation
		
		CASE PRO_TEXT_VAR_HEAD
			tPedComponentToReturn = PED_COMP_HEAD
		BREAK
		
		CASE PRO_TEXT_VAR_HAIR
			tPedComponentToReturn = PED_COMP_HAIR
		BREAK
	
	ENDSWITCH
	
	RETURN tPedComponentToReturn
ENDFUNC

FUNC INT GET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION(PED_INDEX pedUniqueProstitute, UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS whichComponent)
	RETURN GET_PED_DRAWABLE_VARIATION(pedUniqueProstitute, GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_DRAWABLE_COMPONENT(whichComponent))
ENDFUNC

FUNC INT GET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION(PED_INDEX pedUniqueProstitute, UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS whichTextureVariation)
	RETURN GET_PED_TEXTURE_VARIATION(pedUniqueProstitute, GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_TEXTURE_VARIATION(whichTextureVariation))
ENDFUNC


/// PURPOSE:
///    
/// PARAMS:
///    pedProstitute - 
///    iUniqueProIndex - 
///    whichComponent - 
/// RETURNS:
///    
FUNC BOOL DOES_PROSTITUTE_MATCH_UNIQUE_PRO_DRAW_COMPONENT(PED_INDEX pedProstitute, INT iUniqueProIndex , UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS whichComponent )
	
	PED_COMPONENT thisComponent = GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_DRAWABLE_COMPONENT(whichComponent)
	
	RETURN GET_PED_DRAWABLE_VARIATION(pedProstitute , thisComponent) = GET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION_FOR_COMPONENT(iUniqueProIndex, whichComponent)
	
ENDFUNC
/// PURPOSE:
///    
/// PARAMS:
///    pedProstitute - 
///    iUniqueProIndex - 
///    whichComponent - 
/// RETURNS:
///    
FUNC BOOL DOES_PROSTITUTE_MATCH_UNIQUE_PRO_TEXTURE_COMPONENT(PED_INDEX pedProstitute, INT iUniqueProIndex , UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS whichVariation )
	
	PED_COMPONENT thisVariation = GET_PED_COMPONENT_FROM_UNIQUE_PROSTITUTE_TEXTURE_VARIATION(whichVariation)
	
	RETURN GET_PED_TEXTURE_VARIATION(pedProstitute , thisVariation) = GET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION_FOR_COMPONENT(iUniqueProIndex, whichVariation)
	
ENDFUNC

/// PURPOSE: Loops through all drawable components on a unique prostitutes to see if there is a match.
///    
/// PARAMS:
///    pedProstitute - 
///    iUniqueProIndex - 
/// RETURNS:
///    
FUNC BOOL DO_UNIQUE_PRO_DRAW_COMPONENTS_MATCH(PED_INDEX pedProstitute, INT iUniqueProIndex)

	INT iComponentIterator
	REPEAT NUM_PRO_COMPONENTS iComponentIterator
		IF NOT DOES_PROSTITUTE_MATCH_UNIQUE_PRO_DRAW_COMPONENT(pedProstitute, iUniqueProIndex, INT_TO_ENUM(UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS,iComponentIterator ))
		
			#IF IS_DEBUG_BUILD
			#IF SHOW_UNIQUE_INFO_IN_DEBUG
		
			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] DO_UNIQUE_PRO_DRAW_COMPONENTS_MATCH - Mismatch on component ",iComponentIterator, " for :" ,GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) ) 
		
			#ENDIF
			#ENDIF
		
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	
	
	#IF IS_DEBUG_BUILD
	#IF SHOW_UNIQUE_INFO_IN_DEBUG

	CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] DO_UNIQUE_PRO_DRAW_COMPONENTS_MATCH - SUCCESS on ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) ) 

	#ENDIF
	#ENDIF
			
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_UNIQUE_PRO_TEXTURE_VARIATIONS_MATCH(PED_INDEX pedProstitute, INT iUniqueProIndex)

	INT iTextureVariationIterator
	REPEAT NUM_PRO_TEXTURE_VARIATIONS iTextureVariationIterator
		IF NOT DOES_PROSTITUTE_MATCH_UNIQUE_PRO_TEXTURE_COMPONENT(pedProstitute, iUniqueProIndex, INT_TO_ENUM(UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS,iTextureVariationIterator ))

			#IF IS_DEBUG_BUILD
			#IF SHOW_UNIQUE_INFO_IN_DEBUG

				CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] DO_UNIQUE_PRO_TEXTURE_VARIATIONS_MATCH - Mismatch on variation ",iTextureVariationIterator, " for :" ,GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) ) 

			#ENDIF
			#ENDIF
	
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	#IF SHOW_UNIQUE_INFO_IN_DEBUG

		CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] DO_UNIQUE_PRO_TEXTURE_VARIATIONS_MATCH - SUCCESS on ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) ) 

	#ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_UNIQUE_PRO_MODELS_MATCH(PED_INDEX pedProstitute,INT iUniqueProIndex)

	IF IS_PED_MODEL(pedProstitute, INT_TO_ENUM(MODEL_NAMES, g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iModelType))
	
		#IF IS_DEBUG_BUILD
		#IF SHOW_UNIQUE_INFO_IN_DEBUG

			CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] DO_UNIQUE_PRO_MODELS_MATCH - SUCCESS on ",GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iUniqueProIndex) ) 

		#ENDIF
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    pedProstitute - 
/// RETURNS:
///    
FUNC INT GET_PROSTITUTE_UNIQUE_ID(PED_INDEX pedProstitute)

	IF NOT IS_ENTITY_DEAD(pedProstitute)
		
		INT iUniqueProstituteIterator

		REPEAT MAX_UNIQUE_PROSTITUTES iUniqueProstituteIterator
			
			IF GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProstituteIterator) > 0
				
				//Loop through all components to see if we have a match
				IF DO_UNIQUE_PRO_DRAW_COMPONENTS_MATCH(pedProstitute,iUniqueProstituteIterator )
				AND DO_UNIQUE_PRO_TEXTURE_VARIATIONS_MATCH(pedProstitute,iUniqueProstituteIterator)
				AND DO_UNIQUE_PRO_MODELS_MATCH(pedProstitute,iUniqueProstituteIterator)
					RETURN iUniqueProstituteIterator
				ENDIF
			ENDIF

		ENDREPEAT	
	
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    pedProstitute - 
/// RETURNS:
///    
FUNC BOOL IS_PROSTITUTE_UNIQUE(PED_INDEX pedProstitute)

	IF IS_ENTITY_DEAD(pedProstitute)
		RETURN FALSE
	ENDIF
	
	RETURN ( GET_PROSTITUTE_UNIQUE_ID(pedProstitute) > -1 )

ENDFUNC
FUNC STRING GET_UNIQUE_PROSTITUTE_NAME_FOR_PED(PED_INDEX pedProstitute)
	RETURN GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(GET_PROSTITUTE_UNIQUE_ID(pedProstitute))
ENDFUNC



FUNC BOOL HAS_PROSTITUTE_PED_MET_PLAYER(PED_INDEX pedUniqueProstitute)
	
	INT iUniqueProID = GET_PROSTITUTE_UNIQUE_ID(pedUniqueProstitute)
	
	RETURN HAS_UNIQUE_PROSTITUTE_MET_PLAYER(iUniqueProID)
	
ENDFUNC


/// PURPOSE:
///    
/// PARAMS:
///    pedProstitute - 
PROC ADD_UNIQUE_PROSTITUTE_TO_CONVERSATION(PED_INDEX pedProstitute)
	
	INT iUniqueProID = GET_PROSTITUTE_UNIQUE_ID(pedProstitute)
	
	IF  iUniqueProID >-1
		//Removing her cause I think it asserts if she's added in 2 slots. I believe
		//Remove her from her current dialogue
		REMOVE_PED_FOR_DIALOGUE(cultresConversation, CONST_iDialogueProstituteSlot)
		
		//Add her here
		ADD_PED_FOR_DIALOGUE(cultresConversation, CONST_iDialogueUniqueSlot, pedProstitute, GET_UNIQUE_PROSTITUTE_DIALOGUE_NAME(iUniqueProID))
		CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] Added: ", GET_UNIQUE_PROSTITUTE_DIALOGUE_NAME(iUniqueProID), "to conversation struct")
	
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    iUniqueProIndex - 
PROC PLAY_NEXT_LINE_FOR_UNIQUE_PROSTITUTE_BACK_STORY(INT iUniqueProIndex)

	//Pattern for dialogue root = "pbpro_H (uniqueID)V(NumCurrentLine)
	INT iNumLine = GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProIndex)
	
	IF iNumLine < CONST_iMaxNumStoryLines
		TEXT_LABEL_23 sConvoRoot = "pbpro_H"
		sConvoRoot+= iUniqueProIndex + 1
		sConvoRoot+= "V"
		sConvoRoot+= iNumLine
		
		
		CREATE_CONVERSATION(cultresConversation, "pbproau", sConvoRoot, CONV_PRIORITY_AMBIENT_HIGH)
		CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] Playing backstory with root: ", sConvoRoot, "for :", iNumLine)
	ELSE
		CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] ",GET_UNIQUE_PROSTITUTE_DIALOGUE_NAME(iUniqueProIndex), "finished story.")	
	ENDIF		
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    pedUniqueProstitute - 
PROC CREATE_CONVERSATION_FOR_UNIQUE_PROSTITUTE(PED_INDEX pedUniqueProstitute)
	
	INT iUniqueProID = GET_PROSTITUTE_UNIQUE_ID(pedUniqueProstitute)
	
	IF  iUniqueProID >-1
		IF IS_ANY_SPEECH_PLAYING(pedUniqueProstitute)
			STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedUniqueProstitute)
		ENDIF
		
		IF NOT IS_ANY_SPEECH_PLAYING(pedUniqueProstitute)
			ADD_UNIQUE_PROSTITUTE_TO_CONVERSATION(pedUniqueProstitute)
			PLAY_NEXT_LINE_FOR_UNIQUE_PROSTITUTE_BACK_STORY(iUniqueProID)
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_UNIQUE_PROSTITUTE_BACKSTORY_AVAILABLE(PED_INDEX pedUniqueProstitute)
	
	#IF FORCE_UNIQUE
		RETURN TRUE
	#ENDIF
	
	INT iUniqueProID = GET_PROSTITUTE_UNIQUE_ID(pedUniqueProstitute)
	
	//Not Unique Exit out
	IF iUniqueProID = -1
		RETURN FALSE
	ENDIF
	
	RETURN GET_UNIQUE_PROSTITUTE_NUM_TIMES_SLEPT_WITH_PLAYER(iUniqueProID) < CONST_iMaxNumStoryLines
	
ENDFUNC

PROC SAVE_UNIQUE_PROSTITUTE_MODEL(PED_INDEX pedUniqueProstitute)

	INT iUniqueProIndex = GET_PROSTITUTE_UNIQUE_ID(pedUniqueProstitute)
	
	IF  iUniqueProIndex > -1
		g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iModelType = ENUM_TO_INT(GET_ENTITY_MODEL(pedUniqueProstitute))
		CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] Saving Model Type  " ,g_savedGlobals.sAmbient.sProstituteData[iUniqueProIndex].iModelType )
	ENDIF
ENDPROC
/// PURPOSE:
///    
/// PARAMS:
///    pedUniqueProstitute - 
PROC SAVE_UNIQUE_PROSTITUTE_COMPONENT_INFO(PED_INDEX pedUniqueProstitute)
	
	IF IS_ENTITY_DEAD(pedUniqueProstitute)
		EXIT
	ENDIF
	
	INT iNextUniqueProstitute = GET_UNUSED_UNIQUE_PROSTITUTE_INDEX()
	
	IF iNextUniqueProstitute = -1
		CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] No more slots available. ")
		EXIT
	ENDIF
	
	UPDATE_UNIQUE_PROSTITUTE_SLEPT_WITH_PLAYER(iNextUniqueProstitute)
	
	SET_PED_NAME_DEBUG(pedUniqueProstitute,GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iNextUniqueProstitute))
	
	//Model Type----------------------------------------------------------
	g_savedGlobals.sAmbient.sProstituteData[iNextUniqueProstitute].iModelType = ENUM_TO_INT(GET_ENTITY_MODEL(pedUniqueProstitute))
	CDEBUG1LN(DEBUG_PROSTITUTE,"[UNIQUE] Saving Model Type  " ,g_savedGlobals.sAmbient.sProstituteData[iNextUniqueProstitute].iModelType )
	
	
	//Drawable Components-------------------------------------------------
	UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS iCurrentProstituteDrawableComponent
	INT iComponentIterator
	INT iDrawableCompToSave
	
	//SAVE DRAWABLE COMPONENTS
	REPEAT NUM_PRO_COMPONENTS iComponentIterator
		
		//Check each pro component
		iCurrentProstituteDrawableComponent = INT_TO_ENUM(UNIQUE_PROSTITUTE_DRAWABLE_COMPONENTS, iComponentIterator)
		
		//Grab the pro's component
		iDrawableCompToSave = GET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION(pedUniqueProstitute,iCurrentProstituteDrawableComponent)
		
		//Save the pro's component
		SET_UNIQUE_PROSTITUTE_DRAWABLE_VARIATION_FOR_COMPONENT(iNextUniqueProstitute, iCurrentProstituteDrawableComponent, iDrawableCompToSave)
		
		CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Pro ", 	GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iNextUniqueProstitute) , " has component# ",iComponentIterator, "  Drawable Component#", iDrawableCompToSave)
	ENDREPEAT
	
	//SAVE TEXTURE VARIATIONS
	UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS iCurrentProstituteTextureVariation
	
	REPEAT NUM_PRO_TEXTURE_VARIATIONS iComponentIterator
		
		//Check each pro component
		iCurrentProstituteTextureVariation = INT_TO_ENUM(UNIQUE_PROSTITUTE_TEXTURE_VARIATIONS, iComponentIterator)
		
		//Grab the pro's component
		iDrawableCompToSave = GET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION(pedUniqueProstitute,iCurrentProstituteTextureVariation)
		
		//Save the pro's component
		SET_UNIQUE_PROSTITUTE_TEXTURE_VARIATION_FOR_COMPONENT(iNextUniqueProstitute, iCurrentProstituteTextureVariation, iDrawableCompToSave)

		CDEBUG1LN(DEBUG_PROSTITUTE, "[UNIQUE] Pro ",	GET_UNIQUE_PROSTITUTE_NAME_VIA_INDEX(iNextUniqueProstitute) , " has component# ",iComponentIterator, "  Texture Var#", iDrawableCompToSave)
		
	ENDREPEAT
ENDPROC


//------------------------------------------------------------------------------------------------------------------
//EOF
