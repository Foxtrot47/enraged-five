
//Sex Cam Tweaks
USING "commands_entity.sch"
CONST_INT CONST_iSexCamLookXLimit							10
CONST_INT CONST_iSexCamLookYLimit							10
CONST_INT CONST_iSexCamRollLimit							3

CONST_FLOAT CONST_fMinPanLimit								-3.0
CONST_FLOAT CONST_fMaxPanLimit								3.0

CONST_FLOAT SWVL_ROT_INTERP			0.050
CONST_FLOAT SWVL_ROT_INTERP_MOUSE	0.050
CONST_FLOAT CONST_fCapsuleRadius	0.25

ENUM SEX_CAMERA_POSITIONS
	XCAM_FRONT_WINDSHEILD = 0,
	XCAM_PASSENGER_WINDOW,
	XCAM_DRIVER_WINDOW,
	XCAM_REAR_WINDSHEILD,
	XCAM_TOTAL			// 4 
ENDENUM

SHAPETEST_INDEX stCameraProbe[XCAM_TOTAL]

STRUCT SWIVEL_CAM_STRUCT

	CAMERA_INDEX theCam
	VECTOR vInitCamRot
	VECTOR vCamRotOffsetCurrent

ENDSTRUCT
SWIVEL_CAM_STRUCT fpSexCam[XCAM_TOTAL]


FUNC VECTOR GET_0_VEC()
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC INIT_SWIVEL_CAMERA(SWIVEL_CAM_STRUCT& thisSwivelCam, VECTOR vInitPos, VECTOR vInitRot, FLOAT fInitFov)
										
	thisSwivelCam.vInitCamRot = vInitRot
	
	thisSwivelCam.vCamRotOffsetCurrent = <<0,0,0>>
	
	thisSwivelCam.theCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	SET_CAM_ACTIVE(thisSwivelCam.theCam,TRUE)
	SET_CAM_PARAMS(	thisSwivelCam.theCam,
					vInitPos,
					thisSwivelCam.vInitCamRot,
					fInitFov)
					
	 
	RENDER_SCRIPT_CAMS(TRUE,FALSE)

ENDPROC
PROC CLEANUP_SWIVEL_CAM(SWIVEL_CAM_STRUCT &thisSwivelCam)
	IF DOES_CAM_EXIST(thisSwivelCam.theCam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF IS_CAM_ACTIVE(thisSwivelCam.theCam)
			SET_CAM_ACTIVE(thisSwivelCam.theCam,FALSE)
		ENDIF

		DESTROY_CAM(thisSwivelCam.theCam)
	ENDIF
		
ENDPROC



PROC CLEANUP_SEX_CAMS(BOOL bPostSexFrame = FALSE)
	IF bPostSexFrame
		VECTOR fVehicleRot = GET_ENTITY_ROTATION(iPlayersVehicle)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(180.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-5.5 - fVehicleRot.x)
		
		CDEBUG1LN(DEBUG_PROSTITUTE, "[CLEANUP_SEX_CAMS] - Pointing cam at player")
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE,FALSE)

	
	CLEANUP_SWIVEL_CAM(fpSexCam[XCAM_FRONT_WINDSHEILD])
	CLEANUP_SWIVEL_CAM(fpSexCam[XCAM_REAR_WINDSHEILD])
	CLEANUP_SWIVEL_CAM(fpSexCam[XCAM_DRIVER_WINDOW])
	CLEANUP_SWIVEL_CAM(fpSexCam[XCAM_PASSENGER_WINDOW])
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_CAM_TOG")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_CAM_OC")
		CLEAR_HELP()
	ENDIF
	
	CDEBUG1LN(DEBUG_PROSTITUTE, "[MANAGE_SEX_ANIMS] - CLEANUP_SEX_CAMS")

ENDPROC
FUNC VECTOR GET_REAR_SEX_CAM_POS_FOR_VEHICLE()
	
	VECTOR vFrontWindsheild, vCamHeight, vModelMin, vModelMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(iPlayersVehicle), vModelMin, vModelMax)
	
	vFrontWindsheild = GET_WORLD_POSITION_OF_ENTITY_BONE(iPlayersVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(iPlayersVehicle, "windscreen"))

	vCamHeight = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(iPlayersVehicle,vFrontWindsheild)
	
	vCamHeight.z += 0.09
	vModelMin.y -= 1.0
	
	RETURN GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersVehicle,<<0.0,-ABSF(vModelMin.y),vCamHeight.z>>)
ENDFUNC

#IF IS_JAPANESE_BUILD
FUNC VECTOR GET_POS_FOR_JAPANESE_REAR_CAMERA()
	VECTOR vModelMin, vModelMax, vRearCamPos
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(iPlayersVehicle), vModelMin, vModelMax)
		
	vModelMin.y -= 0.65	//push it back a little bit

	vRearCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersVehicle, <<0.0,-ABSF(vModelMin.y),-0.02>>)
	
//	CDEBUG1LN(DEBUG_PROSTITUTE, "[JAPANESE EDIT] - Model Min =",vModelMin, "Model Max = ", vModelMax, "Origin = ",  vModelOrigin, "RearCamPos = ", vRearCamPos)

	
	RETURN vRearCamPos
ENDFUNC
#ENDIF

/// PURPOSE: The sex cameras are all based off of the 4 different windshield bones. In case where the windshield bones aren't defined we check for this 
///    and do math based off the front windshield, the root of the vehicle, and the dimensions of the vehicle.
///    In place of the driver side window and passenger side window we're use the head pones of the driver and passenger.
///    Will go back and try to do this for all cams if time permits.
///    
/// PARAMS:
///    tWindow - 
///    iPlayersVehicle - 
/// RETURNS:
///    
FUNC VECTOR GET_POSITION_FOR_SEX_CAM(SEX_CAMERA_POSITIONS tWindow)
	
	VECTOR vCamPos
	VECTOR vModelMin, vModelMax
	VECTOR vFrontWindsheild, vCamHeight
	
	IF NOT IS_ENTITY_DEAD(prostitute_ped)
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(iPlayersVehicle)
	ENDIF
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(iPlayersVehicle), vModelMin, vModelMax)

	vFrontWindsheild = GET_WORLD_POSITION_OF_ENTITY_BONE(iPlayersVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(iPlayersVehicle, "windscreen"))

	vCamHeight = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(iPlayersVehicle,vFrontWindsheild)
	
	
	SWITCH tWindow
	
		CASE XCAM_FRONT_WINDSHEILD
			vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersVehicle,<<0.0,(vModelMax.y + 1.4),vCamHeight.z>>)
		BREAK
	
	
		CASE XCAM_PASSENGER_WINDOW
			vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersVehicle,<<1.5,(vModelMax.y + 1.2),(vCamHeight.z-0.1)>>)
		BREAK
		
		CASE XCAM_DRIVER_WINDOW

			vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(iPlayersVehicle,<<-3.7,0.7,vCamHeight.z>>)
			
		BREAK
		
		CASE XCAM_REAR_WINDSHEILD
			#IF NOT IS_JAPANESE_BUILD
				vCamPos = GET_REAR_SEX_CAM_POS_FOR_VEHICLE()
			#ENDIF
		
			#IF IS_JAPANESE_BUILD
				vCamPos = GET_POS_FOR_JAPANESE_REAR_CAMERA()
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN vCamPos
ENDFUNC

PROC SET_CAM_BIT_TO_OCCLUDED(SEX_CAMERA_POSITIONS tWindow)
	SWITCH tWindow
	
		CASE XCAM_FRONT_WINDSHEILD
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
				SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
				CLEAR_BITMASK_AS_ENUM(iCameraBitFlags,PSA_FRONTCAM_IS_CLEAR)
				
				CDEBUG1LN(DEBUG_PROSTITUTE, "[SET_CAM_BIT_TO_OCLUDED] - PSA_FRONT_CAM_IS_OCCLUDED")
			ENDIF
		BREAK
		
		CASE XCAM_PASSENGER_WINDOW
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
				SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
				CLEAR_BITMASK_AS_ENUM(iCameraBitFlags,PSA_PASSENGER_CAM_IS_CLEAR)
			
				CDEBUG1LN(DEBUG_PROSTITUTE, "[SET_CAM_BIT_TO_OCLUDED] - PSA_PASSENGER_CAM_IS_OCCLUDED")

			ENDIF
		BREAK
		
		CASE XCAM_DRIVER_WINDOW
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
				SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
				CLEAR_BITMASK_AS_ENUM(iCameraBitFlags,PSA_DRIVER_CAM_IS_CLEAR)
				
				CDEBUG1LN(DEBUG_PROSTITUTE, "[SET_CAM_BIT_TO_OCLUDED] - PSA_DRIVER_CAM_IS_OCCLUDED")

			ENDIF
		BREAK
		
		//Not possible
		
		CASE XCAM_REAR_WINDSHEILD
		
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_REAR_CAM_IS_OCCLUDED)
				SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_REAR_CAM_IS_OCCLUDED)
				CLEAR_BITMASK_AS_ENUM(iCameraBitFlags,PSA_REARCAM_IS_CLEAR)
			ENDIF
			
		BREAK
		
		
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
#IF SHOW_CAM_DEBUG_INFO
		
PROC DRAW_SEX_CAMERAS_POS()
	VECTOR vCameraPos[XCAM_TOTAL]
	
	vCameraPos[XCAM_FRONT_WINDSHEILD]	 = 	GET_POSITION_FOR_SEX_CAM(XCAM_FRONT_WINDSHEILD)
	vCameraPos[XCAM_PASSENGER_WINDOW] 	= 	GET_POSITION_FOR_SEX_CAM(XCAM_PASSENGER_WINDOW)
	vCameraPos[XCAM_DRIVER_WINDOW] 		= 	GET_POSITION_FOR_SEX_CAM(XCAM_DRIVER_WINDOW)
	vCameraPos[XCAM_REAR_WINDSHEILD] 	= 	GET_POSITION_FOR_SEX_CAM(XCAM_REAR_WINDSHEILD)

	DRAW_DEBUG_SPHERE(vCameraPos[XCAM_FRONT_WINDSHEILD],CONST_fCapsuleRadius,0,0,255,60)
	DRAW_DEBUG_SPHERE(vCameraPos[XCAM_PASSENGER_WINDOW],CONST_fCapsuleRadius,0,0,255,60)
	DRAW_DEBUG_SPHERE(vCameraPos[XCAM_DRIVER_WINDOW],CONST_fCapsuleRadius,0,0,255,60)
	DRAW_DEBUG_SPHERE(vCameraPos[XCAM_REAR_WINDSHEILD],CONST_fCapsuleRadius,0,0,255,60)
ENDPROC
#ENDIF
#ENDIF

FUNC BOOL IS_THEIR_ROOM_FOR_SEX_CAMERAS(SEX_CAMERA_POSITIONS tWindow)

	VECTOR vPosCameraForTest = 	GET_POSITION_FOR_SEX_CAM(tWindow)
	VECTOR vPosPlayersVehicle = GET_ENTITY_COORDS(iPlayersVehicle)
	
//	IF (GET_GAME_TIMER() > iNextShapeTest)
		IF (stCameraProbe[tWindow] = NULL)
			
			IF GET_ENTITY_MODEL(iPlayersVehicle) = TYRANT
				stCameraProbe[tWindow] = START_SHAPE_TEST_SWEPT_SPHERE(vPosCameraForTest, vPosPlayersVehicle, 0.10,SCRIPT_INCLUDE_ALL /*SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_MOVER*/, iPlayersVehicle, SCRIPT_SHAPETEST_OPTION_IGNORE_SEE_THROUGH | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)
			ELIF GET_ENTITY_MODEL(iPlayersVehicle) = CASCO
			OR GET_ENTITY_MODEL(iPlayersVehicle) = FELTZER3
			OR GET_ENTITY_MODEL(iPlayersVehicle) = FAGALOA
			OR GET_ENTITY_MODEL(iPlayersVehicle) = WARRENER2
				stCameraProbe[tWindow] = START_SHAPE_TEST_SWEPT_SPHERE(vPosCameraForTest, vPosPlayersVehicle, 0.15,SCRIPT_INCLUDE_ALL /*SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_MOVER*/, iPlayersVehicle, SCRIPT_SHAPETEST_OPTION_IGNORE_SEE_THROUGH | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)
			ELSE
				stCameraProbe[tWindow] = START_SHAPE_TEST_SWEPT_SPHERE(vPosCameraForTest, vPosPlayersVehicle, CONST_fCapsuleRadius,SCRIPT_INCLUDE_ALL /*SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_MOVER*/, iPlayersVehicle, SCRIPT_SHAPETEST_OPTION_IGNORE_SEE_THROUGH | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)
			ENDIF
			CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] Shapee Test started for ", ENUM_TO_INT(tWindow))
		ELSE
			INT iHitResult
			VECTOR retPos, vRetNormal
			ENTITY_INDEX hitEntity
			
			SWITCH GET_SHAPE_TEST_RESULT(stCameraProbe[tWindow], iHitResult, retPos, vRetNormal, hitEntity)
			
				CASE SHAPETEST_STATUS_RESULTS_READY
					CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] Test is ready #", ENUM_TO_INT(tWindow), " Result = ", iHitResult)
					
					// Delay our next test.
//					iNextShapeTest = GET_GAME_TIMER() + 1500
					stCameraProbe[tWindow] = NULL
					
					IF iHitResult != 0
						CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] - Something was hit at: ",retPos )
						SET_CAM_BIT_TO_OCCLUDED(tWindow)
						RETURN FALSE
					ELSE
						CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] Nothing hit: ", retPos)
						RETURN TRUE
					ENDIF
				BREAK
				
				CASE SHAPETEST_STATUS_RESULTS_NOTREADY
					CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] Shapee Not Ready For ", ENUM_TO_INT(tWindow))
				BREAK
				
				CASE SHAPETEST_STATUS_NONEXISTENT
					stCameraProbe[tWindow] = NULL
					CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] NON EXISTENTRecreating Test for  ", ENUM_TO_INT(tWindow))
				BREAK
				
			ENDSWITCH
		ENDIF
//	ELSE
//		CDEBUG1LN(DEBUG_PROSTITUTE, "[Cam Occlusion] Game Timer not ready  ", ENUM_TO_INT(tWindow))
//	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE: Runs the capsule shape tests on the driver /passenger/ front cameras for prostitution
///    
PROC CHECK_NON_CRITICAL_SEX_CAMS_FOR_OCCLUSION()
	#IF NOT IS_JAPANESE_BUILD
	#IF IS_DEBUG_BUILD
	#IF SHOW_CAM_DEBUG_INFO
		TEXT_LABEL_31 sCamInfoFront
		sCamInfoFront = "Front Cam = "
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONTCAM_IS_CLEAR)
			sCamInfoFront += " CLEAR"
		ELIF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
			sCamInfoFront += " OCCLUDED"
		ELSE
			sCamInfoFront += " TESTING..."
		ENDIF
		
		TEXT_LABEL_31 sCamInfoSide
		sCamInfoSide = "Pass Cam = "
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_CLEAR)
			sCamInfoSide += " CLEAR"
		ELIF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
			sCamInfoSide += " OCCLUDED"
		ELSE
			sCamInfoSide += " TESTING..."
		ENDIF
		
		TEXT_LABEL_31 sCamInfoDriver
		sCamInfoDriver = "Driver Cam = "
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_CLEAR)
			sCamInfoDriver += " CLEAR"
		ELIF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
			sCamInfoDriver += " OCCLUDED"
		ELSE
			sCamInfoDriver += " TESTING..."
		ENDIF
		
		DRAW_DEBUG_TEXT_2D(sCamInfoFront, <<0.8,0.5,0.0>>)
		DRAW_DEBUG_TEXT_2D(sCamInfoSide, <<0.8,0.52,0.0>>)
		DRAW_DEBUG_TEXT_2D(sCamInfoDriver, <<0.8,0.54,0.0>>)
	#ENDIF
	#ENDIF
	
	
	IF iNumOfTimesGettingOff = 0	
		
	//	IF NOT IS_THIS_MP_PROSTITUTION()
			//Front Cam
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONTCAM_IS_CLEAR)
			AND NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
				IF IS_THEIR_ROOM_FOR_SEX_CAMERAS(XCAM_FRONT_WINDSHEILD)
					SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_FRONTCAM_IS_CLEAR)
				ENDIF
			ENDIF
			
			//Driver Cam
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_CLEAR)
			AND NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
				IF IS_THEIR_ROOM_FOR_SEX_CAMERAS(XCAM_DRIVER_WINDOW)
					SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_DRIVER_CAM_IS_CLEAR)
				ENDIF
			ENDIF				
		
			//Passenger Cam
			IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_CLEAR)
			AND NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
				IF IS_THEIR_ROOM_FOR_SEX_CAMERAS(XCAM_PASSENGER_WINDOW)
					SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_PASSENGER_CAM_IS_CLEAR)
				ENDIF
			ENDIF
	//	ENDIF
		
	ENDIF
	#ENDIF	//Japanese Build
ENDPROC

FUNC BOOL WOULD_SEX_CAMERAS_BE_OCCLUDED()

	SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_REAR_CAM_TEST_STARTED)
	
	//Always check rear for MP, JAPANSE_BUILD and SP
	IF NOT IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_REARCAM_IS_CLEAR)
		IF IS_THEIR_ROOM_FOR_SEX_CAMERAS(XCAM_REAR_WINDSHEILD)
			SET_BITMASK_AS_ENUM(iCameraBitFlags,PSA_REARCAM_IS_CLEAR)
			CLEAR_BITMASK_AS_ENUM(iCameraBitFlags,PSA_REAR_CAM_IS_OCCLUDED)
		ENDIF
		RETURN TRUE
	ELSE		
		CHECK_NON_CRITICAL_SEX_CAMS_FOR_OCCLUSION()
	ENDIF
	//Rear camera HAS TO BE CLEAR, OTHERWISE NO OTHER LOGIC WORKS!!!! What can I say , 1 week to final submission lockdown.	
	RETURN FALSE
	
ENDFUNC

PROC INITIALIZE_SEX_CAMS_WITH_BONE_CHECKS(INT &iCurrentCamIndex)
		
	FLOAT fHeadingFromDrvCamToWindow 
	FLOAT fHeadingFromPassCamToWindow

	//GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD,<<0.0,0.0,0.0>>)
	
	fHeadingFromDrvCamToWindow = GET_HEADING_BETWEEN_VECTORS_2D(GET_POSITION_FOR_SEX_CAM(XCAM_DRIVER_WINDOW), GET_WORLD_POSITION_OF_ENTITY_BONE(iPlayersVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(iPlayersVehicle, "windscreen") ))
	fHeadingFromPassCamToWindow = GET_HEADING_BETWEEN_VECTORS_2D(GET_POSITION_FOR_SEX_CAM(XCAM_PASSENGER_WINDOW), GET_WORLD_POSITION_OF_ENTITY_BONE(iPlayersVehicle,GET_ENTITY_BONE_INDEX_BY_NAME(iPlayersVehicle, "windscreen" ) ))



	//NOW SET UP CAMERAS
	VECTOR vCarRotation = GET_ENTITY_ROTATION(iPlayersVehicle) 
	FLOAT fCarHeading = GET_ENTITY_HEADING(iPlayersVehicle)
	FLOAT fInverseHeading
	
//	FLOAT fCameraPitch[XCAM_TOTAL]
	
	IF fCarHeading >= 180
		fInverseHeading = fCarHeading - 180
	ELSE
		fInverseHeading = fCarHeading + 180
	ENDIF
	
	FLOAT fDriverSidePitch = -7 - vCarRotation.y
	
	FLOAT fFrontCamPitch = -5.0 - vCarRotation.x
	
	
	FLOAT fRearCamPitch= -2.5 +vCarRotation.x
	
	
	FLOAT fPassengerSidePitch = 0 - (0.7*vCarRotation.x) + (0.3 * vCarRotation.y)
	
	
	//Pitch-------------------------------------
	
	IF NOT DOES_CAM_EXIST(fpSexCam[XCAM_DRIVER_WINDOW].theCam)
		INIT_SWIVEL_CAMERA(fpSexCam[XCAM_DRIVER_WINDOW],GET_POSITION_FOR_SEX_CAM(XCAM_DRIVER_WINDOW),<<fDriverSidePitch,vCarRotation.x,fHeadingFromDrvCamToWindow >>,50.0)
	ENDIF
	
	IF NOT DOES_CAM_EXIST(fpSexCam[XCAM_PASSENGER_WINDOW].theCam)
		INIT_SWIVEL_CAMERA(fpSexCam[XCAM_PASSENGER_WINDOW],GET_POSITION_FOR_SEX_CAM(XCAM_PASSENGER_WINDOW),<<fPassengerSidePitch,-vCarRotation.y,fHeadingFromPassCamToWindow>>,37.3)
	ENDIF
	
	
	IF NOT DOES_CAM_EXIST(fpSexCam[XCAM_FRONT_WINDSHEILD].theCam)
		INIT_SWIVEL_CAMERA(fpSexCam[XCAM_FRONT_WINDSHEILD],GET_POSITION_FOR_SEX_CAM(XCAM_FRONT_WINDSHEILD),<<fFrontCamPitch ,-vCarRotation.y,fInverseHeading>>,50.0)
	ENDIF	

	IF NOT DOES_CAM_EXIST(fpSexCam[XCAM_REAR_WINDSHEILD].theCam)
		#IF IS_JAPANESE_BUILD // point it at the bumper		
			INIT_SWIVEL_CAMERA(fpSexCam[XCAM_REAR_WINDSHEILD],GET_POS_FOR_JAPANESE_REAR_CAMERA(),<<fRearCamPitch,vCarRotation.y,fCarHeading>>,60.0)
		#ENDIF
		
		#IF NOT IS_JAPANESE_BUILD
			INIT_SWIVEL_CAMERA(fpSexCam[XCAM_REAR_WINDSHEILD],GET_POSITION_FOR_SEX_CAM(XCAM_REAR_WINDSHEILD),<<fRearCamPitch,vCarRotation.y,fCarHeading>>,45.0)
		#ENDIF
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE,FALSE)
	
	SET_CAM_ACTIVE(fpSexCam[iCurrentCamIndex].theCam, TRUE)
	
	CDEBUG1LN(DEBUG_PROSTITUTE, "[MANAGE_SEX_ANIMS] - SETUP_SEX_CAMS at: <<0.0,-4.0, 1.0>>")
	
ENDPROC
//Check that all other cams are blocked
FUNC BOOL IS_ONLY_REAR_CAM_CLEAR()
	
	IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
	AND IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
	AND IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Rear cam will always be good
/// PARAMS:
///    iCurrentCamIndex - 
PROC SWITCH_TO_NEXT_CAM( INT &iCurrentCamIndex)
	
	//Process input in here
	IF iCurrentCamIndex < ENUM_TO_INT(XCAM_REAR_WINDSHEILD)
		iCurrentCamIndex++
	ELIF iCurrentCamIndex = ENUM_TO_INT(XCAM_REAR_WINDSHEILD)
		iCurrentCamIndex = ENUM_TO_INT(XCAM_FRONT_WINDSHEILD)
	ENDIF

	
	IF iCurrentCamIndex = ENUM_TO_INT(XCAM_FRONT_WINDSHEILD)
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
			iCurrentCamIndex++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[SWITCH_TO_NEXT_CAM] - Front Cam is occluded forwarding along")
		ENDIF
	ENDIF
	
	IF iCurrentCamIndex = ENUM_TO_INT(XCAM_PASSENGER_WINDOW)
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED)
			iCurrentCamIndex++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[SWITCH_TO_NEXT_CAM] - Passenger Cam is occluded forwarding along")
		ENDIF
	ENDIF
	
	IF iCurrentCamIndex = ENUM_TO_INT(XCAM_DRIVER_WINDOW)
		IF IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
			iCurrentCamIndex++
			CDEBUG1LN(DEBUG_PROSTITUTE, "[SWITCH_TO_NEXT_CAM] - Driver Cam is occluded forwarding along")
		ENDIF
	ENDIF
	
ENDPROC
/// PURPOSE:
///    *Based off UPDATE_FIRST_PERSON_CAMERA from X:\gta5\script\dev\multiplayer\include\public\net_fps_cam.sch
///    Wrote my own version to avoid using the FIRST_PERSON_CAM_STRUCT that takes up too much stack space.
PROC HANDLE_SEX_CAM_PANNING(SWIVEL_CAM_STRUCT &thisSwivelCam)
	//Analogue stick axis indexes.
	CONST_INT FPC_LEFT_X	0
	CONST_INT FPC_LEFT_Y	1
	CONST_INT FPC_RIGHT_X	2
	CONST_INT FPC_RIGHT_Y	3

	INT iStickPosition[4]
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	//Get analogue stick positions.
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iStickPosition[FPC_LEFT_X],
									 	 iStickPosition[FPC_LEFT_Y],
									 	 iStickPosition[FPC_RIGHT_X],
									 	 iStickPosition[FPC_RIGHT_Y])
	
	//Update camera rotation stick target based on right stick.
	VECTOR vCamRotStickOffsetTarget
	vCamRotStickOffsetTarget.z = -(TO_FLOAT(iStickPosition[FPC_RIGHT_X])/127)* CONST_iSexCamLookXLimit
	vCamRotStickOffsetTarget.y = -vCamRotStickOffsetTarget.z*CONST_iSexCamRollLimit/CONST_iSexCamLookXLimit
	IF IS_LOOK_INVERTED()
		vCamRotStickOffsetTarget.x = (TO_FLOAT(iStickPosition[FPC_RIGHT_Y])/127)*CONST_iSexCamLookYLimit
	ELSE
		vCamRotStickOffsetTarget.x = -(TO_FLOAT(iStickPosition[FPC_RIGHT_Y])/127)*CONST_iSexCamLookYLimit
	ENDIF
										 
	//*** Steps all camera interpolated values towards their target values. ***
	//Scale by timestep to ensure this is frame rate independent.
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
	
	//Interpolate camera rotation offset. Limit rotation to max 3 degrees per frame.
	VECTOR vCamRotOffsetTarget = vCamRotStickOffsetTarget //+ thisSwivelCam.vCamRotFocusOffsetTarget
	
	// PC mouse needs slightly different treatment from gamepad. Steve R LDS. 24/JUN/2014
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		thisSwivelCam.vCamRotOffsetCurrent.x = CLAMP(thisSwivelCam.vCamRotOffsetCurrent.x + vCamRotOffsetTarget.x * SWVL_ROT_INTERP_MOUSE, CONST_fMinPanLimit, CONST_fMaxPanLimit)
		thisSwivelCam.vCamRotOffsetCurrent.y = CLAMP(thisSwivelCam.vCamRotOffsetCurrent.y + vCamRotOffsetTarget.y * SWVL_ROT_INTERP_MOUSE, CONST_fMinPanLimit, CONST_fMaxPanLimit)
		thisSwivelCam.vCamRotOffsetCurrent.z = CLAMP(thisSwivelCam.vCamRotOffsetCurrent.z + vCamRotOffsetTarget.z * SWVL_ROT_INTERP_MOUSE, CONST_fMinPanLimit, CONST_fMaxPanLimit)
	ELSE
		thisSwivelCam.vCamRotOffsetCurrent.x += CLAMP((vCamRotOffsetTarget.x - thisSwivelCam.vCamRotOffsetCurrent.x) * SWVL_ROT_INTERP * fFrameRateModifier, CONST_fMinPanLimit, CONST_fMaxPanLimit)
		thisSwivelCam.vCamRotOffsetCurrent.y += CLAMP((vCamRotOffsetTarget.y - thisSwivelCam.vCamRotOffsetCurrent.y) * SWVL_ROT_INTERP * fFrameRateModifier, CONST_fMinPanLimit, CONST_fMaxPanLimit)
		thisSwivelCam.vCamRotOffsetCurrent.z += CLAMP((vCamRotOffsetTarget.z - thisSwivelCam.vCamRotOffsetCurrent.z) * SWVL_ROT_INTERP * fFrameRateModifier, CONST_fMinPanLimit, CONST_fMaxPanLimit)
	ENDIF
	

	//Update the camera with interpolated values.
	SET_CAM_ROT(thisSwivelCam.theCam, thisSwivelCam.vInitCamRot + thisSwivelCam.vCamRotOffsetCurrent)
ENDPROC

PROC TOGGLE_SEX_CAM_POSITIONS(INT &iCurrentCamIndex)
	//Exits--------------------------------
	
	IF NOT DOES_CAM_EXIST(fpSexCam[iCurrentCamIndex].theCam)
		CDEBUG3LN(DEBUG_PROSTITUTE, "[TOGGLE_SEX_CAM_POSITIONS] - Cam doesn't exist")
		EXIT
	ENDIF
	
	IF NOT IS_CAM_RENDERING(fpSexCam[iCurrentCamIndex].theCam)
		CDEBUG3LN(DEBUG_PROSTITUTE, "[TOGGLE_SEX_CAM_POSITIONS] - Cam not rendering")
		EXIT
	ENDIF
	
	//Add Camera Shake
	SHAKE_CAM(fpSexCam[iCurrentCamIndex].theCam, "HAND_SHAKE", 0.25)

	
	
	//---------------------------------------------------------------------------------
	//Only NON - JPN version has multiple cameas B*1342474
	#IF NOT IS_JAPANESE_BUILD
		#IF IS_DEBUG_BUILD
		#IF SHOW_CAM_DEBUG_INFO
			VECTOR vCarsRotation = GET_ENTITY_ROTATION(iPlayersVehicle)
			TEXT_LABEL_31 sDebugCarRotation
			sDebugCarRotation = "Car Rot: x: "
			sDebugCarRotation += ROUND(vCarsRotation.x)
			sDebugCarRotation += " y: "
			sDebugCarRotation += ROUND(vCarsRotation.y)
			sDebugCarRotation += " z: "
			sDebugCarRotation += ROUND(vCarsRotation.z)
			DRAW_DEBUG_TEXT_2D(sDebugCarRotation,<<0.06,0.4,0.0>>)
			
			VECTOR vCamRotation = GET_CAM_ROT(fpSexCam[iCurrentCamIndex].theCam)
			TEXT_LABEL_31 sDebugCamRotation
			sDebugCamRotation = "Cam Rot: x: "
			sDebugCamRotation += ROUND(vCamRotation.x)
			sDebugCamRotation += " y: "
			sDebugCamRotation += ROUND(vCamRotation.y)
			sDebugCamRotation += " z: "
			sDebugCamRotation += ROUND(vCamRotation.z)
			DRAW_DEBUG_TEXT_2D(sDebugCamRotation,<<0.06,0.42,0.0>>)
			
			
			VECTOR vModelMin, vModelMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(iPlayersVehicle), vModelMin, vModelMax)
			
			TEXT_LABEL_31 sDebugCarWidth
			sDebugCarWidth = "Width Min x: "
			sDebugCarWidth += ROUND(vModelMin.x)
			sDebugCarWidth += " Max x: "
			sDebugCarWidth += ROUND(vModelMax.x)
			DRAW_DEBUG_TEXT_2D(sDebugCarWidth,<<0.06,0.44,0.0>>)
			
			TEXT_LABEL_31 sDebugCarLength
			sDebugCarLength = "Length Min y: "
			sDebugCarLength += ROUND(vModelMin.y)
			sDebugCarLength += " Max y: "
			sDebugCarLength += ROUND(vModelMax.y)
			DRAW_DEBUG_TEXT_2D(sDebugCarLength,<<0.06,0.46,0.0>>)
			
			TEXT_LABEL_31 sDebugCarHeight
			sDebugCarHeight = "Length Min z: "
			sDebugCarHeight += ROUND(vModelMin.z)
			sDebugCarHeight += " Max z: "
			sDebugCarHeight += ROUND(vModelMax.z)
			DRAW_DEBUG_TEXT_2D(sDebugCarHeight,<<0.06,0.48,0.0>>)
		#ENDIF
		#ENDIF
		
		//Tilt Control	
		IF NOT IS_PAUSE_MENU_ACTIVE()
			HANDLE_SEX_CAM_PANNING(fpSexCam[iCurrentCamIndex])
		ENDIF
		
		IF IS_ONLY_REAR_CAM_CLEAR()
			IF NOT IS_THIS_MP_PROSTITUTION()
				//Help = "Move camera around with R-Stick"
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_CAM_OC")
					PRINT_HELP_FOREVER("PROS_CAM_OC")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_THIS_MP_PROSTITUTION()
				//Help = "Press ~INPUT_SCRIPT_SELECT~ to switch cameras for a better view."
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_CAM_TOG")
					PRINT_HELP_FOREVER("PROS_CAM_TOG")
				ENDIF
			ENDIF
			
			//Press SELECT to TOGGLE
			//SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_NEXT_CAMERA) 
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_NEXT_CAMERA) 
			AND NOT IS_PAUSE_MENU_ACTIVE()	
			AND NOT IS_ANY_FIRST_PERSON_CAM_ACTIVE(FALSE)
				SWITCH_TO_NEXT_CAM(iCurrentCamIndex)
				
				SET_CAM_ACTIVE(fpSexCam[iCurrentCamIndex].theCam, TRUE)
				
			
				CDEBUG1LN(DEBUG_PROSTITUTE, "[TOGGLE_SEX_CAM_POSITIONS] - iCurrentCamIndex = ", iCurrentCamIndex)
			ENDIF
		ENDIF
	#ENDIF
	
	
ENDPROC
