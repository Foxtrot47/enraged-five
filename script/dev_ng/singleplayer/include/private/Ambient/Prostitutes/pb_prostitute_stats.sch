USING "net_leaderboards.sch"

INT iNumBlowJobs
INT iNumFullService
BOOl bUpdateProsLeaderboard = FALSE

FUNC BOOL IS_PROSTITUTE_LEADERBOARD_AVAILABLE_FOR_WRITE()
	IF  NETWORK_IS_SIGNED_ONLINE() AND NETWORK_IS_SIGNED_IN() AND NETWORK_IS_CLOUD_AVAILABLE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PROSTITUTE_STAT_SCORE()
	RETURN 0
ENDFUNC

FUNC INT GET_NUM_BLOW_JOBS()
	RETURN iNumBlowJobs
ENDFUNC

FUNC INT GET_NUM_FULL_SERVICE()
	RETURN iNumFullService
ENDFUNC

//Prostiute leaderboards
PROC PROSTITUTE_WRITE_TO_LEADERBOARD()

	IF NOT IS_PROSTITUTE_LEADERBOARD_AVAILABLE_FOR_WRITE()
		EXIT //not online
	ENDIF
	
	IF NOT bUpdateProsLeaderboard
		EXIT
	ENDIF
	
//ID: 275 - LEADERBOARD_MINI_GAMES_PROSTITUTES
//Inputs: 9
//	LB_INPUT_COL_SCORE (long)
//	LB_INPUT_COL_NUM_SEX_ACT1 (int)
//	LB_INPUT_COL_NUM_SEX_ACT2 (int)
//	LB_INPUT_COL_NUM_SEX_ACT3 (int)
//	LB_INPUT_COL_NUM_SEX_ACT4 (int)
//	LB_INPUT_COL_NUM_SEX_ACT5 (int)
//	LB_INPUT_COL_NUM_SEX_ACT6 (int)
//	LB_INPUT_COL_NUM_SEX_ACT7 (int)
//	LB_INPUT_COL_NUM_SEX_ACT8 (int)
//Columns: 9
//	SCORE_COLUMN ( AGG_Sum ) - InputId: LB_INPUT_COL_SCORE
//	NUMBER_TIMES_SEX_1 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT1
//	NUMBER_TIMES_SEX_2 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT2
//	NUMBER_TIMES_SEX_3 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT3
//	NUMBER_TIMES_SEX_4 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT4
//	NUMBER_TIMES_SEX_5 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT5
//	NUMBER_TIMES_SEX_6 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT6
//	NUMBER_TIMES_SEX_7 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT7
//	NUMBER_TIMES_SEX_8 ( AGG_Sum ) - InputId: LB_INPUT_COL_NUM_SEX_ACT8
//Instances: 3
//	GameType,Type
//	GameType,Type,Challenge
//	GameType,Type,ScEvent

	TEXT_LABEL_31 categoryNames[2] 
    TEXT_LABEL_23 uniqueIdentifiers[2]
    categoryNames[0] = "GameType" 
	categoryNames[1] = "Type" 
    
	// Begin setting the leaderboard data types
	uniqueIdentifiers[0] = "SP"	
	uniqueIdentifiers[1] = "????" 
	
	IF INIT_LEADERBOARD_WRITE(LEADERBOARD_MINI_GAMES_PROSTITUTES, uniqueIdentifiers, categoryNames, 2)
	
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,  			GET_PROSTITUTE_STAT_SCORE(), 	0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_SEX_ACT1,  	GET_NUM_BLOW_JOBS(), 			0.0)
		LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_SEX_ACT2, 	GET_NUM_FULL_SERVICE(), 		0.0)

	ENDIF
ENDPROC

PROC INC_NUM_BLOW_JOBS()
	iNumBlowJobs++
	bUpdateProsLeaderboard = TRUE
ENDPROC

PROC INC_NUM_FULL_SERVICE()
	iNumFullService++
	bUpdateProsLeaderboard = TRUE
ENDPROC

//Separate from Leaderboard Stats---------------------------------------------
FUNC INT GET_NUM_PROSTITUTE_SERVICES_SOLICITED_BY_PLAYER(enumCharacterList ePlayerEnum)
	
	SWITCH ePlayerEnum
	
		CASE CHAR_MICHAEL
			RETURN g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_MICHAEL]
		BREAK
		
		CASE CHAR_FRANKLIN
			RETURN g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_FRANKLIN]
		BREAK
		
		CASE CHAR_TREVOR
			RETURN  g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_TREVOR]
		BREAK
		
	ENDSWITCH
	
	RETURN 0		

ENDFUNC

PROC INC_NUM_PROSTITUTE_SERVICES_SOLICITED_BY_PLAYER()
	SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	
		CASE CHAR_MICHAEL
			g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_MICHAEL]++
			INCREMENT_PLAYER_PED_STAT(CHAR_MICHAEL,PS_STAMINA,1)
		
		BREAK
		
		CASE CHAR_FRANKLIN
			g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_FRANKLIN]++
 			INCREMENT_PLAYER_PED_STAT(CHAR_FRANKLIN,PS_STAMINA,1)

		BREAK
		
		CASE CHAR_TREVOR
			g_savedGlobals.sAmbient.iNumTimesPlayerPaidForProstituteServices[CHAR_TREVOR]++
 			INCREMENT_PLAYER_PED_STAT(CHAR_TREVOR,PS_STAMINA,1)

		BREAK
		
		CASE CHAR_MULTIPLAYER
			INCREMENT_PLAYER_PED_STAT(CHAR_MULTIPLAYER,PS_STAMINA,1)
		BREAK
		
	ENDSWITCH
ENDPROC


//EOF
