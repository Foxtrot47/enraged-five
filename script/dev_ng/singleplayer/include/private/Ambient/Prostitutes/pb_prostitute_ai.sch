USING "pb_prostitute_player.sch"

// *** ai punter functions *** //
PROC STOP_THE_CUSTOMERS_VEHICLE()
	
	VECTOR prostitute_current_pos
	
//	DRIVINGMODE cartDrivingMode = DF_StopForCars | DF_SteerAroundPeds | DF_UseShortCutLinks

	//FLOAT  punter_VEHICLE_speed
	IF NOT IS_PED_INJURED(customer_ped)
		IF IS_VEHICLE_DRIVEABLE(customer_VEHICLE)
			IF IS_PED_IN_VEHICLE(customer_ped, customer_VEHICLE)
				IF IS_VEHICLE_SUITABLE(customer_VEHICLE)
				
				
					IF IS_COP_IN_PROSTITUTE_AREA(GET_ENTITY_COORDS(customer_ped))
						EXIT
					ENDIF
				
				// if player relatively close
					//IF IS_ENTITY_AT_COORD(prostitute_ped, customer_ped, 10.0, 10.0, 5.0, FALSE)							
						// check on VEHICLE speed							
						//punter_VEHICLE_speed = GET_ENTITY_SPEED(customer_VEHICLE)
						prostitute_current_pos = GET_ENTITY_COORDS(prostitute_ped)	
						
						IF GET_CLOSEST_VEHICLE_NODE (prostitute_current_pos, prostitute_current_pos, NF_IGNORE_SWITCHED_OFF_DEADENDS)
							
							SET_DRIVER_ABILITY(customer_ped, 1.0)
							SET_DRIVER_RACING_MODIFIER(customer_ped, 0.2)
							SET_DRIVER_AGGRESSIVENESS(customer_ped, 0.05)
							
							#IF IS_DEBUG_BUILD
								IF IS_PROSTITUTE_FORCE_TO_USE_RANDOM()
									CREATE_CUSTOMER_BLIP()
	 								SET_CUSTOMER_BLIP_COLOUR(BLIP_COLOUR_BLUE)
									
									CREATE_PROSTITUTE_BLIP()
									SET_PROSITUTE_BLIP_COLOUR(BLIP_COLOUR_RED) 
								ENDIF
							#ENDIF
							// have prostitute approach the player
							CDEBUG1LN(DEBUG_PROSTITUTE, "[AI] Parking at: ", prostitute_current_pos)
							TASK_VEHICLE_PARK(customer_ped, customer_VEHICLE, prostitute_current_pos, 0, PARK_TYPE_PULL_OVER, 360,TRUE) 
				
							TASK_LOOK_AT_ENTITY(customer_ped, prostitute_ped, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							
							// have prostitute go towards the VEHICLE
							PROST_PB_DEBUG_MESSAGE("OTHER TOLD TO PULL OVER")						
							eCurrentProstituteState = prostitute_APPROACHING_CUSTOMER
						ENDIF					
					//ENDIF
				ELSE
					PROST_PB_DEBUG_MESSAGE("EXIT - Vehicle is not suitable")					
					SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
				ENDIF
			ELSE
				PROST_PB_DEBUG_MESSAGE("EXIT - OTHER OUT OF THE VEHICLE")					
				SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
			ENDIF
		ELSE
			PROST_PB_DEBUG_MESSAGE("EXIT - OTHER VEHICLE DEAD")					
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("EXIT - OTHER ped INJURED")			
		SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
	ENDIF
ENDPROC

PROC MAKE_PROSTITUTE_GO_TOWARDS_AI_VEHICLE()
			
	FLOAT current_VEHICLE_speed		
	VECTOR max_pos_of_model
	VECTOR min_pos_of_model
	VECTOR model_dimensions
	VECTOR door_pos
	VECTOR temp_vector
	VECTOR prostitute_current_pos
	
	IF NOT IS_PED_INJURED (customer_ped)
		IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
			IF IS_PED_IN_VEHICLE (customer_ped, customer_VEHICLE)					
				// if prostitute at the player						
				current_VEHICLE_speed = GET_ENTITY_SPEED(customer_VEHICLE)						

				IF GET_SCRIPT_TASK_STATUS (customer_ped, SCRIPT_TASK_VEHICLE_PARK) = FINISHED_TASK							
				OR current_VEHICLE_speed <= 1.0								

					GET_MODEL_DIMENSIONS (GET_ENTITY_MODEL(customer_VEHICLE), min_pos_of_model, max_pos_of_model)
					model_dimensions = max_pos_of_model - min_pos_of_model
					model_dimensions =  model_dimensions /2.0

					//door_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(customer_VEHICLE, -(model_dimensions.x+ 0.2))
					door_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(customer_VEHICLE, <<0.2,0.2,0.0>>)
					// find if there is a closer door?
					//temp_vector = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(customer_VEHICLE, (model_dimensions.x +  0.2), 0.0, 0.0 )
					temp_vector = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(customer_VEHICLE, <<0.2,0.2,0.0>> )
					prostitute_current_pos = GET_ENTITY_COORDS(prostitute_ped)						
					IF (VDIST(prostitute_current_pos, temp_vector) < VDIST(prostitute_current_pos, door_pos))
						door_pos = temp_vector
					ENDIF
					
					OPEN_SEQUENCE_TASK (prostitute_seq)				
						
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<door_pos.x, door_pos.y, door_pos.z>>, PEDMOVE_WALK, -1, 0.5,ENAV_SUPPRESS_EXACT_STOP)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, customer_ped)

						TASK_STAND_STILL(NULL, -1)
						CLOSE_SEQUENCE_TASK(prostitute_seq)
						TASK_PERFORM_SEQUENCE(prostitute_ped, prostitute_seq)
					CLEAR_SEQUENCE_TASK(prostitute_seq)
					
					PROST_PB_DEBUG_MESSAGE("prostitute ASKING OTHER FOR MONEY BY VEHICLE")											
					eCurrentProstituteState = prostitute_ASKING_FOR_MONEY				
				ELSE
					PROST_PB_DEBUG_MESSAGE("WAITING FOR CUTSTOMER FINSIH TASK OR STOP")					
				ENDIF
			ELSE
				PROST_PB_DEBUG_MESSAGE("EXIT - OTHER OUT OF VEHICLE")					
				REMOVE_NON_PLAYER_CUSTOMER()
			ENDIF
		ELSE
			PROST_PB_DEBUG_MESSAGE("EXIT - OTHER VEHICLE DEAD")				
			REMOVE_NON_PLAYER_CUSTOMER()
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("EXIT - OTHER ped INJURED")			
		REMOVE_NON_PLAYER_CUSTOMER()
	ENDIF
ENDPROC

PROC PROSTITUTE_ASKING_AI_FOR_CASH()
	FLOAT current_VEHICLE_speed
	IF NOT IS_PED_INJURED (customer_ped)
		IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
			IF IS_PED_IN_VEHICLE (customer_ped, customer_VEHICLE)

				// if finished asking for cash
				IF GET_SCRIPT_TASK_STATUS (prostitute_ped, SCRIPT_TASK_PERFORM_SEQUENCE)	 = PERFORMING_TASK	
				
					IF GET_SEQUENCE_PROGRESS(prostitute_ped) > 1 // stand still 						
				
						IF IS_ENTITY_AT_ENTITY(prostitute_ped, customer_ped, <<20.0, 20.0, 10.0>>, FALSE)
							current_VEHICLE_speed = GET_ENTITY_SPEED(customer_VEHICLE)
							IF current_VEHICLE_speed <= 1.0
								/// have prostitute follow the player
								PLAY_PROSTITUTE_ASK_PLAYER_FOR_A_RIDE(FALSE)
								
								/// start the clock								
								timer_start = GET_GAME_TIMER()							
								random_wait_time = GET_RANDOM_INT_IN_RANGE (4000, 8000)
							
								PROST_PB_DEBUG_MESSAGE("prostitute proposition other in VEHICLE VEHICLE")								
								eCurrentProstituteState = prostitute_WAITING_FOR_ANSWER
							ENDIF
						ELSE
							PROST_PB_DEBUG_MESSAGE("EXIT - OTHER IN VEHICLE OUT OF RANGE")						
							REMOVE_NON_PLAYER_CUSTOMER()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PROST_PB_DEBUG_MESSAGE("EXIT - OTHER OUT OF THE VEHICLE")					
				REMOVE_NON_PLAYER_CUSTOMER()
			ENDIF
		ELSE
			PROST_PB_DEBUG_MESSAGE("EXIT - OTHER VEHICLE DEAD")					
			REMOVE_NON_PLAYER_CUSTOMER()
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("EXIT - OTHER ped INJURED")			
		REMOVE_NON_PLAYER_CUSTOMER()
	ENDIF
ENDPROC

PROC PROSTITUTE_WAITING_FOR_ANSWER_FROM_AI()
	FLOAT current_VEHICLE_speed	
	
	IF NOT IS_PED_INJURED (customer_ped)
		IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
			IF IS_PED_IN_VEHICLE (customer_ped, customer_VEHICLE)

				current_VEHICLE_speed = GET_ENTITY_SPEED(customer_VEHICLE)

				IF IS_ENTITY_AT_ENTITY(customer_ped, prostitute_ped, <<15.0, 15.0, 5.0>>, FALSE)
				AND current_VEHICLE_speed <= 1.0		
					
					SET_PROSTITUTE_PROPOSITION_SETTINGS(prostitute_ped)
					
					timer_current = GET_GAME_TIMER()
					timer_difference = timer_current - timer_start
					IF timer_difference >= random_wait_time
					
						IF IS_THIS_MP_PROSTITUTION()
							IF NETWORK_HAS_CONTROL_OF_ENTITY( customer_ped )
								TASK_CLEAR_LOOK_AT(customer_ped)
							ENDIF
						ELSE
							TASK_CLEAR_LOOK_AT(customer_ped)
						ENDIF
						
						/// should ped accept or decline
						random_wait_time = GET_RANDOM_INT_IN_RANGE (0, 10000) 
						IF random_wait_time  > 2500  //3 in 4 chance that he'll accept
														
							/// customer accepts, prostitute has to get in the vehicle
							CLEAR_PED_TASKS(prostitute_ped)		
							CLEAR_SEQUENCE_TASK(prostitute_seq)
							OPEN_SEQUENCE_TASK (prostitute_seq)
							//	SET_NEXT_DESIRED_MOVE_STATE (PEDMOVE_WALK)
								TASK_ENTER_VEHICLE(NULL, customer_VEHICLE, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK ,ECF_RESUME_IF_INTERRUPTED | ECF_DONT_JACK_ANYONE)											
								TASK_STAND_STILL(NULL, -1)
							CLOSE_SEQUENCE_TASK(prostitute_seq)
							
							TASK_PERFORM_SEQUENCE(prostitute_ped, prostitute_seq)
							CLEAR_SEQUENCE_TASK(prostitute_seq)
							
							PROST_PB_DEBUG_MESSAGE("other GIVES MONEY")
							eCurrentProstituteState = prostitute_GOT_MONEY
						ELSE
							// rejects	
							PLAY_DECLINED_SPEECH_FOR_SPECIFIC_PED()
							PLAY_PROSTITUTE_IS_REJECTED()
							
							// have guy drive off?
							IF NOT IS_ped_INJURED (customer_ped)
								IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
									IF IS_PED_IN_VEHICLE (customer_ped, customer_VEHICLE)										
									
										CLEAR_SEQUENCE_TASK(prostitute_seq)
										OPEN_SEQUENCE_TASK (prostitute_seq)
											TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(500, 2000))
											TASK_VEHICLE_DRIVE_WANDER ( NULL, customer_VEHICLE, 10.0, DRIVINGMODE_STOPFORCARS )//9
										CLOSE_SEQUENCE_TASK(prostitute_seq)
										
										TASK_PERFORM_SEQUENCE(customer_ped, prostitute_seq)
										CLEAR_SEQUENCE_TASK(prostitute_seq)
										timer_start = GET_GAME_TIMER()
										
										
									ENDIF
								ENDIF
							ENDIF
														
							PROST_PB_DEBUG_MESSAGE("prostitute REFUSED MONEY 2")
							
							IF IS_THIS_MP_PROSTITUTION()
								IF NETWORK_HAS_CONTROL_OF_ENTITY( customer_ped )
									TASK_CLEAR_LOOK_AT(customer_ped)
								ENDIF
							ELSE
								TASK_CLEAR_LOOK_AT(customer_ped)
							ENDIF
							eCurrentProstituteState = prostitute_REFUSED
						ENDIF
					ENDIF
				//ENDIF
				ELSE
					/// quit					
					IF IS_THIS_MP_PROSTITUTION()
						IF NETWORK_HAS_CONTROL_OF_ENTITY( customer_ped )
							TASK_CLEAR_LOOK_AT(customer_ped)
						ENDIF
					ELSE
						TASK_CLEAR_LOOK_AT(customer_ped)
					ENDIF
					REMOVE_NON_PLAYER_CUSTOMER()
				ENDIF
			ELSE
				PROST_PB_DEBUG_MESSAGE("EXIT - OTHER OUT OF THE VEHICLE")		
				IF IS_THIS_MP_PROSTITUTION()
					IF NETWORK_HAS_CONTROL_OF_ENTITY( customer_ped )
						TASK_CLEAR_LOOK_AT(customer_ped)
					ENDIF
				ELSE
					TASK_CLEAR_LOOK_AT(customer_ped)
				ENDIF
				REMOVE_NON_PLAYER_CUSTOMER()
			ENDIF
		ELSE
			PROST_PB_DEBUG_MESSAGE("EXIT - OTHER VEHICLE DEAD")				
			REMOVE_NON_PLAYER_CUSTOMER()
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("EXIT - OTHER ped INJURED")			
		REMOVE_NON_PLAYER_CUSTOMER()
	ENDIF
ENDPROC

PROC PROSTITUTE_GETTING_INTO_AI_VEHICLE()
	IF NOT IS_PED_INJURED (customer_ped)
		IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
			IF IS_PED_IN_VEHICLE (customer_ped, customer_VEHICLE)

				// if finished asking for cash
				IF GET_SCRIPT_TASK_STATUS (prostitute_ped, SCRIPT_TASK_PERFORM_SEQUENCE ) = PERFORMING_TASK	
			
					IF GET_SEQUENCE_PROGRESS(prostitute_ped) >0 // stand still 
						IF IS_PED_IN_VEHICLE(prostitute_ped, customer_VEHICLE)
							PROST_PB_DEBUG_MESSAGE("prostitute WAITING TO END   - DRIVING OFF (WANDER)")
							
							OPEN_SEQUENCE_TASK (prostitute_seq)
								TASK_VEHICLE_DRIVE_WANDER ( NULL, customer_VEHICLE, 10.0, DRIVINGMODE_STOPFORCARS )//9
							CLOSE_SEQUENCE_TASK(prostitute_seq)
							
							TASK_PERFORM_SEQUENCE(customer_ped, prostitute_seq)
							CLEAR_SEQUENCE_TASK(prostitute_seq)
							
							
							//Find Quiet Location to Use for Sex 
							vSavedQuietLocation = FIND_PROSTITUTE_CLOSEST_QUIET_LOCATION(GET_ENTITY_COORDS(prostitute_ped))
							CDEBUG1LN(DEBUG_PROSTITUTE,"*************Quiet Location = ", vSavedQuietLocation, " Distance = ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(customer_VEHICLE,vSavedQuietLocation))
							
							//Request proper anims
							REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_SEX_FOR_VEHICLE_PED_IS_IN(prostitute_ped))
			
							eCurrentProstituteState = PROSTITUTE_IN_VEHICLE
							timer_start = GET_GAME_TIMER ()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PROST_PB_DEBUG_MESSAGE("EXIT - OTHER OUT OF THE VEHICLE")					
				REMOVE_NON_PLAYER_CUSTOMER()
			ENDIF
		ELSE
			PROST_PB_DEBUG_MESSAGE("EXIT - OTHER VEHICLE DEAD")				
			REMOVE_NON_PLAYER_CUSTOMER()
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("EXIT - OTHER ped INJURED")			
		REMOVE_NON_PLAYER_CUSTOMER()
	ENDIF
ENDPROC


PROC PROSTITUTE_SERVING_AI()		
	
	SWITCH eProsServiceState
		CASE PROSTITUTE_SERVICE_INIT
			SET_SCRIPTED_ANIM_SEAT_OFFSET (prostitute_ped, CONST_fSeatOffset)			
			
			IF INIT_PROSTITUTE_SERVICE_ANIMS(customer_ped)
				RESTART_TIMER_NOW(tControl)					
				eProsServiceState = PROSTITUTE_SERVICE_LOOPING
			ENDIF
		BREAK
	
		CASE PROSTITUTE_SERVICE_LOOPING
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(prostitute_ped, FALSE)) < (150*150)
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2246277
			ENDIF
			
			MANAGE_VEHICLE_BOUNCING_DURING_SERVICE(customer_VEHICLE)
			
			IF HAS_PLAYER_TAKEN_PICTURE_OF_ENTITY(customer_ped)
				CREATE_CONVERSATION(cultresConversation, "pbproau", "pbpro_pic", CONV_PRIORITY_AMBIENT_HIGH)
				SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
			ENDIF
		
			IF (GET_TIMER_IN_SECONDS(tControl) > 1.0)
				IF GET_SCRIPT_TASK_STATUS(prostitute_ped, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
					CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_sex_anims_have_been_started)
					//KICK_PROSTITUTE_OUT_OF_VEHICLE()
						
					DEBUG_MESSAGE("Moving to PROSTITUTE_SERVICE_FINISHED")
					eProsServiceState = PROSTITUTE_SERVICE_FINISHED
				ENDIF
			ENDIF
		BREAK
		
		CASE PROSTITUTE_SERVICE_FINISHED
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
		BREAK
	
		CASE PROSTITUTE_SERVICE_CANCELLED
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
		BREAK
	
	ENDSWITCH	
ENDPROC
/// PURPOSE: Use this to generate the min/max values to use for REQUEST_PATH_NODES_IN_AREA_THIS_FRAME and ARE_NODES_LOADED_FOR_AREA 
///    
/// PARAMS:
///    iWhichParamter - 0 = MIN x | 1 = MIN y | 2 = MAX x | 3 MAX y
/// RETURNS:
///    
FUNC FLOAT GET_POS_FOR_NPC_DRIVE_TASK(INT iWhichParamter)
	
	VECTOR vProstitutePos = GET_ENTITY_COORDS(prostitute_ped)
	VECTOR vQuietLocation = vSavedQuietLocation
	
	IF iWhichParamter = 0
		IF vProstitutePos.x < vQuietLocation.x
			RETURN vProstitutePos.x - 20.0
		ELSE
			RETURN vQuietLocation.x - 20.0
		ENDIF
	
	ELIF iWhichParamter = 1
		IF vProstitutePos.y < vQuietLocation.y
			RETURN vProstitutePos.y - 20.0
		ELSE
			RETURN vQuietLocation.y - 20.0
		ENDIF
	
	ELIF iWhichParamter = 2
		IF vProstitutePos.x > vQuietLocation.x
			RETURN vProstitutePos.x + 20.0
		ELSE
			RETURN vQuietLocation.x + 20.0
		ENDIF
	
	ELSE
		IF vProstitutePos.y > vQuietLocation.y
			RETURN vProstitutePos.y + 20.0
		ELSE
			RETURN vQuietLocation.y + 20.0
		ENDIF
	ENDIF	
ENDFUNC


PROC OTHER_IN_VEHICLE_SECTION()
	
	// chekc if it should cancel
	IF eCurrentProstituteState > prostitute_IDLE
		IF IS_VEHICLE_DRIVEABLE (customer_VEHICLE)
			IF NOT IS_PED_INJURED (customer_ped)
				IF NOT IS_PED_IN_VEHICLE(customer_ped, customer_VEHICLE)
					// need a locate check?	
					REMOVE_NON_PLAYER_CUSTOMER()					
				ENDIF			
			ENDIF
		ENDIF
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(customer_ped)
	OR IS_ENTITY_DEAD(customer_ped)
		SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
	ENDIF
	
	SWITCH eCurrentProstituteState
		CASE PROSTITUTE_IDLE
			/// determine if player is the customer or use a nearby ped
			IF NOT (customer_type = CUSTOMER_NO_CUSTOMER)
				eCurrentProstituteState = prostitute_SPOTTED_CUSTOMER				
			ENDIF
		BREAK
		
		CASE PROSTITUTE_SPOTTED_CUSTOMER
			STOP_THE_CUSTOMERS_VEHICLE()			
		BREAK
		
		CASE PROSTITUTE_APPROACHING_CUSTOMER		
			MAKE_PROSTITUTE_GO_TOWARDS_AI_VEHICLE()			
		BREAK
		
		CASE PROSTITUTE_ASKING_FOR_MONEY
			PROSTITUTE_ASKING_AI_FOR_CASH()
		BREAK
		
		CASE PROSTITUTE_WAITING_FOR_ANSWER
			PROSTITUTE_WAITING_FOR_ANSWER_FROM_AI()
		BREAK
		
		CASE PROSTITUTE_GOT_MONEY
			PROSTITUTE_GETTING_INTO_AI_VEHICLE()			
		BREAK
		
		CASE PROSTITUTE_REFUSED
			IF NOT IS_PED_INJURED(customer_ped)
				// if finished asking for cash
				IF GET_SCRIPT_TASK_STATUS(prostitute_ped, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
					timer_current = GET_GAME_TIMER()
					timer_difference = timer_current - timer_start
					IF timer_difference >= 10000
					OR NOT IS_ENTITY_AT_ENTITY(prostitute_ped, customer_ped, <<12.0, 12.0, 5.0>>, FALSE)					
						PROST_PB_DEBUG_MESSAGE("prostitute WAITING TO END")						
						REMOVE_NON_PLAYER_CUSTOMER()					
					ENDIF
				ENDIF
			ELSE
				REMOVE_NON_PLAYER_CUSTOMER()
			ENDIF
		BREAK
		
		CASE PROSTITUTE_IN_VEHICLE
			
			IF NOT ARE_NODES_LOADED_FOR_AREA(GET_POS_FOR_NPC_DRIVE_TASK(0),GET_POS_FOR_NPC_DRIVE_TASK(1), GET_POS_FOR_NPC_DRIVE_TASK(2), GET_POS_FOR_NPC_DRIVE_TASK(3))
			
				REQUEST_PATH_NODES_IN_AREA_THIS_FRAME( GET_POS_FOR_NPC_DRIVE_TASK(0),GET_POS_FOR_NPC_DRIVE_TASK(1), GET_POS_FOR_NPC_DRIVE_TASK(2), GET_POS_FOR_NPC_DRIVE_TASK(3) )
			
			ELSE
				TASK_VEHICLE_DRIVE_TO_COORD(customer_ped, customer_VEHICLE, vSavedQuietLocation, GET_VEHICLE_ESTIMATED_MAX_SPEED(customer_VEHICLE) * 0.65, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, 
				DRIVINGMODE_STOPFORCARS | DF_GoOffRoadWhenAvoiding, 4.0, -1)
			
				CDEBUG1LN(DEBUG_PROSTITUTE,"*************Quiet Location has had 2 seconds to load, don't give me no jive!")
				eCurrentProstituteState = PROSTITUTE_CHOOSING_SERVICE
			ENDIF	
		
		BREAK
		
		
		CASE PROSTITUTE_CHOOSING_SERVICE
			//IF at quiet location
			IF VDIST2(vSavedQuietLocation, GET_ENTITY_COORDS(prostitute_ped)) < 5*5
			AND GET_SCRIPT_TASK_STATUS(customer_ped, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
		
				chosen_sexual_activity = INT_TO_ENUM(SEX_SERVICE_BOUGHT, GET_RANDOM_INT_IN_RANGE(1, 3))
				eCurrentProstituteState = PROSTITUTE_CUSTOMER_GETTING_SERVICED
			ENDIF
			
			//B*1204578 - Setting prostitute to flee if player hijacks car
			IF HANDLE_CUSTOMER_GETTING_JACKED(customer_ped)
				SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
			ENDIF		
		BREAK
		
		CASE PROSTITUTE_CUSTOMER_GETTING_SERVICED
			PROSTITUTE_SERVING_AI()	
		BREAK
		
		CASE PROSTITUTE_UNDER_ATTACK
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
		BREAK
		
		CASE PROSTITUTE_WAITING_TO_END	
			IF DOES_ENTITY_EXIST(prostitute_ped) AND DOES_ENTITY_EXIST(customer_VEHICLE)
				IF IS_PED_IN_VEHICLE(prostitute_ped, customer_VEHICLE)
					IF NOT IS_ENTITY_DEAD(customer_ped)
						IF GET_SCRIPT_TASK_STATUS(customer_ped, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							TASK_LEAVE_ANY_VEHICLE(prostitute_ped,0,ECF_WAIT_FOR_ENTRY_POINT_TO_BE_CLEAR | ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						ENDIF
					ENDIF
				ELSE
					SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)
				ENDIF
			ENDIF
		BREAK		
	ENDSWITCH
ENDPROC

