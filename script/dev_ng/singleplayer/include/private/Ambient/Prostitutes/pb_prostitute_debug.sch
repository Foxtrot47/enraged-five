///    pb_prostitute_debug.sch
///    Author: John R. Diaz

CONST_FLOAT	CONST_fDebugColumnX		0.4

PROC DISPLAY_SCENE_PHASE_TIME(INT iThisSceneID)
	
	
	IF NOT PROS_IS_SYNC_SCENE_RUNNING(iThisSceneID)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 sDebugPhaseTime
		sDebugPhaseTime = "Phase= "
		sDebugPhaseTime += ROUND(PROS_GET_SYNC_SCENE_PHASE(iThisSceneID) * 1000)
		DRAW_DEBUG_TEXT_2D(sDebugPhaseTime,<<CONST_fDebugColumnX, 0.14,0.0>>)
	#ENDIF
	
	UNUSED_PARAMETER(iThisSceneID)
ENDPROC

FUNC INT GET_PLAYER_STAMINA_FOR_PROSTITUTE()

	IF IS_THIS_MP_PROSTITUTION()
		RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_STAMINA)
	ELSE
		RETURN GET_SP_PLAYER_PED_STAT_VALUE(GET_PLAYER_PED_ENUM(PLAYER_PED_ID()), PS_STAMINA)
	ENDIF

ENDFUNC

FUNC INT GET_NUM_SEX_LOOPS_BASED_ON_PLAYER_STAMINA()
	
	INT iPlayerStamina = GET_PLAYER_STAMINA_FOR_PROSTITUTE()
	INT iNumNutsPlayerCanAttain
	
	IF  iPlayerStamina < 48
		iNumNutsPlayerCanAttain = 2
	ELIF iPlayerStamina >= 48 AND iPlayerStamina < 85
		iNumNutsPlayerCanAttain = 3
	ELSE
		iNumNutsPlayerCanAttain =  4
	ENDIF
	
//	CDEBUG1LN(DEBUG_PROSTITUTE, "[SEX STAMINA] - Player = ",iPlayerStamina , "Num Sex Loops = ", iNumNutsPlayerCanAttain )
	
	RETURN iNumNutsPlayerCanAttain
ENDFUNC
PROC PROSTITUTE_DEBUG_SEX_SCENE()
	
	#IF IS_DEBUG_BUILD
	
		TEXT_LABEL_31 sDebugSexStamina
		INT iStamina = GET_PLAYER_STAMINA_FOR_PROSTITUTE()
		sDebugSexStamina = "Stamina:"
		sDebugSexStamina += iStamina
		sDebugSexStamina += " = "
		sDebugSexStamina += PICK_INT(IS_THIS_MP_PROSTITUTION(),GET_NUM_SEX_LOOPS_BASED_ON_CHAT(),GET_NUM_SEX_LOOPS_BASED_ON_PLAYER_STAMINA())
		DRAW_DEBUG_TEXT_2D(sDebugSexStamina,<<CONST_fDebugColumnX, 0.04,0.0>>)
		
		TEXT_LABEL_31 sDebugTimes
		sDebugTimes = "Num Loops:"
		sDebugTimes += iTimesToLoopSex
		DRAW_DEBUG_TEXT_2D(sDebugTimes,<<CONST_fDebugColumnX, 0.06,0.0>>)
		
			
		TEXT_LABEL_31 sDebugNuts
		sDebugNuts = "# Gotten Off:"
		sDebugNuts += iNumOfTimesGettingOff
		DRAW_DEBUG_TEXT_2D(sDebugNuts,<<CONST_fDebugColumnX, 0.08,0.0>>)
		
		TEXT_LABEL_31 sDebugSelection
		sDebugSelection = "Service:"
		
		IF detailed_chosen_sexual_activity = SEX_SPECIFIC_BLOW_JOB	
			sDebugSelection += " $50"
		ELIF detailed_chosen_sexual_activity = SEX_SPECIFIC_FULL_SERVICE
			sDebugSelection += " $70"
		ELIF detailed_chosen_sexual_activity = SEX_SPECIFIC_FULL_SERVICE2
			sDebugSelection += " $100"
		ENDIF
		DRAW_DEBUG_TEXT_2D(sDebugSelection,<<CONST_fDebugColumnX, 0.1,0.0>>)
		
		DRAW_DEBUG_TEXT_2D(GET_SEX_STATE_INDEX_NAME(prostituteServiceIndex),<<CONST_fDebugColumnX, 0.12,0.0>>)

	#ENDIF
ENDPROC

//EOF

