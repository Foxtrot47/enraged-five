///    File: pb_prostitute_network_player.sch
///    Original Author: John R. Diaz
///    This will include handy functions that are to be utlilized in MP Prostitutes
///    


CONST_FLOAT dbgConst_fHUDTextNetworkColumn 0.06
CONST_FLOAT	CONST_fSpeechLevelTrigger	0.4
CONST_FLOAT	CONST_fSpeechLevelShutIt	0.7


FUNC BOOL IS_NETWORK_PRO_FLAG_SET(BITS_FOR_NETWORK_PROSTITUTE_iProNetBits eWhichFlagToCheck)
	RETURN IS_BITMASK_AS_ENUM_SET(iProNetBits, eWhichFlagToCheck)	
ENDFUNC

/// PURPOSE: Wrapper that manipulates the bitflag iProNetBits
PROC SET_NETWORK_PRO_FLAG( BITS_FOR_NETWORK_PROSTITUTE_iProNetBits eWhichFlagToSet )
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iProNetBits, eWhichFlagToSet)
		SET_BITMASK_AS_ENUM(iProNetBits,eWhichFlagToSet)
		CPRINTLN(DEBUG_PROSTITUTE, " [NETWORK] Setting Prostitute Flag Bit ", eWhichFlagToSet , " to ON")
	ENDIF
	
ENDPROC

/// PURPOSE: Wrapper that clears the bitflag iProNetBits
PROC CLEAR_NETWORK_PRO_FLAG( BITS_FOR_NETWORK_PROSTITUTE_iProNetBits eWhichFlagToSet )
	
	IF IS_BITMASK_AS_ENUM_SET(iProNetBits, eWhichFlagToSet)
		CLEAR_BITMASK_AS_ENUM(iProNetBits,eWhichFlagToSet)
		CPRINTLN(DEBUG_PROSTITUTE, " [NETWORK] Setting Prostitute Flag Bit ", eWhichFlagToSet , " to OFF")
	ENDIF
	
ENDPROC

PROC PROCESS_PROSTITUTE_EVENTS()
		
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
			GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
			IF Details.Type = SCRIPT_EVENT_MP_PROSTITUTION_KILL_THREAD
				IF Details.FromPlayerIndex = PLAYER_ID()
					SET_NETWORK_PRO_FLAG(NPF_MP_SHOULD_TERMINATE)
				ENDIF
			ENDIF
		ENDIF	
	ENDREPEAT
ENDPROC

/// PURPOSE: checks to see if we're in MP or SP
///    
/// RETURNS: TRUE if we're ONLINE Hoin
FUNC BOOL IS_THIS_MP_PROSTITUTION()
	RETURN IS_NETWORK_PRO_FLAG_SET(NPF_IS_NETWORK_SESSION)
ENDFUNC

/// PURPOSE: Turn on that we're in MP instead of SP
///    
PROC SET_THIS_AS_NETWORK_MP_PROSTITUTION_SESSION()
	SET_NETWORK_PRO_FLAG(NPF_IS_NETWORK_SESSION)
ENDPROC
/// PURPOSE: gated func that pops up the alert message to buy cash from the PS/XBOX store using LAUNCH_STORE_CASH_ALERT()
PROC LAUNCH_NETWORK_PROSTITUTE_CASH_ALERT()

	//B*1568255 - pop up the alert message to buy cash from the PS/XBOX store
	IF NOT IS_NETWORK_PRO_FLAG_SET(NPF_DISPLAY_CASH_ALERT)
		LAUNCH_STORE_CASH_ALERT()
		SET_NETWORK_PRO_FLAG(NPF_DISPLAY_CASH_ALERT)
	ENDIF
	
ENDPROC

FUNC BOOL IS_NETWORK_PLAYER_SPEAKING_TO_PROSTITUTE(FLOAT fTriggerLevel)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		//Update player's shout
		FLOAT fPlayerLoudness = NETWORK_GET_PLAYER_LOUDNESS(PLAYER_ID())
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_31 sDebugPlayerVoice
			sDebugPlayerVoice = "Voice = "
			sDebugPlayerVoice += CEIL(fPlayerLoudness * 100)
			DRAW_DEBUG_TEXT_2D(sDebugPlayerVoice,<<dbgConst_fHUDTextNetworkColumn,0.62,0.0>>)
			
			TEXT_LABEL_31 sDebugNumSpoken
			sDebugNumSpoken = " Gimme More # "
			sDebugNumSpoken += iNumTimesProstituteSpokenTo
			DRAW_DEBUG_TEXT_2D(sDebugNumSpoken,<<dbgConst_fHUDTextNetworkColumn,0.64,0.0>>)
//			
//			TEXT_LABEL_31 sDebugNumShutIt
//			sDebugNumShutIt = " ShutIt # "
//			sDebugNumShutIt += iNumTimesProstituteToldToShutUp
//			DRAW_DEBUG_TEXT_2D(sDebugNumShutIt,<<dbgConst_fHUDTextNetworkColumn,0.66,0.0>>)
			
			
		#ENDIF
		
		IF fPlayerLoudness > fTriggerLevel
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
/// PURPOSE: Based off iNumTimesProstituteSpokenTo to determine amount of times to loop sex anim
FUNC INT GET_NUM_SEX_LOOPS_BASED_ON_CHAT()
	
	INT iNumNutsPlayerCanAttain
	
	IF chosen_sexual_activity != SEX_TYPE_CHOSEN_BLOWJOB
		
		IF  iNumTimesProstituteSpokenTo < 2
			iNumNutsPlayerCanAttain = 3
		
		ELIF iNumTimesProstituteSpokenTo = 2
			iNumNutsPlayerCanAttain = 4
		
		ELIF iNumTimesProstituteSpokenTo =3
			iNumNutsPlayerCanAttain =  5
		
		ELSE
			iNumNutsPlayerCanAttain =  6
		ENDIF
		
	//Blowjob	
	ELSE
		IF  iNumTimesProstituteSpokenTo < 3
			iNumNutsPlayerCanAttain = 2
		ELSE
			iNumNutsPlayerCanAttain =  3
		ENDIF
	ENDIF
	
	RETURN iNumNutsPlayerCanAttain
ENDFUNC

/// PURPOSE: checks if any other camera besides the rear is available. Used to figure out whether to give the SELECT CAMERA PROMPT.
///    

FUNC BOOL ARE_ALL_CAMS_OCCLUDED()
	RETURN (IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_FRONT_CAM_IS_OCCLUDED)
		AND IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_DRIVER_CAM_IS_OCCLUDED)
		AND IS_BITMASK_AS_ENUM_SET(iCameraBitFlags,PSA_PASSENGER_CAM_IS_OCCLUDED))
ENDFUNC

PROC PLAY_NETWORK_SEX_SCENE_HELP()
		
	// Removed help text about speaking to the prostitute - B* 1614827 B.S.
	//Help = "Press ~INPUT_SCRIPT_SELECT~ to switch cameras for a better view."
	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_CAM_TOG")
	AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PROS_NET_SPEAK")
		
		IF ARE_ALL_CAMS_OCCLUDED()
			PRINT_HELP_FOREVER("PROS_CAM_OC")			
		ELSE
			PRINT_HELP_FOREVER("PROS_CAM_TOG")
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: Handles the player speaking with the hooker during MP Prostituion
///    
PROC PLAY_SPEECH_DURING_NETWORK_SEX()
	IF IS_PED_INJURED(prostitute_ped )
		EXIT
	ENDIF
	
	//Speech
	#IF IS_DEBUG_BUILD
		IF NOT IS_ANY_SPEECH_PLAYING(prostitute_ped)
			DRAW_DEBUG_TEXT_2D("Pro is SILENT",<<dbgConst_fHUDTextNetworkColumn,0.6,0.0>>)
		ELSE
			DRAW_DEBUG_TEXT_2D("Pro is SPEAKING",<<dbgConst_fHUDTextNetworkColumn,0.6,0.0>>)
		ENDIF
	#ENDIF
	
//	//Audio Scenes
//	IF NOT IS_ANY_SPEECH_PLAYING(prostitute_ped)
//		//B*1517716 - Turn off BJ Audio Scene when she's done talking
//		IF chosen_sexual_activity = SEX_TYPE_CHOSEN_BLOWJOB
//			CDEBUG1LN(DEBUG_PROSTITUTE,"[SPEECH] Pro - Done - Resetting Timer & Clearing AUDIO_SCENE " )
//			STOP_AUDIO_SCENE_FOR_PROSTITUTE("PROSTITUTES_BJ_SPEECH_SCENE")
//		ENDIF
//	ENDIF
	
	
	//PLAY_NETWORK_SEX_SCENE_HELP()
	
		
	IF IS_NETWORK_PLAYER_SPEAKING_TO_PROSTITUTE(CONST_fSpeechLevelTrigger)
//		IF IS_NETWORK_PLAYER_SPEAKING_TO_PROSTITUTE(CONST_fSpeechLevelShutIt)
//			//If I'm speaking loudly, make the pro shutup
//			IF IS_ANY_SPEECH_PLAYING(prostitute_ped)
//				//STOP_CURRENT_PLAYING_AMBIENT_SPEECH(prostitute_ped)
//				iNumTimesProstituteToldToShutUp++
//			ENDIF
//		ENDIF
		
		SET_NETWORK_PRO_FLAG(NPF_IS_PLAYER_SPEAKING)
	ENDIF
	
	
	IF IS_NETWORK_PRO_FLAG_SET(NPF_IS_PLAYER_SPEAKING)
		IF NOT IS_TIMER_STARTED(tNetPlayerSpoke)
			START_TIMER_NOW(tNetPlayerSpoke)
		ELSE
		
			IF GET_TIMER_IN_SECONDS(tNetPlayerSpoke) > CONST_fPlayerSpeechDelayMin//GET_RANDOM_FLOAT_IN_RANGE(CONST_fPlayerSpeechDelayMin, CONST_fPlayerSpeechDelayMax )
				IF NOT IS_NETWORK_PRO_FLAG_SET(NPF_IS_PROSTITUTE_SPEAKING)
				AND NOT IS_NETWORK_PLAYER_SPEAKING_TO_PROSTITUTE(CONST_fSpeechLevelTrigger)
				
					SET_NETWORK_PRO_FLAG(NPF_IS_PROSTITUTE_SPEAKING)
					iNumTimesProstituteSpokenTo++
					//determine sex type
					//PLAY_SPEECH_FOR_SEX(detailed_chosen_sexual_activity)
				
				ELIF IS_NETWORK_PRO_FLAG_SET(NPF_IS_PROSTITUTE_SPEAKING)
				
					IF NOT IS_ANY_SPEECH_PLAYING(prostitute_ped)
						CLEAR_NETWORK_PRO_FLAG(NPF_IS_PLAYER_SPEAKING)
						CLEAR_NETWORK_PRO_FLAG(NPF_IS_PROSTITUTE_SPEAKING)
						RESTART_TIMER_NOW(tNetPlayerSpoke)
					ENDIF
					
				ENDIF	
			ENDIF
		ENDIF
	ENDIF

		
		
		

	
ENDPROC
/// PURPOSE: Makes sure the vehicle the player is currently in is valid on the network.
///    
/// RETURNS: TRUE if the network has control of the vehicle
FUNC BOOL IS_VEHICLE_SAFE_FOR_MP_PROSTITUTE()

	IF NOT IS_THIS_MP_PROSTITUTION()
		RETURN TRUE
	ELSE
		IF DOES_ENTITY_EXIST( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) )
			IF NETWORK_GET_ENTITY_IS_NETWORKED( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) )
				IF GET_PED_IN_VEHICLE_SEAT( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ), VS_DRIVER ) = PLAYER_PED_ID()
					IF NETWORK_HAS_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) )
						IF NOT IS_ENTITY_A_MISSION_ENTITY( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) )
							SET_ENTITY_AS_MISSION_ENTITY( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ), FALSE, FALSE )
						ENDIF
						RETURN TRUE
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() )  )
						CDEBUG1LN(DEBUG_PROSTITUTE,"IS_VEHICLE_SAFE_FOR_MP_PROSTITUTE - We're in the driver's seat but don't have control.  Requesting control.")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_PROSTITUTE,"IS_VEHICLE_SAFE_FOR_MP_PROSTITUTE - Player not in driver's seat.")
				ENDIF
			ELSE
				NETWORK_REGISTER_ENTITY_AS_NETWORKED( GET_VEHICLE_PED_IS_IN( PLAYER_PED_ID() ) )
				CDEBUG1LN(DEBUG_PROSTITUTE,"IS_VEHICLE_SAFE_FOR_MP_PROSTITUTE - Car isn't networked.  Registering it.")	
				
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//EOF 
