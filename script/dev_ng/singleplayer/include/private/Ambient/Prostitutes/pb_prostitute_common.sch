USING "pb_prostitute.sch"
USING "common_sex.sch"

//Dialogue Slots
CONST_INT	CONST_iDialoguePlayerSlot						0
CONST_INT	CONST_iDialogueProstituteSlot					4
CONST_INT	CONST_iDialogueUniqueSlot						6

CONST_INT 	health_frequency								1000
CONST_INT 	health_pedge									2
CONST_INT 	times_player_allowed_to_cancel   				4	
CONST_INT 	CONST_iSexDuration			   					15000
CONST_INT 	CONST_iMinMilliSecsToPlayRejectLoopAnim			2000
CONST_INT 	CONST_iMaxMilliSecsToPlayRejectLoopAnim			4000
CONST_INT 	CONST_WAIT_FOR_PLAYER_TO_PICK_SERVICE_MIN_TIME	1
CONST_INT 	CONST_WAIT_FOR_PLAYER_TO_PICK_SERVICE_MAX_TIME	2
CONST_INT 	CONST_iMaxNumOfLoadsPlayerCanBlow				3
CONST_INT 	CONST_iPadShakeFrequency						42
CONST_INT 	CONST_iPadShakeDuration							200
CONST_INT 	CONST_iNumProstituteAnimDictionaries			1
CONST_INT 	CONST_iCostOfNoServices							20
CONST_INT 	CONST_iProstituteLODDistance					50
CONST_INT 	CONST_iNumQuietLocationsForAI					32

CONST_FLOAT CONST_iProstituteLODMult						5.0

CONST_FLOAT	CONST_fProstituteFleeDist						300.0



CONST_FLOAT	CONST_fForceSex									-0.1
CONST_FLOAT	CONST_fForceBJ									-0.05
CONST_FLOAT CONST_fTimeBeforeSheSolicitsPlayerAgain			25.0
CONST_FLOAT CONST_fScenarioRadius							20.0
CONST_FLOAT CONST_fDistToHeadTrackPlayer					30.0
CONST_FLOAT	CONST_fBJShakeDelay								0.75
CONST_FLOAT	CONST_fSexShakeDelay							0.72
CONST_FLOAT pros_cancel_range 								15.0
CONST_FLOAT	CONST_fWaitForPlayerToSolicit					20.0
CONST_FLOAT	CONST_fFailForNotStopping						20.0
CONST_FLOAT	CONST_fFailForYoyoing							20.0
CONST_FLOAT CONST_fSexBounceDuration						0.1
CONST_FLOAT CONST_fPlayerSpeechDelay						14.0
CONST_FLOAT CONST_fPlayerSpeechDelayMin						2.0
CONST_FLOAT CONST_fPlayerSpeechDelayMax						5.0
CONST_FLOAT CONST_fPlayerSpeechCarTooSmall					50.0

CONST_FLOAT CONST_fProFindSecludedSpotSpeechDelay			15.0
CONST_FLOAT	CONST_fShockingEventRadius						30.0
CONST_FLOAT	CONST_fDistPlayer2Call							11.0
CONST_FLOAT	CONST_fDistToLookAway							15.0
CONST_FLOAT	CONST_fMinSpeedToPickUpPro						2.0
CONST_FLOAT	CONST_fDistToCar								2.5
CONST_FLOAT	CONST_fSeatOffset								0.916
CONST_FLOAT	CONST_fTimeToRegainHealthDelay					1.0
CONST_FLOAT CONST_fHookerComeOverDelay						2.0

CONST_FLOAT CONST_fDelayForPlayerResponseToUniqueStory		15.0

//Prostitute sex spot distances
CONST_FLOAT	CONST_fTimeToStopBeforeService					2.5
CONST_FLOAT CONST_fMinDistanceFromCops						30.0
CONST_FLOAT CONST_fMinDistanceFromPedsAndVehs				20.0
CONST_FLOAT	CONST_fDistanceFromRoadNodeToBeSecluded			12.0
CONST_FLOAT	CONST_fDistanceFromRoadNodeToBeSecludedx2		25.0

CONST_FLOAT	CONST_fDistanceFromRoadNodeExceptionBoardwalk	40.0
CONST_FLOAT	CONST_fDistanceFromRoadNodeException			15.0

CONST_FLOAT CONST_fDistToBeSecludedProper					56.25 //7.5*7.5
CONST_FLOAT CONST_fDistToBeSecludedProperLarge				400.0 //20.0*20.0

//Sex Blend Data
CONST_FLOAT CONST_fSyncdSceneEndPhase_P1_P2					0.925
CONST_FLOAT CONST_fSyncdSceneEndPhase_ACTION				0.980
CONST_FLOAT CONST_fSyncdSceneEndPhase						0.975

CONST_FLOAT CONST_fSyncdSceneEndPhase_P1_P2_MP				0.986
CONST_FLOAT CONST_fSyncdSceneEndPhase_ACTION_MP				0.986
CONST_FLOAT CONST_fSyncdSceneEndPhase_MP					0.986
CONST_FLOAT	CONST_fSexBlendDelta							0.7

#IF IS_JAPANESE_BUILD
CONST_FLOAT CONST_fJPFinishTime								14.0
CONST_FLOAT CONST_fSyncdSceneEndTimeJP_P1_P2				2.0
CONST_FLOAT CONST_fSyncdSceneEndTimeJP_ACTION				4.0
CONST_FLOAT CONST_fSyncdSceneEndTimeJP						10.0
CONST_FLOAT CONST_fSyncdSceneEndTimeJP_FINAL				14.0
#ENDIF


ENUM BITS_FOR_PB_PROSTITUTION_iProScriptBits
	
	PRO_BIT_Speech_Car_Too_Small 						= BIT0,
	PRO_BIT_IS_THIS_SCRIPT_INTERACTING 					= BIT1,
	PRO_BIT_did_this_script_set_global_flag				= BIT2,
	PRO_BIT_bDidLowCarAnimClipSetGetReqd				= BIT3,
	PRO_BIT_script_has_disabled_driveby					= BIT4,
	PRO_BIT_bIsClosestProstitute						= BIT5,
	PRO_BIT_bInteractedWithAI               			= BIT6,
	PRO_BIT_bShopsClosedForProstitution					= BIT7,
	PRO_BIT_bMoneyShotComing							= BIT8,
	PRO_BIT_sex_anims_have_been_started 				= BIT9,
	PRO_BIT_player_has_made_a_choice					= BIT10,
	PRO_BIT_bSwitchedOffPlayerControl					= BIT11,
	PRO_BIT_bProstituteHasWitnessedAShockingEvent		= BIT12,
	PRO_BIT_bProstituteQuickEnter						= BIT13,
	PRO_BIT_bRejectedByPlayer							= BIT14,
	PRO_BIT_bIsPlayerInLowCar							= BIT15,
	PRO_BIT_bSexDialogueIsPlaying						= BIT16,
	PRO_BIT_bSexOfferInitiatedPlayer					= BIT17,
	PRO_BIT_bSexOfferInitiatedHooker					= BIT18,
	PRO_BIT_bPlayerIgnoredHookerOnce					= BIT19,
	PRO_BIT_bShowHowToGetHookerHelp						= BIT20,
	PRO_BIT_bIgnoreMaterial								= BIT21,
	PRO_BIT_bAnimsAllStreamed							= BIT22,
	PRO_BIT_bProstituteIgnoringPlayer					= BIT23,
	PRO_BIT_bLoopSex									= BIT24,
	PRO_BIT_bLostYoHo									= BIT25,
	PRO_BIT_bDebugInfo									= BIT26,
	PRO_BIT_bHookerIsFreakingOut						= BIT27,

#IF IS_PIMP_AVAILABLE
	PRO_BIT_bIsPimpTalkingToPro							= BIT28,
	PRO_BIT_bInteractedWithPimp             			= BIT29,
#ENDIF

	PRO_BIT_bTotal										= BIT30
	
ENDENUM
INT iProScriptBits = 0



ENUM BITS_FOR_PROSTITUTION_SEX_ANIMS_iActionBitFlags
	PSA_SEX						= BIT0,
	PSA_BJ						= BIT1,
	PSA_PRO_COMPLETED_P2A_P1	= BIT2,
	PSA_PLYR_COMPLETED_P2A_P1	= BIT3,
	PSA_PRO_COMPLETED_P2A_P2	= BIT4,
	PSA_PLYR_COMPLETED_P2A_P2	= BIT5,
	PSA_PRO_COMPLETED_ACTION	= BIT6,
	PSA_PLYR_COMPLETED_ACTION	= BIT7,
	PSA_PRO_COMPLETED_A2P_P1	= BIT8,
	PSA_PLYR_COMPLETED_A2P_P1	= BIT9,
	PSA_PRO_COMPLETED_A2P_P2	= BIT10,
	PSA_PLYR_COMPLETED_A2P_P2	= BIT11,
	
	PSA_HELP_HOW_TO_FIND_SPOT	= BIT12,
	PSA_HELP_COPS_IN_AREA		= BIT13
	
ENDENUM
INT iActionBitFlags

ENUM BITS_FOR_PROSTITUTION_SEX_INT_iCameraBitFlags
	
	PSA_FRONTCAM_IS_CLEAR					= BIT0,
	PSA_REARCAM_IS_CLEAR					= BIT1,
	PSA_DRIVER_CAM_IS_CLEAR					= BIT2,
	PSA_PASSENGER_CAM_IS_CLEAR				= BIT3,
	
	PSA_FRONT_CAM_IS_OCCLUDED				= BIT4,
	PSA_REAR_CAM_IS_OCCLUDED				= BIT5,
	PSA_DRIVER_CAM_IS_OCCLUDED				= BIT6,
	PSA_PASSENGER_CAM_IS_OCCLUDED			= BIT7,
	
	PSA_REAR_CAM_TEST_STARTED				= BIT8
	
ENDENUM
INT iCameraBitFlags

//MP INFO

ENUM BITS_FOR_NETWORK_PROSTITUTE_iProNetBits
	NPF_IS_NETWORK_SESSION					= BIT0,	//formerlly bMpProstitutes
	NPF_PLAYER_VEHICLE_DOORS_LOCKED			= BIT1,	//formerly bDoorsLocked
	NPF_IS_PLAYER_SPEAKING					= BIT2,
	NPF_DISPLAY_VOICE_HELP					= BIT3,
	NPF_IS_PROSTITUTE_SPEAKING				= BIT4,
	NPF_DISPLAY_HELP_SHUTIT					= BIT5,
	NPF_DISPLAY_CASH_ALERT					= BIT6,
	NPF_MP_SHOULD_TERMINATE					= BIT7,
	NPF_SWITCHED_FEET						= BIT8,
	NPF_BOUNCED								= BIT9,
	NPF_CHANGED_CLOTHES						= BIT10,
	NPF_TERMINATE_THREAD					= BIT11,
	NPF_DISPLAY_HELP_VEHICLE				= BIT12
ENDENUM
INT iProNetBits

ENUM PROSTITUTION_ASK_FOR_RIDE_STATES
	PAFRS_INIT = 0,
	PAFRS_PLAY_INTRO,
	PAFRS_PLAY_SEDUCTION,
	PAFRS_WAIT_FOR_RESPONSE,
	PAFRS_EXIT
ENDENUM
PROSTITUTION_ASK_FOR_RIDE_STATES eProsAskForRideState

ENUM PROSTITUTION_REJECTED_STATE
	PRS_INIT = 0,
	PRS_PLAY_INTRO,
	PRS_USE_SCENARIO,
	PRS_EXIT
ENDENUM
PROSTITUTION_REJECTED_STATE eProsRejectState

ENUM PROSTITUTION_IGNORED_STATE
	PIS_INIT = 0,
	PIS_PLAY_INTRO,
	PIS_PLAY_LOOP,
	PIS_EXIT
ENDENUM
PROSTITUTION_IGNORED_STATE eProsIgnoreState

VECTOR vProsVehiclePos = <<0,0,0>>
INT iInitialPlayerHealth = 0
INT iInitialProsHealth = 0
INT iNumTimesProstituteSpokenTo
BOOL bBrainIsRunning = FALSE

STRING	sAnimDictionaryName


/// INTs 
INT timer_start
INT timer_current
INT timer_difference
INT times_ped_damaged = 0
INT random_wait_time
INT chosen_sexual_activity_int
INT iServiceMenuSelection
INT iNumServicesAvailable
INT iNumOfTimesGettingOff
INT iTimesToLoopSex = 0
INT prostituteContextID = NEW_CONTEXT_INTENTION
INT iPlayerServiceBitFlags

INT iSexSceneID_Passenger_Start		=	-1
INT iSexSceneID_Driver_Start		=	-1

INT iSexSceneID_MoveToLoop			=	-1

INT iSexSceneID_Loop				=	-1

INT iSexSceneID_MoveFromLoop		=	-1

INT iSexSceneID_Passenger_End		=	-1
INT iSexSceneID_Driver_End			=	-1

INT	iXCamPositions  = 0
INT iNextShapeTest = -1
INT iNavMeshBlockingObjectID

INT						iFemalePedComp			=	-1
INT						iFemalePedTex			= 	-1
BOOL					bCannotUseCarHelpDisplayed	= FALSE

INT iChangeSelectionTimer

//VECTORS
VECTOR vSavedQuietLocation
VECTOR vProsWalkToPos

/// PED_INDEX
PED_INDEX prostitute_ped	
PED_INDEX customer_ped

/// VEHICLES
VEHICLE_INDEX  customer_VEHICLE
VEHICLE_INDEX  iPlayersVehicle

SHAPETEST_INDEX stMaterialProbe = NULL


SEQUENCE_INDEX prostitute_seq

CAMERA_INDEX camTakeHome0, camTakeHome1
//------------------------------------------

// structTimers
structTimer 				cutsceneTimer
structTimer					tPlayerSpoke			//19
structTimer					tNetPlayerSpoke
structTimer					tProstituteApproach		//15
structTimer					tServiceTime			//18
structTimer					tControl				//10


/// MISC
structPedsForConversation 		cultresConversation
STREAMED_ANIM 					pbStreamedAnims[CONST_iNumProstituteAnimDictionaries]
SEX_SPECIFIC_ACT				AvailableSexActs[COUNT_OF(SEX_SPECIFIC_ACT)]
PROSTITUTE_TYPE					eProstituteType				= PRO_TYPE_INVALID
PROSTITUTE_PED_BRAIN_STATE		ePBState					= PROS_PB_INIT
PROSTITUTE_STATE 				eCurrentProstituteState		= PROSTITUTE_IDLE
PROSTITUTE_CUSTOMER_TYPE 		customer_type				= CUSTOMER_NO_CUSTOMER
SEX_SERVICE_BOUGHT 				chosen_sexual_activity 		= SEX_TYPE_CHOSEN_BLOWJOB //SEX_TYPE_CHOSEN_NONE /!
SEX_SPECIFIC_ACT 				detailed_chosen_sexual_activity
SEX_SERVICE_BOUGHT 				new_chosen_sexual_activity
PROSTITUTE_INIT_STATE			ePBInitState				= PROSTITUTE_INIT_REQUEST
SERVICE_MENU_STATE				eServiceMenuState			= SERVICE_MENU_INIT
PROSTITUTE_SERVICE_STATE 		eProsServiceState 			= PROSTITUTE_SERVICE_INIT
PRO_BACKSTORY_STATES			eProBackstoryState			= PRO_BS_INIT
TIMEOFDAY						eTODStripperOfferedServices
SEX_STATE_ENUM 					prostituteServiceIndex 		= SEX_PROP_TO_ACTION_P1
PRO_BACKSTORY_PLAYER_RESPONSE 	ePlayerStoryResponse = PRO_RESPONSE_NOTHING
REL_GROUP_HASH					tProstRelGroup

//Pimp Variables---------------------------------------------------------
#IF IS_PIMP_AVAILABLE

#IF IS_DEBUG_BUILD

BOOL force_to_use_pimp								= FALSE // if true forces the pros to only accept pimps

#ENDIF


CONST_INT				CONST_iDialoguePimpSlot							5



PED_INDEX 				pimp_ped


INT 					iPimpFlags
INT 					iPimpConv
INT 					iPimpSyncSceneID		= 	-1

structTimer				tPimpPlayerInVehicle	//5
structTimer				tPimpPlayerOutVehicle	//5

PIMP_STATE 				current_pimp_state			= PIMP_STATE_STARTING	
BLIP_INDEX 				pimp_blip

#ENDIF

#IF ALLOW_PROSTITUTE_DEBUG
#IF IS_DEBUG_BUILD	/// debug flags
BOOL debug_text										= FALSE // if true displays various debug massages to the console window
BOOL force_to_use_player							= FALSE // if true forces the pros to only accept player as a cutsomer
BOOL force_to_use_random							= FALSE // if true forces the pros to only accept random peds as a cutsomer
BOOL forgetting_about_seclusions					= FALSE // if true sex can happen anywhere
BOOL bDbgBlipPeds									= FALSE
BLIP_INDEX blipNearby
BLIP_INDEX prostitute_blip
BLIP_INDEX customer_blip
#endif
#ENDIF

PROC REMOVE_PROSTITUTE_BLIP()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(prostitute_blip)
			REMOVE_BLIP(prostitute_blip)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

PROC REMOVE_CUSTOMER_BLIP()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(customer_blip)
			REMOVE_BLIP(customer_blip)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

PROC CREATE_PROSTITUTE_BLIP()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF NOT DOES_BLIP_EXIST(prostitute_blip) AND DOES_ENTITY_EXIST(prostitute_ped)
			prostitute_blip = ADD_BLIP_FOR_ENTITY(prostitute_ped)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

PROC CREATE_CUSTOMER_BLIP()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF NOT DOES_BLIP_EXIST(customer_blip) AND DOES_ENTITY_EXIST(customer_VEHICLE)
			customer_blip = ADD_BLIP_FOR_ENTITY(customer_VEHICLE)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

PROC SET_PROSITUTE_BLIP_COLOUR(INT iColour)
	UNUSED_PARAMETER(iColour)
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(prostitute_blip)
			SET_BLIP_COLOUR(prostitute_blip, iColour)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

PROC SET_CUSTOMER_BLIP_COLOUR(INT iColour)
	UNUSED_PARAMETER(iColour)
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(customer_blip)
			SET_BLIP_COLOUR(customer_blip, iColour)
		ENDIF
	#ENDIF #ENDIF
ENDPROC

FUNC BOOL IS_PROSTITUTE_DEBUG_TEXT_ON()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		RETURN debug_text
	#ENDIF #ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROSTITUTE_FORCE_TO_USE_PLAYER()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		RETURN force_to_use_player
	#ENDIF #ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROSTITUTE_FORCE_TO_USE_RANDOM()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		RETURN force_to_use_random
	#ENDIF #ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL PROSTITUTE_DONT_NEED_SECLUSION()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		RETURN forgetting_about_seclusions
	#ENDIF #ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DRAW_PROSTITUTE_DEBUG_BLIP()
	#IF ALLOW_PROSTITUTE_DEBUG #IF IS_DEBUG_BUILD
		RETURN bDbgBlipPeds
	#ENDIF #ENDIF

	RETURN FALSE
ENDFUNC
//EOF
