USING "ambience_run_checks.sch"

FUNC BOOL IS_THERE_A_CAR_BLOCKING_THIS_CAR(VEHICLE_INDEX InCar)
	VEHICLE_INDEX TempCarID
	
	
	TempCarID = GET_RANDOM_VEHICLE_IN_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(InCar, <<0.0, 5.0, 0.0>>), 1.5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF DOES_ENTITY_EXIST(TempCarID)
		IF NOT (TempCarID = InCar)
			// is the vehicle infront moving?
			IF IS_VEHICLE_DRIVEABLE(TempCarID)
				IF IS_VEHICLE_STOPPED(TempCarID)
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

//FUNC BOOL IS_TAXI_SAFE_TO_MOVE(VEHICLE_INDEX InCar)
//	// we don't want to hail taxi's that are in a rank or traffic jam 
//	IF NOT IS_CAR_STOPPED_AT_TRAFFIC_LIGHTS(InCar)
//		IF NOT IS_THERE_A_CAR_BLOCKING_THIS_CAR(InCar)
//		OR NOT IS_CAR_STOPPED(InCar) 
//			RETURN(TRUE)
//		ENDIF
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC


/// PURPOSE:
///    cleanup the globals g_WaitingTaxiDriver & g_WaitingTaxi to NULL
///    sets driver ai up
///    resets globals - g_sTaxiEntitiesOwnedByThisScript / g_iTaxiHailedTime
PROC CLEANUP_WAITING_TAXI()

	// set driver's behaviour
	IF DOES_ENTITY_EXIST(g_WaitingTaxiDriver)
		IF NOT IS_PED_INJURED(g_WaitingTaxiDriver)
			
			// RESET FLAG for cleanup B*1536974 - Press to enter taxi as passenger hold to jack driver - opposite to how it worked on IV
			// to get IV behaviour you have to ensur this flag is false then task the player in script to enter as rear passenger when input detected
			SET_PED_CONFIG_FLAG(g_WaitingTaxiDriver, PCF_AICanDrivePlayerAsRearPassenger, FALSE)
					
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_WaitingTaxiDriver, FALSE)
			
			IF NOT IS_PED_FLEEING(g_WaitingTaxiDriver)
			AND NOT IS_PED_IN_COMBAT(g_WaitingTaxiDriver)
				IF IS_PED_IN_ANY_VEHICLE(g_WaitingTaxiDriver)					
					IF DOES_ENTITY_EXIST(g_WaitingTaxi)
						IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
							IF IS_PED_SITTING_IN_VEHICLE(g_WaitingTaxiDriver, g_WaitingTaxi)
								IF (GET_SCRIPT_TASK_STATUS(g_WaitingTaxiDriver, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != PERFORMING_TASK)
								AND (GET_SCRIPT_TASK_STATUS(g_WaitingTaxiDriver, SCRIPT_TASK_VEHICLE_DRIVE_WANDER) != WAITING_TO_START_TASK)	
									SEQUENCE_INDEX seq
									OPEN_SEQUENCE_TASK(seq)
										// if any of the players buddies are in the taxi then wait for then to get out
										IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_LEFT) 
										OR NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_RIGHT)
										OR NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_FRONT_RIGHT)											
											TASK_PAUSE(NULL, 2000)
										ELSE
											TASK_PAUSE(NULL, 500)
										ENDIF
										TASK_VEHICLE_DRIVE_WANDER(NULL, g_WaitingTaxi, 12.0, DRIVINGMODE_STOPFORCARS | DF_AvoidRestrictedAreas)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(g_WaitingTaxiDriver, seq)
									CLEAR_SEQUENCE_TASK(seq)
									CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI g_WaitingTaxiDriver to TASK_VEHICLE_DRIVE_WANDER")
								ELSE
									CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : g_WaitingTaxiDriver already performing TASK_VEHICLE_DRIVE_WANDER")
								ENDIF
							ELSE
								TASK_SMART_FLEE_PED(g_WaitingTaxiDriver, PLAYER_PED_ID(), 500, -1)
								CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : tasked g_WaitingTaxiDriver driver to flee as not sat in g_WaitingTaxi")
							ENDIF
						ELSE
							TASK_SMART_FLEE_PED(g_WaitingTaxiDriver, PLAYER_PED_ID(), 500, -1)
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : tasked g_WaitingTaxiDriver driver to flee as g_WaitingTaxi was not driverable")
						ENDIF
					ENDIF
				ELSE
					// just walk off not fleeing but on foot
					IF (GET_SCRIPT_TASK_STATUS(g_WaitingTaxiDriver, SCRIPT_TASK_WANDER_STANDARD) != PERFORMING_TASK)
					AND (GET_SCRIPT_TASK_STATUS(g_WaitingTaxiDriver, SCRIPT_TASK_WANDER_STANDARD) != WAITING_TO_START_TASK)
						TASK_WANDER_STANDARD(g_WaitingTaxiDriver)
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : tasked g_WaitingTaxiDriver to TASK_WANDER_STANDARD on foot")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	// Release driver
	IF DOES_ENTITY_EXIST(g_WaitingTaxiDriver)
		IF IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxiDriver)
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_WaitingTaxiDriver, FALSE)
				IF NOT IS_PED_INJURED(g_WaitingTaxiDriver)
					SET_ENTITY_LOAD_COLLISION_FLAG(g_WaitingTaxiDriver, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_WaitingTaxiDriver, FALSE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(g_WaitingTaxiDriver)
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : set g_WaitingTaxiDriver as no longer needed")
			ELSE
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : set g_WaitingTaxiDriver as no longer needed Failed! belong to this script check")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : set g_WaitingTaxiDriver as no longer needed Failed! mission entity check")
		ENDIF
	ENDIF
	
	// Release taxi
	IF DOES_ENTITY_EXIST(g_WaitingTaxi)
		IF IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxi)
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_WaitingTaxi, FALSE)
				IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
					SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(g_WaitingTaxi, FALSE)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_WaitingTaxi, TRUE)
					SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(g_WaitingTaxi, FALSE)	
					//SET_LAST_DRIVEN_VEHICLE(NULL)	//Asserts if NULL is passed in! // B*1555065 - stop taxi getting returned as GET_PLAYERS_LAST_VEHICLE()
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(g_WaitingTaxi)
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : set g_WaitingTaxi as no longer needed")
			ELSE
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " CLEANUP_WAITING_TAXI() : set g_WaitingTaxi as no longer needed Failed! belong to this script check")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " TAXI_SERVICE_SCRIPT_CLEANUP() : set g_WaitingTaxi as no longer needed Failed! mission entity check")
		ENDIF
	ENDIF
	g_WaitingTaxiDriver = NULL
	g_WaitingTaxi = NULL
	g_sTaxiEntitiesOwnedByThisScript = "NULL"
	g_iTaxiHailedTime = -1
ENDPROC

/// PURPOSE:
///    Setup taxi for player to usee
///    Sets g_WaitingTaxi & g_WaitingTaxiDriver handles
/// PARAMS:
///    vehicleIndex - the taxi vehicle
///    pedIndex - the vehicleIndex driver
///    bGrabFromOtherScript - if TRUE entities are force grabbed from other script (used by taxiService.sc to take full ownership)
FUNC BOOL STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER(VEHICLE_INDEX &vehicleIndex, PED_INDEX &pedIndex, BOOL bGrabFromOtherScript = FALSE)
	
	// if there is already a waiting taxi, tell it to bugger off
	IF NOT (vehicleIndex = g_WaitingTaxi)
	AND NOT (g_WaitingTaxi = NULL)
		CLEANUP_WAITING_TAXI()
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : vehicleIndex didn't match existing g_WaitingTaxi so removed")
	ENDIF
	
	// store this car as the new waiting taxi
	IF DOES_ENTITY_EXIST(vehicleIndex)
	AND DOES_ENTITY_EXIST(pedIndex)

		IF IS_VEHICLE_DRIVEABLE(vehicleIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				IF IS_PED_SITTING_IN_VEHICLE(pedIndex, vehicleIndex)
					
					g_WaitingTaxi = vehicleIndex
					g_WaitingTaxiDriver = pedIndex

					g_bTaxiIsWaiting = TRUE
					
					IF bGrabFromOtherScript					
						SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxiDriver, TRUE, TRUE)						
						SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxi, TRUE, TRUE)
						g_sTaxiEntitiesOwnedByThisScript = GET_THIS_SCRIPT_NAME()
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : bGrabFromOtherScript - set g_WaitingTaxi & g_WaitingTaxiDriver as mission entity")
					ELSE
						IF NOT IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxiDriver)
							SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxiDriver)						
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : set g_WaitingTaxiDriver as mission entity")
						ENDIF

						IF NOT IS_ENTITY_A_MISSION_ENTITY(g_WaitingTaxi)
							SET_ENTITY_AS_MISSION_ENTITY(g_WaitingTaxi)
							g_sTaxiEntitiesOwnedByThisScript = GET_THIS_SCRIPT_NAME()
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : set g_WaitingTaxi as mission entity")
						ENDIF
					ENDIF
					
					// B*1536974 - Press to enter taxi as passenger hold to jack driver - opposite to how it worked on IV
					// to get IV behaviour you have to ensur this flag is false then task the player in script to enter as rear passenger when input detected
					SET_PED_CONFIG_FLAG(g_WaitingTaxiDriver, PCF_AICanDrivePlayerAsRearPassenger, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(g_WaitingTaxiDriver, CA_ALWAYS_FIGHT, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(g_WaitingTaxiDriver, CA_ALWAYS_FLEE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(g_WaitingTaxiDriver, FA_NEVER_FLEE, FALSE)
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_WaitingTaxiDriver, TRUE)
					SET_AMBIENT_VOICE_NAME(g_WaitingTaxiDriver, "A_M_M_EASTSA_02_LATINO_FULL_01")
					// alternative coming soon - 
					// SET_AMBIENT_VOICE_NAME(g_WaitingTaxiDriver, "S_M_M_BOUNCER_01_LATINO_FULL_01")
					
					SET_VEHICLE_DOORS_LOCKED(g_WaitingTaxi, VEHICLELOCK_UNLOCKED)
					SET_VEHICLE_HAS_BEEN_OWNED_BY_PLAYER(g_WaitingTaxi, TRUE)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_WaitingTaxi, FALSE)
					SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(g_WaitingTaxi, TRUE)
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : return TRUE")
					RETURN TRUE
				ELSE
					CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : return FALSE driver was sitting in vehicle")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : return FALSE driver is injured")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : return FALSE vehicle isn't driverable")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : STORE_CAR_AS_TAXI_WAITING_FOR_PLAYER : return FALSE driver or vehicle don't exist")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_JACKED_PED_TO_FLEE()
	PED_INDEX tempPed
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		tempPed = GET_RANDOM_PED_AT_COORD(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1.5,1.5,1.5>>)
		IF NOT IS_PED_INJURED(tempPed)
			IF IS_PED_ON_FOOT(tempPed)
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " SET_JACKED_PED_TO_FLEE(), ped is alive and should be fleeing.")
				TASK_SMART_FLEE_PED(tempPed, PLAYER_PED_ID(), 150, -1)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC NON_GROUPED_PASSENGERS_SHOULD_FLEE()
	PED_INDEX pedTemp
	IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
		IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_LEFT)
			pedTemp = GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_LEFT)
			IF PLAYER_PED_ID() != pedTemp
				IF NOT IS_PED_GROUP_MEMBER(pedTemp, GET_PLAYER_GROUP(PLAYER_ID()))
					TASK_SMART_FLEE_PED(pedTemp, PLAYER_PED_ID(), 200, -1)
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_RIGHT)
			pedTemp = GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_RIGHT)
			IF PLAYER_PED_ID() != pedTemp
				IF NOT IS_PED_GROUP_MEMBER(pedTemp, GET_PLAYER_GROUP(PLAYER_ID()))
					TASK_SMART_FLEE_PED(pedTemp, PLAYER_PED_ID(), 200, -1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER()
	PED_INDEX TempPedID
	IF DOES_ENTITY_EXIST(g_WaitingTaxi)
		IF IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//IF GET_INTERIOR_FROM_ENTITY(g_WaitingTaxi) = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
					// check driver is alive and in the driver seat
					TempPedID = GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi)
					IF DOES_ENTITY_EXIST(g_WaitingTaxiDriver)
						IF NOT IS_PED_INJURED(TempPedID)
							IF(TempPedID = g_WaitingTaxiDriver)
							
								/* // allow player to jack passengers
								// check there are no passengers in the car								
								IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_LEFT)
									IF NOT (GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_LEFT) = PLAYER_PED_ID())
										IF NOT IS_PED_GROUP_MEMBER(GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_LEFT), PLAYER_GROUP_ID())
											//PRINTSTRING("IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER - fail 1")
											RETURN(FALSE)
										ENDIF
									ENDIF
								ENDIF
								IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_BACK_RIGHT)
									IF NOT (GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_RIGHT) = PLAYER_PED_ID())
										IF NOT IS_PED_GROUP_MEMBER(GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_BACK_RIGHT), PLAYER_GROUP_ID())
											//PRINTSTRING("IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER - fail 2")
											RETURN(FALSE)
										ENDIF
									ENDIF
								ENDIF
								IF NOT IS_VEHICLE_SEAT_FREE(g_WaitingTaxi, VS_FRONT_RIGHT)
									IF NOT (GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_FRONT_RIGHT) = PLAYER_PED_ID())
										IF NOT IS_PED_GROUP_MEMBER(GET_PED_IN_VEHICLE_SEAT(g_WaitingTaxi, VS_FRONT_RIGHT), PLAYER_GROUP_ID())
											//PRINTSTRING("IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER - fail 3")
											RETURN(FALSE)
										ENDIF
									ENDIF
								ENDIF	*/

								RETURN(TRUE)

							ENDIF
						ELSE
							CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE IS_PED_INJURED(TempPedID)")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE DOES_ENTITY_EXIST(g_WaitingTaxiDriver)")
					ENDIF
//				ENDIF
			ELSE
				CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE IS_PED_INJURED(PLAYER_PED_ID())")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE IS_VEHICLE_DRIVEABLE(g_WaitingTaxi)")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE DOES_ENTITY_EXIST(g_WaitingTaxi)")
	ENDIF

	CPRINTLN(DEBUG_TAXI_SERVICE, GET_THIS_SCRIPT_NAME(), " : IS_THERE_A_HAILED_TAXI_WAITING_FOR_PLAYER return FALSE ")
	RETURN(FALSE)

ENDFUNC

