//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	photographyWildlife.sch										//
//		AUTHOR			:	Colin Considine												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "email_public.sch"


CONST_INT 	BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED		0	// SCRIPT LAUNCHER CONTROLLER
CONST_INT 	BIT_SET_ALL_PHOTOGRAPHS_COLLECTED 	   				1	// COLLECTED ALL PHOTOGRAPHS
CONST_INT 	BIT_SET_START_EMAIL_SENT 							2	// START EMAIL
CONST_INT 	BIT_SET_END_EMAIL_SENT								3	// END EMAIL
CONST_INT 	BIT_SET_ANIMAL_01									4	// BOAR			-	A_C_BOAR
CONST_INT 	BIT_SET_ANIMAL_02									5	// CAT			-	A_C_CAT_01
CONST_INT 	BIT_SET_ANIMAL_03									6	// CHICKENHAWK	-	A_C_CHICKENHAWK
CONST_INT 	BIT_SET_ANIMAL_04									7	// CORMORANT	-	A_C_CORMORANT
CONST_INT 	BIT_SET_ANIMAL_05									8	// COW			-	A_C_COW
CONST_INT 	BIT_SET_ANIMAL_06									9	// COYOTE		-	A_C_COYOTE
CONST_INT 	BIT_SET_ANIMAL_07									10	// CROW			-	A_C_CROW
CONST_INT 	BIT_SET_ANIMAL_08									11	// DEER			-	A_C_DEER
CONST_INT 	BIT_SET_ANIMAL_09									12	// DOLPHIN		-	A_C_DOLPHIN
CONST_INT 	BIT_SET_ANIMAL_10									13	// FISH			-	A_C_FISH
CONST_INT 	BIT_SET_ANIMAL_11									14	// HEN			-	A_C_HEN
CONST_INT 	BIT_SET_ANIMAL_12									15	// HUMPBACK		-	A_C_HUMPBACK
CONST_INT 	BIT_SET_ANIMAL_13									16	// HUSKY		-	A_C_HUSKY
CONST_INT 	BIT_SET_ANIMAL_14									17	// KILLERWHALE	-	A_C_KILLERWHALE
CONST_INT 	BIT_SET_ANIMAL_15									18	// MTLION		-	A_C_MTLION
CONST_INT 	BIT_SET_ANIMAL_16									19	// PIG			-	A_C_PIG
CONST_INT 	BIT_SET_ANIMAL_17									20	// PIGEON		-	A_C_PIGEON
CONST_INT 	BIT_SET_ANIMAL_18									21	// POODLE		-	A_C_POODLE
CONST_INT 	BIT_SET_ANIMAL_19									22	// PUG			-	A_C_PUG
CONST_INT 	BIT_SET_ANIMAL_20									23	// RABBIT		-	A_C_RABBIT_01
CONST_INT 	BIT_SET_ANIMAL_21									24	// RETRIEVER	-	A_C_RETRIEVER
CONST_INT 	BIT_SET_ANIMAL_22									25	// ROTTWEILER	-	A_C_ROTTWEILER	|	A_C_CHOP
CONST_INT 	BIT_SET_ANIMAL_23									26	// SEAGULL		-	A_C_SEAGULL
CONST_INT 	BIT_SET_ANIMAL_24									27	// SHARKHAMMER	-	A_C_SHARKHAMMER
CONST_INT 	BIT_SET_ANIMAL_25									28	// SHARKTIGER	-	A_C_SHARKTIGER
CONST_INT 	BIT_SET_ANIMAL_26									29	// SHEPARD		-	A_C_SHEPARD
CONST_INT 	BIT_SET_ANIMAL_27									30	// STINGRAY		-	A_C_STINGRAY
CONST_INT 	BIT_SET_ANIMAL_28									31	// WESTY		-	A_C_WESTY


CONST_INT	NUMBER_OF_ANIMALS									20	// 28


/// PURPOSE:
///    Resets all bits
PROC CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_DATA()
	INT i_Temp
	REPEAT 32 i_Temp
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
			CLEAR_BIT(g_savedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
		ENDIF
	ENDREPEAT
ENDPROC



/// PURPOSE:
///    Removes all Wildlife Photography text messages in player's cellphone inbox
PROC CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_TEXT_DATA()
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("PHT_WLD_BEV1")
ENDPROC



/// PURPOSE:
///    Removes all emails in player's cellphone inbox
PROC CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_EMAIL_DATA()
	INITIALISE_EMAIL_SYSTEM()
ENDPROC



/// PURPOSE:
///    Gets the number of completed photographs sent by the player
/// RETURNS:
///    INT
FUNC INT GET_NUMBER_OF_COMPLETED_ANIMAL_PHOTOS()
	INT completeCount, iTemp
	REPEAT 32 iTemp
		IF iTemp > 3
			IF IS_BIT_SET(g_savedGlobals.sAmbient.iWildlifePhotographsFlags, iTemp)
				completeCount++
			ENDIF
		ENDIF
	ENDREPEAT
	
	// SCALEFORM FIX FOR DEBUG COLLEDCT ALL PHOTOGRAPHS
	IF completeCount > 20
		completeCount = 20
	ENDIF
	
	RETURN completeCount
ENDFUNC
