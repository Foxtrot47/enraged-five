#IF IS_DEBUG_BUILD
USING "family_Debug.sch"
#ENDIF

USING "respawn_location_private.sch"

///private header for family coord scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY COORD PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC BOOL PRIVATE_Get_FamilyMember_Init_Offset(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR &vInitOffset, FLOAT &fInitHead)
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<13.9040, 1.1930, 0.7000>>
					fInitHead = -144.0000
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					/*
					vInitOffset = <<14.7130, 0.0199, 0.7000>>
					fInitHead = 132.7661
					RETURN TRUE
					*/
					
					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, eFamilyEvent, vInitOffset, fInitHead)
						vInitOffset	+= <<0.8090, -1.1731, 0.0>>
						fInitHead	+= 456.7661-360
						RETURN TRUE
					ENDIF
				BREAK
				CASE FM_MICHAEL_WIFE
					/*
					vInitOffset = <<15.8655, 1.9770, 0.7000>>
					fInitHead = 93.0000
					RETURN TRUE
					*/
					
//					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, eFamilyEvent, vInitOffset, fInitHead)
//						vInitOffset	+= <<1.9615, 0.7840, 0.0000>>
//						fInitHead	+= 237.0000
//						RETURN TRUE
//					ENDIF
					
					
					vInitOffset	= <<-796.9911,180.5483,71.8356>>	- << -812.0607, 179.5117, 71.1531 >>
					fInitHead	= 16.00								- 222.8314							 + 360	
					RETURN TRUE
				BREAK
			ENDSWITCH
			
			RETURN FALSE
		BREAK
		CASE FE_M_FAMILY_MIC4_locked_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<10.9793, -5.9000, 4.6000>>
					fInitHead = 163.0716
					RETURN TRUE
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					/*
					vInitOffset = <<14.7130, 0.0199, 0.7000>>
					fInitHead = 132.7661
					RETURN TRUE
					*/
					
					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, eFamilyEvent, vInitOffset, fInitHead)
						vInitOffset	+= <<-0.6, 0.2500, 0.0>>
						fInitHead	+= 0.0
						RETURN TRUE
					ENDIF
				BREAK
				CASE FM_MICHAEL_WIFE
					/*
					vInitOffset = <<15.8145, 1.8120, 0.7000>>
					fInitHead = 282.0000
					RETURN TRUE
					*/
					
					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, eFamilyEvent, vInitOffset, fInitHead)
						vInitOffset	+= <<-1.2, -0.5000, 0.0>>
						fInitHead	+= 0.0
						RETURN TRUE
					ENDIF
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M7_FAMILY_finished_breakfast
		CASE FE_M7_FAMILY_finished_pizza
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
				CASE FM_MICHAEL_WIFE
					vInitOffset	= <<-796.991,180.548,71.835>>		- << -812.0607, 179.5117, 71.1531 >>
					fInitHead	= 16.00+90							- 222.8314							 + 360	
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<6.8878, -5.5247, 1.5228>>
					fInitHead = 160.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_SON_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<4.1937, -9.0309, 4.5877>>
					fInitHead = 255.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_in_room_asks_for_munchies
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<4.4577, -9.6399, 4.5876>>
					fInitHead = 252.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_SON_gaming_loop
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<3.7990, -9.4100, 4.5877>>
					fInitHead = 273.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_gaming
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M2_SON_gaming_loop, vInitOffset, fInitHead)
						vInitOffset	+= <<0,0,0>>
						fInitHead	+= 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_SON_gaming_exit
//			RETURN PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M2_SON_gaming_loop, vInitOffset, fInitHead)
//		BREAK
//		CASE FE_M7_SON_gaming_exit
//			RETURN PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M7_SON_gaming, vInitOffset, fInitHead)
//		BREAK
		CASE FE_M_SON_rapping_in_the_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<12.5520, 4.3730, 4.6000+1.0>>
					fInitHead = 318.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Borrows_sisters_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<-0.4395, -11.0022, 0.0747>>
					fInitHead = 306.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_watching_porn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<5.5001, -12.4406, 4.6000>>
					fInitHead = 0.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_phone_calls_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<3.5000, -9.1000, 4.6000>>
					fInitHead = 330.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<0.8490, 1.5750, 0.0000>>
					fInitHead = 272.8570
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<8.1830, -3.0467, 5.5876>>
					fInitHead = 158.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_B
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<9.3830, -3.3887, 5.7300>>
					fInitHead = 339.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_C
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<-802.702,176.176,76.890>>	- << -812.0607, 179.5117, 71.1531 >>	//<<9.2990, -3.3637, 5.7300>>
					fInitHead = -159.23							- 222.8314+360							//339.0000
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_D
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<-802.702,176.176,76.890>>	- << -812.0607, 179.5117, 71.1531 >>	//<<9.3830, -3.4987, 5.7300>>
					fInitHead = -159.23							- 222.8314+360							//330.0000
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_SON_smoking_weed_in_a_bong
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<4.4880, -9.0980, 5.4864>>
					fInitHead = 147.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_raids_fridge_for_food
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<9.0400, 5.7100, 1.4400>>
					fInitHead = 159.3182
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_jumping_jacks
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					
					vInitOffset = <<3.2500, -9.5300, 4.5880>>
					fInitHead = 333.0000
					RETURN TRUE
					
//					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, FE_M2_SON_gaming_loop, vInitOffset, fInitHead)
//						vInitOffset	+= <<0,0,1>>
//						fInitHead	+= 90
//						RETURN TRUE
//					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_going_for_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<-0.4395, -11.0022, 0.0>>+<<get_random_float_in_range(-0.5,0.5), get_random_float_in_range(-0.5,0.5), 0.0>>
					fInitHead = 306.0000+get_random_float_in_range(10,10)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset =  <<-863.5425, 141.5129, 60.2063>>		- << -812.0607, 179.5117, 71.1531 >> - <<0,0,1>>
					vInitOffset *= 0.95
					fInitHead = GET_HEADING_FROM_VECTOR_2D(vInitOffset.x, vInitOffset.y)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
//					vInitOffset = <<13.9040, 1.1930, 0.7000>>
//					fInitHead = -144.0000
//					RETURN TRUE
				
					vInitOffset = <<-796.7593, 180.4725, 71.8266>>		- << -812.0607, 179.5117, 71.1531 >> //- <<0,0,1>>
					fInitHead = 26.0870									- 222.8314+360
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_SON_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					vInitOffset = <<-805.1730, 173.9870, 72.6876>>		- << -812.0607, 179.5117, 71.1531 >> //- <<0,0,1>>
					fInitHead = 13.0000									- 222.8314+360
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<-803.1823, 172.4196, 72.8447>>		- << -812.0607, 179.5117, 71.1531 >> //- <<0,0,1>>
					fInitHead = -28.7112								- 222.8314+360
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<27.9648, 8.3766, 0.7000>>	//<<26.9648, 8.3766, 0.6000>>
					fInitHead = 319.0000						//56.1021
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.3000, -6.5000, 0.6000>>+<<0,0,0.1>>
					fInitHead = 78.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<-0.3142, -10.9926, 0.0747>>
					fInitHead = 304.1026
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_walks_to_room_music
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.3000, -6.5000, 4.6000>>
					fInitHead = 180.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_dancing_practice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_DAUGHTER, FE_M_DAUGHTER_workout_with_mp3, vInitOffset, fInitHead)
						vInitOffset	+= <<0,0,0.0>>
						fInitHead	+= 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<8.5050, -10.9717, 4.6019>>
					fInitHead = 228.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<12.7165, 4.7109, 4.6000+1.0>>
					fInitHead = 310.6696
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<9.0529, -8.9175, 1.6907>>
					fInitHead = 102.1560
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<8.4799, -7.5315, 0.6820>>
					fInitHead = 264.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<9.3520, -3.3560, 5.7338>>
					fInitHead = 338.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<8.3003, -10.3127, 4.5874>>
					fInitHead = 262.7890
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.76, -5.95, 4.59>>
					fInitHead = 177.6766
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<9.6691, -7.6405, 0.6817>>
					fInitHead = 159.1560
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<1.2280, 4.0440, 0.7665>>		//<<1.2280, 4.1190, 0.7665>>
					fInitHead = 165.0000							//180.0000
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.2365, -7.6073, 6.2761>>
					fInitHead = 157.0716
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_couchsleep
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<8.1610, -7.5620, 2.1700>>
					fInitHead = 78.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends
		CASE FE_M_DAUGHTER_on_phone_LOCKED
		CASE FE_M7_DAUGHTER_studying_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.7668, -7.8757, 5.5559>>
					fInitHead = 144.6939
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<10.6257, -9.4927, 5.5876>>
					fInitHead = 257.4583
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_sunbathing
			RETURN PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, FE_M2_DAUGHTER_sunbathing, vInitOffset, fInitHead)
		BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<8.4451, 3.1568, 1.4700>>
					fInitHead = 159.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<9.0477, -7.9397, 0.6817>>
					fInitHead = 88.5686
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_playing_tennis
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<0.0, 0.0, 0.0>>
					fInitHead = 0.0
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_doing_yoga
		CASE FE_M7_WIFE_doing_yoga
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<21.7000, 9.0000, 1.7029>>
					fInitHead = 0.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_nails_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<9.1622, -7.8038, 0.6000>>
//					fInitHead = 175.5910
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_leaving_in_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<2.2978, 2.7840, 1.1823>>
//					fInitHead = 247.7200
					
					vInitOffset = <<1.3278, 1.5340, 0.0000>>
					fInitHead = 159.7200
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<4.1908, 3.4879, 2.5500>>
//					fInitHead = 252.7200
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
////					vInitOffset = <<2.2978, 2.7840, 1.1823>>
////					fInitHead = 247.7200
//					
//					vInitOffset = <<0.9678, 1.4440, 0.0>>
//					fInitHead = 247.7200
//					
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<-3.0265, 1.7627, 0.1273>>
					fInitHead = 338.5000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle
//		CASE FE_M7_WIFE_with_shopping_bags_idle
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<-2.1475, 1.8947, 0.0>>+<<0,0,1>>
//					fInitHead = 342.0000
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit
//		CASE FE_M7_WIFE_with_shopping_bags_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<-2.1475, 1.8947, 0.0>>
//					fInitHead = 342.0000
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<15.2374, 8.7230, 1.4495>>
					fInitHead = 205.6770
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<28.7000, 8.8241, 0.6000>>
					fInitHead = 39.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_botox_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<7.8611, -4.7644, 0.6000>>
//					fInitHead = 10.0839
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<9.0567, -9.0107, 1.1936>>+<<0,0,0.03 - 0.02>>
					fInitHead = 94.2086
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_passed_out_BED
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<-2.8213, 2.0403, 5.2891>>+<<0,0,0.03 - 0.05>>
					fInitHead = 336.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//				CASE FM_MICHAEL_SON
//					vInitOffset = <<5.5962, 5.7205, 3.8550>>
//					fInitHead = 324.4000
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				CASE FM_MICHAEL_SON
					vInitOffset = <<5.5782, 5.7755, 3.8400>>
					fInitHead = 311.4000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_screaming_at_son_P3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				CASE FM_MICHAEL_SON
					vInitOffset = <<5.5782, 5.7755, 3.8418>>
					fInitHead = 306.6000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<5.8092, 5.7965, 3.8500>>	//<<5.5782, 4.9055, 3.8500>>
					fInitHead = 337.4000						// 344.4000
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist
		CASE FE_M7_WIFE_phones_man_OR_therapist
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<13.3221, 0.0684, 0.6817>>
					fInitHead = 277.3170
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, FE_M2_WIFE_phones_man_OR_therapist, vInitOffset, fInitHead)
				vInitOffset +=  <<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>
				fInitHead += get_random_float_in_range(-10,10)
				RETURN TRUE
			ENDIF
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M_WIFE_using_vibrator_END
		CASE FE_M7_WIFE_using_vibrator
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<-2.2350, 1.7250, 4.5877>>
					fInitHead = 172.7140
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
		CASE FE_M2_WIFE_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					vInitOffset = <<-814.2460, 181.2640, 75.7407>>	- << -812.0607, 179.5117, 71.1531 >>		//<<-2.5500, 1.4700, 4.6>>
					fInitHead = -158.0000							- 222.8314+360								//340.7140
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE 
					vInitOffset = <<-812.8960, 181.1140, 76.7233>>	- << -812.0607, 179.5117, 71.1531 >>		//<<-2.5500, 1.4700, 4.6>>
					fInitHead = -164.0000							- 222.8314+360								//340.7140
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_Making_juice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					vInitOffset = <<7.3940, 5.3680, 1.4510>>
					fInitHead = 235.0656
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_shopping_with_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
				CASE FM_MICHAEL_DAUGHTER
					vInitOffset = <<1.0704, -1.4624, 0.9990>>
					fInitHead = 157.4400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M7_WIFE_shopping_with_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//				CASE FM_MICHAEL_SON
//					vInitOffset = <<1.0704, -1.4624, 0.9990>>
//					fInitHead = 157.4400
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M7_WIFE_on_phone
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					vInitOffset = <<9.1880, 4.9480, 0.4524>>
//					fInitHead = 348.8163
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
//		CASE FE_M_MEXMAID_cooking_for_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID
//					vInitOffset = <<12.1186, 7.4253, 0.5000>>
//					fInitHead = 169.3193
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<14.8186, 6.8253, 0.5000>>
					fInitHead = 87.3368
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_MEXMAID_clean_surface_a
		CASE FE_M2_MEXMAID_clean_surface_c
		CASE FE_M7_MEXMAID_clean_surface
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<13.6486, 6.2253, 0.4500>>
					fInitHead = 262.3200
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_MEXMAID_clean_surface_b
			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, FE_M_WIFE_screams_at_mexmaid, vInitOffset, fInitHead)
				vInitOffset += <<0,0,0>>
				fInitHead += 0.0
				RETURN TRUE
			ENDIF
		BREAK
		CASE FE_M2_MEXMAID_clean_window
		CASE FE_M7_MEXMAID_clean_window
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<16.3190, -1.3750, 0.6817>>
					fInitHead = 334.1260
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_MEXMAID_MIC4_clean_surface
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID
//					vInitOffset = <<0.6930, -0.7890, 0.0000>>
//					fInitHead = 335.2406
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<-0.3900, 3.2700, 0.0>>
					fInitHead = 154.1260
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_MEXMAID_does_the_dishes
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<15.0213, 6.9622, 1.3900>>
					fInitHead = 142.8890
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_MEXMAID_makes_calls
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID
//					vInitOffset = <<11.9397, 4.1404, 0.3000>>
//					fInitHead = 273.0000
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_MEXMAID_watching_TV
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID
//					vInitOffset = <<8.8767, -7.4807, 1.8100>>
//					fInitHead = 82.2086
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_MEXMAID_stealing_stuff
		CASE FE_M_MEXMAID_stealing_stuff_caught
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					vInitOffset = <<15.1280, 6.8420, 0.4524>>
					fInitHead = 54.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<-10.9019, -23.8349, -2.1000>>
					fInitHead = 339.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_planting_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<-10.9937, -24.9005, -2.1800>>
					fInitHead = 314.2132
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_trimming_hedges
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER
//					vInitOffset = <<-8.2000, -15.2000, -1.1000>>
//					fInitHead = 208.5030
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_cleaning_pool
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<31.7401, -1.0943, 0.6822>>
					fInitHead = 147.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_mowing_lawn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<-27.1728, -3.7500, -1.6260>>
					fInitHead = 147.0000+90
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_watering_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<14.7722, -10.9336, -0.5370>>
					fInitHead = 110.8505
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER
//					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_GARDENER, FE_M_GARDENER_watering_flowers, vInitOffset, fInitHead)	vInitOffset	+= <<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>	fInitHead	+= get_random_float_in_range(-10,10)	RETURN TRUE	ENDIF
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<-17.2536, 2.4510, -0.6000>>
					fInitHead = 0.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_smoking_weed
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER
					vInitOffset = <<23.9716, -20.3533, -3.180>>
					fInitHead = 324.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_M_MICHAEL_MIC2_washing_face
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MICHAEL
//					vInitOffset = <<-803.884, 169.288, 76.750>>	- << -812.0607, 179.5117, 71.1531 >>+<<0,0,-1>>
//					fInitHead = 105.0000							- 222.8314
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					vInitOffset = <<3.1416, -2.0799, -0.0145>>
					fInitHead = 33.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_in_face_mask
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					vInitOffset = <<3.2746, -4.1995, -0.0144>>
					fInitHead = 69.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_watching_TV
		CASE FE_F_AUNT_returned_to_aunts
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					vInitOffset = <<2.9830, -4.0940, 0.4629>>
					fInitHead = 33.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					vInitOffset = <<5.5235, 2.9133, 0.7288>>
					fInitHead = 71.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//				CASE FM_FRANKLIN_STRETCH
//					vInitOffset = <<2.5530, 9.5050, 0.0000>>
//					fInitHead = 183.1200
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//				CASE FM_FRANKLIN_STRETCH
//					vInitOffset = <<-7.3527, 12.0919, 0.5827>>
//					fInitHead = 200.8240
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			SWITCH eFamilyMember						
//				CASE FM_FRANKLIN_LAMAR
//				CASE FM_FRANKLIN_STRETCH
//					vInitOffset = <<	-1.6062, -13.4051, -0.47>>+<<0,0,1>>
//					fInitHead =	352.335
//					RETURN TRUE
//				BREAK						
//			ENDSWITCH
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//				CASE FM_FRANKLIN_STRETCH
//					vInitOffset = <<-1.6062, -12.9351, 0.5311>>
//					fInitHead = 352.3350
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_LAMAR
					vInitOffset = <<-42.2059, -1454.6757, 30.7131>>	- << -14.3064, -1435.9974, 30.1160 >>
					fInitHead = 84.7009								- 188.5817
					RETURN TRUE
				BREAK
				CASE FM_FRANKLIN_STRETCH
					/* */
					vInitOffset = <<-40.7478, -1455.8477, 30.6803>>	- << -14.3064, -1435.9974, 30.1160 >>
					fInitHead = 90.6417								- 188.5817
					RETURN TRUE
					/* */
					
//					IF PRIVATE_Get_FamilyMember_Init_Offset(FM_FRANKLIN_LAMAR, eFamilyEvent, vInitOffset, fInitHead)
//						vInitOffset	+= <<	-1.3962, -0.0449, 0>>
//						fInitHead	+= -310.847
//						RETURN TRUE
//					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RON_monitoring_police_frequency
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<2.0126, 0.3422, -0.0086>>
					fInitHead = 354.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<1.2931, 1.4476, 1.0127>>
					fInitHead = 61.7525
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING
		CASE FE_T0_RON_ranting_about_government_SITTING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<0.0158, 1.4182, 0.9913>>
					fInitHead = 119.0080
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK

		CASE FE_T0_RON_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON 
					vInitOffset = <<-3.4670, 0.0740, 0.0>>
					fInitHead = 30.0000
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON 
					vInitOffset = <<-2.1014, -5.4089, -0.0130>>
					fInitHead = 123.7530
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_stares_through_binoculars
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<-2.8020, -6.3295, -0.0100>>
					fInitHead = 42.7570
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					vInitOffset = <<-2.1560, -4.9564, 1.4886>>
					fInitHead = 136.3090
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_sunbathing
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					vInitOffset = <<7.2656, 3.0220, -0.9061>>
					fInitHead = 92.8891
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_drinking_beer
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					vInitOffset = <<0.5066, 1.6992, 0.0168>>
					fInitHead = 115.7700
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL
					vInitOffset = <<-0.2121, -6.2791, -0.0089>>
					fInitHead = 114.3391
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR       
				CASE FM_TREVOR_0_WIFE
					vInitOffset = <<8.2458, -0.7136, -0.2503>>
					fInitHead = 252.2687
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
				CASE FM_TREVOR_0_WIFE
					vInitOffset = <<2.3340, 1.6889, 0.5860>>
					fInitHead = 109.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					vInitOffset = <<-0.6331, 1.2093, -0.0086>>
					fInitHead = 137.1861
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					vInitOffset = <<-3.8870, -0.1660, 0.0>>
					fInitHead = 30.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		/*
		CASE FE_T0_TREVOR_and_kidnapped_wife_laugh       
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR   
				CASE FM_TREVOR_0_WIFE
					vInitOffset = <<-0.6868, 0.8875, 0.0535>>
					fInitHead = 98.5571
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		*/
		CASE FE_T0_TREVOR_blowing_shit_up
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					vInitOffset = <<3.5179, 9.1355, -1.0000>>
					fInitHead = 141.7900
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					vInitOffset = <<-6.0130, -1.9326, 0.1500>>+<<0,0,1>>
					fInitHead = 121.6591
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<10.2823, 2.2595, -1.0742>>
					fInitHead = 170.2796-180
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<1.0770, -5.1446, 0.9904>>
					fInitHead = 107.1399
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON 
					vInitOffset = <<-12.9030, -6.6230, -0.0406>>
					fInitHead = 63.4800
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_doing_target_practice
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					vInitOffset = <<10.5514, 5.3930, -0.0998>>
					fInitHead = 327.4830
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_RONEX_conspiracies_boring_Michael       
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_RON
//					vInitOffset = <<3.5000, 1.0000, 0.0000>>
//					fInitHead = 20.8535
//					RETURN TRUE
//				BREAK
//				CASE FM_TREVOR_0_MICHAEL
//					vInitOffset = <<2.5000, 0.0000, 0.0000>>
//					fInitHead = 270.7200
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					vInitOffset = <<0.9794, -0.3043, -0.0>>	//<<0.9794, -0.3043, -0.0600>>
					fInitHead = 179.7612
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					vInitOffset = <<11.9759, 1.0000, -1.0728>>
					fInitHead = 238.9422
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
				CASE FM_TREVOR_0_MICHAEL 
					vInitOffset = <<-0.5760, 1.6710, 0.6010>>
					fInitHead = 296.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_KIDNAPPED_WIFE_cooking_a_meal
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_WIFE
//					vInitOffset = <<0.7560, -0.6500, 0.0>>
//					fInitHead = 138.3330
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar
//		CASE FE_T0_MOTHER_something_b
//		CASE FE_T0_MOTHER_something_c
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MOTHER
					vInitOffset = <<-0.6532, 0.9556, -0.0087>>
					fInitHead = 121.0556
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T1_FLOYD_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<-0.1800, -4.1730, 0.9981>>
					fInitHead = 216.6100
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
//					/*
					vInitOffset = <<6.5937, 0.3521, 1.0000>>
					fInitHead = 119.4000
					RETURN TRUE
//					*/
//					
//					IF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDSPOON_A
//					OR g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDSPOON_A2
//						vInitOffset = <<-1145.3680, -1515.5900, 9.5847>>	- << -1152.5707, -1517.6010, 9.6346 >>	+ <<0,0,1>>
//						fInitHead = 28.7271									- 90.6729								+ 180.0
//						RETURN TRUE
//					ELIF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDSPOON_B
//					OR g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDSPOON_B2
//						vInitOffset = <<-1145.4370, -1515.6490, 9.5894>>	- << -1152.5707, -1517.6010, 9.6346 >>	+ <<0,0,1>>
//						fInitHead = 28.7271									- 90.6729								+ 180.0
//						RETURN TRUE
//					ENDIF
//					
//					IF GET_RANDOM_BOOL()
//						vInitOffset = <<-1145.3680, -1515.5900, 9.5847>>	- << -1152.5707, -1517.6010, 9.6346 >>	+ <<0,0,1>>
//						fInitHead = 28.7271									- 90.6729								+ 180.0
//						RETURN TRUE
//					ELSE
//						vInitOffset = <<-1145.4370, -1515.6490, 9.5894>>	- << -1152.5707, -1517.6010, 9.6346 >>	+ <<0,0,1>>
//						fInitHead = 28.7271									- 90.6729								+ 180.0
//						RETURN TRUE
//					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_on_sofa
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					IF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDCRYING_A
						vInitOffset = <<-1158.1331, -1521.3940, 9.6327>>	- << -1152.5707, -1517.6010, 9.6346 >>
						fInitHead = 34.6610									- 90.6729
						RETURN TRUE
					ELIF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDCRYING_E0
					OR g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDCRYING_E1
					OR g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDCRYING_E2
					OR g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_TREVOR] = PR_SCENE_T_FLOYDCRYING_E3
						vInitOffset = <<-1157.8030, -1521.3340, 9.6327>>	- << -1152.5707, -1517.6010, 9.6346 >>
						fInitHead = 32.0000									- 90.6729
						RETURN TRUE
					ENDIF
					
					IF GET_RANDOM_BOOL()
						vInitOffset = <<-1158.1331, -1521.3940, 9.6327>>	- << -1152.5707, -1517.6010, 9.6346 >>
						fInitHead = 34.6610									- 90.6729
						RETURN TRUE
					ELSE
						vInitOffset = <<-1157.8030, -1521.3340, 9.6327>>	- << -1152.5707, -1517.6010, 9.6346 >>
						fInitHead = 32.0000									- 90.6729
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_pineapple
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<-1156.4220, -1519.5610, 10.6327>>	- << -1152.5707, -1517.6010, 9.6346 >>
					fInitHead = 102.0000								- 90.6729
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<-2.8352, 0.0552, 0.0>>
					fInitHead = 334.4400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, FE_T1_FLOYD_on_phone_to_girlfriend, vInitOffset, fInitHead)
						vInitOffset +=  <<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>
						fInitHead += get_random_float_in_range(-10,10)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<-7.2130, -1.4536, 0.9981>>
					fInitHead = 42.6000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<-1148.4664, -1518.7363, 9.6327+1>>	- << -1152.5707, -1517.6010, 9.6346 >>
					fInitHead = get_random_float_in_range(0,360)
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					#IF NOT USE_TU_CHANGES
					vInitOffset = <<-1147.4023, -1511.5037, 9.6327+1>>	- << -1152.5707, -1517.6010, 9.6346 >>
					fInitHead = get_random_float_in_range(0,360)
					#ENDIF
					#IF USE_TU_CHANGES
					vInitOffset = <<5.240 , 6.217 , 0.998>>
					fInitHead = 315.720
					#ENDIF
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_is_sleeping
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<6.6447, 0.7635, 1.0507>>
					fInitHead = 296.6830
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T1_FLOYD_with_wade_post_trevor3
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_1_FLOYD
//					vInitOffset = <<-2.6358, -0.6578, 0.0>>
//					fInitHead = 12.8970
//					RETURN TRUE
//				BREAK
//				CASE FM_TREVOR_1_WADE
//					vInitOffset = <<-4.8394, -0.1848, 0.0>>
//					fInitHead = 112.8970
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T1_FLOYD_with_wade_post_docks1
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					vInitOffset = <<3.9680, -1.0400, 0.0000>>
					fInitHead = 214.9200
					RETURN TRUE
				BREAK
				CASE FM_TREVOR_1_WADE
					vInitOffset = <<4.6170, -1.1314, 0.0000>>
					fInitHead = 39.4085
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		
		CASE FE_ANY_find_family_event
		CASE FE_ANY_wander_family_event
			//
			vInitOffset = <<0,0,0>>
			fInitHead = 0.0
			RETURN FALSE
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			//
			vInitOffset = <<0,0,0>>
			fInitHead = 0.0
			RETURN FALSE
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in PRIVATE_Get_FamilyMember_Init_Offset()")
			PRINTNL()
			
			SCRIPT_ASSERT("invalid eFamilyEvent in PRIVATE_Get_FamilyMember_Init_Offset()")
			#ENDIF
			
			vInitOffset = <<get_random_float_in_range(-10,10), get_random_float_in_range(-10,10), 0.0>>
			fInitHead = get_random_float_in_range(0,360)
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD					
	TEXT_LABEL_63 sRandom
	sRandom  = ("[")
	INT iFamilyMemberLength
	iFamilyMemberLength = GET_LENGTH_OF_LITERAL_STRING(Get_String_From_FamilyMember(eFamilyMember))
	IF iFamilyMemberLength >= 6
		sRandom += GET_STRING_FROM_STRING(Get_String_From_FamilyMember(eFamilyMember),
				iFamilyMemberLength - 6,
				iFamilyMemberLength)
	ELSE
		sRandom += Get_String_From_FamilyMember(eFamilyMember)
	ENDIF
	
	sRandom += ("] \"")
	INT iFamilyEventLength
	iFamilyEventLength = GET_LENGTH_OF_LITERAL_STRING(Get_String_From_FamilyEvent(eFamilyEvent))
	IF iFamilyEventLength >= 10
		sRandom += GET_STRING_FROM_STRING(Get_String_From_FamilyEvent(eFamilyEvent),
				iFamilyEventLength - 10,
				iFamilyEventLength)
	ELSE
		sRandom += Get_String_From_FamilyEvent(eFamilyEvent)
	ENDIF
	
	sRandom += ("\" invalid famMember offset")
	
	PRINTSTRING("<")
	PRINTSTRING(GET_THIS_SCRIPT_NAME())
	PRINTSTRING("> ")
	PRINTSTRING(sRandom)
	PRINTNL()
	
	SCRIPT_ASSERT(sRandom)
	#ENDIF
	
	vInitOffset = <<get_random_float_in_range(-10,10), get_random_float_in_range(-10,10), 0.0>>
	fInitHead = get_random_float_in_range(0,360)
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Get_FamilyMember_Vehicle_Offset(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR &vVehOffset, FLOAT &fVehHead, enumCharacterList &eVehPed, VEHICLE_CREATE_TYPE_ENUM &eVehCreateType,
		BOOL bParkedWithoutEvent = FALSE)
	
	VECTOR vMicSceneCoord	= << -812.0607, 179.5117, 71.1531 >>
	FLOAT fMicSceneHead		= 222.8314
	
	eVehCreateType = VEHICLE_TYPE_DEFAULT
	IF bParkedWithoutEvent
		SWITCH eFamilyMember
			CASE FM_MICHAEL_DAUGHTER
				vVehOffset	=  <<-826.3746, 156.3374, 68.3336>>		- vMicSceneCoord	//<<-15.3933, -2.9421, 0.0>>
				fVehHead	= 81.6041								- fMicSceneHead		//272.5119
				
				eVehCreateType = VEHICLE_TYPE_CAR
				eVehPed = CHAR_TRACEY
				RETURN TRUE
			BREAK
			CASE FM_MICHAEL_WIFE
				vVehOffset	= <<-816.9271, 159.4610, 69.9654>>		- vMicSceneCoord	//<<-4.8664, -20.0507, -1.1877>>
				fVehHead	= 105.9127								- fMicSceneHead		//-116.9187				
				
				eVehCreateType = VEHICLE_TYPE_CAR
				eVehPed = CHAR_AMANDA
				RETURN TRUE
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH eFamilyEvent
		CASE FE_M_SON_Borrows_sisters_car
//			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
//					vVehOffset, fVehHead)
//				vVehOffset	+= <<-3.4000, -9.4000, 0.0>>
//				fVehHead	+= -110+45 + GET_RANDOM_FLOAT_IN_RANGE(-1,1)
//				
//				eVehCreateType = VEHICLE_TYPE_CAR
//				eVehPed = CHAR_TRACEY
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car
			INT iRandom_DAUGHTER_Going_out_in_her_car
			iRandom_DAUGHTER_Going_out_in_her_car = GET_RANDOM_INT_IN_RANGE(0, 100)
			
			IF (iRandom_DAUGHTER_Going_out_in_her_car < 5)
				vVehOffset	=  <<-44.279,-1.958,-0.500>>
				fVehHead	= 135.720
			
				eVehCreateType = VEHICLE_TYPE_CAR
				eVehPed = CHAR_TRACEY
				RETURN TRUE
			ELIF (iRandom_DAUGHTER_Going_out_in_her_car < 35)
				vVehOffset	= <<-16.800,-21.300,-0.500>>		//<<-11.279,0.142,-0.500>>
				fVehHead	= -130.944							//-82.944
				
				eVehCreateType = VEHICLE_TYPE_CAR
				eVehPed = CHAR_TRACEY
				RETURN TRUE
			ENDIF
			
			vVehOffset	= <<-3.779, -18.458, -0.5000>>
			fVehHead 	= -100.944
			
			eVehCreateType = VEHICLE_TYPE_CAR
			eVehPed = CHAR_TRACEY
			RETURN TRUE
		BREAK
		CASE FE_M7_SON_going_for_a_bike_ride
			INT iRandom_SON_going_for_a_bike_ride
			iRandom_SON_going_for_a_bike_ride = GET_RANDOM_INT_IN_RANGE(0, 100)
			
			IF (iRandom_SON_going_for_a_bike_ride < 5)
				vVehOffset	=  <<-44.279,-1.958,-0.500>>
				fVehHead	= 135.720
			
				eVehCreateType = VEHICLE_TYPE_BIKE
				eVehPed = CHAR_JIMMY
				RETURN TRUE
			ELIF (iRandom_SON_going_for_a_bike_ride < 35)
				vVehOffset	= <<-16.800,-21.300,-0.500>>		//<<-11.279,0.142,-0.500>>
				fVehHead	= -130.944							//-82.944
				
				eVehCreateType = VEHICLE_TYPE_BIKE
				eVehPed = CHAR_JIMMY
				RETURN TRUE
			ENDIF
			
			vVehOffset	= <<-3.779, -18.458, -0.5000>>
			fVehHead 	= -100.944
			
			eVehCreateType = VEHICLE_TYPE_BIKE
			eVehPed = CHAR_JIMMY
			RETURN TRUE
		BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride
			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
					vVehOffset, fVehHead)
				vVehOffset	+= <<0,0,0>>
				fVehHead	+= 0
				
				eVehCreateType = VEHICLE_TYPE_BIKE
				eVehPed = CHAR_JIMMY
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FE_M_WIFE_leaving_in_car
			IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
					vVehOffset, fVehHead)
				INT iRandom_WIFE_leaving_in_car
				iRandom_WIFE_leaving_in_car = GET_RANDOM_INT_IN_RANGE(0, 100)
				
				IF (iRandom_WIFE_leaving_in_car < 5)
					vVehOffset	=  <<-857.7297, 167.3424, 65.8745>>		- vMicSceneCoord	//<<-15.3933, -2.9421, 0.0>>
					fVehHead	= 354.6213								- fMicSceneHead		//272.5119
				
					eVehCreateType = VEHICLE_TYPE_CAR
					eVehPed = CHAR_AMANDA
					RETURN TRUE
				ELIF (iRandom_WIFE_leaving_in_car < 35)
					vVehOffset	+= <<-3.4000, -9.4000, 0.0>>
					fVehHead	+= -110+45 + GET_RANDOM_FLOAT_IN_RANGE(-1,1)
					
					eVehCreateType = VEHICLE_TYPE_CAR
					eVehPed = CHAR_AMANDA
					RETURN TRUE
				ENDIF
				
				vVehOffset	+= <<-8.5541, -0.3000, 0.0>>				- (<<2.3978, 2.8340, 1.2923>> - <<-5.6492, -1.3721, 0.0>>)
				fVehHead	+= 42 + GET_RANDOM_FLOAT_IN_RANGE(-1,1)		- (259.7200 - 242.5119)+90
				
				eVehCreateType = VEHICLE_TYPE_CAR
				eVehPed = CHAR_AMANDA
				RETURN TRUE
			ENDIF
		BREAK
		
		
		DEFAULT
			eVehPed = NO_CHARACTER
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("invalid eFamilyMember ")
	PRINTSTRING(Get_String_From_FamilyMember(eFamilyMember))
	PRINTSTRING(" in PRIVATE_Get_FamilyMember_Vehicle_Offset()")
	PRINTNL()
	
	SCRIPT_ASSERT("invalid eFamilyMember PRIVATE_Get_FamilyMember_Vehicle_Offset()")
	#ENDIF
	
	eVehPed = NO_CHARACTER
	RETURN FALSE
ENDFUNC
