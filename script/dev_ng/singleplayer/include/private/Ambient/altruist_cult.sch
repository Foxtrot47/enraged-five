
USING "globals.sch"
USING "dialogue_public.sch"
USING "random_events_public.sch"

FLOAT fDistanceBetweenPlayerAndDestination = -1

FUNC BOOL IS_CULT_FINISHED()
	IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds = 1000
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC REGISTER_CURRENT_GROUP_MEMBERS_FOR_CULT()
//Set in RE
	IF NOT IS_CULT_FINISHED()
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			g_bDeliverCurrentGroupMembersToCult = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
//Check in alruistcult.sc
	RETURN g_bDeliverCurrentGroupMembersToCult
ENDFUNC

PROC REGISTERED_GROUP_MEMBERS_ARE_DELIVERED_FOR_CULT()
//Set in alruistcult.sc
	g_bCurrentGroupMembersHaveBeenDeliveredToCult = TRUE
ENDPROC

FUNC BOOL HAVE_CURRENT_GROUP_MEMBERS_BEEN_DELIVERED_TO_CULT()
//Check in RE
	IF g_bCurrentGroupMembersHaveBeenDeliveredToCult
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_ALTRUIST_DROP_OFF)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL READY_FOR_CULT_DIALOGUE(VECTOR vHomeDestination)
//Check in RE
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
		AND NOT g_bAltruistCultDialogueHasPlayed
			IF fDistanceBetweenPlayerAndDestination = -1
				fDistanceBetweenPlayerAndDestination = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHomeDestination)
			ENDIF
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHomeDestination) > (fDistanceBetweenPlayerAndDestination + 200)
			OR GET_DISTANCE_BETWEEN_COORDS(<< -1014.15350, 4881.41113, 245.00009 >>, GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE)) < 400
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					g_bAltruistCultDialogueHasPlayed = TRUE
					RETURN TRUE
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL READY_FOR_FINAL_CULT_DIALOGUE_AND_MUSIC()
//Check in RE
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF ARE_CURRENT_GROUP_MEMBERS_REGISTERED_FOR_CULT()
			IF GET_DISTANCE_BETWEEN_COORDS(<< -1014.15350, 4881.41113, 245.00009 >>, GET_ENTITY_COORDS(GET_PLAYER_PED(GET_PLAYER_INDEX()), FALSE)) < 400
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT g_bAltruistCultExitMusicPlayed
					TRIGGER_MUSIC_EVENT("AC_EN_ROUTE_CULT")
					g_bAltruistCultExitMusicPlayed = TRUE
					IF NOT g_bAltruistCultFinalDialogueHasPlayed
						g_bAltruistCultFinalDialogueHasPlayed = TRUE
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				IF g_bAltruistCultExitMusicPlayed
					TRIGGER_MUSIC_EVENT("AC_LEFT_AREA")
					g_bAltruistCultExitMusicPlayed = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC RESET_CULT_STATUS()
//Cleanup in RE
	g_bDeliverCurrentGroupMembersToCult = FALSE
	g_bCurrentGroupMembersHaveBeenDeliveredToCult = FALSE
	g_bAltruistCultDialogueHasPlayed = FALSE
	g_bAltruistCultFinalDialogueHasPlayed = FALSE
	g_bAltruistCultExitMusicPlayed = FALSE
ENDPROC

PROC PRINT_CULT_HELP()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF NOT g_bAltruistCultHelpHasDisplayed
			PRINT_HELP("CULT_BLIP_HELP")
			g_bAltruistCultHelpHasDisplayed = TRUE
		ENDIF
	ENDIF
ENDPROC
