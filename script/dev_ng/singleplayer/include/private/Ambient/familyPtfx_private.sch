USING "commands_graphics.sch"
USING "commands_streaming.sch"

///private header for family particle effects control scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY particle effectsS PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC BOOL PRIVATE_Update_Family_Ptfx_Looped(ENTITY_INDEX entityIndex, enumFamilyEvents eFamilyEvent,
		STRING fxName, PTFX_ID &eventPtfx, FLOAT fDebugOffsetY)
	
	SWITCH eFamilyEvent
		CASE FE_M_SON_smoking_weed_in_a_bong
			IF (eventPtfx = NULL)
				
				REQUEST_PTFX_ASSET()
				IF NOT HAS_PTFX_ASSET_LOADED()
					
					PRINTSTRING("REQUESTING SCRIPT PTFX ")
					PRINTSTRING(fxName)
					PRINTSTRING("...")
					PRINTNL()
					
					REQUEST_PTFX_ASSET()
				ELSE
				
					VECTOR vSmoke_Pos, vSmoke_Rot
					
					CONST_FLOAT scale		1.0
					
					IF NOT ARE_STRINGS_EQUAL(fxName, "scr_fam_door_smoke")
						
						
						IF IS_ENTITY_A_PED(entityIndex)
//							vSmoke_Pos = <<0,0,0>>
//							vSmoke_Rot = <<0,0,0>>
//							IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
//									vSmoke_Pos, vSmoke_Rot, BONETAG_HEAD, scale)
//								eventPtfx = 0                     //INT_TO_NATIVE(PTFX_ID, GET_RANDOM_INT_IN_RANGE(1, 500000))
//								
//								IF bLooped
//									SCRIPT_ASSERT("FE_M_SON_smoking_weed_in_a_bong looped?")
//								ENDIF
//								
//								#IF IS_DEBUG_BUILD
//								PRINTSTRING("starting script ptfx non-looped \"")
//								PRINTSTRING(fxName)
//								PRINTSTRING("\" on ")
//								PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//								PRINTSTRING(" for ")
//								PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//								PRINTNL()
//								#ENDIF
//							ELSE
//								#IF IS_DEBUG_BUILD
//								PRINTSTRING("failing at starting a script ptfx non-looped \"")
//								PRINTSTRING(fxName)
//								PRINTSTRING("\" on ")
//								PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//								PRINTSTRING(" for ")
//								PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//								PRINTNL()
//								#ENDIF
//							ENDIF
						ELSE
							IF NOT DOES_ENTITY_EXIST(entityIndex)
								SCRIPT_ASSERT("#1709797 - entity doesn't exist (FE_M_SON_smoking_weed_in_a_bong)")
								RETURN FALSE
							ENDIF
							
							vSmoke_Pos = <<0.0, 0.0, 0.05>>
							vSmoke_Rot = <<0,0,0>>
							eventPtfx = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxName, entityIndex,
									vSmoke_Pos, vSmoke_Rot, scale)
							#IF IS_DEBUG_BUILD
							PRINTSTRING("starting script ptfx looped \"")
							PRINTSTRING(fxName)
							PRINTSTRING("\" on ")
							PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
							PRINTSTRING(" for ")
							PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
							PRINTNL()
							#ENDIF
						ENDIF
					ELSE
						vSmoke_Pos = <<-806.8,174.0,76.9>>
						vSmoke_Rot = <<0,0,0>>
						
						eventPtfx = START_PARTICLE_FX_LOOPED_AT_COORD(fxName,
								vSmoke_Pos, vSmoke_Rot,
								scale)
					
					
//						IF NOT bLooped
//							SCRIPT_ASSERT("FE_M_SON_smoking_weed_in_a_bong not looped?")
//						ENDIF
						
						#IF IS_DEBUG_BUILD
						PRINTSTRING("starting script ptfx looped \"")
						PRINTSTRING(fxName)
						PRINTSTRING("\" on ")
						PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
						PRINTSTRING(" for ")
						PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
						PRINTNL()
						#ENDIF
					ENDIF
					
				ENDIF
				
				RETURN FALSE
			ELSE
				//
				IF DOES_PARTICLE_FX_LOOPED_EXIST(eventPtfx)
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str
					str  = ("ptfx \"")
					str += (fxName)
					str += ("\" looped[")
					str += (NATIVE_TO_INT(eventPtfx))
					str += ("]")
					
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityIndex, FALSE), fDebugOffsetY, HUD_COLOUR_ORANGELIGHT)
					
					IF NOT IS_ENTITY_A_PED(entityIndex)
						DrawDebugFamilyTextWithOffset("a", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.025>>), 0, HUD_COLOUR_ORANGELIGHT)
						DrawDebugFamilyTextWithOffset("b", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.050>>), 0, HUD_COLOUR_ORANGELIGHT)
						DrawDebugFamilyTextWithOffset("c", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.075>>), 0, HUD_COLOUR_ORANGELIGHT)
						DrawDebugFamilyTextWithOffset("d", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.100>>), 0, HUD_COLOUR_ORANGELIGHT)
					ENDIF
					
					#ENDIF
					
				ELSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str
					str  = ("ptfx \"")
					str += (fxName)
					str += ("\" non-looped[")
					str += (NATIVE_TO_INT(eventPtfx))
					str += ("]")
					
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityIndex, FALSE), fDebugOffsetY, HUD_COLOUR_ORANGEDARK)
					DrawDebugFamilyLine(<<-806.8,174.0,76.9>>, GET_ENTITY_COORDS(entityIndex, FALSE), HUD_COLOUR_ORANGEDARK)
					#ENDIF
					
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
//		CASE FE_M_DAUGHTER_purges_in_the_bathroom
//			IF (eventPtfx = NULL)
//				
//				REQUEST_PTFX_ASSET()
//				IF NOT HAS_PTFX_ASSET_LOADED()
//					
//					PRINTSTRING("REQUESTING SCRIPT PTFX ")
//					PRINTSTRING(fxName)
//					PRINTSTRING("...")
//					PRINTNL()
//					
//					REQUEST_PTFX_ASSET()
//				ELSE
//				
//					VECTOR vPuke_Pos, vPuke_Rot
//					vPuke_Pos = <<0,0,0>>
//					vPuke_Rot = <<0,0,0>>
//					
//					FLOAT scale
//					scale		= 1.0
//					
//					IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
//							vPuke_Pos, vPuke_Rot,
//							BONETAG_HEAD, scale)
//						eventPtfx = 0                     //INT_TO_NATIVE(PTFX_ID, GET_RANDOM_INT_IN_RANGE(1, 500000))
//						
//						IF bLooped
//							SCRIPT_ASSERT("FE_M_DAUGHTER_purges_in_the_bathroom looped?")
//						ENDIF
//						
//						#IF IS_DEBUG_BUILD
//						PRINTSTRING("starting script ptfx non-looped \"")
//						PRINTSTRING(fxName)
//						PRINTSTRING("\" on ")
//						PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//						PRINTSTRING(" for ")
//						PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//						PRINTNL()
//						#ENDIF
//						
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ELSE
//				//
//			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_M_GARDENER_watering_flowers
			IF (eventPtfx = NULL)
				
				REQUEST_PTFX_ASSET()
				IF NOT HAS_PTFX_ASSET_LOADED()
					
					PRINTSTRING("REQUESTING SCRIPT PTFX ")
					PRINTSTRING(fxName)
					PRINTSTRING("...")
					PRINTNL()
						
					REQUEST_PTFX_ASSET()
				ELSE
					VECTOR vPuke_Pos, vPuke_Rot
					vPuke_Pos = <<0,0,0>>
					vPuke_Rot = <<0,0,0>>
					
					CONST_FLOAT scale		1.0
					
					eventPtfx = START_PARTICLE_FX_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
							vPuke_Pos, vPuke_Rot,
							BONETAG_PH_L_HAND, scale)
					
					
					#IF IS_DEBUG_BUILD
					PRINTSTRING("starting script ptfx looped \"")
					PRINTSTRING(fxName)
					PRINTSTRING("\" on ")
					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
					PRINTSTRING(" for ")
					PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
					PRINTNL()
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ELSE
				//
				#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_W)
					eventPtfx = NULL
				ENDIF
				#ENDIF
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			IF (eventPtfx = NULL)
				
				REQUEST_PTFX_ASSET()
				IF NOT HAS_PTFX_ASSET_LOADED()
					
					PRINTSTRING("REQUESTING SCRIPT PTFX ")
					PRINTSTRING(fxName)
					PRINTSTRING("...")
					PRINTNL()
					
					REQUEST_PTFX_ASSET()
				ELSE
					IF NOT DOES_ENTITY_EXIST(entityIndex)
						PRINTLN("#1709797 - entity doesn't exist (FE_T0_RONEX_working_a_moonshine_sill)")
						RETURN FALSE
					ENDIF
					
					VECTOR vPuke_Pos, vPuke_Rot
					vPuke_Pos = <<0,0,0>>
					vPuke_Rot = <<0,0,0>>
					
					eventPtfx = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxName, entityIndex,
							vPuke_Pos, vPuke_Rot)
						
					
					#IF IS_DEBUG_BUILD
					PRINTSTRING("starting script ptfx non-looped \"")
					PRINTSTRING(fxName)
					PRINTSTRING("\" on ")
					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
					PRINTSTRING(" for ")
					PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
					PRINTNL()
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ELSE
				//
			ENDIF
			
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("invalid eFamilyEvent ")
	PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
	PRINTSTRING(" in PRIVATE_Update_Family_Ptfx()")
	PRINTNL()
	
	SCRIPT_ASSERT("invalid eFamilyEvent PRIVATE_Update_Family_Ptfx()")
	#ENDIF
	
	fDebugOffsetY = fDebugOffsetY
	eventPtfx = NULL
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Update_Family_Ptfx_Nonlooped(ENTITY_INDEX entityIndex, enumFamilyEvents eFamilyEvent,
		STRING fxName, BOOL &bEventPtfx, FLOAT fDebugOffsetY)
	
	SWITCH eFamilyEvent
		CASE FE_M_SON_smoking_weed_in_a_bong
			IF (NOT bEventPtfx)
				
				REQUEST_PTFX_ASSET()
				IF NOT HAS_PTFX_ASSET_LOADED()
					
					PRINTSTRING("REQUESTING SCRIPT PTFX ")
					PRINTSTRING(fxName)
					PRINTSTRING("...")
					PRINTNL()
					
					REQUEST_PTFX_ASSET()
				ELSE
				
					VECTOR vSmoke_Pos, vSmoke_Rot
					
					CONST_FLOAT scale		1.0
					
					IF NOT ARE_STRINGS_EQUAL(fxName, "scr_fam_door_smoke")
						
						
						IF IS_ENTITY_A_PED(entityIndex)
							vSmoke_Pos = <<0,0,0>>
							vSmoke_Rot = <<0,0,0>>
							IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
									vSmoke_Pos, vSmoke_Rot, BONETAG_HEAD, scale)
								bEventPtfx = TRUE
								
								#IF IS_DEBUG_BUILD
								PRINTSTRING("starting script ptfx non-looped \"")
								PRINTSTRING(fxName)
								PRINTSTRING("\" on ")
								PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
								PRINTSTRING(" for ")
								PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
								PRINTNL()
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								PRINTSTRING("failing at starting a script ptfx non-looped \"")
								PRINTSTRING(fxName)
								PRINTSTRING("\" on ")
								PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
								PRINTSTRING(" for ")
								PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
								PRINTNL()
								#ENDIF
							ENDIF
						ELSE
//							vSmoke_Pos = <<0.0, 0.0, 0.05>>
//							vSmoke_Rot = <<0,0,0>>
//							eventPtfx = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxName, entityIndex,
//									vSmoke_Pos, vSmoke_Rot, scale)
//							#IF IS_DEBUG_BUILD
//							PRINTSTRING("starting script ptfx looped \"")
//							PRINTSTRING(fxName)
//							PRINTSTRING("\" on ")
//							PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//							PRINTSTRING(" for ")
//							PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//							PRINTNL()
//							#ENDIF
						ENDIF
					ELSE
//						vSmoke_Pos = <<-806.8,174.0,76.9>>
//						vSmoke_Rot = <<0,0,0>>
//						
//						eventPtfx = START_PARTICLE_FX_LOOPED_AT_COORD(fxName,
//								vSmoke_Pos, vSmoke_Rot,
//								scale)
//					
//					
//						IF NOT bLooped
//							SCRIPT_ASSERT("FE_M_SON_smoking_weed_in_a_bong not looped?")
//						ENDIF
//						
//						#IF IS_DEBUG_BUILD
//						PRINTSTRING("starting script ptfx looped \"")
//						PRINTSTRING(fxName)
//						PRINTSTRING("\" on ")
//						PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//						PRINTSTRING(" for ")
//						PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//						PRINTNL()
//						#ENDIF
					ENDIF
					
				ENDIF
				
				RETURN FALSE
			ELSE
				//
//				IF bLooped
//				AND DOES_PARTICLE_FX_LOOPED_EXIST(eventPtfx)
//					
//					#IF IS_DEBUG_BUILD
//					TEXT_LABEL_63 str
//					str  = ("ptfx \"")
//					str += (fxName)
//					str += ("\" looped[")
//					str += (NATIVE_TO_INT(eventPtfx))
//					str += ("]")
//					
//					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityIndex, FALSE), fDebugOffsetY, HUD_COLOUR_ORANGELIGHT)
//					
//					IF NOT IS_ENTITY_A_PED(entityIndex)
//						DrawDebugFamilyTextWithOffset("a", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.025>>), 0, HUD_COLOUR_ORANGELIGHT)
//						DrawDebugFamilyTextWithOffset("b", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.050>>), 0, HUD_COLOUR_ORANGELIGHT)
//						DrawDebugFamilyTextWithOffset("c", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.075>>), 0, HUD_COLOUR_ORANGELIGHT)
//						DrawDebugFamilyTextWithOffset("d", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityIndex, <<0.0, 0.0, 0.100>>), 0, HUD_COLOUR_ORANGELIGHT)
//					ENDIF
//					
//					#ENDIF
//					
//				ELSE
//					
//					#IF IS_DEBUG_BUILD
//					TEXT_LABEL_63 str
//					str  = ("ptfx \"")
//					str += (fxName)
//					str += ("\" non-looped[")
//					str += (NATIVE_TO_INT(eventPtfx))
//					str += ("]")
//					
//					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(entityIndex, FALSE), fDebugOffsetY, HUD_COLOUR_ORANGEDARK)
//					DrawDebugFamilyLine(<<-806.8,174.0,76.9>>, GET_ENTITY_COORDS(entityIndex, FALSE), HUD_COLOUR_ORANGEDARK)
//					#ENDIF
//					
//				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom
			IF (NOT bEventPtfx)
				
				REQUEST_PTFX_ASSET()
				IF NOT HAS_PTFX_ASSET_LOADED()
					
					PRINTSTRING("REQUESTING SCRIPT PTFX ")
					PRINTSTRING(fxName)
					PRINTSTRING("...")
					PRINTNL()
					
					REQUEST_PTFX_ASSET()
				ELSE
				
					VECTOR vPuke_Pos, vPuke_Rot
					vPuke_Pos = <<0,0,0>>
					vPuke_Rot = <<0,0,0>>
					
					FLOAT scale
					scale		= 1.0
					
					IF START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
							vPuke_Pos, vPuke_Rot,
							BONETAG_HEAD, scale)
						bEventPtfx = TRUE
						
//						IF bLooped
//							SCRIPT_ASSERT("FE_M_DAUGHTER_purges_in_the_bathroom looped?")
//						ENDIF
						
						#IF IS_DEBUG_BUILD
						PRINTSTRING("starting script ptfx non-looped \"")
						PRINTSTRING(fxName)
						PRINTSTRING("\" on ")
						PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
						PRINTSTRING(" for ")
						PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
						PRINTNL()
						#ENDIF
						
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				//
			ENDIF
			
			RETURN FALSE
		BREAK
//		CASE FE_M_GARDENER_watering_flowers
//			IF (eventPtfx = NULL)
//				
//				REQUEST_PTFX_ASSET()
//				IF NOT HAS_PTFX_ASSET_LOADED()
//					
//					PRINTSTRING("REQUESTING SCRIPT PTFX ")
//					PRINTSTRING(fxName)
//					PRINTSTRING("...")
//					PRINTNL()
//						
//					REQUEST_PTFX_ASSET()
//				ELSE
//					VECTOR vPuke_Pos, vPuke_Rot
//					vPuke_Pos = <<0,0,0>>
//					vPuke_Rot = <<0,0,0>>
//					
//					CONST_FLOAT scale		1.0
//					
//					eventPtfx = START_PARTICLE_FX_LOOPED_ON_PED_BONE(fxName, GET_PED_INDEX_FROM_ENTITY_INDEX(entityIndex),
//							vPuke_Pos, vPuke_Rot,
//							BONETAG_PH_L_HAND, scale)
//					
//					IF NOT bLooped
//						SCRIPT_ASSERT("FE_M_GARDENER_watering_flowers not looped?")
//					ENDIF
//					
//					#IF IS_DEBUG_BUILD
//					PRINTSTRING("starting script ptfx looped \"")
//					PRINTSTRING(fxName)
//					PRINTSTRING("\" on ")
//					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//					PRINTSTRING(" for ")
//					PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//					PRINTNL()
//					#ENDIF
//					
//					RETURN TRUE
//				ENDIF
//			ELSE
//				//
//				#IF IS_DEBUG_BUILD
//				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_W)
//					eventPtfx = NULL
//				ENDIF
//				#ENDIF
//			ENDIF
//			
//			RETURN FALSE
//		BREAK
//		CASE FE_T0_RONEX_working_a_moonshine_sill
//			IF (eventPtfx = NULL)
//				
//				REQUEST_PTFX_ASSET()
//				IF NOT HAS_PTFX_ASSET_LOADED()
//					
//					PRINTSTRING("REQUESTING SCRIPT PTFX ")
//					PRINTSTRING(fxName)
//					PRINTSTRING("...")
//					PRINTNL()
//					
//					REQUEST_PTFX_ASSET()
//				ELSE
//				
//					VECTOR vPuke_Pos, vPuke_Rot
//					vPuke_Pos = <<0,0,0>>
//					vPuke_Rot = <<0,0,0>>
//					
//					eventPtfx = START_PARTICLE_FX_LOOPED_ON_ENTITY(fxName, entityIndex,
//							vPuke_Pos, vPuke_Rot)
//							
//					IF NOT bLooped
//						SCRIPT_ASSERT("FE_T0_RONEX_working_a_moonshine_sill not looped?")
//					ENDIF
//					
//					#IF IS_DEBUG_BUILD
//					PRINTSTRING("starting script ptfx non-looped \"")
//					PRINTSTRING(fxName)
//					PRINTSTRING("\" on ")
//					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entityIndex)))
//					PRINTSTRING(" for ")
//					PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
//					PRINTNL()
//					#ENDIF
//					
//					RETURN TRUE
//				ENDIF
//			ELSE
//				//
//			ENDIF
//			
//			RETURN FALSE
//		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("invalid eFamilyEvent ")
	PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
	PRINTSTRING(" in PRIVATE_Update_Family_Ptfx()")
	PRINTNL()
	
	SCRIPT_ASSERT("invalid eFamilyEvent PRIVATE_Update_Family_Ptfx()")
	#ENDIF
	
	fDebugOffsetY = fDebugOffsetY
//	eventPtfx = NULL
	RETURN FALSE
ENDFUNC


FUNC BOOL ar_FIND_ANIM_EVENT_PHASE(STRING pAnimDictName, STRING pAnimName, STRING pEventName, FLOAT& ReturnStartPhase, FLOAT& ReturnEndPhase)
	IF FIND_ANIM_EVENT_PHASE(pAnimDictName, pAnimName, pEventName, ReturnStartPhase, ReturnEndPhase)
		RETURN TRUE
	ENDIF
	
	IF ARE_STRINGS_EQUAL(pEventName, "exhale")
	
		/* not needed - tags in anims
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@JIMMY@IG_1@IDLE_A")
		AND ARE_STRINGS_EQUAL(pAnimName, "That_Was_The_Whole")
			ReturnStartPhase = 0.376
			ReturnEndPhase = 0.500
			
			#IF IS_DEBUG_BUILD
			SAVE_STRING_TO_DEBUG_FILE("pAnimName: ")
			SAVE_STRING_TO_DEBUG_FILE(pAnimName)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@JIMMY@IG_1@IDLE_A")
		AND ARE_STRINGS_EQUAL(pAnimName, "This_Is_Some_Serious")
			ReturnStartPhase = 0.298
			ReturnEndPhase = 0.357
			
			#IF IS_DEBUG_BUILD
			SAVE_STRING_TO_DEBUG_FILE("pAnimName: ")
			SAVE_STRING_TO_DEBUG_FILE(pAnimName)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		*/
		
		
		
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@TRACY@IG_7@BASE")
		AND ARE_STRINGS_EQUAL(pAnimName, "BASE")
			ReturnStartPhase = -1.0
			ReturnEndPhase = -1.0
			
			#IF IS_DEBUG_BUILD
			printSTRING("pAnimName: ")
			printSTRING(pAnimName)
			printSTRING(", ")
			#ENDIF
			
			RETURN FALSE	//TRUE - no vomitting in base
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@TRACY@IG_7@IDLE_A")
		AND ARE_STRINGS_EQUAL(pAnimName, "IDLE_A")
			ReturnStartPhase = 0.058
			ReturnEndPhase = 0.195
			
			#IF IS_DEBUG_BUILD
			printSTRING("pAnimName: ")
			printSTRING(pAnimName)
			printSTRING(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@TRACY@IG_7@IDLE_A")
		AND ARE_STRINGS_EQUAL(pAnimName, "IDLE_B")
			ReturnStartPhase = 0.298
			ReturnEndPhase = 0.357
			
			#IF IS_DEBUG_BUILD
			printSTRING("pAnimName: ")
			printSTRING(pAnimName)
			printSTRING(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@TRACY@IG_7@IDLE_A")
		AND ARE_STRINGS_EQUAL(pAnimName, "IDLE_C")
			ReturnStartPhase = 0.298
			ReturnEndPhase = 0.357
			
			#IF IS_DEBUG_BUILD
			printSTRING("pAnimName: ")
			printSTRING(pAnimName)
			printSTRING(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
		IF ARE_STRINGS_EQUAL(pAnimDictName, "TIMETABLE@TRACY@IG_7@IDLE_B")
		AND ARE_STRINGS_EQUAL(pAnimName, "IDLE_D")
			ReturnStartPhase = 0.298
			ReturnEndPhase = 0.357
			
			#IF IS_DEBUG_BUILD
			printSTRING("pAnimName: ")
			printSTRING(pAnimName)
			printSTRING(", ")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_AnimPtfx(PED_INDEX PedIndex, enumFamilyEvents eFamilyEvent,
			STRING fxName, PTFX_ID &eventPtfx, BOOL &bEventPtfx,
			TEXT_LABEL_63 tAnimDict, TEXT_LABEL_63 tAnimClip, INT &iScene,
			STRING pEventName, FLOAT fDebugOffsetY, BOOL bLooped, ENTITY_INDEX entityIndex = NULL)
	
	IF (entityIndex = NULL)
		entityIndex = PedIndex
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(entityIndex, FALSE), GET_ENTITY_COORDS(player_ped_Id())) > (10*10)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		str  = ("particle effect vdist is too great!")
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), fDebugOffsetY, HUD_COLOUR_BLUEDARK)
		#ENDIF
						
		RETURN TRUE
	ENDIF
	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tAnimDict)
		
		FLOAT fAnimClipCurrentTime = -1
		
		IF IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip, ANIM_SCRIPT)
			fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tAnimDict, tAnimClip)
		ELIF IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip, ANIM_SYNCED_SCENE)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				fAnimClipCurrentTime = GET_SYNCHRONIZED_SCENE_PHASE(iScene)
			ENDIF
		ENDIF
		
		IF fAnimClipCurrentTime >= 0
			FLOAT ReturnStartPhase, ReturnEndPhase
			IF ar_FIND_ANIM_EVENT_PHASE(tAnimDict, tAnimClip, pEventName, ReturnStartPhase, ReturnEndPhase)
				
				IF fAnimClipCurrentTime >= ReturnStartPhase
				AND fAnimClipCurrentTime <= ReturnEndPhase
				
					IF bLooped
						IF PRIVATE_Update_Family_Ptfx_Looped(entityIndex, eFamilyEvent,
								fxName, eventPtfx, fDebugOffsetY)
							
							PRINTSTRING("PRIVATE_Update_Family_Ptfx: true")
							PRINTNL()
							
							RETURN TRUE
						ELSE
							
							PRINTSTRING("PRIVATE_Update_Family_Ptfx: false")
							PRINTNL()
							
							RETURN FALSE
						ENDIF
					ELSE
						IF PRIVATE_Update_Family_Ptfx_Nonlooped(entityIndex, eFamilyEvent,
								fxName, bEventPtfx, fDebugOffsetY)
							
							PRINTSTRING("PRIVATE_Update_Family_Ptfx: true")
							PRINTNL()
							
							RETURN TRUE
						ELSE
							
							PRINTSTRING("PRIVATE_Update_Family_Ptfx: false")
							PRINTNL()
							
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					
				
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str
					str  = ("pEventName \"")
					str += (pEventName)
					str += ("\" ")
					
					IF fAnimClipCurrentTime < ReturnStartPhase
						str += ("waiting for ")
						str += GET_STRING_FROM_FLOAT(fAnimClipCurrentTime)
						str += (" < ")
						str += GET_STRING_FROM_FLOAT(ReturnStartPhase, 2)
						
					ELIF fAnimClipCurrentTime > ReturnEndPhase
						str += ("gone past ")
						str += GET_STRING_FROM_FLOAT(fAnimClipCurrentTime)
						str += (" > ")
						str += GET_STRING_FROM_FLOAT(ReturnEndPhase, 2)
						
					ELSE
						str += ("???")
					ENDIF
					
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), fDebugOffsetY, HUD_COLOUR_ORANGEDARK)
					#ENDIF
					
					IF bLooped
					AND DOES_PARTICLE_FX_LOOPED_EXIST(eventPtfx)
						STOP_PARTICLE_FX_LOOPED(eventPtfx)
					ENDIF
				ENDIF
				
			ELSE
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str
				str  = ("pEventName \"")
				str += (pEventName)
				str += ("\" not in \"")
				str += (tAnimClip)
				str += ("\"")
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), fDebugOffsetY, HUD_COLOUR_BLUEDARK)
				#ENDIF
				
			ENDIF
		ELSE
			
			PRINTSTRING("NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip)")PRINTNL()
			
		ENDIF
	ELSE
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str
		str  = ("tAnimDict \"")
		str += (tAnimDict)
		str += ("\" is null... not playing anim")
		
		DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), fDebugOffsetY, HUD_COLOUR_RED)
		#ENDIF
		
	ENDIF
	
	bEventPtfx = FALSE
	eventPtfx = NULL
	RETURN FALSE
ENDFUNC
