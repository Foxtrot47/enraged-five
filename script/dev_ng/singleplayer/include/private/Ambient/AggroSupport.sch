
//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	Aggro Checks												//
//		AUTHOR			:	Brenda Carey												//
//		DESCRIPTION		:	Ambient used aggro checks							 		//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
///    
///    
///    
///    
///    

USING "globals.sch"
//USING "rage_builtins.sch"
//USING "commands_player.sch"
//USING "commands_brains.sch"
//USING "commands_script.sch"
//USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
//USING "brains.sch"
USING "script_player.sch"
//USING "commands_pad.sch"
//USING "commands_object.sch"
//USING "commands_path.sch"
USING "Commands_audio.sch"
USING "shared_debug_text.sch"


ENUM EAggro
	EAggro_Danger = 0,
	EAggro_ShotNear = 1,
	EAggro_HostileOrEnemy = 2,
	EAggro_Attacked = 3,
	EAggro_HeardShot = 4,
	EAggro_Shoved = 5
ENDENUM



PROC PLAY_TAUNT(PED_INDEX myPed)
//	SAY_SINGLE_LINE_CONTEXT(myPed,SPEECH_Major_Shock, gPlayer, TRUE, FALSE, AUD_AMBIENT_SPEECH_VOLUME_SHOUTED)
	PLAY_PED_AMBIENT_SPEECH(myPed, "")
		
ENDPROC

PROC SET_PED_AGGROED(PED_INDEX myPed)
	STOP_CURRENT_PLAYING_AMBIENT_SPEECH(myPed)

//	AI_SPEECH_SET_ALLOW_FOR_ACTOR(MyActor, TRUE)
//	DISABLE_NONCOMBAT_SPEECH_INDIVIDUAL(MyActor, FALSE)
	//PLAY_TAUNT(myPed)
	
	//Might need a way to make the ped neutral to the player
//	SET_PED_RELATIONSHIP(myPed, ACQUAINTANCE_TYPE_PED_IGNORE ,RELGROUP_PLAYER)
	//TASK_CLEAR_LOOK_AT(myPed)

ENDPROC


FUNC BOOL HAS_PLAYER_DAMAGED_PED(PED_INDEX myPed, INT CarDamageSensitivity = 0)
	
	IF IS_PED_ON_FOOT(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myPed, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ELSE
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myPed, PLAYER_PED_ID())
			IF GET_PED_MAX_HEALTH(myPed) - GET_ENTITY_HEALTH(myPed) > CarDamageSensitivity
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF


	RETURN FALSE
ENDFUNC


INT iTimeLastNotLockedOn
/// PURPOSE:
///    Checks if the player has aggroed the ped
/// PARAMS:
///    myPed - The ped you want to check the player has aggroed
///    aggroReason - The reason returned which aggro condition has been flagged
///    lockOnTimer - The time the player has locked on aim to the ped
///    iBitFieldDontCheck - The dont check bit int
///    bHasAggroed - the check if the ped is already aggroed
///    projectileHearRange - The projectile hearing range
/// RETURNS:
///    returns a bool if the ped has just been aggroed
FUNC BOOL HAS_PLAYER_AGGROED_PED(PED_INDEX myPed, EAggro& aggroReason, INT& lockOnTimer, INT iBitFieldDontCheck = 0, BOOL bHasAggroed = FALSE, FLOAT projectileHearRange = 3.0, INT CarDamageSensitivity = 0)
	FLOAT lockonDistance
	FLOAT LocalProjectileHearRange
	BOOL isLockedOn = FALSE
	
	IF NOT IS_ENTITY_DEAD(myPed) AND NOT bHasAggroed
		IF IS_PED_ON_FOOT(myPed) 
			LocalProjectileHearRange = projectileHearRange
		ELSE
			LocalProjectileHearRange = projectileHearRange+3.0
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(myPed) AND NOT bHasAggroed
			VECTOR PlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR PedCoords = GET_ENTITY_COORDS(myPed)
			FLOAT Distance = VDIST(PlayerCoords, PedCoords)
			
			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Attacked))
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(myPed, PLAYER_PED_ID())
				IF HAS_PLAYER_DAMAGED_PED(myPed, CarDamageSensitivity)
					printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
					printDebugString("	aggroReason = EAggro_Attacked\n")
					aggroReason = EAggro_Attacked
					SET_PED_AGGROED(myPed)
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_ShotNear))
				IF LocalProjectileHearRange < 20
				ENDIF
				IF IS_BULLET_IN_AREA(PedCoords, projectileHearRange, TRUE)
					printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
					printDebugString("	aggroReason = EAggro_ShotNear\n")
					aggroReason = EAggro_ShotNear
					SET_PED_AGGROED(myPed)
					RETURN TRUE
				ENDIF
				IF IS_PROJECTILE_IN_AREA((PedCoords-<<projectileHearRange/2, projectileHearRange/2, projectileHearRange/2>>), (PedCoords+<<projectileHearRange/2, projectileHearRange/2, projectileHearRange/2>>))
					printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
					printDebugString("	aggroReason = EAggro_ShotNear\n")
					aggroReason = EAggro_ShotNear
					SET_PED_AGGROED(myPed)
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_HostileOrEnemy))
				lockonDistance = GET_LOCKON_DISTANCE_OF_CURRENT_PED_WEAPON(PLAYER_PED_ID())
				IF lockonDistance < 0
					lockonDistance = 25
				ELIF lockonDistance > 25
					lockonDistance = 25
				ENDIF
				
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), myPed)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), myPed) 
						IF (Distance < lockonDistance) 
						
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(myPed,PLAYER_PED_ID())
	//						IF HAS_PED_CLEAR_LOS_TO_ENTITY_IN_FRONT(myPed,PLAYER_PED_ID()) //And is the ped aware of ped
								printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
								printDebugString("	aggro Ped knows player is pointing gun\n")
								printDebugStringAndInt("		lockOnTimer = ", lockOnTimer)
								printDebugString("\n")
								printDebugStringAndInt("		time since not LockedOn = ", (GET_GAME_TIMER()-iTimeLastNotLockedOn))
								printDebugString("\n")
								isLockedOn = TRUE
								
								IF GET_GAME_TIMER() > (iTimeLastNotLockedOn + lockOnTimer)
									printDebugString("			aggroReason = EAggro_HostileOrEnemy\n")
									aggroReason = EAggro_HostileOrEnemy
									SET_PED_AGGROED(myPed)
									RETURN TRUE
								ENDIF
		
							ENDIF
						ELSE
							isLockedOn = FALSE
						ENDIF
					ELSE
						isLockedOn = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Danger))
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
					printDebugString("			aggroReason = EAggro_Danger\n")
					aggroReason = EAggro_Danger
					SET_PED_AGGROED(myPed)
					RETURN TRUE
				ENDIF
			ENDIF
			
//			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_HeardShot))
//				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), myPed, <<18,18,18>>)
//					IF IS_PED_SHOOTING(PLAYER_PED_ID()) // Will need to account for silenced weapons
//						printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
//						printDebugString("			aggroReason = EAggro_HeardShot\n")
//						aggroReason = EAggro_HeardShot
//						SET_PED_AGGROED(myPed)
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
			
//			IF NOT IS_BIT_SET(iBitFieldDontCheck, ENUM_TO_INT(EAggro_Shoved))
//				IF IS_PED_RAGDOLL(myPed)
//					IF IS_ENTITY_AT_ENTITY(myPed, PLAYER_PED_ID(), <<1.0, 1.0, 1.0>>)
//					//IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), myPed)
//						printDebugString("FUNC BOOL HAS_PLAYER_AGGROED_PED\n")
//						printDebugString("			aggroReason = EAggro_Shoved\n")
//						aggroReason = EAggro_Shoved
//						SET_PED_AGGROED(myPed)
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
		ENDIF
	ENDIF
	
	IF NOT isLockedOn
		iTimeLastNotLockedOn = GET_GAME_TIMER()
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_AGGROED_PEDS(PED_INDEX& MyPeds[], INT& agrroedIDX, EAggro& aggroReason, INT& lockOnTimer, INT iBitFieldDontCheck = 0, BOOL bHasAggroed = FALSE, FLOAT projectileHearRange = 2.5, INT CarDamageSensitivity = 0)
	INT idx
	projectileHearRange = projectileHearRange
	IF NOT bHasAggroed
		REPEAT COUNT_OF(MyPeds) idx
			IF HAS_PLAYER_AGGROED_PED(MyPeds[idx], aggroReason, lockOnTimer, iBitFieldDontCheck, bHasAggroed, projectileHearRange, CarDamageSensitivity)
				agrroedIDX = idx
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC






