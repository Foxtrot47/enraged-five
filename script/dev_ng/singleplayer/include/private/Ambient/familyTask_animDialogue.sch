///private header for family task control scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "commands_misc.sch"
USING "commands_task.sch"

// *******************************************************************************************
//	FAMILY DIALOGUE ANIM TASKS PRIVATE FUNCTIONS
// *******************************************************************************************

FUNC INT PRIVATE_GetIntFromConv(enumFamilyEvents eFamilyEvent, TEXT_LABEL &rootLabel)

	rootLabel = g_ConversationData.ConversationSegmentToGrab
	
	INT iRandCount, iSpeechBit, iPlayerBitset
	Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
	IF IS_BITMASK_AS_ENUM_SET(iSpeechBit, FSC_singleRandomLine)
		rootLabel += "A"
		RETURN GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(rootLabel)
	ELSE
		
		INT rootLabelLength = GET_LENGTH_OF_LITERAL_STRING(rootLabel)
		IF rootLabelLength <= 0
			RETURN -1
		ENDIF
		
		TEXT_LABEL rootLabelChar = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(rootLabel, rootLabelLength-1, rootLabelLength)
		rootLabel += "_1A"
		
		SWITCH GET_HASH_KEY(rootLabelChar)
			CASE HASH("a")
				RETURN 1
			BREAK
			CASE HASH("b")
				RETURN 2
			BREAK
			CASE HASH("c")
				RETURN 3
			BREAK
			CASE HASH("d")
				RETURN 4
			BREAK
			
		ENDSWITCH
		
		RETURN GET_VARIATION_CHOSEN_FOR_SCRIPTED_LINE(rootLabel)
	ENDIF
ENDFUNC

FUNC BOOL PRIVATE_GetTextLabelFromConv(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent, TEXT_LABEL_63 &tAnimDialogueDict, TEXT_LABEL_63 &tAnimDialogueClip, INT iRootFilenameNum = -1)

	tAnimDialogueDict = "null"
	tAnimDialogueClip = "null"
	
	TEXT_LABEL rootLabel = ""	//g_ConversationData.ConversationSegmentToGrab
	
	IF (iRootFilenameNum = -1)
		iRootFilenameNum = PRIVATE_GetIntFromConv(eFamilyEvent, rootLabel)
		
		IF (iRootFilenameNum = -1)
			INT iRandCount, iSpeechBit, iPlayerBitset
			STRING sExpectedRootLabel = Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
			
			/*
			unrecognised root filename num: M7_SON_jumping_jacks
				[rootLabel: FMM_HOME3A != FAMR_IG_2a, line: -1]
			*/
			
			IF NOT ARE_STRINGS_EQUAL(rootLabel, sExpectedRootLabel)
				#IF IS_DEBUG_BUILD
				CWARNINGLN(DEBUG_FAMILY, "negative root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), " [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, "]")
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iRandCount, iSpeechBit, iPlayerBitset
	STRING sExpectedRootLabel = Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
	INT iRootLength = GET_LENGTH_OF_LITERAL_STRING(rootLabel)
	INT iExpectedRootLength = GET_LENGTH_OF_LITERAL_STRING(sExpectedRootLabel)
	TEXT_LABEL sDiddyRootLabel = ""
	IF iExpectedRootLength <= iRootLength
		IF iExpectedRootLength > 0
			sDiddyRootLabel = GET_STRING_FROM_STRING(rootLabel, 0, iExpectedRootLength)
		ENDIF
		IF ARE_STRINGS_EQUAL(sDiddyRootLabel, sExpectedRootLabel)
			//
			
			CWARNINGLN(DEBUG_FAMILY, "<famConvTest> ", Get_String_From_FamilyEvent(eFamilyEvent),
					" [rootLabel: ", rootLabel, ", ", iExpectedRootLength,
					" <= sExpectedRootLabel: ", sExpectedRootLabel, ", ", iExpectedRootLength,
					"] == sDiddyRootLabel: ", sDiddyRootLabel)
		ELSE
			//
			CWARNINGLN(DEBUG_FAMILY, "<famConvTest> ", Get_String_From_FamilyEvent(eFamilyEvent),
					" [rootLabel: ", rootLabel, ", ", iExpectedRootLength,
					" <= sExpectedRootLabel: ", sExpectedRootLabel, ", ", iExpectedRootLength,
					"] != sDiddyRootLabel: ", sDiddyRootLabel)
		ENDIF
	ELSE
		//
		sDiddyRootLabel = ""
		CWARNINGLN(DEBUG_FAMILY, "<famConvTest> ", Get_String_From_FamilyEvent(eFamilyEvent),
				" [rootLabel: ", rootLabel, ", ", iExpectedRootLength,
				" > sExpectedRootLabel: ", sExpectedRootLabel, ", ", iExpectedRootLength,
				"]")
	ENDIF
	#ENDIF
	
	SWITCH eFamilyEvent
		CASE FE_T0_RON_monitoring_police_frequency
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@RON@HAND_RADIO_IG_1"
							tAnimDialogueClip = "IG_1_IDLE_A_FascistPricks"
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@RON@HAND_RADIO_IG_1"
							tAnimDialogueClip = "IG_1_IDLE_B_RoutineProcedureMyAss"
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@RON@HAND_RADIO_IG_1"
							tAnimDialogueClip = "IG_1_IDLE_C_ThoseFederalChumps"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T0_RON_monitoring_police_frequency [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING
		CASE FE_T0_RON_ranting_about_government_SITTING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@RON@IG_3_COUCH"
							tAnimDialogueClip = "TrevorDidYouHearAbout"
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@RON@IG_3_COUCH"
							tAnimDialogueClip = "TrevorTrevorYouWouldnt"
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@RON@IG_3_COUCH"
							tAnimDialogueClip = "TrevorYouAreReallyGonnaWant"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T0_RON_ranting_about_government [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@RON@IG_4_SMOKING_METH"
							tAnimDialogueClip = "ChefIsCookingUp"		//0.2
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@RON@IG_4_SMOKING_METH"
							tAnimDialogueClip = "IfYouWantToBorrow"		//0.6
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@RON@IG_4_SMOKING_METH"
							tAnimDialogueClip = "OneMoreHit"			//0.4
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T0_RON_smoking_crystal [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "TrevOnLav_Midwife"		//('I need a fuckin midwife for this thing)
							tAnimDialogueDict = "TIMETABLE@TREVOR@ON_THE_TOILET"
							tAnimDialogueClip = "TrevOnLav_Midwife"
						BREAK
						CASE 2
							// sShit_anim = "TrevOnLav_ComeHere"	//('Come Here!!  I wanna talk to you')
							tAnimDialogueDict = "TIMETABLE@TREVOR@ON_THE_TOILET"
							tAnimDialogueClip = "TrevOnLav_ComeHere"
						BREAK
						CASE 3
							// sShit_anim = "TrevOnLav_BackedUp"	//('ARrgghh,  I'm backed up,  gonna need a C-Section')
							tAnimDialogueDict = "TIMETABLE@TREVOR@ON_THE_TOILET"
							tAnimDialogueClip = "TrevOnLav_BackedUp"
						BREAK
						CASE 4
							// "Ach! No, It's good."
							RETURN FALSE
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T0_TREVOR_doing_a_shit [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
//		CASE FE_M2_SON_gaming_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_SON
//					SWITCH iRootFilenameNum
//						CASE 1
//							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_2@IG_2_P2"
//							tAnimDialogueClip = "JIM_IG_2_P2_Lagging"
//						BREAK
//						CASE 2
//							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_2@EXIT"
//							tAnimDialogueClip = "Exit"
//						BREAK
//						
//						DEFAULT
//							#IF IS_DEBUG_BUILD
//							SAVE_STRING_TO_DEBUG_FILE("ANY_SPEECH_PLAYING")
//							
//							SAVE_STRING_TO_DEBUG_FILE(", rootLabel: ")
//							SAVE_STRING_TO_DEBUG_FILE(rootLabel)
//							
//							SAVE_STRING_TO_DEBUG_FILE(" line: ")
//							SAVE_INT_TO_DEBUG_FILE(iRo""otFilenameNum)
//							
//							SAVE_NEWLINE_TO_DEBUG_FILE()
//							#ENDIF
//							
//							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M2_SON_gaming_exit")
//							
//		
//							RETURN FALSE
//						BREAK
//						
//					ENDSWITCH
//					
//					//
//					RETURN TRUE
//				BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_M_SON_smoking_weed_in_a_bong
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "Thats_Good_Shit_Dog"		//(Uhhh...That's good shit.)
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_1@IDLE_A"
							tAnimDialogueClip = "Thats_Some_Good_Shit"
						BREAK
						CASE 2
							// sShit_anim = "That_Was_The_Whole"		//(whoa That was the whole fucking chamber, dog.  )
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_1@IDLE_A"
							tAnimDialogueClip = "Whole_Chamber_Dog"
						BREAK
						CASE 3
							// sShit_anim = "This_Is_Some_Serious"		//(Oh, This is some serious bud. It's like Hydrotropic or something. )
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_1@IDLE_A"
							tAnimDialogueClip = "Hydrotropic_Bud_Or_Something"
						BREAK
						CASE 4
							// sShit_anim = "Dont_You_Dare_Judge"		//(Don't you dare judge me, dad)
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_1@IDLE_A"
							tAnimDialogueClip = "Dont_Dudge_Me_Dad"
						BREAK
						CASE 5
//							// sShit_anim = "Hot_I_Need_Ice"			//(Ahh! Hot!  I need ice in this fucking chamber.  )
//							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_1@IDLE_B"
//							tAnimDialogueClip = "Hot_I_Need_Ice"
							
							RETURN FALSE
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_SON_smoking_weed_in_a_bong [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_jumping_jacks
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					SWITCH iRootFilenameNum
						CASE 1	//Oh, God, I thought masturbation was exercise.
						CASE 4	//Eugh. Come on.
						CASE 7	//This. Is. Like. Impossible.
							tAnimDialogueDict = "TIMETABLE@REUNITED@IG_2"
							tAnimDialogueClip = "Jimmy_Masterbation"
						BREAK
						CASE 2	//Rolls. Flapping. Everywhere.
						CASE 5	//I'm gonna be a better me.
						CASE 8	//Argh.  Argh.
							tAnimDialogueDict = "TIMETABLE@REUNITED@IG_2"
							tAnimDialogueClip = "Jimmy_RollsFlapping"
						BREAK
						CASE 3	//Oh, God, I'm going to get knocked out by my own man boobs. 
						CASE 6	//I need a burger.
						CASE 9	//Come onnnn.
							tAnimDialogueDict = "TIMETABLE@REUNITED@IG_2"
							tAnimDialogueClip = "Jimmy_GetKnocked"
						BREAK
						
						DEFAULT
							#IF IS_DEBUG_BUILD
							IF ARE_STRINGS_EQUAL(rootLabel, sExpectedRootLabel)
								CASSERTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), " [rootLabel: ", rootLabel, " == ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							ELSE
								IF ARE_STRINGS_EQUAL(sDiddyRootLabel, sExpectedRootLabel)
									CASSERTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), " [sDiddyRootLabel: ", sDiddyRootLabel, " == ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
								ELSE
									CASSERTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent),  " [rootLabel: ", rootLabel, " != ", sExpectedRootLabel, ", sDiddyRootLabel: ", sDiddyRootLabel, ", line: ", iRootFilenameNum, "]")
								ENDIF
							ENDIF
							#ENDIF
							#IF NOT IS_DEBUG_BUILD
							CASSERTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent),  " [rootLabel: ", rootLabel, ", line: ", iRootFilenameNum, "]")
							#ENDIF
							
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_in_room_asks_for_munchies
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					SWITCH iRootFilenameNum
						CASE 1
							//(When they said body high, they weren't bullshitting.)
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_3@SLEEPING"
							tAnimDialogueClip = "Body_High2"
						BREAK
						CASE 2
							//(Ahhh. I really got to start growing this strain.)
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_3@SLEEPING"
							tAnimDialogueClip = "Grow_This_Strain"
						BREAK
						CASE 3
							//(Ahhh. This shit really helps my glands.)
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_3@SLEEPING"
							tAnimDialogueClip = "Helps_My_Glands"
						BREAK
						CASE 4
							//(Dad! Dad! Can you bring me some food please? For your only son?  )
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_3@SLEEPING"
							tAnimDialogueClip = "Bring_Me_Some_Food"
						BREAK
						CASE 5
							//(Ugh, my mouth is so dry. It's like the Sahara in here.)
							tAnimDialogueDict = ""
							tAnimDialogueClip = ""
							RETURN TRUE
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_SON_in_room_asks_for_munchies [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_5@IDLE_A"
							tAnimDialogueClip = "Good_To_See_You"
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_5@IDLE_A"
							tAnimDialogueClip = "I_Dont_Tell_You"
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@JIMMY@IG_5@IDLE_A"
							tAnimDialogueClip = "Its_The_Big_Man"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_SON_on_ecstasy_AND_friendly [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "idle_worry_lines"		//(Have you seen the worry lines you've given me!)
							tAnimDialogueDict = "TIMETABLE@AMANDA@FACEMASK@IDLE_A"
							tAnimDialogueClip = "idle_worry_lines"
						BREAK
						CASE 2
							// sShit_anim = "idle_old_hag"		//(It's a waste of time!  I'm an old hag.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@FACEMASK@IDLE_A"
							tAnimDialogueClip = "idle_old_hag"
						BREAK
						CASE 3
							// sShit_anim = "idle_years_wasted"		//(When I think of the years I've wasted on you...)
							tAnimDialogueDict = "TIMETABLE@AMANDA@FACEMASK@IDLE_A"
							tAnimDialogueClip = "idle_years_wasted"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_WIFE_in_face_mask [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_passed_out_BED
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE



					
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "idle_buzzed"		//(I'm really buzzed.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@DRUNK@IDLE_A"
							tAnimDialogueClip = "idle_buzzed"
						BREAK
						CASE 2
							// sShit_anim = "idle_only_you"		//(I only had a glass of pinot and a downer. Not sure what's wrong. Come here.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@DRUNK@IDLE_A"
							tAnimDialogueClip = "idle_only_you"
						BREAK
						CASE 3
							// sShit_anim = "idle_pinot"		//(Oh.  It's only you...)
							tAnimDialogueDict = "TIMETABLE@AMANDA@DRUNK@IDLE_A"
							tAnimDialogueClip = "idle_pinot"
						BREAK
						
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_WIFE_passed_out [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "idle_buzzed"		//(Oh God, You're creeping me out. Uch.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_5"
							tAnimDialogueClip = "IG_5_YouRCreepingMeOut"
						BREAK
						CASE 2
							// sShit_anim = "idle_only_you"		//(Not now, Michael. I was almost happy.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_5"
							tAnimDialogueClip = "IG_5_NotNowMichael"
						BREAK
						CASE 3
							// sShit_anim = "idle_pinot"		//(Please. I was trying to forget what a mess you've made of my life. )
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_5"
							tAnimDialogueClip = "IG_5_IWasTryingToForget"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_WIFE_sunbathing [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "IG_11_EXIT_OhShit"				//(Uh huh, yes. Stop it!)
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_11"
							tAnimDialogueClip = "IG_11_EXIT_OhShit"
						BREAK
						CASE 2
							// sShit_anim = "IG_11_EXIT_IMAFastLearner"		//(You are so bad. )
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_11"
							tAnimDialogueClip = "IG_11_EXIT_IKnowHeIsInTherapy"		//"IG_11_EXIT_IMAFastLearner"
						BREAK
						CASE 3
							// sShit_anim = "IG_11_EXIT_IKnowHeIsInTherapy"	//(Um hum, yes. I know, I know! Still.)
							tAnimDialogueDict = "TIMETABLE@AMANDA@IG_11"
							tAnimDialogueClip = "IG_11_EXIT_IKnowHeIsInTherapy"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_WIFE_hangs_up_and_wanders [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL



					
					SWITCH iRootFilenameNum
						CASE 1
							//TIMETABLE@MICHAEL@TALK_PHONEEXIT_A
							//TALK_PHONE_Exit_A.anim
							//TALK_PHONE_Exit_B.anim
							//TALK_PHONE_Exit_C.anim
							
							//(Doc Freidlander, I don't think tough love is going to work on this guy,)
							tAnimDialogueDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT_A"
							tAnimDialogueClip = "TALK_PHONE_Exit_A"
						BREAK
						CASE 2
							//TIMETABLE@MICHAEL@TALK_PHONEEXIT_B
							//TALK_PHONE_Exit_D.anim
							//TALK_PHONE_Exit_E.anim
							
							//(Please, baby. I know I was an asshole, okay? Just take one of my calls one day. Okay.)
							tAnimDialogueDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT_B"
							tAnimDialogueClip = "TALK_PHONE_Exit_D"
						BREAK
						CASE 3
							//TIMETABLE@MICHAEL@TALK_PHONEEXIT_C
							//TALK_PHONE_Exit_F.anim
							//TALK_PHONE_Exit_G.anim
							
							//(Look, Doc, I really don't think I'm enabling him ok? He does that all by himself.)
							tAnimDialogueDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT_C"
							tAnimDialogueClip = "TALK_PHONE_Exit_F"
						BREAK
						CASE 4
							//TIMETABLE@MICHAEL@TALK_PHONEEXIT_D
							//TALK_PHONE_Exit_H.anim
							//TALK_PHONE_Exit_I.anim
							
							//(Look Doc, all I'm saying is; you know how you spoke about my need to find humility? I think I might have found it.)
							tAnimDialogueDict = "TIMETABLE@MICHAEL@TALK_PHONEEXIT_D"
							tAnimDialogueClip = "TALK_PHONE_Exit_I"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T0_MICHAEL_hangs_up_and_wanders [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_DAUGHTER_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "Im_Youre_Daughter"		//(I'm your daughter, you pervert.)
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_3@IDLE_A"
							tAnimDialogueClip = "Im_Youre_Daughter"
						BREAK
						CASE 2
							// sShit_anim = "Get_Out_Of_The"		//(Get out of the sun, dad.  You're too old.)
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_3@IDLE_A"
							tAnimDialogueClip = "Get_Out_Of_The"
						BREAK
						CASE 3
							// sShit_anim = "Fucking_Old_Geezer"		//(Fuckin' old geezer...jesus.)
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_3@IDLE_B"
							tAnimDialogueClip = "Fucking_Old_Geezer"
						BREAK
						CASE 4
							// sShit_anim = "Youll_Melt_Out_Here"		//(You'll melt out here, big man.)
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_3@IDLE_B"
							tAnimDialogueClip = "Youll_Melt_Out_Here"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M2_DAUGHTER_sunbathing [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_1@IDLE_A"
							tAnimDialogueClip = "Of_Course_I_Go"
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_1@IDLE_A"
							tAnimDialogueClip = "Go_Stick_Youre_Head"
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_1@IDLE_A"
							tAnimDialogueClip = "Youre_All_The_Fucking"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_DAUGHTER_crying_over_a_guy [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER
					SWITCH iRootFilenameNum
						CASE 1	RETURN TRUE	BREAK	//You're the best.
						CASE 2						//Parenting at its finest.
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_14@"
							tAnimDialogueClip = "IG_14_ParentingAtItsFinest"
						BREAK
						CASE 3						//I wish all the boys were like you.
							tAnimDialogueDict = "TIMETABLE@TRACY@IG_14@"
							tAnimDialogueClip = "IG_14_IWishAll_A"
						BREAK
						CASE 4	RETURN TRUE	BREAK	//That wasn't so hard. 
						CASE 5	RETURN TRUE	BREAK	//Thank you.
						CASE 6	RETURN TRUE	BREAK	//You ain't so bad, daddio.
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_DAUGHTER_Coming_home_drunk [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_MEXMAID_stealing_stuff_caught
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@MAID@IG_8@"
							tAnimDialogueClip = "IG_8_IDLE_A"	//"IG_8_P2_IBringThisToday"
						BREAK
						CASE 2
							tAnimDialogueDict = "TIMETABLE@MAID@IG_8@"
							tAnimDialogueClip = "IG_8_IDLE_A"	//"IG_8_P3_ItLooksBroken"
						BREAK
						CASE 3
							tAnimDialogueDict = "TIMETABLE@MAID@IG_8@"
							tAnimDialogueClip = "IG_8_IDLE_A"	//"IG_8_P4_ITakeHomeToPolish"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "M_MEXMAID_stealing_stuff_caught [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_in_face_mask
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT
					SWITCH iRootFilenameNum
						CASE 1
							// sShit_anim = "idle_A_WhenWasYourLastTime"	//(When's the last time you gave your auntie a foot rub, boy?  These corns ain't never bit nobody.)
							tAnimDialogueDict = "TIMETABLE@DENICE@IG_2"
							tAnimDialogueClip = "idle_A_WhenWasYourLastTime"
						BREAK
						CASE 2
							// sShit_anim = "idle_B_BoyU"					//(Boy, you drive me sick with worry.)
							tAnimDialogueDict = "TIMETABLE@DENICE@IG_2"
							tAnimDialogueClip = "idle_B_BoyU"
						BREAK
						CASE 3
							// sShit_anim = "idle_C_ImSoStressedOut"		//(I am so stressed out, you've interrupted my menopause)
							tAnimDialogueDict = "TIMETABLE@DENICE@IG_2"
							tAnimDialogueClip = "idle_C_ImSoStressedOut"
						BREAK
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "F_AUNT_in_face_mask [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
				
					SWITCH iRootFilenameNum
						CASE 1
							tAnimDialogueDict = "TIMETABLE@FLOYD@CALLING"
							tAnimDialogueClip = "FL_IG_3_ThatsJustTheTv"		//(Oh, That's just the TV.  I got Fame or Shame on.)
						BREAK
						CASE 2	RETURN TRUE	BREAK						//Well, It might not be swearing, but it still hurts my feelings.
						CASE 3	RETURN TRUE	BREAK						//How, How's Bill? Is He enjoying himself out there?
						CASE 4	RETURN TRUE	BREAK						//Well It sounds like you're learning a lot at the conference.
						CASE 5
							tAnimDialogueDict = "TIMETABLE@FLOYD@CALLING"
							tAnimDialogueClip = "FL_IG_3_NoTheCondoIsFine"		//(N-n-n-no no, the condo is fine.  It's fine)
						BREAK
						CASE 6	RETURN TRUE	BREAK	//You made any new friends out there?  any uh, Male or female friends.
						CASE 7
							tAnimDialogueDict = "TIMETABLE@FLOYD@CALLING"
							tAnimDialogueClip = "FL_IG_3_IAmNotBeingJealous"	//(I ain't being jealous.  I'm just interested in what you're up to.)
						BREAK
						CASE 8	RETURN TRUE	BREAK						//I tried your room last night and nobody was in.
						CASE 9	RETURN TRUE	BREAK						//I just miss you so g-damn much.
						CASE 10	RETURN TRUE	BREAK						//No, I understand that you can't say you love me with your work friends around, you know,  but that don't explain why I aint able to.
						CASE 11	RETURN TRUE	BREAK						//Now I'm the one hearing things.  Who's that man in the back ground?
						CASE 12	RETURN TRUE	BREAK						//Well you know, Maybe next time I can come on the conference with you.  No?  I'll stay here.
						
						DEFAULT
							CPRINTLN(DEBUG_FAMILY, "unrecognised root filename num: ", Get_String_From_FamilyEvent(eFamilyEvent), "T1_FLOYD_on_phone_to_girlfriend [rootLabel: ", rootLabel, ", ", sExpectedRootLabel, ", line: ", iRootFilenameNum, "]")
							RETURN FALSE
						BREAK
						
					ENDSWITCH
					
					//
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DialogueAnim(", Get_String_From_FamilyEvent(eFamilyEvent), ")	\"", rootLabel, "\", ", iRootFilenameNum)
	CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DialogueAnim(", Get_String_From_FamilyEvent(eFamilyEvent), ")	\"", rootLabel, "\", ", iRootFilenameNum)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Update_Family_DialogueAnim(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		TEXT_LABEL_63 &tAnimDict, TEXT_LABEL_63 &tAnimClip,
		VECTOR vFamilyScenePos, FLOAT fFamilySceneHead)
	
	vFamilyScenePos = vFamilyScenePos
	fFamilySceneHead = fFamilySceneHead
	
	IF NOT IS_STRING_NULL_OR_EMPTY(tAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(tAnimClip)
		

		//#1718486
		#IF IS_DEBUG_BUILD
		IF ARE_STRINGS_EQUAL(tAnimDict, "null")
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "somehow a null dict eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DialogueAnim(\"", Get_String_From_FamilyEvent(eFamilyEvent), "\", 1718486)")
			CASSERTLN(DEBUG_FAMILY, "somehow a null dict eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DialogueAnim(\"", Get_String_From_FamilyEvent(eFamilyEvent), "\", 1718486)")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		#ENDIF
		// // // // //
		
		IF IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDict, tAnimClip)
		
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str  = ("tDialogueClip: ")
			str += (tAnimClip)
			str += " "
			str += GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tAnimDict, tAnimClip))
			
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_RED)
			#ENDIF
			
			CONST_FLOAT fANIM_CLIP_MAX_TIME 0.99
			FLOAT fAnimClipCurrentTime = GET_ENTITY_ANIM_CURRENT_TIME(PedIndex, tAnimDict, tAnimClip)
			IF fAnimClipCurrentTime <= fANIM_CLIP_MAX_TIME
				RETURN TRUE
			ENDIF
		ENDIF
		
		tAnimDict = ""
		tAnimClip = ""
	ENDIF
	
	IF IS_ANY_SPEECH_PLAYING(PedIndex)
		
		TEXT_LABEL_63 tAnimDialogueDict, tAnimDialogueClip
		IF PRIVATE_GetTextLabelFromConv(eFamilyMember, eFamilyEvent, tAnimDialogueDict, tAnimDialogueClip)
			REQUEST_ANIM_DICT(tAnimDialogueDict)
			IF HAS_ANIM_DICT_LOADED(tAnimDialogueDict)
				
				IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, tAnimDialogueDict, tAnimDialogueClip)
					
					TEXT_LABEL_63 tFamilyAnimDict
					TEXT_LABEL_63 tFamilyAnimClip
					ANIMATION_FLAGS eFamilyAnimFlag
					enumFamilyAnimProgress eFamilyAnimProgress
					IF PRIVATE_Get_FamilyMember_Anim(eFamilyMember, eFamilyEvent,
							tFamilyAnimDict, tFamilyAnimClip, eFamilyAnimFlag, eFamilyAnimProgress)
						
						
						#IF IS_DEBUG_BUILD
						#IF NOT IS_JAPANESE_BUILD
						IF (eFamilyAnimProgress <> FAP_2_dialogue)
							IF (eFamilyEvent = FE_T0_TREVOR_doing_a_shit)
							AND (eFamilyAnimProgress = FAP_3_array)
								CPRINTLN(DEBUG_FAMILY, "eFamilyAnimProgress <> FAP_2_dialogue for ", Get_String_From_FamilyEvent(eFamilyEvent))
							ELSE
								CASSERTLN(DEBUG_FAMILY, "eFamilyAnimProgress <> FAP_2_dialogue for ", Get_String_From_FamilyEvent(eFamilyEvent), ", eFamilyAnimProgress: FAP_", eFamilyAnimProgress)
							ENDIF
						ENDIF
						#ENDIF
						#ENDIF
						
						IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_LOOPING)
							CLEAR_BITMASK_ENUM_AS_ENUM(eFamilyAnimFlag, AF_LOOPING)
						ELSE
							//
						ENDIF
						
						
						IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_USE_KINEMATIC_PHYSICS)
							SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)		//#1421348
							SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
						ENDIF
						
						IK_CONTROL_FLAGS ikFlags
						PRIVATE_Get_FamilyMember_Anim_Ik_Control_Flags(eFamilyMember, eFamilyEvent, ikFlags)
						
						FLOAT blendOutDelta = NORMAL_BLEND_OUT
						IF eFamilyEvent = FE_M_WIFE_hangs_up_and_wanders
						OR eFamilyEvent = FE_M_MEXMAID_stealing_stuff_caught
							blendOutDelta = WALK_BLEND_OUT
						ENDIF
						
						IF eFamilyEvent = FE_M_WIFE_hangs_up_and_wanders
							IF IS_BITMASK_ENUM_AS_ENUM_SET(eFamilyAnimFlag, AF_DISABLE_FORCED_PHYSICS_UPDATE)
								//
							ELSE
								SET_BITMASK_ENUM_AS_ENUM(eFamilyAnimFlag, AF_DISABLE_FORCED_PHYSICS_UPDATE)
							ENDIF
							
							blendOutDelta = SLOW_BLEND_OUT
						ENDIF
						
						TASK_PLAY_ANIM(PedIndex, tAnimDialogueDict, tAnimDialogueClip,
								NORMAL_BLEND_IN, blendOutDelta, -1,
								eFamilyAnimFlag, 0, DEFAULT, ikFlags)
						
						tAnimDict = tAnimDialogueDict
						tAnimClip = tAnimDialogueClip
						
						RETURN TRUE
					ENDIF
					
					RETURN FALSE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 str
				str  = ("dialogueAnim: ")
				str += (tAnimDialogueClip)
				
				DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(PedIndex, FALSE), 2, HUD_COLOUR_PINK)
				#ENDIF
				
//				RETURN TRUE
			ENDIF
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "invalid eFamilyMember ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Update_Family_DialogueAnim(", Get_String_From_FamilyEvent(eFamilyEvent))
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC
