///private header for family door control scripts
///    alwyn.roberts@rockstarnorth.com
///    

USING "commands_entity.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_ped.sch"
USING "commands_recording.sch"

// *******************************************************************************************
//	FAMILY DOORS PRIVATE FUNCTIONS
// *******************************************************************************************

ENUM enumFamilyDoors
	FD_0_MICHAEL_SON,
	FD_1_MICHAEL_DAUGHTER,
	FD_2_MICHAEL_BATHROOM,
	FD_7_MICHAEL_BEDROOM,
	
	FD_3_MICHAEL_FRIDGE_L,
	FD_4_MICHAEL_FRIDGE_R,
	
	FD_5_MICHAEL_BACKDOOR_L,
	FD_6_MICHAEL_BACKDOOR_R,
	FD_7_MICHAEL_FRONTDOOR_L,
	FD_8_MICHAEL_FRONTDOOR_R,
	
	FD_9_TREVOR_1_BATHROOM,
	
	MAX_FAMILY_DOORS,
	NO_FAMILY_DOORS
ENDENUM

#IF IS_DEBUG_BUILD
FUNC BOOL DrawDebugFamilyDoorTextWithOffset(enumFamilyDoors eFamilyDoor, STRING text, VECTOR VecCoors, FLOAT Offset_y, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE, FLOAT fAlphaMult = 1.0)
	TEXT_LABEL_63 str
	IF (eFamilyDoor < MAX_FAMILY_DOORS)
		str  = "door "
		str += (ENUM_TO_INT(eFamilyDoor))
		str += " "
	ELSE
		str  = "prop "
	ENDIF
	str += text
	
	IF DrawDebugFamilyTextWithOffset(str, VecCoors, Offset_y, eColour, fAlphaMult)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

PROC PRIVATE_Get_Family_Door_Attributes(enumFamilyDoors eFamilyDoor,
		MODEL_NAMES &ObjectModel, VECTOR &vecPos, DOOR_NAME_ENUM &eDoorName, DOOR_HASH_ENUM &eDoorHash, TEXT_LABEL_63 &roomName, BOOL &bIgnoreInRoomCheck)

	SWITCH eFamilyDoor
		CASE FD_0_MICHAEL_SON
			ObjectModel = V_ILEV_MM_DOORSON
			vecPos = <<-806.8,174.0,76.9>>
			roomName = "V_Michael_1_Son"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DOORHASH_M_MANSION_SON
			
			bIgnoreInRoomCheck = FALSE
		BREAK
		CASE FD_1_MICHAEL_DAUGHTER
			ObjectModel = V_ILEV_MM_DOORDAUGHTER
			vecPos = <<-802.7,176.2,76.9>>
			roomName = "V_Michael_1_Daught"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DOORHASH_M_MANSION_DAUGHTER
			
			bIgnoreInRoomCheck = FALSE
		BREAK
		CASE FD_2_MICHAEL_BATHROOM
			ObjectModel = V_ILEV_MM_DOORW
			vecPos = <<-805.0,171.9,76.9>>
			roomName = "V_Michael_1_WC"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DOORHASH_M_MANSION_BATHROOM
			
			bIgnoreInRoomCheck = FALSE
		BREAK
		
		CASE FD_7_MICHAEL_BEDROOM
			ObjectModel = V_ILEV_MM_DOORW
			vecPos = <<-809.281,177.855,76.890>>
			roomName = "V_Michael_1_WC"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DOORHASH_M_MANSION_BEDROOM
			
			bIgnoreInRoomCheck = FALSE
		BREAK
		
		CASE FD_3_MICHAEL_FRIDGE_L
			ObjectModel = V_ILEV_MM_FRIDGE_L
			vecPos = <<-804.1,185.8,72.7>>
			roomName = "V_Michael_G_Kitche"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DUMMY_DOORHASH
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		CASE FD_4_MICHAEL_FRIDGE_R
			ObjectModel = V_ILEV_MM_FRIDGE_R
			vecPos = <<-802.8,186.3,72.7>>
			roomName = "V_Michael_G_Kitche"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DUMMY_DOORHASH
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		CASE FD_5_MICHAEL_BACKDOOR_L
			ObjectModel = PROP_BH1_48_BACKDOOR_L
			vecPos = <<-796.5657, 177.2214, 73.0405>>
			roomName = "V_Michael_G_Kitche"
			eDoorName = DOORNAME_M_MANSION_R_L1
			eDoorHash = DOORHASH_M_MANSION_R_L1
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		CASE FD_6_MICHAEL_BACKDOOR_R
			ObjectModel = PROP_BH1_48_BACKDOOR_R
			vecPos = <<-794.5051, 178.0124, 73.0405>>
			roomName = "V_Michael_G_Kitche"
			eDoorName = DOORNAME_M_MANSION_R_R1
			eDoorHash = DOORHASH_M_MANSION_R_R1
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		CASE FD_7_MICHAEL_FRONTDOOR_L
			ObjectModel = V_ILEV_MM_DOORM_L
			vecPos = << -817.0, 179.0, 73.0 >>
			roomName = "V_Michael_G_Front"
			eDoorName = DOORNAME_M_MANSION_F_L
			eDoorHash = DOORHASH_M_MANSION_F_L
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		CASE FD_8_MICHAEL_FRONTDOOR_R
			ObjectModel = V_ILEV_MM_DOORM_R
			vecPos = << -816.0, 178.0, 73.0 >>
			roomName = "V_Michael_G_Front"
			eDoorName = DOORNAME_M_MANSION_F_R
			eDoorHash = DOORHASH_M_MANSION_F_R
			
			bIgnoreInRoomCheck = TRUE
		BREAK
		
		CASE FD_9_TREVOR_1_BATHROOM
			ObjectModel = v_ilev_trev_doorbath
			vecPos = <<-1150.158,-1518.768,10.781>>
			roomName = "rm_bathroom"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DOORHASH_T_APARTMENT_VB_BATHROOM
			
			bIgnoreInRoomCheck = FALSE
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_FAMILY, "invalid eFamilyDoor ", ENUM_TO_INT(eFamilyDoor), " in PRIVATE_Get_Family_Door_Attributes()")
			
			SCRIPT_ASSERT("invalid eFamilyDoor PRIVATE_Get_Family_Door_Attributes()")
			
			ObjectModel = DUMMY_MODEL_FOR_SCRIPT
			vecPos = <<0,0,0>>
			roomName = "NULL"
			eDoorName = DUMMY_DOORNAME
			eDoorHash = DUMMY_DOORHASH
			
			bIgnoreInRoomCheck = FALSE
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SetStateOfClosestFamilyDoorOfType(MODEL_NAMES ObjectModelHashKey, VECTOR vecPos,
		DOOR_NAME_ENUM eDoorName, DOOR_HASH_ENUM eDoorHash,
		DOOR_STATE_ENUM state, FLOAT fOpenRatio, TEXT_LABEL_63 roomName)
	
	
	#IF IS_DEBUG_BUILD
//	CPRINTLN(DEBUG_FAMILY, "<")
//	CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME())
//	CPRINTLN(DEBUG_FAMILY, "> SetStateOfClosestFamilyDoorOfType(")
//	CPRINTLN(DEBUG_FAMILY, GET_MODEL_NAME_FOR_DEBUG(ObjectModelHashKey))
//	CPRINTLN(DEBUG_FAMILY, ", ")
//	CprintVECTOR(vecPos)
//	CPRINTLN(DEBUG_FAMILY, ", ")
//	CPRINTLN(DEBUG_FAMILY, ENUM_TO_INT(eDoorHash))
//	CPRINTLN(DEBUG_FAMILY, ", locked:")
//	CPRINTLN(DEBUG_FAMILY, ENUM_TO_INT(state))
//	CPRINTLN(DEBUG_FAMILY, ", ratio:")
//	CPRINTLN(DEBUG_FAMILY, fOpenRatio)
//	CPRINTLN(DEBUG_FAMILY, ")")
//	CprintNL()
	
	TEXT_LABEL_63 str2
	HUD_COLOURS eHudColour = HUD_COLOUR_PURE_WHITE
	#ENDIF
	
	IF (eDoorName <> DUMMY_DOORNAME)
		SET_DOOR_STATE(eDoorName, state)
		
		#IF IS_DEBUG_BUILD
		str2 = ("SET_DOOR_STATE...")
		eHudColour = HUD_COLOUR_BLUE
		#ENDIF
	ELSE
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
			ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(eDoorHash), ObjectModelHashKey, vecPos)
			
			#IF IS_DEBUG_BUILD
			str2 = ("ADD_DOOR_TO_SYSTEM...")
			eHudColour = HUD_COLOUR_REDDARK
			#ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str3
			#ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(roomName)
				
				#IF IS_DEBUG_BUILD
				str3  = ("room name is \"")
				str3 += (roomName)
				str3 += ("\"")
				#ENDIF
				
				IF (GET_HASH_KEY(roomName) = g_sShopSettings.playerRoom)
					#IF IS_DEBUG_BUILD
					str3 += (" player inside")
					eHudColour = HUD_COLOUR_PURPLE
					
					DrawDebugFamilyTextWithOffset(str3, vecPos,	3,	eHudColour)
					#ENDIF
					
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_UNLOCKED)
					
					RETURN FALSE
				ELSE
					#IF IS_DEBUG_BUILD
					str3 += (" player outside")
					eHudColour = HUD_COLOUR_ORANGE
					#ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyTextWithOffset(str3, vecPos,	3,	eHudColour)
				#ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				str3  = ("room name is null")
				eHudColour = HUD_COLOUR_PURPLE
				
				DrawDebugFamilyTextWithOffset(str2, vecPos,	3,	eHudColour)
				#ENDIF
			ENDIF
			
			BOOL bForceDoorForLock = FALSE
			IF state = DOORSTATE_FORCE_LOCKED_THIS_FRAME
				IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(eDoorHash)) <> DOORSTATE_FORCE_LOCKED_THIS_FRAME
					IF fOpenRatio < 0.5
//						SCRIPT_ASSERT("bForceDoorForLock = TRUE")
						bForceDoorForLock = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			CONST_FLOAT fCONST_tooCloseToSet	3.0
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vecPos) > (fCONST_tooCloseToSet*fCONST_tooCloseToSet)
			OR bForceDoorForLock
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), state, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), fOpenRatio, default, TRUE)
				
				IF state = DOORSTATE_FORCE_LOCKED_THIS_FRAME
					FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 5.0
					
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vecPos) < (fReplayBlockRange*fReplayBlockRange)	//30.0f radius on replay recording
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2184277
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
				str2 = ("DOOR_SYSTEM_SET_DOOR_STATE...")
				eHudColour = HUD_COLOUR_REDLIGHT
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				str2 = ("Player too close to the door [")
				str2 += GET_STRING_FROM_FLOAT(vdist(get_entity_coords(PLAYER_PED_ID(), FALSE), vecPos))
				str2 += ("m]...")
				
				eHudColour = HUD_COLOUR_RED
				
				DrawDebugFamilySphere(vecPos, fCONST_tooCloseToSet, HUD_COLOUR_RED, 0.5)
				#ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str2, vecPos,	2,	eHudColour)
	#ENDIF
	
	fOpenRatio = fOpenRatio
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Set_Family_Door(enumFamilyDoors eFamilyDoor,
		DOOR_STATE_ENUM eIntendedState,
		enumFamilyDoors &eFamilyDoorEnum,
		FLOAT fUnlockedRatio = 0.0)
	MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vecPos = <<0,0,0>>
	DOOR_NAME_ENUM eDoorName = DUMMY_DOORNAME
	DOOR_HASH_ENUM eDoorHash = DUMMY_DOORHASH
	TEXT_LABEL_63 roomName
	
	BOOL bIgnoreInRoomCheck = FALSE
	
	PRIVATE_Get_Family_Door_Attributes(eFamilyDoor,
			ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	HUD_COLOURS hud_colour_status
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bDrawDebugFamilyStuff
	DOOR_STATE_ENUM eLockedState
	FLOAT fOpenRatio
//	GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(ObjectModel, vecPos, bLockedState, fOpenRatio)
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
		//
	ELSE
		eLockedState = DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(eDoorHash))
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(eDoorHash))
	ENDIF

	str = ""
	
	IF (eIntendedState=DOORSTATE_LOCKED)
		IF (eLockedState=DOORSTATE_LOCKED)
			str += ("LOCKED_Family_Door -  ")
			hud_colour_status = HUD_COLOUR_BLACK
		ELSE
			str += ("locking_Family_Door - ")
			hud_colour_status = HUD_COLOUR_PURE_WHITE
		ENDIF
	ELIF (eIntendedState=DOORSTATE_UNLOCKED)
		IF (eLockedState=DOORSTATE_LOCKED)
			str += ("unlocking_Family_Door() - ")
			hud_colour_status = HUD_COLOUR_PURE_WHITE
		ELSE
			str += ("UNLOCKED_Family_Door() -  ")
			hud_colour_status = HUD_COLOUR_BLACK
		ENDIF
	ELSE
		IF (eLockedState=DOORSTATE_LOCKED)
			str += "?locked?"
		ELSE
			str += "?unlocked?"
		ENDIF
		hud_colour_status = HUD_COLOUR_GREY
	ENDIF
	
	str += (" door_")
	str += (enum_to_int(eFamilyDoor))
	str += (": [")
	str += GET_STRING_FROM_FLOAT(fOpenRatio)
	str += ("]")
	
	ENDIF
	#ENDIF
	
	TEXT_LABEL_63 passedRoomName = ""
	IF NOT bIgnoreInRoomCheck
		passedRoomName = roomName
	ENDIF
	SetStateOfClosestFamilyDoorOfType(ObjectModel, vecPos, eDoorName, eDoorHash, eIntendedState, fUnlockedRatio, passedRoomName)
	
	#IF IS_DEBUG_BUILD
	DrawDebugFamilyTextWithOffset(str, vecPos,	0,	HUD_COLOUR_status)
	#ENDIF
	
	eFamilyDoorEnum = eFamilyDoor
	RETURN TRUE
		
ENDFUNC

FUNC BOOL PRIVATE_Set_Family_SyncSceneProp(OBJECT_INDEX &propObj,
		STRING tPropSyncDict, STRING sPropSynchClip, STRING sPropSynchClipTailA, STRING sPropSynchClipTailB,
		INT &iScene, enumFamilyDoors eFamilyDoor = NO_FAMILY_DOORS)
	IF DOES_ENTITY_EXIST(propObj)
		#IF IS_DEBUG_BUILD
		VECTOR vecPos = GET_ENTITY_COORDS(propObj, FALSE)
		#ENDIF
			
		IF NOT IS_STRING_NULL_OR_EMPTY(tPropSyncDict)
//		AND NOT IS_STRING_NULL_OR_EMPTY(sPropSynchClip)
		
		
			INT iMatchClip_length = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(sPropSynchClip)
				iMatchClip_length = GET_LENGTH_OF_LITERAL_STRING(sPropSynchClip)
			ENDIF
			INT iMatchTail_length = GET_LENGTH_OF_LITERAL_STRING(sPropSynchClipTailA)
			
			INT iMatchBody_length = iMatchClip_length - iMatchTail_length
			
			TEXT_LABEL_63 tMatchBody = ""
			IF NOT IS_STRING_NULL_OR_EMPTY(sPropSynchClip)
				tMatchBody = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sPropSynchClip, 0, iMatchBody_length)
			ENDIF
			
			TEXT_LABEL_63 tPropSynchClip = tMatchBody
			tPropSynchClip += sPropSynchClipTailB
			
			IF DOES_ENTITY_HAVE_DRAWABLE(propObj)
				IF NOT HAS_ANIM_DICT_LOADED(tPropSyncDict)
					
					CPRINTLN(DEBUG_FAMILY, "REQUEST_ANIM_DICT [", tPropSyncDict, "]")
					
					REQUEST_ANIM_DICT(tPropSyncDict)
					RETURN FALSE
				ENDIF
				
				IF NOT IS_ENTITY_PLAYING_ANIM(propObj, tPropSyncDict, tPropSynchClip)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "Synch not playing", vecPos, 0, HUD_COLOUR_ORANGE, 1)
						#ENDIF
						
						CPRINTLN(DEBUG_FAMILY, "Synch not playing [", iScene, "]")
						
						RETURN FALSE
					ENDIF
					
					
					IF (eFamilyDoor < MAX_FAMILY_DOORS)
						CPRINTLN(DEBUG_FAMILY, "update door ", ENUM_TO_INT(eFamilyDoor), " ")
					ELSE
						CPRINTLN(DEBUG_FAMILY, "update prop ")
					ENDIF
					
					CPRINTLN(DEBUG_FAMILY, "Synchation to [", tPropSyncDict, ", ", tPropSynchClip, "] iScene:", iScene)
					
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(propObj)
						DETACH_ENTITY(propObj)
					ENDIF
					
					FLOAT fBlendInDelta = NORMAL_BLEND_IN
					FLOAT fBlendOutDelta = NORMAL_BLEND_OUT
					SYNCED_SCENE_PLAYBACK_FLAGS eFlags = SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS
					FLOAT eFlagsMoverBlendInDelta = INSTANT_BLEND_IN
					
					IF (GET_ENTITY_MODEL(propObj) = S_Prop_hdphones)
					OR (GET_ENTITY_MODEL(propObj) = V_RES_FA_CHAIR02)
					OR (GET_ENTITY_MODEL(propObj) = P_NOVEL_01_S)
					OR (GET_ENTITY_MODEL(propObj) = p_whiskey_bottle_s)
						fBlendInDelta = INSTANT_BLEND_IN
						fBlendOutDelta = INSTANT_BLEND_OUT
					ENDIF
					
					PLAY_SYNCHRONIZED_ENTITY_ANIM(propObj, iScene,
							tPropSynchClip, tPropSyncDict,
							fBlendInDelta, fBlendOutDelta, ENUM_TO_INT(eFlags), eFlagsMoverBlendInDelta)
					
					IF fBlendInDelta = INSTANT_BLEND_IN
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(propObj)
					ENDIF
					
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				
				TEXT_LABEL_63 str = "synch \""
				str += tPropSynchClip
				str += "\": "
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
					str += "null"
				ELSE
					str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
				ENDIF
				DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, str, vecPos, 0, HUD_COLOUR_ORANGE, 1)
				
				//DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, str, vecPos, 1, HUD_COLOUR_ORANGE, 1)
				#ENDIF
				
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "without drawable", vecPos, 0, HUD_COLOUR_ORANGE, 1)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "nameless", vecPos, 0, HUD_COLOUR_ORANGEDARK, 1)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "exists", vecPos, 0, HUD_COLOUR_ORANGE, 1)
		#ENDIF
		
		SET_OBJECT_AS_NO_LONGER_NEEDED(propObj)
		RETURN FALSE
	ELSE
		#IF IS_DEBUG_BUILD
		VECTOR vecPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "doesnt exist at coords", vecPos, 0, HUD_COLOUR_ORANGE, 1)
		#ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL PRIVATE_Set_Family_SyncSceneDoor(OBJECT_INDEX &doorObj, OBJECT_INDEX &fakeDoorObj, enumFamilyMember eFamilyMember,
		enumFamilyDoors eFamilyDoor,
		STRING tDoorSyncDict, STRING sDoorSynchClip, STRING sDoorSynchClipTailA, STRING sDoorSynchClipTailB,
		INT &iScene, BOOL bDeleteDoor)
	
	MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vecPos = <<0,0,0>>
	DOOR_NAME_ENUM eDoorName = DUMMY_DOORNAME
	DOOR_HASH_ENUM eDoorHash = DUMMY_DOORHASH
	TEXT_LABEL_63 roomName
	
	BOOL bIgnoreInRoomCheck = FALSE
	PRIVATE_Get_Family_Door_Attributes(eFamilyDoor,
			ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
	
	IF (ObjectModel <> DUMMY_MODEL_FOR_SCRIPT)
		//
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "door ", ENUM_TO_INT(eFamilyDoor), " has invalid model in PRIVATE_Set_Family_StaticDoor()")
		
		DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "has invalid model", vecPos, 0, HUD_COLOUR_ORANGE, 1)
		#ENDIF
		
		SCRIPT_ASSERT("door has invalid model in PRIVATE_Set_Family_StaticDoor()")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
		//
		
		#IF IS_DEBUG_BUILD
		DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "test0 - not running...", vecPos, 2, HUD_COLOUR_ORANGE, 1)
		#ENDIF
		
		RETURN FALSE
	ELSE
		IF IS_SYNCHRONIZED_SCENE_LOOPED(iScene)
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "test1 - not looped...", vecPos, 2, HUD_COLOUR_ORANGE, 1)
			#ENDIF
			
		ELSE
			IF NOT bDeleteDoor
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "test2 - not delete door...", vecPos, 2, HUD_COLOUR_ORANGE, 1)
				#ENDIF
				
			ELSE
				FLOAT fExitDoorPhase = 0.95
				IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iScene)
					fExitDoorPhase = 1.0
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) >= fExitDoorPhase
					
					BOOL bDoorDeleted = FALSE
					IF DOES_ENTITY_EXIST(fakeDoorObj)
						DELETE_OBJECT(fakeDoorObj)
						bDoorDeleted = TRUE
					ENDIF
					IF DOES_ENTITY_EXIST(doorObj)
						REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
						DELETE_OBJECT(doorObj)
						bDoorDeleted = TRUE
					ENDIF
					
					IF bDoorDeleted
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_LOCKED_THIS_FRAME, default, TRUE)
							DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.0, default, TRUE)
						ENDIF
					ENDIF
					
					RETURN FALSE
				ELSE
					
					#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 str = "test3 - not phase ["
					str += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iScene))
					str += "]"
					DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, str, vecPos, 2, HUD_COLOUR_ORANGE, 1)
//					CPRINTLN(DEBUG_FAMILY, GET_SYNCHRONIZED_SCENE_PHASE(iScene))
//					CPRINTLN(DEBUG_FAMILY, "]")CprintNL()
					#ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	FLOAT fFakeDoorHead = 999.0
	
	SWITCH eFamilyDoor
		CASE FD_0_MICHAEL_SON
			fFakeDoorHead = 300.0
		BREAK
		CASE FD_1_MICHAEL_DAUGHTER
			fFakeDoorHead = 210.0
		BREAK
	ENDSWITCH
	
	IF NOT DOES_ENTITY_EXIST(doorObj)
		REQUEST_MODEL(ObjectModel)
		
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vecPos, 10.0, ObjectModel)
			//
			
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
				#IF IS_DEBUG_BUILD
				DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "door not registered", vecPos, 0, HUD_COLOUR_ORANGE, 1)
				#ENDIF
				
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(eDoorHash), ObjectModel, vecPos)
				
				RETURN FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilyDoorTextWithOffset(eFamilyDoor, "doesnt exist at coords", vecPos, 0, HUD_COLOUR_ORANGE, 1)
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "CREATE_OBJECT(", GET_MODEL_NAME_FOR_DEBUG(ObjectModel), ", ", vecPos, ")")
		#ENDIF
		
		IF NOT HAS_MODEL_LOADED(ObjectModel)
			REQUEST_MODEL(ObjectModel)
			RETURN FALSE
		ENDIF
		
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_OPEN_THIS_FRAME, default, TRUE)
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.1, default, TRUE)
		ENDIF
		
		CREATE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
		
		doorObj = CREATE_OBJECT(ObjectModel, vecPos)
		SET_CAN_CLIMB_ON_ENTITY(doorObj, FALSE)
		PRIVATE_ForceRoomForFamilyEntity(doorObj, eFamilyMember)
		
		IF bDeleteDoor
			IF fFakeDoorHead < 360
				fakeDoorObj = CREATE_OBJECT(ObjectModel, vecPos+<<0,0,-1>>)
				SET_ENTITY_ROTATION(fakeDoorObj, <<0,0,fFakeDoorHead>>)
				
				FREEZE_ENTITY_POSITION(fakeDoorObj, TRUE)
				
				SET_CAN_CLIMB_ON_ENTITY(fakeDoorObj, FALSE)
				PRIVATE_ForceRoomForFamilyEntity(doorObj, eFamilyMember)
				
				SET_ENTITY_ALPHA(fakeDoorObj, 5, FALSE)
			ENDIF
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(ObjectModel)
		
		PRIVATE_Set_Family_SyncSceneProp(doorObj, tDoorSyncDict, sDoorSynchClip, sDoorSynchClipTailA, sDoorSynchClipTailB, iScene)
		
		WAIT(0)
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.0, default, TRUE)
	ENDIF
	
	IF PRIVATE_Set_Family_SyncSceneProp(doorObj,
			tDoorSyncDict, sDoorSynchClip, sDoorSynchClipTailA, sDoorSynchClipTailB,
			iScene)
		
		IF DOES_ENTITY_EXIST(fakeDoorObj)
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_OPEN_THIS_FRAME, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 1.0, default, TRUE)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF fFakeDoorHead > 360
				SET_ENTITY_ROTATION(fakeDoorObj, GET_ENTITY_ROTATION(fakeDoorObj)+<<0,0,10>>)
			ENDIF
			
			DrawDebugFamilyTextWithOffset("fakeDoorObj", get_ENTITY_coords(fakeDoorObj), 0, HUD_COLOUR_ORANGE, 1)
			DrawDebugFamilyTextWithOffset(GET_STRING_FROM_FLOAT(GET_ENTITY_HEADING(fakeDoorObj)), get_ENTITY_coords(fakeDoorObj), 1, HUD_COLOUR_ORANGE, 1)
			#ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Update_Family_Doors(enumFamilyEvents eFamilyEvent,
		enumFamilyDoors &eFamilyDoorEnum)
	SWITCH eFamilyEvent
		CASE FE_M2_SON_gaming_loop					PRIVATE_Set_Family_Door(FD_0_MICHAEL_SON,			DOORSTATE_FORCE_OPEN_THIS_FRAME, eFamilyDoorEnum, -1.0)	RETURN TRUE	BREAK
		CASE FE_M7_SON_Gaming						RETURN PRIVATE_Update_Family_Doors(FE_M2_SON_gaming_loop, eFamilyDoorEnum) BREAK
		CASE FE_M_SON_phone_calls_in_room			PRIVATE_Set_Family_Door(FD_0_MICHAEL_SON,			DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
		CASE FE_M_SON_watching_porn					PRIVATE_Set_Family_Door(FD_0_MICHAEL_SON,			DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
//		CASE FE_M_SON_raids_fridge_for_food
//			
////		CONST_FLOAT fFridgeDoor_orig		21.0
////		CONST_FLOAT fFridgeDoor_offset		85.0
//			
//			PRIVATE_Set_Family_AnimatedDoor(doorObj, FD_3_MICHAEL_FRIDGE_L, "Base_Fridge_L")
//			PRIVATE_Set_Family_AnimatedDoor(doorObj, FD_4_MICHAEL_FRIDGE_R, "Base_Fridge_R")
//			
//			RETURN TRUE
//		BREAK
		
		CASE FE_M_FAMILY_MIC4_locked_in_room		PRIVATE_Set_Family_Door(FD_1_MICHAEL_DAUGHTER,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
		
		CASE FE_M_DAUGHTER_purges_in_the_bathroom	PRIVATE_Set_Family_Door(FD_2_MICHAEL_BATHROOM,	DOORSTATE_FORCE_OPEN_THIS_FRAME, eFamilyDoorEnum, -0.75)	RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
			PRIVATE_Set_Family_Door(FD_2_MICHAEL_BATHROOM,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)
			
			//#1451588
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BW))
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BW), DOORSTATE_FORCE_LOCKED_THIS_FRAME, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BW), 0.0, default, TRUE)
			ENDIF
			
			RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room		PRIVATE_Set_Family_Door(FD_1_MICHAEL_DAUGHTER,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
		CASE FE_M_DAUGHTER_on_phone_LOCKED	PRIVATE_Set_Family_Door(FD_1_MICHAEL_DAUGHTER,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
		
//		CASE FE_M_MICHAEL_MIC2_washing_face			PRIVATE_Set_Family_Door(FD_2_MICHAEL_BATHROOM,	DOORSTATE_FORCE_OPEN_THIS_FRAME, eFamilyDoorEnum, -0.75)	RETURN TRUE	BREAK
		
		CASE FE_M2_MEXMAID_clean_window
		CASE FE_M7_MEXMAID_clean_window
			
			SWITCH eFamilyDoorEnum
				CASE FD_6_MICHAEL_BACKDOOR_R
					PRIVATE_Set_Family_Door(FD_5_MICHAEL_BACKDOOR_L,		DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)
				BREAK
				CASE FD_5_MICHAEL_BACKDOOR_L
					PRIVATE_Set_Family_Door(FD_6_MICHAEL_BACKDOOR_R,		DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)
				BREAK
				DEFAULT
					
					IF GET_RANDOM_BOOL()
						PRIVATE_Set_Family_Door(FD_5_MICHAEL_BACKDOOR_L,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)
					ELSE
						PRIVATE_Set_Family_Door(FD_6_MICHAEL_BACKDOOR_R,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)
					ENDIF
					
				BREAK
			ENDSWITCH
			
			RETURN TRUE
		BREAK
		
		CASE FE_T1_FLOYD_with_wade_post_docks1		PRIVATE_Set_Family_Door(FD_9_TREVOR_1_BATHROOM,	DOORSTATE_FORCE_LOCKED_THIS_FRAME, eFamilyDoorEnum)			RETURN TRUE	BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_Doors()")
	#ENDIF
	
	SCRIPT_ASSERT("invalid eFamilyEvent PRIVATE_Update_Family_Doors()")
	eFamilyDoorEnum = NO_FAMILY_DOORS
	RETURN FALSE
ENDFUNC



FUNC BOOL PRIVATE_Cleanup_Family_Doors()
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> PRIVATE_Cleanup_Family_Doors()")
	#ENDIF
	
	MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vecPos = <<0,0,0>>
	DOOR_NAME_ENUM eDoorName = DUMMY_DOORNAME
	DOOR_HASH_ENUM eDoorHash = DUMMY_DOORHASH
	TEXT_LABEL_63 roomName
	
	BOOL bIgnoreInRoomCheck = FALSE
	
	enumFamilyDoors eDoor
	REPEAT MAX_FAMILY_DOORS eDoor
		PRIVATE_Get_Family_Door_Attributes(eDoor, ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
		
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vecPos, 10.0, ObjectModel)
			OBJECT_INDEX doorObj = GET_CLOSEST_OBJECT_OF_TYPE(vecPos, 10.0, ObjectModel, FALSE)
			
			IF DOES_ENTITY_EXIST(doorObj)
			
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "			cleanup door[", ENUM_TO_INT(eDoor), "] ", " DOES_ENTITY_EXIST(", NATIVE_TO_INT(doorObj), ")")
				#ENDIF
				
				IF (eDoorHash = DUMMY_DOORHASH)
					CONST_FLOAT fFridgeDoor_orig		21.0
					SET_ENTITY_ROTATION(doorObj, <<0,0,fFridgeDoor_orig>>)
				ELSE
					//
				ENDIF
				
				SET_OBJECT_AS_NO_LONGER_NEEDED(doorObj)	//501471
			ENDIF
		ENDIF
		
		IF (eDoorName <> DUMMY_DOORNAME)
			//do nothing...
		ELSE
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
				//
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "			not registered door[", ENUM_TO_INT(eDoor), "] ", " (", ENUM_TO_INT(eDoorHash), ")")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "			cleanup door[", ENUM_TO_INT(eDoor), "] ", " REMOVE_DOOR_FROM_SYSTEM(", ENUM_TO_INT(eDoorHash), ")")
				#ENDIF
				
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.0, default, TRUE)
				
				IF (eDoor <> FD_5_MICHAEL_BACKDOOR_L)
				AND (eDoor <> FD_6_MICHAEL_BACKDOOR_R)
				AND (eDoor <> FD_7_MICHAEL_FRONTDOOR_L)
				AND (eDoor <> FD_8_MICHAEL_FRONTDOOR_R)
					REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(eDoorHash))
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Cleanup the autodoors usage too.
	FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, FALSE)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "")
	#ENDIF
	
	RETURN FALSE
ENDFUNC







// *******************************************************************************************
//	FAMILY DECALS PRIVATE FUNCTIONS
// *******************************************************************************************


FUNC BOOL PRIVATE_Update_Family_Decals(enumFamilyEvents eFamilyEvent, DECAL_ID &thisFamilyDecal, VECTOR vSequencePos)
	
	DECAL_RENDERSETTING_ID renderSettingsId
	
	VECTOR pos, dir, side
	FLOAT width, height
	FLOAT colR, colG, colB, colA
	FLOAT life
	
	SWITCH eFamilyEvent
		CASE FE_M_MEXMAID_MIC4_clean_window
			//
			//DECAL_RSID_BLOOD_SPLATTER = 1110,
			//DECAL_RSID_BLOOD_DIRECTIONAL = 1015,
			
			renderSettingsId = DECAL_RSID_BLOOD_SPLATTER
			
			pos		= <<-812.71619, 183.21797, 73.06655>>	- vSequencePos
			dir		= <<0.0, 0.0, -1.0>>
			side	= NORMALISE_VECTOR(<<1.0, 0.0, 0.0>>)
			
			width	= 1.0
			height	= 1.0
			
			colR	= 1
			colG	= 1
			colB	= 1
			colA	= 1
			
			life	= -1
			
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	//
	IF NOT IS_DECAL_ALIVE(thisFamilyDecal)
		thisFamilyDecal = ADD_DECAL(renderSettingsId, vSequencePos+pos, dir, side, width, height, colR, colG, colB, colA, life)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "add decal \"", ENUM_TO_INT(renderSettingsId), "\" for ", Get_String_From_FamilyEvent(eFamilyEvent), " (offset: ", pos, ")")
		
		DrawDebugFamilyTextWithOffset("add decal", vSequencePos+pos, 0.0, HUD_COLOUR_RED, 1.0)
		DrawDebugFamilyTextWithOffset("offset", vSequencePos+pos, 1.0, HUD_COLOUR_REDLIGHT, 1.0)

		#ENDIF
		
	ELSE
		
		#IF IS_DEBUG_BUILD
//		CPRINTLN(DEBUG_FAMILY, "decal ")
//		CPRINTLN(DEBUG_FAMILY, NATIVE_TO_INT(thisFamilyDecal))
//		CPRINTLN(DEBUG_FAMILY, " exists for ")
//		CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
//		
//		CPRINTLN(DEBUG_FAMILY, " (wash level: ")
//		CPRINTLN(DEBUG_FAMILY, GET_DECAL_WASH_LEVEL(thisFamilyDecal))
//		CPRINTLN(DEBUG_FAMILY, ")")
//		
//		CprintNL()
		
		DrawDebugFamilyTextWithOffset("decal exists", vSequencePos+pos, 0.0, HUD_COLOUR_GREEN, 1.0)
		DrawDebugFamilyTextWithOffset("wash level", vSequencePos+pos, 1.0, HUD_COLOUR_GREENLIGHT, 1.0)
		
		#ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	FAMILY CHAIRS PRIVATE FUNCTIONS
// *******************************************************************************************


FUNC BOOL PRIVATE_CanFreezeThisChairPos(OBJECT_INDEX chairObj)
	IF (GET_ENTITY_MODEL(chairObj) = V_ILEV_M_SOFA)
	OR (GET_ENTITY_MODEL(chairObj) = V_ILEV_M_DINECHAIR)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

BOOL bAmandasBreakfastChairSettled = FALSE

FUNC BOOL PRIVATE_Update_Family_SatInChair(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent, OBJECT_INDEX &chairObj
		, BOOL &bUpdateChairAttach
		)
	
	MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
	VECTOR vecPos = <<0,0,0>>, vecRot = <<0,0,0>>
	VECTOR vAttachOffset = <<0,0,0>>
	VECTOR vAttachRotation = <<0,0,0>>
	INT SecondEntityBoneIndex = 0
	
	VECTOR vecMasterPosition = <<0,0,0>>
	VECTOR vecMasterHeading = <<0,0,0>>
	
	CONST_INT	iSATINCHAIR_0_pedAttachedToChair		0
	CONST_INT	iSATINCHAIR_1_chairAttachedToPed		1
	CONST_INT	iSATINCHAIR_2_no_attachment				2
	INT iAttachPedToChair = iSATINCHAIR_0_pedAttachedToChair
	
	BOOL bDetachWhenDead = FALSE
	
	#IF NOT IS_DEBUG_BUILD			//cheap relase fix
	IF vecRot.x = vecMasterPosition.x
		IF vecMasterHeading.x = vecMasterPosition.x
		ENDIF
	ENDIF
	#ENDIF
	
	
	
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops
		CASE FE_M7_SON_on_laptop_looking_for_jobs
			
			IF eFamilyEvent = FE_M_FAMILY_on_laptops
				bDetachWhenDead = TRUE
			ENDIF
			
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-797.77,180.96,71.83>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<0,0,1>>
					vAttachRotation = <<0,0,180>>
					
					iAttachPedToChair = iSATINCHAIR_0_pedAttachedToChair
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-797.46,179.88,71.83>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<0,0,1>>
					vAttachRotation = <<0,0,180>>
					
					iAttachPedToChair = iSATINCHAIR_0_pedAttachedToChair
				BREAK
				CASE FM_MICHAEL_WIFE
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-796.65,181.23,71.83>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<-0.0, 0.0, -1.0>>
					vAttachRotation = <<0.0, 0.0, 180.0>>
					
					iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
				BREAK
				
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_finished_breakfast
		CASE FE_M7_FAMILY_finished_pizza
			bDetachWhenDead = TRUE
			
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-797.774,180.960,71.836>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<-0.0, 0.0, -1.0>>
					vAttachRotation = <<0.0, 0.0, 180.0>>
					
					iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-796.655,181.225,71.836>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<-0.0, 0.0, -1.0>>
					vAttachRotation = <<0.0, 0.0, 180.0>>
					
					iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
				BREAK
				CASE FM_MICHAEL_WIFE
					ObjectModel = V_ILEV_M_DINECHAIR
					vecPos = <<-797.461,179.875,71.836>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<-0.0, 0.0, -1.0>>
					vAttachRotation = <<0.0, 0.0, 180.0>>
					
					iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
				BREAK
				
			ENDSWITCH
			
//			SecondEntityBoneIndex = GET_PED_BONE_INDEX(PedIndex, BONETAG_NULL)
			iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
		BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing
		CASE FE_M7_DAUGHTER_sunbathing
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			bDetachWhenDead = TRUE
			
			ObjectModel = PROP_PATIO_LOUNGER1
			vecPos = <<-784.0,188.2,71.8>>
			vecRot = <<0,0,0>>
			
			IF (eFamilyEvent = FE_M2_WIFE_sunbathing)
			OR (eFamilyEvent = FE_M7_WIFE_sunbathing)
				vAttachOffset = <<-0.0900, -1.5700, 1.0000>>
				vAttachRotation = <<0,0,180>>
			ENDIF
		BREAK
		
//		CASE FE_M_DAUGHTER_crying_over_a_guy				FALLTHRU
//		CASE FE_M_DAUGHTER_watching_TV_sober		//		FALLTHRU
//		CASE FE_M_WIFE_passed_out_SOFA						FALLTHRU
//		CASE FE_M_MEXMAID_watching_TV
//			ObjectModel = V_ILEV_M_SOFA
//			vecPos = <<-803.5,171.9,71.8>>
//			vecRot = <<0,0,0>>
//			
//			IF (eFamilyEvent = FE_M_DAUGHTER_crying_over_a_guy)
//				vecMasterPosition = <<-803.72, 172.106, 71.877>>
//				vecMasterHeading = <<0,0,-67.76>>
//				
//			ELIF (eFamilyEvent = FE_M_DAUGHTER_watching_TV_sober)
//				vecMasterPosition = <<-803.97, 171.670, 71.874>>
//				vecMasterHeading = <<0,0,54.360>>
//				
//			ELIF (eFamilyEvent = FE_M_WIFE_passed_out_SOFA)
//				vAttachOffset = <<-1.590,0.330,1.250>>
//				vAttachRotation = <<0,0,180.0>>
//			
//			ELIF (eFamilyEvent = FE_M_MEXMAID_watching_TV)
//				vAttachOffset = <<0.00,-0.18,1.25>>
//				vAttachRotation = <<0,0,180.0>>
//				
//			ELSE
//				vAttachOffset = <<0,0,1>>
//				vecMasterPosition = <<0,0,0>>
//				
//				vAttachRotation = <<0,0,0>>
//				vecMasterHeading = <<0,0,0>>
//			ENDIF
//			
//			SecondEntityBoneIndex = 0
//		BREAK
//		CASE FE_F_AUNT_listens_to_selfhelp_tapes
//			ObjectModel = V_RES_FA_CHAIR02
//			vecPos = <<-8.79,-1432.22,30.10>>
//			vecRot = <<0,0,0>>
//			
//			vAttachOffset = <<-0.090,-0.575,1.050>>
//			vAttachRotation = <<0,0,180>>
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					ObjectModel = PROP_TABLE_03B_CHR
//					vecPos = <<-17.73,-1427.19,29.66>>
//					vecRot = <<0,0,0>>
//					
//					vAttachOffset = <<0,0,0>>
//					vAttachRotation = <<0,0,0>>
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					ObjectModel = PROP_TABLE_03B_CHR
//					vecPos = <<-17.41,-1425.65,29.66>>
//					vecRot = <<0,0,0>>
//					
//					vAttachOffset = <<0,0,0>>
//					vAttachRotation = <<0,0,0>>
//				BREAK
//				
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					ObjectModel = PROP_BBQ_3
//					vecPos = <<-22.379,-1423.889,29.664>>
//					vecRot = <<0,0,0>>
//					
//					vAttachOffset = <<0,0,0>>
//					vAttachRotation = <<0,0,0>>
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					RETURN FALSE
//				BREAK
//				
//			ENDSWITCH
//			
//			iAttachPedToChair = iSATINCHAIR_2_no_attachment
//		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					RETURN FALSE
				BREAK
				CASE FM_TREVOR_0_WIFE
					ObjectModel = PROP_CHAIR_06
					vecPos = <<1977.433,3819.045,32.453>>
					vecRot = <<0,0,0>>
					
					vAttachOffset = <<0.05,0,-0.6>>
					vAttachRotation = <<0.0,0,180>>
					
					iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed
				BREAK
				
			ENDSWITCH
		BREAK
		
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in PRIVATE_Update_Family_SatInChair()")
			#ENDIF
			
			UNUSED_PARAMETER(bUpdateChairAttach)
			SCRIPT_ASSERT("invalid eFamilyEvent PRIVATE_Update_Family_SatInChair()")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	IF NOT DOES_ENTITY_EXIST(chairObj)
		chairObj = GET_CLOSEST_OBJECT_OF_TYPE(vecPos, 10.0, ObjectModel, TRUE)
	ELSE
		IF (GET_ENTITY_MODEL(chairObj) <> ObjectModel)
			IF IS_ENTITY_ATTACHED_TO_ENTITY(PedIndex, chairObj)
				DETACH_ENTITY(PedIndex)
			ENDIF
			IF IS_ENTITY_ATTACHED_TO_ENTITY(chairObj, PedIndex)
				DETACH_ENTITY(chairObj)
			ENDIF
			
			SCRIPT_ASSERT("chair props dont match???")
			chairObj = NULL
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(chairObj)
		RETURN FALSE
	ELSE
		
		
		IF (iAttachPedToChair = iSATINCHAIR_0_pedAttachedToChair)
			IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(PedIndex, chairObj)
				BOOL bDetachWhenRagdoll = FALSE
				BOOL bActiveCollisions = TRUE //FALSE
				BOOL bUseBasicAttachIfPed = FALSE
				
			
				ATTACH_ENTITY_TO_ENTITY(PedIndex, chairObj, SecondEntityBoneIndex,
						vAttachOffset, vAttachRotation,
						bDetachWhenDead, bDetachWhenRagdoll,
						bActiveCollisions, bUseBasicAttachIfPed)
				
				FREEZE_ENTITY_POSITION(PedIndex, FALSE)
				IF PRIVATE_CanFreezeThisChairPos(chairObj)
					FREEZE_ENTITY_POSITION(chairObj, TRUE)
				ENDIF
				
				SET_ENTITY_NO_COLLISION_ENTITY(chairObj, PedIndex, FALSE)
				
				#IF IS_DEBUG_BUILD
				VECTOR vChairCoord = GET_ENTITY_COORDS(chairObj)
				CPRINTLN(DEBUG_FAMILY, " attached PedIndex to chairObj[", Get_String_From_FamilyEvent(eFamilyEvent), "] ", GET_MODEL_NAME_FOR_DEBUG(ObjectModel), " ", vChairCoord)
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ELIF (iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed)
			
			VECTOR vChairCoords = GET_ENTITY_COORDS(chairObj), vChairRot = GET_ENTITY_ROTATION(chairObj)
			
			VECTOR vOffsetCoords = GET_ENTITY_COORDS(PedIndex)+vAttachOffset
			VECTOR vOffsetRot = GET_ENTITY_ROTATION(PedIndex)+vAttachRotation
			
			IF VDIST(vChairCoords, vOffsetCoords) > 0.1
			OR (VDIST(vChairRot, vOffsetRot) > 0.1 AND VDIST(vChairRot, vOffsetRot) < 360)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "VDIST(vChairCoords, vOffsetCoords): ", VDIST(vChairCoords, vOffsetCoords), ", VDIST(vChairRot, vOffsetRot): ", VDIST(vChairRot, vOffsetRot))
				#ENDIF
				
				SET_ENTITY_COORDS(chairObj, GET_ENTITY_COORDS(PedIndex)+vAttachOffset)
				SET_ENTITY_ROTATION(chairObj, GET_ENTITY_ROTATION(PedIndex)+vAttachRotation)
				IF PRIVATE_CanFreezeThisChairPos(chairObj)
					FREEZE_ENTITY_POSITION(chairObj, TRUE)
				ENDIF
				SET_ENTITY_DYNAMIC(chairObj, FALSE)
				SET_ENTITY_COLLISION(chairObj, FALSE)
				
				SET_ENTITY_NO_COLLISION_ENTITY(chairObj, PedIndex, FALSE)
				SET_ENTITY_NO_COLLISION_ENTITY(PedIndex, chairObj, FALSE)
			ELSE
				IF eFamilyEvent = FE_M7_FAMILY_finished_breakfast
				OR eFamilyEvent = FE_M7_FAMILY_finished_pizza
					IF eFamilyMember = FM_MICHAEL_WIFE
						bAmandasBreakfastChairSettled = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF NOT bUpdateChairAttach
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_X)
				
				//IF (eFamilyMember = FM_MICHAEL_WIFE)
					bUpdateChairAttach = TRUE
				//ENDIF
				
			ENDIF
		ELSE	
			
			VECTOR VecMasterOffset
			
			TEXT_LABEL_63 str = "Update_"
			str += Get_String_From_FamilyMember(eFamilyMember)
			str += "_SatInChair"
			
			START_WIDGET_GROUP(str)
				ADD_WIDGET_BOOL("bUpdateChairAttach", bUpdateChairAttach)
				
				ADD_WIDGET_VECTOR_SLIDER("vecPos", vecPos, -6000.0, 6000.0, 0.0)
				ADD_WIDGET_VECTOR_SLIDER("vecRot", vecRot, -180.0, 180.0, 0.0)
				ADD_WIDGET_STRING(GET_MODEL_NAME_FOR_DEBUG(ObjectModel))
				
				ADD_WIDGET_VECTOR_SLIDER("vAttachOffset", vAttachOffset, -10.0, 10.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vAttachRotation", vAttachRotation, -180.0, 180.0, 1.0)
				ADD_WIDGET_INT_SLIDER("SecondEntityBoneIndex", SecondEntityBoneIndex, -1, 8, 1)
				
				ADD_WIDGET_STRING("master settings")
				ADD_WIDGET_VECTOR_SLIDER("vecMasterPosition", vecMasterPosition, -6000.0, 6000.0, 0.0)
				ADD_WIDGET_VECTOR_SLIDER("VecMasterOffset", VecMasterOffset, -10.0, 10.0, 0.0)
				ADD_WIDGET_VECTOR_SLIDER("vecMasterHeading", vecMasterHeading, -180.0, 180.0, 0.0)
			STOP_WIDGET_GROUP()
			
			WHILE bUpdateChairAttach
		
				IF (iAttachPedToChair = iSATINCHAIR_0_pedAttachedToChair)
					ATTACH_ENTITY_TO_ENTITY(PedIndex, chairObj, SecondEntityBoneIndex,
							vAttachOffset, vAttachRotation)
				ELIF (iAttachPedToChair = iSATINCHAIR_1_chairAttachedToPed)
					SET_ENTITY_COORDS(chairObj, GET_ENTITY_COORDS(PedIndex)+vAttachOffset)
					SET_ENTITY_ROTATION(chairObj, GET_ENTITY_ROTATION(PedIndex)+vAttachRotation)
					
					SET_ENTITY_DYNAMIC(chairObj, FALSE)
					FREEZE_ENTITY_POSITION(chairObj, TRUE)
					SET_ENTITY_COLLISION(chairObj, FALSE)
					
	//				ATTACH_ENTITY_TO_ENTITY(chairObj, PedIndex, SecondEntityBoneIndex,
	//						vAttachOffset, vAttachRotation,
	//						bDetachWhenDead, bDetachWhenRagdoll,
	//						bActiveCollisions, bUseBasicAttachIfPed)
				ELSE
					//
				ENDIF
				
				vecPos = GET_ENTITY_COORDS(chairObj)
				vecRot = GET_ENTITY_ROTATION(chairObj)
				VecMasterOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(chairObj, vecMasterPosition)
				
				WAIT(0)
			ENDWHILE
			
			SAVE_STRING_TO_DEBUG_FILE("vecPos = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vecPos)
			
			SAVE_STRING_TO_DEBUG_FILE(", vecRot = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vecPos)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("ObjectModel = ")
			SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(ObjectModel))
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("vAttachOffset = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vAttachOffset)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("vAttachRotation = ")
			SAVE_VECTOR_TO_DEBUG_FILE(vAttachRotation)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("SecondEntityBoneIndex = ")
			SAVE_INT_TO_DEBUG_FILE(SecondEntityBoneIndex)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
		ENDIF
		#ENDIF
		
		RETURN TRUE
	
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Cleanup_Family_Chairs(PED_INDEX &PedIndexes[], OBJECT_INDEX &chairObjs[])
	
	bAmandasBreakfastChairSettled = bAmandasBreakfastChairSettled
	bAmandasBreakfastChairSettled = FALSE
	
	INT iChairs
	REPEAT COUNT_OF(chairObjs) iChairs
		IF DOES_ENTITY_EXIST(chairObjs[iChairs])
			
			REMOVE_MODEL_HIDE(GET_ENTITY_COORDS(chairObjs[iChairs]), 5.0, GET_ENTITY_MODEL(chairObjs[iChairs]))
			
			IF NOT IS_ENTITY_DEAD(PedIndexes[iChairs])
				IF IS_ENTITY_ATTACHED_TO_ENTITY(PedIndexes[iChairs], chairObjs[iChairs])
					
					IF PRIVATE_CanFreezeThisChairPos(chairObjs[iChairs])
						FREEZE_ENTITY_POSITION(chairObjs[iChairs], FALSE)
					ENDIF
					
					FREEZE_ENTITY_POSITION(PedIndexes[iChairs], FALSE)
					DETACH_ENTITY(PedIndexes[iChairs])
				ENDIF
			ENDIF
		ENDIF
		
		SET_OBJECT_AS_NO_LONGER_NEEDED(chairObjs[iChairs])
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
