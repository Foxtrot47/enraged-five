USING "globals.sch"

USING "Commands_ped.sch"
USING "Commands_weapon.sch"

USING "LineActivation.sch"
USING "Ambient_Speech.sch"
USING "Minigame_private.sch"
USING "flow_public_core_override.sch"
USING "savegame_public.sch"

FUNC BOOL IS_VEHICLE_MODEL_ON_BLACKLIST(MODEL_NAMES targetVehicleModel)

    INT iTemp
    MODEL_NAMES mBlacklisted[92]
	
    mBlacklisted[0] =       AMBULANCE 
    mBlacklisted[1] =       BENSON 
    mBlacklisted[2] =       BIFF 
    mBlacklisted[3] =       BUS 
    mBlacklisted[4] =       FIRETRUK
    mBlacklisted[5] =      	FORKLIFT 
    mBlacklisted[6] =      	MULE
	mBlacklisted[7] =      	MULE2
    mBlacklisted[8] =      	PACKER 
    mBlacklisted[9] =      	PHANTOM
    mBlacklisted[10] =      POLICE 
    mBlacklisted[11] =      STOCKADE
    mBlacklisted[12] =      SQUALO
    mBlacklisted[13] =      MAVERICK 
    mBlacklisted[14] =      POLMAV
    mBlacklisted[15] =      AIRTUG
    mBlacklisted[16] =      FBI
    mBlacklisted[17] =      ANNIHILATOR
    mBlacklisted[18] =      DINGHY
    mBlacklisted[19] =      POLICE
    mBlacklisted[20] =      RIPLEY
    mBlacklisted[21] =      TRASH
    mBlacklisted[22] =      BURRITO
    mBlacklisted[23] =      PONY
    mBlacklisted[24] =      SPEEDO
    mBlacklisted[25] =      MARQUIS
    mBlacklisted[26] =      SANCHEZ
 	mBlacklisted[27] =      AIRTUG
	mBlacklisted[28] =      TACO
	mBlacklisted[29] =      BARRACKS
	mBlacklisted[30] = 		ROMERO
	mBlacklisted[31] = 		BLAZER	
	mBlacklisted[32] = 		BLAZER2
	mBlacklisted[33] = 		DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[34] = 		BODHI2
	mBlacklisted[35] = 		BOXVILLE2
	mBlacklisted[36] = 		BULLDOZER
	mBlacklisted[37] = 		CADDY
	mBlacklisted[38] = 		CADDY2
	mBlacklisted[39] = 		DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[40] = 		CAMPER
	mBlacklisted[41] = 		DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[42] =      TIPTRUCK
	mBlacklisted[43] =      TOURBUS
	mBlacklisted[44] =      TOWTRUCK
	mBlacklisted[45] =      TOWTRUCK2
	mBlacklisted[46] =      TRACTOR
	mBlacklisted[47] =      TRACTOR2
	mBlacklisted[48] =      UTILLITRUCK
	mBlacklisted[49] =      UTILLITRUCK2
	mBlacklisted[50] =      UTILLITRUCK3
	mBlacklisted[51] = 		BFINJECTION
	mBlacklisted[52] = 		DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[53] = 		DLOADER
	mBlacklisted[54] = 		DOCKTUG
	mBlacklisted[55] = 		DUMP
	mBlacklisted[56] =      GBURRITO
	mBlacklisted[57] =      HANDLER
	mBlacklisted[58] =      HAULER
	mBlacklisted[59] =      JOURNEY
	mBlacklisted[60] =      LGUARD
	mBlacklisted[61] =      DUMMY_MODEL_FOR_SCRIPT//LOADER //TAXI2 TAXI2 is now redundant. Removed so game compiles. KW 14/10/2011
	mBlacklisted[62] =      DUMMY_MODEL_FOR_SCRIPT//LOADER
	mBlacklisted[63] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[64] =      MIXER
	mBlacklisted[65] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[66] =      RHINO
	mBlacklisted[67] =      CUTTER
	mBlacklisted[68] =      POUNDER
	mBlacklisted[69] =      TIPTRUCK2
	mBlacklisted[70] =      MIXER2
	mBlacklisted[71] =      TIPTRUCK2
	mBlacklisted[72] =      RUBBLE
	mBlacklisted[73] =      SCRAP
	mBlacklisted[74] =      ARMYTANKER
	mBlacklisted[75] =      BARRACKS2
	mBlacklisted[76] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[77] =      AIRBUS
	mBlacklisted[78] =      COACH
	mBlacklisted[79] =      PBUS
	mBlacklisted[80] =      RIOT
	mBlacklisted[81] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[82] =      STOCKADE3
	mBlacklisted[83] =      FLATBED
	mBlacklisted[84] =      BOXVILLE
	mBlacklisted[85] =      BURRITO2
	mBlacklisted[86] =      BURRITO3
	mBlacklisted[87] =      BURRITO4
	mBlacklisted[88] =      RUMPO
	mBlacklisted[89] =      SPEEDO2
	mBlacklisted[90] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[91] =      DUMMY_MODEL_FOR_SCRIPT
	
    REPEAT COUNT_OF(mBlacklisted) iTemp
        IF targetVehicleModel = mBlacklisted[iTemp]
            RETURN TRUE
        ENDIF
    ENDREPEAT
	
	// just in case we've missed anything
	IF IS_MODEL_POLICE_VEHICLE(targetVehicleModel)
	OR IS_THIS_MODEL_A_BIKE(targetVehicleModel)
	OR IS_THIS_MODEL_A_BOAT(targetVehicleModel)
	OR IS_THIS_MODEL_A_HELI(targetVehicleModel)
	OR IS_THIS_MODEL_A_PLANE(targetVehicleModel)
		RETURN TRUE
	ENDIF
	
    RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_VEHICLE_MODEL_ON_REPAIR_BLACKLIST(MODEL_NAMES targetVehicleModel)

    INT iTemp
    MODEL_NAMES mBlacklisted[96]
	
    mBlacklisted[0] =       AMBULANCE 
    mBlacklisted[1] =       BENSON 
    mBlacklisted[2] =       BIFF 
    mBlacklisted[3] =       BUS 
    mBlacklisted[4] =       FIRETRUK
    mBlacklisted[5] =      	FORKLIFT 
    mBlacklisted[6] =      	MULE
	mBlacklisted[7] =      	MULE2
    mBlacklisted[8] =      	PACKER 
    mBlacklisted[9] =      	PHANTOM
    mBlacklisted[10] =      POLICE 
    mBlacklisted[11] =      STOCKADE
    mBlacklisted[12] =      SQUALO
    mBlacklisted[13] =      MAVERICK 
    mBlacklisted[14] =      POLMAV
    mBlacklisted[15] =      AIRTUG
    mBlacklisted[16] =      FBI
    mBlacklisted[17] =      ANNIHILATOR
    mBlacklisted[18] =      DINGHY
    mBlacklisted[19] =      POLICE
    mBlacklisted[20] =      RIPLEY
    mBlacklisted[21] =      TRASH
    mBlacklisted[22] =      BURRITO
    mBlacklisted[23] =      PONY
    mBlacklisted[24] =      SPEEDO
    mBlacklisted[25] =      MARQUIS
    mBlacklisted[26] =      SANCHEZ
 	mBlacklisted[27] =      AIRTUG
	mBlacklisted[28] =      TACO
	mBlacklisted[29] =      BARRACKS
	mBlacklisted[30] = 		ROMERO
	mBlacklisted[31] = 		BLAZER	
	mBlacklisted[32] = 		BLAZER2
	mBlacklisted[33] = 		VACCA
	mBlacklisted[34] = 		BODHI2
	mBlacklisted[35] = 		BOXVILLE2
	mBlacklisted[36] = 		BULLDOZER
	mBlacklisted[37] = 		CADDY
	mBlacklisted[38] = 		CADDY2
	mBlacklisted[39] = 		MONROE
	mBlacklisted[40] = 		CAMPER
	mBlacklisted[41] = 		ADDER
	mBlacklisted[42] =      TIPTRUCK
	mBlacklisted[43] =      TOURBUS
	mBlacklisted[44] =      TOWTRUCK
	mBlacklisted[45] =      TOWTRUCK2
	mBlacklisted[46] =      TRACTOR
	mBlacklisted[47] =      TRACTOR2
	mBlacklisted[48] =      UTILLITRUCK
	mBlacklisted[49] =      UTILLITRUCK2
	mBlacklisted[50] =      UTILLITRUCK3
	mBlacklisted[51] = 		BFINJECTION
	mBlacklisted[52] = 		DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[53] = 		DLOADER
	mBlacklisted[54] = 		DOCKTUG
	mBlacklisted[55] = 		DUMP
	mBlacklisted[56] =      GBURRITO
	mBlacklisted[57] =      HANDLER
	mBlacklisted[58] =      HAULER
	mBlacklisted[59] =      JOURNEY
	mBlacklisted[60] =      LGUARD
	mBlacklisted[61] =      cheetah
	mBlacklisted[62] =      COMET2
	mBlacklisted[63] =      ENTITYXF
	mBlacklisted[64] =      MIXER
	mBlacklisted[65] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[66] =      RHINO
	mBlacklisted[67] =      CUTTER
	mBlacklisted[68] =      POUNDER
	mBlacklisted[69] =      TIPTRUCK2
	mBlacklisted[70] =      MIXER2
	mBlacklisted[71] =      TIPTRUCK2
	mBlacklisted[72] =      RUBBLE
	mBlacklisted[73] =      SCRAP
	mBlacklisted[74] =      ARMYTANKER
	mBlacklisted[75] =      BARRACKS2
	mBlacklisted[76] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[77] =      AIRBUS
	mBlacklisted[78] =      COACH
	mBlacklisted[79] =      PBUS
	mBlacklisted[80] =      RIOT
	mBlacklisted[81] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[82] =      STOCKADE3
	mBlacklisted[83] =      FLATBED
	mBlacklisted[84] =      BOXVILLE
	mBlacklisted[85] =      BURRITO2
	mBlacklisted[86] =      BURRITO3
	mBlacklisted[87] =      BURRITO4
	mBlacklisted[88] =      RUMPO
	mBlacklisted[89] =      SPEEDO2
	mBlacklisted[90] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[91] =      DUMMY_MODEL_FOR_SCRIPT
	mBlacklisted[92] =      BULLET
	mBlacklisted[93] =      NINEF
	mBlacklisted[94] =      NINEF2
	mBlacklisted[95] =      VOLTIC
	
    REPEAT COUNT_OF(mBlacklisted) iTemp
        IF targetVehicleModel = mBlacklisted[iTemp]
            RETURN TRUE
        ENDIF
    ENDREPEAT
	
	// just in case we've missed anything
	IF IS_MODEL_POLICE_VEHICLE(targetVehicleModel)
	OR IS_THIS_MODEL_A_BIKE(targetVehicleModel)
	OR IS_THIS_MODEL_A_BOAT(targetVehicleModel)
	OR IS_THIS_MODEL_A_HELI(targetVehicleModel)
	OR IS_THIS_MODEL_A_PLANE(targetVehicleModel)
		RETURN TRUE
	ENDIF
		
    RETURN FALSE

ENDFUNC

FUNC INT GET_NEAREST_PASSENGER_SIDE_TO_PED(PED_INDEX ped, VEHICLE_INDEX ThisCar)
	
	VECTOR char_pos
	VECTOR right_pos
	VECTOR left_pos
	VECTOR right_vec
	VECTOR left_vec
	CONST_INT LEFT_PASSENGER 1
	CONST_INT RIGHT_PASSENGER 2
	
	IF IS_VEHICLE_DRIVEABLE(ThisCar)
		
		// get player coords
		IF NOT IS_PED_INJURED(ped)
			char_pos = GET_ENTITY_COORDS(ped)
		ELSE
			RETURN(-1)
		ENDIF

		// right side
		right_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<1.0, -0.5, 0.0>>)
		right_vec = right_pos - char_pos

		// left side
		left_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ThisCar, <<-1.0, -0.5, 0.0>>)
		left_vec = left_pos - char_pos 

		// choose the smaller of the two
		IF VMAG(right_vec)  < VMAG(left_vec)
			RETURN(RIGHT_PASSENGER)
		ELSE
			RETURN(LEFT_PASSENGER)
		ENDIF

	ELSE
		RETURN(-1)
	ENDIF

ENDFUNC

FUNC BOOL IS_VEHICLE_HEADING_ACCEPTABLE(VEHICLE_INDEX vehTemp, FLOAT fIdealHeading, FLOAT fLeeway)
	FLOAT fCarHeading
	FLOAT fUpperH
	FLOAT fLowerH
	IF IS_VEHICLE_DRIVEABLE(vehTemp)
		fCarHeading = GET_ENTITY_HEADING(vehTemp)
		fLowerH = fIdealHeading - fLeeway
		IF fLowerH < 0.0
			fLowerH += 360.0
		ENDIF
		fUpperH = fIdealHeading + fLeeway
		IF fUpperH >= 360.0
			fUpperH -= 360.0
		ENDIF
		IF fUpperH > fLowerH
			IF fCarHeading < fUpperH
			AND fCarHeading > fLowerH
				RETURN TRUE
			ENDIF
		ELSE
			IF fCarHeading < fUpperH
			OR fCarHeading > fLowerH
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SCALE_CUTSCENE_TRIGGERS_BASED_ON_PLAYER_SPEED(VECTOR vCutPos, FLOAT &fLocateSize, FLOAT &fStoppingDistance)
	BOOL bFlag = FALSE
	VECTOR vTemp
	FLOAT fTemp
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID()) - vCutPos
		fTemp = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
		IF IS_VEHICLE_HEADING_ACCEPTABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fTemp, 15)
			bFlag = TRUE
		ENDIF
		vTemp = vCutPos - GET_ENTITY_COORDS(PLAYER_PED_ID())
		fTemp = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
		IF IS_VEHICLE_HEADING_ACCEPTABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fTemp, 15)
			bFlag = TRUE
		ENDIF
	ENDIF
	IF bFlag
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 30
			fLocateSize = LOCATE_SIZE_ANY_MEANS*4
		ELIF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 20
			fLocateSize = LOCATE_SIZE_ANY_MEANS*3	
		ELIF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 10
			fLocateSize = LOCATE_SIZE_ANY_MEANS*2
		ELSE
			fLocateSize = LOCATE_SIZE_ANY_MEANS
		ENDIF
		fStoppingDistance = (fLocateSize/1.33)-1
	ELSE
		fLocateSize = LOCATE_SIZE_ANY_MEANS
		fStoppingDistance = (fLocateSize/1.33)-1
	ENDIF
ENDPROC


/// PURPOSE:
///    Loads and plays the ambient event pass music for the current player character enum.
/// RETURNS:
///    TRUE when the stream has been played.
FUNC BOOL LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Autosaves after RE completion
/// RETURNS:
///    
PROC RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	MAKE_AUTOSAVE_REQUEST()
ENDPROC

PROC DEACTIVATE_VENDING_MACHINES_THIS_FRAME()
	g_bVendingMachinesActive = FALSE
ENDPROC


	









