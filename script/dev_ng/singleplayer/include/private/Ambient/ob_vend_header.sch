// Chris McMahon

// Includes -----------------------------------------------//
USING "commands_misc.sch"
USING "commands_ped.sch"
USING "commands_vehicle.sch"
USING "commands_object.sch"
USING "commands_hud.sch"
USING "commands_player.sch"
USING "commands_task.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_pad.sch"
USING "commands_debug.sch"
USING "commands_path.sch"
USING "types.sch"
USING "commands_camera.sch"
USING "commands_brains.sch"
USING "brains.sch"
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_interiors.sch"
USING "script_player.sch"

USING "model_enums.sch"

USING "LineActivation.sch"
USING "finance_control_public.sch"
USING "player_ped_public.sch"
USING "cutscene_public.sch"
USING "shop_public.sch"
USING "context_control_public.sch"
USING "ambient_common.sch"
USING "shop_private.sch"
USING "freemode_header.sch"
 
// Variables ----------------------------------------------//
ENUM ambStageFlag
	ambCanRun,
	ambSpecial,
	ambRunning,
	ambEnd
ENDENUM

FUNC STRING GET_STRING_AMB_STAGE_FLAG(ambStageFlag stage)
	SWITCH stage
		CASE ambCanRun  RETURN "ambCanRun"
		CASE ambSpecial RETURN "ambSpecial"
		CASE ambRunning RETURN "ambRunning"
		CASE ambEnd		RETURN "ambEnd"
	ENDSWITCH
	RETURN ""
ENDFUNC

ambStageFlag ambStage = ambCanRun

PROC SET_AMB_STAGE_FLAG(ambStageFlag stage)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_STAGE: from ", GET_STRING_AMB_STAGE_FLAG(ambStage), " to ", GET_STRING_AMB_STAGE_FLAG(stage))
	ambStage = stage
ENDPROC

ENUM vendTypeFlag
	VEND_NONE=0,
	
	VEND_ECOLA,
	VEND_SPRUNK,
	VEND_COFFEE,
	VEND_CONDOM,
	VEND_FAGS,
	VEND_SNACK,
	VEND_WATER,
	VEND_JUNK,
	
	VEND_END
ENDENUM
vendTypeFlag vendType = VEND_NONE

ENUM vendStageFlag
	playerOutOfRange,
	waitForPlayer,
	grabPlayer,
	waitForHelmet,
	runVend,
	resetVend
ENDENUM

FUNC STRING GET_STRING_VEND_STAGE_FLAG(vendStageFlag stage)
	SWITCH stage
		CASE playerOutOfRange 	RETURN "playerOutOfRange"
		CASE waitForPlayer 		RETURN "waitForPlayer"
		CASE grabPlayer 		RETURN "grabPlayer"
		CASE waitForHelmet		RETURN "waitForHelmet"
		CASE runVend			RETURN "runVend"
		CASE resetVend			RETURN "resetVend"
	ENDSWITCH
	RETURN ""
ENDFUNC

vendStageFlag vendStage = playerOutOfRange

PROC SET_VEND_STAGE_FLAG(vendStageFlag stage)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_STAGE: from ", GET_STRING_VEND_STAGE_FLAG(vendStage), " to ", GET_STRING_VEND_STAGE_FLAG(stage))
	vendStage = stage
ENDPROC


STRING sAnimDict = "NULL"

BOOL bTurnedPlayerControlOff, bCashPaid, bSaveHasCoupon = FALSE//, bQueuedHelp, bPrintedHelp
WEAPON_TYPE storedWeapon
OBJECT_INDEX objCan
//VECTOR vOffset = <<0.06,0.01,-0.02>>, vRotation = <<-90,0,0>>, 
VECTOR vImpulse = <<6,10,2>>
//3rd Person	FLOAT fCanDropTime = 0.306, fCanCreateTime = 0.31, fTransitionStartPhase = 0.985
//1st person 	FLOAT fCanDropTime = 0.306, fCanCreateTime = 0.31, fTransitionStartPhase = 0.980
FLOAT fCanDropTime = 0.306, fCanCreateTime = 0.31, fTransitionStartPhase = 0.980
PED_COMP_NAME_ENUM eRemovedMaskBerd = DUMMY_PED_COMP
BOOL bSwappedChemSuit = FALSE


INT iPurchaseCounter, iContextButtonIntention = NEW_CONTEXT_INTENTION//, iTransitionTimer, iTransitionDuration = 200 //, iControlCounter
VECTOR vBuyGotoPos, vBuyGotoOffset
VECTOR vBuyLocatorPos, vBuyLocatorSize, vBuyLocatorOffset

SCRIPT_TIMER timeoutTimer

MODEL_NAMES vendModel
MODEL_NAMES canModel

CONST_INT iBS_STORE_ACTIVE					0
CONST_INT iBS_HELMET_REMOVED				1

CONST_INT iBS_ActionOn 						0

INT iBoolsBitSet
INT iActionModeBitSet

BOOL bStartedInMP

BOOL bProcessingBasket = FALSE
INT iProcessingBasketStage = 0

#IF IS_DEBUG_BUILD
BOOL bUpdateOffsets
#ENDIF

// Functions ----------------------------------------------//
FUNC BOOL assetsAreLoaded(vendTypeFlag machineType)
	// Do some loading in here...
	//sAnimDict = "MINI@SPRUNK"
	//PED_BUY_DRINK
	//PLYR_BUY_DRINK
	SWITCH machineType
		CASE VEND_ECOLA
			IF ARE_STRINGS_EQUAL(sAnimDict, "NULL")
				//3rd Person	//sAnimDict = "MINI@SPRUNK"
				//1st Person 	//sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
				sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
			ENDIF
		BREAK
		CASE VEND_SPRUNK
			IF ARE_STRINGS_EQUAL(sAnimDict, "NULL")
				//3rd Person	//sAnimDict = "MINI@SPRUNK"
				//1st Person 	//sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
				sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
			ENDIF
		BREAK
		CASE VEND_COFFEE
		
		BREAK
		CASE VEND_CONDOM
		
		BREAK
		CASE VEND_FAGS
		
		BREAK
		CASE VEND_SNACK
		
		BREAK
		CASE VEND_WATER
		
		BREAK
	ENDSWITCH
	PRINTSTRING("ob_vend - Loading vending anims \n")
	//3rd Person	//sAnimDict = "MINI@SPRUNK"
	//1st Person 	//sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
	sAnimDict = "MINI@SPRUNK@FIRST_PERSON"
	REQUEST_ANIM_DICT(sAnimDict)
	IF HAS_ANIM_DICT_LOADED(sAnimDict)
		HINT_AMBIENT_AUDIO_BANK("VENDING_MACHINE")
		PRINTSTRING("ob_vend -- Loaded vending anims \n")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Do necessary pre game start ini
PROC PROCESS_PRE_GAME()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		bStartedInMP = TRUE
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		PRINTSTRING("Ob_vend: set started in MP game")
		PRINTNL()
	ENDIF
ENDPROC

PROC givePlayerHealth()
	// Should be a percentage of max health for consistency, awaiting GET_PED_MAX_HEALTH command
	// -100 as 100 for whatever reason is dead
//	INT addedHealth = ROUND((GET_PED_MAX_HEALTH(PLAYER_PED_ID())-100)*0.1) //(10%)
//	IF GET_ENTITY_HEALTH(PLAYER_PED_ID()) < (GET_PED_MAX_HEALTH(PLAYER_PED_ID())-addedHealth)
//		SET_ENTITY_HEALTH(PLAYER_PED_ID(), (GET_ENTITY_HEALTH(PLAYER_PED_ID()) +addedHealth)) 
//	ELSE
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID())) // #1339330 - Set player health to max health
//	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "Ob_vend: gave player health")
	iPurchaseCounter++
ENDPROC

PROC registerPurchaseWithStockmarket(vendTypeFlag machineType)
	SWITCH machineType
		CASE VEND_ECOLA
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_ECL)
		BREAK
		CASE VEND_SPRUNK
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_BEN)
		BREAK
		CASE VEND_COFFEE
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_BEN)
		BREAK
		CASE VEND_CONDOM
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_GLY)
		BREAK
		CASE VEND_FAGS
			
		BREAK
		CASE VEND_SNACK
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_CDY)
		BREAK
		CASE VEND_WATER
			////BAWSAQ_INCREMENT_MODIFIER(BSMF_VENDUSED_FOR_WTR)
		BREAK
		CASE VEND_JUNK
			
		BREAK
	ENDSWITCH
ENDPROC

PROC DISABLE_ADVANCED_MOVEMENT()
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: IS_PED_USING_ACTION_MODE: ", IS_PED_USING_ACTION_MODE(PLAYER_PED_ID()))
	#ENDIF
	
	SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: DISABLE_ADVANCED_MOVEMENT: SET_PED_USING_ACTION_MODE")
	#ENDIF
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: IS_PED_USING_ACTION_MODE: AfterSet: ", IS_PED_USING_ACTION_MODE(PLAYER_PED_ID()))
	#ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, true)
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontWalkRoundObjects , true)
	ENDIF
	
	DISABLE_SELECTOR_THIS_FRAME()
ENDPROC

PROC CLEANUP_CAN(BOOL addImpulse = TRUE)
	DISABLE_ADVANCED_MOVEMENT()
	IF DOES_ENTITY_EXIST(objCan)
		IF IS_ENTITY_ATTACHED(objCan)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Ob_vend: CLEANUP_CAN() Detached Can")
			DETACH_ENTITY(objCan)
			IF addImpulse
				APPLY_FORCE_TO_ENTITY(objCan, APPLY_TYPE_IMPULSE, vImpulse, << 0, 0, 0 >>, 0, TRUE, TRUE, FALSE)
			ENDIF
		ENDIF
		IF g_bInMultiplayer
			NETWORK_SET_NO_LONGER_NEEDED(objCan,FALSE)
		ENDIF
		SET_OBJECT_AS_NO_LONGER_NEEDED(objCan)
	ENDIF
	bCashPaid = FALSE
	bSaveHasCoupon = FALSE
//	iTransitionTimer = 0
ENDPROC

FUNC BOOL DOES_PLAYER_HAVE_ENOUGH_MONEY()
	IF g_bInMultiplayer
		IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) > 0 //The logic calling this function already corrects for SCRIPT_MAX_INT32, so GET_LOCAL_PLAYER_VC_AMOUNT doesn't need fixing here.
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_VENDING_MACHINE_ITEM_HASH()
	SWITCH vendType
		CASE VEND_ECOLA		RETURN HASH("VEND_ECOLA")	BREAK
		CASE VEND_SPRUNK	RETURN HASH("VEND_SPRUNK")	BREAK
		CASE VEND_COFFEE	RETURN HASH("VEND_COFFEE")	BREAK
		CASE VEND_CONDOM	RETURN HASH("VEND_CONDOM")	BREAK
		CASE VEND_FAGS		RETURN HASH("VEND_FAGS")	BREAK
		CASE VEND_SNACK		RETURN HASH("VEND_SNACK")	BREAK
		CASE VEND_WATER		RETURN HASH("VEND_WATER")	BREAK
		CASE VEND_JUNK		RETURN HASH("VEND_JUNK")	BREAK
	ENDSWITCH
	
	//If nothing for the above then return the original value setup for this script
	RETURN HASH("soda")
ENDFUNC


FUNC INT GET_VENDING_MACHINE_ITEM_HASH_FOR_TELEMETRY()
	SWITCH vendType
		CASE VEND_ECOLA		RETURN HASH("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")	BREAK
		CASE VEND_SPRUNK	RETURN HASH("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0")	BREAK
		CASE VEND_COFFEE	RETURN HASH("VEND_COFFEE")	BREAK
		CASE VEND_CONDOM	RETURN HASH("VEND_CONDOM")	BREAK
		CASE VEND_FAGS		RETURN HASH("VEND_FAGS")	BREAK
		CASE VEND_SNACK		RETURN HASH("VEND_SNACK")	BREAK
		CASE VEND_WATER		RETURN HASH("VEND_WATER")	BREAK
		CASE VEND_JUNK		RETURN HASH("VEND_JUNK")	BREAK
	ENDSWITCH
	
	//If nothing for the above then return the original value setup for this script
	RETURN HASH("soda")
ENDFUNC

PROC TAKE_MONEY_FROM_PLAYER(INT iVendCost)

	BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_VENUSE_FOR_SPU)
	
	BOOL bVendPay = TRUE
	BOOL bAllowCoupon = TRUE
	
	#IF FEATURE_DLC_1_2022
	IF g_sMPTunables.bSWAP_VENDING_MACHINE_ECOLA_MODEL
	AND vendType = VEND_ECOLA
		bAllowCoupon = FALSE
	ENDIF
	#ENDIF
	
	IF DOES_SAVE_HAVE_COUPON(COUPON_SPRUNK)
	AND bAllowCoupon	
		REDEEM_COUPON(COUPON_SPRUNK)	//	check something
		bVendPay = FALSE
	ENDIF
	
	IF bVendPay
		IF g_bInMultiplayer
		
			IF USE_SERVER_TRANSACTIONS()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
				NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
			ENDIF
			
			INT iItemHash = GET_VENDING_MACHINE_ITEM_HASH()
			
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(2, GET_VENDING_MACHINE_ITEM_HASH_FOR_TELEMETRY(), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("VENDING_MACHINE"))//, PICK_INT(USE_SERVER_TRANSACTIONS(), GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()), 0))
			
			IF GET_LOCAL_PLAYER_VC_AMOUNT(FALSE) > 0 //The SCRIPT_MAX_INT32 overflow of GET_LOCAL_PLAYER_VC_AMOUNT doesn't need fixing here (see the comment in GET_LOCAL_PLAYER_VC_AMOUNT).
				NETWORK_BUY_ITEM(iVendCost, iItemHash, PURCHASE_FOOD)
			ELSE
				NETWORK_BUY_ITEM(iVendCost, iItemHash, PURCHASE_FOOD,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			ENDIF
		ELSE
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iVendCost)
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND USE_SERVER_TRANSACTIONS()
		DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_TAKING_DAMAGE()
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_OBJECT(PLAYER_PED_ID())
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(PLAYER_PED_ID())
	OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(PLAYER_PED_ID())
	OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SMOKEGRENADE)
		CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC tidyVendingMachine(vendTypeFlag machineType)
	IF IS_BIT_SET(iActionModeBitSet, iBS_ActionOn)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: Turning on Action mode " )
		#ENDIF	
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		CLEAR_BIT(iActionModeBitSet, iBS_ActionOn)
	ENDIF

	IF g_bInMultiplayer
		IF bTurnedPlayerControlOff
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		g_bCurrentlyUsingVendingMachine = FALSE
	ENDIF
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1")
		OR IS_PLAYER_TAKING_DAMAGE()
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	IF g_bInMultiplayer
		IF IS_WEAPON_VALID(storedWeapon)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: 3 Setting storedWeapon: ", storedWeapon)	
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, FALSE)
		ENDIF
		IF eRemovedMaskBerd != DUMMY_PED_COMP
			SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eRemovedMaskBerd, FALSE)
			eRemovedMaskBerd = DUMMY_PED_COMP
		ENDIF
		IF bSwappedChemSuit
			SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
			bSwappedChemSuit = FALSE
		ENDIF
		ENABLE_INTERACTION_MENU()
	ENDIF
	bTurnedPlayerControlOff = FALSE
//	iTransitionTimer = 0
	IF DOES_ENTITY_EXIST(objCan)
		registerPurchaseWithStockmarket(machineType)
		givePlayerHealth()
		CLEANUP_CAN(FALSE)
	ENDIF
	SET_VEND_STAGE_FLAG(playerOutOfRange)
	RESET_NET_TIMER(timeoutTimer)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: tidyVendingMachine ")	
ENDPROC
FUNC BOOL IS_AREA_BLOCKED_BY_AMMUNATION_CHAIR(OBJECT_INDEX oMachine)
	IF GET_ROOM_KEY_FROM_ENTITY(oMachine) = GET_HASH_KEY("V_7_RecAreaRm")
	OR GET_ROOM_KEY_FROM_ENTITY(oMachine) = GET_HASH_KEY("V_7_GunSHopRm")
		IF IS_OBJECT_NEAR_POINT(V_RET_GC_CHAIR02, vBuyGotoPos, 1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_AREA_CLEAR()
	#IF IS_DEBUG_BUILD
	DRAW_DEBUG_SPHERE(vBuyGotoPos,0.5,255,0,0,255)
	#ENDIF
	IF IS_POSITION_OCCUPIED(vBuyGotoPos,0.05,FALSE,TRUE,FALSE,FALSE,FALSE)
	OR IS_POSITION_OCCUPIED(vBuyGotoPos,0.05,FALSE,FALSE,TRUE,FALSE,FALSE,PLAYER_PED_ID(),TRUE)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//CONST_INT SHOP_BASKET_STAGE_ADD 	0
//CONST_INT SHOP_BASKET_STAGE_PENDING 1
//CONST_INT SHOP_BASKET_STAGE_SUCCESS 2
//CONST_INT SHOP_BASKET_STAGE_FAILED 	3

FUNC BOOL PROCESSING_VEND_SHOPPING_BASKET(INT &iProcessSuccess, SHOP_ITEM_CATEGORIES eCategory, ITEM_ACTION_TYPES eAction, INT iPrice, BOOL bDoCoupon)
    IF bProcessingBasket
        SWITCH iProcessingBasketStage
            // Add item to basket
            CASE SHOP_BASKET_STAGE_ADD
				
				IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
					PRINTLN("PROCESSING_VEND_SHOPPING_BASKET - Waiting on GET_BASKET_TRANSACTION_SCRIPT_INDEX. currently != -1")
					RETURN TRUE
				ENDIF
				
				// Override the players cash.
				INT iWalletAmount, iBankAmount, iTempPrice
				iWalletAmount = 0
				iBankAmount = 0
				iTempPrice = iPrice
				IF bDoCoupon
					iTempPrice = 0
				ENDIF
				// Bank first
				IF (NETWORK_GET_VC_BANK_BALANCE() > 0)
					IF NETWORK_GET_VC_BANK_BALANCE() >= iTempPrice
						iBankAmount = iTempPrice
					ELSE
						iBankAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_BANK_BALANCE())
					ENDIF
					iTempPrice -= iBankAmount
				ENDIF
				IF iTempPrice > 0
					IF (NETWORK_GET_VC_WALLET_BALANCE() > 0)
						IF NETWORK_GET_VC_WALLET_BALANCE() >= iTempPrice
							iWalletAmount = iTempPrice
						ELSE
							iWalletAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_WALLET_BALANCE())
						ENDIF
						iTempPrice -= iWalletAmount
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF iTempPrice > 0
					SCRIPT_ASSERT("PROCESSING_VEND_SHOPPING_BASKET - Player can't afford this item!")	// iTempPrice=", iTempPrice, ", iWalletAmount=", iWalletAmount, ", iBankAmount", iBankAmount)
				ENDIF
				#ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - Adding basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iPrice:$", iPrice)
				
				CONST_INT iItemId HASH("MT_VEND_SODA_t0_v0")
//				INT iInventoryKey
//				iInventoryKey = 0	//GET_SNACK_INVENTORY_KEY_FOR_CATALOGUE(GET_SNACK_NAME(iCurrentSelection))
				
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, 1, CATALOG_ITEM_FLAG_WALLET_THEN_BANK)
				
				/*	<Item type="netCatalogOnlyItem" key="PO_COUPON_SPRUNK">
				      <keyhash value="283722020" />" />	*/
					IF bDoCoupon
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, 0, 1, CATALOG_ITEM_FLAG_WALLET_THEN_BANK, HASH("PO_COUPON_SPRUNK"))
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - Successfully added coupon hash ", HASH("PO_COUPON_SPRUNK"), " to basket (iItemId: ", iItemId, ").")
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - Faied to add coupon to basket.")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
                        CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - basket checkout started")
						
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - CHANGE_FAKE_MP_CASH(wallet=$-", iWalletAmount, ", bank=$-", iBankAmount, "): iPrice=$", iPrice, ", bankBalance=$", NETWORK_GET_VC_BANK_BALANCE(), ", walletBalance=$", NETWORK_GET_VC_WALLET_BALANCE())
						USE_FAKE_MP_CASH(TRUE)
						CHANGE_FAKE_MP_CASH(-iWalletAmount, -iBankAmount)
						
						iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
                    ELSE
                        CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - failed to start basket checkout")
                        iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                    ENDIF
                ELSE
                    CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - failed to add item to basket")
                    iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                ENDIF
            BREAK
            // Pending
            CASE SHOP_BASKET_STAGE_PENDING
                IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
                    IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
                        CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - Basket transaction finished, success!")
                        iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
                    ELSE
                        CDEBUG1LN(DEBUG_SAFEHOUSE, "[VEND_BASKET] - Basket transaction finished, failed!")
                        iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
                    ENDIF
                ENDIF
            BREAK
            
            // Success
            CASE SHOP_BASKET_STAGE_SUCCESS
                TAKE_MONEY_FROM_PLAYER(iPrice)
                bProcessingBasket = FALSE
                iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                iProcessSuccess = 2
                RETURN FALSE
            BREAK
            
            //Failed
            CASE SHOP_BASKET_STAGE_FAILED
                DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
                
                bProcessingBasket = FALSE
                iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                iProcessSuccess = 0
                RETURN FALSE
            BREAK
        ENDSWITCH
        
		IS_NET_PLAYER_OK(PLAYER_ID())
		
        RETURN TRUE
    ENDIF
    
    iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
    iProcessSuccess = -1
    RETURN FALSE
ENDFUNC

FUNC INT GET_VENDING_MACHINE_COST(vendTypeFlag machineType)
	SWITCH machineType
		#IF FEATURE_DLC_1_2022
		CASE VEND_ECOLA		RETURN g_sMPTunables.iVendingMachineEColaCanCost	BREAK
		CASE VEND_SPRUNK	RETURN g_sMPTunables.iVendingMachineSprunkCanCost	BREAK
		#ENDIF
		CASE VEND_JUNK		RETURN 1	BREAK
	ENDSWITCH
	
	RETURN 1
ENDFUNC

PROC runVendingMachine(OBJECT_INDEX oMachine, vendTypeFlag machineType)
	INT vendCost = GET_VENDING_MACHINE_COST(machineType)
//	VECTOR locateSize = <<1,1,1>>
	
//	ANIM_DATA none
//	ANIM_DATA animDataBlend
//	animDataBlend.type = APT_SINGLE_ANIM
//	animDataBlend.anim0 = "PLYR_BUY_DRINK_PT3"
//	animDataBlend.dictionary0 = sAnimDict
//	animDataBlend.phase0 = 0.0
//	animDataBlend.rate0 = 1.0
//	animDataBlend.filter = GET_HASH_KEY("BONEMASK_HEAD_NECK_AND_ARMS")
//	animDataBlend.flags = AF_UPPERBODY | AF_SECONDARY 
//	animDataBlend.ikFlags = AIK_DISABLE_HEAD_IK
	
	DRAW_DEBUG_TEXT_2D("runVendingMachine", <<0.02, 0.05, 0>>)
	#IF IS_DEBUG_BUILD
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: vendStage: ", vendStage)
	#ENDIF
	SWITCH vendStage
		CASE playerOutOfRange
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: playerOutOfRange")
				ENDIF
			#ENDIF
			DRAW_DEBUG_TEXT_2D("playerOutOfRange", <<0.2, 0.05, 0>>)
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3")
				IF g_bInMultiplayer
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3") > 0.9
						IF IS_WEAPON_VALID(storedWeapon)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: 1 Setting storedWeapon: ", storedWeapon)	
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, FALSE)
							#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: PLYR_BUY_DRINK_PT3 > 0.9")
							#ENDIF
						ENDIF
						IF eRemovedMaskBerd != DUMMY_PED_COMP
							SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eRemovedMaskBerd, FALSE)
							eRemovedMaskBerd = DUMMY_PED_COMP
						ENDIF
						IF bSwappedChemSuit
							SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
							bSwappedChemSuit = FALSE
						ENDIF
						ENABLE_INTERACTION_MENU()
					ENDIF
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "VEND STILL PLAYING ANIM")
				DISABLE_ADVANCED_MOVEMENT()
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBuyGotoPos, <<3.2,3.2,3.2>>)
						IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(oMachine)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - SET_VEND_STAGE_FLAG(waitForPlayer)")
							bCashPaid = FALSE
							bSaveHasCoupon = FALSE
							SET_VEND_STAGE_FLAG(waitForPlayer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE waitForPlayer
			#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: WaitForPlayer")
			#ENDIF
			
			
			DRAW_DEBUG_TEXT_2D("waitForPlayer", <<0.2, 0.05, 0>>)
			
			IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
				IF NOT g_sShopSettings.bProcessStoreAlert
					CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
					//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "@@@@@@@@@@  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
				ENDIF
			ENDIF
			
			IF CAN_PLAYER_START_CUTSCENE()
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					IF NOT (IS_PLAYER_FREE_AIMING(PLAYER_ID()) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN))
					AND NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
					//AND NOT IS_ANY_OBJECT_NEAR_POINT(vBuyGotoPos, 0.6)
					AND NOT IS_AREA_BLOCKED_BY_AMMUNATION_CHAIR(oMachine)
					AND NOT IS_OBJECT_NEAR_POINT(PROP_BIN_06A, vBuyGotoPos, 1)
					//AND NOT GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
					AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBuyLocatorPos, vBuyLocatorSize)
					AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2")
					AND NOT IS_PLAYER_TAKING_DAMAGE()
					AND g_bVendingMachinesActive
					AND !IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
					AND !IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					AND NOT ARE_SNACKS_BLOCKED()
					AND NOT IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
					AND NOT IS_PLAYER_AN_ANIMAL(PLAYER_ID()) 
//						IF IS_AREA_CLEAR()
							IF DOES_PLAYER_HAVE_ENOUGH_MONEY()
							OR g_bInMultiplayer
							AND NETWORK_CAN_SPEND_MONEY(vendCost, FALSE, FALSE, TRUE)
								
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
								AND NOT IS_PAUSE_MENU_ACTIVE()
								
									IF iContextButtonIntention = NEW_CONTEXT_INTENTION
										REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "VENDHLP")
									ENDIF
									IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
									
										IF g_bInMultiplayer
										AND NOT IS_PAUSE_MENU_ACTIVE()
											
											IF DOES_SAVE_HAVE_COUPON(COUPON_SPRUNK)
											OR bSaveHasCoupon
												IF USE_SERVER_TRANSACTIONS()
													IF NOT bSaveHasCoupon
														CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: don't perform server transaction, save has COUPON_SPRUNK")
													ENDIF
													
													bSaveHasCoupon = TRUE
												ENDIF
											ENDIF
											
											//IF NETWORK_CAN_SPEND_MONEY(1, FALSE, FALSE, TRUE)
												NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_LEAVE_CAMERA_CONTROL_ON)
												//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
												CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: GET_CURRENT_PED_WEAPON: ", GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon))		
												CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: storedWeapon: ", storedWeapon)		
												GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
												SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
												DISABLE_INTERACTION_MENU()
												
//												INT tempComp
//												tempComp = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(),PED_COMP_BERD)
//												CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: tempComp: ", tempComp)
												IF IS_ITEM_A_MASK(GET_PLAYER_MODEL(), COMP_TYPE_BERD, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD))
													eRemovedMaskBerd = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD)
													SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, BERD_FMM_0_0, FALSE)
												ENDIF
												IF IS_PED_WEARING_PILOT_SUIT(PLAYER_PED_ID(), PED_COMP_TEETH) 
													CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
													SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
												ENDIF
												IF IS_PED_WEARING_HAZ_HOOD_UP(PLAYER_PED_ID())
												AND SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
													bSwappedChemSuit = TRUE
												ENDIF
//												PED_COMPONENT eCurrentItem
//												//swap chem suit hood for alternate
//												eCurrentItem = GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_SPECIAL)
////												eCurrentItem = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
//												IF SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
////													sData.eSpecialMask = eCurrentItem
////													SET_PED_COMPONENT_VARIATION()
//													
//												ENDIF
												#IF IS_DEBUG_BUILD
													CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: IS_ITEM_A_HELMET: ", IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))))
													CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: GET_PED_COMP_ITEM_CURRENT_MP: ", GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
												#ENDIF
												IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)))
													REMOVE_PLAYER_HELMET(GET_PLAYER_INDEX(), TRUE)
												ENDIF
//											ELSE
//												CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - NETWORK_CAN_SPEND_MONEY(vendCost, FALSE, FALSE, TRUE) FAILED")	
//											ENDIF
										ELSE
											
											CLEAR_AREA_OF_PROJECTILES(vBuyGotoPos, 3)
											SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
											SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT,FALSE)	//Don't enable blinders for this
											//GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon)
											//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
										ENDIF
										g_bCurrentlyUsingVendingMachine = TRUE // B*1567131 Prevent Chop running off when using a vending machine
										SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
										REQUEST_AMBIENT_AUDIO_BANK("VENDING_MACHINE")
										//SET_ENTITY_COLLISION(oMachine, FALSE)
										SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
										bTurnedPlayerControlOff = TRUE
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), oMachine, 2000, SLF_WHILE_NOT_IN_FOV)
										
										SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontWalkRoundObjects , true)
										TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vBuyGotoPos, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, GET_ENTITY_HEADING(oMachine), 0.1)
										//TASK_PED_SLIDE_TO_COORD_HDG_RATE(PLAYER_PED_ID(), vBuyGotoPos, GET_ENTITY_HEADING(oMachine), 0.6, 270.0)
										
										
										//replaced with net timer command it should work in SP as well.
										START_NET_TIMER(timeoutTimer)
										//iControlCounter = GET_GAME_TIMER()
										CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - SET_VEND_STAGE_FLAG(grabPlayer)")
										RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
										DISABLE_CELLPHONE_THIS_FRAME_ONLY()
										IF NOT IS_BIT_SET(iBoolsBitSet, iBS_HELMET_REMOVED)
											SET_VEND_STAGE_FLAG(grabPlayer)
										ELSE
											SET_VEND_STAGE_FLAG(waitForHelmet)
										ENDIF
									ENDIF
								
								ELSE
									RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - custom menu on screen diabling interaction")
								ENDIF 
								
							ELSE
								IF NOT NETWORK_IS_GAME_IN_PROGRESS()
									RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - NOT DOES_PLAYER_HAVE_ENOUGH_MONEY()<= 0")
									PRINT_HELP("VENDCSH")
								ELSE
									IF iContextButtonIntention = NEW_CONTEXT_INTENTION
										REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MEDIUM_PRIORITY, "VENDHLP")
									ENDIF
									
									IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
										IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
											STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(HASH("soda"), 1, 0)
											LAUNCH_STORE_CASH_ALERT(DEFAULT, DEFAULT, SPL_STORE)
											SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
											//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, FALSE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "@@@@@@@@@@  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CALLED")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
//						ELSE
//							RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
//							//CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - area is occupied cant purchase")
//						ENDIF
					ELSE
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						//CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - SET_VEND_STAGE_FLAG(playerOutOfRange)")
						//SET_VEND_STAGE_FLAG(playerOutOfRange)
					ENDIF
				ELSE
					RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
				//	CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - player not playing")
				ENDIF
			ELSE
				RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - player can't start CS")
			ENDIF
		BREAK
		
		CASE waitForHelmet
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: waitForHelmet")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: IS_PED_WEARING_HELMET: ", IS_PED_WEARING_HELMET(PLAYER_PED_ID()))	
			IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				SET_VEND_STAGE_FLAG(grabPlayer)
				CLEAR_BIT(iBoolsBitSet, iBS_HELMET_REMOVED)
			ENDIF
		BREAK
		CASE grabPlayer
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: GrabPlayer")
			#ENDIF
			
//			#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: IS_PED_USING_ACTION_MODE: ", IS_PED_USING_ACTION_MODE(PLAYER_PED_ID()))
//			#ENDIF
//			IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
//				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
//				SET_BIT(iActionModeBitSet, iBS_ActionOn)
//				#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: Turning off Action mode " )
//				#ENDIF	
//			ENDIF
			
			DRAW_DEBUG_TEXT_2D("grabPlayer", <<0.2, 0.05, 0>>)
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_ADVANCED_MOVEMENT()
			
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBuyGotoPos, << 0.1, 0.1, 0.1 >>)
				// Attach and play
				//SET_ENTITY_COLLISION(oMachine, TRUE)
//				OPEN_SEQUENCE_TASK(seq)
				//	TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_HOLD_LAST_FRAME)
				//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON)//|AF_HOLD_LAST_FRAME)
					
//					TASK_PLAY_ANIM(NULL, sAnimDict, "PLYR_BUY_DRINK_PT2", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_HIDE_WEAPON)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//				CLEAR_SEQUENCE_TASK(seq)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ob_vend - SET_VEND_STAGE_FLAG(runVend)")
				RESET_NET_TIMER(timeoutTimer)
				SET_VEND_STAGE_FLAG(runVend)
			ELSE
				//IF GET_GAME_TIMER() > (iControlCounter+5000)
				IF HAS_NET_TIMER_EXPIRED(timeoutTimer, 2500)
				OR IS_PLAYER_TAKING_DAMAGE()
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					IF g_bInMultiplayer
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: 2 Setting storedWeapon: ", storedWeapon)		
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), storedWeapon, FALSE)
					ENDIF
					IF eRemovedMaskBerd != DUMMY_PED_COMP
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, eRemovedMaskBerd, FALSE)
						eRemovedMaskBerd = DUMMY_PED_COMP
					ENDIF
					IF bSwappedChemSuit
						SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(PLAYER_PED_ID())
						bSwappedChemSuit = FALSE
					ENDIF
					ENABLE_INTERACTION_MENU()
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					g_bCurrentlyUsingVendingMachine = FALSE
					SET_VEND_STAGE_FLAG(playerOutOfRange)
					RESET_NET_TIMER(timeoutTimer)
					
				ENDIF
			ENDIF
		BREAK
		
		CASE runVend
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: RunVend")
			#ENDIF
			
//			#IF IS_DEBUG_BUILD
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: IS_PED_USING_ACTION_MODE: ", IS_PED_USING_ACTION_MODE(PLAYER_PED_ID()))
//			#ENDIF
//			IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
//				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
//				SET_BIT(iActionModeBitSet, iBS_ActionOn)
//				#IF IS_DEBUG_BUILD
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: Turning off Action mode " )
//				#ENDIF	
//			ENDIF
			
			DRAW_DEBUG_TEXT_2D("runVend", <<0.2, 0.05, 0>>)
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_ADVANCED_MOVEMENT()
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1")
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1") < 0.52
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBuyGotoPos, << 0.1, 0.1, 0.1 >>)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "@@@@@@@@@@@ CLEANUP 4 @@@@@@@@@@@")
						CLEANUP_CAN()
						tidyVendingMachine(machineType)
					ENDIF
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1") > fCanCreateTime
						IF DOES_ENTITY_EXIST(objCan)
							IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1") > fTransitionStartPhase
//								IF iTransitionTimer = 0
									//TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2", INSTANT_BLEND_IN , SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_SECONDARY|AF_UPPERBODY)

//3rd Person						//TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2", REALLY_SLOW_BLEND_IN , NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_SECONDARY|AF_UPPERBODY, 0, FALSE, AIK_DISABLE_HEAD_IK)
//3rd Person						//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())

//1st Person						//// The previous logic meant that the PT2 anim was constantly restarting while PT1 blended out. Wrapped play call to prevent that. [MT] Also needed to turn off AF_UPPERBODY only so camera is not messed up.
//1st Person						//IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2")
//1st Person						//	TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2", SLOW_BLEND_IN , NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON, 0, FALSE, AIK_DISABLE_HEAD_IK | AIK_USE_FP_ARM_RIGHT)
//1st Person						//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//1st Person						//ENDIF	
																		
									// The previous logic meant that the PT2 anim was constantly restarting while PT1 blended out. Wrapped play call to prevent that. [MT] Also needed to turn off AF_UPPERBODY only so camera is not messed up.
									IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2")
										TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2", SLOW_BLEND_IN , INSTANT_BLEND_OUT, -1, AF_HIDE_WEAPON, 0, FALSE, AIK_DISABLE_HEAD_IK | AIK_USE_FP_ARM_RIGHT)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									ENDIF
									
//									iTransitionTimer = GET_GAME_TIMER()
//								ELIF GET_GAME_TIMER() > iTransitionTimer + iTransitionDuration
									IF g_bInMultiplayer
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									ELSE
										SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
										SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
									ENDIF
									bTurnedPlayerControlOff = FALSE
									IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1")
										STOP_ANIM_TASK(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1", WALK_BLEND_OUT)
									ENDIF
//								ENDIF
							ENDIF
						ELSE
							IF g_bInMultiplayer
								objCan = CREATE_OBJECT_NO_OFFSET(canModel, vBuyGotoPos, TRUE, FALSE)
								ATTACH_ENTITY_TO_ENTITY(objCan, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>,TRUE,TRUE)//vOffset, vRotation)
							ELSE
								objCan = CREATE_OBJECT_NO_OFFSET(canModel, vBuyGotoPos, FALSE, FALSE)
								ATTACH_ENTITY_TO_ENTITY(objCan, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>,TRUE,TRUE)//vOffset, vRotation)
							ENDIF
						ENDIF
					ELIF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT1") > 0.1
						IF NOT bCashPaid
							IF NETWORK_IS_GAME_IN_PROGRESS()
							AND USE_SERVER_TRANSACTIONS()
						        bProcessingBasket = TRUE
						        iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
						        
						        INT iProcessSuccess
								iProcessSuccess = -1
								WHILE PROCESSING_VEND_SHOPPING_BASKET(iProcessSuccess, CATEGORY_INVENTORY_ITEM, NET_SHOP_ACTION_SPEND, vendCost, bSaveHasCoupon)
						            WAIT(0)
						        ENDWHILE
						        
						        SWITCH iProcessSuccess
						            CASE 0
						                CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: failed to process transaction")
								        EXIT
						            BREAK
						            CASE 2
						                CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: success!!")
						            BREAK
						            DEFAULT
						                CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
						            BREAK
						        ENDSWITCH
						    ELSE
						        CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
								TAKE_MONEY_FROM_PLAYER(vendCost)
						    ENDIF
							
							bCashPaid = TRUE
							
							SWITCH machineType
								CASE VEND_SPRUNK
									BAWSAQ_INCREMENT_MODIFIER(   BSMF_SM_VENUSE_FOR_SPU )
									BREAK
								CASE VEND_WATER
									BAWSAQ_INCREMENT_MODIFIER(   BSMF_SM_VENUSE_FOR_RAI )
									BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT2") > 0.98
//						TASK_SCRIPTED_ANIMATION(PLAYER_PED_ID(), animDataBlend, none, none, SLOW_BLEND_DURATION, SLOW_BLEND_DURATION)

//3rd Person			//TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_UPPERBODY|AF_SECONDARY)
						
//1st Person			//// The previous logic meant that the PT3 anim was constantly restarting while PT2 blended out. Wrapped play call to prevent that. [MT]
//1st Person			//IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3")
//1st Person			//	TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_UPPERBODY|AF_SECONDARY, 0, FALSE, AIK_USE_FP_ARM_RIGHT)
//1st Person			//	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//1st Person			//ENDIF 
						
						// The previous logic meant that the PT3 anim was constantly restarting while PT2 blended out. Wrapped play call to prevent that. [MT]
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3")
							TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3", INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_HIDE_WEAPON|AF_UPPERBODY|AF_SECONDARY, 0, FALSE, AIK_USE_FP_ARM_RIGHT)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						ENDIF
						
						SET_VEND_STAGE_FLAG(resetVend)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP 1")
					tidyVendingMachine(machineType)
				ENDIF
			ENDIF
		BREAK
		
		CASE resetVend
			DRAW_DEBUG_TEXT_2D("resetVend", <<0.2, 0.05, 0>>)
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_ADVANCED_MOVEMENT()
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3")
				//IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("DetachCan"))
				
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), sAnimDict, "PLYR_BUY_DRINK_PT3") > fCanDropTime
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HAS_ANIM_EVENT_FIRED")
					givePlayerHealth()
					g_bCurrentlyUsingVendingMachine = FALSE

					
					IF g_bInMultiplayer
						ENABLE_INTERACTION_MENU()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ELSE
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					ENDIF
					registerPurchaseWithStockmarket(machineType)
					bTurnedPlayerControlOff = FALSE
					SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),FALSE)
					IF REQUEST_AMBIENT_AUDIO_BANK("VENDING_MACHINE")
						RELEASE_AMBIENT_AUDIO_BANK()
					ENDIF
					HINT_AMBIENT_AUDIO_BANK("VENDING_MACHINE")
					CLEANUP_CAN()
					IF iPurchaseCounter > 9
						ambStage = ambEnd
					ELSE
						DISABLE_ADVANCED_MOVEMENT()
						SET_VEND_STAGE_FLAG(playerOutOfRange)
					ENDIF
				ENDIF
			ELSE
				SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),FALSE)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP 2")
				CLEANUP_CAN()
				tidyVendingMachine(machineType)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
PROC SETUP_DEBUG()
	START_WIDGET_GROUP(GET_THIS_SCRIPT_NAME())	
		
		ADD_WIDGET_BOOL("Update Offsets", bUpdateOffsets)
		
		ADD_WIDGET_FLOAT_SLIDER("vBuyGotoOffset.x", vBuyGotoOffset.x, -2, 2, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vBuyGotoOffset.y", vBuyGotoOffset.y, -2, 2, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vBuyGotoOffset.z", vBuyGotoOffset.z, -2, 2, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("vBuyLocatorOffset.x", vBuyLocatorOffset.x, -2, 2, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vBuyLocatorOffset.y", vBuyLocatorOffset.y, -2, 2, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("vBuyLocatorOffset.z", vBuyLocatorOffset.z, -2, 2, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("fCanDropTime", fCanDropTime, 0.3, 0.8, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fCanCreateTime", fCanCreateTime, 0.1, 0.5, 0.01)
		
		ADD_WIDGET_FLOAT_SLIDER("vImpulse.x", vImpulse.x, -100, 100, 1)
		ADD_WIDGET_FLOAT_SLIDER("vImpulse.y", vImpulse.y, -100, 100, 1)
		ADD_WIDGET_FLOAT_SLIDER("vImpulse.z", vImpulse.z, -100, 100, 1)
		
		ADD_WIDGET_FLOAT_SLIDER("fCanDropTime", fTransitionStartPhase, 0.9, 0.99, 0.01)
//		ADD_WIDGET_INT_SLIDER("iTransitionDuration", iTransitionDuration, 100, 500, 10)
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_DEBUG(OBJECT_INDEX oMachine)
	IF bUpdateOffsets
		IF DOES_ENTITY_EXIST(oMachine)
			vBuyGotoPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oMachine, vBuyGotoOffset)
			vBuyLocatorPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oMachine, vBuyLocatorOffset)
		ELSE
			bUpdateOffsets = FALSE
		ENDIF
	ENDIF
ENDPROC
#ENDIF

// Clean Up
PROC missionCleanup()
	IF IS_BIT_SET(iActionModeBitSet, iBS_ActionOn)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: Turning on Action mode " )
		#ENDIF	
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		CLEAR_BIT(iActionModeBitSet, iBS_ActionOn)
	ENDIF
	
	IF bTurnedPlayerControlOff
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF NOT ARE_STRINGS_EQUAL(sAnimDict, "NULL")
		REMOVE_ANIM_DICT(sAnimDict)
		IF canModel != DUMMY_MODEL_FOR_SCRIPT
			SET_MODEL_AS_NO_LONGER_NEEDED(canModel)
		ENDIF
		RELEASE_AMBIENT_AUDIO_BANK()
		RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
	ENDIF
	IF DOES_ENTITY_EXIST(objCan)
	AND IS_ENTITY_A_MISSION_ENTITY(objCan)
		DETACH_ENTITY(objCan)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Ob_vend: Detached Can")
		DELETE_OBJECT(objCan)
	ENDIF
	IF g_bInMultiplayer
		IF g_bCurrentlyUsingVendingMachine
			ENABLE_INTERACTION_MENU()
		ENDIF
	ENDIF
	g_bCurrentlyUsingVendingMachine = FALSE // B*1567131 Prevent Chop running off when using a vending machine
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "Ob_vend: Cleaned up")
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC INITIALISE_VENDING_MACHINE_DATA(OBJECT_INDEX oMachine)

	// Defaults.
	canModel = Prop_LD_Can_01b
	vBuyLocatorSize = <<0.6,0.6,1>>
	vBuyLocatorOffset = <<0.0, -0.97, 0.05>>
	vBuyGotoOffset = <<0.0, -0.97, 0.05>>
	vendModel = GET_ENTITY_MODEL(oMachine)
	
	// Location specifics.
	IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		vBuyLocatorSize = <<0.6, 0.3, 1>>
		vBuyLocatorOffset = <<0.0, -0.650, 0.05>>
	ENDIF
	
	// Prop specifics.
	SWITCH vendModel
		CASE PROP_VEND_SODA_01
			vendType = VEND_ECOLA
			#IF FEATURE_DLC_1_2022
			IF g_sMPTunables.bSWAP_VENDING_MACHINE_ECOLA_MODEL
				canModel = prop_ecola_can
			ENDIF
			#ENDIF
		BREAK
		CASE PROP_VEND_SODA_02
			vendType = VEND_SPRUNK
		BREAK
		CASE PROP_VEND_COFFE_01
			vendType = VEND_COFFEE
		BREAK
		CASE PROP_VEND_CONDOM_01
			vendType = VEND_CONDOM
		BREAK
		CASE PROP_VEND_FAGS_01
			vendType = VEND_FAGS
		BREAK
		CASE PROP_VEND_SNAK_01
			vendType = VEND_SNACK
		BREAK
		CASE PROP_VEND_WATER_01
			vendType = VEND_WATER
		BREAK
		CASE SF_PROP_SF_VEND_DRINK_01a
			vendType = VEND_JUNK
			canModel = SF_PROP_SF_CAN_01A
			vBuyGotoOffset = <<0.0, -0.970, 1.0>>
			vBuyLocatorOffset = <<0.0, -0.650, 0.900>>
		BREAK
	ENDSWITCH
	
	// World positions.
	vBuyLocatorPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oMachine, vBuyLocatorOffset)
	vBuyGotoPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oMachine, vBuyGotoOffset)
ENDPROC

// Mission Script -----------------------------------------//
PROC HANDLE_OBJ_VEN_FUNCTIONALITY(OBJECT_INDEX oVend)

	#IF FEATURE_FREEMODE_ARCADE
		IF IS_FREEMODE_ARCADE()
			missionCleanup()
		ENDIF
	#ENDIF

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP) 
			missionCleanup()
		ENDIF
	ENDIF

	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<ob_vend> - Currently playing as none story character. Terminating...")
		missionCleanup()
	ENDIF
	
	IF DOES_ENTITY_EXIST(oVend)
		SET_ENTITY_PROOFS(oVend, FALSE, TRUE, FALSE, FALSE, FALSE)
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<ob_vend> - Object doesn't exist in init stage. Terminating...")
		missionCleanup()
	ENDIF
	
	// Initialise the vending machine and prop specifcs.
	INITIALISE_VENDING_MACHINE_DATA(oVend)
	//BREAK_ON_NATIVE_COMMAND("IS_PLAYER_READY_FOR_CUTSCENE", FALSE)

	IF vendType = VEND_NONE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "<ob_vend> - This is not a vending machine. Terminating...")
		missionCleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
	SETUP_DEBUG()
	#ENDIF
	PROCESS_PRE_GAME()
	
	// Mission Loop -------------------------------------------//
	WHILE TRUE
		WAIT(0)
	
//	#IF IS_DEBUG_BUILD
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: IS_PED_USING_ACTION_MODE: ", IS_PED_USING_ACTION_MODE(PLAYER_PED_ID()))
//	#ENDIF
//	IF IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
//		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
//		SET_BIT(iActionModeBitSet, iBS_ActionOn)
//		#IF IS_DEBUG_BUILD
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "Vend: VENDING: Turning off Action mode " )
//		#ENDIF	
//	ENDIF

		#IF IS_DEBUG_BUILD
		MAINTAIN_DEBUG(oVend)
		#ENDIF
		
		IF bStartedInMP
			// If we have a match end event, bail.
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				PRINTSTRING("Ob_Vend: Ending script SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()")
				PRINTNL()
				missionCleanup()
			ENDIF
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				PRINTSTRING("Ob_Vend: Ending script not NOT NETWORK_IS_GAME_IN_PROGRESS()")
				PRINTNL()
				missionCleanup()
			ENDIF
		ENDIF
		IF g_bIsOnRampage		//@RJP - 1358453 - preventing player from using vending machines during Rampages
			PRINTSTRING("Ob_Vend: Ending script g_bIsOnRampage")
			PRINTNL()
			missionCleanup()
		ENDIF
		IF DOES_ENTITY_EXIST(oVend)
			BOOL bRunMainState
			
			IF IS_ENTITY_A_MISSION_ENTITY(oVend)
				bRunMainState = TRUE // If entity is created by script, ignore brain activation and proceed.
			ELSE
				IF IS_OBJECT_WITHIN_BRAIN_ACTIVATION_RANGE(oVend)
					bRunMainState = TRUE
				ENDIF
			ENDIF
			
			IF bRunMainState
			AND NOT IS_ENTITY_DEAD(oVend)
				IF GET_ENTITY_HEALTH(oVend) < 875
					SET_ENTITY_HEALTH(oVend, 0)
				ENDIF
				SWITCH ambStage
					CASE ambCanRun
						
						IF assetsAreLoaded(vendType)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_ENTITY_LAST_DAMAGE_ENTITY(PLAYER_PED_ID())
							ENDIF
							ambStage = (ambRunning)
						ENDIF
						
						
					BREAK
					CASE ambRunning
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						AND NOT IS_PLAYER_TAKING_DAMAGE()
							runVendingMachine(oVend, vendType)
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEANUP 3")
							CLEANUP_CAN()
							tidyVendingMachine(vendType)
						ENDIF
					BREAK
					CASE ambEnd
						PRINT_HELP("VENDEMP")
						SET_ENTITY_HEALTH(oVend, 0)
					BREAK
				ENDSWITCH
			ELSE
				missionCleanup()
			ENDIF
		ELSE
			missionCleanup()
		ENDIF
		g_bVendingMachinesActive = TRUE // Always reactivate the machines. 
	ENDWHILE
	
	
ENDPROC
