USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "locates_public.sch"
USING "selector_public.sch"
USING "timer_public.sch"
USING "mission_stat_public.sch"
USING "CompletionPercentage_public.sch"


model_names camera_crew_model = A_M_Y_BevHills_02
model_names video_camera_model = Prop_V_Cam_01
model_names microphone_model = Prop_V_BMike_01


/// PURPOSE: Loads the camera crew assets.
///    NOTE: This function should be called each frame until TRUE is returned
///    		 This is usefull for when the scripter wants to load the assets in advance for starting a cutscene.
///    		 Once all the camera crew assets are loaded plus any cutscene specific assets then the scripter can
///    		 start the cutscene and all they need to do is call create_camera_crew() in the same frame and the 
///    		 camera crew will instantly be created without having to wait for the assets to loaded. 


func bool load_camera_crew_assets()

	request_model(camera_crew_model)
	request_model(video_camera_model)
	request_model(microphone_model)
	request_anim_dict("amb@umbrella@part@idles_moving_a")
	request_anim_dict("amb@cameraman@base")
	
	if has_model_loaded(camera_crew_model)
	and has_model_loaded(video_camera_model)
	and has_model_loaded(microphone_model)
	and has_anim_dict_loaded("amb@umbrella@part@idles_moving_a")
	and has_anim_dict_loaded("amb@cameraman@base")
	
		return true 
		
	endif 

	return false 

endfunc 


/// PURPOSE: Creates a camera crew.
///    NOTE: This function should be called each frame until TRUE is returned.
///    
///    		 camera_man_ped = returned ped_index for the created camera man ped
///    		 camera_man_pos = pos camera man to be created. REMANING 2 PEDS are created at an offset from these coords
///    		 camera_man_heading = heading for camera man
///    		 video_camera_obj = return object_index for the created video camera object
///    		 microphone_man_ped = returned ped_index for the created camera man ped
///    		 microphone_obj = return object_index for the created microphone object
			

func bool create_camera_crew(ped_index &camera_man_ped, vector camera_man_pos, float camera_man_heading, object_index &video_camera_obj, ped_index &microphone_man_ped, object_index microphone_obj, ped_index &reporter_ped)
	
	if load_camera_crew_assets()
		
		if not does_entity_exist(camera_man_ped) and not does_entity_exist(microphone_man_ped)
			
			camera_man_ped = CREATE_PED(pedtype_mission, camera_crew_model, camera_man_pos, camera_man_heading)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_LEG, 0, 1)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(camera_man_ped, PED_COMP_JBIB, 0, 0)
			video_camera_obj = CREATE_OBJECT(video_camera_model, get_offset_from_entity_in_world_coords(camera_man_ped, <<0.0, 0.0, 1.0>>))
			ATTACH_ENTITY_TO_ENTITY(video_camera_obj, camera_man_ped, GET_PED_BONE_INDEX(camera_man_ped, BONETAG_PH_R_HAND), <<-0.020, 0.050, -0.070>>, <<290.000, 0.000, 262.000>>, true, true) 
			TASK_PLAY_ANIM(camera_man_ped, "amb@cameraman@base", "fire", instant_blend_in, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)

			microphone_man_ped = CREATE_PED(pedtype_mission, camera_crew_model, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(camera_man_ped, <<-1.0, 0.2, 0.0>>), get_entity_heading(camera_man_ped) - 20.00)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_HEAD, 1, 1)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_LEG, 0, 2)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(microphone_man_ped, PED_COMP_JBIB, 0, 0)
			TASK_PLAY_ANIM(microphone_man_ped, "amb@umbrella@part@idles_moving_a", "idle_a_mov", instant_blend_in, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
			microphone_obj = CREATE_OBJECT(microphone_model, get_offset_from_entity_in_world_coords(microphone_man_ped, <<0.0, 0.0, 1.0>>))
			ATTACH_ENTITY_TO_ENTITY(microphone_obj, microphone_man_ped, GET_PED_BONE_INDEX(microphone_man_ped, BONETAG_PH_R_HAND), <<0.5, 0, 0.6>>, <<0, -230, 0>>, true, true)
			
			reporter_ped = CREATE_PED(pedtype_mission, camera_crew_model, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(camera_man_ped, <<-0.5, 2.2, 0.0>>), get_entity_heading(camera_man_ped) + 190.00)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_HEAD, 0, 2)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_HAIR, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_TORSO, 1, 1)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_HAND, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(reporter_ped, PED_COMP_JBIB, 0, 0)
			//task_look_at_entity(reporter_ped, camera_man_Ped, -1)
	
			#IF IS_DEBUG_BUILD
				PRINTLN("CREATE_CAMERA_CREW_PEDS_ON_FOOT")
			#ENDIF
			
			return true 
			
		else 
		
			return true 
		
		endif 
	
	endif 

	return false

endfunc

/// PURPOSE: sets the camera crew assets as no longer needed.
proc set_camera_crew_assets_as_no_longer_needed()

	if has_model_loaded(camera_crew_model)
		set_model_as_no_longer_needed(camera_crew_model)
	endif 
	
	if has_model_loaded(video_camera_model)
		set_model_as_no_longer_needed(video_camera_model)
	endif 
	
	if has_model_loaded(microphone_model)
		set_model_as_no_longer_needed(microphone_model)
	endif
	
	if has_anim_dict_loaded("amb@umbrella@part@idles_moving_a")
		remove_anim_dict("amb@umbrella@part@idles_moving_a")
	endif 
	
	if has_anim_dict_loaded("amb@cameraman@base")
		remove_anim_dict("amb@cameraman@base")
	endif 

endproc 




////pseudocode ai system example
//switch camera_crew_ai_status 
//
//	case create_the_camera_crew
//	
//		if create_camera_crew()
//			camera_crew_ai_status = monitor_camera_crew_ai
//		endif 
//	
//	break 
//	
//	case monitor_camera_crew_ai
//	
//		if camera_man_attacked or microphone_man_attacked 
//			
//			drop_camera 
//			
//			drop microphone
//			
//			crew run away 
//			
//			camera_crew_ai_status = do_nothing
//		endif 
//	
//	break 
//
//endswitch 



