USING "tv_control_public.sch"

#IF IS_DEBUG_BUILD
	USING "family_debug.sch"
#ENDIF

#if USE_CLF_DLC
	USING "player_scene_Assets.sch"
	USING "familycoords_private.sch"
#endif
#if USE_NRM_DLC
	USING "player_scene_Assets.sch"
	USING "familycoords_private.sch"
#ENDIF
///private header for family scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY PRIVATE COORD FUNCTIONS
// *******************************************************************************************

FUNC enumCharacterList PRIVATE_Get_CharList_From_FamilyMember(enumFamilyMember eFamilyMember, MODEL_NAMES &eModelIfNoChar)
	eModelIfNoChar = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON				RETURN CHAR_JIMMY		BREAK
		CASE FM_MICHAEL_DAUGHTER		RETURN CHAR_TRACEY		BREAK
		CASE FM_MICHAEL_WIFE
//			IF TRUE		//if Amandas heels are broken
			RETURN CHAR_AMANDA
//			ELSE
//			eModelIfNoChar = S_F_Y_HOOKER_01	RETURN NO_CHARACTER		
//			ENDIF
			BREAK
		
		CASE FM_FRANKLIN_AUNT			RETURN CHAR_DENISE		BREAK
		CASE FM_FRANKLIN_LAMAR			RETURN CHAR_LAMAR		BREAK
		CASE FM_FRANKLIN_STRETCH		RETURN CHAR_STRETCH		BREAK
		
		CASE FM_TREVOR_0_RON			RETURN CHAR_RON			BREAK
		CASE FM_TREVOR_0_MICHAEL		RETURN CHAR_MICHAEL		BREAK
		CASE FM_TREVOR_0_TREVOR			RETURN CHAR_TREVOR		BREAK
		CASE FM_TREVOR_0_WIFE			RETURN CHAR_PATRICIA	BREAK
		CASE FM_TREVOR_0_MOTHER			eModelIfNoChar = S_F_M_MAID_01		RETURN NO_CHARACTER		BREAK
		
		CASE FM_TREVOR_1_FLOYD			RETURN CHAR_FLOYD		BREAK
		CASE FM_TREVOR_1_WADE			RETURN CHAR_WADE		BREAK
		
		//not a char
		CASE FM_MICHAEL_MEXMAID			eModelIfNoChar = S_F_M_MAID_01		RETURN NO_CHARACTER		BREAK
		CASE FM_MICHAEL_GARDENER		eModelIfNoChar = S_M_M_GARDENER_01	RETURN NO_CHARACTER		BREAK
		
		//
		CASE MAX_FAMILY_MEMBER			RETURN GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()	BREAK
		
	ENDSWITCH
	
	RETURN NO_CHARACTER
ENDFUNC


// *******************************************************************************************
//	FAMILY PUBLIC FUNCTIONS
// *******************************************************************************************

FUNC enumFamilyMember GET_enumFamilyMember_from_ped(PED_INDEX PedIndex)
	MODEL_NAMES 		ePedIndexModel = GET_ENTITY_MODEL(PedIndex)
	
	IF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_JIMMY))			RETURN FM_MICHAEL_SON
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_TRACEY))		RETURN FM_MICHAEL_DAUGHTER
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_AMANDA))		RETURN FM_MICHAEL_WIFE
	ELIF (ePedIndexModel = S_F_M_MAID_01)						RETURN FM_MICHAEL_MEXMAID
	ELIF (ePedIndexModel = S_M_M_GARDENER_01)					RETURN FM_MICHAEL_GARDENER
	
	ELIF (ePedIndexModel = IG_DENISE)							RETURN FM_FRANKLIN_AUNT
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_LAMAR))		RETURN FM_FRANKLIN_LAMAR
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_STRETCH))		RETURN FM_FRANKLIN_STRETCH
	
	ELIF (ePedIndexModel = GET_PLAYER_PED_MODEL(CHAR_MICHAEL))	RETURN FM_TREVOR_0_MICHAEL
	ELIF (ePedIndexModel = GET_PLAYER_PED_MODEL(CHAR_TREVOR))	RETURN FM_TREVOR_0_TREVOR
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_RON))			RETURN FM_TREVOR_0_RON
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_PATRICIA))	RETURN FM_TREVOR_0_WIFE
	
	ELIF (ePedIndexModel = S_F_M_MAID_01)						RETURN FM_TREVOR_0_MOTHER
	
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_FLOYD))		RETURN FM_TREVOR_1_FLOYD
	ELIF (ePedIndexModel = GET_NPC_PED_MODEL(CHAR_WADE))		RETURN FM_TREVOR_1_WADE
	
	ELSE
	
//		#IF IS_DEBUG_BUILD
//		CPRINTLN(DEBUG_FAMILY, "<")
//		CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME())
//		CPRINTLN(DEBUG_FAMILY, "> invalid ePedIndexModel in GET_enumFamilyMember_from_ped[")
//		
//		CPRINTLN(DEBUG_FAMILY, GET_MODEL_NAME_FOR_DEBUG(ePedIndexModel))
//		
//		CPRINTLN(DEBUG_FAMILY, "]")
//		CprintNL()
//		#ENDIF
//		
//		CASSERTLN(DEBUG_FAMILY, "invalid ePedIndexModel in GET_enumFamilyMember_from_ped()")
		RETURN NO_FAMILY_MEMBER
	ENDIF

ENDFUNC

PROC PRIVATE_Set_Additional_Current_Family_Member_Event(enumFamilyMember eAdditionalFamilyMember, enumFamilyEvents eFamilyEvent)
	Update_Previous_Event_For_FamilyMember(eAdditionalFamilyMember)
	g_eCurrentFamilyEvent[eAdditionalFamilyMember] = eFamilyEvent
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "ADDITIONAL: set current family event for ", Get_String_From_FamilyMember(eAdditionalFamilyMember), " to ", Get_String_From_FamilyEvent(eFamilyEvent))
	#ENDIF
ENDPROC
PROC PRIVATE_Set_Current_Family_Member_Event(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	Update_Previous_Event_For_FamilyMember(eFamilyMember)
	g_eCurrentFamilyEvent[eFamilyMember] = eFamilyEvent
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "set current family event for ", Get_String_From_FamilyMember(eFamilyMember), " to ", Get_String_From_FamilyEvent(eFamilyEvent))
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_FAMILY_MIC4_locked_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE FE_M7_FAMILY_finished_breakfast
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M7_FAMILY_finished_pizza
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M7_FAMILY_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE FE_M_SON_Fighting_with_sister_A
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_SON_Fighting_with_sister_B
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_SON_Fighting_with_sister_C
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_SON_Fighting_with_sister_D
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_WIFE_screams_at_mexmaid
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_MEXMAID, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_MEXMAID
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
//				BREAK
//				CASE FM_MICHAEL_SON
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_M_WIFE_screaming_at_son_P3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_SON
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE FE_M_WIFE_screaming_at_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
//		CASE FE_M7_WIFE_shopping_with_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_SON, eFamilyEvent)
//				BREAK
//				CASE FM_MICHAEL_SON
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
		
		CASE FE_M7_WIFE_shopping_with_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_DAUGHTER, eFamilyEvent)
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_MICHAEL_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_STRETCH, eFamilyEvent)
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_LAMAR, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_STRETCH, eFamilyEvent)
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_LAMAR, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_STRETCH, eFamilyEvent)
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_LAMAR, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_STRETCH, eFamilyEvent)
//				BREAK
//				CASE FM_FRANKLIN_STRETCH
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_LAMAR, eFamilyEvent)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_LAMAR
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_STRETCH, eFamilyEvent)
				BREAK
				CASE FM_FRANKLIN_STRETCH
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_FRANKLIN_LAMAR, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_WIFE, eFamilyEvent)
				BREAK
				CASE FM_TREVOR_0_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_TREVOR, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_WIFE, eFamilyEvent)
				BREAK
				CASE FM_TREVOR_0_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_TREVOR, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		/*
		CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_WIFE, eFamilyEvent)
				BREAK
				CASE FM_TREVOR_0_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_TREVOR, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		*/
		
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_MICHAEL, eFamilyEvent)
				BREAK
				CASE FM_TREVOR_0_MICHAEL
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_0_WIFE, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
		
//		CASE FE_T1_FLOYD_with_wade_post_trevor3
//			
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_1_FLOYD
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_1_WADE] = FE_T1_FLOYD_with_wade_post_trevor3
//					
//					#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_FAMILY, "ADDITIONAL: set current family event for ")
//					CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(FM_TREVOR_1_WADE))
//					CPRINTLN(DEBUG_FAMILY, " to ")
//					CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(FE_T1_FLOYD_with_wade_post_trevor3))
//					CprintNL()
//					#ENDIF
//					
//				BREAK
//				CASE FM_TREVOR_1_WADE
//					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_1_FLOYD] = FE_T1_FLOYD_with_wade_post_trevor3
//					
//					#IF IS_DEBUG_BUILD
//					CPRINTLN(DEBUG_FAMILY, "ADDITIONAL: set current family event for ")
//					CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(FM_TREVOR_1_FLOYD))
//					CPRINTLN(DEBUG_FAMILY, " to ")
//					CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(FE_T1_FLOYD_with_wade_post_trevor3))
//					CprintNL()
//					#ENDIF
//					
//				BREAK
//			ENDSWITCH
//			
//		BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_1_WADE, eFamilyEvent)
				BREAK
				CASE FM_TREVOR_1_WADE
					PRIVATE_Set_Additional_Current_Family_Member_Event(FM_TREVOR_1_FLOYD, eFamilyEvent)
				BREAK
			ENDSWITCH
			
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL SAVE_FORCE_ROOM_FOR_ENTITY(ENTITY_INDEX EntityIndex, INTERIOR_INSTANCE_INDEX InteriorInstanceIndex, INT RoomKey)
	IF IS_VALID_INTERIOR(InteriorInstanceIndex)
		IF IS_INTERIOR_READY(InteriorInstanceIndex)
			FORCE_ROOM_FOR_ENTITY(EntityIndex, InteriorInstanceIndex, RoomKey)
			RETURN TRUE
		ELSE
			RETAIN_ENTITY_IN_INTERIOR(EntityIndex, InteriorInstanceIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_ForceRoomForFamilyEntity(ENTITY_INDEX pedIndex, enumFamilyMember eFamilyMember)
	BOOL bForcedRoomForPed
	
	//
	STRING sRoomHashKey = ""
	INTERIOR_INSTANCE_INDEX InteriorInstanceIndex = NULL
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
//		CASE FE_M7_WIFE_shopping_with_son
		CASE FE_M7_WIFE_shopping_with_daughter
			sRoomHashKey = "V_Michael_G_Hall"
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" force sRoomHashKey \"", sRoomHashKey, "\"")
			#ENDIF
			
			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<< -812.6, 179.4,72.2 >>, "v_michael")
			IF SAVE_FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(sRoomHashKey))
				bForcedRoomForPed = TRUE
			ENDIF
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1	FALLTHRU
		CASE FE_M_WIFE_screaming_at_son_P2	FALLTHRU
		CASE FE_M_WIFE_screaming_at_son_P3
			
			IF (eFamilyMember = FM_MICHAEL_SON)
				sRoomHashKey = "V_Michael_1_Hall"
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" force sRoomHashKey \"", sRoomHashKey, "\"")
				#ENDIF
				
				InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<< -812.2079, 181.3398, 71.1530 >>, "v_michael")
				IF SAVE_FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(sRoomHashKey))
					bForcedRoomForPed = TRUE
				ENDIF
				
			ELSE
				
				IF NOT g_bMagDemoActive
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] DEFAULT room ")
					#ENDIF
					
					CLEAR_ROOM_FOR_ENTITY(PedIndex)
				ENDIF
				
				bForcedRoomForPed = FALSE
			ENDIF
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_b	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_C	FALLTHRU
		CASE FE_M_SON_Fighting_with_sister_D
			sRoomHashKey = "V_Michael_1_Hall"
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" force sRoomHashKey \"", sRoomHashKey, "\"")
			#ENDIF
			
			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<< -812.2079, 181.3398, 71.1530 >>, "v_michael")
			IF SAVE_FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(sRoomHashKey))
				bForcedRoomForPed = TRUE
			ENDIF
		BREAK
		CASE FE_M_WIFE_screams_at_mexmaid
			sRoomHashKey = "V_Michael_G_Kitche"
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" force sRoomHashKey \"", sRoomHashKey, "\"")
			#ENDIF
			
			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<<-803.4384, 182.9567, 73.2608>>, "v_michael")
			IF SAVE_FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(sRoomHashKey))
				bForcedRoomForPed = TRUE
			ENDIF
		BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			sRoomHashKey = "V_Michael_G_Kitche"
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] \"", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), "\" force sRoomHashKey \"", sRoomHashKey, "\"")
			#ENDIF
			
			InteriorInstanceIndex = GET_INTERIOR_AT_COORDS_with_type(<<-803.4384, 182.9567, 73.2608>>, "v_michael")
			IF SAVE_FORCE_ROOM_FOR_ENTITY(PedIndex, InteriorInstanceIndex, GET_HASH_KEY(sRoomHashKey))
				bForcedRoomForPed = TRUE
			ENDIF
		BREAK
		
		DEFAULT
			
			IF NOT g_bMagDemoActive
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family member[", Get_String_From_FamilyMember(eFamilyMember), "] no specified room ")
				#ENDIF
				
//				CLEAR_ROOM_FOR_ENTITY(PedIndex)
			ENDIF
			
			bForcedRoomForPed = FALSE
		BREAK
		
	ENDSWITCH
	
	RETURN bForcedRoomForPed
ENDFUNC

PROC PRIVATE_Update_Tv_Globals(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
//	#IF IS_DEBUG_BUILD
//	CPRINTLN(DEBUG_FAMILY, "Force Start Ambient TV as ")
//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
//	CPRINTLN(DEBUG_FAMILY, " event is ")
//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
//	CprintNL()
//	#ENDIF

	SWITCH eFamilyEvent
	
		CASE FE_M_DAUGHTER_dancing_practice
		CASE FE_M_DAUGHTER_workout_with_mp3
//				CASE FE_M_MEXMAID_watching_TV
			
			IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_MICHAEL_PROJECTOR)
					START_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR, TVCHANNELTYPE_CHANNEL_SPECIAL, TV_PLAYLIST_SPECIAL_WORKOUT)
					SET_TV_INDESTRUCTIBLE(TV_LOC_MICHAEL_PROJECTOR, TRUE)
				ELSE
					
					//B* 545077 - Tracey's dancing game not on the TV.
					IF NOT IS_THIS_TV_ON(TV_LOC_JIMMY_BEDROOM)
						IF NOT IS_TVSHOW_CURRENTLY_PLAYING( HASH("exerciseShow"))
							SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SPECIAL, "PL_SP_WORKOUT")
							SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_SPECIAL)
							
							CPRINTLN(DEBUG_FAMILY, "force TV channel to exerciseShow")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_FAMILY, " Ambient TV TV_LOC_MICHAEL_PROJECTOR not available for ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
			//	CPRINTLN(DEBUG_FAMILY, " event is ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
			//	CprintNL()
				#ENDIF
				EXIT				
			ENDIF
		BREAK
		
		CASE FE_M_DAUGHTER_watching_TV_sober
		CASE FE_M_DAUGHTER_watching_TV_drunk
		CASE FE_M2_SON_watching_TV
		CASE FE_M7_SON_watching_TV_with_tracey
		CASE FE_M7_FAMILY_watching_TV
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
		
			IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_MICHAEL_PROJECTOR)
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_MICHAEL_PROJECTOR)
					START_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
					SET_TV_INDESTRUCTIBLE(TV_LOC_MICHAEL_PROJECTOR, TRUE)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_FAMILY, " Ambient TV TV_LOC_MICHAEL_PROJECTOR not available for ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
			//	CPRINTLN(DEBUG_FAMILY, " event is ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
			//	CprintNL()
				#ENDIF
				EXIT				
			ENDIF
		BREAK
		
		CASE FE_M2_SON_gaming_loop
		CASE FE_M7_SON_gaming
			IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_JIMMY_BEDROOM)
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_JIMMY_BEDROOM)
					START_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM, TVCHANNELTYPE_CHANNEL_SPECIAL, TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER)
					SET_TV_INDESTRUCTIBLE(TV_LOC_JIMMY_BEDROOM, TRUE)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_FAMILY, " Ambient TV TV_LOC_JIMMY_BEDROOM not available for ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
			//	CPRINTLN(DEBUG_FAMILY, " event is ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
			//	CprintNL()
				#ENDIF
				EXIT
			ENDIF
		BREAK
		CASE FE_F_AUNT_watching_TV
		CASE FE_F_AUNT_returned_to_aunts
			
			IF IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOC_FRANKLIN_LIVING)
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_FRANKLIN_LIVING)
					START_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_LIVING)
					SET_TV_INDESTRUCTIBLE(TV_LOC_FRANKLIN_LIVING, TRUE)
				ENDIF
				
				
				#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_FAMILY, " Ambient TV TV_LOC_FRANKLIN_LIVING available for ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
			//	CPRINTLN(DEBUG_FAMILY, " event is ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
				
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_FRANKLIN_LIVING)
				//	CPRINTLN(DEBUG_FAMILY, " NOT forced on")
				ELSE
				//	CPRINTLN(DEBUG_FAMILY, " forced on")
				ENDIF
				
			//	CprintNL()
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
			//	CPRINTLN(DEBUG_FAMILY, " Ambient TV TV_LOC_FRANKLIN_LIVING not available for ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
			//	CPRINTLN(DEBUG_FAMILY, " event is ")
			//	CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
				
				IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_FRANKLIN_LIVING)
				//	CPRINTLN(DEBUG_FAMILY, " NOT forced on")
				ELSE
				//	CPRINTLN(DEBUG_FAMILY, " forced on")
				ENDIF
				
			//	CprintNL()
				#ENDIF
				EXIT
			ENDIF
		BREAK
		
		DEFAULT
			//RESTORE_STANDARD_CHANNELS()
			eFamilyMember = eFamilyMember
		BREAK
	ENDSWITCH
		
ENDPROC
FUNC BOOL PRIVATE_FamilyMemberWatchingTv(PED_INDEX &PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent,
		VECTOR vSequencePos, FLOAT fSequenceHead, INT &iWanderStage,
		enumFamilyEvents eDesiredFamilyEvent = FE_ANY_find_family_event)
	IF (g_eCurrentSafehouseActivity = SA_NONE)
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			RETURN FALSE
		ENDIF
		
		PED_REQUEST_SCENE_ENUM eScene = Get_Player_Timetable_Scene_In_Progress()
		IF (eScene <> PR_SCENE_INVALID)
			TEXT_LABEL_31 RoomName = ""
			FLOAT fTurnOffTVPhase = 0.0
			TV_LOCATION eRoomTVLocation
			TVCHANNELTYPE eTVChannelType
			TV_CHANNEL_PLAYLIST eTVPlaylist

			IF CONTROL_PLAYER_WATCHING_TV(eScene, RoomName, fTurnOffTVPhase, eRoomTVLocation, eTVChannelType, eTVPlaylist)
				RETURN FALSE
			ENDIF
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		IF (GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_SPECIAL)
//			CPRINTLN(DEBUG_FAMILY, "PRIVATE_FamilyMemberWatchingTv(GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_SPECIAL)")
//			CprintNL()
//		ENDIF
//		#ENDIF
		
		#IF IS_DEBUG_BUILD
		IF (iWanderStage <> 0)
			CASSERTLN(DEBUG_FAMILY, "PRIVATE_FamilyMemberWatchingTv(iWanderStage <> ???)")
			iWanderStage = 0
		ENDIF
		#ENDIF
		
		PRIVATE_Update_Tv_Globals(eFamilyMember, eFamilyEvent)
		RETURN TRUE
	ELSE
		VECTOR vGetupOffset
		FLOAT fGetupHead
		IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
				vGetupOffset, fGetupHead)
			vGetupOffset += vSequencePos
			fGetupHead += fSequenceHead
			
			CONST_INT iGETUP_0_requestGetup	0
			CONST_INT iGETUP_1_playGetup	1
			CONST_INT iGETUP_2_endGetup		2
			CONST_INT iGETUP_3_delete		3
			
			STRING sGetupAnimDict = "AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@EXIT", sGetupAnimClip = "EXIT_FORWARD"		
			SWITCH eFamilyEvent
				CASE FE_M7_FAMILY_watching_TV
					iWanderStage = iGETUP_3_delete
				BREAK
				CASE FE_M2_SON_watching_TV
				CASE FE_M7_SON_watching_TV_with_tracey
					iWanderStage = iGETUP_3_delete
				BREAK
				CASE FE_M2_WIFE_in_face_mask
				CASE FE_M7_WIFE_in_face_mask
					sGetupAnimDict = ""
					sGetupAnimClip = ""
					iWanderStage = iGETUP_3_delete
				BREAK
				CASE FE_M_DAUGHTER_workout_with_mp3
				CASE FE_M_DAUGHTER_dancing_practice
					sGetupAnimDict = ""
					sGetupAnimClip = ""
					iWanderStage = iGETUP_3_delete
				BREAK
				CASE FE_M_DAUGHTER_watching_TV_sober
					iWanderStage = iGETUP_3_delete
				BREAK
				CASE FE_M_DAUGHTER_watching_TV_drunk
					iWanderStage = iGETUP_3_delete
				BREAK
//				CASE FE_M_MEXMAID_watching_TV
//				BREAK
				
				CASE FE_F_AUNT_watching_TV
				CASE FE_F_AUNT_returned_to_aunts
					iWanderStage = iGETUP_3_delete
				BREAK
							
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": invalid eFamilyEvent for PRIVATE_FamilyMemberWatchingTv: ", Get_String_From_FamilyEvent(eFamilyEvent))
					CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": invalid eFamilyEvent for PRIVATE_FamilyMemberWatchingTv: ", Get_String_From_FamilyEvent(eFamilyEvent))
					#ENDIF
					
					RETURN FALSE
				BREAK
			ENDSWITCH
		
			
			SWITCH iWanderStage
				CASE iGETUP_0_requestGetup
					REQUEST_ANIM_DICT(sGetupAnimDict)
					IF HAS_ANIM_DICT_LOADED(sGetupAnimDict)
						TASK_PLAY_ANIM_ADVANCED(PedIndex, sGetupAnimDict, sGetupAnimClip,
								vGetupOffset,
								<<0,0,fGetupHead>>,
								INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,
								AF_NOT_INTERRUPTABLE, 0)
						
						CPRINTLN(DEBUG_FAMILY, "ped[", Get_String_From_FamilyMember(eFamilyMember), "] watching TV: iWanderStage = iGETUP_1_playGetup (g_eCurrentSafehouseActivity: ",g_eCurrentSafehouseActivity, ")")
						iWanderStage = iGETUP_1_playGetup
						RETURN FALSE
					ENDIF
				BREAK
				CASE iGETUP_1_playGetup
					IF NOT IS_ENTITY_PLAYING_ANIM(PedIndex, sGetupAnimDict, sGetupAnimClip)
						CPRINTLN(DEBUG_FAMILY, "ped[", Get_String_From_FamilyMember(eFamilyMember), "] watching TV: iWanderStage = iGETUP_2_endGetup (g_eCurrentSafehouseActivity: ",g_eCurrentSafehouseActivity, ")")
						iWanderStage = iGETUP_2_endGetup
						RETURN FALSE
					ENDIF
				BREAK
				CASE iGETUP_2_endGetup
					IF NOT IS_STRING_NULL_OR_EMPTY(sGetupAnimDict)
						REMOVE_ANIM_DICT(sGetupAnimDict)
					ENDIF
					
					PLAY_PED_AMBIENT_SPEECH(PedIndex, "GENERIC_INSULT_MED")
					
					CLEAR_PED_TASKS(PedIndex)
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
					CPRINTLN(DEBUG_FAMILY, "ped[", Get_String_From_FamilyMember(eFamilyMember), "] watching TV: iWanderStage = 0 (endGetup, g_eCurrentSafehouseActivity: ",g_eCurrentSafehouseActivity, ")")
					iWanderStage = 0
					RETURN FALSE
				BREAK
				CASE iGETUP_3_delete
					IF NOT IS_STRING_NULL_OR_EMPTY(sGetupAnimDict)
						REMOVE_ANIM_DICT(sGetupAnimDict)
					ENDIF
					
					IF DOES_ENTITY_EXIST(PedIndex)
						DELETE_PED(PedIndex)
					ENDIF
					
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FAMILY_MEMBER_BUSY)
					CPRINTLN(DEBUG_FAMILY, "ped[", Get_String_From_FamilyMember(eFamilyMember), "] watching TV: iWanderStage = 0 (delete, g_eCurrentSafehouseActivity: ",g_eCurrentSafehouseActivity, ")")
					iWanderStage = 0
					RETURN FALSE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": invalid iWanderStage for PRIVATE_FamilyMemberWatchingTv: ", Get_String_From_FamilyEvent(eFamilyEvent))
					CASSERTLN(DEBUG_FAMILY, GET_THIS_SCRIPT_NAME(), ": invalid iWanderStage for PRIVATE_FamilyMemberWatchingTv: ", Get_String_From_FamilyEvent(eFamilyEvent))
					#ENDIF
					
					RETURN FALSE
				BREAK 
			ENDSWITCH
		ENDIF
		
		RETURN FALSE
	ENDIF
ENDFUNC
PROC PRIVATE_Cleanup_Tv_Globals()
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Tv_Globals -() STARTED ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	IF IS_THIS_TV_FORCED_ON(TV_LOC_MICHAEL_PROJECTOR)
		
		SET_TV_INDESTRUCTIBLE(TV_LOC_MICHAEL_PROJECTOR, FALSE)
		
		STOP_AMBIENT_TV_PLAYBACK(TV_LOC_MICHAEL_PROJECTOR)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Tv_Globals -() CLEANUP MICHAEL'S PROJECTOR ", GET_THIS_SCRIPT_NAME())
		#ENDIF
	ENDIF
	
	IF IS_THIS_TV_FORCED_ON(TV_LOC_JIMMY_BEDROOM)
		
		SET_TV_INDESTRUCTIBLE(TV_LOC_JIMMY_BEDROOM, FALSE)
		
		STOP_AMBIENT_TV_PLAYBACK(TV_LOC_JIMMY_BEDROOM)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Tv_Globals -() CLEANUP JIMMY'S TV ", GET_THIS_SCRIPT_NAME())
		#ENDIF
	ENDIF
	
	IF IS_THIS_TV_FORCED_ON(TV_LOC_FRANKLIN_LIVING)
		
		SET_TV_INDESTRUCTIBLE(TV_LOC_FRANKLIN_LIVING, FALSE)
		
		STOP_AMBIENT_TV_PLAYBACK(TV_LOC_FRANKLIN_LIVING)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Cleanup_Tv_Globals -() CLEANUP DENISE'S TV ", GET_THIS_SCRIPT_NAME())
		#ENDIF
	ENDIF
	

ENDPROC


USING "familyCoords_private.sch"
USING "familyComponent_private.sch"
USING "familyDoor_private.sch"
USING "familyPtfx_private.sch"
#if USE_CLF_DLC
	USING "familySchedule_privateCLF.sch"	
#endif
#if USE_NRM_DLC
	USING "familySchedule_privateNRM.sch"	
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	USING "familySchedule_private.sch"	
#endif
#endif
USING "familySfx_private.sch"
USING "familySpeech_private.sch"
USING "familyTask_private.sch"
