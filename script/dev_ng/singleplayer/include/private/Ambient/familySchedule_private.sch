USING "flow_public_core_override.sch"

USING "player_scene_schedule.sch"

///private header for family task control scripts
///    alwyn.roberts@rockstarnorth.com
///    

FUNC BOOL HasTimeToWaitPassedFromLastPassedMission(FLOAT fSecondsToWait)
	IF (g_iLastMissionPassedGameTime < 0)
		RETURN FALSE
	ENDIF
	
	INT iGameTime = GET_GAME_TIMER()
	IF (iGameTime < (g_iLastMissionPassedGameTime+ROUND(fSecondsToWait*1000.0)))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(enumFamilyMember eFamilyMember)
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_TREVOR_1)
		RETURN TRUE
	ENDIF
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M2_SON_watching_TV
			OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M7_SON_watching_TV_with_tracey
				RETURN TRUE
			ENDIF
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_workout_with_mp3
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_walks_to_room_music
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_dancing_practice
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_watching_TV_sober
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_watching_TV_drunk
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_crying_over_a_guy
			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M7_SON_watching_TV_with_tracey
				RETURN TRUE
			ENDIF
		BREAK
		CASE FM_MICHAEL_WIFE
			IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M2_WIFE_in_face_mask
			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M7_WIFE_in_face_mask
//			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_getting_nails_done
//			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_getting_botox_done
			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M2_WIFE_passed_out_SOFA
			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M7_WIFE_passed_out_SOFA
				RETURN TRUE
			ENDIF
		BREAK
//		CASE FM_MICHAEL_MEXMAID
//			IF g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID]	= FE_M_MEXMAID_watching_TV
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelLivingroom(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_MICHAEL_SON)
		IF PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_SON)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_DAUGHTER)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_WIFE)
		
		IF g_eLastMissionPassed = SP_MISSION_ME_AMANDA
			IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_WIFE)
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M2_WIFE_phones_man_OR_therapist
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M7_WIFE_phones_man_OR_therapist
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_hangs_up_and_wanders
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_MEXMAID)
		IF PRIVATE_Does_FamilyMember_Have_MichaelSofa_Event(FM_MICHAEL_MEXMAID)
		OR g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID]	= FE_M_MEXMAID_stealing_stuff
		OR g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID]	= FE_M_MEXMAID_stealing_stuff_caught
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelShower(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_shower
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_SON)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_rapping_in_the_shower
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelGarden(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M2_DAUGHTER_sunbathing
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M7_DAUGHTER_sunbathing
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_WIFE)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M2_WIFE_sunbathing
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M7_WIFE_sunbathing
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelDriveway(enumFamilyMember eFamilyMember)
	
	IF g_eLastMissionPassed = SP_MISSION_ME_TRACEY
		IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M2_CARSLEEP_a)
		RETURN TRUE
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_SON)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_Borrows_sisters_car
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_Going_out_in_her_car
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_WIFE)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_leaving_in_car
//		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_leaving_in_car_v2
//		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_MD_leaving_in_car_v3
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_MICHAEL_SON)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_Fighting_with_sister_A
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_Fighting_with_sister_B
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_Fighting_with_sister_C
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_SON_Fighting_with_sister_D
		
//		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_WIFE_screaming_at_son_P1
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_WIFE_screaming_at_son_P2
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_WIFE_screaming_at_son_P3
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_screaming_at_dad
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_SON_Fighting_with_sister_A
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_SON_Fighting_with_sister_B
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_SON_Fighting_with_sister_C
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_SON_Fighting_with_sister_D
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_WIFE_screaming_at_daughter
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_WIFE)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_screaming_at_daughter
//		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_screaming_at_son_P1
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_screaming_at_son_P2
		OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_WIFE_screaming_at_son_P3
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_MichaelTV(enumFamilyMember eFamilyMember)
	IF (eFamilyMember <> FM_MICHAEL_SON)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M2_SON_gaming_loop
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M7_SON_gaming
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M2_SON_watching_TV
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M7_SON_watching_TV_with_tracey
		OR g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M7_FAMILY_watching_TV
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_DAUGHTER)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M7_FAMILY_watching_TV
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_watching_TV_drunk
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_watching_TV_sober
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_dancing_practice
		OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_DAUGHTER_workout_with_mp3
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_MICHAEL_WIFE)
		IF g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M7_FAMILY_watching_TV
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_Trevor0Trailer(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_TREVOR_0_RON)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_RON]		= FE_T0_RONEX_trying_to_pick_up_signals
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_TREVOR_0_MICHAEL)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL]	= FE_T0_MICHAEL_depressed_head_in_hands
		OR g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL]	= FE_T0_MICHAEL_on_phone_to_therapist
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap_Trevor0Garden(enumFamilyMember eFamilyMember)
	
	IF (eFamilyMember <> FM_TREVOR_0_RON)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_RON]		= FE_T0_RONEX_outside_looking_lonely
		OR g_eCurrentFamilyEvent[FM_TREVOR_0_RON]		= FE_T0_RONEX_doing_target_practice
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_TREVOR_0_MICHAEL)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL]	= FE_T0_MICHAEL_sunbathing
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_TREVOR_0_TREVOR)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR]	= FE_T0_TREVOR_blowing_shit_up
		OR g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR]	= FE_T0_TREVOR_and_kidnapped_wife_walk
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (eFamilyMember <> FM_TREVOR_0_WIFE)
		IF g_eCurrentFamilyEvent[FM_TREVOR_0_WIFE]		= FE_T0_KIDNAPPED_WIFE_does_garden_work
		OR g_eCurrentFamilyEvent[FM_TREVOR_0_WIFE]		= FE_T0_TREVOR_and_kidnapped_wife_walk
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Does_FamilyEvents_Overlap(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	SWITCH eFamilyEvent
		CASE FE_M_SON_rapping_in_the_shower
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelShower(eFamilyMember)
		BREAK
		CASE FE_M_DAUGHTER_shower
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelShower(eFamilyMember)
		BREAK
		
		CASE FE_M_SON_Borrows_sisters_car
		CASE FE_M7_SON_going_for_a_bike_ride		//#1713901
		CASE FE_M7_SON_coming_back_from_a_bike_ride	//#1713901
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelDriveway(eFamilyMember)
		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelDriveway(eFamilyMember)
		BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2
//		CASE FE_M_WIFE_MD_leaving_in_car_v3
		CASE FE_M_WIFE_leaving_in_car
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelDriveway(eFamilyMember)
		BREAK
		
		CASE FE_M_DAUGHTER_crying_over_a_guy
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelLivingroom(eFamilyMember)
		BREAK
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelLivingroom(eFamilyMember)
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober		FALLTHRU
		CASE FE_M_DAUGHTER_watching_TV_drunk		FALLTHRU
		CASE FE_M2_SON_watching_TV		FALLTHRU
		CASE FE_M7_SON_watching_TV_with_tracey		FALLTHRU
		CASE FE_M_DAUGHTER_workout_with_mp3			FALLTHRU
		CASE FE_M_DAUGHTER_dancing_practice			FALLTHRU
		CASE FE_M7_FAMILY_watching_TV
			IF PRIVATE_Does_FamilyEvents_Overlap_MichaelLivingroom(eFamilyMember)
			OR PRIVATE_Does_FamilyEvents_Overlap_MichaelTV(eFamilyMember)
				RETURN TRUE
			ENDIF
		BREAK

		CASE FE_M_SON_Fighting_with_sister_A
		CASE FE_M_SON_Fighting_with_sister_B
		CASE FE_M_SON_Fighting_with_sister_C
		CASE FE_M_SON_Fighting_with_sister_D
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
		BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
//			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
		BREAK
		CASE FE_M_WIFE_screaming_at_son_P3
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
		BREAK
		CASE FE_M_WIFE_screaming_at_daughter
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelStairs(eFamilyMember)
		BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen
			IF g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID]	= FE_M_MEXMAID_stealing_stuff
			OR g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID]	= FE_M_MEXMAID_stealing_stuff_caught
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FE_M2_SON_gaming_loop
		CASE FE_M7_SON_gaming
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelTV(eFamilyMember)
		BREAK
		CASE FE_M2_DAUGHTER_sunbathing
		CASE FE_M7_DAUGHTER_sunbathing
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelGarden(eFamilyMember)
		BREAK
		
//		CASE FE_M_MEXMAID_watching_TV		FALLTHRU
		CASE FE_M_MEXMAID_stealing_stuff
		CASE FE_M_MEXMAID_stealing_stuff_caught
			RETURN PRIVATE_Does_FamilyEvents_Overlap_MichaelLivingroom(eFamilyMember)
		BREAK
//		CASE FE_M_MEXMAID_cooking_for_son
//			IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]		= FE_M_FAMILY_on_laptops
//			OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	= FE_M_FAMILY_on_laptops
//			OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		= FE_M_FAMILY_on_laptops
//				RETURN TRUE
//			ENDIF
//		BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			IF g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL]	= FAMILY_MEMBER_BUSY
				RETURN TRUE
			ENDIF
		BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals
		CASE FE_T0_MICHAEL_depressed_head_in_hands
		CASE FE_T0_MICHAEL_on_phone_to_therapist
			RETURN PRIVATE_Does_FamilyEvents_Overlap_Trevor0Trailer(eFamilyMember)
		BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely
		CASE FE_T0_RONEX_doing_target_practice
		CASE FE_T0_MICHAEL_sunbathing
		CASE FE_T0_TREVOR_blowing_shit_up
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			RETURN PRIVATE_Does_FamilyEvents_Overlap_Trevor0Garden(eFamilyMember)
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_Shared_FamilyEvents_Busy(enumFamilyEvents eFamilyEvent)
	SWITCH eFamilyEvent
		CASE FE_M_FAMILY_on_laptops
		CASE FE_M7_FAMILY_finished_breakfast
		CASE FE_M7_FAMILY_finished_pizza
		CASE FE_M7_FAMILY_watching_TV
			IF (g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	<> NO_FAMILY_EVENTS)
			OR (g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]		<> NO_FAMILY_EVENTS)
				RETURN TRUE
			ENDIF
		BREAK
		CASE FE_M2_SON_watching_TV
			IF (g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	<> NO_FAMILY_EVENTS)
				RETURN TRUE
			ENDIF
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
		CASE FE_M_WIFE_screaming_at_son_P2
		CASE FE_M_WIFE_screaming_at_son_P3
//		CASE FE_M7_WIFE_shopping_with_son
			IF (g_eCurrentFamilyEvent[FM_MICHAEL_SON]		<> NO_FAMILY_EVENTS)
				RETURN TRUE
			ENDIF
		BREAK
		CASE FE_M_WIFE_screaming_at_daughter
		CASE FE_M7_WIFE_shopping_with_daughter
			IF (g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]	<> NO_FAMILY_EVENTS)
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC




FUNC BOOL PRIVATE_Is_FamilyEvent_With_Busy_Friend(enumFamilyMember eFamilyMember)	//, enumFamilyEvents eFamilyEvent)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
//	PED_SCENE_STRUCT sPedScene
//	PLAYER_TIMETABLE_SCENE_STRUCT sPassedScene
//	
//	sPedScene.iStage = 0
//	sPedScene.eScene = eReqScene
//	sPedScene.ePed = g_sPlayerPedRequest.ePed
//	
//	IF SETUP_PLAYER_TIMETABLE_FOR_SCENE(sPedScene, sPassedScene)
		
		IF (eFamilyMember <> NO_FAMILY_MEMBER)
		AND (eFamilyMember <> MAX_FAMILY_MEMBER)
			enumCharacterList friendCharID = NO_CHARACTER
			INT   iBIT_SCENE_BUDDY = BIT_NOBODY
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_LAMAR
				CASE FM_FRANKLIN_STRETCH
					friendCharID = CHAR_LAMAR
					iBIT_SCENE_BUDDY = BIT_LAMAR
				BREAK
				CASE FM_MICHAEL_SON
					friendCharID = CHAR_JIMMY
					iBIT_SCENE_BUDDY = BIT_JIMMY
				BREAK
				CASE FM_MICHAEL_WIFE
					friendCharID = CHAR_AMANDA
					iBIT_SCENE_BUDDY = BIT_AMANDA
				BREAK
				CASE FM_MICHAEL_DAUGHTER
					friendCharID = CHAR_TRACEY
					iBIT_SCENE_BUDDY = BIT_NOBODY
				BREAK
				
				DEFAULT
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			IF (iBIT_SCENE_BUDDY != BIT_NOBODY)
				INT availableMissionIndex
				REPEAT COUNT_OF(g_availableMissions) availableMissionIndex
					//Is a valid mission saved in this slot?
					IF g_availableMissions[availableMissionIndex].index != ILLEGAL_ARRAY_POSITION
						//Yes. Check the coreVars index it points to.
						
						INT iMissionID = g_flowUnsaved.coreVars[g_availableMissions[availableMissionIndex].index].iValue1
						
						SP_MISSIONS eMissionID = INT_TO_ENUM(SP_MISSIONS, iMissionID)
						
						IF IS_BITMASK_SET(g_sMissionStaticData[eMissionID].friendCharBitset, iBIT_SCENE_BUDDY)
	//						#IF IS_DEBUG_BUILD
	//						IF g_bDebugCprint_SceneScheduleInfo
	//						CPRINTLN(DEBUG_FAMILY, "eFamilyMember ")
	//						CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
	//						CPRINTLN(DEBUG_FAMILY, " blocked on mission \"")
	//						CPRINTLN(DEBUG_FAMILY, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID))
	//						CPRINTLN(DEBUG_FAMILY, "\"")
	//						CprintNL()
	//						ENDIF
	//						#ENDIF
							
							RETURN TRUE
						ENDIF
						
					ENDIF
				ENDREPEAT
			
			
				IF GET_CONNECTION_FROM_FRIENDS(GET_FRIEND_FROM_CHAR(GET_CURRENT_PLAYER_PED_ENUM()), GET_FRIEND_FROM_CHAR(friendCharID)) < MAX_FRIEND_CONNECTIONS
				
					CONST_FLOAT	CONST_fPlayerRealtimeMinuteWait_phone	1.0
					CONST_FLOAT	CONST_fPlayerRealtimeMinuteWait_face	5.0
					FLOAT fLastContactTime = Private_GET_FRIEND_LAST_CONTACT_TIME(GET_CURRENT_PLAYER_PED_ENUM(), friendCharID) 
					SWITCH Private_GET_FRIEND_LAST_CONTACT_TYPE(GET_CURRENT_PLAYER_PED_ENUM(), friendCharID) 
						CASE FRIEND_CONTACT_PHONE
							IF fLastContactTime <= (CONST_fPlayerRealtimeMinuteWait_phone*60.0)
								RETURN TRUE
							ENDIF
						BREAK
						CASE FRIEND_CONTACT_FACE
							IF fLastContactTime <= (CONST_fPlayerRealtimeMinuteWait_face*60.0)
								RETURN TRUE
							ENDIF
						BREAK
						
						DEFAULT
							//
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			
			INT iQueueGameTimer = GET_GAME_TIMER()
			CONST_INT iCONST_QueueGameTimerMs 150000	//2.5min
			
			INT index
			REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
				
				IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = friendCharID
					INT iQueuedCallsDelay = g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedCallsDelay < iCONST_QueueGameTimerMs)
						
						CPRINTLN(DEBUG_FAMILY, "schedule blocked for queued comms")
						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_FamilyEvent_Near_Player(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_ANY_wander_family_event)
		RETURN FALSE
	ENDIF
	
	VECTOR vFamilySceneCoord
	FLOAT fInitHead
	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
			vFamilySceneCoord, fInitHead)
		
		SWITCH eFamilyMember
			CASE FM_MICHAEL_SON
			CASE FM_MICHAEL_DAUGHTER
			CASE FM_MICHAEL_WIFE
			CASE FM_MICHAEL_MEXMAID
			CASE FM_MICHAEL_GARDENER
				vFamilySceneCoord += << -812.0607, 179.5117, 71.1531 >>
			BREAK
			
			CASE FM_FRANKLIN_AUNT
			CASE FM_FRANKLIN_LAMAR
			CASE FM_FRANKLIN_STRETCH
				vFamilySceneCoord += << -14.3064, -1435.9974, 30.1160 >>
			BREAK
			
			CASE FM_TREVOR_0_RON
			CASE FM_TREVOR_0_MICHAEL
			CASE FM_TREVOR_0_TREVOR
			CASE FM_TREVOR_0_WIFE
			CASE FM_TREVOR_0_MOTHER
				vFamilySceneCoord += << 1974.6129, 3819.1438, 32.4374 >>
			BREAK
			
			CASE FM_TREVOR_1_FLOYD
				vFamilySceneCoord += << -1152.5707, -1517.6010, 9.6346 >>
			BREAK
		ENDSWITCH
		
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		
		CONST_FLOAT fMIN_DIST_FROM_PLAYER	5.0		//2.5
		
		IF VDIST2(vFamilySceneCoord, vPlayerCoord) < (fMIN_DIST_FROM_PLAYER*fMIN_DIST_FROM_PLAYER)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_FamilyEvent_Near_Trigger(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
		RETURN FALSE
	ENDIF
	#ENDIF
	
	VECTOR vFamilySceneCoord
//	FLOAT fInitHead
//	IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent,
//			vFamilySceneCoord, fInitHead)
		
		enumCharacterList eFamilyChar = NO_CHARACTER
		
		SWITCH eFamilyMember
			CASE FM_MICHAEL_SON
			CASE FM_MICHAEL_DAUGHTER
			CASE FM_MICHAEL_WIFE
			CASE FM_MICHAEL_MEXMAID
			CASE FM_MICHAEL_GARDENER
				vFamilySceneCoord += << -812.0607, 179.5117, 71.1531 >>
			BREAK
			
			CASE FM_FRANKLIN_AUNT
			CASE FM_FRANKLIN_LAMAR
			CASE FM_FRANKLIN_STRETCH
				vFamilySceneCoord += << -14.3064, -1435.9974, 30.1160 >>
			BREAK
			
			CASE FM_TREVOR_0_RON
			CASE FM_TREVOR_0_MICHAEL
			CASE FM_TREVOR_0_TREVOR
			CASE FM_TREVOR_0_WIFE
			CASE FM_TREVOR_0_MOTHER
				vFamilySceneCoord += << 1974.6129, 3819.1438, 32.4374 >>
				
				IF (eFamilyMember = FM_TREVOR_0_MICHAEL)
					eFamilyChar = CHAR_MICHAEL
				ELIF (eFamilyMember = FM_TREVOR_0_TREVOR)
					eFamilyChar = CHAR_TREVOR
				ELSE
					IF (eFamilyEvent = FE_T0_KIDNAPPED_WIFE_talks_to_Michael)
						eFamilyChar = CHAR_MICHAEL
//					ELIF (eFamilyEvent = FM_TREVOR_0_TREVOR)
//						eFamilyChar = CHAR_TREVOR
					ELSE
						eFamilyChar = NO_CHARACTER
					ENDIF
				ENDIF
				
			BREAK
			
			CASE FM_TREVOR_1_FLOYD
				vFamilySceneCoord += << -1152.5707, -1517.6010, 9.6346 >>
			BREAK
		ENDSWITCH
		
		IF (eFamilyChar = NO_CHARACTER)
			RETURN FALSE
		ENDIF
		
// // // //
//		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//		
//		IF VDIST2(vFamilySceneCoord, vPlayerCoord) < (2.5*2.5)
//			RETURN TRUE
//		ENDIF
// // // //
		
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
			IF g_TriggerableMissions[iTriggerIndex].bUsed
				SP_MISSIONS eMissionID = g_TriggerableMissions[iTriggerIndex].eMissionID
				FLOAT fFriendRejectDistance = g_TriggerableMissions[iTriggerIndex].sScene.fFriendRejectDistance
				INT iFriendsToAcceptBitset = g_TriggerableMissions[iTriggerIndex].sScene.iFriendsToAcceptBitset
				
				STATIC_BLIP_NAME_ENUM eBlip = g_sMissionStaticData[eMissionID].blip
				
				VECTOR vBlipCoord = GET_STATIC_BLIP_POSITION(eBlip)
				FLOAT fDist2FamilyToTrigger = VDIST2(vFamilySceneCoord, vBlipCoord)
				
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Near_Trigger(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")	[", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), ", ", fFriendRejectDistance, ":", SQRT(fDist2FamilyToTrigger), ", ", IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(eFamilyChar)), "]")
				#ENDIF
				
				IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(eFamilyChar))
					IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
						RETURN TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDREPEAT
		
//	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	SWITCH g_eLastMissionPassed
//		CASE SP_MISSION_ARMENIAN_1		//#550889 
//			IF (eFamilyMember = FM_FRANKLIN_AUNT)
//				
//				#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(")
//				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
//				CPRINTLN(DEBUG_FAMILY, ", ")
//				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
//				CPRINTLN(DEBUG_FAMILY, ")")
//				CprintNL()
//				#ENDIF
//				
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE SP_MISSION_FAMILY_3		//#407123
			IF (eFamilyEvent = FE_M_SON_Borrows_sisters_car)
			OR (eFamilyEvent = FE_M_DAUGHTER_Going_out_in_her_car)
//			OR (eFamilyEvent = FE_M_WIFE_leaving_in_car_v2)
//			OR (eFamilyEvent = FE_M_WIFE_MD_leaving_in_car_v3)
			OR (eFamilyEvent = FE_M_WIFE_leaving_in_car)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyEvent(eFamilyEvent), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")")
				#ENDIF
				
				RETURN TRUE
			ENDIF
			IF (eFamilyMember = FM_MICHAEL_GARDENER)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")")
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SP_MISSION_MICHAEL_4		//#1076003 & #1573264
			IF (eFamilyMember = FM_MICHAEL_SON)
			OR (eFamilyMember = FM_MICHAEL_DAUGHTER)
			OR (eFamilyMember = FM_MICHAEL_WIFE)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")")
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE SP_MISSION_ME_AMANDA 
			IF (eFamilyMember = FM_MICHAEL_WIFE)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(")
				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
				CPRINTLN(DEBUG_FAMILY, ", ")
				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
				CPRINTLN(DEBUG_FAMILY, ")")
				CprintNL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE SP_MISSION_ME_JIMMY 
			IF (eFamilyMember = FM_MICHAEL_SON)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(")
				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyMember(eFamilyMember))
				CPRINTLN(DEBUG_FAMILY, ", ")
				CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent))
				CPRINTLN(DEBUG_FAMILY, ")")
				CprintNL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		CASE SP_MISSION_ME_TRACEY 
			IF (eFamilyEvent = FE_M_SON_Borrows_sisters_car)
			#IF USE_TU_CHANGES
			OR (eFamilyEvent = FE_M7_SON_coming_back_from_a_bike_ride)
			#ENDIF
			OR (eFamilyEvent = FE_M_DAUGHTER_Going_out_in_her_car)
//			OR (eFamilyEvent = FE_M_WIFE_leaving_in_car_v2)
//			OR (eFamilyEvent = FE_M_WIFE_MD_leaving_in_car_v3)
			OR (eFamilyEvent = FE_M_WIFE_leaving_in_car)
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")")
				#ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	
	//#1542252
	IF (eFamilyEvent = FE_M2_SON_gaming_loop)
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_2)
		AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
				
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_FamilyEvent_In_Bad_Weather(enumFamilyEvents eFamilyEvent)
	CONST_INT iEXTRASUNNY	HASH("EXTRASUNNY")
	CONST_INT iCLEAR		HASH("CLEAR")
	CONST_INT iCLOUDS		HASH("CLOUDS")
	CONST_INT iSMOG			HASH("SMOG")
	CONST_INT iCLOUDY		HASH("CLOUDY")
	CONST_INT iOVERCAST		HASH("OVERCAST")
	CONST_INT iRAIN			HASH("RAIN")
	CONST_INT iTHUNDER		HASH("THUNDER")
	CONST_INT iCLEARING		HASH("CLEARING")
	CONST_INT iNEUTRAL		HASH("NEUTRAL")
	CONST_INT iSNOW			HASH("SNOW")
	CONST_INT iMULTIPLAYER	HASH("MULTIPLAYER")
	
	INT iPREV_WEATHER_TYPE_HASH_NAME, iNEXT_WEATHER_TYPE_HASH_NAME
	
	SWITCH eFamilyEvent
		CASE FE_M2_DAUGHTER_sunbathing
		CASE FE_M7_DAUGHTER_sunbathing
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
		CASE FE_T0_MICHAEL_sunbathing
		
		// * bug #1590884 * * * * * * * * * * * //
		CASE FE_M_GARDENER_with_leaf_blower
		CASE FE_M_GARDENER_planting_flowers		//
		CASE FE_M_GARDENER_cleaning_pool
		CASE FE_M_GARDENER_mowing_lawn			//
		CASE FE_M_GARDENER_watering_flowers
		CASE FE_M_GARDENER_on_phone				//
		CASE FE_M_GARDENER_smoking_weed
		// * * * * * * * * * * * * * * * * * *  //
		
			iPREV_WEATHER_TYPE_HASH_NAME = GET_PREV_WEATHER_TYPE_HASH_NAME()
			iNEXT_WEATHER_TYPE_HASH_NAME = GET_NEXT_WEATHER_TYPE_HASH_NAME()
			
			BOOL bInvalidPrevWeather, bInvalidNextWeather
			bInvalidPrevWeather = FALSE
			bInvalidNextWeather = FALSE
			
			IF (iPREV_WEATHER_TYPE_HASH_NAME = iRAIN)
			OR (iPREV_WEATHER_TYPE_HASH_NAME = iTHUNDER)
			OR (iPREV_WEATHER_TYPE_HASH_NAME = iSNOW)
				bInvalidPrevWeather = TRUE
			ENDIF
			IF (iNEXT_WEATHER_TYPE_HASH_NAME = iRAIN)
			OR (iNEXT_WEATHER_TYPE_HASH_NAME = iTHUNDER)
			OR (iNEXT_WEATHER_TYPE_HASH_NAME = iSNOW)
				bInvalidPrevWeather = TRUE
			ENDIF
			
			IF bInvalidPrevWeather OR bInvalidNextWeather
				RETURN TRUE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PRIVATE_Is_FamilyEvent_Available(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)
		IF (eFamilyMember = FM_MICHAEL_SON)
		OR (eFamilyMember = FM_MICHAEL_DAUGHTER)
		OR (eFamilyMember = FM_MICHAEL_WIFE)
		OR (eFamilyMember = FM_MICHAEL_MEXMAID)
		OR (eFamilyMember = FM_MICHAEL_GARDENER)
			IF IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_TrevorBeforeFam4(", Get_String_From_FamilyMember(eFamilyMember), ")")
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF eFamilyEvent = g_savedGlobals.sFamilyData.ePreviousFamilyEvent[eFamilyMember]
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Previous_Family_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Does_FamilyEvents_Overlap(eFamilyMember, eFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Does_FamilyEvents_Overlap(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(eFamilyMember, eFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Blocked_For_Last_Passed_Mission(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Is_Shared_FamilyEvents_Busy(eFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_Shared_FamilyEvents_Busy(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Is_FamilyEvent_Near_Player(eFamilyMember, eFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_Near_Player(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF PRIVATE_Is_FamilyEvent_In_Bad_Weather(eFamilyEvent)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_In_Bad_Weather(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA)
		IF eFamilyEvent = FE_M_FAMILY_on_laptops
		OR eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
		OR eFamilyEvent = FE_M7_FAMILY_finished_breakfast
		OR eFamilyEvent = FE_M7_FAMILY_finished_pizza
		OR eFamilyEvent = FE_M7_FAMILY_watching_TV
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_During_Fakeyoga(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PRIVATE_Get_Desired_FamilyEventFromArray(enumFamilyMember eFamilyMember,
		enumFamilyEvents &eEventArray[], FLOAT &fPercentArray[],
		enumFamilyEvents &eDesiredEvent)
	
	//get a percentage...
	FLOAT fMaxPercent = 0
	
	INT iLoop
	REPEAT COUNT_OF(fPercentArray) iLoop
		IF PRIVATE_Is_FamilyEvent_Available(eFamilyMember, eEventArray[iLoop])
			fMaxPercent += fPercentArray[iLoop]
		ENDIF
	ENDREPEAT
	
	FLOAT fPercent = GET_RANDOM_FLOAT_IN_RANGE(0, fMaxPercent)
	
	REPEAT COUNT_OF(fPercentArray) iLoop
		IF PRIVATE_Is_FamilyEvent_Available(eFamilyMember, eEventArray[iLoop])
			IF fPercent < fPercentArray[iLoop]
				eDesiredEvent = eEventArray[iLoop]
				RETURN TRUE
			ENDIF
			
			fPercent -= fPercentArray[iLoop]
		ENDIF
	ENDREPEAT
	
	eDesiredEvent = NO_FAMILY_EVENTS
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = ("invalid fPercent in Get_Desired_")
	str += GET_STRING_FROM_STRING(Get_String_From_FamilyMember(eFamilyMember),
			GET_LENGTH_OF_LITERAL_STRING("FM_"),
			GET_LENGTH_OF_LITERAL_STRING(Get_String_From_FamilyMember(eFamilyMember)))
	str += ("_Event()")
	CPRINTLN(DEBUG_FAMILY, str)
//	SCRIPT_ASSERT(str)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ScheduleEventForThisChar(enumCharacterList ePed)
	IF IS_PED_THE_CURRENT_PLAYER_PED(ePed)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "ScheduleEventForThisChar(", GET_PLAYER_PED_STRING(ePed), ") - ped us current ped")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF Is_TIMEOFDAY_Valid(sLastTimeActive)
		IF NOT HasHourPassedSinceCharLastTimeActive(ePed)
			
			#IF IS_DEBUG_BUILD
			INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
			GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastTimeActive, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
			
			
			CPRINTLN(DEBUG_FAMILY, "ScheduleEventForThisChar(", GET_PLAYER_PED_STRING(ePed), ") - hour hasnt passes since last active[", iSeconds, "s ", iMinutes, "m ", iHours, "h ", iDays, "d ", iMonths+(iYears*12), "m", "]")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_PED_AVAILABLE(ePed)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "ScheduleEventForThisChar(", GET_PLAYER_PED_STRING(ePed), ") - ped is not available")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE SON SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
FUNC BOOL PRIVATE_Force_Family_EventAvailableMissions(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent, SP_MISSIONS &eReturnedSpMissions)
	
	eReturnedSpMissions = SP_MISSION_NONE
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)
				eReturnedSpMissions = SP_MISSION_FAMILY_1
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)			//#1088344
				eReturnedSpMissions = SP_MISSION_FAMILY_4
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF (g_eLastMissionPassed = SP_MISSION_LESTER_1)			//#1336824
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_LESTER_1
					
					#IF NOT IS_NEXTGEN_BUILD
					eFamilyEvent = FE_M_SON_rapping_in_the_shower
					#ENDIF
					#IF IS_NEXTGEN_BUILD
					eFamilyEvent = NO_FAMILY_EVENTS
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			IF GetMichaelScheduleStage() = MSS_M2_WithFamily
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)	//tracey leaves after fam4
					eReturnedSpMissions = SP_MISSION_FAMILY_4
					eFamilyEvent = NO_FAMILY_EVENTS
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_2)
				eReturnedSpMissions = SP_MISSION_FAMILY_2
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)			//#1024884
				eReturnedSpMissions = SP_MISSION_FAMILY_1
				eFamilyEvent = FE_M_DAUGHTER_watching_TV_sober
				RETURN TRUE
			ENDIF
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)			//#1122724
				eReturnedSpMissions = SP_MISSION_FAMILY_4
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)			//#1282380
				eReturnedSpMissions = SP_MISSION_FAMILY_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF (g_eLastMissionPassed = SP_MISSION_LESTER_1)		//#1336824
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_LESTER_1
					eFamilyEvent = FE_M_DAUGHTER_on_phone_to_friends
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE FM_MICHAEL_WIFE
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)			//#1024884
				eReturnedSpMissions = SP_MISSION_FAMILY_1
				eFamilyEvent = FE_M_WIFE_gets_drink_in_kitchen
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)
				eReturnedSpMissions = SP_MISSION_FAMILY_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)			//#1088344
				eReturnedSpMissions = SP_MISSION_FAMILY_4
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF (g_eLastMissionPassed = SP_MISSION_ME_AMANDA)		//#1738671
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_ME_AMANDA
					eFamilyEvent = FE_M_WIFE_gets_drink_in_kitchen
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		CASE FM_MICHAEL_MEXMAID
			
			IF (g_eLastMissionPassed = SP_MISSION_LESTER_1)				//#1771082
				eReturnedSpMissions = SP_MISSION_LESTER_1
				eFamilyEvent = NO_FAMILY_EVENTS
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			IF (g_eLastMissionPassed = SP_MISSION_ARMENIAN_1)		//#1390895
				IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_ARMENIAN_1
					eFamilyEvent = FE_F_AUNT_watching_TV
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_LAMAR)
				eReturnedSpMissions = SP_MISSION_LAMAR
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)
				eReturnedSpMissions = SP_MISSION_FRANKLIN_1
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
		BREAK
		CASE FM_FRANKLIN_LAMAR
		CASE FM_FRANKLIN_STRETCH
			
			IF (g_eLastMissionPassed = SP_MISSION_LAMAR)
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					IF g_savedGlobals.sFamilyData.ePreviousFamilyEvent[FM_FRANKLIN_STRETCH] != FE_F_LAMAR_and_STRETCH_wandering
					AND g_savedGlobals.sFamilyData.ePreviousFamilyEvent[FM_FRANKLIN_LAMAR] != FE_F_LAMAR_and_STRETCH_wandering
						eReturnedSpMissions = SP_MISSION_LAMAR
						eFamilyEvent = FE_F_LAMAR_and_STRETCH_wandering
						RETURN TRUE
					ELSE
						PRINTLN("bug #1668574 would have happened here, fix must have worked")
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_LAMAR)		//1081258
				eReturnedSpMissions = SP_MISSION_LAMAR
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_FRANKLIN_1)			//1090775
				eReturnedSpMissions = SP_MISSION_FRANKLIN_1
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FRANKLIN_1)	//1081258
				eReturnedSpMissions = SP_MISSION_FRANKLIN_1
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FM_TREVOR_0_RON
			IF (g_eLastMissionPassed = SP_MISSION_TREVOR_1)			//1111358
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_TREVOR_1
					eFamilyEvent = NO_FAMILY_EVENTS
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF (g_eLastMissionPassed = SP_MISSION_EXILE_1)			//1167803
				IF HasTimeToWaitPassedFromLastPassedMission(60.0)
					eReturnedSpMissions = SP_MISSION_EXILE_1
					eFamilyEvent = NO_FAMILY_EVENTS
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_TREVOR_2)
				eReturnedSpMissions = SP_MISSION_TREVOR_2
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_TREVOR_3)
				eReturnedSpMissions = SP_MISSION_TREVOR_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
			
			IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_3)				//1090653
				eReturnedSpMissions = SP_MISSION_EXILE_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
		BREAK
		
//		CASE FM_TREVOR_0_MICHAEL
//			IF g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[CHAR_MICHAEL] = PR_SCENE_Ma_MICHAEL2
//				eFamilyEvent = FE_M_MICHAEL_MIC2_washing_face
//				RETURN TRUE
//			ENDIF
//			
//			eFamilyEvent = NO_FAMILY_EVENTS
//			RETURN FALSE
//		BREAK
		
		CASE FM_TREVOR_0_TREVOR
			IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_3)	//1090653
				eReturnedSpMissions = SP_MISSION_EXILE_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FM_TREVOR_0_WIFE
			IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_3)	//1090653
				eReturnedSpMissions = SP_MISSION_EXILE_3
				eFamilyEvent = NO_FAMILY_EVENTS
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE FM_TREVOR_1_FLOYD	// block Floyd for the pre-Docks Heist 1 setup stuff)
			IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_1)
				eReturnedSpMissions = SP_HEIST_DOCKS_1
				eFamilyEvent = FAMILY_MEMBER_BUSY
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF PRIVATE_Is_FamilyEvent_With_Busy_Friend(eFamilyMember)
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Is_FamilyEvent_With_Busy_Friend(", Get_String_From_FamilyMember(eFamilyMember), ")")
		#ENDIF
		
		eReturnedSpMissions = SP_MISSION_MAX
		eFamilyEvent = FAMILY_MEMBER_BUSY
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE SON SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Son_Morning_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		
		CONST_FLOAT fCHANCE_SON_sleeping					60.0
		CONST_FLOAT fCHANCE_SON_gaming						10.0
		CONST_FLOAT fCHANCE_SON_out							10.0
		CONST_FLOAT fCHANCE_SON_rapping						10.0
		CONST_FLOAT fCHANCE_FAMILY_allOnline				10.0
		
		enumFamilyEvents	eEventArray[5]
		FLOAT 				fPercentArray[5]
		
		eEventArray[0]	= FE_M_SON_sleeping					fPercentArray[0] = fCHANCE_SON_sleeping
		eEventArray[1]	= FE_M2_SON_gaming_loop				fPercentArray[1] = fCHANCE_SON_gaming
		eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = fCHANCE_SON_out
		eEventArray[3]	= FE_M_SON_rapping_in_the_shower	fPercentArray[3] = fCHANCE_SON_rapping
		
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)	//tracey leaves after fam4
			eEventArray[4]	= FE_M_FAMILY_on_laptops		fPercentArray[4] = fCHANCE_FAMILY_allOnline
		ELSE
			eEventArray[4]	= NO_FAMILY_EVENTS				fPercentArray[4] = 0.0
		ENDIF
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_SON_jumping_jacks					15.0
		CONST_FLOAT fCHANCE_SON_Gaming							15.0
		CONST_FLOAT fCHANCE_SON_going_for_a_bike_ride			15.0
		CONST_FLOAT fCHANCE_SON_coming_back_from_a_bike_ride	15.0
		CONST_FLOAT fCHANCE_SON_on_laptop_looking_for_jobs		15.0
		CONST_FLOAT fCHANCE_SON_watching_TV_with_tracey			10.0
		
		CONST_FLOAT fCHANCE_FAMILY_finished_breakfast			15.0
		CONST_FLOAT fCHANCE_FAMILY_finished_pizza				15.0
		CONST_FLOAT fCHANCE_FAMILY_watching_TV					15.0
		
		enumFamilyEvents	eEventArray[9]
		FLOAT 				fPercentArray[9]
		
		eEventArray[0]	= FE_M7_SON_jumping_jacks					fPercentArray[0] = fCHANCE_SON_jumping_jacks
		eEventArray[1]	= FE_M7_SON_Gaming							fPercentArray[1] = fCHANCE_SON_Gaming
		eEventArray[2]	= FE_M7_SON_going_for_a_bike_ride			fPercentArray[2] = fCHANCE_SON_going_for_a_bike_ride
		eEventArray[3]	= FE_M7_SON_coming_back_from_a_bike_ride	fPercentArray[3] = fCHANCE_SON_coming_back_from_a_bike_ride
		eEventArray[4]	= FE_M7_SON_on_laptop_looking_for_jobs		fPercentArray[4] = fCHANCE_SON_on_laptop_looking_for_jobs
		eEventArray[5]	= FE_M7_FAMILY_watching_TV					fPercentArray[5] = fCHANCE_SON_watching_TV_with_tracey
		
		eEventArray[6]	= FE_M7_FAMILY_finished_breakfast			fPercentArray[6] = fCHANCE_FAMILY_finished_breakfast
		eEventArray[7]	= FE_M7_FAMILY_finished_pizza				fPercentArray[7] = fCHANCE_FAMILY_finished_pizza
		eEventArray[8]	= FE_M7_FAMILY_watching_TV					fPercentArray[8] = fCHANCE_FAMILY_watching_TV
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Son_Morning_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    12:00 - 17:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Son_Afternoon_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_SON_borrowCar					20.0
		CONST_FLOAT fCHANCE_FAMILY_allOnline				10.0
		CONST_FLOAT fCHANCE_SON_out							20.0
		CONST_FLOAT fCHANCE_SON_gaming						40.0
		CONST_FLOAT fCHANCE_SON_crackingOff					10.0
		
		enumFamilyEvents	eEventArray[5]
		FLOAT 				fPercentArray[5]
		
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)		//tracey leaves after fam4
			IF NOT IS_MISSION_AVAILABLE(SP_MISSION_ME_TRACEY)
				eEventArray[0]	= FE_M_SON_Borrows_sisters_car	fPercentArray[0] = fCHANCE_SON_borrowCar
			ELSE
				eEventArray[0]	= NO_FAMILY_EVENTS			fPercentArray[0] = 0.0
			ENDIF
			
			eEventArray[1]	= FE_M_FAMILY_on_laptops		fPercentArray[1] = fCHANCE_FAMILY_allOnline
		ELSE
			eEventArray[0]	= NO_FAMILY_EVENTS				fPercentArray[0] = 0.0
			eEventArray[1]	= NO_FAMILY_EVENTS				fPercentArray[1] = 0.0
		ENDIF
		
		eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = fCHANCE_SON_out
		eEventArray[3]	= FE_M2_SON_gaming_loop				fPercentArray[3] = fCHANCE_SON_gaming
		eEventArray[4]	= FE_M_SON_watching_porn			fPercentArray[4] = fCHANCE_SON_crackingOff
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_SON_jumping_jacks					15.0
		CONST_FLOAT fCHANCE_SON_Gaming							15.0
		CONST_FLOAT fCHANCE_SON_going_for_a_bike_ride			15.0
		CONST_FLOAT fCHANCE_SON_coming_back_from_a_bike_ride	15.0
		CONST_FLOAT fCHANCE_SON_on_laptop_looking_for_jobs		15.0
		CONST_FLOAT fCHANCE_SON_watching_TV_with_tracey			10.0
		
		CONST_FLOAT fCHANCE_FAMILY_finished_breakfast			15.0
		CONST_FLOAT fCHANCE_FAMILY_finished_pizza				15.0
		CONST_FLOAT fCHANCE_FAMILY_watching_TV					15.0
		
		enumFamilyEvents	eEventArray[9]
		FLOAT 				fPercentArray[9]
		
		eEventArray[0]	= FE_M7_SON_jumping_jacks					fPercentArray[0] = fCHANCE_SON_jumping_jacks
		eEventArray[1]	= FE_M7_SON_Gaming							fPercentArray[1] = fCHANCE_SON_Gaming
		eEventArray[2]	= FE_M7_SON_going_for_a_bike_ride			fPercentArray[2] = fCHANCE_SON_going_for_a_bike_ride
		eEventArray[3]	= FE_M7_SON_coming_back_from_a_bike_ride	fPercentArray[3] = fCHANCE_SON_coming_back_from_a_bike_ride
		eEventArray[4]	= FE_M7_SON_on_laptop_looking_for_jobs		fPercentArray[4] = fCHANCE_SON_on_laptop_looking_for_jobs
		eEventArray[5]	= FE_M7_FAMILY_watching_TV			fPercentArray[5] = fCHANCE_SON_watching_TV_with_tracey
		
		eEventArray[6]	= FE_M7_FAMILY_finished_breakfast			fPercentArray[6] = fCHANCE_FAMILY_finished_breakfast
		eEventArray[7]	= FE_M7_FAMILY_finished_pizza				fPercentArray[7] = fCHANCE_FAMILY_finished_pizza
		eEventArray[8]	= FE_M7_FAMILY_watching_TV					fPercentArray[8] = fCHANCE_FAMILY_watching_TV
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Son_Afternoon_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    18:00 - 23:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Son_Evening_Event(enumFamilyEvents &eFamilyEvent)
	
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_SON_munchRoom						10.0
		CONST_FLOAT fCHANCE_SON_phone							10.0
		CONST_FLOAT fCHANCE_SON_ecstasy							15.0
		CONST_FLOAT fCHANCE_FAMILY_allOnline					10.0
		CONST_FLOAT fCHANCE_SON_fightSister						15.0
		CONST_FLOAT fCHANCE_SON_bedroomPot						40.0
		
		enumFamilyEvents	eEventArray[6+3]
		FLOAT 				fPercentArray[6+3]
		
		eEventArray[0]	= FE_M_SON_in_room_asks_for_munchies	fPercentArray[0] = fCHANCE_SON_munchRoom
		eEventArray[1]	= FE_M_SON_phone_calls_in_room			fPercentArray[1] = fCHANCE_SON_phone
		eEventArray[2]	= FE_M_SON_on_ecstasy_AND_friendly		fPercentArray[2] = fCHANCE_SON_ecstasy
		
		eEventArray[3]	= FE_M_SON_smoking_weed_in_a_bong		fPercentArray[3] = fCHANCE_SON_bedroomPot
		
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)		//tracey leaves after fam4
			eEventArray[4]	= FE_M_FAMILY_on_laptops			fPercentArray[4] = fCHANCE_FAMILY_allOnline
			eEventArray[5]	= FE_M_SON_Fighting_with_sister_A	fPercentArray[5] = fCHANCE_SON_fightSister / 4.0
			eEventArray[6]	= FE_M_SON_Fighting_with_sister_B	fPercentArray[6] = fCHANCE_SON_fightSister / 4.0
			eEventArray[7]	= FE_M_SON_Fighting_with_sister_C	fPercentArray[7] = fCHANCE_SON_fightSister / 4.0
			eEventArray[8]	= FE_M_SON_Fighting_with_sister_D	fPercentArray[8] = fCHANCE_SON_fightSister / 4.0
		ELSE
			eEventArray[4]	= NO_FAMILY_EVENTS					fPercentArray[4] = 0.0
			eEventArray[5]	= NO_FAMILY_EVENTS					fPercentArray[5] = 0.0
			eEventArray[6]	= NO_FAMILY_EVENTS					fPercentArray[6] = 0.0
			eEventArray[7]	= NO_FAMILY_EVENTS					fPercentArray[7] = 0.0
			eEventArray[8]	= NO_FAMILY_EVENTS					fPercentArray[8] = 0.0
		ENDIF
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_SON_jumping_jacks					15.0
		CONST_FLOAT fCHANCE_SON_Gaming							15.0
		CONST_FLOAT fCHANCE_SON_going_for_a_bike_ride			15.0
		CONST_FLOAT fCHANCE_SON_coming_back_from_a_bike_ride	15.0
		CONST_FLOAT fCHANCE_SON_on_laptop_looking_for_jobs		15.0
		CONST_FLOAT fCHANCE_SON_watching_TV_with_tracey			10.0
		
		CONST_FLOAT fCHANCE_FAMILY_finished_breakfast			15.0
		CONST_FLOAT fCHANCE_FAMILY_finished_pizza				15.0
		CONST_FLOAT fCHANCE_FAMILY_watching_TV					15.0
		
		enumFamilyEvents	eEventArray[9]
		FLOAT 				fPercentArray[9]
		
		eEventArray[0]	= FE_M7_SON_jumping_jacks					fPercentArray[0] = fCHANCE_SON_jumping_jacks
		eEventArray[1]	= FE_M7_SON_Gaming							fPercentArray[1] = fCHANCE_SON_Gaming
		eEventArray[2]	= FE_M7_SON_going_for_a_bike_ride			fPercentArray[2] = fCHANCE_SON_going_for_a_bike_ride
		eEventArray[3]	= FE_M7_SON_coming_back_from_a_bike_ride	fPercentArray[3] = fCHANCE_SON_coming_back_from_a_bike_ride
		eEventArray[4]	= FE_M7_SON_on_laptop_looking_for_jobs		fPercentArray[4] = fCHANCE_SON_on_laptop_looking_for_jobs
		eEventArray[5]	= FE_M7_FAMILY_watching_TV			fPercentArray[5] = fCHANCE_SON_watching_TV_with_tracey
		
		eEventArray[6]	= FE_M7_FAMILY_finished_breakfast			fPercentArray[6] = fCHANCE_FAMILY_finished_breakfast
		eEventArray[7]	= FE_M7_FAMILY_finished_pizza				fPercentArray[7] = fCHANCE_FAMILY_finished_pizza
		eEventArray[8]	= FE_M7_FAMILY_watching_TV					fPercentArray[8] = fCHANCE_FAMILY_watching_TV
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Son_Evening_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    00:00 - 05:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Son_Night_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_SON_munchFridge					30.0
		CONST_FLOAT fCHANCE_SON_phone						30.0
		CONST_FLOAT fCHANCE_SON_out							10.0
		CONST_FLOAT fCHANCE_SON_gaming						30.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		eEventArray[0]	= FE_M_SON_raids_fridge_for_food	fPercentArray[0] = fCHANCE_SON_munchFridge
		eEventArray[1]	= FE_M_SON_phone_calls_in_room		fPercentArray[1] = fCHANCE_SON_phone
		eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = fCHANCE_SON_out
		eEventArray[3]	= FE_M2_SON_gaming_loop				fPercentArray[3] = fCHANCE_SON_gaming
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_SON_jumping_jacks					15.0
		CONST_FLOAT fCHANCE_SON_Gaming							15.0
		CONST_FLOAT fCHANCE_SON_going_for_a_bike_ride			15.0
		CONST_FLOAT fCHANCE_SON_coming_back_from_a_bike_ride	15.0
		CONST_FLOAT fCHANCE_SON_on_laptop_looking_for_jobs		15.0
		CONST_FLOAT fCHANCE_SON_watching_TV_with_tracey			10.0
		
		CONST_FLOAT fCHANCE_FAMILY_finished_breakfast			15.0
		CONST_FLOAT fCHANCE_FAMILY_finished_pizza				15.0
		CONST_FLOAT fCHANCE_FAMILY_watching_TV					15.0
		
		enumFamilyEvents	eEventArray[9]
		FLOAT 				fPercentArray[9]
		
		eEventArray[0]	= FE_M7_SON_jumping_jacks					fPercentArray[0] = fCHANCE_SON_jumping_jacks
		eEventArray[1]	= FE_M7_SON_Gaming							fPercentArray[1] = fCHANCE_SON_Gaming
		eEventArray[2]	= FE_M7_SON_going_for_a_bike_ride			fPercentArray[2] = fCHANCE_SON_going_for_a_bike_ride
		eEventArray[3]	= FE_M7_SON_coming_back_from_a_bike_ride	fPercentArray[3] = fCHANCE_SON_coming_back_from_a_bike_ride
		eEventArray[4]	= FE_M7_SON_on_laptop_looking_for_jobs		fPercentArray[4] = fCHANCE_SON_on_laptop_looking_for_jobs
		eEventArray[5]	= FE_M7_FAMILY_watching_TV					fPercentArray[5] = fCHANCE_SON_watching_TV_with_tracey
		
		eEventArray[6]	= FE_M7_FAMILY_finished_breakfast			fPercentArray[6] = fCHANCE_FAMILY_finished_breakfast
		eEventArray[7]	= FE_M7_FAMILY_finished_pizza				fPercentArray[7] = fCHANCE_FAMILY_finished_pizza
		eEventArray[8]	= FE_M7_FAMILY_watching_TV					fPercentArray[8] = fCHANCE_FAMILY_watching_TV
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_SON,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Son_Night_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE DAUGHTER SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Daughter_Morning_Event(enumFamilyEvents &eFamilyEvent)
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_DAUGHTER_sunbathing					15.0
		CONST_FLOAT fCHANCE_DAUGHTER_doing_work_out				15.0
		CONST_FLOAT fCHANCE_DAUGHTER_Going_out_in_her_car		15.0
		CONST_FLOAT fCHANCE_DAUGHTER_Walking_to_room_ipod		10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Out						20.0
		CONST_FLOAT fCHANCE_DAUGHTER_dancing_practice			15.0
		
		enumFamilyEvents	eEventArray[6]
		FLOAT 				fPercentArray[6]
		
		eEventArray[0]	= FE_M2_DAUGHTER_sunbathing				fPercentArray[0] = fCHANCE_DAUGHTER_sunbathing
		eEventArray[1]	= FE_M_DAUGHTER_workout_with_mp3		fPercentArray[1] = fCHANCE_DAUGHTER_doing_work_out
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_ME_TRACEY)
			eEventArray[2]	= FE_M_DAUGHTER_Going_out_in_her_car	fPercentArray[2] = fCHANCE_DAUGHTER_Going_out_in_her_car
		ELSE
			eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = 0.0
		ENDIF
		
		eEventArray[3]	= FE_M_DAUGHTER_walks_to_room_music		fPercentArray[3] = fCHANCE_DAUGHTER_Walking_to_room_ipod
		eEventArray[4]	= NO_FAMILY_EVENTS						fPercentArray[4] = fCHANCE_DAUGHTER_Out
		eEventArray[5]	= FE_M_DAUGHTER_dancing_practice		fPercentArray[5] = fCHANCE_DAUGHTER_dancing_practice
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_DAUGHTER_studying_on_phone			33.33333333						
		CONST_FLOAT fCHANCE_DAUGHTER_studying_does_nails		33.33333333						
		CONST_FLOAT fCHANCE_DAUGHTER_sunbathing					33.33333333
		
		enumFamilyEvents	eEventArray[3]
		FLOAT 				fPercentArray[3]
		
		eEventArray[0]	= FE_M7_DAUGHTER_studying_on_phone		fPercentArray[0] = fCHANCE_DAUGHTER_studying_on_phone
		eEventArray[1]	= FE_M7_DAUGHTER_studying_does_nails	fPercentArray[1] = fCHANCE_DAUGHTER_studying_does_nails
		eEventArray[2]	= FE_M7_DAUGHTER_sunbathing				fPercentArray[2] = fCHANCE_DAUGHTER_sunbathing
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Daughter_Morning_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    12:00 - 17:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Daughter_Afternoon_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_DAUGHTER_sunbathing					15.0
		CONST_FLOAT fCHANCE_DAUGHTER_purging_in_the_bathroom	10.0
		CONST_FLOAT fCHANCE_DAUGHTER_on_phone					10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Going_out_in_her_car		10.0
		CONST_FLOAT fCHANCE_DAUGHTER_shower						10.0
		CONST_FLOAT fCHANCE_DAUGHTER_dancing_practice			20.0
		CONST_FLOAT fCHANCE_DAUGHTER_watching_TV				10.0
		CONST_FLOAT fCHANCE_DAUGHTER_couchsleep					10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Out						5.0
		
		enumFamilyEvents	eEventArray[10]
		FLOAT 				fPercentArray[10]
		
		eEventArray[0]	= FE_M2_DAUGHTER_sunbathing				fPercentArray[0] = fCHANCE_DAUGHTER_sunbathing
		eEventArray[1]	= FE_M_DAUGHTER_purges_in_the_bathroom	fPercentArray[1] = fCHANCE_DAUGHTER_purging_in_the_bathroom
		eEventArray[2]	= FE_M_DAUGHTER_on_phone_to_friends		fPercentArray[2] = fCHANCE_DAUGHTER_on_phone
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_ME_TRACEY)
			eEventArray[3]	= FE_M_DAUGHTER_Going_out_in_her_car	fPercentArray[3] = fCHANCE_DAUGHTER_Going_out_in_her_car
		ELSE
			eEventArray[3]	= NO_FAMILY_EVENTS					fPercentArray[3] = 0.0
		ENDIF
		
		eEventArray[4]	= FE_M_DAUGHTER_shower					fPercentArray[4] = fCHANCE_DAUGHTER_shower
		eEventArray[5]	= FE_M_DAUGHTER_dancing_practice		fPercentArray[5] = fCHANCE_DAUGHTER_dancing_practice
		eEventArray[6]	= FE_M_DAUGHTER_watching_TV_sober		fPercentArray[6] = fCHANCE_DAUGHTER_watching_TV / 2.0
		eEventArray[7]	= FE_M_DAUGHTER_watching_TV_drunk		fPercentArray[7] = fCHANCE_DAUGHTER_watching_TV / 2.0
		eEventArray[8]	= FE_M_DAUGHTER_couchsleep				fPercentArray[8] = fCHANCE_DAUGHTER_couchsleep
		eEventArray[9]	= NO_FAMILY_EVENTS						fPercentArray[9] = fCHANCE_DAUGHTER_Out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_DAUGHTER_studying_on_phone			33.33333333						
		CONST_FLOAT fCHANCE_DAUGHTER_studying_does_nails		33.33333333						
		CONST_FLOAT fCHANCE_DAUGHTER_sunbathing					33.33333333
		
		enumFamilyEvents	eEventArray[3]
		FLOAT 				fPercentArray[3]
		
		eEventArray[0]	= FE_M7_DAUGHTER_studying_on_phone		fPercentArray[0] = fCHANCE_DAUGHTER_studying_on_phone
		eEventArray[1]	= FE_M7_DAUGHTER_studying_does_nails	fPercentArray[1] = fCHANCE_DAUGHTER_studying_does_nails
		eEventArray[2]	= FE_M7_DAUGHTER_sunbathing				fPercentArray[2] = fCHANCE_DAUGHTER_sunbathing
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Daughter_Afternoon_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    18:00 - 23:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Daughter_Evening_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_DAUGHTER_purging_in_the_bathroom	10.0
		CONST_FLOAT fCHANCE_DAUGHTER_screaming_to_dad			10.0
		CONST_FLOAT fCHANCE_DAUGHTER_sniffing_drugs				10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Sex_sounds_from_room		10.0
		CONST_FLOAT fCHANCE_DAUGHTER_on_phone_to_friends		10.0
		CONST_FLOAT fCHANCE_DAUGHTER_crying_over_a_guy			10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Fighting_with_Jimmy		20.0
		CONST_FLOAT fCHANCE_DAUGHTER_Going_out_in_her_car		10.0
		
		enumFamilyEvents	eEventArray[7]
		FLOAT 				fPercentArray[7]
		
		eEventArray[0]	= FE_M_DAUGHTER_purges_in_the_bathroom	fPercentArray[0] = fCHANCE_DAUGHTER_purging_in_the_bathroom
		eEventArray[1]	= FE_M_DAUGHTER_screaming_at_dad		fPercentArray[1] = fCHANCE_DAUGHTER_screaming_to_dad
		eEventArray[2]	= FE_M_DAUGHTER_sniffs_drugs_in_toilet	fPercentArray[2] = fCHANCE_DAUGHTER_sniffing_drugs
		eEventArray[3]	= FE_M_DAUGHTER_sex_sounds_from_room	fPercentArray[3] = fCHANCE_DAUGHTER_Sex_sounds_from_room
		eEventArray[4]	= FE_M_DAUGHTER_on_phone_to_friends		fPercentArray[4] = fCHANCE_DAUGHTER_on_phone_to_friends
		eEventArray[5]	= FE_M_DAUGHTER_crying_over_a_guy		fPercentArray[5] = fCHANCE_DAUGHTER_crying_over_a_guy
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_ME_TRACEY)
			eEventArray[6]	= FE_M_DAUGHTER_Going_out_in_her_car	fPercentArray[6] = fCHANCE_DAUGHTER_Going_out_in_her_car
		ELSE
			eEventArray[6]	= NO_FAMILY_EVENTS					fPercentArray[6] = 0.0
		ENDIF
		
//		eEventArray[7]	= FE_M_SON_Fighting_with_sister_A		fPercentArray[7] = fCHANCE_DAUGHTER_Fighting_with_Jimmy / 4.0
//		eEventArray[8]	= FE_M_SON_Fighting_with_sister_B		fPercentArray[8] = fCHANCE_DAUGHTER_Fighting_with_Jimmy / 4.0
//		eEventArray[9]	= FE_M_SON_Fighting_with_sister_C		fPercentArray[9] = fCHANCE_DAUGHTER_Fighting_with_Jimmy / 4.0
//		eEventArray[10]	= FE_M_SON_Fighting_with_sister_D		fPercentArray[10] = fCHANCE_DAUGHTER_Fighting_with_Jimmy / 4.0
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_DAUGHTER_studying_on_phone			30.0
		CONST_FLOAT fCHANCE_DAUGHTER_studying_does_nails		30.0
		CONST_FLOAT fCHANCE_DAUGHTER_on_phone_to_friends		30.0
		CONST_FLOAT fCHANCE_DAUGHTER_Out						10.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		eEventArray[0]	= FE_M7_DAUGHTER_studying_on_phone		fPercentArray[0] = fCHANCE_DAUGHTER_studying_on_phone
		eEventArray[1]	= FE_M7_DAUGHTER_studying_does_nails	fPercentArray[1] = fCHANCE_DAUGHTER_studying_does_nails
		eEventArray[2]	= FE_M_DAUGHTER_on_phone_to_friends		fPercentArray[2] = fCHANCE_DAUGHTER_on_phone_to_friends
		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = fCHANCE_DAUGHTER_Out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Daughter_Evening_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    00:00 - 05:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Daughter_Night_Event(enumFamilyEvents &eFamilyEvent)
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_DAUGHTER_Sex_sounds_from_room		10.0
		CONST_FLOAT fCHANCE_DAUGHTER_crying_over_a_guy			10.0
		CONST_FLOAT fCHANCE_DAUGHTER_Coming_home_drunk			30.0
		CONST_FLOAT fCHANCE_DAUGHTER_sleeping					30.0
		CONST_FLOAT fCHANCE_DAUGHTER_Out						20.0
		
		enumFamilyEvents	eEventArray[5]
		FLOAT 				fPercentArray[5]
		
		eEventArray[0]	= FE_M_DAUGHTER_sex_sounds_from_room	fPercentArray[0] = fCHANCE_DAUGHTER_Sex_sounds_from_room
		eEventArray[1]	= FE_M_DAUGHTER_crying_over_a_guy		fPercentArray[1] = fCHANCE_DAUGHTER_crying_over_a_guy
		eEventArray[2]	= FE_M_DAUGHTER_Coming_home_drunk		fPercentArray[2] = fCHANCE_DAUGHTER_Coming_home_drunk
		eEventArray[3]	= FE_M_DAUGHTER_sleeping				fPercentArray[3] = fCHANCE_DAUGHTER_sleeping
		eEventArray[4]	= NO_FAMILY_EVENTS						fPercentArray[4] = fCHANCE_DAUGHTER_Out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_DAUGHTER_studying_on_phone			30.0
		CONST_FLOAT fCHANCE_DAUGHTER_studying_does_nails		30.0
		CONST_FLOAT fCHANCE_DAUGHTER_sleeping					30.0
		CONST_FLOAT fCHANCE_DAUGHTER_Out						10.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		eEventArray[0]	= FE_M7_DAUGHTER_studying_on_phone		fPercentArray[0] = fCHANCE_DAUGHTER_studying_on_phone
		eEventArray[1]	= FE_M7_DAUGHTER_studying_does_nails	fPercentArray[1] = fCHANCE_DAUGHTER_studying_does_nails
		eEventArray[2]	= FE_M_DAUGHTER_sleeping				fPercentArray[2] = fCHANCE_DAUGHTER_sleeping
		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = fCHANCE_DAUGHTER_Out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_DAUGHTER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Daughter_Night_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE WIFE SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************


FUNC BOOL CanSchedule_FE_M_WIFE_playing_tennis()
	//"launcher_tennis"
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-773.723145,168.832703,65>>, <<-772.290100,136.815643,68>>, 22)
		CPRINTLN(DEBUG_FAMILY, "Player is on the tennis court. Not scheduling wife to play...")
		RETURN FALSE
	ENDIF
	CPRINTLN(DEBUG_FAMILY, "Player is NOT on the tennis court. Wife is allowed to play.")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Wife_Morning_Event(enumFamilyEvents &eFamilyEvent)
	
	IF Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_WIFE_screams_at_Mexican_maid		10.0
		CONST_FLOAT fCHANCE_WIFE_in_face_mask					20.0
		CONST_FLOAT fCHANCE_WIFE_doing_yoga_OR_Tennis			20.0
//		CONST_FLOAT fCHANCE_WIFE_getting_nails_done				20.0
		CONST_FLOAT fCHANCE_WIFE_Out							10.0
		CONST_FLOAT fCHANCE_WIFE_leaving_in_car					10.0
		
		enumFamilyEvents	eEventArray[7-1-1]
		FLOAT 				fPercentArray[7-1-1]
		
		eEventArray[0]	= FE_M_WIFE_screams_at_mexmaid			fPercentArray[0] = fCHANCE_WIFE_screams_at_Mexican_maid
		eEventArray[1]	= FE_M2_WIFE_in_face_mask				fPercentArray[1] = fCHANCE_WIFE_in_face_mask
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
			
			IF CanSchedule_FE_M_WIFE_playing_tennis()
				eEventArray[2]		= FE_M_WIFE_playing_tennis
				fPercentArray[2]	= fCHANCE_WIFE_doing_yoga_OR_Tennis
			ELSE
				eEventArray[2]		= NO_FAMILY_EVENTS
				fPercentArray[2]	= 0.0
			ENDIF
		ELSE
			eEventArray[2]			= FE_M2_WIFE_doing_yoga
			fPercentArray[2]		= fCHANCE_WIFE_doing_yoga_OR_Tennis
		ENDIF
//		eEventArray[3]	= FE_M_WIFE_getting_nails_done			fPercentArray[3] = fCHANCE_WIFE_getting_nails_done
		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = fCHANCE_WIFE_Out
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_5)		//865391
//			eEventArray[4]	= FE_M_WIFE_leaving_in_car_v2		fPercentArray[4] = fCHANCE_WIFE_leaving_in_car / 2.0
			eEventArray[6-1-1]	= FE_M_WIFE_leaving_in_car			fPercentArray[6-1-1] = fCHANCE_WIFE_leaving_in_car
		ELSE
//			eEventArray[5-1-1]	= NO_FAMILY_EVENTS					fPercentArray[5-1-1]		= 0.0
			eEventArray[6-1-1]	= NO_FAMILY_EVENTS					fPercentArray[6-1-1]		= 0.0
		ENDIF
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_WIFE_Making_juice				25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_daughter		25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_son			25.0
		CONST_FLOAT fCHANCE_WIFE_on_phone					25.0
		
		CONST_FLOAT fCHANCE_WIFE_in_face_mask				20.0
		CONST_FLOAT fCHANCE_WIFE_doing_yoga_OR_Tennis		20.0
		
		enumFamilyEvents	eEventArray[6]
		FLOAT 				fPercentArray[6]
		
		eEventArray[0] = FE_M7_WIFE_Making_juice			fPercentArray[0] = fCHANCE_WIFE_Making_juice
		eEventArray[1] = FE_M7_WIFE_shopping_with_daughter	fPercentArray[1] = fCHANCE_WIFE_shopping_with_daughter
//		eEventArray[2] = FE_M7_WIFE_shopping_with_son		fPercentArray[2] = fCHANCE_WIFE_shopping_with_son
		eEventArray[2] = NO_FAMILY_EVENTS					fPercentArray[2] = 0.0
//		eEventArray[3] = FE_M7_WIFE_on_phone				fPercentArray[3] = fCHANCE_WIFE_on_phone
		eEventArray[3] = NO_FAMILY_EVENTS					fPercentArray[3] = 0.0
		eEventArray[4] = FE_M7_WIFE_in_face_mask			fPercentArray[4] = fCHANCE_WIFE_in_face_mask
		eEventArray[5] = FE_M7_WIFE_doing_yoga				fPercentArray[5] = fCHANCE_WIFE_doing_yoga_OR_Tennis
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Wife_Morning_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    12:00 - 17:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Wife_Afternoon_Event(enumFamilyEvents &eFamilyEvent)
	
	IF Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_WIFE_screams_at_Mexican_maid		10.0
		CONST_FLOAT fCHANCE_WIFE_with_shopping_bags_enter		10.0
		CONST_FLOAT fCHANCE_WIFE_gets_drink_in_kitchen			10.0
		CONST_FLOAT fCHANCE_WIFE_sunbathing						20.0
		CONST_FLOAT fCHANCE_WIFE_doing_yoga_OR_Tennis			10.0
//		CONST_FLOAT fCHANCE_WIFE_getting_botox_done				5.0		//20.0
		CONST_FLOAT fCHANCE_WIFE_passed_out_SOFA				15.0	//10.0
		CONST_FLOAT fCHANCE_WIFE_Out							10.0
		
		enumFamilyEvents	eEventArray[7]
		FLOAT 				fPercentArray[7]
		
		eEventArray[0]	= FE_M_WIFE_screams_at_mexmaid			fPercentArray[0]	= fCHANCE_WIFE_screams_at_Mexican_maid
		eEventArray[1]	= FE_M2_WIFE_with_shopping_bags_enter	fPercentArray[1]	= fCHANCE_WIFE_with_shopping_bags_enter
		eEventArray[2]	= FE_M_WIFE_gets_drink_in_kitchen		fPercentArray[2]	= fCHANCE_WIFE_gets_drink_in_kitchen
		eEventArray[3]	= FE_M2_WIFE_sunbathing					fPercentArray[3]	= fCHANCE_WIFE_sunbathing
//		eEventArray[4]	= FE_M_WIFE_getting_botox_done			fPercentArray[4]	= fCHANCE_WIFE_getting_botox_done
		eEventArray[4]	= FE_M2_WIFE_passed_out_SOFA			fPercentArray[4]	= fCHANCE_WIFE_passed_out_SOFA
		eEventArray[6-1]	= NO_FAMILY_EVENTS					fPercentArray[6-1]	= fCHANCE_WIFE_Out
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
			IF CanSchedule_FE_M_WIFE_playing_tennis()
				eEventArray[7-1]	= FE_M_WIFE_playing_tennis
				fPercentArray[7-1]	= fCHANCE_WIFE_doing_yoga_OR_Tennis
			ELSE
				eEventArray[7-1]		= NO_FAMILY_EVENTS
				fPercentArray[7-1]	= 0.0
			ENDIF
			
		ELSE
			eEventArray[7-1]		= FE_M2_WIFE_doing_yoga
			fPercentArray[7-1]		= fCHANCE_WIFE_doing_yoga_OR_Tennis
		ENDIF
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_WIFE_Making_juice				25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_daughter		25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_son			25.0
		CONST_FLOAT fCHANCE_WIFE_on_phone					25.0
		CONST_FLOAT fCHANCE_WIFE_sunbathing					20.0
		CONST_FLOAT fCHANCE_WIFE_doing_yoga_OR_Tennis		10.0
		CONST_FLOAT fCHANCE_WIFE_passed_out_SOFA			15.0	//10.0
		CONST_FLOAT fCHANCE_WIFE_with_shopping_bags_enter	10.0
		
		enumFamilyEvents	eEventArray[8]
		FLOAT 				fPercentArray[8]
		
		eEventArray[0]	= FE_M7_WIFE_Making_juice				fPercentArray[0] = fCHANCE_WIFE_Making_juice
		eEventArray[1]	= FE_M7_WIFE_shopping_with_daughter		fPercentArray[1] = fCHANCE_WIFE_shopping_with_daughter
//		eEventArray[2]	= FE_M7_WIFE_shopping_with_son			fPercentArray[2] = fCHANCE_WIFE_shopping_with_son
		eEventArray[2]	= NO_FAMILY_EVENTS						fPercentArray[2] = 0.0
//		eEventArray[3]	= FE_M7_WIFE_on_phone					fPercentArray[3] = fCHANCE_WIFE_on_phone
		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = 0.0
		eEventArray[4]	= FE_M7_WIFE_doing_yoga					fPercentArray[5] = fCHANCE_WIFE_doing_yoga_OR_Tennis
		eEventArray[5]	= FE_M7_WIFE_passed_out_SOFA			fPercentArray[5] = fCHANCE_WIFE_passed_out_SOFA
		eEventArray[6]	= FE_M7_WIFE_sunbathing					fPercentArray[6] = fCHANCE_WIFE_sunbathing
		eEventArray[7]	= FE_M7_WIFE_with_shopping_bags_enter	fPercentArray[7] = fCHANCE_WIFE_with_shopping_bags_enter
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Wife_Afternoon_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    18:00 - 23:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Wife_Evening_Event(enumFamilyEvents &eFamilyEvent)
	
	IF Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_WIFE_in_face_mask					20.0
		CONST_FLOAT fCHANCE_WIFE_passed_out_SOFA				20.0
		CONST_FLOAT fCHANCE_WIFE_Out							25.0
		CONST_FLOAT fCHANCE_WIFE_screaming_at_son				 7.5
		CONST_FLOAT fCHANCE_WIFE_screaming_at_daughter			 7.5
		CONST_FLOAT fCHANCE_WIFE_phones_man_OR_therapist		10.0
		
		enumFamilyEvents	eEventArray[8]
		FLOAT 				fPercentArray[8]
		
		eEventArray[0]	= FE_M2_WIFE_in_face_mask				fPercentArray[0] = fCHANCE_WIFE_in_face_mask
		eEventArray[1]	= FE_M2_WIFE_passed_out_SOFA			fPercentArray[1] = fCHANCE_WIFE_passed_out_SOFA
		eEventArray[2]	= NO_FAMILY_EVENTS						fPercentArray[2] = fCHANCE_WIFE_Out
//		eEventArray[3]	= FE_M_WIFE_screaming_at_son_P1			fPercentArray[3] = fCHANCE_WIFE_screaming_at_son / 3.0
		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = 0.0
		eEventArray[4]	= FE_M_WIFE_screaming_at_son_P2			fPercentArray[4] = fCHANCE_WIFE_screaming_at_son / 3.0
		eEventArray[5]	= FE_M_WIFE_screaming_at_son_P3			fPercentArray[5] = fCHANCE_WIFE_screaming_at_son / 3.0
		
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)	//tracey leaves after fam4
			eEventArray[6]	= FE_M_WIFE_screaming_at_daughter	fPercentArray[6] = fCHANCE_WIFE_screaming_at_daughter
		ELSE
			eEventArray[6]	= NO_FAMILY_EVENTS					fPercentArray[6] = 0.0
		ENDIF
		
		eEventArray[7]	= FE_M2_WIFE_phones_man_OR_therapist		fPercentArray[7] = fCHANCE_WIFE_phones_man_OR_therapist
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_WIFE_Making_juice				25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_daughter		25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_son			25.0
		CONST_FLOAT fCHANCE_WIFE_on_phone					25.0
		CONST_FLOAT fCHANCE_WIFE_in_face_mask				20.0
		CONST_FLOAT fCHANCE_WIFE_passed_out_SOFA			20.0
		CONST_FLOAT fCHANCE_WIFE_phones_man_OR_therapist	10.0
		
		enumFamilyEvents	eEventArray[7]
		FLOAT 				fPercentArray[7]
		
		eEventArray[0] = FE_M7_WIFE_Making_juice			fPercentArray[0] = fCHANCE_WIFE_Making_juice
		eEventArray[1] = FE_M7_WIFE_shopping_with_daughter	fPercentArray[1] = fCHANCE_WIFE_shopping_with_daughter
//		eEventArray[2] = FE_M7_WIFE_shopping_with_son		fPercentArray[2] = fCHANCE_WIFE_shopping_with_son
		eEventArray[2] = NO_FAMILY_EVENTS					fPercentArray[2] = 0.0
//		eEventArray[3] = FE_M7_WIFE_on_phone				fPercentArray[3] = fCHANCE_WIFE_on_phone
		eEventArray[3] = NO_FAMILY_EVENTS					fPercentArray[3] = 0.0
		eEventArray[4] = FE_M7_WIFE_in_face_mask			fPercentArray[4] = fCHANCE_WIFE_in_face_mask
		eEventArray[5] = FE_M7_WIFE_passed_out_SOFA			fPercentArray[5] = fCHANCE_WIFE_passed_out_SOFA
		eEventArray[6] = FE_M7_WIFE_phones_man_OR_therapist	fPercentArray[6] = fCHANCE_WIFE_phones_man_OR_therapist
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Wife_Evening_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    00:00 - 05:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Wife_Night_Event(enumFamilyEvents &eFamilyEvent)
	
	IF Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		
		CONST_FLOAT fCHANCE_WIFE_using_vibrator					10.0
		CONST_FLOAT fCHANCE_WIFE_passed_out_BED					10.0
		CONST_FLOAT fCHANCE_WIFE_sleeping						70.0
		CONST_FLOAT fCHANCE_WIFE_Out							10.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		#IF NOT IS_JAPANESE_BUILD
		eEventArray[0]	= FE_M2_WIFE_using_vibrator			fPercentArray[0] = fCHANCE_WIFE_using_vibrator
		#ENDIF
		#IF IS_JAPANESE_BUILD
		eEventArray[0]	= FE_M2_WIFE_sleeping				fPercentArray[0] = fCHANCE_WIFE_using_vibrator
		#ENDIF
		
		eEventArray[1]	= FE_M_WIFE_passed_out_BED			fPercentArray[1] = fCHANCE_WIFE_passed_out_BED
		eEventArray[2]	= FE_M2_WIFE_sleeping				fPercentArray[2] = fCHANCE_WIFE_sleeping
		eEventArray[3]	= NO_FAMILY_EVENTS					fPercentArray[3] = fCHANCE_WIFE_Out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)	//#1153884
			eFamilyEvent = FE_M_FAMILY_MIC4_locked_in_room
			RETURN TRUE
		ENDIF
		
		CONST_FLOAT fCHANCE_WIFE_Making_juice				25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_daughter		25.0
		CONST_FLOAT fCHANCE_WIFE_shopping_with_son			25.0
		CONST_FLOAT fCHANCE_WIFE_on_phone					25.0
		CONST_FLOAT fCHANCE_WIFE_using_vibrator				10.0
		CONST_FLOAT fCHANCE_WIFE_sleeping					20.0
		
		enumFamilyEvents	eEventArray[6]
		FLOAT 				fPercentArray[6]
		
		eEventArray[0]	= FE_M7_WIFE_Making_juice			fPercentArray[0] = fCHANCE_WIFE_Making_juice
		eEventArray[1]	= FE_M7_WIFE_shopping_with_daughter	fPercentArray[1] = fCHANCE_WIFE_shopping_with_daughter
//		eEventArray[2]	= FE_M7_WIFE_shopping_with_son		fPercentArray[2] = fCHANCE_WIFE_shopping_with_son
		eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = 0.0
//		eEventArray[3]	= FE_M7_WIFE_on_phone				fPercentArray[3] = fCHANCE_WIFE_on_phone
		eEventArray[3]	= NO_FAMILY_EVENTS					fPercentArray[3] = 0.0
		#IF NOT IS_JAPANESE_BUILD
		eEventArray[4]	= FE_M7_WIFE_using_vibrator			fPercentArray[4] = fCHANCE_WIFE_using_vibrator
		#ENDIF
		#IF IS_JAPANESE_BUILD
		eEventArray[4]	= FE_M7_WIFE_sleeping				fPercentArray[4] = fCHANCE_WIFE_using_vibrator
		#ENDIF
		eEventArray[5]	= FE_M7_WIFE_sleeping				fPercentArray[5] = fCHANCE_WIFE_sleeping
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_WIFE,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Wife_Night_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE MEXMAID SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_MexMaid_Morning_Event(enumFamilyEvents &eFamilyEvent)
	
	IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)				//#815139
		IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0*60.0)	//#1167871
			eFamilyEvent	= FE_M_MEXMAID_MIC4_clean_window
			RETURN TRUE
		ENDIF
	ENDIF
		
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		
		CONST_FLOAT fCHANCE_MEXMAID_cleaning_up					20.0
		CONST_FLOAT fCHANCE_MEXMAID_does_the_dishes				20.0
//		CONST_FLOAT fCHANCE_MEXMAID_cooking_for_son				20.0
//		CONST_FLOAT fCHANCE_MEXMAID_watching_TV					20.0
		CONST_FLOAT fCHANCE_MEXMAID_out							10.0
		
		enumFamilyEvents	eEventArray[5-1-1]
		FLOAT 				fPercentArray[5-1-1]
		
		eEventArray[0]	= FE_M2_MEXMAID_cleans_booze_pot_other	fPercentArray[0]	= fCHANCE_MEXMAID_cleaning_up
		eEventArray[1]	= FE_M_MEXMAID_does_the_dishes			fPercentArray[1]	= fCHANCE_MEXMAID_does_the_dishes
//		eEventArray[2]	= FE_M_MEXMAID_cooking_for_son			fPercentArray[2]	= fCHANCE_MEXMAID_cooking_for_son
//		eEventArray[2]	= FE_M_MEXMAID_watching_TV			fPercentArray[2]	= fCHANCE_MEXMAID_watching_TV
		eEventArray[4-1-1]	= NO_FAMILY_EVENTS					fPercentArray[4-1-1]	= fCHANCE_MEXMAID_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_MEXMAID,
				eEventArray, fPercentArray,
				eFamilyEvent)
		
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		
		CONST_FLOAT fCHANCE_MEXMAID_cleaning_up					20.0
		CONST_FLOAT fCHANCE_MEXMAID_does_the_dishes				20.0
//		CONST_FLOAT fCHANCE_MEXMAID_cooking_for_son				20.0
		CONST_FLOAT fCHANCE_MEXMAID_watching_TV					20.0
		CONST_FLOAT fCHANCE_MEXMAID_out							20.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		eEventArray[0]	= FE_M7_MEXMAID_cleans_booze_pot_other	fPercentArray[0]	= fCHANCE_MEXMAID_cleaning_up
		eEventArray[1]	= FE_M_MEXMAID_does_the_dishes			fPercentArray[1]	= fCHANCE_MEXMAID_does_the_dishes
//		eEventArray[2]	= FE_M_MEXMAID_cooking_for_son			fPercentArray[2]	= fCHANCE_MEXMAID_cooking_for_son
//		eEventArray[2]	= FE_M_MEXMAID_watching_TV			fPercentArray[2]	= fCHANCE_MEXMAID_watching_TV
		eEventArray[4-1-1]	= NO_FAMILY_EVENTS					fPercentArray[4-1-1]	= fCHANCE_MEXMAID_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_MEXMAID,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_MexMaid_Morning_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    12:00 - 17:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_MexMaid_Afternoon_Event(enumFamilyEvents &eFamilyEvent)
	
	IF (g_eLastMissionPassed = SP_MISSION_MICHAEL_4)				//#815139
		IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0*60.0)	//#1167871
			eFamilyEvent	= FE_M_MEXMAID_MIC4_clean_window
			RETURN TRUE
		ENDIF
	ENDIF
		
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_MEXMAID_cleaning_up					25.0	//20.0
//		CONST_FLOAT fCHANCE_MEXMAID_watching_TV					25.0	//20.0
		CONST_FLOAT fCHANCE_MEXMAID_stealing_stuff				25.0	//20.0
		CONST_FLOAT fCHANCE_MEXMAID_out							12.5	//20.0
		
		enumFamilyEvents	eEventArray[4]
		FLOAT 				fPercentArray[4]
		
		eEventArray[0]	= FE_M2_MEXMAID_cleans_booze_pot_other	fPercentArray[0] = fCHANCE_MEXMAID_cleaning_up
//		eEventArray[1]	= FE_M_MEXMAID_watching_TV				fPercentArray[1] = fCHANCE_MEXMAID_watching_TV
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)		//1416624
			eEventArray[2-1]	= FE_M_MEXMAID_stealing_stuff	fPercentArray[2-1] = fCHANCE_MEXMAID_stealing_stuff
		ELSE
			eEventArray[2-1]	= NO_FAMILY_EVENTS				fPercentArray[2-1] = 0.0
		ENDIF
		eEventArray[2]	= NO_FAMILY_EVENTS						fPercentArray[2] = fCHANCE_MEXMAID_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_MEXMAID,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_MEXMAID_cleaning_up					25.0	//20.0
//		CONST_FLOAT fCHANCE_MEXMAID_watching_TV					25.0	//20.0
		CONST_FLOAT fCHANCE_MEXMAID_stealing_stuff				25.0	//20.0
		CONST_FLOAT fCHANCE_MEXMAID_out							12.5	//20.0
		
		enumFamilyEvents	eEventArray[3]
		FLOAT 				fPercentArray[3]
		
		eEventArray[0]	= FE_M7_MEXMAID_cleans_booze_pot_other	fPercentArray[0] = fCHANCE_MEXMAID_cleaning_up
//		eEventArray[1]	= FE_M_MEXMAID_watching_TV				fPercentArray[1] = fCHANCE_MEXMAID_watching_TV
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)		//1416624
			eEventArray[2-1]	= FE_M_MEXMAID_stealing_stuff	fPercentArray[2-1] = fCHANCE_MEXMAID_stealing_stuff
		ELSE
			eEventArray[2-1]	= NO_FAMILY_EVENTS				fPercentArray[2-1] = 0.0
		ENDIF
		
		eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = fCHANCE_MEXMAID_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_MEXMAID,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_MexMaid_Afternoon_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE GARDENER SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Michael_Gardener_Morning_Event(enumFamilyEvents &eFamilyEvent)
	IF (g_eLastMissionPassed = SP_MISSION_FAMILY_3)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
	IF (thisMichaelScheduleStage = MSS_M2_WithFamily)
		CONST_FLOAT fCHANCE_GARDENER_with_leaf_blower		12.5
		CONST_FLOAT fCHANCE_GARDENER_planting_flowers		12.5
		CONST_FLOAT fCHANCE_GARDENER_trimming_hedges		12.5
		CONST_FLOAT fCHANCE_GARDENER_mowing_lawn			12.5
		CONST_FLOAT fCHANCE_GARDENER_watering_flowers		12.5
		CONST_FLOAT fCHANCE_GARDENER_spraying_for_weeds		12.5
		CONST_FLOAT fCHANCE_GARDENER_cleaning_pool			12.5
		CONST_FLOAT fCHANCE_GARDENER_on_phone				12.5
		CONST_FLOAT fCHANCE_GARDENER_smoking_weed			12.0
		CONST_FLOAT fCHANCE_GARDENER_out					 0.5
		
		enumFamilyEvents	eEventArray[8]
		FLOAT 				fPercentArray[8]
		
		eEventArray[0]	= FE_M_GARDENER_with_leaf_blower	fPercentArray[0] = fCHANCE_GARDENER_with_leaf_blower
		eEventArray[1]	= FE_M_GARDENER_planting_flowers	fPercentArray[1] = fCHANCE_GARDENER_planting_flowers
		eEventArray[2]	= FE_M_GARDENER_mowing_lawn			fPercentArray[2] = fCHANCE_GARDENER_mowing_lawn
		eEventArray[3]	= FE_M_GARDENER_watering_flowers	fPercentArray[3] = fCHANCE_GARDENER_watering_flowers
		
		IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)
			eEventArray[4]	= FE_M_GARDENER_cleaning_pool	fPercentArray[4] = fCHANCE_GARDENER_cleaning_pool
		ELSE
			eEventArray[4]	= NO_FAMILY_EVENTS				fPercentArray[4] = 0.0
		ENDIF
		
		eEventArray[5]	= FE_M_GARDENER_on_phone			fPercentArray[6-1] = fCHANCE_GARDENER_on_phone
		eEventArray[6]	= FE_M_GARDENER_smoking_weed		fPercentArray[7-1] = fCHANCE_GARDENER_smoking_weed
		eEventArray[7]	= NO_FAMILY_EVENTS					fPercentArray[7] = fCHANCE_GARDENER_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_GARDENER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELIF (thisMichaelScheduleStage = MSS_M4_WithoutFamily)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF ((thisMichaelScheduleStage = MSS_M6_Exiled) OR (thisMichaelScheduleStage = MSS_M9_killedMichael))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ELIF (thisMichaelScheduleStage = MSS_M7_ReunitedWithFamily)
		CONST_FLOAT fCHANCE_GARDENER_with_leaf_blower		12.5
		CONST_FLOAT fCHANCE_GARDENER_planting_flowers		12.5
		CONST_FLOAT fCHANCE_GARDENER_trimming_hedges		12.5
		CONST_FLOAT fCHANCE_GARDENER_mowing_lawn			12.5
		CONST_FLOAT fCHANCE_GARDENER_watering_flowers		12.5
		CONST_FLOAT fCHANCE_GARDENER_spraying_for_weeds		12.5
		CONST_FLOAT fCHANCE_GARDENER_cleaning_pool			12.5
		CONST_FLOAT fCHANCE_GARDENER_on_phone				12.5
		CONST_FLOAT fCHANCE_GARDENER_smoking_weed			12.0
		CONST_FLOAT fCHANCE_GARDENER_out					 0.5
		
		enumFamilyEvents	eEventArray[8]
		FLOAT 				fPercentArray[8]
		
		eEventArray[0]	= FE_M_GARDENER_with_leaf_blower	fPercentArray[0] = fCHANCE_GARDENER_with_leaf_blower
		eEventArray[1]	= FE_M_GARDENER_planting_flowers	fPercentArray[1] = fCHANCE_GARDENER_planting_flowers
//		eEventArray[2]	= FE_M_GARDENER_trimming_hedges		fPercentArray[2] = fCHANCE_GARDENER_trimming_hedges
		eEventArray[2]	= FE_M_GARDENER_mowing_lawn			fPercentArray[2] = fCHANCE_GARDENER_mowing_lawn
		eEventArray[3]	= FE_M_GARDENER_watering_flowers	fPercentArray[3] = fCHANCE_GARDENER_watering_flowers
		eEventArray[4]	= FE_M_GARDENER_cleaning_pool		fPercentArray[4] = fCHANCE_GARDENER_cleaning_pool
		eEventArray[6-1]	= FE_M_GARDENER_on_phone			fPercentArray[6-1] = fCHANCE_GARDENER_on_phone
		eEventArray[7-1]	= FE_M_GARDENER_smoking_weed		fPercentArray[7-1] = fCHANCE_GARDENER_smoking_weed
		eEventArray[7]	= NO_FAMILY_EVENTS					fPercentArray[7] = fCHANCE_GARDENER_out
		
		RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_MICHAEL_GARDENER,
				eEventArray, fPercentArray,
				eFamilyEvent)
	ELSE
		SCRIPT_ASSERT("invalid thisMichaelScheduleStage in PRIVATE_Get_Desired_Michael_Gardener_Morning_Event()")
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE AUNT SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Franklin_Aunt_Event(enumFamilyEvents &eFamilyEvent)
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_ARMENIAN_1)		//1350535
	OR (g_eLastMissionPassed = SP_MISSION_ARMENIAN_1)	//555813
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	//#1582147
	IF (g_eSelectedPlayerCharScene = PR_SCENE_F0_TANISHAFIGHT)
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		OR Is_Player_Timetable_Scene_In_Progress()
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		eFamilyEvent = FE_F_AUNT_returned_to_aunts
		RETURN TRUE
	ENDIF
	
	CONST_FLOAT fCHANCE_F_AUNT_pelvic_floor_exercises		25.0
	CONST_FLOAT fCHANCE_F_AUNT_in_face_mask					25.0
	CONST_FLOAT fCHANCE_F_AUNT_watching_TV					25.0
	CONST_FLOAT fCHANCE_F_AUNT_listens_to_selfhelp_tapes	25.0
	
	enumFamilyEvents	eEventArray[4]
	FLOAT 				fPercentArray[4]
	
	eEventArray[0]	= FE_F_AUNT_pelvic_floor_exercises		fPercentArray[0] = fCHANCE_F_AUNT_pelvic_floor_exercises
	eEventArray[1]	= FE_F_AUNT_in_face_mask				fPercentArray[1] = fCHANCE_F_AUNT_in_face_mask
	eEventArray[2]	= FE_F_AUNT_watching_TV					fPercentArray[2] = fCHANCE_F_AUNT_watching_TV
	eEventArray[3]	= FE_F_AUNT_listens_to_selfhelp_tapes_x	fPercentArray[3] = fCHANCE_F_AUNT_listens_to_selfhelp_tapes
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_FRANKLIN_AUNT,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

/*
// *******************************************************************************************
//	FAMILY PRIVATE LAMAR SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Franklin_Lamar_Event(enumFamilyEvents &eFamilyEvent)
	
	IF g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH] = FAMILY_MEMBER_BUSY
		eFamilyEvent = FAMILY_MEMBER_BUSY
		RETURN FALSE
	ENDIF
	
	IF (g_eLastMissionPassed = SP_MISSION_LAMAR)
		eFamilyEvent = FE_F_LAMAR_and_STRETCH_wandering
		RETURN TRUE
	ENDIF
	
	IF Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)	//1128582
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_chill_outside	25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_bbq_outside		25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_arguing			25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_shout_at_cops	25.0
	
	enumFamilyEvents	eEventArray[4]
	FLOAT 				fPercentArray[4]
	
	eEventArray[0]	= FE_F_LAMAR_and_STRETCH_chill_outside	fPercentArray[0] = fCHANCE_F_LAMAR_and_STRETCH_chill_outside
	eEventArray[1]	= FE_F_LAMAR_and_STRETCH_bbq_outside	fPercentArray[1] = fCHANCE_F_LAMAR_and_STRETCH_bbq_outside
	eEventArray[2]	= FE_F_LAMAR_and_STRETCH_arguing		fPercentArray[2] = fCHANCE_F_LAMAR_and_STRETCH_arguing
	
//	IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)		//#1097888
//		eEventArray[3]	= FE_F_LAMAR_and_STRETCH_shout_at_cops	fPercentArray[3] = fCHANCE_F_LAMAR_and_STRETCH_shout_at_cops
//	ELSE
//		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = 0.0
//	ENDIF
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_FRANKLIN_LAMAR,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE STRETCH SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Franklin_Stretch_Event(enumFamilyEvents &eFamilyEvent)
	
	IF g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR] = FAMILY_MEMBER_BUSY
		eFamilyEvent = FAMILY_MEMBER_BUSY
		RETURN FALSE
	ENDIF
	
	IF (g_eLastMissionPassed = SP_MISSION_LAMAR)
		eFamilyEvent = FE_F_LAMAR_and_STRETCH_wandering
		RETURN TRUE
	ENDIF
	
	IF Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)	//1128582
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_chill_outside	25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_bbq_outside		25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_arguing			25.0
	CONST_FLOAT fCHANCE_F_LAMAR_and_STRETCH_shout_at_cops	25.0
	
	enumFamilyEvents	eEventArray[5]
	FLOAT 				fPercentArray[5]
	
	eEventArray[0]	= FE_F_LAMAR_and_STRETCH_chill_outside		fPercentArray[0] = fCHANCE_F_LAMAR_and_STRETCH_chill_outside
	eEventArray[1]	= FE_F_LAMAR_and_STRETCH_bbq_outside		fPercentArray[1] = fCHANCE_F_LAMAR_and_STRETCH_bbq_outside
	eEventArray[2]	= FE_F_LAMAR_and_STRETCH_arguing			fPercentArray[2] = fCHANCE_F_LAMAR_and_STRETCH_arguing
	
//	IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)		//#1097888
//		eEventArray[3]	= FE_F_LAMAR_and_STRETCH_shout_at_cops	fPercentArray[3] = fCHANCE_F_LAMAR_and_STRETCH_shout_at_cops
//	ELSE
//		eEventArray[3]	= NO_FAMILY_EVENTS						fPercentArray[3] = 0.0
//	ENDIF
	
	eEventArray[4]	= FE_F_LAMAR_and_STRETCH_wandering			fPercentArray[4] = 1.0
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_FRANKLIN_STRETCH,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC
*/

// *******************************************************************************************
//	FAMILY PRIVATE RON 0 SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_0_Ron_Event(enumFamilyEvents &eFamilyEvent)
	
	//#1494189
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCHANCE_RON_monitoring_police_frequency				16.6
	CONST_FLOAT fCHANCE_RON_listens_to_radio_broadcast				16.6
	CONST_FLOAT fCHANCE_RON_ranting_about_government				16.6
	CONST_FLOAT fCHANCE_RON_smoking_crystal							16.6
	CONST_FLOAT fCHANCE_RON_drinks_moonshine_from_a_jar				16.6
	CONST_FLOAT fCHANCE_RON_stares_through_binoculars				16.6
	
	CONST_FLOAT fCHANCE_RONEX_outside_looking_lonely				20.0
	CONST_FLOAT fCHANCE_RONEX_trying_to_pick_up_signals				20.0
	CONST_FLOAT fCHANCE_RONEX_working_a_moonshine_sill				20.0
	CONST_FLOAT fCHANCE_RONEX_doing_target_practice					20.0
	CONST_FLOAT fCHANCE_RONEX_conspiracies_boring_Michael			20.0
	
	enumFamilyEvents	eEventArray[6]
	FLOAT 				fPercentArray[6]
	
	
	IF g_eLastMissionPassed = SP_MISSION_TREVOR_1			//#1492828
		IF HasTimeToWaitPassedFromLastPassedMission(60.0)
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		ENDIF
	ENDIF

	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		
		IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		ENDIF
		
		eEventArray[0]	= FE_T0_RON_monitoring_police_frequency		fPercentArray[0] = fCHANCE_RON_monitoring_police_frequency
		eEventArray[1]	= FE_T0_RON_listens_to_radio_broadcast		fPercentArray[1] = fCHANCE_RON_listens_to_radio_broadcast
		eEventArray[2]	= FE_T0_RON_ranting_about_government_LAYING	fPercentArray[2] = fCHANCE_RON_ranting_about_government
		eEventArray[3]	= FE_T0_RON_smoking_crystal					fPercentArray[3] = fCHANCE_RON_smoking_crystal
		eEventArray[4]	= FE_T0_RON_drinks_moonshine_from_a_jar		fPercentArray[4] = fCHANCE_RON_drinks_moonshine_from_a_jar
		
		INT iGET_CLOCK_HOURS = GET_CLOCK_HOURS()
		IF (iGET_CLOCK_HOURS > 20 or iGET_CLOCK_HOURS < 6)
			eEventArray[5]	= FE_T0_RON_stares_through_binoculars	fPercentArray[5] = fCHANCE_RON_stares_through_binoculars
		ELSE
			eEventArray[5]	= NO_FAMILY_EVENTS						fPercentArray[5] = 0.0
		ENDIF
	ELSE
		
		IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
			IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_2)
				eFamilyEvent = FE_T0_RONEX_outside_looking_lonely
				RETURN TRUE
			ENDIF
			
			eEventArray[0]	= FE_T0_RONEX_outside_looking_lonely	fPercentArray[0] = fCHANCE_RONEX_outside_looking_lonely
		ELSE
			INT iGET_CLOCK_HOURS = GET_CLOCK_HOURS()
			IF (iGET_CLOCK_HOURS > 20 or iGET_CLOCK_HOURS < 6)
				eEventArray[0]	= FE_T0_RON_stares_through_binoculars	fPercentArray[0] = fCHANCE_RON_stares_through_binoculars
			ELSE
				eEventArray[0]	= NO_FAMILY_EVENTS						fPercentArray[0] = 0.0
			ENDIF
		ENDIF
		
		eEventArray[1]	= FE_T0_RONEX_trying_to_pick_up_signals		fPercentArray[1] = fCHANCE_RONEX_trying_to_pick_up_signals
		eEventArray[2]	= FE_T0_RONEX_working_a_moonshine_sill		fPercentArray[2] = fCHANCE_RONEX_working_a_moonshine_sill
		eEventArray[3]	= FE_T0_RONEX_doing_target_practice			fPercentArray[3] = fCHANCE_RONEX_doing_target_practice
		
		eEventArray[4]	= NO_FAMILY_EVENTS							fPercentArray[4] = 0.0
		eEventArray[5]	= NO_FAMILY_EVENTS							fPercentArray[5] = 0.0
	ENDIF
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_TREVOR_0_RON,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE MICHAEL 1 SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_0_Michael_Event(enumFamilyEvents &eFamilyEvent)
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF NOT ScheduleEventForThisChar(CHAR_MICHAEL)
		eFamilyEvent = FAMILY_MEMBER_BUSY
		RETURN FALSE
	ENDIF
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCHANCE_MICHAEL_depressed_head_in_hands			25.0
	CONST_FLOAT fCHANCE_MICHAEL_sunbathing						25.0
	CONST_FLOAT fCHANCE_MICHAEL_drinking_beer					25.0
	CONST_FLOAT fCHANCE_MICHAEL_on_phone_to_therapist			25.0
	
	enumFamilyEvents	eEventArray[4]
	FLOAT 				fPercentArray[4]
	
	eEventArray[0]	= FE_T0_MICHAEL_depressed_head_in_hands		fPercentArray[0] = fCHANCE_MICHAEL_depressed_head_in_hands
	eEventArray[1]	= FE_T0_MICHAEL_sunbathing					
	IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sLimitedWardrobeItems[LPW_MICHAEL_COUNTRYSIDE].iItemBitset[COMP_TYPE_OUTFIT][(ENUM_TO_INT(OUTFIT_P0_YOGA_FLIP_FLOPS)/32)], (ENUM_TO_INT(OUTFIT_P0_YOGA_FLIP_FLOPS)%32))
		fPercentArray[1] = fCHANCE_MICHAEL_sunbathing * 2.0
	ELSE
		fPercentArray[1] = fCHANCE_MICHAEL_sunbathing
	ENDIF
	
	eEventArray[2]	= FE_T0_MICHAEL_drinking_beer				fPercentArray[2] = fCHANCE_MICHAEL_drinking_beer
	eEventArray[3]	= FE_T0_MICHAEL_on_phone_to_therapist		fPercentArray[3] = fCHANCE_MICHAEL_on_phone_to_therapist
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_TREVOR_0_MICHAEL,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE TREVOR 1 SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_0_Trevor_Event(enumFamilyEvents &eFamilyEvent)
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF NOT ScheduleEventForThisChar(CHAR_TREVOR)
		eFamilyEvent = FAMILY_MEMBER_BUSY
		RETURN FALSE
	ENDIF
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	CONST_FLOAT fCHANCE_TREVOR_and_kidnapped_wife_walk		15.0
	CONST_FLOAT fCHANCE_TREVOR_and_kidnapped_wife_stare		12.5
	CONST_FLOAT fCHANCE_TREVOR_smoking_crystal				15.0
	CONST_FLOAT fCHANCE_TREVOR_doing_a_shit					15.0
	CONST_FLOAT fCHANCE_TREVOR_and_kidnapped_wife_laugh		12.5
	CONST_FLOAT fCHANCE_TREVOR_blowing_shit_up				15.0
	CONST_FLOAT fCHANCE_TREVOR_passed_out_naked_drunk		15.0
	
	enumFamilyEvents	eEventArray[7]
	FLOAT 				fPercentArray[7]
	
	IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
		eEventArray[0]	= FE_T0_TREVOR_and_kidnapped_wife_walk		fPercentArray[0] = fCHANCE_TREVOR_and_kidnapped_wife_walk
		eEventArray[1]	= FE_T0_TREVOR_and_kidnapped_wife_stare		fPercentArray[1] = fCHANCE_TREVOR_and_kidnapped_wife_stare
		//eEventArray[4]	= FE_T0_TREVOR_and_kidnapped_wife_laugh		fPercentArray[4] = fCHANCE_TREVOR_and_kidnapped_wife_laugh
		eEventArray[4]	= NO_FAMILY_EVENTS							fPercentArray[4] = 0.0
	ELSE
		eEventArray[0]	= NO_FAMILY_EVENTS							fPercentArray[0] = 0.0
		eEventArray[1]	= NO_FAMILY_EVENTS							fPercentArray[1] = 0.0
		eEventArray[4]	= NO_FAMILY_EVENTS							fPercentArray[4] = 0.0
	ENDIF
	
	eEventArray[2]	= FE_T0_TREVOR_smoking_crystal					fPercentArray[2] = fCHANCE_TREVOR_smoking_crystal
	#IF NOT IS_JAPANESE_BUILD
	eEventArray[3]	= FE_T0_TREVOR_doing_a_shit						fPercentArray[3] = fCHANCE_TREVOR_doing_a_shit
	#ENDIF
	#IF IS_JAPANESE_BUILD
	eEventArray[3]	= NO_FAMILY_EVENTS								fPercentArray[3] = 0.0
	#ENDIF
	eEventArray[5]	= FE_T0_TREVOR_blowing_shit_up					fPercentArray[5] = fCHANCE_TREVOR_blowing_shit_up
	eEventArray[6]	= FE_T0_TREVOR_passed_out_naked_drunk			fPercentArray[6] = fCHANCE_TREVOR_passed_out_naked_drunk
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_TREVOR_0_TREVOR,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE WIFE 1 SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_0_Wife_Event(enumFamilyEvents &eFamilyEvent)
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_5)
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF IS_MISSION_AVAILABLE(SP_MISSION_EXILE_2)
		eFamilyEvent = FE_T0_KIDNAPPED_WIFE_cleaning
		RETURN TRUE
	ENDIF
	
	CONST_FLOAT fCHANCE_kidnapped_wife_cleaning				25.0
	CONST_FLOAT fCHANCE_kidnapped_wife_does_garden_work		25.0
	CONST_FLOAT fCHANCE_kidnapped_wife_talks_to_Michael		25.0
	CONST_FLOAT fCHANCE_kidnapped_wife_cooking_a_meal		25.0
	
	enumFamilyEvents	eEventArray[3]
	FLOAT 				fPercentArray[3]
	
	eEventArray[0]	= FE_T0_KIDNAPPED_WIFE_cleaning				fPercentArray[0] = fCHANCE_kidnapped_wife_cleaning
	eEventArray[1]	= FE_T0_KIDNAPPED_WIFE_does_garden_work		fPercentArray[1] = fCHANCE_kidnapped_wife_does_garden_work
//	eEventArray[3]	= FE_T0_KIDNAPPED_WIFE_cooking_a_meal		fPercentArray[3] = fCHANCE_kidnapped_wife_cooking_a_meal
	
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
		IF ScheduleEventForThisChar(CHAR_MICHAEL)
			eEventArray[2]	= FE_T0_KIDNAPPED_WIFE_talks_to_Michael	fPercentArray[2] = fCHANCE_kidnapped_wife_talks_to_Michael
		ELSE
			eEventArray[2]	= NO_FAMILY_EVENTS					fPercentArray[2] = 0.0
		ENDIF
	ELSE
		eEventArray[2]	= NO_FAMILY_EVENTS						fPercentArray[2] = 0.0
	ENDIF
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_TREVOR_0_WIFE,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE MOTHER 1 SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_0_Mother_Event(enumFamilyEvents &eFamilyEvent)
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_MRS_PHILIPS_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		eFamilyEvent = FE_T0_MOTHER_duringRandomChar
		RETURN TRUE
	ENDIF
	
	eFamilyEvent = NO_FAMILY_EVENTS
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE FLOYD SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_Trevor_1_Floyd_Event(enumFamilyEvents &eFamilyEvent)

	#IF IS_DEBUG_BUILD
	IF g_savedGlobals.sFlow.isGameflowActive
	#ENDIF

	
	
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_3)	//#1530187
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		ENDIF
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_4)
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		ENDIF
		
		IF g_eLastMissionPassed = SP_HEIST_DOCKS_1			//#414673
			IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0)
				eFamilyEvent = FE_T1_FLOYD_with_wade_post_docks1
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF g_eLastMissionPassed = SP_MISSION_TREVOR_3		//#951026
			eFamilyEvent = FAMILY_MEMBER_BUSY
			RETURN FALSE
		ENDIF
		
		IF IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_1)			//#1265175
		OR IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_2A)			//#1265175
		OR IS_MISSION_AVAILABLE(SP_MISSION_TREVOR_4)		//#1265175
			eFamilyEvent = FAMILY_MEMBER_BUSY
			RETURN FALSE
		ENDIF

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "in flow mode???")
	ELSE
		CPRINTLN(DEBUG_FAMILY, "NOT in flow mode???")
	ENDIF
	#ENDIF
	
	CONST_FLOAT fCHANCE_T1_FLOYD_cleaning							20.0
	CONST_FLOAT fCHANCE_T1_FLOYD_cries_in_foetal_position			20.0
	CONST_FLOAT fCHANCE_T1_FLOYD_on_phone_to_girlfriend				20.0
	CONST_FLOAT fCHANCE_T1_FLOYD_hiding_from_Trevor					20.0
	CONST_FLOAT fCHANCE_T1_FLOYD_is_sleeping						20.0
	CONST_FLOAT fCHANCE_T1_FLOYD_out								20.0
	
	enumFamilyEvents	eEventArray[6+2]
	FLOAT 				fPercentArray[6+2]
	
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1) != BUILDINGSTATE_DESTROYED
		eEventArray[0]	= FE_T1_FLOYD_cleaning					fPercentArray[0] = fCHANCE_T1_FLOYD_cleaning
	ELSE
		eEventArray[0]	= NO_FAMILY_EVENTS						fPercentArray[0] = 0.0
	ENDIF
	
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_4)			//#1529111
		eEventArray[1]	= FE_T1_FLOYD_cries_in_foetal_position	fPercentArray[1] = fCHANCE_T1_FLOYD_cries_in_foetal_position
	ELSE
		eEventArray[1]	= NO_FAMILY_EVENTS						fPercentArray[1] = 0.0
	ENDIF
	
	eEventArray[2]	= FE_T1_FLOYD_on_phone_to_girlfriend		fPercentArray[2] = fCHANCE_T1_FLOYD_on_phone_to_girlfriend
	eEventArray[3]	= FE_T1_FLOYD_hiding_from_Trevor_a			fPercentArray[3] = fCHANCE_T1_FLOYD_hiding_from_Trevor / 3.0
	eEventArray[3+1] = FE_T1_FLOYD_hiding_from_Trevor_b			fPercentArray[3+1] = fCHANCE_T1_FLOYD_hiding_from_Trevor / 3.0
	eEventArray[3+2] = FE_T1_FLOYD_hiding_from_Trevor_c			fPercentArray[3+2] = fCHANCE_T1_FLOYD_hiding_from_Trevor / 3.0
	eEventArray[4+2] = FE_T1_FLOYD_is_sleeping					fPercentArray[4+2] = fCHANCE_T1_FLOYD_is_sleeping
	eEventArray[5+2] = NO_FAMILY_EVENTS							fPercentArray[5+2] = fCHANCE_T1_FLOYD_out
	
	RETURN PRIVATE_Get_Desired_FamilyEventFromArray(FM_TREVOR_1_FLOYD,
			eEventArray, fPercentArray,
			eFamilyEvent)
ENDFUNC

// *******************************************************************************************
//	FAMILY PRIVATE SCHEDULED SLOT FUNCTIONS
// *******************************************************************************************
/// PURPOSE:
///    06:00 - 11:59
/// PARAMS:
///    eFamilyMember - 
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_FamilyMember_Morning_Event(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			RETURN PRIVATE_Get_Desired_Michael_Son_Morning_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			RETURN PRIVATE_Get_Desired_Michael_Daughter_Morning_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_WIFE
			RETURN PRIVATE_Get_Desired_Michael_Wife_Morning_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_MEXMAID
			RETURN PRIVATE_Get_Desired_Michael_MexMaid_Morning_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_GARDENER
			RETURN PRIVATE_Get_Desired_Michael_Gardener_Morning_Event(eFamilyEvent)
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			RETURN PRIVATE_Get_Desired_Franklin_Aunt_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_LAMAR
			RETURN FALSE	//PRIVATE_Get_Desired_Lamar_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_STRETCH
			RETURN FALSE	//PRIVATE_Get_Desired_Franklin_Stretch_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_0_RON
			RETURN PRIVATE_Get_Desired_Trevor_0_Ron_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MICHAEL
			RETURN PRIVATE_Get_Desired_Trevor_0_Michael_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_TREVOR
			RETURN PRIVATE_Get_Desired_Trevor_0_Trevor_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_WIFE
			RETURN PRIVATE_Get_Desired_Trevor_0_Wife_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MOTHER
			RETURN PRIVATE_Get_Desired_Trevor_0_Mother_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_1_FLOYD
			RETURN PRIVATE_Get_Desired_Trevor_1_Floyd_Event(eFamilyEvent)
		BREAK
	ENDSWITCH
	
	eFamilyEvent = NO_FAMILY_EVENTS
	SCRIPT_ASSERT("invalid eFamilyMember in PRIVATE_Get_Desired_FamilyMember_Morning_Event()")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    12:00 - 17:59
/// PARAMS:
///    eFamilyMember - 
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_FamilyMember_Afternoon_Event(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			RETURN PRIVATE_Get_Desired_Michael_Son_Afternoon_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			RETURN PRIVATE_Get_Desired_Michael_Daughter_Afternoon_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_WIFE
			RETURN PRIVATE_Get_Desired_Michael_Wife_Afternoon_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_MEXMAID
			RETURN PRIVATE_Get_Desired_Michael_MexMaid_Afternoon_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_GARDENER
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			RETURN PRIVATE_Get_Desired_Franklin_Aunt_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_LAMAR
			RETURN FALSE	//PRIVATE_Get_Desired_Lamar_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_STRETCH
			RETURN FALSE	//PRIVATE_Get_Desired_Stretch_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_0_RON
			RETURN PRIVATE_Get_Desired_Trevor_0_Ron_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MICHAEL
			RETURN PRIVATE_Get_Desired_Trevor_0_Michael_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_TREVOR
			RETURN PRIVATE_Get_Desired_Trevor_0_Trevor_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_WIFE
			RETURN PRIVATE_Get_Desired_Trevor_0_Wife_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MOTHER
			RETURN PRIVATE_Get_Desired_Trevor_0_Mother_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_1_FLOYD
			RETURN PRIVATE_Get_Desired_Trevor_1_Floyd_Event(eFamilyEvent)
		BREAK
	ENDSWITCH
	
	eFamilyEvent = NO_FAMILY_EVENTS
	SCRIPT_ASSERT("invalid eFamilyMember in PRIVATE_Get_Desired_FamilyMember_Afternoon_Event()")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    18:00 - 23:59
/// PARAMS:
///    eFamilyMember - 
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_FamilyMember_Evening_Event(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			RETURN PRIVATE_Get_Desired_Michael_Son_Evening_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			RETURN PRIVATE_Get_Desired_Michael_Daughter_Evening_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_WIFE
			RETURN PRIVATE_Get_Desired_Michael_Wife_Evening_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_MEXMAID
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		BREAK
		CASE FM_MICHAEL_GARDENER
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			RETURN PRIVATE_Get_Desired_Franklin_Aunt_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_LAMAR
			RETURN FALSE	//PRIVATE_Get_Desired_Lamar_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_STRETCH
			RETURN FALSE	//PRIVATE_Get_Desired_Stretch_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_0_RON
			RETURN PRIVATE_Get_Desired_Trevor_0_Ron_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MICHAEL
			RETURN PRIVATE_Get_Desired_Trevor_0_Michael_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_TREVOR
			RETURN PRIVATE_Get_Desired_Trevor_0_Trevor_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_WIFE
			RETURN PRIVATE_Get_Desired_Trevor_0_Wife_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MOTHER
			RETURN PRIVATE_Get_Desired_Trevor_0_Mother_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_1_FLOYD
			RETURN PRIVATE_Get_Desired_Trevor_1_Floyd_Event(eFamilyEvent)
		BREAK
	ENDSWITCH
	
	eFamilyEvent = NO_FAMILY_EVENTS
	SCRIPT_ASSERT("invalid eFamilyMember in PRIVATE_Get_Desired_FamilyMember_Evening_Event()")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    00:00 - 05:59
/// PARAMS:
///    eFamilyMember - 
///    eFamilyEvent - 
/// RETURNS:
///    
FUNC BOOL PRIVATE_Get_Desired_FamilyMember_Night_Event(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent)
	
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			RETURN PRIVATE_Get_Desired_Michael_Son_Night_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			RETURN PRIVATE_Get_Desired_Michael_Daughter_Night_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_WIFE
			RETURN PRIVATE_Get_Desired_Michael_Wife_Night_Event(eFamilyEvent)
		BREAK
		CASE FM_MICHAEL_MEXMAID
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		BREAK
		CASE FM_MICHAEL_GARDENER
			eFamilyEvent = NO_FAMILY_EVENTS
			RETURN FALSE
		BREAK
		
		CASE FM_FRANKLIN_AUNT
			RETURN PRIVATE_Get_Desired_Franklin_Aunt_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_LAMAR
			RETURN FALSE	//PRIVATE_Get_Desired_Lamar_Event(eFamilyEvent)
		BREAK
		CASE FM_FRANKLIN_STRETCH
			RETURN FALSE	//PRIVATE_Get_Desired_Stretch_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_0_RON
			RETURN PRIVATE_Get_Desired_Trevor_0_Ron_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MICHAEL
			RETURN PRIVATE_Get_Desired_Trevor_0_Michael_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_TREVOR
			RETURN PRIVATE_Get_Desired_Trevor_0_Trevor_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_WIFE
			RETURN PRIVATE_Get_Desired_Trevor_0_Wife_Event(eFamilyEvent)
		BREAK
		CASE FM_TREVOR_0_MOTHER
			RETURN PRIVATE_Get_Desired_Trevor_0_Mother_Event(eFamilyEvent)
		BREAK
		
		CASE FM_TREVOR_1_FLOYD
			RETURN PRIVATE_Get_Desired_Trevor_1_Floyd_Event(eFamilyEvent)
		BREAK
	ENDSWITCH
	
	eFamilyEvent = NO_FAMILY_EVENTS
	SCRIPT_ASSERT("invalid eFamilyMember in PRIVATE_Get_Desired_FamilyMember_Night_Event()")
	RETURN FALSE
ENDFUNC


// *******************************************************************************************
//	FAMILY PRIVATE SCHEDULE FUNCTIONS
// *******************************************************************************************
FUNC BOOL PRIVATE_Get_Desired_FamilyMember_Event(enumFamilyMember eFamilyMember, enumFamilyEvents &eFamilyEvent, BOOL bIgnoreInvalidReturns = FALSE)
	
	IF g_bMagDemoActive
		SWITCH eFamilyMember
			CASE FM_MICHAEL_DAUGHTER	eFamilyEvent = FE_M_DAUGHTER_workout_with_mp3	RETURN TRUE BREAK
			CASE FM_MICHAEL_MEXMAID		eFamilyEvent = FE_M2_MEXMAID_clean_window		RETURN TRUE BREAK
			CASE FM_MICHAEL_WIFE		eFamilyEvent = FE_M_WIFE_leaving_in_car			RETURN TRUE BREAK
		ENDSWITCH
		
		eFamilyEvent = NO_FAMILY_EVENTS
		RETURN FALSE
	ENDIF
	
	IF NOT bIgnoreInvalidReturns
		SP_MISSIONS eReturnedSpMissions
		IF PRIVATE_Force_Family_EventAvailableMissions(eFamilyMember, eFamilyEvent, eReturnedSpMissions)

			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ") = FALSE, PRIVATE_Force_Family_EventAvailableMissions(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eReturnedSpMissions, TRUE), ")")
			#ENDIF
			
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), "... PRIVATE_Force_Family_EventAvailableMissions(", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eReturnedSpMissions, TRUE), ")")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ")... bIgnoreInvalidReturns!!!")
		#ENDIF
	ENDIF
	
	//#1586404
	IF (eFamilyMember = FM_MICHAEL_WIFE)
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		AND (g_eSelectedPlayerCharScene = PR_SCENE_M7_FAKEYOGA)

			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ") = FALSE, PR_SCENE_M7_FAKEYOGA(switch)")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		IF (Get_Player_Timetable_Scene_In_Progress() = PR_SCENE_M7_FAKEYOGA)

			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ") = FALSE, PR_SCENE_M7_FAKEYOGA(scene)")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (eFamilyEvent <> NO_FAMILY_EVENTS)
		IF PRIVATE_Is_FamilyEvent_Near_Trigger(eFamilyMember, eFamilyEvent)

			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ") = FALSE, PRIVATE_Is_FamilyEvent_Near_Trigger()")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	
	SWITCH GET_CLOCK_HOURS()
		/*	06:00 - 11:59	*/
		CASE 6 CASE 7 CASE 8
        CASE 9 CASE 10 CASE 11 
			//time window A (morning)
			RETURN PRIVATE_Get_Desired_FamilyMember_Morning_Event(eFamilyMember, eFamilyEvent)
		BREAK
		
		/*	12:00 - 17:59	*/
        CASE 12 CASE 13 CASE 14 
        CASE 15 CASE 16 CASE 17
			//time window B (afternoon)
			RETURN PRIVATE_Get_Desired_FamilyMember_Afternoon_Event(eFamilyMember, eFamilyEvent)
		BREAK
		
		/*	18:00 - 23:59	*/
        CASE 18 CASE 19 CASE 20
        CASE 21 CASE 22 CASE 23
			//time window C (evening)
			RETURN PRIVATE_Get_Desired_FamilyMember_Evening_Event(eFamilyMember, eFamilyEvent)
		BREAK
		
		/*	00:00 - 05:59	*/
        CASE 0 CASE 1 CASE 2
        CASE 3 CASE 4 CASE 5
			//time window D (night)
			RETURN PRIVATE_Get_Desired_FamilyMember_Night_Event(eFamilyMember, eFamilyEvent)
		BREAK
		
    ENDSWITCH
	
	eFamilyEvent = NO_FAMILY_EVENTS
	SCRIPT_ASSERT("invalid GET_CLOCK_HOURS() in PRIVATE_Get_Desired_FamilyMember_Event()")
	RETURN FALSE
ENDFUNC




// *******************************************************************************************
//	FAMILY PRIVATE SCHEDULE FUNCTIONS
// *******************************************************************************************
FUNC BOOL PRIVATE_Is_Valid_Find_FamilyMember_Event(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
	SWITCH eFamilyMember
		CASE FM_MICHAEL_SON
			SWITCH eFamilyEvent
//				CASE FE_M_SON_gaming						FALLTHRU
//				CASE FE_M_SON_rapping_in_the_shower			FALLTHRU
//				CASE FE_M_SON_Borrows_sisters_car			FALLTHRU
//				CASE FE_M_SON_watching_porn					FALLTHRU
//				CASE FE_M_SON_in_room_asks_for_munchies		FALLTHRU
//				CASE FE_M_SON_phone_calls_in_room			FALLTHRU
//				CASE FE_M_SON_on_ecstasy_AND_friendly		FALLTHRU
//				CASE FE_M_SON_Fighting_with_sister			FALLTHRU
//				CASE FE_M_SON_smoking_weed_in_a_bong		FALLTHRU
//				CASE FE_M_SON_raids_fridge_for_food			FALLTHRU
//				CASE FE_M_SON_sleeping						FALLTHRU
				CASE FE_M7_SON_jumping_jacks				
//				CASE FE_M7_SON_Gaming						FALLTHRU
//				CASE FE_M7_SON_going_for_a_bike_ride		FALLTHRU
//				CASE FE_M7_SON_coming_back_from_a_bike_ride	FALLTHRU
//				CASE FE_M7_SON_on_laptop_looking_for_jobs	
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						RETURN TRUE
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ") - player char is not michael")
						#ENDIF
						
						RETURN FALSE
					ENDIF
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
			ENDSWITCH
		
		BREAK
		CASE FM_MICHAEL_DAUGHTER
			SWITCH eFamilyEvent
//				CASE FE_M_DAUGHTER_sunbathing				FALLTHRU
//				CASE FE_M_DAUGHTER_Going_out_in_her_car		FALLTHRU
//				CASE FE_M_DAUGHTER_Coming_home_drunk		FALLTHRU
				CASE FE_M_DAUGHTER_on_phone_to_friends		FALLTHRU
//				CASE FE_M_DAUGHTER_shower					FALLTHRU
//				CASE FE_M_DAUGHTER_workout_with_mp3			FALLTHRU
//				CASE FE_M_DAUGHTER_dancing_practice			FALLTHRU
//				CASE FE_M_DAUGHTER_watching_TV_sober		FALLTHRU
//				CASE FE_M_DAUGHTER_watching_TV_drunk		FALLTHRU
//				CASE FE_M_DAUGHTER_crying_over_a_guy		FALLTHRU
//				CASE FE_M_DAUGHTER_screaming_at_dad			FALLTHRU
//				CASE FE_M_DAUGHTER_purges_in_the_bathroom	FALLTHRU
//				CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet	FALLTHRU
//				CASE FE_M_DAUGHTER_sex_sounds_from_room		FALLTHRU
				CASE FE_M_DAUGHTER_walks_to_room_music		FALLTHRU
//				CASE FE_M_DAUGHTER_sleeping					FALLTHRU
//				CASE FE_M_DAUGHTER_couchsleep				FALLTHRU
				CASE FE_M7_DAUGHTER_studying_on_phone		FALLTHRU
				CASE FE_M7_DAUGHTER_studying_does_nails		//FALLTHRU
//				CASE FE_M7_DAUGHTER_sunbathing				
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
			ENDSWITCH
		
		BREAK
		CASE FM_MICHAEL_WIFE
			SWITCH eFamilyEvent
//				CASE FE_M_WIFE_screams_at_mexmaid			FALLTHRU
//				CASE FE_M_WIFE_in_face_mask					FALLTHRU
//				CASE FE_M_WIFE_playing_tennis				FALLTHRU
//				CASE FE_M2_WIFE_doing_yoga					FALLTHRU
//				CASE FE_M7_WIFE_doing_yoga					FALLTHRU
//				CASE FE_M_WIFE_getting_nails_done			FALLTHRU
//				CASE FE_M_WIFE_leaving_in_car				FALLTHRU
//				CASE FE_M_WIFE_leaving_in_car_v2			FALLTHRU
//				CASE FE_M_WIFE_MD_leaving_in_car_v3			FALLTHRU
//				CASE FE_M_WIFE_with_shopping_bags_enter		FALLTHRU
//				CASE FE_M_WIFE_with_shopping_bags_idle		FALLTHRU
//				CASE FE_M_WIFE_with_shopping_bags_exit		FALLTHRU
				CASE FE_M_WIFE_gets_drink_in_kitchen		
//				CASE FE_M2_WIFE_sunbathing					FALLTHRU
//				CASE FE_M7_WIFE_sunbathing					FALLTHRU
//				CASE FE_M_WIFE_getting_botox_done			FALLTHRU
//				CASE FE_M_WIFE_passed_out_SOFA  			FALLTHRU
//				CASE FE_M_WIFE_screaming_at_son_P1			FALLTHRU
//				CASE FE_M_WIFE_screaming_at_son_P2			FALLTHRU
//				CASE FE_M_WIFE_screaming_at_son_P3			FALLTHRU
//				CASE FE_M_WIFE_screaming_at_daughter		FALLTHRU
//				CASE FE_M2_WIFE_phones_man_OR_therapist		FALLTHRU
//				CASE FE_M_WIFE_hangs_up_and_wanders			FALLTHRU
//				CASE FE_M_WIFE_using_vibrator				FALLTHRU
//				CASE FE_M_WIFE_passed_out_BED				FALLTHRU
//				CASE FE_M_WIFE_sleeping						FALLTHRU
//				CASE FE_M7_WIFE_Making_juice				FALLTHRU
//				CASE FE_M7_WIFE_shopping_with_daughter		FALLTHRU
//				CASE FE_M7_WIFE_on_phone					FALLTHRU
//				CASE FE_M7_WIFE_shopping_with_son			
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		CASE FM_MICHAEL_MEXMAID
			SWITCH eFamilyEvent
				CASE FE_M2_MEXMAID_cleans_booze_pot_other
				CASE FE_M7_MEXMAID_cleans_booze_pot_other
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		#IF USE_TU_CHANGES
		CASE FM_MICHAEL_GARDENER
			SWITCH eFamilyEvent
				CASE FE_M_GARDENER_on_phone
				CASE FE_M_GARDENER_smoking_weed
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		#ENDIF
		
		CASE FM_FRANKLIN_LAMAR
		CASE FM_FRANKLIN_STRETCH
			SWITCH eFamilyEvent
//				CASE FE_F_LAMAR_and_STRETCH_chill_outside	FALLTHRU
//				CASE FE_F_LAMAR_and_STRETCH_bbq_outside		FALLTHRU
//				CASE FE_F_LAMAR_and_STRETCH_arguing			FALLTHRU
//				CASE FE_F_LAMAR_and_STRETCH_shout_at_cops	FALLTHRU
				CASE FE_F_LAMAR_and_STRETCH_wandering
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		CASE FM_TREVOR_0_RON
			SWITCH eFamilyEvent
//				CASE FE_T0_RON_monitoring_police_frequency	FALLTHRU
//				CASE FE_T0_RON_listens_to_radio_broadcast	FALLTHRU
//				CASE FE_T0_RON_ranting_about_government		FALLTHRU
//				CASE FE_T0_RON_smoking_crystal				FALLTHRU
//				CASE FE_T0_RON_drinks_moonshine_from_a_jar	FALLTHRU
				CASE FE_T0_RON_stares_through_binoculars
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		CASE FM_TREVOR_0_WIFE
			RETURN FALSE
		BREAK
		CASE FM_TREVOR_1_FLOYD
			SWITCH eFamilyEvent
				CASE FE_T1_FLOYD_cleaning			//		FALLTHRU
//				CASE FE_T1_FLOYD_cries_in_foetal_position	FALLTHRU
//				CASE FE_T1_FLOYD_pineapple					FALLTHRU
//				CASE FE_T1_FLOYD_on_phone_to_girlfriend		FALLTHRU
//				CASE FE_T1_FLOYD_hangs_up_and_wanders		FALLTHRU
//				CASE FE_T1_FLOYD_hiding_from_Trevor_a		FALLTHRU
//				CASE FE_T1_FLOYD_hiding_from_Trevor_b		FALLTHRU
//				CASE FE_T1_FLOYD_hiding_from_Trevor_c
//				CASE FE_T1_FLOYD_is_sleeping
					RETURN TRUE
				BREAK
				
				DEFAULT
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyMember(eFamilyMember), " in PRIVATE_Is_Valid_Find_FamilyMember_Event(", Get_String_From_FamilyEvent(eFamilyEvent), ")")
					#ENDIF
					
					RETURN FALSE
				BREAK
				
			ENDSWITCH
		
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 str
			str  = ("invalid eFamilyMember ")
			str += (Get_String_From_FamilyMember(eFamilyMember))
			str += (" in Is_Valid_Find_FamEvent")
			CPRINTLN(DEBUG_FAMILY, str)
			
			SCRIPT_ASSERT(str)
			#ENDIF
		
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

