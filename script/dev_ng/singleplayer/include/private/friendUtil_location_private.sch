//-	commands headers	-//
USING "rage_builtins.sch"
USING "globals.sch"

//- script headers	-//
USING "commands_script.sch"

//-	public headers	-//
USING "friends_public.sch"
USING "script_clock.sch"

//-	private headers	-//
USING "respawn_location_private.sch"
USING "comms_control_public.sch"
USING "friends_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "script_debug.sch"
	USING "shared_debug.sch"
#ENDIF

///private header for friends location utils
///    sam.hackett@rockstarnorth.com
///    


//---------------------------------------------------------------------------------------------------
//-- STRUCTS
//---------------------------------------------------------------------------------------------------

STRUCT structBits64
	INT bitsA
	INT bitsB
ENDSTRUCT


// Respot
CONST_INT	RESPOT_EXPAND_NONE			0
CONST_INT	RESPOT_EXPAND_X_LEFT		1
CONST_INT	RESPOT_EXPAND_X_RIGHT		2
CONST_INT	RESPOT_EXPAND_Y_FORWARD		4
CONST_INT	RESPOT_EXPAND_Y_BACK		8

STRUCT structFRespotData
	VECTOR				vCarPos
	FLOAT				fCarRot
	INT					expandDir
ENDSTRUCT


// Cut scenes
STRUCT structFCam
	VECTOR	vPos
	VECTOR	vRot
ENDSTRUCT

STRUCT structFCamPan
	structFCam		mStart
	structFCam		mEnd
	FLOAT			fFov
	FLOAT			fDuration
	BOOL			bEnabled
ENDSTRUCT

STRUCT structFDropoffScene
//	structFCamPan		mCarPans[4]
	structFCamPan		mFootPans[6]
	
	structFCam			mFootCatchupCam
	FLOAT				fFootCatchupFov
	
	structFCam			mCarCatchupCam
	FLOAT				fCarCatchupFov
	
	VECTOR				vCarPos
	FLOAT				fCarRot
	VECTOR				vPlayerPos
	VECTOR				vPedPos[2]
	VECTOR				vExitPos
	FLOAT				fFinalCamRot
//	FLOAT				fCarSpeechDelay
	FLOAT				fFootSpeechDelay
	FLOAT				fFootFinalDelay
	FLOAT				fCarFinalDelay0
	FLOAT				fCarFinalDelay
//	BOOL				bCarCam2IsAlternative
	BOOL				bFootCam2IsAlternative
	BOOL				bIsTaxiStyle
	
	VECTOR				vCarFakeWalkPos
	VECTOR				vCarFakeDrivePos
	structFRespotData	mExtraRespot

	VECTOR				vSwitchPos[3]
	FLOAT				fSwitchRot[3]
ENDSTRUCT

ENUM enumFDropoffStyle
	FDROPOFF_NORMAL,
	FDROPOFF_FRIENDACAR,
	FDROPOFF_FRIENDBCAR,
	FDROPOFF_TAXI
ENDENUM

STRUCT structFDropoff
	enumFDropoffStyle eStyle
ENDSTRUCT

STRUCT structFActivityScene
	structFCam		mCamPanA[2]
	structFCam		mCamTime
	structFCam		mCamPanB[2]
	
	FLOAT			fCamPanAFov
	FLOAT			fCamTimeFov
	FLOAT			fCamPanBFov
	FLOAT			fPanADurationNormal
	FLOAT			fPanADurationDrunk
	
	structFCam		mCamCatch
	FLOAT			fCamCatchFov

	BOOL			bIsCinema
	BOOL			bExtraCatchupCam
	VECTOR			vClearA
	VECTOR			vClearB
	FLOAT			fClearW
	
	
	VECTOR			vPedPPos[3]
	VECTOR			vPedAPos[3]
	VECTOR			vPedBPos[3]
ENDSTRUCT


//---------------------------------------------------------------------------------------------------
//-- CONSTS
//---------------------------------------------------------------------------------------------------

CONST_FLOAT CONST_FriendLoc_fMaxComfortableDist	1800.0//2000.0
CONST_FLOAT CONST_FriendLoc_fMinDistAvoidLoc	200.0
CONST_FLOAT	CONST_FriendLoc_fMinDistPickup		350.0
CONST_FLOAT CONST_FriendLoc_fMinDistMission		100.0
CONST_FLOAT	CONST_FriendLoc_fMinDistDropoff		100.0//25.0


//---------------------------------------------------------------------------------------------------
//-- Backup/restore location blips
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_BackupLocationBlips()

	CPRINTLN(DEBUG_FRIENDS, "Private_BackupLocationBlips()")
	
	// For each friend activity location blip
	enumActivityLocation eLoc
	REPEAT MAX_ACTIVITY_LOCATIONS eLoc
		STATIC_BLIP_NAME_ENUM eGameBlip = g_ActivityLocations[eLoc].sprite
		
		IF IS_BIT_SET(g_GameBlips[eGameBlip].iSetting, STATIC_BLIP_SETTING_ACTIVE)
			CPRINTLN(DEBUG_FRIENDS, "Private_BackupLocationBlips() - Backing up (on)  ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eGameBlip)))
		ELSE
			CPRINTLN(DEBUG_FRIENDS, "Private_BackupLocationBlips() - Backing up (off) ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eGameBlip)))
		ENDIF

		// Store blip colour
		g_ActivityLocationBackups[eLoc].iColour = GET_STATIC_BLIP_COLOUR(eGameBlip)
		
		// Store blip state
		g_ActivityLocationBackups[eLoc].iSettings = g_GameBlips[eGameBlip].iSetting
		
		// Clear blip
		SET_STATIC_BLIP_ACTIVE_STATE(eGameBlip, FALSE)
		
	ENDREPEAT

	RETURN TRUE
ENDFUNC

PROC Private_ClearLocationBlips()
	enumActivityLocation	eLoc
	STATIC_BLIP_NAME_ENUM	activitySprite
	
	REPEAT MAX_ACTIVITY_LOCATIONS eLoc
		activitySprite = g_ActivityLocations[eLoc].sprite
		IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(activitySprite)
			SET_STATIC_BLIP_ACTIVE_STATE(activitySprite, FALSE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC Private_RestoreLocationBlips()

	CPRINTLN(DEBUG_FRIENDS, "Private_RestoreLocationBlips()")
	
	// For each friend activity location blip
	enumActivityLocation eLoc
	REPEAT MAX_ACTIVITY_LOCATIONS eLoc
		STATIC_BLIP_NAME_ENUM eGameBlip = g_ActivityLocations[eLoc].sprite
		
		IF IS_BIT_SET(g_ActivityLocationBackups[eLoc].iSettings, STATIC_BLIP_SETTING_ACTIVE)
			CPRINTLN(DEBUG_FRIENDS, "Private_RestoreLocationBlips() - Restoring (on)  ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eGameBlip)))
		ELSE
			CPRINTLN(DEBUG_FRIENDS, "Private_RestoreLocationBlips() - Restoring (off) ", DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(eGameBlip)))
		ENDIF

		// Set blip colour
		IF GET_STATIC_BLIP_COLOUR(eGameBlip) <> g_ActivityLocationBackups[eLoc].iColour
			SET_STATIC_BLIP_COLOUR(eGameBlip, g_ActivityLocationBackups[eLoc].iColour)
			SET_BIT(g_GameBlips[eGameBlip].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)//g_GameBlips[eGameBlip].bStatusChanged = TRUE
		ENDIF

		// Set blip state
		IF g_GameBlips[eGameBlip].iSetting <> g_ActivityLocationBackups[eLoc].iSettings
			g_GameBlips[eGameBlip].iSetting = g_ActivityLocationBackups[eLoc].iSettings
			SET_BIT(g_GameBlips[eGameBlip].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)//g_GameBlips[eGameBlip].bStatusChanged = TRUE
		ENDIF

		// TODO only change these if the setting is actually changed
		IF IS_BIT_SET(g_GameBlips[eGameBlip].iSetting,STATIC_BLIP_SETTING_STATUS_CHANGED)
			IF g_bBlipSystemRefreshDetector = TRUE
			 	g_bBlipChangeDuringUpdate = TRUE
			ENDIF
			g_bBlipSystemRefreshDetector = TRUE
		ENDIF
		
	ENDREPEAT

ENDPROC


// *******************************************************************************************
//	EXTENDED BITSETS
// *******************************************************************************************

PROC RESET_EXTENDED_BITS(structBits64& ext)
	ext.bitsA = 0
	ext.bitsB = 0
ENDPROC

PROC SET_EXTENDED_BIT(structBits64& ext, INT iIndex)
	IF iIndex < 32
		SET_BIT(ext.bitsA, iIndex)
	ELIF iIndex < 64
		SET_BIT(ext.bitsB, iIndex - 32)
	ELSE
		CPRINTLN(DEBUG_FRIENDS, "SET_EXTENDED_BIT() - iIndex too big for 64 bit limit, (", iIndex, ")")
		SCRIPT_ASSERT("SET_EXTENDED_BIT() - iIndex too big for 64 bit limit")
	ENDIF
ENDPROC

PROC CLEAR_EXTENDED_BIT(structBits64& ext, INT iIndex)
	IF iIndex < 32
		CLEAR_BIT(ext.bitsA, iIndex)
	ELIF iIndex < 64
		CLEAR_BIT(ext.bitsB, iIndex - 32)
	ELSE
		CPRINTLN(DEBUG_FRIENDS, "CLEAR_EXTENDED_BIT() - iIndex too big for 64 bit limit, (", iIndex, ")")
		SCRIPT_ASSERT("CLEAR_EXTENDED_BIT() - iIndex too big for 64 bit limit")
	ENDIF
ENDPROC

FUNC BOOL IS_EXTENDED_BIT_SET(structBits64& ext, INT iIndex)
	IF iIndex < 32
		RETURN IS_BIT_SET(ext.bitsA, iIndex)
	ELIF iIndex < 64
		RETURN IS_BIT_SET(ext.bitsB, iIndex - 32)
	ELSE
		CPRINTLN(DEBUG_FRIENDS, "IS_EXTENDED_BIT_SET() - iIndex too big for 64 bit limit, (", iIndex, ")")
		SCRIPT_ASSERT("IS_EXTENDED_BIT_SET() - iIndex too big for 64 bit limit")
		RETURN FALSE
	ENDIF
ENDFUNC


FUNC BOOL IS_MISSION_AVAILABLE_FOR_CHAR(enumCharacterList eChar, SP_MISSIONS eMission)
	IF IS_MISSION_AVAILABLE(eMission)
		IF IS_BITMASK_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_PLAYER_PED_BIT(eChar))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MISSION_TRIGGER_MOVE(SP_MISSIONS eMission)
#if USE_CLF_DLC
eMission = eMission
#ENDIF
#if USE_NRM_DLC
eMission = eMission
#ENDIF
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH eMission
		CASE SP_HEIST_JEWELRY_PREP_2A
		CASE SP_HEIST_RURAL_PREP_1
		CASE SP_MISSION_FBI_4_PREP_1
			RETURN TRUE
		BREAK
	ENDSWITCH
#ENDIF
#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CINEMA_OPEN_NOW()
	TIMEOFDAY tod = GET_CURRENT_TIMEOFDAY()
	INT iHour = GET_TIMEOFDAY_HOUR(tod)
	
	IF iHour >= 10 AND iHour <= 22
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GOLF_OPEN_NOW()
	TIMEOFDAY tod = GET_CURRENT_TIMEOFDAY()
	INT iHour = GET_TIMEOFDAY_HOUR(tod)
	
	IF iHour >= 6 AND iHour < 18
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Private_IsLocationAvailable(enumActivityLocation activityLoc, enumCharacterList playerChar, enumCharacterList buddyCharA, enumCharacterList buddyCharB, structBits64 bitsVisitedActivities, structBits64 bitsVisitedLocations, enumFriendLocation eDropoffFriendLoc, BOOL bIsCinemaOpen, BOOL bIsGolfOpen, BOOL bVerbose = TRUE)
	
	// Check if the location type has already been visited
	IF activityLoc >= MAX_ACTIVITY_LOCATIONS
		SCRIPT_ASSERT("Private_IsLocationAvailable() - Invalid activityLoc")
		RETURN FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
		TEXT_LABEL_31 tActivityLoc = GetLabel_enumActivityLocation(activityLoc)
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		TEXT_LABEL_31 tActivityType = GetLabel_enumActivityType(g_ActivityLocations[activityLoc].type)
		#endif
		#endif
	#ENDIF

	IF activityLoc = ALOC_suspendFriends
		IF bVerbose
			CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Suspend is a dummy location for system use")
		ENDIF
		RETURN FALSE
	ENDIF

	//-- Check timed activities
	
		IF g_ActivityLocations[activityLoc].type = ATYPE_cinema
			IF bIsCinemaOpen = FALSE
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Cinema is not available at this time of day")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	
		IF g_ActivityLocations[activityLoc].type = ATYPE_golf
			IF bIsGolfOpen = FALSE
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Golf is not available at this time of day")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	
	//-- Check for missions that start at same location
	
		// Don't allow drinking at yellow jacks if mission starts there
//		IF (activityLoc = ALOC_bar_yellowjack)
//			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_CHINESE_1)
//			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_CHINESE_2)
//				IF bVerbose
//					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow drinking at Yellow Jack bar, a mission starts there")
//				ENDIF
//				RETURN FALSE
//			ENDIF
//		ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		// Don't allow darts at yellow jacks if mission starts there
		IF (activityLoc = ALOC_darts_hickBar)
			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_CHINESE_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_CHINESE_2)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow darts at Yellow Jack bar, a mission starts there")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	
		// Don't allow tennis at michaels house if mission starts there
		IF (activityLoc = ALOC_tennis_michaelHouse)
			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_EXILE_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_2)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_3)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_4)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_5)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_FAMILY_6)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_MICHAEL_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_TREVOR_1)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow tennis at Michael's house, a mission starts there")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	
		// Don't allow cinema on vinewood if mission starts there
		IF (activityLoc = ALOC_cinema_vinewood)
			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_MICHAEL_4)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow vinewood cinema, a mission starts there")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF

		// Don't allow stripclub if mission starts there
		IF (activityLoc = ALOC_stripclub_southCentral)
			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_FINALE_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_FINALE_2_INTRO)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_FINALE_2A)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_FINALE_2B)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow stripclub, a mission starts there")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF

		// Don't allow beach tennis if mission starts nearby	- NOTE: Mission does not actually start on activity loc, SO COULD REDUCE REJECTION ZONE INSTEAD
		IF (activityLoc = ALOC_tennis_beachCourt)
			IF IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_DOCKS_1)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_DOCKS_2A)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_HEIST_DOCKS_2B)
			OR IS_MISSION_AVAILABLE_FOR_CHAR(playerChar, SP_MISSION_TREVOR_4)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow tennis at beach, a mission starts nearby - SO COULD REDUCE REJECTION ZONE INSTEAD")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF

	#ENDIF
	#ENDIF
		
	//-- Check location specific conditions
	
		// Check if the activity location/type has already been visited
		IF g_ActivityLocations[activityLoc].type <> ATYPE_bar
			IF IS_EXTENDED_BIT_SET(bitsVisitedActivities, ENUM_TO_INT(g_ActivityLocations[activityLoc].type))
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: already visited activity type")
				ENDIF
				RETURN FALSE
			ENDIF
		ELSE
			IF IS_EXTENDED_BIT_SET(bitsVisitedLocations, ENUM_TO_INT(activityLoc))
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: already visited activity location")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// If dropoff location is a bar/stripclub, don't allow the corresponding activity location
		IF eDropoffFriendLoc = FLOC_bar_DT
			IF activityLoc = ALOC_bar_downtown
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Bar would clash with dropoff location")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		IF eDropoffFriendLoc = FLOC_bar_VB
			IF activityLoc = ALOC_bar_bahamas
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Bar would clash with dropoff location")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		IF eDropoffFriendLoc = FLOC_trevor_SC
			IF activityLoc = ALOC_stripclub_southCentral
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Stripclub would clash with dropoff location")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF

		// Don't allow darts or tennis if player has brought two friends
		IF g_ActivityLocations[activityLoc].type = ATYPE_darts
		OR g_ActivityLocations[activityLoc].type = ATYPE_tennis
			IF buddyCharA <> NO_CHARACTER
			AND buddyCharB <> NO_CHARACTER
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow darts or tennis with 3 players")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Only allow tennis at michaels house if michael is one of the friends on the activity
		IF (activityLoc = ALOC_tennis_michaelHouse)
			IF (playerChar <> CHAR_MICHAEL)
			AND (buddyCharA <> CHAR_MICHAEL)
			AND (buddyCharB <> CHAR_MICHAEL)
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Can't allow tennis at Michael's house unless one of you is Michael")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Amanda to do anything except drinking/tennis
		IF g_ActivityLocations[activityLoc].type <> ATYPE_bar
		AND g_ActivityLocations[activityLoc].type <> ATYPE_tennis
			IF buddyCharA = CHAR_AMANDA
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Amanda can only play tennis/go drinking")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Franklin to play tennis
		IF g_ActivityLocations[activityLoc].type = ATYPE_tennis
			IF playerChar = CHAR_FRANKLIN
			OR buddyCharA = CHAR_FRANKLIN
			OR buddyCharB = CHAR_FRANKLIN
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Franklin can't play tennis")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Lamar to play tennis
		IF g_ActivityLocations[activityLoc].type = ATYPE_tennis
			IF playerChar = CHAR_LAMAR
			OR buddyCharA = CHAR_LAMAR
			OR buddyCharB = CHAR_LAMAR
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Lamar can't play tennis")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Jimmy/Lamar to play golf
		IF g_ActivityLocations[activityLoc].type = ATYPE_golf
			IF playerChar = CHAR_JIMMY OR playerChar = CHAR_LAMAR
			OR buddyCharA = CHAR_JIMMY OR buddyCharA = CHAR_LAMAR
			OR buddyCharB = CHAR_JIMMY OR buddyCharB = CHAR_LAMAR
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Jimmy/Lamar can't play golf")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Jimmy to do drinking/stripclub with Michael
		IF g_ActivityLocations[activityLoc].type = ATYPE_bar
		OR g_ActivityLocations[activityLoc].type = ATYPE_stripclub
			IF (playerChar = CHAR_MICHAEL OR playerChar = CHAR_TREVOR) AND buddyCharA = CHAR_JIMMY
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Jimmy can't do drinking/stripclub with Michael")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
		// Don't allow Trevor to drink in Lost clubhouse
		IF activityLoc = ALOC_bar_biker
			IF playerChar = CHAR_TREVOR
			OR buddyCharA = CHAR_TREVOR
			OR buddyCharB = CHAR_TREVOR
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Trevor can't drink in Lost clubhouse")
				ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
		
	//-- Check flow
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	
		// Don't allow city activities on exile (for Michael, Trevor, Amanda and Jimmy)
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
		AND NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
			
			IF (playerChar <> CHAR_FRANKLIN AND playerChar <> CHAR_LAMAR)
			OR (buddyCharA <> CHAR_FRANKLIN AND buddyCharA <> CHAR_LAMAR AND buddyCharA <> NO_CHARACTER)
			OR (buddyCharB <> CHAR_FRANKLIN AND buddyCharB <> CHAR_LAMAR AND buddyCharB <> NO_CHARACTER)
			
				VECTOR vActivityPos = GET_STATIC_BLIP_POSITION(g_ActivityLocations[activityLoc].sprite)
				IF PRIVATE_IS_POINT_IN_CITY(vActivityPos)
					IF bVerbose
						CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: One of the characters is in exile")
					ENDIF
					RETURN FALSE
				ENDIF
				
			ENDIF
			
		ENDIF

		// Don't allow yellow jack darts until after chinese2
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_DARTS_YELLOW_JACK_AVAILABLE)
			IF activityLoc = ALOC_darts_hickBar
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Darts at Yellow Jack not available til after Chinese2")
				ENDIF
				RETURN FALSE
			ENDIF			
		ENDIF
		
	
	
		// if gameflow is on, check the flowflag for allowed minigames
		IF (g_savedGlobals.sFlow.isGameflowActive)

			BOOL bTypeEnabled = FALSE
			
			SWITCH g_ActivityLocations[activityLoc].type
				CASE ATYPE_golf
					bTypeEnabled = GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_GOLF))
				BREAK
				
				CASE ATYPE_tennis
					bTypeEnabled = GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_TENNIS))
				BREAK
				
				CASE ATYPE_stripclub
					bTypeEnabled = GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB))
				BREAK
				
				CASE ATYPE_darts
					bTypeEnabled = GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_DARTS))
				BREAK
				
				CASE ATYPE_cinema
					bTypeEnabled = GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALLOW_CINEMA_ACTIVITY)
				BREAK
				
				CASE ATYPE_bar
					bTypeEnabled = TRUE
				BREAK
				
				DEFAULT
					IF bVerbose
						CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Activity type ", tActivityType, " unknown")
					ENDIF
					RETURN FALSE
				BREAK
			ENDSWITCH
			
			IF NOT bTypeEnabled
				IF bVerbose
					CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") - REJECTING: Activity type ", tActivityType, " not unlocked")
				ENDIF
				RETURN FALSE
			ENDIF

		ENDIF
		
	#endif
	#endif
	
	IF bVerbose
		CPRINTLN(DEBUG_FRIENDS, "Private_IsLocationAvailable(", tActivityLoc, ") + ACCEPTING.")
	ENDIF
	RETURN TRUE
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Setup (FriendLocations)
//---------------------------------------------------------------------------------------------------

enumFriendLocation gSetupFriendLoc = FLOC_adhoc
#if USE_CLF_DLC
PROC PRIVATE_SetupFriendLoc(enumFriendLocation thisFriendLocation, VECTOR vPickupCoord, VECTOR vPedOffsetA, VECTOR vPedOffsetB, SAVEHOUSE_NAME_ENUM hSavehouse = NUMBER_OF_CLF_SAVEHOUSE)
	
	gSetupFriendLoc = thisFriendLocation
	
	g_FriendLocations[gSetupFriendLoc].vPickupCoord		= vPickupCoord
	g_FriendLocations[gSetupFriendLoc].vPedOffsetA		= vPedOffsetA
	g_FriendLocations[gSetupFriendLoc].vPedOffsetB		= vPedOffsetB
	g_FriendLocations[gSetupFriendLoc].iFlags			= 0
	g_FriendLocations[gSetupFriendLoc].hSavehouse		= hSavehouse
	
ENDPROC
#endif
#if USE_NRM_DLC
PROC PRIVATE_SetupFriendLoc(enumFriendLocation thisFriendLocation, VECTOR vPickupCoord, VECTOR vPedOffsetA, VECTOR vPedOffsetB, SAVEHOUSE_NAME_ENUM hSavehouse = NUMBER_OF_NRM_SAVEHOUSE)
	
	gSetupFriendLoc = thisFriendLocation
	
	g_FriendLocations[gSetupFriendLoc].vPickupCoord		= vPickupCoord
	g_FriendLocations[gSetupFriendLoc].vPedOffsetA		= vPedOffsetA
	g_FriendLocations[gSetupFriendLoc].vPedOffsetB		= vPedOffsetB
	g_FriendLocations[gSetupFriendLoc].iFlags			= 0
	g_FriendLocations[gSetupFriendLoc].hSavehouse		= hSavehouse
	
ENDPROC
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
PROC PRIVATE_SetupFriendLoc(enumFriendLocation thisFriendLocation, VECTOR vPickupCoord, VECTOR vPedOffsetA, VECTOR vPedOffsetB, SAVEHOUSE_NAME_ENUM hSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS)
	
	gSetupFriendLoc = thisFriendLocation
	
	g_FriendLocations[gSetupFriendLoc].vPickupCoord		= vPickupCoord
	g_FriendLocations[gSetupFriendLoc].vPedOffsetA		= vPedOffsetA
	g_FriendLocations[gSetupFriendLoc].vPedOffsetB		= vPedOffsetB
	g_FriendLocations[gSetupFriendLoc].iFlags			= 0
	g_FriendLocations[gSetupFriendLoc].hSavehouse		= hSavehouse
	
ENDPROC
#endif
#endif
PROC PRIVATE_SetupFriendLoc_SpawnPoint(VECTOR vSpawnPos, FLOAT fSpawnRot, VECTOR vParkPos)
	g_FriendLocations[gSetupFriendLoc].vSpawnPos = vSpawnPos
	g_FriendLocations[gSetupFriendLoc].fSpawnRot = fSpawnRot
	g_FriendLocations[gSetupFriendLoc].vParkPos = vParkPos
ENDPROC

PROC PRIVATE_SetupFriendLoc_CharFlags(INT iCharFlags)
	
	#IF IS_DEBUG_BUILD
		INT iStrippedFlags = iCharFlags & (FLF_AllChars)
		IF iStrippedFlags <> iCharFlags
			CPRINTLN(DEBUG_FRIENDS, "PRIVATE_SetupFriendLoc_SetCharUsage(", GetLabel_enumFriendLocation(gSetupFriendLoc), ") - Invalid char flags: ", iCharFlags)
			SCRIPT_ASSERT("PRIVATE_SetupFriendLoc_CharFlags() - Invalid char flags")
		ENDIF
		IF iCharFlags = 0
			CPRINTLN(DEBUG_FRIENDS, "PRIVATE_SetupFriendLoc_SetCharUsage(", GetLabel_enumFriendLocation(gSetupFriendLoc), ") - Empty char flags: ", iCharFlags)
			SCRIPT_ASSERT("PRIVATE_SetupFriendLoc_CharFlags() - Empty char flags")
		ENDIF
	#ENDIF

//	CLEAR_BITMASK(g_FriendLocations[gSetupFriendLoc].iFlags, FLF_AllChars)
	g_FriendLocations[gSetupFriendLoc].iFlags = g_FriendLocations[gSetupFriendLoc].iFlags | iCharFlags
	
ENDPROC

PROC PRIVATE_SetupFriendLoc_UsageFlags(INT iUsageFlags)
	
	#IF IS_DEBUG_BUILD
		INT iStrippedFlags = iUsageFlags & (FLF_AllUsage)
		IF iStrippedFlags <> iUsageFlags
			CPRINTLN(DEBUG_FRIENDS, "PRIVATE_SetupFriendLoc_SetCharUsage(", GetLabel_enumFriendLocation(gSetupFriendLoc), ") - Invalid usage flags: ", iUsageFlags)
			SCRIPT_ASSERT("PRIVATE_SetupFriendLoc_UsageFlags() - Invalid usage flags")
		ENDIF
		IF iUsageFlags = 0
			CPRINTLN(DEBUG_FRIENDS, "PRIVATE_SetupFriendLoc_SetCharUsage(", GetLabel_enumFriendLocation(gSetupFriendLoc), ") - Empty usage flags: ", iUsageFlags)
			SCRIPT_ASSERT("PRIVATE_SetupFriendLoc_UsageFlags() - Empty usage flags")
		ENDIF
	#ENDIF

//	CLEAR_BITMASK(g_FriendLocations[gSetupFriendLoc].iFlags, FLF_AllUsage)
	g_FriendLocations[gSetupFriendLoc].iFlags = g_FriendLocations[gSetupFriendLoc].iFlags | iUsageFlags
	
ENDPROC

FUNC VECTOR FriendLoc_GetCoord(enumFriendLocation friendPickupLoc)
	RETURN g_FriendLocations[friendPickupLoc].vPickupCoord
ENDFUNC


FUNC VECTOR FriendLoc_GetPedOffset(enumFriendLocation friendPickupLoc)
	RETURN g_FriendLocations[friendPickupLoc].vPedOffsetA
ENDFUNC

FUNC VECTOR FriendLoc_GetDoorstep(enumFriendLocation friendPickupLoc)
	RETURN g_FriendLocations[friendPickupLoc].vPickupCoord + g_FriendLocations[friendPickupLoc].vPedOffsetA
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Setup (ActivityLocations)
//---------------------------------------------------------------------------------------------------


PROC PRIVATE_SetupActivityLoc(enumActivityLocation activityLoc, enumActivityType locType, STATIC_BLIP_NAME_ENUM locSprite)

	g_ActivityLocations[activityLoc].type = locType
	g_ActivityLocations[activityLoc].sprite = locSprite
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tLoc = GetLabel_enumActivityLocation(activityLoc)
		TEXT_LABEL_63 tType = GetLabel_enumActivityType(locType)
		TEXT_LABEL_63 tBlip = DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(locSprite))
		CPRINTLN(DEBUG_FRIENDS, "PRIVATE_SetupActivityLoc(", tLoc, "[", ENUM_TO_INT(activityLoc), "], ", tType, ", ", tBlip, ")")
	#ENDIF
	
ENDPROC

FUNC VECTOR ActivityLoc_GetCoord(enumActivityLocation eActivityLoc)
	RETURN GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
ENDFUNC



//---------------------------------------------------------------------------------------------------
//-- Adhoc location functions
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_IsAdhocFriendLocValid()
	RETURN (g_FriendLocations[FLOC_adhoc].iFlags <> 0)
ENDFUNC

FUNC BOOL Private_IsAdhocFriendLocReadyForUse()
	RETURN ((g_FriendLocations[FLOC_adhoc].iFlags & FLF_AdhocReadyForUse) <> 0)
ENDFUNC

PROC Private_ClearAdhocFriendLoc()
	g_FriendLocations[FLOC_adhoc].iFlags = 0
ENDPROC

FUNC BOOL Private_GetAvailableAdhocFriendLoc(VECTOR vPlayerPos)

	// If adhoc position is waiting to be used... 
	IF Private_IsAdhocFriendLocReadyForUse()

		// If in range -> Use it
		// Otherwise -> Clear it
		IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, g_FriendLocations[FLOC_adhoc].vPickupCoord) < 10.0)
			RETURN TRUE
		ELSE
			Private_ClearAdhocFriendLoc()
		ENDIF
	
	ENDIF
	
	// If no adhoc position is setup...
	IF NOT Private_IsAdhocFriendLocValid()
	
		IF g_bTriggerSceneActive = FALSE

			// Find road node to base it all off
			VECTOR vRoadPos
			FLOAT fRoadHeading
			INT iRoadLanes
			IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vPlayerPos, 1, vRoadPos, fRoadHeading, iRoadLanes, NF_IGNORE_SLIPLANES)

				// Find position on pavement
				VECTOR vPavementPos
				VECTOR vRoadSide
				IF GET_POSITION_BY_SIDE_OF_ROAD(vRoadPos, 0, vRoadSide)
				AND (GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRoadSide) < 10.0)
				AND (ABSF(vPlayerPos.z - vRoadSide.z) < 2.5)
					vPavementPos = vRoadSide
			
				ELIF GET_POSITION_BY_SIDE_OF_ROAD(vRoadPos, 1, vRoadSide)
				AND (GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vRoadSide) < 10.0)
				AND (ABSF(vPlayerPos.z - vRoadSide.z) < 2.5)
					vPavementPos = vRoadSide

				ELSE
					RETURN FALSE
				ENDIF
				

				// Work out which side of the road we're on
				VECTOR vRoadToPavement = vPavementPos - vRoadPos
				VECTOR vRoadToLeftSide = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<0,0,0>>, fRoadHeading-90, <<0,1,0>>)
				VECTOR vRoadToRightSide = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(<<0,0,0>>, fRoadHeading+90, <<0,1,0>>)
				
				IF DOT_PRODUCT_XY(vRoadToPavement, vRoadToLeftSide) > 0.0
					vRoadToPavement = vRoadToLeftSide
				ELSE
					vRoadToPavement = vRoadToRightSide
				ENDIF
				
				// Calculate where everything should go
				VECTOR vAlongRoad	= << -vRoadToPavement.y, vRoadToPavement.x, 0.0 >>
				
				VECTOR vParkPos		= vPavementPos - (vRoadToPavement * 4.0)
				VECTOR vPedAPos		= vPavementPos + (vRoadToPavement * 1.0) - (vAlongRoad * 2.0)
				VECTOR vPedBPos		= vPavementPos + (vRoadToPavement * 1.0) + (vAlongRoad * 2.0)
				
				IF GET_SAFE_COORD_FOR_PED(vPavementPos + (vRoadToPavement * 1.0), TRUE, vPedAPos)
				AND GET_DISTANCE_BETWEEN_COORDS(vPavementPos, vPedAPos) < 10.0
				AND GET_DISTANCE_BETWEEN_COORDS(vParkPos, vPedAPos) > 2.0
					vPedAPos = vPedAPos
				ELSE
					RETURN FALSE
				ENDIF

				IF GET_SAFE_COORD_FOR_PED(vPavementPos + (vRoadToPavement * 1.0), TRUE, vPedBPos)
				AND GET_DISTANCE_BETWEEN_COORDS(vPavementPos, vPedBPos) < 10.0
				AND GET_DISTANCE_BETWEEN_COORDS(vParkPos, vPedBPos) > 2.0
					vPedBPos = vPedBPos
				ELSE
					vPedBPos = vPedAPos
				ENDIF

				// Setup adhoc location
				CPRINTLN(DEBUG_FRIENDS, "Private_GetAvailableAdhocFriendLoc() - Setting up new loc   <<", vPavementPos.x, ", ", vPavementPos.y, ", ", vPavementPos.z, ">>")
				
				g_FriendLocations[FLOC_adhoc].vPickupCoord		= vPavementPos
				g_FriendLocations[FLOC_adhoc].vPedOffsetA		= vPedAPos - vPavementPos
				g_FriendLocations[FLOC_adhoc].vPedOffsetB		= vPedBPos - vPavementPos
				
				g_FriendLocations[FLOC_adhoc].vSpawnPos			= << 0.0, 0.0, 0.0 >>
				g_FriendLocations[FLOC_adhoc].fSpawnRot			= 0.0
				g_FriendLocations[FLOC_adhoc].vParkPos			= vParkPos
				#if USE_CLF_DLC
					g_FriendLocations[FLOC_adhoc].hSavehouse		= NUMBER_OF_CLF_SAVEHOUSE
				#endif
				#if USE_NRM_DLC
					g_FriendLocations[FLOC_adhoc].hSavehouse		= NUMBER_OF_NRM_SAVEHOUSE	
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					g_FriendLocations[FLOC_adhoc].hSavehouse		= NUMBER_OF_SAVEHOUSE_LOCATIONS
				#endif
				#endif				
				g_FriendLocations[FLOC_adhoc].iFlags			= FLF_AllChars|FLF_Pickup|FLF_AdhocReadyForUse
				
				RETURN TRUE

			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- Usage functions
//---------------------------------------------------------------------------------------------------

FUNC BOOL FriendLoc_IsFriendAllowed(enumFriendLocation eLoc, enumFriend eFriend)
	
	SWITCH eFriend
		CASE FR_MICHAEL		RETURN (g_FriendLocations[eLoc].iFlags & FLF_Michael) <> 0		BREAK
		CASE FR_TREVOR		RETURN (g_FriendLocations[eLoc].iFlags & FLF_Trevor) <> 0		BREAK
		CASE FR_LAMAR		RETURN (g_FriendLocations[eLoc].iFlags & FLF_Lamar) <> 0		BREAK
		CASE FR_JIMMY		RETURN (g_FriendLocations[eLoc].iFlags & FLF_Jimmy) <> 0		BREAK
		CASE FR_AMANDA		RETURN (g_FriendLocations[eLoc].iFlags & FLF_Amanda) <> 0		BREAK
		CASE FR_FRANKLIN	RETURN (g_FriendLocations[eLoc].iFlags & FLF_Franklin) <> 0		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 tLoc = GetLabel_enumFriendLocation(eLoc)
		TEXT_LABEL_63 tFriend = GetLabel_enumFriend(eFriend)
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_IsFriendAllowed(", tLoc, ") - Invalid friend ID: ", tFriend)
		SCRIPT_ASSERT("FriendLoc_IsFriendAllowed() - Invalid friend ID")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL FriendLoc_AreFlagsSet(enumFriendLocation eLoc, INT iTestFlags)
	RETURN (g_FriendLocations[eLoc].iFlags & iTestFlags) = iTestFlags
ENDFUNC
#if USE_CLF_DLC
FUNC BOOL FriendLoc_IsUsableCLF(enumFriend ePlayer, enumFriend eFriend, enumFriendLocation eFriendLoc, BOOL bIsForDropoff)
	ePlayer = ePlayer
	eFriend = eFriend	
	// Check safehouse cutscene isn't primed
	
	// If close to a mission, don't use
	SP_MISSIONS eMission
	REPEAT SP_MISSION_MAX_CLF eMission
		IF IS_MISSION_AVAILABLE(eMission) AND NOT DOES_MISSION_TRIGGER_MOVE(eMission)
			VECTOR vMissionPos
			STATIC_BLIP_NAME_ENUM eMissionBlip
			
			eMissionBlip = g_sMissionStaticData[eMission].blip
			IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
				IF IS_STATIC_BLIP_MULTIMODE(eMissionBlip)
					vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip, GET_CURRENT_PLAYER_PED_INT())
				ELSE
				 	vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip)
				ENDIF
				
				FLOAT fDistToMission = VDIST(vMissionPos, g_FriendLocations[eFriendLoc].vPickupCoord)

				IF fDistToMission < CONST_FriendLoc_fMinDistMission
					#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 tMission = "<unknown>"
						IF eMission < SP_MISSION_MAX_CLF
							tMission = g_sMissionStaticData[eMission].scriptName
						ENDIF
						CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to mission \"", tMission, "\" (", fDistToMission, "m)")
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Is this loc too close to player?
	VECTOR vLocPos			= g_FriendLocations[eFriendLoc].vPickupCoord
	FLOAT fDistToPlayer2	= VDIST2(vLocPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FLOAT fMinPlayerDist

	IF NOT bIsForDropoff
		fMinPlayerDist = CONST_FriendLoc_fMinDistPickup
	ELSE
		fMinPlayerDist = CONST_FriendLoc_fMinDistDropoff
	ENDIF

	IF fDistToPlayer2 < fMinPlayerDist*fMinPlayerDist
		CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to player, ", SQRT(fDistToPlayer2))
		RETURN FALSE
	ENDIF
	
	// If linked to a savehouse, is the savehouse unlocked?
	IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_CLF_SAVEHOUSE
		IF NOT Is_Savehouse_Respawn_Available(g_FriendLocations[eFriendLoc].hSavehouse)
			CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - linked savehouse is not unlocked")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL FriendLoc_IsUsableNRM(enumFriend ePlayer, enumFriend eFriend, enumFriendLocation eFriendLoc, BOOL bIsForDropoff)
	ePlayer = ePlayer
	eFriend = eFriend
	
	// If close to a mission, don't use
	SP_MISSIONS eMission
	REPEAT SP_MISSION_MAX_NRM eMission
		IF IS_MISSION_AVAILABLE(eMission) AND NOT DOES_MISSION_TRIGGER_MOVE(eMission)
			VECTOR vMissionPos
			STATIC_BLIP_NAME_ENUM eMissionBlip
			
			eMissionBlip = g_sMissionStaticData[eMission].blip
			IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
				IF IS_STATIC_BLIP_MULTIMODE(eMissionBlip)
					vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip, GET_CURRENT_PLAYER_PED_INT())
				ELSE
				 	vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip)
				ENDIF
				
				FLOAT fDistToMission = VDIST(vMissionPos, g_FriendLocations[eFriendLoc].vPickupCoord)

				IF fDistToMission < CONST_FriendLoc_fMinDistMission
					#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 tMission = "<unknown>"
						IF eMission < SP_MISSION_MAX_NRM
							tMission = g_sMissionStaticData[eMission].scriptName
						ENDIF
						CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to mission \"", tMission, "\" (", fDistToMission, "m)")
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Is this loc too close to player?
	VECTOR vLocPos			= g_FriendLocations[eFriendLoc].vPickupCoord
	FLOAT fDistToPlayer2	= VDIST2(vLocPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FLOAT fMinPlayerDist

	IF NOT bIsForDropoff
		fMinPlayerDist = CONST_FriendLoc_fMinDistPickup
	ELSE
		fMinPlayerDist = CONST_FriendLoc_fMinDistDropoff
	ENDIF

	IF fDistToPlayer2 < fMinPlayerDist*fMinPlayerDist
		CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to player, ", SQRT(fDistToPlayer2))
		RETURN FALSE
	ENDIF
	
	// If linked to a savehouse, is the savehouse unlocked?
	IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_NRM_SAVEHOUSE
		IF NOT Is_Savehouse_Respawn_Available(g_FriendLocations[eFriendLoc].hSavehouse)
			CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - linked savehouse is not unlocked")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC
#endif
FUNC BOOL FriendLoc_IsUsable(enumFriend ePlayer, enumFriend eFriend, enumFriendLocation eFriendLoc, BOOL bIsForDropoff)
	#if USE_CLF_DLC
		return FriendLoc_IsUsableCLF(ePlayer,eFriend,eFriendLoc,bIsForDropoff)
	#endif
	#if USE_NRM_DLC
		return FriendLoc_IsUsableNRM(ePlayer,eFriend,eFriendLoc,bIsForDropoff)
	#endif	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	// If in exile (and Michael or Trevor involved), don't allow locations in the city
	IF ePlayer = FR_MICHAEL OR ePlayer = FR_TREVOR
	OR eFriend = FR_MICHAEL OR eFriend = FR_TREVOR
		IF PRIVATE_IS_POINT_IN_CITY(g_FriendLocations[eFriendLoc].vPickupCoord)
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED)
			AND NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
				CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - Michael and Trevor are in exile")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Check safehouse cutscene isn't primed
	IF eFriendLoc = FLOC_franklin_VH
		BOOL bCutscenePrimed = FALSE
		
		IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) <= 31 
			bCutscenePrimed = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1], ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS))
		ELSE 
			bCutscenePrimed = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2], ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) - 31)
		ENDIF
		
		IF bCutscenePrimed
			CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - Franklin's safehouse cutscene is primed")
			RETURN FALSE
		ENDIF
	ENDIF

	IF eFriendLoc = FLOC_michael_RH
		BOOL bCutscenePrimed = FALSE
		
		IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) <= 31 
			bCutscenePrimed = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1], ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME))
		ELSE 
			bCutscenePrimed = IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2], ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) - 31)
		ENDIF
		
		IF bCutscenePrimed
			CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - Michael's safehouse cutscene is primed")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// If close to a mission, don't use
	SP_MISSIONS eMission
	REPEAT SP_MISSION_MAX eMission
		IF IS_MISSION_AVAILABLE(eMission) AND NOT DOES_MISSION_TRIGGER_MOVE(eMission)
			VECTOR vMissionPos
			STATIC_BLIP_NAME_ENUM eMissionBlip
			
			eMissionBlip = g_sMissionStaticData[eMission].blip
			IF eMissionBlip <> STATIC_BLIP_NAME_DUMMY_FINAL
				IF IS_STATIC_BLIP_MULTIMODE(eMissionBlip)
					vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip, GET_CURRENT_PLAYER_PED_INT())
				ELSE
				 	vMissionPos = GET_STATIC_BLIP_POSITION(eMissionBlip)
				ENDIF
				
				FLOAT fDistToMission = VDIST(vMissionPos, g_FriendLocations[eFriendLoc].vPickupCoord)

				IF fDistToMission < CONST_FriendLoc_fMinDistMission
					#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 tMission = "<unknown>"
						IF eMission < SP_MISSION_MAX
							tMission = g_sMissionStaticData[eMission].scriptName
						ENDIF
						CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to mission \"", tMission, "\" (", fDistToMission, "m)")
					#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Is this loc too close to player?
	VECTOR vLocPos			= g_FriendLocations[eFriendLoc].vPickupCoord
	FLOAT fDistToPlayer2	= VDIST2(vLocPos, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FLOAT fMinPlayerDist

	IF NOT bIsForDropoff
		fMinPlayerDist = CONST_FriendLoc_fMinDistPickup
	ELSE
		fMinPlayerDist = CONST_FriendLoc_fMinDistDropoff
	ENDIF

	IF fDistToPlayer2 < fMinPlayerDist*fMinPlayerDist
		CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - too close to player, ", SQRT(fDistToPlayer2))
		RETURN FALSE
	ENDIF
	
	// If linked to a savehouse, is the savehouse unlocked?
	IF g_FriendLocations[eFriendLoc].hSavehouse <> NUMBER_OF_SAVEHOUSE_LOCATIONS
		IF NOT Is_Savehouse_Respawn_Available(g_FriendLocations[eFriendLoc].hSavehouse)
			CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eFriendLoc), "		REJECTED - linked savehouse is not unlocked")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
	#endif
	#endif	
ENDFUNC


FUNC BOOL FriendLoc_GetNearestUsableLoc(VECTOR vPos, enumFriend ePlayer, enumFriend eFriend, BOOL bIsForDropoff, BOOL bIsHomeLoc, enumFriendLocation& eNearestLoc, FLOAT& fNearestDist, enumFriendLocation eAvoidLoc = NO_FRIEND_LOCATION)
	
	// Check all friend locations...
	eNearestLoc = NO_FRIEND_LOCATION
	fNearestDist = 999999.0*999999.0
	
	enumFriendLocation eLoc
	REPEAT MAX_FRIEND_LOCATIONS eLoc
			
		IF eAvoidLoc = NO_FRIEND_LOCATION
		OR VDIST(g_FriendLocations[eLoc].vPickupCoord, g_FriendLocations[eAvoidLoc].vPickupCoord) > CONST_FriendLoc_fMinDistAvoidLoc
		
			// Can this friend use this location?
			IF FriendLoc_IsFriendAllowed(eLoc, eFriend)

				// Is this location for pickup/dropoff?
				IF (bIsForDropoff = FALSE AND FriendLoc_AreFlagsSet(eLoc, FLF_Pickup))
				OR (bIsForDropoff = TRUE  AND FriendLoc_AreFlagsSet(eLoc, FLF_Dropoff))
				
					// Is this location home/generic?
					IF bIsHomeLoc = FriendLoc_AreFlagsSet(eLoc, FLF_IsHomeLoc)

						// Is location acceptable for use?
						IF FriendLoc_IsUsable(ePlayer, eFriend, eLoc, bIsForDropoff)
							
							FLOAT fDist = VDIST(vPos, g_FriendLocations[eLoc].vPickupCoord)
							CPRINTLN(DEBUG_FRIENDS, " * ", GetLabel_enumFriendLocation(eLoc), "		dist = ", fDist)
							
							// If location is nearer than any other, store as best yet
							IF fNearestDist > fDist
								fNearestDist = fDist
								eNearestLoc = eLoc
							ENDIF
						
						ENDIF
					
					ENDIF
				
				ENDIF
			
			ENDIF
				
		ENDIF
	ENDREPEAT
	
	// Return result
	IF bIsHomeLoc = TRUE
		CPRINTLN(DEBUG_FRIENDS, " HOME RESULT:  		", GetLabel_enumFriendLocation(eNearestLoc))
	ELSE
		CPRINTLN(DEBUG_FRIENDS, " GENERIC RESULT:  		", GetLabel_enumFriendLocation(eNearestLoc))
	ENDIF
	RETURN (eNearestLoc <> NO_FRIEND_LOCATION)

ENDFUNC


FUNC enumFriendLocation FriendLoc_GetBestPickupLoc(enumCharacterList playerChar, enumCharacterList buddyChar, BOOL bCheckConnectionInitMode, enumFriendLocation eAvoidLoc = NO_FRIEND_LOCATION)
	
	CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(", GetLabel_enumCharacterList(buddyChar), ")...")

	#IF IS_DEBUG_BUILD
		IF g_ForceFriendLocation < MAX_FRIEND_LOCATIONS
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc() - Debug force friend location...")
			RETURN g_ForceFriendLocation
		ENDIF
	#ENDIF

	enumFriend	ePlayer		= GET_FRIEND_FROM_CHAR(playerChar)
	enumFriend	eFriend		= GET_FRIEND_FROM_CHAR(buddyChar)
	VECTOR		vIdealPos	= GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF buddyChar = NO_CHARACTER
		SCRIPT_ASSERT("FriendLoc_GetBestPickupLoc() - Invalid buddyChar")
		RETURN NO_FRIEND_LOCATION
	ENDIF
	
	// If have been asked to check the friend connection's init mode...
	enumFriendConnection eConnection
	
	IF bCheckConnectionInitMode AND GET_CONNECTION(playerChar, buddyChar, eConnection)
		SWITCH GET_CONNECTION_MODE(eConnection)
			CASE FC_MODE_Adhoc
			
				IF Private_IsAdhocFriendLocReadyForUse()
							
					// Set as in use, and return
					CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(FLOC_adhoc), " (adhoc location was requested and valid)")
					CLEAR_BITMASK(g_FriendLocations[FLOC_adhoc].iFlags, FLF_AdhocReadyForUse)
					RETURN FLOC_adhoc
				
				ELSE
					CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(", GetLabel_enumCharacterList(buddyChar), ") - Connection init mode is adhoc, but adhoc loc not available for use")
				ENDIF
			
			BREAK
		ENDSWITCH
	ENDIF
	

	// If this is a pickup, and friend has last known position in comfortable range, find location near there
	IF IS_PLAYER_PED_PLAYABLE(buddyChar)
		IF NOT HasNumOfHoursPassedSincePedTimeStruct(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[buddyChar], 1)
			VECTOR vLastKnownPos = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[buddyChar]
			
			IF VDIST(vIdealPos, vLastKnownPos) < (CONST_FriendLoc_fMaxComfortableDist * 0.5)
				CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc() - Use friends last known location...")
				vIdealPos = vLastKnownPos
			ENDIF
		ENDIF	
	ENDIF
	
	// Find nearest home and generic locs
	enumFriendLocation	eHomeLoc, eGenericLoc
	FLOAT				fHomeDist, fGenericDist

	BOOL bFoundHomeLoc = FriendLoc_GetNearestUsableLoc(vIdealPos, ePlayer, eFriend, FALSE, TRUE, eHomeLoc, fHomeDist, eAvoidLoc)
	BOOL bFoundGenericLoc = FriendLoc_GetNearestUsableLoc(vIdealPos, ePlayer, eFriend, FALSE, FALSE, eGenericLoc, fGenericDist, eAvoidLoc)
	
	IF bFoundHomeLoc AND bFoundGenericLoc
		IF fHomeDist > CONST_FriendLoc_fMaxComfortableDist
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eGenericLoc), " (no home locs in comfortable dist)")
			RETURN eGenericLoc

		ELIF GET_RANDOM_INT_IN_RANGE(0, 100) < 25
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eGenericLoc), " (randomly discarded home loc)")
			RETURN eGenericLoc
		ELSE
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eGenericLoc))
			RETURN eHomeLoc
		ENDIF
	
	ELIF bFoundHomeLoc
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eHomeLoc))
		RETURN eHomeLoc
	
	ELIF bFoundGenericLoc
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eGenericLoc))
		RETURN eGenericLoc
	ENDIF
	
	// No usable location could be found - return a failsafe loc
	enumFriendLocation eFailsafeLoc = FLOC_coffeeShop_RH
	CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eFailsafeLoc), " (no usable locs, using failsafe)")
	RETURN eFailsafeLoc

ENDFUNC

FUNC enumFriendLocation FriendLoc_GetBestDropoffLoc(enumCharacterList playerChar, enumCharacterList buddyChar, BOOL bPreferGenericLoc = FALSE)
	
	CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(", GetLabel_enumCharacterList(buddyChar), ")...")

	#IF IS_DEBUG_BUILD
		IF g_ForceFriendLocation < MAX_FRIEND_LOCATIONS
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc() - Debug force friend location...")
			RETURN g_ForceFriendLocation
		ENDIF
	#ENDIF

	enumFriend	ePlayer		= GET_FRIEND_FROM_CHAR(playerChar)
	enumFriend	eFriend		= GET_FRIEND_FROM_CHAR(buddyChar)
	VECTOR		vIdealPos	= GET_ENTITY_COORDS(PLAYER_PED_ID())

	IF buddyChar = NO_CHARACTER
		SCRIPT_ASSERT("FriendLoc_GetBestPickupLoc() - Invalid buddyChar")
		RETURN NO_FRIEND_LOCATION
	ENDIF

	// Find nearest home and generic locs
	enumFriendLocation	eHomeLoc, eGenericLoc
	FLOAT				fHomeDist, fGenericDist

	BOOL bFoundHomeLoc = FriendLoc_GetNearestUsableLoc(vIdealPos, ePlayer, eFriend, TRUE, TRUE, eHomeLoc, fHomeDist)
	BOOL bFoundGenericLoc = FriendLoc_GetNearestUsableLoc(vIdealPos, ePlayer, eFriend, TRUE, FALSE, eGenericLoc, fGenericDist)
	
	IF bPreferGenericLoc AND bFoundGenericLoc
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eGenericLoc), " (generic loc is prefered in this case)")
		RETURN eGenericLoc
	
	ELIF bFoundHomeLoc AND bFoundGenericLoc
		IF fHomeDist > CONST_FriendLoc_fMaxComfortableDist
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eGenericLoc), " (no home locs in comfortable dist)")
			RETURN eGenericLoc

		ELIF GET_RANDOM_INT_IN_RANGE(0, 100) < 50
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eGenericLoc), " (randomly discarded home loc)")
			RETURN eGenericLoc
		ELSE
			CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eGenericLoc))
			RETURN eHomeLoc
		ENDIF
	
	ELIF bFoundHomeLoc
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eHomeLoc))
		RETURN eHomeLoc
	
	ELIF bFoundGenericLoc
		CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestPickupLoc(): ", GetLabel_enumFriendLocation(eGenericLoc))
		RETURN eGenericLoc
	ENDIF
	
	// No usable location could be found - return a failsafe loc
	enumFriendLocation eFailsafeLoc = FLOC_coffeeShop_RH
	CPRINTLN(DEBUG_FRIENDS, "FriendLoc_GetBestDropoffLoc(): ", GetLabel_enumFriendLocation(eFailsafeLoc), " (no usable locs, using failsafe)")
	RETURN eFailsafeLoc

ENDFUNC




//---------------------------------------------------------------------------------------------------
//-- Initialise friend/activity locations
//---------------------------------------------------------------------------------------------------

PROC PRIVATE_InitialiseFriendLocations(BOOL bIncludingAdhocLoc)
	
	// Adhoc location (clear it)
	IF bIncludingAdhocLoc
		PRIVATE_SetupFriendLoc(FLOC_adhoc, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			PRIVATE_SetupFriendLoc_SpawnPoint(<<0.0, 0.0, 0.0>>, 0.0, <<0.0, 0.0, 0.0>>)
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	// Michael - Beverly Hills
	PRIVATE_SetupFriendLoc(FLOC_michael_RH, <<-824.9975, 179.9752, 70.4895>>, <<5.8945, -0.6014, 0.6636>>, <<7.3145, -2.6014, 0.7136>>, SAVEHOUSE_MICHAEL_BH)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-860.8041, 136.4404, 59.0250>>, 0.6456, <<-828.7663, 178.8821, 71.2117>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Michael)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Michael - Country Side
	PRIVATE_SetupFriendLoc(FLOC_michael_CS, <<1986.8676, 3810.9841, 31.1159>>, <<-2.2800, 0.4200, 0.1100>>, <<-1.0200, 2.2800, 0.0138>>, SAVEHOUSE_MICHAEL_CS)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<2138.5615, 3767.5957, 32.1819>>, 120.8022, <<1995.6809, 3818.5762, 32.1837>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Michael)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Franklin - South Central
	PRIVATE_SetupFriendLoc(FLOC_franklin_SC, <<-13.8595, -1454.3317, 29.4997>>, <<0.1999, 8.5446, 0.1003>>, <<-1.0001, 7.9046, 0.1003>>, SAVEHOUSE_FRANKLIN_SC)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<70.9006, -1477.2407, 28.2852>>, 140.9580, <<-24.3995, -1457.3259, 30.6446>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Franklin - Vinewood Hills
	PRIVATE_SetupFriendLoc(FLOC_franklin_VH, <<10.8316, 549.0906, 174.9739>>, <<-1.4644, -9.0339, 0.0539>>, <<-2.4844, -8.6139, -0.0211>>, SAVEHOUSE_FRANKLIN_VH)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-90.6814, 511.1370, 142.8975>>, 68.9617, <<5.0911, 547.0732, 174.9908>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Trevor - Country Side
	PRIVATE_SetupFriendLoc(FLOC_trevor_CS, <<1986.8674, 3810.9839, 31.1159>>, <<-2.2800, 0.4200, 0.1100>>, <<-1.0200, 2.2800, 0.0138>>, SAVEHOUSE_TREVOR_CS)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<2138.5615, 3767.5957, 32.1819>>, 120.8022, <<1995.6809, 3818.5762, 32.1837>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Trevor - Venice
	PRIVATE_SetupFriendLoc(FLOC_trevor_VB, <<-1158.2629, -1515.8615, 3.2873>>, <<3.7331, 0.3630, 0.0341>>, <<3.1931, 1.5630, 0.0341>>, SAVEHOUSE_TREVOR_VB)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-1079.3735, -1475.2400, 4.1272>>, 25.0007, <<-1166.1702, -1519.4685, 4.2290>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	// Trevor - South Central
	PRIVATE_SetupFriendLoc(FLOC_trevor_SC, <<132.7787, -1305.2081, 28.1826>>, <<-1.1933, 4.1322, 0.0451>>, <<-2.1933, 3.8922, -0.0049>>, SAVEHOUSE_TREVOR_SC)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<65.7439, -1308.3253, 28.3417>>, 210.5102, <<125.2967, -1307.9919, 29.2187>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Dropoff|FLF_IsHomeLoc)

	// Trevor - South Central
	PRIVATE_SetupFriendLoc(FLOC_trevor_SCp, <<176.9471, -1340.8126, 28.2908>>, <<-1.2000, 2.1000, 0.0032>>, <<-3.0000, 0.1200, 0.0032>>, SAVEHOUSE_TREVOR_SC)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<361.4975, -1296.7698, 31.3318>>, 140.8159, <<182.2761, -1333.3795, 28.8827>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Pickup|FLF_IsHomeLoc)
	#endif
	#endif
	
	PRIVATE_SetupFriendLoc(FLOC_lamar_SC, <<-57.8633, -1459.2397, 30.9940>>, <<-1.1400, 2.5200, 0.2200>>, <<-3.3600, 1.7400, 0.1667>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<68.9364, -1479.6398, 29.2941>>, 145.6588, <<-50.0051, -1459.1348, 31.9645>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Lamar)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff|FLF_IsHomeLoc)

	PRIVATE_SetupFriendLoc(FLOC_coffeeShop_RH, <<-14.7793, -114.3548, 55.8073>>, <<-0.7199, 3.7608, 0.0500>>, <<0.7600, 3.4800, 0.0500>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<40.2160, -256.6415, 47.4194>>, 340.2766, <<-23.3476, -113.7569, 55.8956>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Michael)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_shoppingPlaza_RH, <<-597.8970, -305.1185, 33.9548>>, <<-1.0200, 0.8400, -0.0470>>, <<-0.0000, 1.6800, -0.0470>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-584.9772, -258.7058, 34.8225>>, 210.2552, <<-590.3728, -302.9131, 34.8530>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Amanda)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_shoppingMall_RH, <<-155.6107, -175.4378, 42.7614>>, <<4.5615, 6.7732, -0.1419>>, <<3.1815, 7.1932, -0.1419>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-107.3334, -191.8515, 45.7935>>, 165.1659, <<-158.1629, -174.8294, 43.2478>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Jimmy)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_minimartCarpark_RH, <<194.0434, -42.2946, 67.5703>>, <<0.2400, 4.8600, 0.0000>>, <<1.5000, 4.6800, 0.0000>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<298.4399, -19.2893, 75.8441>>, 157.1038, <<202.8657, -46.2839, 68.6749>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin|FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_coffeeShop_DT, <<-165.4400, -788.5505, 30.9774>>, <<-6.3719, 2.4632, 0.0021>>, <<-7.3019, 1.7432, 0.0021>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-156.6818, -713.4197, 33.5995>>, 241.8992, <<-159.4454, -778.2214, 32.2215>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Michael|FLF_Amanda)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

//	PRIVATE_SetupFriendLoc(FLOC_gasStation_DT, <<-533.4040, -1216.7991, 17.2878>>, <<-1.9116, -1.5079, 0.1537>>, <<0.4284, -2.7079, 0.1537>>)
//		PRIVATE_SetupFriendLoc_SpawnPoint(<<-624.6393, -1307.6018, 10.5829>>, 326.5069, <<-532.8104, -1197.4027, 18.2464>>)
//		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin|FLF_Trevor)
//		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Pickup)

	PRIVATE_SetupFriendLoc(FLOC_minimartCarpark_DT, <<-820.9531, -1088.2756, 10.0086>>, <<-1.8040, 4.1191, 0.1191>>, <<-3.1240, 3.2191, 0.1191>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-841.6274, -1165.0057, 6.0014>>, 294.8121, <<-819.1121, -1091.4512, 10.6171>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin|FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Pickup)

	PRIVATE_SetupFriendLoc(FLOC_artPlaza_DT, <<234.7150, -959.0950, 28.2856>>, <<-1.6816, -0.2787, 0.0127>>, <<-1.0216, 1.4013, 0.0127>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<354.8806, -855.5883, 28.3444>>, 89.9428, <<240.9607, -948.6324, 28.1587>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin|FLF_Jimmy)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_bar_DT, <<249.3746, -1008.3277, 28.2705>>, <<3.8480, -3.0003, 0.0001>>, <<4.3280, -1.8003, 0.0001>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<238.4106, -1127.9969, 28.2691>>, 87.3779, <<244.4703, -1020.0197, 29.1059>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Dropoff)

	PRIVATE_SetupFriendLoc(FLOC_recCentre_SC, <<-210.3783, -1498.2042, 30.4273>>, <<2.2800, -2.7270, 0.1000>>, <<0.6000, -2.7270, 0.1000>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-281.0350, -1533.1519, 26.3419>>, 343.7635, <<-219.2698, -1492.0404, 31.2461>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Lamar)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_shoppingPlaza_VB, <<-1220.5693, -773.4271, 17.1055>>, <<3.8194, 4.9089, 0.0656>>, <<5.9194, 4.1889, -0.0844>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-1212.7676, -876.4072, 12.1331>>, 309.3317, <<-1209.4172, -785.0978, 16.9499>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Michael|FLF_Jimmy|FLF_Amanda)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_bar_VB, <<-1391.7634, -584.7326, 29.2332>>, <<-2.1715, -1.7725, -0.0100>>, <<-2.6515, -3.3925, -0.1100>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-1513.6841, -678.0259, 27.4965>>, 47.7650, <<-1409.2719, -590.8218, 30.3243>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Trevor)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_Dropoff)

	PRIVATE_SetupFriendLoc(FLOC_minimartCarpark_MW, <<-1345.2792, -385.1048, 35.7550>>, <<1.8600, -0.7800, 0.0000>>, <<1.2000, -1.6200, 0.0000>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<-1325.3550, -453.5642, 33.4523>>, 35.2950, <<-1346.2457, -397.8710, 36.6207>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_Franklin|FLF_Lamar)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_paletoMainSt_PA, <<-14.6068, 6505.2427, 30.5105>>, <<-0.5104, -7.4223, 0.0239>>, <<0.8696, -6.2223, 0.0239>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<90.1534, 6595.7891, 30.5382>>, 47.4841, <<-8.4122, 6513.6074, 31.3368>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_AllChars)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)

	PRIVATE_SetupFriendLoc(FLOC_minimartCarpark_SS, <<1393.7161, 3592.5854, 33.9297>>, <<0.8771, 4.9569, 0.0859>>, <<-0.9829, 5.4369, 0.0859>>)
		PRIVATE_SetupFriendLoc_SpawnPoint(<<1449.0967, 3669.6899, 33.1474>>, 201.6678, <<1408.6346, 3599.1831, 34.8791>>)
		PRIVATE_SetupFriendLoc_CharFlags(FLF_AllChars)
		PRIVATE_SetupFriendLoc_UsageFlags(FLF_PickupDropoff)
	
ENDPROC

PROC PRIVATE_InitialiseActivityLocations()
	
	PRIVATE_SetupActivityLoc(ALOC_golf_countryClub, 		ATYPE_golf, STATIC_BLIP_MINIGAME_GOLF)
	
	PRIVATE_SetupActivityLoc(ALOC_tennis_beachCourt,		ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS)
	PRIVATE_SetupActivityLoc(ALOC_tennis_michaelHouse, 		ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_MICHAEL_HOUSE)
	PRIVATE_SetupActivityLoc(ALOC_tennis_vinewoodhotel1,	ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_VINEWOOD_HOTEL1)
	PRIVATE_SetupActivityLoc(ALOC_tennis_richmanHotel1, 	ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_RICHMAN_HOTEL1)
	PRIVATE_SetupActivityLoc(ALOC_tennis_LSUCourt1, 		ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_LSU_COURT1)
	PRIVATE_SetupActivityLoc(ALOC_tennis_vespucciHotel, 	ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_VESPUCCI_HOTEL)
	PRIVATE_SetupActivityLoc(ALOC_tennis_weazelCourt1, 		ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_WEAZEL_COURT)
	PRIVATE_SetupActivityLoc(ALOC_tennis_chumashHotel, 		ATYPE_tennis, STATIC_BLIP_MINIGAME_TENNIS_CHUMASH_HOTEL)
	
	PRIVATE_SetupActivityLoc(ALOC_stripclub_southCentral, 	ATYPE_stripclub, STATIC_BLIP_MINIGAME_STRIPCLUB)
	
//	PRIVATE_SetupActivityLoc(ALOC_darts_rockClub, 			ATYPE_darts, STATIC_BLIP_MINIGAME_DARTS1)
	PRIVATE_SetupActivityLoc(ALOC_darts_hickBar, 			ATYPE_darts, STATIC_BLIP_MINIGAME_DARTS2)
	
	
	PRIVATE_SetupActivityLoc(ALOC_cinema_vinewood, 			ATYPE_cinema, STATIC_BLIP_ACTIVITY_CINEMA_VINEWOOD)
	PRIVATE_SetupActivityLoc(ALOC_cinema_downtown, 			ATYPE_cinema, STATIC_BLIP_ACTIVITY_CINEMA_DOWNTOWN)
	PRIVATE_SetupActivityLoc(ALOC_cinema_morningwood, 		ATYPE_cinema, STATIC_BLIP_ACTIVITY_CINEMA_MORNINGWOOD)

	
	PRIVATE_SetupActivityLoc(ALOC_bar_bahamas, 				ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_BAHAMA)
	PRIVATE_SetupActivityLoc(ALOC_bar_baybar, 				ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_BAYBAR)
	PRIVATE_SetupActivityLoc(ALOC_bar_biker, 				ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_BIKER)
	PRIVATE_SetupActivityLoc(ALOC_bar_downtown, 			ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_DOWNTOWN)
	PRIVATE_SetupActivityLoc(ALOC_bar_himen, 				ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_HIMEN)
	PRIVATE_SetupActivityLoc(ALOC_bar_mojitos, 				ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_MOJITO)
	PRIVATE_SetupActivityLoc(ALOC_bar_singletons, 			ATYPE_bar, STATIC_BLIP_ACTIVITY_BAR_SINGLETONS)

	#IF NOT USE_SP_DLC
		PRIVATE_SetupActivityLoc(ALOC_suspendFriends, 			ATYPE_suspend, STATIC_BLIP_MISSION_ARMENIAN)
	#ENDIF

ENDPROC



//---------------------------------------------------------------------------------------------------
//-- DROPOFF SCENE DATA
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_FLOC_GetDropoffScene(enumFriendLocation eFriendLoc, structFDropoffScene& scene)

	// Get loc-specific positions
	SWITCH eFriendLoc

		CASE FLOC_michael_RH
			scene.mFootPans[0].mStart.vPos	= <<-826.7827, 183.7083, 72.2750>>
			scene.mFootPans[0].mStart.vRot	= <<24.3451, 0.0000, -121.7598>>
			scene.mFootPans[0].mEnd.vPos	= <<-826.7559, 183.7018, 71.8113>>
			scene.mFootPans[0].mEnd.vRot	= <<3.8335, 0.0000, -148.4988>>
			scene.mFootPans[0].fFov			= 47.4693
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-826.2693, 180.6290, 71.8522>>
			scene.mFootPans[1].mStart.vRot	= <<0.3385, 0.0000, -125.3694>>
			scene.mFootPans[1].mEnd.vPos	= <<-826.1978, 180.5609, 71.8527>>
			scene.mFootPans[1].mEnd.vRot	= <<0.3385, 0.0000, -125.3694>>
			scene.mFootPans[1].fFov			= 32.2780
			scene.mFootPans[1].fDuration	= 2.6000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-825.2714, 181.3561, 71.8458>>
			scene.mFootPans[2].mStart.vRot	= <<0.3457, 0.0000, -154.4318>>
			scene.mFootPans[2].mEnd.vPos	= <<-825.2325, 181.2749, 71.8464>>
			scene.mFootPans[2].mEnd.vRot	= <<0.3457, 0.0000, -154.4318>>
			scene.mFootPans[2].fFov			= 29.2584
			scene.mFootPans[2].fDuration	= 3.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-823.1770, 180.3802, 71.8216>>
			scene.mFootPans[3].mStart.vRot	= <<-3.9551, 0.0000, 122.8645>>
			scene.mFootPans[3].mEnd.vPos	= <<-823.3178, 180.3653, 71.8129>>
			scene.mFootPans[3].mEnd.vRot	= <<-3.7621, 0.0000, 116.8534>>
			scene.mFootPans[3].fFov			= 50.0000
			scene.mFootPans[3].fDuration	= 3.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-826.7559, 183.7018, 71.8113>>
			scene.mFootPans[5].mStart.vRot	= <<3.8335, 0.0000, -148.4988>>
			scene.mFootPans[5].mEnd.vPos	= <<-826.7559, 183.7018, 71.8113>>
			scene.mFootPans[5].mEnd.vRot	= <<3.8335, 0.0000, -148.4988>>
			scene.mFootPans[5].fFov			= 47.4693
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-826.7559, 183.7018, 71.8113>>
			scene.mFootCatchupCam.vRot		= <<3.8335, 0.0000, -148.4988>>
			scene.fFootCatchupFov			= 47.4693

			scene.mCarCatchupCam.vPos		= <<-826.7559, 183.7018, 71.8113>>
			scene.mCarCatchupCam.vRot		= <<3.8335, 0.0000, -148.4988>>
			scene.fCarCatchupFov			= 47.4693

			scene.vCarPos					= <<-830.1575, 175.3299, 71.1486>>
			scene.fCarRot					= 335.0000
			scene.vPlayerPos				= <<-825.5045, 179.6075, 70.4304>>
			scene.vPedPos[0]				= <<-824.0706, 179.5434, 70.4812>>
			scene.vPedPos[1]				= <<-824.3851, 178.7221, 70.3815>>
			scene.vExitPos					= <<-816.7786, 178.2218, 71.2278>>
			scene.fFootSpeechDelay			= 3.0300
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-816.7786, 178.2218, 71.2278>>
			scene.vCarFakeDrivePos			= <<-816.7786, 178.2218, 71.2278>>

			scene.mExtraRespot.vCarPos		= <<-857.8395, 158.6697, 64.0398>>
			scene.mExtraRespot.fCarRot		= 352.0800
			scene.mExtraRespot.expandDir	= 1

			scene.vSwitchPos[0]				= <<-791.8809, 155.0175, 70.6749>>
			scene.fSwitchRot[0]				= 189.9119
			scene.vSwitchPos[1]				= <<-795.0798, 176.0365, 72.8350>>
			scene.fSwitchRot[1]				= 186.5013
			RETURN TRUE
		BREAK


		CASE FLOC_michael_CS
			scene.mFootPans[0].mStart.vPos	= <<1988.6624, 3809.2454, 33.8890>>
			scene.mFootPans[0].mStart.vRot	= <<2.3222, 0.0000, 59.8915>>
			scene.mFootPans[0].mEnd.vPos	= <<1988.4095, 3808.3806, 32.7818>>
			scene.mFootPans[0].mEnd.vRot	= <<-3.5227, 0.0000, 34.7012>>
			scene.mFootPans[0].fFov			= 41.7859
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<1999.1309, 3808.3823, 37.3600>>
			scene.mFootPans[1].mStart.vRot	= <<-19.3735, 0.0000, 77.9903>>
			scene.mFootPans[1].mEnd.vPos	= <<1999.1309, 3808.3823, 34.3600>>
			scene.mFootPans[1].mEnd.vRot	= <<-19.3735, 0.0000, 77.9903>>
			scene.mFootPans[1].fFov			= 50.0000
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<1986.5365, 3808.7122, 32.7272>>
			scene.mFootPans[2].mStart.vRot	= <<-4.2982, 0.0419, -15.1045>>
			scene.mFootPans[2].mEnd.vPos	= <<1986.5460, 3808.7468, 32.7245>>
			scene.mFootPans[2].mEnd.vRot	= <<-4.2982, 0.0419, -17.9532>>
			scene.mFootPans[2].fFov			= 27.6284
			scene.mFootPans[2].fDuration	= 4.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<1984.6704, 3817.9023, 32.6427>>
			scene.mFootPans[3].mStart.vRot	= <<-1.1635, 0.0008, -168.3680>>
			scene.mFootPans[3].mEnd.vPos	= <<1984.8875, 3817.2974, 32.6298>>
			scene.mFootPans[3].mEnd.vRot	= <<-1.1635, 0.0008, -168.3680>>
			scene.mFootPans[3].fFov			= 39.0874
			scene.mFootPans[3].fDuration	= 5.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootPans[5].mStart.vRot	= <<-3.2000, 0.0000, -172.8000>>
			scene.mFootPans[5].mEnd.vPos	= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.2000, 0.0000, -172.8000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootCatchupCam.vRot		= <<-3.2000, 0.0000, -172.8000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<1985.8000, 3811.2000, 32.6000>>
			scene.mCarCatchupCam.vRot		= <<-3.2000, 0.0000, -172.8000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<1984.5452, 3797.0647, 31.2530>>
			scene.fCarRot					= 303.7500
			scene.vPlayerPos				= <<1986.3514, 3809.7446, 31.1284>>
			scene.vPedPos[0]				= <<1986.5624, 3811.7476, 31.1313>>
			scene.vPedPos[1]				= <<1987.7986, 3811.0820, 31.0998>>
			scene.vExitPos					= <<1977.8676, 3815.6641, 32.7359>>
			scene.fFootSpeechDelay			= 3.2250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<1977.8676, 3815.6641, 32.7359>>
			scene.vCarFakeDrivePos			= <<1977.8676, 3815.6641, 32.7359>>

			scene.mExtraRespot.vCarPos		= <<1996.4338, 3822.8494, 31.1168>>
			scene.mExtraRespot.fCarRot		= 200.8800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<1970.1608, 3832.6973, 31.0051>>
			scene.fSwitchRot[0]				= 345.2400
			scene.vSwitchPos[1]				= <<1923.8464, 3793.4844, 31.3044>>
			scene.fSwitchRot[1]				= 24.6026
			RETURN TRUE
		BREAK


		CASE FLOC_franklin_SC
			scene.mFootPans[0].mStart.vPos	= <<-15.8397, -1454.1493, 32.0213>>
			scene.mFootPans[0].mStart.vRot	= <<-0.6298, 0.0085, -9.3542>>
			scene.mFootPans[0].mEnd.vPos	= <<-15.0177, -1455.2998, 31.1352>>
			scene.mFootPans[0].mEnd.vRot	= <<-4.2034, -0.0715, -24.1621>>
			scene.mFootPans[0].fFov			= 38.6495
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-25.7417, -1450.1324, 33.5422>>
			scene.mFootPans[1].mStart.vRot	= <<-11.1198, 0.0000, -123.8078>>
			scene.mFootPans[1].mEnd.vPos	= <<-25.7417, -1450.1324, 30.5422>>
			scene.mFootPans[1].mEnd.vRot	= <<-11.1198, 0.0000, -123.8078>>
			scene.mFootPans[1].fFov			= 38.2875
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<-13.6389, -1451.0558, 31.0825>>
			scene.mFootPans[2].mStart.vRot	= <<-3.2369, -0.0290, 176.9312>>
			scene.mFootPans[2].mEnd.vPos	= <<-25.7417, -1450.1324, 30.5422>>
			scene.mFootPans[2].mEnd.vRot	= <<-11.1198, 0.0000, -123.8078>>
			scene.mFootPans[2].fFov			= 26.0970
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-15.5813, -1457.1489, 30.8433>>
			scene.mFootPans[3].mStart.vRot	= <<-1.3844, 0.0000, -15.1833>>
			scene.mFootPans[3].mEnd.vPos	= <<-15.6248, -1457.0215, 30.8406>>
			scene.mFootPans[3].mEnd.vRot	= <<-0.4872, 0.0000, -12.8425>>
			scene.mFootPans[3].fFov			= 32.9661
			scene.mFootPans[3].fDuration	= 4.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-14.4000, -1453.1000, 31.0000>>
			scene.mFootPans[5].mStart.vRot	= <<-3.0000, 0.0000, -144.0000>>
			scene.mFootPans[5].mEnd.vPos	= <<-14.4000, -1453.1000, 31.0000>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.0000, 0.0000, -144.0000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-14.4000, -1453.1000, 31.0000>>
			scene.mFootCatchupCam.vRot		= <<-3.0000, 0.0000, -144.0000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-14.4000, -1453.1000, 31.0000>>
			scene.mCarCatchupCam.vRot		= <<-3.0000, 0.0000, -144.0000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-5.0612, -1456.5503, 29.4097>>
			scene.fCarRot					= 96.1200
			scene.vPlayerPos				= <<-13.8595, -1454.3317, 29.5001>>
			scene.vPedPos[0]				= <<-14.2219, -1452.3647, 29.5429>>
			scene.vPedPos[1]				= <<-13.2385, -1452.1836, 29.5480>>
			scene.vExitPos					= <<-13.9595, -1441.4316, 30.2797>>
			scene.fFootSpeechDelay			= 3.0000
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-13.9595, -1441.4316, 30.2797>>
			scene.vCarFakeDrivePos			= <<-13.9595, -1441.4316, 30.2797>>

			scene.mExtraRespot.vCarPos		= <<-31.3914, -1468.3141, 30.0697>>
			scene.mExtraRespot.fCarRot		= 274.6800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-14.3429, -1423.3442, 29.7124>>
			scene.fSwitchRot[0]				= 257.0400
			scene.vSwitchPos[1]				= <<9.0406, -1416.8967, 28.3067>>
			scene.fSwitchRot[1]				= 270.7200
			scene.vSwitchPos[2]				= <<0.0000, 0.0000, 0.0000>>
			scene.fSwitchRot[2]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_franklin_VH
			scene.mFootPans[0].mStart.vPos	= <<21.2376, 576.7308, 185.7817>>
			scene.mFootPans[0].mStart.vRot	= <<1.9456, 0.0255, 179.1536>>
			scene.mFootPans[0].mEnd.vPos	= <<21.2591, 576.9102, 183.7657>>
			scene.mFootPans[0].mEnd.vRot	= <<-6.2299, 0.0255, 171.9914>>
			scene.mFootPans[0].fFov			= 39.7040
			scene.mFootPans[0].fDuration	= 6.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<12.3045, 550.5663, 176.4523>>
			scene.mFootPans[1].mStart.vRot	= <<-4.0739, 0.0000, 153.0748>>
			scene.mFootPans[1].mEnd.vPos	= <<12.1710, 550.1313, 176.4203>>
			scene.mFootPans[1].mEnd.vRot	= <<-3.1393, 0.0000, 151.8330>>
			scene.mFootPans[1].fFov			= 31.4978
			scene.mFootPans[1].fDuration	= 10.0000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-9.2007, 554.8126, 180.3407>>
			scene.mFootPans[2].mStart.vRot	= <<-8.5854, -0.0669, -106.6038>>
			scene.mFootPans[2].mEnd.vPos	= <<-9.2007, 554.8126, 177.3407>>
			scene.mFootPans[2].mEnd.vRot	= <<-8.5854, -0.0669, -106.6038>>
			scene.mFootPans[2].fFov			= 35.9812
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<11.9662, 545.9982, 176.3420>>
			scene.mFootPans[3].mStart.vRot	= <<-4.2197, 0.0086, 36.2193>>
			scene.mFootPans[3].mEnd.vPos	= <<11.9368, 546.1504, 176.3316>>
			scene.mFootPans[3].mEnd.vRot	= <<-4.2197, 0.0086, 35.5229>>
			scene.mFootPans[3].fFov			= 50.0000
			scene.mFootPans[3].fDuration	= 4.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<21.2591, 576.9102, 183.7657>>
			scene.mFootPans[5].mStart.vRot	= <<-6.2299, 0.0255, 171.9914>>
			scene.mFootPans[5].mEnd.vPos	= <<21.2591, 576.9102, 183.7657>>
			scene.mFootPans[5].mEnd.vRot	= <<-6.2299, 0.0255, 171.9914>>
			scene.mFootPans[5].fFov			= 39.7040
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<21.2591, 576.9102, 183.7657>>
			scene.mFootCatchupCam.vRot		= <<-6.2299, 0.0255, 171.9914>>
			scene.fFootCatchupFov			= 39.7040

			scene.mCarCatchupCam.vPos		= <<21.2591, 576.9102, 183.7657>>
			scene.mCarCatchupCam.vRot		= <<-6.2299, 0.0255, 171.9914>>
			scene.fCarCatchupFov			= 39.7040

			scene.vCarPos					= <<16.4339, 549.3853, 175.3116>>
			scene.fCarRot					= 56.2500
			scene.vPlayerPos				= <<10.8375, 549.0540, 174.9718>>
			scene.vPedPos[0]				= <<11.4257, 547.3162, 174.9038>>
			scene.vPedPos[1]				= <<10.0769, 547.0983, 174.7723>>
			scene.vExitPos					= <<8.1316, 539.2505, 174.9739>>
			scene.fFootSpeechDelay			= 3.0250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<8.1316, 539.2505, 174.9739>>
			scene.vCarFakeDrivePos			= <<8.1316, 539.2505, 174.9739>>

			scene.mExtraRespot.vCarPos		= <<24.9549, 566.2881, 177.4016>>
			scene.mExtraRespot.fCarRot		= 110.1600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<14.6234, 526.1260, 173.6292>>
			scene.fSwitchRot[0]				= 115.5600
			scene.vSwitchPos[1]				= <<11.0096, 521.3150, 169.2275>>
			scene.fSwitchRot[1]				= 197.1696
			scene.vSwitchPos[2]				= <<0.0000, 0.0000, 0.0000>>
			scene.fSwitchRot[2]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_trevor_CS
			scene.mFootPans[0].mStart.vPos	= <<1988.6624, 3809.2454, 33.8890>>
			scene.mFootPans[0].mStart.vRot	= <<2.3222, 0.0000, 59.8915>>
			scene.mFootPans[0].mEnd.vPos	= <<1988.4095, 3808.3806, 32.7818>>
			scene.mFootPans[0].mEnd.vRot	= <<-3.5227, 0.0000, 34.7012>>
			scene.mFootPans[0].fFov			= 41.7859
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<1999.1309, 3808.3823, 37.3600>>
			scene.mFootPans[1].mStart.vRot	= <<-19.3735, 0.0000, 77.9903>>
			scene.mFootPans[1].mEnd.vPos	= <<1999.1309, 3808.3823, 34.3600>>
			scene.mFootPans[1].mEnd.vRot	= <<-19.3735, 0.0000, 77.9903>>
			scene.mFootPans[1].fFov			= 50.0000
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<1986.5365, 3808.7122, 32.7272>>
			scene.mFootPans[2].mStart.vRot	= <<-4.2982, 0.0419, -15.1045>>
			scene.mFootPans[2].mEnd.vPos	= <<1986.5460, 3808.7468, 32.7245>>
			scene.mFootPans[2].mEnd.vRot	= <<-4.2982, 0.0419, -17.9532>>
			scene.mFootPans[2].fFov			= 27.6284
			scene.mFootPans[2].fDuration	= 4.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<1984.6704, 3817.9023, 32.6427>>
			scene.mFootPans[3].mStart.vRot	= <<-1.1635, 0.0008, -168.3680>>
			scene.mFootPans[3].mEnd.vPos	= <<1984.8875, 3817.2974, 32.6298>>
			scene.mFootPans[3].mEnd.vRot	= <<-1.1635, 0.0008, -168.3680>>
			scene.mFootPans[3].fFov			= 39.0874
			scene.mFootPans[3].fDuration	= 5.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootPans[5].mStart.vRot	= <<-3.2000, 0.0000, -172.8000>>
			scene.mFootPans[5].mEnd.vPos	= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.2000, 0.0000, -172.8000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<1985.8000, 3811.2000, 32.6000>>
			scene.mFootCatchupCam.vRot		= <<-3.2000, 0.0000, -172.8000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<21.2591, 576.9102, 183.7657>>
			scene.mCarCatchupCam.vRot		= <<-6.2299, 0.0255, 171.9914>>
			scene.fCarCatchupFov			= 39.7040

			scene.vCarPos					= <<1984.5452, 3797.0647, 31.2530>>
			scene.fCarRot					= 303.7500
			scene.vPlayerPos				= <<1986.3514, 3809.7446, 31.1284>>
			scene.vPedPos[0]				= <<1986.5624, 3811.7476, 31.1313>>
			scene.vPedPos[1]				= <<1987.7986, 3811.0820, 31.0998>>
			scene.vExitPos					= <<1977.8676, 3815.6641, 32.7359>>
			scene.fFootSpeechDelay			= 3.2250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<8.1316, 539.2505, 174.9739>>
			scene.vCarFakeDrivePos			= <<8.1316, 539.2505, 174.9739>>

			scene.mExtraRespot.vCarPos		= <<1996.4338, 3822.8494, 31.1168>>
			scene.mExtraRespot.fCarRot		= 200.8800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<1970.1608, 3832.6973, 31.0051>>
			scene.fSwitchRot[0]				= 345.2400
			scene.vSwitchPos[1]				= <<1923.8464, 3793.4844, 31.3044>>
			scene.fSwitchRot[1]				= 24.6026
			RETURN TRUE
		BREAK


		CASE FLOC_trevor_VB
			scene.mFootPans[0].mStart.vPos	= <<-1189.4192, -1506.4888, 4.8110>>
			scene.mFootPans[0].mStart.vRot	= <<4.0569, 0.0000, -111.2564>>
			scene.mFootPans[0].mEnd.vPos	= <<-1189.1421, -1505.7709, 4.8109>>
			scene.mFootPans[0].mEnd.vRot	= <<4.1072, 0.0399, -111.7328>>
			scene.mFootPans[0].fFov			= 41.5362
			scene.mFootPans[0].fDuration	= 5.5500
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-1160.0498, -1514.5123, 4.7395>>
			scene.mFootPans[1].mStart.vRot	= <<-2.2734, 0.0031, -127.6174>>
			scene.mFootPans[1].mEnd.vPos	= <<-1159.9036, -1514.6249, 4.7321>>
			scene.mFootPans[1].mEnd.vRot	= <<-0.9723, 0.0031, -126.8698>>
			scene.mFootPans[1].fFov			= 26.7346
			scene.mFootPans[1].fDuration	= 5.6500
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-1159.9540, -1514.0223, 4.7491>>
			scene.mFootPans[2].mStart.vRot	= <<-2.2734, 0.0031, -143.3719>>
			scene.mFootPans[2].mEnd.vPos	= <<-1159.8395, -1514.1755, 4.7415>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.2734, 0.0031, -144.9591>>
			scene.mFootPans[2].fFov			= 26.7346
			scene.mFootPans[2].fDuration	= 10.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-1157.1967, -1524.0415, 4.3609>>
			scene.mFootPans[3].mStart.vRot	= <<2.9759, 0.0000, 4.5130>>
			scene.mFootPans[3].mEnd.vPos	= <<-1157.1533, -1523.6282, 4.3821>>
			scene.mFootPans[3].mEnd.vRot	= <<2.9759, 0.0000, 4.5130>>
			scene.mFootPans[3].fFov			= 26.7346
			scene.mFootPans[3].fDuration	= 4.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-1156.8000, -1515.6000, 4.6000>>
			scene.mFootPans[5].mStart.vRot	= <<-2.9000, 0.0000, 94.0000>>
			scene.mFootPans[5].mEnd.vPos	= <<-1156.8000, -1515.6000, 4.6000>>
			scene.mFootPans[5].mEnd.vRot	= <<-2.9000, 0.0000, 94.0000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-1156.8000, -1515.6000, 4.6000>>
			scene.mFootCatchupCam.vRot		= <<-2.9000, 0.0000, 94.0000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-1156.8000, -1515.6000, 4.6000>>
			scene.mCarCatchupCam.vRot		= <<-2.9000, 0.0000, 94.0000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-1168.0422, -1519.4583, 3.2867>>
			scene.fCarRot					= 306.2500
			scene.vPlayerPos				= <<-1158.2429, -1515.2214, 3.2073>>
			scene.vPedPos[0]				= <<-1157.5536, -1517.0989, 3.3741>>
			scene.vPedPos[1]				= <<-1158.4923, -1517.4436, 3.3570>>
			scene.vExitPos					= <<-1148.8429, -1521.8014, 3.4073>>
			scene.fFootSpeechDelay			= 2.9500
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-1148.8429, -1521.8014, 3.4073>>
			scene.vCarFakeDrivePos			= <<-1148.8429, -1521.8014, 3.4073>>

			scene.mExtraRespot.vCarPos		= <<-1176.7615, -1521.6707, 3.4037>>
			scene.mExtraRespot.fCarRot		= 301.6800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-1148.8997, -1523.4792, 9.6331>>
			scene.fSwitchRot[0]				= 34.9200
			scene.vSwitchPos[1]				= <<-1134.4351, -1532.0051, 3.3150>>
			scene.fSwitchRot[1]				= 309.2400
			scene.vSwitchPos[2]				= <<0.0000, 0.0000, 0.0000>>
			scene.fSwitchRot[2]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_trevor_SC
			scene.mFootPans[0].mStart.vPos	= <<129.8136, -1313.4695, 29.9182>>
			scene.mFootPans[0].mStart.vRot	= <<21.2995, -0.0209, -12.3644>>
			scene.mFootPans[0].mEnd.vPos	= <<129.8168, -1313.4559, 29.3543>>
			scene.mFootPans[0].mEnd.vRot	= <<3.8903, -0.0209, -14.9554>>
			scene.mFootPans[0].fFov			= 36.2014
			scene.mFootPans[0].fDuration	= 5.3250
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<132.7778, -1307.6270, 29.7443>>
			scene.mFootPans[1].mStart.vRot	= <<-2.5340, 0.0048, -0.6283>>
			scene.mFootPans[1].mEnd.vPos	= <<132.7788, -1307.5266, 29.7398>>
			scene.mFootPans[1].mEnd.vRot	= <<-2.5340, 0.0048, -0.6283>>
			scene.mFootPans[1].fFov			= 27.0556
			scene.mFootPans[1].fDuration	= 5.1000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<132.4454, -1307.6449, 29.7480>>
			scene.mFootPans[2].mStart.vRot	= <<-3.0580, 0.0048, -11.2444>>
			scene.mFootPans[2].mEnd.vPos	= <<132.4589, -1307.5769, 29.7443>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.5340, 0.0048, -11.3535>>
			scene.mFootPans[2].fFov			= 27.0556
			scene.mFootPans[2].fDuration	= 10.0500
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<129.1269, -1301.7227, 29.4008>>
			scene.mFootPans[3].mStart.vRot	= <<0.3160, -0.0209, -128.7821>>
			scene.mFootPans[3].mEnd.vPos	= <<128.7942, -1301.4554, 29.3985>>
			scene.mFootPans[3].mEnd.vRot	= <<0.3160, -0.0209, -128.7821>>
			scene.mFootPans[3].fFov			= 40.4680
			scene.mFootPans[3].fDuration	= 10.7250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<132.8000, -1305.1000, 29.7000>>
			scene.mFootPans[5].mStart.vRot	= <<-6.6000, 0.0000, 177.3000>>
			scene.mFootPans[5].mEnd.vPos	= <<132.8000, -1305.1000, 29.7000>>
			scene.mFootPans[5].mEnd.vRot	= <<-6.6000, 0.0000, 177.3000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<132.8000, -1305.1000, 29.7000>>
			scene.mFootCatchupCam.vRot		= <<-6.6000, 0.0000, 177.3000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<132.8000, -1305.1000, 29.7000>>
			scene.mCarCatchupCam.vRot		= <<-6.6000, 0.0000, 177.3000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<133.1947, -1312.1494, 28.1072>>
			scene.fCarRot					= 126.0500
			scene.vPlayerPos				= <<133.0748, -1306.5093, 28.1330>>
			scene.vPedPos[0]				= <<132.4439, -1304.6599, 28.2045>>
			scene.vPedPos[1]				= <<133.2145, -1304.7501, 28.1905>>
			scene.vExitPos					= <<128.5787, -1298.3682, 28.3026>>
			scene.fFootSpeechDelay			= 2.9250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<128.5787, -1298.3682, 28.3026>>
			scene.vCarFakeDrivePos			= <<128.5787, -1298.3682, 28.3026>>

			scene.mExtraRespot.vCarPos		= <<154.0517, -1307.6957, 28.2026>>
			scene.mExtraRespot.fCarRot		= 149.0400
			scene.mExtraRespot.expandDir	= 10

			scene.vSwitchPos[0]				= <<95.7223, -1313.1714, 28.2940>>
			scene.fSwitchRot[0]				= 83.1600
			scene.vSwitchPos[1]				= <<112.8357, -1276.4932, 28.0221>>
			scene.fSwitchRot[1]				= 99.3600
			scene.vSwitchPos[2]				= <<0.0000, 0.0000, 0.0000>>
			scene.fSwitchRot[2]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_trevor_SCp
			scene.mFootPans[0].mStart.vPos	= <<166.9471, -1340.8126, 31.2908>>
			scene.mFootPans[0].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[0].mEnd.vPos	= <<166.9471, -1340.8126, 29.2908>>
			scene.mFootPans[0].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 5.3250
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<132.7778, -1307.6270, 29.7443>>
			scene.mFootPans[1].mStart.vRot	= <<-2.5340, 0.0048, -0.6283>>
			scene.mFootPans[1].mEnd.vPos	= <<132.7788, -1307.5266, 29.7398>>
			scene.mFootPans[1].mEnd.vRot	= <<-2.5340, 0.0048, -0.6283>>
			scene.mFootPans[1].fFov			= 27.0556
			scene.mFootPans[1].fDuration	= 5.1000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<132.4454, -1307.6449, 29.7480>>
			scene.mFootPans[2].mStart.vRot	= <<-3.0580, 0.0048, -11.2444>>
			scene.mFootPans[2].mEnd.vPos	= <<132.4589, -1307.5769, 29.7443>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.5340, 0.0048, -11.3535>>
			scene.mFootPans[2].fFov			= 27.0556
			scene.mFootPans[2].fDuration	= 10.0500
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<129.1269, -1301.7227, 29.4008>>
			scene.mFootPans[3].mStart.vRot	= <<0.3160, -0.0209, -128.7821>>
			scene.mFootPans[3].mEnd.vPos	= <<128.7942, -1301.4554, 29.3985>>
			scene.mFootPans[3].mEnd.vRot	= <<0.3160, -0.0209, -128.7821>>
			scene.mFootPans[3].fFov			= 40.4680
			scene.mFootPans[3].fDuration	= 10.7250
			scene.mFootPans[3].bEnabled		= FALSE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<166.9471, -1340.8126, 31.2908>>
			scene.mFootPans[5].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[5].mEnd.vPos	= <<166.9471, -1340.8126, 31.2908>>
			scene.mFootPans[5].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<166.9471, -1340.8126, 31.2908>>
			scene.mFootCatchupCam.vRot		= <<0.0000, 0.0000, 0.0000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<132.8000, -1305.1000, 29.7000>>
			scene.mCarCatchupCam.vRot		= <<-6.6000, 0.0000, 177.3000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<176.9471, -1340.8126, 28.2908>>
			scene.fCarRot					= 0.0000
			scene.vPlayerPos				= <<176.9471, -1340.8126, 28.2908>>
			scene.vPedPos[0]				= <<177.5471, -1340.5126, 28.2908>>
			scene.vPedPos[1]				= <<177.5471, -1341.1127, 28.2908>>
			scene.vExitPos					= <<181.9471, -1340.8126, 28.2908>>
			scene.fFootSpeechDelay			= 2.9250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<128.5787, -1298.3682, 28.3026>>
			scene.vCarFakeDrivePos			= <<128.5787, -1298.3682, 28.3026>>

			scene.mExtraRespot.vCarPos		= <<154.0517, -1307.6957, 28.2026>>
			scene.mExtraRespot.fCarRot		= 149.0400
			scene.mExtraRespot.expandDir	= 10

			scene.vSwitchPos[0]				= <<176.9471, -1340.8126, 28.2908>>
			scene.fSwitchRot[0]				= 0.0000
			scene.vSwitchPos[1]				= <<176.9471, -1340.8126, 28.2908>>
			scene.fSwitchRot[1]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_lamar_SC
			scene.mFootPans[0].mStart.vPos	= <<-56.3573, -1460.4274, 34.1991>>
			scene.mFootPans[0].mStart.vRot	= <<8.1986, 0.0000, 33.5765>>
			scene.mFootPans[0].mEnd.vPos	= <<-56.5558, -1460.1489, 32.6568>>
			scene.mFootPans[0].mEnd.vRot	= <<-4.0126, 0.0000, 51.7138>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 6.3000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-57.7509, -1459.4553, 32.5180>>
			scene.mFootPans[1].mStart.vRot	= <<-2.6848, 0.0000, 75.5677>>
			scene.mFootPans[1].mEnd.vPos	= <<-57.7917, -1459.4448, 32.5160>>
			scene.mFootPans[1].mEnd.vRot	= <<-2.4783, 0.0000, 75.5677>>
			scene.mFootPans[1].fFov			= 26.9657
			scene.mFootPans[1].fDuration	= 5.3750
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-57.7837, -1459.7937, 32.5223>>
			scene.mFootPans[2].mStart.vRot	= <<-2.6848, 0.0000, 58.9447>>
			scene.mFootPans[2].mEnd.vPos	= <<-57.8125, -1459.6873, 32.5185>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.6848, 0.0000, 58.1690>>
			scene.mFootPans[2].fFov			= 26.9657
			scene.mFootPans[2].fDuration	= 10.9750
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-61.4802, -1449.4677, 32.2744>>
			scene.mFootPans[3].mStart.vRot	= <<0.1485, 0.0000, -174.2878>>
			scene.mFootPans[3].mEnd.vPos	= <<-61.4802, -1449.4677, 32.2744>>
			scene.mFootPans[3].mEnd.vRot	= <<0.1485, 0.0000, -177.7354>>
			scene.mFootPans[3].fFov			= 35.5635
			scene.mFootPans[3].fDuration	= 4.1750
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-58.6000, -1458.2000, 32.7000>>
			scene.mFootPans[5].mStart.vRot	= <<-9.2000, 0.0000, 149.0000>>
			scene.mFootPans[5].mEnd.vPos	= <<-58.6000, -1458.2000, 32.7000>>
			scene.mFootPans[5].mEnd.vRot	= <<-9.2000, 0.0000, 149.0000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-58.6000, -1458.2000, 32.7000>>
			scene.mFootCatchupCam.vRot		= <<-9.2000, 0.0000, 149.0000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-58.6000, -1458.2000, 32.7000>>
			scene.mCarCatchupCam.vRot		= <<-9.2000, 0.0000, 149.0000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-62.9473, -1462.4595, 30.9713>>
			scene.fCarRot					= 116.3700
			scene.vPlayerPos				= <<-59.0633, -1459.5398, 30.9904>>
			scene.vPedPos[0]				= <<-60.5552, -1458.2078, 31.0545>>
			scene.vPedPos[1]				= <<-59.8892, -1457.4618, 31.0777>>
			scene.vExitPos					= <<-67.2833, -1451.7397, 30.9940>>
			scene.fFootSpeechDelay			= 3.5500
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-67.2833, -1451.7397, 30.9940>>
			scene.vCarFakeDrivePos			= <<-67.2833, -1451.7397, 30.9940>>

			scene.mExtraRespot.vCarPos		= <<-46.4771, -1460.5109, 30.8037>>
			scene.mExtraRespot.fCarRot		= 95.7600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-68.4237, -1436.7537, 31.1236>>
			scene.fSwitchRot[0]				= 116.6400
			scene.vSwitchPos[1]				= <<-74.2421, -1420.8674, 28.3225>>
			scene.fSwitchRot[1]				= 271.8000
			scene.vSwitchPos[2]				= <<0.0000, 0.0000, 0.0000>>
			scene.fSwitchRot[2]				= 0.0000
			RETURN TRUE
		BREAK


		CASE FLOC_coffeeShop_RH
			scene.mFootPans[0].mStart.vPos	= <<-10.9320, -117.3033, 57.4867>>
			scene.mFootPans[0].mStart.vRot	= <<28.1405, -0.0662, 15.2927>>
			scene.mFootPans[0].mEnd.vPos	= <<-11.2653, -117.2236, 57.1882>>
			scene.mFootPans[0].mEnd.vRot	= <<0.5113, -0.0662, 30.5650>>
			scene.mFootPans[0].fFov			= 49.9706
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-7.0757, -120.2365, 61.4475>>
			scene.mFootPans[1].mStart.vRot	= <<-4.7436, 0.1427, 49.9124>>
			scene.mFootPans[1].mEnd.vPos	= <<-6.8992, -120.3944, 58.5944>>
			scene.mFootPans[1].mEnd.vRot	= <<-4.7436, 0.1427, 49.9124>>
			scene.mFootPans[1].fFov			= 44.4311
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<-11.5998, -116.8049, 57.3232>>
			scene.mFootPans[2].mStart.vRot	= <<-1.4412, 0.0000, 19.3340>>
			scene.mFootPans[2].mEnd.vPos	= <<-6.8992, -120.3944, 58.5944>>
			scene.mFootPans[2].mEnd.vRot	= <<-4.7436, 0.1427, 49.9124>>
			scene.mFootPans[2].fFov			= 28.2808
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-20.2221, -111.6242, 57.1864>>
			scene.mFootPans[3].mStart.vRot	= <<-1.4928, 0.0000, -95.3855>>
			scene.mFootPans[3].mEnd.vPos	= <<-20.2221, -111.6242, 57.1864>>
			scene.mFootPans[3].mEnd.vRot	= <<-1.4928, 0.0000, -89.5594>>
			scene.mFootPans[3].fFov			= 42.4439
			scene.mFootPans[3].fDuration	= 0.0000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-12.2428, -118.5385, 57.0926>>
			scene.mFootPans[4].mStart.vRot	= <<-2.3380, -0.0148, -1.9302>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 49.9706
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-10.7000, -114.5000, 57.2000>>
			scene.mFootPans[5].mStart.vRot	= <<-0.7000, 0.0000, 115.2000>>
			scene.mFootPans[5].mEnd.vPos	= <<-10.7000, -114.5000, 57.2000>>
			scene.mFootPans[5].mEnd.vRot	= <<-0.7000, 0.0000, 109.1433>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 4.5750
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-10.7000, -114.5000, 57.2000>>
			scene.mFootCatchupCam.vRot		= <<-0.7000, 0.0000, 115.2000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-13.0725, -115.1900, 57.2984>>
			scene.mCarCatchupCam.vRot		= <<-4.2214, -0.8691, -107.3625>>
			scene.fCarCatchupFov			= 49.9706

			scene.vCarPos					= <<-18.5821, -115.3713, 55.7963>>
			scene.fCarRot					= 73.0800
			scene.vPlayerPos				= <<-11.6174, -115.3955, 55.7810>>
			scene.vPedPos[0]				= <<-13.4109, -114.6339, 55.8221>>
			scene.vPedPos[1]				= <<-12.6469, -114.0475, 55.8117>>
			scene.vExitPos					= <<-13.4678, -109.2617, 56.1351>>
			scene.fFootSpeechDelay			= 2.6750
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 6.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-16.8713, -118.8766, 55.8675>>
			scene.vCarFakeDrivePos			= <<-97.4889, -89.0346, 56.5371>>

			scene.mExtraRespot.vCarPos		= <<-1.6820, -108.0647, 55.8574>>
			scene.mExtraRespot.fCarRot		= 160.5600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-60.4338, -88.2760, 56.7463>>
			scene.fSwitchRot[0]				= 346.3200
			scene.vSwitchPos[1]				= <<-29.7926, -85.2089, 56.2538>>
			scene.fSwitchRot[1]				= 42.1200
			scene.vSwitchPos[2]				= <<-64.4417, -76.5780, 57.1300>>
			scene.fSwitchRot[2]				= 350.7988
			RETURN TRUE
		BREAK


		CASE FLOC_shoppingPlaza_RH
			scene.mFootPans[0].mStart.vPos	= <<-599.6635, -307.1347, 35.5723>>
			scene.mFootPans[0].mStart.vRot	= <<6.6136, 0.0661, 14.4573>>
			scene.mFootPans[0].mEnd.vPos	= <<-599.6488, -307.1032, 35.4806>>
			scene.mFootPans[0].mEnd.vRot	= <<0.6974, 0.0661, -24.9427>>
			scene.mFootPans[0].fFov			= 45.1942
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-598.3406, -305.7075, 35.4950>>
			scene.mFootPans[1].mStart.vRot	= <<-1.8206, 0.0000, -24.4412>>
			scene.mFootPans[1].mEnd.vPos	= <<-598.2725, -305.5562, 35.4897>>
			scene.mFootPans[1].mEnd.vRot	= <<-1.2461, 0.0000, -23.8667>>
			scene.mFootPans[1].fFov			= 27.1855
			scene.mFootPans[1].fDuration	= 10.2250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-595.8862, -335.8199, 39.8311>>
			scene.mFootPans[2].mStart.vRot	= <<-2.4437, 0.0925, 2.7068>>
			scene.mFootPans[2].mEnd.vPos	= <<-595.8862, -335.8199, 36.8311>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.4437, 0.0925, 2.7068>>
			scene.mFootPans[2].fFov			= 30.8234
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<-603.5041, -298.5019, 35.5324>>
			scene.mFootPans[3].mStart.vRot	= <<-0.4957, 0.0000, -127.7728>>
			scene.mFootPans[3].mEnd.vPos	= <<-603.7752, -298.2920, 35.8069>>
			scene.mFootPans[3].mEnd.vRot	= <<-0.4957, 0.0000, -127.7728>>
			scene.mFootPans[3].fFov			= 37.7459
			scene.mFootPans[3].fDuration	= 4.8750
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-589.7507, -307.4630, 35.5272>>
			scene.mFootPans[4].mStart.vRot	= <<-4.1072, 0.0055, 69.2885>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 24.1002
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-598.8000, -304.1000, 35.4000>>
			scene.mFootPans[5].mStart.vRot	= <<0.3000, 0.0000, -105.1000>>
			scene.mFootPans[5].mEnd.vPos	= <<-598.8000, -304.1000, 35.4000>>
			scene.mFootPans[5].mEnd.vRot	= <<0.3000, -0.0000, -118.5139>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 6.5250
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-598.8000, -304.1000, 35.4000>>
			scene.mFootCatchupCam.vRot		= <<0.3000, 0.0000, -105.1000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-598.8000, -304.1000, 35.4000>>
			scene.mCarCatchupCam.vRot		= <<0.3000, -0.0000, -118.5139>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-591.6484, -304.4530, 33.8604>>
			scene.fCarRot					= 122.0400
			scene.vPlayerPos				= <<-597.5052, -304.8516, 33.9584>>
			scene.vPedPos[0]				= <<-597.8029, -303.0807, 33.9633>>
			scene.vPedPos[1]				= <<-596.7050, -302.7705, 33.9718>>
			scene.vExitPos					= <<-604.7370, -293.2985, 33.9548>>
			scene.fFootSpeechDelay			= 2.9250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 8.3700
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-591.2324, -302.5901, 33.9939>>
			scene.vCarFakeDrivePos			= <<-612.3061, -316.7069, 33.7226>>

			scene.mExtraRespot.vCarPos		= <<-585.7039, -300.9887, 33.9348>>
			scene.mExtraRespot.fCarRot		= 116.6400
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-623.0575, -252.5577, 37.6049>>
			scene.fSwitchRot[0]				= 346.3200
			scene.vSwitchPos[1]				= <<-585.6531, -270.6682, 34.6228>>
			scene.fSwitchRot[1]				= 33.4800
			scene.vSwitchPos[2]				= <<-649.7907, -296.3604, 34.3442>>
			scene.fSwitchRot[2]				= 32.4000
			RETURN TRUE
		BREAK


		CASE FLOC_shoppingMall_RH
			scene.mFootPans[0].mStart.vPos	= <<-158.1838, -175.1498, 44.1235>>
			scene.mFootPans[0].mStart.vRot	= <<-1.2340, 0.0000, -24.3748>>
			scene.mFootPans[0].mEnd.vPos	= <<-157.4660, -176.0162, 44.1356>>
			scene.mFootPans[0].mEnd.vRot	= <<-1.2340, 0.0000, -54.2213>>
			scene.mFootPans[0].fFov			= 38.9525
			scene.mFootPans[0].fDuration	= 7.5000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-162.7006, -153.3978, 45.8229>>
			scene.mFootPans[1].mStart.vRot	= <<-4.6874, 0.0837, -166.4122>>
			scene.mFootPans[1].mEnd.vPos	= <<-162.7006, -153.3978, 42.8229>>
			scene.mFootPans[1].mEnd.vRot	= <<-4.6874, 0.0837, -166.4122>>
			scene.mFootPans[1].fFov			= 30.9732
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<-162.7006, -153.3978, 45.8229>>
			scene.mFootPans[2].mStart.vRot	= <<-4.6874, 0.0837, -166.4122>>
			scene.mFootPans[2].mEnd.vPos	= <<-162.7006, -153.3978, 42.8229>>
			scene.mFootPans[2].mEnd.vRot	= <<-4.6874, 0.0837, -166.4122>>
			scene.mFootPans[2].fFov			= 30.9732
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].fFov			= 0.0000
			scene.mFootPans[3].fDuration	= 0.0000
			scene.mFootPans[3].bEnabled		= FALSE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-155.5000, -173.6000, 44.3000>>
			scene.mFootPans[5].mStart.vRot	= <<-5.3000, 0.0000, 176.7000>>
			scene.mFootPans[5].mEnd.vPos	= <<-155.5000, -173.6000, 44.3000>>
			scene.mFootPans[5].mEnd.vRot	= <<-5.3000, 0.0000, 176.7000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-155.5000, -173.6000, 44.3000>>
			scene.mFootCatchupCam.vRot		= <<-5.3000, 0.0000, 176.7000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-155.5000, -173.6000, 44.3000>>
			scene.mCarCatchupCam.vRot		= <<-5.3000, 0.0000, 176.7000>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-159.0514, -179.6376, 42.6038>>
			scene.fCarRot					= 340.7900
			scene.vPlayerPos				= <<-155.6107, -175.4378, 42.7616>>
			scene.vPedPos[0]				= <<-155.2113, -173.4781, 42.7592>>
			scene.vPedPos[1]				= <<-154.2314, -173.6778, 42.7592>>
			scene.vExitPos					= <<-151.4107, -167.0978, 42.7614>>
			scene.fFootSpeechDelay			= 2.6750
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 5.9850
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-157.8152, -173.4110, 42.6205>>
			scene.vCarFakeDrivePos			= <<-180.3291, -85.6264, 51.5502>>

			scene.mExtraRespot.vCarPos		= <<-178.2606, -176.7970, 42.6219>>
			scene.mExtraRespot.fCarRot		= 247.3200
			scene.mExtraRespot.expandDir	= 8

			scene.vSwitchPos[0]				= <<-111.5168, -176.4249, 49.3869>>
			scene.fSwitchRot[0]				= 337.9647
			scene.vSwitchPos[1]				= <<-146.1066, -289.7199, 40.3190>>
			scene.fSwitchRot[1]				= 162.6801
			scene.vSwitchPos[2]				= <<-184.0584, -92.2390, 51.4793>>
			scene.fSwitchRot[2]				= 339.5543
			RETURN TRUE
		BREAK


		CASE FLOC_minimartCarpark_RH
			scene.mFootPans[0].mStart.vPos	= <<216.6692, -50.2341, 71.0802>>
			scene.mFootPans[0].mStart.vRot	= <<9.2136, 0.0713, 39.6390>>
			scene.mFootPans[0].mEnd.vPos	= <<215.4209, -49.6516, 69.8986>>
			scene.mFootPans[0].mEnd.vRot	= <<-2.9289, 0.0803, 45.7664>>
			scene.mFootPans[0].fFov			= 24.7144
			scene.mFootPans[0].fDuration	= 6.0250
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<210.3946, -45.6195, 69.3571>>
			scene.mFootPans[1].mStart.vRot	= <<0.0814, 0.0714, 46.1003>>
			scene.mFootPans[1].mEnd.vPos	= <<210.3504, -45.5770, 69.3572>>
			scene.mFootPans[1].mEnd.vRot	= <<0.0814, 0.0714, 46.1003>>
			scene.mFootPans[1].fFov			= 24.7144
			scene.mFootPans[1].fDuration	= 5.9000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<210.6824, -46.2715, 69.2401>>
			scene.mFootPans[2].mStart.vRot	= <<-0.8841, 0.0714, 32.8208>>
			scene.mFootPans[2].mEnd.vPos	= <<210.5762, -46.1068, 69.2371>>
			scene.mFootPans[2].mEnd.vRot	= <<-0.8841, 0.0714, 31.8309>>
			scene.mFootPans[2].fFov			= 24.7144
			scene.mFootPans[2].fDuration	= 11.2000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<215.5824, -47.3697, 69.1787>>
			scene.mFootPans[3].mStart.vRot	= <<2.7258, 0.0713, 56.2490>>
			scene.mFootPans[3].mEnd.vPos	= <<215.5824, -47.3697, 69.1787>>
			scene.mFootPans[3].mEnd.vRot	= <<2.5404, 0.0713, 52.4503>>
			scene.mFootPans[3].fFov			= 33.9919
			scene.mFootPans[3].fDuration	= 4.7500
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<202.2701, -56.2778, 69.5264>>
			scene.mFootPans[4].mStart.vRot	= <<-4.8356, 0.0133, -30.2715>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 24.7144
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<209.4000, -43.8000, 69.5000>>
			scene.mFootPans[5].mStart.vRot	= <<-6.8000, 0.0000, 164.6000>>
			scene.mFootPans[5].mEnd.vPos	= <<209.4000, -43.8000, 69.5000>>
			scene.mFootPans[5].mEnd.vRot	= <<-6.8000, 0.0000, 158.8225>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 5.6250
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<209.4000, -43.8000, 69.5000>>
			scene.mFootCatchupCam.vRot		= <<-6.8000, 0.0000, 164.6000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<209.4000, -43.8000, 69.5000>>
			scene.mCarCatchupCam.vRot		= <<-6.8000, 0.0000, 158.8225>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<210.5713, -51.1838, 67.8538>>
			scene.fCarRot					= 70.7100
			scene.vPlayerPos				= <<209.1140, -45.0546, 67.9221>>
			scene.vPedPos[0]				= <<208.0977, -42.7594, 67.8671>>
			scene.vPedPos[1]				= <<209.2602, -42.3165, 67.9124>>
			scene.vExitPos					= <<201.3204, -27.2177, 68.9116>>
			scene.fFootSpeechDelay			= 3.4250
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 8.1000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<201.3804, -45.9723, 67.6405>>
			scene.vCarFakeDrivePos			= <<169.5868, -39.8977, 67.1079>>

			scene.mExtraRespot.vCarPos		= <<220.3531, -54.6546, 68.0668>>
			scene.mExtraRespot.fCarRot		= 72.3600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<142.2029, -8.6707, 66.6010>>
			scene.fSwitchRot[0]				= 341.2800
			scene.vSwitchPos[1]				= <<193.9097, -0.0854, 72.4415>>
			scene.fSwitchRot[1]				= 63.3600
			scene.vSwitchPos[2]				= <<141.1481, 4.3401, 67.0326>>
			scene.fSwitchRot[2]				= 340.2000
			RETURN TRUE
		BREAK


		CASE FLOC_coffeeShop_DT
			scene.mFootPans[0].mStart.vPos	= <<-190.1953, -796.7072, 31.3248>>
			scene.mFootPans[0].mStart.vRot	= <<5.1543, -0.1219, 2.2711>>
			scene.mFootPans[0].mEnd.vPos	= <<-190.1855, -796.9056, 31.3069>>
			scene.mFootPans[0].mEnd.vRot	= <<-1.8464, -0.1219, 21.1543>>
			scene.mFootPans[0].fFov			= 37.4511
			scene.mFootPans[0].fDuration	= 5.7000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-195.8433, -782.9503, 30.9256>>
			scene.mFootPans[1].mStart.vRot	= <<-1.2833, 0.0184, 56.2469>>
			scene.mFootPans[1].mEnd.vPos	= <<-196.0999, -782.7787, 30.9187>>
			scene.mFootPans[1].mEnd.vRot	= <<-1.2833, 0.0184, 56.2469>>
			scene.mFootPans[1].fFov			= 27.5662
			scene.mFootPans[1].fDuration	= 11.4000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-174.1382, -798.4118, 34.3625>>
			scene.mFootPans[2].mStart.vRot	= <<-4.1411, 0.0443, -50.8390>>
			scene.mFootPans[2].mEnd.vPos	= <<-174.1382, -798.4118, 31.3625>>
			scene.mFootPans[2].mEnd.vRot	= <<-4.1411, 0.0443, -50.8390>>
			scene.mFootPans[2].fFov			= 45.0000
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<-207.4530, -783.0007, 30.6883>>
			scene.mFootPans[3].mStart.vRot	= <<0.9957, -0.0267, -64.6160>>
			scene.mFootPans[3].mEnd.vPos	= <<-207.4830, -782.9033, 30.6885>>
			scene.mFootPans[3].mEnd.vRot	= <<0.9957, -0.0267, -64.1235>>
			scene.mFootPans[3].fFov			= 39.0431
			scene.mFootPans[3].fDuration	= 3.9250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-199.9620, -783.7169, 30.9963>>
			scene.mFootPans[4].mStart.vRot	= <<-3.9471, 0.0848, -37.9213>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 27.5662
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-198.6000, -781.3000, 31.0000>>
			scene.mFootPans[5].mStart.vRot	= <<-3.1000, 0.0000, -86.6000>>
			scene.mFootPans[5].mEnd.vPos	= <<-198.6000, -781.3000, 31.0000>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.1000, 0.0000, -86.6000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-198.6000, -781.3000, 31.0000>>
			scene.mFootCatchupCam.vRot		= <<-3.1000, 0.0000, -86.6000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<-194.9262, -780.7396, 30.7532>>
			scene.mCarCatchupCam.vRot		= <<-1.3809, 0.0000, 100.1161>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<-162.7400, -789.3305, 30.9774>>
			scene.fCarRot					= 154.8000
			scene.vPlayerPos				= <<-197.2339, -781.2570, 29.4540>>
			scene.vPedPos[0]				= <<-200.0240, -781.1829, 29.4540>>
			scene.vPedPos[1]				= <<-199.6951, -780.3373, 29.4540>>
			scene.vExitPos					= <<-208.5797, -778.6299, 29.3554>>
			scene.fFootSpeechDelay			= 3.0500
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.2000
			scene.fCarFinalDelay			= 5.1750
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-193.8266, -787.1005, 29.4540>>
			scene.vCarFakeDrivePos			= <<-176.0065, -830.4868, 29.6752>>

			scene.mExtraRespot.vCarPos		= <<-160.5266, -783.0484, 31.0433>>
			scene.mExtraRespot.fCarRot		= 164.5200
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-245.7672, -772.6562, 31.4945>>
			scene.fSwitchRot[0]				= 340.2000
			scene.vSwitchPos[1]				= <<-240.8987, -803.7341, 29.8505>>
			scene.fSwitchRot[1]				= 78.1200
			scene.vSwitchPos[2]				= <<-219.2494, -869.4808, 28.8640>>
			scene.fSwitchRot[2]				= 73.0800
			RETURN TRUE
		BREAK


		CASE FLOC_minimartCarpark_DT
			scene.mFootPans[0].mStart.vPos	= <<-830.9531, -1088.2756, 13.0086>>
			scene.mFootPans[0].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[0].mEnd.vPos	= <<-830.9531, -1088.2756, 11.0086>>
			scene.mFootPans[0].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 0.0000
			scene.mFootPans[0].bEnabled		= FALSE

			scene.mFootPans[1].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[1].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[1].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[1].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[1].fFov			= 0.0000
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[2].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[2].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[2].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[2].fFov			= 0.0000
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[3].fFov			= 0.0000
			scene.mFootPans[3].fDuration	= 0.0000
			scene.mFootPans[3].bEnabled		= FALSE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-830.9531, -1088.2756, 13.0086>>
			scene.mFootPans[5].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[5].mEnd.vPos	= <<-830.9531, -1088.2756, 13.0086>>
			scene.mFootPans[5].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-830.9531, -1088.2756, 13.0086>>
			scene.mFootCatchupCam.vRot		= <<0.0000, 0.0000, 0.0000>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<0.0000, 0.0000, 0.0000>>
			scene.mCarCatchupCam.vRot		= <<0.0000, 0.0000, 0.0000>>
			scene.fCarCatchupFov			= 0.0000

			scene.vCarPos					= <<-820.9531, -1088.2756, 10.0086>>
			scene.fCarRot					= 0.0000
			scene.vPlayerPos				= <<-820.9531, -1088.2756, 10.0086>>
			scene.vPedPos[0]				= <<-820.3531, -1087.9756, 10.0086>>
			scene.vPedPos[1]				= <<-820.3531, -1088.5757, 10.0086>>
			scene.vExitPos					= <<-815.9531, -1088.2756, 10.0086>>
			scene.fFootSpeechDelay			= 0.0000
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= FALSE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<0.0000, 0.0000, 0.0000>>
			scene.vCarFakeDrivePos			= <<0.0000, 0.0000, 0.0000>>

			scene.mExtraRespot.vCarPos		= <<0.0000, 0.0000, 0.0000>>
			scene.mExtraRespot.fCarRot		= 0.0000
			scene.mExtraRespot.expandDir	= 0

			scene.vSwitchPos[0]				= <<-820.9531, -1088.2756, 10.0086>>
			scene.fSwitchRot[0]				= 0.0000
			scene.vSwitchPos[1]				= <<-820.9531, -1088.2756, 10.0086>>
			scene.fSwitchRot[1]				= 0.0000
			scene.vSwitchPos[2]				= <<-64.4417, -76.5780, 57.1300>>
			scene.fSwitchRot[2]				= 350.7988
			RETURN TRUE
		BREAK


		CASE FLOC_artPlaza_DT
			scene.mFootPans[0].mStart.vPos	= <<231.1337, -972.4833, 30.5744>>
			scene.mFootPans[0].mStart.vRot	= <<16.5540, 0.0000, 19.9826>>
			scene.mFootPans[0].mEnd.vPos	= <<231.1004, -972.3924, 30.3643>>
			scene.mFootPans[0].mEnd.vRot	= <<5.1901, 0.0000, 18.1092>>
			scene.mFootPans[0].fFov			= 39.7270
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<231.1243, -960.4684, 29.9486>>
			scene.mFootPans[1].mStart.vRot	= <<-3.3250, 0.0034, 44.3253>>
			scene.mFootPans[1].mEnd.vPos	= <<231.0919, -960.4258, 29.9455>>
			scene.mFootPans[1].mEnd.vRot	= <<-3.3250, 0.0034, 44.3253>>
			scene.mFootPans[1].fFov			= 18.4380
			scene.mFootPans[1].fDuration	= 4.9250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<231.1691, -961.2233, 29.9703>>
			scene.mFootPans[2].mStart.vRot	= <<-1.4860, 0.0000, 33.1161>>
			scene.mFootPans[2].mEnd.vPos	= <<231.0190, -960.9930, 29.9631>>
			scene.mFootPans[2].mEnd.vRot	= <<-1.4860, 0.0000, 33.6074>>
			scene.mFootPans[2].fFov			= 18.8257
			scene.mFootPans[2].fDuration	= 9.5250
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<201.2578, -968.2456, 31.4906>>
			scene.mFootPans[3].mStart.vRot	= <<-3.6870, 0.0000, -68.9103>>
			scene.mFootPans[3].mEnd.vPos	= <<201.1927, -968.0770, 31.4906>>
			scene.mFootPans[3].mEnd.vRot	= <<-3.6870, 0.0000, -68.9103>>
			scene.mFootPans[3].fFov			= 30.4755
			scene.mFootPans[3].fDuration	= 4.9500
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<242.8073, -959.5851, 29.7023>>
			scene.mFootPans[4].mStart.vRot	= <<0.2064, 0.0551, 93.2706>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 35.9540
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<229.2498, -959.5234, 29.8773>>
			scene.mFootPans[5].mStart.vRot	= <<-3.2186, 0.0000, -71.4419>>
			scene.mFootPans[5].mEnd.vPos	= <<229.2498, -959.5234, 29.8773>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.2186, 0.0000, -75.0536>>
			scene.mFootPans[5].fFov			= 50.0360
			scene.mFootPans[5].fDuration	= 5.3100
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<229.2498, -959.5234, 29.8773>>
			scene.mFootCatchupCam.vRot		= <<-3.2186, 0.0000, -71.4419>>
			scene.fFootCatchupFov			= 50.0360

			scene.mCarCatchupCam.vPos		= <<229.2498, -959.5234, 29.8773>>
			scene.mCarCatchupCam.vRot		= <<-3.2186, 0.0000, -75.0536>>
			scene.fCarCatchupFov			= 50.0360

			scene.vCarPos					= <<240.7464, -951.9853, 28.1878>>
			scene.fCarRot					= 158.9700
			scene.vPlayerPos				= <<230.4242, -959.2939, 28.3431>>
			scene.vPedPos[0]				= <<228.2315, -958.0656, 28.3444>>
			scene.vPedPos[1]				= <<228.8929, -957.1745, 28.3397>>
			scene.vExitPos					= <<213.1038, -944.3844, 29.6918>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 7.6350
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<236.7469, -954.6210, 28.2767>>
			scene.vCarFakeDrivePos			= <<229.3741, -985.0717, 28.2070>>

			scene.mExtraRespot.vCarPos		= <<243.9274, -942.5368, 28.2134>>
			scene.mExtraRespot.fCarRot		= 160.5600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<209.7067, -912.3889, 29.6919>>
			scene.fSwitchRot[0]				= 325.0800
			scene.vSwitchPos[1]				= <<195.2277, -985.6662, 29.0919>>
			scene.fSwitchRot[1]				= 108.0000
			scene.vSwitchPos[2]				= <<145.6692, -1008.5768, 28.4015>>
			scene.fSwitchRot[2]				= 74.2663
			RETURN TRUE
		BREAK


		CASE FLOC_bar_DT
			scene.mFootPans[0].mStart.vPos	= <<248.2705, -1007.8845, 29.7324>>
			scene.mFootPans[0].mStart.vRot	= <<62.7218, 0.0000, -110.9959>>
			scene.mFootPans[0].mEnd.vPos	= <<248.4682, -1007.9604, 29.7240>>
			scene.mFootPans[0].mEnd.vRot	= <<-2.2530, 0.0000, -107.2616>>
			scene.mFootPans[0].fFov			= 40.2691
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<229.6498, -1017.9327, 34.3134>>
			scene.mFootPans[1].mStart.vRot	= <<-8.7545, -0.0430, -60.4954>>
			scene.mFootPans[1].mEnd.vPos	= <<229.6498, -1017.9327, 31.3134>>
			scene.mFootPans[1].mEnd.vRot	= <<-8.7545, -0.0430, -60.4954>>
			scene.mFootPans[1].fFov			= 36.2565
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= FALSE

			scene.mFootPans[2].mStart.vPos	= <<229.6498, -1017.9327, 34.3134>>
			scene.mFootPans[2].mStart.vRot	= <<-8.7545, -0.0430, -60.4954>>
			scene.mFootPans[2].mEnd.vPos	= <<229.6498, -1017.9327, 31.3134>>
			scene.mFootPans[2].mEnd.vRot	= <<-8.7545, -0.0430, -60.4954>>
			scene.mFootPans[2].fFov			= 36.2565
			scene.mFootPans[2].fDuration	= 0.0000
			scene.mFootPans[2].bEnabled		= FALSE

			scene.mFootPans[3].mStart.vPos	= <<245.8116, -1013.3066, 29.7147>>
			scene.mFootPans[3].mStart.vRot	= <<1.2147, 0.0000, -54.9007>>
			scene.mFootPans[3].mEnd.vPos	= <<245.8373, -1013.3431, 29.7147>>
			scene.mFootPans[3].mEnd.vRot	= <<1.2147, 0.0000, -57.7030>>
			scene.mFootPans[3].fFov			= 40.2691
			scene.mFootPans[3].fDuration	= 3.7500
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<244.2720, -1004.5430, 29.5566>>
			scene.mFootPans[4].mStart.vRot	= <<-4.8904, -0.4852, -110.0739>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 45.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<250.6561, -1009.1505, 29.8025>>
			scene.mFootPans[5].mStart.vRot	= <<-5.4451, 0.0000, 14.2227>>
			scene.mFootPans[5].mEnd.vPos	= <<250.6561, -1009.1505, 29.8025>>
			scene.mFootPans[5].mEnd.vRot	= <<-5.4451, 0.0000, 12.1978>>
			scene.mFootPans[5].fFov			= 49.9613
			scene.mFootPans[5].fDuration	= 6.9450
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<250.6561, -1009.1505, 29.8025>>
			scene.mFootCatchupCam.vRot		= <<-5.4451, 0.0000, 14.2227>>
			scene.fFootCatchupFov			= 49.9613

			scene.mCarCatchupCam.vPos		= <<248.7408, -1006.5201, 29.6853>>
			scene.mCarCatchupCam.vRot		= <<-2.7843, 0.0116, -145.7124>>
			scene.fCarCatchupFov			= 49.9613

			scene.vCarPos					= <<247.4841, -1004.8810, 28.1653>>
			scene.fCarRot					= 345.6000
			scene.vPlayerPos				= <<250.0948, -1007.8876, 28.2720>>
			scene.vPedPos[0]				= <<251.6908, -1009.0301, 28.2721>>
			scene.vPedPos[1]				= <<250.5304, -1009.5927, 28.2708>>
			scene.vExitPos					= <<255.7346, -1013.6077, 28.2705>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 0.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 8.1000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<247.3293, -1011.4324, 28.2669>>
			scene.vCarFakeDrivePos			= <<282.2794, -906.4485, 27.9033>>

			scene.mExtraRespot.vCarPos		= <<244.7239, -1014.8864, 28.1332>>
			scene.mExtraRespot.fCarRot		= 348.4800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<265.8661, -1031.1510, 28.2119>>
			scene.fSwitchRot[0]				= 218.5200
			scene.vSwitchPos[1]				= <<279.8312, -962.0749, 28.4125>>
			scene.fSwitchRot[1]				= 266.7600
			scene.vSwitchPos[2]				= <<284.2156, -955.7428, 28.3454>>
			scene.fSwitchRot[2]				= 267.5414
			RETURN TRUE
		BREAK


		CASE FLOC_recCentre_SC
			scene.mFootPans[0].mStart.vPos	= <<-223.0400, -1494.4606, 32.1563>>
			scene.mFootPans[0].mStart.vRot	= <<-2.3031, 0.0137, 170.1147>>
			scene.mFootPans[0].mEnd.vPos	= <<-222.5391, -1494.2854, 32.1495>>
			scene.mFootPans[0].mEnd.vRot	= <<-2.3031, 0.0137, -141.8289>>
			scene.mFootPans[0].fFov			= 33.2670
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-216.6314, -1498.6526, 31.9180>>
			scene.mFootPans[1].mStart.vRot	= <<-1.2591, -0.0294, -155.8858>>
			scene.mFootPans[1].mEnd.vPos	= <<-216.5979, -1498.7275, 31.9162>>
			scene.mFootPans[1].mEnd.vRot	= <<-1.2591, -0.0294, -157.2869>>
			scene.mFootPans[1].fFov			= 17.2517
			scene.mFootPans[1].fDuration	= 0.0000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-216.0729, -1498.9364, 31.8561>>
			scene.mFootPans[2].mStart.vRot	= <<-1.2116, 0.0137, -167.8524>>
			scene.mFootPans[2].mEnd.vPos	= <<-216.0350, -1499.1124, 31.8523>>
			scene.mFootPans[2].mEnd.vRot	= <<-1.2116, 0.0137, -169.2618>>
			scene.mFootPans[2].fFov			= 21.1725
			scene.mFootPans[2].fDuration	= 10.4000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-215.1879, -1494.0775, 31.6461>>
			scene.mFootPans[3].mStart.vRot	= <<-0.3716, 0.0137, 167.1272>>
			scene.mFootPans[3].mEnd.vPos	= <<-215.4108, -1494.4918, 31.6431>>
			scene.mFootPans[3].mEnd.vRot	= <<-0.3716, 0.0137, 164.7599>>
			scene.mFootPans[3].fFov			= 34.7576
			scene.mFootPans[3].fDuration	= 4.8250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-214.1399, -1497.8629, 31.5615>>
			scene.mFootPans[4].mStart.vRot	= <<-1.6427, 0.0712, 174.9291>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 50.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-215.9396, -1502.0917, 31.9881>>
			scene.mFootPans[5].mStart.vRot	= <<-8.1820, 0.0137, -39.2424>>
			scene.mFootPans[5].mEnd.vPos	= <<-215.9396, -1502.0917, 31.9881>>
			scene.mFootPans[5].mEnd.vRot	= <<-8.1820, 0.0137, -42.0890>>
			scene.mFootPans[5].fFov			= 50.0069
			scene.mFootPans[5].fDuration	= 6.7950
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-215.9396, -1502.0917, 31.9881>>
			scene.mFootCatchupCam.vRot		= <<-8.1820, 0.0137, -39.2424>>
			scene.fFootCatchupFov			= 50.0069

			scene.mCarCatchupCam.vPos		= <<-213.5682, -1500.0767, 31.8540>>
			scene.mCarCatchupCam.vRot		= <<-2.4576, 0.0837, 111.3005>>
			scene.fCarCatchupFov			= 50.0069

			scene.vCarPos					= <<-209.7381, -1495.0710, 30.2667>>
			scene.fCarRot					= 141.4800
			scene.vPlayerPos				= <<-215.1646, -1500.8119, 30.4305>>
			scene.vPedPos[0]				= <<-215.0512, -1503.3674, 30.4623>>
			scene.vPedPos[1]				= <<-215.9888, -1503.2892, 30.4622>>
			scene.vExitPos					= <<-222.2676, -1510.9921, 30.6938>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.1750
			scene.fCarFinalDelay0			= 0.5400
			scene.fCarFinalDelay			= 9.6900
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-206.9685, -1494.6144, 30.4733>>
			scene.vCarFakeDrivePos			= <<-162.9734, -1508.3855, 32.3938>>

			scene.mExtraRespot.vCarPos		= <<-224.7263, -1488.1644, 30.3038>>
			scene.mExtraRespot.fCarRot		= 139.6800
			scene.mExtraRespot.expandDir	= 8

			scene.vSwitchPos[0]				= <<-239.6648, -1549.7312, 30.6344>>
			scene.fSwitchRot[0]				= 136.4400
			scene.vSwitchPos[1]				= <<-263.6150, -1540.6875, 30.6904>>
			scene.fSwitchRot[1]				= 187.5600
			scene.vSwitchPos[2]				= <<-144.4396, -1549.4728, 33.4471>>
			scene.fSwitchRot[2]				= 139.0999
			RETURN TRUE
		BREAK


		CASE FLOC_shoppingPlaza_VB
			scene.mFootPans[0].mStart.vPos	= <<-1181.3975, -826.8450, 16.5792>>
			scene.mFootPans[0].mStart.vRot	= <<9.6959, 0.0000, 47.2246>>
			scene.mFootPans[0].mEnd.vPos	= <<-1181.9803, -827.2338, 14.7335>>
			scene.mFootPans[0].mEnd.vRot	= <<4.1175, 0.0000, 52.9134>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-1184.3304, -826.4483, 14.7437>>
			scene.mFootPans[1].mStart.vRot	= <<-0.6489, -0.0007, 62.8269>>
			scene.mFootPans[1].mEnd.vPos	= <<-1184.5710, -826.3248, 14.7407>>
			scene.mFootPans[1].mEnd.vRot	= <<-0.1044, -0.0007, 62.8269>>
			scene.mFootPans[1].fFov			= 18.4352
			scene.mFootPans[1].fDuration	= 5.5500
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-1184.3384, -826.9095, 14.7463>>
			scene.mFootPans[2].mStart.vRot	= <<-0.6489, -0.0007, 50.0980>>
			scene.mFootPans[2].mEnd.vPos	= <<-1184.4329, -826.8304, 14.7449>>
			scene.mFootPans[2].mEnd.vRot	= <<-0.6489, -0.0007, 48.6321>>
			scene.mFootPans[2].fFov			= 18.4352
			scene.mFootPans[2].fDuration	= 11.4250
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-1183.2001, -830.7167, 14.5212>>
			scene.mFootPans[3].mStart.vRot	= <<1.0563, 0.0000, 27.3790>>
			scene.mFootPans[3].mEnd.vPos	= <<-1183.0219, -830.6245, 14.5212>>
			scene.mFootPans[3].mEnd.vRot	= <<2.0449, 0.0000, 26.6091>>
			scene.mFootPans[3].fFov			= 42.0117
			scene.mFootPans[3].fDuration	= 5.2000
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-1188.4661, -824.3413, 14.8546>>
			scene.mFootPans[4].mStart.vRot	= <<1.5615, -0.1037, -118.6105>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 53.3860
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-1186.4714, -824.6526, 14.8865>>
			scene.mFootPans[5].mStart.vRot	= <<-3.8721, 0.0000, 169.2043>>
			scene.mFootPans[5].mEnd.vPos	= <<-1186.4714, -824.6526, 14.8865>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.8721, 0.0000, 173.8093>>
			scene.mFootPans[5].fFov			= 49.9849
			scene.mFootPans[5].fDuration	= 5.5200
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-1186.4714, -824.6526, 14.8865>>
			scene.mFootCatchupCam.vRot		= <<-3.8721, 0.0000, 169.2043>>
			scene.fFootCatchupFov			= 49.9849

			scene.mCarCatchupCam.vPos		= <<-1186.4286, -827.6542, 14.7835>>
			scene.mCarCatchupCam.vRot		= <<-0.7502, 0.0313, -9.5858>>
			scene.fCarCatchupFov			= 49.9849

			scene.vCarPos					= <<-1189.1278, -832.9972, 13.2161>>
			scene.fCarRot					= 218.5200
			scene.vPlayerPos				= <<-1186.4250, -825.9056, 13.3321>>
			scene.vPedPos[0]				= <<-1187.7395, -824.2661, 13.3448>>
			scene.vPedPos[1]				= <<-1186.3999, -824.4038, 13.3008>>
			scene.vExitPos					= <<-1190.9611, -806.6936, 17.0174>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.0000
			scene.fCarFinalDelay0			= 0.2700
			scene.fCarFinalDelay			= 8.6250
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-1192.5244, -834.0498, 13.3085>>
			scene.vCarFakeDrivePos			= <<-1131.6940, -912.2311, 2.2148>>

			scene.mExtraRespot.vCarPos		= <<-1196.7207, -872.5267, 12.5092>>
			scene.mExtraRespot.fCarRot		= 299.5200
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-1265.4348, -727.5261, 21.1402>>
			scene.fSwitchRot[0]				= 25.9200
			scene.vSwitchPos[1]				= <<-1222.2693, -784.6736, 16.6973>>
			scene.fSwitchRot[1]				= 30.9600
			scene.vSwitchPos[2]				= <<-1140.7695, -935.9708, 1.6022>>
			scene.fSwitchRot[2]				= 125.0739
			RETURN TRUE
		BREAK


		CASE FLOC_bar_VB
			scene.mFootPans[0].mStart.vPos	= <<-1400.4379, -585.9051, 30.9432>>
			scene.mFootPans[0].mStart.vRot	= <<32.3305, 0.0000, -105.6953>>
			scene.mFootPans[0].mEnd.vPos	= <<-1400.3621, -585.9331, 30.9394>>
			scene.mFootPans[0].mEnd.vRot	= <<-2.7052, 0.0000, -110.3139>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-1397.6010, -589.7619, 30.8651>>
			scene.mFootPans[1].mStart.vRot	= <<-4.6041, 0.0007, -69.4751>>
			scene.mFootPans[1].mEnd.vPos	= <<-1397.4280, -589.6965, 30.8501>>
			scene.mFootPans[1].mEnd.vRot	= <<-4.6041, 0.0007, -69.4751>>
			scene.mFootPans[1].fFov			= 25.7248
			scene.mFootPans[1].fDuration	= 5.0750
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-1398.3654, -590.4213, 30.7041>>
			scene.mFootPans[2].mStart.vRot	= <<-1.8133, 0.0007, -54.5359>>
			scene.mFootPans[2].mEnd.vPos	= <<-1398.2682, -590.3511, 30.7003>>
			scene.mFootPans[2].mEnd.vRot	= <<-1.8133, 0.0007, -53.8792>>
			scene.mFootPans[2].fFov			= 25.7248
			scene.mFootPans[2].fDuration	= 10.0500
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-1392.4342, -584.4755, 30.6697>>
			scene.mFootPans[3].mStart.vRot	= <<-3.5396, 0.0000, 147.9476>>
			scene.mFootPans[3].mEnd.vPos	= <<-1392.6837, -584.8740, 30.6406>>
			scene.mFootPans[3].mEnd.vRot	= <<-2.1681, 0.0000, 149.1801>>
			scene.mFootPans[3].fFov			= 32.1336
			scene.mFootPans[3].fDuration	= 8.0250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-1395.9674, -590.8463, 30.9775>>
			scene.mFootPans[5].mStart.vRot	= <<-7.0494, 0.0000, -16.7103>>
			scene.mFootPans[5].mEnd.vPos	= <<-1395.9674, -590.8463, 30.9775>>
			scene.mFootPans[5].mEnd.vRot	= <<-7.0494, 0.0000, -2.2177>>
			scene.mFootPans[5].fFov			= 49.3689
			scene.mFootPans[5].fDuration	= 4.7850
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-1395.9674, -590.8463, 30.9775>>
			scene.mFootCatchupCam.vRot		= <<-7.0494, 0.0000, -16.7103>>
			scene.fFootCatchupFov			= 49.3689

			scene.mCarCatchupCam.vPos		= <<-1395.9674, -590.8463, 30.9775>>
			scene.mCarCatchupCam.vRot		= <<-7.0494, 0.0000, -2.2177>>
			scene.fCarCatchupFov			= 49.3689

			scene.vCarPos					= <<-1396.8634, -583.7726, 29.2332>>
			scene.fCarRot					= 305.2800
			scene.vPlayerPos				= <<-1395.6896, -589.4706, 29.2947>>
			scene.vPedPos[0]				= <<-1394.4778, -588.1750, 29.2765>>
			scene.vPedPos[1]				= <<-1395.2493, -586.9717, 29.2791>>
			scene.vExitPos					= <<-1387.3744, -586.2861, 29.2117>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 8.1600
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-1395.3645, -585.9979, 29.2759>>
			scene.vCarFakeDrivePos			= <<-1437.2358, -602.3437, 29.7106>>

			scene.mExtraRespot.vCarPos		= <<-1402.1808, -587.1375, 29.2292>>
			scene.mExtraRespot.fCarRot		= 301.6800
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-1363.9341, -597.2744, 28.1150>>
			scene.fSwitchRot[0]				= 212.4000
			scene.vSwitchPos[1]				= <<-1317.0858, -534.9244, 31.6545>>
			scene.fSwitchRot[1]				= 284.0400
			scene.vSwitchPos[2]				= <<-1521.9221, -692.8118, 27.4762>>
			scene.fSwitchRot[2]				= 232.8838
			RETURN TRUE
		BREAK


		CASE FLOC_minimartCarpark_MW
			scene.mFootPans[0].mStart.vPos	= <<-1366.9222, -398.7310, 41.0798>>
			scene.mFootPans[0].mStart.vRot	= <<17.4675, -0.1845, -77.0057>>
			scene.mFootPans[0].mEnd.vPos	= <<-1366.9297, -398.9865, 40.0866>>
			scene.mFootPans[0].mEnd.vRot	= <<-5.1001, -0.1845, -65.4537>>
			scene.mFootPans[0].fFov			= 30.8234
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-1345.7203, -386.2633, 37.2103>>
			scene.mFootPans[1].mStart.vRot	= <<-1.0508, -0.1845, -61.7459>>
			scene.mFootPans[1].mEnd.vPos	= <<-1345.5809, -386.1886, 37.2074>>
			scene.mFootPans[1].mEnd.vRot	= <<-1.0508, -0.1845, -61.7459>>
			scene.mFootPans[1].fFov			= 21.5932
			scene.mFootPans[1].fDuration	= 6.9250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-1345.9550, -386.2108, 37.2211>>
			scene.mFootPans[2].mStart.vRot	= <<-3.4291, -0.1845, -72.0063>>
			scene.mFootPans[2].mEnd.vPos	= <<-1345.7772, -386.1530, 37.2099>>
			scene.mFootPans[2].mEnd.vRot	= <<-3.1850, -0.1845, -72.0063>>
			scene.mFootPans[2].fFov			= 21.5932
			scene.mFootPans[2].fDuration	= 10.0750
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-1337.8141, -390.8861, 37.0116>>
			scene.mFootPans[3].mStart.vRot	= <<-0.8397, -0.1845, 42.1048>>
			scene.mFootPans[3].mEnd.vPos	= <<-1338.0637, -391.1115, 37.0105>>
			scene.mFootPans[3].mEnd.vRot	= <<-0.8397, -0.1845, 42.1048>>
			scene.mFootPans[3].fFov			= 33.8628
			scene.mFootPans[3].fDuration	= 10.3750
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<-1347.6074, -384.3000, 37.2427>>
			scene.mFootPans[4].mStart.vRot	= <<-5.1716, -0.1784, -101.7165>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 33.8628
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-1342.9149, -385.4987, 37.2378>>
			scene.mFootPans[5].mStart.vRot	= <<-3.0101, -0.1845, 62.5899>>
			scene.mFootPans[5].mEnd.vPos	= <<-1342.9149, -385.4987, 37.2378>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.0101, -0.1845, 49.9371>>
			scene.mFootPans[5].fFov			= 49.9943
			scene.mFootPans[5].fDuration	= 5.5200
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-1342.9149, -385.4987, 37.2378>>
			scene.mFootCatchupCam.vRot		= <<-3.0101, -0.1845, 62.5899>>
			scene.fFootCatchupFov			= 49.9943

			scene.mCarCatchupCam.vPos		= <<-1344.6967, -383.5359, 37.1767>>
			scene.mCarCatchupCam.vRot		= <<-7.7089, -0.2200, -177.5366>>
			scene.fCarCatchupFov			= 49.9943

			scene.vCarPos					= <<-1346.6592, -382.4048, 35.7550>>
			scene.fCarRot					= 304.2000
			scene.vPlayerPos				= <<-1344.3192, -385.1048, 35.7534>>
			scene.vPedPos[0]				= <<-1342.3458, -384.7795, 35.7455>>
			scene.vPedPos[1]				= <<-1342.1832, -385.7662, 35.7450>>
			scene.vExitPos					= <<-1315.9821, -383.2860, 35.7237>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.0000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 5.6850
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-1341.6716, -376.5873, 35.7475>>
			scene.vCarFakeDrivePos			= <<-1275.4410, -344.9638, 35.6584>>

			scene.mExtraRespot.vCarPos		= <<-1326.1108, -395.0894, 35.4495>>
			scene.mExtraRespot.fCarRot		= 212.4000
			scene.mExtraRespot.expandDir	= 8

			scene.vSwitchPos[0]				= <<-1300.4432, -383.3367, 35.5787>>
			scene.fSwitchRot[0]				= 208.4400
			scene.vSwitchPos[1]				= <<-1331.9393, -325.6764, 36.3622>>
			scene.fSwitchRot[1]				= 29.8800
			scene.vSwitchPos[2]				= <<-1293.1658, -383.7935, 35.5591>>
			scene.fSwitchRot[2]				= 207.2801
			RETURN TRUE
		BREAK


		CASE FLOC_paletoMainSt_PA
			scene.mFootPans[0].mStart.vPos	= <<-12.9499, 6502.8452, 32.7873>>
			scene.mFootPans[0].mStart.vRot	= <<9.5209, 0.0000, 28.9591>>
			scene.mFootPans[0].mEnd.vPos	= <<-12.9172, 6502.9810, 32.0578>>
			scene.mFootPans[0].mEnd.vRot	= <<-3.2163, 0.0000, 28.4487>>
			scene.mFootPans[0].fFov			= 31.3500
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-15.8414, 6504.5591, 32.0731>>
			scene.mFootPans[1].mStart.vRot	= <<-2.4909, 0.0000, -79.5938>>
			scene.mFootPans[1].mEnd.vPos	= <<-15.7190, 6504.5776, 32.0677>>
			scene.mFootPans[1].mEnd.vRot	= <<-2.4909, 0.0000, -79.5938>>
			scene.mFootPans[1].fFov			= 26.7402
			scene.mFootPans[1].fDuration	= 6.8000
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-15.2964, 6503.4614, 32.0741>>
			scene.mFootPans[2].mStart.vRot	= <<-3.2163, 0.0000, -40.2756>>
			scene.mFootPans[2].mEnd.vPos	= <<-15.2178, 6503.5542, 32.0673>>
			scene.mFootPans[2].mEnd.vRot	= <<-3.2163, 0.0000, -40.2756>>
			scene.mFootPans[2].fFov			= 31.3500
			scene.mFootPans[2].fDuration	= 9.4250
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-17.4111, 6499.1289, 31.9122>>
			scene.mFootPans[3].mStart.vRot	= <<-1.2605, 0.0000, -35.0484>>
			scene.mFootPans[3].mEnd.vPos	= <<-17.3962, 6499.2446, 31.9101>>
			scene.mFootPans[3].mEnd.vRot	= <<-1.2605, 0.0000, -38.0048>>
			scene.mFootPans[3].fFov			= 28.2785
			scene.mFootPans[3].fDuration	= 10.8250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-13.4570, 6504.1426, 32.0189>>
			scene.mFootPans[5].mStart.vRot	= <<-5.4280, 0.0000, 29.7930>>
			scene.mFootPans[5].mEnd.vPos	= <<-13.4570, 6504.1426, 32.0189>>
			scene.mFootPans[5].mEnd.vRot	= <<-5.4280, 0.0000, 25.0133>>
			scene.mFootPans[5].fFov			= 49.9863
			scene.mFootPans[5].fDuration	= 5.1000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-13.4570, 6504.1426, 32.0189>>
			scene.mFootCatchupCam.vRot		= <<-5.4280, 0.0000, 29.7930>>
			scene.fFootCatchupFov			= 49.9863

			scene.mCarCatchupCam.vPos		= <<-14.0094, 6506.8496, 32.0390>>
			scene.mCarCatchupCam.vRot		= <<-7.8212, -0.0000, 154.0325>>
			scene.fCarCatchupFov			= 49.9863

			scene.vCarPos					= <<-14.8887, 6510.3130, 30.2857>>
			scene.fCarRot					= 317.1600
			scene.vPlayerPos				= <<-14.6068, 6505.2427, 30.5313>>
			scene.vPedPos[0]				= <<-13.4060, 6504.6445, 30.5605>>
			scene.vPedPos[1]				= <<-13.3913, 6506.0820, 30.5421>>
			scene.vExitPos					= <<-2.3765, 6505.2393, 30.4432>>
			scene.fFootSpeechDelay			= 2.5050
			scene.fFootFinalDelay			= 4.1250
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 7.3200
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<-14.5603, 6510.5703, 30.2871>>
			scene.vCarFakeDrivePos			= <<21.5285, 6548.2393, 30.2918>>

			scene.mExtraRespot.vCarPos		= <<-22.4419, 6502.5464, 30.2973>>
			scene.mExtraRespot.fCarRot		= 313.5600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<0.2704, 6440.6191, 30.4253>>
			scene.fSwitchRot[0]				= 226.0800
			scene.vSwitchPos[1]				= <<42.3786, 6499.0776, 30.4253>>
			scene.fSwitchRot[1]				= 230.7600
			scene.vSwitchPos[2]				= <<85.8481, 6593.3062, 30.4944>>
			scene.fSwitchRot[2]				= 229.9428
			RETURN TRUE
		BREAK

		CASE FLOC_minimartCarpark_SS
			scene.mFootPans[0].mStart.vPos	= <<1398.3656, 3589.7920, 35.7675>>
			scene.mFootPans[0].mStart.vRot	= <<16.1439, 0.0000, 12.7999>>
			scene.mFootPans[0].mEnd.vPos	= <<1398.3864, 3589.7795, 35.4151>>
			scene.mFootPans[0].mEnd.vRot	= <<-3.1277, 0.0000, 60.2753>>
			scene.mFootPans[0].fFov			= 41.4763
			scene.mFootPans[0].fDuration	= 6.4500
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<1393.1771, 3589.7578, 35.3621>>
			scene.mFootPans[1].mStart.vRot	= <<-1.7755, 0.0000, -47.7455>>
			scene.mFootPans[1].mEnd.vPos	= <<1393.2351, 3589.8105, 35.3597>>
			scene.mFootPans[1].mEnd.vRot	= <<-1.7755, 0.0000, -47.7455>>
			scene.mFootPans[1].fFov			= 25.6712
			scene.mFootPans[1].fDuration	= 5.9250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<1393.2328, 3589.3816, 35.3779>>
			scene.mFootPans[2].mStart.vRot	= <<-4.1375, 0.0000, -43.9328>>
			scene.mFootPans[2].mEnd.vPos	= <<1393.3842, 3589.5383, 35.3621>>
			scene.mFootPans[2].mEnd.vRot	= <<-3.8599, 0.0000, -43.0485>>
			scene.mFootPans[2].fFov			= 25.6712
			scene.mFootPans[2].fDuration	= 10.6750
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<1401.4360, 3593.2778, 34.9226>>
			scene.mFootPans[3].mStart.vRot	= <<4.7665, 0.0000, 89.9214>>
			scene.mFootPans[3].mEnd.vPos	= <<1401.4110, 3593.2778, 34.9247>>
			scene.mFootPans[3].mEnd.vRot	= <<2.3666, 0.0000, 93.3675>>
			scene.mFootPans[3].fFov			= 40.2577
			scene.mFootPans[3].fDuration	= 7.0250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<1393.0144, 3590.3909, 35.3415>>
			scene.mFootPans[5].mStart.vRot	= <<-3.6763, 0.0000, -73.5766>>
			scene.mFootPans[5].mEnd.vPos	= <<1393.0144, 3590.3909, 35.3415>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.6763, 0.0000, -76.3552>>
			scene.mFootPans[5].fFov			= 49.9894
			scene.mFootPans[5].fDuration	= 5.8950
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<1393.0144, 3590.3909, 35.3415>>
			scene.mFootCatchupCam.vRot		= <<-3.6763, 0.0000, -73.5766>>
			scene.fFootCatchupFov			= 49.9894

			scene.mCarCatchupCam.vPos		= <<1395.9813, 3590.8003, 35.2591>>
			scene.mCarCatchupCam.vRot		= <<-1.4534, -0.0615, 64.6092>>
			scene.fCarCatchupFov			= 49.9894

			scene.vCarPos					= <<1401.1758, 3591.5845, 33.9257>>
			scene.fCarRot					= 287.2800
			scene.vPlayerPos				= <<1394.2062, 3591.3108, 33.8731>>
			scene.vPedPos[0]				= <<1395.7977, 3592.1204, 33.8850>>
			scene.vPedPos[1]				= <<1395.8606, 3591.1497, 33.8331>>
			scene.vExitPos					= <<1394.2177, 3599.6748, 33.9149>>
			scene.fFootSpeechDelay			= 3.5050
			scene.fFootFinalDelay			= 4.1250
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 8.2050
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= FALSE

			scene.vCarFakeWalkPos			= <<1398.3474, 3589.3406, 33.9425>>
			scene.vCarFakeDrivePos			= <<1489.7838, 3614.9602, 33.8271>>

			scene.mExtraRespot.vCarPos		= <<1382.2739, 3593.8064, 33.8937>>
			scene.mExtraRespot.fCarRot		= 12.9600
			scene.mExtraRespot.expandDir	= 5

			scene.vSwitchPos[0]				= <<1452.4491, 3638.9067, 33.6778>>
			scene.fSwitchRot[0]				= 13.6800
			scene.vSwitchPos[1]				= <<1347.2308, 3612.0645, 33.8760>>
			scene.fSwitchRot[1]				= 28.8212
			scene.vSwitchPos[2]				= <<1459.6494, 3651.6704, 33.5887>>
			scene.fSwitchRot[2]				= 14.7733
			RETURN TRUE
		BREAK

	ENDSWITCH

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FRIENDS, "Private_FLOC_GetDropoffScene(): No data for ", GetLabel_enumFriendLocation(eFriendLoc))
		SCRIPT_ASSERT("Private_FLOC_GetDropoffScene(): friend loc not supported")

		VECTOR vAnchor = g_FriendLocations[eFriendLoc].vPickupCoord
		
		scene.mFootPans[0].mStart.vPos	= vAnchor + <<-10.0, 0.0, 3.0>>
		scene.mFootPans[0].mStart.vRot	= <<0.0, 0.0, 0.0>>
		scene.mFootPans[0].mEnd.vPos	= vAnchor + <<-10.0, 0.0, 1.0>>
		scene.mFootPans[0].mEnd.vRot	= <<0.0, 0.0, 0.0>>
		scene.mFootPans[0].fFov			= 50.0
		
		scene.mFootPans[1].bEnabled		= FALSE
		scene.mFootPans[2].bEnabled		= FALSE
		scene.mFootPans[3].bEnabled		= FALSE
		
		scene.mFootCatchupCam.vPos			= vAnchor + <<-10.0, 0.0, 3.0>>
		scene.mFootCatchupCam.vRot			= <<0.0, 0.0, 0.0>>
		scene.fFootCatchupFov					= 50

		scene.vCarPos				= vAnchor
		scene.fCarRot				= 0.0
		scene.vPlayerPos			= vAnchor
		scene.vPedPos[0]			= vAnchor + <<0.6, 0.3, 0.0>>
		scene.vPedPos[1]			= vAnchor + <<0.6, -0.3, 0.0>>
		scene.vExitPos				= vAnchor + <<5.0, 0.0, 0.0>>

		scene.vSwitchPos[0]			= vAnchor
		scene.fSwitchRot[0]			= 0.0
		scene.vSwitchPos[1]			= vAnchor
		scene.fSwitchRot[1]			= 0.0
	#ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL Private_ALOC_GetDropoffScene(enumActivityLocation eActivityLoc, structFDropoffScene& scene)

	// Get loc-specific positions
	SWITCH eActivityLoc

		CASE ALOC_cinema_downtown
			scene.mFootPans[0].mStart.vPos	= <<405.9127, -711.7676, 31.2618>>
			scene.mFootPans[0].mStart.vRot	= <<19.9190, 0.0000, 90.3396>>
			scene.mFootPans[0].mEnd.vPos	= <<405.4205, -711.7840, 29.3979>>
			scene.mFootPans[0].mEnd.vRot	= <<5.5077, 0.0000, 99.6259>>
			scene.mFootPans[0].fFov			= 50.0000
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<398.0591, -714.2009, 29.6786>>
			scene.mFootPans[1].mStart.vRot	= <<-2.2484, 0.0024, 51.3791>>
			scene.mFootPans[1].mEnd.vPos	= <<397.9690, -714.1289, 29.6741>>
			scene.mFootPans[1].mEnd.vRot	= <<-2.2484, 0.0024, 51.1740>>
			scene.mFootPans[1].fFov			= 38.1457
			scene.mFootPans[1].fDuration	= 5.5800
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<398.3901, -714.3004, 29.7155>>
			scene.mFootPans[2].mStart.vRot	= <<-1.9825, -0.0437, 50.2219>>
			scene.mFootPans[2].mEnd.vPos	= <<398.3167, -714.2393, 29.7122>>
			scene.mFootPans[2].mEnd.vRot	= <<-1.9825, -0.0437, 48.0760>>
			scene.mFootPans[2].fFov			= 39.2093
			scene.mFootPans[2].fDuration	= 13.0000
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<405.5188, -709.9571, 29.6613>>
			scene.mFootPans[3].mStart.vRot	= <<-2.8617, 0.0312, 110.4090>>
			scene.mFootPans[3].mEnd.vPos	= <<405.8275, -709.2303, 29.6882>>
			scene.mFootPans[3].mEnd.vRot	= <<-2.8617, 0.0312, 103.4981>>
			scene.mFootPans[3].fFov			= 38.1457
			scene.mFootPans[3].fDuration	= 8.3750
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<397.4691, -712.0601, 29.7563>>
			scene.mFootPans[5].mStart.vRot	= <<-5.9870, -0.0170, 154.1207>>
			scene.mFootPans[5].mEnd.vPos	= <<397.4691, -712.0601, 29.7563>>
			scene.mFootPans[5].mEnd.vRot	= <<-5.9870, -0.0170, 154.1207>>
			scene.mFootPans[5].fFov			= 50.0000
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<397.4691, -712.0601, 29.7563>>
			scene.mFootCatchupCam.vRot		= <<-5.9870, -0.0170, 154.1207>>
			scene.fFootCatchupFov			= 50.0000

			scene.mCarCatchupCam.vPos		= <<397.6112, -710.4716, 29.7051>>
			scene.mCarCatchupCam.vRot		= <<4.6835, -0.3053, 163.9312>>
			scene.fCarCatchupFov			= 50.0000

			scene.vCarPos					= <<401.2501, -709.6287, 28.1688>>
			scene.fCarRot					= 0.0000
			scene.vPlayerPos				= <<396.8529, -713.7012, 28.2854>>
			scene.vPedPos[0]				= <<395.8499, -711.9299, 28.2849>>
			scene.vPedPos[1]				= <<397.0676, -712.2994, 28.2850>>
			scene.vExitPos					= <<396.5359, -692.8924, 28.2871>>
			scene.fFootSpeechDelay			= 1.5500
			scene.fFootFinalDelay			= 7.7050
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 5.1150
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= TRUE

			scene.vCarFakeWalkPos			= <<398.3131, -708.6296, 28.2844>>
			scene.vCarFakeDrivePos			= <<402.7359, -689.6898, 28.2549>>

			scene.mExtraRespot.vCarPos		= <<401.3918, -718.0711, 28.1762>>
			scene.mExtraRespot.fCarRot		= 0.0000
			scene.mExtraRespot.expandDir	= 10

			scene.vSwitchPos[0]				= <<390.1166, -686.0222, 28.2700>>
			scene.fSwitchRot[0]				= 94.1853
			scene.vSwitchPos[1]				= <<444.7804, -686.5525, 27.5881>>
			scene.fSwitchRot[1]				= 270.7200
			scene.vSwitchPos[2]				= <<382.3910, -671.3192, 28.2481>>
			scene.fSwitchRot[2]				= 57.3318
			RETURN TRUE
		BREAK


		CASE ALOC_cinema_morningwood
			scene.mFootPans[0].mStart.vPos	= <<-1405.1483, -195.8240, 50.5379>>
			scene.mFootPans[0].mStart.vRot	= <<15.4864, 0.0000, 126.9167>>
			scene.mFootPans[0].mEnd.vPos	= <<-1405.4717, -195.2269, 48.3459>>
			scene.mFootPans[0].mEnd.vRot	= <<-6.4786, 0.0000, 133.4543>>
			scene.mFootPans[0].fFov			= 43.8728
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<-1414.1422, -200.2502, 47.8477>>
			scene.mFootPans[1].mStart.vRot	= <<-3.1055, 0.0000, -105.8073>>
			scene.mFootPans[1].mEnd.vPos	= <<-1414.1171, -200.2495, 47.8464>>
			scene.mFootPans[1].mEnd.vRot	= <<-3.1055, 0.0000, -105.1865>>
			scene.mFootPans[1].fFov			= 35.8476
			scene.mFootPans[1].fDuration	= 8.4250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<-1414.2623, -199.6994, 47.8791>>
			scene.mFootPans[2].mStart.vRot	= <<-3.8011, 0.0000, -118.3642>>
			scene.mFootPans[2].mEnd.vPos	= <<-1414.0961, -199.7538, 47.8676>>
			scene.mFootPans[2].mEnd.vRot	= <<-3.8011, 0.0000, -119.1302>>
			scene.mFootPans[2].fFov			= 35.8476
			scene.mFootPans[2].fDuration	= 11.1750
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<-1410.3827, -193.2492, 47.9417>>
			scene.mFootPans[3].mStart.vRot	= <<-2.9923, 0.0000, 176.5927>>
			scene.mFootPans[3].mEnd.vPos	= <<-1410.3827, -193.2492, 47.9417>>
			scene.mFootPans[3].mEnd.vRot	= <<-2.9923, 0.0000, -172.9450>>
			scene.mFootPans[3].fFov			= 40.3821
			scene.mFootPans[3].fDuration	= 6.3500
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<-1411.7035, -200.2036, 47.7778>>
			scene.mFootPans[5].mStart.vRot	= <<-3.8893, 0.0000, 130.3052>>
			scene.mFootPans[5].mEnd.vPos	= <<-1411.7035, -200.2036, 47.7778>>
			scene.mFootPans[5].mEnd.vRot	= <<-3.8893, 0.0000, 130.3052>>
			scene.mFootPans[5].fFov			= 50.0049
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<-1411.7035, -200.2036, 47.7778>>
			scene.mFootCatchupCam.vRot		= <<-3.8893, 0.0000, 130.3052>>
			scene.fFootCatchupFov			= 50.0049

			scene.mCarCatchupCam.vPos		= <<-1411.7035, -200.2036, 47.7778>>
			scene.mCarCatchupCam.vRot		= <<-3.8893, 0.0000, 130.3052>>
			scene.fCarCatchupFov			= 50.0049

			scene.vCarPos					= <<-1407.5757, -199.3733, 46.0344>>
			scene.fCarRot					= 216.2500
			scene.vPlayerPos				= <<-1412.9637, -200.9753, 46.2845>>
			scene.vPedPos[0]				= <<-1410.9816, -200.6391, 46.2294>>
			scene.vPedPos[1]				= <<-1411.2316, -201.4050, 46.2381>>
			scene.vExitPos					= <<-1427.7881, -187.1937, 46.4753>>
			scene.fFootSpeechDelay			= 1.7500
			scene.fFootFinalDelay			= 9.8400
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= TRUE

			scene.vCarFakeWalkPos			= <<-1427.7881, -187.1937, 46.4753>>
			scene.vCarFakeDrivePos			= <<-1358.1135, -272.4378, 41.4143>>

			scene.mExtraRespot.vCarPos		= <<-1439.4601, -202.4132, 46.6154>>
			scene.mExtraRespot.fCarRot		= 322.5600
			scene.mExtraRespot.expandDir	= 9

			scene.vSwitchPos[0]				= <<-1383.7635, -264.7188, 41.9284>>
			scene.fSwitchRot[0]				= 130.3200
			scene.vSwitchPos[1]				= <<-1473.4860, -257.2262, 48.9357>>
			scene.fSwitchRot[1]				= 44.6400
			scene.vSwitchPos[2]				= <<-1382.0347, -271.8694, 41.9838>>
			scene.fSwitchRot[2]				= 132.0433
			RETURN TRUE
		BREAK


		CASE ALOC_cinema_vinewood
			scene.mFootPans[0].mStart.vPos	= <<290.8533, 172.5629, 105.4460>>
			scene.mFootPans[0].mStart.vRot	= <<24.0829, 0.0000, -19.9766>>
			scene.mFootPans[0].mEnd.vPos	= <<290.8804, 172.6505, 105.1818>>
			scene.mFootPans[0].mEnd.vRot	= <<-3.3518, 0.0000, -17.3689>>
			scene.mFootPans[0].fFov			= 36.3239
			scene.mFootPans[0].fDuration	= 5.0000
			scene.mFootPans[0].bEnabled		= TRUE

			scene.mFootPans[1].mStart.vPos	= <<298.6032, 181.1218, 104.7438>>
			scene.mFootPans[1].mStart.vRot	= <<-3.2463, 0.0000, 81.3680>>
			scene.mFootPans[1].mEnd.vPos	= <<298.5056, 181.1366, 104.7382>>
			scene.mFootPans[1].mEnd.vRot	= <<-3.1642, 0.0000, 81.8605>>
			scene.mFootPans[1].fFov			= 35.1220
			scene.mFootPans[1].fDuration	= 6.6250
			scene.mFootPans[1].bEnabled		= TRUE

			scene.mFootPans[2].mStart.vPos	= <<298.3110, 180.8920, 104.7343>>
			scene.mFootPans[2].mStart.vRot	= <<-3.3284, 0.0000, 74.3088>>
			scene.mFootPans[2].mEnd.vPos	= <<298.2302, 180.9147, 104.7294>>
			scene.mFootPans[2].mEnd.vRot	= <<-2.3434, 0.0000, 73.7343>>
			scene.mFootPans[2].fFov			= 35.1220
			scene.mFootPans[2].fDuration	= 10.1750
			scene.mFootPans[2].bEnabled		= TRUE

			scene.mFootPans[3].mStart.vPos	= <<299.5133, 174.0084, 104.6379>>
			scene.mFootPans[3].mStart.vRot	= <<0.7548, 0.0000, 40.5820>>
			scene.mFootPans[3].mEnd.vPos	= <<299.5133, 174.0084, 104.6379>>
			scene.mFootPans[3].mEnd.vRot	= <<0.7548, 0.0000, 60.3641>>
			scene.mFootPans[3].fFov			= 42.6649
			scene.mFootPans[3].fDuration	= 6.9250
			scene.mFootPans[3].bEnabled		= TRUE

			scene.mFootPans[4].mStart.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mStart.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vPos	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].mEnd.vRot	= <<0.0000, 0.0000, 0.0000>>
			scene.mFootPans[4].fFov			= 0.0000
			scene.mFootPans[4].fDuration	= 0.0000
			scene.mFootPans[4].bEnabled		= FALSE

			scene.mFootPans[5].mStart.vPos	= <<295.9940, 180.8288, 104.8471>>
			scene.mFootPans[5].mStart.vRot	= <<-9.6963, 0.0000, -42.3682>>
			scene.mFootPans[5].mEnd.vPos	= <<295.9940, 180.8288, 104.8471>>
			scene.mFootPans[5].mEnd.vRot	= <<-9.6963, 0.0000, -42.3682>>
			scene.mFootPans[5].fFov			= 49.9720
			scene.mFootPans[5].fDuration	= 0.0000
			scene.mFootPans[5].bEnabled		= TRUE

			scene.mFootCatchupCam.vPos		= <<295.9940, 180.8288, 104.8471>>
			scene.mFootCatchupCam.vRot		= <<-9.6963, 0.0000, -42.3682>>
			scene.fFootCatchupFov			= 49.9720

			scene.mCarCatchupCam.vPos		= <<295.9940, 180.8288, 104.8471>>
			scene.mCarCatchupCam.vRot		= <<-9.6963, 0.0000, -42.3682>>
			scene.fCarCatchupFov			= 49.9720

			scene.vCarPos					= <<290.7559, 177.1301, 103.1523>>
			scene.fCarRot					= 74.5200
			scene.vPlayerPos				= <<297.1597, 181.7558, 103.2131>>
			scene.vPedPos[0]				= <<295.0739, 181.1341, 103.2455>>
			scene.vPedPos[1]				= <<294.9557, 182.0589, 103.2534>>
			scene.vExitPos					= <<301.1894, 190.1084, 103.1125>>
			scene.fFootSpeechDelay			= 1.6500
			scene.fFootFinalDelay			= 11.1000
			scene.fCarFinalDelay0			= 0.0000
			scene.fCarFinalDelay			= 0.0000
			scene.bFootCam2IsAlternative	= TRUE
			scene.bIsTaxiStyle				= TRUE

			scene.vCarFakeWalkPos			= <<301.1894, 190.1084, 103.1125>>
			scene.vCarFakeDrivePos			= <<153.4835, 223.6109, 105.7286>>

			scene.mExtraRespot.vCarPos		= <<280.2050, 152.9264, 103.1841>>
			scene.mExtraRespot.fCarRot		= 248.7600
			scene.mExtraRespot.expandDir	= 5

			scene.vSwitchPos[0]				= <<227.2966, 223.5158, 104.5494>>
			scene.fSwitchRot[0]				= 340.2000
			scene.vSwitchPos[1]				= <<196.4739, 139.1099, 100.2864>>
			scene.fSwitchRot[1]				= 159.8148
			scene.vSwitchPos[2]				= <<225.8337, 237.2527, 104.5132>>
			scene.fSwitchRot[2]				= 337.7378
			RETURN TRUE
		BREAK

	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FRIENDS, "Private_ALOC_GetDropoffScene(): No data for ", GetLabel_enumActivityLocation(eActivityLoc))
		SCRIPT_ASSERT("Private_ALOC_GetDropoffScene(): activity loc not supported")

		VECTOR vAnchor = GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
	
		scene.mFootPans[0].mStart.vPos	= vAnchor + <<-10.0, 0.0, 3.0>>
		scene.mFootPans[0].mStart.vRot	= <<0.0, 0.0, 0.0>>
		scene.mFootPans[0].mEnd.vPos	= vAnchor + <<-10.0, 0.0, 1.0>>
		scene.mFootPans[0].mEnd.vRot	= <<0.0, 0.0, 0.0>>
		scene.mFootPans[0].fFov			= 50.0
		
		scene.mFootPans[1].bEnabled		= FALSE
		scene.mFootPans[2].bEnabled		= FALSE
		scene.mFootPans[3].bEnabled		= FALSE
		
		scene.mFootCatchupCam.vPos			= vAnchor + <<-10.0, 0.0, 3.0>>
		scene.mFootCatchupCam.vRot			= <<0.0, 0.0, 0.0>>
		scene.fFootCatchupFov					= 50

		scene.vCarPos				= vAnchor
		scene.fCarRot				= 0.0
		scene.vPlayerPos			= vAnchor
		scene.vPedPos[0]			= vAnchor + <<0.6, 0.3, 0.0>>
		scene.vPedPos[1]			= vAnchor + <<0.6, -0.3, 0.0>>
		scene.vExitPos				= vAnchor + <<5.0, 0.0, 0.0>>

		scene.vSwitchPos[0]			= vAnchor
		scene.fSwitchRot[0]			= 0.0
		scene.vSwitchPos[1]			= vAnchor
		scene.fSwitchRot[1]			= 0.0
	#ENDIF

	RETURN FALSE

ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- ACTIVITY RESPOT DATA
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_ALOC_GetRespotData(enumActivityLocation eActivityLoc, structFRespotData& respot)

	// Get loc-specific positions
	SWITCH eActivityLoc

		CASE ALOC_bar_bahamas
			respot.vCarPos		= <<-1396.9028, -584.5709, 29.1500>>
			respot.fCarRot		= 298.9717
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_bar_baybar
			respot.vCarPos		= <<-255.8321, 6287.2563, 30.4584>>
			respot.fCarRot		= 127.8000
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_bar_biker
			respot.vCarPos		= <<972.5530, -117.5186, 73.3531>>
			respot.fCarRot		= 318.8789
			respot.expandDir	= RESPOT_EXPAND_X_RIGHT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_bar_downtown
			respot.vCarPos		= <<246.8546, -1007.3677, 28.2705>>
			respot.fCarRot		= 345.6000
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_FORWARD
			RETURN TRUE
		BREAK

		CASE ALOC_bar_himen
			respot.vCarPos		= <<506.4850, -1535.9747, 28.1243>>
			respot.fCarRot		= 143.9599
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_bar_mojitos
			respot.vCarPos		= <<-141.1919, 6386.0737, 30.5182>>
			respot.fCarRot		= 311.4000
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_FORWARD
			RETURN TRUE
		BREAK

		CASE ALOC_bar_singletons
			respot.vCarPos		= <<231.4387, 301.6899, 104.5082>>
			respot.fCarRot		= 266.2629
			respot.expandDir	= RESPOT_EXPAND_Y_FORWARD
			RETURN TRUE
		BREAK

		CASE ALOC_cinema_downtown
			respot.vCarPos		= <<401.4480, -706.8287, 28.2877>>
			respot.fCarRot		= 176.4000
			respot.expandDir	= RESPOT_EXPAND_X_LEFT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_cinema_morningwood
			respot.vCarPos		= <<-1404.6110, -203.9962, 45.6678>>//<<-1405.0282, -204.3950, 45.6301>>//<<-1404.9075, -204.5926, 45.6097>>
			respot.fCarRot		= 33.7834//31.2726//31.3120
			respot.expandDir	= RESPOT_EXPAND_X_RIGHT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_cinema_vinewood
			respot.vCarPos		= <<292.0633, 176.6080, 103.3739>>
			respot.fCarRot		= 74.5200
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_darts_hickBar
			respot.vCarPos		= <<1991.6521, 3075.7288, 46.0322>>
			respot.fCarRot		= 64.5087
			respot.expandDir	= RESPOT_EXPAND_X_RIGHT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

//		CASE ALOC_darts_rockClub
//			respot.vCarPos		= <<-564.3402, 268.2539, 81.8879>>
//			respot.fCarRot		= 83.7466
//			RETURN TRUE
//		BREAK

		CASE ALOC_golf_countryClub
			respot.vCarPos		= <<-1391.1478, 53.6687, 52.5963>>
			respot.fCarRot		= 309.1061
			respot.expandDir	= RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_beachCourt
			respot.vCarPos		= <<-1161.3501, -1593.9133, 3.3570>>
			respot.fCarRot		= 214.4588
			respot.expandDir	= RESPOT_EXPAND_X_RIGHT | RESPOT_EXPAND_Y_FORWARD
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_chumashHotel
			respot.vCarPos		= <<-2885.4353, -24.0344, 4.7220>>
			respot.fCarRot		= 72.8704
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_LSUCourt1
			respot.vCarPos		= <<-1655.6605, 292.1499, 59.3852>>
			respot.fCarRot		= 291.5096
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_michaelHouse
			respot.vCarPos		= <<-824.0479, 157.3689, 68.7834>>
			respot.fCarRot		= 263.9772
			respot.expandDir	= RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_richmanHotel1
			respot.vCarPos		= <<-1246.8259, 383.8191, 74.3899>>
			respot.fCarRot		= 108.8693
			respot.expandDir	= RESPOT_EXPAND_X_RIGHT | RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_vespucciHotel
			respot.vCarPos		= <<-939.0586, -1230.3401, 4.1746>>
			respot.fCarRot		= 300.1688
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_vinewoodhotel1
			respot.vCarPos		= <<510.0929, -241.3109, 47.5251>>
			respot.fCarRot		= 159.3072
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_tennis_weazelCourt1
			respot.vCarPos		= <<-1391.2991, -123.1333, 48.9688>>
			respot.fCarRot		= 4.0577
			respot.expandDir	= RESPOT_EXPAND_X_LEFT
			RETURN TRUE
		BREAK

		CASE ALOC_stripclub_southCentral
			respot.vCarPos		= <<136.4575, -1301.6420, 28.2124>>
			respot.fCarRot		= 60.6745
			respot.expandDir	= RESPOT_EXPAND_Y_BACK
			RETURN TRUE
		BREAK
		
		CASE ALOC_suspendFriends
			respot.vCarPos = <<0.0, 0.0, 0.0>>
			respot.fCarRot = 0.0
			respot.expandDir = RESPOT_EXPAND_X_LEFT
			SCRIPT_ASSERT("Private_ALOC_GetRespotData() - Trying to get respot data for ALOC_suspendFriends")
			RETURN FALSE
		BREAK

	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FRIENDS, "Private_ALOC_GetRespotData(): No data for ", GetLabel_enumActivityLocation(eActivityLoc))
		SCRIPT_ASSERT("Private_ALOC_GetRespotData(): No data for eActivityLoc")

		IF eActivityLoc < MAX_ACTIVITY_LOCATIONS
			respot.vCarPos = GET_STATIC_BLIP_POSITION(g_ActivityLocations[eActivityLoc].sprite)
			respot.fCarRot = 0.0
			respot.expandDir = RESPOT_EXPAND_X_LEFT
		ELSE
			respot.vCarPos = <<0.0, 0.0, 0.0>>
			respot.fCarRot = 0.0
			respot.expandDir = RESPOT_EXPAND_X_LEFT
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- ACTIVITY SCENE DATA
//---------------------------------------------------------------------------------------------------

FUNC BOOL Private_ALOC_GetActivityScene(enumActivityLocation eActivityLoc, structFActivityScene& scene)

	// Get loc-specific positions
	SWITCH eActivityLoc

		CASE ALOC_bar_bahamas
			scene.mCamPanA[0].vPos		= <<-1391.3512, -582.5974, 30.6630>>
			scene.mCamPanA[0].vRot		= <<0.5957, 0.0000, -144.2531>>
			scene.mCamPanA[1].vPos		= <<-1391.2810, -581.3097, 31.7334>>
			scene.mCamPanA[1].vRot		= <<39.9521, 0.0000, -167.3445>>
			scene.fCamPanAFov			= 39.9409
			scene.mCamTime.vPos			= <<-1391.2810, -581.3097, 31.7334>>
			scene.mCamTime.vRot			= <<39.9521, 0.0000, -167.3445>>
			scene.fCamTimeFov			= 39.9409
			scene.mCamPanB[0].vPos		= <<-1391.2810, -581.3097, 31.7334>>
			scene.mCamPanB[0].vRot		= <<39.9521, 0.0000, -167.3445>>
			scene.mCamPanB[1].vPos		= <<-1392.1105, -581.7819, 30.0333>>
			scene.mCamPanB[1].vRot		= <<6.5184, 0.0000, -146.8236>>
			scene.fCamPanBFov			= 39.9409
			scene.fPanADurationNormal	= 5.0000
			scene.fPanADurationDrunk	= 6.2500
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<-1391.6658, -583.7601, 29.2301>>
			scene.vPedPPos[1]			= <<-1388.3436, -587.0388, 29.2182>>
			scene.vPedPPos[2]			= <<-1390.4368, -583.8992, 29.2238>>
			scene.vPedAPos[0]			= <<-1392.9694, -585.0537, 29.2474>>
			scene.vPedAPos[1]			= <<-1389.9476, -586.8618, 29.2263>>
			scene.vPedAPos[2]			= <<-1391.2449, -585.6469, 29.2325>>
			scene.vPedBPos[0]			= <<-1388.3442, -583.0257, 29.2097>>
			scene.vPedBPos[1]			= <<-1387.8643, -586.0078, 29.2138>>
			scene.vPedBPos[2]			= <<-1388.8230, -584.4099, 29.2160>>
			scene.vClearA				= <<-1394.7904, -577.7897, 29.2828>>
			scene.vClearB				= <<-1388.2317, -587.2910, 38.4828>>
			scene.fClearW				= 7.4000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK
		
		CASE ALOC_bar_baybar
			scene.mCamPanA[0].vPos		= <<-259.9789, 6286.2612, 31.6806>>
			scene.mCamPanA[0].vRot		= <<1.7404, 0.0000, 32.0027>>
			scene.mCamPanA[1].vPos		= <<-260.7272, 6283.5938, 34.3499>>
			scene.mCamPanA[1].vRot		= <<12.5339, 0.0000, 13.1607>>
			scene.fCamPanAFov			= 39.9699
			scene.mCamTime.vPos			= <<-260.7272, 6283.5938, 34.3499>>
			scene.mCamTime.vRot			= <<12.5339, 0.0000, 13.1607>>
			scene.fCamTimeFov			= 39.9699
			scene.mCamPanB[0].vPos		= <<-257.0971, 6298.8657, 32.2329>>
			scene.mCamPanB[0].vRot		= <<5.4313, 0.0000, 156.4600>>
			scene.mCamPanB[1].vPos		= <<-255.0959, 6296.0127, 31.8561>>
			scene.mCamPanB[1].vRot		= <<3.6231, 0.0000, 140.3088>>
			scene.fCamPanBFov			= 49.9655
			scene.fPanADurationNormal	= 4.5000
			scene.fPanADurationDrunk	= 6.0000
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<-262.3289, 6286.1719, 30.4615>>
			scene.vPedPPos[1]			= <<-262.2624, 6291.0640, 30.4905>>
			scene.vPedPPos[2]			= <<-260.3908, 6289.6592, 30.4718>>
			scene.vPedAPos[0]			= <<-263.8451, 6286.9497, 30.4752>>
			scene.vPedAPos[1]			= <<-262.6830, 6290.6328, 30.4900>>
			scene.vPedAPos[2]			= <<-261.1694, 6288.4624, 30.4690>>
			scene.vPedBPos[0]			= <<-263.6732, 6285.2891, 30.4640>>
			scene.vPedBPos[1]			= <<-261.9075, 6292.0776, 30.4943>>
			scene.vPedBPos[2]			= <<-260.8339, 6291.5010, 30.4857>>
			scene.vClearA				= <<-264.4563, 6284.8921, 30.4666>>
			scene.vClearB				= <<-252.8467, 6295.8970, 39.4666>>
			scene.fClearW				= 13.2000

			scene.mCamCatch.vPos		= <<-258.315399,6289.812500,31.883162>>
			scene.mCamCatch.vRot		= <<2.278223,-0.211094,96.083275>>
			scene.fCamCatchFov			= 50.006855
			scene.bExtraCatchupCam		= TRUE
			RETURN TRUE
		BREAK

		CASE ALOC_bar_biker
			scene.mCamPanA[0].vPos		= <<988.7534, -105.3700, 75.0866>>
			scene.mCamPanA[0].vRot		= <<1.7315, 0.0000, 79.8844>>
			scene.mCamPanA[1].vPos		= <<990.175415,-104.452583,77.386162>>
			scene.mCamPanA[1].vRot		= <<15.288944,0.000000,99.281036>>
			scene.fCamPanAFov			= 39.2167
			scene.mCamTime.vPos			= <<990.175415,-104.452583,77.386162>>
			scene.mCamTime.vRot			= <<15.288944,0.000000,99.281036>>
			scene.fCamTimeFov			= 39.2167
			scene.mCamPanB[0].vPos		= <<990.175415,-104.452583,77.386162>>
			scene.mCamPanB[0].vRot		= <<15.288944,0.000000,99.281036>>
			scene.mCamPanB[1].vPos		= <<988.2397, -108.0459, 74.6292>>
			scene.mCamPanB[1].vRot		= <<0.8744, 0.0000, 65.9740>>
			scene.fCamPanBFov			= 39.2167
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 5.0
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<984.4031, -107.6140, 73.3531>>
			scene.vPedPPos[1]			= <<981.8692, -103.1675, 73.8535>>
			scene.vPedPPos[2]			= <<984.3040, -106.5439, 73.3531>>
			scene.vPedAPos[0]			= <<984.5980, -108.5928, 73.3531>>
			scene.vPedAPos[1]			= <<981.6747, -104.1510, 73.8535>>
			scene.vPedAPos[2]			= <<983.0098, -106.4819, 73.3531>>
			scene.vPedBPos[0]			= <<985.7677, -107.4711, 73.3531>>
			scene.vPedBPos[1]			= <<982.6229, -102.9315, 73.8535>>
			scene.vPedBPos[2]			= <<983.2742, -103.8405, 73.8535>>
			scene.vClearA				= <<988.8345, -110.7915, 73.0401>>
			scene.vClearB				= <<981.2740, -103.3357, 82.7901>>
			scene.fClearW				= 12.9000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK

		CASE ALOC_bar_downtown
			scene.mCamPanA[0].vPos		= <<250.2980, -1011.3746, 28.8803>>
			scene.mCamPanA[0].vRot		= <<9.4187, 0.0000, -112.0547>>
			scene.mCamPanA[1].vPos		= <<250.1848, -1011.1891, 28.9707>>
			scene.mCamPanA[1].vRot		= <<68.6477, 0.0000, -125.9116>>
			scene.fCamPanAFov			= 41.2174
			scene.mCamTime.vPos			= <<250.1848, -1011.1891, 28.9707>>
			scene.mCamTime.vRot			= <<68.6477, 0.0000, -125.9116>>
			scene.fCamTimeFov			= 41.2174
			scene.mCamPanB[0].vPos		= <<250.5783, -1019.1293, 29.3398>>
			scene.mCamPanB[0].vRot		= <<2.5216, 0.0000, -25.5830>>
			scene.mCamPanB[1].vPos		= <<250.4407, -1018.3758, 29.3662>>
			scene.mCamPanB[1].vRot		= <<2.5216, 0.0000, -16.6002>>
			scene.fCamPanBFov			= 50.0143
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 6.5
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<251.4444, -1010.5997, 28.2705>>
			scene.vPedPPos[1]			= <<255.0215, -1013.0981, 28.2704>>
			scene.vPedPPos[2]			= <<252.7474, -1012.3459, 28.2699>>
			scene.vPedAPos[0]			= <<250.1186, -1013.4296, 28.2671>>
			scene.vPedAPos[1]			= <<254.7028, -1013.8532, 28.2703>>
			scene.vPedAPos[2]			= <<251.1483, -1012.7039, 28.2685>>
			scene.vPedBPos[0]			= <<251.5051, -1009.0486, 28.2719>>
			scene.vPedBPos[1]			= <<255.8904, -1012.5917, 28.2714>>
			scene.vPedBPos[2]			= <<251.1808, -1010.1984, 28.2707>>
			scene.vClearA				= <<252.6843, -1007.8513, 28.2784>>
			scene.vClearB				= <<249.2258, -1018.6283, 32.5284>>
			scene.fClearW				= 8.8000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK
		
		CASE ALOC_bar_himen
			scene.mCamPanA[0].vPos		= <<499.1641, -1542.4603, 29.6950>>
			scene.mCamPanA[0].vRot		= <<-1.8745, 0.0000, 76.9106>>
			scene.mCamPanA[1].vPos		= <<501.6507, -1541.1014, 31.2484>>
			scene.mCamPanA[1].vRot		= <<16.1310, 0.0000, 87.0601>>
			scene.fCamPanAFov			= 37.9427
			scene.mCamTime.vPos			= <<501.6507, -1541.1014, 31.2484>>
			scene.mCamTime.vRot			= <<16.1310, 0.0000, 87.0601>>
			scene.fCamTimeFov			= 37.9427
			scene.mCamPanB[0].vPos		= <<501.6507, -1541.1014, 31.2484>>
			scene.mCamPanB[0].vRot		= <<16.1310, 0.0000, 87.0601>>
			scene.mCamPanB[1].vPos		= <<501.2673, -1539.4803, 29.6989>>
			scene.mCamPanB[1].vRot		= <<-3.6143, 0.0075, 113.3040>>
			scene.fCamPanBFov			= 37.9427
			scene.fPanADurationNormal	= 5.5
			scene.fPanADurationDrunk	= 6.75
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<499.1487, -1540.3121, 28.2512>>
			scene.vPedPPos[1]			= <<493.8625, -1541.5378, 28.2877>>
			scene.vPedPPos[2]			= <<496.6065, -1542.5469, 28.2572>>
			scene.vPedAPos[0]			= <<499.6836, -1538.1771, 28.2656>>
			scene.vPedAPos[1]			= <<495.8752, -1540.6436, 28.2805>>
			scene.vPedAPos[2]			= <<496.1659, -1541.4502, 28.2708>>
			scene.vPedBPos[0]			= <<499.7010, -1543.8729, 28.1129>>
			scene.vPedBPos[1]			= <<494.0248, -1542.4833, 28.2833>>
			scene.vPedBPos[2]			= <<495.5604, -1544.2053, 28.2538>>
			scene.vClearA				= <<492.2227, -1547.4340, 28.2585>>
			scene.vClearB				= <<500.1553, -1537.7700, 35.2585>>
			scene.fClearW				= 14.0000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK

		CASE ALOC_bar_mojitos
			scene.mCamPanA[0].vPos		= <<-138.1448, 6379.6206, 33.5900>>
			scene.mCamPanA[0].vRot		= <<-12.7782, 0.0000, -106.0116>>
			scene.mCamPanA[1].vPos		= <<-138.6336, 6380.5029, 34.4018>>
			scene.mCamPanA[1].vRot		= <<23.2267, 0.0000, -107.6306>>
			scene.fCamPanAFov			= 50.0000
			scene.mCamTime.vPos			= <<-138.6336, 6380.5029, 34.4018>>
			scene.mCamTime.vRot			= <<23.2267, 0.0000, -107.6306>>
			scene.fCamTimeFov			= 50.0000
			scene.mCamPanB[0].vPos		= <<-138.6336, 6380.5029, 34.4018>>
			scene.mCamPanB[0].vRot		= <<23.2267, 0.0000, -107.6306>>
			scene.mCamPanB[1].vPos		= <<-139.2361, 6381.0630, 31.9137>>
			scene.mCamPanB[1].vRot		= <<0.8374, 0.0000, -107.1374>>
			scene.fCamPanBFov			= 50.0000
			scene.fPanADurationNormal	= 5.2500
			scene.fPanADurationDrunk	= 6.5000
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<-136.3300, 6380.1670, 30.5513>>
			scene.vPedPPos[1]			= <<-132.3319, 6376.8076, 31.1800>>
			scene.vPedPPos[2]			= <<-135.7501, 6379.6777, 30.6291>>
			scene.vPedAPos[0]			= <<-137.1064, 6379.5210, 30.5464>>
			scene.vPedAPos[1]			= <<-133.3788, 6376.0688, 31.1800>>
			scene.vPedAPos[2]			= <<-135.8515, 6378.6450, 30.6931>>
			scene.vPedBPos[0]			= <<-136.2626, 6382.0498, 30.5032>>
			scene.vPedBPos[1]			= <<-132.1531, 6377.4521, 31.1800>>
			scene.vPedBPos[2]			= <<-133.3998, 6379.0327, 30.8417>>
			scene.vClearA				= <<-141.2375, 6383.4414, 30.5265>>
			scene.vClearB				= <<-133.2909, 6375.6445, 36.5765>>
			scene.fClearW				= 8.1000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK

		CASE ALOC_bar_singletons
			scene.mCamPanA[0].vPos		= <<228.6531, 306.2192, 106.3708>>
			scene.mCamPanA[0].vRot		= <<-5.0435, 0.2052, 109.1608>>
			scene.mCamPanA[1].vPos		= <<229.2501, 305.8874, 107.8613>>
			scene.mCamPanA[1].vRot		= <<22.4767, 0.2052, 118.4014>>
			scene.fCamPanAFov			= 49.9886
			scene.mCamTime.vPos			= <<229.2501, 305.8874, 107.8613>>
			scene.mCamTime.vRot			= <<22.4767, 0.2052, 118.4014>>
			scene.fCamTimeFov			= 49.9886
			scene.mCamPanB[0].vPos		= <<229.2501, 305.8874, 107.8613>>
			scene.mCamPanB[0].vRot		= <<22.4767, 0.2052, 118.4014>>
			scene.mCamPanB[1].vPos		= <<228.8797, 305.0008, 106.0803>>
			scene.mCamPanB[1].vRot		= <<-7.3693, 0.2052, 137.6210>>
			scene.fCamPanBFov			= 49.9886
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 6.0
			scene.bIsCinema				= FALSE
			scene.vPedPPos[0]			= <<227.0380, 302.3240, 104.5340>>
			scene.vPedPPos[1]			= <<221.2218, 304.1971, 104.5653>>
			scene.vPedPPos[2]			= <<227.0380, 302.3240, 104.5340>>
			scene.vPedAPos[0]			= <<228.5144, 302.3003, 104.3811>>
			scene.vPedAPos[1]			= <<219.9782, 305.6431, 104.5847>>
			scene.vPedAPos[2]			= <<224.1672, 302.8337, 104.5366>>
			scene.vPedBPos[0]			= <<227.6132, 301.1576, 104.5328>>
			scene.vPedBPos[1]			= <<220.1857, 303.6531, 104.5743>>
			scene.vPedBPos[2]			= <<222.3438, 302.9540, 104.5386>>
			scene.vClearA				= <<230.5813, 304.3873, 104.4136>>
			scene.vClearB				= <<220.4306, 307.9557, 112.5136>>
			scene.fClearW				= 12.5000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK
		
		CASE ALOC_cinema_downtown
			scene.mCamPanA[0].vPos		= <<405.9127, -711.7676, 31.2618>>
			scene.mCamPanA[0].vRot		= <<19.9190, 0.0000, 90.3396>>
			scene.mCamPanA[1].vPos		= <<405.4205, -711.7840, 29.3979>>
			scene.mCamPanA[1].vRot		= <<5.5077, 0.0000, 99.6259>>
			scene.fCamPanAFov			= 50.0000
			scene.mCamTime.vPos			= <<405.4205, -711.7840, 29.3979>>
			scene.mCamTime.vRot			= <<5.5077, 0.0000, 99.6259>>
			scene.fCamTimeFov			= 50.0000
			scene.mCamPanB[0].vPos		= <<405.4205, -711.7840, 29.3979>>
			scene.mCamPanB[0].vRot		= <<5.5077, 0.0000, 99.6259>>
			scene.mCamPanB[1].vPos		= <<405.4205, -711.7840, 29.3979>>
			scene.mCamPanB[1].vRot		= <<5.5077, 0.0000, 99.6259>>
			scene.fCamPanBFov			= 50.0000
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 5.0
			scene.bIsCinema				= TRUE
			scene.vPedPPos[0]			= <<393.1420, -713.4531, 28.2855>>
			scene.vPedPPos[1]			= <<397.1506, -714.0617, 28.2857>>
			scene.vPedPPos[2]			= <<399.4089, -714.5928, 28.2858>>
			scene.vPedAPos[0]			= <<394.7705, -712.7902, 28.2853>>
			scene.vPedAPos[1]			= <<397.9654, -713.1153, 28.2854>>
			scene.vPedAPos[2]			= <<398.8918, -713.4561, 28.2855>>
			scene.vPedBPos[0]			= <<395.3366, -714.9562, 28.2859>>
			scene.vPedBPos[1]			= <<397.7031, -715.1816, 28.2860>>
			scene.vPedBPos[2]			= <<398.9623, -715.4068, 28.2860>>
			scene.vClearA				= <<392.7992, -712.5184, 28.2852>>
			scene.vClearB				= <<411.5248, -712.1509, 32.5352>>
			scene.fClearW				= 9.2000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK

		CASE ALOC_cinema_morningwood
			scene.mCamPanA[0].vPos		= <<-1405.7139, -195.7280, 54.0098>>
			scene.mCamPanA[0].vRot		= <<-4.2783, 0.0000, 121.8633>>
			scene.mCamPanA[1].vPos		= <<-1407.5447, -194.7853, 47.9021>>
			scene.mCamPanA[1].vRot		= <<4.3037, 0.0000, 134.3144>>
			scene.fCamPanAFov			= 50.0000
			scene.mCamTime.vPos			= <<-1407.5447, -194.7853, 47.9021>>
			scene.mCamTime.vRot			= <<4.3037, 0.0000, 134.3144>>
			scene.fCamTimeFov			= 50.0000
			scene.mCamPanB[0].vPos		= <<989.1804, -103.8445, 78.8565>>
			scene.mCamPanB[0].vRot		= <<2.7704, 0.0000, 103.6486>>
			scene.mCamPanB[1].vPos		= <<988.2397, -108.0459, 74.6292>>
			scene.mCamPanB[1].vRot		= <<0.8744, 0.0000, 65.9740>>
			scene.fCamPanBFov			= 39.2167
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 5.0
			scene.bIsCinema				= TRUE
			scene.vPedPPos[0]			= <<-1417.2899, -203.6409, 45.5004>>
			scene.vPedPPos[1]			= <<-1412.4402, -199.2623, 46.2752>>
			scene.vPedPPos[2]			= <<-1403.6200, -162.3560, 46.5711>>
			scene.vPedAPos[0]			= <<-1416.8081, -201.9135, 46.1048>>
			scene.vPedAPos[1]			= <<-1412.8599, -197.2908, 46.2686>>
			scene.vPedAPos[2]			= <<-1404.7800, -163.0730, 46.5422>>
			scene.vPedBPos[0]			= <<-1419.2917, -202.7134, 45.5004>>
			scene.vPedBPos[1]			= <<-1414.3977, -198.1992, 46.2804>>
			scene.vPedBPos[2]			= <<-1411.0284, -198.9657, 46.2444>>
			scene.vClearA				= <<-1426.3295, -215.1618, 45.5004>>
			scene.vClearB				= <<-1404.3307, -192.3439, 51.2004>>
			scene.fClearW				= 9.6000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK
		
		CASE ALOC_cinema_vinewood
			scene.mCamPanA[0].vPos		= <<293.4810, 195.6959, 105.3287>>
			scene.mCamPanA[0].vRot		= <<28.8303, 0.0000, -42.0262>>
			scene.mCamPanA[1].vPos		= <<293.4810, 195.6959, 105.3287>>
			scene.mCamPanA[1].vRot		= <<7.0245, 0.0000, -60.5823>>
			scene.fCamPanAFov			= 50.0000
			scene.mCamTime.vPos			= <<320.8777, 167.7101, 104.4684>>
			scene.mCamTime.vRot			= <<6.1110, 0.0000, -66.0239>>
			scene.fCamTimeFov			= 50.0000
			scene.mCamPanB[0].vPos		= <<323.4883, 156.7916, 103.6546>>
			scene.mCamPanB[0].vRot		= <<5.5771, 0.0000, -38.8920>>
			scene.mCamPanB[1].vPos		= <<323.4883, 156.7916, 103.6546>>
			scene.mCamPanB[1].vRot		= <<5.5771, 0.0000, -38.8920>>
			scene.fCamPanBFov			= 50.0000
			scene.fPanADurationNormal	= 5.0
			scene.fPanADurationDrunk	= 5.0
			scene.bIsCinema				= TRUE
			scene.vPedPPos[0]			= <<300.1136, 202.9304, 103.3684>>
			scene.vPedPPos[1]			= <<298.3760, 199.1103, 103.3446>>
			scene.vPedPPos[2]			= <<337.5520, 166.9108, 102.3808>>
			scene.vPedAPos[0]			= <<301.1823, 202.7771, 103.3837>>
			scene.vPedAPos[1]			= <<299.3044, 198.6458, 103.3357>>
			scene.vPedAPos[2]			= <<238.3950, 299.4760, 104.5890>>
			scene.vPedBPos[0]			= <<299.5274, 203.6378, 103.3739>>
			scene.vPedBPos[1]			= <<297.0100, 198.7480, 103.3481>>
			scene.vPedBPos[2]			= <<238.3940, 299.4760, 104.5890>>
			scene.vClearA				= <<336.7123, 180.7974, 102.0090>>
			scene.vClearB				= <<313.8644, 161.0981, 106.6090>>
			scene.fClearW				= 10.7000
			scene.bExtraCatchupCam		= FALSE
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FRIENDS, "Private_ALOC_GetActivityScene(): No data for ", GetLabel_enumActivityLocation(eActivityLoc))
		SCRIPT_ASSERT("Private_ALOC_GetActivityScene(): No data for eActivityLoc")
	#ENDIF

	RETURN FALSE

ENDFUNC


//---------------------------------------------------------------------------------------------------
//-- DEBUG WARP POSITIONS
//---------------------------------------------------------------------------------------------------

#IF IS_DEBUG_BUILD

	FUNC VECTOR DEBUG_GetFriendLocJumpOffset(enumFriendLocation eDropoffLoc)
		CONST_FLOAT	fSpawnRadius	15.0//20.0
		FLOAT		fJumpAngle		= 0.0
		
		SWITCH eDropoffLoc
			CASE FLOC_michael_RH
				fJumpAngle = 225.0
				RETURN <<COS(fJumpAngle), SIN(fJumpAngle), 0.0>>*fSpawnRadius		//RETURN <<15,15,0>>
			BREAK
			CASE FLOC_franklin_SC
				fJumpAngle = 210.0
				RETURN <<COS(fJumpAngle), SIN(fJumpAngle), 0.0>>*fSpawnRadius		//RETURN <<-15,-7.5,0>>
			BREAK
			CASE FLOC_trevor_CS
				fJumpAngle = 345.0
				RETURN <<COS(fJumpAngle), SIN(fJumpAngle), 0.0>>*fSpawnRadius		//RETURN <<15,15,0>>
			BREAK
			
			CASE FLOC_lamar_SC
				fJumpAngle = 350.0
				RETURN <<COS(fJumpAngle), SIN(fJumpAngle), 0.0>>*fSpawnRadius		//RETURN <<-15,-7.5,0>>
			BREAK
			
			CASE FLOC_franklin_VH
				RETURN <<19.0268, 560.8762, 176.7073>> - g_FriendLocations[FLOC_franklin_VH].vPickupCoord
			BREAK
			
//			CASE FLOC_gasStation_DT
//				RETURN <<-539.5134, -1200.3910, 17.0048>> - g_FriendLocations[FLOC_gasStation_DT].vPickupCoord
//			BREAK

			DEFAULT
				VECTOR vOffset
				vOffset = NORMALISE_VECTOR(g_FriendLocations[eDropoffLoc].vPedOffsetA) * -15.0
				vOffset.z = 0.0
				RETURN vOffset
				
////				IF g_FriendLocations[eDropoffLoc].iSpawnPosCount > 0
////					INT iSpawnIndex
////					iSpawnIndex = g_FriendLocations[eDropoffLoc].iSpawnPosCount
////					RETURN g_FriendLocations[eDropoffLoc].vSpawnPosPoints[GET_RANDOM_INT_IN_RANGE(0, iSpawnIndex)] - g_FriendLocations[eDropoffLoc].vPickupCoord
////				ELSE
//					fJumpAngle = GET_RANDOM_FLOAT_IN_RANGE(0.0, 360.0)
//					RETURN <<COS(fJumpAngle), SIN(fJumpAngle), 0.0>>*fSpawnRadius		//RETURN <<-15,-7.5,0>>
////				ENDIF
			BREAK
		ENDSWITCH
		
		SCRIPT_ASSERT("DEBUG_GetFriendJumpOffset - invalid eFriend")
		RETURN <<0,0,0>>
	ENDFUNC

	FUNC VECTOR DEBUG_GetActivityLocJumpOffset(enumActivityLocation eJumpLocation)

		VECTOR vAnchor = GET_STATIC_BLIP_POSITION(g_ActivityLocations[eJumpLocation].sprite)
		
		SWITCH eJumpLocation
			CASE ALOC_golf_countryClub			RETURN <<-1370.7950, 49.2175, 52.7057>>		- vAnchor	BREAK
			CASE ALOC_tennis_beachCourt			RETURN <<-1162.9464, -1598.2458, 3.3259>>	- vAnchor 	BREAK
			CASE ALOC_tennis_michaelHouse		RETURN <<-787.7080, 151.7993, 67.6744>>		- vAnchor 	BREAK
			CASE ALOC_stripclub_southCentral	RETURN <<133.8653, -1306.9590, 28.0722>>	- vAnchor 	BREAK
//			CASE ALOC_darts_rockClub			RETURN  <<-548.7711, 272.4878, 81.9662>>	- vAnchor 	BREAK
			CASE ALOC_darts_hickBar				RETURN <<2008.2849, 3058.3877, 46.0502>>	- vAnchor 	BREAK
			CASE ALOC_cinema_vinewood			RETURN <<329.5648, 170.5077, 102.5544>>		- vAnchor 	BREAK
			CASE ALOC_cinema_downtown			RETURN <<398.1588, -710.9405, 28.2847>>		- vAnchor 	BREAK
			CASE ALOC_cinema_morningwood		RETURN <<-1412.8745, -200.0044, 46.2527>>	- vAnchor 	BREAK
			CASE ALOC_bar_bahamas				RETURN <<-1393.7988, -586.1046, 29.2600>>	- vAnchor 	BREAK
			CASE ALOC_bar_baybar				RETURN <<-264.3293, 6284.1465, 30.4614>>	- vAnchor 	BREAK
			CASE ALOC_bar_biker					RETURN <<971.0001, -132.2628, 73.2911>>		- vAnchor 	BREAK
			CASE ALOC_bar_downtown				RETURN <<252.9373, -1003.6371, 28.2687>>	- vAnchor 	BREAK
			CASE ALOC_bar_himen					RETURN <<502.5184, -1536.1772, 28.2164>>	- vAnchor 	BREAK
			CASE ALOC_bar_mojitos				RETURN <<-148.1390, 6376.0708, 30.5287>>	- vAnchor 	BREAK
			CASE ALOC_bar_singletons			RETURN <<225.0749, 296.9051, 104.5345>>		- vAnchor 	BREAK
//			CASE ALOC_bar_yellowjack			RETURN <<2008.2849, 3058.3877, 46.0502>>	- vAnchor 	BREAK
		ENDSWITCH
		
		IF g_ActivityLocations[eJumpLocation].type = ATYPE_bar
			VECTOR vSpawn = vAnchor + << 10.0, 0.0, 0.0 >>
			
			IF NOT GET_SAFE_COORD_FOR_PED(vSpawn, TRUE, vSpawn, GSC_FLAG_NOT_INTERIOR|GSC_FLAG_NOT_ISOLATED|GSC_FLAG_NOT_WATER)
				GET_SAFE_COORD_FOR_PED(vSpawn, FALSE, vSpawn, GSC_FLAG_NOT_INTERIOR|GSC_FLAG_NOT_ISOLATED|GSC_FLAG_NOT_WATER)
			ENDIF
			
			RETURN vSpawn - vAnchor
		ENDIF

//		RETURN NORMALISE_VECTOR(g_ActivityLocations[eJumpLocation].vResetVehicleOffset) * 15.0
		RETURN <<0.0, 0.0, 0.0>>
	ENDFUNC

#ENDIF


