USING "rage_builtins.sch"
USING "globals.sch"

USING "selector_public.sch"
USING "player_ped_scenes.sch"
USING "flow_mission_trigger_scenes.sch"
USING "commands_graphics.sch"

USING "mission_control_public.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Mission_triggerer core scripts.
//		AUTHOR			:	Ak / BenR
//		DESCRIPTION		:	Parses the g_TriggerableMissions list and checks for activated
//							triggers, on finding an active trigger an attempt will be made to
//							obtain mission candidate permissions for the trigger, mission 
//							script launching should be handled by the relevant scripts.
//
//							Core scripts split into a header so we can have multiple
//							instances of the mission triggerer thread with different data
//							compiled into each. Keeps byte code memory useage down.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
   
// ===========================================================================================================
//		Debug
// ===========================================================================================================

#IF IS_DEBUG_BUILD

	INT iDebugDisplayTriggerModeTextUntil
	STRING sDebugCurrentTriggerModeMessage


	PROC Debug_Configure_On_Screen_Text()
		SET_TEXT_SCALE (0.48, 0.44)
		SET_TEXT_WRAP(0.0, 1.0)
		SET_TEXT_DROPSHADOW (0,0,0,0,255)
		SET_TEXT_COLOUR(0,200,0,180)
		SET_TEXT_EDGE (0,0,0,0,255)     
		SET_TEXT_PROPORTIONAL (FALSE)                                                 
		SET_TEXT_FONT(FONT_STANDARD)
	ENDPROC


	PROC Debug_Display_Trigger_Mode_Screen_Text()
		SWITCH g_eTriggerDebugMode
			CASE TDM_OFF
				sDebugCurrentTriggerModeMessage = "Off"
			BREAK
			CASE TDM_TRIGGER
				sDebugCurrentTriggerModeMessage = "Trigger Zone"
			BREAK
			CASE TDM_TRIGGER_TEXT
				sDebugCurrentTriggerModeMessage = "Trigger Zone + Labels"
			BREAK
			CASE TDM_LOAD
				sDebugCurrentTriggerModeMessage = "Load/Unload Zones"
			BREAK
			CASE TDM_LOAD_TEXT
				sDebugCurrentTriggerModeMessage = "Load/Unload Zones + Labels"
			BREAK
			CASE TDM_FRIEND_REJECT
				sDebugCurrentTriggerModeMessage = "Friend Reject Zone"
			BREAK
			CASE TDM_FRIEND_REJECT_TEXT
				sDebugCurrentTriggerModeMessage = "Friend Reject Zone + Labels"
			BREAK
			CASE TDM_BBUDDY_CALL
				sDebugCurrentTriggerModeMessage = "Battle Buddy Call Zone"
			BREAK
			CASE TDM_BBUDDY_CALL_TEXT
				sDebugCurrentTriggerModeMessage = "Battle Buddy Call Zone + Labels"
			BREAK
			CASE TDM_AUTODOOR
				sDebugCurrentTriggerModeMessage = "Autodoor Zones"
			BREAK
			CASE TDM_AUTODOOR_TEXT
				sDebugCurrentTriggerModeMessage = "Autodoor Zones + Labels"
			BREAK
			CASE TDM_ALL
				sDebugCurrentTriggerModeMessage = "All"
			BREAK
			CASE TDM_ALL_TEXT
				sDebugCurrentTriggerModeMessage = "All + Labels"
			BREAK
		ENDSWITCH
		iDebugDisplayTriggerModeTextUntil = GET_GAME_TIMER() + 4000
	ENDPROC


	PROC Update_Trigger_Mode_Screen_Text()
		IF GET_GAME_TIMER() < iDebugDisplayTriggerModeTextUntil
			Debug_Configure_On_Screen_Text()
			FLOAT fHalfStringWidth = GET_STRING_WIDTH_WITH_STRING("STRING", sDebugCurrentTriggerModeMessage) * 0.5
			DISPLAY_TEXT_WITH_LITERAL_STRING(	0.5 - fHalfStringWidth, 
												0.06, 
												"STRING", 
												sDebugCurrentTriggerModeMessage)
		ENDIF
	ENDPROC

#ENDIF


// ===========================================================================================================
//		Cleanup
// ===========================================================================================================


PROC MISSION_TRIGGERER_INIT()
	CPRINTLN(DEBUG_TRIGGER, "Resetting trigger scene state flags as new mission triggerer comes online.")
	INT iTriggerIndex
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		RESET_TRIGGER_SCENE_STATE(g_TriggerableMissionsTU[iTriggerIndex].sScene)
	ENDREPEAT
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		RESET_TRIGGER_SCENE_STATE(g_TriggerableMissionsTU[iTriggerIndex].sScene)
	ENDREPEAT
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
		RESET_TRIGGER_SCENE_STATE(g_TriggerableMissions[iTriggerIndex].sScene)
	ENDREPEAT
#ENDIF
#ENDIF
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
ENDPROC


PROC MISSION_TRIGGERER_TERMINATE()
	CPRINTLN(DEBUG_TRIGGER, "Mission triggerer terminating.")
	IF g_bPlayerLockedInToTrigger
		CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
	ENDIF
	#IF IS_DEBUG_BUILD
		g_eTriggerDebugMode = TDM_OFF
	#ENDIF
	
	SET_THIS_IS_A_TRIGGER_SCRIPT(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

#IF USE_CLF_DLC
PROC CLEANUP_ACTIVE_TRIGGER_SCENE_CLF(MISSION_TRIGGERER_LOCAL_VARS_TU &sLocalsVars)
	CPRINTLN(DEBUG_TRIGGER, "Cleaning up any active trigger scenes AGT.")
	
	//Run a full cleanup if the mission is still registered with a trigger.
	INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToCleanup)
	IF iMissionTriggerIndex != -1
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup CLF for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_MISSION_TRIGGERS_TU iMissionTriggerIndex
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup CLF for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
		//Cleanup any active TOD cutscenes.
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
		OR IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			SET_TODS_CUTSCENE_RUNNING(sLocalsVars.sTimeOfDay, FALSE)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER,"Timelapse cutscene cleaned up CLF for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.eMission), ".")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	//Set all the trigger scene assets to null values to be safe.
	INT index
	REPEAT MAX_TRIG_SCENE_VEHICLES index
		g_sTriggerSceneAssets.veh[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_PEDS index
		g_sTriggerSceneAssets.ped[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_OBJECTS index
		g_sTriggerSceneAssets.object[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_ROPES index
		g_sTriggerSceneAssets.rope[index] = NULL
	ENDREPEAT
	structPedsForConversation sBlankPedsForConversation
	g_sTriggerSceneAssets.conversation = sBlankPedsForConversation
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	//Unflag the system.
	g_bTriggerSceneActive = FALSE
	g_bMissionTriggerLoading = FALSE
	g_eMissionSceneToCleanup = SP_MISSION_NONE
	g_bCleanupTriggerScene = FALSE
	
	//Run any bound local release function as an extra safeguard to ensure we get assets out of memory.
	IF sLocalsVars.bReleaseFunctionBound
		CPRINTLN(DEBUG_TRIGGER, "Local release function pointer is bound so running it and unbinding.")
		CALL sLocalsVars.fpReleaseAssetsForLoadedScene()
		sLocalsVars.bReleaseFunctionBound = FALSE
	ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC CLEANUP_ACTIVE_TRIGGER_SCENE_NRM(MISSION_TRIGGERER_LOCAL_VARS_TU &sLocalsVars)
	CPRINTLN(DEBUG_TRIGGER, "Cleaning up any active trigger scenes NRM.")
	
	//Run a full cleanup if the mission is still registered with a trigger.
	INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToCleanup)
	IF iMissionTriggerIndex != -1
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup NRM for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_MISSION_TRIGGERS_TU iMissionTriggerIndex
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup NRM for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
		//Cleanup any active TOD cutscenes.
		IF IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
		OR IS_BIT_SET(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			SET_TODS_CUTSCENE_RUNNING(sLocalsVars.sTimeOfDay, FALSE)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER,"Timelapse cutscene cleaned up NRM for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.eMission), ".")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	//Set all the trigger scene assets to null values to be safe.
	INT index
	REPEAT MAX_TRIG_SCENE_VEHICLES index
		g_sTriggerSceneAssets.veh[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_PEDS index
		g_sTriggerSceneAssets.ped[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_OBJECTS index
		g_sTriggerSceneAssets.object[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_ROPES index
		g_sTriggerSceneAssets.rope[index] = NULL
	ENDREPEAT
	structPedsForConversation sBlankPedsForConversation
	g_sTriggerSceneAssets.conversation = sBlankPedsForConversation
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	//Unflag the system.
	g_bTriggerSceneActive = FALSE
	g_bMissionTriggerLoading = FALSE
	g_eMissionSceneToCleanup = SP_MISSION_NONE
	g_bCleanupTriggerScene = FALSE
	
	//Run any bound local release function as an extra safeguard to ensure we get assets out of memory.
	IF sLocalsVars.bReleaseFunctionBound
		CPRINTLN(DEBUG_TRIGGER, "Local release function pointer is bound so running it and unbinding.")
		CALL sLocalsVars.fpReleaseAssetsForLoadedScene()
		sLocalsVars.bReleaseFunctionBound = FALSE
	ENDIF
ENDPROC
#ENDIF


PROC CLEANUP_ACTIVE_TRIGGER_SCENE(MISSION_TRIGGERER_LOCAL_VARS &sLocalsVars)
#if USE_CLF_DLC
	CLEANUP_ACTIVE_TRIGGER_SCENE_CLF(sLocalsVars)
#endif
#if USE_NRM_DLC
	CLEANUP_ACTIVE_TRIGGER_SCENE_NRM(sLocalsVars)
#endif
#if not USE_SP_DLC
	CPRINTLN(DEBUG_TRIGGER, "Cleaning up any active trigger scenes.")
	
	//Run a full cleanup if the mission is still registered with a trigger.
	INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToCleanup)
	IF iMissionTriggerIndex != -1
		IF IS_BIT_SET(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
	ENDIF
	
	REPEAT MAX_MISSION_TRIGGERS iMissionTriggerIndex
		IF IS_BIT_SET(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		AND sLocalsVars.sScenePointers[iMissionTriggerIndex].bSceneBound
			UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[iMissionTriggerIndex].sScene, sLocalsVars.sScenePointers[iMissionTriggerIndex], sLocalsVars.bReleaseFunctionBound)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER, "Ran full scene cleanup for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iMissionTriggerIndex].eMissionID), ".")
			#ENDIF
		ENDIF
		//Cleanup any active TOD cutscenes.
		IF IS_BIT_SET(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
		OR IS_BIT_SET(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
			CLEAR_BIT(g_TriggerableMissions[iMissionTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
			SET_TODS_CUTSCENE_RUNNING(sLocalsVars.sTimeOfDay, FALSE)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_TRIGGER,"Timelapse cutscene cleaned up for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iMissionTriggerIndex].sScene.eMission), ".")
			#ENDIF
		ENDIF
	ENDREPEAT
	
	//Set all the trigger scene assets to null values to be safe.
	INT index
	REPEAT MAX_TRIG_SCENE_VEHICLES index
		g_sTriggerSceneAssets.veh[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_PEDS index
		g_sTriggerSceneAssets.ped[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_OBJECTS index
		g_sTriggerSceneAssets.object[index] = NULL
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_ROPES index
		g_sTriggerSceneAssets.rope[index] = NULL
	ENDREPEAT
	structPedsForConversation sBlankPedsForConversation
	g_sTriggerSceneAssets.conversation = sBlankPedsForConversation
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	//Unflag the system.
	g_bTriggerSceneActive = FALSE
	g_bMissionTriggerLoading = FALSE
	g_eMissionSceneToCleanup = SP_MISSION_NONE
	g_bCleanupTriggerScene = FALSE
	
	//Run any bound local release function as an extra safeguard to ensure we get assets out of memory.
	IF sLocalsVars.bReleaseFunctionBound
		CPRINTLN(DEBUG_TRIGGER, "Local release function pointer is bound so running it and unbinding.")
		CALL sLocalsVars.fpReleaseAssetsForLoadedScene()
		sLocalsVars.bReleaseFunctionBound = FALSE
	ENDIF
#endif	
ENDPROC
#IF USE_CLF_DLC
PROC MISSION_TRIGGERER_FULL_CLEANUP_CLF(MISSION_TRIGGERER_LOCAL_VARS_TU &sLocalsVars)
	CLEANUP_ACTIVE_TRIGGER_SCENE_CLF(sLocalsVars)

	//Look for trigger scene that need cleaning up before we terminate.
	INT index 
	REPEAT g_iRegisteredMissionTriggers index
		sLocalsVars.sScenePointers[index].bSceneBound = FALSE
		CLEAR_BIT(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_BIT_SET(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
			SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[index].eBlip, FALSE)
			CLEAR_BIT(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
		ENDIF
	ENDREPEAT
	
	//Make sure any timelapse blips are cleaned up.
	IF DOES_BLIP_EXIST(sLocalsVars.blipTODTrigger)
		REMOVE_BLIP(sLocalsVars.blipTODTrigger)
	ENDIF
	
	MISSION_TRIGGERER_TERMINATE()
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC MISSION_TRIGGERER_FULL_CLEANUP_NRM(MISSION_TRIGGERER_LOCAL_VARS_TU &sLocalsVars)
	CLEANUP_ACTIVE_TRIGGER_SCENE_NRM(sLocalsVars)

	//Look for trigger scene that need cleaning up before we terminate.
	INT index 
	REPEAT g_iRegisteredMissionTriggers index
		sLocalsVars.sScenePointers[index].bSceneBound = FALSE
		CLEAR_BIT(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_BIT_SET(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
			SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[index].eBlip, FALSE)
			CLEAR_BIT(g_TriggerableMissionsTU[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
		ENDIF
	ENDREPEAT
	
	//Make sure any timelapse blips are cleaned up.
	IF DOES_BLIP_EXIST(sLocalsVars.blipTODTrigger)
		REMOVE_BLIP(sLocalsVars.blipTODTrigger)
	ENDIF
	
	MISSION_TRIGGERER_TERMINATE()
ENDPROC
#ENDIF

PROC MISSION_TRIGGERER_FULL_CLEANUP(MISSION_TRIGGERER_LOCAL_VARS &sLocalsVars)
#if USE_CLF_DLC
	MISSION_TRIGGERER_FULL_CLEANUP_CLF(sLocalsVars)
#endif
#if USE_NRM_DLC
	MISSION_TRIGGERER_FULL_CLEANUP_NRM(sLocalsVars)
#endif

#if not USE_SP_DLC
	CLEANUP_ACTIVE_TRIGGER_SCENE(sLocalsVars)

	//Look for trigger scene that need cleaning up before we terminate.
	INT index 
	REPEAT g_iRegisteredMissionTriggers index
		sLocalsVars.sScenePointers[index].bSceneBound = FALSE
		CLEAR_BIT(g_TriggerableMissions[index].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_BIT_SET(g_TriggerableMissions[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
			SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissions[index].eBlip, FALSE)
			CLEAR_BIT(g_TriggerableMissions[index].sScene.iStateBitset, TSS_BLIP_ACTIVE)
		ENDIF
	ENDREPEAT
	
	//Make sure any timelapse blips are cleaned up.
	IF DOES_BLIP_EXIST(sLocalsVars.blipTODTrigger)
		REMOVE_BLIP(sLocalsVars.blipTODTrigger)
	ENDIF
	
	MISSION_TRIGGERER_TERMINATE()
#endif	
ENDPROC


// ===========================================================================================================
//		Utility
// ===========================================================================================================

#if not USE_SP_DLC
PROC DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(SP_MISSIONS paramMissionID, VECTOR paramTriggerPos, BOOL &paramDisplayedHelp)
	//Check if this character can actually trigger this. If not display help.
	IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramMissionID].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
									
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF VDIST2(paramTriggerPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 64
				
					//Don't allow switching while standing on an invalid mission trigger.
					//Helps guard against juggling two characters into invalid mission locations.
					DISABLE_SELECTOR_THIS_FRAME()
				
					IF NOT paramDisplayedHelp
						STRING strHelp
						BOOL bMichael = IS_BIT_SET(g_sMissionStaticData[paramMissionID].triggerCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
						BOOL bFranklin = IS_BIT_SET(g_sMissionStaticData[paramMissionID].triggerCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
						BOOL bTrevor = IS_BIT_SET(g_sMissionStaticData[paramMissionID].triggerCharBitset, ENUM_TO_INT(CHAR_TREVOR))
						
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								IF bFranklin
									IF bTrevor
										strHelp = "TRIG_FT"
									ELSE
										strHelp = "TRIG_F"
									ENDIF
								ELIF bTrevor
									strHelp = "TRIG_T"
								ENDIF
							BREAK
							
							CASE CHAR_FRANKLIN
								IF bMichael
									IF bTrevor
										strHelp = "TRIG_MT"
									ELSE
										strHelp = "TRIG_M"
									ENDIF
								ELIF bTrevor
									strHelp = "TRIG_T"
								ENDIF
							BREAK
							
							CASE CHAR_TREVOR
								IF bMichael
									IF bFranklin
										strHelp = "TRIG_MF"
									ELSE
										strHelp = "TRIG_M"
									ENDIF
								ELIF bFranklin
									strHelp = "TRIG_F"
								ENDIF
							BREAK
						ENDSWITCH
						
						IF NOT IS_STRING_NULL_OR_EMPTY(strHelp)
							SWITCH GET_FLOW_HELP_MESSAGE_STATUS(strHelp)
								CASE FHS_EXPIRED
									ADD_HELP_TO_FLOW_QUEUE(strHelp, FHP_VERY_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, GET_CURRENT_PLAYER_PED_BIT())
									CPRINTLN(DEBUG_FLOW_HELP, "Mission: ", GET_MISSION_DISPLAY_STRING_FROM_ID(CP_GROUP_MISSIONS,ENUM_TO_INT(paramMissionID)),". ID calling player bit check")
								BREAK
								CASE FHS_DISPLAYED
									paramDisplayedHelp = TRUE
									g_txtFlowHelpLastDisplayed = ""
									IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_SELECTOR_MISSIONS_A)
										ADD_HELP_TO_FLOW_QUEUE("TRIG_SWTCH", FHP_VERY_HIGH, 0, 21000, DEFAULT_HELP_TEXT_TIME, GET_CURRENT_PLAYER_PED_BIT())
									ENDIF
								BREAK
							ENDSWITCH
						ELSE
							SCRIPT_ASSERT("DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION: Player was an invalid character but we couldn't work out the valid characters to display help for.")
						ENDIF
					ENDIF
				ELIF paramDisplayedHelp
					paramDisplayedHelp = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

#IF USE_CLF_DLC
PROC DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(SP_MISSIONS paramMissionID, VECTOR paramTriggerPos, BOOL &paramDisplayedHelp)
	UNUSED_PARAMETER(paramMissionID)
	UNUSED_PARAMETER(paramTriggerPos)
	UNUSED_PARAMETER(paramDisplayedHelp)
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_TIME() % 240) = 0
			CPRINTLN(DEBUG_TRIGGER, "Reminder. Need to implement DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION for CLF - BenR")
		ENDIF
	#ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(SP_MISSIONS paramMissionID, VECTOR paramTriggerPos, BOOL &paramDisplayedHelp)
	UNUSED_PARAMETER(paramMissionID)
	UNUSED_PARAMETER(paramTriggerPos)
	UNUSED_PARAMETER(paramDisplayedHelp)
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_TIME() % 240) = 0
			CPRINTLN(DEBUG_TRIGGER, "Reminder. Need to implement DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION for NRM - BenR")
		ENDIF
	#ENDIF
ENDPROC
#ENDIF


// ===========================================================================================================
//		Core Trigger Checks
// ===========================================================================================================

#IF USE_CLF_DLC
PROC SCENE_MISSION_TRIGGER_CHECK_CLF(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	SP_MISSIONS eMissionID = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
	
	//Multicoord check.
	INT coordsAndSpriteIndex = 0
	IF IS_BIT_SET(g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].bMultiCoordAndSprite
		coordsAndSpriteIndex = GET_CURRENT_PLAYER_PED_INT()
		IF (coordsAndSpriteIndex > 2) OR (coordsAndSpriteIndex < 0)
			coordsAndSpriteIndex = 0
		ENDIF
	ENDIF
	
	VECTOR vTrigger = g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].vCoords[coordsAndSpriteIndex]
	
	// Manage trigger scene and check for triggering.
	IF DOES_MISSION_HAVE_TRIGGER_SCENE(eMissionID)
	
		//Setup the trigger scene.
		IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
			CALL paramLocalVars.fpBindTriggerScene(eMissionID, g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex])
			RESET_TRIGGER_SCENE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
		ENDIF
		
		DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(eMissionID, vTrigger, paramLocalVars.bDisplayedTriggerRejectHelp)

		//Update the trigger scene and wait for it to trigger.
		IF NOT MANAGE_TRIGGER_SCENE_CLF(paramTriggerIndex, paramLocalVars)
			EXIT
		ENDIF
	ENDIF

	g_iMissionTriggerableToTrigger = paramTriggerIndex
	g_bMissionTriggerFired = TRUE
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "SCENE trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " fired.")
		CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " set to apply to candidate controller.")
	#ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC SCENE_MISSION_TRIGGER_CHECK_NRM(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	SP_MISSIONS eMissionID = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
	
	//Multicoord check.
	INT coordsAndSpriteIndex = 0
	IF IS_BIT_SET(g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].bMultiCoordAndSprite
		coordsAndSpriteIndex = GET_CURRENT_PLAYER_PED_INT()
		IF (coordsAndSpriteIndex > 2) OR (coordsAndSpriteIndex < 0)
			coordsAndSpriteIndex = 0
		ENDIF
	ENDIF
	
	VECTOR vTrigger = g_GameBlips[g_TriggerableMissionsTU[paramTriggerIndex].eBlip].vCoords[coordsAndSpriteIndex]
	
	// Manage trigger scene and check for triggering.
	IF DOES_MISSION_HAVE_TRIGGER_SCENE(eMissionID)
	
		//Setup the trigger scene.
		IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
			CALL paramLocalVars.fpBindTriggerScene(eMissionID, g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex])
			RESET_TRIGGER_SCENE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
		ENDIF
		
		DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(eMissionID, vTrigger, paramLocalVars.bDisplayedTriggerRejectHelp)

		//Update the trigger scene and wait for it to trigger.
		IF NOT MANAGE_TRIGGER_SCENE_NRM(paramTriggerIndex, paramLocalVars)
			EXIT
		ENDIF
	ENDIF

	g_iMissionTriggerableToTrigger = paramTriggerIndex
	g_bMissionTriggerFired = TRUE
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "SCENE trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " fired.")
		CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " set to apply to candidate controller.")
	#ENDIF
ENDPROC
#ENDIF

PROC SCENE_MISSION_TRIGGER_CHECK(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	SP_MISSIONS eMissionID = g_TriggerableMissions[paramTriggerIndex].eMissionID
	
	//Multicoord check.
	INT coordsAndSpriteIndex = 0
	IF IS_BIT_SET(g_GameBlips[g_TriggerableMissions[paramTriggerIndex].eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[g_TriggerableMissions[paramTriggerIndex].eBlip].bMultiCoordAndSprite
		coordsAndSpriteIndex = GET_CURRENT_PLAYER_PED_INT()
		IF (coordsAndSpriteIndex > 2) OR (coordsAndSpriteIndex < 0)
			coordsAndSpriteIndex = 0
		ENDIF
	ENDIF
	
	VECTOR vTrigger = g_GameBlips[g_TriggerableMissions[paramTriggerIndex].eBlip].vCoords[coordsAndSpriteIndex]
	
	// Manage trigger scene and check for triggering.
	IF DOES_MISSION_HAVE_TRIGGER_SCENE(eMissionID)
	
		//Setup the trigger scene.
		IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
			CALL paramLocalVars.fpBindTriggerScene(eMissionID, g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex])
			RESET_TRIGGER_SCENE_STATE(g_TriggerableMissions[paramTriggerIndex].sScene)
		ENDIF
		
		DO_CHARACTER_REJECT_HELP_AT_TRIGGER_POSITION(eMissionID, vTrigger, paramLocalVars.bDisplayedTriggerRejectHelp)

		//Update the trigger scene and wait for it to trigger.
		IF NOT MANAGE_TRIGGER_SCENE(paramTriggerIndex, paramLocalVars)
			EXIT
		ENDIF
	ENDIF

	g_iMissionTriggerableToTrigger = paramTriggerIndex
	g_bMissionTriggerFired = TRUE
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "SCENE trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " fired.")
		CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), " set to apply to candidate controller.")
	#ENDIF
ENDPROC

#IF USE_CLF_DLC
PROC SWITCH_MISSION_TRIGGER_CHECK_CLF(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID))
		EXIT
	ENDIF

	//Set all hint bits for characters associated with this switch trigger.
	INT index
	REPEAT 3 index
		IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, index)
			IF NOT IS_BIT_SET(g_iPlayerFlowHintActive, index)
				CPRINTLN(DEBUG_TRIGGER, "Setting selector hint bit for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)), " due to SWITCH mission trigger.")
				SET_BIT(g_iPlayerFlowHintActive, index)
			ENDIF
		ENDIF
	ENDREPEAT

	// Check if we are in the middle of a switch.
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		//Check if we have passed the stage where the character is swapped.
		IF GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_JUMPCUT_DESCENT
			//Have we swapped to a triggerable character?
			IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
				//Yes. Fire the trigger.
				g_iMissionTriggerableToTrigger = paramTriggerIndex
				g_bMissionTriggerFired = TRUE
				paramLocalVars.bSkipNextSwitchCheck = TRUE
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " fired.")
					CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_bSceneAutoTrigger
			
			IF g_flowUnsaved.bUpdatingGameflow
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bUpdatingGameflow.")
				EXIT
			ENDIF
			
			IF g_flowUnsaved.bFlowControllerBusy
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bFlowControllerBusy.")
				EXIT
			ENDIF
			
			IF g_sMissionActiveData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].leaveArea
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for leaveArea.")
				EXIT
			ENDIF
		
			g_iMissionTriggerableToTrigger = paramTriggerIndex
			g_bMissionTriggerFired = TRUE
			g_bSceneAutoTrigger = FALSE
			
			CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " fired due to autotrigger.")
			CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
			EXIT
		ENDIF
	#ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC SWITCH_MISSION_TRIGGER_CHECK_NRM(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID))
		EXIT
	ENDIF

	//Set all hint bits for characters associated with this switch trigger.
	INT index
	REPEAT 3 index
		IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, index)
			IF NOT IS_BIT_SET(g_iPlayerFlowHintActive, index)
				CPRINTLN(DEBUG_TRIGGER, "Setting selector hint bit for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)), " due to SWITCH mission trigger.")
				SET_BIT(g_iPlayerFlowHintActive, index)
			ENDIF
		ENDIF
	ENDREPEAT

	// Check if we are in the middle of a switch.
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		//Check if we have passed the stage where the character is swapped.
		IF GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_JUMPCUT_DESCENT
			//Have we swapped to a triggerable character?
			IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
				//Yes. Fire the trigger.
				g_iMissionTriggerableToTrigger = paramTriggerIndex
				g_bMissionTriggerFired = TRUE
				paramLocalVars.bSkipNextSwitchCheck = TRUE
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " fired.")
					CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_bSceneAutoTrigger
			
			IF g_flowUnsaved.bUpdatingGameflow
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bUpdatingGameflow.")
				EXIT
			ENDIF
			
			IF g_flowUnsaved.bFlowControllerBusy
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bFlowControllerBusy.")
				EXIT
			ENDIF
			
			IF g_sMissionActiveData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].leaveArea
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for leaveArea.")
				EXIT
			ENDIF
		
			g_iMissionTriggerableToTrigger = paramTriggerIndex
			g_bMissionTriggerFired = TRUE
			g_bSceneAutoTrigger = FALSE
			
			CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " fired due to autotrigger.")
			CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
			EXIT
		ENDIF
	#ENDIF
ENDPROC

#ENDIF

PROC SWITCH_MISSION_TRIGGER_CHECK(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS &paramLocalVars)
	#IF IS_DEBUG_BUILD
		IF g_bBlockMissionTrigger
			EXIT
		ENDIF
	#ENDIF
	
	IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissions[paramTriggerIndex].eMissionID))
		EXIT
	ENDIF

	//Set all hint bits for characters associated with this switch trigger.
	INT index
	REPEAT 3 index
		IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset, index)
			IF NOT IS_BIT_SET(g_iPlayerFlowHintActive, index)
				CPRINTLN(DEBUG_TRIGGER, "Setting selector hint bit for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(INT_TO_ENUM(enumCharacterList, index)), " due to SWITCH mission trigger.")
				SET_BIT(g_iPlayerFlowHintActive, index)
			ENDIF
		ENDIF
	ENDREPEAT

	// Check if we are in the middle of a switch.
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		//Check if we have passed the stage where the character is swapped.
		IF GET_PLAYER_SWITCH_STATE() >= SWITCH_STATE_JUMPCUT_DESCENT
			//Have we swapped to a triggerable character?
			IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
				//Yes. Fire the trigger.
				g_iMissionTriggerableToTrigger = paramTriggerIndex
				g_bMissionTriggerFired = TRUE
				paramLocalVars.bSkipNextSwitchCheck = TRUE
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " fired.")
					CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_bSceneAutoTrigger
			
			IF g_flowUnsaved.bUpdatingGameflow
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bUpdatingGameflow.")
				EXIT
			ENDIF
			
			IF g_flowUnsaved.bFlowControllerBusy
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for bFlowControllerBusy.")
				EXIT
			ENDIF
			
			IF g_sMissionActiveData[g_TriggerableMissions[paramTriggerIndex].eMissionID].leaveArea
				CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " trying to autotriggering but waiting for leaveArea.")
				EXIT
			ENDIF
		
			g_iMissionTriggerableToTrigger = paramTriggerIndex
			g_bMissionTriggerFired = TRUE
			g_bSceneAutoTrigger = FALSE
			
			CPRINTLN(DEBUG_TRIGGER, "SWITCH trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " fired due to autotrigger.")
			CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), " set to apply to candidate controller.")
			EXIT
		ENDIF
	#ENDIF
ENDPROC


// ===========================================================================================================
//		Main Update
// ===========================================================================================================
#IF USE_CLF_DLC
PROC MISSION_TRIGGERER_MAIN_UPDATE_CLF(MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	INT i
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		paramLocalVars.iSwitchTriggerCooldown = GET_GAME_TIMER() + 2000
	ENDIF

	//Only allow triggering while the gameflow is active.
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
		//Are we already on mission or in the process of firing a trigger?
		IF NOT g_bMissionTriggerFired
		
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				//No missions running.
				//Check all triggers to see if they can fire.
				REPEAT g_iRegisteredMissionTriggers i
				
					//Flag which mission we are currently processing.
					g_eMissionTriggerProcessing = g_TriggerableMissionsTU[i].eMissionID
				
					//Check the trigger isn't already fired or in use.
					IF (g_TriggerableMissionsTU[i].bUsed = TRUE) AND (NOT g_bMissionTriggerFired)
					
					//Check the trigger's strand is active.
					IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[g_TriggerableMissionsTU[i].eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
					OR IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[g_TriggerableMissionsTU[i].eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
						CPRINTLN(DEBUG_TRIGGER, "Strand associated with trigger is not active. Removing trigger.")
						Remove_Mission_Trigger(i)
					ELSE
						//Run trigger type specific checks.
						SWITCH g_TriggerableMissionsTU[i].eType
							CASE MISSION_TRIGGER_TYPE_SCENE
								SCENE_MISSION_TRIGGER_CHECK_CLF(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_SWITCH
								SWITCH_MISSION_TRIGGER_CHECK_CLF(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_CALL
								SCRIPT_ASSERT("mission_triggerer: Unimplemented triggering type fired! MISSION_TRIGGER_TYPE_CALL")
							BREAK
						ENDSWITCH
					ENDIF						
			
					ENDIF
				ENDREPEAT
				
				//Are there any autotrigger requests to process?
				IF g_eMissionForceTrigger != SP_MISSION_NONE
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_TRIGGER, "Processing force trigger request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionForceTrigger), ".")
					#ENDIF
					REPEAT g_iRegisteredMissionTriggers i
						IF NOT g_bMissionTriggerFired
							IF g_TriggerableMissionsTU[i].eMissionID = g_eMissionForceTrigger
								g_iMissionTriggerableToTrigger = i
								g_bMissionTriggerFired = TRUE
								SET_BIT(g_TriggerableMissionsTU[i].sScene.iStateBitset, TSS_HAS_TRIGGERED)
								paramLocalVars.bSkipNextSwitchCheck = TRUE
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_TRIGGER, "Trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[i].eMissionID), " fired due to force trigger request.")
									CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[i].eMissionID), " set to apply to candidate controller.")
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					g_eMissionForceTrigger = SP_MISSION_NONE
				ENDIF
			ENDIF
		ELIF g_bMissionTriggerFired = TRUE
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				MISSION_CANDIDATE_MISSION_TYPE_ENUM eMissionTriggerType = GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].eMissionID)

				m_enumMissionCandidateReturnValue eCandidateReturnValue
				eCandidateReturnValue = Request_Mission_Launch(	g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex, 
																g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].eCandidateType, 
																eMissionTriggerType, 
																paramLocalVars.bSkipNextSwitchCheck)
																
				//Flag that we are no longer processing any specific mission trigger.
				g_eMissionTriggerProcessing = SP_MISSION_NONE
										
				//TODO: Fix this to remove possible race conditon.
				SWITCH eCandidateReturnValue
					CASE MCRET_DENIED
						// Failed to launch because something else was higher priority or has already launched
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_TRIGGER, "Candidate controller denied requested trigger launch.")
						#ENDIF
						g_bMissionTriggerFired = FALSE
						paramLocalVars.bSkipNextSwitchCheck = FALSE
					BREAK
					CASE MCRET_ACCEPTED
						CPRINTLN(DEBUG_TRIGGER, "Candidate controller accepted requested trigger launch.")
						
						//Ready to launch! Control of the flag is set.
						g_bMissionTriggerFired = FALSE
						g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].bLaunchMe = TRUE
						g_iMissionTriggerableToTrigger = -1 // No longer need this value. Invalidate it.
						g_iPlayerFlowHintActive = 0			// Clear player hints as we trigger any mission.
						paramLocalVars.bSkipNextSwitchCheck = FALSE
						
						// Make the player invincible while the  mission is in the process of launching. 
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
								CPRINTLN(DEBUG_TRIGGER, "DO_MISSION_AT_BLIP setting player as invincible now.")
							ELSE
								CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player dead 1.")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player not playing 1.")
						ENDIF
					BREAK
					CASE MCRET_PROCESSING
						// Candidate controller is still processing. Do nothing and wait.
					BREAK
				ENDSWITCH	
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "player has died whilst trying to get a candidate ID.")
				IF g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex != NO_CANDIDATE_ID
					//Mission_Over(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex)
					//mission cannot have started at this point so retract instead of end
					RetractCandidateRequest(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex)
				ENDIF
				g_iMissionTriggerableToTrigger = -1
				g_bMissionTriggerFired = FALSE
				paramLocalVars.bSkipNextSwitchCheck = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//Check for requests to preload a trigger scene's assets.
	IF g_eMissionSceneToPreLoad != SP_MISSION_NONE
		IF g_eMissionSceneToCleanup = SP_MISSION_NONE
		OR g_eMissionSceneToCleanup = g_eMissionSceneToPreLoad
			INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToPreLoad)
			IF iMissionTriggerIndex != -1
			
				//Setup the trigger scene.
				IF NOT paramLocalVars.sScenePointers[iMissionTriggerIndex].bSceneBound
					CALL paramLocalVars.fpBindTriggerScene(	g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID, 
															g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, 
															paramLocalVars.sScenePointers[iMissionTriggerIndex])
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), "> ",
											"Binding function pointers for scene to allow preloading.")
				ENDIF
			

				CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
				IF CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
					CPRINTLN(DEBUG_TRIGGER, "Trigger scene for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " assets finished preloading.")
					g_eMissionSceneToCleanup = g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.eMission
					
					paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[iMissionTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
					paramLocalVars.bReleaseFunctionBound = TRUE
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), "> ",
											"Local release function pointer bound as scene was requested to preload.")
					
					g_bMissionTriggerLoading = FALSE
					g_eMissionSceneToPreLoad = SP_MISSION_NONE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but this mission is not triggerable at this time.")
				SCRIPT_ASSERT("Mission triggerer was asked to preload trigger scene assets for a mission that was not triggerable at the time.")
				g_bMissionTriggerLoading = FALSE
				g_eMissionSceneToPreLoad = SP_MISSION_NONE
			ENDIF
#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but blocked by currently loaded screen ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), ".")
#ENDIF
		ENDIF
	ENDIF
	
	//Check for disabling player actions while the player is locked in to a trigger.
	IF g_bPlayerLockedInToTrigger
		DISABLE_SELECTOR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	ENDIF
	
	IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY_FRIENDS)
		IF paramLocalVars.iNoFramesMissionRunning != 0
			paramLocalVars.iNoFramesMissionRunning = 0
		ENDIF
		IF g_bCleanupTriggerScene
			CPRINTLN(DEBUG_TRIGGER, "Trigger scene cleaning up due to global flag being set.")
			CLEANUP_ACTIVE_TRIGGER_SCENE_CLF(paramLocalVars)
		ENDIF
	ELSE
		paramLocalVars.iNoFramesMissionRunning++	
	ENDIF
	
	//Check for thread termination conditions.
	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		IF IS_BIT_SET(g_sMissionStaticData[g_eMissionSceneToCleanup].settingsBitset, MF_INDEX_HAS_LEADIN)
			IF g_bCleanupTriggerScene
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up as mission with lead-in has allowed it.")
				MISSION_TRIGGERER_FULL_CLEANUP_CLF(paramLocalVars)
			ENDIF
		ELSE
			IF paramLocalVars.iNoFramesMissionRunning > 50
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up due to being on mission for 50 frames while a trigger scene with no lead-in is active.")
				MISSION_TRIGGERER_FULL_CLEANUP_CLF(paramLocalVars)
			ENDIF
		ENDIF
	ELIF paramLocalVars.iNoFramesMissionRunning > 0
		CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up instantly due to a mission launching while no trigger scene is active.")
		MISSION_TRIGGERER_FULL_CLEANUP_CLF(paramLocalVars)
	ENDIF
	
	//Check if the help text block has been left active when a scene has cleaned up.
	IF g_bTriggerSceneBlockHelp
		IF NOT g_bTriggerSceneActive
			CPRINTLN(DEBUG_TRIGGER, "Cleared help text block flag as it was flagged when no trigger scenes were active.")
			g_bTriggerSceneBlockHelp = FALSE
		ENDIF
	ENDIF
										
	//Check for toggling of trigger debug rendering.
	#IF IS_DEBUG_BUILD
		Update_Trigger_Mode_Screen_Text()
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_SHIFT, "Toggle mission trigger debug mode.")
			INT iMode = ENUM_TO_INT(g_eTriggerDebugMode)
			iMode++
			IF iMode > ENUM_TO_INT(TDM_ALL_TEXT)
				iMode = 0
				g_bTriggerDebugOn = FALSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ELSE
				g_bTriggerDebugOn = TRUE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ENDIF
			g_eTriggerDebugMode = INT_TO_ENUM(TriggerDebugMode, iMode)
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Trigger debug mode changed to ", iMode, ".")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_CTRL, "Clear mission trigger debug.")
			g_eTriggerDebugMode = TDM_OFF
			g_bTriggerDebugOn = FALSE
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Cleared trigger debug.")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_NONE, "Toggle basic trigger debug.")
			g_bTriggerDebugOn = NOT g_bTriggerDebugOn
			CPRINTLN(DEBUG_TRIGGER, "Toggled basic trigger debug.")
		ENDIF
	#ENDIF
		
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC MISSION_TRIGGERER_MAIN_UPDATE_NRM(MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	INT i
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		paramLocalVars.iSwitchTriggerCooldown = GET_GAME_TIMER() + 2000
	ENDIF

	//TODO: Needs moving to a better location. BenR
	SUPRESS_PLAYER_VEHICLE_ON_ENTERING_RANDOM_EVENT()
	
	//Only allow triggering while the gameflow is active.
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
		//Are we already on mission or in the process of firing a trigger?
		IF NOT g_bMissionTriggerFired
		
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				//No missions running.
				//Check all triggers to see if they can fire.
				REPEAT g_iRegisteredMissionTriggers i
				
					//Flag which mission we are currently processing.
					g_eMissionTriggerProcessing = g_TriggerableMissionsTU[i].eMissionID
				
					//Check the trigger isn't already fired or in use.
					IF (g_TriggerableMissionsTU[i].bUsed = TRUE) AND (NOT g_bMissionTriggerFired)
					
					//Check the trigger's strand is active.
					IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[g_TriggerableMissionsTU[i].eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
					OR IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[g_TriggerableMissionsTU[i].eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
						CPRINTLN(DEBUG_TRIGGER, "Strand associated with trigger is not active. Removing trigger.")
						Remove_Mission_Trigger(i)
					ELSE
						//Run trigger type specific checks.
						SWITCH g_TriggerableMissionsTU[i].eType
							CASE MISSION_TRIGGER_TYPE_SCENE
								SCENE_MISSION_TRIGGER_CHECK_NRM(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_SWITCH
								SWITCH_MISSION_TRIGGER_CHECK_NRM(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_CALL
								SCRIPT_ASSERT("mission_triggerer: Unimplemented triggering type fired! MISSION_TRIGGER_TYPE_CALL")
							BREAK
						ENDSWITCH
					ENDIF						
			
					ENDIF
				ENDREPEAT
				
				//Are there any autotrigger requests to process?
				IF g_eMissionForceTrigger != SP_MISSION_NONE
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_TRIGGER, "Processing force trigger request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionForceTrigger), ".")
					#ENDIF
					REPEAT g_iRegisteredMissionTriggers i
						IF NOT g_bMissionTriggerFired
							IF g_TriggerableMissionsTU[i].eMissionID = g_eMissionForceTrigger
								g_iMissionTriggerableToTrigger = i
								g_bMissionTriggerFired = TRUE
								SET_BIT(g_TriggerableMissionsTU[i].sScene.iStateBitset, TSS_HAS_TRIGGERED)
								paramLocalVars.bSkipNextSwitchCheck = TRUE
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_TRIGGER, "Trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[i].eMissionID), " fired due to force trigger request.")
									CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[i].eMissionID), " set to apply to candidate controller.")
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					g_eMissionForceTrigger = SP_MISSION_NONE
				ENDIF
			ENDIF
		ELIF g_bMissionTriggerFired = TRUE
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				MISSION_CANDIDATE_MISSION_TYPE_ENUM eMissionTriggerType = GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].eMissionID)

				m_enumMissionCandidateReturnValue eCandidateReturnValue
				eCandidateReturnValue = Request_Mission_Launch(	g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex, 
																g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].eCandidateType, 
																eMissionTriggerType, 
																paramLocalVars.bSkipNextSwitchCheck)
																
				//Flag that we are no longer processing any specific mission trigger.
				g_eMissionTriggerProcessing = SP_MISSION_NONE
										
				//TODO: Fix this to remove possible race conditon.
				SWITCH eCandidateReturnValue
					CASE MCRET_DENIED
						// Failed to launch because something else was higher priority or has already launched
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_TRIGGER, "Candidate controller denied requested trigger launch.")
						#ENDIF
						g_bMissionTriggerFired = FALSE
						paramLocalVars.bSkipNextSwitchCheck = FALSE
					BREAK
					CASE MCRET_ACCEPTED
						CPRINTLN(DEBUG_TRIGGER, "Candidate controller accepted requested trigger launch.")
						
						//Ready to launch! Control of the flag is set.
						g_bMissionTriggerFired = FALSE
						g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].bLaunchMe = TRUE
						g_iMissionTriggerableToTrigger = -1 // No longer need this value. Invalidate it.
						g_iPlayerFlowHintActive = 0			// Clear player hints as we trigger any mission.
						paramLocalVars.bSkipNextSwitchCheck = FALSE
						
						// Make the player invincible while the  mission is in the process of launching. 
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
								CPRINTLN(DEBUG_TRIGGER, "DO_MISSION_AT_BLIP setting player as invincible now.")
							ELSE
								CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player dead 1.")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player not playing 1.")
						ENDIF
					BREAK
					CASE MCRET_PROCESSING
						// Candidate controller is still processing. Do nothing and wait.
					BREAK
				ENDSWITCH	
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "player has died whilst trying to get a candidate ID.")
				IF g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex != NO_CANDIDATE_ID
					//Mission_Over(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex)
					//mission cannot have started at this point so retract instead of end
					RetractCandidateRequest(g_TriggerableMissionsTU[g_iMissionTriggerableToTrigger].iCandidateIndex)
				ENDIF
				g_iMissionTriggerableToTrigger = -1
				g_bMissionTriggerFired = FALSE
				paramLocalVars.bSkipNextSwitchCheck = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//Check for requests to preload a trigger scene's assets.
	IF g_eMissionSceneToPreLoad != SP_MISSION_NONE
		IF g_eMissionSceneToCleanup = SP_MISSION_NONE
		OR g_eMissionSceneToCleanup = g_eMissionSceneToPreLoad
			INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToPreLoad)
			IF iMissionTriggerIndex != -1
			
				//Setup the trigger scene.
				IF NOT paramLocalVars.sScenePointers[iMissionTriggerIndex].bSceneBound
					CALL paramLocalVars.fpBindTriggerScene(	g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID, 
															g_TriggerableMissionsTU[iMissionTriggerIndex].sScene, 
															paramLocalVars.sScenePointers[iMissionTriggerIndex])
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[iMissionTriggerIndex].eMissionID), "> ",
											"Binding function pointers for scene to allow preloading.")
				ENDIF
			

				CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
				IF CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
					CPRINTLN(DEBUG_TRIGGER, "Trigger scene for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " assets finished preloading.")
					g_eMissionSceneToCleanup = g_TriggerableMissionsTU[iMissionTriggerIndex].sScene.eMission
					
					paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[iMissionTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
					paramLocalVars.bReleaseFunctionBound = TRUE
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), "> ",
											"Local release function pointer bound as scene was requested to preload.")
					
					g_bMissionTriggerLoading = FALSE
					g_eMissionSceneToPreLoad = SP_MISSION_NONE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but this mission is not triggerable at this time.")
				SCRIPT_ASSERT("Mission triggerer was asked to preload trigger scene assets for a mission that was not triggerable at the time.")
				g_eMissionSceneToPreLoad = SP_MISSION_NONE
			ENDIF
#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but blocked by currently loaded screen ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), ".")
#ENDIF
		ENDIF
	ENDIF
	
	//Check for disabling player actions while the player is locked in to a trigger.
	IF g_bPlayerLockedInToTrigger
		DISABLE_SELECTOR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	ENDIF
	
	IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY_FRIENDS)
		IF paramLocalVars.iNoFramesMissionRunning != 0
			paramLocalVars.iNoFramesMissionRunning = 0
		ENDIF
		IF g_bCleanupTriggerScene
			CPRINTLN(DEBUG_TRIGGER, "Trigger scene cleaning up due to global flag being set.")
			CLEANUP_ACTIVE_TRIGGER_SCENE_NRM(paramLocalVars)
		ENDIF
	ELSE
		paramLocalVars.iNoFramesMissionRunning++	
	ENDIF
	
	//Check for thread termination conditions.
	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		IF IS_BIT_SET(g_sMissionStaticData[g_eMissionSceneToCleanup].settingsBitset, MF_INDEX_HAS_LEADIN)
			IF g_bCleanupTriggerScene
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up as mission with lead-in has allowed it.")
				MISSION_TRIGGERER_FULL_CLEANUP_NRM(paramLocalVars)
			ENDIF
		ELSE
			IF paramLocalVars.iNoFramesMissionRunning > 50
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up due to being on mission for 50 frames while a trigger scene with no lead-in is active.")
				MISSION_TRIGGERER_FULL_CLEANUP_NRM(paramLocalVars)
			ENDIF
		ENDIF
	ELIF paramLocalVars.iNoFramesMissionRunning > 0
		CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up instantly due to a mission launching while no trigger scene is active.")
		MISSION_TRIGGERER_FULL_CLEANUP_NRM(paramLocalVars)
	ENDIF
	
	//Check if the help text block has been left active when a scene has cleaned up.
	IF g_bTriggerSceneBlockHelp
		IF NOT g_bTriggerSceneActive
			CPRINTLN(DEBUG_TRIGGER, "Cleared help text block flag as it was flagged when no trigger scenes were active.")
			g_bTriggerSceneBlockHelp = FALSE
		ENDIF
	ENDIF
										
	//Check for toggling of trigger debug rendering.
	#IF IS_DEBUG_BUILD
		Update_Trigger_Mode_Screen_Text()
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_SHIFT, "Toggle mission trigger debug mode.")
			INT iMode = ENUM_TO_INT(g_eTriggerDebugMode)
			iMode++
			IF iMode > ENUM_TO_INT(TDM_ALL_TEXT)
				iMode = 0
				g_bTriggerDebugOn = FALSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ELSE
				g_bTriggerDebugOn = TRUE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ENDIF
			g_eTriggerDebugMode = INT_TO_ENUM(TriggerDebugMode, iMode)
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Trigger debug mode changed to ", iMode, ".")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_CTRL, "Clear mission trigger debug.")
			g_eTriggerDebugMode = TDM_OFF
			g_bTriggerDebugOn = FALSE
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Cleared trigger debug.")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_NONE, "Toggle basic trigger debug.")
			g_bTriggerDebugOn = NOT g_bTriggerDebugOn
			CPRINTLN(DEBUG_TRIGGER, "Toggled basic trigger debug.")
		ENDIF
	#ENDIF
		
ENDPROC
#ENDIF


// For all registered triggers check if they can trigger this frame.
// If they can try and secure a candidate ID for them.
// If a candidate ID is secured then set the mission to trigger.
PROC MISSION_TRIGGERER_MAIN_UPDATE(MISSION_TRIGGERER_LOCAL_VARS &paramLocalVars)
	INT i
	
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		paramLocalVars.iSwitchTriggerCooldown = GET_GAME_TIMER() + 2000
	ENDIF

#if USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlow.isGameflowActive
#endif
#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlow.isGameflowActive
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	//Only allow triggering while the gameflow is active.
	IF g_savedGlobals.sFlow.isGameflowActive
#endif	
#endif
		//Are we already on mission or in the process of firing a trigger?
		IF NOT g_bMissionTriggerFired
		
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				//No missions running.
				//Check all triggers to see if they can fire.
				REPEAT g_iRegisteredMissionTriggers i
				
					//Flag which mission we are currently processing.
					g_eMissionTriggerProcessing = g_TriggerableMissions[i].eMissionID
				
					//Check the trigger isn't already fired or in use.
					IF (g_TriggerableMissions[i].bUsed = TRUE) AND (NOT g_bMissionTriggerFired)
					
				
					
				#if USE_CLF_DLC							
					//Check the trigger's strand is active.
					IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
					OR IS_BIT_SET(g_savedGlobalsClifford.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
						CPRINTLN(DEBUG_TRIGGER, "Strand associated with trigger is not active. Removing trigger.")
						Remove_Mission_Trigger(i)
					ELSE
						//Run trigger type specific checks.
						SWITCH g_TriggerableMissions[i].eType
							CASE MISSION_TRIGGER_TYPE_SCENE
								SCENE_MISSION_TRIGGER_CHECK(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_SWITCH
								SWITCH_MISSION_TRIGGER_CHECK(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_CALL
								SCRIPT_ASSERT("mission_triggerer: Unimplemented triggering type fired! MISSION_TRIGGER_TYPE_CALL")
							BREAK
						ENDSWITCH
					ENDIF						
				#endif
				
				#if USE_NRM_DLC	
					//Check the trigger's strand is active.
					IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
					OR IS_BIT_SET(g_savedGlobalsnorman.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
						CPRINTLN(DEBUG_TRIGGER, "Strand associated with trigger is not active. Removing trigger.")
						Remove_Mission_Trigger(i)
					ELSE
						//Run trigger type specific checks.
						SWITCH g_TriggerableMissions[i].eType
							CASE MISSION_TRIGGER_TYPE_SCENE
								SCENE_MISSION_TRIGGER_CHECK(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_SWITCH
								SWITCH_MISSION_TRIGGER_CHECK(i, paramLocalVars)
							BREAK
							CASE MISSION_TRIGGER_TYPE_CALL
								SCRIPT_ASSERT("mission_triggerer: Unimplemented triggering type fired! MISSION_TRIGGER_TYPE_CALL")
							BREAK
						ENDSWITCH
					ENDIF						
				#endif
				
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
						//Check the trigger's strand is active.
						IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
						OR IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[g_TriggerableMissions[i].eStrand].savedBitflags, SAVED_BITS_STRAND_TERMINATED)
							CPRINTLN(DEBUG_TRIGGER, "Strand associated with trigger is not active. Removing trigger.")
							Remove_Mission_Trigger(i)
						ELSE
							//Run trigger type specific checks.
							SWITCH g_TriggerableMissions[i].eType
								CASE MISSION_TRIGGER_TYPE_SCENE
									SCENE_MISSION_TRIGGER_CHECK(i, paramLocalVars)
								BREAK
								CASE MISSION_TRIGGER_TYPE_SWITCH
									SWITCH_MISSION_TRIGGER_CHECK(i, paramLocalVars)
								BREAK
								CASE MISSION_TRIGGER_TYPE_CALL
									SCRIPT_ASSERT("mission_triggerer: Unimplemented triggering type fired! MISSION_TRIGGER_TYPE_CALL")
								BREAK
							ENDSWITCH
						ENDIF
				#endif
				#endif
				
					ENDIF
				ENDREPEAT
				
				//Are there any autotrigger requests to process?
				IF g_eMissionForceTrigger != SP_MISSION_NONE
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_TRIGGER, "Processing force trigger request for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionForceTrigger), ".")
					#ENDIF
					REPEAT g_iRegisteredMissionTriggers i
						IF NOT g_bMissionTriggerFired
							IF g_TriggerableMissions[i].eMissionID = g_eMissionForceTrigger
								g_iMissionTriggerableToTrigger = i
								g_bMissionTriggerFired = TRUE
								SET_BIT(g_TriggerableMissions[i].sScene.iStateBitset, TSS_HAS_TRIGGERED)
								paramLocalVars.bSkipNextSwitchCheck = TRUE
								#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_TRIGGER, "Trigger for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[i].eMissionID), " fired due to force trigger request.")
									CPRINTLN(DEBUG_TRIGGER, GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[i].eMissionID), " set to apply to candidate controller.")
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					g_eMissionForceTrigger = SP_MISSION_NONE
				ENDIF
			ENDIF
		ELIF g_bMissionTriggerFired = TRUE
			//Only worth checking for triggering if the player is alive.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
				MISSION_CANDIDATE_MISSION_TYPE_ENUM eMissionTriggerType = GET_SP_MISSION_TRIGGER_TYPE(g_TriggerableMissions[g_iMissionTriggerableToTrigger].eMissionID)

				m_enumMissionCandidateReturnValue eCandidateReturnValue
				eCandidateReturnValue = Request_Mission_Launch(	g_TriggerableMissions[g_iMissionTriggerableToTrigger].iCandidateIndex, 
																g_TriggerableMissions[g_iMissionTriggerableToTrigger].eCandidateType, 
																eMissionTriggerType, 
																paramLocalVars.bSkipNextSwitchCheck)
																
				//Flag that we are no longer processing any specific mission trigger.
				g_eMissionTriggerProcessing = SP_MISSION_NONE
										
				//TODO: Fix this to remove possible race conditon.
				SWITCH eCandidateReturnValue
					CASE MCRET_DENIED
						// Failed to launch because something else was higher priority or has already launched
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_TRIGGER, "Candidate controller denied requested trigger launch.")
						#ENDIF
						g_bMissionTriggerFired = FALSE
						paramLocalVars.bSkipNextSwitchCheck = FALSE
					BREAK
					CASE MCRET_ACCEPTED
						CPRINTLN(DEBUG_TRIGGER, "Candidate controller accepted requested trigger launch.")
						
						//Ready to launch! Control of the flag is set.
						g_bMissionTriggerFired = FALSE
						g_TriggerableMissions[g_iMissionTriggerableToTrigger].bLaunchMe = TRUE
						g_iMissionTriggerableToTrigger = -1 // No longer need this value. Invalidate it.
						g_iPlayerFlowHintActive = 0			// Clear player hints as we trigger any mission.
						paramLocalVars.bSkipNextSwitchCheck = FALSE
						
						// Make the player invincible while the  mission is in the process of launching. 
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
								CPRINTLN(DEBUG_TRIGGER, "DO_MISSION_AT_BLIP setting player as invincible now.")
							ELSE
								CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player dead 1.")
							ENDIF
						ELSE
							CPRINTLN(DEBUG_TRIGGER, "Mission triggerer player not playing 1.")
						ENDIF
					BREAK
					CASE MCRET_PROCESSING
						// Candidate controller is still processing. Do nothing and wait.
					BREAK
				ENDSWITCH	
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "player has died whilst trying to get a candidate ID.")
				IF g_TriggerableMissions[g_iMissionTriggerableToTrigger].iCandidateIndex != NO_CANDIDATE_ID
					//Mission_Over(g_TriggerableMissions[g_iMissionTriggerableToTrigger].iCandidateIndex)
					//mission cannot have started at this point so retract instead of end
					RetractCandidateRequest(g_TriggerableMissions[g_iMissionTriggerableToTrigger].iCandidateIndex)
				ENDIF
				g_iMissionTriggerableToTrigger = -1
				g_bMissionTriggerFired = FALSE
				paramLocalVars.bSkipNextSwitchCheck = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//Check for requests to preload a trigger scene's assets.
	IF g_eMissionSceneToPreLoad != SP_MISSION_NONE
		IF g_eMissionSceneToCleanup = SP_MISSION_NONE
		OR g_eMissionSceneToCleanup = g_eMissionSceneToPreLoad
			INT iMissionTriggerIndex = GET_TRIGGER_INDEX_FOR_MISSION(g_eMissionSceneToPreLoad)
			IF iMissionTriggerIndex != -1
			
				//Setup the trigger scene.
				IF NOT paramLocalVars.sScenePointers[iMissionTriggerIndex].bSceneBound
					CALL paramLocalVars.fpBindTriggerScene(	g_TriggerableMissions[iMissionTriggerIndex].eMissionID, 
															g_TriggerableMissions[iMissionTriggerIndex].sScene, 
															paramLocalVars.sScenePointers[iMissionTriggerIndex])
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[iMissionTriggerIndex].eMissionID), "> ",
											"Binding function pointers for scene to allow preloading.")
				ENDIF
			

				CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
				IF CALL paramLocalVars.sScenePointers[iMissionTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
					CPRINTLN(DEBUG_TRIGGER, "Trigger scene for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " assets finished preloading.")
					g_eMissionSceneToCleanup = g_TriggerableMissions[iMissionTriggerIndex].sScene.eMission
					
					paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[iMissionTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
					paramLocalVars.bReleaseFunctionBound = TRUE
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), "> ",
											"Local release function pointer bound as scene was requested to preload.")
					
					g_bMissionTriggerLoading = FALSE
					g_eMissionSceneToPreLoad = SP_MISSION_NONE
				ENDIF
			ELSE
				CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but this mission is not triggerable at this time.")
				SCRIPT_ASSERT("Mission triggerer was asked to preload trigger scene assets for a mission that was not triggerable at the time.")
				g_eMissionSceneToPreLoad = SP_MISSION_NONE
			ENDIF
#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_TRIGGER, "Requested to preload assets for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToPreLoad), " but blocked by currently loaded screen ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eMissionSceneToCleanup), ".")
#ENDIF
		ENDIF
	ENDIF
	
	//Check for disabling player actions while the player is locked in to a trigger.
	IF g_bPlayerLockedInToTrigger
		DISABLE_SELECTOR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			#if USE_CLF_DLC
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
			#endif
			#if USE_NRM_DLC
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				IF  TRIGGER_SCENE_GET_SP_MISSION() != SP_MISSION_SOLOMON_2
				AND (TRIGGER_SCENE_GET_SP_MISSION() != SP_MISSION_FINALE_INTRO OR GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON)
				AND (TRIGGER_SCENE_GET_SP_MISSION() != SP_MISSION_FBI_4 OR GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON)
					UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				ELSE
					UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE, TRUE)
				ENDIF
			#endif
			#endif
	ENDIF
	
	IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY_FRIENDS)
		IF paramLocalVars.iNoFramesMissionRunning != 0
			paramLocalVars.iNoFramesMissionRunning = 0
		ENDIF
		IF g_bCleanupTriggerScene
			CPRINTLN(DEBUG_TRIGGER, "Trigger scene cleaning up due to global flag being set.")
			CLEANUP_ACTIVE_TRIGGER_SCENE(paramLocalVars)
		ENDIF
	ELSE
		paramLocalVars.iNoFramesMissionRunning++	
	ENDIF
	
	//Check for thread termination conditions.
	IF g_eMissionSceneToCleanup != SP_MISSION_NONE
		IF IS_BIT_SET(g_sMissionStaticData[g_eMissionSceneToCleanup].settingsBitset, MF_INDEX_HAS_LEADIN)
			IF g_bCleanupTriggerScene
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up as mission with lead-in has allowed it.")
				MISSION_TRIGGERER_FULL_CLEANUP(paramLocalVars)
			ENDIF
		ELSE
			IF paramLocalVars.iNoFramesMissionRunning > 50
				CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up due to being on mission for 50 frames while a trigger scene with no lead-in is active.")
				MISSION_TRIGGERER_FULL_CLEANUP(paramLocalVars)
			ENDIF
		ENDIF
	ELIF paramLocalVars.iNoFramesMissionRunning > 0
		CPRINTLN(DEBUG_TRIGGER, "Mission triggerer cleaning up instantly due to a mission launching while no trigger scene is active.")
		MISSION_TRIGGERER_FULL_CLEANUP(paramLocalVars)
	ENDIF
	
	//Check if the help text block has been left active when a scene has cleaned up.
	IF g_bTriggerSceneBlockHelp
		IF NOT g_bTriggerSceneActive
			CPRINTLN(DEBUG_TRIGGER, "Cleared help text block flag as it was flagged when no trigger scenes were active.")
			g_bTriggerSceneBlockHelp = FALSE
		ENDIF
	ENDIF
										
	//Check for toggling of trigger debug rendering.
	#IF IS_DEBUG_BUILD
		Update_Trigger_Mode_Screen_Text()
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_SHIFT, "Toggle mission trigger debug mode.")
			INT iMode = ENUM_TO_INT(g_eTriggerDebugMode)
			iMode++
			IF iMode > ENUM_TO_INT(TDM_ALL_TEXT)
				iMode = 0
				g_bTriggerDebugOn = FALSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ELSE
				g_bTriggerDebugOn = TRUE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ENDIF
			g_eTriggerDebugMode = INT_TO_ENUM(TriggerDebugMode, iMode)
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Trigger debug mode changed to ", iMode, ".")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_T, KEYBOARD_MODIFIER_CTRL, "Clear mission trigger debug.")
			g_eTriggerDebugMode = TDM_OFF
			g_bTriggerDebugOn = FALSE
			Debug_Display_Trigger_Mode_Screen_Text()
			CPRINTLN(DEBUG_TRIGGER, "Cleared trigger debug.")
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_L, KEYBOARD_MODIFIER_NONE, "Toggle basic trigger debug.")
			g_bTriggerDebugOn = NOT g_bTriggerDebugOn
			CPRINTLN(DEBUG_TRIGGER, "Toggled basic trigger debug.")
		ENDIF
	#ENDIF
		
ENDPROC
