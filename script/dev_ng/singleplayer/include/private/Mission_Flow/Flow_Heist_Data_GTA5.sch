//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 29/01/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							   Heist Data Header								│
//│																				│
//│			This header contains all permanent data relating to the 			│
//│			heists and heist planning locations.								│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "blip_control_public.sch"
USING "heist_private.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════╡ Data Setup and Storage Procedures  ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


PROC Store_Heist_Choice_Crew_Data(	INT paramHeistChoice,
									CrewMemberType	paramCrewType1,
									CrewMemberType	paramCrewType2,
									CrewMemberType	paramCrewType3,
									CrewMemberType	paramCrewType4,
									CrewMemberType	paramCrewType5)
									  
	g_sHeistChoiceData[paramHeistChoice].eCrewType[0] = paramCrewType1
	g_sHeistChoiceData[paramHeistChoice].eCrewType[1] = paramCrewType2
	g_sHeistChoiceData[paramHeistChoice].eCrewType[2] = paramCrewType3
	g_sHeistChoiceData[paramHeistChoice].eCrewType[3] = paramCrewType4
	g_sHeistChoiceData[paramHeistChoice].eCrewType[4] = paramCrewType5
	
	//Set size of crew based on crew member type enums passed in.
	IF paramCrewType1 = CMT_EMPTY
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 0
	ELIF paramCrewType2 = CMT_EMPTY
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 1
	ELIF paramCrewType3 = CMT_EMPTY
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 2
	ELIF paramCrewType4 = CMT_EMPTY
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 3
	ELIF paramCrewType5 = CMT_EMPTY
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 4
	ELSE
		g_sHeistChoiceData[paramHeistChoice].iCrewSize = 5
	ENDIF
ENDPROC


PROC Store_Heist_Choice_Dialogue_Data(	INT paramHeistChoice,
										STRING paramChoiceLabel,
										STRING paramGunmanLabel,
										STRING paramDriverLabel,
										STRING paramHackerLabel,
										STRING paramExitLabel)
									
	TEXT_LABEL_7 tChoice = paramChoiceLabel
	TEXT_LABEL_7 tGunman = paramGunmanLabel
	TEXT_LABEL_7 tDriver = paramDriverLabel
	TEXT_LABEL_7 tHacker = paramHackerLabel
	TEXT_LABEL_7 tExit =   paramExitLabel
															
	g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[HCDS_CHOICE] = tChoice
	g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[HCDS_GUNMAN] = tGunman
	g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[HCDS_DRIVER]	= tDriver
	g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[HCDS_HACKER] = tHacker
	g_sHeistChoiceData[paramHeistChoice].tChoiceDialogueLabels[HCDS_EXIT]	= tExit
ENDPROC


PROC Store_Planning_Loc_Data(	PlanningLocationName paramPlanningLocation, 
								VECTOR paramPosition, 
								VECTOR paramBoardPosition, 
								FLOAT paramBoardHeading,
								STRING paramInteriorName)
	
	//Store data into global array.
	g_sPlanningLocationData[paramPlanningLocation].vPlanningLocation = paramPosition
	g_sPlanningLocationData[paramPlanningLocation].vPlanningLocationBoard = paramBoardPosition
	g_sPlanningLocationData[paramPlanningLocation].fPlanningLocationBoardHeading = paramBoardHeading
	g_sPlanningLocationData[paramPlanningLocation].tPlanningLocationRoomName = paramInteriorName
ENDPROC


PROC Store_CrewMember_Base_Data(CrewMember 		eCrewMember,
								CrewMemberType	eType,
								INT				iJobCut,
								BOOL			bUnlockedFromStart)
											
	g_sCrewMemberStaticData[eCrewMember].type = eType
	g_sCrewMemberStaticData[eCrewMember].jobCut = iJobCut
	IF bUnlockedFromStart
		SET_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, ENUM_TO_INT(eCrewMember))
	ELSE
		CLEAR_BIT(g_savedGlobals.sHeistData.iCrewUnlockedBitset, ENUM_TO_INT(eCrewMember))
	ENDIF
	CLEAR_BIT(g_savedGlobals.sHeistData.iCrewDeadBitset, ENUM_TO_INT(eCrewMember))
ENDPROC


PROC Store_CrewMember_Base_Model_Data(	CrewMember	eCrewMember,
										MODEL_NAMES	eCharModel,
										INT iHeadDrawable,
										INT iDefaultOutfit)
	
	g_sCrewMemberStaticData[eCrewMember].model 					= eCharModel
	g_sCrewMemberStaticData[eCrewMember].headVariation 			= iHeadDrawable
	g_sCrewMemberStaticData[eCrewMember].defaultOutfitVariation = iDefaultOutfit
ENDPROC


PROC Store_CrewMember_Starting_Stat_Data(CrewMember 	eCrewMember,
										CrewMemberSkill	eBaseSkill,
										INT				iStat1,
										INT				iStat2,
										INT				iStat3,
										INT				iStat4)
									
	g_savedGlobals.sHeistData.sCrewActiveData[eCrewMember].skill = eBaseSkill
	PRIVATE_Store_Crew_Member_Stat_At_Index(eCrewMember, 0, iStat1)
	PRIVATE_Store_Crew_Member_Stat_At_Index(eCrewMember, 1, iStat2)
	PRIVATE_Store_Crew_Member_Stat_At_Index(eCrewMember, 2, iStat3)
	PRIVATE_Store_Crew_Member_Stat_At_Index(eCrewMember, 3, iStat4)
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ GTA5 Heist Data Setup ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


PROC Store_All_GTA5_Planning_Location_Data()
	//						Planning Location	Room Position						Board Position						Board Heading	Interior Name		
	Store_Planning_Loc_Data(PLN_STRIPCLUB,		<<97.6772,-1290.7390,28.2688>>,		<<99.083,-1290.025,29.829>>,		300.00,			"strp3off"		)
	Store_Planning_Loc_Data(PLN_SWEATSHOP,		<<706.4884,-964.5811,29.4334>>,		<<705.301,-963.586,30.562>>,		45.00,			"V_35_OfficeRm"	)
 
	Store_Planning_Loc_Data(PLN_METH_LAB,		<<1398.7949,3603.7756,37.9419>>,	<<1399.308,3602.098,39.472>>,		200.130,		"V_39_UpperRm2"	)
	Store_Planning_Loc_Data(PLN_TREV_CITY,		<<-1157.0675,-1524.0396,9.6346>>,	<<-1156.341,-1525.126,11.275>>,		215.00,			"rm_Lounge"		)
ENDPROC


PROC Store_All_GTA5_Heist_Choice_Data()
	//								Heist Choice Index					Crew Type 1		Crew Type 2		Crew Type 3		Crew Type 4 	Crew Type 5
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_JEWEL_STEALTH,			CMT_DRIVER,		CMT_GUNMAN,		CMT_HACKER,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_JEWEL_HIGH_IMPACT,		CMT_DRIVER,		CMT_GUNMAN,		CMT_HACKER,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_DOCKS_DEEP_SEA,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_DOCKS_BLOW_UP_BOAT,	CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_RURAL_NO_TANK,			CMT_GUNMAN,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_AGENCY_FIRETRUCK,		CMT_GUNMAN,		CMT_GUNMAN,		CMT_EMPTY,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_AGENCY_HELICOPTER,		CMT_HACKER,		CMT_GUNMAN,		CMT_DRIVER,		CMT_EMPTY,		CMT_EMPTY	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_FINALE_TRAFFCONT,		CMT_GUNMAN,		CMT_GUNMAN,		CMT_DRIVER,		CMT_DRIVER,		CMT_HACKER	)
	Store_Heist_Choice_Crew_Data( 	HEIST_CHOICE_FINALE_HELI,			CMT_DRIVER,		CMT_DRIVER,		CMT_GUNMAN,		CMT_GUNMAN,		CMT_EMPTY	)

	//								Heist Choice Index					Dialogue Label	Gunman Select	Driver Select	Hacker Select	Exit
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_JEWEL_STEALTH,		"JHFP2",		"JHFP5", 		"JHFP7", 		"JHFP8",		"JHFP9a"	)
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_JEWEL_HIGH_IMPACT,	"JHFP3", 		"JHFP6", 		"JHFP7", 		"JHFP8",		"JHFP9b"	)	
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_DOCKS_BLOW_UP_BOAT,	"DHP2", 		"", 			"", 			"",				"DHP8A"		)	
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_DOCKS_DEEP_SEA,		"DHP3", 		"", 			"", 			"",				"DHP8A"		)		
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_RURAL_NO_TANK,		"", 			"", 			"", 			"",				""			)
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_AGENCY_FIRETRUCK,		"AHP1", 		"", 			"", 			"",				""			)
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_AGENCY_HELICOPTER,	"AHP2",			"AHP4", 		"AHP6", 		"AHP5",			""			)
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_FINALE_TRAFFCONT,		"FHP1", 		"FHP6", 		"FHP7A", 		"FHP7B",		"FHP2"		)	
	Store_Heist_Choice_Dialogue_Data(HEIST_CHOICE_FINALE_HELI,			"FHP3", 		"FHP8A", 		"FHP8B", 		"",				"FHP4"		)
ENDPROC


PROC Store_All_GTA5_Crew_Member_Data()
	//							Crew Enum					Type Enum		Job Cut		Unlocked From Start
	Store_CrewMember_Base_Data(	CM_GUNMAN_G_GUSTAV,			CMT_GUNMAN,		14,			TRUE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_G_PACKIE_UNLOCK,	CMT_GUNMAN,		12,			FALSE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_G_CHEF_UNLOCK,	CMT_GUNMAN,		12,			FALSE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_M_HUGH,			CMT_GUNMAN,		7,			FALSE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_G_KARL,			CMT_GUNMAN,		8,			FALSE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_B_NORM,			CMT_GUNMAN,		7,			TRUE)
	Store_CrewMember_Base_Data(	CM_GUNMAN_B_DARYL,			CMT_GUNMAN,		6,			FALSE)
																	
	Store_CrewMember_Base_Data(	CM_HACKER_G_PAIGE,			CMT_HACKER,		15,			TRUE)
	Store_CrewMember_Base_Data(	CM_HACKER_M_CHRIS,			CMT_HACKER,		10,			TRUE)
	Store_CrewMember_Base_Data(	CM_HACKER_B_RICKIE_UNLOCK,	CMT_HACKER,		4,			FALSE)
																		
	Store_CrewMember_Base_Data(	CM_DRIVER_G_EDDIE,			CMT_DRIVER,		14,			TRUE)
	Store_CrewMember_Base_Data(	CM_DRIVER_G_TALINA_UNLOCK,	CMT_DRIVER,		5,			FALSE)
	Store_CrewMember_Base_Data(	CM_DRIVER_B_KARIM,			CMT_DRIVER,		8,			TRUE)
	
	
	//								 Crew Enum						Model Name		Head	Default-Outfit
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_G_GUSTAV,			HC_GUNMAN,		4,		1)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_G_PACKIE_UNLOCK,		HC_GUNMAN,		5,		2)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_G_CHEF_UNLOCK,		HC_GUNMAN,		6,		1)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_G_KARL,				HC_GUNMAN,		1,		1)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_M_HUGH,				HC_GUNMAN,		3,		0)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_B_NORM,				HC_GUNMAN,		0,		0)
	Store_CrewMember_Base_Model_Data(CM_GUNMAN_B_DARYL,				HC_GUNMAN,		2,		2)
													
	Store_CrewMember_Base_Model_Data(CM_HACKER_G_PAIGE,				HC_HACKER,		1,		1)
	Store_CrewMember_Base_Model_Data(CM_HACKER_M_CHRIS,				HC_HACKER,		0,		0)
	Store_CrewMember_Base_Model_Data(CM_HACKER_B_RICKIE_UNLOCK,		HC_HACKER,		2,		3)
													
	Store_CrewMember_Base_Model_Data(CM_DRIVER_G_EDDIE,				HC_DRIVER,		0,		1)
	Store_CrewMember_Base_Model_Data(CM_DRIVER_G_TALINA_UNLOCK,		HC_DRIVER,		2,		4)
	Store_CrewMember_Base_Model_Data(CM_DRIVER_B_KARIM,				HC_DRIVER,		1,		0)	
	
	
	//									Crew Enum						Base Skill		Max Health		Accuracy		Shoot Rate		Weapon Choice
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_G_GUSTAV,				CMSK_GOOD,		700,			79,				83,				77				)	
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_G_PACKIE_UNLOCK,		CMSK_GOOD,		800,			91,				89,				71				)		
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_G_CHEF_UNLOCK,		CMSK_GOOD,		675,			86,				78,				90				)		
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_G_KARL,				CMSK_BAD,		275,			40,				28,				52				)		
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_M_HUGH,				CMSK_BAD,		300,			35,				32,				48				)	
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_B_NORM,				CMSK_BAD,		250,			39,				40,				44				)		
	Store_CrewMember_Starting_Stat_Data(CM_GUNMAN_B_DARYL,				CMSK_BAD,		200,			42,				35,				46				)		
	
	//									Crew Enum						Base Skill		Sys Knowledge	Decrypt Skill	Access Speed	NOT USED																
	Store_CrewMember_Starting_Stat_Data(CM_HACKER_G_PAIGE,				CMSK_GOOD,		85,				77,				93,				0				)		
	Store_CrewMember_Starting_Stat_Data(CM_HACKER_M_CHRIS,				CMSK_MEDIUM,	59,				59,				43,				0				)	
	Store_CrewMember_Starting_Stat_Data(CM_HACKER_B_RICKIE_UNLOCK,		CMSK_BAD,		36,				45,				39,				0				)		
	
	//									Crew Enum						Base Skill		Driving Skill	Composure		Vehicle Choice	NOT USED															
	Store_CrewMember_Starting_Stat_Data(CM_DRIVER_G_EDDIE,				CMSK_GOOD,		72,				91,				86,				0				)		
	Store_CrewMember_Starting_Stat_Data(CM_DRIVER_G_TALINA_UNLOCK,		CMSK_GOOD,		94,				83,				68,				0				)		
	Store_CrewMember_Starting_Stat_Data(CM_DRIVER_B_KARIM,				CMSK_BAD,		60,				54,				24,				0				)		
ENDPROC


PROC Initialise_GTA5_Heist_Data_On_Startup()
	Store_All_GTA5_Planning_Location_Data()
	Store_All_GTA5_Heist_Choice_Data()
	Store_All_GTA5_Crew_Member_Data()
ENDPROC
