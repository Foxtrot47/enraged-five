USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_private_core.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_GAME.sch"
#ENDIF


// All individual strand initialiser header files


#if USE_CLF_DLC
	USING "Strand_CLF_ARA_Initialiser.sch"
	USING "Strand_CLF_Initialiser.sch"
	USING "Strand_CLF_CAS1_Initialiser.sch"
	USING "Strand_CLF_CAS2_Initialiser.sch"
	USING "Strand_CLF_CAS3_Initialiser.sch"
	USING "Strand_CLF_RUS_Initialiser.sch"
	USING "Strand_CLF_KOR_Initialiser.sch"
	USING "Strand_CLF_IAA_Initialiser.sch"
	USING "Strand_CLF_ASS_Initialiser.sch"
	USING "Strand_CLF_ASS2_Initialiser.sch"
	USING "Strand_CLF_Jetpack_Initialiser.sch"
	USING "Strand_CLF_MIL_Initialiser.sch"
	USING "Strand_CLF_MIL2_Initialiser.sch"
	USING "Strand_CLF_MIL3_Initialiser.sch"
	USING "Strand_CLF_RC_ALEX_Initialiser.sch"
	USING "Strand_CLF_RC_MEL_Initialiser.sch"
	USING "Strand_CLF_RC_AGN_Initialiser.sch"
	USING "Strand_CLF_RC_CLA_Initialiser.sch"
#endif

#if USE_NRM_DLC
	USING "Strand_NRM_survive_Initialiser.sch"
	USING "Strand_NRM_rescueEng_Initialiser.sch"
	USING "Strand_NRM_rescueMed_Initialiser.sch"
	USING "Strand_NRM_rescueGun_Initialiser.sch"
	USING "Strand_NRM_supFuel_Initialiser.sch"
	USING "Strand_NRM_supAmmo_Initialiser.sch"
	USING "Strand_NRM_supMeds_Initialiser.sch"
	USING "Strand_NRM_supFood_Initialiser.sch"
	USING "Strand_NRM_radio_Initialiser.sch"	
#endif

#if not USE_SP_DLC
	USING "Strand_Agency_Heist_Initialiser.sch"
	USING "Strand_Armenian_Initialiser.sch"
	USING "Strand_Assassinations_Initialiser.sch"
	USING "Strand_Car_Steal_Initialiser.sch"
	USING "Strand_Chinese_Initialiser.sch"
	USING "Strand_Docks_Heist_Initialiser.sch"
	USING "Strand_Docks_Heist_2_Initialiser.sch"
	USING "Strand_Exile_Initialiser.sch"
	USING "Strand_Family_Initialiser.sch"
	USING "Strand_FBI_Officers_Initialiser.sch"
	USING "Strand_FBI_Officers_2_Initialiser.sch"
	USING "Strand_FBI_Officers_3_Initialiser.sch"
	USING "Strand_FBI_Officers_4_Initialiser.sch"
	USING "Strand_FBI_Officers_5_Initialiser.sch"
	USING "Strand_Finale_Initialiser.sch"
	USING "Strand_Finale_Heist_Initialiser.sch"
	USING "Strand_Finale_Heist_2_Initialiser.sch"
	USING "Strand_Finale_Heist_3_Initialiser.sch"
	USING "Strand_Finale_Heist_4_Initialiser.sch"
	USING "Strand_Franklin_Initialiser.sch"
	USING "Strand_Jewel_Heist_Initialiser.sch"
	USING "Strand_Jewel_Heist_2_Initialiser.sch"
	USING "Strand_Lamar_Initialiser.sch"
	USING "Strand_Lester_Initialiser.sch"
	USING "Strand_Martin_Initialiser.sch"
	USING "Strand_Michael_Initialiser.sch"
	USING "Strand_Michael_Events_Initialiser.sch"
	USING "Strand_Prologue_Initialiser.sch"
	USING "Strand_Rural_Bank_Heist_Initialiser.sch"
	USING "Strand_Shrink_Initialiser.sch"
	USING "Strand_Solomon_Initialiser.sch"
	USING "Strand_Trevor_Initialiser.sch"
#endif

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Mission_Flow_Initialiser.sch
//		CREATED			:	Keith
//		MAINTAINED		:	Robert Wright, Ben Rollinson
//		DESCRIPTION		:	Re-initialises all global mission flow array data prior to
//							loading any saved game or during SP/MP transition.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// ===========================================================================================================
//		Initialise Mission Flow Globals
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Strand Control Arrays
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Adds the commands for each mission flow strand to the commands array
// 
#if USE_CLF_DLC
PROC Store_Mission_Flow_Global_Strand_Arrays_For_SP_CLF()
		
	INT 		tempLoop = 0
	STRANDS	loopAsStrandID
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "SP MISSION FLOW COMMANDS SETUP")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
		
	REPEAT MAX_STRANDS_CLF tempLoop
		loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)
		
		//Original vs DLC check
	
		SWITCH (loopAsStrandID)
			CASE STRAND_CLF_ARA
				Initialise_Strand_CLF_ARA()
				BREAK
			CASE STRAND_CLF_CAS1
				Initialise_Strand_CLF_CAS1()
				BREAK
			CASE STRAND_CLF_CAS2
				Initialise_Strand_CLF_CAS2()
				BREAK
			CASE STRAND_CLF_CAS3
				Initialise_Strand_CLF_CAS3()
				BREAK
			CASE STRAND_CLF_IAA
				Initialise_Strand_CLF_IAA()
				BREAK
			CASE STRAND_CLF_KOR
				Initialise_Strand_CLF_KOR()
				BREAK
			CASE STRAND_CLF_RUS
				Initialise_Strand_CLF_RUS()
				BREAK
			CASE STRAND_CLF
				Initialise_Strand_CLF()
				BREAK
			CASE STRAND_CLF_ASS_PAP
				Initialise_Strand_CLF_ASS()
				BREAK	
			CASE STRAND_CLF_ASS_A14
				Initialise_Strand_CLF_ASS2()
				BREAK	
			CASE STRAND_CLF_JET
				Initialise_Strand_CLF_Jetpack()
				BREAK	
			CASE STRAND_CLF_MIL
				Initialise_Strand_CLF_MIL()
				BREAK
			CASE STRAND_CLF_MIL2
				Initialise_Strand_CLF_MIL2()
				BREAK
			CASE STRAND_CLF_MIL3
				Initialise_Strand_CLF_MIL3()
				BREAK
			CASE STRAND_CLF_RC_ALEX
				Initialise_Strand_CLF_RC_Alex()
				BREAK
			CASE STRAND_CLF_RC_MELODY
				Initialise_Strand_CLF_RC_Melody()
				BREAK
			CASE STRAND_CLF_RC_AGNES
				Initialise_Strand_CLF_RC_Agnes()
				BREAK
			CASE STRAND_CLF_RC_CLAIRE
				Initialise_Strand_CLF_RC_Claire()
				BREAK
			DEFAULT
				SCRIPT_ASSERT("Store_Mission_Flow_Global_Strand_Arrays_For_SP_CLF: Failed to find strand ID for commands")
				BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
		//Need a wait here to safeguard against instruction per frame overruns.
		WAIT(0)
	ENDREPEAT
	
	
ENDPROC
#endif

#if USE_NRM_DLC
PROC Store_Mission_Flow_Global_Strand_Arrays_For_SP_NRM()
		
	INT 		tempLoop = 0
	STRANDS	loopAsStrandID
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "SP MISSION FLOW COMMANDS SETUP")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
		
	REPEAT MAX_STRANDS_NRM tempLoop
		loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)
		
		//Original vs DLC check
	
		SWITCH (loopAsStrandID)
			CASE STRAND_NRM_SURVIVE
				Initialise_Strand_NRM_survive()
				BREAK
				
			CASE STRAND_NRM_RESCUE_ENG
				Initialise_Strand_NRM_rescueEng()
				BREAK	
			CASE STRAND_NRM_RESCUE_MED
				Initialise_Strand_NRM_rescueMed()
				BREAK	
			CASE STRAND_NRM_RESCUE_GUN
				Initialise_Strand_NRM_rescueGun()
				BREAK	
				
			CASE STRAND_NRM_SUPPLY_FUEL
				Initialise_Strand_NRM_suppliesFuel()
				BREAK	
			CASE STRAND_NRM_SUPPLY_AMMO
				Initialise_Strand_NRM_suppliesAmmo()
				BREAK
			CASE STRAND_NRM_SUPPLY_MED
				Initialise_Strand_NRM_suppliesMeds()
				BREAK	
			CASE STRAND_NRM_SUPPLY_FOOD
				Initialise_Strand_NRM_suppliesFood()
				BREAK		
				
			CASE STRAND_NRM_RADIO
				Initialise_Strand_NRM_radio()
				BREAK	
				
				
			DEFAULT
				SCRIPT_ASSERT("Store_Mission_Flow_Global_Strand_Arrays_FOR_SP_NRM: Failed to find strand ID for commands")
				BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
		//Need a wait here to safeguard against instruction per frame overruns.
		WAIT(0)
	ENDREPEAT
	
	
ENDPROC
#endif

#IF NOT DEFINED(Store_Mission_Flow_Global_Strand_Arrays_For_SP)

PROC Store_Mission_Flow_Global_Strand_Arrays_For_SP()
	
	#IF USE_CLF_DLC
		Store_Mission_Flow_Global_Strand_Arrays_For_SP_CLF()
		EXIT
	#ENDIF
	
	#IF USE_NRM_DLC
		Store_Mission_Flow_Global_Strand_Arrays_For_SP_NRM()
		EXIT
	#ENDIF
	
	#IF NOT USE_SP_DLC
	INT tempLoop = 0
	STRANDS	loopAsStrandID
	
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "SP MISSION FLOW COMMANDS SETUP")
	CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")
	
	REPEAT MAX_STRANDS tempLoop
	
		loopAsStrandID = INT_TO_ENUM(STRANDS, tempLoop)
		SWITCH (loopAsStrandID)
			CASE STRAND_AGENCY_HEIST
				Initialise_Strand_Agency_Heist()
				BREAK
		
			CASE STRAND_ARMENIAN
				Initialise_Strand_Armenian()
				BREAK
				
			CASE STRAND_ASSASSINATIONS
				Initialise_Strand_Assassinations()
				BREAK
				
			CASE STRAND_CAR_STEAL
				Initialise_Strand_Car_Steal()
				BREAK
				
			CASE STRAND_CHINESE
				Initialise_Strand_Chinese()
				BREAK
				
			CASE STRAND_DOCKS_HEIST
				Initialise_Strand_Docks_Heist()
				BREAK
				
			CASE STRAND_DOCKS_HEIST_2
				Initialise_Strand_Docks_Heist_2()
				BREAK
				
			CASE STRAND_EXILE
				Initialise_Strand_Exile()
				BREAK
				
			CASE STRAND_FAMILY
				Initialise_Strand_Family()
				BREAK
				
			CASE STRAND_FBI_OFFICERS
				Initialise_Strand_FBI_Officers()
				BREAK
				
			CASE STRAND_FBI_OFFICERS_2
				Initialise_Strand_FBI_Officers_2()
				BREAK
				
			CASE STRAND_FBI_OFFICERS_3
				Initialise_Strand_FBI_Officers_3()
				BREAK
				
			CASE STRAND_FBI_OFFICERS_4
				Initialise_Strand_FBI_Officers_4()
				BREAK
				
			CASE STRAND_FBI_OFFICERS_5
				Initialise_Strand_FBI_Officers_5()
				BREAK
				
			CASE STRAND_FINALE
				Initialise_Strand_Finale()
				BREAK
				
			CASE STRAND_FINALE_HEIST
				Initialise_Strand_Finale_Heist()
				BREAK
				
			CASE STRAND_FINALE_HEIST_2
				Initialise_Strand_Finale_Heist_2()
				BREAK
			
			CASE STRAND_FINALE_HEIST_3
				Initialise_Strand_Finale_Heist_3()
				BREAK
				
			CASE STRAND_FINALE_HEIST_4
				Initialise_Strand_Finale_Heist_4()
				BREAK
				
			CASE STRAND_FRANKLIN
				Initialise_Strand_Franklin()
				BREAK
				
			CASE STRAND_JEWEL_HEIST
				Initialise_Strand_Jewel_Heist()
				BREAK
				
			CASE STRAND_JEWEL_HEIST_2
				Initialise_Strand_Jewel_Heist_2()
				BREAK
				
			CASE STRAND_LAMAR
				Initialise_Strand_Lamar()
				BREAK
				
			CASE STRAND_LESTER
				Initialise_Strand_Lester()
				BREAK
				
			CASE STRAND_MARTIN
				Initialise_Strand_Martin()
				BREAK
				
			CASE STRAND_MICHAEL
				Initialise_Strand_Michael()
				BREAK
				
			CASE STRAND_MICHAEL_EVENTS
				Initialise_Strand_Michael_Events()
				BREAK
				
			CASE STRAND_PROLOGUE
				Initialise_Strand_Prologue()
				BREAK
				
			CASE STRAND_RURAL_BANK_HEIST
				Initialise_Strand_Rural_Bank_Heist()
				BREAK
				
			CASE STRAND_SHRINK
				Initialise_Strand_Shrink()
				BREAK
				
			CASE STRAND_SOLOMON
				Initialise_Strand_Solomon()
				BREAK
				
			CASE STRAND_TREVOR
				Initialise_Strand_Trevor()
				BREAK
				
			DEFAULT
				SCRIPT_ASSERT("Store_Mission_Flow_Global_Strand_Arrays: Failed to find strand ID for commands")
				BREAK
		ENDSWITCH
	
		CDEBUG1LN(DEBUG_FLOW_STORAGE, "------------------------------")

		//Need a wait here to safeguard against instruction per frame overruns.
		WAIT(0)
	ENDREPEAT
	
	#ENDIF
	
ENDPROC

#ENDIF


// ===========================================================================================================
//		Mission Flow Globals Setup
// ===========================================================================================================

// PURPOSE:	Called from startup to refill the mission flow global variables with their initial values
// 
PROC Initialise_Mission_Flow_Global_Variables_On_SP_Startup()

	CPRINTLN(DEBUG_INIT_SP, "============================================")
	CPRINTLN(DEBUG_INIT_SP, "---- INITIALISE SP MISSION FLOW GLOBALS ----")
	CPRINTLN(DEBUG_INIT_SP, "============================================")

	Clear_Mission_Flow_Non_Saved_Globals()
	Store_Mission_Flow_Global_Strand_Arrays_For_SP()
	
	#IF IS_DEBUG_BUILD
		// Check basic consistency of mission flow data
		Check_Mission_Flow_Commands_Array()
		Output_Mission_Flow_Array_Usage()
		#IF ENABLE_FLOW_COMMAND_HASH_ID_CHECKING
			RUN_FLOW_COMMAND_HASH_ID_UNIQUENESS_CHECK()
		#ENDIF
	#ENDIF
ENDPROC
