USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Jewel_Heist_2_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the Strand Jewel Heist 2 global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Jewel_Heist_2()

	STRANDS	thisStrand = STRAND_JEWEL_HEIST_2

	START_STRAND_COMMANDS_STORAGE				(thisStrand)
	SETFLOW_AWAIT_ACTIVATION					()
	
	SETFLOW_TEXT_PLAYER							(BIT_MICHAEL, CHAR_LESTER, CT_FLOW, TEXT_JEWEL_P2A_UNLOCK, 240000, 10000, VID_BLANK, CID_UNLOCK_BZ_GAS_PREP, FLOW_CHECK_NONE, BIT_UNLOCKS_MISSION|BIT_NO_SILENT_TXTMSG)
	SETFLOW_DO_FLAG_JUMPS						(FLOWFLAG_HEIST_JEWEL_PREP2A_READY, NEXT_SEQUENTIAL_COMMAND, STAY_ON_THIS_COMMAND, NEXT_SEQUENTIAL_COMMAND)
	SETFLOW_WAIT_IN_REAL_TIME					(3000)
	SETFLOW_DISPLAY_HELP_TEXT					("AM_H_PREP2A", DEFAULT_HELP_TEXT_TIME, FHP_HIGH, BIT_MICHAEL)
	SETFLOW_DISPLAY_HELP_TEXT					("AM_H_PREP2B", DEFAULT_HELP_TEXT_TIME, FHP_HIGH, BIT_MICHAEL)
	
	SETFLOW_LABEL								(LABEL_HEIST_JEWEL_RUN_PREP_2A)
	SETFLOW_DO_MISSION_AT_BLIP					(SP_HEIST_JEWELRY_PREP_2A,	NEXT_SEQUENTIAL_COMMAND,	LABEL_HEIST_JEWEL_RUN_PREP_2A)
	SETFLOW_CANCEL_HELP_TEXT					("AM_H_PREP2A")
	SETFLOW_CANCEL_HELP_TEXT					("AM_H_PREP2B")
	SETFLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY	(HEIST_JEWEL, PBDG_10, TRUE)
	
	//Michael phones Lester to tell him he's got the BZ. Modify call if both both preps are completed.
	SETFLOW_IS_MISSION_COMPLETED				(SP_HEIST_JEWELRY_PREP_1A, LABEL_HEIST_JEWEL_CALL_P2A_PREPS_DONE, NEXT_SEQUENTIAL_COMMAND)
	SETFLOW_PLAYER_PHONE_NPC					(CHAR_MICHAEL, CHAR_LESTER, CT_END_OF_MISSION, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME, CALL_JEWEL_PREP2A_DONE, VID_BLANK, CID_BLANK, FLOW_CHECK_NONE, BIT_WAIT_SENT)
	SETFLOW_LABEL								(LABEL_HEIST_JEWEL_CALL_P2A_PREPS_DONE)
	
	SETFLOW_SEND_SYNCHRONISATION				(SYNC_JEWEL_HEIST_TO_JEWEL_HEIST_2)
	
	SETFLOW_TERMINATE							()
	STOP_STRAND_COMMANDS_STORAGE				(thisStrand)
	
ENDPROC
