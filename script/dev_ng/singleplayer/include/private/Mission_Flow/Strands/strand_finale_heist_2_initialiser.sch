USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Finale_Heist_2_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the Strand Finale Heist 2 global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Finale_Heist_2()

	STRANDS	thisStrand = STRAND_FINALE_HEIST_2

	//Store Flow Commands
	START_STRAND_COMMANDS_STORAGE(thisStrand)
	SETFLOW_AWAIT_ACTIVATION		()
	
	SETFLOW_DO_INT_JUMPS			(FLOWINT_HEIST_CHOICE_FINALE, HEIST_CHOICE_FINALE_TRAFFCONT, LABEL_HEIST_FINALE_PC_EMAIL, LABEL_HEIST_FINALE_PD_TEXT)
	
	//Email the player about the prep mission. Quality of email based on the hacker they picked.
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_PC_EMAIL)
	SETFLOW_STORE_FLAG_STATE		(FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE, FALSE)
	SETFLOW_RUN_CODE_ID				(CID_SEND_BIG_SCORE_PREPC_EMAIL, 0)
	SETFLOW_DO_FLAG_JUMPS			(FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE, NEXT_SEQUENTIAL_COMMAND, STAY_ON_THIS_COMMAND, NEXT_SEQUENTIAL_COMMAND)
	SETFLOW_WAIT_IN_REAL_TIME		(3000)
	SETFLOW_DISPLAY_HELP_TEXT		("AM_H_GAUNT_R")
	
	SETFLOW_ACTIVATE_STRAND			(STRAND_FINALE_HEIST_3)
	SETFLOW_ACTIVATE_STRAND			(STRAND_FINALE_HEIST_4)

	//Run mission "finale_hest_prepC" Gauntlet .
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_PC1)
	SETFLOW_DO_MISSION_AT_BLIP		(SP_HEIST_FINALE_PREP_C1, NEXT_SEQUENTIAL_COMMAND, LABEL_HEIST_FINALE_PC1)
	SETFLOW_RUN_CODE_ID				(CID_BIG_SCORE_PREPC_COMPLETED, 0)
	SETFLOW_SEND_SYNCHRONISATION	(SYNC_FINALE_HEIST_TO_FINALE_HEIST2)
	SETFLOW_GOTO					(LABEL_HEIST_FINALE_2_TERMINATE)
	
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_PD_TEXT)
	SETFLOW_TEXT_PLAYER				(BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR, CHAR_LESTER, CT_FLOW, TEXT_FINH_PD_UNLOCK, 240000, 10000, VID_BLANK, CID_BLANK, FLOW_CHECK_NONE, BIT_UNLOCKS_MISSION|BIT_WAIT_SENT|BIT_NO_SILENT_TXTMSG)
	SETFLOW_WAIT_IN_REAL_TIME		(3001)
		
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_PD)
	SETFLOW_DO_MISSION_AT_BLIP		(SP_HEIST_FINALE_PREP_D, NEXT_SEQUENTIAL_COMMAND, LABEL_HEIST_FINALE_PD)
	SETFLOW_RUN_CODE_ID				(CID_BIG_SCORE_PREPD_COMPLETED, 0)
	SETFLOW_SEND_SYNCHRONISATION	(SYNC_FINALE_HEIST_TO_FINALE_HEIST2, 1)
	
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_2_TERMINATE)
	SETFLOW_TERMINATE				()
	STOP_STRAND_COMMANDS_STORAGE	(thisStrand)
	
ENDPROC

