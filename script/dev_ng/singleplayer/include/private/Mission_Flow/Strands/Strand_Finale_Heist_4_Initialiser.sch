USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Finale_Heist_4_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the Strand Finale Heist 4 global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Finale_Heist_4()

	STRANDS	thisStrand = STRAND_FINALE_HEIST_4

	//Store Flow Commands
	START_STRAND_COMMANDS_STORAGE(thisStrand)
	SETFLOW_AWAIT_ACTIVATION		()
	
	SETFLOW_LABEL					(LABEL_HEIST_FINALE_PC3)
	SETFLOW_DO_MISSION_AT_BLIP		(SP_HEIST_FINALE_PREP_C3, NEXT_SEQUENTIAL_COMMAND, LABEL_HEIST_FINALE_PC3)
	SETFLOW_RUN_CODE_ID				(CID_BIG_SCORE_PREPC_COMPLETED, 0)
	SETFLOW_SEND_SYNCHRONISATION	(SYNC_FINALE_HEIST_TO_FINALE_HEIST4)
	
	SETFLOW_TERMINATE				()
	STOP_STRAND_COMMANDS_STORAGE	(thisStrand)
	
ENDPROC

