USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_FBI_Officers_4_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the FBI Officers 4 strand global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_FBI_Officers_4()

	STRANDS	thisStrand = STRAND_FBI_OFFICERS_4

	// Store Flow Commands
	START_STRAND_COMMANDS_STORAGE		(thisStrand)
	SETFLOW_AWAIT_ACTIVATION			()
	
	SETFLOW_LABEL						(LABEL_MISS_FBI_4_PREP_4)
	SETFLOW_RUN_CODE_ID					(CID_ACTIVATE_SHOP_CLOTHES_AMB, 0)
	SETFLOW_DO_MISSION_AT_BLIP			(SP_MISSION_FBI_4_PREP_4, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_FBI_4_PREP_4)
	SETFLOW_RUN_CODE_ID					(CID_ACTIVATE_SHOP_CLOTHES_AMB, 1) // Unlock again to change blip
	SETFLOW_SET_BUILDING_STATE			(BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG, BUILDINGSTATE_DESTROYED)
	SETFLOW_SEND_SYNCHRONISATION		(SYNC_FBI_OFF_TO_FBI_OFF4)
	
	SETFLOW_TERMINATE					()
	STOP_STRAND_COMMANDS_STORAGE		(thisStrand)
	
ENDPROC


