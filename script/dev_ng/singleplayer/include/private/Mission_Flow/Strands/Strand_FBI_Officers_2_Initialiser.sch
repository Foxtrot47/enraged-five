USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_FBI_Officers_2_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the FBI Officers 2 strand global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_FBI_Officers_2()

	STRANDS	thisStrand = STRAND_FBI_OFFICERS_2

	// Store Flow Commands
	START_STRAND_COMMANDS_STORAGE		(thisStrand)
	SETFLOW_AWAIT_ACTIVATION			()
	
	SETFLOW_LABEL						(LABEL_MISS_FBI_4_PREP_2)
	SETFLOW_DO_MISSION_AT_BLIP			(SP_MISSION_FBI_4_PREP_2, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_FBI_4_PREP_2)
	SETFLOW_SET_VEHICLE_GEN_STATE		(VEHGEN_FBI4_TOWING, TRUE)
	SETFLOW_SEND_SYNCHRONISATION		(SYNC_FBI_OFF_TO_FBI_OFF2)
	
	SETFLOW_TERMINATE					()
	STOP_STRAND_COMMANDS_STORAGE		(thisStrand)
	
ENDPROC


