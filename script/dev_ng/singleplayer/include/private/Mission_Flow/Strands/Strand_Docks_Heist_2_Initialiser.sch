USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Docks_Heist_2_Initialiser.sch
//		CREATED			:	Ben R
//		DESCRIPTION		:	Re-initialises the Strand Docks Heist global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Docks_Heist_2()

	STRANDS	thisStrand = STRAND_DOCKS_HEIST_2

	START_STRAND_COMMANDS_STORAGE				(thisStrand)
	SETFLOW_AWAIT_ACTIVATION					()

	SETFLOW_TEXT_PLAYER							(BIT_TREVOR, CHAR_WADE, CT_FLOW, TEXT_DOCKS_P2B_UNLOCK, 240000, 10000, VID_BLANK, CID_BLANK, FLOW_CHECK_NONE, BIT_UNLOCKS_MISSION|BIT_WAIT_SENT|BIT_NO_SILENT_TXTMSG)
	SETFLOW_WAIT_IN_REAL_TIME					(3000)
	SETFLOW_DISPLAY_HELP_TEXT					("AM_H_PREP3B", DEFAULT_GOD_TEXT_TIME, FHP_HIGH, BIT_TREVOR)
	SETFLOW_TEXT_PLAYER							(BIT_TREVOR, CHAR_WADE, CT_FLOW, TEXT_DOCKS_P2B_REMINDER, 600000, 10000)

	SETFLOW_LABEL								(LABEL_HEIST_DOCKS_RUN_P2B)
	SETFLOW_DO_MISSION_AT_BLIP					(SP_HEIST_DOCKS_PREP_2B, NEXT_SEQUENTIAL_COMMAND, LABEL_HEIST_DOCKS_RUN_P2B)
	SETFLOW_CANCEL_TEXT							(TEXT_DOCKS_P2B_REMINDER)
	SETFLOW_SET_VEHICLE_GEN_STATE				(VEHGEN_DOCKSP2B_CHINOOK, TRUE)
	SETFLOW_SET_BOARD_DISPLAY_GROUP_VISIBILITY	(HEIST_DOCKS, PBDG_6, TRUE)
	SETFLOW_SEND_SYNCHRONISATION				(SYNC_DOCKS_HEIST_TO_DOCKS_HEIST_2)
	
	SETFLOW_TERMINATE							()
	STOP_STRAND_COMMANDS_STORAGE				(thisStrand)
	
ENDPROC
