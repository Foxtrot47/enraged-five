USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_FBI_Officers_5_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the FBI Officers 5 strand global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_FBI_Officers_5()

	STRANDS	thisStrand = STRAND_FBI_OFFICERS_5

	// Store Flow Commands
	START_STRAND_COMMANDS_STORAGE		(thisStrand)
	SETFLOW_AWAIT_ACTIVATION			()
	
	SETFLOW_LABEL						(LABEL_MISS_FBI_4_PREP_5)
	SETFLOW_SET_BUILDING_STATE			(BUILDINGNAME_ES_FIB4_SWEATSHOP_KITBAG, BUILDINGSTATE_NORMAL)				// Remove kitbag from sweatshop
	SETFLOW_DO_MISSION_AT_BLIP			(SP_MISSION_FBI_4_PREP_5, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_FBI_4_PREP_5)
	
	SETFLOW_SET_SHOP_STATE				(GUN_SHOP_01_DT, TRUE) // Force shop blip to update
	
	// add the boiler suits to wardrobe
	SETFLOW_DEBUG_RUN_CODE_ID			(CID_DEBUG_GIVE_PREP_BOILER_SUITS,0)
	SETFLOW_SEND_SYNCHRONISATION		(SYNC_FBI_OFF_TO_FBI_OFF5)
	
	SETFLOW_TERMINATE					()
	STOP_STRAND_COMMANDS_STORAGE		(thisStrand)
	
ENDPROC


