USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		HEADER NAME		:	Strand_Lamar_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the Strand Lamar global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Lamar()

	STRANDS	thisStrand = STRAND_LAMAR

	START_STRAND_COMMANDS_STORAGE				(thisStrand)
	SETFLOW_AWAIT_ACTIVATION					()
	
	SETFLOW_DO_FLAG_JUMPS						(FLOWFLAG_UNLOCK_LAMAR_1, NEXT_SEQUENTIAL_COMMAND, STAY_ON_THIS_COMMAND)
	SETFLOW_AWAIT_SYNCHRONISATION				(SYNC_LAMAR1_TO_FRANKLIN0)
	
	SETFLOW_LABEL								(LABEL_MISS_LAMAR_1_HOLD)
	SETFLOW_DO_FLAG_JUMPS						(FLOWFLAG_BLOCK_FRAN_MISSIONS_FOR_TREV, STAY_ON_THIS_COMMAND, NEXT_SEQUENTIAL_COMMAND)

	SETFLOW_LABEL								(LABEL_MISS_LAMAR_1)
	SETFLOW_DO_MISSION_AT_BLIP					(SP_MISSION_LAMAR,	NEXT_SEQUENTIAL_COMMAND,	LABEL_MISS_LAMAR_1)
		
	SETFLOW_SET_WEAPON_LOCK_STATE				(WEAPONTYPE_SAWNOFFSHOTGUN, TRUE) // Gunshop unlocked during lamar1 so unlock weapons here
	SETFLOW_SET_WEAPON_LOCK_STATE				(WEAPONTYPE_PUMPSHOTGUN, TRUE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_STRETCH,	CHAR_FRANKLIN,	TRUE)
	SETFLOW_SET_CHAR_CHAT_PHONECALL				(CHAR_STRETCH, 	BIT_FRANKLIN, CHAT_STR01)
	
	SETFLOW_RUN_CODE_ID							(CID_ACTIVATE_RE_BLOCK_POST_LAMAR1, 0)
	SETFLOW_LABEL_LAUNCHER_ONLY					(LABEL_RE_BATCH_POST_LAMAR1)
	
	SETFLOW_SET_FRANKLIN_OUTFIT_BITSET_STATE	(FALSE) // Stop tracking Franklin's outfit changes
	
	SETFLOW_SEND_SYNCHRONISATION				(SYNC_FRANKLIN1_TO_LAMAR)
	
	SETFLOW_TERMINATE							()
	STOP_STRAND_COMMANDS_STORAGE				(thisStrand)
	
ENDPROC


