USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Prologue_Initialiser.sch
//		CREATED			:	Keith
//		DESCRIPTION		:	Re-initialises the Strand Prologue global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Prologue()

	STRANDS	thisStrand = STRAND_PROLOGUE

	START_STRAND_COMMANDS_STORAGE				(thisStrand)
	SETFLOW_AWAIT_ACTIVATION					()
	
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)
	SETFLOW_SET_BUILDING_STATE					(BUILDINGNAME_IPL_CRUISESHIP, BUILDINGSTATE_DESTROYED)
	
	//These three activities are being unlocked at the beginning of the game at the moment.
	//They will be moved to relevant points in the flow later.
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_ALLOW_CINEMA_ACTIVITY,	TRUE)
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_ALLOW_COMEDYCLUB_ACTIVITY,TRUE)
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_ALLOW_LIVEMUSIC_ACTIVITY,	TRUE)
	
	SETFLOW_SET_BIKES_SUPRESSED_STATE			(TRUE) // Suppresses all motorbikes
	SETFLOW_RUN_CODE_ID							(CID_TAXI_HAILING_DISABLE, 0)
	
	SETFLOW_SET_SAVEHOUSE_STATE					(SAVEHOUSE_MICHAEL_PRO, TRUE, FALSE) 	// Allow an autosave to be performed at the end of the prologue mission
	SETFLOW_SET_SAVEHOUSE_STATE					(SAVEHOUSE_FRANKLIN_PRO, TRUE, FALSE) 	// 
	SETFLOW_SET_SAVEHOUSE_STATE					(SAVEHOUSE_TREVOR_PRO, TRUE, FALSE) 	// 
	
	SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("flowStartAccept")
//	
//	//email in phone before the player plays as franklin
//	SETFLOW_EMAIL_PLAYER						(BIT_FRANKLIN,CHAR_TANISHA,CT_AMBIENT,EMAIL_TANISHA_EMAIL_NG,0,1000,VID_BLANK,CID_BLANK,FLOW_CHECK_NONE,BIT_NONCRIT_TXTMSG)
//	SETFLOW_EMAIL_PLAYER						(BIT_MICHAEL,CHAR_DR_FRIEDLANDER,CT_AMBIENT,EMAIL_DRFRIEDLANDER_EMAIL_A1,0,1000,VID_BLANK,CID_BLANK,FLOW_CHECK_NONE,BIT_NONCRIT_TXTMSG)
//			
	SETFLOW_LABEL								(LABEL_MISS_PROLOGUE)
	SETFLOW_DO_MISSION_NOW						(SP_MISSION_PROLOGUE, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_PROLOGUE, STAY_ON_THIS_COMMAND)
	SETFLOW_LAUNCH_SCRIPT_AND_FORGET			("flowIntroTitle")
	
	// Set up initial phonebooks and friends.
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_SIMEON,			CHAR_FRANKLIN,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_LAMAR,			CHAR_FRANKLIN,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_TANISHA,			CHAR_FRANKLIN,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_DENISE,			CHAR_FRANKLIN,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_HAO,				CHAR_FRANKLIN,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_CALL911,			CHAR_FRANKLIN, 	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_DR_FRIEDLANDER,	CHAR_MICHAEL,	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_DAVE,				CHAR_MICHAEL, 	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_LESTER,			CHAR_MICHAEL, 	FALSE)
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_CALL911,			CHAR_MICHAEL, 	FALSE)
	
	SETFLOW_ADD_PHONE_CONTACT					(CHAR_CALL911,			CHAR_TREVOR, 	FALSE)
	
	SETFLOW_ADD_NEW_FRIEND_CONTACT				(CHAR_MICHAEL,			CHAR_FRANKLIN)	// To allow ambient meetup, M/F friend connection init here, but phone contacts not setup until family3
	SETFLOW_ADD_NEW_FRIEND_CONTACT				(CHAR_TREVOR,			CHAR_FRANKLIN)	// To allow ambient meetup, T/F friend connection init here, but phone contacts not setup until fbi2
	
	SETFLOW_RUN_CODE_ID							(CID_RESET_PLAYER_VARIATION_MICHAEL,	0)
	SETFLOW_RUN_CODE_ID							(CID_RESET_PLAYER_VARIATION_TREVOR,		0)
	SETFLOW_RUN_CODE_ID							(CID_DISABLE_LOST_SCENARIO_GROUP,		0)
	SETFLOW_RUN_CODE_ID							(CID_START_EMAILS,	0)
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_MIC_PRO_MASK_REMOVED, TRUE)
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_TRV_PRO_MASK_REMOVED, TRUE)
	
	SETFLOW_RUN_CODE_ID							(CID_INITIALISE_BANK_DATA, 0)
	SETFLOW_RUN_CODE_ID							(CID_INITIALISE_STAT_OFFSETS, 0)
	
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN, TRUE) //Movie studio is open for the armenian 1 chase
	
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_PLAYER_PED_INTRODUCED_M, 	FALSE) 	// Clear the Michael introduced flag as we set it in the rologue strand
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_PLAYER_PED_INTRODUCED_T, 	FALSE) 	// Clear the Trevor introduced flag as we set it in the Prologue strand
	SETFLOW_STORE_FLAG_STATE					(FLOWFLAG_PLAYER_PED_INTRODUCED_F,	TRUE)
	
	// Make Franklin the current player character.
	SETFLOW_SET_CHAR_HOTSWAP_AVAILABILITY		(CHAR_MICHAEL, 		FALSE)
	SETFLOW_SET_CHAR_HOTSWAP_AVAILABILITY		(CHAR_FRANKLIN, 	TRUE)
	SETFLOW_SET_CHAR_HOTSWAP_AVAILABILITY		(CHAR_TREVOR, 		FALSE)
	SETFLOW_RESET_PRIORITY_SWITCH_SCENES		(CHAR_FRANKLIN)
	SETFLOW_SET_PLAYER_CHARACTER				(CHAR_FRANKLIN)
	SETFLOW_RUN_CODE_ID							(CID_POST_PROLOGUE_FRANKLIN_POS_CHECK, 0)
	SETFLOW_DO_FLAG_JUMPS						(FLOWFLAG_FRANKLIN_FROZEN_POST_PROLOGUE, NEXT_SEQUENTIAL_COMMAND, STAY_ON_THIS_COMMAND)
	
	SETFLOW_ACTIVATE_STRAND						(STRAND_ARMENIAN)
	
	SETFLOW_TERMINATE							()
	STOP_STRAND_COMMANDS_STORAGE(thisStrand)
	
ENDPROC


