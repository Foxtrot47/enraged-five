USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_FBI_Officers_3_Initialiser.sch
//		CREATED			:	BenR
//		DESCRIPTION		:	Re-initialises the FBI Officers 3 strand global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_FBI_Officers_3()

	STRANDS	thisStrand = STRAND_FBI_OFFICERS_3

	// Store Flow Commands
	START_STRAND_COMMANDS_STORAGE		(thisStrand)
	SETFLOW_AWAIT_ACTIVATION			()
	
	SETFLOW_LABEL						(LABEL_MISS_FBI_4_PREP_3)
	SETFLOW_DO_MISSION_AT_BLIP			(SP_MISSION_FBI_4_PREP_3, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_FBI_4_PREP_3)
	SETFLOW_SEND_SYNCHRONISATION		(SYNC_FBI_OFF_TO_FBI_OFF3)
	
	SETFLOW_TERMINATE					()
	STOP_STRAND_COMMANDS_STORAGE		(thisStrand)
	
ENDPROC


