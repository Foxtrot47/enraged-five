USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_storage_GAME.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Strand_Lester_Initialiser.sch
//		CREATED			:	Keith
//		DESCRIPTION		:	Re-initialises the Strand Lester global variables.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// PURPOSE:	Adds all commands for this strand to the Mission Flow Commands array
// 
PROC Initialise_Strand_Lester()

	STRANDS	thisStrand = STRAND_LESTER
	
	// Store Flow Commands
	START_STRAND_COMMANDS_STORAGE(thisStrand)
	
	SETFLOW_AWAIT_ACTIVATION				()

	SETFLOW_LABEL							(LABEL_MISS_LESTER_1)
	SETFLOW_DO_MISSION_AT_BLIP				(SP_MISSION_LESTER_1, NEXT_SEQUENTIAL_COMMAND, LABEL_MISS_LESTER_1)
	SETFLOW_ADD_PHONE_CONTACT				(CHAR_LESTER, CHAR_MICHAEL, FALSE)
	SETFLOW_SET_COMPONENT_ACQUIRED			(CHAR_FRANKLIN, OUTFIT_P1_UPTOWN_1, COMP_TYPE_OUTFIT)
	SETFLOW_SET_COMPONENT_ACQUIRED			(CHAR_FRANKLIN, OUTFIT_P1_UPTOWN_2, COMP_TYPE_OUTFIT)
	SETFLOW_SET_COMPONENT_ACQUIRED			(CHAR_FRANKLIN, OUTFIT_P1_UPTOWN_3, COMP_TYPE_OUTFIT)
	SETFLOW_SET_COMPONENT_ACQUIRED			(CHAR_FRANKLIN, OUTFIT_P1_UPTOWN_4, COMP_TYPE_OUTFIT)
	
	SETFLOW_RUN_CODE_ID						(CID_UNLOCK_LAMAR_1, 0)
	SETFLOW_RUN_CODE_ID					    (CID_STOCK_EVENT_LESTER1, 0)

	//Unlock clothes shop with help text.
	SETFLOW_DISPLAY_HELP_TEXT				("AM_H_WARDROB", DEFAULT_GOD_TEXT_TIME, FHP_HIGH, BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR)
	
	SETFLOW_RUN_CODE_ID						(CID_UNLOCK_SHOPS_POST_LESTER_1A, 0)
	
	//Unlock new carmods
	SETFLOW_EMAIL_PLAYER					(BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR, CHAR_LS_CUSTOMS, CT_AMBIENT, EMAIL_CARMOD_UNLOCK, 130000, 10000)
	SETFLOW_RUN_CODE_ID						(CID_CARMOD_UNLOCK_STAGE_1, 0)
	
	SETFLOW_ACTIVATE_STRAND					(STRAND_MICHAEL_EVENTS)
	
	//Lifehacker coder phones Michael to tell him he knows about the phone assassination and tries to get work.
	SETFLOW_PHONE_PLAYER					(BIT_MICHAEL, CHAR_RICKIE, CT_AMBIENT, 500000, 30000, CALL_HACKER_2, COMM_NONE, VID_BLANK, CID_UNLOCK_LIFEHACK_CREW)
	
	//Lester 1 end of mission call. Unlocks Jewel Store Job.
	SETFLOW_PLAYER_PHONE_NPC      			(CHAR_MICHAEL, CHAR_LESTER, CT_END_OF_MISSION, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME, CALL_JEWEL_SETUP_UNLOCK, VID_BLANK, CID_BLANK, FLOW_CHECK_NONE, BIT_WAIT_SENT|BIT_QUICK)
	SETFLOW_ACTIVATE_STRAND					(STRAND_JEWEL_HEIST)
	
	SETFLOW_LABEL_LAUNCHER_ONLY				(LABEL_SHOP_CLOTHES_LOW)
	SETFLOW_LABEL_LAUNCHER_ONLY				(LABEL_SHOP_CLOTHES_MID)
	SETFLOW_LABEL_LAUNCHER_ONLY				(LABEL_SHOP_CLOTHES_HIGH)

	SETFLOW_EMAIL_PLAYER					(BIT_MICHAEL, CHAR_LESTER, CT_FLOW, EMAIL_STOCKMARKET, 240000, 10000)
	
	SETFLOW_TERMINATE						()
	
	STOP_STRAND_COMMANDS_STORAGE			(thisStrand)
	
ENDPROC
