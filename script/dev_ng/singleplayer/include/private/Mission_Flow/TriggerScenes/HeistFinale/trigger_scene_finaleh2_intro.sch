//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Finale Heist 2 Intro - Trigger Scene Data					│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "stripclub_public.sch"

CONST_FLOAT 	TS_FINALEH2_INTRO_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH2_INTRO_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH2_INTRO_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINALEH2_INTRO_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH2_INTRO_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH2_INTRO_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH2_INTRO_REQUEST_ASSETS()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		REQUEST_NPC_PED_MODEL(CHAR_LESTER)
		REQUEST_MODEL(PROP_CS_WALKING_STICK)
		REQUEST_ANIM_DICT("MISSBIGSCORE2ALEADINOUT@BS_2A_2B_INT")
	ENDIF
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, TRUE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH2_INTRO_RELEASE_ASSETS()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_LESTER)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
		REMOVE_ANIM_DICT("MISSBIGSCORE2ALEADINOUT@BS_2A_2B_INT")
	ENDIF
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, FALSE)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH2_INTRO_HAVE_ASSETS_LOADED()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
		OR NOT HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
		OR NOT HAS_ANIM_DICT_LOADED("MISSBIGSCORE2ALEADINOUT@BS_2A_2B_INT")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT HAS_HEIST_BOARD_LOADED(HEIST_FINALE)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH2_INTRO_CREATE()

	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		//Create Lester.
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, << 99.266, -1294.164, 28.265 >>, 217.5577)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0, 0)
		
		//Create Lester's walking stick and attach it to him.
		g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_WALKING_STICK, <<99.8142, -1293.7738, 28.2679>>)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)

		//Start synchronized scene on Lester.
		INT iSyncScene = CREATE_SYNCHRONIZED_SCENE(<< 99.266, -1294.164, 28.265 >>, << 0.000, 0.000, -43.000 >>)		
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iSyncScene, "MISSBIGSCORE2ALEADINOUT@BS_2A_2B_INT", "LESTER_BASE_IDLE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(iSyncScene, TRUE)
	ENDIF
	
	CREATE_PINNED_HEIST_BOARD(HEIST_FINALE)

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_FINALE_2_INTRO,
												"BS_2A_2B_INT",
												CS_3,
												CS_2|CS_3,
												CS_ALL)
												
	MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_HEIST_FINALE_2_INTRO, "LESTER", ANCHOR_EYES, 0, 0)
												
												
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_LOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH2_INTRO_RELEASE()
	
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH2_INTRO_DELETE()
	
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH2_INTRO_HAS_BEEN_TRIGGERED()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	VEHICLE_INDEX viLast
	IF IS_PLAYER_PED_PLAYABLE(ePlayer)
		SWITCH ePlayer
			CASE CHAR_MICHAEL
			CASE CHAR_FRANKLIN
				VECTOR vTriggerPosition
				vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_2I, GET_CURRENT_PLAYER_PED_INT())
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					//Render trigger zone debug.
					#IF IS_DEBUG_BUILD
						DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINALEH2_INTRO_TRIGGER_DIST)
					#ENDIF
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition) < (TS_FINALEH2_INTRO_TRIGGER_DIST*TS_FINALEH2_INTRO_TRIGGER_DIST)
						
						//B* 1757046: Clear Franklin's vehicle since you finish as Michael
						viLast= GET_LAST_DRIVEN_VEHICLE()
						IF DOES_ENTITY_EXIST(viLast) 
							IF NOT IS_ENTITY_DEAD(viLast)
								//CPRINTLN(debug_golf,"Deleting F's car")
								UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_MISSION_VEH,<<-23.9,-1437.3,31.2>>,0)
								SET_ENTITY_COORDS(viLast,<<-23.9,-1437.3,31.2>>)
								//SET_LAST_DRIVEN_VEHICLE(viLast)
							ENDIF
						ENDIF
					
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE CHAR_TREVOR
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<99.678421,-1295.230957,27.515306>>, <<101.810539,-1294.028809,30.768753>>, 5.000000)
				
					//B* 1757046: Also clear Trevors's vehicle (since you finish as Michael)
						viLast= GET_LAST_DRIVEN_VEHICLE()
						IF DOES_ENTITY_EXIST(viLast) 
							IF NOT IS_ENTITY_DEAD(viLast)
								//CPRINTLN(debug_golf,"Deleting T's car")
								UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_MISSION_VEH,<<138.5988, -1274.9763, 28.2980>>,292.4026)
								SET_ENTITY_COORDS(viLast,<<138.5988, -1274.9763, 28.2980>>)
								SET_ENTITY_HEADING(viLast,292.4026)
								//SET_LAST_DRIVEN_VEHICLE(viLast)
							ENDIF
						ENDIF
				
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH2_INTRO_HAS_BEEN_DISRUPTED()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//B* 2074010: Angled area check for vehicle in the corridor, disrupt scene if yes
	IF NOT IS_ENTITY_DEAD(player_ped_id())
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<99.790749,-1293.349854,27.749529>>, <<103.507774,-1299.958496,31.018785>>, 2.375000)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH2_INTRO_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH2_INTRO_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH2_INTRO_AMBIENT_UPDATE()
ENDPROC
