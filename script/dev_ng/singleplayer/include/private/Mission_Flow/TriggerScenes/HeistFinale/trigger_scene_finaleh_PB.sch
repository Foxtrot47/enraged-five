//╒═════════════════════════════════════════════════════════════════════════════╕
//│			Docks_Prep2B Steal Cutter Trigger Scene - Ben Barclay				│
//│																				│
//│								Date: 11/12/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "locates_public.sch"
USING "flow_mission_trigger_public.sch"
USING "script_ped.sch"

CONST_FLOAT 	TS_FINALEH_PB_STREAM_IN_DIST			130.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH_PB_STREAM_OUT_DIST			150.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH_PB_TRIGGER_DIST				65.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT		TS_FINALEH_PB_FLYING_TRIGGER_DIST		90.0	// Distance from trigger's blip at which the scene triggers when flying.
CONST_FLOAT 	TS_FINALEH_PB_BATTLE_BUDDY_CALL_DIST	500.0 	// Distance from trigger's blip at which the player can call in battle buddies.

CONST_FLOAT		TS_FINALEH_PB_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH_PB_FRIEND_ACCEPT_BITS	BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player

BOOL PlayersWeaponPutAwayPB
BOOL MissionHasBeenDisrupted
BOOL WorkerScenarioTaskGiven[7]

//Groups
REL_GROUP_HASH workerGroup
REL_GROUP_HASH GuardGroup

//Scenario
SCENARIO_BLOCKING_INDEX YardWorkersArea1, YardWorkersArea2

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH_PB_RESET()

	MissionHasBeenDisrupted 		= FALSE
	PlayersWeaponPutAwayPB			= FALSE
	WorkerScenarioTaskGiven[0]		= FALSE
	WorkerScenarioTaskGiven[1]		= FALSE
	WorkerScenarioTaskGiven[2]		= FALSE
	WorkerScenarioTaskGiven[3]		= FALSE
	WorkerScenarioTaskGiven[4]		= FALSE
	WorkerScenarioTaskGiven[5]		= FALSE
	WorkerScenarioTaskGiven[6]		= FALSE
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH_PB_REQUEST_ASSETS()
	
	REQUEST_ADDITIONAL_TEXT("FINPRB", MISSION_TEXT_SLOT)
//	REQUEST_MODEL(CUTTER)
	REQUEST_MODEL(ARMYTRAILER2)
	REQUEST_MODEL(PACKER)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_M_SECURITY_01)
	
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH_PB_RELEASE_ASSETS()

//	SET_MODEL_AS_NO_LONGER_NEEDED(CUTTER)
	SET_MODEL_AS_NO_LONGER_NEEDED(ARMYTRAILER2)
	SET_MODEL_AS_NO_LONGER_NEEDED(PACKER)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_SECURITY_01)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, TRUE)
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH_PB_HAVE_ASSETS_LOADED()
	
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
//	AND HAS_MODEL_LOADED(CUTTER)
	AND HAS_MODEL_LOADED(ARMYTRAILER2)
	AND HAS_MODEL_LOADED(PACKER)
	AND HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	AND HAS_MODEL_LOADED(S_M_M_SECURITY_01)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH_PB_CREATE()
	
	ADD_RELATIONSHIP_GROUP("WorkerPedGroup", WorkerGroup)	
	ADD_RELATIONSHIP_GROUP("GuardGroup", GuardGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GuardGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, WorkerGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, GuardGroup, WorkerGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, WorkerGroup, GuardGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, GuardGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, GuardGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, WorkerGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, WorkerGroup)
	
	CLEAR_AREA(<<897.3, -1556.1, 30.4>>, 40, TRUE)
	YardWorkersArea1 = ADD_SCENARIO_BLOCKING_AREA(<<851.6, -1599.7, 27.9>>, <<961.8, -1476.4, 49.7>>)
	YardWorkersArea2 = ADD_SCENARIO_BLOCKING_AREA(<<851.2, -1598.2, 29.7>>, <<855.7, -1587.1, 33>>)
	
	//Create the trailer and the cutter 
//	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
//		CLEAR_AREA(<< -1915.7711, -635.8125, 9.4879 >>, 5, TRUE)
//		g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(CUTTER, << -1915.7711, -635.8125, 9.4879 >>, 33.6688)
//	ENDIF
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		CLEAR_AREA(<<919.5082, -1546.9348, 29.7786>>, 5, TRUE)
		g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(ARMYTRAILER2, <<919.3030, -1553.8795, 29.7789>>, 167.1917)
	ENDIF		
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
		CLEAR_AREA(<<918.2448, -1551.2578, 29.7820>>, 5, TRUE)
		g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(PACKER, <<919.3030, -1553.8795, 29.7789>>, 167.1917)
	ENDIF	
	
	//Attach the trailer to the truck and the cutter to the trailer
	IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(g_sTriggerSceneAssets.veh[2])
		ATTACH_VEHICLE_TO_TRAILER(g_sTriggerSceneAssets.veh[2], g_sTriggerSceneAssets.veh[1], 0.5)
	ENDIF
//	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(g_sTriggerSceneAssets.veh[0], g_sTriggerSceneAssets.veh[1])
//		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.veh[0], g_sTriggerSceneAssets.veh[1], 0, <<0, -1.3, -0.6>>, <<0,0,0>>, FALSE, FALSE, TRUE)	
//	ENDIF
	
//	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[4])
//		CLEAR_AREA(<<914.1436, -1578.9545, 29.6482>>, 5, TRUE)
//		g_sTriggerSceneAssets.veh[4] = CREATE_VEHICLE(BISON2, <<914.1436, -1578.9545, 29.6482>>, 2.6570)
//	ENDIF	
	
	//Create peds 
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		CLEAR_AREA(<<912.3400, -1543.2975, 29.6469>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<912.3400, -1543.2975, 29.6469>>, 16.1690)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[0], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[0], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		CLEAR_AREA(<<917.5028, -1517.4009, 29.9673>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<917.5028, -1517.4009, 29.9673>>, 158.5738)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[1], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[1], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[1], TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		CLEAR_AREA(<<869.6423, -1541.4226, 29.2516>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<869.6423, -1541.4226, 29.2516>>, 346.9848)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[2], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[2], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[2], TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
		CLEAR_AREA(<<884.3046, -1573.1875, 29.8247>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<884.3046, -1573.1875, 29.8247>>, 182.9722)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[3], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[3], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[3], TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
		CLEAR_AREA(<<903.8805, -1575.0199, 29.8327>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<903.8805, -1575.0199, 29.8327>>, 308.1952)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[4], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[4], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[4], TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[5])
		CLEAR_AREA(<<906.2186, -1575.1078, 29.8125>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<906.2186, -1575.1078, 29.8125>>, 55.9906)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[5], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[5], 30)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[5], TRUE)
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[8])
		CLEAR_AREA(<<889.2850, -1561.4847, 29.6539>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[8] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<889.2850, -1561.4847, 29.6539>>, 22.2456)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[8], workerGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[8], 20, 5, 120, -45, 45)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[8], TRUE)
	ENDIF
	//The 2 guards
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[6])
		CLEAR_AREA(<<863.1551, -1564.5721, 29.3231>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[6] = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<863.1551, -1564.5721, 29.3231>>, 130.7946)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[6], GuardGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[6], 40)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[6], TRUE)
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[7])
		CLEAR_AREA(<<940.2881, -1573.8774, 29.3866>>, 2, TRUE)
		g_sTriggerSceneAssets.ped[7] = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<940.2881, -1573.8774, 29.3866>>, 269.1856)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[7], GuardGroup)
		SET_PED_VISUAL_FIELD_PROPERTIES(g_sTriggerSceneAssets.ped[7], 40)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[7], TRUE)
	ENDIF		
	
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH_PB_RELEASE()
	INT iCount 
	FOR iCount = 0 TO 3
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[iCount])
	ENDFOR
	FOR iCount = 0 TO 8
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[iCount])
	ENDFOR
	
	REMOVE_RELATIONSHIP_GROUP(WorkerGroup)
	REMOVE_RELATIONSHIP_GROUP(GuardGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea1)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea2)
	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH_PB_DELETE()
	INT iCount 
	FOR iCount = 0 TO 3
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[iCount])
	ENDFOR
	FOR iCount = 0 TO 8
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[iCount])
	ENDFOR	
	
	IF DOES_SCENARIO_GROUP_EXIST("SCRAP_SECURITY") 
		IF IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
			SET_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY",FALSE)
		ENDIF
	ENDIF
	
	REMOVE_RELATIONSHIP_GROUP(WorkerGroup)
	REMOVE_RELATIONSHIP_GROUP(GuardGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea1)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea2)
	
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH_PB_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[2])
			vTriggerPosition = GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[2])
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			IF g_bTriggerDebugOn
				//@CV adding a larger area if the player is in a heli as requested in bug:1205511
				IF IS_PED_IN_FLYING_VEHICLE(player_ped_id())
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINALEH_PB_FLYING_TRIGGER_DIST)
				ELSE
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINALEH_PB_TRIGGER_DIST)
				ENDIF
			ENDIF
		#ENDIF
		//@CV adding a larger area if the player is in a heli as requested in bug:1205511
		IF IS_PED_IN_FLYING_VEHICLE(player_ped_id())
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition) < TS_FINALEH_PB_FLYING_TRIGGER_DIST
				REMOVE_RELATIONSHIP_GROUP(WorkerGroup)
				REMOVE_RELATIONSHIP_GROUP(GuardGroup)
				RETURN TRUE
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition) < TS_FINALEH_PB_TRIGGER_DIST
				REMOVE_RELATIONSHIP_GROUP(WorkerGroup)
				REMOVE_RELATIONSHIP_GROUP(GuardGroup)
				RETURN TRUE
			ENDIF
			
			//Start if player is approaching either of the guards
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[6])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[6])				
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[6]) < 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[7])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[7])				
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[7]) < 10
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH_PB_HAS_BEEN_DISRUPTED()

	//Don't allow mission to start if any of these vehicles are destroyed
//	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
//		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
//			MissionHasBeenDisrupted = TRUE
//		ENDIF
//	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
			MissionHasBeenDisrupted = TRUE	
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[2])
			MissionHasBeenDisrupted = TRUE
		ENDIF
	ENDIF
	
	IF MissionHasBeenDisrupted = TRUE
		KILL_ANY_CONVERSATION()
		CLEAR_PRINTS()
		PRINT_HELP("HELP_2")//~s~Mission was disrupted. Come back later to try again.	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH_PB_IS_BLOCKED()
	RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR)
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH_PB_UPDATE()

	//Control the guards	

	//Put the guards back to their posts to stand guard
	IF DOES_SCENARIO_GROUP_EXIST("SCRAP_SECURITY") 
		IF NOT IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
			SET_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY",TRUE)
		ENDIF
		IF IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[6])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[6])
					IF GET_SCRIPT_TASK_STATUS(g_sTriggerSceneAssets.ped[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
						IF DOES_SCENARIO_EXIST_IN_AREA(<<861.3, -1565, 29.4>>, 5, TRUE)
							PRINTSTRING("TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD has been given to worker ped 6") PRINTNL()
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(g_sTriggerSceneAssets.ped[6], <<861.3, -1565, 29.4>>, 5, 0)
						ELSE
							PRINTSTRING("DOES_SCENARIO_EXIST_IN_AREA = FALSE") PRINTNL()
						ENDIF
					ELSE
						PRINTSTRING("GET_SCRIPT_TASK_STATUS = PERFORMING_TASK") PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[7])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[7])	
					IF GET_SCRIPT_TASK_STATUS(g_sTriggerSceneAssets.ped[7], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
						IF DOES_SCENARIO_EXIST_IN_AREA(<<940.2881, -1573.8774, 29.3866>>, 5, TRUE)
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(g_sTriggerSceneAssets.ped[7], <<940.2881, -1573.8774, 29.3866>>, 5, 0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("IS_SCENARIO_GROUP_ENABLED = FALSE") PRINTNL()
		ENDIF	
	ELSE
		PRINTSTRING("SCRAP_SECURITY group doesn't Exist") PRINTNL()
	ENDIF

	//Give the workers their scenarios once the scenarios have loaded in
	IF WorkerScenarioTaskGiven[0] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<912.3, -1542.6, 30.4>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[0], <<912.3, -1542.6, 30.4>>, 5, 0)
					WorkerScenarioTaskGiven[0] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF WorkerScenarioTaskGiven[1] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<917.5028, -1517.4009, 29.9673>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[1], <<917.5028, -1517.4009, 29.9673>>, 5, 0)
					WorkerScenarioTaskGiven[1] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	IF WorkerScenarioTaskGiven[2] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<869.6423, -1541.4226, 29.2516>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[2], <<869.6423, -1541.4226, 29.2516>>, 5, 0)
					WorkerScenarioTaskGiven[2] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF WorkerScenarioTaskGiven[3] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<884.3046, -1573.1875, 29.8247>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[3], <<884.3046, -1573.1875, 29.8247>>, 5, 0)
					WorkerScenarioTaskGiven[3] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF WorkerScenarioTaskGiven[4] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[4])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<903.8805, -1575.0199, 29.8327>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[4], <<903.8805, -1575.0199, 29.8327>>, 5, 0)
					WorkerScenarioTaskGiven[4] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF WorkerScenarioTaskGiven[5] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[5])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[5])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<906.2186, -1575.1078, 29.8125>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[5], <<906.2186, -1575.1078, 29.8125>>, 5, 0)
					WorkerScenarioTaskGiven[5] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF WorkerScenarioTaskGiven[6] = FALSE
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[8])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[8])
				IF DOES_SCENARIO_EXIST_IN_AREA(<<889.2850, -1561.4847, 29.6539>>, 5, TRUE)
					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[8], <<889.2850, -1561.4847, 29.6539>>, 5, 0)
					WorkerScenarioTaskGiven[6] = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
		
		
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			g_sTriggerSceneAssets.veh[3] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
	ENDIF

	IF PlayersWeaponPutAwayPB = FALSE
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				PlayersWeaponPutAwayPB = TRUE
			ELSE
				PlayersWeaponPutAwayPB = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH_PB_AMBIENT_UPDATE()
		
ENDPROC
