//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Big Score Prep 2C - Trigger Scene Data					│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "friends_public.sch"


CONST_FLOAT 	TS_FINALEH_PC_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH_PC_STREAM_OUT_DIST		160.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH_PC_TRIGGER_DIST			8.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINALEH_PC_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH_PC_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

BOOL TS_FINALEH_PC_b_CanTrigger[3]
enumBankAccountName	TS_FINALEH_PC_ePlayerAccount
BOOL TS_FINALEH_PC_bNoMoneyHelp = FALSE
SP_MISSIONS eGauntletSpawned

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH_PC_RESET()
	INT i
	FOR i=0 TO 2
//		TS_FINALEH_PC_b_CanTrigger[i] = FALSE
	ENDFOR
	g_sTriggerSceneAssets.timer = -1
	TS_FINALEH_PC_bNoMoneyHelp = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH_PC_REQUEST_ASSETS()
	REQUEST_MODEL(GAUNTLET)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH_PC_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GAUNTLET)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH_PC_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(GAUNTLET)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_TRIGGER_VEHICLE_FOR_PREP(SP_MISSIONS mission)

	SWITCH mission
		CASE SP_HEIST_FINALE_PREP_C1
//			CPRINTLN(DEBUG_MISSION, "Returning vehicle 0 for ", GET_THIS_SCRIPT_NAME())
			RETURN 0
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
//			CPRINTLN(DEBUG_MISSION, "Returning vehicle 1 for ", GET_THIS_SCRIPT_NAME())
			RETURN 1
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
//			CPRINTLN(DEBUG_MISSION, "Returning vehicle 2 for ", GET_THIS_SCRIPT_NAME())
			RETURN 2
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MISSION, "Trigger invalid for Prep returning safe value")
	RETURN 0
ENDFUNC

PROC CREATE_MISSION_VEHICLE(VECTOR vCarPos, FLOAT fDir, SP_MISSIONS mission, INT iColour = 0 )
	//Clear area and create the police bike.
	CLEAR_AREA(vCarPos, 12.0, TRUE)

	WHILE NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)])
		CPRINTLN(DEBUG_MISSION, "Creating trigger vehicle " ,GET_TRIGGER_VEHICLE_FOR_PREP(mission), " for = ", GET_THIS_SCRIPT_NAME())
		g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)] = CREATE_VEHICLE(GAUNTLET, vCarPos, fDir)
		WAIT(0)
	ENDWHILE

	IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)])
		SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)], iColour)
		IF NOT DECOR_EXIST_ON(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)], "MapGauntlet")
			DECOR_SET_INT(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)], "MapGauntlet", ENUM_TO_INT(mission))
		ENDIF
		SET_VEHICLE_IGNORED_BY_QUICK_SAVE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)])
		CPRINTLN(DEBUG_MISSION, "Setting vehicle colour for = ", GET_THIS_SCRIPT_NAME())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CPRINTLN(DEBUG_MISSION, "2  ", GET_THIS_SCRIPT_NAME())
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCarPos) <= 2.5*2.5
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)])
				CPRINTLN(DEBUG_MISSION, "3 ", GET_THIS_SCRIPT_NAME())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(mission)])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, " Setting player in to gauntlet 1")
			ENDIF
		ENDIF
	ENDIF
	eGauntletSpawned = mission
	CPRINTLN(DEBUG_MISSION, "Surpressing GAUNTLET")
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
ENDPROC

PROC CREATE_MAPPED_GAUNTLET(SP_MISSIONS mission) 
	VECTOR vCreatePos
	SWITCH mission
		CASE SP_HEIST_FINALE_PREP_C1
			vCreatePos = <<-311.1739, -771.6993, 52.2467>>
			CREATE_MISSION_VEHICLE(vCreatePos, 182.1060, mission, 13)
			CPRINTLN(DEBUG_MISSION, "Created trigger vehicle for SP_HEIST_FINALE_PREP_C1 = ", GET_THIS_SCRIPT_NAME())
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			vCreatePos = <<-657.8753, -272.2921, 34.7628>>
			CREATE_MISSION_VEHICLE(vCreatePos, 30.6575, mission, 4)
			CPRINTLN(DEBUG_MISSION, "Created trigger vehicle for SP_HEIST_FINALE_PREP_C2 = ", GET_THIS_SCRIPT_NAME())
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			vCreatePos = <<307.1230, -1084.8005, 28.3597>>
			CREATE_MISSION_VEHICLE(vCreatePos, 299.7018, mission, 0)
			CPRINTLN(DEBUG_MISSION, "Created trigger vehicle for SP_HEIST_FINALE_PREP_C3 = ", GET_THIS_SCRIPT_NAME())
		BREAK
	ENDSWITCH


	INT i
	FOR i=0 TO 2
		TS_FINALEH_PC_b_CanTrigger[i] = FALSE
	ENDFOR

ENDPROC

PROC CLEAR_GAUNTLET_AT_POINT(VECTOR vPos)
	VEHICLE_INDEX veh = GET_RANDOM_VEHICLE_IN_SPHERE(vPos, 2.51, GAUNTLET, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF DOES_ENTITY_EXIST(veh)
	AND IS_VEHICLE_DRIVEABLE(veh)
		CPRINTLN(DEBUG_MISSION, "There was a gauntlet at point", vPos, " its being deleted = ", GET_THIS_SCRIPT_NAME())
		SET_ENTITY_AS_MISSION_ENTITY(veh, TRUE, TRUE)
		DELETE_VEHICLE(veh)
	ENDIF
ENDPROC

PROC CLEAR_NEARBY_GAUNTLETS()
	IF TRIGGER_SCENE_GET_SP_MISSION() = SP_HEIST_FINALE_PREP_C2
		CPRINTLN(DEBUG_MISSION, "Clearing nearby gauntlets = ", GET_THIS_SCRIPT_NAME())
		VECTOR vPos = <<-653.4072, -280.5184, 34.5603>>
		CLEAR_GAUNTLET_AT_POINT(vPos)

		vPos = <<-662.5220, -265.6498, 34.9485>>
		CLEAR_GAUNTLET_AT_POINT(vPos)
	ENDIF
ENDPROC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH_PC_CREATE()
	VEHICLE_INDEX veh = GET_PLAYERS_LAST_VEHICLE()
	CPRINTLN(DEBUG_MISSION, "TS_FINALEH_PC_CREATE")
	IF DOES_ENTITY_EXIST(veh)
	AND IS_VEHICLE_DRIVEABLE(veh)
		IF NOT DECOR_EXIST_ON(veh, "MapGauntlet")
			CPRINTLN(DEBUG_MISSION, "creating gauntlet as MapGauntlet decor doesnt exist on vehicle = ", GET_THIS_SCRIPT_NAME())
			CLEAR_NEARBY_GAUNTLETS()
			CREATE_MAPPED_GAUNTLET(TRIGGER_SCENE_GET_SP_MISSION())
		ELSE
			IF DECOR_GET_INT(veh, "MapGauntlet") != ENUM_TO_INT(TRIGGER_SCENE_GET_SP_MISSION())
				CPRINTLN(DEBUG_MISSION, "creating gauntlet as MapGauntlet decor doesnt exist on vehicle = ", GET_THIS_SCRIPT_NAME())
				CLEAR_NEARBY_GAUNTLETS()
				CREATE_MAPPED_GAUNTLET(TRIGGER_SCENE_GET_SP_MISSION())
			ELSE
				FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(veh))
				IF fDist > (TS_FINALEH_PC_STREAM_IN_DIST*TS_FINALEH_PC_STREAM_IN_DIST)
					CPRINTLN(DEBUG_MISSION, "creating gauntlet as its a map gauntlet and outside streaming radius ", GET_THIS_SCRIPT_NAME())
					CLEAR_NEARBY_GAUNTLETS()
					CREATE_MAPPED_GAUNTLET(TRIGGER_SCENE_GET_SP_MISSION())
				ELSE
					CPRINTLN(DEBUG_MISSION, "Not creating gauntlet as its a map gauntlet and too close ", GET_THIS_SCRIPT_NAME())
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, "creating gauntlet as vehicle doesnt exist = ", GET_THIS_SCRIPT_NAME())
		CLEAR_NEARBY_GAUNTLETS()
		CREATE_MAPPED_GAUNTLET(TRIGGER_SCENE_GET_SP_MISSION())
	ENDIF
ENDPROC

/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH_PC_RELEASE()
	
	IF NOT TS_FINALEH_PC_b_CanTrigger[0]
	AND NOT TS_FINALEH_PC_b_CanTrigger[1]
	AND NOT TS_FINALEH_PC_b_CanTrigger[2]
		CPRINTLN(DEBUG_MISSION, "UNsurpressing GAUNTLET 1")
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, FALSE)
	ENDIF
	INT i
	FOR i=0 TO 2
		TS_FINALEH_PC_b_CanTrigger[i] = FALSE
	ENDFOR

//	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//				CPRINTLN(DEBUG_MISSION, "NOT RELEASING ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()), " For = ", GET_THIS_SCRIPT_NAME())
//				EXIT
//			ENDIF
//		ENDIF
//	ENDIF
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
	CPRINTLN(DEBUG_MISSION, "Releaseing trigger vehicle = ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()), " For = ", GET_THIS_SCRIPT_NAME())
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH_PC_DELETE()
	IF NOT TS_FINALEH_PC_b_CanTrigger[0]
	AND NOT TS_FINALEH_PC_b_CanTrigger[1]
	AND NOT TS_FINALEH_PC_b_CanTrigger[2]
		CPRINTLN(DEBUG_MISSION, "UNsurpressing GAUNTLET 2")
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, FALSE)
	ENDIF
	INT i
	FOR i=0 TO 2
		TS_FINALEH_PC_b_CanTrigger[i] = FALSE
	ENDFOR
	
//	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
//				IF 
//				CPRINTLN(DEBUG_MISSION, "NOT RELEASING ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()), " For = ", GET_THIS_SCRIPT_NAME())
//				EXIT
//			ENDIF
//		ENDIF
//	ENDIF

	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
	CPRINTLN(DEBUG_MISSION, "Deleting trigger vehicle = ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()), " For = ", GET_THIS_SCRIPT_NAME())
ENDPROC

FUNC INT GET_REQUIRED_CASH_MOD(BOOL bCalcRepairs = FALSE)
	INT iCash
		IF bCalcRepairs
			iCash = ABSI(GET_ACCOUNT_BALANCE(TS_FINALEH_PC_ePlayerAccount) - (11000+GET_CARMOD_REPAIR_COST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])))
			CPRINTLN(DEBUG_MISSION, "Repairs calculation = ", iCash)
		ELSE
			iCash = ABSI(GET_ACCOUNT_BALANCE(TS_FINALEH_PC_ePlayerAccount) - 11000)
		ENDIF

		CPRINTLN(DEBUG_MISSION, "AMOUNT OF CASH NEEDED ====== ", iCash, " AMOUNT OF CASH OWNED ===== ", GET_ACCOUNT_BALANCE(TS_FINALEH_PC_ePlayerAccount))
	RETURN iCash
ENDFUNC


FUNC BOOL IS_REPAIR_COST_TOO_HIGH()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
		TS_FINALEH_PC_ePlayerAccount = GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(GET_PLAYER_PED_ENUM(PLAYER_PED_ID()))
		IF (GET_ACCOUNT_BALANCE(TS_FINALEH_PC_ePlayerAccount) - GET_CARMOD_REPAIR_COST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])) < 11000
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC



FUNC BOOL DOES_PLAYER_HAVE_THE_CASH()
	TS_FINALEH_PC_ePlayerAccount = GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(GET_PLAYER_PED_ENUM(PLAYER_PED_ID()))

	IF GET_ACCOUNT_BALANCE(TS_FINALEH_PC_ePlayerAccount) >= 11000
		IF NOT IS_REPAIR_COST_TOO_HIGH()
			RETURN TRUE
		ELSE
			PRINT_HELP_WITH_NUMBER("AM_H_FHPCREP", GET_REQUIRED_CASH_MOD(TRUE), DEFAULT_HELP_TEXT_TIME)//$~1~ needed to purchase the mods required, and repair the vehicle, for the heist.
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT TS_FINALEH_PC_bNoMoneyHelp
			PRINT_HELP_WITH_NUMBER("AM_H_FHPCCASH", GET_REQUIRED_CASH_MOD(), DEFAULT_HELP_TEXT_TIME)//$~1~ needed to purchase the mods required for the heist.
			TS_FINALEH_PC_bNoMoneyHelp = TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH_PC_HAS_BEEN_TRIGGERED()

	//Render trigger zone debug.
	#IF IS_DEBUG_BUILD
		VECTOR vTriggerPosition
		SWITCH TRIGGER_SCENE_GET_SP_MISSION()
			CASE SP_HEIST_FINALE_PREP_C1
				vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC1)
			BREAK
			CASE SP_HEIST_FINALE_PREP_C2
				vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC2)
			BREAK
			CASE SP_HEIST_FINALE_PREP_C3
				vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC3)
			BREAK
		ENDSWITCH	
		DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINALEH_PC_TRIGGER_DIST)
	#ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF TS_FINALEH_PC_b_CanTrigger[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())]
		AND NOT g_sVehicleGenNSData.bInGarage
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(eGauntletSpawned)])
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(eGauntletSpawned)], TRUE)
					IF DECOR_EXIST_ON(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(eGauntletSpawned)], "MapGauntlet")
						IF  eGauntletSpawned = SP_HEIST_FINALE_PREP_C1
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION,"FLOWFLAG_HEIST_FINALE_PREPC_CAR_1_STOLEN_MAP true")
							#ENDIF
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPC_CAR_1_STOLEN_MAP, TRUE)
							g_FHP3_FirstGauntWarn	= FALSE
						ELIF  eGauntletSpawned = SP_HEIST_FINALE_PREP_C2
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION,"FLOWFLAG_HEIST_FINALE_PREPC_CAR_2_STOLEN_MAP true")
							#ENDIF
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPC_CAR_2_STOLEN_MAP, TRUE)
							g_FHP3_FirstGauntWarn	= FALSE
						ELIF  eGauntletSpawned = SP_HEIST_FINALE_PREP_C3
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_MISSION,"FLOWFLAG_HEIST_FINALE_PREPC_CAR_3_STOLEN_MAP true")
							#ENDIF
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPC_CAR_3_STOLEN_MAP, TRUE)
							g_FHP3_FirstGauntWarn	= FALSE
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF		
			REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_GAUNT_R")//remove reminder for getting cars if triggered
			CPRINTLN(DEBUG_TRIGGER, "Triggering a Big score vehicle prep C mission, ID is = ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()))
			CPRINTLN(DEBUG_TRIGGER, "suppress gauntlet again = ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()))
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH_PC_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH_PC_IS_BLOCKED()
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_PLAYER_IN_RANGE_TO_SPAWN_A_GAUNTLET(SP_MISSIONS misson, BOOL bStreamIn = TRUE)
	VECTOR vTriggerPosition
	SWITCH misson
		CASE SP_HEIST_FINALE_PREP_C1
			vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC1)
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC2)
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PC3)
		BREAK
	ENDSWITCH
	
	FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)

	FLOAT Range = TS_FINALEH_PC_STREAM_IN_DIST
	
	IF NOT bStreamIn
		Range = TS_FINALEH_PC_STREAM_OUT_DIST
	ENDIF
	
	IF fDist <= (Range*Range)
		CPRINTLN(DEBUG_MISSION, "Player is in range = ", fDist, "From Mission = ", misson)
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC FLOW_FLAG_IDS GET_MISSION_SPEC_FLAG(SP_MISSIONS mission)
	SWITCH mission
		CASE SP_HEIST_FINALE_PREP_C1
			RETURN FLOWFLAG_HEIST_FINALE_PREPC_CAR_1_STOLEN_MAP
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			RETURN FLOWFLAG_HEIST_FINALE_PREPC_CAR_2_STOLEN_MAP
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			RETURN FLOWFLAG_HEIST_FINALE_PREPC_CAR_3_STOLEN_MAP
		BREAK
	ENDSWITCH
	
	RETURN FLOWFLAG_NONE
ENDFUNC

PROC MONITOR_SPAWNING_NON_COLLECTED_CAR()
	IF NOT g_sTriggerSceneAssets.flag
		INT mission
		
		FOR mission = ENUM_TO_INT(SP_HEIST_FINALE_PREP_C1) TO  ENUM_TO_INT(SP_HEIST_FINALE_PREP_C3)
			
			IF NOT GET_MISSION_FLOW_FLAG_STATE(GET_MISSION_SPEC_FLAG( INT_TO_ENUM(SP_MISSIONS, mission)))
			AND GET_MISSION_COMPLETE_STATE(INT_TO_ENUM(SP_MISSIONS, mission))
				IF IS_PLAYER_IN_RANGE_TO_SPAWN_A_GAUNTLET(INT_TO_ENUM(SP_MISSIONS, mission))
					TS_FINALEH_PC_REQUEST_ASSETS()
					IF TS_FINALEH_PC_HAVE_ASSETS_LOADED()
						CREATE_MAPPED_GAUNTLET(INT_TO_ENUM(SP_MISSIONS, mission))
						eGauntletSpawned = INT_TO_ENUM(SP_MISSIONS, mission)
						g_sTriggerSceneAssets.flag = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF NOT IS_PLAYER_IN_RANGE_TO_SPAWN_A_GAUNTLET(eGauntletSpawned, FALSE)
			TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(eGauntletSpawned)])
			TS_FINALEH_PC_RELEASE_ASSETS()
			CPRINTLN(DEBUG_MISSION, "Clearing suppressed gauntlet 1 = ", GET_THIS_SCRIPT_NAME())
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, FALSE)
			g_sTriggerSceneAssets.flag = FALSE
		ENDIF
	ENDIF
ENDPROC



PROC MONITOR_PLAYER_STILL_IN_GAUNTLET_OUTSIDE_OF_TRIGGER()
		INT i
		FOR i=0 TO 2
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
			AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[i]) //is scene spawned
				CPRINTLN(DEBUG_MISSION, "Trigger guntlet still alive = ", i)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[i])
					CPRINTLN(DEBUG_MISSION, "Trigger guntlet still alive player not in it  = ", i)
					
					FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[i]))
					IF fDist > (200*200)
						CPRINTLN(DEBUG_MISSION, "Trigger guntlet still alive player not in it far away DELETE IT NOW = ", i)
						TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
ENDPROC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH_PC_UPDATE()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT g_sVehicleGenNSData.bInGarage
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//if we havent already shown the money help do the checks
				//if the player is in the right car but doesnt have the cash 
				//we check once display the text then dont display the text again till the player enters a new car or reenters this one
				IF NOT TS_FINALEH_PC_bNoMoneyHelp
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
					AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())]) //is scene spawned
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())])
							IF DOES_PLAYER_HAVE_THE_CASH()
								CPRINTLN(DEBUG_MISSION, "Player is in a trigger scene guntlet launch mission, gauntlet = ", GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION()))
								TS_FINALEH_PC_bNoMoneyHelp = TRUE //expire money help
								//we dont need to care about which mission is about to trigger as the scene is created 
								TS_FINALEH_PC_b_CanTrigger[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())] = TRUE
								EXIT
							ELSE
								TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
							ENDIF
						ELSE
							TS_FINALEH_PC_bNoMoneyHelp = FALSE
						ENDIF
					ENDIF

					VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(veh)
					AND IS_VEHICLE_DRIVEABLE(veh)
						IF IS_VEHICLE_MODEL(veh, GAUNTLET)
//							IF NOT IS_VEHICLE_PLAYER_BOUGHT(veh)
								IF DOES_PLAYER_HAVE_THE_CASH()
									CPRINTLN(DEBUG_MISSION, "Player is in a guntlet near and a trigger scene is created launch mission")
									TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
									//we dont need to care about which mission is about to trigger as the scene is created 
									TS_FINALEH_PC_b_CanTrigger[GET_TRIGGER_VEHICLE_FOR_PREP(TRIGGER_SCENE_GET_SP_MISSION())] = TRUE
								ELSE
									TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
								ENDIF
//							ELSE
////								TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
					//Reset money help if the player isn't in a car
					TS_FINALEH_PC_bNoMoneyHelp = FALSE
					
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    If the player goes near the garage while off mission we want the 
///    doors to remain closed
///    can run in the ambient update as they will be no where near a trigger scene by these doors
PROC MANAGE_GARAGE_DOORS()
	INT iDoor
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-442.3102, -2178.7183, 9.3993>>) > 100*100
		iDoor = HASH("LeftLockup")
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
			CPRINTLN(DEBUG_MISSION, "Trigger, Removing door Left with hash == ", iDoor)
			REMOVE_DOOR_FROM_SYSTEM(iDoor)
		ENDIF

		iDoor = HASH("RightLockup")
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
			CPRINTLN(DEBUG_MISSION, "Trigger, Removing door RIGHT with hash == ", iDoor)
			REMOVE_DOOR_FROM_SYSTEM(iDoor)
		ENDIF
	ELSE
		VECTOR vGaragePosA = <<-442.56024, -2184.60547, 14.55648>> 
		VECTOR vGaragePosB = <<-442.66803, -2172.49561, 9.39933>>
		FLOAT fGarage = 13.47
		iDoor = HASH("LeftLockup")
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
			CPRINTLN(DEBUG_MISSION, "Trigger, Adding door Left with hash == ", iDoor)
			ADD_DOOR_TO_SYSTEM(iDoor, PROP_COM_GAR_DOOR_01, <<-440.0606, -2171.8267, 11.3672>>)
		ELSE
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGaragePosA, vGaragePosB, fGarage)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoor, DOORSTATE_LOCKED, TRUE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(iDoor, DOORSTATE_UNLOCKED, TRUE, TRUE)
			ENDIF
		ENDIF

		iDoor = HASH("RightLockup")
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoor)
			CPRINTLN(DEBUG_MISSION, "Trigger, Adding door RIGHT with hash == ", iDoor)
			ADD_DOOR_TO_SYSTEM(iDoor, PROP_COM_GAR_DOOR_01, <<-445.3054, -2171.8267, 11.3672>>)
		ELSE
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGaragePosA, vGaragePosB, fGarage)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoor, DOORSTATE_LOCKED, TRUE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(iDoor, DOORSTATE_UNLOCKED, TRUE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH_PC_AMBIENT_UPDATE()
	//Gaunlet reminder
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPC_EMAIL_DONE)
	AND g_OnMissionState = MISSION_TYPE_OFF_MISSION	
		IF g_FHPC_tod_NextRemindTime  = INVALID_TIMEOFDAY
			g_FHPC_tod_NextRemindTime = GET_CURRENT_TIMEOFDAY()	
			if g_FHP3_FirstGauntWarn				
				ADD_TIME_TO_TIMEOFDAY(g_FHPC_tod_NextRemindTime,0,30,2)				
			else	
				ADD_TIME_TO_TIMEOFDAY(g_FHPC_tod_NextRemindTime,0,30,7)
			endif	
		ELSE
			IF IS_NOW_AFTER_TIMEOFDAY(g_FHPC_tod_NextRemindTime)
				ADD_HELP_TO_FLOW_QUEUE("AM_H_GAUNT_R")	
				g_FHPC_tod_NextRemindTime = INVALID_TIMEOFDAY	
				g_FHP3_FirstGauntWarn	= FALSE
			ENDIF 
		ENDIF		
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT g_sVehicleGenNSData.bInGarage
//			MONITOR_PLAYER_STILL_IN_GAUNTLET_OUTSIDE_OF_TRIGGER()

			MANAGE_GARAGE_DOORS()
			//No trigger scene is created so wait for the player to be in a vehicle 
			IF NOT IS_MISSION_LEADIN_ACTIVE()
				IF g_eMissionRunningOnMPSwitchStart != SP_HEIST_FINALE_PREP_C1
				AND g_eMissionRunningOnMPSwitchStart != SP_HEIST_FINALE_PREP_C2
				AND g_eMissionRunningOnMPSwitchStart != SP_HEIST_FINALE_PREP_C3
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

						IF NOT TS_FINALEH_PC_bNoMoneyHelp
							
							VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF DOES_ENTITY_EXIST(veh)
							AND IS_VEHICLE_DRIVEABLE(veh)
								
								//If player is in a gauntlet we run through all the checks 
								IF IS_VEHICLE_MODEL(veh, GAUNTLET)
	//								IF NOT IS_VEHICLE_PLAYER_BOUGHT(veh)
										IF DOES_PLAYER_HAVE_THE_CASH()
											SWITCH TRIGGER_SCENE_GET_SP_MISSION()
												CASE SP_HEIST_FINALE_PREP_C1
													IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C1)
														CPRINTLN(DEBUG_MISSION, "TRIGGERED SP_HEIST_FINALE_PREP_C1")
														TS_FINALEH_PC_b_CanTrigger[0] = TRUE
													ELSE
														CPRINTLN(DEBUG_MISSION, " NOT TRIGGERING SP_HEIST_FINALE_PREP_C1")
													ENDIF
												BREAK
												CASE SP_HEIST_FINALE_PREP_C2
													IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C1)
													AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C2)
														CPRINTLN(DEBUG_MISSION, "TRIGGERED SP_HEIST_FINALE_PREP_C2")
														TS_FINALEH_PC_b_CanTrigger[1] = TRUE
													ELSE
														CPRINTLN(DEBUG_MISSION, " NOT TRIGGERING SP_HEIST_FINALE_PREP_C2")
													ENDIF
												BREAK
												CASE SP_HEIST_FINALE_PREP_C3
													IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C1)
													AND GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C2)
													AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_C3)
														CPRINTLN(DEBUG_MISSION, "TRIGGERED SP_HEIST_FINALE_PREP_C3")
														TS_FINALEH_PC_b_CanTrigger[2] = TRUE
													ELSE
														CPRINTLN(DEBUG_MISSION, " NOT TRIGGERING SP_HEIST_FINALE_PREP_C3")
													ENDIF
												BREAK
											ENDSWITCH
											CPRINTLN(DEBUG_MISSION, "Player is in a guntlet and a trigger scene is NOT created - launch mission")
											//TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
										ELSE
											TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
										ENDIF
	//								ELSE
	//									TS_FINALEH_PC_bNoMoneyHelp = TRUE//expire money help
	//								ENDIF
								ENDIF
							ENDIF

						ENDIF
					ELSE
						IF TS_FINALEH_PC_bNoMoneyHelp
							TS_FINALEH_PC_bNoMoneyHelp = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF




			IF g_sTriggerSceneAssets.timer != -1
				IF (GET_GAME_TIMER() - g_sTriggerSceneAssets.timer) > 30
					MONITOR_SPAWNING_NON_COLLECTED_CAR()
					g_sTriggerSceneAssets.timer = -1
				ENDIF
			ELSE
				g_sTriggerSceneAssets.timer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
ENDPROC
