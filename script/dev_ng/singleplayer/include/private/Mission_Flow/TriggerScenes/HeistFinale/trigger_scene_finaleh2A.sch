//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Finale Heist 2A - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FINALEH2A_STREAM_IN_DIST		40.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH2A_STREAM_OUT_DIST	55.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH2A_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINALEH2A_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH2A_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH2A_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH2A_REQUEST_ASSETS()
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, TRUE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH2A_RELEASE_ASSETS()
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, FALSE)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH2A_HAVE_ASSETS_LOADED()
	IF HAS_HEIST_BOARD_LOADED(HEIST_FINALE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH2A_CREATE()

	//Create planning board.
	CREATE_PINNED_HEIST_BOARD(HEIST_FINALE)
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_FINALE_2A,
												"BS_2A_MCS_10",
												CS_2|CS_3|CS_4|CS_5,
												CS_3|CS_4|CS_5,
												CS_ALL)
					
	//Make sure the correct hacker shows up in the into cutscne.
	//These routines mirror the ones found in SET_HEIST_CREW_MEMBER_OUTFIT_EXTRA_PARAMS
	//but call the flow versions of the cutscene commands.
	CrewMember eHacker = GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 4)
	CrewMember eGunman = GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_HEAD, g_sCrewMemberStaticData[eHacker].headVariation, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_HAIR, g_sCrewMemberStaticData[eHacker].defaultOutfitVariation, 0)
	IF eHacker = CM_HACKER_G_PAIGE
	OR eHacker = CM_HACKER_M_CHRIS
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_HAND, 0, 0)
	ELSE
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_HAND, 2, 0)
	ENDIF	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_TORSO, g_sCrewMemberStaticData[eHacker].defaultOutfitVariation, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_LEG, g_sCrewMemberStaticData[eHacker].defaultOutfitVariation, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "HACKER_SELECTION", PED_COMP_SPECIAL, 0, 0)
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_FINALE_2A, "gunman_selection_1", PED_COMP_HEAD, g_sCrewMemberStaticData[eGunman].headVariation, 0)
										
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_LOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player disrupts a scene before triggering it.
PROC TS_FINALEH2A_RELEASE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH2A_DELETE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH2A_HAS_BEEN_TRIGGERED()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		
		//Trigger the mission as soon as the player enters the Stripclub interior.
		RETURN (GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<120.7764, -1292.8864, 28.2815>>, "v_strip3"))

	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH2A_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH2A_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH2A_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH2A_AMBIENT_UPDATE()
ENDPROC
