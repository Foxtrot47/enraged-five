//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Big Score Prep 2D - Trigger Scene Data					│
//│																				│
//│								Date: 27/04/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "friends_public.sch"


CONST_FLOAT 	TS_FINALEH_PD_STREAM_IN_DIST			300.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH_PD_STREAM_OUT_DIST			320.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH_PD_TRIGGER_DIST				250.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_FINALEH_PD_BATTLE_BUDDY_CALL_DIST	500.0 	// Distance from trigger's blip at which the player can call in battle buddies. - Could reduce to 150m if needed?

CONST_FLOAT		TS_FINALEH_PD_FRIEND_REJECT_DIST		225.00	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH_PD_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH_PD_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH_PD_REQUEST_ASSETS()

	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)

ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH_PD_RELEASE_ASSETS()

	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)

ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH_PD_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH_PD_CREATE()

	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<2595.1243, 2807.5581, 30.7274>>, <<2649.7778, 2988.8291, 53.4642>>)
	SET_PED_NON_CREATION_AREA(<<2595.1243, 2807.5581, 30.7274>>, <<2649.7778, 2988.8291, 53.4642>>)
	
	CLEAR_AREA_OF_PEDS(<<2627.9658, 2941.1003, 39.4282>>, 30.0)

	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<2628.9084, 2947.6255, 40.4280>>, 338.9562)//<<2628.9084, 2947.6255, 39.4280>>, 347.0879)
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<2632.8145, 2933.4819, 43.7436>>, 53.5715)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH_PD_RELEASE()

	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])

ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH_PD_DELETE()

	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH_PD_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PD)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_BIT_SET(g_sMissionStaticData[SP_HEIST_FINALE_PREP_D].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Render trigger zone debug.
			#IF IS_DEBUG_BUILD
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINALEH_PD_TRIGGER_DIST)
			#ENDIF
			
			FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
			IF fDistanceSquaredFromTrigger < (TS_FINALEH_PD_TRIGGER_DIST*TS_FINALEH_PD_TRIGGER_DIST)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH_PD_HAS_BEEN_DISRUPTED()

	IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
		IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), 50.0)
		OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[1]), 50.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), 50.0)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[1]), 50.0)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH_PD_IS_BLOCKED()
	RETURN GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR)
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH_PD_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH_PD_AMBIENT_UPDATE()
ENDPROC
