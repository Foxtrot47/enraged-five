//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Finale Heist Prep A - Trigger Scene Data					│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//│					g_sTriggerSceneAssets.veh[0] - Police Car					│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FINALEH_PA_STREAM_IN_DIST			140.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH_PA_STREAM_OUT_DIST			150.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH_PA_TRIGGER_DIST				90.0 	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_FINALEH_PA_BATTLE_BUDDY_CALL_DIST	500.0 	// Distance from trigger's blip at which the player can call in battle buddies. - Could reduce to 150m if needed?

CONST_FLOAT		TS_FINALEH_PA_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH_PA_FRIEND_ACCEPT_BITS	BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

INT	iSideGate

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH_PA_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH_PA_REQUEST_ASSETS()
	REQUEST_MODEL(POLICE3)
	REQUEST_MODEL(POLICET)
	REQUEST_MODEL(S_M_Y_COP_01)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH_PA_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(POLICE3)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(POLICET)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH_PA_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(POLICE3)
	AND HAS_MODEL_LOADED(S_M_Y_COP_01)
	AND HAS_MODEL_LOADED(POLICET)
	AND PREPARE_MUSIC_EVENT("FHPRA_START")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC POPULATE_GATES()
	iSideGate 	= HASH("SIDE_GATE")
	ADD_DOOR_TO_SYSTEM(iSideGate, PROP_FNCLINK_03GATE5, <<391.86, -1636.07, 29.97>>)
	DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
ENDPROC

PROC SET_COP_VARIATION()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
		
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)	
	ENDIF
ENDPROC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH_PA_CREATE()
	POPULATE_GATES()

	//Clear area and create the police bike.
	VECTOR vCarPos = <<372.9868, -1623.5313, 28.2928>> 
	CLEAR_AREA(vCarPos, 8.0, TRUE)

	WHILE NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(POLICET, vCarPos, 321.7075)
		WAIT(0)
	ENDWHILE

	CLEAR_AREA(<<351.0698, -1594.3324, 28.2928>>, 10.0, TRUE)
	CLEAR_AREA(<<368.3560, -1609.6256, 28.2928>>, 10.0, TRUE)
	
	VECTOR vCopPos = <<353.1191, -1589.6061, 28.2928>>

	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<402.9022, -1633.6042, 25.0472>>, <<315.6288, -1558.8143, 38.7931>>)
	
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, vCopPos, 270.0968)
	TASK_START_SCENARIO_AT_POSITION(g_sTriggerSceneAssets.ped[0], "WORLD_HUMAN_HANG_OUT_STREET", vCopPos, 270.0968)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], FALSE)
	SET_PED_NAME_DEBUG(g_sTriggerSceneAssets.ped[0], "COP 1")
	SET_PED_TARGET_LOSS_RESPONSE(g_sTriggerSceneAssets.ped[0], TLR_SEARCH_FOR_TARGET)

	vCopPos = <<354.5570, -1590.5488, 28.2928>>
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, vCopPos, 16.3702)
	TASK_START_SCENARIO_AT_POSITION(g_sTriggerSceneAssets.ped[1], "WORLD_HUMAN_STAND_MOBILE", vCopPos, 3.8862)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[1], FALSE)
	SET_PED_NAME_DEBUG(g_sTriggerSceneAssets.ped[1], "COP 2")
	SET_PED_TARGET_LOSS_RESPONSE(g_sTriggerSceneAssets.ped[1], TLR_SEARCH_FOR_TARGET)

	vCopPos = <<369.8186, -1611.0293, 28.2928>>
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01, vCopPos, 9.0837)
	TASK_USE_MOBILE_PHONE(g_sTriggerSceneAssets.ped[2], TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[2], FALSE)
	SET_PED_NAME_DEBUG(g_sTriggerSceneAssets.ped[2], "COP 3")
	SET_PED_TARGET_LOSS_RESPONSE(g_sTriggerSceneAssets.ped[2], TLR_SEARCH_FOR_TARGET)
	SET_COP_VARIATION()
	
	vCarPos = <<400.5771, -1618.9274, 28.2928>>
	CLEAR_AREA(vCarPos, 6.0, TRUE)
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(POLICE3, vCarPos, 48.8115)
	
	vCarPos = <<396.6843, -1623.2220, 28.2928>>
	CLEAR_AREA(vCarPos, 6.0, TRUE)
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(POLICE3, vCarPos, 231.3392)

	vCarPos = <<394.3527, -1625.3374, 28.2928>>
	CLEAR_AREA(vCarPos, 6.0, TRUE)
	g_sTriggerSceneAssets.veh[3] = CREATE_VEHICLE(POLICE3, vCarPos, 49.3732)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH_PA_RELEASE()
	INT i 
	FOR i=0 TO 3
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	ENDFOR
	FOR i=0 TO 3
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH_PA_DELETE()
	INT i 
	FOR i=0 TO 3
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	ENDFOR
	FOR i=0 TO 3
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH_PA_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK_PA)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<331.3899, -1569.8737, 60.000>>, <<434.0138, -1659.1022, 28.1882>>, TS_FINALEH_PA_TRIGGER_DIST)
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
				CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH_PA_HAS_BEEN_DISRUPTED()
	//Check for scene vehicles being damaged or destroyed.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH_PA_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_FINALEH_PA_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH_PA_AMBIENT_UPDATE()
ENDPROC
