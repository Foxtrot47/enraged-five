//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Finale Heist 2B - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FINALEH2B_STREAM_IN_DIST		40.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH2B_STREAM_OUT_DIST	55.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH2B_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINALEH2B_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH2B_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINALEH2B_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH2B_REQUEST_ASSETS()
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, TRUE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH2B_RELEASE_ASSETS()
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_FINALE, FALSE)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH2B_HAVE_ASSETS_LOADED()
	IF HAS_HEIST_BOARD_LOADED(HEIST_FINALE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH2B_CREATE()

	//Create planning board.
	CREATE_PINNED_HEIST_BOARD(HEIST_FINALE)

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_FINALE_2B,
												"BS_2B_MCS_3",
												ENUM_TO_INT(CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4),
												ENUM_TO_INT(CS_SECTION_3 | CS_SECTION_4),
												ENUM_TO_INT(CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4))
	
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_LOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH2B_RELEASE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH2B_DELETE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_STRIPCLUB_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH2B_HAS_BEEN_TRIGGERED()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		enumCharacterList ePlayerChar = GET_CURRENT_PLAYER_PED_ENUM()
		
		//When out of hours trigger the mission as soon as the player enters the Stripclub interior.
		BOOL bInStripclub = FALSE
		INTERIOR_INSTANCE_INDEX interiorStripClub = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<120.7764, -1292.8864, 28.2815>>, "v_strip3")
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorStripClub
			bInStripclub = TRUE
		ENDIF
		
		IF IS_HOUR_BETWEEN_THESE_HOURS(GET_CLOCK_HOURS(), g_sMissionStaticData[SP_HEIST_FINALE_2B].startHour, g_sMissionStaticData[SP_HEIST_FINALE_2B].endHour)
			
			//Remember that we are in the stripclub while in valid hours. This is
			//so if we suddenly trigger the mission due to going out of hours we know
			//not to start the timelapse with a pan up from the door.
			IF bInStripclub
				IF NOT g_FH2_bTimelapseTriggeredInside
					CPRINTLN(DEBUG_TRIGGER, "Flagged Finale Heist 2B trigger as being in stripclub while in valid hours.")
					g_FH2_bTimelapseTriggeredInside = TRUE
				ENDIF
			ENDIF
			
			IF IS_PLAYER_PED_PLAYABLE(ePlayerChar)
				SWITCH ePlayerChar
					CASE CHAR_MICHAEL
					CASE CHAR_FRANKLIN
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<113.952866,-1293.915405,28.270266>>, <<115.790627,-1297.233521,31.519138>>, 2.750000)
							RETURN TRUE
						ENDIF
					BREAK
					CASE CHAR_TREVOR
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<117.460655,-1293.652466,28.282537>>, <<118.776756,-1295.871094,31.519581>>, 2.750000)
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			//When out of hours trigger the mission as soon as the player enters the Stripclub interior.
			IF bInStripclub
				RETURN TRUE
			ELIF g_FH2_bTimelapseTriggeredInside
				CPRINTLN(DEBUG_TRIGGER, "Unflagged Finale Heist 2B trigger as being in stripclub while in valid hours.")
				g_FH2_bTimelapseTriggeredInside = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH2B_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH2B_IS_BLOCKED()
	RETURN NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_DONE)
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH2B_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH2B_AMBIENT_UPDATE()
ENDPROC
