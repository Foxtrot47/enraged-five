//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Finale Heist 1 - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│		ped[0] = trev		ped[1] = mike										│	
//│			 																	│	
//│																				│	
//│																				│
//│		obj[0] = bottle 	 obj[1] =fridgedoor		obj[2] = fridge				│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"

CONST_FLOAT 	TS_FINALEH1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINALEH1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINALEH1_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINALEH1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINALEH1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

int 			OfficeScene
int 			OfficeSceneTrev
int				iLeadStage
bool 			bleadinStart = false

vector			vOfficeScenePos 	= << 95.840, -1291.440, 28.275>>
vector			vOfficeSceneRot		= << 0.000, 0.000, 30.0>>

bool			bBottleAttached = false

VECTOR 		vFH1HintCamCoord 	= <<92.83592, -1291.29590, 29.36181>>
FLOAT 		fFH1HintCamFollow 	= 0.35
FLOAT 		fFH1HintCamOffset 	= 0
FLOAT 		fFH1HintCamFove 	= 40.00

OBJECT_INDEX triggerObj_fridgeDoor
OBJECT_INDEX triggerObj_fridge

//dialogue
structPedsForConversation 	convo_struct
bool btriggeredleadin		= false

PROC TS_FINALEH1_RESET()
	iLeadStage 			= 0
	bBottleAttached 	= false
	bleadinStart 		= false
 	btriggeredleadin	= false
	vOfficeScenePos 	= << 95.840, -1291.440, 28.275>>
	vOfficeSceneRot		= << -0.000, 0.000, 30>>
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINALEH1_REQUEST_ASSETS()
	REQUEST_MODEL(PROP_CS_BEER_BOT_01)
	REQUEST_MODEL(PROP_CS_FRIDGE_DOOR)
	REQUEST_MODEL(PROP_CS_FRIDGE)
	REQUEST_MODEL(player_zero)
	REQUEST_MODEL(player_two)
	REQUEST_ANIM_DICT("missbigscore1leadinoutbs_1_int")
	
	//Put a tiny streaming volume on the fridge door to guarantee we can grab it from the map.
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Starting NEW_LOAD_SCENE as we request assets.")
			NEW_LOAD_SCENE_START_SPHERE(<<93.3024, -1290.9473, 29.2074>>, 0.5, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINALEH1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_BEER_BOT_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_FRIDGE_DOOR)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_FRIDGE)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
	REMOVE_ANIM_DICT("missbigscore1leadinoutbs_1_int")
	
	//Clear the streaming volume on the fridge door.
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Stopping NEW_LOAD_SCENE as we release assets.")
		NEW_LOAD_SCENE_STOP()
	ENDIF
ENDPROC
/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINALEH1_HAVE_ASSETS_LOADED()
	
	IF HAS_MODEL_LOADED(PROP_CS_BEER_BOT_01)
	AND HAS_MODEL_LOADED(PROP_CS_FRIDGE_DOOR)
	AND HAS_MODEL_LOADED(PROP_CS_FRIDGE)	
	AND HAS_MODEL_LOADED(PLAYER_ZERO)
	AND HAS_MODEL_LOADED(player_two)
	AND HAS_ANIM_DICT_LOADED("missbigscore1leadinoutbs_1_int")
	AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	
		//Check our load scene is finished.
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			IF NOT IS_NEW_LOAD_SCENE_LOADED()
				RETURN FALSE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Starting NEW_LOAD_SCENE as we request assets.")
			NEW_LOAD_SCENE_START_SPHERE(<<93.3024, -1290.9473, 29.2074>>, 0.5, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINALEH1_CREATE()

	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK), 2.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
		
	//--------------LOOPs--------------
		
	
	if GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		//create mike	
//		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1],CHAR_MICHAEL,<<94.71292, -1290.84094, 28.26876>>, DEFAULT, DEFAULT, TRUE)
//		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
//		SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[1],FALSE)
	//mike walks in to see trev	
	elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL		
		//create trev
		while not CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0],CHAR_TREVOR,vOfficeScenePos, DEFAULT, DEFAULT, TRUE)
			wait(0)
		endwhile
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		//objects
		g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_BEER_BOT_01,vOfficeScenePos)
		bBottleAttached = false
		triggerObj_fridgeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<93.2458,-1291.0306,29.1533>>,5,PROP_CS_FRIDGE_DOOR)
		triggerObj_fridge = GET_CLOSEST_OBJECT_OF_TYPE(<<92.7504,-1290.9009,29.1933>>,5,PROP_CS_FRIDGE)
		
		//Clear the streaming volume on the fridge door.
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Stopping NEW_LOAD_SCENE after grabbing fridge objects.")
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//syn scene
		OfficeScene = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],OfficeScene,"missbigscore1leadinoutbs_1_int","leadin_loop_b_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(OfficeScene,true)
	//frank walks in to see mike and trev
	elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		
		//create trev
		while not CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0],CHAR_TREVOR,vOfficeScenePos, DEFAULT, DEFAULT, TRUE)
			wait(0)
		endwhile
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		
		//create mike			
		while not CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1],CHAR_MICHAEL,<<94.71292, -1290.84094, 28.26876>>, DEFAULT, DEFAULT, TRUE)
			wait(0)
		endwhile
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[1],27.17)		
		
		//objects
		g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_BEER_BOT_01,vOfficeScenePos)
		bBottleAttached = false
		triggerObj_fridgeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<93.2458,-1291.0306,29.1533>>,5,PROP_CS_FRIDGE_DOOR)
		triggerObj_fridge = GET_CLOSEST_OBJECT_OF_TYPE(<<92.7504,-1290.9009,29.1933>>,5,PROP_CS_FRIDGE)
		
		//Clear the streaming volume on the fridge door.
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Stopping NEW_LOAD_SCENE after grabbing fridge objects.")
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		OfficeScene = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],OfficeScene,"missbigscore1leadinoutbs_1_int","leadin_loop_c_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(OfficeScene,true)
		
		
	endif
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_FINALE_1,
												"BS_1_INT",
												CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
												CS_4|CS_5|CS_6|CS_7|CS_8,
												CS_ALL)
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Trevor",PLAYER_PED_ID())
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Trevor",g_sTriggerSceneAssets.ped[0])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Michael",PLAYER_PED_ID())
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Trevor",g_sTriggerSceneAssets.ped[0])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Michael",g_sTriggerSceneAssets.ped[1])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_FINALE_1,"Franklin",PLAYER_PED_ID())
	ENDIF										
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINALEH1_RELEASE()	

	//Clear the streaming volume on the fridge door.
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Stopping NEW_LOAD_SCENE as the scene is released.")
		NEW_LOAD_SCENE_STOP()
	ENDIF

	int i
	for i = 0 to 6 
		TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[i])		
	endfor
	
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(triggerObj_fridgeDoor)
	TRIGGER_SCENE_RELEASE_OBJECT(triggerObj_fridge)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINALEH1_DELETE()

	//Clear the streaming volume on the fridge door.
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_FINALE_1> Stopping NEW_LOAD_SCENE as the scene is deleted.")
		NEW_LOAD_SCENE_STOP()
	ENDIF

	int i
	for i = 0 to 6 
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	endfor
	
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(triggerObj_fridgeDoor)
	TRIGGER_SCENE_RELEASE_OBJECT(triggerObj_fridge)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)	
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINALEH1_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_BANK, GET_CURRENT_PLAYER_PED_INT())
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iLeadStage = 2			
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			IF GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_TREVOR
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)		
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(true)
				SET_PLAYER_CONTROL(player_id(),false)
				TASK_LOOK_AT_COORD(player_ped_id(),vFH1HintCamCoord,-1,SLF_WHILE_NOT_IN_FOV)
			endif
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINALEH1_HAS_BEEN_DISRUPTED()
	int i
	for i = 0 to 6
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[i], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	endfor
	if bleadinStart
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			return true
		endif		
	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINALEH1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINALEH1_UPDATE()
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_TREVOR
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<92.584557,-1293.605225,28.075377>>, <<98.855873,-1289.740967,30.268753>>, 6.500000)
			SET_GAMEPLAY_COORD_HINT(vFH1HintCamCoord,-1,2000)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fFH1HintCamFollow)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fFH1HintCamOffset)
			SET_GAMEPLAY_HINT_FOV(fFH1HintCamFove)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
			if not IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(),vFH1HintCamCoord,30)
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(),vFH1HintCamCoord,-1,SLF_WHILE_NOT_IN_FOV)
			endif
		ELSE
			STOP_GAMEPLAY_HINT()
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
		ENDIF
	ENDIF
	
	SET_DOOR_STATE(DOORNAME_STRIPCLUB_R,DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	
	if not bleadinStart
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<127.79243, -1290.52930, 31.48345>>,<<126.72432, -1288.84412, 28.28557>>,4.5) 
			bleadinstart = true
		endif		
	else
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<129.19241, -1299.39246, 28.23270>>,<<127.47752, -1296.46045, 31.70886>>,4) 
			bleadinstart = FALSE
		endif
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(true)				
	endif
	
	switch iLeadStage
		case 0 // lead in loops
							
			if GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR			
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<93.70387, -1289.81226, 31.11002>>,<<94.82489, -1291.76880, 28.26875>>,2.67) // lock area
					ADD_PED_FOR_DIALOGUE(convo_struct,0,g_sTriggerSceneAssets.ped[0],"TREVOR")
					//ADD_PED_FOR_DIALOGUE(convo_struct,1,g_sTriggerSceneAssets.ped[1],"MICHAEL")
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_MULTIHEAD_SAFE(TRUE)	//Call the blinders in early
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)	
					iLeadStage = 2
				endif	
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<100.15879, -1294.27966, 28.26663>>,<<98.84040, -1291.97729, 31.24332>>,1.75) // lock area
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_MULTIHEAD_SAFE(TRUE)	//Call the blinders in early
					ADD_PED_FOR_DIALOGUE(convo_struct,0,g_sTriggerSceneAssets.ped[0],"TREVOR")
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)	
					OfficeScene = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],OfficeScene,"missbigscore1leadinoutbs_1_int","leadin_action_a_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(triggerObj_fridgeDoor,OfficeScene,"leadin_action_a_fridge_door","missbigscore1leadinoutbs_1_int",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(triggerObj_fridge,OfficeScene,"leadin_action_a_fridge","missbigscore1leadinoutbs_1_int",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(OfficeScene,FALSE)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),g_sTriggerSceneAssets.ped[0],-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					iLeadStage++
				endif	
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<100.15879, -1294.27966, 28.26663>>,<<98.84040, -1291.97729, 31.24332>>,1.75) // lock area
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_MULTIHEAD_SAFE(TRUE)	//Call the blinders in early
					ADD_PED_FOR_DIALOGUE(convo_struct,0,g_sTriggerSceneAssets.ped[0],"TREVOR")
					ADD_PED_FOR_DIALOGUE(convo_struct,1,g_sTriggerSceneAssets.ped[1],"MICHAEL")
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)	
					OfficeScene = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
					OfficeSceneTrev = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
					//TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],OfficeSceneTrev,"missbigscore1leadinoutbs_1_int","leadin_action_b_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],OfficeScene,"missbigscore1leadinoutbs_1_int","leadin_action_c_michael",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(OfficeScene,FALSE)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),g_sTriggerSceneAssets.ped[0],-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT | SLF_WIDEST_PITCH_LIMIT)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					iLeadStage++
				endif	
			endif			
		break
		case 1 //trigger check
			
			if GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<96.83046, -1290.32776, 28.26876>>,<<98.01225, -1292.35120, 31.24332>>,3.5) // disable
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
					SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_STILL)
				endif
				if not btriggeredleadin	
					ADD_PED_FOR_DIALOGUE(convo_struct,0,g_sTriggerSceneAssets.ped[0],"TREVOR")
					if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_INTLI3",CONV_PRIORITY_MEDIUM)
						btriggeredleadin = true
					endif
				endif
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<91.93958, -1292.48755, 30.86913>>,<<98.35245, -1288.74597, 28.38858>>,2.8) // disable
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
					SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_STILL)
				endif
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<99.386589,-1290.619873,27.882570>>, <<96.550072,-1292.252563,31.268753>>, 1.750000)
					//OR IS_ENTITY_ON_SCREEN(g_sTriggerSceneAssets.ped[0])
						if not btriggeredleadin	
							ADD_PED_FOR_DIALOGUE(convo_struct,0,g_sTriggerSceneAssets.ped[0],"TREVOR")
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_INTLI4",CONV_PRIORITY_MEDIUM)
								btriggeredleadin = true
							endif
						endif
					ENDIF
				ENDIF
			endif
			IF IS_SYNCHRONIZED_SCENE_RUNNING(OfficeSceneTrev)
				IF GET_SYNCHRONIZED_SCENE_PHASE(OfficeSceneTrev) > 0.95
					if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							OfficeSceneTrev = CREATE_SYNCHRONIZED_SCENE(vOfficeScenePos,vOfficeSceneRot)
							TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],OfficeSceneTrev,"missbigscore1leadinoutbs_1_int","leadin_loop_c_trevor",WALK_BLEND_IN,NORMAL_BLEND_OUT)
						endif
					endif
				endif
			endif	
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(OfficeScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(OfficeScene) >= 0.70	
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iLeadStage++
				endif
			endif
		break
		case 2
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
					SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[1],TRUE)
				ENDIF
			ENDIF
		break
	endswitch

//
	//attach the bottle to trev
	if not bBottleAttached
		if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		and does_entity_exist(g_sTriggerSceneAssets.ped[0])
			if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				if DOES_ENTITY_HAVE_PHYSICS(g_sTriggerSceneAssets.object[0])
					ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.ped[0]
							,GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0],BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
					bBottleAttached = true
				endif
			endif
		endif
	endif
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINALEH1_AMBIENT_UPDATE()

	if not bleadinStart
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<127.79243, -1290.52930, 31.48345>>,<<126.72432, -1288.84412, 28.28557>>,4.5) 			
			bleadinstart = true
		endif		
	else
		if IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(),<<129.19241, -1299.39246, 28.23270>>,<<127.47752, -1296.46045, 31.70886>>,4) 
			bleadinstart = FALSE
		endif
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(true)				
	endif
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		SET_DOOR_STATE(DOORNAME_STRIPCLUB_R,DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	ENDIF
ENDPROC
