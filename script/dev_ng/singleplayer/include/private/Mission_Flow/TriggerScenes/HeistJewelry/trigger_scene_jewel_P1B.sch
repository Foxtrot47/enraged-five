//╒═════════════════════════════════════════════════════════════════════╕
//│						Jewel Prep 1B - Trigger Scene Data				│
//│																		│
//│								Date: 06/04/13							│
//╞═════════════════════════════════════════════════════════════════════╡
//│																		│
//│				----------------- CONVOY LAYOUT ------------------		│
//│				ped[See below]											│	
//│													 					│
//│				[----]													│	
//│				|D	1|		FBI2 	=	veh[0]		 					│
//│				|2	3|								 					│	
//│			   4[----]5							 						│
//│							object[0] = crate							│
//╘═════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "script_clock.sch"
USING "comms_control_public.sch"
USING "script_ped.sch"

CONST_FLOAT 	TS_JEWEL_P1B_STREAM_IN_DIST			320.0		// Distance at which the scene loads and is created.
CONST_FLOAT		TS_JEWEL_P1B_STREAM_OUT_DIST		350.0		// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_JEWEL_P1B_TRIGGER_DIST			60.0		// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT		TS_JEWEL_P1B_HELI_TRIGGER_DIST		110.0		// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT		TS_JEWEL_P1B_BLIP_SPEED				0.2			// speed modifier of the blip when the van isn't spawned


CONST_FLOAT		TS_JEWEL_P1B_FRIEND_REJECT_DIST		TS_JEWEL_P1B_STREAM_OUT_DIST		//Distance friends will bail from player group.
CONST_INT		TS_JEWEL_P1B_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

VECTOR			vWANDER1 		=	<<441.86075, -1015.75806, 27.66605>>
VECTOR			vWANDER2		=	<<-1511.45569, -654.47925, 28.23983>> 

TS_MOVING_BLIP_STRUCT		TS_JEWEL_P1B_MOVING_BLIP

BOOL						TS_JEWEL_P1B_bIsActive 						= FALSE
BOOL						TS_JEWEL_P1B_bDisplayedBlipAppearHelp		= FALSE
BOOL						TS_JEWEL_P1B_bDisplayedBlipRemovedHelp		= FALSE 

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 		TS_JEWEL_P1B_DEBUG_WIDGET
#ENDIF


//--------------- FUNCS & PROCS --------------------------
FUNC BOOL TS_JEWEL_P1B_IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 				
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC

proc TS_JEWEL_P1B_create_enemy_in_vehicle(int enemy,VEHICLE_INDEX veh,bool bPolice = false)
	
	VEHICLE_SEAT 	vsSeat
	MODEL_NAMES		model
	WEAPON_TYPE		wtEnemyWpn
	if bpolice
		model		= S_M_Y_COP_01
		wtEnemyWpn 	= WEAPONTYPE_PISTOL
	else
		model		= S_M_Y_SWAT_01
		wtEnemyWpn 	= WEAPONTYPE_SMG
	endif
	
	if 	enemy  = 0
		vsSeat 		= VS_DRIVER	
	elif enemy = 1
		vsSeat 		= VS_FRONT_RIGHT
	elif enemy = 2
		vsSeat 		= VS_BACK_LEFT				
	elif enemy = 3
		vsSeat 		= VS_BACK_RIGHT	
	endif
	
	g_sTriggerSceneAssets.ped[enemy] = CREATE_PED_INSIDE_VEHICLE(veh,PEDTYPE_COP,model,vsSeat)
	
	SET_PED_AS_ENEMY(g_sTriggerSceneAssets.ped[enemy], TRUE)	
	SET_PED_TO_LOAD_COVER(g_sTriggerSceneAssets.ped[enemy], TRUE)
	GIVE_WEAPON_TO_PED(g_sTriggerSceneAssets.ped[enemy], wtEnemyWpn, INFINITE_AMMO, TRUE,true)
	SET_PED_MONEY(g_sTriggerSceneAssets.ped[enemy],0)		
	SET_PED_CAN_PEEK_IN_COVER(g_sTriggerSceneAssets.ped[enemy], TRUE)
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[enemy],250)
	SET_PED_ALERTNESS(g_sTriggerSceneAssets.ped[enemy],AS_ALERT)
	SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[enemy])
	//combat attributes		
	SET_PED_COMBAT_ABILITY(g_sTriggerSceneAssets.ped[enemy],CAL_PROFESSIONAL)	
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[enemy],PCF_PreventAutoShuffleToDriversSeat,true)	
	
	if not bpolice
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[enemy],PED_COMP_DECL,0,0)
	endif
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[enemy],TRUE)
	STOP_PED_SPEAKING(g_sTriggerSceneAssets.ped[enemy],true)	
	SET_PED_AS_COP(g_sTriggerSceneAssets.ped[enemy])
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[enemy], PCF_DontBlip, TRUE)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.ped[enemy],100,false)
	SET_PED_ARMOUR(g_sTriggerSceneAssets.ped[enemy], 25)
	SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[enemy], RELGROUPHASH_SECURITY_GUARD)
//	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[enemy],true)
endproc
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_JEWEL_P1B_RESET()

	// Reset local flags
	g_JP1b_string_WayPoint				= ""
	g_JP1b_iTriggerRunCount				= 0
	g_JP1B_tod_NextTriggerTime			= INVALID_TIMEOFDAY
	
	TS_MOVING_BLIP_STRUCT newBlank
	TS_JEWEL_P1B_MOVING_BLIP				= newBlank
	TS_JEWEL_P1B_bIsActive					= FALSE
	TS_JEWEL_P1B_bDisplayedBlipAppearHelp 	= FALSE
	TS_JEWEL_P1B_bDisplayedBlipRemovedHelp	= FALSE
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_JEWEL_P1B_REQUEST_ASSETS()
	REQUEST_MODEL(FBI2)
	REQUEST_MODEL(S_M_Y_SWAT_01)
	REQUEST_MODEL(PROP_BOX_AMMO03A)	
	
	REQUEST_ADDITIONAL_TEXT("JHP1B", MISSION_TEXT_SLOT)
	
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_JEWEL_P1B_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(FBI2)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_SWAT_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BOX_AMMO03A)	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_JEWEL_P1B_HAVE_ASSETS_LOADED()
	if  HAS_MODEL_LOADED(FBI2)
	and HAS_MODEL_LOADED(S_M_Y_SWAT_01)		
	and not IS_STRING_NULL_OR_EMPTY(g_JP1b_string_WayPoint)
	and GET_IS_WAYPOINT_RECORDING_LOADED(g_JP1b_string_WayPoint)	
	and HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	and HAS_MODEL_LOADED(PROP_BOX_AMMO03A)	
		RETURN TRUE
	endif	
	return false
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_JEWEL_P1B_CREATE()
	int 	iTotalWaypointNodes
	VECTOR 	vVeh_current_node_pos
	VECTOR	vVeh_prev_node_pos
	FLOAT 	fSpawnHeading
	
	g_sTriggerSceneAssets.id = 1
	WAYPOINT_RECORDING_GET_NUM_POINTS(g_JP1b_string_WayPoint,iTotalWaypointNodes)	
	WAYPOINT_RECORDING_GET_COORD(g_JP1b_string_WayPoint, TS_JEWEL_P1B_MOVING_BLIP.iTargetNode, vVeh_current_node_pos)	
	WAYPOINT_RECORDING_GET_COORD(g_JP1b_string_WayPoint, CLAMP_INT(TS_JEWEL_P1B_MOVING_BLIP.iTargetNode - 1,0,iTotalWaypointNodes),vVeh_prev_node_pos)
	fSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(vVeh_prev_node_pos,vVeh_current_node_pos)
	
	CLEAR_AREA(vVeh_current_node_pos, 60, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(fbi2,vVeh_current_node_pos, fSpawnHeading)
	SET_MODEL_AS_NO_LONGER_NEEDED(FBI2)
	SET_VEHICLE_FORWARD_SPEED(g_sTriggerSceneAssets.veh[0],15)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0],100,false)
	SET_VEHICLE_TYRES_CAN_BURST(g_sTriggerSceneAssets.veh[0], FALSE)

//--------------------	Create Convoy peds	--------------------------
	TS_JEWEL_P1B_create_enemy_in_vehicle(0,g_sTriggerSceneAssets.veh[0])
	SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[0], 7)
	TS_JEWEL_P1B_create_enemy_in_vehicle(1,g_sTriggerSceneAssets.veh[0])
	SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[1], 7)
	TS_JEWEL_P1B_create_enemy_in_vehicle(2,g_sTriggerSceneAssets.veh[0])
	SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[2], 7)
	TS_JEWEL_P1B_create_enemy_in_vehicle(3,g_sTriggerSceneAssets.veh[0])
	SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[3], 7)
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0],g_JP1b_string_WayPoint, DRIVINGMODE_AVOIDCARS, 
										   TS_JEWEL_P1B_MOVING_BLIP.iTargetNode,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,25)	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[0])
	
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.veh[0],250)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])			
	SET_VEHICLE_STRONG(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0])*3))
	SET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[0])*3))
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_PETROL_TANK_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_VEHICLE_PETROL_TANK_HEALTH(g_sTriggerSceneAssets.veh[0])*3))
	SET_VEHICLE_CAN_LEAK_PETROL(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_CAN_LEAK_OIL(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_PROVIDES_COVER(g_sTriggerSceneAssets.veh[0],true)	
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_SIREN(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_CAN_BREAK(g_sTriggerSceneAssets.veh[0], FALSE)
    SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(g_sTriggerSceneAssets.veh[0], SC_DOOR_BOOT, FALSE)
	TS_JEWEL_P1B_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_MISSION_JEWELRY_P1B, g_sTriggerSceneAssets.veh[0])
	
	g_sTriggerSceneAssets.object[0]  =  CREATE_OBJECT(PROP_BOX_AMMO03A,g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P1B].vCoords[0])
	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.veh[0],0,<<0,-1.77,0.0>>,<<0,0,90>>)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_JEWEL_P1B_RELEASE()
	INT iEntityInstanceID
	STRING strEntityScript	
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])	
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])	
	int i
	for i = 0 to 9 
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[i])
				strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.ped[i], iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
						SET_PED_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.ped[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	endfor	
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	REMOVE_SCENARIO_BLOCKING_AREAS()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_JEWEL_P1B_DELETE()

	IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MISSION_JEWELRY_P1B)
		TS_JEWEL_P1B_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_JEWELRY_P1B, g_JP1b_string_WayPoint, TS_JEWEL_P1B_BLIP_SPEED, -1)
	ENDIF
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])	
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])	
	int i
	for i = 0 to 9
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	endfor	
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	REMOVE_SCENARIO_BLOCKING_AREAS()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_JEWEL_P1B_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_JEWELRY_P1B)
	
	bool btriggerMission
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
		
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			IF g_bTriggerDebugOn
				if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_JEWEL_P1B_HELI_TRIGGER_DIST)
				else
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_JEWEL_P1B_TRIGGER_DIST)
				ENDIF
			endif
		#ENDIF

		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
			IF fDistanceSquaredFromTrigger < (TS_JEWEL_P1B_HELI_TRIGGER_DIST*TS_JEWEL_P1B_HELI_TRIGGER_DIST)			
				bTriggerMission = true
			ENDIF
		else
			IF fDistanceSquaredFromTrigger < (TS_JEWEL_P1B_TRIGGER_DIST*TS_JEWEL_P1B_TRIGGER_DIST)
				bTriggerMission = true
			ENDIF
		endif

		int i 
		for i = 0 to 2
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
				if IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[i])
					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[i],player_ped_id())					
						bTriggerMission = true					
					endif
				else				
					bTriggerMission = true				
				endif	
			endif	
		endfor
		for i = 0 to 9
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
				if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[i],player_ped_id())					
						bTriggerMission = true								
					endif
				else
					bTriggerMission = true
				endif	
			endif	
		endfor
	
	ENDIF
	
	IF bTriggerMission
		REMOVE_SCENARIO_BLOCKING_AREAS()
		CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
		g_JP1B_tod_NextTriggerTime 			= INVALID_TIMEOFDAY		
		TS_JEWEL_P1B_bIsActive				= FALSE	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_JEWEL_P1B_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_JEWEL_P1B_IS_BLOCKED()

	#if IS_DEBUG_BUILD
		IF NOT DOES_WIDGET_GROUP_EXIST(TS_JEWEL_P1B_DEBUG_WIDGET)
			TS_JEWEL_P1B_DEBUG_WIDGET = START_WIDGET_GROUP("TS: JHP1B")
				ADD_WIDGET_BOOL("TS_JEWEL_P1B_bIsActive", TS_JEWEL_P1B_bIsActive)
				ADD_WIDGET_BOOL("TS_JEWEL_P1B_bDisplayedBlipAppearHelp", TS_JEWEL_P1B_bDisplayedBlipAppearHelp)
				ADD_WIDGET_BOOL("TS_JEWEL_P1B_bDisplayedBlipRemovedHelp", TS_JEWEL_P1B_bDisplayedBlipRemovedHelp)
				ADD_WIDGET_INT_READ_ONLY("g_JP1b_iTriggerRunCount", g_JP1b_iTriggerRunCount)
				ADD_WIDGET_FLOAT_SLIDER("Blip move speed", TS_JEWEL_P1B_MOVING_BLIP.fMoveSpeedMulti, 0.1, 200.0, 0.05)
			STOP_WIDGET_GROUP()
		ENDIF
	
	#endif
	
	IF TS_JEWEL_P1B_bIsActive
		RETURN FALSE		
		
	// Not active check if we need to block
	ELIF IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))		
	
		// Initialise time of day stuff
		IF g_JP1B_tod_NextTriggerTime = INVALID_TIMEOFDAY
			g_JP1B_tod_NextTriggerTime = GET_CURRENT_TIMEOFDAY()		
			ADD_TIME_TO_TIMEOFDAY(g_JP1B_tod_NextTriggerTime, 0, 0, 1)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 txtDebugTOD = TIMEOFDAY_TO_TEXT_LABEL(g_JP1B_tod_NextTriggerTime)
				CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] Set up new unblocking TimeOfDay: ", txtDebugTOD)
			#ENDIF
		ENDIF
		
		// See if this trigger scene can now become active
		IF IS_NOW_AFTER_TIMEOFDAY(g_JP1B_tod_NextTriggerTime)	
		or g_JP1b_iTriggerRunCount = 0	
		
			if IS_STRING_NULL_OR_EMPTY(g_jp1b_string_WayPoint)
				// Decide on route
				SWITCH g_iForcePrepMissionRoute
					CASE 0
					CASE 1
						//WANDER 1 
						g_sTriggerSceneAssets.id = 0
						g_JP1b_string_WayPoint = "JHP1bRoute2"
						CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] DEBUG JHP1bRoute2.")
					BREAK
					CASE 2
					CASE 3
						//WANDER 2
						g_sTriggerSceneAssets.id = 1
						g_JP1b_string_WayPoint = "JHP1b"
						CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] DEBUG JHP1b.")
					BREAK
					DEFAULT											
						float WANDER1_dis
						float WANDER2_dis
						WANDER1_dis  = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),vWANDER1)
						WANDER2_dis  = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),vWANDER2)
						if  WANDER1_dis >= WANDER2_dis
							//WANDER 1
							g_sTriggerSceneAssets.id = 0
							g_JP1b_string_WayPoint = "JHP1bRoute2" 	
							CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] WANDER 1.")
						elif WANDER2_dis >= WANDER1_dis
							//WANDER 2 
							g_sTriggerSceneAssets.id = 1
							g_JP1b_string_WayPoint = "JHP1b"
							CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] WANDER 2.")
						endif
					BREAK
				ENDSWITCH
				g_iForcePrepMissionRoute = -1	// reset flow global									
			else
				REQUEST_WAYPOINT_RECORDING(g_JP1b_string_WayPoint)
				IF GET_IS_WAYPOINT_RECORDING_LOADED(g_JP1b_string_WayPoint)
					// Start the blip moving on the wp recording					
					TS_JEWEL_P1B_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_JEWELRY_P1B, g_JP1b_string_WayPoint, TS_JEWEL_P1B_BLIP_SPEED, 0)
					TS_JEWEL_P1B_bIsActive 					= TRUE
					TS_JEWEL_P1B_bDisplayedBlipAppearHelp 	= FALSE
					TS_JEWEL_P1B_bDisplayedBlipRemovedHelp 	= FALSE					
					g_JP1b_iTriggerRunCount++
					CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] Trigger scene has been set to active, using route \"", g_JP1b_string_WayPoint, "\"")
					RETURN FALSE
				ENDIF
			endif			
		
		ELSE
		
			IF NOT TS_JEWEL_P1B_bDisplayedBlipRemovedHelp	// not shown it yet
			AND NOT TS_JEWEL_P1B_bIsActive					// blip is not active and being shown
				IF g_JP1b_iTriggerRunCount <= 2				// only display this message twice
					
					SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_JHP1B_MISS")
			            CASE FHS_EXPIRED
			                ADD_HELP_TO_FLOW_QUEUE("AM_H_JHP1B_MISS", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
			            BREAK
			            CASE FHS_DISPLAYED
							g_txtFlowHelpLastDisplayed = ""
							TS_JEWEL_P1B_bDisplayedBlipRemovedHelp  = TRUE
			            BREAK
			     	ENDSWITCH
				
				ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
				
					TS_JEWEL_P1B_bDisplayedBlipRemovedHelp = TRUE
					
				ENDIF
			ENDIF
		
		ENDIF
	
	// else player character isn't valid, reset trigger scene variables
	ELSE
		g_JP1B_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
		TS_JEWEL_P1B_bIsActive 				= FALSE
		g_JP1b_string_WayPoint	 			= ""
	endif
	RETURN TRUE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_JEWEL_P1B_UPDATE()
	IF TS_JEWEL_P1B_bIsActive
	
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(HASH("POLICE_GARAGE_R"))
			ADD_DOOR_TO_SYSTEM(HASH("POLICE_GARAGE_R"), PROP_COM_GAR_DOOR_01, <<452.2993, -1001.1797, 26.7593>>)	
		ENDIF
		IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(HASH("POLICE_GARAGE_L"))
			ADD_DOOR_TO_SYSTEM(HASH("POLICE_GARAGE_L"), PROP_COM_GAR_DOOR_01, <<447.480, -1001.1707, 26.7126>>)	
		ENDIF
		
		// Lock the left door as we're not using it		
		IF DOOR_SYSTEM_GET_DOOR_STATE(HASH("POLICE_GARAGE_L")) != DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_DOOR_STATE(HASH("POLICE_GARAGE_L"), DOORSTATE_LOCKED, DEFAULT, TRUE)
		ENDIF
	
		if TS_JEWEL_P1B_IsEntityAlive(g_sTriggerSceneAssets.ped[0])
		and TS_JEWEL_P1B_IsEntityAlive(g_sTriggerSceneAssets.veh[0])		
		
			// Update the blip
			TS_UPDATE_MOVING_BLIP(TS_JEWEL_P1B_MOVING_BLIP)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
			
			// Is doing the drive into the garage task
			IF IS_ENTITY_IN_ANGLED_AREA(g_sTriggerSceneAssets.ped[0], <<449.772156,-998.623352,23.115438>>, <<449.768372,-993.625549,27.647564>>, 11.750000)
			
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(g_sTriggerSceneAssets.ped[0], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)

					IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(HASH("POLICE_GARAGE_R"))) < 0.05
					
						// reset the time of day blocking
						CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] Vehicle reached end of route.")
						g_JP1B_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
						g_JP1b_string_WayPoint				= ""
						TS_JEWEL_P1B_bIsActive 				= FALSE	
					
					ELSE
						IF DOOR_SYSTEM_GET_DOOR_STATE(HASH("POLICE_GARAGE_R")) = DOORSTATE_UNLOCKED
							DOOR_SYSTEM_SET_DOOR_STATE( HASH("POLICE_GARAGE_R"), DOORSTATE_LOCKED, DEFAULT, TRUE)
						ENDIF
					ENDIF
					
				ENDIF
			
			// Reached the start of the garage
			ELIF IS_ENTITY_IN_ANGLED_AREA(g_sTriggerSceneAssets.veh[0], <<446.324249,-993.339233,24.194170>>, <<445.690826,-1029.076294,30.285543>>, 26.437500)
				
				// Door is open enough to drive in
				IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(HASH("POLICE_GARAGE_R"))) > 0.75
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(g_sTriggerSceneAssets.ped[0], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
						TASK_VEHICLE_DRIVE_TO_COORD(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0], <<452.2002, -997.1097, 24.7618>>, 5.0, DRIVINGSTYLE_ACCURATE, FBI2, DRIVINGMODE_STOPFORCARS_STRICT, 0.5, 2.0)
					ENDIF
					
				// Door not open yet, wait on the stop
				ELSE
					IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(g_sTriggerSceneAssets.ped[0], SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(g_sTriggerSceneAssets.ped[0], SCRIPT_TASK_VEHICLE_MISSION)
							TASK_VEHICLE_MISSION_COORS_TARGET(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0], <<452.3164, -1004.6366, 25.9034>>, MISSION_GOTO, 2.0, DRIVINGMODE_STOPFORCARS_STRICT, 0.5, 1.0, FALSE)
						ENDIF
					ENDIF
					
					IF DOOR_SYSTEM_GET_DOOR_STATE(HASH("POLICE_GARAGE_R")) != DOORSTATE_UNLOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(HASH("POLICE_GARAGE_R"), DOORSTATE_UNLOCKED, DEFAULT, TRUE)
					ENDIF
				ENDIF
				
			// normal behavior
			ELSE

				SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(g_sTriggerSceneAssets.veh[0],true)
				SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(g_sTriggerSceneAssets.veh[0],true)
				if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.veh[0])
					RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0])
				ENDIF
				if not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE( g_sTriggerSceneAssets.veh[0])					
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0],
							g_JP1b_string_WayPoint,
							DRIVINGMODE_STOPFORCARS,
							TS_JEWEL_P1B_MOVING_BLIP.iTargetNode,
							EWAYPOINT_START_FROM_CLOSEST_POINT	| EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,15)
				endif	
				
			ENDIF
			
		ENDIF
	
		
		
		
		
		int i
		for i = 0 to 9
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])				
				if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])	
					if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.ped[i])
						RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.ped[i])
					ENDIF					
				endif	
			endif	
		endfor
					
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_JEWEL_P1B_AMBIENT_UPDATE()
	IF TS_JEWEL_P1B_bIsActive
	
		IF NOT TS_JEWEL_P1B_bDisplayedBlipAppearHelp				// not shown it yet
		
			IF g_JP1b_iTriggerRunCount = 1	
			
				TS_JEWEL_P1B_bDisplayedBlipAppearHelp 	= TRUE	
				
			ELIF g_JP1b_iTriggerRunCount <= 3					// only display this message twice
				
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_JHP1B_REAP")
		            CASE FHS_EXPIRED 
		                ADD_HELP_TO_FLOW_QUEUE("AM_H_JHP1B_REAP", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
		            BREAK
		            CASE FHS_DISPLAYED
						g_txtFlowHelpLastDisplayed = ""
						TS_JEWEL_P1B_bDisplayedBlipAppearHelp = TRUE
		            BREAK
		     	ENDSWITCH
			
			ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
			
				TS_JEWEL_P1B_bDisplayedBlipAppearHelp = TRUE
				
			ENDIF
		ENDIF
		// When active move the blip along the route		
		IF NOT TS_UPDATE_MOVING_BLIP(TS_JEWEL_P1B_MOVING_BLIP)
		OR NOT IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_1B].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
			// reset the time of day blocking
			CPRINTLN(DEBUG_TRIGGER, "[JEWEL_P1B] Blip reached end of route.")
			g_JP1B_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
			g_JP1b_string_WayPoint				= ""
			TS_JEWEL_P1B_bIsActive 				= FALSE	
		ENDIF
		
	ENDIF
ENDPROC
