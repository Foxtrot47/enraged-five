//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Jewelry Heist Prep 1A - Trigger Scene Data					│
//│																				│
//│								Date: 26/10/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"

CONST_FLOAT						TS_JEWEL_P1A_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT						TS_JEWEL_P1A_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

CONST_FLOAT 					TS_JEWEL_P1A_STREAM_IN_DIST				325.0			// Distance at which the scene loads and is created.
CONST_FLOAT						TS_JEWEL_P1A_STREAM_OUT_DIST			335.0			// Distance at which the scene is deleted and unloaded.														 	
TEXT_LABEL_31					TS_JEWEL_P1A_WP_SEC_ARRIVE				= "jhp1a_sec_arrive"

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_JEWEL_P1A_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_JEWEL_P1A_REQUEST_ASSETS()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		REQUEST_MODEL(BURRITO2)
		REQUEST_MODEL(S_M_Y_PESTCONT_01)
		REQUEST_MODEL(S_M_M_SECURITY_01)
		REQUEST_MODEL(DILETTANTE2)
		REQUEST_MODEL(P_AMB_CLIPBOARD_01)
		REQUEST_ANIM_DICT("misslsdhsclipboard@base")
		REQUEST_WAYPOINT_RECORDING(TS_JEWEL_P1A_WP_SEC_ARRIVE)
		PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS_WITH_TYPE(<<154.947784,-3092.523438,4.911984>>, "po1_08_warehouseint1"))
	ENDIF
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_JEWEL_P1A_RELEASE_ASSETS()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO2)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_PESTCONT_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_SECURITY_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(DILETTANTE2)	
		SET_MODEL_AS_NO_LONGER_NEEDED(P_AMB_CLIPBOARD_01)
		REMOVE_ANIM_DICT("misslsdhsclipboard@base")
		UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS_WITH_TYPE(<<154.947784,-3092.523438,4.911984>>, "po1_08_warehouseint1"))
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_JEWEL_P1A_HAVE_ASSETS_LOADED()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF HAS_MODEL_LOADED(BURRITO2)
		AND HAS_MODEL_LOADED(S_M_Y_PESTCONT_01)
		AND HAS_MODEL_LOADED(S_M_M_SECURITY_01)
		AND HAS_MODEL_LOADED(DILETTANTE2)
		AND HAS_MODEL_LOADED(P_AMB_CLIPBOARD_01)
		AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@base")
		AND GET_IS_WAYPOINT_RECORDING_LOADED(TS_JEWEL_P1A_WP_SEC_ARRIVE)
		AND IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(<<154.947784,-3092.523438,4.911984>>, "po1_08_warehouseint1"))
	// Check scenario points have streamed in, they dont have to be free as they will be cleared
		AND DOES_SCENARIO_EXIST_IN_AREA(<<155.74, -3098.89, 4.93>>, 0.5, FALSE)				// Foreman
		AND DOES_SCENARIO_EXIST_IN_AREA(<<159.80, -3085.96, 5.00>>, 0.5, FALSE)				// guys out the front
		AND DOES_SCENARIO_EXIST_IN_AREA(<<159.06, -3085.00, 5.01>>, 0.5, FALSE)				
		// Security has diff scenario at diff time of day
		AND ( ( GET_CLOCK_HOURS() >= 5 AND GET_CLOCK_HOURS() < 21	AND DOES_SCENARIO_EXIST_IN_AREA(<<203.68, -3132.46, 4.79>>, 1.0, FALSE) )
		OR	( GET_CLOCK_HOURS() < 5 OR GET_CLOCK_HOURS() >= 21		AND DOES_SCENARIO_EXIST_IN_AREA(<<162.67, -3115.61, 4.95>>, 1.0, FALSE) ) )
		AND NOT (IS_PLAYER_A_TAXI_PASSENGER() AND IS_SCREEN_FADED_OUT() AND IS_NEW_LOAD_SCENE_ACTIVE())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_JEWEL_P1A_CREATE()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(
			<<139.579529,-3092.962402,8.646310>> - <<33.312500,24.000000,4.187500>>,
			<<139.579529,-3092.962402,8.646310>> + <<33.312500,24.000000,4.187500>>,
			FALSE)
	
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(
			<<138.426773,-3092.467041,7.271310>> - <<19.437500,22.750000,2.375000>>,
			<<138.426773,-3092.467041,7.271310>> + <<19.437500,22.750000,2.375000>>)
		
		CLEAR_AREA(<<138.43, -3092.47, 4.90>>, 28.375, TRUE)

	// Create vans
		g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(BURRITO2, <<148.7243, -3104.6189, 4.8962>>, 179.6158)
		SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		SET_VEHICLE_ALARM(g_sTriggerSceneAssets.veh[0], TRUE)
		
		// with hood up
		g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(BURRITO2, <<145.1268, -3078.9316, 4.8963>>, 283.8845)
		SET_VEHICLE_ENGINE_ON(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(g_sTriggerSceneAssets.veh[1], SC_DOOR_BONNET, FALSE)
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[1], SC_DOOR_FRONT_LEFT, TRUE, FALSE)
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[1], SC_DOOR_BONNET, FALSE, FALSE)
		SET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[1], 100.0)
		
		// Van with ped standing at the back of it
		g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(BURRITO2, <<129.9696, -3089.3313, 4.8796>>, 269.9255)
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[2], SC_DOOR_REAR_LEFT, TRUE,  FALSE)
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[2], SC_DOOR_REAR_RIGHT, TRUE, FALSE)
		
		// Ped at the back of the van
		g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<126.2174, -3089.3833, 4.9199>>, 275.4068)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[0], TRUE)
		SET_ENTITY_HEALTH(g_sTriggerSceneAssets.ped[0], 150)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_HEAD, 0, 2)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_TORSO, 0, 1)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_LEG, 0, 1)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_DECL, 1, 0)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_HEAD, 1)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0)
		SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[0], 2.0)
		TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[0], "misslsdhsclipboard@base", "base", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
		
		// Create and attach the clip board
		g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(P_AMB_CLIPBOARD_01, GET_PED_BONE_COORDS(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND, <<0,0,0>>))
		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
		
		// Peds out the front to the left
		//----------------------------------------------------------
		// world human stand mobile	- FOREMAN
		g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<155.74, -3098.89, 5.93>>)		
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[2], TRUE)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_SPECIAL, 1, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_DECL, 0, 0)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[2], ANCHOR_HEAD, 0)
		SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[2], 2.0)
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[2], <<155.74, -3098.89, 4.93>>, 0.5)
		
		// Peds out the front to the right
		//----------------------------------------------------------
		
		// world human smoking 1	
		g_sTriggerSceneAssets.ped[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<159.80, -3085.96, 6.00>>)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[3], TRUE)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[3], PED_COMP_HEAD, 0, 1)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[3], PED_COMP_TORSO, 0, 2)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[3], PED_COMP_LEG, 0, 2)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[3], PED_COMP_SPECIAL, 1, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[3], PED_COMP_DECL, 1, 0)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[3], ANCHOR_EYES, 0)
		SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[3], 2.0)
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[3], <<159.80, -3085.96, 5.00>>, 0.5)
		// world human smoking 2	
		g_sTriggerSceneAssets.ped[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<159.06, -3085.00, 6.01>>)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[4], TRUE)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], PED_COMP_HEAD, 1, 1)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], PED_COMP_TORSO, 0, 3)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], PED_COMP_LEG, 0, 3)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], PED_COMP_SPECIAL, 1, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], PED_COMP_DECL, 0, 0)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[4], ANCHOR_HEAD, 0)
		SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[4], 2.0)
		TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(g_sTriggerSceneAssets.ped[4], <<159.06, -3085.00, 5.01>>, 0.5)
		
		// Security
		IF GET_CLOCK_HOURS() >= 5 AND GET_CLOCK_HOURS() < 21			
			g_sTriggerSceneAssets.veh[3] = CREATE_VEHICLE(DILETTANTE2, <<144.84, -2982.75, 5.32>>, 266.5972)
			g_sTriggerSceneAssets.ped[5] = CREATE_PED_INSIDE_VEHICLE(g_sTriggerSceneAssets.veh[3], PEDTYPE_MISSION, S_M_M_SECURITY_01, VS_DRIVER)
			
			SEQUENCE_INDEX seq
			
			OPEN_SEQUENCE_TASK(seq)
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(null, g_sTriggerSceneAssets.veh[3], TS_JEWEL_P1A_WP_SEC_ARRIVE, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
				TASK_LEAVE_VEHICLE(null, g_sTriggerSceneAssets.veh[3])
				TASK_USE_NEAREST_SCENARIO_TO_COORD(null, <<203.68, -3132.46, 4.79>>, 1.0)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[5], seq)
			CLEAR_SEQUENCE_TASK(seq)
			
			g_sTriggerSceneAssets.flag = TRUE

		ELSE
			g_sTriggerSceneAssets.veh[3] = CREATE_VEHICLE(DILETTANTE2, <<169.3554, -3110.0254, 4.8228>>, 88.4411)
			SET_VEHICLE_LIGHTS(g_sTriggerSceneAssets.veh[3], FORCE_VEHICLE_LIGHTS_ON)
			
			g_sTriggerSceneAssets.ped[5] = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<162.69, -3115.67, 4.95>>, 3.5948)
			SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[5], TRUE)
			TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(g_sTriggerSceneAssets.ped[5], <<162.69, -3115.67, 4.95>>, 1.0)
			
			g_sTriggerSceneAssets.flag = FALSE
		
		ENDIF
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[5], TRUE)
		SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[5], 5)
	
	ENDIF
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_JEWEL_P1A_RELEASE()
	INT i
	
	PRINTLN("RELEASING TRIGGER SCENE JEWEL PREP 1A")

	//Release vans and security vehicle
	REPEAT 4 i
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	ENDREPEAT
	
	// Release the clipboard from the ped
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	// Release peds
	REPEAT 6 i
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
	ENDREPEAT
	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_JEWEL_P1A_DELETE()
	INT i
	
	PRINTLN("DELETING TRIGGER SCENE JEWEL PREP 1A")
	
	//Release vans and security vehicle
	REPEAT 4 i
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	ENDREPEAT
	
	// Release the clipboard from the ped
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	// Release peds
	REPEAT 6 i
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDREPEAT

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_JEWEL_P1A_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<182.953690,-3119.883789,101.922592>>,<<94.250000,239.750000,108.062500>>)
		AND NOT (IS_PLAYER_A_TAXI_PASSENGER() AND IS_SCREEN_FADED_OUT() AND IS_NEW_LOAD_SCENE_ACTIVE())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_JEWEL_P1A_HAS_BEEN_DISRUPTED()

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_JEWEL_P1A_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene. 
PROC TS_JEWEL_P1A_UPDATE()


ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_JEWEL_P1A_AMBIENT_UPDATE()


ENDPROC
