//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Jewelry Heist 1 - Trigger Scene Data					│
//│																				│
//│				Author: Ben Rollinson				Date: 06/08/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│						ped[0] = Lester											│
//│						object = Lester's walking stick							│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "dialogue_public.sch"
USING "locates_public.sch"
USING "Flow_Mission_Trigger_Public.sch"
USING "timelapse.sch"
USING "script_ped.sch"
USING "clearmissionarea.sch"

//structPedsForConversation sPedsForConversation - replaced with g_sTriggerSceneAssets.conversation
BOOL bGetSuitConversationPlayed 
BOOL LesterWaypointTaskGiven
BOOL PlayerInsideWarehouse
BOOL doneChatPH33
BOOL isPlayerLockedIn
BOOL doneChatPH34b
BOOL doneChatPH34c
BOOL doneChatPH34d
BOOL LesterLoopedAnimStarted
BOOL MichaelWearingSuit
BOOL GetVehicleOutTimerStarted
BOOL lookAtTaskGiven
BOOL scenarioWorkersEnabled
BOOL doneNegativeAnim

CONST_FLOAT 	TS_JEWEL1_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_JEWEL1_STREAM_OUT_DIST		120.0	// Distance at which the scene is deleted and unloaded.

CONST_FLOAT		TS_JEWEL1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_JEWEL1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

//VECTOR vOffset, vRot

//INT iDoor
INT JewelLeadInSyncdScene
INT iAnimTimer
INT iGetVehicleOutChatTimer
INT iGetVehicleOutFailTimer

//═════════╡ SCENARIO'S ╞═════════
SCENARIO_BLOCKING_INDEX iBinManBlockingArea

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_JEWEL1_RESET()
	
	//Flags
	isPlayerLockedIn 				= FALSE
	bGetSuitConversationPlayed 		= FALSE
	LesterWaypointTaskGiven 		= FALSE
	LesterLoopedAnimStarted			= FALSE
	PlayerInsideWarehouse 			= FALSE
	doneChatPH33 					= FALSE
	doneChatPH34b					= FALSE
	doneChatPH34c					= FALSE
 	doneChatPH34d					= FALSE
	GetVehicleOutTimerStarted	 	= FALSE
	lookAtTaskGiven					= FALSE
	scenarioWorkersEnabled 			= FALSE
	doneNegativeAnim 				= FALSE
	iGetVehicleOutChatTimer			= 0	
	iGetVehicleOutFailTimer			= 0
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_JEWEL1_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
	REQUEST_MODEL(PROP_CS_WALKING_STICK)
	REQUEST_ANIM_DICT("MISSHEIST_JEWELLEADINOUT")
	REQUEST_ADDITIONAL_TEXT("JH1TRIG", MISSION_TEXT_SLOT)
	REQUEST_WAYPOINT_RECORDING("bb_jew_7")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_JEWEL1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
	REMOVE_ANIM_DICT("MISSHEIST_JEWELLEADINOUT")
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_WAYPOINT_RECORDING("bb_jew_7")
	
	//Flags
	bGetSuitConversationPlayed 		= FALSE
	LesterWaypointTaskGiven 		= FALSE
	LesterLoopedAnimStarted			= FALSE
	PlayerInsideWarehouse 			= FALSE
	doneChatPH33					= FALSE	
	isPlayerLockedIn 				= FALSE
	lookAtTaskGiven					= FALSE
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_JEWEL1_HAVE_ASSETS_LOADED()
	
	//Used to preload variations on an invisible ped before the scene is created!
	TSPedCompRequest sLesterComponents[2]
	TS_STORE_PED_COMP_REQUEST(sLesterComponents[0], PED_COMP_TORSO, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sLesterComponents[1], PED_COMP_LEG, 1, 0)
	TSPedPropRequest sLesterProps[1]
	TS_STORE_PED_PROP_REQUEST(sLesterProps[0], ANCHOR_EYES, 0, 0)
	
	IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, <<718.6170, -964.8593, 29.3956>>, sLesterComponents, sLesterProps)
	OR NOT HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
	OR NOT HAS_ANIM_DICT_LOADED("MISSHEIST_JEWELLEADINOUT")
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("bb_jew_7")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_JEWEL1_CREATE()

	CLEAR_AREA(<<718.6170, -964.8593, 29.3956>>, 15.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	//Remove the bin man scenario
	iBinManBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<713.7754, -996.4443, 22.3085>>, <<715.7624, -991.7067, 25.6214>>)

	//Create Lester.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
		SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[0], 120.4053)

		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, g_sTriggerSceneAssets.ped[0], "LESTER", TRUE)
		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
	ENDIF

	//Create Lester's walking stick and attach it to him.
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_WALKING_STICK, <<1393.0402, -2069.2317, 51.0114>>)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
	
	//Start synchronized scene on Lester.
	JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "lester_base_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	SET_SYNCHRONIZED_SCENE_LOOPED(JewelLeadInSyncdScene, TRUE)
	
	SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[0], TRUE)
	
	IF NOT g_bPlayerIsInTaxi
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<714.728821,-976.242065,23.133692>>, <<721.381409,-976.239136,27.194174>>, 5.750000, 
										<<707.3029, -980.5249, 23.1296>>, 43.7514,
										TRUE, FALSE, TRUE)
	ENDIF
	
	IF IS_MICHAEL_WEARING_SMART_OUTFIT()
		MichaelWearingSuit = TRUE
	ELSE
		MichaelWearingSuit = FALSE
	ENDIF
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_JEWELRY_1,"JH_1_INT",CS_ALL,CS_NONE,CS_NONE)

	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_JEWELRY_1, "Lester", PED_COMP_TORSO, 0, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_HEIST_JEWELRY_1, "Lester", PED_COMP_LEG, 1, 0)
	MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_HEIST_JEWELRY_1, "Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_JEWEL1_RELEASE()
	//Release Lester's walkinYou'g stick.
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])

	//Release Lester.
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_JEWEL1_DELETE()
	//Delete Lester's walking stick.
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	//Delete Lester.
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)

	CLEAR_HELP(TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(iBinManBlockingArea)	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_JEWEL1_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
	
		IF MichaelWearingSuit = TRUE
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//					PRINTSTRING("************************J-Skipping trigger scene being set to return TRUE to start mission.************************")
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
					REMOVE_ANIM_DICT("MISSHEIST_JEWELLEADINOUT")
					REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
					isPlayerLockedIn = TRUE
					RETURN TRUE
				ENDIF
			#ENDIF
			IF isPlayerLockedIn = TRUE
				IF LesterWaypointTaskGiven = TRUE			
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<710.604126,-965.157471,28.895330>>, <<710.628235,-963.393921,31.314669>>, 2.000000)
						CLEAR_PRINTS()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
							REMOVE_ANIM_DICT("MISSHEIST_JEWELLEADINOUT")
							REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
							SET_FORCE_FOOTSTEP_UPDATE(g_sTriggerSceneAssets.ped[0], FALSE)
							RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bGetSuitConversationPlayed = FALSE
				IF PlayerInsideWarehouse = TRUE
					IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JH1_SUIT")
							PRINT_HELP_FOREVER("JH1_SUIT")
						ENDIF
					ENDIF
					
					// Make the high end clothes shop blip long range.
					SET_SHOP_BLIP_FLASH_WHEN_ACTIVATED(CLOTHES_SHOP_H_01_BH, TRUE)
					SET_SHOP_BLIP_LONG_RANGE(CLOTHES_SHOP_H_01_BH, TRUE)
					SET_SHOP_BLIP_NEEDS_UPDATED(CLOTHES_SHOP_H_01_BH)
					
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "JHS1AUD", "JHS1_PH34", CONV_PRIORITY_HIGH)
					//Mike, I said look nice. Can you put on a suit on, please?
					//Jesus, Michael. You look like a bum. Put something nice on.
					//I said look respectable. Go change, and come back here. 
					JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "lester_1st_suitless_entrance_negative_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(JewelLeadInSyncdScene, FALSE)
					bGetSuitConversationPlayed = TRUE
					g_sTriggerSceneAssets.flag = TRUE
				ENDIF
			ENDIF			
			RETURN FALSE
		ENDIF
//		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_JEWEL1_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		INT iJewelInjuredcount	
		
		//Check for Lester or any of the workers being injured or harmed.
		FOR iJewelInjuredcount = 0 TO 3
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iJewelInjuredcount])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[iJewelInjuredcount])
					KILL_ANY_CONVERSATION()
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[iJewelInjuredcount], PLAYER_PED_ID())
						KILL_ANY_CONVERSATION()						
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		//Fail mission if player is shooting any weapon
		IF PlayerInsideWarehouse = TRUE
			WEAPON_TYPE playersWeapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playersWeapon)
			IF playersWeapon != WEAPONTYPE_UNARMED
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					KILL_ANY_CONVERSATION()
					RETURN TRUE		
				ENDIF
			ENDIF
		ENDIF
		
		//Warn the player then fail if he brings a vehicle into the warehouse
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND PlayerInsideWarehouse = TRUE
			IF GetVehicleOutTimerStarted = FALSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "JHS1AUD", "JHS1_PH56", CONV_PRIORITY_MEDIUM)
					//What the fuck! Michael get that thing out of here.
					//Stop messing around. We've got work to do.
					//Michael! Move it out of here. Now!
					iGetVehicleOutChatTimer = GET_GAME_TIMER()
					iGetVehicleOutFailTimer = GET_GAME_TIMER()
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					GetVehicleOutTimerStarted = TRUE
				ENDIF
			ENDIF
			IF GetVehicleOutTimerStarted = TRUE
				IF GET_GAME_TIMER() > (iGetVehicleOutChatTimer + 5000)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "JHS1AUD", "JHS1_PH56", CONV_PRIORITY_MEDIUM)
						//What the fuck! Michael get that thing out of here.
						//Stop messing around. We've got work to do.
						//Michael! Move it out of here. Now!
						iGetVehicleOutChatTimer = GET_GAME_TIMER()
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
			//Now fail the mission if he takes longer than 40 seconds to get it out
			IF GetVehicleOutTimerStarted = TRUE
				IF GET_GAME_TIMER() > (iGetVehicleOutFailTimer + 10000)
					KILL_ANY_CONVERSATION()
					RETURN TRUE	
				ENDIF
			ENDIF
		ENDIF			
			
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_JEWEL1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_JEWEL1_UPDATE()	
	
	//Switch on the scenario's
	IF scenarioWorkersEnabled = FALSE
		IF DOES_SCENARIO_GROUP_EXIST("SEW_MACHINE")
			IF NOT IS_SCENARIO_GROUP_ENABLED("SEW_MACHINE")
				SET_SCENARIO_GROUP_ENABLED("SEW_MACHINE", TRUE)
				PRINTSTRING("Sceanrio peds should now be active") PRINTNL()
				scenarioWorkersEnabled = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Run this every frame to ensure Lester looks at the player regardless of situation	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			IF PlayerInsideWarehouse = TRUE
				IF lookAtTaskGiven = FALSE
					TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					lookAtTaskGiven = TRUE
				ENDIF
			ELSE 
				IF lookAtTaskGiven = TRUE
					TASK_CLEAR_LOOK_AT(g_sTriggerSceneAssets.ped[0])
					lookAtTaskGiven = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Run a check to see if player is inside the warehouse or not.
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<718.6170, -964.8593, 29.3956>>) < 25
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<720.109497,-974.879761,23.414156>>, <<716.210571,-974.832031,25.664173>>, 1.000000)
		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<718.6170, -964.8593, 29.3956>>) < 9
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<736.031921,-951.946716,29.367319>>, <<703.665344,-951.890320,34.117321>>, 12.000000)
				PlayerInsideWarehouse = TRUE
			ENDIF
		ENDIF
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<714.390991,-976.440125,22.644867>>, <<721.805359,-976.489441,26.2>>, 1.750000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<736.031921,-951.946716,29.367319>>, <<703.665344,-951.890320,34.117321>>, 12.000000)
			PlayerInsideWarehouse = FALSE
		ENDIF	
	ENDIF	

	//Run a check to see what car the player is in to keep for during the mission and moving to safe zone.
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<695.650574,-985.349121,21.716518>>, <<755.726868,-984.118408,28.208454>>, 18.750000)
				g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTSTRING("grabbing players car and setting it as g_sTriggerSceneAssets.veh[0]") PRINTNL()
			ENDIF
		ENDIF
	ENDIF	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		//Check when to lock player in to the scene 
		IF isPlayerLockedIn = FALSE
			IF MichaelWearingSuit = TRUE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<717.5, -964.7, 29.4>>, <<4,4,4>>)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<719.316284,-964.717346,28.645611>>, <<715.993835,-964.718994,31.645613>>, 2.750000)
						isPlayerLockedIn = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Have the player follow waypoint recording and activate the push cam now.
		IF isPlayerLockedIn = TRUE
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				TASK_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), "bb_jew_7", 3)
				//fix for B*2028864 - set the hint a bit higher when playing in first person so we don't look at Lester's butt
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0.5>>, TRUE, -1)
				ELSE
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, -1)
				ENDIF
				SET_GAMEPLAY_HINT_FOV(30.0)
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.7)
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.1)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
			ENDIF	
		ENDIF
		
		//Start waypoint playback for Lester once player is on foot and less than 10 metres away from him.
		IF MichaelWearingSuit = TRUE
			
			//Do some dialogue between Lester and Michael as they head up the stairs.
			IF PlayerInsideWarehouse = TRUE
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF g_sTriggerSceneAssets.flag = TRUE
					//If player was here earlier in wrong outfit but has got changed do the following dialogue.
					IF doneChatPH34c = FALSE
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
								IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02)
									OBJECT_INDEX objOfficeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02, FALSE)
									IF objOfficeDoor != NULL
										IF DOES_ENTITY_HAVE_DRAWABLE(objOfficeDoor)
											JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
											TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "Lester_Leadin_Action", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
											PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<709.9813, -963.5311, 30.5453>>, 2, v_ilev_ss_door02, JewelLeadInSyncdScene,"ss_door02_leadin_action", "MISSHEIST_JEWELLEADINOUT", NORMAL_BLEND_IN)
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(JewelLeadInSyncdScene, FALSE)
											SET_SYNCHRONIZED_SCENE_LOOPED(JewelLeadInSyncdScene, TRUE)
											SET_FORCE_FOOTSTEP_UPDATE(g_sTriggerSceneAssets.ped[0], TRUE)
											iAnimTimer = GET_GAME_TIMER()
											LesterWaypointTaskGiven = TRUE
											doneChatPH34c = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ELSE
					//If this is 1st time player has come and is wearing correct outfit say the following
					IF doneChatPH34d = FALSE
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
								IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02)
									OBJECT_INDEX objOfficeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02, FALSE)
									IF objOfficeDoor != NULL
										IF DOES_ENTITY_HAVE_DRAWABLE(objOfficeDoor)
											JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
											TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "Lester_Leadin_Action", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
											PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<709.9813, -963.5311, 30.5453>>, 2, v_ilev_ss_door02, JewelLeadInSyncdScene,"ss_door02_leadin_action", "MISSHEIST_JEWELLEADINOUT", NORMAL_BLEND_IN)
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(JewelLeadInSyncdScene, FALSE)
											SET_SYNCHRONIZED_SCENE_LOOPED(JewelLeadInSyncdScene, TRUE)
											SET_FORCE_FOOTSTEP_UPDATE(g_sTriggerSceneAssets.ped[0], TRUE)
											iAnimTimer = GET_GAME_TIMER()
											LesterWaypointTaskGiven = TRUE
											doneChatPH34d = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
			
			//Now start the chat to start the mission.
			IF doneChatPH34d = TRUE
			OR doneChatPH34c = TRUE
				IF doneChatPH33 = FALSE
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0]) < 9
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "JHS1AUD", "JHS1_LI_1", CONV_PRIORITY_MEDIUM)
										//What the hell is this place?
										//A garment factory. I needed a job that didn't require me to do anything apart from, uh, paying taxes.
										//Uh, makes me look legitimate, keeps the databases happy. 
										doneChatPH33 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
							
			//Start the looped anim of lester holding the door open at the office waiting for the player.
			IF LesterLoopedAnimStarted = FALSE
				IF LesterWaypointTaskGiven = TRUE
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02)
								OBJECT_INDEX objOfficeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<709.9806, -963.5297, 30.5460>>, 3.0, v_ilev_ss_door02, FALSE)
								IF objOfficeDoor != NULL
									IF DOES_ENTITY_HAVE_DRAWABLE(objOfficeDoor)
										IF GET_GAME_TIMER() > (iAnimTimer + 3000)
											IF IS_SYNCHRONIZED_SCENE_RUNNING(JewelLeadInSyncdScene)
												IF GET_SYNCHRONIZED_SCENE_PHASE(JewelLeadInSyncdScene) > 0.99
													STOP_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.ped[0], INSTANT_BLEND_OUT, TRUE)
													STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<709.9813, -963.5311, 30.5453>>, 2, v_ilev_ss_door02, INSTANT_BLEND_OUT)
													
													JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
													TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "lester_leadin_loop", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
													PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<709.9813, -963.5311, 30.5453>>, 2, v_ilev_ss_door02, JewelLeadInSyncdScene, "ss_door02_leadin_loop", "MISSHEIST_JEWELLEADINOUT", INSTANT_BLEND_IN)
													SET_SYNCHRONIZED_SCENE_LOOPED(JewelLeadInSyncdScene, TRUE)
													SET_FORCE_FOOTSTEP_UPDATE(g_sTriggerSceneAssets.ped[0], FALSE)
													LesterLoopedAnimStarted = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
			ENDIF
			
			//Pause the chat if the player goes further than 10metres away from Lester.
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					IF PlayerInsideWarehouse = FALSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF									
		ELSE
			
			//Play some dialogue if player is not wearing correct suit and is runnning around inside the warehouse.
			IF PlayerInsideWarehouse = TRUE
				IF bGetSuitConversationPlayed = TRUE
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_1st_suitless_entrance_negative_a")
								IF doneChatPH34b = FALSE
									IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "JHS1AUD", "JHS1_PH34b", CONV_PRIORITY_MEDIUM)
										//Sooner you change, the sooner we can find you a score.
										//When you look decent, we can go.
										//You're wasting my time. 
										//We're not casing a joint with you looking like that.
										//You're boring me, Michael. Just go change.
										IF doneNegativeAnim = FALSE
											JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
											TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "lester_hanging_around_suitless_negative_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
											SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(JewelLeadInSyncdScene, FALSE)
											doneNegativeAnim = TRUE
										ENDIF
										SETTIMERB(0)
										doneChatPH34b = TRUE
									ENDIF
								ELSE
									IF TIMERB() > 2000
									AND NOT IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_1st_suitless_entrance_negative_a")
									AND NOT IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_base_idle")
										JewelLeadInSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<716.060, -965.837, 29.396>>, <<0, 0, 0>>)		
										TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], JewelLeadInSyncdScene, "MISSHEIST_JEWELLEADINOUT", "lester_base_idle", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)	
										SET_SYNCHRONIZED_SCENE_LOOPED(JewelLeadInSyncdScene, TRUE)
									ENDIF
									IF TIMERB() > 15000
										doneChatPH34b = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		
			//Set the capsule for Lester if playing wrong clothes anims to stop player clipping through him
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_base_idle")
					OR IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_hanging_around_suitless_negative_a")
					OR IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHEIST_JEWELLEADINOUT", "lester_1st_suitless_entrance_negative_a")
						SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.55)
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
		
		//Player can only walk when inside the warehouse to prevent him from running ahead of Lester.
		IF PlayerInsideWarehouse = TRUE
		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<718.1, -975.3, 23.9>>) < 3
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			WEAPON_TYPE Current_Ped_weapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Current_Ped_weapon)
			IF Current_Ped_weapon <> WEAPONTYPE_UNARMED
			AND NOT IS_PHONE_ONSCREEN()
				// url:bugstar:2099723
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN("JH1 - Holstering player gun in first person")
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, FALSE)
				ELSE
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_JEWEL1_AMBIENT_UPDATE()
	VECTOR vTriggerPosition = <<724.68976, -984.86383, 23.17047>>
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fDistanceSquaredFromTrigger = VDIST2(vPlayerPos, vTriggerPosition)
		
	IF fDistanceSquaredFromTrigger < (60.0 * 60.0)
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<695.650574,-985.349121,21.716518>>, <<755.726868,-984.118408,28.208454>>, 18.750000)
					g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					PRINTSTRING("grabbing players car and setting it as g_sTriggerSceneAssets.veh[0]") PRINTNL()
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC
