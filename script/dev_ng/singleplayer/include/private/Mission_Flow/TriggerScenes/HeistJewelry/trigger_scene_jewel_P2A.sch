//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Jewelry Heist Prep 2A - Trigger Scene Data					│
//│																				│
//│									Date: 06/08/12								│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│							veh[0] = Bugstar Van								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "script_clock.sch"
USING "script_maths.sch"
USING "flow_help_public.sch"

// Constants
CONST_FLOAT 				TS_JEWEL_P2A_STREAM_IN_DIST					250.0							// Distance at which the scene loads and is created.
CONST_FLOAT					TS_JEWEL_P2A_STREAM_OUT_DIST				270.0							// Distance at which the scene is deleted and unloaded.
CONST_FLOAT 				TS_JEWEL_P2A_TRIGGER_DIST					50.0							// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT					TS_JEWEL_P2A_BLIP_SPEED						0.2								// speed modifier of the blip when the van isn't spawned
CONST_FLOAT					TS_JEWEL_P2A_VAN_MAX_SPEED					12.0							// speed of the van on the wp rec
STRING						TS_JEWEL_P2A_WP_ROUTE_1						= "jhp2a_alt"					// alt waypoint rec
STRING						TS_JEWEL_P2A_WP_ROUTE_2						= "jhp2a_main"					// Waypoint recording that is used as the route for the blip

CONST_FLOAT					TS_JEWEL_P2A_FRIEND_REJECT_DIST				TS_JEWEL_P2A_STREAM_OUT_DIST	//Distance friends will bail from player group.
CONST_INT					TS_JEWEL_P2A_FRIEND_ACCEPT_BITS				BIT_NOBODY						//Friends who can trigger the mission with the player.

// Variables
TS_MOVING_BLIP_STRUCT		TS_JEWEL_P2A_MOVING_BLIP													// Moving blips struct used to move a mission trigger and tis blip
BOOL						TS_JEWEL_P2A_bIsActive						= FALSE
BOOL						TS_JEWEL_P2A_bDisplayedBlipAppearHelp		= FALSE
BOOL						TS_JEWEL_P2A_bDisplayedBlipRemovedHelp		= FALSE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 		TS_JEWEL_P2A_DEBUG_WIDGET
#ENDIF


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_JEWEL_P2A_RESET()
	
	// Reset global flags
	g_JHP2A_string_WayPoint						= ""
	g_JHP2A_iTriggerRunCount					= 0
	g_JHP2A_tod_NextTriggerTime					= INVALID_TIMEOFDAY
	
	TS_MOVING_BLIP_STRUCT newBlank
	TS_JEWEL_P2A_MOVING_BLIP					= newBlank
	TS_JEWEL_P2A_bIsActive						= FALSE
	TS_JEWEL_P2A_bDisplayedBlipAppearHelp 		= FALSE
	TS_JEWEL_P2A_bDisplayedBlipRemovedHelp		= FALSE
	
	CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] TS_JEWEL_P2A_RESET() called")
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_JEWEL_P2A_REQUEST_ASSETS()
	REQUEST_MODEL(BOXVILLE3)
	REQUEST_MODEL(S_M_M_Armoured_01)
	REQUEST_MODEL(PROP_IDOL_CASE_02)
	REQUEST_MODEL(PROP_YELL_PLASTIC_TARGET)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BOXVILLE3, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_ARMOURED_01, TRUE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_JEWEL_P2A_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(BOXVILLE3)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Armoured_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_IDOL_CASE_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_YELL_PLASTIC_TARGET)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BOXVILLE3, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_ARMOURED_01, FALSE)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_JEWEL_P2A_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(BOXVILLE3)
	AND HAS_MODEL_LOADED(S_M_M_Armoured_01)
	AND NOT IS_STRING_NULL_OR_EMPTY(g_JHP2A_string_WayPoint)
	AND GET_IS_WAYPOINT_RECORDING_LOADED(g_JHP2A_string_WayPoint)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_JEWEL_P2A_CREATE()
	
	VECTOR vNextPointCoord
	WAYPOINT_RECORDING_GET_COORD(g_JHP2A_string_WayPoint, TS_JEWEL_P2A_MOVING_BLIP.iTargetNode, vNextPointCoord)
	VECTOR vVel				= NORMALISE_VECTOR(vNextPointCoord - g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P2A].vCoords[0]) * WAYPOINT_RECORDING_GET_SPEED_AT_POINT(g_JHP2A_string_WayPoint, TS_JEWEL_P2A_MOVING_BLIP.iTargetNode)
	FLOAT fSpawnHeading 	= GET_HEADING_FROM_VECTOR_2D(vNextPointCoord.x - g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P2A].vCoords[0].x, vNextPointCoord.y - g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P2A].vCoords[0].y)

	//Create delivery van. 
	CLEAR_AREA(g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P2A].vCoords[0], 15.0, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(BOXVILLE3, g_GameBlips[STATIC_BLIP_MISSION_JEWELRY_P2A].vCoords[0], fSpawnHeading)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0], 100, FALSE)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[0], TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[0], TRUE)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0], 3000)
	SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[0], 0)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 1, FALSE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 2, TRUE)
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])
	SET_ENTITY_VELOCITY(g_sTriggerSceneAssets.veh[0], vVel)
	SET_MODEL_AS_NO_LONGER_NEEDED(BOXVILLE3)
	
	g_sTriggerSceneAssets.ped[0] = CREATE_PED_INSIDE_VEHICLE(g_sTriggerSceneAssets.veh[0], PEDTYPE_MISSION, S_M_M_Armoured_01, VS_DRIVER)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Armoured_01)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[0], PCF_GetOutBurningVehicle, FALSE)
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[0], PCF_RunFromFiresAndExplosions, FALSE)
	
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0], g_JHP2A_string_WayPoint, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, TS_JEWEL_P2A_MOVING_BLIP.iTargetNode, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, TS_JEWEL_P2A_VAN_MAX_SPEED)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[0])
	
	TS_JEWEL_P2A_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_MISSION_JEWELRY_P2A, g_sTriggerSceneAssets.veh[0])
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_JEWEL_P2A_RELEASE()
	
	TRIGGER_SCENE_RELEASE_PED_FLEE_IN_VEHICLE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])

ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_JEWEL_P2A_DELETE()

	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	
	IF IS_STATIC_BLIP_CURRENTLY_VISIBLE_TO_SYSTEM(STATIC_BLIP_MISSION_JEWELRY_P2A)	
		TS_JEWEL_P2A_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_JEWELRY_P2A, g_JHP2A_string_WayPoint, TS_JEWEL_P2A_BLIP_SPEED, -1)
	ENDIF
	
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_JEWEL_P2A_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_JEWELRY_P2A)
	
	BOOL bTriggerMission
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
	
		// Distance trigger
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)	
		VECTOR vDistanceVector = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0])
	
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			SWITCH g_eTriggerDebugMode
				CASE TDM_TRIGGER_TEXT
				CASE TDM_ALL_TEXT
					DRAW_DEBUG_TEXT("Trigger", <<vTriggerPosition.X-TS_JEWEL_P2A_TRIGGER_DIST, vTriggerPosition.Y, vTriggerPosition.z>>, 0, 0, 255, 255)
					IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						TEXT_LABEL_31 strDebug 
						strDebug = "Z distance off triggering: " 
						strDebug += CLAMP_INT(ROUND(ABSF(vDistanceVector.z) - 7.0), 0, CEIL(ABSF(vDistanceVector.z)))
						DRAW_DEBUG_TEXT(strDebug, <<vTriggerPosition.X, vTriggerPosition.Y, vTriggerPosition.z + 3.0>>, 0, 0, 255, 255)
					ENDIF
				FALLTHRU
				CASE TDM_TRIGGER
				CASE TDM_ALL
					IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						DRAW_DEBUG_SPHERE(vTriggerPosition, 120, 0, 0, 255, 65) 
					ELSE
						DRAW_DEBUG_SPHERE(vTriggerPosition, TS_JEWEL_P2A_TRIGGER_DIST, 0, 0, 255, 65) 
					ENDIF
				BREAK
			ENDSWITCH
		#ENDIF
		
		IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
		AND fDistanceSquaredFromTrigger < 12100
		
			bTriggerMission = TRUE
	
		ELIF fDistanceSquaredFromTrigger < TS_JEWEL_P2A_TRIGGER_DIST*TS_JEWEL_P2A_TRIGGER_DIST
		AND ABSF(vDistanceVector.z) < 5.0
		
			bTriggerMission = TRUE
		
		// Vehicle damaged trigger
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[0], PLAYER_PED_ID())
			bTriggerMission = TRUE
		
		// Driver was killed or damaged by the player
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
			bTriggerMission = TRUE
			
		// Player shot near the vehicle
		ELIF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0]), 7.0)
			bTriggerMission = TRUE
			
		ENDIF
	ENDIF
	
	IF bTriggerMission
		// Reset the trigger scene for its next use
		CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
		g_JHP2A_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
		TS_JEWEL_P2A_bIsActive 					= FALSE
		//g_JHP2A_string_WayPoint 				= ""		// don't reset this as the retries need to know which one they are using

		//TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_JEWELRY_P2A, <<-1135.2375, 7370.3818, 1019.7178>>)	// move the blip out the way to a safe place for now
		
		CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Mission has been triggered")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_JEWEL_P2A_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_JEWEL_P2A_IS_BLOCKED()

	#IF IS_DEBUG_BUILD
	IF NOT DOES_WIDGET_GROUP_EXIST(TS_JEWEL_P2A_DEBUG_WIDGET)
		TS_JEWEL_P2A_DEBUG_WIDGET = START_WIDGET_GROUP("TS: JHP2A")
			ADD_WIDGET_BOOL("TS_JEWEL_P2A_bIsActive", TS_JEWEL_P2A_bIsActive)
			ADD_WIDGET_BOOL("TS_JEWEL_P2A_bDisplayedBlipAppearHelp", TS_JEWEL_P2A_bDisplayedBlipAppearHelp)
			ADD_WIDGET_BOOL("TS_JEWEL_P2A_bDisplayedBlipRemovedHelp", TS_JEWEL_P2A_bDisplayedBlipRemovedHelp)
			ADD_WIDGET_INT_READ_ONLY("g_JHP2A_iTriggerRunCount", g_JHP2A_iTriggerRunCount)
		STOP_WIDGET_GROUP()
	ENDIF
	#ENDIF

	// Flow was reset so reset the trigger scene
	IF g_iForcePrepMissionRoute != -1
		TS_JEWEL_P2A_RESET()
	ENDIF

	// Not blocked once active
	IF TS_JEWEL_P2A_bIsActive
		RETURN FALSE
		
	// Not active check if we need to block
	ELIF IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
	
		// Initialise time of day stuff
		IF g_JHP2A_tod_NextTriggerTime = INVALID_TIMEOFDAY
			g_JHP2A_tod_NextTriggerTime = GET_CURRENT_TIMEOFDAY()
			ADD_TIME_TO_TIMEOFDAY(g_JHP2A_tod_NextTriggerTime, 0, 24, 0)
			CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Setting time of day delay")
		ENDIF
		
		// See if this trigger scene can now become active
		IF IS_NOW_AFTER_TIMEOFDAY(g_JHP2A_tod_NextTriggerTime)	
		OR g_JHP2A_iTriggerRunCount = 0

			IF IS_STRING_NULL_OR_EMPTY(g_JHP2A_string_WayPoint)
			
				// Decide on route
				SWITCH g_iForcePrepMissionRoute
					CASE 1
						// Flow has explicitly decided upon route 1
						g_JHP2A_string_WayPoint = TS_JEWEL_P2A_WP_ROUTE_1
						CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Received alt route")
					BREAK
					CASE 2
						//... decided upon route 2
						g_JHP2A_string_WayPoint = TS_JEWEL_P2A_WP_ROUTE_2
						CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Received main route")
					BREAK
					DEFAULT
						// Decide on the route whose start is the furthest away.
						VECTOR vPlayerCoord 
						vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
						IF VDIST2(vPlayerCoord, <<1519.9181, -1001.8893, 56.8241>>) > VDIST2(vPlayerCoord, <<1388.3323, 683.8095, 78.2656>>)
							g_JHP2A_string_WayPoint 	= TS_JEWEL_P2A_WP_ROUTE_2
						ELSE
							g_JHP2A_string_WayPoint 	= TS_JEWEL_P2A_WP_ROUTE_1
						ENDIF
						
						CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Using furthest away route: ", g_JHP2A_string_WayPoint)
					BREAK
				ENDSWITCH
				g_iForcePrepMissionRoute = -1

			ELSE
				REQUEST_WAYPOINT_RECORDING(g_JHP2A_string_WayPoint)
				IF GET_IS_WAYPOINT_RECORDING_LOADED(g_JHP2A_string_WayPoint)
					TS_JEWEL_P2A_MOVING_BLIP 				= TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_JEWELRY_P2A, g_JHP2A_string_WayPoint, TS_JEWEL_P2A_BLIP_SPEED, 0)
					TS_JEWEL_P2A_bIsActive 					= TRUE
					TS_JEWEL_P2A_bDisplayedBlipAppearHelp 	= FALSE
					TS_JEWEL_P2A_bDisplayedBlipRemovedHelp 	= FALSE
					g_JHP2A_iTriggerRunCount++
					
					CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Trigger scene has been set to active, using route \"", g_JHP2A_string_WayPoint, "\"")
					
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
		
			IF NOT TS_JEWEL_P2A_bDisplayedBlipRemovedHelp			// not shown it yet
			AND NOT TS_JEWEL_P2A_bIsActive							// blip is not active and being shown
				IF g_JHP2A_iTriggerRunCount  <= 2						// only display this message twice
					
					SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_JHP2A_MISS")
			            CASE FHS_EXPIRED
			                ADD_HELP_TO_FLOW_QUEUE("AM_H_JHP2A_MISS", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
			            BREAK
			            CASE FHS_DISPLAYED
							g_txtFlowHelpLastDisplayed = ""
							TS_JEWEL_P2A_bDisplayedBlipRemovedHelp  = TRUE
			            BREAK
			     	ENDSWITCH
				
				ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
				
					TS_JEWEL_P2A_bDisplayedBlipRemovedHelp = TRUE
					
				ENDIF
			ENDIF
			
		ENDIF
	
	// else player character isn't valid, reset trigger scene variables
	ELSE
		g_JHP2A_tod_NextTriggerTime 		= INVALID_TIMEOFDAY
		TS_JEWEL_P2A_bIsActive 				= FALSE
		g_JHP2A_string_WayPoint 			= ""
	ENDIF

	RETURN TRUE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_JEWEL_P2A_UPDATE()
	IF TS_JEWEL_P2A_bIsActive	

		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
		
			IF HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.veh[0])
				RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0])
			ENDIF
		
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
				IF NOT TS_UPDATE_MOVING_BLIP(TS_JEWEL_P2A_MOVING_BLIP)
					CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Actually van has reached end of route.")
					g_JHP2A_tod_NextTriggerTime			= INVALID_TIMEOFDAY
					TS_JEWEL_P2A_bIsActive 				= FALSE
					g_JHP2A_string_WayPoint 			= ""
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_JEWEL_P2A_AMBIENT_UPDATE()
	IF TS_JEWEL_P2A_bIsActive
		
		IF NOT TS_JEWEL_P2A_bDisplayedBlipAppearHelp				// not shown it yet

			IF g_JHP2A_iTriggerRunCount = 1
			
				TS_JEWEL_P2A_bDisplayedBlipAppearHelp 	= TRUE
			
			ELIF g_JHP2A_iTriggerRunCount <= 3					// only display this message twice
				
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_JHP2A_REAP")
		            CASE FHS_EXPIRED
		                ADD_HELP_TO_FLOW_QUEUE("AM_H_JHP2A_REAP", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
		            BREAK
		            CASE FHS_DISPLAYED
						g_txtFlowHelpLastDisplayed = ""
						TS_JEWEL_P2A_bDisplayedBlipAppearHelp = TRUE
		            BREAK
		     	ENDSWITCH
			
			ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
			
				TS_JEWEL_P2A_bDisplayedBlipAppearHelp = TRUE
				
			ENDIF
		ENDIF
	
		// When active move the blip along the route, start blocking again once the blip has reached the end of the route
		// or if the player is no longer acceptable for this mission
		IF NOT TS_UPDATE_MOVING_BLIP(TS_JEWEL_P2A_MOVING_BLIP)
		OR NOT IS_BIT_SET(g_sMissionStaticData[SP_HEIST_JEWELRY_PREP_2A].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
			// reset the time of day blocking
			g_JHP2A_tod_NextTriggerTime 		= INVALID_TIMEOFDAY
			TS_JEWEL_P2A_bIsActive 				= FALSE
			g_JHP2A_string_WayPoint 			= ""
			CPRINTLN(DEBUG_MISSION, "[TS_JEWEL_2PA] Moving blip ONLY has reached end of route OR current player character is not valid for this mission.")
		ENDIF
	ENDIF
ENDPROC
