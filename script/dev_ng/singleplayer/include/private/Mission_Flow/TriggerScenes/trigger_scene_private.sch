//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Ben Rollinson				Date: 20/06/2012		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 				Trigger Scene Handling Private Header						║
//║																				║
//║			All functions and procedures to handle off mission trigger			║
//║			scenes loading, creation, deletion, disruption and cleanup.			║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

//═════════════════════════════════╡ HEADERS ╞═══════════════════════════════════

USING "rage_builtins.sch"
USING "globals.sch"
USING "blip_control_public.sch"
USING "mission_control_public.sch"
USING "selector_public.sch"
USING "candidate_public.sch"
USING "Flow_Mission_Data_Public.sch"
USING "friend_flow_public.sch"
USING "timelapse.sch"
USING "flow_mission_trigger_public.sch"
USING "comms_control_public.sch"
USING "load_queue_public.sch"
USING "commands_graphics.sch"
USING "player_ped_public.sch"

#IF IS_DEBUG_BUILD
	USING "flow_debug_game.sch"
#ENDIF

#if USE_CLF_DLC
	USING "static_mission_data_helpCLF.sch"
#endif
#if USE_NRM_DLC
	USING "static_mission_data_helpNRM.sch"
#endif

CONST_INT TS_BIT_CAN_TRIG_BEFORE_CREATE		BIT0
CONST_INT TS_BIT_ONLY_RESET_ON_INIT			BIT1
CONST_INT TS_BIT_SPANS_MULTIPLE_TRIGGERS	BIT2

CONST_INT TS_BIT_INDEX_CAN_TRIG_BEFORE_CREATE	0
CONST_INT TS_BIT_INDEX_ONLY_RESET_ON_INIT		1
CONST_INT TS_BIT_INDEX_SPANS_MULTIPLE_TRIGGERS	2

//═════════════════════════╡ SCENE UTILITY PROCS ╞═══════════════════════════

#IF IS_DEBUG_BUILD

PROC DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(VECTOR paramTriggerPos, FLOAT paramTriggerSize)
	SWITCH g_eTriggerDebugMode
		CASE TDM_TRIGGER_TEXT
		CASE TDM_ALL_TEXT
			DRAW_DEBUG_TEXT("Trigger", <<paramTriggerPos.X-paramTriggerSize, paramTriggerPos.Y, paramTriggerPos.Z>>, 0, 0, 255, 255)
		FALLTHRU
		CASE TDM_TRIGGER
		CASE TDM_ALL
			DRAW_DEBUG_SPHERE(paramTriggerPos, paramTriggerSize, 0, 0, 255, 65)
		BREAK
	ENDSWITCH
ENDPROC
	
#ENDIF

FUNC SP_MISSIONS TRIGGER_SCENE_GET_SP_MISSION()
	RETURN g_eMissionTriggerProcessing
ENDFUNC


PROC TRIGGER_SCENE_SETUP_FRIENDLY_PED(PED_INDEX paramPed, REL_GROUP_HASH paramRelgroupPlayer)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, paramRelgroupPlayer, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, paramRelgroupPlayer)
	
	IF DOES_ENTITY_EXIST(paramPed)
		IF NOT IS_ENTITY_DEAD(paramPed)
			SET_PED_AS_ENEMY(paramPed, FALSE)
			SET_PED_CAN_BE_TARGETTED(paramPed, FALSE)
			SET_PED_SUFFERS_CRITICAL_HITS(paramPed, FALSE)
			SET_ENTITY_IS_TARGET_PRIORITY(paramPed, FALSE)
			SET_PED_CAN_RAGDOLL(paramPed, TRUE)
			SET_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_MELEE)
			SET_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_PLAYER_IMPACT)
			SET_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_BULLET_IMPACT)
			SET_PED_DIES_WHEN_INJURED(paramPed, FALSE)
			SET_PED_CAN_EVASIVE_DIVE(paramPed, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(paramPed, TRUE)
			
			SET_PED_CONFIG_FLAG(paramPed, PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			SET_PED_CONFIG_FLAG(paramPed, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(paramPed, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(paramPed, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
			SET_PED_CONFIG_FLAG(paramPed, PCF_CannotBeTargeted, TRUE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(paramPed, paramRelgroupPlayer)
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(PED_INDEX paramPed)
	CLEAR_PED_TASKS(paramPed)
	SET_PED_DIES_WHEN_INJURED(paramPed, TRUE)
	SET_PED_SUFFERS_CRITICAL_HITS(paramPed, TRUE)
	SET_PED_CAN_EVASIVE_DIVE(paramPed, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(paramPed, FALSE)
	SET_PED_CONFIG_FLAG(paramPed, PCF_DisableExplosionReactions, FALSE)
	SET_PED_CONFIG_FLAG(paramPed, PCF_RunFromFiresAndExplosions, TRUE)
	CLEAR_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_MELEE)
	CLEAR_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_PLAYER_IMPACT)
	CLEAR_RAGDOLL_BLOCKING_FLAGS(paramPed, RBF_BULLET_IMPACT)
ENDPROC


FUNC BOOL IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth)
	IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vCoords1, vCoords2, fWidth, WEAPONTYPE_GRENADELAUNCHER)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vCoords1, vCoords2, fWidth, WEAPONTYPE_RPG)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vCoords1, vCoords2, fWidth, WEAPONTYPE_GRENADE)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vCoords1, vCoords2, fWidth, WEAPONTYPE_MOLOTOV)
	OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(vCoords1, vCoords2, fWidth, WEAPONTYPE_FLARE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


STRUCT TSPedCompRequest
	PED_COMPONENT component
	INT texture
	INT palette
ENDSTRUCT


STRUCT TSPedPropRequest
	PED_PROP_POSITION position
	INT prop
	INT texture
ENDSTRUCT


PROC TS_STORE_PED_COMP_REQUEST(TSPedCompRequest &paramStruct, PED_COMPONENT paramComponent, INT paramTexture = -1, INT paramPalette = -1)
	paramStruct.component = paramComponent
	paramStruct.texture = paramTexture
	paramStruct.palette = paramPalette
ENDPROC


PROC TS_STORE_PED_PROP_REQUEST(TSPedPropRequest &paramStruct, PED_PROP_POSITION paramPosition, INT paramProp = -1, INT paramTexture = -1)
	paramStruct.position = paramPosition
	paramStruct.prop = paramProp
	paramStruct.texture = paramTexture
ENDPROC


FUNC BOOL TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(PED_INDEX &paramPedIndex, enumCharacterList paramPlayerChar, VECTOR paramPos, PED_COMP_TYPE_ENUM paramCompType = COMP_TYPE_OUTFIT, PED_COMP_NAME_ENUM paramComp = DUMMY_PED_COMP)
	IF NOT HAS_PLAYER_PED_MODEL_LOADED(paramPlayerChar)
		RETURN FALSE
	ELIF NOT DOES_ENTITY_EXIST(paramPedIndex)
		CPRINTLN(DEBUG_TRIGGER, "Creating invisible dummy player ped and beginning streaming variations.")
		
		//Copied from CREATE_PLAYER_PED_NPC_ON_FOOT.
		paramPedIndex = CREATE_PED(PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(paramPlayerChar), paramPos, 0.0, FALSE, TRUE)
		SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(paramPedIndex, FALSE)
	    RESTORE_PLAYER_PED_VARIATIONS(paramPedIndex)
	    RESTORE_PLAYER_PED_WEAPONS(paramPedIndex)
	    RESTORE_PLAYER_PED_ARMOUR(paramPedIndex)
		RESTORE_PLAYER_PED_TATTOOS(paramPedIndex)
		SET_FLOW_OUTFIT_REQUIREMENTS(paramPedIndex)
		STORE_TEMP_PLAYER_PED_ID(paramPedIndex)

		SET_BIT(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(paramPlayerChar)))
		CPRINTLN(DEBUG_TRIGGER, "Flagged selector pedID for ", GET_PLAYER_PED_STRING(paramPlayerChar), " as created for lead-in.")
		
		IF paramComp != DUMMY_PED_COMP
			SET_PED_COMP_ITEM_CURRENT_SP(paramPedIndex, paramCompType, paramComp, FALSE)
		ENDIF
		SET_ENTITY_VISIBLE(paramPedIndex, FALSE)
		FREEZE_ENTITY_POSITION(paramPedIndex, TRUE)
		SET_ENTITY_COLLISION(paramPedIndex, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(paramPedIndex, TRUE)
		RETURN FALSE
	ELIF IS_ENTITY_DEAD(paramPedIndex)
		RETURN FALSE
	ELIF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(paramPedIndex)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(PED_INDEX &paramPedIndex, enumCharacterList paramNPCChar, VECTOR paramPos, TSPedCompRequest &paramCompRequests[], TSPedPropRequest &paramPropRequests[])
	IF NOT HAS_NPC_PED_MODEL_LOADED(paramNPCChar)
		RETURN FALSE
	ELIF NOT DOES_ENTITY_EXIST(paramPedIndex)
		CPRINTLN(DEBUG_TRIGGER, "Creating invisible dummy NPC ped and beginning streaming variations.")
		CREATE_NPC_PED_ON_FOOT(paramPedIndex, paramNPCChar, paramPos, 0.0, FALSE)
		INT index
		REPEAT COUNT_OF(paramCompRequests) index
			IF paramCompRequests[index].texture != -1
				SET_PED_COMPONENT_VARIATION(paramPedIndex, 
											paramCompRequests[index].component, 
											paramCompRequests[index].texture,
											paramCompRequests[index].palette)
			ENDIF
		ENDREPEAT
		REPEAT COUNT_OF(paramPropRequests) index
			IF paramPropRequests[index].prop != -1
				SET_PED_PROP_INDEX(	paramPedIndex, 
									paramPropRequests[index].position, 
									paramPropRequests[index].prop,
									paramPropRequests[index].texture)
			ENDIF
		ENDREPEAT
		SET_ENTITY_VISIBLE(paramPedIndex, FALSE)
		FREEZE_ENTITY_POSITION(paramPedIndex, TRUE)
		SET_ENTITY_COLLISION(paramPedIndex, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(paramPedIndex, TRUE)
		RETURN FALSE
	ELIF IS_ENTITY_DEAD(paramPedIndex)
		DELETE_PED(paramPedIndex)
		RETURN FALSE
	ELIF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(paramPedIndex)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


PROC TRIGGER_SCENE_REVEAL_DUMMY_PED(PED_INDEX paramPedIndex)
	IF DOES_ENTITY_EXIST(paramPedIndex)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(paramPedIndex, FALSE)	
		SET_ENTITY_VISIBLE(paramPedIndex, TRUE)
		SET_ENTITY_COLLISION(paramPedIndex, TRUE)
		FREEZE_ENTITY_POSITION(paramPedIndex, FALSE)
	ELSE	
		SCRIPT_ASSERT("TRIGGER_SCENE_REVEAL_DUMMY_PED: Command called when dummy ped didn't already exist.")
	ENDIF
ENDPROC


PROC CLEANUP_TRIGGER_SCENE_SELECTOR_PED(PED_INDEX paramPed)
	INT iSelectorIndex
	REPEAT 3 iSelectorIndex
		IF IS_BIT_SET(g_sPlayerPedRequest.iBitsetPedInLeadIn, iSelectorIndex)
			IF g_sPlayerPedRequest.sSelectorPeds.pedID[iSelectorIndex] = paramPed
				CLEAR_BIT(g_sPlayerPedRequest.iBitsetPedInLeadIn, iSelectorIndex)
				CPRINTLN(DEBUG_TRIGGER, "Unflagged a selector pedID lead-in flag as trigger scene cleans up.")
			ENDIF
			
			IF Is_TIMEOFDAY_Valid(g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[iSelectorIndex])
				g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[iSelectorIndex] = INVALID_TIMEOFDAY
				CPRINTLN(DEBUG_TRIGGER, "Reset selector pedID last active time.")
			ENDIF
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[iSelectorIndex], <<0,0,0>>, 1.5)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[iSelectorIndex] = <<0,0,0>>
				g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[iSelectorIndex] = 0.0
				CPRINTLN(DEBUG_TRIGGER, "Reset selector pedID last known coords.")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC TRIGGER_SCENE_DELETE_PED(PED_INDEX &paramPed)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to delete ped [", NATIVE_TO_INT(paramPed), "].")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramPed)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				DELETE_PED(paramPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(PED_INDEX &paramPed)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [", NATIVE_TO_INT(paramPed), "] as no longer needed and then flee player.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramPed)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(paramPed)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(paramPed)
						IF NOT IS_PED_IN_ANY_VEHICLE(paramPed, TRUE)
							IF IS_ENTITY_ATTACHED(paramPed)
								DETACH_ENTITY(paramPed)
							ENDIF
							SET_ENTITY_COLLISION(paramPed, TRUE)
						ENDIF
						SET_ENTITY_HAS_GRAVITY(paramPed, TRUE)
						FREEZE_ENTITY_POSITION(paramPed, FALSE)
						SET_PED_FLEE_ATTRIBUTES(paramPed, FA_DISABLE_COWER, TRUE)
						SET_PED_FLEE_ATTRIBUTES(paramPed, FA_DISABLE_HANDS_UP, TRUE)
						SET_PED_FLEE_ATTRIBUTES(paramPed, FA_WANDER_AT_END, TRUE)
						CLEAR_PED_SECONDARY_TASK(paramPed)
						TASK_SMART_FLEE_PED(paramPed, PLAYER_PED_ID(), 300.0, INFINITE_TASK_TIME, TRUE)
						SET_PED_KEEP_TASK(paramPed, TRUE)
					ENDIF
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(paramPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_PED_FLEE_IN_VEHICLE(PED_INDEX &paramPed, VEHICLE_INDEX paramVehicle)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [", NATIVE_TO_INT(paramPed), "] as no longer needed and then flee in vehicle [", NATIVE_TO_INT(paramVehicle), "].")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramVehicle) AND IS_VEHICLE_DRIVEABLE(paramVehicle)
		IF DOES_ENTITY_EXIST(paramPed)
			INT iEntityInstanceID
			STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
			IF NOT IS_STRING_NULL(strEntityScript)
				IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
					IF NOT IS_ENTITY_DEAD(paramPed)
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(paramPed)
							IF NOT IS_PED_IN_ANY_VEHICLE(paramPed, TRUE)
								IF IS_ENTITY_ATTACHED(paramPed)
									DETACH_ENTITY(paramPed)
								ENDIF
								SET_ENTITY_COLLISION(paramPed, TRUE)
							ENDIF
							SET_ENTITY_HAS_GRAVITY(paramPed, TRUE)
							FREEZE_ENTITY_POSITION(paramPed, FALSE)
							SET_PED_FLEE_ATTRIBUTES(paramPed, FA_DISABLE_COWER, TRUE)
							SET_PED_FLEE_ATTRIBUTES(paramPed, FA_DISABLE_HANDS_UP, TRUE)
							SET_PED_FLEE_ATTRIBUTES(paramPed, FA_WANDER_AT_END, TRUE)
							CLEAR_PED_SECONDARY_TASK(paramPed)
							TASK_VEHICLE_MISSION_PED_TARGET(paramPed, paramVehicle, PLAYER_PED_ID(), MISSION_FLEE, 70.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 300.0, 300.0, TRUE)
							SET_PED_KEEP_TASK(paramPed, TRUE)
						ENDIF
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(paramPed)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("paramVehicle either doesn't exist or is not drivable.")
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_PED_COWER(PED_INDEX &paramPed)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [", NATIVE_TO_INT(paramPed), "] as no longer needed and then cower.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramPed)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(paramPed)
					TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(paramPed)
					IF NOT IS_PED_IN_ANY_VEHICLE(paramPed, TRUE)
						IF IS_ENTITY_ATTACHED(paramPed)
							DETACH_ENTITY(paramPed)
						ENDIF
						SET_ENTITY_COLLISION(paramPed, TRUE)
					ENDIF
					SET_ENTITY_HAS_GRAVITY(paramPed, TRUE)
					FREEZE_ENTITY_POSITION(paramPed, FALSE)
					CLEAR_PED_SECONDARY_TASK(paramPed)
					TASK_COWER(paramPed)
					SET_PED_KEEP_TASK(paramPed, TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(paramPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_PED_GENERAL(PED_INDEX &paramPed)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [", NATIVE_TO_INT(paramPed), "] as no longer needed and then wander.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramPed)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(paramPed)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(paramPed)
						SET_ENTITY_HAS_GRAVITY(paramPed, TRUE)
						FREEZE_ENTITY_POSITION(paramPed, FALSE)
						CLEAR_PED_SECONDARY_TASK(paramPed)
						IF NOT IS_PED_IN_ANY_VEHICLE(paramPed, TRUE)
							IF IS_ENTITY_ATTACHED(paramPed)
								DETACH_ENTITY(paramPed)
							ENDIF
							SET_ENTITY_COLLISION(paramPed, TRUE)
							
							TASK_WANDER_STANDARD(paramPed)
						ELSE
							SEQUENCE_INDEX seqExitAndWander
							OPEN_SEQUENCE_TASK(seqExitAndWander)
								TASK_LEAVE_ANY_VEHICLE(NULL, 0)
								TASK_WANDER_STANDARD(NULL)
							CLOSE_SEQUENCE_TASK(seqExitAndWander)
							TASK_PERFORM_SEQUENCE(paramPed, seqExitAndWander)
						ENDIF
						SET_PED_KEEP_TASK(paramPed, TRUE)
					ENDIF
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(paramPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_PED_NO_ACTION(PED_INDEX &paramPed)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [", NATIVE_TO_INT(paramPed), "] as no longer needed with no action.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(paramPed)
	IF DOES_ENTITY_EXIST(paramPed)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramPed, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(paramPed)
				IF NOT IS_PED_IN_ANY_VEHICLE(paramPed, TRUE)
					IF IS_ENTITY_ATTACHED(paramPed)
						DETACH_ENTITY(paramPed)
					ENDIF
					SET_ENTITY_COLLISION(paramPed, TRUE)
				ENDIF
				CLEAR_PED_SECONDARY_TASK(paramPed)
				SET_ENTITY_HAS_GRAVITY(paramPed, TRUE)
				FREEZE_ENTITY_POSITION(paramPed, FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(paramPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_DELETE_VEHICLE(VEHICLE_INDEX &paramVehicle)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to delete vehicle [", NATIVE_TO_INT(paramVehicle), "].")

	IF DOES_ENTITY_EXIST(paramVehicle)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramVehicle, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), paramVehicle, TRUE)
						DELETE_VEHICLE(paramVehicle)
					ELSE
						SET_VEHICLE_AS_NO_LONGER_NEEDED(paramVehicle)
					ENDIF
				ELSE
					DELETE_VEHICLE(paramVehicle)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_VEHICLE(VEHICLE_INDEX &paramVehicle)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set vehicle [", NATIVE_TO_INT(paramVehicle), "] as no longer needed .")

	IF DOES_ENTITY_EXIST(paramVehicle)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramVehicle, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(paramVehicle)
					IF IS_ENTITY_ATTACHED(paramVehicle)
						DETACH_ENTITY(paramVehicle)
					ENDIF
				ENDIF
				BLIP_INDEX blip = GET_BLIP_FROM_ENTITY(paramVehicle)
				IF blip != NULL
					REMOVE_BLIP(blip)
				ENDIF
				
				//Special case fix for Armenian3 vehicle anim.
				IF IS_ENTITY_PLAYING_ANIM(paramVehicle, "missarmenian3leadinoutArmenian_3_int", "_intro_loop_car", ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(paramVehicle, 1.0, TRUE)
				ENDIF
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(paramVehicle)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_DELETE_OBJECT(OBJECT_INDEX &paramObject)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to delete object [", NATIVE_TO_INT(paramObject), "].")
	
	IF DOES_ENTITY_EXIST(paramObject)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramObject, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				DELETE_OBJECT(paramObject)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TRIGGER_SCENE_RELEASE_OBJECT(OBJECT_INDEX &paramObject)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set object [", NATIVE_TO_INT(paramObject), "] as no longer needed .")

	IF DOES_ENTITY_EXIST(paramObject)
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(paramObject, iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF IS_ENTITY_ATTACHED(paramObject)
					DETACH_ENTITY(paramObject)
				ENDIF
				SET_OBJECT_AS_NO_LONGER_NEEDED(paramObject)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TRIGGER_SCENE_RELEASE_ROPE(ROPE_INDEX &paramRope)
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set rope [", NATIVE_TO_INT(paramRope), "] as no longer needed .")
	IF DOES_ROPE_EXIST(paramRope)
		IF DOES_SCRIPT_OWN_ROPE(paramRope)
			DELETE_ROPE(paramRope)
		ENDIF
	ENDIF
ENDPROC


PROC PROOF_TRIGGER_SCENE(SP_MISSIONS paramTriggerMission)
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramTriggerMission].settingsBitset, MF_INDEX_IS_PREP)
		INT index

		//Set scene vehicle proofs.
		REPEAT MAX_TRIG_SCENE_VEHICLES index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[index], TRUE, TRUE, TRUE, TRUE, TRUE)
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[index]), 3.75)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene ped proofs.
		REPEAT MAX_TRIG_SCENE_PEDS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.ped[index], TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_RAGDOLL_BLOCKING_FLAGS(g_sTriggerSceneAssets.ped[index], RBF_PLAYER_IMPACT) 
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[index]), 3.75)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene object proofs.
		REPEAT MAX_TRIG_SCENE_OBJECTS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.object[index], TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		//Stop grenades going off around the player.
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramTriggerMission].settingsBitset, MF_INDEX_IS_PREP)
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5.0)
		ENDIF
	
		//Deal with the player being in a vehicle.
		IF NOT IS_BIT_SET(g_sMissionStaticData[paramTriggerMission].settingsBitset, MF_INDEX_IS_PREP)
		AND NOT IS_BIT_SET(g_sMissionStaticData[paramTriggerMission].settingsBitset, MF_INDEX_NO_VEH_EXIT)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				//Bring the vehicle out of critical state if it is in one.
				IF GET_ENTITY_HEALTH(vehPlayer) < 1
					SET_ENTITY_HEALTH(vehPlayer, 1)
				ENDIF
				IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
					SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
				ENDIF
				IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
					SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
				ENDIF
				
				//Stop the vehicle.
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.25)
				
				//Get the player out.
				IF 	NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
						IF 	GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 500)
						ENDIF
					ELSE
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC CLEAR_TRIGGER_SCENE_PROOFS(SP_MISSIONS paramTriggerMission)
	IF NOT IS_BIT_SET(g_sMissionStaticData[paramTriggerMission].settingsBitset, MF_INDEX_IS_PREP)
		INT index

		//Set scene vehicle proofs.
		REPEAT MAX_TRIG_SCENE_VEHICLES index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[index], FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene ped proofs.
		REPEAT MAX_TRIG_SCENE_PEDS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.ped[index], FALSE, FALSE, FALSE, FALSE, FALSE)
					CLEAR_RAGDOLL_BLOCKING_FLAGS(g_sTriggerSceneAssets.ped[index], RBF_PLAYER_IMPACT) 
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene object proofs.
		REPEAT MAX_TRIG_SCENE_OBJECTS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.object[index], FALSE, FALSE, FALSE, FALSE, FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


//═════════════════════════╡ BLIP SCENE CONTROL PROCS ╞═══════════════════════════

//NB. Bitfield indexes defined in mission_control_private.sch
//NB. If this proc is changed need to update mirrored script in mission_control_private.sch - BenR.
PROC RESET_TRIGGER_SCENE_STATE(TRIGGER_SCENE &paramTrigScene)
	CDEBUG3LN(DEBUG_TRIGGER, 	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
								"Resetting trigger scene state.")

	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_BLIP_ACTIVE)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_SCENE_LOADING)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_SCENE_CREATED)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_HAS_TRIGGERED)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_DOING_TOD_SKIP)
	
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
#IF IS_DEBUG_BUILD
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
#ENDIF

ENDPROC


PROC SETUP_TRIGGER_SCENE(	TRIGGER_SCENE 	&paramTrigScene,
							SP_MISSIONS		paramMission,
							FLOAT			paramLoadDistance,
							FLOAT			paramUnloadDistance,
							FLOAT			paramFriendRejectDistance,
							INT				paramFriendsToRejectBits,
							FLOAT			paramBattleBuddyCallDistance = 0.0,
							INT				paramSetupSettingBits = 0)
							
	CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), "> ",
							"Setting up base data for scene.")
										
	//Store config data.
	paramTrigScene.eMission 				= paramMission
	paramTrigScene.fLoadDistance 			= paramLoadDistance
	paramTrigScene.fUnloadDistance			= paramUnloadDistance
	paramTrigScene.fFriendRejectDistance	= paramFriendRejectDistance
	paramTrigScene.iFriendsToAcceptBitset	= paramFriendsToRejectBits
	paramTrigScene.fBattleBuddyCallDistance	= paramBattleBuddyCallDistance
	
	//Update this mission's leave area distance.
	IF IS_BIT_SET(g_sMissionStaticData[paramMission].settingsBitset, MF_INDEX_HAS_LEADIN)
		g_sMissionActiveData[paramMission].leaveAreaDistance = paramLoadDistance + 5.0
		CPRINTLN(DEBUG_FLOW, "Leave area distance for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), " set to ", g_sMissionActiveData[paramMission].leaveAreaDistance, " by the mission's trigger scene.")
	ELSE
		g_sMissionActiveData[paramMission].leaveAreaDistance = 20.0
		CPRINTLN(DEBUG_FLOW, "Leave area distance for ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), " set to standard ", g_sMissionActiveData[paramMission].leaveAreaDistance, " as there is no lead-in.")
	ENDIF
	
	IF IS_BIT_SET(paramSetupSettingBits, TS_BIT_INDEX_CAN_TRIG_BEFORE_CREATE)
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), "> ",
								"Can trigger before creation.")
		SET_BIT(paramTrigScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
	ELSE
		CLEAR_BIT(paramTrigScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
	ENDIF
	IF IS_BIT_SET(paramSetupSettingBits, TS_BIT_INDEX_ONLY_RESET_ON_INIT)
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), "> ",
								"Will only reset on first initialisation.")
		SET_BIT(paramTrigScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
	ELSE
		CLEAR_BIT(paramTrigScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
	ENDIF
	IF IS_BIT_SET(paramSetupSettingBits, TS_BIT_INDEX_SPANS_MULTIPLE_TRIGGERS)
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), "> ",
								"Trigger scene spans multiple strands and will reset on triggerer initialisation.")
		SET_BIT(paramTrigScene.iStateBitset, TSS_RESET_ON_TRIGGERER_INIT)
	ELSE
		CLEAR_BIT(paramTrigScene.iStateBitset, TSS_RESET_ON_TRIGGERER_INIT)	
	ENDIF
	
	//Set initial trigger scene state.
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_BLIP_ACTIVE)
	CLEAR_BIT(paramTrigScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)

	SET_BIT(paramTrigScene.iStateBitset, TSS_SCENE_ACTIVE)
	CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMission), "> ",
							"Scene state flagged as active.")
ENDPROC


PROC CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(TRIGGER_SCENE &paramTrigScene)
	IF NOT IS_BIT_SET(paramTrigScene.iStateBitset, TSS_HAS_TRIGGERED)
		IF g_eFriendMissionZoneID = paramTrigScene.eMission
			CLEAR_FRIEND_MISSION_ZONE(paramTrigScene.eMission)
		ENDIF
		CLEAR_BIT(paramTrigScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
		CLEAR_BIT(paramTrigScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
	ENDIF
ENDPROC


PROC CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
	CPRINTLN(DEBUG_TRIGGER, "Trigger scene player locked in cleared.")
	INT index
	
	//Set scene vehicle proofs.
	REPEAT MAX_TRIG_SCENE_VEHICLES index
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
				SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[index], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Set scene ped proofs.
	REPEAT MAX_TRIG_SCENE_PEDS index
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[index])
				SET_ENTITY_PROOFS(g_sTriggerSceneAssets.ped[index], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Set scene object proofs.
	REPEAT MAX_TRIG_SCENE_OBJECTS index
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[index])
				SET_ENTITY_PROOFS(g_sTriggerSceneAssets.object[index], FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Set player proofs.
	CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
	
	g_bPlayerLockedInToTrigger = FALSE
ENDPROC


PROC TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
	CDEBUG1LN(DEBUG_TRIGGER, "Running trigger scene general scene release.")

	INT index
	REPEAT MAX_TRIG_SCENE_OBJECTS index
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
			IF IS_ENTITY_ON_SCREEN(g_sTriggerSceneAssets.object[index])
				TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[index])
			ELSE	
				TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[index])
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_VEHICLES index
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[index])
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_ROPES index
		TRIGGER_SCENE_RELEASE_ROPE(g_sTriggerSceneAssets.rope[index])
	ENDREPEAT
	REPEAT MAX_TRIG_SCENE_PEDS index
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[index])
				
				BOOL bPedVisible = TRUE
				
				IF IS_PED_TRACKED(g_sTriggerSceneAssets.ped[index])
					bPedVisible = IS_TRACKED_PED_VISIBLE(g_sTriggerSceneAssets.ped[index])
					CDEBUG1LN(DEBUG_TRIGGER, "Scene ped ", index, " was tracked for visiblity. Result was ", PICK_STRING(bPedVisible, "VISIBLE", "NOT VISIBLE"), ".")
				ENDIF
				
				IF bPedVisible
					TRIGGER_SCENE_RELEASE_PED_GENERAL(g_sTriggerSceneAssets.ped[index])
				ELSE
					TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
				ENDIF
			ELSE
				TRIGGER_SCENE_RELEASE_PED_GENERAL(g_sTriggerSceneAssets.ped[index])
			ENDIF
		ENDIF
	ENDREPEAT

	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
	
	g_bFlowCleanupIntroCutscene = TRUE
ENDPROC


PROC UNLOAD_TRIGGER_SCENE(TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers, BOOL &paramReleaseFunctionBound, BOOL paramLeavingArea = FALSE)

	CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(paramTrigScene)
	
	CLEAR_TRIGGER_SCENE_PROOFS(paramTrigScene.eMission)
	
	Set_Leave_Area_Flag_For_Mission(paramTrigScene.eMission, TRUE)
	
	IF g_bPlayerLockedInToTrigger
		CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
	ENDIF
	
	IF g_eMissionSceneToCleanup = paramTrigScene.eMission
		g_bSceneAutoTrigger = FALSE
		g_eMissionSceneToCleanup = SP_MISSION_NONE
		g_bTriggerSceneActive = FALSE
		g_bTriggerSceneBlockHelp = FALSE
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
								"Flagged as the active scene but is cleaning up. Unflagging as active scene.")
	ENDIF
	
	IF g_bTriggerSceneBlockHelp
		CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
								"Cleared help text block flag as trigger scene unloads.")
		g_bTriggerSceneBlockHelp = FALSE
	ENDIF
			
	IF paramScenePointers.bSceneBound
		IF IS_BIT_SET(paramTrigScene.iStateBitset, TSS_SCENE_CREATED)
			IF paramTrigScene.eMission = g_eRunningMission
				CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
										"Deleting scene as we are cleaning up while the scene's mission is running.")
				CALL paramScenePointers.DELETE_TRIGGER_SCENE()
			ELIF paramLeavingArea
				CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
										"Deleting scene as we are leaving the trigger scene's area.")
				CALL paramScenePointers.DELETE_TRIGGER_SCENE()
			ELSE
				CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
										"Releasing scene as we are cleaning up while off-mission.")
				TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
			ENDIF
			CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
			CLEAR_BIT(paramTrigScene.iStateBitset, TSS_SCENE_CREATED)
		ENDIF
		
		IF IS_BIT_SET(paramTrigScene.iStateBitset, TSS_SCENE_LOADING)
			CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
									"Scene cleaning up. Releasing trigger scene assets.")
			CALL paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS()
			IF NOT IS_BIT_SET(paramTrigScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
				CDEBUG3LN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
										"Running scene specific reset routine.")
				CALL paramScenePointers.RESET_TRIGGER_SCENE()
			ENDIF
			CLEAR_BIT(paramTrigScene.iStateBitset, TSS_SCENE_LOADING)
			CLEAR_BIT(paramTrigScene.iStateBitset, TSS_HAS_TRIGGERED)
			CLEAR_BIT(paramTrigScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
			CPRINTLN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
									"Local release function pointer unbound as we unload trigger scene.")
			paramReleaseFunctionBound = FALSE
			g_eMissionSceneToCleanup = SP_MISSION_NONE
			g_bMissionTriggerLoading = FALSE
			g_bTriggerSceneActive 	 = FALSE
			g_bTriggerSceneBlockHelp = false
			
			//Optimisation. Preload mission scripts for prep missions. Combats slow blipping of mission objectives.
			IF IS_BIT_SET(g_sMissionStaticData[paramTrigScene.eMission].settingsBitset, MF_INDEX_IS_PREP)
				CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
										"Unloading stat_watcher and mission script for seamless prep mission.")
				#IF USE_CLF_DLC
					REQUEST_SCRIPT(get_script_from_hash(g_sMissionStaticData[paramTrigScene.eMission].scriptHash))
					REQUEST_SCRIPT("mission_stat_watchCLF")
				#ENDIF
				#IF USE_NRM_DLC
					REQUEST_SCRIPT(get_script_from_hash_NRM(g_sMissionStaticData[paramTrigScene.eMission].scriptHash))
					REQUEST_SCRIPT("mission_stat_watchNRM")
				#ENDIF
				#IF NOT USE_SP_DLC
					SET_SCRIPT_AS_NO_LONGER_NEEDED(g_sMissionStaticData[paramTrigScene.eMission].scriptName)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("mission_stat_watcher")
				#ENDIF
				
			ENDIF
			
			CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
									"Trigger scene active flag cleared as scene cleans up.")
		ENDIF
	ELSE
		CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramTrigScene.eMission), "> ",
								"Running general release as scene is no longer bound.")
		TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
	ENDIF
	
	IF g_sPlayerPedRequest.iBitsetPedInLeadIn != 0
		CPRINTLN(DEBUG_TRIGGER, "Unflagged all selector pedID lead-in flag as trigger scene is unloaded.")
		g_sPlayerPedRequest.iBitsetPedInLeadIn = 0
	ENDIF
	
ENDPROC


FUNC VECTOR GET_TRIGGER_SCENE_BLIP_POSITION(STATIC_BLIP_NAME_ENUM paramBlip)
	IF IS_BIT_SET(g_GameBlips[paramBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[paramBlip].bMultiCoordAndSprite
		RETURN GET_STATIC_BLIP_POSITION(paramBlip, GET_CURRENT_PLAYER_PED_INT())
	ELSE
		RETURN GET_STATIC_BLIP_POSITION(paramBlip)
	ENDIF
ENDFUNC


FUNC FLOAT GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(VECTOR vTriggerPos)
	RETURN VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPos)
ENDFUNC


FUNC BOOL IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
	IF IS_PLAYER_SWITCH_IN_PROGRESS() 
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	OR Is_Player_Timetable_Scene_In_Progress()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC UPDATE_TRIGGER_SCENE_PED_TRACKING()
	INT iScenePedIndex
	REPEAT MAX_TRIG_SCENE_PEDS iScenePedIndex
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iScenePedIndex])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[iScenePedIndex])
				IF NOT IS_PED_TRACKED(g_sTriggerSceneAssets.ped[iScenePedIndex])
					CDEBUG1LN(DEBUG_TRIGGER, "Flagging trigger scene ped ", iScenePedIndex, " for visibility tracking.")
					REQUEST_PED_VISIBILITY_TRACKING(g_sTriggerSceneAssets.ped[iScenePedIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


#IF USE_CLF_DLC
FUNC BOOL DO_TRIGGER_SCENE_TRIGGER_CHECK_CLF(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	SP_MISSIONS eMissionID = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID

	//Is the player a triggerable character at the moment?
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
	
		SCRIPT_TYPE eScriptType = ST_STORY_MISSION
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_IS_PREP)
			eScriptType = ST_STORY_MISSION_SEAMLESS_PREP
		ENDIF
		
		//Watch for the player locking in.
		BOOL bTimeOutTrigger = FALSE
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_LEADIN_TIMEOUT)
			IF g_bPlayerLockedInToTrigger
				IF paramLocalVars.iLockInTimer = -1
					paramLocalVars.iLockInTimer = GET_GAME_TIMER()
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player locked in. Setting lock in timer.")
				ELIF (GET_GAME_TIMER() - paramLocalVars.iLockInTimer) > TS_LOCK_IN_TIMEOUT_TIME
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player lock in timed out. Triggering mission.")
					bTimeOutTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF IS_SCREEN_FADED_IN()
			IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_TRIGGERED() OR bTimeOutTrigger
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(eScriptType)
					IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
					OR g_eMissionSceneToCleanup = eMissionID
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene has been triggered by the player.")					
						SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
						SET_FRIEND_MISSION_ZONE(eMissionID, FRIEND_MISSION_ZONE_LAUNCHING, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
						g_flowUnsaved.bFlowProcessAllStrandsNextFrame = TRUE
						paramLocalVars.iLockInTimer = -1
						
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Turning off scene blips as the mission has triggered.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(g_sMissionStaticData[eMissionID].blip, FALSE)
						ENDIF
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
							REMOVE_BLIP(paramLocalVars.blipTODTrigger)
						ENDIF
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
						
						PROOF_TRIGGER_SCENE(eMissionID)
						
						RETURN TRUE	
	#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene requested to trigger but another trigger scene is active.")
	#ENDIF					
					ENDIF
	#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"Scene requested to trigger but ST_STORY_MISSION_TRIGGER_SCENE safety checks failed.")
	#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

#IF USE_NRM_DLC
FUNC BOOL DO_TRIGGER_SCENE_TRIGGER_CHECK_NRM(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)
	SP_MISSIONS eMissionID = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID

	//Is the player a triggerable character at the moment?
	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
	
		SCRIPT_TYPE eScriptType = ST_STORY_MISSION
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_IS_PREP)
			eScriptType = ST_STORY_MISSION_SEAMLESS_PREP
		ENDIF
		
		//Watch for the player locking in.
		BOOL bTimeOutTrigger = FALSE
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_LEADIN_TIMEOUT)
			IF g_bPlayerLockedInToTrigger
				IF paramLocalVars.iLockInTimer = -1
					paramLocalVars.iLockInTimer = GET_GAME_TIMER()
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player locked in. Setting lock in timer.")
				ELIF (GET_GAME_TIMER() - paramLocalVars.iLockInTimer) > TS_LOCK_IN_TIMEOUT_TIME
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player lock in timed out. Triggering mission.")
					bTimeOutTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF IS_SCREEN_FADED_IN()
			IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_TRIGGERED() OR bTimeOutTrigger
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(eScriptType)
					IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
					OR g_eMissionSceneToCleanup = eMissionID
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene has been triggered by the player.")					
						SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
						SET_FRIEND_MISSION_ZONE(eMissionID, FRIEND_MISSION_ZONE_LAUNCHING, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
						g_flowUnsaved.bFlowProcessAllStrandsNextFrame = TRUE
						paramLocalVars.iLockInTimer = -1
						
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Turning off scene blips as the mission has triggered.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(g_sMissionStaticData[eMissionID].blip, FALSE)
						ENDIF
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
							REMOVE_BLIP(paramLocalVars.blipTODTrigger)
						ENDIF
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
						
						PROOF_TRIGGER_SCENE(eMissionID)
						
						RETURN TRUE	
	#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene requested to trigger but another trigger scene is active.")
	#ENDIF					
					ENDIF
	#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"Scene requested to trigger but ST_STORY_MISSION_TRIGGER_SCENE safety checks failed.")
	#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL DO_TRIGGER_SCENE_TRIGGER_CHECK(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS &paramLocalVars)

#IF USE_CLF_DLC
	return DO_TRIGGER_SCENE_TRIGGER_CHECK_CLF(paramTriggerIndex,paramLocalVars)
#ENDIF

#IF USE_NRM_DLC
	return DO_TRIGGER_SCENE_TRIGGER_CHECK_NRM(paramTriggerIndex,paramLocalVars)
#ENDIF

#IF NOT USE_SP_DLC
	SP_MISSIONS eMissionID = g_TriggerableMissions[paramTriggerIndex].eMissionID

	//Is the player a triggerable character at the moment?
	IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
	
		SCRIPT_TYPE eScriptType = ST_STORY_MISSION
		IF IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_IS_PREP)
			eScriptType = ST_STORY_MISSION_SEAMLESS_PREP
		ENDIF
		
		//Watch for the player locking in.
		BOOL bTimeOutTrigger = FALSE
		IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_NO_LEADIN_TIMEOUT)
			IF g_bPlayerLockedInToTrigger
				IF paramLocalVars.iLockInTimer = -1
					paramLocalVars.iLockInTimer = GET_GAME_TIMER()
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player locked in. Setting lock in timer.")
				ELIF (GET_GAME_TIMER() - paramLocalVars.iLockInTimer) > TS_LOCK_IN_TIMEOUT_TIME
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"The player lock in timed out. Triggering mission.")
					bTimeOutTrigger = TRUE
				ENDIF
			ENDIF
		ENDIF

		IF IS_SCREEN_FADED_IN()
			IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_TRIGGERED() OR bTimeOutTrigger
				IF IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(eScriptType)
					IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
					OR g_eMissionSceneToCleanup = eMissionID
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene has been triggered by the player.")					
						SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
						SET_FRIEND_MISSION_ZONE(eMissionID, FRIEND_MISSION_ZONE_LAUNCHING, g_TriggerableMissions[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
						g_flowUnsaved.bFlowProcessAllStrandsNextFrame = TRUE
						paramLocalVars.iLockInTimer = -1
						
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Turning off scene blips as the mission has triggered.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMissionID].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(g_sMissionStaticData[eMissionID].blip, FALSE)
						ENDIF
						CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
							REMOVE_BLIP(paramLocalVars.blipTODTrigger)
						ENDIF
						CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
						
						PROOF_TRIGGER_SCENE(eMissionID)
						
						RETURN TRUE	
	#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
												"Scene requested to trigger but another trigger scene is active.")
	#ENDIF					
					ENDIF
	#IF IS_DEBUG_BUILD
				ELSE
					CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMissionID), "> ",
											"Scene requested to trigger but ST_STORY_MISSION_TRIGGER_SCENE safety checks failed.")
	#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
	
#ENDIF

ENDFUNC


FUNC BOOL DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(SP_MISSIONS eMission)

	//Dan H Special check for jewelry heist set-up timelapse.
	#IF NOT USE_SP_DLC
		IF eMission = SP_HEIST_JEWELRY_1
			if not IS_MICHAEL_WEARING_SMART_OUTFIT()
				RETURN false
			endif
		ENDIF
	#ENDIF
	
	IF g_bPlayerLockedInToTrigger
//				CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//											"No lead-in ToD allowed. The player is locked in.")	
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//											"No lead-in ToD allowed. Conversation is ongoing.")	
		RETURN FALSE
	ENDIF

	IF (g_sMissionStaticData[eMission].startHour = NULL_HOUR AND g_sMissionStaticData[eMission].endHour = NULL_HOUR)
//		CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//									"No lead-in ToD required. There is no time window for this mission.")	
		RETURN FALSE
	ENDIF
	
	IF IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[eMission].startHour, g_sMissionStaticData[eMission].endHour)
//		CDEBUG3LN(DEBUG_TRIGGER, 	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//									"No lead-in ToD required. The time is in the valid mission window.")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TOD_AUTO_START)
		CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> Auto TOD skip performed")
//											"Auto started TOD skip")
		RETURN TRUE
	ENDIF
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL 
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_M)
//				CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//											"No lead-in ToD required. The player is Michael and he isn't flagged to have one.")
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN 
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_F)
//				CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//											"No lead-in ToD required. The player is Franklin and he isn't flagged to have one.")
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_TREVOR 
			IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TRIGGER_TOD_T)
//				CDEBUG3LN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
//											"No lead-in ToD required.The player is Trevor and he isn't flagged to have one.")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH	
	CDEBUG3LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> TOD function returned true")
	RETURN TRUE
ENDFUNC


#IF USE_CLF_DLC
FUNC BOOL MANAGE_TRIGGER_SCENE_CLF(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)

	SP_MISSIONS eMission = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()

	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_PLAYER_PED_PLAYABLE(ePlayer)
		
			//Calculate distance squared from scene blip this frame.
			STATIC_BLIP_NAME_ENUM eSceneBlip = g_sMissionStaticData[eMission].blip
			FLOAT fDistanceSquaredFromTrigger = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(eMission))
			
				//Should we set the leave area flag because the player has just switched very close to a lead-in?
				IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF paramLocalVars.iSwitchTriggerCooldown > GET_GAME_TIMER()
					OR GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
						IF NOT g_sMissionActiveData[eMission].leaveArea
							FLOAT fQuarterLoadDistance = g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance*0.25
							IF fDistanceSquaredFromTrigger < (fQuarterLoadDistance*fQuarterLoadDistance)
								#IF IS_DEBUG_BUILD
									IF GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player has an end-of-mission comm queued. Setting leave area flag to avoid scene conflict.")
									ELSE
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player just switched on top of a mission lead-in. Setting leave area flag to avoid scene popping.")
									ENDIF
								#ENDIF
								g_sMissionActiveData[eMission].leaveArea = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				//Does the player need to leave the mission area before we can start managing the trigger?
				IF NOT g_sMissionActiveData[eMission].leaveArea
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Leave area flag cleared.")
							CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
					
					//Is the trigger scene logic asking for the trigger to be blocked?
					IF NOT CALL paramLocalVars.sScenePointers[paramTriggerIndex].IS_TRIGGER_SCENE_BLOCKED()
						IF g_TriggerableMissionsTU[paramTriggerIndex].bBlocked
							g_TriggerableMissionsTU[paramTriggerIndex].bBlocked = FALSE
							g_sMissionActiveData[eMission].leaveArea = TRUE
						ENDIF
					
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Scene blocking ended.")
								CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF
						
						//Blip the mission.
						IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								
								//Deactivate any active timelapse trigger blip before turning on the main blip.
								IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
									IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
										REMOVE_BLIP(paramLocalVars.blipTODTrigger)
									ENDIF
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
								ENDIF
								
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Setting scene blip visible.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, TRUE)
								ENDIF
								SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ELSE
							//Flash the blip if it is activate for this misison for the first time.
							IF NOT PRIVATE_Is_Story_Mission_First_Activated_Bit_Set(eMission)
								IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(eSceneBlip)
								AND IS_STATIC_BLIP_CURRENTLY_VISIBLE_TO_SYSTEM(eSceneBlip)
									FLASH_STATIC_BLIP(eSceneBlip)
									PRIVATE_Set_Story_Mission_First_Activated_Bit(eMission)
								ENDIF
							ENDIF
						
							IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Hiding scene blip as a timelapse trigger has activated.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
								ENDIF
								CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ENDIF
						
						//Can we trigger this scene without it being created?
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
							//Yes. Has the scene triggered?
							IF DO_TRIGGER_SCENE_TRIGGER_CHECK_CLF(paramTriggerIndex, paramLocalVars)
								RETURN TRUE
							ENDIF 
						ENDIF
						
						//Turn on and off the friend rejection mode.
						IF NOT g_bCleanupTriggerScene
						AND g_eMissionSceneToPreLoad = SP_MISSION_NONE
							IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
								IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Entered friend rejection area. Toggling on reject mode.")
										
										SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_REJECT, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
										SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ELSE
									IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance + 30.0)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Left friend rejection area. Toggling off reject mode.")
										
										CLEAR_FRIEND_MISSION_ZONE(eMission)
										CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Turn on and off the battle buddy call zone.
						IF g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance != 0.0
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Entered battle buddy call area. Toggling on ability to call battle buddies.")

									SET_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].sScene.eMission, FRIEND_MISSION_ZONE_CALL, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
									SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ELSE
								IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance + 30.0)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Left battle buddy call area. Toggling off ability to call battle buddies.")

									CLEAR_FRIEND_MISSION_ZONE(eMission)
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ENDIF
						ENDIF
						
						//Player range checks.
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
							
							//Is the player close enough to stream in and create the scene?
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
							
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance)
								
									IF NOT g_bBlockTriggerSceneLoadDuringSwitch
										IF NOT g_bTriggerSceneActive
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Requesting assets.")
						
											CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
											
											//Optimisation. Preload mission scripts for prep missions. Combats slow blipping of mission objectives.
											IF IS_BIT_SET(g_sMissionStaticData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].settingsBitset, MF_INDEX_IS_PREP)
												CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), "> ",
																		"Loading stat_watcher and mission script for seamless prep mission.")
												
													REQUEST_SCRIPT(get_script_from_hash(g_sMissionStaticData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].scriptHash))
													REQUEST_SCRIPT("mission_stat_watchCLF")
												
											ENDIF
											
											//Store a global reference to the cleanup for this scene so
											//the mission triggerer can clean it up if necessary.
											g_eMissionSceneToCleanup = eMission
											paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
											paramLocalVars.bReleaseFunctionBound = TRUE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Local release function pointer bound as scene naturally starts loading.")
											
											SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
											g_bMissionTriggerLoading = TRUE
											IF NOT g_bTriggerSceneActive
												CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Trigger scene active flag set this frame.")
												g_bTriggerSceneActive = TRUE
											ENDIF
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Scene blocked from loading due to in progress switch.")
										IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Switch loading block is set and no switch is in progress. Clearing flag.")
											g_bBlockTriggerSceneLoadDuringSwitch = FALSE
										ENDIF
									ENDIF
								ENDIF
								
							ELIF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
								
								//Continue to request loading in case conditions changed since last frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
								
								//Back out mid load due to switch in progress block.
								IF g_bBlockTriggerSceneLoadDuringSwitch
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene loading blocked for a switch during load. Unloading scene.")
									UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
									RETURN FALSE
								ENDIF
								
								IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
								
									//Are we a valid character to create this scene for?
									IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
									
										//Are we waiting for a TOD skip to finish?
										IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)

											IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												//Does this mission need to do a timelapse skip at the moment?
												IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Scene flagged to do a timelapse.")
													SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												ELSE
													IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
														//Inside valid hours or no TOD skip to do. Create the scene.
														CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																				"Creating scene.")
																						
														CALL paramLocalVars.sScenePointers[paramTriggerIndex].CREATE_TRIGGER_SCENE()
														
														g_bMissionTriggerLoading = FALSE
														SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
														
														IF g_eFriendMissionZoneState = FRIEND_MISSION_ZONE_OFF
															SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_ON, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
														ENDIF
														
														IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
															CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																					"Cleaning up TOD cutscene.")
															SET_TODS_CUTSCENE_RUNNING(paramLocalVars.sTimeOfDay, FALSE)
															structTimelapse sNewTimelapse
															paramLocalVars.sTimeOfDay = sNewTimelapse
														
															IF NOT IS_SCREEN_FADED_IN()
															AND NOT IS_SCREEN_FADING_IN()
																DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
															ENDIF
															CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
														ENDIF

														//Check if we can trigger immediately after creation.
														IF DO_TRIGGER_SCENE_TRIGGER_CHECK_CLF(paramTriggerIndex, paramLocalVars)
															RETURN TRUE
														ENDIF
													ELSE
														CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
													ENDIF
												ENDIF
											ELSE
												//We need to do a time of day skip. Wait for the player to hit the TOD trigger.
												VECTOR vTODTrigger = GET_MISSION_TOD_TRIGGER_POSITION(eMission)
												FLOAT fTODDistanceSquared = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(vTODTrigger)
												FLOAT fTODTriggerRadius = GET_MISSION_TOD_TRIGGER_RADIUS(eMission)

												//Activate the timelapse trigger blip if it isn't already.
												IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Activating a timelapse trigger blip.")
													IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
														REMOVE_BLIP(paramLocalVars.blipTODTrigger)
													ENDIF
													paramLocalVars.blipTODTrigger = CREATE_BLIP_FOR_COORD(vTODTrigger)
													SET_BLIP_SPRITE(paramLocalVars.blipTODTrigger, GET_STATIC_BLIP_ICON(eSceneBlip))
													IF NOT IS_STRING_NULL_OR_EMPTY(g_GameBlips[eSceneBlip].txtBlipName)
														SET_BLIP_NAME_FROM_TEXT_FILE(paramLocalVars.blipTODTrigger, g_GameBlips[eSceneBlip].txtBlipName)
													ENDIF
													SET_BLIP_HIGH_DETAIL(paramLocalVars.blipTODTrigger, TRUE)
													SET_BLIP_AS_SHORT_RANGE(paramLocalVars.blipTODTrigger, FALSE)
													SET_BLIP_DISPLAY(paramLocalVars.blipTODTrigger, DISPLAY_BOTH)
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_MICHAEL)		BREAK
														CASE CHAR_FRANKLIN	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_FRANKLIN)	BREAK
														CASE CHAR_TREVOR	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_TREVOR)		BREAK
													ENDSWITCH
													SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
												ENDIF
												
												DRAW_MARKER_FOR_TRIGGER_LOCATION(vTODTrigger)

												IF fTODDistanceSquared < fTODTriggerRadius*fTODTriggerRadius
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Player walked into TOD trigger.")
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Force cleaning up random events.")						
													FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
													CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
													
													//Vehicle management on trigger being hit.
													IF IS_PLAYER_PLAYING(PLAYER_ID())
														IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
															IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
																//Bring the vehicle out of critical state if it is in one.
																IF GET_ENTITY_HEALTH(vehPlayer) < 1
																	SET_ENTITY_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
																ENDIF
																IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_NO_VEH_STOP)																
																	IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
																		BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF

													//Check what behaviour we want when we hit a TOD trigger for this mission.
													IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TOD_AUTO_START)
														//Mission flagged to auto start when hitting TOD trigger.
														RETURN TRUE
													ELSE
														//Need to do a timelapse cutscene as we hit TOD trigger.
														SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
													ENDIF	
												ENDIF
												#IF IS_DEBUG_BUILD
													IF g_eTriggerDebugMode = TDM_TRIGGER
													OR g_eTriggerDebugMode = TDM_ALL
													OR g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_SPHERE(vTODTrigger, fTODTriggerRadius, 0, 0, 255, 65)
													ENDIF
													IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_TEXT("ToD Trigger", <<vTODTrigger.X-fTODTriggerRadius, vTODTrigger.Y, vTODTrigger.Z>>, 0, 0, 255, 255)
													ENDIF
												#ENDIF
											ENDIF
										//TOD trigger hit, do the timelapse.
										ELSE
											//If we are in valid hours hours we need to force the cutscene to play anyway.
											BOOL bForceToD = FALSE
											IF IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[eMission].startHour, g_sMissionStaticData[eMission].endHour)
												bForceToD = TRUE
											ENDIF
										
											IF DO_TIMELAPSE(eMission, paramLocalVars.sTimeOfDay, bForceToD, TRUE, FALSE, TRUE, FALSE)
												CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Timelapse finished. Resetting timelapse state.")

												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_MISSION_TOD_END_POSITION(eMission))
													SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_MISSION_TOD_END_HEADING(eMission))
													IF GET_MISSION_TOD_WALK_PLAYER_AT_END(eMission)
														IF IS_PLAYER_PLAYING(PLAYER_ID())
															SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
														ENDIF
														FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
													ENDIF
													FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
													SET_GAMEPLAY_CAM_RELATIVE_HEADING()
													SET_GAMEPLAY_CAM_RELATIVE_PITCH()
												ENDIF
												
												CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
												SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							//The scene is created.
							ELSE
								IF NOT g_bTriggerSceneActive
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag set this frame.")
									g_bTriggerSceneActive = TRUE
								ENDIF
								
								//Draw the trigger scene marker if the blip is visible.
								IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
									IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
										IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
											BOOL bDrawMarker = TRUE
											IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
												IF IS_BIT_SET(g_iExtraMissionFlags[eMission], MF_EXTRA_INDEX_FORCE_TRIGGER_CORONA)
													bDrawMarker = TRUE
												ELSE
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL
															IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_FORCE_CORONA_M)
																bDrawMarker = FALSE
															ENDIF
														BREAK
														CASE CHAR_FRANKLIN
														CASE CHAR_TREVOR
															bDrawMarker = FALSE
														BREAK
													ENDSWITCH
												ENDIF
											ENDIF
											IF bDrawMarker
												IF NOT g_bPlayerLockedInToTrigger
													DRAW_MARKER_FOR_TRIGGER_LOCATION(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Set context help blocks if the player is close enough.																				
								IF fDistanceSquaredFromTrigger < 56.25 //7.5^2
									IF NOT g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Set help text block flag as the player is less than 7.5m from a mission blip.")
								 		g_bTriggerSceneBlockHelp = TRUE
									ENDIF
								ELSE
									IF g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Cleared help text block flag as the player is more than 7.5m from a mission blip.")
										g_bTriggerSceneBlockHelp = FALSE
									ENDIF
								ENDIF
								
								//Run the scene's update.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].UPDATE_TRIGGER_SCENE()
								
								//If any trigger scene peds exist, flag them for hardware visibility checks.
								UPDATE_TRIGGER_SCENE_PED_TRACKING()
								
								//Check for going out of hours or switching to a different character while the scene is created.
								IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
								OR NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
								OR IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
									#IF IS_DEBUG_BUILD
										IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to going out of hours.")
										ELIF IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to a switch being in progress.")
										ELSE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to switching to invalid ped.")
										ENDIF
									#ENDIF
								
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
									TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
									
									IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Running scene reset.")
										CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
									ENDIF

									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag cleared.")
									g_bTriggerSceneActive = FALSE
							
								//Check for it being disrupted.
								ELIF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_DISRUPTED()
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene disrupted. Deactivating trigger, releasing assets, and flagging leave area/scene flags.")
									
									//Display help to tell the player what's happened as the scene is disrupted.
									IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
										ADD_HELP_TO_FLOW_QUEUE("AM_H_DISRU", FHP_HIGH, 0, 10000, DEFAULT_HELP_TEXT_TIME, 7)
									ENDIF
									
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in due to scene disruption.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE()
									CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
									CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
									
									IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Running scene reset after disruption.")
										CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
									ENDIF

									Set_Leave_Area_Flag_For_Mission(eMission, TRUE)
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag cleared as trigger is disrupted.")
									g_bTriggerSceneActive = FALSE

								//Does this scene need to be created to be triggered?
								ELIF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
									//Yes, check for triggering here.
									IF DO_TRIGGER_SCENE_TRIGGER_CHECK_CLF(paramTriggerIndex, paramLocalVars)
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Trigger scene active flag cleared as trigger fires.")
										g_bTriggerSceneActive = FALSE
										RETURN TRUE
									ENDIF 
								ENDIF
								
							ENDIF
						ENDIF

						//Render the streaming ranges if trigger debug is flagged on.
						#IF IS_DEBUG_BUILD
							IF g_eTriggerDebugMode != TDM_OFF
								VECTOR vTriggerSceneCenter
								IF IS_BIT_SET(g_GameBlips[eSceneBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eSceneBlip].bMultiCoordAndSprite
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip, GET_CURRENT_PLAYER_PED_INT())
								ELSE
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip)
								ENDIF
								IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), vTriggerSceneCenter, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD 
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance, 0, 255, 0, 50)
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance, 127, 127, 0, 75)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Unload", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
									DRAW_DEBUG_TEXT("Load", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance, vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance, 255, 0, 0, 100)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Friend reject", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance, 255, 255, 0, 40)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("BattleBuddy call", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
							ENDIF
						#ENDIF
					ELSE
						//Scene blocked.
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Setting scene blip invisible due to blocking.")
							IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
								SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
							ENDIF
							IF g_bPlayerLockedInToTrigger
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Clearing player lock-in due to scene being blocked.")
								CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
							ENDIF
							CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						ENDIF

						IF NOT g_TriggerableMissionsTU[paramTriggerIndex].bBlocked
							g_TriggerableMissionsTU[paramTriggerIndex].bBlocked = TRUE
						ENDIF
						
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					
						#IF IS_DEBUG_BUILD
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Won't setup trigger scene as scene logic has requested it be blocked.")
								SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF	
					ENDIF
				ELSE
					//Waiting for player to leave area.
					IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE) 
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Setting scene blip invisible due to leave area flag.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
						ENDIF
						CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
					ENDIF
					
					IF g_eMissionSceneToCleanup = g_TriggerableMissionsTU[paramTriggerIndex].sScene.eMission
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as the leave area flag was set while it was active.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Won't setup trigger scene until leave area flag is cleared.")
							SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
				IF g_eMissionSceneToCleanup != SP_MISSION_NONE
					IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as a different mission has started.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
				ENDIF
			ENDIF
			
			//Is the player far enough away to delete and release the scene?
			IF g_eRunningMission != g_eMissionSceneToCleanup
				IF g_eMissionSceneToCleanup = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance)
							CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Player left streaming range for mission trigger. Cleaning up.")
							UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("MANAGE_TRIGGER_SCENE: A mission trigger tried to manage a trigger scene that was not setup. Bug BenR.")
	ENDIF
		
	RETURN FALSE
ENDFUNC
#ENDIF

#IF USE_NRM_DLC
FUNC BOOL MANAGE_TRIGGER_SCENE_NRM(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS_TU &paramLocalVars)

	SP_MISSIONS eMission = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()

	IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_PLAYER_PED_PLAYABLE(ePlayer)
		
			//Calculate distance squared from scene blip this frame.
			STATIC_BLIP_NAME_ENUM eSceneBlip = g_sMissionStaticData[eMission].blip
			FLOAT fDistanceSquaredFromTrigger = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(eMission))
			
				//Should we set the leave area flag because the player has just switched very close to a lead-in?
				IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF paramLocalVars.iSwitchTriggerCooldown > GET_GAME_TIMER()
					OR GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
						IF NOT g_sMissionActiveData[eMission].leaveArea
							FLOAT fQuarterLoadDistance = g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance*0.25
							IF fDistanceSquaredFromTrigger < (fQuarterLoadDistance*fQuarterLoadDistance)
								#IF IS_DEBUG_BUILD
									IF GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player has an end-of-mission comm queued. Setting leave area flag to avoid scene conflict.")
									ELSE
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player just switched on top of a mission lead-in. Setting leave area flag to avoid scene popping.")
									ENDIF
								#ENDIF
								g_sMissionActiveData[eMission].leaveArea = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				//Does the player need to leave the mission area before we can start managing the trigger?
				IF NOT g_sMissionActiveData[eMission].leaveArea
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Leave area flag cleared.")
							CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
					
					//Is the trigger scene logic asking for the trigger to be blocked?
					IF NOT CALL paramLocalVars.sScenePointers[paramTriggerIndex].IS_TRIGGER_SCENE_BLOCKED()
						IF g_TriggerableMissionsTU[paramTriggerIndex].bBlocked
							g_TriggerableMissionsTU[paramTriggerIndex].bBlocked = FALSE
							g_sMissionActiveData[eMission].leaveArea = TRUE
						ENDIF
					
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Scene blocking ended.")
								CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF
						
						//Blip the mission.
						IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								
								//Deactivate any active timelapse trigger blip before turning on the main blip.
								IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
									IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
										REMOVE_BLIP(paramLocalVars.blipTODTrigger)
									ENDIF
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
								ENDIF
								
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Setting scene blip visible.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, TRUE)
								ENDIF
								SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ELSE
							//Flash the blip if it is activate for this misison for the first time.
							IF NOT PRIVATE_Is_Story_Mission_First_Activated_Bit_Set(eMission)
								IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(eSceneBlip)
								AND IS_STATIC_BLIP_CURRENTLY_VISIBLE_TO_SYSTEM(eSceneBlip)
									FLASH_STATIC_BLIP(eSceneBlip)
									PRIVATE_Set_Story_Mission_First_Activated_Bit(eMission)
								ENDIF
							ENDIF
						
							IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Hiding scene blip as a timelapse trigger has activated.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
								ENDIF
								CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ENDIF
						
						//Can we trigger this scene without it being created?
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
							//Yes. Has the scene triggered?
							IF DO_TRIGGER_SCENE_TRIGGER_CHECK_NRM(paramTriggerIndex, paramLocalVars)
								RETURN TRUE
							ENDIF 
						ENDIF
						
						//Turn on and off the friend rejection mode.
						IF NOT g_bCleanupTriggerScene
						AND g_eMissionSceneToPreLoad = SP_MISSION_NONE
							IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
								IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Entered friend rejection area. Toggling on reject mode.")
										
										SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_REJECT, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
										SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ELSE
									IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance + 30.0)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Left friend rejection area. Toggling off reject mode.")
										
										CLEAR_FRIEND_MISSION_ZONE(eMission)
										CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Turn on and off the battle buddy call zone.
						IF g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance != 0.0
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Entered battle buddy call area. Toggling on ability to call battle buddies.")

									SET_FRIEND_MISSION_ZONE(g_TriggerableMissionsTU[paramTriggerIndex].sScene.eMission, FRIEND_MISSION_ZONE_CALL, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
									SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ELSE
								IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance + 30.0)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Left battle buddy call area. Toggling off ability to call battle buddies.")

									CLEAR_FRIEND_MISSION_ZONE(eMission)
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ENDIF
						ENDIF
						
						//Player range checks.
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
							
							//Is the player close enough to stream in and create the scene?
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
							
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance)
								
									IF NOT g_bBlockTriggerSceneLoadDuringSwitch
										IF NOT g_bTriggerSceneActive
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Requesting assets.")
						
											CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
											
											//Optimisation. Preload mission scripts for prep missions. Combats slow blipping of mission objectives.
											IF IS_BIT_SET(g_sMissionStaticData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].settingsBitset, MF_INDEX_IS_PREP)
												CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissionsTU[paramTriggerIndex].eMissionID), "> ",
																		"Loading stat_watcher and mission script for seamless prep mission.")	
												REQUEST_SCRIPT(get_script_from_hash_NRM(g_sMissionStaticData[g_TriggerableMissionsTU[paramTriggerIndex].eMissionID].scriptHash))
												REQUEST_SCRIPT("mission_stat_watchNRM")												
											ENDIF
											
											//Store a global reference to the cleanup for this scene so
											//the mission triggerer can clean it up if necessary.
											g_eMissionSceneToCleanup = eMission
											paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
											paramLocalVars.bReleaseFunctionBound = TRUE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Local release function pointer bound as scene naturally starts loading.")
											
											SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
											g_bMissionTriggerLoading = TRUE
											IF NOT g_bTriggerSceneActive
												CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Trigger scene active flag set this frame.")
												g_bTriggerSceneActive = TRUE
											ENDIF
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Scene blocked from loading due to in progress switch.")
										IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Switch loading block is set and no switch is in progress. Clearing flag.")
											g_bBlockTriggerSceneLoadDuringSwitch = FALSE
										ENDIF
									ENDIF
								ENDIF
								
							ELIF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
								
								//Continue to request loading in case conditions changed since last frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
								
								//Back out mid load due to switch in progress block.
								IF g_bBlockTriggerSceneLoadDuringSwitch
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene loading blocked for a switch during load. Unloading scene.")
									UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
									RETURN FALSE
								ENDIF
								
								IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
								
									//Are we a valid character to create this scene for?
									IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
									
										//Are we waiting for a TOD skip to finish?
										IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)

											IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												//Does this mission need to do a timelapse skip at the moment?
												IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Scene flagged to do a timelapse.")
													SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												ELSE
													IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
														//Inside valid hours or no TOD skip to do. Create the scene.
														CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																				"Creating scene.")
																						
														CALL paramLocalVars.sScenePointers[paramTriggerIndex].CREATE_TRIGGER_SCENE()
														
														g_bMissionTriggerLoading = FALSE
														SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
														
														IF g_eFriendMissionZoneState = FRIEND_MISSION_ZONE_OFF
															SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_ON, g_TriggerableMissionsTU[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
														ENDIF
														
														IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
															CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																					"Cleaning up TOD cutscene.")
															SET_TODS_CUTSCENE_RUNNING(paramLocalVars.sTimeOfDay, FALSE)
															structTimelapse sNewTimelapse
															paramLocalVars.sTimeOfDay = sNewTimelapse
														
															IF NOT IS_SCREEN_FADED_IN()
															AND NOT IS_SCREEN_FADING_IN()
																DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
															ENDIF
															CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
														ENDIF

														//Check if we can trigger immediately after creation.
														IF DO_TRIGGER_SCENE_TRIGGER_CHECK_NRM(paramTriggerIndex, paramLocalVars)
															RETURN TRUE
														ENDIF
													ELSE
														CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
													ENDIF
												ENDIF
											ELSE
												//We need to do a time of day skip. Wait for the player to hit the TOD trigger.
												VECTOR vTODTrigger = GET_MISSION_TOD_TRIGGER_POSITION(eMission)
												FLOAT fTODDistanceSquared = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(vTODTrigger)
												FLOAT fTODTriggerRadius = GET_MISSION_TOD_TRIGGER_RADIUS(eMission)

												//Activate the timelapse trigger blip if it isn't already.
												IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Activating a timelapse trigger blip.")
													IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
														REMOVE_BLIP(paramLocalVars.blipTODTrigger)
													ENDIF
													paramLocalVars.blipTODTrigger = CREATE_BLIP_FOR_COORD(vTODTrigger)
													SET_BLIP_SPRITE(paramLocalVars.blipTODTrigger, GET_STATIC_BLIP_ICON(eSceneBlip))
													IF NOT IS_STRING_NULL_OR_EMPTY(g_GameBlips[eSceneBlip].txtBlipName)
														SET_BLIP_NAME_FROM_TEXT_FILE(paramLocalVars.blipTODTrigger, g_GameBlips[eSceneBlip].txtBlipName)
													ENDIF
													SET_BLIP_HIGH_DETAIL(paramLocalVars.blipTODTrigger, TRUE)
													SET_BLIP_AS_SHORT_RANGE(paramLocalVars.blipTODTrigger, FALSE)
													SET_BLIP_DISPLAY(paramLocalVars.blipTODTrigger, DISPLAY_BOTH)
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_MICHAEL)		BREAK
														CASE CHAR_FRANKLIN	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_FRANKLIN)	BREAK
														CASE CHAR_TREVOR	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_TREVOR)		BREAK
													ENDSWITCH
													SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
												ENDIF
												
												DRAW_MARKER_FOR_TRIGGER_LOCATION(vTODTrigger)

												IF fTODDistanceSquared < fTODTriggerRadius*fTODTriggerRadius
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Player walked into TOD trigger.")
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Force cleaning up random events.")						
													FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
													CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
													
													//Vehicle management on trigger being hit.
													IF IS_PLAYER_PLAYING(PLAYER_ID())
														IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
															IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
																//Bring the vehicle out of critical state if it is in one.
																IF GET_ENTITY_HEALTH(vehPlayer) < 1
																	SET_ENTITY_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
																ENDIF
																IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_NO_VEH_STOP)
																#if not USE_SP_DLC
																OR (eMission = SP_MISSION_EXILE_2 AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
																#endif
																	IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
																		BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF

													//Check what behaviour we want when we hit a TOD trigger for this mission.
													IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TOD_AUTO_START)
														//Mission flagged to auto start when hitting TOD trigger.
														RETURN TRUE
													ELSE
														//Need to do a timelapse cutscene as we hit TOD trigger.
														SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
													ENDIF	
												ENDIF
												#IF IS_DEBUG_BUILD
													IF g_eTriggerDebugMode = TDM_TRIGGER
													OR g_eTriggerDebugMode = TDM_ALL
													OR g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_SPHERE(vTODTrigger, fTODTriggerRadius, 0, 0, 255, 65)
													ENDIF
													IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_TEXT("ToD Trigger", <<vTODTrigger.X-fTODTriggerRadius, vTODTrigger.Y, vTODTrigger.Z>>, 0, 0, 255, 255)
													ENDIF
												#ENDIF
											ENDIF
										//TOD trigger hit, do the timelapse.
										ELSE
											//If we are in valid hours hours we need to force the cutscene to play anyway.
											BOOL bForceToD = FALSE
											IF IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[eMission].startHour, g_sMissionStaticData[eMission].endHour)
												bForceToD = TRUE
											ENDIF
										
											IF DO_TIMELAPSE(eMission, paramLocalVars.sTimeOfDay, bForceToD, TRUE, FALSE, TRUE, FALSE)
												CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Timelapse finished. Resetting timelapse state.")

												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_MISSION_TOD_END_POSITION(eMission))
													SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_MISSION_TOD_END_HEADING(eMission))
													IF GET_MISSION_TOD_WALK_PLAYER_AT_END(eMission)
														IF IS_PLAYER_PLAYING(PLAYER_ID())
															SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
														ENDIF
														FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
													ENDIF
													FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
													SET_GAMEPLAY_CAM_RELATIVE_HEADING()
													SET_GAMEPLAY_CAM_RELATIVE_PITCH()
												ENDIF
												
												CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
												SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							//The scene is created.
							ELSE
								IF NOT g_bTriggerSceneActive
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag set this frame.")
									g_bTriggerSceneActive = TRUE
								ENDIF
								
								//Draw the trigger scene marker if the blip is visible.
								IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
									IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
										IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
											BOOL bDrawMarker = TRUE
											IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
												IF IS_BIT_SET(g_iExtraMissionFlags[eMission], MF_EXTRA_INDEX_FORCE_TRIGGER_CORONA)
													bDrawMarker = TRUE
												ELSE
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL
															IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_FORCE_CORONA_M)
																bDrawMarker = FALSE
															ENDIF
														BREAK
														CASE CHAR_FRANKLIN
														CASE CHAR_TREVOR
															bDrawMarker = FALSE
														BREAK
													ENDSWITCH
												ENDIF
											ENDIF
											IF bDrawMarker
												IF NOT g_bPlayerLockedInToTrigger
													DRAW_MARKER_FOR_TRIGGER_LOCATION(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Set context help blocks if the player is close enough.																				
								IF fDistanceSquaredFromTrigger < 56.25 //7.5^2
									IF NOT g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Set help text block flag as the player is less than 7.5m from a mission blip.")
								 		g_bTriggerSceneBlockHelp = TRUE
									ENDIF
								ELSE
									IF g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Cleared help text block flag as the player is more than 7.5m from a mission blip.")
										g_bTriggerSceneBlockHelp = FALSE
									ENDIF
								ENDIF
								
								//Run the scene's update.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].UPDATE_TRIGGER_SCENE()
								
								//If any trigger scene peds exist, flag them for hardware visibility checks.
								UPDATE_TRIGGER_SCENE_PED_TRACKING()
								
								//Check for going out of hours or switching to a different character while the scene is created.
								IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
								OR NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
								OR IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
									#IF IS_DEBUG_BUILD
										IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to going out of hours.")
										ELIF IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to a switch being in progress.")
										ELSE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to switching to invalid ped.")
										ENDIF
									#ENDIF
								
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
									TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
									
									IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Running scene reset.")
										CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
									ENDIF

									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag cleared.")
									g_bTriggerSceneActive = FALSE
							
								//Check for it being disrupted.
								ELIF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_DISRUPTED()
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene disrupted. Deactivating trigger, releasing assets, and flagging leave area/scene flags.")
									
									//Display help to tell the player what's happened as the scene is disrupted.
									IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
										ADD_HELP_TO_FLOW_QUEUE("AM_H_DISRU", FHP_HIGH, 0, 10000, DEFAULT_HELP_TEXT_TIME, 7)
									ENDIF
									
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in due to scene disruption.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE()
									CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
									CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
									CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
									
									IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Running scene reset after disruption.")
										CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
									ENDIF

									Set_Leave_Area_Flag_For_Mission(eMission, TRUE)
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag cleared as trigger is disrupted.")
									g_bTriggerSceneActive = FALSE

								//Does this scene need to be created to be triggered?
								ELIF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
									//Yes, check for triggering here.
									IF DO_TRIGGER_SCENE_TRIGGER_CHECK_NRM(paramTriggerIndex, paramLocalVars)
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Trigger scene active flag cleared as trigger fires.")
										g_bTriggerSceneActive = FALSE
										RETURN TRUE
									ENDIF 
								ENDIF
								
							ENDIF
						ENDIF

						//Render the streaming ranges if trigger debug is flagged on.
						#IF IS_DEBUG_BUILD
							IF g_eTriggerDebugMode != TDM_OFF
								VECTOR vTriggerSceneCenter
								IF IS_BIT_SET(g_GameBlips[eSceneBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eSceneBlip].bMultiCoordAndSprite
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip, GET_CURRENT_PLAYER_PED_INT())
								ELSE
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip)
								ENDIF
								IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), vTriggerSceneCenter, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD 
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance, 0, 255, 0, 50)
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance, 127, 127, 0, 75)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Unload", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
									DRAW_DEBUG_TEXT("Load", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fLoadDistance, vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance, 255, 0, 0, 100)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Friend reject", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fFriendRejectDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance, 255, 255, 0, 40)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("BattleBuddy call", <<vTriggerSceneCenter.X-g_TriggerableMissionsTU[paramTriggerIndex].sScene.fBattleBuddyCallDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
							ENDIF
						#ENDIF
					ELSE
						//Scene blocked.
						IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Setting scene blip invisible due to blocking.")
							IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
								SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
							ENDIF
							IF g_bPlayerLockedInToTrigger
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Clearing player lock-in due to scene being blocked.")
								CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
							ENDIF
							CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						ENDIF

						IF NOT g_TriggerableMissionsTU[paramTriggerIndex].bBlocked
							g_TriggerableMissionsTU[paramTriggerIndex].bBlocked = TRUE
						ENDIF
						
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					
						#IF IS_DEBUG_BUILD
							IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Won't setup trigger scene as scene logic has requested it be blocked.")
								SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF	
					ENDIF
				ELSE
					//Waiting for player to leave area.
					IF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE) 
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Setting scene blip invisible due to leave area flag.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
						ENDIF
						CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissionsTU[paramTriggerIndex].sScene)
						CLEAR_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
					ENDIF
					
					IF g_eMissionSceneToCleanup = g_TriggerableMissionsTU[paramTriggerIndex].sScene.eMission
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as the leave area flag was set while it was active.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Won't setup trigger scene until leave area flag is cleared.")
							SET_BIT(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
				IF g_eMissionSceneToCleanup != SP_MISSION_NONE
					IF NOT IS_BIT_SET(g_TriggerableMissionsTU[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as a different mission has started.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
				ENDIF
			ENDIF
			
			//Is the player far enough away to delete and release the scene?
			IF g_eRunningMission != g_eMissionSceneToCleanup
				IF g_eMissionSceneToCleanup = g_TriggerableMissionsTU[paramTriggerIndex].eMissionID
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF fDistanceSquaredFromTrigger > (g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance*g_TriggerableMissionsTU[paramTriggerIndex].sScene.fUnloadDistance)
							CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Player left streaming range for mission trigger. Cleaning up.")
							UNLOAD_TRIGGER_SCENE(g_TriggerableMissionsTU[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("MANAGE_TRIGGER_SCENE_NRM: A mission trigger tried to manage a trigger scene that was not setup. Bug BenR.")
	ENDIF
		
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL MANAGE_TRIGGER_SCENE(INT paramTriggerIndex, MISSION_TRIGGERER_LOCAL_VARS &paramLocalVars)

#IF USE_CLF_DLC
	RETURN MANAGE_TRIGGER_SCENE_CLF(paramTriggerIndex,paramLocalVars)
#ENDIF

#IF USE_NRM_DLC
	RETURN MANAGE_TRIGGER_SCENE_NRM(paramTriggerIndex,paramLocalVars)
#ENDIF

#IF NOT USE_SP_DLC
	SP_MISSIONS eMission = g_TriggerableMissions[paramTriggerIndex].eMissionID
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()

	IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_ACTIVE)
		IF IS_PLAYER_PED_PLAYABLE(ePlayer)
		
			//Calculate distance squared from scene blip this frame.
			STATIC_BLIP_NAME_ENUM eSceneBlip = g_sMissionStaticData[eMission].blip
			FLOAT fDistanceSquaredFromTrigger = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(GET_SP_MISSION_TRIGGER_TYPE(eMission))
			
				//Should we set the leave area flag because the player has just switched very close to a lead-in?
				IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
					IF paramLocalVars.iSwitchTriggerCooldown > GET_GAME_TIMER()
					OR GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
						IF NOT g_sMissionActiveData[eMission].leaveArea
							FLOAT fQuarterLoadDistance = g_TriggerableMissions[paramTriggerIndex].sScene.fLoadDistance*0.25
							IF fDistanceSquaredFromTrigger < (fQuarterLoadDistance*fQuarterLoadDistance)
								#IF IS_DEBUG_BUILD
									IF GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePlayer) = CPR_VERY_HIGH
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player has an end-of-mission comm queued. Setting leave area flag to avoid scene conflict.")
									ELSE
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Player just switched on top of a mission lead-in. Setting leave area flag to avoid scene popping.")
									ENDIF
								#ENDIF
								g_sMissionActiveData[eMission].leaveArea = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//B*1911269 - Move asset queue update outside leavearea check
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())	
						IF NOT CALL paramLocalVars.sScenePointers[paramTriggerIndex].IS_TRIGGER_SCENE_BLOCKED()
							//Always manage the lead-in load queue at this point.
							UPDATE_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
						ENDIF
					ENDIF
				ENDIF
				
				//Does the player need to leave the mission area before we can start managing the trigger?
				IF NOT g_sMissionActiveData[eMission].leaveArea
					#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Leave area flag cleared.")
							CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
					
					//Is the trigger scene logic asking for the trigger to be blocked?
					IF NOT CALL paramLocalVars.sScenePointers[paramTriggerIndex].IS_TRIGGER_SCENE_BLOCKED()
						IF g_TriggerableMissions[paramTriggerIndex].bBlocked
							g_TriggerableMissions[paramTriggerIndex].bBlocked = FALSE
							g_sMissionActiveData[eMission].leaveArea = TRUE
						ENDIF
					
						#IF IS_DEBUG_BUILD
							IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Scene blocking ended.")
								CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF
						
						//Blip the mission.
						IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								
								//Deactivate any active timelapse trigger blip before turning on the main blip.
								IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
									IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
										REMOVE_BLIP(paramLocalVars.blipTODTrigger)
									ENDIF
									CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
								ENDIF
								
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Setting scene blip visible.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, TRUE)
								ENDIF
								SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ELSE
							//Flash the blip if it is activate for this misison for the first time.
							IF NOT PRIVATE_Is_Story_Mission_First_Activated_Bit_Set(eMission)
								IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(eSceneBlip)
								AND IS_STATIC_BLIP_CURRENTLY_VISIBLE_TO_SYSTEM(eSceneBlip)
									FLASH_STATIC_BLIP(eSceneBlip)
									PRIVATE_Set_Story_Mission_First_Activated_Bit(eMission)
								ENDIF
							ENDIF
						
							IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Hiding scene blip as a timelapse trigger has activated.")
								IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
									SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
								ENDIF
								CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							ENDIF
						ENDIF
						
						//Can we trigger this scene without it being created?
						IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
							//Yes. Has the scene triggered?
							IF DO_TRIGGER_SCENE_TRIGGER_CHECK(paramTriggerIndex, paramLocalVars)
								RETURN TRUE
							ENDIF 
						ENDIF
						
						//Turn on and off the friend rejection mode.
						IF NOT g_bCleanupTriggerScene
						AND g_eMissionSceneToPreLoad = SP_MISSION_NONE
							IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
								IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									IF fDistanceSquaredFromTrigger < (g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Entered friend rejection area. Toggling on reject mode.")
										
										SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_REJECT, g_TriggerableMissions[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
										SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ELSE
									IF fDistanceSquaredFromTrigger > (g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance + 30.0)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Left friend rejection area. Toggling off reject mode.")
										
										CLEAR_FRIEND_MISSION_ZONE(eMission)
										CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_FRIEND_REJECT_ACTIVE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Turn on and off the battle buddy call zone.
						IF g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance != 0.0
							IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Entered battle buddy call area. Toggling on ability to call battle buddies.")

									SET_FRIEND_MISSION_ZONE(g_TriggerableMissions[paramTriggerIndex].sScene.eMission, FRIEND_MISSION_ZONE_CALL, g_TriggerableMissions[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
									SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ELSE
								IF fDistanceSquaredFromTrigger > (g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance + 30.0)
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Left battle buddy call area. Toggling off ability to call battle buddies.")

									CLEAR_FRIEND_MISSION_ZONE(eMission)
									CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BATTLE_BUDDY_CALL_ACTIVE)
								ENDIF
							ENDIF
						ENDIF
						
						//Player range checks.
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						
							//Is the player close enough to stream in and create the scene?
							IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
								
								IF fDistanceSquaredFromTrigger < (g_TriggerableMissions[paramTriggerIndex].sScene.fLoadDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fLoadDistance)
								
									IF NOT g_bBlockTriggerSceneLoadDuringSwitch
										IF NOT g_bTriggerSceneActive
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Requesting assets.")
						
											CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
											
											//Optimisation. Preload mission scripts for prep missions. Combats slow blipping of mission objectives.
											IF IS_BIT_SET(g_sMissionStaticData[g_TriggerableMissions[paramTriggerIndex].eMissionID].settingsBitset, MF_INDEX_IS_PREP)
												CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_TriggerableMissions[paramTriggerIndex].eMissionID), "> ",
																		"Loading stat_watcher and mission script for seamless prep mission.")
												
												#if USE_CLF_DLC
													REQUEST_SCRIPT(get_script_from_hash(g_sMissionStaticData[g_TriggerableMissions[paramTriggerIndex].eMissionID].scriptHash))
													REQUEST_SCRIPT("mission_stat_watchCLF")
												#endif
												#if USE_NRM_DLC
													REQUEST_SCRIPT(get_script_from_hash_NRM(g_sMissionStaticData[g_TriggerableMissions[paramTriggerIndex].eMissionID].scriptHash))
													REQUEST_SCRIPT("mission_stat_watchNRM")
												#endif
												#if not USE_CLF_DLC
												#if not USE_NRM_DLC
													REQUEST_SCRIPT(g_sMissionStaticData[g_TriggerableMissions[paramTriggerIndex].eMissionID].scriptName)
													REQUEST_SCRIPT("mission_stat_watcher")
												#endif
												#endif
												
											ENDIF
											
											//Store a global reference to the cleanup for this scene so
											//the mission triggerer can clean it up if necessary.
											g_eMissionSceneToCleanup = eMission
											paramLocalVars.fpReleaseAssetsForLoadedScene = paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS
											paramLocalVars.bReleaseFunctionBound = TRUE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Local release function pointer bound as scene naturally starts loading.")
											
											SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
											g_bMissionTriggerLoading = TRUE
											IF NOT g_bTriggerSceneActive
												CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Trigger scene active flag set this frame.")
												g_bTriggerSceneActive = TRUE
											ENDIF
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Scene blocked from loading due to in progress switch.")
										IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Switch loading block is set and no switch is in progress. Clearing flag.")
											g_bBlockTriggerSceneLoadDuringSwitch = FALSE
										ENDIF
									ENDIF
								ENDIF
								
							ELIF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
							
								//Scene isn't created. Run the ambient update this frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].AMBIENT_UPDATE_TRIGGER_SCENE()
								
								//Continue to request loading in case conditions changed since last frame.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].REQUEST_TRIGGER_SCENE_ASSETS()
								
								//Back out mid load due to switch in progress block.
								IF g_bBlockTriggerSceneLoadDuringSwitch
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene loading blocked for a switch during load. Unloading scene.")
									UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
									RETURN FALSE
								ENDIF
								
								IF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAVE_TRIGGER_SCENE_ASSETS_LOADED()
								
									//Are we a valid character to create this scene for?
									IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
									
										//Are we waiting for a TOD skip to finish?
										IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)

											IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												//Does this mission need to do a timelapse skip at the moment?
												IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Scene flagged to do a timelapse.")
													SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
												ELSE
													IF NOT IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
														//Inside valid hours or no TOD skip to do. Create the scene.
														CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																				"Creating scene.")
																						
														CALL paramLocalVars.sScenePointers[paramTriggerIndex].CREATE_TRIGGER_SCENE()
														
														g_bMissionTriggerLoading = FALSE
														SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
														
														IF g_eFriendMissionZoneState = FRIEND_MISSION_ZONE_OFF
															SET_FRIEND_MISSION_ZONE(eMission, FRIEND_MISSION_ZONE_ON, g_TriggerableMissions[paramTriggerIndex].sScene.iFriendsToAcceptBitset)
														ENDIF
														
														IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
															CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																					"Cleaning up TOD cutscene.")
															SET_TODS_CUTSCENE_RUNNING(paramLocalVars.sTimeOfDay, FALSE)
															structTimelapse sNewTimelapse
															paramLocalVars.sTimeOfDay = sNewTimelapse
														
															IF NOT IS_SCREEN_FADED_IN()
															AND NOT IS_SCREEN_FADING_IN()
																DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
															ENDIF
															CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
														ENDIF

														//Check if we can trigger immediately after creation.
														IF DO_TRIGGER_SCENE_TRIGGER_CHECK(paramTriggerIndex, paramLocalVars)
															RETURN TRUE
														ENDIF
													ELSE
														CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissions[paramTriggerIndex].sScene)
													ENDIF
												ENDIF
											ELSE
												//We need to do a time of day skip. Wait for the player to hit the TOD trigger.
												VECTOR vTODTrigger = GET_MISSION_TOD_TRIGGER_POSITION(eMission)
												FLOAT fTODDistanceSquared = GET_PLAYER_DISTANCE_SQUARDED_FROM_TRIGGER_COORD(vTODTrigger)
												FLOAT fTODTriggerRadius = GET_MISSION_TOD_TRIGGER_RADIUS(eMission)

												//Activate the timelapse trigger blip if it isn't already.
												IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Activating a timelapse trigger blip.")
													IF DOES_BLIP_EXIST(paramLocalVars.blipTODTrigger)
														REMOVE_BLIP(paramLocalVars.blipTODTrigger)
													ENDIF
													paramLocalVars.blipTODTrigger = CREATE_BLIP_FOR_COORD(vTODTrigger)
													SET_BLIP_SPRITE(paramLocalVars.blipTODTrigger, GET_STATIC_BLIP_ICON(eSceneBlip))
													IF NOT IS_STRING_NULL_OR_EMPTY(g_GameBlips[eSceneBlip].txtBlipName)
														SET_BLIP_NAME_FROM_TEXT_FILE(paramLocalVars.blipTODTrigger, g_GameBlips[eSceneBlip].txtBlipName)
													ENDIF
													SET_BLIP_HIGH_DETAIL(paramLocalVars.blipTODTrigger, TRUE)
													SET_BLIP_AS_SHORT_RANGE(paramLocalVars.blipTODTrigger, FALSE)
													SET_BLIP_DISPLAY(paramLocalVars.blipTODTrigger, DISPLAY_BOTH)
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_MICHAEL)		BREAK
														CASE CHAR_FRANKLIN	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_FRANKLIN)	BREAK
														CASE CHAR_TREVOR	SET_BLIP_COLOUR(paramLocalVars.blipTODTrigger, BLIP_COLOUR_TREVOR)		BREAK
													ENDSWITCH
													SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_BLIP_ACTIVE)
												ENDIF
												
												DRAW_MARKER_FOR_TRIGGER_LOCATION(vTODTrigger)

												IF fTODDistanceSquared < fTODTriggerRadius*fTODTriggerRadius
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Player walked into TOD trigger.")
													CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																			"Force cleaning up random events.")						
													FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
													CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_TOD_TRIGGER_ACTIVATED)
													
													//Vehicle management on trigger being hit.
													IF IS_PLAYER_PLAYING(PLAYER_ID())
														IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
															IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
																VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
																//Bring the vehicle out of critical state if it is in one.
																IF GET_ENTITY_HEALTH(vehPlayer) < 1
																	SET_ENTITY_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
																ENDIF
																IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
																	SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
																ENDIF
																IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_NO_VEH_STOP)
																#if not USE_CLF_DLC
																#if not USE_NRM_DLC
																OR (eMission = SP_MISSION_EXILE_2 AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
																#endif
																#endif
																	IF IS_VEHICLE_ON_ALL_WHEELS(vehPlayer)
																		BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5, TRUE)
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF

													//Check what behaviour we want when we hit a TOD trigger for this mission.
													IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_DO_TOD_AUTO_START)
														//Mission flagged to auto start when hitting TOD trigger.
														RETURN TRUE
													ELSE
														//Need to do a timelapse cutscene as we hit TOD trigger.
														SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
													ENDIF	
												ENDIF
												#IF IS_DEBUG_BUILD
													IF g_eTriggerDebugMode = TDM_TRIGGER
													OR g_eTriggerDebugMode = TDM_ALL
													OR g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_SPHERE(vTODTrigger, fTODTriggerRadius, 0, 0, 255, 65)
													ENDIF
													IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
													OR g_eTriggerDebugMode = TDM_ALL_TEXT
														DRAW_DEBUG_TEXT("ToD Trigger", <<vTODTrigger.X-fTODTriggerRadius, vTODTrigger.Y, vTODTrigger.Z>>, 0, 0, 255, 255)
													ENDIF
												#ENDIF
											ENDIF
										//TOD trigger hit, do the timelapse.
										ELSE
											//If we are in valid hours hours we need to force the cutscene to play anyway.
											BOOL bForceToD = FALSE
											IF IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[eMission].startHour, g_sMissionStaticData[eMission].endHour)
												bForceToD = TRUE
											ENDIF
										
											IF DO_TIMELAPSE(eMission, paramLocalVars.sTimeOfDay, bForceToD, TRUE, FALSE, TRUE, FALSE)
												CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Timelapse finished. Resetting timelapse state.")

												IF NOT IS_PED_INJURED(PLAYER_PED_ID())
													SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_MISSION_TOD_END_POSITION(eMission))
													SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_MISSION_TOD_END_HEADING(eMission))
													IF GET_MISSION_TOD_WALK_PLAYER_AT_END(eMission)
														IF IS_PLAYER_PLAYING(PLAYER_ID())
															SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
														ENDIF
														FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
													ENDIF
													FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
													SET_GAMEPLAY_CAM_RELATIVE_HEADING()
													SET_GAMEPLAY_CAM_RELATIVE_PITCH()
												ENDIF
												
												CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_DOING_TOD_SKIP)
												SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CLEANUP_TOD_SKIP)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
							//The scene is created.
							ELSE
								IF NOT g_bTriggerSceneActive
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag set this frame.")
									g_bTriggerSceneActive = TRUE
								ENDIF
								
								//Draw the trigger scene marker if the blip is visible.
								IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
									IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
										IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
											BOOL bDrawMarker = TRUE
											IF IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_HAS_LEADIN)
												IF IS_BIT_SET(g_iExtraMissionFlags[eMission], MF_EXTRA_INDEX_FORCE_TRIGGER_CORONA)
													bDrawMarker = TRUE
												ELSE
													SWITCH GET_CURRENT_PLAYER_PED_ENUM()
														CASE CHAR_MICHAEL
															IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_FORCE_CORONA_M)
																bDrawMarker = FALSE
															ENDIF
														BREAK
														CASE CHAR_FRANKLIN
														CASE CHAR_TREVOR
															bDrawMarker = FALSE
														BREAK
													ENDSWITCH
												ENDIF
											ENDIF
											IF bDrawMarker
												IF NOT g_bPlayerLockedInToTrigger
													DRAW_MARKER_FOR_TRIGGER_LOCATION(GET_TRIGGER_SCENE_BLIP_POSITION(eSceneBlip))
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Set context help blocks if the player is close enough.																				
								IF fDistanceSquaredFromTrigger < 56.25 //7.5^2
									IF NOT g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Set help text block flag as the player is less than 7.5m from a mission blip.")
								 		g_bTriggerSceneBlockHelp = TRUE
									ENDIF
								ELSE
									IF g_bTriggerSceneBlockHelp
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Cleared help text block flag as the player is more than 7.5m from a mission blip.")
										g_bTriggerSceneBlockHelp = FALSE
									ENDIF
								ENDIF
								
								//Run the scene's update.
								CALL paramLocalVars.sScenePointers[paramTriggerIndex].UPDATE_TRIGGER_SCENE()
								
								//If any trigger scene peds exist, flag them for hardware visibility checks.
								UPDATE_TRIGGER_SCENE_PED_TRACKING()
								
								//Check for going out of hours or switching to a different character while the scene is created.
								IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
								OR NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].iTriggerableCharBitset, GET_CURRENT_PLAYER_PED_INT())
								OR IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
									#IF IS_DEBUG_BUILD
										IF DOES_TRIGGER_SCENE_REQUIRE_TOD_SKIP_NOW(eMission)
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to going out of hours.")
										ELIF IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS()
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to a switch being in progress.")
										ELSE
											CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Cleaning up created scene due to switching to invalid ped.")
										ENDIF
									#ENDIF
								
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									
									//1953978 - Special case fix for triggering Rural Heist prep: don't do cleanup on character switch.
									#IF NOT USE_SP_DLC
										IF NOT (IS_PLAYER_SWITCH_TO_CHARACTER_IN_PROGRESS() AND eMission = SP_HEIST_RURAL_PREP_1)
											CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissions[paramTriggerIndex].sScene)
											TRIGGER_SCENE_GENERAL_SCENE_RELEASE()
											CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
											
											CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
											CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
											
											IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
												CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																		"Running scene reset.")
												CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
											ENDIF

											CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																	"Trigger scene active flag cleared.")
											g_bTriggerSceneActive = FALSE
										ENDIF
									#ENDIF
									
								//Check for it being disrupted.
								ELIF CALL paramLocalVars.sScenePointers[paramTriggerIndex].HAS_TRIGGER_SCENE_BEEN_DISRUPTED()
									CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Scene disrupted. Deactivating trigger, releasing assets, and flagging leave area/scene flags.")
									
									//Display help to tell the player what's happened as the scene is disrupted.
									IF IS_BIT_SET(g_sMissionStaticData[eMission].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
										ADD_HELP_TO_FLOW_QUEUE("AM_H_DISRU", FHP_HIGH, 0, 10000, DEFAULT_HELP_TEXT_TIME, 7)
									ENDIF
									
									IF g_bPlayerLockedInToTrigger
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Clearing player lock-in due to scene disruption.")
										CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
									ENDIF
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE()
									CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissions[paramTriggerIndex].sScene)
									CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_CREATED)
									CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
									
									CALL paramLocalVars.sScenePointers[paramTriggerIndex].RELEASE_TRIGGER_SCENE_ASSETS()
									CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
									
									IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_ONLY_RESET_ON_INIT)
										CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Running scene reset after disruption.")
										CALL paramLocalVars.sScenePointers[paramTriggerIndex].RESET_TRIGGER_SCENE()
									ENDIF

									Set_Leave_Area_Flag_For_Mission(eMission, TRUE)
									CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
															"Trigger scene active flag cleared as trigger is disrupted.")
									g_bTriggerSceneActive = FALSE

								//Does this scene need to be created to be triggered?
								ELIF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_CAN_TRIGGER_BEFORE_CREATION)
									//Yes, check for triggering here.
									IF DO_TRIGGER_SCENE_TRIGGER_CHECK(paramTriggerIndex, paramLocalVars)
										CDEBUG1LN(DEBUG_TRIGGER,"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
																"Trigger scene active flag cleared as trigger fires.")
										g_bTriggerSceneActive = FALSE
										RETURN TRUE
									ENDIF 
								ENDIF
								
							ENDIF
						ENDIF

						//Render the streaming ranges if trigger debug is flagged on.
						#IF IS_DEBUG_BUILD
							IF g_eTriggerDebugMode != TDM_OFF
								VECTOR vTriggerSceneCenter
								IF IS_BIT_SET(g_GameBlips[eSceneBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eSceneBlip].bMultiCoordAndSprite
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip, GET_CURRENT_PLAYER_PED_INT())
								ELSE
									vTriggerSceneCenter = GET_STATIC_BLIP_POSITION(eSceneBlip)
								ENDIF
								IF g_eTriggerDebugMode = TDM_TRIGGER_TEXT
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT(GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), vTriggerSceneCenter, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD 
								OR g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissions[paramTriggerIndex].sScene.fUnloadDistance, 0, 255, 0, 50)
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissions[paramTriggerIndex].sScene.fLoadDistance, 127, 127, 0, 75)
								ENDIF
								IF g_eTriggerDebugMode = TDM_LOAD_TEXT 
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Unload", <<vTriggerSceneCenter.X-g_TriggerableMissions[paramTriggerIndex].sScene.fUnloadDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
									DRAW_DEBUG_TEXT("Load", <<vTriggerSceneCenter.X-g_TriggerableMissions[paramTriggerIndex].sScene.fLoadDistance, vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT
								OR g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance, 255, 0, 0, 100)
								ENDIF
								IF g_eTriggerDebugMode = TDM_FRIEND_REJECT_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("Friend reject", <<vTriggerSceneCenter.X-g_TriggerableMissions[paramTriggerIndex].sScene.fFriendRejectDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL
								OR g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_SPHERE(vTriggerSceneCenter, g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance, 255, 255, 0, 40)
								ENDIF
								IF g_eTriggerDebugMode = TDM_BBUDDY_CALL_TEXT
								OR g_eTriggerDebugMode = TDM_ALL_TEXT
									DRAW_DEBUG_TEXT("BattleBuddy call", <<vTriggerSceneCenter.X-g_TriggerableMissions[paramTriggerIndex].sScene.fBattleBuddyCallDistance , vTriggerSceneCenter.Y, vTriggerSceneCenter.Z>>, 0, 0, 255, 255)
								ENDIF
							ENDIF
						#ENDIF
					ELSE
						//Scene blocked.
						IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Setting scene blip invisible due to blocking.")
							IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
								SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
							ENDIF
							IF g_bPlayerLockedInToTrigger
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Clearing player lock-in due to scene being blocked.")
								CLEAR_PLAYER_TRIGGER_SCENE_LOCK_IN()
							ENDIF
							CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
						ENDIF

						IF NOT g_TriggerableMissions[paramTriggerIndex].bBlocked
							g_TriggerableMissions[paramTriggerIndex].bBlocked = TRUE
						ENDIF
						
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					
						#IF IS_DEBUG_BUILD
							IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
								CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
														"Won't setup trigger scene as scene logic has requested it be blocked.")
								SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_BLOCKING_DEBUG)
							ENDIF
						#ENDIF	
					ENDIF
				ELSE
					//Waiting for player to leave area.
					IF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE) 
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Setting scene blip invisible due to leave area flag.")
						IF NOT IS_BIT_SET(g_sMissionStaticData[eMission].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
							SET_STATIC_BLIP_ACTIVE_STATE(eSceneBlip, FALSE)
						ENDIF
						CLEAR_TRIGGER_SCENE_FRIEND_ZONE_STATE(g_TriggerableMissions[paramTriggerIndex].sScene)
						CLEAR_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_BLIP_ACTIVE)
					ENDIF
					
					IF g_eMissionSceneToCleanup = g_TriggerableMissions[paramTriggerIndex].sScene.eMission
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as the leave area flag was set while it was active.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
							CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Won't setup trigger scene until leave area flag is cleared.")
							SET_BIT(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_PRINTED_LEAVE_AREA_DEBUG)
						ENDIF
					#ENDIF
				ENDIF
			ELIF IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_SCENE_LOADING)
				IF g_eMissionSceneToCleanup != SP_MISSION_NONE
					IF NOT IS_BIT_SET(g_TriggerableMissions[paramTriggerIndex].sScene.iStateBitset, TSS_HAS_TRIGGERED)
						CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
												"Trigger scene unloading as a different mission has started.")
						UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound)
					ENDIF
				ENDIF
			ENDIF
			
			//Is the player far enough away to delete and release the scene?
			IF g_eRunningMission != g_eMissionSceneToCleanup
				IF g_eMissionSceneToCleanup = g_TriggerableMissions[paramTriggerIndex].eMissionID
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF fDistanceSquaredFromTrigger > (g_TriggerableMissions[paramTriggerIndex].sScene.fUnloadDistance*g_TriggerableMissions[paramTriggerIndex].sScene.fUnloadDistance)
							CPRINTLN(DEBUG_TRIGGER,	"<TRIG-SCENE-",GET_SP_MISSION_DISPLAY_STRING_FROM_ID(eMission), "> ",
													"Player left streaming range for mission trigger. Cleaning up.")
							UNLOAD_TRIGGER_SCENE(g_TriggerableMissions[paramTriggerIndex].sScene, paramLocalVars.sScenePointers[paramTriggerIndex], paramLocalVars.bReleaseFunctionBound, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("MANAGE_TRIGGER_SCENE: A mission trigger tried to manage a trigger scene that was not setup. Bug BenR.")
	ENDIF
		
	RETURN FALSE
	
#ENDIF

ENDFUNC
