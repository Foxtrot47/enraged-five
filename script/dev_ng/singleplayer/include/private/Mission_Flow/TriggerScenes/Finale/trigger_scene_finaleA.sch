//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Finale A - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "friends_public.sch"
Using "Locates_public.sch"

CONST_FLOAT 	TS_FINA_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINA_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINA_TRIGGER_DIST		8.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINA_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINA_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINA_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINA_REQUEST_ASSETS()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINA_RELEASE_ASSETS()

ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINA_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINA_CREATE()

	//Call this here to give me full control over peds variations in cutscenes
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)

	SET_ROADS_IN_AREA(<<1199.4, -2661.9, 70>>, <<1597.1, -2447.7, -5>>, FALSE)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FINALE_A,
												"FIN_A_INT",
												CS_NONE,
												CS_ALL,
												CS_NONE)
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		//Trevor
		SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)	
		
		//Franklin
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID(), PLAYER_ONE)
		PRINTSTRING("Cutscene Variations set for Trev and Franklin 0")
	ENDIF			
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINA_RELEASE()

	SET_ROADS_BACK_TO_ORIGINAL(<<1199.4, -2661.9, 70>>, <<1597.1, -2447.7, -5>>)
	
	IF MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_FINALE_A
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	ENDIF

ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINA_DELETE()

	SET_ROADS_BACK_TO_ORIGINAL(<<1199.4, -2661.9, 70>>, <<1597.1, -2447.7, -5>>)
	
	IF MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_FINALE_A
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	ENDIF
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINA_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_A)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_BIT_SET(g_sMissionStaticData[SP_MISSION_FINALE_A].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Render trigger zone debug.
			#IF IS_DEBUG_BUILD
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FINA_TRIGGER_DIST)
			#ENDIF
			
			if not HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_PISTOL)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PISTOL,25,FALSE,FALSE)
			endif			
			
			FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
			IF fDistanceSquaredFromTrigger < (TS_FINA_TRIGGER_DIST*TS_FINA_TRIGGER_DIST)
				if IS_PED_IN_ANY_VEHICLE(player_ped_id())
					if IS_VEHICLE_ALMOST_STOPPED(GET_VEHICLE_PED_IS_IN(player_ped_id()))
						RETURN TRUE
					else
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(player_ped_id()))	
					endif
				else
					RETURN TRUE
				endif
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINA_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINA_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINA_UPDATE()
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		//Trevor
		SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
		SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)	
		
		//Franklin
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Franklin", PLAYER_PED_ID(), PLAYER_ONE)
		PRINTSTRING("Cutscene Variations set for Trev and Franklin 1")
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINA_AMBIENT_UPDATE()
ENDPROC
