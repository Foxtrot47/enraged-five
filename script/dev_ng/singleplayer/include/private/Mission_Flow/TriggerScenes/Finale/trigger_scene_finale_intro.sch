//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Finale Intro - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_FIN_INTRO_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FIN_INTRO_STREAM_OUT_DIST	75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FIN_INTRO_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FIN_INTRO_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FIN_INTRO_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

ENUM TS_FIN_INTRO_LEAD_IN_STATE
	TS_FIN_INTRO_LIS_CONTACT,
	TS_FIN_INTRO_LIS_LOCKED,
	TS_FIN_INTRO_LIS_TRIGGERED
ENDENUM
TS_FIN_INTRO_LEAD_IN_STATE TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_CONTACT
BLIP_INDEX TS_FIN_INTRO_BLIP 

INT iTimeDoorLocked = -1
INT iBuzzerSoundID = -1
INT iBuzzerTimer = -1
BOOL bStartedBuzzer = FALSE

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FIN_INTRO_RESET()
	g_sTriggerSceneAssets.flag = FALSE
	iTimeDoorLocked = -1
	iBuzzerSoundID = -1
	TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_CONTACT
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FIN_INTRO_REQUEST_ASSETS()
	REQUEST_MISSION_AUDIO_BANK("INTERCOM")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FIN_INTRO_RELEASE_ASSETS()
	IF iBuzzerSoundID != -1
		STOP_SOUND(iBuzzerSoundID)
		RELEASE_SOUND_ID(iBuzzerSoundID)
		iBuzzerSoundID = -1
	ENDIF
	RELEASE_MISSION_AUDIO_BANK()
	CLEAR_BIT(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FIN_INTRO_HAVE_ASSETS_LOADED()
	IF REQUEST_MISSION_AUDIO_BANK("INTERCOM")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FIN_INTRO_CREATE()
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)

	TS_FIN_INTRO_RESET()

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FINALE_INTRO,
												"CHOICE_INT",
												CS_NONE,
												CS_ALL,
												CS_NONE)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FIN_INTRO_RELEASE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	IF iBuzzerSoundID != -1
		STOP_SOUND(iBuzzerSoundID)
		RELEASE_SOUND_ID(iBuzzerSoundID)
		iBuzzerSoundID = -1
	ENDIF
	IF TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_TRIGGERED
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	IF DOES_BLIP_EXIST(TS_FIN_INTRO_BLIP)
		REMOVE_BLIP(TS_FIN_INTRO_BLIP)
	ENDIF
	CLEAR_BIT(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
	UNREGISTER_SCRIPT_WITH_AUDIO()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FIN_INTRO_DELETE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	IF iBuzzerSoundID != -1
		STOP_SOUND(iBuzzerSoundID)
		RELEASE_SOUND_ID(iBuzzerSoundID)
		iBuzzerSoundID = -1
	ENDIF
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), DOORSTATE_UNLOCKED, FALSE, TRUE)
	CLEAR_BIT(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
	UNREGISTER_SCRIPT_WITH_AUDIO()
	IF DOES_BLIP_EXIST(TS_FIN_INTRO_BLIP)
		REMOVE_BLIP(TS_FIN_INTRO_BLIP)
	ENDIF
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FIN_INTRO_HAS_BEEN_TRIGGERED()
	IF TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_TRIGGERED
		IF iBuzzerSoundID != -1
			STOP_SOUND(iBuzzerSoundID)
			RELEASE_SOUND_ID(iBuzzerSoundID)
			iBuzzerSoundID = -1
		ENDIF
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), 0, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), DOORSTATE_UNLOCKED, FALSE, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FIN_INTRO_HAS_BEEN_DISRUPTED()
	IF TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_LOCKED
		IF IS_PED_INJURED(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FIN_INTRO_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

PROC TS_FIN_INTRO_CONTACT()
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF iTimeDoorLocked = -1
		IF VDIST2(vPlayerPos, GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FINALE_I)) < 3.00
			//If the door is off camera snap it shut and lock it.
			IF NOT IS_SPHERE_VISIBLE(<<7.5179, 539.5260, 176.1781>>, 1.5)

				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), 0, FALSE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), DOORSTATE_LOCKED, FALSE, TRUE)
				iTimeDoorLocked = GET_GAME_TIMER()
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
				SET_BIT(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
				SET_STATIC_BLIP_ACTIVE_STATE(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].blip, FALSE)
			//If the door is on camera but close to shut lock it.
			ELSE
				FLOAT fOpenRation = 0.0
				IF ABSF(fOpenRation) < 0.02
					DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), fOpenRation, FALSE, TRUE)
					iTimeDoorLocked = GET_GAME_TIMER()
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_BIT(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].settingsBitset, MF_INDEX_BLIP_IS_HIDDEN)
					SET_STATIC_BLIP_ACTIVE_STATE(g_sMissionStaticData[SP_MISSION_FINALE_INTRO].blip, FALSE)
				ENDIF
			ENDIF
		ENDIF
	//Door is locked, start the doorbell ringing.
	ELIF (GET_GAME_TIMER() - iTimeDoorLocked) > 3000
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<8.18818, 538.83167, 178.22221>>, <<4.80666, 533.29614, 174.34355>>, 5)
		DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_FRANKLIN_SAVEHOUSE_HILLS_CAR)
		TS_FIN_INTRO_BLIP = CREATE_BLIP_FOR_COORD(<<7.8325, 538.7512, 175.0281>>)
		TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_LOCKED

		IF NOT bStartedBuzzer
			CPRINTLN(DEBUG_MISSION, "Start buzzer")
			iBuzzerSoundID = GET_SOUND_ID()
			PLAY_SOUND_FROM_COORD(iBuzzerSoundID, "DOOR_BUZZER_LOOP", <<7.1155, 539.1238, 175.0281>>, "FINALE_INTRO_SOUNDSET")
			iBuzzerTimer = GET_GAME_TIMER()
			bStartedBuzzer = TRUE
		ENDIF

		IF NOT g_sTriggerSceneAssets.flag 
			VEHICLE_INDEX veh = GET_PLAYERS_LAST_VEHICLE()
			IF DOES_ENTITY_EXIST(veh)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(veh)
					SET_ENTITY_AS_MISSION_ENTITY(veh)
					CPRINTLN(DEBUG_MISSION, "player last vehicle SET_ENTITY_AS_MISSION_ENTITY SK - ", GET_THIS_SCRIPT_NAME())
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(veh)
					IF IS_ENTITY_AT_COORD(veh, <<8.09956, 539.19971, 175.02812>>, <<5.0,5.0,5.0>>)
						SET_ENTITY_COORDS(veh, <<2.7713, 546.1324, 173.5474>>)
						SET_ENTITY_HEADING(veh, 125.6127)
						CPRINTLN(DEBUG_MISSION, "Moved player vehicle SK - ", GET_THIS_SCRIPT_NAME())
						g_sTriggerSceneAssets.flag = TRUE
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "Player vehicle dead or something not moving it  SK - ", GET_THIS_SCRIPT_NAME())
				g_sTriggerSceneAssets.flag = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TS_FIN_INTRO_LOCKED()
	REPLAY_PREVENT_RECORDING_THIS_FRAME()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()

	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	IF VDIST2(vPlayerPos, <<-7.2915, 512.9512, 173.6282>>) < 81 //Franklin near the back door. Trigger cutscene to stop him leaving.
	OR VDIST2(vPlayerPos, <<7.5179, 539.5260, 176.1781>>) < 6.5 //Franklin near the door. Open it.
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), 0, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_SC_G), DOORSTATE_LOCKED, FALSE, TRUE)
		IF DOES_BLIP_EXIST(TS_FIN_INTRO_BLIP)
			REMOVE_BLIP(TS_FIN_INTRO_BLIP)
		ENDIF
		TS_FIN_INTRO_eLeadinState = TS_FIN_INTRO_LIS_TRIGGERED
	ENDIF

	IF iBuzzerSoundID != -1
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<8.18818, 538.83167, 178.22221>>, <<4.80666, 533.29614, 174.34355>>, 5)
		OR (GET_GAME_TIMER() - iBuzzerTimer) > 5000
		
			CPRINTLN(DEBUG_MISSION, "Player at door kill buzzer")
			STOP_SOUND(iBuzzerSoundID)
			RELEASE_SOUND_ID(iBuzzerSoundID)
			iBuzzerSoundID = -1
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FIN_INTRO_UPDATE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		SWITCH TS_FIN_INTRO_eLeadinState
			CASE TS_FIN_INTRO_LIS_CONTACT
				TS_FIN_INTRO_CONTACT()
			BREAK
			
			CASE TS_FIN_INTRO_LIS_LOCKED
				TS_FIN_INTRO_LOCKED()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FIN_INTRO_AMBIENT_UPDATE()
ENDPROC

