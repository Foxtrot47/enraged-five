//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Finale C1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│						ped[0] = Lester											│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
using "rgeneral_include.sch"
USING "trigger_box.sch"


CONST_INT		iLESTER_INDEX 0
CONST_INT		iWHEELCHAIR_INDEX 0

CONST_FLOAT 	TS_FINC1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FINC1_STREAM_OUT_DIST	75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FINC1_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FINC1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FINC1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


//State flag bit indexes.
CONST_INT 		TS_FINC1_DOOR_UNLOCKED			0
CONST_INT 		TS_FINC1_LEAD_IN_TRIGGERED		1
CONST_INT 		TS_FINC1_ACTION_TRIGGERED		2
CONST_INT 		TS_FINC1_CONV_L1_TRIGGERED		3
CONST_INT 		TS_FINC1_2ND_LOOP_TRIGGERED		4

structTimer		tmrConvTime


INT 			iFINC_StateFlags = 0
INT 			iLesterScene = 0

VECTOR			vLesterStart = << 1276.880, -1712.571, 54.415 >>//<< 1276.397, -1712.832, 54.420 >>//<< 1276.390, -1712.845, 54.472>>//372 >>
VECTOR			vSynchSceneRot = << 0.000, -0.000, -142.020 >>//<< 0.000, -0.000, -155.520 >>//<< 0.000, 0.000, -155.520 >>

TRIGGER_BOX		tbLesterInnerVol
TRIGGER_BOX		tbLesterOuterVol
TRIGGER_BOX		tbStopVolume

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FINC1_RESET()
	iFINC_StateFlags = 0
	iLesterScene = 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FINC1_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
	REQUEST_MODEL(Prop_WheelChair_01_S)
	REQUEST_ANIM_DICT("missfinale_c2leadinoutfin_c_int")
	REQUEST_SCRIPT_AUDIO_BANK("Lester1A_01")
	SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FINC1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_WheelChair_01_S)
	REMOVE_ANIM_DICT("missfinale_c2leadinoutfin_c_int")
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
	RELEASE_SCRIPT_AUDIO_BANK()
	
	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FINC1_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LESTER))
	AND HAS_MODEL_LOADED(Prop_WheelChair_01_S)
	AND HAS_ANIM_DICT_LOADED("missfinale_c2leadinoutfin_c_int")
	AND REQUEST_SCRIPT_AUDIO_BANK("Lester1A_01")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FINC1_CREATE()

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FINALE_C1,
												"FIN_C_INT",
												CS_NONE,
												CS_ALL,
												CS_NONE)
												
	tbLesterInnerVol = CREATE_TRIGGER_BOX((<<1273.325195,-1709.457764,53.771469>>), (<<1274.834717,-1712.467041,56.271469>>), 5.500000)
	tbLesterOuterVol = CREATE_TRIGGER_BOX((<<1274.518066,-1718.665039,53.771469>>), (<<1272.736084,-1707.608398,55.773109>>), 6.500000)
	tbStopVolume = CREATE_TRIGGER_BOX((<<1271.854248,-1712.835083,53.771469>>), (<<1276.384277,-1710.632080,56.888542>>), 3.2)
	
	ADD_RELATIONSHIP_GROUP("TS_FINC1", g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.ped[iLESTER_INDEX] = CREATE_PED(PEDTYPE_MISSION, GET_NPC_PED_MODEL(CHAR_LESTER), vLesterStart, 338.0729)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[iLESTER_INDEX], g_sTriggerSceneAssets.relGroup)
	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[iLESTER_INDEX], ANCHOR_EYES, 0)
	g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX] = CREATE_OBJECT(Prop_WheelChair_01_S, vLesterStart)
	
	iLesterScene = CREATE_SYNCHRONIZED_SCENE(vLesterStart, vSynchSceneRot)
	SET_SYNCHRONIZED_SCENE_LOOPED(iLesterScene, TRUE)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[iLESTER_INDEX], iLesterScene, "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP1_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT )
	PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX], iLesterScene, "_LEADIN_LOOP1_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
	
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[iLESTER_INDEX], "LESTER")
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, PLAYER_PED_ID(), "FRANKLIN")
	
	// maybe this should be a sync scene, but the single (brief) attempt I made had screwed up positioning
	//TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[iLESTER_INDEX], "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP1_LESTER", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING)
	//PLAY_ENTITY_ANIM(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX], "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP1_WCHAIR", INSTANT_BLEND_IN, TRUE, TRUE)
	
	CPRINTLN(DEBUG_MISSION, "BSW unlocking door")
	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FINC1_RELEASE()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	CPRINTLN(DEBUG_MISSION, "BSWts release called")
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
	CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	
	STOP_GAMEPLAY_HINT()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
		SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[iLESTER_INDEX], TRUE)
	ENDIF
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)

//	TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [g_sTriggerSceneAssets.ped[iLESTER_INDEX]] as no longer needed with no action.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.ped[iLESTER_INDEX], iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
//				TRIGGER_SCENE_CLEAR_FRIENDLY_PED_STATE(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
				IF NOT IS_PED_IN_ANY_VEHICLE(g_sTriggerSceneAssets.ped[iLESTER_INDEX], TRUE)
					IF IS_ENTITY_ATTACHED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
						DETACH_ENTITY(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
					ENDIF
					SET_ENTITY_COLLISION(g_sTriggerSceneAssets.ped[iLESTER_INDEX], TRUE)
				ENDIF
				CLEAR_PED_SECONDARY_TASK(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
				SET_ENTITY_HAS_GRAVITY(g_sTriggerSceneAssets.ped[iLESTER_INDEX], TRUE)
				FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[iLESTER_INDEX], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
			ENDIF
		ENDIF
	ENDIF
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX])

	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
	
	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)
	
	IF( MISSION_FLOW_GET_RUNNING_MISSION() <> SP_MISSION_FINALE_C1 )
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_LESTERS, TRUE)
	ENDIF
	
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FINC1_DELETE()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	CPRINTLN(DEBUG_MISSION, "BSWts delete called")
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
	CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	//Delete scene peds.
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
	
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX])
	
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)

	SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_UNLOCKED)
	
	IF( MISSION_FLOW_GET_RUNNING_MISSION() <> SP_MISSION_FINALE_C1 )
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_LESTERS, TRUE)
	ENDIF
	
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)
ENDPROC

FLOAT fSideOffset = -0.64
/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FINC1_HAS_BEEN_TRIGGERED()
	
		IF IS_BIT_SET(iFINC_StateFlags, TS_FINC1_CONV_L1_TRIGGERED)
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				IF IS_ENTITY_OK(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[iLESTER_INDEX], <<0,0,0>>, TRUE, DEFAULT)
					SET_GAMEPLAY_HINT_FOV(50.0)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.35)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fSideOffset)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.3)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				ENDIF
			ELSE
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
			ENDIF
		
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND GET_TIMER_IN_SECONDS_SAFE(tmrConvTime) > 2.1
				RETURN TRUE
			ENDIF
		ENDIF
		
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FINC1_HAS_BEEN_DISRUPTED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Check for scene peds being injured or harmed.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[iLESTER_INDEX], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(tbLesterInnerVol.vMin, tbLesterInnerVol.vMax, tbLesterInnerVol.flWidth)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, tbLesterOuterVol.vMin, tbLesterOuterVol.vMax, tbLesterOuterVol.flWidth)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FINC1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FINC1_UPDATE()

	IF IS_ENTITY_OK(PLAYER_PED_ID())
	AND IS_ENTITY_OK(g_sTriggerSceneAssets.ped[iLESTER_INDEX])
	AND IS_ENTITY_OK(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX])
		
		// sounds
		IF NOT IS_BIT_SET(iFINC_StateFlags, TS_FINC1_DOOR_UNLOCKED)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), (<<1275.72, -1719.97, 54.97>>)) < 5.0
				PLAY_SOUND_FROM_COORD(-1, "UNLOCK_DOOR", <<1275.72, -1719.97, 54.97>>, "LESTER1A_SOUNDS")
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", FALSE, TRUE)
				SET_BIT(iFINC_StateFlags, TS_FINC1_DOOR_UNLOCKED)
			ENDIF
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1275.72, -1719.97, 54.97>>) < (50.0 * 50.0)
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2190395
		ENDIF
		
		// Player triggers lead in
		IF NOT IS_BIT_SET(iFINC_StateFlags, TS_FINC1_LEAD_IN_TRIGGERED)
			IF IS_PLAYER_IN_TRIGGER_BOX(tbLesterOuterVol)
			AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
				BLOCK_PLAYER_FOR_LEAD_IN(FALSE)
				// Keep player from leaving now
				SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_LOCKED)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_LESTER_F), 0.0, TRUE, FALSE) 
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_LESTER_F), DOORSTATE_LOCKED, TRUE, TRUE)

				SET_BIT(iFINC_StateFlags, TS_FINC1_LEAD_IN_TRIGGERED)
				
//				START_WIDGET_GROUP("Push-in Fiddling")
//				ADD_WIDGET_FLOAT_SLIDER("Side Offset", fSideOffset, -2, 2, 0.1)
//				STOP_WIDGET_GROUP()
			ENDIF
		ELSE 
			UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
			
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2190395
			
			IF IS_PLAYER_IN_TRIGGER_BOX(tbStopVolume)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			ENDIF
			
			// Lester moves to meet Franklin
			IF NOT IS_BIT_SET(iFINC_StateFlags, TS_FINC1_ACTION_TRIGGERED)
				iLesterScene = CREATE_SYNCHRONIZED_SCENE(vLesterStart, vSynchSceneRot)
				TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[iLESTER_INDEX], iLesterScene, "missfinale_c2leadinoutfin_c_int", "_LEADIN_ACTION_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT )
				PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX], iLesterScene, "_LEADIN_ACTION_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
				SET_BIT(iFINC_StateFlags, TS_FINC1_ACTION_TRIGGERED)
			ELIF IS_SYNCHRONIZED_SCENE_RUNNING(iLesterScene)
				
				// Play conversation
				IF NOT IS_BIT_SET(iFINC_StateFlags, TS_FINC1_CONV_L1_TRIGGERED)
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "FINC1AU", "FINC1_INTLI", CONV_PRIORITY_VERY_HIGH)
						SET_BIT(iFINC_StateFlags, TS_FINC1_CONV_L1_TRIGGERED)
						
						IF IS_ENTITY_OK(g_sTriggerSceneAssets.ped[iLESTER_INDEX]) AND IS_ENTITY_OK(PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[iLESTER_INDEX], -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						ENDIF
						
						START_TIMER_NOW_SAFE(tmrConvTime)
					ENDIF
				ENDIF
				
				// final loop
				IF NOT IS_BIT_SET(iFINC_StateFlags, TS_FINC1_2ND_LOOP_TRIGGERED)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLesterScene) >= 0.99
						iLesterScene = CREATE_SYNCHRONIZED_SCENE(vLesterStart, vSynchSceneRot)
						SET_SYNCHRONIZED_SCENE_LOOPED(iLesterScene, TRUE)
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[iLESTER_INDEX], iLesterScene, "missfinale_c2leadinoutfin_c_int", "_LEADIN_LOOP2_LESTER", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT )
						PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[iWHEELCHAIR_INDEX], iLesterScene, "_LEADIN_LOOP2_WCHAIR", "missfinale_c2leadinoutfin_c_int", INSTANT_BLEND_IN, 8, ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
						SET_BIT(iFINC_StateFlags, TS_FINC1_2ND_LOOP_TRIGGERED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FINC1_AMBIENT_UPDATE()
ENDPROC
