//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Lamar 1 - Trigger Scene Data							│
//│																				│
//│								Date: 5/12/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "tv_control_public.sch"


CONST_FLOAT 	TS_LAMAR1_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_LAMAR1_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_LAMAR1_TRIGGER_DIST			8.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_LAMAR1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_LAMAR1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_LAMAR1_RESET()

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_LAMAR1_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("misstimelapse@franklinold_home")
	REQUEST_MODEL(V_ILEV_FA_FRONTDOOR)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_LAMAR1_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("misstimelapse@franklinold_home")
	SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_FA_FRONTDOOR)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_LAMAR1_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_LAMAR1_CREATE()
//	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_LAMAR,
//														"LAMAR_1_INT",
//														CS_NONE,
//														CS_ALL,
//														CS_NONE)
//	
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend", PED_COMP_HEAD, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend", PED_COMP_TORSO, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend", PED_COMP_LEG, 0, 0)
//	
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend^1", PED_COMP_HEAD, 1, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend^1", PED_COMP_TORSO, 1, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise_Friend^1", PED_COMP_LEG, 1, 0)
//		
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise", PED_COMP_HEAD, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise", PED_COMP_TORSO, 0, 1)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise", PED_COMP_DECL, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Denise", PED_COMP_LEG, 0, 0)
//	
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_HAIR, 2, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_TORSO, 2, 2)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_LEG, 5, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_HAND, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_SPECIAL2, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_SPECIAL, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Lamar", PED_COMP_BERD, 0, 0)
//
//	MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_MISSION_LAMAR, "Stretch", ANCHOR_HEAD, 0, 0)
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_LAMAR, "Stretch", PED_COMP_SPECIAL, 1, 0)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_LAMAR1_RELEASE()
	//MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_LAMAR1_DELETE()
	//MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_LAMAR1_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-14.243821,-1441.824219,30.891863>>,<<2.500000,3.500000,1.500000>>)
			SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_LAMAR1_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_LAMAR1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_LAMAR1_UPDATE()
	SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F, DOORSTATE_FORCE_LOCKED_THIS_FRAME)

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-14.200673,-1443.824585,31.183750>>,<<4.000000,5.000000,1.750000>>)
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_LAMAR1_AMBIENT_UPDATE()
ENDPROC
