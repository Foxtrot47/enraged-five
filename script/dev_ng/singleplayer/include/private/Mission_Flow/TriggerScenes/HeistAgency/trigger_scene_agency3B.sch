//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Agency Heist 3B - Trigger Scene Data					│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_AGENCY3B_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_AGENCY3B_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_AGENCY3B_TRIGGER_DIST		8.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_AGENCY3B_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_AGENCY3B_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

INT 			TS_AGENCY3B_iLeadInStage		= 0


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_AGENCY3B_RESET()

	TS_AGENCY3B_iLeadInStage = 0

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_AGENCY3B_REQUEST_ASSETS()
	REQUEST_MODEL(PLAYER_ZERO)
	REQUEST_MODEL(IG_LESTERCREST)
	REQUEST_MODEL(PROP_CS_WALKING_STICK)
	REQUEST_MODEL(PROP_CS_LESTER_CRATE)
	REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	REQUEST_ANIM_DICT("MISSHeistFBI3BleadInOut")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_AGENCY3B_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_LESTERCREST)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_LESTER_CRATE)
	REMOVE_ANIM_DICT("MISSHeistFBI3BleadInOut")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_AGENCY3B_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(PLAYER_ZERO)
	AND HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	AND HAS_MODEL_LOADED(IG_LESTERCREST)
	AND HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
	AND HAS_MODEL_LOADED(PROP_CS_LESTER_CRATE)
	AND HAS_ANIM_DICT_LOADED("MISSHeistFBI3BleadInOut")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_AGENCY3B_CREATE()

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_AGENCY_3B, 
												"AH_3B_INT",
												CS_ALL,
												CS_2,
												CS_NONE)
												
	MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_HEIST_AGENCY_3B, "Lester", ANCHOR_EYES, 0, 0)
	
	ADD_RELATIONSHIP_GROUP("REL_TS_FRIENDLY", g_sTriggerSceneAssets.relGroup)
	
//	VEHICLE_INDEX vehUponArrival
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		vehUponArrival = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//	ELSE
//		vehUponArrival = GET_PLAYERS_LAST_VEHICLE()
//	ENDIF
//
//	// Check car is near enough and is of the correct type
//	IF DOES_ENTITY_EXIST(vehUponArrival)
//	AND IS_VEHICLE_DRIVEABLE(vehUponArrival)
//		IF IS_PLAYERS_VEHICLE_OF_TYPE(PV_CAR)
//		AND IS_ENTITY_IN_ANGLED_AREA(vehUponArrival, <<671.714294,-972.906372,21.112833>>, <<765.014282,-963.060730,25.897289>>, 60.750000)
//			g_sTriggerSceneAssets.veh[0] = vehUponArrival
//		ENDIF
//	ENDIF
	
	//Pin the office doors open as we create the scene. 
	DOOR_SYSTEM_SET_OPEN_RATIO(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), -1.0, FALSE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_LOCKED, FALSE, TRUE)
	
	DOOR_SYSTEM_SET_OPEN_RATIO(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), 1.0, FALSE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_LOCKED, FALSE, TRUE)
	
	DOOR_SYSTEM_SET_OPEN_RATIO(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), 0.0, FALSE, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_LOCKED, FALSE, TRUE)
	
	IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
		
		//g_sTriggerSceneAssets.ped[0] 		= CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<707.1259, -961.3305, 29.3957>>, 165.8747)
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, <<707.1259, -961.3305, 29.3957>>, 165.8747, FALSE)
		g_sTriggerSceneAssets.object[0] 	= CREATE_OBJECT(PROP_CS_WALKING_STICK, <<707.1259, -961.3305, 29.3957>>)
		g_sTriggerSceneAssets.object[1] 	= CREATE_OBJECT(PROP_CS_LESTER_CRATE, <<707.8559, -964.3258, 29.5025>>)
		SET_ENTITY_COORDS_NO_OFFSET(g_sTriggerSceneAssets.object[1], <<707.8559, -964.3258, 29.5025>>)
		SET_ENTITY_HEADING(g_sTriggerSceneAssets.object[1], -147.823111)
		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[1], TRUE)
		
		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
		
		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
		
	ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
	
		IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_MICHAEL, VEHICLE_TYPE_CAR, GET_STATIC_BLIP_POSITION(g_sMissionStaticData[SP_HEIST_AGENCY_3B].blip), TS_AGENCY3B_STREAM_OUT_DIST)
			CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[1], CHAR_MICHAEL, << 718.3592, -983.2876, 23.1379 >>, 270.6242)
		ENDIF

		//g_sTriggerSceneAssets.ped[0] 		= CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<711.9644, -965.5922, 29.3957>>, 359.9821)
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, <<711.9644, -965.5922, 29.3957>>, 359.9821, FALSE)
		//g_sTriggerSceneAssets.ped[1] 		= CREATE_PED(PEDTYPE_MISSION, PLAYER_ZERO, <<711.8387, -963.7095, 29.3957>>, 183.2461)
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_MICHAEL, <<711.8387, -963.7095, 29.3957>>, 183.2461, FALSE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[1], TRUE)
		
		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[1], "MICHAEL")
		
	ENDIF
	
	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0, 0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], TRUE)
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 3, g_sTriggerSceneAssets.ped[0], "Lester")
	
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("HEIST_SWEATSHOP_ZONES", FALSE, TRUE)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_AGENCY3B_RELEASE()

	g_sTriggerSceneAssets.veh[0] = NULL
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
	
	//Release doors so they can swing again.
	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_AGENCY3B_DELETE()
	
	g_sTriggerSceneAssets.veh[0] = NULL
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	
	//Release doors so they can swing again.
	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_AGENCY3B_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF TS_AGENCY3B_iLeadInStage >= 4
			TS_AGENCY3B_RESET()
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_AGENCY3B_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_AGENCY3B_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_AGENCY3B_UPDATE()

	INTERIOR_INSTANCE_INDEX interiorSweat = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<715.9622, -964.9521, 29.3953>>, "v_sweatempty")

	IF IS_VALID_INTERIOR(interiorSweat)
	AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorSweat
	AND ((GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL AND TS_AGENCY3B_iLeadInStage > 1)
	OR (GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN AND TS_AGENCY3B_iLeadInStage > 0))
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L), DOORSTATE_LOCKED)
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R), DOORSTATE_LOCKED)
	ELSE
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L), DOORSTATE_UNLOCKED)
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R), DOORSTATE_UNLOCKED)
	ENDIF
	
	SWITCH TS_AGENCY3B_iLeadInStage
		CASE 0
		
			IF IS_VALID_INTERIOR(interiorSweat)
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorSweat

				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL

					FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], FALSE)
					TS_AGENCY3B_iLeadInStage++
					
				ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
				
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "AH3BAUD", "AH_INTF", CONV_PRIORITY_HIGH)
					
						FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], FALSE)
						FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[1], FALSE)
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						TS_AGENCY3B_iLeadInStage++
						
					ENDIF
					
				ENDIF
			
			ENDIF
			
		BREAK
		CASE 1
			// Locate at the top of the stairs
			IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<717.739563,-963.905396,31.645670>>,<<3.250000,3.125000,2.250000>>)
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SEQUENCE_INDEX seq
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM_ADVANCED(null, "MISSHeistFBI3BleadInOut", "ah_3b_int_leadin_lester", <<709.855, -963.434, 29.390>>, <<0,0,0>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET)
								TASK_PLAY_ANIM_ADVANCED(null, "MISSHeistFBI3BleadInOut", "AH_3B_INT_loop_Lester", <<709.855, -963.434, 29.390>>, <<0,0,0>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_LOOPING)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							IF NOT IS_GAMEPLAY_HINT_ACTIVE()
								SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, -1)
								SET_GAMEPLAY_HINT_FOV(30.0)
								SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.7)
								SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.0)
								SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
								SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.1)
								SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)					
							ENDIF
							
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								OPEN_SEQUENCE_TASK(seq)
									TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<710.9573, -964.1301, 29.3953>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_NO_STOPPING, 93.3411)
									TASK_TURN_PED_TO_FACE_ENTITY(null, g_sTriggerSceneAssets.ped[0], -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
								
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
							ENDIF
							
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
							g_sTriggerSceneAssets.id = GET_GAME_TIMER() + 1000
							TS_AGENCY3B_iLeadInStage++
						ENDIF
					ENDIF
				ENDIF
					
			// as franklin
			ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<717.450684,-968.105408,27.978029>>, <<717.492432,-965.037964,31.395611>>, 4.062500)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					TS_AGENCY3B_iLeadInStage = 4
				
				// start the cutscene once the conversation has finished
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TS_AGENCY3B_iLeadInStage = 4
				ENDIF
				
			ENDIF
		BREAK
		CASE 2
			IF GET_GAME_TIMER() > g_sTriggerSceneAssets.id
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "AH3BAUD", "AH_INTL", CONV_PRIORITY_HIGH)
						TS_AGENCY3B_iLeadInStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			
				IF NOT IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "MISSHeistFBI3BleadInOut", "AH_3B_INT_leadin_lester")
				OR GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "MISSHeistFBI3BleadInOut", "AH_3B_INT_leadin_lester") >= 0.764
					
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TS_AGENCY3B_iLeadInStage = 4	// trigger mission
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_AGENCY3B_AMBIENT_UPDATE()
ENDPROC
