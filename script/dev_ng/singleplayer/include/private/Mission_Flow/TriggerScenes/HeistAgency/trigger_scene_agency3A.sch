//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Agency Heist 3A - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_AGENCY3A_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_AGENCY3A_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_AGENCY3A_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_AGENCY3A_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_AGENCY3A_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


int trigger_scene_flow_status = 0
int lester_scene_id

vector cutscene_pos
vector cutscene_rot

sequence_index ah3a_seq

door_data_struct trigger_scene_door


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.

PROC TS_AGENCY3A_RESET()

	trigger_scene_flow_status = 0

	lester_scene_id = -1

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_AGENCY3A_REQUEST_ASSETS()

	request_model(get_npc_ped_model(char_lester))
	request_model(prop_cs_walking_stick)
	request_model(prop_cs_lester_crate)
	request_anim_dict("missheist_agency3aleadinoutah_3a_int")
	
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_AGENCY3A_RELEASE_ASSETS()
	
	set_model_as_no_longer_needed(get_npc_ped_model(char_lester))
	set_model_as_no_longer_needed(prop_cs_walking_stick)
	set_model_as_no_longer_needed(prop_cs_lester_crate)
	
	remove_anim_dict("missheist_agency3aleadinoutah_3a_int")
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_AGENCY3A_HAVE_ASSETS_LOADED()

	if has_model_loaded(get_npc_ped_model(char_lester))
	and has_model_loaded(prop_cs_walking_stick)
	and has_model_loaded(prop_cs_lester_crate)
	and has_anim_dict_loaded("missheist_agency3aleadinoutah_3a_int")

		RETURN TRUE
		
	endif 
	
	return false
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_AGENCY3A_CREATE()

	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	add_ped_for_dialogue(g_sTriggerSceneAssets.conversation, 0, player_ped_id(), "michael") 
	
	create_npc_ped_on_foot(g_sTriggerSceneAssets.ped[0], char_lester, <<708.1230, -966.4412, 29.3956>>, 16.7531) 
	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 5, g_sTriggerSceneAssets.ped[0], "lester")
	
	g_sTriggerSceneAssets.object[0] = create_object(prop_cs_walking_stick, <<708.1230, -966.4412, 30.3956>>)
	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.00, 0.0, 0.0>>, true, true) 
	
	cutscene_pos = <<708.119, -966.655, 30.411>> 
	cutscene_rot = <<0.0, 0.0, 50.4>> 
	
	lester_scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], lester_scene_id, "missheist_agency3aleadinoutah_3a_int", "_leadin_loop_lester", INSTANT_BLEND_IN, normal_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	set_synchronized_scene_looped(lester_scene_id, true)
	force_ped_ai_and_animation_update(g_sTriggerSceneAssets.ped[0])
	
	g_sTriggerSceneAssets.object[1] = create_object_no_offset(prop_cs_lester_crate, <<707.320, -964.610, 29.500>>)
	set_entity_heading(g_sTriggerSceneAssets.object[1], 253.500)

	if not is_door_registered_with_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE))
		trigger_scene_door = GET_DOOR_DATA(DOORNAME_SWEATSHOP_OFFICE)
		add_door_to_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), trigger_scene_door.model, trigger_scene_door.coords)	
	endif 
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), -1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_force_locked_this_frame, false, true)

	if not is_door_registered_with_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE))
		trigger_scene_door = GET_DOOR_DATA(DOORNAME_SWEATSHOP_STORE)
		add_door_to_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), trigger_scene_door.model, trigger_scene_door.coords)	
	endif 
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), 0.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_force_locked_this_frame, false, true)
	
	if not is_door_registered_with_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE))
		trigger_scene_door = GET_DOOR_DATA(DOORNAME_SWEATSHOP_OFFICE_STORE)
		add_door_to_system(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), trigger_scene_door.model, trigger_scene_door.coords)	
	endif 
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), 1.0, false, false) 
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_force_locked_this_frame, false, true)
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_AGENCY_3A, "AH_3A_INT", CS_ALL, CS_NONE, CS_NONE)
	//MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_AGENCY_3A, "AH_3A_INT_CONCAT", CS_ALL, CS_NONE, CS_NONE)
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_AGENCY_3A, "lester", g_sTriggerSceneAssets.ped[0])  
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_HEIST_AGENCY_3A, "lester", ANCHOR_EYES, 0, 0)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_AGENCY3A_RELEASE()

	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	
	trigger_scene_release_object(g_sTriggerSceneAssets.object[0])
	trigger_scene_release_object(g_sTriggerSceneAssets.object[1])
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()

//	//Release doors so they can swing again.
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_UNLOCKED, FALSE, TRUE)
//	
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
//	
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
	
	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE))
	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE))
	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE))
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_AGENCY3A_DELETE()

	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
	//Release doors so they can swing again.
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_UNLOCKED, FALSE, TRUE)
//	
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)
//	
//	DOOR_SYSTEM_SET_HOLD_OPEN(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), FALSE)
//	DOOR_SYSTEM_SET_DOOR_STATE(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_UNLOCKED, FALSE, TRUE)

	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE))
	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_STORE))
	REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE))
	
ENDPROC

func bool has_ped_task_finished_2(ped_index ped, script_task_name taskname = script_task_perform_sequence, bool use_seq = true, int sequence_progress = -2)
	
	scripttaskstatus status
	status = get_script_task_status(ped, taskname)
	
	if use_seq
	
		if status = finished_task 
		or status = dormant_task
		or get_sequence_progress(ped) = sequence_progress
			return true
		endif
		
	else 
	
		if status = finished_task 
		or status = dormant_task
			return true
		endif
	
	endif 
	
	return false
endfunc


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_AGENCY3A_HAS_BEEN_TRIGGERED()

	//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("lester", g_sTriggerSceneAssets.ped[0])

	switch trigger_scene_flow_status 
	
		case 0
	
			if is_entity_in_angled_area(player_ped_id(), <<714.499, -965.007, 29.395>>, <<720.499, -965.007, 31.395>>, 4.5)//2.0    
				
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
				
				cutscene_pos = <<708.119, -966.655, 30.411>> 
				cutscene_rot = <<0.0, 0.0, 50.4>> 
				
				lester_scene_id = CREATE_SYNCHRONIZED_SCENE(cutscene_pos, cutscene_rot)
				TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], lester_scene_id, "missheist_agency3aleadinoutah_3a_int", "_leadin_action_lester", normal_BLEND_IN, normal_BLEND_OUT)
				set_synchronized_scene_phase(lester_scene_id, 0.15)
				force_ped_ai_and_animation_update(g_sTriggerSceneAssets.ped[0])
				
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "fbiS4AU", "ah3a_INT_LI", CONV_PRIORITY_VERY_HIGH)

				trigger_scene_flow_status++
				
			endif 

		break 
		
		case 1
		
			if is_entity_in_angled_area(player_ped_id(), <<714.477, -966.454, 29.396>>, <<714.477, -958.954, 32.396>>, 3.1)
				
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.2)
				SET_GAMEPLAY_coord_HINT(<<705.8, -964.4, 31.3>>, 10000, 2000) 
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
	
				open_sequence_task(ah3a_seq)
					task_follow_nav_mesh_to_coord(null, <<710.1847, -964.2543, 29.3957>>, pedmove_walk, -1, 0.2, enav_default, 89.7550) 
				close_sequence_task(ah3a_seq)
				task_perform_sequence(player_ped_id(), ah3a_seq)
				clear_sequence_task(ah3a_seq)
				
				set_player_control(player_id(), false, spc_leave_camera_control_on)
				
				trigger_scene_flow_status++

			endif 
		
			if is_synchronized_scene_running(lester_scene_id)

				if get_synchronized_scene_phase(lester_scene_id) >= 0.78
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					
						stop_gameplay_hint()
					
						return true 
					
					endif 
					
				endif 

			endif 
			
//			if is_entity_in_angled_area(player_ped_id(), <<709.616, -965.395, 29.396>>, <<709.616, -960.095, 32.396>>, 1.0) 
//			or is_entity_in_angled_area(player_ped_id(), <<709.789, -965.364, 29.396>>, <<709.789, -963.614, 32.396>>, 2.2)  
//				
//				set_player_control(player_id(), false, spc_leave_camera_control_on)
//
//				trigger_scene_flow_status++
//			endif 
		
		break 
		
		case 2
		
			//if not is_entity_in_angled_area(player_ped_id(), <<709.616, -965.395, 29.396>>, <<709.616, -960.095, 32.396>>, 1.0) 
			if not is_entity_in_angled_area(player_ped_id(), <<709.789, -965.364, 29.396>>, <<709.789, -963.614, 32.396>>, 2.2)  
		
				if has_ped_task_finished_2(player_ped_id())
				
					open_sequence_task(ah3a_seq)
						task_follow_nav_mesh_to_coord(null, <<705.8, -964.4, 31.3>>, pedmove_walk, -1, 0.2, enav_default, 89.7550) 
					close_sequence_task(ah3a_seq)
					task_perform_sequence(player_ped_id(), ah3a_seq)
					clear_sequence_task(ah3a_seq)
					
				endif 
				
			endif 
		
			if get_synchronized_scene_phase(lester_scene_id) >= 0.78
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				
					stop_gameplay_hint()
				
					return true 
				
				endif 
				
			endif 
		
		break 
		
	endswitch 
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_AGENCY3A_HAS_BEEN_DISRUPTED()

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			
			IF IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[0])
			OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[0])
			or is_bullet_in_area(get_entity_coords(g_sTriggerSceneAssets.ped[0]), 5.0)
			OR IS_EXPLOSION_IN_sphere(EXP_TAG_DONTCARE, get_entity_coords(g_sTriggerSceneAssets.ped[0]), 5.0)
			
				RETURN TRUE
			
			ENDIF
		ELSE
			RETURN TRUE
		endif 
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_AGENCY3A_IS_BLOCKED()
	RETURN NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PREP_2_DONE)
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_AGENCY3A_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_AGENCY3A_AMBIENT_UPDATE()
ENDPROC
