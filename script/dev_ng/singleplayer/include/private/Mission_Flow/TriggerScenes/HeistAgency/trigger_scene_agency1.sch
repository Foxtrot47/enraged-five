//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Agency Heist 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"

CONST_FLOAT 	TS_AGENCY1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_AGENCY1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_AGENCY1_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_AGENCY1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_AGENCY1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

INT iLoopScene, iActionScene

structPedsForConversation 	sSpeech

BOOL bAgency1Triggered = FALSE
BOOL bActionSceneStarted = FALSE
BOOL bAgency1PInteriorPinned = FALSE

INTERIOR_INSTANCE_INDEX intGarmentFactory

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_AGENCY1_RESET()
ENDPROC

/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_AGENCY1_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("missheistfbisetup1leadinoutah_1_int")
	REQUEST_MODEL(PROP_CS_WALKING_STICK)
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
	REQUEST_ADDITIONAL_TEXT("FBS1AUD", MISSION_DIALOGUE_TEXT_SLOT)
ENDPROC

/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_AGENCY1_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("missheistfbisetup1leadinoutah_1_int")
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT, TRUE)
	IF bAgency1PInteriorPinned
		UNPIN_INTERIOR(intGarmentFactory)
		bAgency1PInteriorPinned = FALSE
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_AGENCY1_HAVE_ASSETS_LOADED()
	RETURN HAS_ANIM_DICT_LOADED("missheistfbisetup1leadinoutah_1_int")
	AND HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
	AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LESTER))
	AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_AGENCY1_CREATE()

	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, << 719.362, -962.388, 29.910 >>)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[0], RELGROUPHASH_PLAYER)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
		SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0, 0)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
		g_sTriggerSceneAssets.object[1] = CREATE_OBJECT_NO_OFFSET(PROP_CS_WALKING_STICK, <<719.0610, -963.2040, 30.4097>>)
		SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[1], <<-0.0000, 0.0000, -73.7522>>)
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
	AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLoopScene)
		#IF IS_DEBUG_BUILD PRINTLN("STARTING LOOP SCENE") #ENDIF
		iLoopScene = CREATE_SYNCHRONIZED_SCENE(<< 719.362, -962.388, 29.910 >>, << 0, 0, 0>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iLoopScene, "missheistfbisetup1leadinoutah_1_int", "_intro_loop_lester", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
//		PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], iLoopScene, "_intro_loop_chair", "missheistfbisetup1leadinoutah_1_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[1], iLoopScene, "_intro_loop_cane", "missheistfbisetup1leadinoutah_1_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(iLoopScene, TRUE)
	ENDIF
	
	CREATE_MODEL_HIDE(<<718.8765, -962.5751, 29.9050>>, 0.1, V_IND_SS_CHAIR01, TRUE)
	
	IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
		MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_AGENCY_1, "AH_1_INT", CS_ALL, CS_NONE, CS_NONE)
	ENDIF
	
	IF bAgency1PInteriorPinned
		bAgency1PInteriorPinned = FALSE
	ENDIF
	
	bActionSceneStarted = FALSE
	bAgency1Triggered = FALSE
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_AGENCY1_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_AGENCY1_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_AGENCY1_HAS_BEEN_TRIGGERED()

	IF NOT g_sTriggerSceneAssets.flag
		
		CLEAR_AREA(<<717.8286, -961.6875, 29.3953>>, 5, TRUE)
		CLEAR_AREA(<<714.3016, -971.0053, 29.3953>>, 5, TRUE)
		g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<714.3340, -963.6519, 29.3953>> - <<20, 20, 0>>, <<714.3340, -963.6519, 29.3953>> + <<20, 20, 10>>)
		g_sTriggerSceneAssets.flag = TRUE
		
	ENDIF
		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_AGENCY), TS_AGENCY1_TRIGGER_DIST)
		#ENDIF
		IF bAgency1Triggered
			REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
//			STOP_GAMEPLAY_HINT()
			TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_AGENCY1_HAS_BEEN_DISRUPTED()
	IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
	OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[0])
	OR IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
		STOP_GAMEPLAY_HINT()
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_AGENCY1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_AGENCY1_UPDATE()

	IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF NOT bAgency1PInteriorPinned
			intGarmentFactory = GET_INTERIOR_AT_COORDS(<<719.362976,-962.393799,29.906418>>)
			PIN_INTERIOR_IN_MEMORY(intGarmentFactory)	
			bAgency1PInteriorPinned = TRUE
		ELSE
			IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
				IF IS_INTERIOR_READY(intGarmentFactory)
					REQUEST_MODEL(V_IND_SS_CHAIR3_CSO)
					IF HAS_MODEL_LOADED(V_IND_SS_CHAIR3_CSO)
						g_sTriggerSceneAssets.object[0] = CREATE_OBJECT_NO_OFFSET(V_IND_SS_CHAIR3_CSO, <<719.362976,-962.393799,29.906418>>)
						SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[0], <<-0.000000,0.000000,13.506772>>)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<717.748596,-958.600342,28.474697>>, <<717.859619,-969.391907,32.667175>>, 10.250000)
		AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		AND IS_PED_FACING_PED(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 160)
		
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, 13000)
					SET_GAMEPLAY_HINT_FOV(40.0)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.460)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.000)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.100)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				ENDIF
			ELSE
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
			ENDIF
			
		ELSE
		
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			
		ENDIF
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<717.525452,-964.719788,30.145330>>,<<4.250000,5.750000,2.500000>>)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		
			IF NOT bActionSceneStarted
			
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<717.445313,-965.758606,29.145330>>,<<1.500000,10.000000,2.250000>>)
				
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						ADD_PED_FOR_DIALOGUE(sSpeech, 1, g_sTriggerSceneAssets.ped[0], "LESTER")
						ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
					ENDIF
					
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					
					iActionScene = CREATE_SYNCHRONIZED_SCENE(<< 719.362, -962.388, 29.910 >>, << 0, 0, 0>>)
					CLEAR_PED_TASKS(g_sTriggerSceneAssets.ped[0])
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iActionScene, "missheistfbisetup1leadinoutah_1_int", "_leadin_action_lester", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], iActionScene, "_leadin_action_chair", "missheistfbisetup1leadinoutah_1_int", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[1], iActionScene, "_leadin_action_cane", "missheistfbisetup1leadinoutah_1_int", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<718.8762, -964.1069, 29.3953>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 352.5665)
//					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK) 
					PRINTLN("Action scene started")
					
					bActionSceneStarted = TRUE
					
				ENDIF
				
			ELSE
			
				PRINTLN("action scene running")
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_SYNCHRONIZED_SCENE_PHASE(iActionScene) > 0.125
					AND GET_SYNCHRONIZED_SCENE_PHASE(iActionScene) < 0.5
						CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_INT_LI", CONV_PRIORITY_HIGH)
					ENDIF
				ENDIF
				
				TEXT_LABEL_23 sLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				
				IF (IS_SYNCHRONIZED_SCENE_RUNNING(iActionScene) and GET_SYNCHRONIZED_SCENE_PHASE(iActionScene) >= 0.95)
				OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iActionScene)
					IF NOT ARE_STRINGS_EQUAL(sLabel, "FBS1_INT_LI")
						bAgency1Triggered = TRUE
					ENDIF
				ENDIF
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iActionScene)
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLoopScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iActionScene) >= 1.0
						iLoopScene = CREATE_SYNCHRONIZED_SCENE(<< 719.362, -962.388, 29.910 >>, << 0, 0, 0>>)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iLoopScene, "missheistfbisetup1leadinoutah_1_int", "_intro_loop_lester", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], iLoopScene, "_intro_loop_chair", "missheistfbisetup1leadinoutah_1_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[1], iLoopScene, "_intro_loop_cane", "missheistfbisetup1leadinoutah_1_int", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						SET_SYNCHRONIZED_SCENE_LOOPED(iLoopScene, TRUE)
					ENDIF
				ENDIF
				
			ENDIF
		
		ENDIF
		
		SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[0], PRF_DisablePlayerMeleeFriendlyAttacks, TRUE)
		
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<696.425964,-961.024719,22.882015>>, <<739.275208,-961.294128,35.816986>>, 30.500000)
	AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_AGENCY1_AMBIENT_UPDATE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<696.425964,-961.024719,22.882015>>, <<739.275208,-961.294128,35.816986>>, 30.500000)
	AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
	ENDIF
ENDPROC
