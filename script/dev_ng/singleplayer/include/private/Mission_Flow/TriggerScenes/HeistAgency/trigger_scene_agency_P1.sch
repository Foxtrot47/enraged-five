//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Agency Heist Prep 1 - Trigger Scene Data					│
//│																				│
//│								Date: 26/10/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//│					g_sTriggerSceneAssets.veh[0] - Firetruck					│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "emergency_call.sch"

CONST_FLOAT 	TS_AGENCY_P1_STREAM_IN_DIST			80.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_AGENCY_P1_STREAM_OUT_DIST		100.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT 	TS_AGENCY_P1_BATTLE_BUDDY_CALL_DIST	500.0 // Distance from trigger's blip at which the player can call in battle buddies.

CONST_FLOAT		TS_AGENCY_P1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_AGENCY_P1_FRIEND_ACCEPT_BITS		BIT_MICHAEL|BIT_FRANKLIN			//Friends who can trigger the mission with the player.

CONST_INT  		TS_AGENCY_P1_NUMBER_OF_FIREMEN 	4

BOOL bEmergencyCallsForceNoSuppression = FALSE

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_AGENCY_P1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_AGENCY_P1_REQUEST_ASSETS()
	REQUEST_MODEL(FIRETRUK)
	REQUEST_MODEL(S_M_Y_FIREMAN_01)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_AGENCY_P1_RELEASE_ASSETS()	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), " - requesting scene assets")
	#ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(FIRETRUK)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_FIREMAN_01)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_AGENCY_P1_HAVE_ASSETS_LOADED()
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), " - loading scene assets")
	#ENDIF
	RETURN HAS_MODEL_LOADED(FIRETRUK)
	AND HAS_MODEL_LOADED(S_M_Y_FIREMAN_01)
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_AGENCY_P1_CREATE()

	ADD_RELATIONSHIP_GROUP("FIREMEN", g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<215.7186, -1644.6219, 28.7719>>, 357.5736)
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<214.4446, -1643.7203, 28.7776>>, 279.1055)
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<217.0826, -1644.1172, 28.7155>>, 72.8262)
	
	INT iTemp
	FOR iTemp = 0 TO TS_AGENCY_P1_NUMBER_OF_FIREMEN-1
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[iTemp])
			TASK_START_SCENARIO_IN_PLACE(g_sTriggerSceneAssets.ped[iTemp], "WORLD_HUMAN_SMOKING")
			SET_PED_COMBAT_ATTRIBUTES(g_sTriggerSceneAssets.ped[iTemp], CA_ALWAYS_FLEE, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[iTemp], g_sTriggerSceneAssets.relGroup)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[iTemp], PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[iTemp], PED_COMP_HEAD, iTemp%2, iTemp)
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN(GET_THIS_SCRIPT_NAME(), " - trigger scene created")
	#ENDIF
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_AGENCY_P1_RELEASE()
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	INT i 
	REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) i
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[i])
		ENDIF
	ENDREPEAT
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_AGENCY_P1_DELETE()
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	INT i 
	REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) i
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
		ENDIF
	ENDREPEAT
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_AGENCY_P1_HAS_BEEN_TRIGGERED()

	IF g_sTriggerSceneAssets.id = 0
		g_sTriggerSceneAssets.id = GET_HASH_KEY("AGENCY_PREP_1")
	ENDIF
	
	//Display a help message to get in one of the firetrucks
	//if the player gets close enough to the one created by the trigger scene.
	IF NOT g_bAgencyP1HelpDisplayed
		SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_AHP1")
			CASE FHS_EXPIRED
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					ADD_HELP_TO_FLOW_QUEUE("AM_H_AHP1", FHP_HIGH, 0, 2000, DEFAULT_GOD_TEXT_TIME) 
				ENDIF
			BREAK
			CASE FHS_DISPLAYED
				g_bAgencyP1HelpDisplayed = TRUE
			BREAK
		ENDSWITCH
	ENDIF
	
	// Display recurring help text to remind the player about this mission every 8 hours
	// that they don't trigger it. TODO 994283.
	IF g_eAgencyP1TimeNextHelp = INVALID_TIMEOFDAY
		g_eAgencyP1TimeNextHelp = GET_CURRENT_TIMEOFDAY()
		IF g_bAgencyP1OnlyMissionLeft
			// If this is the only mission left increase help frequency.
			ADD_TIME_TO_TIMEOFDAY(g_eAgencyP1TimeNextHelp, 0, 0, 2)
		ELSE
			
			ADD_TIME_TO_TIMEOFDAY(g_eAgencyP1TimeNextHelp, 0, 0, 7)
		ENDIF
		
	//Check if it's time to display the help again.
	ELIF IS_NOW_AFTER_TIMEOFDAY(g_eAgencyP1TimeNextHelp)
		SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_PREP6")
			CASE FHS_EXPIRED
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					ADD_HELP_TO_FLOW_QUEUE("AM_H_PREP6", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL|BIT_FRANKLIN) 
				ENDIF
			BREAK
			CASE FHS_DISPLAYED
				g_eAgencyP1TimeNextHelp = GET_CURRENT_TIMEOFDAY()
				IF g_bAgencyP1OnlyMissionLeft
					// If this is the only mission left increase help frequency.
					ADD_TIME_TO_TIMEOFDAY(g_eAgencyP1TimeNextHelp, 0, 0, 2)
				ELSE
					ADD_TIME_TO_TIMEOFDAY(g_eAgencyP1TimeNextHelp, 0, 0, 7)
				ENDIF
				g_txtFlowHelpLastDisplayed = ""
				#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 txtTimeNextHelp
					txtTimeNextHelp = TIMEOFDAY_TO_TEXT_LABEL(g_eAgencyP1TimeNextHelp)
					CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_AGENCY_PREP1> Trigger help re-displayed. Time it will next display is ", txtTimeNextHelp, ".")
				#ENDIF
			BREAK
		ENDSWITCH
	
	//Check if no other missions are available, display the help immediately.
	ELIF NOT g_bAgencyP1OnlyMissionLeft
		#IF IS_DEBUG_BUILD
			IF NOT g_flowUnsaved.bUpdatingGameflow
		#ENDIF
			IF NOT g_flowUnsaved.bFlowControllerBusy
				IF IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_PREP_1)
					INT iMissionsLeft = 0
					INT iAvailableMissionIndex
					REPEAT MAX_MISSIONS_AVAILABLE iAvailableMissionIndex
						IF g_availableMissions[iAvailableMissionIndex].index != ILLEGAL_ARRAY_POSITION
							iMissionsLeft++
						ENDIF
					ENDREPEAT
					IF iMissionsLeft <= 1
						CPRINTLN(DEBUG_TRIGGER, "<TRIG-SCENE-H_AGENCY_PREP1> Agency Prep is the only mission left to do. Fast tracking help messages.")
						g_eAgencyP1TimeNextHelp = GET_CURRENT_TIMEOFDAY()
						g_bAgencyP1OnlyMissionLeft = TRUE
					ENDIF
				ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
			ENDIF
		#ENDIF
	ENDIF
	
	IF NOT g_sTriggerSceneAssets.flag
		VECTOR vFiretruckPos = <<202.0504, -1655.7734, 28.8031>>
		g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(vFiretruckPos - <<30,30,10>>, vFiretruckPos + <<30,30,10>>)
		CLEAR_AREA(vFiretruckPos, 20.0, TRUE)
		g_sTriggerSceneAssets.flag = TRUE
	ENDIF
	
	//Trigger the mission if the player gets in any firetruck.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FIRETRUK)
				g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK)
			OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK2)
				IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
						IF NOT IS_VEHICLE_EMPTY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
							DETACH_ENTITY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
						ELSE
							g_sTriggerSceneAssets.veh[1] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_AGENCY_P1_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_AGENCY_P1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene. 
PROC TS_AGENCY_P1_UPDATE()
	
	IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
		VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(vehTemp)
		AND IS_VEHICLE_MODEL(vehTemp, FIRETRUK)
			INT iTemp
			FOR iTemp = 0 TO TS_AGENCY_P1_NUMBER_OF_FIREMEN-1
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iTemp])
				AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehTemp)) < 30					
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, g_sTriggerSceneAssets.relGroup, RELGROUPHASH_PLAYER)
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
	
	IF ARE_EMERGENCY_CALLS_SUPPRESSED()
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENDIF
	
	IF NOT bEmergencyCallsForceNoSuppression
		g_bEmergencyCallsSuppressed = FALSE
		g_sEmergencyCallsSuppressedByThisScript = "NULL"
		g_bForceEmergencyCallAnswerphone = FALSE
		bEmergencyCallsForceNoSuppression = TRUE
	ELSE
		IF g_bEmergencyCallsSuppressed = FALSE
		OR NOT ARE_STRINGS_EQUAL(g_sEmergencyCallsSuppressedByThisScript, "NULL")
		OR g_bForceEmergencyCallAnswerphone = FALSE
			bEmergencyCallsForceNoSuppression = FALSE
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_AGENCY_P1_AMBIENT_UPDATE()

	IF ARE_EMERGENCY_CALLS_SUPPRESSED()
		RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	ENDIF
	
	IF NOT bEmergencyCallsForceNoSuppression
		g_bEmergencyCallsSuppressed = FALSE
		g_sEmergencyCallsSuppressedByThisScript = "NULL"
		g_bForceEmergencyCallAnswerphone = FALSE
		bEmergencyCallsForceNoSuppression = TRUE
	ELSE
		IF g_bEmergencyCallsSuppressed = FALSE
		OR NOT ARE_STRINGS_EQUAL(g_sEmergencyCallsSuppressedByThisScript, "NULL")
		OR g_bForceEmergencyCallAnswerphone = FALSE
			bEmergencyCallsForceNoSuppression = FALSE
		ENDIF
	ENDIF
	
ENDPROC
