//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Chinese 1 - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│							ped[0] = Drunk fighter 1							│
//│							ped[1] = Drunk fighter 2							│
//│							ped[2] = Drunk old man 1							│
//│							ped[3] = Drunk old man 2							│
//│							ped[4] = Bar lady									│
//│							object[0] = Tumbler 1								│
//│							object[1] = Tumbler 2								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "flow_mission_trigger_public.sch"
USING "script_heist.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_CHINESE1_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_CHINESE1_STREAM_OUT_DIST		105.0	// Distance at which the scene is deleted and unloaded.	

CONST_FLOAT		TS_CHINESE1_FRIEND_REJECT_DIST	35.0			//Distance friends will bail from player group.
CONST_INT		TS_CHINESE1_FRIEND_ACCEPT_BITS	BIT_NOBODY		//Friends who can trigger the mission with the player.

//State flag bit indexes.
CONST_INT CHIN1_BAR_LADY_REACTING		0
CONST_INT CHIN1_FIGHT_PREPPED			1
CONST_INT CHIN1_FIGHT_STARTED			2
CONST_INT CHIN1_FIGHT_AUDIO_STARTED		3
CONST_INT CHIN1_CUTSCENE_REQUESTED		4
CONST_INT CHIN1_FIGHT_AMBIENT_LOOPING	5
CONST_INT CHIN1_FIGHT_AMBIENT_LOOPING_B	6
CONST_INT CHIN1_DOOR_PINNED_OPEN		7
CONST_INT CHIN1_BAR_CONTROL_PLAYER		8

INT iStateFlags = 0
INT iTimeBarladyReacted = -1
INT iTimeInDoorway = -1
INT iBarfightScene = 0
INT iBarfightSceneB = 0
INT iOldman1Scene = 0
INT iOldman2Scene = 0

VECTOR vScenePosBarGeneral = <<1987.235, 3052.738, 46.220>>
VECTOR vScenePosOldman1 = << 1986.785, 3052.088, 46.320 >>
VECTOR vScenePosOldman2 = << 1987.035, 3052.288, 46.340 >>
VECTOR vSceneRot = <<0.000, 0.000, 53.640>>

//STRING	sTrevAnimDict = "amb@world_human_stand_impatient@male@no_sign@idle_a"

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_CHINESE1_RESET()
	iStateFlags = 0
	iTimeBarladyReacted = -1
	iTimeInDoorway = -1
	iBarfightScene = 0
	iBarfightSceneB = 0
	iOldman1Scene = 0
	iOldman2Scene = 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_CHINESE1_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "MISSCHINESE1LEADINOUTCHINESE_1_INT")
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_OLD_MAN2)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_OLD_MAN1A)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_JOSEF)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_RUSSIANDRUNK)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_JANET)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_TUMBLER_01)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_CHINESE1_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 4)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iBarfightScene)
		IF IS_BIT_SET(iStateFlags, CHIN1_FIGHT_AUDIO_STARTED)
			CPRINTLN(DEBUG_TRIGGER, "RELEASE: STOP_SYNCHRONIZED_AUDIO_EVENT")
			STOP_SYNCHRONIZED_AUDIO_EVENT(iBarfightScene)
		ENDIF
	ELSE
		CPRINTLN(DEBUG_TRIGGER, "RELEASE: PLAY then STOP_SYNCHRONIZED_AUDIO_EVENT")
		iBarfightScene = CREATE_SYNCHRONIZED_SCENE(vScenePosBarGeneral, vSceneRot)
		PLAY_SYNCHRONIZED_AUDIO_EVENT(iBarfightScene)
		STOP_SYNCHRONIZED_AUDIO_EVENT(iBarfightScene)
	ENDIF
		
	IF IS_BIT_SET(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
		DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), DOORSTATE_UNLOCKED, FALSE, TRUE)
		CLEAR_BIT(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_CHINESE1_HAVE_ASSETS_LOADED()	
	IF NOT IS_BIT_SET(iStateFlags, CHIN1_FIGHT_PREPPED)
		INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("CHINESE_1_INT_LEADIN", (<<1986.4412, 3052.1626, 46.2152>>))
		SET_BIT(iStateFlags, CHIN1_FIGHT_PREPPED)
	ENDIF
	
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
	AND PREPARE_SYNCHRONIZED_AUDIO_EVENT("CHINESE_1_INT_LEADIN", 0)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_CHINESE1_CREATE()
	iStateFlags = 0
	iTimeBarladyReacted = -1
	iTimeInDoorway = -1
	iBarfightScene = 0
	iBarfightSceneB = 0
	iOldman1Scene = 0
	iOldman2Scene = 0

	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1984.9971, 3052.9048, 46.8556>>+<<5,5,2>>,<<1984.9971, 3052.9048, 46.8556>>-<<5,5,2>>)
	// @SBA - making smaller so we don't clean up vehicles out in the parking lot
	//CLEAR_AREA(<<1988.8932, 3048.0491, 46.2151>>, 20.0, TRUE)
	CLEAR_AREA(<<1984.136353,3051.933838,46.215084>> , 5.5, TRUE)
	CLEAR_AREA(<<1991.078857,3047.380127,46.215122>> , 5.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	//Create drunk figher 1.
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, IG_JOSEF, <<1990.4844, 3047.2654, 46.2151>>, 55.6093)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JOSEF)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], TRUE)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	//Create drunk figher 2.
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, IG_RUSSIANDRUNK, <<1989.1708, 3048.3350, 46.2151>>, 242.9991)
	SET_MODEL_AS_NO_LONGER_NEEDED(CS_RUSSIANDRUNK)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[1], TRUE)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	
	//Create drunk old man 1.
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, IG_OLD_MAN1A, <<1984.4114, 3051.5398, 46.2151>>, 242.9991)
	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[2], ANCHOR_HEAD,0)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN1A)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[2], TRUE)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
	
	//Create tumbler 1.
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT_NO_OFFSET(PROP_TUMBLER_01, <<1984.75, 3053.25, 47.33>>)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[0], TRUE)
	
	//Create drunk old man 2.
	g_sTriggerSceneAssets.ped[3] = CREATE_PED(PEDTYPE_MISSION, IG_OLD_MAN2, <<1984.4114, 3051.5398, 46.2151>>, 242.9991)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_OLD_MAN2)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[3], TRUE)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
	
	//Create bar lady.
	g_sTriggerSceneAssets.ped[4] = CREATE_PED(PEDTYPE_MISSION, IG_JANET, <<1982.5602, 3053.4136, 46.2151>>, 242.9991)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JANET)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
	SET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[4], (<<1984.312, 3054.372, 46.240>>))
	SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[4], -118.8)

	
	//Start the people around the bar playing their scenes.
	iOldman1Scene = CREATE_SYNCHRONIZED_SCENE(vScenePosOldman1, vSceneRot)
	SET_SYNCHRONIZED_SCENE_LOOPED(iOldman1Scene, TRUE)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[2], iOldman1Scene, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "OLDMAN1_LEADIN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
	
	iOldman2Scene = CREATE_SYNCHRONIZED_SCENE(vScenePosOldman2, vSceneRot)
	SET_SYNCHRONIZED_SCENE_LOOPED(iOldman2Scene, TRUE)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[3], iOldman2Scene, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "OLDMAN2_LEADIN_LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
	
	TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[4], "MISSCHINESE1LEADINOUTCHINESE_1_INT", "janet_leadin_outro_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
	TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[4], PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)

//	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), 1.1750, FALSE, FALSE)
//	DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), TRUE)
//	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), DOORSTATE_LOCKED, FALSE, TRUE)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(v_ilev_fh_door4, (<<1988.3529, 3054.4106, 47.3204>>), TRUE, 0, 0.0, 1.175)
	SET_BIT(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
	
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 4, g_sTriggerSceneAssets.ped[4], "JANET")
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_CHINESE1_RELEASE()
	INT index

	//Make everyone except the bar lady flee.
	REPEAT 4 index
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	//Set the bar lady to cower
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[4])
	
	//Release scene objects.
	REPEAT 2 index
		TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[index])
	ENDREPEAT
	
	IF IS_BIT_SET(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
		DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), DOORSTATE_UNLOCKED, FALSE, TRUE)
		CLEAR_BIT(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
	ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 4)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_CHINESE1_DELETE()
	INT index

	//Delete scene peds.
	REPEAT 5 index
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	//Delete scene objects.
	REPEAT 2 index
		TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[index])
	ENDREPEAT
	
	IF IS_BIT_SET(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
		DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_HICK_BAR_F_INT), DOORSTATE_UNLOCKED, FALSE, TRUE)
		CLEAR_BIT(iStateFlags, CHIN1_DOOR_PINNED_OPEN)
	ENDIF
	
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 4)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_CHINESE1_HAS_BEEN_TRIGGERED()
	BOOL bTriggered = FALSE
	
	IF IS_BIT_SET(iStateFlags, CHIN1_BAR_LADY_REACTING)
	
		IF (GET_GAME_TIMER() - iTimeBarladyReacted) > 1200 //1800
			bTriggered = TRUE
		ELIF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
//			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[4])
//				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MISSRBHS_MCS_3_2LEADOUT", "TRV_PUKING_LEADOUT")
//					bTriggered = TRUE
//				ENDIF
//			ENDIF
		ELSE
			bTriggered = TRUE
		ENDIF
	ENDIF
	
	IF bTriggered
		INT index
		REPEAT 5 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[index], TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.ped[index])
			ENDIF
		ENDREPEAT
		REPEAT 2 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
//				IF IS_ENTITY_ATTACHED(g_sTriggerSceneAssets.object[index])
//					DETACH_ENTITY(g_sTriggerSceneAssets.object[index], FALSE)
//				ENDIF
				SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.object[index])
			ENDIF
		ENDREPEAT
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iBarfightScene)
			IF IS_BIT_SET(iStateFlags, CHIN1_FIGHT_AUDIO_STARTED)
				CPRINTLN(DEBUG_TRIGGER, "STOP_SYNCHRONIZED_AUDIO_EVENT")
				STOP_SYNCHRONIZED_AUDIO_EVENT(iBarfightScene)
			ENDIF
		ENDIF
		
		REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
		TS_CHINESE1_RELEASE_ASSETS()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_CHINESE1_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
	
		//Check for scene peds being injured or harmed.
		REPEAT 5 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(<<1986.219116,3056.189453,46.215336>>, <<1982.283813,3049.868408,49.464962>>, 5.500000)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<1986.219116,3056.189453,46.215336>>, <<1982.283813,3049.868408,49.464962>>, 5.500000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_CHINESE1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_CHINESE1_UPDATE()
	SEQUENCE_INDEX seq
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	
		// Prevent player from moving too fast when near the door (entryway is too short to slow him down)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), (<<1990.9, 3054.3, 47.5>>), (<<2.0, 2.0, 2.5>>))
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK) 
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF

		//Prevent the player from shooting into the bar area - prevents peds glitching into cover
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1989.53,3053.48,46.09>>, <<2039.93,3053.49,48.13>>, 2.4)
			IF IS_SPHERE_VISIBLE(<<1990.796875,3053.551514,47.402416>> ,0.8)
				FLOAT px,py
				GET_SCREEN_COORD_FROM_WORLD_COORD(<<1989.3,3053.5,47.9>>,px,py)
				IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<1989.4,3052.5,47.9>>)<4)
				OR (ABSF(px-0.5) < 0.1 AND ABSF(py-0.5)<0.3)
					//Block the player shooting in that range
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_ATTACK2)
				ENDIF
			
			ENDIF
		
		ENDIF
		
		FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 5.0
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1990.9, 3054.3, 47.5>>) < (fReplayBlockRange*fReplayBlockRange)	//30.0f recording range
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2229084
		ENDIF

		// make sure we have cutscene
		IF NOT IS_BIT_SET(iStateFlags, CHIN1_CUTSCENE_REQUESTED)
			MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_CHINESE_1, "CHINESE_1_INT", CS_NONE, CS_NONE, CS_ALL)
			SET_BIT(iStateFlags, CHIN1_CUTSCENE_REQUESTED)
		ENDIF
					
		IF NOT IS_BIT_SET(iStateFlags, CHIN1_FIGHT_STARTED)
			//Has player stepped into the doorway locate?
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1988.712158,3051.993408,46.218937>>, <<1989.821045,3053.716797,48.715347>>, 3.500000)
				
				//Start the synced scene of the drunk fighters.
				IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("CHINESE_1_INT_LEADIN", 0)
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0]) AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0]) AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], FALSE)
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[1], FALSE)
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[2], FALSE)
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[3], FALSE)
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[4], FALSE)
							iBarfightScene = CREATE_SYNCHRONIZED_SCENE(vScenePosBarGeneral, vSceneRot)
							//iBarfightSceneB = CREATE_SYNCHRONIZED_SCENE(vScenePosBarGeneral, vSceneRot)
							TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iBarfightScene, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "HUSB_LEADIN_ACTION", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
							TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], iBarfightScene, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "RUSS_LEADIN_ACTION", INSTANT_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
							FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[0])
							FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[1])
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
							IF PLAY_SYNCHRONIZED_AUDIO_EVENT(iBarfightScene)
								SET_BIT(iStateFlags, CHIN1_FIGHT_AUDIO_STARTED)
							ENDIF
							
							// failsafe timer to kick off mission
							iTimeInDoorway  = GET_GAME_TIMER()
							SET_BIT(iStateFlags, CHIN1_FIGHT_STARTED)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
		
			IF NOT IS_BIT_SET(iStateFlags, CHIN1_BAR_CONTROL_PLAYER)
				// @SBA - keep player from going in too far or leaving
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, (<<1988.1724, 3053.6685, 46.2153>>), PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 47.3)
					//TASK_STAND_STILL(NULL, 500)
					//TASK_PLAY_ANIM(NULL, sTrevAnimDict, "idle_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, 1000, AF_NOT_INTERRUPTABLE, 0.585)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
				CLEAR_SEQUENCE_TASK(seq)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 4000)
				SET_BIT(iStateFlags, CHIN1_BAR_CONTROL_PLAYER)
			ENDIF
		
			IF NOT IS_BIT_SET(iStateFlags, CHIN1_FIGHT_AMBIENT_LOOPING)
				//Start the ambient loop of the fight scene if it reaches the end.
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iBarfightScene)
				
					// @SBA - NOTE: currently, we'd never get to these loops
					// Go to loop when main fight is done for 1st guy (husband)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iBarfightScene) > 0.99
						iBarfightScene = CREATE_SYNCHRONIZED_SCENE(vScenePosBarGeneral, vSceneRot)
						SET_SYNCHRONIZED_SCENE_LOOPED(iBarfightScene, TRUE)
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
								TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iBarfightScene, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "HUSB_LEADIN_LOOP", WALK_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							ENDIF
						ENDIF
						// @SBA - for other guy (non-husband), start loop
						iBarfightSceneB = CREATE_SYNCHRONIZED_SCENE(vScenePosBarGeneral, vSceneRot)
						SET_SYNCHRONIZED_SCENE_LOOPED(iBarfightSceneB, TRUE)
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
							IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
								TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], iBarfightSceneB, "MISSCHINESE1LEADINOUTCHINESE_1_INT", "RUSS_LEADIN_LOOP", WALK_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
							ENDIF
						ENDIF

						SET_BIT(iStateFlags, CHIN1_FIGHT_AMBIENT_LOOPING)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF IS_BIT_SET(iStateFlags, CHIN1_CUTSCENE_REQUESTED)
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				CPRINTLN(DEBUG_TRIGGER, "Requesting cutscene component variations")
				SET_CUTSCENE_PED_PROP_VARIATION("Old_Man1A", ANCHOR_HEAD, 0)
				SET_CUTSCENE_PED_PROP_VARIATION("Tao", ANCHOR_EYES, 0)
				SET_CUTSCENE_PED_PROP_VARIATION("Taos_Translator", ANCHOR_EYES, 0)
			ENDIF
		ENDIF
		
		//Has player stepped towards the bar?  OR has he waited in the vestibule long enough?
		IF NOT IS_BIT_SET(iStateFlags, CHIN1_BAR_LADY_REACTING)
		AND IS_BIT_SET(iStateFlags, CHIN1_FIGHT_STARTED)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1986.219116,3056.189453,46.215336>>, <<1982.283813,3049.868408,49.464962>>, 5.500000)
			OR ( ((GET_GAME_TIMER() - iTimeInDoorway) > 2900) AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1988.712158,3051.993408,46.218937>>, <<1989.821045,3053.716797,48.715347>>, 3.500000) ))
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
					IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[4])
						// remove reaction B*1310593
//						TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[4], "MISSCHINESE1LEADINOUTCHINESE_1_INT", "JANET_LEADIN_REACT", <<1984.312, 3054.372, 47.240>>, <<0,0,-118.8>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[4], TRUE)
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "METHAUD", "CHIN_INTL", CONV_PRIORITY_VERY_HIGH)
						iTimeBarladyReacted = GET_GAME_TIMER()
						SET_BIT(iStateFlags, CHIN1_BAR_LADY_REACTING)					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_CHINESE1_AMBIENT_UPDATE()
ENDPROC
