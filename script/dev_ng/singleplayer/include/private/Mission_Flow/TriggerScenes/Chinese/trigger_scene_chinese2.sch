//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Chinese 2 - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_CHINESE2_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_CHINESE2_STREAM_OUT_DIST		160.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_CHINESE2_TRIGGER_DIST		25.0	// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_CHINESE2_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_CHINESE2_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

CONST_INT		BIT_TS_CHINESE2_SYNC_STARTED_0		0
CONST_INT		BIT_TS_CHINESE2_SYNC_STARTED_1		1
CONST_INT		BIT_TS_CHINESE2_SYNC_STARTED_2		2
CONST_INT		BIT_TS_CHINESE2_SYNC_STARTED_3		3
CONST_INT		BIT_TS_CHINESE2_SYNC_STARTED_4		4
CONST_INT		BIT_TS_CHINESE2_INTRO_REQUESTED		5


INT triggerFlag
INT triggertime
INT sceneidBarc = 999
INT sceneOldManAct1
INT sceneOldManAct2
INT stateBitset


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_CHINESE2_RESET()
	triggerFlag=0
	triggertime=0
	sceneidBarc=999
	stateBitset = 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_CHINESE2_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "MISSChinese2_crystalMazeMCS1_IG")
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_JANET)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_OLD_MAN2)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_OLD_MAN1A)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_TAOSTRANSLATOR)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_CHENG))
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_CHINESE2_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_CHINESE2_HAVE_ASSETS_LOADED()
	IF NOT HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN FALSE
	ENDIF
	
	TSPedCompRequest sDummyComp[1]
	sDummyComp[0].texture = -1
	TSPedPropRequest sDummyProp[1]
	sDummyProp[0].prop = -1

	IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[4], CHAR_CHENG, <<1985.6656, 3052.6606, 46.2151>>, sDummyComp, sDummyProp)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_CHINESE2_CREATE()
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	triggerFlag=0
	triggertime=0
	sceneidBarc=999

	CLEAR_AREA(<<1985.9707, 3052.4753, 46.2152>>,6,true)	//Dan H Radius used to be 6
	
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION,IG_JANET,<<1985.6656, 3052.6606, 46.2151>>)
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION,IG_OLD_MAN1A,<<1985.6656, 3052.6606, 46.2151>>)
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION,IG_OLD_MAN2,<<1985.6656, 3052.6606, 46.2151>>)
	g_sTriggerSceneAssets.ped[3] = CREATE_PED(PEDTYPE_MISSION,IG_TAOSTRANSLATOR,<<1985.6656, 3052.6606, 46.2151>>)
	
	//Reveal dummy ped with preloaded variations.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[4])
	ENDIF

	CLEAR_BIT(stateBitset, BIT_TS_CHINESE2_SYNC_STARTED_0)
	CLEAR_BIT(stateBitset, BIT_TS_CHINESE2_SYNC_STARTED_1)
	CLEAR_BIT(stateBitset, BIT_TS_CHINESE2_SYNC_STARTED_2)
	CLEAR_BIT(stateBitset, BIT_TS_CHINESE2_SYNC_STARTED_3)
	CLEAR_BIT(stateBitset, BIT_TS_CHINESE2_SYNC_STARTED_4)
	
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
	
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[4], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
	
	INT sceneidBar = CREATE_SYNCHRONIZED_SCENE(<<1987.231,3052.741,46.214>>,<<0,0,57.6>>)
	INT sceneidBarb = CREATE_SYNCHRONIZED_SCENE(<<1987.231,3052.741,46.214>>,<<0,0,57.6>>)
	INT sceneOldMan1 = CREATE_SYNCHRONIZED_SCENE(<< 1986.938, 3052.200, 46.251 >>,<< -0.000, 0.000, 57.600 >>)
	INT sceneOldMan2 = CREATE_SYNCHRONIZED_SCENE(<< 1987.151, 3052.476, 46.214 >>,<< -0.000, 0.000, 57.600 >>)

	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneidBar,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_loop_janet",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneOldMan1,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_loop_old_a",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], sceneOldMan2,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_loop_old_b",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3], sceneidBar,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_loop_transl",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[4], sceneidBarb,"MISSChinese2_crystalMazeMCS1_IG","Dance_loop_tao",INSTANT_BLEND_IN,INSTANT_BLEND_OUT)

	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[3], INT_TO_ENUM(PED_PROP_POSITION,1), 0, 1)

	SET_SYNCHRONIZED_SCENE_LOOPED(sceneidBar,TRUE)
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneidBarb,TRUE)
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneOldMan1,TRUE)
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneOldMan2,TRUE)
	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1987.6060, 3050.2019, 47.3957>>+<<12,12,12>>,<<1987.6060, 3050.2019, 47.3957>>-<<12,12,12>>)
	
	triggerFlag = 0
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_CHINESE2_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[4])
	
	triggerFlag = 0
	triggertime = 0
	sceneidBarc = 999
	stateBitset = 0
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_CHINESE2_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[4])

	triggerFlag = 0
	triggertime = 0
	sceneidBarc = 999
	stateBitset = 0

	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_CHINESE2_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		// Slow zone around the inn door. Force the player to approach slowly to help streaming.
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1993.068848,3057.777344,46.056694>>, <<1990.494385,3053.400635,48.715355>>, 6.000000)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_DIVE)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		ENDIF

		//Load the intro cutscene at a closer range than the rest of the trigger scene assets.
		IF NOT IS_BIT_SET(stateBitset, BIT_TS_CHINESE2_INTRO_REQUESTED)
			IF VDIST2(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CHINESE_2), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 625 //25^2
				MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_CHINESE_2,
															"CHINESE_2_INT",
															CS_NONE,
															CS_NONE,
															CS_ALL)
				SET_BIT(stateBitset, BIT_TS_CHINESE2_INTRO_REQUESTED)
			ENDIF
		ENDIF
	
		IF triggerFlag > 0			
			STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(player_ped_id()),5.0)
		ENDIF
	
		SWITCH triggerFlag
			CASE 0
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1989.207520,3053.569336,45.590302>>, <<1989.932861,3051.354004,48.340218>>, 1.250000)
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1]) 
					AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2]) 
					AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3]) 
					AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[4]) 
					
						sceneidBarc = CREATE_SYNCHRONIZED_SCENE(<<1987.231,3052.741,46.214>>,<<0,0,57.6>>)
						sceneOldManAct1 = CREATE_SYNCHRONIZED_SCENE(<< 1986.938, 3052.200, 46.251 >>,<< -0.000, 0.000, 57.600 >>)
						sceneOldManACt2 = CREATE_SYNCHRONIZED_SCENE(<< 1987.151, 3052.476, 46.214 >>,<< -0.000, 0.000, 57.600 >>)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],sceneidBarc,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_janet",SLOW_BLEND_IN,INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],sceneOldManAct1,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_old_a",SLOW_BLEND_IN,INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],sceneOldManACt2,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_old_b",SLOW_BLEND_IN,INSTANT_BLEND_OUT)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3],sceneidBarc,"MISSChinese2_crystalMazeMCS1_IG","bar_peds_action_transl",SLOW_BLEND_IN,INSTANT_BLEND_OUT)
						
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					ENDIF
				
					triggerFlag++
					triggertime = GET_GAME_TIMER() + 1200
				ENDIF
			BREAK
			CASE 1
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1988.081177,3055.030762,45.777798>>, <<1986.831299,3052.973389,48.840191>>, 1.000000)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				ENDIF
				IF GET_GAME_TIMER() > triggertime
					STOP_SYNCHRONIZED_AUDIO_EVENT(sceneidBarc)
					RETURN TRUE
				ENDIF
			BREAK
			DEFAULT
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneidBarc)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneidBarc) = 1.0
						STOP_SYNCHRONIZED_AUDIO_EVENT(sceneidBarc)
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK			
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_PED_INTERRUPTED(PED_INDEX paramPed, INT paramBit)
	IF DOES_ENTITY_EXIST(paramPed)
		IF IS_PED_INJURED(paramPed)
			RETURN TRUE
		ELSE
			IF NOT IS_BIT_SET(stateBitset, paramBit)
				IF GET_SCRIPT_TASK_STATUS(paramPed, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
					SET_BIT(stateBitset, paramBit)
				ENDIF
			ELSE
				IF GET_SCRIPT_TASK_STATUS(paramPed, SCRIPT_TASK_SYNCHRONIZED_SCENE) != PERFORMING_TASK
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_CHINESE2_HAS_BEEN_DISRUPTED()
	IF IS_PED_INTERRUPTED(g_sTriggerSceneAssets.ped[0], BIT_TS_CHINESE2_SYNC_STARTED_0)
	OR IS_PED_INTERRUPTED(g_sTriggerSceneAssets.ped[1], BIT_TS_CHINESE2_SYNC_STARTED_1)
	OR IS_PED_INTERRUPTED(g_sTriggerSceneAssets.ped[2], BIT_TS_CHINESE2_SYNC_STARTED_2)
	OR IS_PED_INTERRUPTED(g_sTriggerSceneAssets.ped[3], BIT_TS_CHINESE2_SYNC_STARTED_3)
	OR IS_PED_INTERRUPTED(g_sTriggerSceneAssets.ped[4], BIT_TS_CHINESE2_SYNC_STARTED_4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_CHINESE2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
///    int scriptID

PROC TS_CHINESE2_UPDATE()

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_CHINESE2_AMBIENT_UPDATE()
ENDPROC
