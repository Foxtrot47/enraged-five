//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Martin 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "script_ped.sch"
USING "Flow_Mission_Trigger_Public.sch"
USING "selector_globals.sch"

CONST_FLOAT 	TS_MARTIN1_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_MARTIN1_STREAM_OUT_DIST		125.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_MARTIN1_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_MARTIN1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_MARTIN1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

BOOL MichaelWalkTaskGiven
BOOL doneChatM1_LEADIN
BOOL playerLockedIn
BOOL MichaelPlayingIdleLoop
//BOOL playerControlTurnedOff
BOOL taskLookAtGiven
BOOl player1stTaskGiven
BOOL player2ndTaskGiven

SEQUENCE_INDEX seq

//PED_INDEX pedLastPlayer

SCENARIO_BLOCKING_INDEX MartinHouseScenarioBlockingArea

//SELECTOR_PED_STRUCT sSelectorPeds

INT StartMissionTimer
INT MichaelLeadInSyncScene
INT MichaelActionSyncScene

VECTOR vPlayerCoords

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_MARTIN1_RESET()
	
	MichaelWalkTaskGiven 	= FALSE
	doneChatM1_LEADIN 		= FALSE
	playerLockedIn			= FALSE
	MichaelPlayingIdleLoop 	= FALSE
//	playerControlTurnedOff 	= FALSE
	taskLookAtGiven 		= FALSE
	player1stTaskGiven		= FALSE
	player2ndTaskGiven		= FALSE
	
	StartMissionTimer 		= 0
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_MARTIN1_REQUEST_ASSETS()

	REQUEST_MODEL(FUSILADE)
	REQUEST_ANIM_DICT("MISSMARTIN1@LEADINOUTMARTIN_1_INT")
	
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_MARTIN1_RELEASE_ASSETS()
	
	REMOVE_ANIM_DICT("MISSMARTIN1@LEADINOUTMARTIN_1_INT")
	SET_MODEL_AS_NO_LONGER_NEEDED(FUSILADE)
//	SET_INTERIOR_DISABLED(INTERIOR_V_RANCH, TRUE)

ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_MARTIN1_HAVE_ASSETS_LOADED()
	
	IF HAS_ANIM_DICT_LOADED("MISSMARTIN1@LEADINOUTMARTIN_1_INT")
	AND HAS_MODEL_LOADED(FUSILADE)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_MARTIN1_CREATE()
	
	SET_INTERIOR_CAPPED(INTERIOR_V_RANCH, FALSE)
	
	//Call this here to give me full control over peds variations in cutscenes
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1367.5, 1122.5, 111.4>>, <<1377.8, 1138.7, 117.5>>, FALSE, FALSE)
	
	//Set a scenario blocking area that covers the whole of the property to stop any scenario peds from spawning in.
	CLEAR_AREA(<<1388.6, 1150.0, 114>>, 70, TRUE)
	MartinHouseScenarioBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<1333, 1095, 137>>, <<1438.9, 1197, 109>>)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		pedLastPlayer = sSelectorPeds.PedID[SELECTOR_PED_TREVOR]
	
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, <<1390.7065, 1142.6586, 113.4431>>, 170.5477, TRUE, TRUE)
			PRINTSTRING("Waiting for Michael to be created") PRINTNL()
			WAIT(0)
		ENDWHILE
		WHILE NOT CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[1], CHAR_MICHAEL, <<1371.3683, 1132.9742, 112.8401>>, 20.5505)
			PRINTSTRING("Waiting for Michael's car to be created") PRINTNL()
			WAIT(0)
		ENDWHILE	
		SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[1], VEHICLELOCK_LOCKED)
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
				SET_PED_CAN_BE_TARGETTED(g_sTriggerSceneAssets.ped[0], FALSE)
				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[0], "MICHAEL")
			ENDIF
		ENDIF
		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 2, PLAYER_PED_ID(), "TREVOR")
	ENDIF	
	
	//Create the fusilade parked up the side of the house
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(FUSILADE, <<1404.8268, 1118.4050, 113.8380>>, 90.5742)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[2], 112, 112)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MARTIN_1, "MARTIN_1_INT", CS_ALL, CS_NONE, CS_3|CS_4|CS_5|CS_6|CS_7|CS_8)
	
	//Set up Trev and Michaels clothes for cutscene depending on who the player is 
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()	
			//Trevor
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 4)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 18, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 16, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)				
			
			//Michael
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
			
			PRINTSTRING("TRIGGER SCENE - Trev and Mike set up for cutscene 1.") PRINTNL()
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR

		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()	
			//Michael
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", g_sTriggerSceneAssets.ped[0], PLAYER_ZERO)
				ENDIF
			ENDIF
			
			//Trevor
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
			
			PRINTSTRING("TRIGGER SCENE - Trev and Mike set up for cutscene 2.") PRINTNL()
		ENDIF
		
	ENDIF
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_MARTIN1_RELEASE()

	PRINTSTRING("TS_MARTIN1_RELEASE BEING CALLED") PRINTNL()

	//Release scene vehicles.
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	
	//Release scene peds.	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1367.5, 1122.5, 111.4>>, <<1377.8, 1138.7, 117.5>>, TRUE, TRUE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(MartinHouseScenarioBlockingArea)
	
//	SET_INTERIOR_DISABLED(INTERIOR_V_RANCH, TRUE)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
	IF MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_MARTIN_1
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	ENDIF
	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_MARTIN1_DELETE()

	PRINTSTRING("TS_MARTIN1_DELETE BEING CALLED") PRINTNL()

	//Delete scene vehicles.
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1367.5, 1122.5, 111.4>>, <<1377.8, 1138.7, 117.5>>, TRUE, TRUE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(MartinHouseScenarioBlockingArea)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	
//	SET_INTERIOR_DISABLED(INTERIOR_V_RANCH, TRUE)

	IF MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_MARTIN_1
		SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	ENDIF

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_MARTIN1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MARTIN, GET_CURRENT_PLAYER_PED_INT())
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MARTIN1_TRIGGER_DIST)
		#ENDIF
	
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
			IF fDistanceSquaredFromTrigger < (TS_MARTIN1_TRIGGER_DIST*TS_MARTIN1_TRIGGER_DIST)
				PRINTSTRING("mission starting due to player(Michael) being at trigger") PRINTNL()
				RETURN TRUE
			ENDIF
		ENDIF
	
		IF player2ndTaskGiven = TRUE
			IF playerLockedIn = TRUE
				
//				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
//					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
//							sSelectorPeds.PedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[0]
//						ENDIF
//					ENDIF
//				    MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
//				    TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
//					
////					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.PedID[SELECTOR_PED_TREVOR])
//				ENDIF
			
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						IF IS_SYNCHRONIZED_SCENE_RUNNING(MichaelActionSyncScene)
							IF GET_SYNCHRONIZED_SCENE_PHASE(MichaelActionSyncScene) > 0.940 
								PRINTSTRING("ANIM greater than phase 0.940. main cutscene should be started now") PRINTNL()
								RETURN TRUE
							ENDIF
							IF HAS_ANIM_EVENT_FIRED(g_sTriggerSceneAssets.ped[0], GET_HASH_KEY("START_CUTSCENE"))
								PRINTSTRING("Anim tag START_CUTSCENE has fired main cutscene should be started now") PRINTNL()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_GAME_TIMER() > (StartMissionTimer + 15000)
				OR GET_DISTANCE_BETWEEN_ENTITIES(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID()) > 12
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						PRINTSTRING("StartMissionTimer is over 15 seconds or the distance between Trev and Michael is over 12 metres") PRINTNL()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_MARTIN1_HAS_BEEN_DISRUPTED()
	
	//If player is shooting near the house block mission
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1393.4, 1141.5, 114.4>>) < 70
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	//If Player is Trevor and he damages Michael or Michaels car block mission
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				RETURN TRUE
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[1], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1], TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
		//Mission shouldn't start if Trev drives a vehicle up to the front door.
		IF IS_ANY_VEHICLE_NEAR_POINT(<<1389.3, 1139.8, 113.8>>, 7)
			RETURN TRUE
		ENDIF
	ENDIF
	

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_MARTIN1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_MARTIN1_UPDATE()
	
//	//Make the camera stay focussed on Trev from now till the cutscene starts.
//	IF player2ndTaskGiven = TRUE
//		IF DOES_ENTITY_EXIST(sSelectorPeds.PedID[SELECTOR_PED_TREVOR])
//			IF NOT IS_PED_INJURED(sSelectorPeds.PedID[SELECTOR_PED_TREVOR])
//				PRINTSTRING("SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE IS BEING CALLED NOW") PRINTNL()
//				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.PedID[SELECTOR_PED_TREVOR])
//			ENDIF
//		ELSE
//			PRINTSTRING("TREV DOESN'T EXIST") PRINTNL()
//		ENDIF
//	ENDIF	
	
	//Set up Trev and Michaels clothes for cutscene depending on who the player is 
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()	
			//Trevor
			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 4)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 18, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 16, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)				
			
			//Michael
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
			
			PRINTSTRING("TRIGGER SCENE - Trev and Mike set up for cutscene 3.") PRINTNL()
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR

		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()	
			//Michael
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Michael", g_sTriggerSceneAssets.ped[0], PLAYER_ZERO)
				ENDIF
			ENDIF
			
			//Trevor
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
			
			PRINTSTRING("TRIGGER SCENE - Trev and Mike set up for cutscene 4.") PRINTNL()
		ENDIF
		
	ENDIF	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	
//		//Set up Trev's clothes for opening cutscene
//		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//			//Trevor
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID(), PLAYER_TWO)
//			
//			//Michael
//			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", g_sTriggerSceneAssets.ped[0], PLAYER_ZERO)
//				ENDIF
//			ENDIF
//			PRINTSTRING("TRIGGER SCENE - Trev's cutscene components set.") PRINTNL()
//		ENDIF
		
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0]) < 20
			IF taskLookAtGiven = FALSE
				TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				taskLookAtGiven = TRUE
			ENDIF
		ELSE
			IF taskLookAtGiven = TRUE
				TASK_CLEAR_LOOK_AT(g_sTriggerSceneAssets.ped[0])
				taskLookAtGiven = FALSE
			ENDIF
		ENDIF		
		
		IF MichaelWalkTaskGiven = FALSE
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					IF MichaelPlayingIdleLoop = FALSE
						IF GET_DISTANCE_BETWEEN_ENTITIES(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID()) > 8.1
							MichaelLeadInSyncScene = CREATE_SYNCHRONIZED_SCENE(<< 1393.589, 1142.884, 113.440 >>, << 0.000, 0.000, 133.560 >>)
							TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], MichaelLeadInSyncScene, "MISSMARTIN1@LEADINOUTMARTIN_1_INT", "Leadin_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
							SET_SYNCHRONIZED_SCENE_LOOPED(MichaelLeadInSyncScene, TRUE)	
							taskLookAtGiven = FALSE
							MichaelPlayingIdleLoop = TRUE
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF GET_DISTANCE_BETWEEN_ENTITIES(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID()) < 8
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1392.3, 1141.6, 114.4>>, <<3,3,3>>, FALSE, TRUE, TM_ON_FOOT)
							IF vPlayerCoords.z < 116
								//clear area of projectiles 
								IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
								ENDIF
								CLEAR_AREA_OF_PROJECTILES(<<1391.9, 1142, 113.8>>, 30)
								
								IF MichaelPlayingIdleLoop = TRUE
									IF IS_SYNCHRONIZED_SCENE_RUNNING(MichaelLeadInSyncScene)
										STOP_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.ped[0], NORMAL_BLEND_OUT, TRUE)
									ENDIF
								ENDIF
								MichaelActionSyncScene = CREATE_SYNCHRONIZED_SCENE(<< 1393.589, 1142.884, 113.440 >>, << 0.000, 0.000, 133.560 >>)
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], MichaelActionSyncScene, "MISSMARTIN1@LEADINOUTMARTIN_1_INT", "Leadin_action", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
								SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
								StartMissionTimer = GET_GAME_TIMER()
								taskLookAtGiven = FALSE
								playerLockedIn = TRUE
								MichaelWalkTaskGiven = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF MichaelWalkTaskGiven = TRUE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			WEAPON_TYPE Current_Ped_weapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Current_Ped_weapon)
			IF Current_Ped_weapon <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF	
			
			IF playerLockedIn = TRUE
			AND player1stTaskGiven = FALSE	
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<1390.3058, 1141.5925, 113.3342>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 279.0005)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, g_sTriggerSceneAssets.ped[0], 0)
					TASK_LOOK_AT_ENTITY(NULL, g_sTriggerSceneAssets.ped[0], -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 
				player1stTaskGiven = TRUE
			ENDIF
			
			//Stop the player if he's trying to get in Michaels way
			IF player1stTaskGiven = TRUE
			AND player2ndTaskGiven = FALSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(MichaelActionSyncScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(MichaelActionSyncScene) > 0.527	
						OPEN_SEQUENCE_TASK(seq)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<1393.3097, 1141.8763, 113.4431>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 270.4300)
							TASK_LOOK_AT_ENTITY(NULL, g_sTriggerSceneAssets.ped[0], -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
						player2ndTaskGiven = TRUE	
					ENDIF
				ENDIF
			ENDIF
			
//				IF IS_SYNCHRONIZED_SCENE_RUNNING(MichaelActionSyncScene)
//					IF GET_SYNCHRONIZED_SCENE_PHASE(MichaelActionSyncScene) < 0.549
//						IF playerControlTurnedOff = FALSE
//							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1392.729492,1143.186401,112.943115>>, <<1392.756836,1139.977783,116.193115>>, 5.250000)
//								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//								TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 0)
//								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
//								taskLookAtGiven = FALSE
//								playerControlTurnedOff = TRUE
//							ENDIF
//						ENDIF
//					ELSE
//						IF GET_SYNCHRONIZED_SCENE_PHASE(MichaelActionSyncScene) < 0.725
//							IF playerControlTurnedOff = TRUE
//								CLEAR_PED_TASKS(PLAYER_PED_ID())
//								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//								taskLookAtGiven = FALSE
//								playerControlTurnedOff = FALSE
//							ENDIF
//						ELSE
//							IF playerControlTurnedOff = FALSE
//								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1395.028931,1142.890747,113.428772>>, <<1395.090942,1140.713501,115.805084>>, 2.250000)
//									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//									TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 0)
//									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
//									taskLookAtGiven = FALSE
//									playerControlTurnedOff = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
					
			IF doneChatM1_LEADIN = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "SLM1AUD", "M1_INT_LI", CONV_PRIORITY_MEDIUM)
						//What is this?
						//It's a thing. You said you wanted work. This is work.
						//Uh-huh. And who's the guy?
						//You'll see.
						doneChatM1_LEADIN = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//	
//		//Set up Trev's clothes for opening cutscene
//		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//			//Trevor
//			SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(CHAR_TREVOR, "Trevor")
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_TORSO, 0, 4)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_LEG, 18, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_HAND, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_FEET, 16, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_SPECIAL2, 0, 0)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", PED_COMP_DECL, 0, 0)	
//			
//			//Michael
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
//			PRINTSTRING("TRIGGER SCENE - Trev's cutscene components set.") PRINTNL()
//		ENDIF		
//	
//	ENDIF

	//Grab Players car to be created after the cutscene.
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
			IF NOT IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
				g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTSTRING("TRIGGER SCENE - Players vehicle being assigned now") PRINTNL()
			ELSE
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_MARTIN1_AMBIENT_UPDATE()
ENDPROC
