//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Solomon 2 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
using "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_SOLOMON2_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_SOLOMON2_STREAM_OUT_DIST		160.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_SOLOMON2_CONTACT_DIST		75.0	// 

CONST_FLOAT		TS_SOLOMON2_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_SOLOMON2_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

FLOAT TS_SOLOMON2_fHintFov = 25.0
FLOAT TS_SOLOMON2_fHintFollow = 0.500
FLOAT TS_SOLOMON2_fHintPitchOrbit = 0.000
FLOAT TS_SOLOMON2_fHintSide = -0.005
FLOAT TS_SOLOMON2_fHintVert = 0.100


//#IF IS_DEBUG_BUILD
//	WIDGET_GROUP_ID Widg
//	BOOL bStopTriggerFireing = FALSE
//	/// PURPOSE:
//	///    Deletes the mission widget
//	PROC CLEANUP_OBJECT_WIDGETS()
//		IF DOES_WIDGET_GROUP_EXIST(Widg)
//			DELETE_WIDGET_GROUP(Widg)
//		ENDIF
//	ENDPROC 
//	
//	/// PURPOSE:
//	///    Sets up getaway vehicle widget
//	PROC SET_UP_RAG_WIDGETS()
//		CLEANUP_OBJECT_WIDGETS()
//	
//		Widg = START_WIDGET_GROUP("Sol2 Trigger")			
//			START_WIDGET_GROUP("Hint cam tweaker")
//				ADD_WIDGET_BOOL("Stop trigger firing ", bStopTriggerFireing)
//				
//				ADD_WIDGET_FLOAT_SLIDER("TS_SOLOMON2_fHintFov", TS_SOLOMON2_fHintFov, -99999, 99999, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("TS_SOLOMON2_fHintFollow", TS_SOLOMON2_fHintFollow, -99999, 99999, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("TS_SOLOMON2_fHintPitchOrbit", TS_SOLOMON2_fHintPitchOrbit, -99999, 99999, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("TS_SOLOMON2_fHintSide", TS_SOLOMON2_fHintSide, -99999, 99999, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("TS_SOLOMON2_fHintVert", TS_SOLOMON2_fHintVert, -99999, 99999, 0.01)
//			STOP_WIDGET_GROUP()
//			
//		STOP_WIDGET_GROUP()
//	ENDPROC
//
//#ENDIF

ENUM TS_SOLOMON2_LEAD_IN_STATE
	TS_SOLOMON2_LIS_WAIT,
	TS_SOLOMON2_LIS_CONTACT,
	TS_SOLOMON2_LIS_LOCKED,
	TS_SOLOMON2_LIS_TRIGGERED
ENDENUM

TS_SOLOMON2_LEAD_IN_STATE TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_WAIT

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_SOLOMON2_RESET()
	g_sTriggerSceneAssets.flag = FALSE
	TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_WAIT
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_SOLOMON2_REQUEST_ASSETS()
	REQUEST_MODEL(IG_ROCCOPELOSI)
	REQUEST_MODEL(U_M_Y_GUIDO_01)
	REQUEST_MODEL(IG_SOLOMON)
	REQUEST_MODEL(FUGITIVE)
	REQUEST_MODEL(BALLER2)
	REQUEST_ANIM_DICT("misssolomon_2leadinout")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_SOLOMON2_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_ROCCOPELOSI)
	SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_GUIDO_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_SOLOMON)
	SET_MODEL_AS_NO_LONGER_NEEDED(FUGITIVE)
	SET_MODEL_AS_NO_LONGER_NEEDED(BALLER2)
	REMOVE_ANIM_DICT("misssolomon_2leadinout")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_SOLOMON2_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(IG_ROCCOPELOSI)
	AND HAS_MODEL_LOADED(U_M_Y_GUIDO_01)
	AND HAS_MODEL_LOADED(IG_SOLOMON)
	AND HAS_MODEL_LOADED(FUGITIVE)
	AND HAS_MODEL_LOADED(BALLER2)
	AND HAS_ANIM_DICT_LOADED("misssolomon_2leadinout")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_SOLOMON2_CREATE()
	VECTOR vPedPos = <<-1032.97, -544.14, 35.30>>
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)

	//ROCCO
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, IG_ROCCOPELOSI, vPedPos,  -103.17)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 3, g_sTriggerSceneAssets.ped[0], "ROCCO")
	CPRINTLN(DEBUG_MISSION, "Created Rocco")
	
	//GOON
	vPedPos = <<-1031.72, -543.42, 35.26>>
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, U_M_Y_GUIDO_01, vPedPos, 110.43)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[1], TRUE)
	SET_PED_DEFAULT_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1])
	CPRINTLN(DEBUG_MISSION, "Created Goon")

	//SOLOMON
	vPedPos = <<-1031.94, -544.25, 35.29>>
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, IG_SOLOMON, vPedPos, 40.97)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[2], TRUE)	
	SET_PED_DEFAULT_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
	CPRINTLN(DEBUG_MISSION, "Created Solomon")

	//ROCCO CAR
	VECTOR vCarPos = <<-1041.64, -545.08, 34.81>>
	CLEAR_AREA(vCarPos, 6.0, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(FUGITIVE, vCarPos, 116.57)
	SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[0], 4)

	//PLAYER CAR
	vCarPos = <<-1046.1864, -554.2908, 33.4636>>
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(BALLER2, vCarPos, 101.5845)
	SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[1], 1)

	SET_VEHICLE_MODEL_IS_SUPPRESSED(FUGITIVE, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BALLER2, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(IG_ROCCOPELOSI, TRUE)	
	SET_PED_MODEL_IS_SUPPRESSED(U_M_Y_GUIDO_01, TRUE)	


	g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<-1034.41260, -545.05914, 34.640>>, << -0.000, 0.000, 22.100 >>)
	SET_SYNCHRONIZED_SCENE_LOOPED(g_sTriggerSceneAssets.id, TRUE)
	//ROCCO
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_01_sol_2_int_rocco", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
	//Milton
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_01_sol_2_int_milton", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
	//SOLOMON
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_01_sol_2_int_solomon", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)

	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-1065.40222, -494.25961, 60.47529>>, <<-986.20276, -561.64142, 17.43359>>)

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_SOLOMON_2,
												"SOL_2_INT_ALT1",
												CS_ALL,
												CS_NONE,
												CS_NONE)

//	#IF IS_DEBUG_BUILD
//		SET_UP_RAG_WIDGETS()
//	#ENDIF
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_SOLOMON2_RELEASE()
	INT i 
	FOR i=0 TO 3
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	ENDFOR

	FOR i=0 TO 3
		IF i = 2
			TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[i])
		ELSE
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
		ENDIF
	ENDFOR

	SET_VEHICLE_MODEL_IS_SUPPRESSED(FUGITIVE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BALLER2, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(IG_ROCCOPELOSI, FALSE)	
	SET_PED_MODEL_IS_SUPPRESSED(U_M_Y_GUIDO_01, FALSE)	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
//	#IF IS_DEBUG_BUILD
//		CLEANUP_OBJECT_WIDGETS()
//	#ENDIF
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_SOLOMON2_DELETE()
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[2])
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FUGITIVE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BALLER2, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(IG_ROCCOPELOSI, FALSE)	
	SET_PED_MODEL_IS_SUPPRESSED(U_M_Y_GUIDO_01, FALSE)	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
//	#IF IS_DEBUG_BUILD
//		CLEANUP_OBJECT_WIDGETS()
//	#ENDIF

ENDPROC

FUNC BOOL TS_SOLOMON2_IS_IN_FOCUS_CAM_AREAS()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1032.11316, -543.71759, 32.31895>>, <<-1047.35828, -515.18719, 39.03860>>, 4.09)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1032.64539, -542.87946, 32.33058>>, <<-1008.59949, -526.54071, 39.62954>>, 8.60)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1060.34192, -555.06348, 32.43643>>, <<-1032.63086, -543.39423, 39.31255>>, 21.04)
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_SOLOMON2_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
	AND NOT (IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
		//Render trigger zone debug.
		VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_SOLOMON_2)
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, 15.50)
			DRAW_DEBUG_POLY_ANGLED_AREA(<<-1009.133606,-547.054016,51.833260>>,<<-1042.9508, -559.6316, 19.5009>>,6.5)// B* - 2004298
			TS_SOLOMON2_IS_IN_FOCUS_CAM_AREAS()
		#ENDIF

		IF TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_TRIGGERED
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
//			#IF IS_DEBUG_BUILD
//				CLEANUP_OBJECT_WIDGETS()
//			#ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				RETURN TRUE
			ENDIF
		// B* - 2004298
		ELIF fDistanceSquaredFromTrigger < 15.50*15.50 AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1009.133606,-547.054016,51.833260>>,<<-1042.9508, -559.6316, 19.5009>>,6.5)
			CPRINTLN(DEBUG_MISSION, "fDistanceSquaredFromTrigger < 15.50*15.50")
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_SCENE_PEDS_ALIVE()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[2])
	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
	AND NOT IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.veh[0])
	AND NOT IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.veh[1])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_SOLOMON2_HAS_BEEN_DISRUPTED()
	IF NOT ARE_SCENE_PEDS_ALIVE()
		STOP_GAMEPLAY_HINT(TRUE)
		RETURN TRUE
	ELIF IS_BULLET_IN_BOX(<<-1027.13489, -550.49103, 30.06013>>, <<-1035.07239, -534.84943, 40.76440>>, FALSE)
	OR IS_PROJECTILE_IN_AREA(<<-1025.52576, -545.96991, 30.06013>>, <<-1042.38940, -538.64526, 40.76440>>)
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, <<-1032.86133, -543.89264, 34.29076>>, 26)
		STOP_GAMEPLAY_HINT(TRUE)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_SOLOMON2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


PROC TS_SOLOMON2_WAIT_CONTACT()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_SOLOMON_2)
	FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[1])
		SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[0], 4)
		SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[1], 1)
	ENDIF

	IF fDistanceSquaredFromTrigger < (TS_SOLOMON2_CONTACT_DIST*TS_SOLOMON2_CONTACT_DIST)
		IF ARE_SCENE_PEDS_ALIVE()
			SET_HD_AREA(vTriggerPosition, 50)
			g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<-1034.41260, -545.05914, 34.640>>, << -0.000, 0.000, 22.100 >>)
			SET_SYNCHRONIZED_SCENE_LOOPED(g_sTriggerSceneAssets.id, TRUE)
			//ROCCO
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_02_sol_2_int_rocco", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			//Milton
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_02_sol_2_int_milton", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			//SOLOMON
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_02_sol_2_int_solomon", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
			CPRINTLN(DEBUG_MISSION, "TS_SOLOMON2_eLeadinState = LIS_CONTACT")
			TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_CONTACT
		ENDIF
	ENDIF
ENDPROC

PROC TS_SOLOMON2_CONTACT()
	IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1027.310547,-550.637573,33.121124>>, <<-1044.72314, -514.87878,120.788727>>, 20.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1004.80530, -530.97308, 32.27923>>, <<-1060.61279, -554.68372, 120.90538>>, 21.37))
	AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
	AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())

		IF ARE_SCENE_PEDS_ALIVE()
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1021.02008, -532.54108, 34.98043>>, <<-1024.51001, -525.20331, 120.90538>>, 16.13)
				IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "SOL2AUD", "SOL2_INTA_LI", CONV_PRIORITY_HIGH)
					g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<-1034.41260, -545.05914, 34.640>>, << -0.000, 0.000, 22.100 >>)
					SET_SYNCHRONIZED_SCENE_LOOPED(g_sTriggerSceneAssets.id, TRUE)
					//ROCCO
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_03_sol_2_int_rocco", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					//Milton
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_03_sol_2_int_milton", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					//SOLOMON
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.id, "misssolomon_2leadinout", "idle_03_sol_2_int_solomon", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					CPRINTLN(DEBUG_MISSION, "TS_SOLOMON2_eLeadinState  = LIS_LOCKED")
					TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_LOCKED
				ENDIF
			ELSE
				TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_TRIGGERED
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TS_SOLOMON2_LOCKED()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			IF ARE_SCENE_PEDS_ALIVE()
				IF TS_SOLOMON2_IS_IN_FOCUS_CAM_AREAS()
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[2], <<0,0,0>>, TRUE, 60000)
					SET_GAMEPLAY_HINT_FOV(TS_SOLOMON2_fHintFov)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(TS_SOLOMON2_fHintFollow)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(TS_SOLOMON2_fHintPitchOrbit)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(TS_SOLOMON2_fHintSide)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(TS_SOLOMON2_fHintVert)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				SET_GAMEPLAY_HINT_FOV(TS_SOLOMON2_fHintFov)
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(TS_SOLOMON2_fHintFollow)
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(TS_SOLOMON2_fHintPitchOrbit)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(TS_SOLOMON2_fHintSide)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(TS_SOLOMON2_fHintVert)
			#ENDIF
			IF TS_SOLOMON2_IS_IN_FOCUS_CAM_AREAS()
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
			ELSE
				STOP_GAMEPLAY_HINT()
			ENDIF
		ENDIF
	ENDIF

//	#IF IS_DEBUG_BUILD
//		IF bStopTriggerFireing
//			g_bPlayerLockedInToTrigger = FALSE	//This is fricking stooopid		
//			EXIT
//		ELSE
//			g_bPlayerLockedInToTrigger = TRUE //works though and debug only
//		ENDIF
//	#ENDIF

	IF  NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1027.310547,-550.637573,33.121124>>, <<-1044.777954,-513.628662,60.795391>>, 40.57)
		CPRINTLN(DEBUG_MISSION, "TS_SOLOMON2_eLeadinState = LIS_TRIGGERED")
		STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(FALSE)
		TS_SOLOMON2_eLeadinState = TS_SOLOMON2_LIS_TRIGGERED
	ENDIF
ENDPROC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_SOLOMON2_UPDATE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			SWITCH TS_SOLOMON2_eLeadinState
				CASE TS_SOLOMON2_LIS_WAIT
					TS_SOLOMON2_WAIT_CONTACT()
				BREAK
				
				CASE TS_SOLOMON2_LIS_CONTACT
					TS_SOLOMON2_CONTACT()
				BREAK
				
				CASE TS_SOLOMON2_LIS_LOCKED
					TS_SOLOMON2_LOCKED()
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_SOLOMON2_AMBIENT_UPDATE()
ENDPROC
