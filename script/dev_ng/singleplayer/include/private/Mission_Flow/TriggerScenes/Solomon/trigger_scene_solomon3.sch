//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Solomon 3 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "building_control_public.sch"


CONST_FLOAT 	TS_SOLOMON3_STREAM_IN_DIST		200.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_SOLOMON3_STREAM_OUT_DIST		220.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_SOLOMON3_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_SOLOMON3_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_SOLOMON3_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_SOLOMON3_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_SOLOMON3_REQUEST_ASSETS()
	REQUEST_MODEL(RAPIDGT)
	REQUEST_MODEL(CARBONIZZARE)
	REQUEST_MODEL(SURANO)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_SOLOMON3_RELEASE_ASSETS()

	SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
	SET_MODEL_AS_NO_LONGER_NEEDED(CARBONIZZARE)
	SET_MODEL_AS_NO_LONGER_NEEDED(SURANO)
	
	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()	
	
	SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, TRUE)

ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_SOLOMON3_HAVE_ASSETS_LOADED()

	IF HAS_MODEL_LOADED(RAPIDGT)
	AND HAS_MODEL_LOADED(CARBONIZZARE)
	AND HAS_MODEL_LOADED(SURANO)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_SOLOMON3_CREATE()
	
	CLEAR_AREA(<<-1026.8, -492.1, 36>>, 18, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1042.5, -500, 41>>, <<-1019.5, -475.0, 34>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-1042.5, -500, 41>>, <<-1019.5, -475.0, 34>>)
	
	//Enable the interior for Solomon's office now.
	SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, FALSE)
	
	//Start a new load scene now for the interior used in the opening cutscene
	NEW_LOAD_SCENE_START_SPHERE(<<-1006, -478, 49>>, 15)
	
	//Create players cars
	CLEAR_AREA(<<-1024.1000, -485.3321, 35.9816>>, 5, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(RAPIDGT, <<-1024.1000, -485.3321, 35.9816>>, 209.7233)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[0], 112, 112)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0], 3000)
	SET_VEHICLE_STRONG(g_sTriggerSceneAssets.veh[0], TRUE)
	SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[0], FALSE, TRUE, FALSE, FALSE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(RAPIDGT, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[0], TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[0], TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(RAPIDGT)
	
	
	CLEAR_AREA(<<-1037.3977, -491.6539, 35.5545>>, 5, TRUE)
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(SURANO, <<-1037.3977, -491.6539, 35.5545>>, 208.8890)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[1], 0, 0)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[1], 3000)
	SET_VEHICLE_STRONG(g_sTriggerSceneAssets.veh[1], TRUE)
	SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[1], FALSE, TRUE, TRUE, FALSE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(SURANO, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[1], TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[1], TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(SURANO)
	
	
	CLEAR_AREA(<<-1033.9381, -489.7475, 35.8323>>, 5, TRUE)
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(CARBONIZZARE, <<-1033.9381, -489.7475, 35.8323>>, 207.1758)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[2], 89, 89)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[2], 3000)
	SET_VEHICLE_STRONG(g_sTriggerSceneAssets.veh[2], TRUE)
	SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[2], FALSE, TRUE, TRUE, FALSE, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CARBONIZZARE, TRUE)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[2], TRUE)
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[2], TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(CARBONIZZARE)

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_SOLOMON_3, "sol_3_int", CS_ALL, CS_NONE, CS_NONE)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_SOLOMON3_RELEASE()

	//Release scene vehicles.
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[3])

	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()
	
	SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, TRUE)

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_SOLOMON3_DELETE()

	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[3])

	SET_INTERIOR_DISABLED(INTERIOR_V_58_SOL_OFFICE, TRUE)

	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()	

	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_SOLOMON3_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_SOLOMON)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_SOLOMON3_TRIGGER_DIST)
		#ENDIF
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition) < 2.5
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_SOLOMON3_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_SOLOMON3_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_SOLOMON3_UPDATE()

	//Get the players start car if he arrives in one.
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		g_sTriggerSceneAssets.veh[3] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_SOLOMON3_AMBIENT_UPDATE()
ENDPROC
