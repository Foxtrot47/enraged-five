//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Solomon 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_SOLOMON1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_SOLOMON1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_SOLOMON1_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_SOLOMON1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_SOLOMON1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

int 			iIntroScene = 0 
CAMERA_INDEX	cameraindex
int 			icamdelay

proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime,true,true)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
	endif
	STOP_FIRE_IN_RANGE(varea,200)
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEH_RADIO_STATION(GET_VEHICLE_PED_IS_IN(player_ped_id()),"OFF")
	endif
	hang_up_and_put_away_phone()
endproc
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_SOLOMON1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_SOLOMON1_REQUEST_ASSETS()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_SOLOMON1_RELEASE_ASSETS()

	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()	//BUG FIX 1402789 

ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_SOLOMON1_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_SOLOMON1_CREATE()

	//Start a new load scene now for the interior used in the opening cutscene
	NEW_LOAD_SCENE_START_SPHERE(<<-1006, -478, 49>>, 15) //BUG FIX 1402789 

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_SOLOMON1_RELEASE()

	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()	 //BUG FIX 1402789 

ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_SOLOMON1_DELETE()

	//Stop the new load scene now
	NEW_LOAD_SCENE_STOP()	//BUG FIX 1402789 

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_SOLOMON1_HAS_BEEN_TRIGGERED()
	if iIntroScene = 3
		return true
	endif
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_SOLOMON1_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_SOLOMON1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_SOLOMON1_UPDATE()
	switch iIntroScene
		case 0
			VECTOR vTriggerPosition
			vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_SOLOMON)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//Render trigger zone debug.
				#IF IS_DEBUG_BUILD
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_SOLOMON1_TRIGGER_DIST)
				#ENDIF
			
				FLOAT fDistanceSquaredFromTrigger 
				fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
				IF fDistanceSquaredFromTrigger < (TS_SOLOMON1_TRIGGER_DIST*TS_SOLOMON1_TRIGGER_DIST)					
					iIntroScene++
				ENDIF
			ENDIF
		break
		case 1
			SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
			SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
			TASK_GO_STRAIGHT_TO_COORD(player_ped_id(),<<-1011.29340, -480.40060, 38.97574>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-1017.0,-483.5,40.7>>,<<49.6,-0,-78.9>>,50)
			SHAKE_CAM(cameraindex,"hand_shake",0.5)
			SET_CAM_ACTIVE(cameraIndex,true)			
			prep_start_cutscene(false,get_entity_coords(player_ped_id()),true,true,true,3500)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			icamdelay = get_game_timer()
			iIntroScene++
		break
		case 2 
			SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
			if get_game_timer() - icamdelay >= 3500
				iIntroScene++
			endif
		break
		case 3
			SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
		break
	endswitch
	//Bug:2034777 stop weird transition
	if iIntroScene > 0
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
	endif
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_SOLOMON1_AMBIENT_UPDATE()
ENDPROC
