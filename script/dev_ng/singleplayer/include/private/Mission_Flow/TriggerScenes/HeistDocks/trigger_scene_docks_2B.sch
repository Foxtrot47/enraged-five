//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Docks Heist 2B - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								ped[0]	- 	Wade								│
//│								ped[1]	- 	Floyd								│
//│								ped[2]	- 	Trevor								│
//│								ped[3]	- 	Michael								│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "properties_public.sch"


CONST_FLOAT 	TS_DOCKS2B_STREAM_IN_DIST		12.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_DOCKS2B_STREAM_OUT_DIST		18.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_DOCKS2B_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_DOCKS2B_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE		//Distance friends will bail from player group.
CONST_INT		TS_DOCKS2B_FRIEND_ACCEPT_BITS		BIT_NOBODY	//Friends who can trigger the mission with the player.

INT				stand12BScene
INT				massage2BScene
INT				Michael2BScene

INT				iDH2BLeadStage

BOOL bDialogue
BOOL bTriggerSS

VECTOR			vDH2BScenePos = <<-1159.9227, -1520.5026, 9.6327>>
VECTOR			vDH2BSceneRot = <<0,0,40>>

VECTOR vDS2BHintCamCoord = <<-1158.2783, -1521.0677, 10.8094>>
FLOAT fDS2BHintCamFollow = 0.65
FLOAT fDS2BHintCamOffset = -0.16
FLOAT fDS2BHintCamFove =   35.00

//dialogue
structPedsForConversation 	convo_struct_dh2b

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_DOCKS2B_RESET()
	iDH2BLeadStage 			= 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_DOCKS2B_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_WADE))
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_FLOYD))
	REQUEST_MODEL(PLAYER_ZERO)
	REQUEST_MODEL(PLAYER_ONE)
	REQUEST_MODEL(PLAYER_TWO)
	REQUEST_MODEL(PROP_TUMBLER_01_EMPTY)
	REQUEST_ANIM_DICT("missheistdocks2bleadinoutlsdh_2b_int")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_DOCKS2B_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ONE)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TUMBLER_01_EMPTY)
	REMOVE_ANIM_DICT("missheistdocks2bleadinoutlsdh_2b_int")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_DOCKS2B_HAVE_ASSETS_LOADED()
	IF  HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_WADE))
	AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	AND HAS_MODEL_LOADED(PLAYER_ZERO)
	AND HAS_MODEL_LOADED(PLAYER_ONE)
	AND HAS_MODEL_LOADED(PLAYER_TWO)
	AND HAS_ANIM_DICT_LOADED("missheistdocks2bleadinoutlsdh_2b_int")
	AND HAS_MODEL_LOADED(PROP_TUMBLER_01_EMPTY)
		RETURN TRUE
	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_DOCKS2B_CREATE()
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_TREVOR
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_WADE, <<-1149.4646, -1519.3779, 10.6327>>)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
		SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[0],40)
		
		CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_FLOYD, <<-1148.4424, -1518.7101, 9.6327>>)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
		SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[1],40)
	ENDIF
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_TREVOR_VB, TRUE)
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, TRUE)
	SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB,DOORSTATE_UNLOCKED)
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		//create mike
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3],CHAR_MICHAEL,<<-1159.9720, -1524.6874, 6.7144>>, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[3],FALSE)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
		REQUEST_SCRIPT_AUDIO_BANK("PORT_OF_LS_2B_HEIST_TOILET_FLUSH")
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[2],CHAR_TREVOR,vDH2BScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[2],40)
		
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_TUMBLER_01_EMPTY,vDH2BScenePos)
			ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
		ENDIF
		
		massage2BScene = CREATE_SYNCHRONIZED_SCENE(vDH2BScenePos,vDH2BSceneRot)
		SET_SYNCHRONIZED_SCENE_LOOPED(massage2BScene,true)
		SET_ENTITY_NO_COLLISION_ENTITY(g_sTriggerSceneAssets.ped[1],g_sTriggerSceneAssets.ped[2],FALSE)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","standing_loop_peda",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS ,RBF_PLAYER_IMPACT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","leg_massage_floyd",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","leg_massage_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT| SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)

	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[2],CHAR_TREVOR,vDH2BScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[2],40)
		
		//create mike
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3],CHAR_MICHAEL,vDH2BScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
		SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[3],FALSE)
		
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_TUMBLER_01_EMPTY,vDH2BScenePos)
			ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
		ENDIF
		
		massage2BScene = CREATE_SYNCHRONIZED_SCENE(vDH2BScenePos,vDH2BSceneRot)
		SET_SYNCHRONIZED_SCENE_LOOPED(massage2BScene,true)
		SET_ENTITY_NO_COLLISION_ENTITY(g_sTriggerSceneAssets.ped[1],g_sTriggerSceneAssets.ped[2],FALSE)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","standing_loop_peda",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS ,RBF_PLAYER_IMPACT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","leg_massage_b_floyd",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],massage2BScene,"missheistdocks2bleadinoutlsdh_2b_int","leg_massage_b_trevor",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT| SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)

		Michael2BScene = CREATE_SYNCHRONIZED_SCENE(vDH2BScenePos,vDH2BSceneRot)
		SET_SYNCHRONIZED_SCENE_LOOPED(Michael2BScene,true)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3],Michael2BScene,"missheistdocks2bleadinoutlsdh_2b_int","standing_loop_michael",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT| SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
	ENDIF
	
	SET_SRL_FORCE_PRESTREAM(  SRL_PRESTREAM_FORCE_COMPLETELY_OFF )
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_DOCKS_2B,
												"LSDH_2B_INT",
												CS_3|CS_4|CS_5|CS_6|CS_7,
												CS_5|CS_6|CS_7,
												CS_ALL)
	
	//Cleans nearby for sale sign models out of memory.
	SUPPRESS_PROPERTY_FOR_SALE_SIGNS(TRUE)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_DOCKS2B_RELEASE()
	INT i
	REPEAT 4 i
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
	ENDREPEAT
		
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	SUPPRESS_PROPERTY_FOR_SALE_SIGNS(FALSE)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_DOCKS2B_DELETE()
	int i
	REPEAT 4 i
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDREPEAT
	RELEASE_SCRIPT_AUDIO_BANK()
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	SUPPRESS_PROPERTY_FOR_SALE_SIGNS(FALSE)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_DOCKS2B_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_DOCKS)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iDH2BLeadStage = 2

			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)		
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_DOCKS2B_HAS_BEEN_DISRUPTED()
	int i
	for i = 0 to 3
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[i], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	endfor
//	if bleadinStart
//		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
//			return true
//		endif
//	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_DOCKS2B_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_DOCKS2B_UPDATE()
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("iDH2BLeadStage: ")PRINTINT(iDH2BLeadStage)PRINTNL()
	#ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1148.289917,-1524.254883,9.363565>>, <<-1151.240112,-1520.051636,12.882723>>, 2.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1147.680786,-1522.989990,6.383060>>, <<-1144.043091,-1520.518433,9.938194>>, 1.500000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
	OR GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1160.324341,-1522.264526,8.382723>>, <<-1152.145630,-1517.004639,12.882723>>, 5.000000)
			SET_GAMEPLAY_COORD_HINT(vDS2BHintCamCoord,-1,2000)
			SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fDS2BHintCamFollow)
			SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fDS2BHintCamOffset)
			SET_GAMEPLAY_HINT_FOV(fDS2BHintCamFove)
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1150.350220,-1521.425659,9.132723>>, <<-1151.579346,-1519.680664,12.632723>>, 2.250000)
				STOP_GAMEPLAY_HINT()
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1150.052002,-1523.333008,12.421901>>, <<-1142.907227,-1518.365845,6.557154>>, 3.750000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1160.780518,-1522.466431,8.705703>>, <<-1150.433228,-1515.172241,12.632723>>, 10.000000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
	ENDIF
	
	SWITCH iDH2BLeadStage
		CASE 0
			bDialogue = FALSE
			bTriggerSS = FALSE
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				IF IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(), <<-1152.418213,-1516.766357,9.132729>>, <<-1155.040039,-1518.588257,11.882729>>, 1.750000)
				OR IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(), <<-1149.493652,-1518.192383,9.512158>>, <<-1151.645752,-1515.085571,11.882729>>, 1.750000)
				OR IS_ENTITY_IN_ANGLED_AREA(player_ped_ID(), <<-1149.905884,-1520.175537,9.391918>>, <<-1151.593628,-1521.361572,12.452510>>, 1.500000) //Just in the door
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,0,player_ped_id(),"TREVOR")
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_VEHICLE_POPULATION_BUDGET(0)
					SET_PED_POPULATION_BUDGET(0)
					iDH2BLeadStage++
				ENDIF
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1154.286743,-1520.627686,9.632729>>, <<-1157.175781,-1516.414185,12.132729>>, 13.000000)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
						ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,0,g_sTriggerSceneAssets.ped[2],"TREVOR")
						ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,1,player_ped_id(),"MICHAEL")
						SET_VEHICLE_POPULATION_BUDGET(0)
						SET_PED_POPULATION_BUDGET(0)
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1156.76, -1519.92, 9.63>>,PEDMOVE_WALK)
						iDH2BLeadStage++
					ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
						ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,0,g_sTriggerSceneAssets.ped[2],"TREVOR")
						ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,1,g_sTriggerSceneAssets.ped[3],"MICHAEL")
						ADD_PED_FOR_DIALOGUE(convo_struct_dh2b,2,player_ped_id(),"FRANKLIN")
						SET_VEHICLE_POPULATION_BUDGET(0)
						SET_PED_POPULATION_BUDGET(0)
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1156.76, -1519.92, 9.63>>,PEDMOVE_WALK)
						iDH2BLeadStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
		
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
			
			IF bDialogue = FALSE
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
					IF REQUEST_SCRIPT_AUDIO_BANK("PORT_OF_LS_2B_HEIST_TOILET_FLUSH")
						IF CREATE_CONVERSATION(convo_struct_dh2b,"D2BAUD","DS2B_INT_LI1",CONV_PRIORITY_MEDIUM)
							bDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF

				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
					IF CREATE_CONVERSATION(convo_struct_dh2b,"D2BAUD","DS2B_INT_LI2",CONV_PRIORITY_MEDIUM)
						bDialogue = TRUE
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
					IF CREATE_CONVERSATION(convo_struct_dh2b,"D2BAUD","DS2B_INT_LI3",CONV_PRIORITY_MEDIUM)
						bDialogue = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				IF bTriggerSS = FALSE
					IF IS_SYNCHRONIZED_SCENE_RUNNING( massage2BScene )
						stand12BScene = CREATE_SYNCHRONIZED_SCENE(vDH2BScenePos,vDH2BSceneRot)
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
							IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],stand12BScene,"missheistdocks2bleadinoutlsdh_2b_int","action_floyd",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
							IF not IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],stand12BScene,"missheistdocks2bleadinoutlsdh_2b_int","action_trevor",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
							ENDIF
						ENDIF
						bTriggerSS = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bDialogue
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
							SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[3],TRUE)
						ENDIF
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
						PLAY_SOUND_FROM_COORD(-1,"Flush",<<-1148.9026, -1519.2570, 9.6327>>,"DOCKS_HEIST_FINALE_2B_SOUNDS")
					ENDIF
					iDH2BLeadStage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
		
		BREAK
	endswitch
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_DOCKS2B_AMBIENT_UPDATE()
ENDPROC
