//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Docks Heist 2A - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								ped[0]	- 	Wade								│
//│								ped[1]	- 	Floyd								│
//│								ped[2]	- 	Trevor								│
//│								ped[3]	- 	Michael								│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "tv_control_public.sch"


CONST_FLOAT 	TS_DOCKS2A_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_DOCKS2A_STREAM_OUT_DIST		65.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_DOCKS2A_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_DOCKS2A_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE		//Distance friends will bail from player group.
CONST_INT		TS_DOCKS2A_FRIEND_ACCEPT_BITS		BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR		//Friends who can trigger the mission with the player.

CONST_INT		TS_DOCKS2A_GAMEPLAY_HINT_SET	0

INT				couchScene
INT				stand1Scene
INT				stand2Scene
INT				cleaningScene
INT				massageScene
INT				MichaelScene
INT 			iDocks2AStateBitset

FLOAT			fTriggerTime

int				iDH2LeadStage

BOOL			bFranklinsFirstLine
INT				iFranklinSecondLineTimer

structTimer		tmrTriggerComplete

VECTOR			vDH2AScenePos = <<-1159.9227, -1520.5026, 9.6327>>
VECTOR			vDH2ASceneRot = <<0,0,40>>

//dialogue
structPedsForConversation 	convo_struct_dh2a

/// PURPOSE:
///    Kills the player if somehow he gets in an explosion
PROC TS_DOCKS2A_KILL_INVINCIBLE_PLAYER_IF_IN_EXPLOSION() 
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 5.0) 
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, FALSE, FALSE, TRUE, TRUE) 
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE) 
		APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), 1000, TRUE) 
		ENDIF 
	ENDIF 
ENDPROC 


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_DOCKS2A_RESET()
	iDH2LeadStage 			= 0
	iDocks2AStateBitset			= 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_DOCKS2A_REQUEST_ASSETS()
	request_model(GET_NPC_PED_MODEL(CHAR_WADE))
	request_model(GET_NPC_PED_MODEL(CHAR_FLOYD))
	request_model(player_zero)
	request_model(player_one)
	request_model(player_two)
	REQUEST_ANIM_DICT("missheistdocks2aleadinoutlsdh_2a_int")
	PIN_INTERIOR_IN_MEMORY(GET_INTERIOR_AT_COORDS(<< -1158.3411, -1520.8929, 9.6345 >>))
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_DOCKS2A_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ONE)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
	REMOVE_ANIM_DICT("missheistdocks2aleadinoutlsdh_2a_int")
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	UNPIN_INTERIOR(GET_INTERIOR_AT_COORDS(<< -1158.3411, -1520.8929, 9.6345 >>))
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_DOCKS2A_HAVE_ASSETS_LOADED()
	if  HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_WADE))
	and HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	and HAS_MODEL_LOADED(PLAYER_ZERO)
	and HAS_MODEL_LOADED(PLAYER_ONE)
	and HAS_MODEL_LOADED(PLAYER_TWO)
	and HAS_ANIM_DICT_LOADED("missheistdocks2aleadinoutlsdh_2a_int")
	AND IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS(<< -1158.3411, -1520.8929, 9.6345 >>))
		RETURN TRUE
	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_DOCKS2A_CREATE()
	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-1164.57434, -1533.09717, 5.41414>>,<<-1140.26831, -1504.86548, 13.82142>>)
	CLEAR_AREA_OF_PEDS( <<-1156.24817, -1520.39722, 9.63273>>, 6.0 )
	CLEAR_AREA_OF_PEDS( <<-1148.87866, -1515.72180, 9.64032>>, 6.0 )
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_WADE, vDH2AScenePos)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_FLOYD, vDH2AScenePos)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_JBIB, 0, 0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_TREVOR_VB, true)
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, true)
	SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB,DOORSTATE_UNLOCKED)
	
	
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		couchScene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)	
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],couchScene,"missheistdocks2aleadinoutlsdh_2a_int","Sitting_loop_Wade",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],couchScene,"missheistdocks2aleadinoutlsdh_2a_int","Sitting_loop_Floyd",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(couchScene,true)
		
		//create mike
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3],CHAR_MICHAEL,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_VISIBLE( g_sTriggerSceneAssets.ped[3], FALSE )
		FREEZE_ENTITY_POSITION( g_sTriggerSceneAssets.ped[3], TRUE )
		CPRINTLN( DEBUG_MISSION, "CREATED MICHAEL" )
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
		
		//create franklin
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[4],CHAR_FRANKLIN,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_VISIBLE( g_sTriggerSceneAssets.ped[4], FALSE )
		FREEZE_ENTITY_POSITION( g_sTriggerSceneAssets.ped[4], TRUE )
		CPRINTLN( DEBUG_MISSION, "CREATED FRANKLIN" )
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ONE)
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[2],CHAR_TREVOR,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		
		//create franklin
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[4],CHAR_FRANKLIN,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_VISIBLE( g_sTriggerSceneAssets.ped[4], FALSE )
		FREEZE_ENTITY_POSITION( g_sTriggerSceneAssets.ped[4], TRUE )
		CPRINTLN( DEBUG_MISSION, "CREATED FRANKLIN" )
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ONE)
		
		cleaningScene = CREATE_SYNCHRONIZED_SCENE((vDH2AScenePos+<<0.2,-0.2,0>>),vDH2ASceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],cleaningScene,"missheistdocks2aleadinoutlsdh_2a_int","cleaning_wade",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(cleaningScene,true)
		
		massageScene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],massageScene,"missheistdocks2aleadinoutlsdh_2a_int","massage_loop_floyd",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],massageScene,"missheistdocks2aleadinoutlsdh_2a_int","massage_loop_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(massageScene,true)
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[2],CHAR_TREVOR,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		
		//create mike
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3],CHAR_MICHAEL,vDH2AScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
		
		cleaningScene = CREATE_SYNCHRONIZED_SCENE((vDH2AScenePos+<<0.2,-0.2,0>>),vDH2ASceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],cleaningScene,"missheistdocks2aleadinoutlsdh_2a_int","cleaning_wade",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(cleaningScene,true)
		
		massageScene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],massageScene,"missheistdocks2aleadinoutlsdh_2a_int","massage_loop_2_floyd",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2],massageScene,"missheistdocks2aleadinoutlsdh_2a_int","massage_loop_2_trevor",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(massageScene,true)
		
		MichaelScene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3],MichaelScene,"missheistdocks2aleadinoutlsdh_2a_int","sitting_loop_michael",INSTANT_BLEND_IN,NORMAL_BLEND_OUT)
		SET_SYNCHRONIZED_SCENE_LOOPED(MichaelScene,true)
	ENDIF
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_DOCKS_2A,
												"LSDH_2A_INT",
												CS_3|CS_4|CS_5|CS_6,
												CS_5|CS_6,
												CS_ALL)
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_2A, "Wade", g_sTriggerSceneAssets.ped[0])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_2A, "Floyd", g_sTriggerSceneAssets.ped[1])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_2A, "Trevor", g_sTriggerSceneAssets.ped[2])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_2A, "Michael", g_sTriggerSceneAssets.ped[3])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_2A, "Franklin", g_sTriggerSceneAssets.ped[4])
	ENDIF
	
	CLEAR_BIT(iDocks2AStateBitset, TS_DOCKS2A_GAMEPLAY_HINT_SET)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_DOCKS2A_RELEASE()
	INT i
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[3])
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[4])
		REPEAT 3 i
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
		ENDREPEAT
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[4])
		REPEAT 3 i
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
		ENDREPEAT
	ELSE
		REPEAT 4 i
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
		ENDREPEAT
	ENDIF
		
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_DOCKS2A_DELETE()
	int i
	REPEAT 5 i
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDREPEAT
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_DOCKS2A_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_DOCKS)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iDH2LeadStage = 4
			KILL_ANY_CONVERSATION()
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)		
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_DOCKS2A_HAS_BEEN_DISRUPTED()
	int i
	for i = 0 to 3
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[i], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
				IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[i]), 5.0)
					SET_ENTITY_HEALTH(g_sTriggerSceneAssets.ped[i], 0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	endfor
//	if bleadinStart
//		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
//			return true
//		endif
//	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_DOCKS2A_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_DOCKS2A_UPDATE()
	IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1148.403442,-1524.313599,9.533060>>, <<-1153.922119,-1516.733643,11.632723>>, 4.000000 )
		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(true)
	ENDIF
	
	switch iDH2LeadStage
		case 0
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1154.044434,-1517.906860,9.432728>>, <<-1150.391479,-1515.200562,11.632728>>, 9.000000)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2a,0,player_ped_id(),"TREVOR")
					iDH2LeadStage++
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1154.044434,-1517.906860,9.432728>>, <<-1150.391479,-1515.200562,11.632728>>, 5.000000)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2a,0,g_sTriggerSceneAssets.ped[2],"TREVOR")
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2a,1,player_ped_id(),"MICHAEL")
					iDH2LeadStage++
				ENDIF
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1150.813232,-1520.601685,9.432723>>, <<-1153.760376,-1516.677124,11.632723>>, 4.000000)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2a,0,g_sTriggerSceneAssets.ped[2],"TREVOR")
					ADD_PED_FOR_DIALOGUE(convo_struct_dh2a,1,player_ped_id(),"FRANKLIN")
					iDH2LeadStage++
				ENDIF
			endif
		break
		case 1
			IF NOT IS_PHONE_ONSCREEN()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(convo_struct_dh2a,"D2AAUD","DH2A_INT_LI1",CONV_PRIORITY_MEDIUM)
						CPRINTLN( DEBUG_MISSION, "CREATED TREVOR'S CONVERSATION" )
						iDH2LeadStage++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION( convo_struct_dh2a,"D2AAUD","DH2A_INT_LI2","DH2A_INT_LI2_3",CONV_PRIORITY_MEDIUM )
						iDH2LeadStage++
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
					IF NOT bFranklinsFirstLine
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION( convo_struct_dh2a,"D2AAUD","DH2A_INT_LI3","DH2A_INT_LI3_3",CONV_PRIORITY_MEDIUM )
							iFranklinSecondLineTimer = GET_GAME_TIMER()+5000
							bFranklinsFirstLine = TRUE
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1154.044434,-1517.906860,9.432728>>, <<-1150.391479,-1515.200562,11.632728>>, 5.00000)
							OR GET_GAME_TIMER() > iFranklinSecondLineTimer
								IF PLAY_SINGLE_LINE_FROM_CONVERSATION( convo_struct_dh2a,"D2AAUD","DH2A_INT_LI3","DH2A_INT_LI3_5",CONV_PRIORITY_MEDIUM )
									iDH2LeadStage++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		break
		case 2
			SEQUENCE_INDEX iSeq
			OPEN_SEQUENCE_TASK(iSeq)
				TASK_FOLLOW_NAV_MESH_TO_COORD( NULL, (<<-1153.1094, -1517.9308, 9.6327>>), PEDMOVE_WALK, -1, DEFAULT, ENAV_NO_STOPPING)
				TASK_FOLLOW_NAV_MESH_TO_COORD( NULL, (<<-1156.7263, -1520.1699, 9.6327>>), PEDMOVE_WALK, -1 )
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
			SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
			
			START_TIMER_NOW_SAFE(tmrTriggerComplete)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				fTriggerTime = 3.65
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				fTriggerTime = 2.3
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				fTriggerTime = 1.2
			ENDIF
			
			iDH2LeadStage++
		break
		case 3
			UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(true)
			TS_DOCKS2A_KILL_INVINCIBLE_PLAYER_IF_IN_EXPLOSION()
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF IS_SYNCHRONIZED_SCENE_RUNNING( couchScene ) AND NOT IS_SYNCHRONIZED_SCENE_RUNNING( stand1Scene )
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1154.905640,-1517.455811,9.632723>>, <<-1149.402100,-1518.129028,11.632723>>, 2.000000)
						stand1Scene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)
						if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],stand1Scene,"missheistdocks2aleadinoutlsdh_2a_int","action_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
							endif
						endif
						if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
							if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],stand1Scene,"missheistdocks2aleadinoutlsdh_2a_int","action_floyd",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
							endif
						endif
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(iDocks2AStateBitset, TS_DOCKS2A_GAMEPLAY_HINT_SET)
				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1154.905640,-1517.455811,9.632723>>, <<-1149.402100,-1518.129028,11.632723>>, 2.000000)
						SET_GAMEPLAY_COORD_HINT((<<-1159.8833, -1522.0243, 10.5408>>), DEFAULT, 2500)
						
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.7 )
						SET_GAMEPLAY_HINT_FOV( 40.0 )
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.2)
						SET_BIT(iDocks2AStateBitset, TS_DOCKS2A_GAMEPLAY_HINT_SET)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				IF IS_SYNCHRONIZED_SCENE_RUNNING( stand1Scene )
					IF GET_SYNCHRONIZED_SCENE_PHASE(stand1Scene) > 0.95
						stand2Scene = CREATE_SYNCHRONIZED_SCENE(vDH2AScenePos,vDH2ASceneRot)
						if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							CPRINTLN( DEBUG_MISSION, "WADE EXISTS" )
							if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
								CPRINTLN( DEBUG_MISSION, "TASK WADE TO IDLE" )
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],stand2Scene,"missheistdocks2aleadinoutlsdh_2a_int","stand_loop_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE)
							endif
						endif
						if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
							CPRINTLN( DEBUG_MISSION, "FLOYD EXISTS" )
							if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
								CPRINTLN( DEBUG_MISSION, "TASK FLOYD TO IDLE" )
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1],stand2Scene,"missheistdocks2aleadinoutlsdh_2a_int","stand_loop_floyd",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_LOOP_WITHIN_SCENE)
							endif
						endif
						SET_SYNCHRONIZED_SCENE_LOOPED(stand2Scene,true)
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1155.075073,-1519.585327,9.432723>>, <<-1157.798706,-1521.591675,11.632723>>, 8.000000)
					SET_PLAYER_CONTROL( player_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON )
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1155.075073,-1519.585327,9.432723>>, <<-1157.798706,-1521.591675,11.632723>>, 8.000000)
					SET_PLAYER_CONTROL( player_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON )
				ENDIF
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF IS_ENTITY_IN_ANGLED_AREA( player_ped_ID(), <<-1155.075073,-1519.585327,9.432723>>, <<-1157.798706,-1521.591675,11.632723>>, 8.000000)
					SET_PLAYER_CONTROL( player_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON )
				ENDIF
			ENDIF
			IF GET_TIMER_IN_SECONDS_SAFE(tmrTriggerComplete) > fTriggerTime
				iDH2LeadStage++
			ENDIF
		break
		case 4
		
		break
	endswitch
	
	IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_TREVOR_VENICE)
		IF NOT IS_THIS_TV_FORCED_ON(TV_LOC_TREVOR_VENICE)
			START_AMBIENT_TV_PLAYBACK(TV_LOC_TREVOR_VENICE, TVCHANNELTYPE_CHANNEL_SPECIAL, TV_PLAYLIST_STD_WEAZEL)
			SET_TV_VOLUME( -25 )
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_DOCKS2A_AMBIENT_UPDATE()
ENDPROC
