//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Docks Heist Prep 2B - Trigger Scene Data					│
//│																				│
//│								Date: 26/10/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "script_ped.sch"


CONST_FLOAT 	TS_DOCKS_P2B_STREAM_IN_DIST			670.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_DOCKS_P2B_STREAM_OUT_DIST		700.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT 	TS_DOCKS_P2B_BATTLE_BUDDY_CALL_DIST	0.0//1100.0 	// Distance from trigger's blip at which the player can call in battle buddies.

CONST_FLOAT		TS_DOCKS_P2B_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_DOCKS_P2B_FRIEND_ACCEPT_BITS		BIT_NOBODY//BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

SCENARIO_BLOCKING_INDEX RemoveHeliBlockingArea
//SCENARIO_BLOCKING_INDEX RemoveFrontGuardBlockingArea

INT iWeaponAwayTimer

//BOOL doneOnYourOwnDialogue
//BOOL doneHelpText
BOOL PlayersWeaponPutAway

//Groups
REL_GROUP_HASH MARINE_LEAD_IN_GROUP_HASH

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_DOCKS_P2B_RESET()

	iWeaponAwayTimer 		= 0
	PlayersWeaponPutAway 	= FALSE

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_DOCKS_P2B_REQUEST_ASSETS()
	REQUEST_MODEL(CARGOBOB)
	REQUEST_MODEL(S_M_Y_MARINE_01)
	REQUEST_MODEL(BUZZARD)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_DOCKS_P2B_RELEASE_ASSETS()

	SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_MARINE_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_DOCKS_P2B_HAVE_ASSETS_LOADED()

	IF HAS_MODEL_LOADED(CARGOBOB)
	AND HAS_MODEL_LOADED(S_M_Y_MARINE_01)
	AND HAS_MODEL_LOADED(BUZZARD)
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_DOCKS_P2B_CREATE()
	
	//Check for which player is attempting this
	if GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		ADD_HELP_TO_FLOW_QUEUE("TRIG_T")
	endif
	
	ADD_RELATIONSHIP_GROUP("MarineLeadInGroupHash", MARINE_LEAD_IN_GROUP_HASH)	
	
	g_AmbientAreaData[eAMBAREA_ARMYBASE].bSuppressGateGuards = TRUE
	
	//Clear area of Cargobob's so only 1 scripted one exists inside military unit
	CLEAR_AREA(<<-1876.8, 2806.1, 32.3>>, 8, TRUE)
	CLEAR_AREA(<<-1857.7, 2797.9, 32.4>>, 8, TRUE)	
	CLEAR_AREA(<<-1591.7, 2799.5, 16.1>>, 3, TRUE)
	
	SET_PED_NON_CREATION_AREA(<<-1593, 2797.5, 19>>, <<-1590.7, 2802.6, 15.4>>) 
	SET_PED_NON_CREATION_AREA(<<-2308.3, 3386.6, 32.5>>, <<-2305.9, 3392.5, 29.4>>)
	
	//Switch off the vehicle generators for the 2 areas where the CARGOBOBs spawn so i can create my own ones. 
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2024.2, 2968, 30.2>>, <<-2004.0, 2921.5, 37.6>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2133.3, 2993.7, 41.1>>, <<-2152.7, 3039.4, 31.6>>, FALSE)	
	//The area where the cargobobs are spawned needs to be clear.
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1884.6, 2827.7, 41.8>>, <<-1843.1, 2772.1, 29.9>>, FALSE)	

	RemoveHeliBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-1884.6, 2827.7, 41.8>>, <<-1843.1, 2772.1, 29.9>>)	
//	RemoveFrontGuardBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-1594.7, 2788.8, 20.2>>, <<-1585.5, 2808.2, 13.3>>)	
	
	//Vehicles
	//Create the heli to steal
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		CLEAR_AREA(<< -2145.4856, 3018.2944, 31.8100 >>, 10, TRUE)
		g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(CARGOBOB, << -2145.4856, 3018.2944, 31.8100 >>, 330.4836)
		SET_VEHICLE_CAN_BE_TARGETTED(g_sTriggerSceneAssets.veh[0], FALSE)
		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.veh[0], TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[0], TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
	ENDIF
	
	//Create the enemy helicopter
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		CLEAR_AREA(<<-1866.4460, 3071.3950, 31.8104>>, 10, TRUE)
		g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(BUZZARD, <<-1866.4460, 3071.3950, 31.8104>>, 150.2863)
		SET_VEHICLE_CAN_BE_TARGETTED(g_sTriggerSceneAssets.veh[1], TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[1], TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[1], TRUE)
		SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[1])
	ENDIF
//	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
//		CLEAR_AREA(<<-2221.2722, 3245.3533, 31.8103>>, 10, TRUE)
//		g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(BUZZARD, <<-2221.2722, 3245.3533, 31.8103>>, 240.2412)
//		SET_VEHICLE_CAN_BE_TARGETTED(g_sTriggerSceneAssets.veh[2], TRUE)
//		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.veh[2], TRUE)
//		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[2], TRUE)
//		SET_VEHICLE_MODEL_IS_SUPPRESSED(BUZZARD, TRUE)
//	ENDIF
	
	//Peds
	//East gates guard
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1590.5227, 2796.3022, 15.9184>>, 247.0896)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[0], TRUE)
		GIVE_WEAPON_TO_PED(g_sTriggerSceneAssets.ped[0], WEAPONTYPE_ASSAULTRIFLE, 2000, TRUE)
		SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[0], 10)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[0], MARINE_LEAD_IN_GROUP_HASH)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	ENDIF
	//West gates guard
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2303.7996, 3387.9900, 30.0340>>, 58.5763)
		SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[1], TRUE)
		GIVE_WEAPON_TO_PED(g_sTriggerSceneAssets.ped[1], WEAPONTYPE_ASSAULTRIFLE, 2000, TRUE)
		SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[1], 10)
		SET_PED_RELATIONSHIP_GROUP_HASH(g_sTriggerSceneAssets.ped[0], MARINE_LEAD_IN_GROUP_HASH)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
	ENDIF	
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, MARINE_LEAD_IN_GROUP_HASH, RELGROUPHASH_PLAYER)

	SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
	SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_MARINE_01)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_DOCKS_P2B_RELEASE()
	
	//Vehicles
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	
	//Peds
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[0], TRUE)
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[1], TRUE)
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
		ENDIF
	ENDIF
	
	REMOVE_RELATIONSHIP_GROUP(MARINE_LEAD_IN_GROUP_HASH)
	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_DOCKS_P2B_DELETE()
	
	//Vehicles
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	
	//Peds
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	
	//Set car generators back to normal.
	CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	CLEAR_PED_NON_CREATION_AREA()
	
	REMOVE_RELATIONSHIP_GROUP(MARINE_LEAD_IN_GROUP_HASH)
	
	REMOVE_SCENARIO_BLOCKING_AREA(RemoveHeliBlockingArea)
//	REMOVE_SCENARIO_BLOCKING_AREA(RemoveFrontGuardBlockingArea)

ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_DOCKS_P2B_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Trigger mission script if player enters the military base.
		IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
			RETURN TRUE
		ENDIF	
		//This is the check for the front gates. If player passes the barrier trigger the mission.
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-1589.7, 2794.7, 16>>) < 40
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.513794,2794.147705,14.842220>>, <<-1587.691650,2806.101318,20.295572>>, 16.000000)
				RETURN TRUE
			ENDIF	
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1590.457886,2780.462891,14.907547>>, <<-1575.294556,2794.159668,20.157387>>, 19.000000)
				RETURN TRUE
			ENDIF	
		ENDIF
		//These are the checks for the back gates. If player passes the barriers trigger the mission.
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-2306.4, 3389, 30>>) < 40
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2309.717529,3387.245361,29.219893>>, <<-2293.503418,3375.455078,34.326920>>, 7.750000)
				RETURN TRUE
			ENDIF	
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2297.018799,3387.863037,29.544601>>, <<-2282.234619,3376.637451,34.745003>>, 9.000000)
				RETURN TRUE
			ENDIF
		ENDIF
		//Start the mission if the player equips a gun in view of the guards
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])		
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0]) < 25
					IF CAN_PED_SEE_HATED_PED(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())	
						IF PlayersWeaponPutAway = TRUE
							IF GET_GAME_TIMER() > (iWeaponAwayTimer + 3000)
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_DOCKS_P2B_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_DOCKS_P2B_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_DOCKS_P2B_UPDATE()
	
	//Disable the scenario guards
	IF DOES_SCENARIO_GROUP_EXIST("ARMY_GUARD")
		IF IS_SCENARIO_GROUP_ENABLED("ARMY_GUARD")
			SET_SCENARIO_GROUP_ENABLED("ARMY_GUARD", FALSE)
		ENDIF
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		g_sTriggerSceneAssets.veh[2] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF PlayersWeaponPutAway = FALSE
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
				iWeaponAwayTimer = GET_GAME_TIMER()
				PlayersWeaponPutAway = TRUE
			ENDIF
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
				iWeaponAwayTimer = GET_GAME_TIMER()
				PlayersWeaponPutAway = TRUE
			ENDIF
		ENDIF
	ENDIF	
					
//	//These coords are infront of the main gates where the barrier and guards are. Give the guards tasks to approach the player as they would in real life.
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1588.852173,2794.052490,14.988657>>, <<-1575.447632,2779.605469,20.364277>>, 18.000000)
//		
//	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_DOCKS_P2B_AMBIENT_UPDATE()
ENDPROC
