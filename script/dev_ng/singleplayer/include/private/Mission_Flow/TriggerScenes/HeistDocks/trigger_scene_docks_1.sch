//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Docks Heist Setup 1 - Trigger Scene Data					│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.
//|			//ped[1] = Wade
//|         //ped[0] = Floyd
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "tv_control_public.sch"
USING "load_queue_public.sch"

USING "flow_mission_trigger_public.sch"

CONST_FLOAT 	TS_DOCKS1_STREAM_IN_DIST		25.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_DOCKS1_STREAM_OUT_DIST		35.0	// Distance at which the scene is deleted and unloaded.	

CONST_FLOAT		TS_DOCKS1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_DOCKS1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

structPedsForConversation sPedsForConversation
VECTOR sceneD1LeadInPosition = << -1159.737, -1520.509, 9.629 >>
VECTOR sceneD1LeadInRotation = << 0.000, 0.000, 45.000 >>
INT sceneD1LeadIn
INT iSceneD1LeadIn

BOOL bTriggerConversation = FALSE
BOOL bSafehouseCargenDisabled = FALSE

INTERIOR_INSTANCE_INDEX interior_living_room

VECTOR vHintCamCoord = <<-1158.2783, -1521.0677, 11.3094>>
FLOAT fHintCamFollow = 0.65
FLOAT fHintCamOffset = 0
FLOAT fHintCamFove = 35.00


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_DOCKS1_RESET()
	bSafehouseCargenDisabled = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_DOCKS1_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_WADE))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_FLOYD))
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missheistdockssetup1leadinout")
	LOAD_QUEUE_MEDIUM_ADD_WAYPOINT_REC(g_sTriggerSceneAssets.loadQueue, "d1leadin")
	LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(g_sTriggerSceneAssets.loadQueue, "DOCKH1", MISSION_TEXT_SLOT)
	LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(g_sTriggerSceneAssets.loadQueue, "D1AUD", MISSION_DIALOGUE_TEXT_SLOT)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_DOCKS1_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	
	IF interior_living_room != NULL
		UNPIN_INTERIOR(interior_living_room)
	ENDIF
	
	IF bSafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as releasing assets.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_CITY)
		bSafehouseCargenDisabled = FALSE
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_DOCKS1_HAVE_ASSETS_LOADED()
	RETURN HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_DOCKS1_CREATE()

	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_JEWELRY_1), 25.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_FLOYD, <<-1159.71, -1521.33, 10.65>>)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_SPECIAL, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_JBIB, 0, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	ADD_PED_FOR_DIALOGUE(sPedsForConversation,3,g_sTriggerSceneAssets.ped[0],"FLOYD")
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_WADE, <<-1158.07, -1520.90, 10.65>>)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 3, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 2, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL2, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_DECL, 2, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_JBIB, 0, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	ADD_PED_FOR_DIALOGUE(sPedsForConversation,4,g_sTriggerSceneAssets.ped[1],"WADE")
	
	ADD_PED_FOR_DIALOGUE(sPedsForConversation,2,PLAYER_PED_ID(),"TREVOR")
	
	sceneD1LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneD1LeadInPosition,sceneD1LeadInRotation)

	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_loop_floyd", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_loop_wade", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneD1LeadIn,TRUE)
	
	iSceneD1LeadIn = 0
		
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("d1leadin",TRUE)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_DOCKS_1,
												"LSDH_INT",
												CS_NONE,
												CS_NONE,
												CS_ALL)
												
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_1, "FLOYD", g_sTriggerSceneAssets.ped[0])
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_HEIST_DOCKS_1, "WADE", g_sTriggerSceneAssets.ped[1])
	
	SET_BUILDING_STATE (BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_CLEANUP)				// Hide the normal Mr. Raspberry Jam
	SET_BUILDING_STATE (BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_DESTROYED)	// Display the cutscene Mr. Raspberry Jam.

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_DOCKS1_RELEASE()

	//Release Floyd
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 3)
	
	//Release Wade
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("d1leadin",FALSE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	IF interior_living_room != NULL
		UNPIN_INTERIOR(interior_living_room)
	ENDIF
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)	
	
	IF bSafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as cleaning up trigger scene.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_CITY)
		bSafehouseCargenDisabled = FALSE
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_DOCKS1_DELETE()

	//Delete Floyd
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 3)
	
	//Delete Wade
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	REMOVE_PED_FOR_DIALOGUE(sPedsForConversation, 4)

	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("d1leadin",FALSE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	SET_BUILDING_STATE (BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_NORMAL, FALSE, FALSE)				// Restore Mr. Raspberry Jam
	SET_BUILDING_STATE (BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_NORMAL, FALSE, FALSE)		// Hide the cutscene Mr. Raspberry Jam

	IF bSafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as cleaning up trigger scene.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_CITY)
		bSafehouseCargenDisabled = FALSE
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_DOCKS1_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iSceneD1LeadIn = 3
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_DOCKS1_HAS_BEEN_DISRUPTED()
	//Has the player injured or damaged Floyd
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	//Has the player injured or damaged Wade
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[1], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_DOCKS1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_DOCKS1_UPDATE()
	
	#IF IS_DEBUG_BUILD
		PRINTSTRING("iSceneD1LeadIn:: ")PRINTINT(iSceneD1LeadIn)PRINTNL()
	#ENDIF
	
	//Fix for 1045386: Freeing up extra memory for this trigger scene by cleaning up vehicle gen 
	//if the player is in the Floyd interior. Flagging it to be recreated as soon as the player steps
	//outside again.- BenR
	IF bSafehouseCargenDisabled
		IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1145.479492,-1512.777344,9.403284>>, <<-1160.304810,-1523.243408,12.632729>>, 7.750000)
			CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as leaving interior.")
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_CITY)
			bSafehouseCargenDisabled = FALSE
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_TREVOR_SAVEHOUSE_CITY))
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1145.479492,-1512.777344,9.403284>>, <<-1160.304810,-1523.243408,12.632729>>, 7.750000)
				CPRINTLN(DEBUG_TRIGGER, "Deleting savehouse cargen to free up memory.")
				DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY)
				bSafehouseCargenDisabled = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1148.289917,-1524.254883,9.363565>>, <<-1151.240112,-1520.051636,12.882723>>, 2.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1147.680786,-1522.989990,6.383060>>, <<-1144.043091,-1520.518433,9.938194>>, 1.500000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF iSceneD1LeadIn = 0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1148.283936,-1524.209595,9.132868>>, <<-1150.174438,-1521.547241,12.132729>>, 2.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1148.297974,-1524.263550,8.892135>>, <<-1150.187256,-1521.566895,11.883015>>, 2.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1149.387451,-1524.254272,8.883018>>, <<-1146.503052,-1522.315063,12.252842>>, 2.250000)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1160.324341,-1522.264526,8.382723>>, <<-1152.145630,-1517.004639,12.882723>>, 5.000000)
		SET_GAMEPLAY_COORD_HINT(vHintCamCoord,-1,2000)
		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintCamFollow)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintCamOffset)
		SET_GAMEPLAY_HINT_FOV(fHintCamFove)
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1150.350220,-1521.425659,9.132723>>, <<-1151.579346,-1519.680664,12.632723>>, 2.250000)
			STOP_GAMEPLAY_HINT()
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
		ENDIF
	ENDIF
	
	
	IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_TREVOR_VENICE)
	AND NOT IS_THIS_TV_FORCED_ON(TV_LOC_TREVOR_VENICE)
	AND NOT IS_THIS_TV_ON(TV_LOC_TREVOR_VENICE)
		START_AMBIENT_TV_PLAYBACK(TV_LOC_TREVOR_VENICE, TVCHANNELTYPE_CHANNEL_1, TV_PLAYLIST_SPECIAL_PLSH1_INTRO, FALSE, TRUE )
		SET_TV_VOLUME( -25.0)
	ENDIF
	
	SWITCH iSceneD1LeadIn
		CASE 0
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1149.637939,-1522.367432,9.382867>>, <<-1151.353516,-1519.901245,12.382729>>, 1.500000)
			AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					sceneD1LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneD1LeadInPosition,sceneD1LeadInRotation)
					TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_action_floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
					TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_action_wade", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneD1LeadIn,FALSE)
					SET_SYNCHRONIZED_SCENE_RATE(sceneD1LeadIn,1.0)
					bTriggerConversation = FALSE
					SET_VEHICLE_POPULATION_BUDGET(0)
					SET_PED_POPULATION_BUDGET(0)	
					iSceneD1LeadIn ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF bTriggerConversation = FALSE
				IF CREATE_CONVERSATION(sPedsForConversation,"D1AUD","DS1_LDI", CONV_PRIORITY_HIGH)
					bTriggerConversation = TRUE
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneD1LeadIn)
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneD1LeadIn) = 1
						sceneD1LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneD1LeadInPosition,sceneD1LeadInRotation)
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_react_floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], sceneD1LeadIn, "missheistdockssetup1leadinout", "lsdh_int_leadin_react_wade", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS )
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneD1LeadIn,FALSE)
						SET_SYNCHRONIZED_SCENE_RATE(sceneD1LeadIn,1.0)
						iSceneD1LeadIn ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			IF bTriggerConversation = FALSE
				IF CREATE_CONVERSATION(sPedsForConversation,"D1AUD","DS1_LDI", CONV_PRIORITY_HIGH)
					bTriggerConversation = TRUE
				ENDIF
			ENDIF
			
			IF bTriggerConversation
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iSceneD1LeadIn ++
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneD1LeadIn)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneD1LeadIn) > 0.7
					iSceneD1LeadIn ++
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneD1LeadIn)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneD1LeadIn) > 0.55
//					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
//					CLEAR_PED_TASKS(PLAYER_PED_ID())
//					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
//					SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
					//iSceneD1LeadIn ++
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1156.520142,-1520.617920,9.382729>>, <<-1157.288696,-1519.428955,12.632729>>, 1.000000)
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			ENDIF
		BREAK
		
		CASE 3
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
		BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_DOCKS1_AMBIENT_UPDATE()
ENDPROC
