//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Docks Heist Prep 1 - Trigger Scene Data						│
//│																				│
//│								Date: 26/10/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			veh[0]	= sub														│
//│			ped[0] = phone dock worker	 ped[1] = clip board dock ped			│
//│			object[0] = dummy1	object[1] = dummy2 								│
//│			object[2] = button	object[3] = phone								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"

STRUCT ROPE_DATA
ROPE_INDEX 	rope
FLOAT 		fLength
INT 		iNumSegments
FLOAT 		fLengthPerSegment
ENDSTRUCT


CONST_FLOAT 	TS_DOCKS_P1_TRIGGER_DIST			90.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_DOCKS_P1_HELI_TRIGGER_DIST		150.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_DOCKS_P1_STREAM_IN_DIST			465.0
CONST_FLOAT		TS_DOCKS_P1_STREAM_OUT_DIST			480.0
CONST_FLOAT 	TS_DOCKS_P1_BATTLE_BUDDY_CALL_DIST	0.0//750.0 	// Distance from trigger's blip at which the player can call in battle buddies.
CONST_FLOAT		ROPE_LENGTH 						3.0

CONST_FLOAT		TS_DOCKS_P1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_DOCKS_P1_FRIEND_ACCEPT_BITS	BIT_NOBODY//BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

bool bPhoneAdded = false
bool bactivatePhysics = FALSE
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_DOCKS_P1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_DOCKS_P1_REQUEST_ASSETS()
	REQUEST_MODEL(SUBMERSIBLE)
	request_model(PROP_LD_TEST_01)
	REQUEST_MODEL(prop_sub_release)
	request_model(S_M_M_DOCKWORK_01)
	request_model(P_AMB_PHONE_01)
	REQUEST_ADDITIONAL_TEXT("DOCKP1", MISSION_TEXT_SLOT)
	REQUEST_ANIM_DICT("cellphone@str")
	REQUEST_ANIM_DICT("missheistdocksprep1ig_1")
	REQUEST_WAYPOINT_RECORDING("docksprep1")
	ROPE_LOAD_TEXTURES()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_DOCKS_P1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(SUBMERSIBLE)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_TEST_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(prop_sub_release)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_DOCKWORK_01)
	set_model_as_no_longer_needed(P_AMB_PHONE_01)
	REMOVE_ANIM_DICT("cellphone@str")
	REMOVE_ANIM_DICT("missheistdocksprep1ig_1")
	ROPE_UNLOAD_TEXTURES()
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_DOCKS_P1_HAVE_ASSETS_LOADED()
	if HAS_MODEL_LOADED(SUBMERSIBLE)
	and HAS_MODEL_LOADED(PROP_LD_TEST_01)
	and HAS_MODEL_LOADED(prop_sub_release)
	and HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
	and HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	and HAS_MODEL_LOADED(P_AMB_PHONE_01)
	and HAS_ANIM_DICT_LOADED("cellphone@str")
	and HAS_ANIM_DICT_LOADED("missheistdocksprep1ig_1")
	and GET_IS_WAYPOINT_RECORDING_LOADED("docksprep1")
	and ROPE_ARE_TEXTURES_LOADED()
		RETURN TRUE
	endif
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_DOCKS_P1_CREATE()
	//sub
	//B* 2359420: Clear area in case trigger scene had run prior
	CLEAR_AREA(<<1260.80225, -3006.41699, 18.3289>>,100,TRUE)

	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(SUBMERSIBLE,<<1260.80225, -3006.41699, 18.3289>>, 178.9076)
	SET_MODEL_AS_NO_LONGER_NEEDED(SUBMERSIBLE)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[0],false)
	
	//peds
	g_sTriggerSceneAssets.ped[0] = create_ped(PEDTYPE_MISSION,S_M_M_DOCKWORK_01,<<1229.32166, -3002.87915, 8.31861>>,270)		
		
	TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[0],"cellphone@str","cellphone_call_listen_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
	SET_PED_FLEE_ATTRIBUTES(g_sTriggerSceneAssets.ped[0],FA_NEVER_FLEE,false)
	SET_PED_COMBAT_ATTRIBUTES(g_sTriggerSceneAssets.ped[0],CA_ALWAYS_FLEE,true)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0],true)
	STOP_PED_SPEAKING(g_sTriggerSceneAssets.ped[0], TRUE)
		
	//objects
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_LD_TEST_01,<<1260.69153, -3008.28589, 23.73365>>)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[0],true)
		
	g_sTriggerSceneAssets.object[1] = CREATE_OBJECT(PROP_LD_TEST_01,<<1260.75208, -3004.99414, 22.68315>>)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[1],true)
	
	g_sTriggerSceneAssets.object[2] = CREATE_OBJECT(prop_sub_release,<<1249.10547, -3007.96777, 9.68718>>)
	SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[2],<<0,0,90>>)	
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[2],true)
	
	g_sTriggerSceneAssets.object[3] = CREATE_OBJECT(P_AMB_PHONE_01,<<1229.32166, -3002.87915, 8.31861>>)
	
	g_sTriggerSceneAssets.flag		= FALSE
	bactivatePhysics				= FALSE
	bPhoneAdded 					= FALSE	
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_DOCKS_P1_RELEASE()

//Delete ropes.
	IF g_sTriggerSceneAssets.flag	
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[0])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[0] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[1])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[1] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[2])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[2] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[3])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[3] )
		endif
		g_sTriggerSceneAssets.flag = FALSE
	ENDIF

//Release sub.
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	
//Release peds.
	TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[1])

//Release objects.
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[2])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[3])
	
ENDPROC

PROC TS_DOCKS_P1_DELETE()
//Delete ropes.
	IF g_sTriggerSceneAssets.flag	
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[0])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[0] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[1])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[1] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[2])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[2] )
		endif
		if DOES_SCRIPT_OWN_ROPE(g_sTriggerSceneAssets.rope[3])
			DELETE_ROPE( g_sTriggerSceneAssets.rope[3] )
		endif
		g_sTriggerSceneAssets.flag = FALSE
	ENDIF
	
//Delete sub.
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	
//Delete peds.
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])

//Delete objects.
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[2])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[3])
	
	bPhoneAdded =  false
ENDPROC

//NB. Docks Heist Prep 1 has a custom trigger radius.
CONST_FLOAT LOCATE_SIZE_DOCKS_P1_CUSTOM	100.0

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_DOCKS_P1_HAS_BEEN_TRIGGERED()
VECTOR vTriggerPosition = <<1240.06799, -3009.08228, 8.31891>>//GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_DOCKS_P1)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			IF IS_PED_IN_FLYING_VEHICLE(player_ped_id())
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_DOCKS_P1_HELI_TRIGGER_DIST)
			ELSE
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_DOCKS_P1_TRIGGER_DIST)
			ENDIF
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
			IF fDistanceSquaredFromTrigger < (TS_DOCKS_P1_HELI_TRIGGER_DIST*TS_DOCKS_P1_HELI_TRIGGER_DIST)			
				RETURN TRUE
			ENDIF
		else
			IF fDistanceSquaredFromTrigger < (TS_DOCKS_P1_TRIGGER_DIST*TS_DOCKS_P1_TRIGGER_DIST)			
				RETURN TRUE
			ENDIF
		endif
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		and IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			if IS_ENTITY_IN_WATER(g_sTriggerSceneAssets.veh[0])
			or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[0],player_ped_id())				
				RETURN TRUE
			endif
		endif
		int docker
		for docker = 0 to 1
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0],player_ped_id())					
					RETURN TRUE
				endif
			endif
		endfor
		int rope
		for rope = 0 to 3
			if DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[rope])
				if not IS_ROPE_ATTACHED_AT_BOTH_ENDS(g_sTriggerSceneAssets.rope[rope])
					RETURN TRUE
				endif
			endif
		endfor
	ENDIF

RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_DOCKS_P1_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		//Check for delivery van being damaged or destroyed.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_DOCKS_P1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_DOCKS_P1_UPDATE()

	IF NOT g_sTriggerSceneAssets.flag
		CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Process creation of ropes")
	
		if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		and DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
		and DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			if IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			
				IF DOES_ENTITY_HAVE_PHYSICS(g_sTriggerSceneAssets.object[0])
					
					IF DOES_ENTITY_HAVE_PHYSICS(g_sTriggerSceneAssets.object[1])

						SET_CG_AT_BOUNDCENTER(g_sTriggerSceneAssets.veh[0])

					//attach rope 1 -front
						if not DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[0])
							g_sTriggerSceneAssets.rope[0] = ADD_ROPE(<<1260.69153, -3008.28589, 23.73365>>,<<0,0,0>>,ROPE_LENGTH,PHYSICS_ROPE_DEFAULT,ROPE_LENGTH,0.5,0.5,false,FALSE,FALSE,2,true)
							ATTACH_ENTITIES_TO_ROPE(g_sTriggerSceneAssets.rope[0],g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.veh[0],
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.object[0],<<0,0,0>>),
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<1.8,1.02,1.11>>)				
							,5,0,0)
							CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Created rope g_sTriggerSceneAssets.object[0]")
						endif
					//attach rope 2 -front
						if not DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[1])
							g_sTriggerSceneAssets.rope[1] = ADD_ROPE(<<1260.69153, -3008.28589, 23.73365>>,<<0,0,0>>,ROPE_LENGTH,PHYSICS_ROPE_DEFAULT,ROPE_LENGTH,0.5,0.5,false,FALSE,FALSE,2,true)
							ATTACH_ENTITIES_TO_ROPE(g_sTriggerSceneAssets.rope[1],g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.veh[0],
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.object[0],<<0,0,0>>),
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<-1.8,1.02,1.11>>),
							ROPE_LENGTH,0,0)
							CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Created rope g_sTriggerSceneAssets.object[1]")
						endif	
					//attach rope 3 - back
						if not DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[2])
							g_sTriggerSceneAssets.rope[2] = ADD_ROPE(<<1260.75208, -3004.99414, 22.68315>>,<<0,0,0>>,ROPE_LENGTH,PHYSICS_ROPE_DEFAULT,ROPE_LENGTH,0.5,0.5,false,FALSE,FALSE,2,true)
							ATTACH_ENTITIES_TO_ROPE(g_sTriggerSceneAssets.rope[2],g_sTriggerSceneAssets.object[1],g_sTriggerSceneAssets.veh[0],
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.object[1],<<0,0,0>>),
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<1.8,-3.07,1.11>>),
							ROPE_LENGTH,0,0)
							CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Created rope g_sTriggerSceneAssets.object[2]")
						endif	
					//attach rope 4 -back
						if not DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[3])
							g_sTriggerSceneAssets.rope[3] = ADD_ROPE(<<1260.75208, -3004.99414, 22.68315>>,<<0,0,0>>,ROPE_LENGTH,PHYSICS_ROPE_DEFAULT,ROPE_LENGTH,0.5,0.5,false,FALSE,FALSE,2,true)
							ATTACH_ENTITIES_TO_ROPE(g_sTriggerSceneAssets.rope[3],g_sTriggerSceneAssets.object[1],g_sTriggerSceneAssets.veh[0],
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.object[1],<<0,0,0>>),
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<-1.8,-3.07,1.11>>),
							ROPE_LENGTH,0,0)	
							CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Created rope g_sTriggerSceneAssets.object[3]")
						endif
						
						if DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[0])
						and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[1])
						and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[2])
						and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[3])
							g_sTriggerSceneAssets.flag 		= TRUE	
							bactivatePhysics 				= TRUE
							CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Ropes created!")
						endif
					
					ELSE
						CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Waiting on physics:object[1]")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Waiting on physics:object[0]")
				ENDIF
			ENDIF			
		endif
	Else
		if bActivatePhysics
			CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Process waking physics")
			
			if DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[0])
			and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[1])
			and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[2])
			and DOES_ROPE_EXIST(g_sTriggerSceneAssets.rope[3])
				if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
					if IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
						FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.veh[0],false)
						ACTIVATE_PHYSICS(g_sTriggerSceneAssets.veh[0])
						bactivatePhysics = FALSE
						CDEBUG1LN(DEBUG_MIKE, "TS_DOCKS_P1_UPDATE() - Physics woken!")
					ENDIF
				endif
			endif
		endif
	endif
	
	if not bPhoneAdded
		if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[3])
		and DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		and not IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			IF DOES_ENTITY_HAVE_PHYSICS(g_sTriggerSceneAssets.object[3])					
				ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[3],g_sTriggerSceneAssets.ped[0]
						,GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0],BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
			endif
		endif
	endif
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_DOCKS_P1_AMBIENT_UPDATE()
ENDPROC
