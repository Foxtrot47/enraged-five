//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Trevor 4 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "RC_Helper_Functions.sch"

CONST_FLOAT 	TS_TREVOR4_STREAM_IN_DIST		25.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_TREVOR4_STREAM_OUT_DIST		35.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_TREVOR4_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_TREVOR4_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_TREVOR4_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

structPedsForConversation sLeadInConversation
VECTOR vLeadInPosition = << -1148.219, -1522.4, 10.633 >>
VECTOR vLeadInRotation = << -0.000, 0.000, -53.75 >>
INT sceneLeadIn
INT iLeadInUpdate = 0
BOOL bCleanedUpSceneDisruption = FALSE
BOOL bStartedTrevorConversation = FALSE
BOOL bDoingExitIdleLoop = FALSE
INT iConversationTimer
OBJECT_INDEX oDoor
INTERIOR_INSTANCE_INDEX interiorApartment
BOOL bCheckForSwitchingCharacter = FALSE

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_TREVOR4_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_TREVOR4_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_FLOYD))
	REQUEST_ANIM_DICT("misstrevor4leadinouttrv_4_int")
	interiorApartment = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-1151.1569, -1520.1526, 9.6327>>, "v_trevors")
	PIN_INTERIOR_IN_MEMORY(interiorApartment)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Assets requested") #ENDIF
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_TREVOR4_RELEASE_ASSETS()
	SAFE_RELEASE_OBJECT(oDoor)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	SET_MODEL_AS_NO_LONGER_NEEDED(v_ilev_trev_doorfront)
	REMOVE_ANIM_DICT("misstrevor4leadinouttrv_4_int")
	UNPIN_INTERIOR(interiorApartment)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Assets released") #ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_TREVOR4_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_FLOYD))
	AND HAS_ANIM_DICT_LOADED("misstrevor4leadinouttrv_4_int")
	AND IS_VALID_INTERIOR(interiorApartment)
	AND IS_INTERIOR_READY(interiorApartment)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Assets now loaded") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_TREVOR4_CREATE()

	iLeadInUpdate = 0

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_4,
												"TRV_5_INT",
												CS_NONE,
												CS_NONE,
												CS_ALL)
												
												
	// Clean-up floyd's apartment as Debra is back
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_2, BUILDINGSTATE_NORMAL)
	
	#IF NOT IS_JAPANESE_BUILD
		SET_BUILDING_STATE			(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SEX_TOYS, BUILDINGSTATE_NORMAL)
	#ENDIF
	
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_TORTURE_TOOLS, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_WHISKY, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_NORMAL)			
	SET_BUILDING_STATE				(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SOFA, BUILDINGSTATE_NORMAL)
	
	iLeadInUpdate = 0
	bCleanedUpSceneDisruption = FALSE
	bStartedTrevorConversation = FALSE
	bDoingExitIdleLoop = FALSE
	bCheckForSwitchingCharacter = FALSE
	
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Completed TS_TREVOR4_CREATE") #ENDIF
												
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player disrupts a scene before triggering it.
PROC TS_TREVOR4_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 0)
	REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 1)
	IF DOES_ENTITY_EXIST(oDoor)		
		STOP_SYNCHRONIZED_ENTITY_ANIM(oDoor, INSTANT_BLEND_DURATION, TRUE)
		//SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_trev_doorfront, <<-1149.71, -1521.09, 10.78>>, TRUE, 0.0) // Lock door // See B*1812652 - this causes a pop in the frame prior to the cutscene, so now locking the door in the mission itself
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: stopped anim on door") #ENDIF
	ENDIF
	SAFE_RELEASE_OBJECT(oDoor)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Entities released") #ENDIF
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_TREVOR4_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 0)
	REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 1)
	IF DOES_ENTITY_EXIST(oDoor)		
		STOP_SYNCHRONIZED_ENTITY_ANIM(oDoor, INSTANT_BLEND_DURATION, TRUE)
		//SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(v_ilev_trev_doorfront, <<-1149.71, -1521.09, 10.78>>, TRUE, 0.0) // Lock door // See B*1812652 - this causes a pop in the frame prior to the cutscene, so now locking the door in the mission itself
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: stopped anim on door") #ENDIF
	ENDIF
	SAFE_RELEASE_OBJECT(oDoor)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Entities deleted") #ENDIF
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_TREVOR4_HAS_BEEN_TRIGGERED()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF iLeadInUpdate = 4
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Starting mission") #ENDIF
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_TREVOR4_HAS_BEEN_DISRUPTED()
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Player is switching character from Trevor") #ENDIF
		TS_TREVOR4_RELEASE()
		TS_TREVOR4_RELEASE_ASSETS()
		bCheckForSwitchingCharacter = TRUE
		RETURN TRUE
	ENDIF
	//Has the player injured or damaged Floyd
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: IS_PED_INJURED returned true") #ENDIF
			RETURN TRUE
		ELSE
			VECTOR v_ped_coord = GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY returned true") #ENDIF
				RETURN TRUE
			ELIF IS_BULLET_IN_AREA(v_ped_coord, 20, TRUE)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: IS_BULLET_IN_AREA returned true") #ENDIF
				RETURN TRUE
			ELIF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, v_ped_coord, 20)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: IS_EXPLOSION_IN_SPHERE returned true") #ENDIF
				RETURN TRUE
			ELIF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(v_ped_coord, WEAPONTYPE_STICKYBOMB, 20, TRUE) // B*1506053
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: IS_PROJECTILE_TYPE_WITHIN_DISTANCE returned true") #ENDIF
				RETURN TRUE
			ELSE
				VECTOR v_fire_coord
				IF GET_CLOSEST_FIRE_POS(v_fire_coord, v_ped_coord)
				AND GET_DISTANCE_BETWEEN_COORDS(v_ped_coord, v_fire_coord) < 10
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Fire is nearby") #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_TREVOR4_IS_BLOCKED()
	IF bCheckForSwitchingCharacter = TRUE
	AND IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Character switch is in progress") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks if the player's last vehicle likely to be in the way of the player walking up the stairs (fix for B*1958371)
FUNC BOOL IS_LAST_PLAYER_VEHICLE_TOO_CLOSE()
	IF IS_ENTITY_ALIVE(GET_LAST_DRIVEN_VEHICLE())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), GET_LAST_DRIVEN_VEHICLE()) < 2
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Player too close to last vehicle driven so not walking up the stairs") #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_TREVOR4_UPDATE()

	IF iLeadInUpdate < 3
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME() // B*2131315 Prevent player seeing the scene being setup
	ENDIF

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF bCleanedUpSceneDisruption = TRUE
			 // Do nothing - prevents the scene being run again
		ELIF TS_TREVOR4_HAS_BEEN_DISRUPTED()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 0)
			REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 1)
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				TASK_CLEAR_LOOK_AT(g_sTriggerSceneAssets.ped[0])
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bCleanedUpSceneDisruption = TRUE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Player disrupted scene") #ENDIF
		ELSE
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
			ENDIF
			SWITCH iLeadInUpdate
				CASE 0
					REQUEST_MODEL(v_ilev_trev_doorfront)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1143.596313,-1531.036865,11.711957>>, <<-1155.015259,-1514.864868,14.711953>>, 19.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1142.949829,-1518.522949,3>>, <<-1149.981201,-1523.386230,12.633015>>, 3.500000)
						IF HAS_MODEL_LOADED(v_ilev_trev_doorfront) // B*1518198
							IF NOT DOES_ENTITY_EXIST(oDoor)
								oDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-1149.71, -1521.09, 10.78>>, 10, v_ilev_trev_doorfront)
							ENDIF
							IF DOES_ENTITY_EXIST(oDoor)
								ADD_PED_FOR_DIALOGUE(sLeadInConversation, 1, PLAYER_PED_ID(), "TREVOR")
								CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_FLOYD, <<-1148.7858, -1523.2659, 9.6330>>)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE) // B*1574213 - Stop Floyd reacting to nearby shootouts
								SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_FLOYD))
								sceneLeadIn = CREATE_SYNCHRONIZED_SCENE(vLeadInPosition, vLeadInRotation)
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneLeadIn, "misstrevor4leadinouttrv_4_int", "leadin_loop_start_idle_floyd", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-1149.71, -1521.09, 10.78>>, 5, v_ilev_trev_doorfront, sceneLeadIn, "leadin_loop_start_idle_front_door", "misstrevor4leadinouttrv_4_int", INSTANT_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneLeadIn, TRUE)
								ADD_PED_FOR_DIALOGUE(sLeadInConversation, 0, g_sTriggerSceneAssets.ped[0], "FLOYD")
								SET_PED_CAN_BE_TARGETTED(g_sTriggerSceneAssets.ped[0], FALSE) // B*1506048
								#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Created Floyd") #ENDIF
								iLeadInUpdate++
							ELSE
								#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: oDoor doesn't exist - iLeadInUpdate = ", iLeadInUpdate) #ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: v_ilev_trev_doorfront hasn't loaded - iLeadInUpdate = ", iLeadInUpdate) #ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					AND CREATE_CONVERSATION(sLeadInConversation, "TRV5AUD", "TRV5_INT_LI1", CONV_PRIORITY_HIGH)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Started TRV5_INT_LI1 conversation") #ENDIF
						iLeadInUpdate++
					ENDIF
				BREAK
				CASE 2
					REQUEST_MODEL(v_ilev_trev_doorfront)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1147.397095,-1525.267578,8.961864>>, <<-1151.109985,-1520.072754,14.711858>>, 2.500000)
					AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Player triggered mission by jumping down from roof so just play the cutscene") #ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						iLeadInUpdate = 4
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1142.395630,-1519.313965,6.058840>>, <<-1148.072021,-1523.278198,10.633021>>, 2.300000)
						AND HAS_MODEL_LOADED(v_ilev_trev_doorfront) // B*1518198
							IF NOT DOES_ENTITY_EXIST(oDoor)
								oDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-1149.71, -1521.09, 10.78>>, 10, v_ilev_trev_doorfront)
							ENDIF
							IF DOES_ENTITY_EXIST(oDoor)
								IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0.3>>, TRUE, -1, 3000)
								ELSE
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
								ENDIF
								SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
								sceneLeadIn = CREATE_SYNCHRONIZED_SCENE(vLeadInPosition, vLeadInRotation)
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneLeadIn, "misstrevor4leadinouttrv_4_int", "leadin_action_floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-1149.71, -1521.09, 10.78>>, 5, v_ilev_trev_doorfront, sceneLeadIn, "leadin_action_front_door", "misstrevor4leadinouttrv_4_int", INSTANT_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneLeadIn, FALSE)
								TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), -1)
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1)
								iConversationTimer = GET_GAME_TIMER()
								iLeadInUpdate++
							ELSE
								#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: oDoor doesn't exist - iLeadInUpdate = ", iLeadInUpdate) #ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // Don't make player walk if he's driven up the stairs on a bike
					AND NOT IsPedPerformingTask(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-1147.62, -1522.95, 9.63>>) > 2
					AND NOT IS_LAST_PLAYER_VEHICLE_TOO_CLOSE()
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-1147.62, -1522.95, 9.63>>, PEDMOVEBLENDRATIO_WALK)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Player walking to top of stairs") #ENDIF
					ENDIF
					IF bStartedTrevorConversation = FALSE
						IF (GET_GAME_TIMER() - iConversationTimer) > 1500
						AND CREATE_CONVERSATION(sLeadInConversation, "TRV5AUD", "TRV5_INT_LI2", CONV_PRIORITY_HIGH)
							bStartedTrevorConversation = TRUE
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Started TRV5_INT_LI2 conversation") #ENDIF
						ENDIF
					ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 0)
						REMOVE_PED_FOR_DIALOGUE(sLeadInConversation, 1)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Finished TRV5_INT_LI2 conversation") #ENDIF
						iLeadInUpdate++
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeadIn)
					AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						REQUEST_MODEL(v_ilev_trev_doorfront)
						IF bDoingExitIdleLoop = FALSE
						AND GET_SYNCHRONIZED_SCENE_PHASE(sceneLeadIn) > 0.98
						AND HAS_MODEL_LOADED(v_ilev_trev_doorfront) // B*1518198
							IF NOT DOES_ENTITY_EXIST(oDoor)
								oDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-1149.71, -1521.09, 10.78>>, 10, v_ilev_trev_doorfront)
							ENDIF
							IF DOES_ENTITY_EXIST(oDoor)
								sceneLeadIn = CREATE_SYNCHRONIZED_SCENE(vLeadInPosition, vLeadInRotation)
								TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneLeadIn, "misstrevor4leadinouttrv_4_int", "leadin_loop_exit_idle_floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
								PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-1149.71, -1521.09, 10.78>>, 5, v_ilev_trev_doorfront, sceneLeadIn, "leadin_loop_exit_idle_front_door", "misstrevor4leadinouttrv_4_int", NORMAL_BLEND_IN)
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneLeadIn, TRUE)
								#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: leadin_action_floyd synched scene ended before TRV5_INT_LI2 conversation") #ENDIF
								bDoingExitIdleLoop = TRUE
							ELSE
								#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: oDoor doesn't exist - iLeadInUpdate = ", iLeadInUpdate) #ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 4
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Trevor4 trigger scene: Waiting to launch mission...") #ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_TREVOR4_AMBIENT_UPDATE()
ENDPROC
