//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Trevor 2 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "Timelapse.sch"
USING "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_TREVOR2_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_TREVOR2_STREAM_OUT_DIST		165.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_TREVOR2_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_TREVOR2_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_TREVOR2_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

structPedsForConversation sPedsForsSpeech
VECTOR sceneT2LeadInPosition = << 1972.956, 3812.079, 32.840 >>
VECTOR sceneT2LeadInRotation = << 0.000, 0.000, 31.000 >>
INT iSceneT2LeadIn
INT sceneT2LeadIn
BOOL bTriggersSpeech = FALSE
BOOL bTriggersSpeechIntro = FALSE
BOOL bRonRelax = FALSE
//BOOL bTriggersSpeechEnd = FALSE
BOOL bT2SafehouseCargenDisabled = FALSE
//structTimelapse sTimelapse

#IF IS_DEBUG_BUILD
	//WIDGET_GROUP_ID wgThisMissionsWidget
#ENDIF	

VECTOR vT2HintCamCoord = <<1972.9822, 3814.1045, 33.9045>>
FLOAT fT2HintCamFollow = 0.35
FLOAT fT2HintCamOffset = -0.78
FLOAT fT2HintCamFove = 30.00

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_TREVOR2_RESET()
	STOP_GAMEPLAY_HINT()
	SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_TREVOR2_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_RON))
	REQUEST_ANIM_DICT("misstrevor2leadinouttrv_2_int")
	REQUEST_WAYPOINT_RECORDING("T2leadin")
	REQUEST_ADDITIONAL_TEXT("SJC", MISSION_TEXT_SLOT)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_TREVOR2_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_RON))
	REMOVE_ANIM_DICT("misstrevor2leadinouttrv_2_int")
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	CLEAR_ADDITIONAL_TEXT(MISSION_TEXT_SLOT, FALSE)
	REMOVE_WAYPOINT_RECORDING("T2leadin")
	
	IF bT2SafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as releasing assets.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_CITY)
		bT2SafehouseCargenDisabled = FALSE
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_TREVOR2_HAVE_ASSETS_LOADED()
	//Used to preload variations on an invisible ped before the scene is created!
	TSPedCompRequest sRonComponents[12]
	TS_STORE_PED_COMP_REQUEST(sRonComponents[0], PED_COMP_HEAD, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[1], PED_COMP_BERD, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[2], PED_COMP_HAIR, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[3], PED_COMP_TORSO, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[4], PED_COMP_LEG, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[5], PED_COMP_HAND, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[6], PED_COMP_FEET, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[7], PED_COMP_TEETH, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[8], PED_COMP_SPECIAL, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[9], PED_COMP_SPECIAL2, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[10], PED_COMP_DECL, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sRonComponents[11], PED_COMP_JBIB, 0, 0)
	TSPedPropRequest sRonProps[2]
	TS_STORE_PED_PROP_REQUEST(sRonProps[0], ANCHOR_HEAD, 0, 0)
	TS_STORE_PED_PROP_REQUEST(sRonProps[0], ANCHOR_EYES, 0, 0)
	
	IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_RON, << 1972.956, 3812.079, 32.840 >>, sRonComponents, sRonProps)
	OR NOT HAS_ANIM_DICT_LOADED("misstrevor2leadinouttrv_2_int")
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("T2leadin")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE:Creates entities for the scene. Runs while the player is in load range of the
///    		scene as soon as all required assets have loaded.
PROC TS_TREVOR2_CREATE()
	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_TREVOR), 3.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_TREVORS_TRAILER, BUILDINGSTATE_DESTROYED) // Trailer trashed
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		ADD_PED_FOR_DIALOGUE(sPedsForsSpeech,3,g_sTriggerSceneAssets.ped[0],"NervousRon")
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(sPedsForsSpeech,2,PLAYER_PED_ID(),"TREVOR")
	
	sceneT2LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneT2LeadInPosition,sceneT2LeadInRotation)

	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneT2LeadIn, "misstrevor2leadinouttrv_2_int", "trv_2_int_ron_idle", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneT2LeadIn,TRUE)
	
	iSceneT2LeadIn = 0
		
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("T2leadin",TRUE)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_2,
												"TREVOR_2_INT",
												CS_NONE,
												CS_NONE,
												CS_ALL)
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_TREVOR_2, "RON", g_sTriggerSceneAssets.ped[0])

	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_2,
												"TREVOR_2_INT",
												CS_NONE,
												CS_NONE,
												CS_ALL)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_TREVOR2_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sPedsForsSpeech, 3)
	
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("t2leadin",FALSE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	STOP_GAMEPLAY_HINT()
	SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
	
	IF bT2SafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as cleaning up trigger scene.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY)
		bT2SafehouseCargenDisabled = FALSE
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_TREVOR2_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	REMOVE_PED_FOR_DIALOGUE(sPedsForsSpeech, 3)

	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("t2leadin",FALSE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	IF bT2SafehouseCargenDisabled
		CPRINTLN(DEBUG_TRIGGER, "Reenabling savehouse cargen as cleaning up trigger scene.")
		CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_TREVOR_SAVEHOUSE_COUNTRY)
		bT2SafehouseCargenDisabled = FALSE
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_TREVOR2_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iSceneT2LeadIn >2
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_TREVOR2_HAS_BEEN_DISRUPTED()
	//Has the player injured or damaged Ron
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			STOP_GAMEPLAY_HINT()
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				STOP_GAMEPLAY_HINT()
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF IS_PED_INJURED(PLAYER_PED_ID())
			STOP_GAMEPLAY_HINT()
			SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_TREVOR2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

FUNC BOOL MANAGE_MY_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT iRonShoutTimer

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_TREVOR2_UPDATE()
	
//	#IF IS_DEBUG_BUILD 
//		//SET_CURRENT_WIDGET_GROUP(wgThisMissionsWidget)
//		wgThisMissionsWidget = START_WIDGET_GROUP("Trevor 2")
//		// Widgets for the Main Mission Controlers
//		START_WIDGET_GROUP(" Lead in Controlers")
//			ADD_WIDGET_VECTOR_SLIDER("vHintCamCoord", vHintCamCoord, -5000, 5000, 0.1)
//			ADD_WIDGET_FLOAT_SLIDER("fHintCamFollow", fHintCamFollow, -100, 100, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("fHintCamOffset", fHintCamOffset, -100, 100, 0.01)
//			ADD_WIDGET_FLOAT_SLIDER("fHintCamFove", fHintCamFove, -100, 100, 0.01)
//			SET_LOCATES_HEADER_WIDGET_GROUP(wgThisMissionsWidget)
//		STOP_WIDGET_GROUP()			
//	#ENDIF

	IF iSceneT2LeadIn < 2
		IF bRonRelax
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1977.694702,3815.276611,33.646553>>, <<1979.087524,3816.118652,30.797792>>, 2.000000)
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1966.745239,3797.720947,30.729702>>, <<1949.704468,3827.021729,35.662704>>, 30.250000)
					AND GET_DISTANCE_BETWEEN_COORDS(<<1973.84045, 3814.07153, 32.45993>>,GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0])) < 15
						sceneT2LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneT2LeadInPosition,sceneT2LeadInRotation)
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneT2LeadIn, "misstrevor2leadinouttrv_2_int", "trv_2_int_ron_idle", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneT2LeadIn,TRUE)
						TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0],PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						bRonRelax = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1966.745239,3797.720947,30.729702>>, <<1949.704468,3827.021729,35.662704>>, 30.250000)
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						sceneT2LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneT2LeadInPosition,sceneT2LeadInRotation)
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneT2LeadIn, "misstrevor2leadinouttrv_2_int", "trv_2_int_ron_idle_slow", SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneT2LeadIn,TRUE)
						TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0],PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
						bRonRelax = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1970.684937,3812.407715,29.383060>>, <<1978.958740,3817.217529,36.794029>>, 5.500000)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
//	VECTOR vHintCamCoord = <<1972.9822, 3814.1045, 34.3045>>
//	FLOAT fHintCamFollow = 0.35
//	FLOAT fHintCamOffset = -0.06
//	FLOAT fHintCamFove = 30.00
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1972.871094,3813.137939,31.174492>>, <<1982.964111,3818.989014,34.557182>>, 3.750000)
		SET_GAMEPLAY_COORD_HINT(vT2HintCamCoord,-1,2000)
		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fT2HintCamFollow)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fT2HintCamOffset)
		SET_GAMEPLAY_HINT_FOV(fT2HintCamFove)
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
	ELSE
		STOP_GAMEPLAY_HINT()
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
	ENDIF

	SWITCH iSceneT2LeadIn
		CASE 0
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1957.900269,3808.964600,30.396786>>, <<1994.085205,3828.923340,36.731987>>, 26.750000)
				TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0],PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
				iSceneT2LeadIn ++
			ENDIF	
		BREAK
		
		CASE 1
			IF bTriggersSpeechIntro = FALSE
				IF CREATE_CONVERSATION(sPedsForsSpeech,"T2AUD","TR2_INT_LI1", CONV_PRIORITY_HIGH)
					bTriggersSpeechIntro = TRUE
					iRonShoutTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF MANAGE_MY_TIMER(iRonShoutTimer,GET_RANDOM_INT_IN_RANGE(7000,12000))
					bTriggersSpeechIntro = FALSE
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1977.694702,3815.276611,33.646553>>, <<1979.087524,3816.118652,30.797792>>, 2.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1971.321289,3812.474121,32.038059>>, <<1977.033447,3815.690918,34.674175>>, 3.000000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1977.357788,3814.966064,31.176128>>, <<1979.914429,3816.569092,34.035728>>, 2.000000)
			AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT)
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<1974.67, 3813.64, 32.43>>,PEDMOVE_WALK)
					SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
					//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					sceneT2LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneT2LeadInPosition,sceneT2LeadInRotation)
					TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneT2LeadIn, "misstrevor2leadinouttrv_2_int", "trv_2_int_ron_action", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
					TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0],PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneT2LeadIn,FALSE)
					iSceneT2LeadIn ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF bTriggersSpeech = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sPedsForsSpeech,"T2AUD","TR2_INT_LI2", CONV_PRIORITY_HIGH)
						bTriggersSpeech = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					iSceneT2LeadIn ++
				ENDIF
			ENDIF
			
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneT2LeadIn) > 0.266
				iSceneT2LeadIn ++
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneT2LeadIn)
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneT2LeadIn) = 1
						sceneT2LeadIn = CREATE_SYNCHRONIZED_SCENE(sceneT2LeadInPosition,sceneT2LeadInRotation)
						TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], sceneT2LeadIn, "misstrevor2leadinouttrv_2_int", "trv_2_int_ron_outro_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneT2LeadIn,FALSE)
						iSceneT2LeadIn ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
			iSceneT2LeadIn ++
		BREAK
		
		CASE 4
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
			//SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
		BREAK
		
	ENDSWITCH
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_TREVOR2_AMBIENT_UPDATE()
ENDPROC
