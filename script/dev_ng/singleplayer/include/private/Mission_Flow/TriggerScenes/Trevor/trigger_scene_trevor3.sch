//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Trevor 3 - Trigger Scene Data							│
//│																				│
//│								Date: 28/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "script_ped.sch"
USING "clearmissionarea_private.sch"


CONST_FLOAT 	TS_TREVOR3_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_TREVOR3_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_TREVOR3_TRIGGER_DIST			8.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_TREVOR3_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_TREVOR3_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

bool bTriggerEnded
int iProgress
bool bOverlappingCheckDone
bool bDidReturnTrue
INTERIOR_INSTANCE_INDEX intTrevor

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_TREVOR3_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
///    
PROC TS_TREVOR3_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("misstrevor3leadinout")
	REQUEST_WAYPOINT_RECORDING("trv3_1")
	request_model(CS_WADE)
	intTrevor = GET_INTERIOR_AT_COORDS(<<1973.1918, 3818.0334, 32.4363>>)
	PIN_INTERIOR_IN_MEMORY(intTrevor)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_TREVOR3_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("misstrevor3leadinout")
	SET_MODEL_AS_NO_LONGER_NEEDED(CS_WADE)
	REMOVE_WAYPOINT_RECORDING("trv3_1")
	UNPIN_INTERIOR(intTrevor)
	REMOVE_IPL("TrevorsTrailerCollision")
	bTriggerEnded=FALSE
	bOverlappingCheckDone = FALSE
	bDidReturnTrue = FALSE
	iProgress=0
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_TREVOR3_HAVE_ASSETS_LOADED()
	IF HAS_ANIM_DICT_LOADED("misstrevor3leadinout")
	AND HAS_MODEL_LOADED(cs_Wade)
	AND GET_IS_WAYPOINT_RECORDING_LOADED("trv3_1")
	AND IS_INTERIOR_READY(intTrevor)
//pin interior
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_TREVOR3_CREATE()
	
	bTriggerEnded=FALSE
	bOverlappingCheckDone = FALSE
	bDidReturnTrue = FALSE
	iProgress=0
	ADD_SCENARIO_BLOCKING_AREA(<<1974.4551, 3814.0129, 32.6770>>+<<7,7,7>>,<<1974.4551, 3814.0129, 32.6770>>-<<7,7,7>>)
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("trv3_1",true,0.7,1)
	REQUEST_IPL("TrevorsTrailerCollision")
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_3,
												"TREVOR_DRIVE_INT",
												CS_NONE,
												CS_NONE,
												CS_ALL)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_TREVOR3_RELEASE()

	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])

	REMOVE_SCENARIO_BLOCKING_AREAS()
	bTriggerEnded=FALSE
	bOverlappingCheckDone = FALSE
	bDidReturnTrue = FALSE
	iProgress=0
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_TREVOR3_DELETE()

	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	bTriggerEnded=FALSE
	iProgress=0
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.

FUNC BOOL TS_TREVOR3_HAS_BEEN_TRIGGERED()

	IF bTriggerEnded
		cprintln(debug_Trevor3,"trigger returns true time: ",get_game_timer())
		RETURN TRUE
	ENDIF
	/*
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1979.970093,3815.533203,30.495016>>, <<1979.006958,3817.099365,34.345322>>, 1.000000)
			bTriggerGood = TRUE
		//	RETURN TRUE
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1973.613037,3813.825928,31.548973>>, <<1977.184082,3815.832764,34.674088>>, 3.187500)
			bTriggerBad = TRUE
		//	RETURN TRUE
		ENDIF

	ENDIF*/
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_TREVOR3_HAS_BEEN_DISRUPTED()
	
	IF GET_NUMBER_OF_FIRES_IN_RANGE(<<1975.318726,3814.919678,32.436695>> ,2.0625) > 0
	OR GET_NUMBER_OF_FIRES_IN_RANGE(<<1971.836670,3811.964111,33.324936>>  ,2.0625) > 0		
	OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,<<1973.4990, 3815.5928, 32.5645>>,2)
	OR (DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0]) AND IS_PED_INJURED(g_sTriggerSceneAssets.ped[0]))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_TREVOR3_IS_BLOCKED()
	

	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.


FUNC BOOL check_player_vehicle_blocking_wade_jump()
	cprintln(debug_Trevor3,"check_player_vehicle_blocking_wade_jump()")
	IF NOT bOverlappingCheckDone		
		IF CHECK_AREA_NEAR_TO_PED_FREE_OF_CARS(g_sTriggerSceneAssets.ped[0],<<1970.069214,3813.558105,30.769392>>, <<1971.171143,3811.620361,33.250465>>, 1.000000)
			cprintln(debug_trevor3,"Vehicle not blocking wade")
			bDidReturnTrue = FALSE
		ELSE
			cprintln(debug_trevor3,"Vehicle blocking wade")
			bDidReturnTrue = TRUE
		ENDIF
		bOverlappingCheckDone = TRUE				
	ENDIF
	IF bDidReturnTrue
		RETURN TRUE
	ENDIF
	return false
endfunc

int sceneWadeB
bool bMakePLayerWalk
PROC TS_TREVOR3_UPDATE()

	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1980.2024, 3816.3694, 31.3084>>) < 4.875
		//IF NOT bMakePLayerWalk
			bMakePLayerWalk = TRUE
			SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)
		//ENDIF
	ELSE
		IF bMakePLayerWalk
			bMakePLayerWalk = FALSE
			SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),pedmove_run)
		ENDIF
	ENDIF
	
	#if IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(key_j)
			bTriggerEnded = true
		ENDIF
	#endif
	SWITCH iProgress
		CASE 0
			
				IF CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0],CHAR_WADE,<<1973.3170, 3816.8462, 32.4287>>, 181.0048)	
					SET_PED_DEFAULT_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0])
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,1), 0, 0, 0) //(berd)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,3), 4, 0, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,4), 3, 0, 0) //(lowr)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,9), 1, 1, 0) //(task)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,10), 3, 0, 0) //(decl)
					SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
					
					//sceneWade = CREATE_SYNCHRONIZED_SCENE(<< 1973.938, 3815.933, 32.432 >>,<< -0.000, 1.250, 29.350 >>)
					TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[0],"misstrevor3leadinout","trv_drive_ext_leadin_door_loop",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0],TRUE)
					iProgress=2
			 	ENDIF
		
						
			
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1980.745605,3814.702148,30.496077>>, <<1978.319824,3818.516846,34.572739>>, 4.187500)
			AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<<1980.083374,3814.580566,30.490093>>, <<1977.620361,3813.189209,34.421539>>, 1.562500)
	
			//IF bTriggerGood
			//OR bTriggerBad
			 	IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					sceneWadeb = CREATE_SYNCHRONIZED_SCENE(<< 1973.938, 3815.933, 32.432 >>,<< -0.000, 1.250, 29.350 >>)
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],sceneWadeb,"misstrevor3leadinout","trevor_dri_int_leadin_action_wade",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb,0.13)

					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()	
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
					SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(player_ped_id(),<<1972.7274, 3813.0557, 32.4245>>,pedmove_walk)
					
					IF CHECK_AREA_NEAR_TO_PED_FREE_OF_CARS(player_ped_id(),<<1979.121948,3816.031006,31.281706>>, <<1979.839722,3816.418213,32.833157>>, 1.625000)					
						sequence_index aaseq
						OPEN_SEQUENCE_TASK(aaseq)
						IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1976.079468,3812.487305,21.041685>>, <<1980.760376,3814.989746,32.371429>>, 1.625000)
							TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1980.0470, 3816.4116, 31.2845>>,pedmove_walk)
						ENDIF
						TASK_FOLLOW_WAYPOINT_RECORDING(null,"trv3_1",0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE) 
						CLOSE_SEQUENCE_TASK(aaseq)
						TASK_PERFORM_SEQUENCE(player_ped_id(),aaseq)
						CLEAR_SEQUENCE_TASK(aaseq)
						
					ELSE
						SEQUENCE_INDEX aSeq
						OPEN_SEQUENCE_TASK(aSeq)
							TASK_LEAVE_ANY_VEHICLE(null)							
							TASK_LOOK_AT_ENTITY(null,g_sTriggerSceneAssets.ped[0],6000)
							TASK_TURN_PED_TO_FACE_ENTITY(null,g_sTriggerSceneAssets.ped[0],-1)
						CLOSE_SEQUENCE_TASK(aSeq)
						TASK_PERFORM_SEQUENCE(player_ped_id(),aSeq)
						CLEAR_SEQUENCE_TASK(aSeq)
					ENDIF
					
					iProgress=3
			 		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation,2,g_sTriggerSceneAssets.ped[0],"Wade")
					cprintln(debug_trevor3,"Triggered normally")
				ENDIF
			ELIF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1973.095459,3813.914063,31.549591>>, <<1977.103516,3816.117920,35.049633>>, 3.750000)
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					sceneWadeb = CREATE_SYNCHRONIZED_SCENE(<< 1973.938, 3815.933, 32.432 >>,<< -0.000, 1.250, 29.350 >>)
					TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],sceneWadeb,"misstrevor3leadinout","trevor_dri_int_leadin_action_wade",NORMAL_BLEND_IN,INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					//SET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb,0.13)
					cprintln(debug_trevor3,"Triggered abnormally")
					//make player walk
					SET_PLAYER_CONTROL(player_id(),FALSE)
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()	
					iProgress=4
			 		ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation,2,g_sTriggerSceneAssets.ped[0],"Wade")
				ENDIF
			ENDIF
			
			
			
		BREAK
		case 3
			IF GET_ENTITY_HEADING(player_ped_id()) > 94
			AND GET_ENTITY_HEADING(player_ped_id()) < 155
				SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)	
			ENDIF
		
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWadeb)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb) > 0.37
						if CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "TRV3AUD", "TRV3_DRINT", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							iProgress=5
						endif
					ENDIF
				ENDIF
			ENDIF							
		break
		
		case 4
			//same as above without the ped min move ratio
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWadeb)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb) > 0.37
						if CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "TRV3AUD", "TRV3_DRINT", CONV_PRIORITY_MEDIUM,DO_NOT_DISPLAY_SUBTITLES)
							iProgress=5
						endif
					ENDIF
				ENDIF
			ENDIF
		break
		
		CASE 5
			cprintln(debug_trevor3,"should I skip Wade running out?")
			
			IF GET_ENTITY_HEADING(player_ped_id()) > 94
			AND GET_ENTITY_HEADING(player_ped_id()) < 155
				//SET_PED_MIN_MOVE_BLEND_RATIO(player_ped_id(),pedmove_walk)	
			ENDIF
			
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneWadeb)
				cprintln(debug_trevor3,"skip now cos wade anim ended")
				UNPIN_INTERIOR(intTrevor)
				bTriggerEnded = TRUE
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWadeb)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb) >= 0.77								
				AND check_player_vehicle_blocking_wade_jump()
					cprintln(debug_trevor3,"skip now cos true and car overlapping")
					UNPIN_INTERIOR(intTrevor)
					bTriggerEnded = TRUE
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWadeb) >= 0.99
					cprintln(debug_trevor3,"skip now cos wade anim > 0.99")
					UNPIN_INTERIOR(intTrevor)
					bTriggerEnded = TRUE
				ENDIF
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<1972.7274, 3813.0557, 32.4245>>,false) < 1.3
				cprintln(debug_trevor3,"skip now cos player close to coord")
				UNPIN_INTERIOR(intTrevor)
				bTriggerEnded = TRUE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_TREVOR3_AMBIENT_UPDATE()
ENDPROC
