//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Trevor 1 - Trigger Scene Data						    │
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								ped[0]	- 	Michael                             │
//│								ped[1]	- 	Trevor                              │
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_TREVOR1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_TREVOR1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_TREVOR1_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_TREVOR1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE		//Distance friends will bail from player group.
CONST_INT		TS_TREVOR1_FRIEND_ACCEPT_BITS		BIT_NOBODY	//Friends who can trigger the mission with the player.

INT				couchT1Scene
INT 			couchT1ActionScene

INT				iT1LeadStage

VECTOR			vT1ScenePos = << -803.523, 171.887, 72.321 >>
VECTOR			vT1SceneRot = << -0.000, 0.000, -59.240 >>

VECTOR vT1HintCamCoord = <<-803.8107, 172.2809, 73.0846>>
FLOAT fT1HintCamFollow = 0.35
FLOAT fT1HintCamOffset = -0.02
FLOAT fT1HintCamFove = 30.00

//dialogue
structPedsForConversation 	convo_struct_t1

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_TREVOR1_RESET()
	iT1LeadStage 			= 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_TREVOR1_REQUEST_ASSETS()
	REQUEST_MODEL(PLAYER_ZERO)
	REQUEST_MODEL(PLAYER_TWO)
	REQUEST_MODEL(PROP_TUMBLER_01_EMPTY)
	REQUEST_ANIM_DICT("missheist_jewelleadinoutjh_endscene")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_TREVOR1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TUMBLER_01_EMPTY)
	REMOVE_ANIM_DICT("missheist_jewelleadinoutjh_endscene")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_TREVOR1_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(PLAYER_ZERO)
	AND HAS_MODEL_LOADED(PLAYER_TWO)
	AND HAS_ANIM_DICT_LOADED("missheist_jewelleadinoutjh_endscene")
	AND HAS_MODEL_LOADED(PROP_TUMBLER_01_EMPTY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_TREVOR1_CREATE()
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_MICHAEL_CS, TRUE)
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_MICHAEL_BH, TRUE)
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_MICHAEL_PRO, TRUE)
	SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB,DOORSTATE_UNLOCKED)

	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1],CHAR_TREVOR,<<1975.3729, 3818.3162, 32.4363>>, DEFAULT, DEFAULT, TRUE)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 23, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_FEET, 10, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL, 14, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_JBIB, 0, 0)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.ped[1],TRUE)
		SET_ENTITY_INVINCIBLE(g_sTriggerSceneAssets.ped[1],TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		//create mike
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0],CHAR_MICHAEL,vT1ScenePos, DEFAULT, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
		
		//create trev
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1],CHAR_TREVOR,<<1975.3729, 3818.3162, 32.4363>>, DEFAULT, DEFAULT, TRUE)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 23, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_FEET, 10, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL, 14, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_JBIB, 0, 0)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.ped[1],TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
		
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_TUMBLER_01_EMPTY,vT1ScenePos)
			ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>)
		ENDIF
		
		couchT1Scene = CREATE_SYNCHRONIZED_SCENE(vT1ScenePos,vT1SceneRot)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],couchT1Scene,"missheist_jewelleadinoutjh_endscene", "loop_mic",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_PLAYER_IMPACT)
		SET_SYNCHRONIZED_SCENE_LOOPED(couchT1Scene,TRUE)
	ENDIF
	
	//Kitchen 4 - end
	//Regular 3, 5 - end
	
	//Start preloading intro cutscene for this mission.
	#IF NOT IS_JAPANESE_BUILD
		MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_1,
													"TREVOR_1_INT",
													CS_1| CS_2| CS_3| CS_5| CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13 ,
													CS_3| CS_5| CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13,
													CS_5 | CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13)
	#ENDIF
	
	#IF IS_JAPANESE_BUILD
		MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_TREVOR_1,
													"Trevor_1_INT_JAP",
													CS_1| CS_2| CS_3| CS_5| CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13,
													CS_3| CS_5| CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13,
													CS_5 | CS_6| CS_7| CS_8| CS_9| CS_10| CS_11| CS_12 | CS_13)
	#ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_TREVOR_1,"Trevor",PLAYER_PED_ID())
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_TREVOR_1,"Michael",PLAYER_PED_ID())
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_TREVOR_1,"Michael",g_sTriggerSceneAssets.ped[0])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_TREVOR_1,"Franklin",PLAYER_PED_ID())
	ENDIF

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_TREVOR1_RELEASE()

	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
		
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_TREVOR1_DELETE()

	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)	
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_TREVOR1_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_DOCKS)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		IF iT1LeadStage = 2

			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)		
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_TREVOR1_HAS_BEEN_DISRUPTED()

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_TREVOR1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_TREVOR1_UPDATE()

	//Keep safehouse doors locked.
	SET_DOOR_STATE(DOORNAME_M_MANSION_BW, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_G1, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_GA_SM, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.102539,175.442062,71.334709>>, <<-807.325073,172.382065,73.834709>>, 12.250000)
		SET_GAMEPLAY_COORD_HINT(vT1HintCamCoord,-1,2000)
		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fT1HintCamFollow)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fT1HintCamOffset)
		SET_GAMEPLAY_HINT_FOV(fT1HintCamFove)
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE) 
	ELSE
		STOP_GAMEPLAY_HINT()
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTSTRING("iT1LeadStage: ")PRINTINT(iT1LeadStage)PRINTNL()
	#ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-820.734192,179.547073,68.112610>>, <<-818.672668,174.172440,74.362816>>, 7.500000)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.228027,178.244812,68.672440>>, <<-801.275330,182.816757,75.355469>>, 7.500000)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
		ENDIF
	ENDIF
	
	SWITCH iT1LeadStage
		CASE 0
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1971.284058,3812.500244,30.847073>>, <<1978.907837,3816.800781,35.188240>>, 3.500000)
					ADD_PED_FOR_DIALOGUE(convo_struct_t1,0,PLAYER_PED_ID(),"TREVOR")
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					iT1LeadStage++
				ENDIF
			ELSE
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-814.801331,179.061569,68.659195>>, <<-818.581970,177.397659,74.977814>>, 3.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-816.228027,178.244812,68.672440>>, <<-801.275330,182.816757,75.355469>>, 7.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.645142,179.609406,70.477386>>, <<-816.496155,176.435730,75.227386>>, 6.000000)
						iT1LeadStage = 2
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-808.709106,178.385315,68.653091>>, <<-807.496338,175.338837,75.594688>>, 3.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.749329,176.768280,69.334709>>, <<-802.039185,181.418320,75.355469>>, 3.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-800.817261,168.554489,69.334709>>, <<-804.960632,177.791168,75.688103>>, 8.750000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.213989,181.683319,70.857773>>, <<-810.235901,179.372910,74.403091>>, 3.750000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.710999,180.372467,70.903084>>, <<-812.651306,178.424606,74.909195>>, 3.500000)
						ADD_PED_FOR_DIALOGUE(convo_struct_t1,0,g_sTriggerSceneAssets.ped[0],"MICHAEL")
						ADD_PED_FOR_DIALOGUE(convo_struct_t1,2,PLAYER_PED_ID(),"FRANKLIN")
						IF IS_SYNCHRONIZED_SCENE_RUNNING(couchT1Scene )
							couchT1ActionScene = CREATE_SYNCHRONIZED_SCENE(vT1ScenePos,vT1SceneRot)
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
								IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
//									FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT)
									SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
									TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-808.4160, 177.4646, 71.6071>>,PEDMOVE_WALK)
									TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0],couchT1ActionScene,"missheist_jewelleadinoutjh_endscene", "action_mic",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT,RBF_PLAYER_IMPACT)
									SET_SYNCHRONIZED_SCENE_RATE(couchT1ActionScene,0.7)
									SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
									iT1LeadStage++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(couchT1ActionScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(couchT1ActionScene) > 0.8
						iT1LeadStage++
					ENDIF
				ENDIF
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				iT1LeadStage++
				//ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
			ENDIF
		
		BREAK
	endswitch
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_TREVOR1_AMBIENT_UPDATE()
ENDPROC
