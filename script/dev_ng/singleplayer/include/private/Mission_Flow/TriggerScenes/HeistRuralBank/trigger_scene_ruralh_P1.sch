//╒═════════════════════════════════════════════════════════════════════╕
//│						Rural Heist Prep - Trigger Scene Data			│
//│																		│
//│								Date: 07/11/12							│
//╞═════════════════════════════════════════════════════════════════════╡
//│																		│
//│				----------------- CONVOY LAYOUT ------------------		│
//│				ped[]													│	
//│													 					│
//│				[----]													│	
//│				|2	3|		Convoy 1 	=	veh[1]		 				│
//│				|4	5|								 					│	
//│				[----]								 					│	
//│													 					│
//│				[~~~~]								 					│	
//│				|0	 | 		Target 		=	veh[0]	 					│
//│				|	 |								 					│	
//│				[~~~~]								 					│	
//│													 					│
//│				[----]								 					│	
//│				|6	7|		Convoy 2 	=	veh[2]	 					│		 
//│				|8	9|								 					│
//│				[----]								 					│	
//│						obj[0] - box 1 obj[1] - box 2 					│
//╘═════════════════════════════════════════════════════════════════════╛


USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "script_clock.sch"

CONST_FLOAT 	TS_RURALH_P1_STREAM_IN_DIST			550.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_RURALH_P1_STREAM_OUT_DIST		580.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_RURALH_P1_TRIGGER_DIST			65.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_RURALH_P1_HELI_TRIGGER_DIST		120.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_RURALH_P1_BATTLE_BUDDY_CALL_DIST	900.0 	// Distance from trigger's blip at which the player can call in battle buddies.		NB: Could be made slightly bigger?
CONST_FLOAT		TS_RURALH_P1_BLIP_SPEED				0.4		// speed the blip travels around the map when the vehicles are not spawned

CONST_FLOAT		TS_RURALH_P1_FRIEND_REJECT_DIST		TS_RURALH_P1_STREAM_OUT_DIST		//Distance friends will bail from player group.
CONST_INT		TS_RURALH_P1_FRIEND_ACCEPT_BITS		BIT_MICHAEL|BIT_TREVOR				//Friends who can trigger the mission with the player.

TS_MOVING_BLIP_STRUCT		TS_RURALH_P1_MOVING_BLIP

BOOL						TS_RURALH_P1_bIsActive 						= FALSE
BOOL						TS_RURALH_P1_bDisplayedBlipAppearHelp		= FALSE
BOOL						TS_RURALH_P1_bDisplayedBlipRemovedHelp		= FALSE

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 		TS_RURALH_P1_DEBUG_WIDGET
#ENDIF



FUNC BOOL TS_RURALH_P1_IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 				
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC
proc TS_RURALH_P1_create_enemy_in_vehicle(int enemy,VEHICLE_INDEX veh)
	
	MODEL_NAMES		model
	VEHICLE_SEAT 	vsSeat
	WEAPON_TYPE		wtEnemyWpn = WEAPONTYPE_CARBINERIFLE
	
	if veh = g_sTriggerSceneAssets.veh[1]
	or veh = g_sTriggerSceneAssets.veh[2]
		model = S_M_Y_MARINE_03
	elif veh = g_sTriggerSceneAssets.veh[0]
		model = S_M_M_MARINE_01
	endif

	if 	enemy = 2 	or enemy = 6 or enemy = 0
		vsSeat 		= VS_DRIVER	
		wtEnemyWpn 	= WEAPONTYPE_PISTOL
		
	elif enemy = 3	or 	enemy = 7 or enemy = 1
		vsSeat 		= VS_FRONT_RIGHT		
		wtEnemyWpn 	= WEAPONTYPE_SMG
		
	elif enemy = 4	or enemy =	8
		vsSeat 		= VS_BACK_LEFT		
		wtEnemyWpn 	= WEAPONTYPE_MICROSMG
		
	elif enemy = 5	or enemy = 9
		vsSeat 		= VS_BACK_RIGHT	
		wtEnemyWpn 	= WEAPONTYPE_CARBINERIFLE	
	endif
	
	g_sTriggerSceneAssets.ped[enemy] = CREATE_PED_INSIDE_VEHICLE(veh,pedtype_mission,model,vsSeat)
		
	SET_PED_AS_ENEMY(g_sTriggerSceneAssets.ped[enemy], TRUE)	
	GIVE_WEAPON_TO_PED(g_sTriggerSceneAssets.ped[enemy], wtEnemyWpn, INFINITE_AMMO, TRUE,true)
	SET_PED_MONEY(g_sTriggerSceneAssets.ped[enemy],0)		
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[enemy],PED_COMP_HAIR,1,0)
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.ped[enemy],250)
	SET_PED_ALERTNESS(g_sTriggerSceneAssets.ped[enemy],AS_ALERT)
	SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[enemy])
//	SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(g_sTriggerSceneAssets.ped[enemy],veh,<<0,0,0>>,13)
	//combat attributes
	SET_PED_TARGET_LOSS_RESPONSE(g_sTriggerSceneAssets.ped[enemy],TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(g_sTriggerSceneAssets.ped[enemy], CA_USE_COVER,true)
	SET_PED_COMBAT_ATTRIBUTES(g_sTriggerSceneAssets.ped[enemy], CA_USE_VEHICLE, false)
	SET_PED_COMBAT_MOVEMENT(g_sTriggerSceneAssets.ped[enemy], CM_DEFENSIVE)
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[enemy],PCF_PreventAutoShuffleToDriversSeat,true)
	set_ped_config_flag(g_sTriggerSceneAssets.ped[enemy],PCF_DontBlipCop,true)
	SET_PED_CONFIG_FLAG(g_sTriggerSceneAssets.ped[enemy], PCF_DontBlip, true)
	STOP_PED_SPEAKING(g_sTriggerSceneAssets.ped[enemy],true)	
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.ped[enemy],TRUE)		
	SET_PED_ACCURACY(g_sTriggerSceneAssets.ped[enemy],10)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[enemy],true)
	
endproc


/// PURPOSE:
///    Resets the trigger scene, will be called when the flow is reset
PROC TS_RURALH_P1_RESET()
	
	// Reset local flags
	g_RHP_string_WayPoint			= ""
	g_RHP_iTriggerRunCount			= 0	
	g_RHP_tod_NextTriggerTime		= INVALID_TIMEOFDAY
	
	TS_MOVING_BLIP_STRUCT newBlank
	TS_RURALH_P1_MOVING_BLIP				= newBlank
	TS_RURALH_P1_bIsActive					= FALSE
	TS_RURALH_P1_bDisplayedBlipAppearHelp 	= FALSE
	TS_RURALH_P1_bDisplayedBlipRemovedHelp	= FALSE
	
	
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_RURALH_P1_REQUEST_ASSETS()
	REQUEST_MODEL(BARRACKS)
	REQUEST_MODEL(CRUSADER)
	REQUEST_MODEL(S_M_Y_MARINE_03)
	REQUEST_MODEL(S_M_M_MARINE_01)
	request_model(PROP_MIL_CRATE_01)	
	REQUEST_ADDITIONAL_TEXT("RURALP", MISSION_TEXT_SLOT)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_RURALH_P1_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(CRUSADER)
	SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_MARINE_03)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_MARINE_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_MIL_CRATE_01)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_RURALH_P1_HAVE_ASSETS_LOADED()
	if  HAS_MODEL_LOADED(CRUSADER)
	and HAS_MODEL_LOADED(BARRACKS)
	and HAS_MODEL_LOADED(s_M_y_marine_03)
	and HAS_MODEL_LOADED(s_M_M_marine_01)
	and HAS_MODEL_LOADED(PROP_MIL_CRATE_01)
	and not IS_STRING_NULL_OR_EMPTY(g_RHP_string_WayPoint)
	and GET_IS_WAYPOINT_RECORDING_LOADED(g_RHP_string_WayPoint)	
	and HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		return true
	endif
	RETURN false
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_RURALH_P1_CREATE()

	g_sTriggerSceneAssets.id = -1
	g_sTriggerSceneAssets.timer = -1
	
	int iTotalWaypointNodes
	BOOL bNodeFound
	INT iNodes
	WAYPOINT_RECORDING_GET_NUM_POINTS(g_RHP_string_WayPoint,iTotalWaypointNodes)
	VECTOR vThisNodePos
	
//---------------truck--------------
	VECTOR 	vVeh_truck_current_node_pos
	VECTOR 	vVeh_truck_prev_node_pos
	FLOAT 	fSpawnHeading_truck
	
	WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, TS_RURALH_P1_MOVING_BLIP.iTargetNode, vVeh_truck_current_node_pos)		
	WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, CLAMP_INT(TS_RURALH_P1_MOVING_BLIP.iTargetNode - 1,0,iTotalWaypointNodes), vVeh_truck_prev_node_pos)
	fSpawnHeading_truck 			= GET_HEADING_BETWEEN_VECTORS_2D(vVeh_truck_prev_node_pos,vVeh_truck_current_node_pos)	
	
//-----------Leader start--------------
	VECTOR vVeh_Leader_current_node_pos
	VECTOR vVeh_Leader_prev_node_pos
	FLOAT fSpawnHeading_leader
	INT iLeadNode
	
	bNodeFound = FALSE
	
	FOR iNodes = 3 TO 13
		PRINTLN("Counting through nodes ahead of target node")
		WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, CLAMP_INT(TS_RURALH_P1_MOVING_BLIP.iTargetNode + iNodes,0,iTotalWaypointNodes), vThisNodePos)
		PRINTLN("Node ", TS_RURALH_P1_MOVING_BLIP.iTargetNode + iNodes, " is ", VDIST(vVeh_truck_current_node_pos, vThisNodePos), "m away from truck pos")
		IF VDIST2(vThisNodePos, vVeh_truck_current_node_pos) > 225
			PRINTLN("NODE FOUND! Setting node ", TS_RURALH_P1_MOVING_BLIP.iTargetNode + iNodes, " as selected node.")
			vVeh_Leader_current_node_pos = vThisNodePos
			iLeadNode = TS_RURALH_P1_MOVING_BLIP.iTargetNode + iNodes
			bNodeFound = TRUE
			iNodes = 13
		ENDIF
		IF TS_RURALH_P1_MOVING_BLIP.iTargetNode + iNodes >= iTotalWaypointNodes
			PRINTLN("Hit limit, leaving loop")
			iNodes = 13
		ENDIF
	ENDFOR
	
	IF bNodeFound	
		WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, CLAMP_INT(iLeadNode - 1,0,iTotalWaypointNodes), vVeh_Leader_prev_node_pos)
		fSpawnHeading_leader 				= GET_HEADING_BETWEEN_VECTORS_2D(vVeh_Leader_prev_node_pos,vVeh_Leader_current_node_pos)
	ELSE
		vVeh_Leader_current_node_pos		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[1],<<0,20,0>>)	
	ENDIF
	
//---------------tail----------------
	VECTOR 	vVeh_tail_current_node_pos
	VECTOR 	vVeh_tail_prev_node_pos
	FLOAT 	fSpawnHeading_tail
	int 	itailNode
	
	bNodeFound = FALSE
	
	FOR iNodes = 3 TO 13
		PRINTLN("Counting through nodes behind target node")
		WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, CLAMP_INT(TS_RURALH_P1_MOVING_BLIP.iTargetNode - iNodes,0,iTotalWaypointNodes), vThisNodePos)
		PRINTLN("Node ", TS_RURALH_P1_MOVING_BLIP.iTargetNode - iNodes, " is ", VDIST(vVeh_truck_current_node_pos, vThisNodePos), "m away from truck pos")
		IF VDIST2(vThisNodePos, vVeh_truck_current_node_pos) > 225
			PRINTLN("NODE FOUND! Setting node ", TS_RURALH_P1_MOVING_BLIP.iTargetNode - iNodes, " as selected node.")
			vVeh_tail_current_node_pos = vThisNodePos
			itailNode = TS_RURALH_P1_MOVING_BLIP.iTargetNode - iNodes
			bNodeFound = TRUE
			iNodes = 13
		ENDIF
		IF TS_RURALH_P1_MOVING_BLIP.iTargetNode - iNodes <= 0
			iNodes = 13
		ENDIF
	ENDFOR
	
	if bNodeFound
		WAYPOINT_RECORDING_GET_COORD(g_RHP_string_WayPoint, CLAMP_INT(itailNode - 1,0,iTotalWaypointNodes), vVeh_tail_prev_node_pos)
		fSpawnHeading_tail 				= GET_HEADING_BETWEEN_VECTORS_2D(vVeh_tail_prev_node_pos,vVeh_tail_current_node_pos)
	else
		vVeh_tail_current_node_pos 		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<0,-20,0>>)
	endif
	
//--------------------	Create Convoy Vehicles	--------------------------

//---------------------------- Leader ------------------------------------
	CLEAR_AREA(vVeh_Leader_current_node_pos, 60.0, TRUE)
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(CRUSADER,vVeh_Leader_current_node_pos, fSpawnHeading_leader)
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.veh[1],250)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[1],true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[1],true)
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[1])
	SET_VEHICLE_FORWARD_SPEED(g_sTriggerSceneAssets.veh[1],15)
	SET_VEHICLE_PROVIDES_COVER(g_sTriggerSceneAssets.veh[1],true)
	SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(g_sTriggerSceneAssets.veh[1],true)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[1],100,FALSE)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[1], FALSE)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[1], "PS_PREP_VEHICLES_GROUP")
// ------------------------ target truck ----------------------------
	CLEAR_AREA(vVeh_truck_current_node_pos, 60, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(BARRACKS,vVeh_truck_current_node_pos, fSpawnHeading_truck)
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.veh[0],250)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])
	SET_VEHICLE_FORWARD_SPEED(g_sTriggerSceneAssets.veh[0],15)
	SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
	SET_VEHICLE_STRONG(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_ENTITY_HEALTH(g_sTriggerSceneAssets.veh[0])*2))
	SET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[0])*2.5))
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_PETROL_TANK_HEALTH(g_sTriggerSceneAssets.veh[0],(GET_VEHICLE_PETROL_TANK_HEALTH(g_sTriggerSceneAssets.veh[0])*2.5))
	SET_VEHICLE_CAN_LEAK_PETROL(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_CAN_LEAK_OIL(g_sTriggerSceneAssets.veh[0],false)
	SET_VEHICLE_PROVIDES_COVER(g_sTriggerSceneAssets.veh[0],true)	
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[0],true)
	SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(g_sTriggerSceneAssets.veh[0],true)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0],100,false)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[0], FALSE)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[0], "PS_PREP_VEHICLES_GROUP")
//----------------------------  Tail --------------------------------
	CLEAR_AREA(vVeh_tail_current_node_pos, 60.0, TRUE)
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(CRUSADER,vVeh_tail_current_node_pos, fSpawnHeading_tail)
	SET_ENTITY_LOAD_COLLISION_FLAG(g_sTriggerSceneAssets.veh[2],true)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[2],true)
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.veh[2],250)	
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[2])
	SET_VEHICLE_FORWARD_SPEED(g_sTriggerSceneAssets.veh[2],15)
	SET_VEHICLE_PROVIDES_COVER(g_sTriggerSceneAssets.veh[2],true)
	SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(g_sTriggerSceneAssets.veh[2],true)
	SET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[2],100,false)
	SET_MODEL_AS_NO_LONGER_NEEDED(CRUSADER)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[2], FALSE)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[2], "PS_PREP_VEHICLES_GROUP")
//--------------------	Create Convoy peds	--------------------------
	TS_RURALH_P1_create_enemy_in_vehicle(0,g_sTriggerSceneAssets.veh[0])
	TASK_VEHICLE_ESCORT(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0],g_sTriggerSceneAssets.veh[1],VEHICLE_ESCORT_REAR,13,DRIVINGMODE_STOPFORCARS,8, 20, 5)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[0])	
	
	// convoy 1	
	TS_RURALH_P1_create_enemy_in_vehicle(2,g_sTriggerSceneAssets.veh[1])
	TS_RURALH_P1_create_enemy_in_vehicle(3,g_sTriggerSceneAssets.veh[1])
	TS_RURALH_P1_create_enemy_in_vehicle(4,g_sTriggerSceneAssets.veh[1])
	TS_RURALH_P1_create_enemy_in_vehicle(5,g_sTriggerSceneAssets.veh[1])
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.veh[1],
										   g_RHP_string_WayPoint, DRIVINGMODE_STOPFORCARS, 
										   TS_RURALH_P1_MOVING_BLIP.iTargetNode,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,12)	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[2])
	
	// convoy 2	
	TS_RURALH_P1_create_enemy_in_vehicle(6,g_sTriggerSceneAssets.veh[2])
	TS_RURALH_P1_create_enemy_in_vehicle(7,g_sTriggerSceneAssets.veh[2])
	TS_RURALH_P1_create_enemy_in_vehicle(8,g_sTriggerSceneAssets.veh[2])
	TS_RURALH_P1_create_enemy_in_vehicle(9,g_sTriggerSceneAssets.veh[2])	
	TASK_VEHICLE_ESCORT(g_sTriggerSceneAssets.ped[6], g_sTriggerSceneAssets.veh[2],g_sTriggerSceneAssets.veh[0],VEHICLE_ESCORT_REAR,13,DRIVINGMODE_STOPFORCARS, 8, 20, 5)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[6])
//--------------------	Create crate object	--------------------------
	
	g_sTriggerSceneAssets.object[0]  = CREATE_OBJECT(PROP_MIL_CRATE_01,g_GameBlips[STATIC_BLIP_MISSION_RURAL_P1].vCoords[0])
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.object[0],250)
	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.veh[0],0,<<0,-0.5,1.5>>,<<0,0,-90>>)
	g_sTriggerSceneAssets.object[1]  = CREATE_OBJECT(PROP_MIL_CRATE_01,g_GameBlips[STATIC_BLIP_MISSION_RURAL_P1].vCoords[0])
	SET_ENTITY_LOD_DIST(g_sTriggerSceneAssets.object[1],250)
	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[1],g_sTriggerSceneAssets.veh[0],0,<<0,-2.85,1.5>>,<<0,0,-90>>)
	
	TS_RURALH_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_MISSION_RURAL_P1, g_sTriggerSceneAssets.veh[0])

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_RURALH_P1_RELEASE()
	int i
	for i = 0 to 2 
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[i])
	endfor
	for i = 0 to 9 
		TRIGGER_SCENE_RELEASE_PED_NO_ACTION(g_sTriggerSceneAssets.ped[i])
	endfor
	for i = 0 to 1 
		TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[i])
	endfor
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_RURALH_P1_DELETE()

	IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_MISSION_RURAL_P1)
		TS_RURALH_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_RURAL_P1, g_RHP_string_WayPoint, TS_RURALH_P1_BLIP_SPEED, -1)
	ENDIF
		
	int i
	for i = 0 to 2 
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[i])
		ENDIF
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])	
	endfor
	for i = 0 to 9 
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	endfor
	
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_RURALH_P1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_RURAL_P1)
	
	bool btriggerMission

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_BIT_SET(g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
	
		IF NOT IS_AUDIO_SCENE_ACTIVE("PS_PREP_INTERCEPT_CONVOY")
			INT i
			FOR i = 0 TO 2
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
					IF VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[i], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))	< 1000
						START_AUDIO_SCENE("PS_PREP_INTERCEPT_CONVOY")
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			IF g_bTriggerDebugOn
				if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_RURALH_P1_HELI_TRIGGER_DIST)
				else
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_RURALH_P1_TRIGGER_DIST)
				ENDIF
			endif
		#ENDIF
		
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vTriggerPosition)
		if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
			IF fDistanceSquaredFromTrigger < (TS_RURALH_P1_HELI_TRIGGER_DIST*TS_RURALH_P1_HELI_TRIGGER_DIST)			
				bTriggerMission = true
			ENDIF
		else
			IF fDistanceSquaredFromTrigger < (TS_RURALH_P1_TRIGGER_DIST*TS_RURALH_P1_TRIGGER_DIST)
				bTriggerMission = true
			ENDIF
		endif
		int i
		for i = 0 to 2
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
			
				IF g_sTriggerSceneAssets.id = -1
					IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[i], FALSE), 5)
						g_sTriggerSceneAssets.id = i
						g_sTriggerSceneAssets.timer = GET_GAME_TIMER()
						PRINTLN("TRIGGER SCENE STICKYBOMB - sticky bomb near car ", g_sTriggerSceneAssets.id)
					ENDIF
				ENDIF
				
				if IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[i])
					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[i],player_ped_id())
						bTriggerMission = true					
					endif
				else				
					bTriggerMission = true				
				endif	
				
			endif	
		endfor
		for i = 0 to 9
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
				if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[i],player_ped_id())					
						bTriggerMission = true								
					endif
				else
					bTriggerMission = true
				endif	
			endif	
		endfor
		
	ENDIF
		
	IF bTriggerMission
		g_RHP_tod_NextTriggerTime 		= INVALID_TIMEOFDAY	
		TS_RURALH_P1_bIsActive			= FALSE
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_RURALH_P1_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_RURALH_P1_IS_BLOCKED()

	#if IS_DEBUG_BUILD
		IF NOT DOES_WIDGET_GROUP_EXIST(TS_RURALH_P1_DEBUG_WIDGET)
			TS_RURALH_P1_DEBUG_WIDGET = START_WIDGET_GROUP("TS: RHP1")
				ADD_WIDGET_BOOL("TS_RURALH_P1_bIsActive", TS_RURALH_P1_bIsActive)
				ADD_WIDGET_BOOL("TS_RURALH_P1_bDisplayedBlipAppearHelp", TS_RURALH_P1_bDisplayedBlipAppearHelp)
				ADD_WIDGET_BOOL("TS_RURALH_P1_bDisplayedBlipRemovedHelp", TS_RURALH_P1_bDisplayedBlipRemovedHelp)
				ADD_WIDGET_INT_READ_ONLY("g_RHP_iTriggerRunCount", g_RHP_iTriggerRunCount)
				ADD_WIDGET_FLOAT_SLIDER("Blip move speed", TS_RURALH_P1_MOVING_BLIP.fMoveSpeedMulti, 0.1, 200.0, 0.05)
			STOP_WIDGET_GROUP()
		ENDIF
	
		if g_iForcePrepMissionRoute != -1
			TS_RURALH_P1_RESET()
		endif
	#endif
	
	
	IF TS_RURALH_P1_bIsActive
		RETURN FALSE		
	// Not active check if we need to block
	ELIF IS_BIT_SET(g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
		// Initialise time of day stuff
		IF g_RHP_tod_NextTriggerTime = INVALID_TIMEOFDAY
			g_RHP_tod_NextTriggerTime = GET_CURRENT_TIMEOFDAY()		
			ADD_TIME_TO_TIMEOFDAY(g_RHP_tod_NextTriggerTime, 0, 0, 1)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 txtDebugTOD = TIMEOFDAY_TO_TEXT_LABEL(g_RHP_tod_NextTriggerTime)
				CPRINTLN(DEBUG_TRIGGER, "[RURALH_P1] Set up new unblocking TimeOfDay: ", txtDebugTOD)
			#ENDIF
		ENDIF
		
		// See if this trigger scene can no become active
		IF IS_NOW_AFTER_TIMEOFDAY(g_RHP_tod_NextTriggerTime)	
		or g_RHP_iTriggerRunCount = 0
			
			if IS_STRING_NULL_OR_EMPTY(g_RHP_string_WayPoint)
				// Decide on route
				SWITCH g_iForcePrepMissionRoute
					CASE 1
						// Flow has explicitly decided upon route 1
						g_RHP_string_WayPoint = "Rural_prep_trigger1"
					BREAK
					CASE 2
						//... decided upon route 2
						g_RHP_string_WayPoint = "Rural_prep_trigger2"
					BREAK
					DEFAULT
						if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id(), FALSE),<<1274.82654, 1647.32288, 87.20920>>) 
							< GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id(), FALSE),<<527.50574, 4268.42334, 52.26282>>)
							g_RHP_string_WayPoint = "Rural_prep_trigger2"
							g_sTriggerSceneAssets.flag = false
							CPRINTLN(DEBUG_TRIGGER, "[RURALH_P1] Picked trigger route 2 for the next convoy route.")
						else
							g_RHP_string_WayPoint = "Rural_prep_trigger1"
							g_sTriggerSceneAssets.flag = true
							CPRINTLN(DEBUG_TRIGGER, "[RURALH_P1] Picked trigger route 1 for the next convoy route.")
						endif
					BREAK
				ENDSWITCH
				g_iForcePrepMissionRoute = -1	// reset flow global
									
			else
				REQUEST_WAYPOINT_RECORDING(g_RHP_string_WayPoint)
				IF GET_IS_WAYPOINT_RECORDING_LOADED(g_RHP_string_WayPoint)
					// Start the blip moving on the wp recording					
					TS_RURALH_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_RURAL_P1, g_RHP_string_WayPoint, TS_RURALH_P1_BLIP_SPEED, 0)
					TS_RURALH_P1_bIsActive 					= TRUE						
					TS_RURALH_P1_bDisplayedBlipAppearHelp 	= FALSE
					TS_RURALH_P1_bDisplayedBlipRemovedHelp 	= FALSE					
					g_RHP_iTriggerRunCount++
					CPRINTLN(DEBUG_TRIGGER, "[RURALH_P1] Trigger scene has been set to active, using route \"", g_RHP_string_WayPoint, "\"")
					RETURN FALSE
				ENDIF
			endif			
		
		ELSE
		
			IF NOT TS_RURALH_P1_bDisplayedBlipRemovedHelp		// not shown it yet
			AND NOT TS_RURALH_P1_bIsActive						// blip is not active and being shown
				IF g_RHP_iTriggerRunCount <= 2					// only display this message twice
				
					SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_RHP_MISS")
			            CASE FHS_EXPIRED
			                ADD_HELP_TO_FLOW_QUEUE("AM_RHP_MISS", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
			            BREAK
			            CASE FHS_DISPLAYED
							g_txtFlowHelpLastDisplayed = ""
							TS_RURALH_P1_bDisplayedBlipRemovedHelp  = TRUE
			            BREAK
			     	ENDSWITCH
				
				ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
				
					TS_RURALH_P1_bDisplayedBlipRemovedHelp = TRUE
					
				ENDIF
			ENDIF
		
		ENDIF
	else
		g_RHP_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
		TS_RURALH_P1_bIsActive 				= FALSE
	endif
	RETURN TRUE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_RURALH_P1_UPDATE()

	IF TS_RURALH_P1_bIsActive
	
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
		AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		
//			VECTOR vBlipPosition    = GET_BLIP_COORDS(g_GameBlips[TS_RURALH_P1_MOVING_BLIP.blipName].biBlip)
//			VECTOR vVehiclePosition = GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[1])
//			vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 25.0)
//			vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 25.0)
//			vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 25.0)
//			SET_BLIP_COORDS(g_GameBlips[TS_RURALH_P1_MOVING_BLIP.blipName].biBlip, vBlipPosition)
			
			if not TS_UPDATE_MOVING_BLIP(TS_RURALH_P1_MOVING_BLIP)			
				CPRINTLN(DEBUG_TRIGGER, "[TS_RURAL_P] Convoy entities have reached end of route.")
			endif

		ELSE
			PRINTLN("g_sTriggerSceneAssets.veh[1] is not driveable")
		ENDIF
		
		int i
		for i = 0 to 9
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])				
				if not IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])	
//					if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.ped[i])
//						RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.ped[i])
//					ENDIF
				//	REMOVE_COP_BLIP_FROM_PED(g_sTriggerSceneAssets.ped[i])
				endif	
			endif	
		endfor
		
		if TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.ped[0])
		and TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.ped[2])
		and TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.ped[6])
		and TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.veh[0])
		and TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.veh[1])
		and TS_RURALH_P1_IsEntityAlive(g_sTriggerSceneAssets.veh[2])
		
			SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(g_sTriggerSceneAssets.veh[0],true)
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(g_sTriggerSceneAssets.veh[0],true)
			SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(g_sTriggerSceneAssets.veh[0],true)
			SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(g_sTriggerSceneAssets.veh[1],true)
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(g_sTriggerSceneAssets.veh[1],true)
			SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(g_sTriggerSceneAssets.veh[1],true)
			SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(g_sTriggerSceneAssets.veh[2],true)
			SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(g_sTriggerSceneAssets.veh[2],true)
			SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(g_sTriggerSceneAssets.veh[2],true)
			
			if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.veh[0])
				RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[0])
			ENDIF
			if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.veh[1])
				RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[1])
			ENDIF
			if HAS_COLLISION_LOADED_AROUND_ENTITY(g_sTriggerSceneAssets.veh[2])
				RESET_ENTITY_ALPHA(g_sTriggerSceneAssets.veh[2])
			ENDIF
			if IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(g_sTriggerSceneAssets.veh[1])
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0], FALSE),get_entity_coords(g_sTriggerSceneAssets.veh[1], FALSE)) > 25	
				or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[2], FALSE),get_entity_coords(g_sTriggerSceneAssets.veh[1], FALSE)) > 65
					VEHICLE_WAYPOINT_PLAYBACK_PAUSE(g_sTriggerSceneAssets.veh[1])
				else		
					VEHICLE_WAYPOINT_PLAYBACK_RESUME(g_sTriggerSceneAssets.veh[1])
				endif				
			else	
			
				if not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE( g_sTriggerSceneAssets.veh[1])					
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.veh[1],
										   g_RHP_string_WayPoint, DRIVINGMODE_STOPFORCARS, 
										   TS_RURALH_P1_MOVING_BLIP.iTargetNode,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,15)
				endif
				
				if GET_SCRIPT_TASK_STATUS(g_sTriggerSceneAssets.ped[0],SCRIPT_TASK_VEHICLE_MISSION)<> PERFORMING_TASK					
					TASK_VEHICLE_ESCORT(g_sTriggerSceneAssets.ped[0],g_sTriggerSceneAssets.veh[0],g_sTriggerSceneAssets.veh[1],VEHICLE_ESCORT_REAR,16,DRIVINGMODE_AVOIDCARS,8)
				endif
				if GET_SCRIPT_TASK_STATUS(g_sTriggerSceneAssets.ped[6],SCRIPT_TASK_VEHICLE_MISSION)<> PERFORMING_TASK
					TASK_VEHICLE_ESCORT(g_sTriggerSceneAssets.ped[6],g_sTriggerSceneAssets.veh[2],g_sTriggerSceneAssets.veh[0],VEHICLE_ESCORT_REAR,16,DRIVINGMODE_AVOIDCARS,8)
				endif
			endif
		endif
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_RURALH_P1_AMBIENT_UPDATE()
	IF TS_RURALH_P1_bIsActive
	
		IF NOT TS_RURALH_P1_bDisplayedBlipAppearHelp				// not shown it yet
		
			IF g_RHP_iTriggerRunCount = 1	
			
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_PREP4")
		            CASE FHS_EXPIRED 
		                ADD_HELP_TO_FLOW_QUEUE("AM_H_PREP4", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME,BIT_TREVOR | BIT_MICHAEL)
		            BREAK
		            CASE FHS_DISPLAYED
						g_txtFlowHelpLastDisplayed = ""
						TS_RURALH_P1_bDisplayedBlipAppearHelp 	= TRUE
		            BREAK
		     	ENDSWITCH
				
			ELIF g_RHP_iTriggerRunCount <= 3					// only display this message twice
				
				SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_RHP_REAP")
		            CASE FHS_EXPIRED 
		                ADD_HELP_TO_FLOW_QUEUE("AM_RHP_REAP", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME,BIT_TREVOR | BIT_MICHAEL)
		            BREAK
		            CASE FHS_DISPLAYED
						g_txtFlowHelpLastDisplayed = ""
						TS_RURALH_P1_bDisplayedBlipAppearHelp = TRUE
		            BREAK
		     	ENDSWITCH
			
			ELSE	// mark it as already displayed if above the count as we dont want to display it again after this point
			
				TS_RURALH_P1_bDisplayedBlipAppearHelp = TRUE
				
			ENDIF
		ENDIF
		// When active move the blip along the route
		IF NOT TS_UPDATE_MOVING_BLIP(TS_RURALH_P1_MOVING_BLIP)
		OR NOT IS_BIT_SET(g_sMissionStaticData[SP_HEIST_RURAL_PREP_1].triggerCharBitset, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
			// reset the time of day blocking
			CPRINTLN(DEBUG_TRIGGER, "[RURALH_P1] Blip reached end of route.")
			g_RHP_tod_NextTriggerTime 			= INVALID_TIMEOFDAY
			g_RHP_string_WayPoint				= ""
			TS_RURALH_P1_bIsActive 				= FALSE					
		ENDIF
	ENDIF
ENDPROC
