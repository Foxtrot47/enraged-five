//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Rural Bank Heist 1 - Trigger Scene Data						│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"

CONST_FLOAT 	TS_RURALH1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_RURALH1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
//CONST_FLOAT		TS_RURALH1_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_RURALH1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_RURALH1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

INT iPeeScene

FLOAT fPaletoSetupHintFov = 30.0
FLOAT fPaletoSetupHintFollow = 0.460
FLOAT fPaletoSetupHintPitchOrbit = 0.000
FLOAT fPaletoSetupHintSide = -0.02
FLOAT fPaletoSetupHintVert = 0.100
FLOAT fPaletoHintTime

BOOL bGreetingDone
BOOL bScenarioBlocked

#IF IS_DEBUG_BUILD

	WIDGET_GROUP_ID wgtFocusPush
	BOOL bStopTriggerFiring = FALSE
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_PALETO_SETUP_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(wgtFocusPush)
			DELETE_WIDGET_GROUP(wgtFocusPush)
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Sets up getaway vehicle widget
	PROC SET_UP_PALETO_SETUP_RAG_WIDGETS()
		CLEANUP_PALETO_SETUP_WIDGETS()
		wgtFocusPush = START_WIDGET_GROUP("Paleto setup trigger scene")
			START_WIDGET_GROUP("Hint cam tweaker")
				ADD_WIDGET_BOOL("Stop trigger firing ", bStopTriggerFiring)
				ADD_WIDGET_FLOAT_SLIDER("fPaletoSetupHintFov", fPaletoSetupHintFov, 1, 180, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fPaletoSetupHintFollow", fPaletoSetupHintFollow, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fPaletoSetupHintPitchOrbit", fPaletoSetupHintPitchOrbit, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fPaletoSetupHintSide", fPaletoSetupHintSide, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fPaletoSetupHintVert", fPaletoSetupHintVert, -99999, 99999, 0.01)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	ENDPROC
	
#ENDIF

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_RURALH1_RESET()
	bScenarioBlocked = FALSE
ENDPROC

/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_RURALH1_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("missheistpaletoscore1leadinoutrbhs_int_1")
	REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	REQUEST_MODEL(A_M_M_HillBilly_02)
	REQUEST_WAYPOINT_RECORDING("paletoleadin")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_RURALH1_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("missheistpaletoscore1leadinoutrbhs_int_1")
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_HillBilly_02)
	REMOVE_WAYPOINT_RECORDING("paletoleadin")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_RURALH1_HAVE_ASSETS_LOADED()
	RETURN HAS_ANIM_DICT_LOADED("missheistpaletoscore1leadinoutrbhs_int_1")
	AND HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	AND HAS_MODEL_LOADED(A_M_M_HillBilly_02)
	AND GET_IS_WAYPOINT_RECORDING_LOADED("paletoleadin")
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_RURALH1_CREATE()

	#IF IS_DEBUG_BUILD
		SET_UP_PALETO_SETUP_RAG_WIDGETS()
	#ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		//Start preloading intro cutscene for this mission.
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_TREVOR, << 1390.075, 3599.438, 38.975 >>, 149.111, DEFAULT, TRUE)
			iPeeScene = CREATE_SYNCHRONIZED_SCENE(<< 1390.075, 3599.438, 38.975 >>, << -0.000, -0.000, 149.111 >>)
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iPeeScene, "missheistpaletoscore1leadinoutrbhs_int_1", "_leadin_trevor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS )
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
			SET_SYNCHRONIZED_SCENE_LOOPED(iPeeScene, TRUE)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_HillBilly_02, <<1392.3279, 3606.5300, 33.9809>>, 183.1381)
			GIVE_WEAPON_TO_PED(g_sTriggerSceneAssets.ped[1], WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE, FALSE)
			TASK_START_SCENARIO_IN_PLACE(g_sTriggerSceneAssets.ped[1], "WORLD_HUMAN_GUARD_STAND", 0, FALSE)
		ENDIF
	ENDIF
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_RURAL_1,
												"RBHS_INT",
												CS_2|CS_3|CS_4|CS_5|CS_6|CS_7,
												CS_NONE,
												CS_ALL)
												
	bGreetingDone = FALSE
	fPaletoHintTime = 0.0
	
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("paletoleadin", TRUE, 1, 0.8)
	
	//MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_HEIST_RURAL_1, "RBHS_INT", CS_ALL, CS_NONE, CS_ALL)
	//IF NOT IS_CUTSCENE_ACTIVE()
	//	REQUEST_CUTSCENE("rbhs_int")
	//ENDIF
	
ENDPROC

/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_RURALH1_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	bScenarioBlocked = FALSE
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("paletoleadin", FALSE, 1, 0.8)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC

/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_RURALH1_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("paletoleadin", FALSE, 1, 0.8)
	bScenarioBlocked = FALSE
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_RURALH1_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1389.602173,3610.129883,37.969654>>, <<1393.319702,3599.918213,41.059391>>, 7.500000)
		AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		AND IS_PED_FACING_PED(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 100)
		
			IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, 13000)
					SET_GAMEPLAY_HINT_FOV(fPaletoSetupHintFov)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fPaletoSetupHintFollow)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fPaletoSetupHintPitchOrbit)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fPaletoSetupHintSide)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fPaletoSetupHintVert)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				ENDIF
				
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
				
			ELSE
			
				#IF IS_DEBUG_BUILD
					SET_GAMEPLAY_HINT_FOV(fPaletoSetupHintFov)
					SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fPaletoSetupHintFollow)
					SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fPaletoSetupHintPitchOrbit)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fPaletoSetupHintSide)
					SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fPaletoSetupHintVert)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				#ENDIF
				
				STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
				
			ENDIF
			
		ELSE
		
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		
			#IF IS_DEBUG_BUILD
			
				IF bStopTriggerFiring
					RETURN FALSE
				ENDIF
			#ENDIF		
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				fPaletoHintTime += GET_FRAME_TIME()
				PRINTLN("Hint time incrementing fov at: ", GET_GAMEPLAY_CAM_FOV())
			ELSE
				IF fPaletoHintTime != 0.0
					fPaletoHintTime = 0.0
				ENDIF
			ENDIF
			
			IF fPaletoHintTime > 2.0
			AND GET_GAMEPLAY_CAM_FOV() = 30.0
				PRINTLN("Hint time reached 2.5 and fov at 30")
				g_sTriggerSceneAssets.flag = FALSE
				USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("paletoleadin", FALSE, 1, 0.8)
				RETURN TRUE
			ENDIF
				
		ENDIF
		
		IF (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1400.117188,3606.125977,37.967976>>, <<1388.662842,3601.978271,40.838947>>, 7.250000))
		OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1388.757446,3609.822510,37.880840>>, <<1392.488892,3599.615967,40.963093>>, 5.200000))
			#IF IS_DEBUG_BUILD
				IF bStopTriggerFiring
					RETURN FALSE
				ENDIF
			#ENDIF
			g_sTriggerSceneAssets.flag = FALSE
			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("paletoleadin", FALSE, 1, 0.8)
			RETURN TRUE
		ENDIF
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_RURALH1_HAS_BEEN_DISRUPTED()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		OR IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
		OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[0])
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT()
			ENDIF
			REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
			bScenarioBlocked = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_RURALH1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_RURALH1_UPDATE()

	IF NOT bScenarioBlocked
		CLEAR_AREA_OF_PEDS(<< 1389.837, 3600.4905, 38.9419 >>, 3, TRUE)
		CLEAR_AREA_OF_PEDS(<<1391.990, 3606.362, 34.981>>, 3, TRUE)
		g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1393.024536,3611.204834,39.691910>> + <<15.250000,18.750000,4.250000>>, <<1393.024536,3611.204834,39.691910>> - <<16.250000,19.750000,7.250000>>)
		bScenarioBlocked = TRUE
		PRINTLN(" TS_RURALH1_UPDATE Cleared area and blocked scenarios for paleto setup lead in.")
	ENDIF
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iPeeScene)
			FLOAT fHead
			fHead = GET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[0])
			IF fHead > 180
				fHead -= 360
			ELIF fHead <= -180
				fHead += 360
			ENDIF
			IF ABSF(fHead - 149.111) > 10
			OR VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), << 1390.075, 3599.438, 38.975 >>) > 2
				IF iPeeScene <> -1 
					iPeeScene = -1
				ENDIF
				TASK_FOLLOW_NAV_MESH_TO_COORD(g_sTriggerSceneAssets.ped[0], << 1390.075, 3599.438, 38.975 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_STOP_EXACTLY, 149.111)
			ELSE
				iPeeScene = CREATE_SYNCHRONIZED_SCENE(<< 1390.075, 3599.438, 38.975 >>, << -0.000, -0.000, 149.111 >>)
				TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iPeeScene, "missheistpaletoscore1leadinoutrbhs_int_1", "_leadin_trevor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
				SET_SYNCHRONIZED_SCENE_LOOPED(iPeeScene, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bGreetingDone
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			AND NOT IS_PED_FLEEING(g_sTriggerSceneAssets.ped[1])
			AND NOT IS_ENTITY_OCCLUDED(g_sTriggerSceneAssets.ped[1])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1391.367554,3606.879883,33.869949>>, <<1393.204102,3601.868896,36.230907>>, 8.250000)
					PLAY_PED_AMBIENT_SPEECH(g_sTriggerSceneAssets.ped[1], "GENERIC_HI", SPEECH_PARAMS_FORCE_FRONTEND)
					IF IS_AMBIENT_SPEECH_PLAYING(g_sTriggerSceneAssets.ped[1])
						bGreetingDone = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1396.191162,3600.963379,41.038197>>, <<1390.999634,3615.519531,38.051826>>, 13.750000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_RURALH1_AMBIENT_UPDATE()

	IF NOT bScenarioBlocked
		CLEAR_AREA_OF_PEDS(<< 1389.837, 3600.4905, 38.9419 >>, 3, TRUE)
		CLEAR_AREA_OF_PEDS(<<1391.990, 3606.362, 34.981>>, 3, TRUE)
		g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1393.024536,3611.204834,39.691910>> + <<15.250000,18.750000,4.250000>>, <<1393.024536,3611.204834,39.691910>> - <<16.250000,19.750000,7.250000>>)	
		bScenarioBlocked = TRUE
		PRINTLN(" TS_RURALH1_AMBIENT_UPDATE Cleared area and blocked scenarios for paleto setup lead in.")
	ENDIF
	
ENDPROC
