//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Franklin 1 - Trigger Scene Data							│
//│																				│
//│								Date: 21/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								veh[0] = Lamar's van							│
//│								ped[0] = Denise									│
//│								ped[1] = Lamar									│
//│								ped[2] = Chop									│
//│								ped[3] = Franklin								│
//│								object[0] = Prop_CS_Trowel						│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "flow_mission_trigger_public.sch"
USING "locates_public.sch"
USING "friends_public.sch"

CONST_FLOAT TS_FRANKLIN1_STREAM_IN_DIST		215.0	// Distance at which the scene loads and is created. Big distance hides the visible van creation.
CONST_FLOAT	TS_FRANKLIN1_STREAM_OUT_DIST	225.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT TS_FRANKLIN1_CUT_STREAM_IN		90.0
CONST_FLOAT TS_FRANKLIN1_CUT_STREAM_OUT		120.0

CONST_FLOAT	TS_FRANKLIN1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT	TS_FRANKLIN1_FRIEND_ACCEPT_BITS		BIT_FRANKLIN|BIT_TREVOR				//Friends who can trigger the mission with the player.

CONST_INT	BIT_TS_FRANKLIN1_CONVERSATION_STARTED	0
CONST_INT	BIT_TS_FRANKLIN1_LEAVE_VEHICLE_STARTED	1
CONST_INT	BIT_TS_FRANKLIN1_GAMEPLAY_HINT_ACTIVE	2
CONST_INT	BIT_TS_FRANKLIN1_CUTSCENE_LOADING		3

INT iStateBitset

PROC TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(PED_INDEX PedIndex)
	
	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
															 			
			SET_PED_AS_ENEMY(PedIndex, FALSE)													//set ped properties
			SET_PED_CAN_BE_TARGETTED(PedIndex, FALSE)
			SET_PED_SUFFERS_CRITICAL_HITS(PedIndex, FALSE)
			SET_ENTITY_IS_TARGET_PRIORITY(PedIndex, FALSE)
			SET_PED_CAN_RAGDOLL(PedIndex, TRUE)
			SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_MELEE)
			SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
			SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_BULLET_IMPACT)
			SET_PED_DIES_WHEN_INJURED(PedIndex, FALSE)
			SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
			
			SET_PED_CONFIG_FLAG(PedIndex, PCF_CannotBeTargeted, TRUE)							 //ped config flags
			SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(PedIndex, PCF_RunFromFiresAndExplosions, FALSE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, g_sTriggerSceneAssets.relGroup)			 //ped rel group hash
			
		    SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)								 //block events
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FRANKLIN1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FRANKLIN1_REQUEST_ASSETS()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_VEH_MODEL(CHAR_LAMAR))
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_DENISE)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_CS_TROWEL)
			LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missfra1leadinout")
			LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(g_sTriggerSceneAssets.loadQueue, "FKN1AUD", MISSION_DIALOGUE_TEXT_SLOT)
		BREAK
		CASE CHAR_TREVOR
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_VEH_MODEL(CHAR_LAMAR))
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_LAMARDAVIS)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_DENISE)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_CHOP_MODEL())
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PLAYER_ONE)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_CS_TROWEL)
			LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missfra1leadinoutfra_1_int_trevor")
			LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(g_sTriggerSceneAssets.loadQueue, "FKN1AUD", MISSION_DIALOGUE_TEXT_SLOT)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FRANKLIN1_RELEASE_ASSETS()
	SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(g_sTriggerSceneAssets.loadQueue, 1)
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	CLEAR_WEATHER_TYPE_PERSIST()
ENDPROC

/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FRANKLIN1_HAVE_ASSETS_LOADED()

	IF g_sTriggerSceneAssets.loadQueue.iFrameDelay != 4
		SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(g_sTriggerSceneAssets.loadQueue, 4)
	ENDIF

	TSPedCompRequest sDeniseComponents[3]
	TSPedCompRequest sLamarComponents[11]
	TSPedPropRequest sEmptyProps[1]
	
	TS_STORE_PED_COMP_REQUEST(sDeniseComponents[0], PED_COMP_HEAD, 	0, 0)
	TS_STORE_PED_COMP_REQUEST(sDeniseComponents[1], PED_COMP_TORSO, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sDeniseComponents[2], PED_COMP_LEG, 	0, 0)
	sEmptyProps[0].prop = -1

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			IF NOT HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_DENISE, << -12.58, -1446.37, 29.72 >>, sDeniseComponents, sEmptyProps)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[0], 	PED_COMP_HEAD, 		0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[1], 	PED_COMP_BERD, 		0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[2], 	PED_COMP_HAIR, 		0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[3], 	PED_COMP_TORSO, 	2, 1)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[4], 	PED_COMP_LEG, 		5, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[5], 	PED_COMP_HAND,		0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[6], 	PED_COMP_FEET, 		1, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[7], 	PED_COMP_TEETH, 	0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[8], 	PED_COMP_SPECIAL,	0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[9], 	PED_COMP_SPECIAL2, 	0, 0)
			TS_STORE_PED_COMP_REQUEST(sLamarComponents[10], PED_COMP_DECL, 		1, 2)

			IF NOT HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_DENISE, << -12.58, -1446.37, 29.72 >>, sDeniseComponents, sEmptyProps)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[1], CHAR_LAMAR, <<-15.5027, -1445.4158, 29.6478>>, sLamarComponents, sEmptyProps)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[3], CHAR_FRANKLIN, <<-14.2182, -1448.2678, 29.6458>>)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FRANKLIN1_CREATE()

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			CLEAR_AREA(<< -12.73, -1445.93, 29.70 >>, 2.0, TRUE)	//ped position
			CLEAR_AREA(<< -19.05, -1455.47, 29.51 >>, 2.0, TRUE)	//van position
		BREAK
		CASE CHAR_TREVOR
			CLEAR_AREA(<< -13.93, -1446.32, 29.65 >>, 2.0, TRUE)	//ped position
			CLEAR_AREA(<< -19.05, -1455.47, 29.51 >>, 2.0, TRUE)	//van position
		BREAK
	ENDSWITCH

	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	ADD_RELATIONSHIP_GROUP("FRANKLIN_1_TS_RELGROUP", g_sTriggerSceneAssets.relGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, g_sTriggerSceneAssets.relGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, g_sTriggerSceneAssets.relGroup)

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			
			//create Lamar's van
			IF CREATE_NPC_VEHICLE(g_sTriggerSceneAssets.veh[0], CHAR_LAMAR, << -19.1293, -1455.2057, 29.5073 >>, 94.6991)
				SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0])
				SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[0], FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_VEH_MODEL(CHAR_LAMAR))
			ENDIF
	
			//Reveal preloaded Denise
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
			   
				TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[0])
				
				TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_loop_denise",
										<< -11.650, -1446.850, 29.725 >>, << -0.00, 0.00, -2.52 >>, INSTANT_BLEND_IN, -2, -1,
										AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION,
										DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
										
								
				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8, g_sTriggerSceneAssets.ped[0], "DENISE")
			ENDIF
			
			CLEAR_BIT(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)

			//create trowel
			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_TROWEL, <<-12.8990, -1446.5100, 29.6976>>)
											  SET_ENTITY_INVINCIBLE(g_sTriggerSceneAssets.object[0], TRUE)
											  ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0],
																	  GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND),
																	  << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
			
		BREAK
		
		CASE CHAR_TREVOR
		
			SEQUENCE_INDEX SequenceIndex
		
			//create Lamar's van
			IF CREATE_NPC_VEHICLE(g_sTriggerSceneAssets.veh[0], CHAR_LAMAR, << -19.1293, -1455.2057, 29.5073 >>, 94.6991)
				SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0])
				SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED)
				SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[0], FALSE)
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_VEH_MODEL(CHAR_LAMAR))
			ENDIF
			
			//Reveal preloaded Denise
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
						   
				TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[0])
			
			
				CLEAR_SEQUENCE_TASK(SequenceIndex)
				OPEN_SEQUENCE_TASK(SequenceIndex)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_denise",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
											AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_talk_denise",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
											AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(SequenceIndex)
				TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[0], SequenceIndex)
				CLEAR_SEQUENCE_TASK(SequenceIndex)
			ENDIF
			
			//create trowel
			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_TROWEL, <<-12.8990, -1446.5100, 29.6976>>)
											  SET_ENTITY_COORDS_NO_OFFSET(g_sTriggerSceneAssets.object[0], <<-12.8990, -1446.5100, 29.6976>>)
											  SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[0], <<-93.2400, -0.0000, 40.7000>>)
											  FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[0], TRUE)
											  SET_ENTITY_INVINCIBLE(g_sTriggerSceneAssets.object[0], TRUE)
			
			
			//Reveal preloaded Lamar
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[1])

				TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[1])
				
				CLEAR_SEQUENCE_TASK(SequenceIndex)
				OPEN_SEQUENCE_TASK(SequenceIndex)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_lamar",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 7000,
											AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_b_lamar",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
											AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(SequenceIndex)
				TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[1], SequenceIndex)
				CLEAR_SEQUENCE_TASK(SequenceIndex)
			ENDIF
			
			//create Chop
			IF CREATE_CHOP(g_sTriggerSceneAssets.ped[2], <<-15.0506, -1446.0521, 29.6444>>, 215.2747)
				TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[2])							 //block events
				
				TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[2], "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_chop",
									<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, INSTANT_BLEND_IN, -2, -1,
									AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
				
			ENDIF
			
			//Reveal preloaded Franklin
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
				TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[3])
				TS_FRANKLIN1_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[3])

				CLEAR_SEQUENCE_TASK(SequenceIndex)
				OPEN_SEQUENCE_TASK(SequenceIndex)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_franklin",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 9000,
											AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					TASK_PLAY_ANIM_ADVANCED(NULL, "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_b_franklin",
											<< -13.865, -1446.120, 29.625 >>, << -0.000, 0.000, 2.880 >>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,
											AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
					SET_SEQUENCE_TO_REPEAT(SequenceIndex, REPEAT_FOREVER)
				CLOSE_SEQUENCE_TASK(SequenceIndex)
				TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[3], SequenceIndex)
				CLEAR_SEQUENCE_TASK(SequenceIndex)
				
			ENDIF
						
			ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8, g_sTriggerSceneAssets.ped[0], "DENISE")
			
			CLEAR_BIT(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
					
		BREAK
		
		
	ENDSWITCH
	

	//request variations depending on current player ped for the other player ped							
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			//MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FRANKLIN_1, "Franklin", PLAYER_PED_ID())
			
			/* //comment this out for TODO B*1579081
			IF IS_SPECIAL_EDITION_GAME()
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FRANKLIN_1, "Trevor", PLAYER_TWO, OUTFIT_P2_DENIM)
			ELSE
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FRANKLIN_1, "Trevor", PLAYER_TWO, OUTFIT_P2_TSHIRT_CARGOPANTS_3)
			ENDIF
			*/
		BREAK
		CASE CHAR_TREVOR
			//MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FRANKLIN_1, "Trevor", PLAYER_PED_ID())
			/* //comment this out for TODO B*1579081
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FRANKLIN_1, "Franklin", PLAYER_ONE, OUTFIT_P1_HOODIE_AND_JEANS_1)		
			*/
		BREAK
	ENDSWITCH
								
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 15.0)
	
ENDPROC

/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FRANKLIN1_RELEASE()
	INT	i
	FOR i = 0 TO 3
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	ENDIF
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	CLEAR_WEATHER_TYPE_PERSIST()
ENDPROC

/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FRANKLIN1_DELETE()
	INT	i
	FOR i = 0 TO 3
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
		TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	ENDIF
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	CLEAR_WEATHER_TYPE_PERSIST()
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FRANKLIN1_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_FRANKLIN
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
						IF IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
							IF ( IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_action_denise") AND  HAS_ANIM_EVENT_FIRED(g_sTriggerSceneAssets.ped[0], GET_HASH_KEY("START_CS")) )
							OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									#IF IS_DEBUG_BUILD
										IF HAS_ANIM_EVENT_FIRED(g_sTriggerSceneAssets.ped[0], GET_HASH_KEY("START_CS"))
											PRINTLN(GET_THIS_SCRIPT_NAME(), ": Anim event START_CS fired.")
										ENDIF
									#ENDIF
									
									//TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
									TASK_CLEAR_LOOK_AT(g_sTriggerSceneAssets.ped[0])
									SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.0)
									
									TS_FRANKLIN1_RELEASE_ASSETS()
								
									REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
								
									RETURN TRUE
									
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
						IF IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							
									//TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
									TASK_CLEAR_LOOK_AT(g_sTriggerSceneAssets.ped[0])
								
									TS_FRANKLIN1_RELEASE_ASSETS()
								
									REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
								
									RETURN TRUE
									
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FRANKLIN1_HAS_BEEN_DISRUPTED()

	INT i

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
				RETURN TRUE
			ENDIF
		ENDIF
		FOR i = 0 TO 3
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
				OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[i])
				OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[i])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FRANKLIN1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

PROC TS_FRANKLIN1_UPDATE_GAMEPLAY_HINT()

	IF NOT IS_GAMEPLAY_HINT_ACTIVE()
		
		SET_GAMEPLAY_HINT_FOV(35.2)
		SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], << 0.0, 0.0, 0.0>>, TRUE, -1, 2500)
		
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
		
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.0)
		SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.0)

		SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.400)
		SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(-4.000)
		
	ENDIF

ENDPROC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FRANKLIN1_UPDATE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vBlipPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FRANKLIN_1, GET_CURRENT_PLAYER_PED_INT())
	
		//Check for the player getting in range to start loading the intro cutscene.
		IF NOT IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_CUTSCENE_LOADING)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBlipPosition) < (TS_FRANKLIN1_CUT_STREAM_IN * TS_FRANKLIN1_CUT_STREAM_IN) 
				MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FRANKLIN_1, "FRANKLIN_1_INT", CS_NONE, CS_ALL, CS_2|CS_3|CS_4)

				//request variations for Lamar
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_HEAD, 		0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_BERD, 		0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_HAIR, 		0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_TORSO, 		2, 1)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_LEG, 		5, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_HAND,		0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_FEET, 		1, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_TEETH, 		0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_SPECIAL,	 	0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_SPECIAL2, 	0, 0)
				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Lamar", PED_COMP_DECL, 		1, 2)

				MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_1, "Chop", PED_COMP_TORSO, 		0, GET_CHOP_COLLAR_FROM_APP())
				SET_BIT(iStateBitset, BIT_TS_FRANKLIN1_CUTSCENE_LOADING)
			ENDIF
		//Check for the player leaving the range to clean up the intro cutscnee.
		ELSE
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vBlipPosition) > (TS_FRANKLIN1_CUT_STREAM_OUT * TS_FRANKLIN1_CUT_STREAM_OUT) 
				MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
				CLEAR_BIT(iStateBitset, BIT_TS_FRANKLIN1_CUTSCENE_LOADING)
			ENDIF
		ENDIF

		//make player leave vehicle they arrived in
		IF NOT IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_LEAVE_VEHICLE_STARTED)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-15.347873,-1442.364746,29.113031>>, <<-14.261008,-1455.565918,32.441841>>, 14.0)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), 5.0)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						SET_BIT(iStateBitset, BIT_TS_FRANKLIN1_LEAVE_VEHICLE_STARTED)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
		
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<84.513809,-1959.554565,19.626844>>, <<86.773445,-1956.918457,22.749155>>, 3.5)
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -11.650, -1446.850, 29.725 >>) < 3.0
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_STILL)
					ELSE
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
					ENDIF
					
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
					
					SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.5)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[0], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[0], PRF_DisablePotentialBlastReactions, TRUE)
					
					IF IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_GAMEPLAY_HINT_ACTIVE)
						TS_FRANKLIN1_UPDATE_GAMEPLAY_HINT()
					ENDIF
					
					//start reaction animation for Denise
					IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_loop_denise")
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-12.736193,-1456.806763,28.922047>>, <<-14.035276,-1441.361938,33.107735>>, 12.0)
							OR  IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-19.772480,-1442.887817,29.157907>>, <<-19.236265,-1450.450928,32.596985>>, 7.5))
							AND	NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<-12.736193,-1456.806763,28.922047>>, <<-14.035276,-1441.361938,33.107735>>, 12.0)
								
								SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
								
								IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								
									SET_BIT(iStateBitset, BIT_TS_FRANKLIN1_GAMEPLAY_HINT_ACTIVE)
								
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-23.826109,-1454.172729,29.230972>>, <<-5.332073,-1452.456543,32.552048>>, 8.000000)
										
										TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-14.3153, -1449.7394, 29.6177>>, PEDMOVE_WALK,
																	  DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS,
																	  ENAV_NO_STOPPING | ENAV_STOP_EXACTLY, 358.6017)
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
									
									ENDIF
								ENDIF
								
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_WHILE_NOT_IN_FOV)
								TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_action_denise",
														<< -11.650, -1446.850, 29.725 >>, << -0.00, 0.00, -2.52 >>, 2, -2, -1,
														AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS |
														AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME, DEFAULT, DEFAULT, AIK_DISABLE_LEG_IK)
							ENDIF
						ENDIF
					ENDIF
					
					//start dialogue for Denise
					IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_action_denise")
						IF NOT IS_BIT_SET(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
							IF ( GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "missfra1leadinout", "franklin_1_int_leadin_action_denise") >= 0.25 )
							OR HAS_ANIM_EVENT_FIRED(g_sTriggerSceneAssets.ped[0], GET_HASH_KEY("START_AUDIO"))
								#IF IS_DEBUG_BUILD
									IF HAS_ANIM_EVENT_FIRED(g_sTriggerSceneAssets.ped[0], GET_HASH_KEY("START_AUDIO"))
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Anim event START_AUDIO fired.")
									ENDIF
								#ENDIF
								IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "FKN1AUD", "F1_INTLF", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
									//TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
									SET_BIT(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		CASE CHAR_TREVOR
		
			//start reaction animation for Denise
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
				
					SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.5)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[0], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[0], PRF_DisablePotentialBlastReactions, TRUE)
					
					IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_loop_denise")
					OR IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfra1leadinoutfra_1_int_trevor", "_trevor_leadin_talk_denise")
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-13.850606,-1442.329590,29.105371>>, <<-13.056397,-1455.564819,32.445381>>, 12.0)
							AND	NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<-13.850606,-1442.329590,29.105371>>, <<-13.056397,-1455.564819,32.445381>>, 12.0)
								SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
								SET_BIT(iStateBitset, BIT_TS_FRANKLIN1_CONVERSATION_STARTED)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			//start reaction animation for Lamar
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
				
					SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[1], 0.5)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[1], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[1], PRF_DisablePotentialBlastReactions, TRUE)
					
				ENDIF
			ENDIF
			
			//start reaction animation for Chop
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[2])
				
					SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[2], 0.5)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[2], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[2], PRF_DisablePotentialBlastReactions, TRUE)
					
				ENDIF
			ENDIF
			
			//start reaction animation for Franklin
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[3])
				
					SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[3], 0.5)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[3], PRF_ExpandPedCapsuleFromSkeleton, TRUE)
					SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[3], PRF_DisablePotentialBlastReactions, TRUE)
					
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FRANKLIN1_AMBIENT_UPDATE()
ENDPROC
