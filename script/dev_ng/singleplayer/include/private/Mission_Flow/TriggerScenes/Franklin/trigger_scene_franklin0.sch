//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Franklin 0 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FRANKLIN0_STREAM_IN_DIST		110.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FRANKLIN0_STREAM_OUT_DIST	135.0	// Distance at which the scene is deleted and unloaded.	

CONST_FLOAT		TS_FRANKLIN0_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FRANKLIN0_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

CONST_INT		FRANKLIN0_LOADED_NORMAL_CUT			1
CONST_INT		FRANKLIN0_LOADED_SPECIAL_CUT		2
CONST_INT		FRANKLIN0_STREAMING_NORMAL_CUT		3
CONST_INT		FRANKLIN0_STREAMING_SPECIAL_CUT		4
CONST_INT		FRANKLIN0_REQUESTED_VARS			5

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FRANKLIN0_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FRANKLIN0_REQUEST_ASSETS()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FRANKLIN0_RELEASE_ASSETS()
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FRANKLIN0_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FRANKLIN0_CREATE()

	//Start preloading intro cutscene for this mission.
	g_sTriggerSceneAssets.id = 0

	

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FRANKLIN0_RELEASE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FRANKLIN0_DELETE()
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC

PROC SET_FRANKLIN_0_INTRO_VARIATION()
	IF NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_REQUESTED_VARS)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_HEAD, 0, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_BERD, 1, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_HAIR, 2, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_TORSO, 2, 1)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_LEG, 4, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_HAND, 0, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_SPECIAL, 0, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_SPECIAL2, 0, 0)
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Lamar", PED_COMP_FEET, 1, 0)	
		
		// Force Chop to be wearing the correct collar in the cutscene
		INT iCurrentChopCollar
		IF HAS_PLAYER_USED_CHOP_APP()
			iCurrentChopCollar = GET_CHOP_COLLAR_FROM_APP()
		ELSE
			iCurrentChopCollar = 4 
		ENDIF			
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FRANKLIN_0, "Chop", PED_COMP_TORSO, 0, iCurrentChopCollar)	
		SET_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_REQUESTED_VARS)
	ENDIF
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FRANKLIN0_HAS_BEEN_TRIGGERED()
	IF IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-18.72549, -1457.39539, 29.46880>>, <<-8.22405, -1449.73218, 30.20160>>, FALSE, FALSE)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)	
	ENDIF

	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-14.031, -1449.824, 29.573>>, <<-17.235, -1409.843, 28.314>>, 40, FALSE, FALSE)
		IF NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_NORMAL_CUT)
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-14.192642,-1446.691895,30.896154>>, <<7,7,7>>, FALSE, FALSE)
			OR NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_SPECIAL_CUT)
				MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()

				SET_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_NORMAL_CUT)
				CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_SPECIAL_CUT)
				CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_REQUESTED_VARS)
			ENDIF
		ELSE
			MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FRANKLIN_0,
												"FRA_0_INT",
												CS_NONE,
												CS_2|CS_3|CS_4,
												CS_NONE)
												
			SET_FRANKLIN_0_INTRO_VARIATION()
												
			SET_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_NORMAL_CUT)
			CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_SPECIAL_CUT)	
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_SPECIAL_CUT)
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-14.192642,-1446.691895,30.896154>>, <<7,7,7>>, FALSE, FALSE)
			OR NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_NORMAL_CUT)	
				MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()		
				
				CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_NORMAL_CUT)
				SET_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_LOADED_SPECIAL_CUT)
				CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_REQUESTED_VARS)
			ENDIF
		ELSE
			MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FRANKLIN_0,
												"FRA_0_INT",
												CS_NONE,
												CS_1|CS_3|CS_4,
												CS_NONE)
												
			SET_FRANKLIN_0_INTRO_VARIATION()
												
			CLEAR_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_NORMAL_CUT)
			SET_BIT(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_SPECIAL_CUT)	
			
		ENDIF	
	ENDIF


	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-14.192642,-1446.691895,30.896154>>,<<3.000000,4.500000,1.500000>>)
		AND (IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_SPECIAL_CUT) OR IS_BIT_SET(g_sTriggerSceneAssets.id, FRANKLIN0_STREAMING_NORMAL_CUT)) 				
			SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FRANKLIN0_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FRANKLIN0_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FRANKLIN0_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FRANKLIN0_AMBIENT_UPDATE()
ENDPROC
