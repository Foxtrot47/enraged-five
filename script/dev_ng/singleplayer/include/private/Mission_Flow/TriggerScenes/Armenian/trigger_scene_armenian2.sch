//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Armenian 2 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "script_ped.sch"
USING "load_queue_public.sch"

bool employeePropSwitch
bool bPreloadingConversation
bool bPreloadLoaded
bool bForcePlayerFromCar
CONST_FLOAT 	TS_ARM2_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_ARM2_STREAM_OUT_DIST		160.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_ARM2_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_ARM2_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_ARM2_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player. 

//Using a local index for this as this object is grabbed from the world. We don't want the general
//trigger scene routines to try and delete it as that is illegal.
OBJECT_INDEX objEmployeeOfMonth

structPedsForConversation MyLocalPedStruct
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_ARM2_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_ARM2_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, SCHWARZER)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BALLER)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BULLET)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_SIEMONYETARIAN)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_EMPLOYEE_MONTH_02)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missarmenian2leadinoutint_alt2")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_ARM2_RELEASE_ASSETS()
	cprintln(debug_Trevor3,"release 1")
	
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	
	//Release here as well to make sure we always set the world object as no longer needed.
	IF DOES_ENTITY_EXIST(objEmployeeOfMonth)
		SET_ENTITY_VISIBLE(objEmployeeOfMonth, TRUE)
	ENDIF
	TRIGGER_SCENE_RELEASE_OBJECT(objEmployeeOfMonth)
	
	employeePropSwitch = FALSE
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_ARM2_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		cprintln(debug_Trevor3,"trigger assets loaded")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.

PROC TS_ARM2_CREATE()
	cprintln(debug_Trevor3,"Create vehicles")
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_ARMENIAN_2, 
												"ARMENIAN_2_INT",
												CS_NONE,
												CS_ALL,
												CS_NONE)
												
	CLEAR_AREA_OF_VEHICLES(<< -11.3885, -1080.6869, 25.6721 >>,8)
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(BALLER,<< -7.6801, -1082.4365, 25.6721 >>, 159.3786)				
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(SCHWARZER,<< -11.3885, -1080.6869, 25.6721 >>, 160.6359)				
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(BULLET,<< -17.3168, -1079.2955, 25.6721 >>, 204.3207)
	
	CLEAR_AREA_OF_VEHICLES(<<-38, -1104, 26.0534>>,4)
	CLEAR_AREA_OF_VEHICLES(<<-41.4259, -1099.6481, 26.0534>>,4)
	CLEAR_AREA_OF_VEHICLES(<<-46.2594, -1097.8386, 26.3444>>,4)
	g_sTriggerSceneAssets.veh[3] = CREATE_VEHICLE(BALLER,<<-41.4259, -1099.6481, 26.0534>>, 132.3786)
	g_sTriggerSceneAssets.veh[4] = CREATE_VEHICLE(SCHWARZER,<<-46.2594, -1097.8386, 26.3444>>, 113.3786)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[3], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[3], FALSE)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[4], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[4], FALSE)
	
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[0],3,1)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[2],37,37)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[1],52,10)
	
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[3],25,16)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[4],32,17)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-43.781937,-1098.931885,25.859827>>-<<9.750000,6.750000,2.562500>>,<<-43.781937,-1098.931885,25.859827>>+<<9.750000,6.750000,2.562500>>,false)
	
	SET_DOOR_STATE(DOORNAME_ARM2_SIMEON_OFFICE,DOORSTATE_LOCKED)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_ARM2_RELEASE()
	cprintln(debug_Trevor3,"Arm 2 trigger scene release")
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[3])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[4])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	IF DOES_ENTITY_EXIST(objEmployeeOfMonth)
		SET_ENTITY_VISIBLE(objEmployeeOfMonth, TRUE)
	ENDIF
	TRIGGER_SCENE_RELEASE_OBJECT(objEmployeeOfMonth)

	employeePropSwitch = FALSE
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-43.781937,-1098.931885,25.859827>>-<<9.750000,6.750000,2.562500>>,<<-43.781937,-1098.931885,25.859827>>+<<9.750000,6.750000,2.562500>>,true)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_ARM2_DELETE()
	cprintln(debug_Trevor3,"Arm 2 trigger scene delete")
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[3])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[4])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	IF DOES_ENTITY_EXIST(objEmployeeOfMonth)
		SET_ENTITY_VISIBLE(objEmployeeOfMonth, TRUE)
	ENDIF
	TRIGGER_SCENE_RELEASE_OBJECT(objEmployeeOfMonth) //Release instead of delete as this object belongs to the world.
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-43.781937,-1098.931885,25.859827>>-<<9.750000,6.750000,2.562500>>,<<-43.781937,-1098.931885,25.859827>>+<<9.750000,6.750000,2.562500>>,true)
	employeePropSwitch = FALSE
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
int triggerFlagArm2,iSimeonScene



FUNC BOOL TS_ARM2_HAS_BEEN_TRIGGERED()
	
	OBJECT_INDEX objOfficeDoor

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH triggerFlagArm2
			CASE 0
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-31.72,-1101.85,26.57>>, 1.0, v_ilev_fib_door1)
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(),<<-27.335527,-1091.912109,24.936195>>, <<-40.877926,-1087.527954,28.109842>>, 4.062500)		
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()						
						FORCE_PED_MOTION_STATE(player_ped_id(),MS_ON_FOOT_WALK,false,FAUS_DEFAULT,true)
						TASK_FOLLOW_NAV_MESH_TO_COORD(player_ped_id(),<<-34.4845, -1099.3879, 25.4223>>,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_NO_STOPPING)
						triggerFlagArm2++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-31.72,-1101.85,26.57>>, 1.0, v_ilev_fib_door1)
					objOfficeDoor = GET_CLOSEST_OBJECT_OF_TYPE(<<-31.72,-1101.85,26.57>>, 1.0, v_ilev_fib_door1, FALSE)
					IF objOfficeDoor != NULL
						IF DOES_ENTITY_HAVE_DRAWABLE(objOfficeDoor)
//							FORCE_PED_MOTION_STATE(player_ped_id(),pedmove_walk,false,FAUS_DEFAULT,true)
							iSimeonScene = CREATE_SYNCHRONIZED_SCENE(<< -31.828, -1101.772, 26.572 >>,<< -0.000, 0.000, 68.400 >>)
							IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
								CLEAR_PED_TASKS_IMMEDIATELY(g_sTriggerSceneAssets.ped[0]) //otherwise his sync scene will try to exit
								TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iSimeonScene, "missarmenian2leadinoutint_alt2", "arm_2_int_alt2_leadin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							SET_DOOR_STATE(DOORNAME_ARM2_SIMEON_OFFICE,DOORSTATE_UNLOCKED)
							PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-31.72,-1101.85,26.57>>,1.0,v_ilev_fib_door1,iSimeonScene,"arm_2_int_alt2_leadin_door","missarmenian2leadinoutint_alt2",INSTANT_BLEND_IN)
							
							triggerFlagArm2++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSimeonScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSimeonScene) > 0.44				
						cprintln(debug_trevor3,"IS THIS UPDATING 2?")
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							STOP_GAMEPLAY_HINT(true)
							SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0],<<0,0,0.5>>,TRUE, 4500, 2500)
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
							//SET_GAMEPLAY_COORD_HINT(<<-34.6802, -1101.3962, 27.1318>>, -1, 2500)
				            SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.570)
				            SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.055)					                        
				            SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.1)					                        
				            SET_GAMEPLAY_HINT_FOV(30.0)
							
							triggerFlagArm2++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSimeonScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSimeonScene) > 0.5				
					//	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,1,g_sTriggerSceneAssets.ped[0], "SIMEON")
					//	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,0,player_ped_id(),"Franklin")
						BEGIN_PRELOADED_CONVERSATION()
						//if CREATE_CONVERSATION(MyLocalPedStruct, "AR2AUD", "AR2_INTRO_LI", CONV_PRIORITY_MEDIUM)
							triggerFlagArm2++
						//ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSimeonScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSimeonScene) > 0.99
						cprintln(debug_trevor3,"LEad in returns TRUE: ",get_Game_timer())
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF triggerFlagArm2 > 1
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	ENDIF
	
	IF NOT employeePropSwitch		
		IF HAS_MODEL_LOADED(prop_employee_month_02)
			objEmployeeOfMonth = GET_CLOSEST_OBJECT_OF_TYPE(<<-29.13,-1102.377,27.26>>,0.5,Prop_Employee_Month_01)
			IF DOES_ENTITY_EXIST(objEmployeeOfMonth)
				SET_ENTITY_VISIBLE(objEmployeeOfMonth,false)
				SET_ENTITY_AS_MISSION_ENTITY(objEmployeeOfMonth, FALSE, TRUE)
			
				g_sTriggerSceneAssets.object[0] = CREATE_OBJECT_NO_OFFSET(prop_employee_month_02, GET_ENTITY_COORDS(objEmployeeOfMonth))
				SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[0],<<0,0,-20>>)				
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Employee_Month_02)
				employeePropSwitch = TRUE
			ENDIF
		ENDIF
	ENDIF
								
			
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
///    
int iLastHealth
FUNC BOOL TS_ARM2_HAS_BEEN_DISRUPTED()

	int iHealthCar
	
	vehicle_index thisVeh
	

	
	thisVeh = GET_CLOSEST_VEHICLE(<<-36.5853, -1101.4738, 26.3444>>, 5.0, BJXL, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF IS_VEHICLE_DRIVEABLE(thisVeh)
		iHealthCar += GET_ENTITY_HEALTH(thisVeh)						
	ENDIF
	

	thisVeh = GET_CLOSEST_VEHICLE(<<-46.2594, -1097.8386, 26.3444>>, 5.0, SCHWARZER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
	IF IS_VEHICLE_DRIVEABLE(thisVeh)
		iHealthCar += GET_ENTITY_HEALTH(thisVeh)	
	ENDIF
	
	thisVeh = GET_CLOSEST_VEHICLE(<<-50.0800, -1094.4625, 26.0671>>, 5.0, TAILGATER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF IS_VEHICLE_DRIVEABLE(thisVeh)
		iHealthCar += GET_ENTITY_HEALTH(thisVeh)	
	ENDIF
	
	thisVeh = GET_CLOSEST_VEHICLE(<<-37.4128, -1088.5618, 26.0671>>, 5.0, TAILGATER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	IF IS_VEHICLE_DRIVEABLE(thisVeh)
		iHealthCar += GET_ENTITY_HEALTH(thisVeh)	
	ENDIF
	
	thisVeh = GET_CLOSEST_VEHICLE(<<-41.4259, -1099.6481, 26.0534>>, 5.0, BALLER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
	IF IS_VEHICLE_DRIVEABLE(thisVeh)
		iHealthCar += GET_ENTITY_HEALTH(thisVeh)	
	ENDIF

	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-39.627991,-1099.240112,25.252590>>) < 150.5
		IF HAS_BULLET_IMPACTED_IN_AREA(<<-39.627991,-1099.240112,25.252590>> ,26.5)
		
			RETURN TRUE
		ENDIF
		
		//check explosions
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,<<-39.627991,-1099.240112,25.252590>> ,26.5)						
			//cprintln(debug_Trevor3,"fail b")
			RETURN TRUE
		ENDIF
		
		//check damage to vehicles in area
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-39.627991,-1099.240112,25.252590>>) < 26.5								
			IF IS_PED_SHOOTING(player_ped_id())
				WEAPON_TYPE wep
				wep = WEAPONTYPE_UNARMED
				GET_CURRENT_PED_WEAPON(player_ped_id(),wep)
				SWITCH wep
					CASE WEAPONTYPE_BAT
					FALLTHRU
					CASE WEAPONTYPE_CROWBAR
					FALLTHRU
					CASE WEAPONTYPE_HAMMER
					FALLTHRU
					CASE WEAPONTYPE_KNIFE
					FALLTHRU
//										CASE WEAPONTYPE_LASSO
//										FALLTHRU
					CASE WEAPONTYPE_NIGHTSTICK
					FALLTHRU
					CASE WEAPONTYPE_PETROLCAN
					FALLTHRU									
					CASE WEAPONTYPE_STICKYBOMB
					FALLTHRU
					CASE WEAPONTYPE_GRENADE
					FALLTHRU
					CASE WEAPONTYPE_DLC_BOTTLE
					BREAK
					DEFAULT
						//cprintln(debug_Trevor3,"fail c")
						RETURN TRUE
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF	
	ENDIF
	
	If iLastHealth != 0
		IF absi(iLastHealth - iHealthCar) < 200
		AND absi(iLastHealth - iHealthCar) > 10
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	iLastHealth = iHealthCar

	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_ARM2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_ARM2_UPDATE()

	IF NOT bForcePlayerFromCar
		IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<-24.006769,-1086.364258,25.010740>>, <<-31.045536,-1083.776367,28.975454>>, 3.562500)
			IF IS_PED_IN_ANY_VEHICLE(player_ped_id())
				vehicle_index aVeh
				aVeh = GET_VEHICLE_PED_IS_IN(player_ped_id())
				IF IS_VEHICLE_DRIVEABLE(aVeh)
					IF NOT IS_ENTITY_IN_AIR(aVeh)
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(aVeh,2.0)
							TASK_LEAVE_ANY_VEHICLE(player_ped_id())
							bForcePlayerFromCar=TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-33.71, -1096.13, 25.42>>) < 18
	
		IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA() OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-33.71, -1096.13, 25.42>>) < 8
			//shorten distance by 5 metres if in 1st person B* - 2024668
			SET_PED_MAX_MOVE_BLEND_RATIO(player_ped_id(),PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF
		
		IF NOT bPreloadingConversation
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<-33.71, -1096.13, 25.42>>) < 18
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
					KILL_FACE_TO_FACE_CONVERSATION()
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,1,g_sTriggerSceneAssets.ped[0], "SIMEON")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct,0,player_ped_id(),"Franklin")						
					bPreloadingConversation = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT bPreloadLoaded
				IF PRELOAD_CONVERSATION(MyLocalPedStruct, "AR2AUD","AR2_INTRO_LI",CONV_PRIORITY_MEDIUM)
					bPreloadLoaded = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF bPreloadingConversation
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			bPreloadingConversation = FALSE
		ENDIF
			
		IF bPreloadLoaded			
			KILL_FACE_TO_FACE_CONVERSATION()
			bPreloadLoaded = FALSE			
		ENDIF		
	ENDIF

	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		INTERIOR_INSTANCE_INDEX intIndex 
		intIndex = GET_INTERIOR_AT_COORDS(<<-31.6922, -1106.5552, 25.4223>>)	
		IF IS_INTERIOR_READY(intIndex)
			g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION,IG_SIEMONYETARIAN,<<-30.2735, -1103.4025, 25.4223>>, 140.5788 )
			SET_PED_DEFAULT_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0])
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
			SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
			TASK_START_SCENARIO_IN_PLACE(g_sTriggerSceneAssets.ped[0],"WORLD_HUMAN_STAND_MOBILE")
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_ARM2_AMBIENT_UPDATE()
ENDPROC
