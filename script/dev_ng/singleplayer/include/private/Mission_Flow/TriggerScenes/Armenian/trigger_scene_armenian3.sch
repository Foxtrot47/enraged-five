//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Armenian 3 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//│								ped[0]  Simeon									│
//│								ped[1]  Customer								│
//│								veh[0]  Customer Car							│
//│								veh[1]  Outside Car 1							│
//│								veh[2]  Outside Car 2							│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_ARM3_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_ARM3_STREAM_OUT_DIST		160.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_ARM3_TRIGGER_DIST		3.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_ARM3_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_ARM3_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

INT iDealershipDoorL = ENUM_TO_INT(DOORHASH_DEALERSHIP_SIDE_L)
INT iDealershipDoorR = ENUM_TO_INT(DOORHASH_DEALERSHIP_SIDE_R)

INT sceneCarDealLoop, sceneCarDealAction

BOOL bArm3FocusPush
BOOL bArm3TaskWalk
INT iArm3OutOfVehicle

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_ARM3_RESET()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.id = 0
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_ARM3_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missarmenian3leadinoutArmenian_3_int")
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_SIMEON))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, A_M_M_BevHills_02)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PREMIER)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TAILGATER)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BJXL)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_ARM3_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorL)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorL)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorR)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorR)
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_ARM3_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_ARM3_CREATE()
	CLEAR_AREA(<<-47.07522, -1114.47644, 25.43581>>, 7.5, TRUE)
	CLEAR_AREA(<<-40.14164, -1113.71350, 25.43738>>, 5.0, TRUE)
	CLEAR_AREA(<<-57.75611, -1112.76880, 25.43581>>, 10.0, TRUE)
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_ARMENIAN_3) - <<25.0, 25.0, 15.0>>, GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_1) + <<25.0, 25.0, 15.0>>)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, FALSE)
	
	sceneCarDealLoop = CREATE_SYNCHRONIZED_SCENE(<<-43.6345, -1110.6049, 25.9582>>, <<0.0, 0.0,  178.9958>>)
	
	SET_SYNCHRONIZED_SCENE_LOOPED(sceneCarDealLoop, TRUE)
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_SIMEON, <<-42.4751, -1110.9316, 25.4343>>, 312.8127)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[0], "SIMEON")
	
	//TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missarmenian3leadinoutArmenian_3_int", "_intro_loop_simeon", <<-40.5449, -1112.3773, 25.9592>>, <<0.0, 0.0, 163.3559>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneCarDealLoop, "missarmenian3leadinoutArmenian_3_int", "_intro_loop_simeon", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(PREMIER, <<-43.6345, -1110.6049, 25.4358>>, 178.9958)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[0], 0.0)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[0], 43, 43)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 10, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 11, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 12, TRUE)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[0], 43, 134)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[0], FALSE)
	
	PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.veh[0], sceneCarDealLoop, "_intro_loop_car", "missarmenian3leadinoutArmenian_3_int", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))	// | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT))
	
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_BevHills_02, <<-39.8746, -1100.5786, 25.4343>>, 139.6499)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 0, 0)
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, g_sTriggerSceneAssets.ped[1], "MrKenneth")
	
	SET_PED_INTO_VEHICLE(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.veh[0])
	TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[1], "missarmenian3leadinoutArmenian_3_int", "_intro_loop_customer", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)	//| AF_SECONDARY
	
	//TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[1], "missarmenian3leadinoutArmenian_3_int", "_intro_loop_customer", <<-40.5449, -1112.3773, 25.9592>>, <<0.0, 0.0, 163.3559>>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_LOOPING)
	//TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneCarDealLoop, "missarmenian3leadinoutArmenian_3_int", "_intro_loop_customer", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
	
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(TAILGATER, <<-57.1048, -1105.8546, 25.4364>> + (<<4.32, -1.55, 0.00>> * 0.0), 190.0401)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[1], 0.0)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[1], 1, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[1], 2, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[1], 3, TRUE)
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(TAILGATER, <<-57.1048, -1105.8546, 25.4364>> + (<<4.32, -1.55, 0.00>> * 1.0), 190.0401)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[2], 0.0)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[2], 1, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[2], 2, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[2], 3, TRUE)
	
	INTERIOR_INSTANCE_INDEX intGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-60.3707, -1098.9924, 25.4262>>, "v_carshowroom")
	
	g_sTriggerSceneAssets.veh[3] = CREATE_VEHICLE(BJXL, <<-36.6411, -1102.1914, 25.4223>>, 154.2468)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[3], 0.0)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[3], 39, 39)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[3], 10, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[3], 11, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[3], 12, TRUE)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[3], 29, 134)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[3], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[3], FALSE)
	RETAIN_ENTITY_IN_INTERIOR(g_sTriggerSceneAssets.veh[3], intGarage)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[3], TRUE)
	g_sTriggerSceneAssets.veh[4] = CREATE_VEHICLE(TAILGATER, <<-41.7113, -1100.0415, 25.4223>>, 138.7067)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[4], 0.0)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[4], 68, 68)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[4], 1, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[4], 2, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[4], 3, TRUE)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[4], 68, 134)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[4], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[4], FALSE)
	RETAIN_ENTITY_IN_INTERIOR(g_sTriggerSceneAssets.veh[4], intGarage)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[4], TRUE)
	g_sTriggerSceneAssets.veh[5] = CREATE_VEHICLE(BJXL, <<-46.3951, -1097.7783, 25.4223>>, 108.3411)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[5], 0.0)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[5], 6, 3)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[5], 10, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[5], 11, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[5], 12, TRUE)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[5], 10, 134)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[5], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[5], FALSE)
	RETAIN_ENTITY_IN_INTERIOR(g_sTriggerSceneAssets.veh[5], intGarage)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[5], TRUE)
	g_sTriggerSceneAssets.veh[6] = CREATE_VEHICLE(TAILGATER, <<-50.0989, -1094.5341, 25.4223>>, 88.9621)
	SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[6], 0.0)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[6], 42, 42)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[6], 1, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[6], 2, TRUE)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[6], 3, TRUE)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[6], 42, 134)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[6], VEHICLELOCK_CANNOT_ENTER)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[6], FALSE)
	RETAIN_ENTITY_IN_INTERIOR(g_sTriggerSceneAssets.veh[6], intGarage)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_sTriggerSceneAssets.veh[6], TRUE)
	
	REMOVE_ANIM_DICT("missarmenian3leadinoutArmenian_3_int")
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_SIMEON)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BevHills_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(PREMIER)
	SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
	SET_MODEL_AS_NO_LONGER_NEEDED(BJXL)
	
	SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_SHUTTERS, BUILDINGSTATE_DESTROYED)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_ARMENIAN_3,
												"ARMENIAN_3_INT",
												CS_NONE,
												CS_2,
												CS_NONE)
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_ARMENIAN_3, "Siemon", g_sTriggerSceneAssets.ped[0])
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_ARMENIAN_3, "customer", g_sTriggerSceneAssets.ped[1])
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_ARM3_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[3])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[4])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[5])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[6])
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorL)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorL)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorR)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorR)
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	STOP_GAMEPLAY_HINT(FALSE)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_ARM3_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[3])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[4])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[5])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[6])
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorL)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorL)
	ENDIF
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorR)
		REMOVE_DOOR_FROM_SYSTEM(iDealershipDoorR)
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-46.890217, -1105.479004, 29.436941>> - <<20.0, 20.0, 5.0>>, <<-46.890217, -1105.479004, 29.436941>> + <<20.0, 20.0, 5.0>>, TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	STOP_GAMEPLAY_HINT(FALSE)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_ARM3_HAS_BEEN_TRIGGERED()
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorL)
		ADD_DOOR_TO_SYSTEM(iDealershipDoorL, V_ILEV_CSR_DOOR_L, <<-38.4641, -1108.4462, 26.7198>>)
	ELSE
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iDealershipDoorL) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoorL) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iDealershipDoorL, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoorL, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDealershipDoorR)
		ADD_DOOR_TO_SYSTEM(iDealershipDoorR, V_ILEV_CSR_DOOR_R, <<-36.6615, -1109.1016, 26.7198>>)
	ELSE
		IF DOOR_SYSTEM_GET_OPEN_RATIO(iDealershipDoorR) <> 0.0
		OR DOOR_SYSTEM_GET_DOOR_STATE(iDealershipDoorR) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
			DOOR_SYSTEM_SET_OPEN_RATIO(iDealershipDoorR, 0.0, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(iDealershipDoorR, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_ARMENIAN_3)
		
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_ARM3_TRIGGER_DIST)
		#ENDIF
		
		//Look for the player locking in when they walk over the mission blip.
		IF NOT g_bPlayerLockedInToTrigger
			IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.200008, -1114.964111, 25.439144>>, <<-60.222214, -1106.471802, 29.185764>>, 10.0)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.200008, -1114.964111, 25.439144>>, <<-61.988548, -1116.239502, 29.183880>>, 9.0))
			AND (NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<-37.200008, -1114.964111, 25.439144>>, <<-60.222214, -1106.471802, 29.185764>>, 10.0)
			AND NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<-37.200008, -1114.964111, 25.439144>>, <<-61.988548, -1116.239502, 29.183880>>, 9.0))
			AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					iArm3OutOfVehicle = -1
				ENDIF
				
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
				
				CLEAR_AREA_OF_PROJECTILES(vTriggerPosition, 100.0)
			ENDIF
		//Player locked in. 
		ELSE
			IF iArm3OutOfVehicle = -1
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iArm3OutOfVehicle = GET_GAME_TIMER()
			ENDIF
			
			IF NOT bArm3FocusPush
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-41.247730, -1124.249756, 25.203350>>, <<-42.225227, -1103.322998, 30.422344>>, 15.0)
				AND NOT (iArm3OutOfVehicle = -1 AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-50.057453, -1107.887939, 25.437035>>, <<-59.550800, -1104.441650, 28.936430>>, 7.0))
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-35.822376, -1111.382080, 25.437346>>, <<-59.892017, -1110.714600, 30.435806>>, 20.0)
					AND iArm3OutOfVehicle != -1 AND (GET_GAME_TIMER() - iArm3OutOfVehicle) > 1500
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<-46.0563, -1113.3688, 25.4358>>, PEDMOVE_WALK, DEFAULT, DEFAULT, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						
						bArm3TaskWalk = TRUE
						
						bArm3FocusPush = TRUE
					ENDIF
				ELSE
					IF iArm3OutOfVehicle = -1 AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-50.057453, -1107.887939, 25.437035>>, <<-59.550800, -1104.441650, 28.936430>>, 7.0)
						bArm3TaskWalk = TRUE
					ENDIF
					
					bArm3FocusPush = TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
				SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[1], PRF_DontCloseVehicleDoor, TRUE)
				SET_PED_RESET_FLAG(g_sTriggerSceneAssets.ped[1], PRF_DisableInVehicleActions, TRUE)
			ENDIF
			
			IF bArm3FocusPush
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-39.264633, -1108.278442, 25.436941>>, <<-42.440727, -1117.464233, 30.434734>>, 9.5)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
				AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
				AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
					IF HAS_THIS_CUTSCENE_LOADED("ARMENIAN_3_INT")
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneCarDealLoop)
							IF IS_SYNCHRONIZED_SCENE_LOOPED(sceneCarDealLoop)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = WAITING_TO_START_TASK
									OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
									OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = DORMANT_TASK
									OR bArm3TaskWalk
										CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "ARM3AUD", "ARM3_INT_LI", CONV_PRIORITY_MEDIUM)
										
										g_sTriggerSceneAssets.id = GET_GAME_TIMER() + 3500
									ELSE
										PLAY_SINGLE_LINE_FROM_CONVERSATION(g_sTriggerSceneAssets.conversation, "ARM3AUD", "ARM3_INT_LI", "ARM3_INT_LI_3", CONV_PRIORITY_MEDIUM)
										
										g_sTriggerSceneAssets.id = -1
									ENDIF
								ELIF GET_GAME_TIMER() > g_sTriggerSceneAssets.id
									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = WAITING_TO_START_TASK
									OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = PERFORMING_TASK
									OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = DORMANT_TASK
										SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
										SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0.0, 0.0, 0.0>>, TRUE, -1, 2500)
										SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.35)
										SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.015)                    
										SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
										SET_GAMEPLAY_HINT_FOV(35.0)
									ENDIF
									
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 10000)
									
									sceneCarDealAction = CREATE_SYNCHRONIZED_SCENE(<<-43.6345, -1110.6049, 25.9582>>, <<0.0, 0.0,  178.9958>>)
									
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneCarDealAction, FALSE)
									SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneCarDealAction, TRUE)
									
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-40.733479, -1123.267700, 24.713160>>, <<-41.518822, -1107.432251, 32.436939>>, 15.0)
										TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneCarDealAction, "missarmenian3leadinoutarmenian_3_int", "_leadin_look_left_simeon", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									ELSE
										TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneCarDealAction, "missarmenian3leadinoutarmenian_3_int", "_leadin_look_right_simeon", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
									ENDIF
									TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[1], "missarmenian3leadinoutArmenian_3_int", "_leadin_customer", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_UPPERBODY)	//| AF_SECONDARY
									PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.veh[0], sceneCarDealAction, "_leadin_car", "missarmenian3leadinoutArmenian_3_int", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))	// | SYNCED_SCENE_VEHICLE_ABORT_ON_LARGE_IMPACT))
									FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.veh[0])
									
									TASK_LOOK_AT_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), 10000)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneCarDealAction)
							IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(sceneCarDealAction)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneCarDealAction) >= 0.35
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								OR (g_sTriggerSceneAssets.id = -1 AND GET_SYNCHRONIZED_SCENE_PHASE(sceneCarDealAction) >= 0.25)
									g_sTriggerSceneAssets.flag = TRUE
									
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF (DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]) - <<0.0, 0.0, 1.0>>, <<2.0, 2.0, 5.0>>))
				OR (NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.200008, -1114.964111, 25.439144>>, <<-62.606720, -1104.861938, 29.311630>>, 16.0)
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-37.200008, -1114.964111, 25.439144>>, <<-63.405399, -1116.214233, 29.180868>>, 13.0))				
					g_sTriggerSceneAssets.flag = TRUE
					
					RETURN TRUE
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_ARM3_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
		
		//Check for scene peds being injured or harmed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[index])
					OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[index])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for scene vehicle being damaged or destroyed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.veh) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
				OR NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(<<-37.200008, -1114.964111, 25.439144>>, <<-60.222214, -1106.471802, 29.185764>>, 10.0)
	OR IS_BULLET_IN_ANGLED_AREA(<<-37.200008, -1114.964111, 25.439144>>, <<-61.988548, -1116.239502, 29.183880>>, 9.0)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-37.200008, -1114.964111, 25.439144>>, <<-60.222214, -1106.471802, 29.185764>>, 10.0)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-37.200008, -1114.964111, 25.439144>>, <<-61.988548, -1116.239502, 29.183880>>, 9.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_ARM3_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_ARM3_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_ARM3_AMBIENT_UPDATE()
ENDPROC
