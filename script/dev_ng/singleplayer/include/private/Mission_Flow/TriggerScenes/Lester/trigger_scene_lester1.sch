//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Lester 1 - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "asset_management_public.sch"
USING "flow_mission_trigger_public.sch"
USING "clearmissionarea.sch"


CONST_FLOAT 	TS_LESTER1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_LESTER1_STREAM_OUT_DIST		65.0	// Distance at which the scene is deleted and unloaded.

CONST_FLOAT		TS_LESTER1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_LESTER1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

BOOL			TS_LESTER1_bDoorCreepComplete
INT				TS_LESTER1_iDoorCreepTimer
INT 			TS_LESTER1_iLeadInStage
FLOAT			TS_LESTER1_fPrevDistToDoor 	= 999999
INT 			TS_LESTER1_iDoorCloseTimer 	= -1
CAMERA_INDEX	TS_LESTER1_cam
SCALEFORM_INDEX	TS_LESTER1_camOverlay

INTERIOR_INSTANCE_INDEX interiorLesterHallway 	= null
INTERIOR_INSTANCE_INDEX interiorLesterBackRoom 	= null

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_LESTER1_RESET()
	
	TS_LESTER1_bDoorCreepComplete	= FALSE
	TS_LESTER1_iLeadInStage 		= 0
	TS_LESTER1_fPrevDistToDoor 		= 999999
	TS_LESTER1_iDoorCloseTimer 		= -1
	TS_LESTER1_iDoorCreepTimer		= -1
	
	
	INTERIOR_INSTANCE_INDEX thisInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
	IF NOT IS_VALID_INTERIOR(thisInterior)
		DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_LESTER_F), 0.0)
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F), DOORSTATE_LOCKED, TRUE, TRUE)
	ENDIF
	
	IF DOES_CAM_EXIST(TS_LESTER1_cam)
		DESTROY_CAM(TS_LESTER1_cam)
	ENDIF
	
	REMOVE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 0.5, PROP_CCTV_CAM_06A)
	
	SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", TRUE, TRUE)

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_LESTER1_REQUEST_ASSETS()
	REQUEST_MODEL(IG_LESTERCREST)
	REQUEST_MODEL(Prop_WheelChair_01_S)
	REQUEST_MODEL(P_CCTV_S)
	REQUEST_ANIM_DICT("MissLester1ALeadInOut")
	REQUEST_SCRIPT_AUDIO_BANK("Lester1A_01")
	SET_HD_AREA(<<1274.42, -1720.42, 54.99>>, 5.0)
	//REQUEST_WAYPOINT_RECORDING("lester1_leadin_int")
	TS_LESTER1_camOverlay = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_LESTER1_RELEASE_ASSETS()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	SET_MODEL_AS_NO_LONGER_NEEDED(IG_LESTERCREST)
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_WheelChair_01_S)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_CCTV_S)
	RELEASE_SCRIPT_AUDIO_BANK()
	REMOVE_ANIM_DICT("MissLester1ALeadInOut")
	REMOVE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 0.5, PROP_CCTV_CAM_06A)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	CLEAR_HD_AREA()
	//REMOVE_WAYPOINT_RECORDING("lester1_leadin_int")
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(TS_LESTER1_camOverlay)
	IF( MISSION_FLOW_GET_RUNNING_MISSION() <> SP_MISSION_LESTER_1 )
		SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_V_LESTERS, TRUE)
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_LESTER1_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(IG_LESTERCREST)
	AND HAS_MODEL_LOADED(Prop_WheelChair_01_S)
	AND HAS_MODEL_LOADED(P_CCTV_S)
	AND HAS_ANIM_DICT_LOADED("MissLester1ALeadInOut")
	AND REQUEST_SCRIPT_AUDIO_BANK("Lester1A_01")
	//AND GET_IS_WAYPOINT_RECORDING_LOADED("lester1_leadin_int")
	AND HAS_SCALEFORM_MOVIE_LOADED(TS_LESTER1_camOverlay)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_LESTER1_CREATE()

	ADD_RELATIONSHIP_GROUP("TS_LES1A", g_sTriggerSceneAssets.relGroup)
	
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8, NULL, "Lester")
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
												
	IF g_bLester1DoorKnocked
		TS_LESTER1_iLeadInStage = 2
	ELSE
		TS_LESTER1_iLeadInStage = 0
	ENDIF
	
	SET_INTERIOR_CAPPED(INTERIOR_V_LESTERS, FALSE)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_LESTER1_RELEASE()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()

	CDEBUG3LN(DEBUG_TRIGGER, "Attempting to set ped [g_sTriggerSceneAssets.ped[0]] as no longer needed with no action.")

	CLEANUP_TRIGGER_SCENE_SELECTOR_PED(g_sTriggerSceneAssets.ped[0])
	
	// Swapped to manual check for #2185522.
	//TRIGGER_SCENE_RELEASE_PED_COWER(g_sTriggerSceneAssets.ped[0])
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		INT iEntityInstanceID
		STRING strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.ped[0], iEntityInstanceID)
		IF NOT IS_STRING_NULL(strEntityScript)
			IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_PED_IN_ANY_VEHICLE(g_sTriggerSceneAssets.ped[0], TRUE)
						IF IS_ENTITY_ATTACHED(g_sTriggerSceneAssets.ped[0])
							DETACH_ENTITY(g_sTriggerSceneAssets.ped[0])
						ENDIF
						SET_ENTITY_COLLISION(g_sTriggerSceneAssets.ped[0], TRUE)
					ENDIF
					CLEAR_PED_SECONDARY_TASK(g_sTriggerSceneAssets.ped[0])
					SET_ENTITY_HAS_GRAVITY(g_sTriggerSceneAssets.ped[0], TRUE)
					FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], FALSE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.ped[0])
			ENDIF
		ENDIF
	ENDIF
	
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
	REMOVE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 0.5, PROP_CCTV_CAM_06A)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_LESTER1_DELETE()
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	REMOVE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 0.5, PROP_CCTV_CAM_06A)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_LESTER1_HAS_BEEN_TRIGGERED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
	AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		IF TS_LESTER1_iLeadInStage > 4			
		
			IF IS_AUDIO_SCENE_ACTIVE("LESTER_1A_ENTER_LESTERS_HOUSE")
				STOP_AUDIO_SCENE("LESTER_1A_ENTER_LESTERS_HOUSE")
			ENDIF
			
			g_bLester1DoorKnocked			= FALSE
			TS_LESTER1_RESET()
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_LESTER1_HAS_BEEN_DISRUPTED()
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		TS_LESTER1_RESET()
		DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F), DOORSTATE_UNLOCKED, TRUE, TRUE)
		g_bLester1DoorKnocked			= FALSE
		g_QuickSaveDisabledByScript 	= FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_LESTER1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_LESTER1_UPDATE()

	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL
	OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1274.4967, -1720.7449, 53.6807>>) < 2.375 // not near the door way
		DISABLE_SELECTOR_THIS_FRAME()
		g_QuickSaveDisabledByScript = TRUE
	ELSE
		g_QuickSaveDisabledByScript = FALSE
	ENDIF

	CDEBUG1LN(DEBUG_MISSION, "[***TS_LESTER1A***] iLeadInStage: ", TS_LESTER1_iLeadInStage)

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	
		FLOAT fDistToDoor = VDIST2(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)), <<1274.45, -1720.51, 53.65>>)
		
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
			IF IS_ENTITY_IN_ANGLED_AREA(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE), <<1274.450806,-1720.513428,53.654980>>, <<1275.782104,-1723.201904,56.267433>>, 3.625000)

				IF fDistToDoor < TS_LESTER1_fPrevDistToDoor
			
					VECTOR vContactNormal = NORMALISE_VECTOR(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)) - <<1274.45, -1720.51, 53.65>>)
					FLOAT fClosingVel = DOT_PRODUCT(-GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)), vContactNormal)
					FLOAT fDotProduct = DOT_PRODUCT(-NORMALISE_VECTOR(GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE))), vContactNormal)
					IF fClosingVel > 0.1
					AND fDotProduct > 0.85
						CPRINTLN(DEBUG_MISSION, "Closing Velocity: ", fClosingVel)
						CPRINTLN(DEBUG_MISSION, "Dot Product: ", fDotProduct)
						CPRINTLN(DEBUG_MISSION, "Distance To Door: ", fDistToDoor)
						CPRINTLN(DEBUG_MISSION, "Prev Dist To Door: ", TS_LESTER1_fPrevDistToDoor)
						BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE), 1.0, 1)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 100)
					ENDIF
				
				ENDIF

			ENDIF
		ENDIF
		
		TS_LESTER1_fPrevDistToDoor = fDistToDoor
	ENDIF
	
	IF TS_LESTER1_iLeadInStage >= 0 AND TS_LESTER1_iLeadInStage <= 2
		IF IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
			IF NOT IS_SCREEN_FADING_OUT()
			AND IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				SET_SYNCHRONIZED_SCENE_RATE(g_sTriggerSceneAssets.id, 0.0)
				
			ELIF IS_SCREEN_FADED_OUT()	
				SET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id, 1.0)
				SET_SYNCHRONIZED_SCENE_RATE(g_sTriggerSceneAssets.id, 1.0)
				TS_LESTER1_iLeadInStage = 2
				
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(TS_LESTER1_cam)
		OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME( PLAYER_ID() )
	ENDIF
	
	IF DOES_CAM_EXIST(TS_LESTER1_cam)
	AND IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
	AND GET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id) >= 0.371
		//OVERLAY_SET_SECURITY_CAM(TRUE, TRUE, 30, 5, TRUE)
		IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
			SET_TIMECYCLE_MODIFIER("scanline_cam")
		ENDIF
		BEGIN_SCALEFORM_MOVIE_METHOD(TS_LESTER1_camOverlay, "SET_TIME")
			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() <= 12
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS() - 12)
			ENDIF
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
			IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() < 12
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_TIMEAM")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_TIMEPM")
			ENDIF
		END_SCALEFORM_MOVIE_METHOD()
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(TS_LESTER1_camOverlay, 255, 255, 255, 255)
	ELSE
		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
			CLEAR_TIMECYCLE_MODIFIER()
		ENDIF
	ENDIF
	
	IF GET_PLAYER_PED_ENUM( PLAYER_PED_ID() ) = CHAR_MICHAEL
	AND IS_ENTITY_AT_COORD( PLAYER_PED_ID(), <<1274.682129,-1720.725586,54.680809>>,<<1.750000,2.312500,1.000000>> )
		SET_PED_MAX_MOVE_BLEND_RATIO( PLAYER_PED_ID(), PEDMOVE_WALK )
	ENDIF

	SWITCH TS_LESTER1_iLeadInStage
		CASE 0
			IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1275.343994,-1722.348145,53.655022>>, <<1274.433105,-1720.447510,55.882668>>, 3.187500)
				
					CREATE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 2.0, PROP_CCTV_CAM_06A, TRUE)
					g_sTriggerSceneAssets.object[1] 	= CREATE_OBJECT(P_CCTV_S, <<1276.50, -1719.98, 56.37>>)
									
					g_sTriggerSceneAssets.id 	= CREATE_SYNCHRONIZED_SCENE(<<1273.895, -1720.816, 53.660>>, <<0,0,27.36>>)
					TS_LESTER1_cam 				= CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
					
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					
					//TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), g_sTriggerSceneAssets.id, "MissLester1ALeadInOut", "lester_1_int_leadin_doorway_michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					TASK_PLAY_ANIM_ADVANCED( PLAYER_PED_ID(), "MissLester1ALeadInOut", "lester_1_int_leadin_doorway_michael", <<1273.895, -1720.816, 53.660>>, <<0,0,27.36>>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET )
					PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[1], g_sTriggerSceneAssets.id, "lester_1_int_leadin_doorway_seccamprop", "MissLester1ALeadInOut", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
					PLAY_SYNCHRONIZED_CAM_ANIM(TS_LESTER1_cam, g_sTriggerSceneAssets.id, "lester_1_int_leadin_doorway_cam", "MissLester1ALeadInOut")
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_sTriggerSceneAssets.id, TRUE)
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					BEGIN_SCALEFORM_MOVIE_METHOD(TS_LESTER1_camOverlay, "SET_DETAILS")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_CAMDETAILS")
					END_SCALEFORM_MOVIE_METHOD()
					
					BEGIN_SCALEFORM_MOVIE_METHOD(TS_LESTER1_camOverlay, "SET_LOCATION")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_CAMLOCALE")
					END_SCALEFORM_MOVIE_METHOD()
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					DISABLE_SELECTOR_THIS_FRAME()
					DISPLAY_HUD(FALSE)
					DISPLAY_RADAR(FALSE)
					
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					ENDIF
					
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<1278.176392,-1729.592163,51.456005>>, <<1274.337280,-1720.473389,56.967484>>, 7.250000, <<1283.7845, -1728.9476, 51.8119>>, 115.1431)
					
					CLEAR_AREA_OF_VEHICLES(<<1275.42, -1721.52, 53.65>>, 5.0)
					CLEAR_AREA_OF_PROJECTILES(<<1275.42, -1721.52, 53.65>>, 5.0)
					STOP_FIRE_IN_RANGE(<<1274.49, -1720.51, 53.70>>, 6.625)
					REMOVE_PARTICLE_FX_IN_RANGE(<<1274.49, -1720.51, 53.70>>, 6.625)
					
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
					
					TS_LESTER1_iLeadInStage++
				
				ENDIF
			ENDIF
		BREAK
		CASE 1
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
			DISABLE_SELECTOR_THIS_FRAME()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
			AND GET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id) >= 0.5	
			AND IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "LS1AAUD", "LES1A_INTL1", CONV_PRIORITY_HIGH)
					TS_LESTER1_iLeadInStage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			DISABLE_SELECTOR_THIS_FRAME()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			
			IF NOT g_sTriggerSceneAssets.flag
				IF IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
					
					FLOAT fDuration
					fDuration = GET_ANIM_DURATION( "MissLester1ALeadInOut", "lester_1_int_leadin_doorway_cam" ) * 1000.0
					
					FLOAT fPhase
					fPhase = GET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id)
					
					FLOAT fCurrentTime
					fCurrentTime = fPhase * fDuration
				
					IF fCurrentTime >= fDuration - 300
						ANIMPOSTFX_PLAY( "CamPushInNeutral", 0, FALSE )
						PLAY_SOUND_FRONTEND( -1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET" )
						g_sTriggerSceneAssets.flag = TRUE
					ENDIF
					
				ENDIF
			ENDIF
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
			OR GET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id) = 1.0
			AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADED_OUT())

				g_sTriggerSceneAssets.ped[0] 		= CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<1275.3646, -1710.7744, 53.7715>>, 333.7887)
				g_sTriggerSceneAssets.object[0] 	= CREATE_OBJECT(Prop_WheelChair_01_S, <<1275.3646, -1710.7744, 53.7715>>)
				TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
				SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[0], ANCHOR_EYES, 0,0)
				
				g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<1277.661, -1713.688, 54.410>>, <<0,0,-151.560>>)
				TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], 			g_sTriggerSceneAssets.id, "MissLester1ALeadInOut", "Lester_1_INT_LeadIn_loop_Lester", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], 	g_sTriggerSceneAssets.id, "Lester_1_INT_LeadIn_loop_wChair", "MissLester1ALeadInOut", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
				SET_SYNCHRONIZED_SCENE_LOOPED(g_sTriggerSceneAssets.id, TRUE)
				
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
				
				IF NOT g_bLester1DoorKnocked
					//SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1274.7963, -1721.3986, 53.6550>>)
					//SET_ENTITY_HEADING(PLAYER_PED_ID(), 321.5812)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(62.7384)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					CLEAR_PED_TASKS( PLAYER_PED_ID() )
					
					//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE, FAUS_CUTSCENE_EXIT)
					FORCE_INSTANT_LEG_IK_SETUP( PLAYER_PED_ID() )
					
					DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
					REMOVE_MODEL_HIDE(<<1276.49, -1720.06, 56.47>>, 2.0, PROP_CCTV_CAM_06A)
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(TS_LESTER1_cam)
					
					TS_LESTER1_bDoorCreepComplete = FALSE
					g_bLester1DoorKnocked = TRUE
				ELSE
					TS_LESTER1_bDoorCreepComplete = TRUE
				ENDIF
				
				PLAY_SOUND_FROM_COORD(-1, "UNLOCK_DOOR", <<1275.72, -1719.97, 54.97>>, "LESTER1A_SOUNDS")

				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8, g_sTriggerSceneAssets.ped[0], "Lester")
				
				SET_AMBIENT_ZONE_LIST_STATE_PERSISTENT("AZL_LESTERS_DOGS", FALSE, TRUE)
				
				//USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("lester1_leadin_int", TRUE, 0.25, 0.8)

				TS_LESTER1_iDoorCreepTimer	= GET_GAME_TIMER() + 750
				TS_LESTER1_iLeadInStage++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			
				IF IS_INTERIOR_SCENE()
					IF NOT IS_AUDIO_SCENE_ACTIVE("LESTER_1A_ENTER_LESTERS_HOUSE")
						START_AUDIO_SCENE("LESTER_1A_ENTER_LESTERS_HOUSE")
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1273.801270,-1717.001465,53.771458>>, <<1272.208130,-1713.674683,56.385838>>, 6.750000)
				AND IS_SAFE_TO_START_CONVERSATION()
				
					IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "LS1AAUD", "LES1A_INTL2", CONV_PRIORITY_HIGH)
						
						g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<1277.661, -1713.688, 54.410>>, <<0,0,-151.560>>)
						
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.id, "MissLester1ALeadInOut", "Lester_1_INT_LeadIn_action_Lester", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.id, "Lester_1_INT_LeadIn_action_wChair", "MissLester1ALeadInOut", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_sTriggerSceneAssets.id, TRUE)
						
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						
						// bug 2040627 - Moved from CREATE proc 
						//Start preloading intro cutscene for this mission.
						MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_LESTER_1,
																	"LESTER_1_INT",
																	CS_ALL,
																	CS_NONE,
																	CS_NONE)

						MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_MISSION_LESTER_1, "Lester", ANCHOR_EYES, 0, 0)
						
						TS_LESTER1_iLeadInStage++
					ENDIF
				ELSE
					IF NOT TS_LESTER1_bDoorCreepComplete
						IF GET_GAME_TIMER() > TS_LESTER1_iDoorCreepTimer
						
							FLOAT fAlpha 
							fAlpha = CLAMP(( GET_GAME_TIMER()-TS_LESTER1_iDoorCreepTimer)/100.0, 0.0, 1.0)
						
							FLOAT fRatio
							fRatio = COSINE_INTERP_FLOAT(0.0, 0.05, fAlpha)
							IF fRatio >= 0.05
								DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_LESTER_F), fRatio)
								DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F), DOORSTATE_UNLOCKED, TRUE, TRUE)
								TS_LESTER1_bDoorCreepComplete = TRUE
							ELSE
								DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_LESTER_F), fRatio)
								DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F), DOORSTATE_LOCKED, TRUE, TRUE)
							ENDIF
						ENDIF
					ELIF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F)) != DOORSTATE_UNLOCKED
						DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_LESTER_F), DOORSTATE_UNLOCKED, TRUE, TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF TS_LESTER1_iDoorCloseTimer = -1
				TS_LESTER1_iDoorCloseTimer = GET_GAME_TIMER()
			ENDIF
			
			FLOAT fDoorRatio
			fDoorRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_LESTER_F))
			IF fDoorRatio != 0.0
			AND DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_LESTER_F)) != DOORSTATE_LOCKED
				FLOAT fAlpha
				fAlpha = CLAMP((GET_GAME_TIMER() - TS_LESTER1_iDoorCloseTimer)/500.0, 0.0, 1.0)
			
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_LESTER_F), LERP_FLOAT(0.0, 1.0, fAlpha))
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_LESTER_F), DOORSTATE_LOCKED, TRUE, TRUE)
			ELSE
				SET_DOOR_STATE(DOORNAME_LESTER_F, DOORSTATE_LOCKED)
			ENDIF

			IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(g_sTriggerSceneAssets.id)
			OR GET_SYNCHRONIZED_SCENE_PHASE(g_sTriggerSceneAssets.id) = 1.0
			OR NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
			AND HAS_CUTSCENE_LOADED()
				
				TS_LESTER1_iLeadInStage++
				
			ELSE
//				IF IS_ANGLE_WITHIN_RANGE_OF_ANGLE_WRAPPED(GET_ENTITY_HEADING(PLAYER_PED_ID()), 3.5432, 120)
//				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1273.779541,-1709.286255,53.763172>>, <<1272.770630,-1707.267090,56.365654>>, 4.937500)
//			
//					SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
//					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
//				ENDIF
			
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_LESTER1_AMBIENT_UPDATE()
	if interiorLesterHallway = NULL
		interiorLesterHallway = GET_INTERIOR_AT_COORDS(<<1273.4475, -1715.7549, 53.7715>>)
	endif
	if interiorLesterBackRoom = NULL
		interiorLesterBackRoom = GET_INTERIOR_AT_COORDS(<<1274.3635, -1708.2041, 53.7715>>)
	endif
             			 
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorLesterHallway
	OR GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interiorLesterBackRoom
	OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1274.4967, -1720.7449, 53.6807>>) < 2.375 // not near the door way
		DISABLE_SELECTOR_THIS_FRAME()
		g_QuickSaveDisabledByScript = TRUE
	ELSE	
		g_QuickSaveDisabledByScript = FALSE
	ENDIF

ENDPROC
