//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 05/07/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  		Flow Mission Blip Scenes							│
//│																				│
//│		DESCRIPTION: Contains all data for linking ambient trigger scenes		│
//│		to GTA5 mission triggers.												│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"

#if NOT USE_SP_DLC
	//Armenian strand headers.
	USING "trigger_scene_armenian2.sch"
	USING "trigger_scene_armenian3.sch"

	//Agency Heist strand headers.
	USING "trigger_scene_agency1.sch"
	USING "trigger_scene_agency_P1.sch"
	USING "trigger_scene_agency3A.sch"
	USING "trigger_scene_agency3B.sch"

	//Assassination strand headers.
	USING "trigger_scene_assassin1.sch"
	USING "trigger_scene_assassin2.sch"
	USING "trigger_scene_assassin3.sch"
	USING "trigger_scene_assassin4.sch"
	USING "trigger_scene_assassin5.sch"

	//Carsteal strand headers.
	USING "trigger_scene_carsteal1.sch"
	USING "trigger_scene_carsteal2.sch"
	USING "trigger_scene_carsteal3.sch"
	USING "trigger_scene_carsteal4.sch"

	//Chinese strand headers.
	USING "trigger_scene_chinese1.sch"
	USING "trigger_scene_chinese2.sch"

	//Docks Heist strand headers.
	USING "trigger_scene_docks_1.sch"
	USING "trigger_scene_docks_P1.sch"
	USING "trigger_scene_docks_2A.sch"
	USING "trigger_scene_docks_2B.sch"
	USING "trigger_scene_docks_P2B.sch"

	//Exile strand headers.
	USING "trigger_scene_exile1.sch"
	USING "trigger_scene_exile2.sch"
	USING "trigger_scene_exile3.sch"

	//Family strand headers.
	USING "trigger_scene_family1.sch"
	USING "trigger_scene_family2.sch"
	USING "trigger_scene_family3.sch"
	USING "trigger_scene_family4.sch"
	USING "trigger_scene_family5.sch"
	USING "trigger_scene_family6.sch"

	//FBI strand headers.
	USING "trigger_scene_fbi1.sch"
	USING "trigger_scene_fbi2.sch"
	USING "trigger_scene_fbi4_intro.sch"
	USING "trigger_scene_fbi4_P1.sch"
	USING "trigger_scene_fbi4_P2.sch"
	USING "trigger_scene_fbi4_P3.sch"
	USING "trigger_scene_fbi4_P4.sch"
	USING "trigger_scene_fbi4_P5.sch"
	USING "trigger_scene_fbi4.sch"
	USING "trigger_scene_fbi5.sch"

	//Finale strand headers.
	USING "trigger_scene_finale_intro.sch"
	USING "trigger_scene_finaleA.sch"
	USING "trigger_scene_finaleB.sch"
	USING "trigger_scene_finaleC1.sch"

	//Finale Heist strand headers.
	USING "trigger_scene_finaleh_PA.sch"
	USING "trigger_scene_finaleh_PB.sch"
	USING "trigger_scene_finaleh_PC.sch"
	USING "trigger_scene_finaleh_PD.sch"
	USING "trigger_scene_finaleh1.sch"
	USING "trigger_scene_finaleh2_intro.sch"
	USING "trigger_scene_finaleh2A.sch"
	USING "trigger_scene_finaleh2B.sch"

	//Franklin strand headers.
	USING "trigger_scene_franklin0.sch"
	USING "trigger_scene_franklin1.sch"
	USING "trigger_scene_franklin2.sch"

	//Jewelry Heist strand headers.
	USING "trigger_scene_jewel1.sch"
	USING "trigger_scene_jewel_P1A.sch"
	USING "trigger_scene_jewel_P2A.sch"
	USING "trigger_scene_jewel_P1B.sch"

	//Lamar strand headers.
	USING "trigger_scene_lamar1.sch"

	//Lester strand headers.
	USING "trigger_scene_lester1.sch"

	//Martin strand headers.
	USING "trigger_scene_martin1.sch"

	//Michael strand headers.
	USING "trigger_scene_michael1.sch"
	USING "trigger_scene_michael2.sch"
	USING "trigger_scene_michael3.sch"
	USING "trigger_scene_michael4.sch"

	//Rural Bank Heist strand headers.
	USING "trigger_scene_ruralh1.sch"
	USING "trigger_scene_ruralh2.sch"
	USING "trigger_scene_ruralh_P1.sch"

	//Solomon strand headers.
	USING "trigger_scene_solomon1.sch"
	USING "trigger_scene_solomon2.sch"
	USING "trigger_scene_solomon3.sch"

	//Shrink strand headers.
	USING "trigger_scene_shrink.sch"

	//Trevor strand headers.
	USING "trigger_scene_trevor1.sch"
	USING "trigger_scene_trevor2.sch"
	USING "trigger_scene_trevor3.sch"
	USING "trigger_scene_trevor4.sch"
#endif

//CLF Trevor strand headers.
#if USE_CLF_DLC
	USING "trigger_scene_CLF_train.sch"	
	USING "trigger_scene_CLF_finale.sch"
	
	USING "trigger_scene_CLF_Space_1.sch"	
	USING "trigger_scene_CLF_Space_2.sch"	
	USING "trigger_scene_CLF_Space_Finale.sch"	
	
	USING "trigger_scene_CLF_RUS_plane.sch"	
	USING "trigger_scene_CLF_RUS_car.sch"	
	USING "trigger_scene_CLF_RUS_satelite.sch"	
	USING "trigger_scene_CLF_RUS_Jetpack.sch"	
	USING "trigger_scene_CLF_RUS_Cloak.sch"	
	USING "trigger_scene_CLF_RUS_Vasily.sch"
	
	USING "trigger_scene_CLF_KOR_SubBase.sch"	
	USING "trigger_scene_CLF_KOR_Protect.sch"	
	USING "trigger_scene_CLF_KOR_5.sch"	
	USING "trigger_scene_CLF_KOR_SATELLITES.sch"	
	USING "trigger_scene_CLF_KOR_Rescue.sch"	
	
	USING "trigger_scene_CLF_IAA_Training.sch"
	USING "trigger_scene_CLF_IAA_Lies.sch"	
	USING "trigger_scene_CLF_IAA_Heli.sch"	
	USING "trigger_scene_CLF_IAA_Drone.sch"	
	USING "trigger_scene_CLF_IAA_RTS.sch"	
	USING "trigger_scene_CLF_IAA_Jetpack.sch"	
	
	USING "trigger_scene_CLF_CAS_setup.sch"	
	USING "trigger_scene_CLF_CAS_prep1.sch"	
	USING "trigger_scene_CLF_CAS_prep2.sch"	
	USING "trigger_scene_CLF_CAS_prep3.sch"	
	USING "trigger_scene_CLF_CAS_heist.sch"
	
	USING "trigger_scene_CLF_MIL_Dam.sch"
	USING "trigger_scene_CLF_MIL_Rocket.sch"
	USING "trigger_scene_CLF_MIL_Planes.sch"
	
	USING "trigger_scene_CLF_ARA_1.sch"
	USING "trigger_scene_CLF_ARA_Defend.sch"	
	USING "trigger_scene_CLF_ARA_Fake.sch"
	USING "trigger_scene_CLF_ARA_Tank.sch"
	
	USING "trigger_scene_CLF_ASS_Gen.sch"
	USING "trigger_scene_CLF_ASS_Sub.sch"
	USING "trigger_scene_CLF_ASS_cblcar.sch"
	USING "trigger_scene_CLF_ASS_heli.sch"
	USING "trigger_scene_CLF_ASS_hunter.sch"
	USING "trigger_scene_CLF_ASS_pol.sch"
	USING "trigger_scene_CLF_ASS_ret.sch"
	USING "trigger_scene_CLF_ASS_skydive.sch"
	USING "trigger_scene_CLF_ASS_vineyard.sch"
	
	USING "trigger_scene_CLF_JET_1.sch"
	USING "trigger_scene_CLF_JET_2.sch"
	USING "trigger_scene_CLF_JET_3.sch"
	USING "trigger_scene_CLF_JET_Repeat.sch"
	
	USING "trigger_scene_CLF_RC_ALEX_Ground.sch"
	USING "trigger_scene_CLF_RC_ALEX_Air.sch"
	USING "trigger_scene_CLF_RC_ALEX_Underwater.sch"
	USING "trigger_scene_CLF_RC_ALEX_Reward.sch"
	
	USING "trigger_scene_CLF_RC_MELODY_Montage.sch"
	USING "trigger_scene_CLF_RC_MELODY_Airport.sch"
	USING "trigger_scene_CLF_RC_MELODY_Wingwalk.sch"
	USING "trigger_scene_CLF_RC_MELODY_Dive.sch"
	
	USING "trigger_scene_CLF_RC_AGNES_Bodyparts.sch"
	USING "trigger_scene_CLF_RC_AGNES_Recapture.sch"
	USING "trigger_scene_CLF_RC_AGNES_Escape.sch"
	
	USING "trigger_scene_CLF_RC_CLAIRE_Hacking.sch"
	USING "trigger_scene_CLF_RC_CLAIRE_Drugs.sch"
	USING "trigger_scene_CLF_RC_CLAIRE_Final.sch"
#endif

#if USE_NRM_DLC
	USING "trigger_NRM_sur_start.sch"
	USING "trigger_NRM_sur_amanda.sch"
	USING "trigger_NRM_sur_tracey.sch"
	USING "trigger_NRM_sur_michael.sch"
	USING "trigger_NRM_sur_home.sch"
	USING "trigger_NRM_sur_jimmy.sch"
	USING "trigger_NRM_sur_party.sch"
	USING "trigger_NRM_sur_cure.sch"	
	USING "trigger_NRM_rescue_eng.sch"
	USING "trigger_NRM_rescue_med.sch"
	USING "trigger_NRM_rescue_gun.sch"
	USING "trigger_NRM_supplies_fuel.sch"
	USING "trigger_NRM_supplies_ammo.sch"
	USING "trigger_NRM_supplies_meds.sch"
	USING "trigger_NRM_supplies_food.sch"
	USING "trigger_NRM_radio_A.sch"
	USING "trigger_NRM_radio_B.sch"
	USING "trigger_NRM_radio_C.sch"
#endif


// NB. Each mission needs to be added to the switch case in DOES_MISSION_HAVE_TRIGGER_SCENE. BenR
#if not USE_SP_DLC
PROC SETUP_SET_A_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for SET A mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID
		CASE SP_MISSION_ARMENIAN_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ARM2_STREAM_IN_DIST, 
								TS_ARM2_STREAM_OUT_DIST,
								TS_ARM2_FRIEND_REJECT_DIST,
								TS_ARM2_FRIEND_ACCEPT_BITS)
			
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ARM2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ARM2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ARM2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ARM2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ARM2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ARM2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ARM2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ARM2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ARM2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ARM2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ARM2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ARM2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ARMENIAN_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_ARM3_STREAM_IN_DIST, 
								TS_ARM3_STREAM_OUT_DIST,
								TS_ARM3_FRIEND_REJECT_DIST,
								TS_ARM3_FRIEND_ACCEPT_BITS)
			
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ARM3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ARM3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ARM3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ARM3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ARM3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ARM3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ARM3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ARM3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ARM3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ARM3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ARM3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ARM3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FAMILY_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FAMILY1_STREAM_IN_DIST, 
								TS_FAMILY1_STREAM_OUT_DIST,
								TS_FAMILY1_FRIEND_REJECT_DIST,
								TS_FAMILY1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FAMILY_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FAMILY2_STREAM_IN_DIST, 
								TS_FAMILY2_STREAM_OUT_DIST,
								TS_FAMILY2_FRIEND_REJECT_DIST,
								TS_FAMILY2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY2_AMBIENT_UPDATE
		BREAK
	
		CASE SP_MISSION_FAMILY_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FAMILY3_STREAM_IN_DIST, 
								TS_FAMILY3_STREAM_OUT_DIST,
								TS_FAMILY3_FRIEND_REJECT_DIST,
								TS_FAMILY3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY3_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_LESTER_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_LESTER1_STREAM_IN_DIST, 
								TS_LESTER1_STREAM_OUT_DIST,
								TS_LESTER1_FRIEND_REJECT_DIST,
								TS_LESTER1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_LESTER1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_LESTER1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_LESTER1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_LESTER1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_LESTER1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_LESTER1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_LESTER1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_LESTER1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_LESTER1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_LESTER1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_LESTER1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_LESTER1_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_FRANKLIN_0
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FRANKLIN0_STREAM_IN_DIST, 
								TS_FRANKLIN0_STREAM_OUT_DIST,
								TS_FRANKLIN0_FRIEND_REJECT_DIST,
								TS_FRANKLIN0_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FRANKLIN0_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FRANKLIN0_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FRANKLIN0_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FRANKLIN0_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FRANKLIN0_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FRANKLIN0_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FRANKLIN0_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FRANKLIN0_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FRANKLIN0_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FRANKLIN0_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FRANKLIN0_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FRANKLIN0_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_LAMAR
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_LAMAR1_STREAM_IN_DIST, 
								TS_LAMAR1_STREAM_OUT_DIST,
								TS_LAMAR1_FRIEND_REJECT_DIST,
								TS_LAMAR1_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_LAMAR1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_LAMAR1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_LAMAR1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_LAMAR1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_LAMAR1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_LAMAR1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_LAMAR1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_LAMAR1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_LAMAR1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_LAMAR1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_LAMAR1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_LAMAR1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_JEWELRY_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_JEWEL1_STREAM_IN_DIST, 
								TS_JEWEL1_STREAM_OUT_DIST,
								TS_JEWEL1_FRIEND_REJECT_DIST,
								TS_JEWEL1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_JEWEL1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_JEWEL1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_JEWEL1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_JEWEL1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_JEWEL1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_JEWEL1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_JEWEL1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_JEWEL1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_JEWEL1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_JEWEL1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_JEWEL1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_JEWEL1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_JEWELRY_PREP_1A
			SETUP_TRIGGER_SCENE(paramTrigScene,
								paramMissionID, 
								TS_JEWEL_P1A_STREAM_IN_DIST, 
								TS_JEWEL_P1A_STREAM_OUT_DIST,
								TS_JEWEL_P1A_FRIEND_REJECT_DIST,
								TS_JEWEL_P1A_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_JEWEL_P1A_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_JEWEL_P1A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_JEWEL_P1A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_JEWEL_P1A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_JEWEL_P1A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_JEWEL_P1A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_JEWEL_P1A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_JEWEL_P1A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_JEWEL_P1A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_JEWEL_P1A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_JEWEL_P1A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_JEWEL_P1A_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_JEWELRY_PREP_2A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_JEWEL_P2A_STREAM_IN_DIST, 
								TS_JEWEL_P2A_STREAM_OUT_DIST,
								TS_JEWEL_P2A_FRIEND_REJECT_DIST,
								TS_JEWEL_P2A_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_ONLY_RESET_ON_INIT)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_JEWEL_P2A_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_JEWEL_P2A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_JEWEL_P2A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_JEWEL_P2A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_JEWEL_P2A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_JEWEL_P2A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_JEWEL_P2A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_JEWEL_P2A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_JEWEL_P2A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_JEWEL_P2A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_JEWEL_P2A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_JEWEL_P2A_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_JEWELRY_PREP_1B
			SETUP_TRIGGER_SCENE(paramTrigScene,
								paramMissionID, 
								TS_JEWEL_P1B_STREAM_IN_DIST, 
								TS_JEWEL_P1B_STREAM_OUT_DIST,
								TS_JEWEL_P1B_FRIEND_REJECT_DIST,
								TS_JEWEL_P1B_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_ONLY_RESET_ON_INIT)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_JEWEL_P1B_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_JEWEL_P1B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_JEWEL_P1B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_JEWEL_P1B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_JEWEL_P1B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_JEWEL_P1B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_JEWEL_P1B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_JEWEL_P1B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_JEWEL_P1B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_JEWEL_P1B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_JEWEL_P1B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_JEWEL_P1B_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_TREVOR_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_TREVOR1_STREAM_IN_DIST, 
								TS_TREVOR1_STREAM_OUT_DIST,
								TS_TREVOR1_FRIEND_REJECT_DIST,
								TS_TREVOR1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_TREVOR1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_TREVOR1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_TREVOR1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_TREVOR1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_TREVOR1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_TREVOR1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_TREVOR1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_TREVOR1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_TREVOR1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_TREVOR1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_TREVOR1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_TREVOR1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SHRINK_1
		CASE SP_MISSION_SHRINK_2
		CASE SP_MISSION_SHRINK_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SHRINK_STREAM_IN_DIST, 
								TS_SHRINK_STREAM_OUT_DIST,
								TS_SHRINK_FRIEND_REJECT_DIST,
								TS_SHRINK_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SHRINK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SHRINK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SHRINK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SHRINK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SHRINK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SHRINK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SHRINK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SHRINK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SHRINK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SHRINK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SHRINK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SHRINK_AMBIENT_UPDATE
		BREAK
	
		DEFAULT
			SCRIPT_ASSERT("SETUP_SET_A_TRIGGER_SCENE: No SetA trigger scene data for passed mission. Bug BenR.")
			EXIT
		BREAK
	ENDSWITCH

	paramScenePointers.bSceneBound = TRUE
ENDPROC










PROC SETUP_SET_B_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene ,TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for SET B mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID
		CASE SP_MISSION_TREVOR_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_TREVOR2_STREAM_IN_DIST, 
								TS_TREVOR2_STREAM_OUT_DIST,
								TS_TREVOR2_FRIEND_REJECT_DIST,
								TS_TREVOR2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_TREVOR2_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_TREVOR2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_TREVOR2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_TREVOR2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_TREVOR2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_TREVOR2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_TREVOR2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_TREVOR2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_TREVOR2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_TREVOR2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_TREVOR2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_TREVOR2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_TREVOR_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_TREVOR3_STREAM_IN_DIST, 
								TS_TREVOR3_STREAM_OUT_DIST,
								TS_TREVOR3_FRIEND_REJECT_DIST,
								TS_TREVOR3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_TREVOR3_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_TREVOR3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_TREVOR3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_TREVOR3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_TREVOR3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_TREVOR3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_TREVOR3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_TREVOR3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_TREVOR3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_TREVOR3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_TREVOR3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_TREVOR3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CHINESE_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CHINESE1_STREAM_IN_DIST, 
								TS_CHINESE1_STREAM_OUT_DIST,
								TS_CHINESE1_FRIEND_REJECT_DIST,
								TS_CHINESE1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CHINESE1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CHINESE1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CHINESE1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CHINESE1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CHINESE1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CHINESE1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CHINESE1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CHINESE1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CHINESE1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CHINESE1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CHINESE1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CHINESE1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CHINESE_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CHINESE2_STREAM_IN_DIST, 
								TS_CHINESE2_STREAM_OUT_DIST,
								TS_CHINESE2_FRIEND_REJECT_DIST,
								TS_CHINESE2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CHINESE2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CHINESE2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CHINESE2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CHINESE2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CHINESE2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CHINESE2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CHINESE2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CHINESE2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CHINESE2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CHINESE2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CHINESE2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CHINESE2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FAMILY_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FAMILY4_STREAM_IN_DIST, 
								TS_FAMILY4_STREAM_OUT_DIST,
								TS_FAMILY4_FRIEND_REJECT_DIST,
								TS_FAMILY4_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FAMILY_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FAMILY5_STREAM_IN_DIST, 
								TS_FAMILY5_STREAM_OUT_DIST,
								TS_FAMILY5_FRIEND_REJECT_DIST,
								TS_FAMILY5_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY5_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI1_STREAM_IN_DIST, 
								TS_FBI1_STREAM_OUT_DIST,
								TS_FBI1_FRIEND_REJECT_DIST,
								TS_FBI1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FBI2_STREAM_IN_DIST, 
								TS_FBI2_STREAM_OUT_DIST,
								TS_FBI2_FRIEND_REJECT_DIST,
								TS_FBI2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI2_AMBIENT_UPDATE
		BREAK
	
		CASE SP_MISSION_FBI_4_INTRO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FBI4I_STREAM_IN_DIST, 
								TS_FBI4I_STREAM_OUT_DIST,
								TS_FBI4I_FRIEND_REJECT_DIST,
								TS_FBI4I_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4I_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4I_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4I_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4I_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4I_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4I_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4I_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4I_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4I_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4I_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4I_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4I_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FBI4_P1_STREAM_IN_DIST, 
								TS_FBI4_P1_STREAM_OUT_DIST,
								TS_FBI4_P1_FRIEND_REJECT_DIST,
								TS_FBI4_P1_FRIEND_ACCEPT_BITS,
								TS_FBI4_P1_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_P1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_P1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_P1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_P1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_P1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_P1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_P1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_P1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_P1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_P1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_P1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_P1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI4_P2_STREAM_IN_DIST, 
								TS_FBI4_P2_STREAM_OUT_DIST,
								TS_FBI4_P2_FRIEND_REJECT_DIST,
								TS_FBI4_P2_FRIEND_ACCEPT_BITS,
								TS_FBI4_P2_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_P2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_P2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_P2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_P2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_P2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_P2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_P2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_P2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_P2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_P2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_P2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_P2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI4_P3_STREAM_IN_DIST, 
								TS_FBI4_P3_STREAM_OUT_DIST,
								TS_FBI4_P3_FRIEND_REJECT_DIST,
								TS_FBI4_P3_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_P3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_P3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_P3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_P3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_P3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_P3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_P3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_P3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_P3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_P3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_P3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_P3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FBI4_P4_STREAM_IN_DIST, 
								TS_FBI4_P4_STREAM_OUT_DIST,
								TS_FBI4_P4_FRIEND_REJECT_DIST,
								TS_FBI4_P4_FRIEND_ACCEPT_BITS,
								TS_FBI4_P4_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_P4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_P4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_P4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_P4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_P4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_P4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_P4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_P4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_P4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_P4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_P4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_P4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4_PREP_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI4_P5_STREAM_IN_DIST, 
								TS_FBI4_P5_STREAM_OUT_DIST,
								TS_FBI4_P5_FRIEND_REJECT_DIST,
								TS_FBI4_P5_FRIEND_ACCEPT_BITS,
								TS_FBI4_P5_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_P5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_P5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_P5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_P5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_P5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_P5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_P5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_P5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_P5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_P5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_P5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_P5_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI4_STREAM_IN_DIST, 
								TS_FBI4_STREAM_OUT_DIST,
								TS_FBI4_FRIEND_REJECT_DIST,
								TS_FBI4_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FRANKLIN_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FRANKLIN1_STREAM_IN_DIST, 
								TS_FRANKLIN1_STREAM_OUT_DIST,
								TS_FRANKLIN1_FRIEND_REJECT_DIST,
								TS_FRANKLIN1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FRANKLIN1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FRANKLIN1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FRANKLIN1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FRANKLIN1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FRANKLIN1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FRANKLIN1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FRANKLIN1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FRANKLIN1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FRANKLIN1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FRANKLIN1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FRANKLIN1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FRANKLIN1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_LAMAR
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_LAMAR1_STREAM_IN_DIST, 
								TS_LAMAR1_STREAM_OUT_DIST,
								TS_LAMAR1_FRIEND_REJECT_DIST,
								TS_LAMAR1_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_LAMAR1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_LAMAR1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_LAMAR1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_LAMAR1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_LAMAR1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_LAMAR1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_LAMAR1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_LAMAR1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_LAMAR1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_LAMAR1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_LAMAR1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_LAMAR1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_DOCKS_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_DOCKS1_STREAM_IN_DIST, 
								TS_DOCKS1_STREAM_OUT_DIST,
								TS_DOCKS1_FRIEND_REJECT_DIST,
								TS_DOCKS1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_DOCKS1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_DOCKS1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_DOCKS1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_DOCKS1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_DOCKS1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_DOCKS1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_DOCKS1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_DOCKS1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_DOCKS1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_DOCKS1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_DOCKS1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_DOCKS1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_DOCKS_PREP_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_DOCKS_P1_STREAM_IN_DIST, 
								TS_DOCKS_P1_STREAM_OUT_DIST,
								TS_DOCKS_P1_FRIEND_REJECT_DIST,
								TS_DOCKS_P1_FRIEND_ACCEPT_BITS,
								TS_DOCKS_P1_BATTLE_BUDDY_CALL_DIST)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_DOCKS_P1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_DOCKS_P1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_DOCKS_P1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_DOCKS_P1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_DOCKS_P1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_DOCKS_P1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_DOCKS_P1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_DOCKS_P1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_DOCKS_P1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_DOCKS_P1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_DOCKS_P1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_DOCKS_P1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_DOCKS_PREP_2B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_DOCKS_P2B_STREAM_IN_DIST, 
								TS_DOCKS_P2B_STREAM_OUT_DIST,
								TS_DOCKS_P2B_FRIEND_REJECT_DIST,
								TS_DOCKS_P2B_FRIEND_ACCEPT_BITS,
								TS_DOCKS_P2B_BATTLE_BUDDY_CALL_DIST)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_DOCKS_P2B_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_DOCKS_P2B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_DOCKS_P2B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_DOCKS_P2B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_DOCKS_P2B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_DOCKS_P2B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_DOCKS_P2B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_DOCKS_P2B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_DOCKS_P2B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_DOCKS_P2B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_DOCKS_P2B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_DOCKS_P2B_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_DOCKS_2A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_DOCKS2A_STREAM_IN_DIST, 
								TS_DOCKS2A_STREAM_OUT_DIST,
								TS_DOCKS2A_FRIEND_REJECT_DIST,
								TS_DOCKS2A_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_DOCKS2A_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_DOCKS2A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_DOCKS2A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_DOCKS2A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_DOCKS2A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_DOCKS2A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_DOCKS2A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_DOCKS2A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_DOCKS2A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_DOCKS2A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_DOCKS2A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_DOCKS2A_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_DOCKS_2B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,   
								TS_DOCKS2B_STREAM_IN_DIST, 
								TS_DOCKS2B_STREAM_OUT_DIST,
								TS_DOCKS2B_FRIEND_REJECT_DIST,
								TS_DOCKS2B_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_DOCKS2B_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_DOCKS2B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_DOCKS2B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_DOCKS2B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_DOCKS2B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_DOCKS2B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_DOCKS2B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_DOCKS2B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_DOCKS2B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_DOCKS2B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_DOCKS2B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_DOCKS2B_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN1_STREAM_IN_DIST, 
								TS_ASSASSIN1_STREAM_OUT_DIST,
								TS_ASSASSIN1_FRIEND_REJECT_DIST,
								TS_ASSASSIN1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN2_STREAM_IN_DIST, 
								TS_ASSASSIN2_STREAM_OUT_DIST,
								TS_ASSASSIN2_FRIEND_REJECT_DIST,
								TS_ASSASSIN2_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SHRINK_1
		CASE SP_MISSION_SHRINK_2
		CASE SP_MISSION_SHRINK_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SHRINK_STREAM_IN_DIST, 
								TS_SHRINK_STREAM_OUT_DIST,
								TS_SHRINK_FRIEND_REJECT_DIST,
								TS_SHRINK_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SHRINK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SHRINK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SHRINK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SHRINK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SHRINK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SHRINK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SHRINK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SHRINK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SHRINK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SHRINK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SHRINK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SHRINK_AMBIENT_UPDATE
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("SETUP_SET_B_TRIGGER_SCENE: No SetB trigger scene data for passed mission. Bug BenR.")
			EXIT
		BREAK
	ENDSWITCH

	paramScenePointers.bSceneBound = TRUE
ENDPROC









PROC SETUP_SET_C_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene ,TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for SET C mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID
		CASE SP_MISSION_ASSASSIN_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN1_STREAM_IN_DIST, 
								TS_ASSASSIN1_STREAM_OUT_DIST,
								TS_ASSASSIN1_FRIEND_REJECT_DIST,
								TS_ASSASSIN1_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN2_STREAM_IN_DIST, 
								TS_ASSASSIN2_STREAM_OUT_DIST,
								TS_ASSASSIN2_FRIEND_REJECT_DIST,
								TS_ASSASSIN2_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN3_STREAM_IN_DIST, 
								TS_ASSASSIN3_STREAM_OUT_DIST,
								TS_ASSASSIN3_FRIEND_REJECT_DIST,
								TS_ASSASSIN3_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN4_STREAM_IN_DIST, 
								TS_ASSASSIN4_STREAM_OUT_DIST,
								TS_ASSASSIN4_FRIEND_REJECT_DIST,
								TS_ASSASSIN4_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN5_STREAM_IN_DIST, 
								TS_ASSASSIN5_STREAM_OUT_DIST,
								TS_ASSASSIN5_FRIEND_REJECT_DIST,
								TS_ASSASSIN5_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN5_AMBIENT_UPDATE
		BREAK
	
		CASE SP_MISSION_CARSTEAL_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CAR1_STREAM_IN_DIST, 
								TS_CAR1_STREAM_OUT_DIST,
								TS_CAR1_FRIEND_REJECT_DIST,
								TS_CAR1_FRIEND_ACCEPT_BITS)
			
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CAR1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CAR1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CAR1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CAR1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CAR1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CAR1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CAR1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CAR1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CAR1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CAR1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CAR1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CAR1_AMBIENT_UPDATE
		BREAK
	
		CASE SP_MISSION_CARSTEAL_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CAR2_STREAM_IN_DIST, 
								TS_CAR2_STREAM_OUT_DIST,
								TS_CAR2_FRIEND_REJECT_DIST,
								TS_CAR2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CAR2_RESET							
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CAR2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CAR2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CAR2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CAR2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CAR2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CAR2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CAR2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CAR2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CAR2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CAR2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CAR2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CARSTEAL_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CAR3_STREAM_IN_DIST, 
								TS_CAR3_STREAM_OUT_DIST,
								TS_CAR3_FRIEND_REJECT_DIST,
								TS_CAR3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CAR3_RESET							
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CAR3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CAR3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CAR3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CAR3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CAR3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CAR3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CAR3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CAR3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CAR3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CAR3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CAR3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SOLOMON_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SOLOMON1_STREAM_IN_DIST, 
								TS_SOLOMON1_STREAM_OUT_DIST,
								TS_SOLOMON1_FRIEND_REJECT_DIST,
								TS_SOLOMON1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SOLOMON1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SOLOMON1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SOLOMON1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SOLOMON1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SOLOMON1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SOLOMON1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SOLOMON1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SOLOMON1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SOLOMON1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SOLOMON1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SOLOMON1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SOLOMON1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_MARTIN_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_MARTIN1_STREAM_IN_DIST, 
								TS_MARTIN1_STREAM_OUT_DIST,
								TS_MARTIN1_FRIEND_REJECT_DIST,
								TS_MARTIN1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_MARTIN1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_MARTIN1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_MARTIN1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_MARTIN1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_MARTIN1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_MARTIN1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_MARTIN1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_MARTIN1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_MARTIN1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_MARTIN1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_MARTIN1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_MARTIN1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_EXILE_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_EXILE1_STREAM_IN_DIST, 
								TS_EXILE1_STREAM_OUT_DIST,
								TS_EXILE1_FRIEND_REJECT_DIST,
								TS_EXILE1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_EXILE1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_EXILE1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_EXILE1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_EXILE1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_EXILE1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_EXILE1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_EXILE1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_EXILE1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_EXILE1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_EXILE1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_EXILE1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_EXILE1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_EXILE_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_EXILE2_STREAM_IN_DIST, 
								TS_EXILE2_STREAM_OUT_DIST,
								TS_EXILE2_FRIEND_REJECT_DIST,
								TS_EXILE2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_EXILE2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_EXILE2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_EXILE2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_EXILE2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_EXILE2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_EXILE2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_EXILE2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_EXILE2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_EXILE2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_EXILE2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_EXILE2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_EXILE2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_EXILE_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_EXILE3_STREAM_IN_DIST, 
								TS_EXILE3_STREAM_OUT_DIST,
								TS_EXILE3_FRIEND_REJECT_DIST,
								TS_EXILE3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_EXILE3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_EXILE3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_EXILE3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_EXILE3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_EXILE3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_EXILE3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_EXILE3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_EXILE3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_EXILE3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_EXILE3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_EXILE3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_EXILE3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_RURAL_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_RURALH1_STREAM_IN_DIST, 
								TS_RURALH1_STREAM_OUT_DIST,
								TS_RURALH1_FRIEND_REJECT_DIST,
								TS_RURALH1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_RURALH1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_RURALH1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_RURALH1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_RURALH1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_RURALH1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_RURALH1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_RURALH1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_RURALH1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_RURALH1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_RURALH1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_RURALH1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_RURALH1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_RURAL_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_RURALH2_STREAM_IN_DIST, 
								TS_RURALH2_STREAM_OUT_DIST,
								TS_RURALH2_FRIEND_REJECT_DIST,
								TS_RURALH2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_RURALH2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_RURALH2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_RURALH2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_RURALH2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_RURALH2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_RURALH2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_RURALH2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_RURALH2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_RURALH2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_RURALH2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_RURALH2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_RURALH2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_RURAL_PREP_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_RURALH_P1_STREAM_IN_DIST, 
								TS_RURALH_P1_STREAM_OUT_DIST,
								TS_RURALH_P1_FRIEND_REJECT_DIST,
								TS_RURALH_P1_FRIEND_ACCEPT_BITS,
								TS_RURALH_P1_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_ONLY_RESET_ON_INIT)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_RURALH_P1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_RURALH_P1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_RURALH_P1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_RURALH_P1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_RURALH_P1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_RURALH_P1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_RURALH_P1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_RURALH_P1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_RURALH_P1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_RURALH_P1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_RURALH_P1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_RURALH_P1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FBI_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FBI5_STREAM_IN_DIST, 
								TS_FBI5_STREAM_OUT_DIST,
								TS_FBI5_FRIEND_REJECT_DIST,
								TS_FBI5_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FBI5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FBI5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FBI5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FBI5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FBI5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FBI5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FBI5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FBI5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FBI5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FBI5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FBI5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FBI5_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_TREVOR_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_TREVOR4_STREAM_IN_DIST, 
								TS_TREVOR4_STREAM_OUT_DIST,
								TS_TREVOR4_FRIEND_REJECT_DIST,
								TS_TREVOR4_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_TREVOR4_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_TREVOR4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_TREVOR4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_TREVOR4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_TREVOR4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_TREVOR4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_TREVOR4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_TREVOR4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_TREVOR4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_TREVOR4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_TREVOR4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_TREVOR4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINALEH1_STREAM_IN_DIST, 
								TS_FINALEH1_STREAM_OUT_DIST,
								TS_FINALEH1_FRIEND_REJECT_DIST,
								TS_FINALEH1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_MICHAEL_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_MICHAEL1_STREAM_IN_DIST, 
								TS_MICHAEL1_STREAM_OUT_DIST,
								TS_MICHAEL1_FRIEND_REJECT_DIST,
								TS_MICHAEL1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_MICHAEL1_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_MICHAEL1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_MICHAEL1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_MICHAEL1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_MICHAEL1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_MICHAEL1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_MICHAEL1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_MICHAEL1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_MICHAEL1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_MICHAEL1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_MICHAEL1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_MICHAEL1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SHRINK_1
		CASE SP_MISSION_SHRINK_2
		CASE SP_MISSION_SHRINK_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SHRINK_STREAM_IN_DIST, 
								TS_SHRINK_STREAM_OUT_DIST,
								TS_SHRINK_FRIEND_REJECT_DIST,
								TS_SHRINK_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SHRINK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SHRINK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SHRINK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SHRINK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SHRINK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SHRINK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SHRINK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SHRINK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SHRINK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SHRINK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SHRINK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SHRINK_AMBIENT_UPDATE
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("SETUP_SET_C_TRIGGER_SCENE: No SetC trigger scene data for passed mission. Bug BenR.")
			EXIT
		BREAK
	ENDSWITCH
	
	paramScenePointers.bSceneBound = TRUE
ENDPROC








PROC SETUP_SET_D_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for SET D mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID
	
		CASE SP_HEIST_AGENCY_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_AGENCY1_STREAM_IN_DIST, 
								TS_AGENCY1_STREAM_OUT_DIST,
								TS_AGENCY1_FRIEND_REJECT_DIST,
								TS_AGENCY1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_AGENCY1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_AGENCY1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_AGENCY1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_AGENCY1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_AGENCY1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_AGENCY1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_AGENCY1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_AGENCY1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_AGENCY1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_AGENCY1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_AGENCY1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_AGENCY1_AMBIENT_UPDATE
		BREAK

		CASE SP_HEIST_AGENCY_PREP_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_AGENCY_P1_STREAM_IN_DIST, 
								TS_AGENCY_P1_STREAM_OUT_DIST,
								TS_AGENCY_P1_FRIEND_REJECT_DIST,
								TS_AGENCY_P1_FRIEND_ACCEPT_BITS,
								TS_AGENCY_P1_BATTLE_BUDDY_CALL_DIST,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_AGENCY_P1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_AGENCY_P1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_AGENCY_P1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_AGENCY_P1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_AGENCY_P1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_AGENCY_P1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_AGENCY_P1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_AGENCY_P1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_AGENCY_P1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_AGENCY_P1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_AGENCY_P1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_AGENCY_P1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_AGENCY_3A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_AGENCY3A_STREAM_IN_DIST, 
								TS_AGENCY3A_STREAM_OUT_DIST,
								TS_AGENCY3A_FRIEND_REJECT_DIST,
								TS_AGENCY3A_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_AGENCY3A_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_AGENCY3A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_AGENCY3A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_AGENCY3A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_AGENCY3A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_AGENCY3A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_AGENCY3A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_AGENCY3A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_AGENCY3A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_AGENCY3A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_AGENCY3A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_AGENCY3A_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_AGENCY_3B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_AGENCY3B_STREAM_IN_DIST, 
								TS_AGENCY3B_STREAM_OUT_DIST,
								TS_AGENCY3B_FRIEND_REJECT_DIST,
								TS_AGENCY3B_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_AGENCY3B_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_AGENCY3B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_AGENCY3B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_AGENCY3B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_AGENCY3B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_AGENCY3B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_AGENCY3B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_AGENCY3B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_AGENCY3B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_AGENCY3B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_AGENCY3B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_AGENCY3B_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_ASSASSIN_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN2_STREAM_IN_DIST, 
								TS_ASSASSIN2_STREAM_OUT_DIST,
								TS_ASSASSIN2_FRIEND_REJECT_DIST,
								TS_ASSASSIN2_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN3_STREAM_IN_DIST, 
								TS_ASSASSIN3_STREAM_OUT_DIST,
								TS_ASSASSIN3_FRIEND_REJECT_DIST,
								TS_ASSASSIN3_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN4_STREAM_IN_DIST, 
								TS_ASSASSIN4_STREAM_OUT_DIST,
								TS_ASSASSIN4_FRIEND_REJECT_DIST,
								TS_ASSASSIN4_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN4_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_ASSASSIN_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_ASSASSIN5_STREAM_IN_DIST, 
								TS_ASSASSIN5_STREAM_OUT_DIST,
								TS_ASSASSIN5_FRIEND_REJECT_DIST,
								TS_ASSASSIN5_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_ASSASSIN5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_ASSASSIN5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_ASSASSIN5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_ASSASSIN5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_ASSASSIN5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_ASSASSIN5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_ASSASSIN5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_ASSASSIN5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_ASSASSIN5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_ASSASSIN5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_ASSASSIN5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_ASSASSIN5_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CARSTEAL_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CAR4_STREAM_IN_DIST, 
								TS_CAR4_STREAM_OUT_DIST,
								TS_CAR4_FRIEND_REJECT_DIST,
								TS_CAR4_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CAR4_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CAR4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CAR4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CAR4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CAR4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CAR4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CAR4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CAR4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CAR4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CAR4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CAR4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CAR4_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_FAMILY_6
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FAMILY6_STREAM_IN_DIST, 
								TS_FAMILY6_STREAM_OUT_DIST,
								TS_FAMILY6_FRIEND_REJECT_DIST,
								TS_FAMILY6_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FAMILY6_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FAMILY6_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FAMILY6_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FAMILY6_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FAMILY6_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FAMILY6_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FAMILY6_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FAMILY6_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FAMILY6_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FAMILY6_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FAMILY6_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FAMILY6_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FINALE_INTRO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FIN_INTRO_STREAM_IN_DIST, 
								TS_FIN_INTRO_STREAM_OUT_DIST,
								TS_FIN_INTRO_FRIEND_REJECT_DIST,
								TS_FIN_INTRO_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FIN_INTRO_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FIN_INTRO_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FIN_INTRO_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FIN_INTRO_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FIN_INTRO_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FIN_INTRO_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FIN_INTRO_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FIN_INTRO_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FIN_INTRO_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FIN_INTRO_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FIN_INTRO_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FIN_INTRO_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FINALE_A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINA_STREAM_IN_DIST, 
								TS_FINA_STREAM_OUT_DIST,
								TS_FINA_FRIEND_REJECT_DIST,
								TS_FINA_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINA_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FINALE_B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINB_STREAM_IN_DIST, 
								TS_FINB_STREAM_OUT_DIST,
								TS_FINB_FRIEND_REJECT_DIST,
								TS_FINB_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINB_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINB_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINB_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINB_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINB_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINB_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINB_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINB_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINB_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINB_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINB_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINB_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FINALE_C1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINC1_STREAM_IN_DIST, 
								TS_FINC1_STREAM_OUT_DIST,
								TS_FINC1_FRIEND_REJECT_DIST,
								TS_FINC1_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINC1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINC1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINC1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINC1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINC1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINC1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINC1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINC1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINC1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINC1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINC1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINC1_AMBIENT_UPDATE
		BREAK

		CASE SP_HEIST_FINALE_PREP_A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FINALEH_PA_STREAM_IN_DIST, 
								TS_FINALEH_PA_STREAM_OUT_DIST,
								TS_FINALEH_PA_FRIEND_REJECT_DIST,
								TS_FINALEH_PA_FRIEND_ACCEPT_BITS,
								TS_FINALEH_PA_BATTLE_BUDDY_CALL_DIST)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH_PA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH_PA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH_PA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH_PA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH_PA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH_PA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH_PA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH_PA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH_PA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH_PA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH_PA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH_PA_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINALEH_PB_STREAM_IN_DIST, 
								TS_FINALEH_PB_STREAM_OUT_DIST,
								TS_FINALEH_PB_FRIEND_REJECT_DIST,
								TS_FINALEH_PB_FRIEND_ACCEPT_BITS,
								TS_FINALEH_PB_BATTLE_BUDDY_CALL_DIST)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH_PB_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH_PB_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH_PB_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH_PB_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH_PB_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH_PB_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH_PB_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH_PB_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH_PB_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH_PB_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH_PB_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH_PB_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_C1
		CASE SP_HEIST_FINALE_PREP_C2
		CASE SP_HEIST_FINALE_PREP_C3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINALEH_PC_STREAM_IN_DIST, 
								TS_FINALEH_PC_STREAM_OUT_DIST,
								TS_FINALEH_PC_FRIEND_REJECT_DIST,
								TS_FINALEH_PC_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_CAN_TRIG_BEFORE_CREATE)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH_PC_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH_PC_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH_PC_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH_PC_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH_PC_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH_PC_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH_PC_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH_PC_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH_PC_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH_PC_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH_PC_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH_PC_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_PREP_D
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINALEH_PD_STREAM_IN_DIST, 
								TS_FINALEH_PD_STREAM_OUT_DIST,
								TS_FINALEH_PD_FRIEND_REJECT_DIST,
								TS_FINALEH_PD_FRIEND_ACCEPT_BITS,
								TS_FINALEH_PD_BATTLE_BUDDY_CALL_DIST)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH_PD_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH_PD_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH_PD_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH_PD_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH_PD_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH_PD_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH_PD_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH_PD_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH_PD_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH_PD_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH_PD_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH_PD_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_2_INTRO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,  
								TS_FINALEH2_INTRO_STREAM_IN_DIST, 
								TS_FINALEH2_INTRO_STREAM_OUT_DIST,
								TS_FINALEH2_INTRO_FRIEND_REJECT_DIST,
								TS_FINALEH2_INTRO_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH2_INTRO_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH2_INTRO_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH2_INTRO_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH2_INTRO_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH2_INTRO_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH2_INTRO_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH2_INTRO_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH2_INTRO_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH2_INTRO_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH2_INTRO_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH2_INTRO_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH2_INTRO_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_2A
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID,   
								TS_FINALEH2A_STREAM_IN_DIST, 
								TS_FINALEH2A_STREAM_OUT_DIST,
								TS_FINALEH2A_FRIEND_REJECT_DIST,
								TS_FINALEH2A_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH2A_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH2A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH2A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH2A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH2A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH2A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH2A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH2A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH2A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH2A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH2A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH2A_AMBIENT_UPDATE
		BREAK
		
		CASE SP_HEIST_FINALE_2B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FINALEH2B_STREAM_IN_DIST, 
								TS_FINALEH2B_STREAM_OUT_DIST,
								TS_FINALEH2B_FRIEND_REJECT_DIST,
								TS_FINALEH2B_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FINALEH2B_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FINALEH2B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FINALEH2B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FINALEH2B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FINALEH2B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FINALEH2B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FINALEH2B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FINALEH2B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FINALEH2B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FINALEH2B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FINALEH2B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FINALEH2B_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_FRANKLIN_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_FRANKLIN2_STREAM_IN_DIST, 
								TS_FRANKLIN2_STREAM_OUT_DIST,
								TS_FRANKLIN2_FRIEND_REJECT_DIST,
								TS_FRANKLIN2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_FRANKLIN2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_FRANKLIN2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_FRANKLIN2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_FRANKLIN2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_FRANKLIN2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_FRANKLIN2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_FRANKLIN2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_FRANKLIN2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_FRANKLIN2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_FRANKLIN2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_FRANKLIN2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_FRANKLIN2_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_MICHAEL_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_MICHAEL2_STREAM_IN_DIST, 
								TS_MICHAEL2_STREAM_OUT_DIST,
								TS_MICHAEL2_FRIEND_REJECT_DIST,
								TS_MICHAEL2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_MICHAEL2_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_MICHAEL2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_MICHAEL2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_MICHAEL2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_MICHAEL2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_MICHAEL2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_MICHAEL2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_MICHAEL2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_MICHAEL2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_MICHAEL2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_MICHAEL2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_MICHAEL2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_MICHAEL_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_MICHAEL3_STREAM_IN_DIST, 
								TS_MICHAEL3_STREAM_OUT_DIST,
								TS_MICHAEL3_FRIEND_REJECT_DIST,
								TS_MICHAEL3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_MICHAEL3_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_MICHAEL3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_MICHAEL3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_MICHAEL3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_MICHAEL3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_MICHAEL3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_MICHAEL3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_MICHAEL3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_MICHAEL3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_MICHAEL3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_MICHAEL3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_MICHAEL3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_MICHAEL_4
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_MICHAEL4_STREAM_IN_DIST, 
								TS_MICHAEL4_STREAM_OUT_DIST,
								TS_MICHAEL4_FRIEND_REJECT_DIST,
								TS_MICHAEL4_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_MICHAEL4_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_MICHAEL4_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_MICHAEL4_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_MICHAEL4_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_MICHAEL4_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_MICHAEL4_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_MICHAEL4_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_MICHAEL4_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_MICHAEL4_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_MICHAEL4_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_MICHAEL4_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_MICHAEL4_AMBIENT_UPDATE
		BREAK

		CASE SP_MISSION_SOLOMON_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SOLOMON2_STREAM_IN_DIST, 
								TS_SOLOMON2_STREAM_OUT_DIST,
								TS_SOLOMON2_FRIEND_REJECT_DIST,
								TS_SOLOMON2_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SOLOMON2_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SOLOMON2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SOLOMON2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SOLOMON2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SOLOMON2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SOLOMON2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SOLOMON2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SOLOMON2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SOLOMON2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SOLOMON2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SOLOMON2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SOLOMON2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SOLOMON_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SOLOMON3_STREAM_IN_DIST, 
								TS_SOLOMON3_STREAM_OUT_DIST,
								TS_SOLOMON3_FRIEND_REJECT_DIST,
								TS_SOLOMON3_FRIEND_ACCEPT_BITS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SOLOMON3_RESET	
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SOLOMON3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SOLOMON3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SOLOMON3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SOLOMON3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SOLOMON3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SOLOMON3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SOLOMON3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SOLOMON3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SOLOMON3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SOLOMON3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SOLOMON3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_SHRINK_1
		CASE SP_MISSION_SHRINK_2
		CASE SP_MISSION_SHRINK_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_SHRINK_STREAM_IN_DIST, 
								TS_SHRINK_STREAM_OUT_DIST,
								TS_SHRINK_FRIEND_REJECT_DIST,
								TS_SHRINK_FRIEND_ACCEPT_BITS,
								0.0,
								TS_BIT_SPANS_MULTIPLE_TRIGGERS)
					
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_SHRINK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_SHRINK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_SHRINK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_SHRINK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_SHRINK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_SHRINK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_SHRINK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_SHRINK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_SHRINK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_SHRINK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_SHRINK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_SHRINK_AMBIENT_UPDATE
		BREAK

		DEFAULT
			SCRIPT_ASSERT("SETUP_SET_D_TRIGGER_SCENE: No SetD trigger scene data for passed mission. Bug BenR.")
			EXIT
		BREAK
	ENDSWITCH
	
	paramScenePointers.bSceneBound = TRUE
ENDPROC
#endif


#if USE_CLF_DLC
PROC SETUP_SET_CLF_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for CLF mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID	
		CASE SP_MISSION_CLF_TRAIN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_TRAIN_STREAM_IN_DIST, 
								TS_CLF_TRAIN_STREAM_OUT_DIST,
								TS_CLF_TRAIN_FRIEND_REJECT_DIST,
								TS_CLF_TRAIN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_TRAIN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_TRAIN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_TRAIN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_TRAIN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_TRAIN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_TRAIN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_TRAIN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_TRAIN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_TRAIN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_TRAIN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_TRAIN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_TRAIN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_FIN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_FIN_STREAM_IN_DIST, 
								TS_CLF_FIN_STREAM_OUT_DIST,
								TS_CLF_FIN_FRIEND_REJECT_DIST,
								TS_CLF_FIN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_FIN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_FIN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_FIN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_FIN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_FIN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_FIN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_FIN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_FIN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_FIN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_FIN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_FIN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_FIN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_SPA_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_SPA_1_STREAM_IN_DIST, 
								TS_CLF_SPA_1_STREAM_OUT_DIST,
								TS_CLF_SPA_1_FRIEND_REJECT_DIST,
								TS_CLF_SPA_1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_SPA_1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_SPA_1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_SPA_1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_SPA_1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_SPA_1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_SPA_1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_SPA_1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_SPA_1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_SPA_1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_SPA_1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_SPA_1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_SPA_1_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_SPA_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_SPA_2_STREAM_IN_DIST, 
								TS_CLF_SPA_2_STREAM_OUT_DIST,
								TS_CLF_SPA_2_FRIEND_REJECT_DIST,
								TS_CLF_SPA_2_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_SPA_2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_SPA_2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_SPA_2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_SPA_2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_SPA_2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_SPA_2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_SPA_2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_SPA_2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_SPA_2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_SPA_2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_SPA_2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_SPA_2_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_SPA_FIN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_SPA_FIN_STREAM_IN_DIST, 
								TS_CLF_SPA_FIN_STREAM_OUT_DIST,
								TS_CLF_SPA_FIN_FRIEND_REJECT_DIST,
								TS_CLF_SPA_FIN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_SPA_FIN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_SPA_FIN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_SPA_FIN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_SPA_FIN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_SPA_FIN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_SPA_FIN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_SPA_FIN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_SPA_FIN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_SPA_FIN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_SPA_FIN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_SPA_FIN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_SPA_FIN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_TRA
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_TRA_STREAM_IN_DIST, 
								TS_CLF_IAA_TRA_STREAM_OUT_DIST,
								TS_CLF_IAA_TRA_FRIEND_REJECT_DIST,
								TS_CLF_IAA_TRA_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_TRA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_TRA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_TRA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_TRA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_TRA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_TRA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_TRA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_TRA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_TRA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_TRA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_TRA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_TRA_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_LIE
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_LIE_STREAM_IN_DIST, 
								TS_CLF_IAA_LIE_STREAM_OUT_DIST,
								TS_CLF_IAA_LIE_FRIEND_REJECT_DIST,
								TS_CLF_IAA_LIE_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_LIE_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_LIE_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_LIE_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_LIE_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_LIE_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_LIE_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_LIE_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_LIE_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_LIE_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_LIE_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_LIE_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_LIE_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_JET
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_JET_STREAM_IN_DIST, 
								TS_CLF_IAA_JET_STREAM_OUT_DIST,
								TS_CLF_IAA_JET_FRIEND_REJECT_DIST,
								TS_CLF_IAA_JET_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_JET_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_JET_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_JET_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_JET_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_JET_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_JET_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_JET_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_JET_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_JET_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_JET_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_JET_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_JET_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_HEL
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_HEL_STREAM_IN_DIST, 
								TS_CLF_IAA_HEL_STREAM_OUT_DIST,
								TS_CLF_IAA_HEL_FRIEND_REJECT_DIST,
								TS_CLF_IAA_HEL_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_HEL_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_HEL_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_HEL_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_HEL_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_HEL_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_HEL_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_HEL_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_HEL_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_HEL_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_HEL_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_HEL_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_JET_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_DRO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_DRO_STREAM_IN_DIST, 
								TS_CLF_IAA_DRO_STREAM_OUT_DIST,
								TS_CLF_IAA_DRO_FRIEND_REJECT_DIST,
								TS_CLF_IAA_DRO_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_DRO_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_DRO_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_DRO_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_DRO_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_DRO_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_DRO_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_DRO_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_DRO_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_DRO_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_DRO_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_DRO_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_DRO_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_IAA_RTS
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_IAA_RTS_STREAM_IN_DIST, 
								TS_CLF_IAA_RTS_STREAM_OUT_DIST,
								TS_CLF_IAA_RTS_FRIEND_REJECT_DIST,
								TS_CLF_IAA_RTS_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_IAA_RTS_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_IAA_RTS_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_IAA_RTS_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_IAA_RTS_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_IAA_RTS_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_IAA_RTS_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_IAA_RTS_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_IAA_RTS_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_IAA_RTS_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_IAA_RTS_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_IAA_RTS_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_IAA_RTS_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_KOR_PRO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_KOR_PRO_STREAM_IN_DIST, 
								TS_CLF_KOR_PRO_STREAM_OUT_DIST,
								TS_CLF_KOR_PRO_FRIEND_REJECT_DIST,
								TS_CLF_KOR_PRO_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_KOR_PRO_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_KOR_PRO_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_KOR_PRO_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_KOR_PRO_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_KOR_PRO_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_KOR_PRO_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_KOR_PRO_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_KOR_PRO_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_KOR_PRO_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_KOR_PRO_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_KOR_PRO_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_KOR_PRO_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_KOR_RES
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_KOR_RES_STREAM_IN_DIST, 
								TS_CLF_KOR_RES_STREAM_OUT_DIST,
								TS_CLF_KOR_RES_FRIEND_REJECT_DIST,
								TS_CLF_KOR_RES_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_KOR_RES_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_KOR_RES_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_KOR_RES_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_KOR_RES_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_KOR_RES_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_KOR_RES_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_KOR_RES_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_KOR_RES_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_KOR_RES_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_KOR_RES_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_KOR_RES_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_KOR_RES_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_KOR_SUB
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_KOR_SUB_STREAM_IN_DIST, 
								TS_CLF_KOR_SUB_STREAM_OUT_DIST,
								TS_CLF_KOR_SUB_FRIEND_REJECT_DIST,
								TS_CLF_KOR_SUB_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_KOR_SUB_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_KOR_SUB_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_KOR_SUB_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_KOR_SUB_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_KOR_SUB_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_KOR_SUB_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_KOR_SUB_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_KOR_SUB_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_KOR_SUB_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_KOR_SUB_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_KOR_SUB_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_KOR_SUB_AMBIENT_UPDATE
		BREAK	
		CASE SP_MISSION_CLF_KOR_SAT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_KOR_SAT_STREAM_IN_DIST, 
								TS_CLF_KOR_SAT_STREAM_OUT_DIST,
								TS_CLF_KOR_SAT_FRIEND_REJECT_DIST,
								TS_CLF_KOR_SAT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_KOR_SAT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_KOR_SAT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_KOR_SAT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_KOR_SAT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_KOR_SAT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_KOR_SAT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_KOR_SAT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_KOR_SAT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_KOR_SAT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_KOR_SAT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_KOR_SAT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_KOR_SAT_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_KOR_5
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_KOR_5_STREAM_IN_DIST, 
								TS_CLF_KOR_5_STREAM_OUT_DIST,
								TS_CLF_KOR_5_FRIEND_REJECT_DIST,
								TS_CLF_KOR_5_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_KOR_5_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_KOR_5_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_KOR_5_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_KOR_5_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_KOR_5_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_KOR_5_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_KOR_5_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_KOR_5_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_KOR_5_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_KOR_5_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_KOR_5_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_KOR_5_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_RUS_PLA
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_PLA_STREAM_IN_DIST, 
								TS_CLF_RUS_PLA_STREAM_OUT_DIST,
								TS_CLF_RUS_PLA_FRIEND_REJECT_DIST,
								TS_CLF_RUS_PLA_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_PLA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_PLA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_PLA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_PLA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_PLA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_PLA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_PLA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_PLA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_PLA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_PLA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_PLA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_PLA_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_RUS_CAR
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_CAR_STREAM_IN_DIST, 
								TS_CLF_RUS_CAR_STREAM_OUT_DIST,
								TS_CLF_RUS_CAR_FRIEND_REJECT_DIST,
								TS_CLF_RUS_CAR_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_CAR_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_CAR_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_CAR_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_CAR_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_CAR_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_CAR_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_CAR_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_CAR_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_CAR_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_CAR_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_CAR_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_CAR_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_RUS_SAT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_SAT_STREAM_IN_DIST, 
								TS_CLF_RUS_SAT_STREAM_OUT_DIST,
								TS_CLF_RUS_SAT_FRIEND_REJECT_DIST,
								TS_CLF_RUS_SAT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_SAT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_SAT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_SAT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_SAT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_SAT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_SAT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_SAT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_SAT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_SAT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_SAT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_SAT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_SAT_AMBIENT_UPDATE
		BREAK		
		CASE SP_MISSION_CLF_RUS_JET
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_JET_STREAM_IN_DIST, 
								TS_CLF_RUS_JET_STREAM_OUT_DIST,
								TS_CLF_RUS_JET_FRIEND_REJECT_DIST,
								TS_CLF_RUS_JET_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_JET_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_JET_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_JET_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_JET_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_JET_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_JET_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_JET_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_JET_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_JET_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_JET_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_JET_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_JET_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_RUS_CLK
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_CLK_STREAM_IN_DIST, 
								TS_CLF_RUS_CLK_STREAM_OUT_DIST,
								TS_CLF_RUS_CLK_FRIEND_REJECT_DIST,
								TS_CLF_RUS_CLK_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_CLK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_CLK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_CLK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_CLK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_CLK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_CLK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_CLK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_CLK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_CLK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_CLK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_CLK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_CLK_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_RUS_VAS
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RUS_VAS_STREAM_IN_DIST, 
								TS_CLF_RUS_VAS_STREAM_OUT_DIST,
								TS_CLF_RUS_VAS_FRIEND_REJECT_DIST,
								TS_CLF_RUS_VAS_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RUS_VAS_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RUS_VAS_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RUS_VAS_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RUS_VAS_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RUS_VAS_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RUS_VAS_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RUS_VAS_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RUS_VAS_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RUS_VAS_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RUS_VAS_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RUS_VAS_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RUS_VAS_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ARA_DEF
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ARA_DEF_STREAM_IN_DIST, 
								TS_CLF_ARA_DEF_STREAM_OUT_DIST,
								TS_CLF_ARA_DEF_FRIEND_REJECT_DIST,
								TS_CLF_ARA_DEF_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ARA_DEF_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ARA_DEF_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ARA_DEF_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ARA_DEF_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ARA_DEF_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ARA_DEF_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ARA_DEF_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ARA_DEF_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ARA_DEF_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ARA_DEF_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ARA_DEF_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ARA_DEF_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ARA_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ARA_1_STREAM_IN_DIST, 
								TS_CLF_ARA_1_STREAM_OUT_DIST,
								TS_CLF_ARA_1_FRIEND_REJECT_DIST,
								TS_CLF_ARA_1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ARA_1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ARA_1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ARA_1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ARA_1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ARA_1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ARA_1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ARA_1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ARA_1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ARA_1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ARA_1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ARA_1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ARA_1_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ARA_FAKE
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ARA_FAKE_STREAM_IN_DIST, 
								TS_CLF_ARA_FAKE_STREAM_OUT_DIST,
								TS_CLF_ARA_FAKE_FRIEND_REJECT_DIST,
								TS_CLF_ARA_FAKE_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ARA_FAKE_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ARA_FAKE_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ARA_FAKE_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ARA_FAKE_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ARA_FAKE_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ARA_FAKE_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ARA_FAKE_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ARA_FAKE_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ARA_FAKE_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ARA_FAKE_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ARA_FAKE_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ARA_FAKE_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ARA_TNK
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ARA_TNK_STREAM_IN_DIST, 
								TS_CLF_ARA_TNK_STREAM_OUT_DIST,
								TS_CLF_ARA_TNK_FRIEND_REJECT_DIST,
								TS_CLF_ARA_TNK_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ARA_TNK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ARA_TNK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ARA_TNK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ARA_TNK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ARA_TNK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ARA_TNK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ARA_TNK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ARA_TNK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ARA_TNK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ARA_TNK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ARA_TNK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ARA_TNK_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_CAS_SET
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_CAS_SET_STREAM_IN_DIST, 
								TS_CLF_CAS_SET_STREAM_OUT_DIST,
								TS_CLF_CAS_SET_FRIEND_REJECT_DIST,
								TS_CLF_CAS_SET_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_CAS_SET_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_CAS_SET_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_CAS_SET_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_CAS_SET_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_CAS_SET_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_CAS_SET_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_CAS_SET_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_CAS_SET_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_CAS_SET_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_CAS_SET_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_CAS_SET_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_CAS_SET_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_CAS_PR1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_CAS_PR1_STREAM_IN_DIST, 
								TS_CLF_CAS_PR1_STREAM_OUT_DIST,
								TS_CLF_CAS_PR1_FRIEND_REJECT_DIST,
								TS_CLF_CAS_PR1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_CAS_PR1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_CAS_PR1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_CAS_PR1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_CAS_PR1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_CAS_PR1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_CAS_PR1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_CAS_PR1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_CAS_PR1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_CAS_PR1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_CAS_PR1_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_CAS_PR2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_CAS_PR2_STREAM_IN_DIST, 
								TS_CLF_CAS_PR2_STREAM_OUT_DIST,
								TS_CLF_CAS_PR2_FRIEND_REJECT_DIST,
								TS_CLF_CAS_PR2_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_CAS_PR2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_CAS_PR2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_CAS_PR2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_CAS_PR2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_CAS_PR2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_CAS_PR2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_CAS_PR2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_CAS_PR2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_CAS_PR2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_CAS_PR2_AMBIENT_UPDATE
		BREAK		
		CASE SP_MISSION_CLF_CAS_PR3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_CAS_PR3_STREAM_IN_DIST, 
								TS_CLF_CAS_PR3_STREAM_OUT_DIST,
								TS_CLF_CAS_PR3_FRIEND_REJECT_DIST,
								TS_CLF_CAS_PR3_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_CAS_PR3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_CAS_PR3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_CAS_PR3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_CAS_PR3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_CAS_PR3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_CAS_PR3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_CAS_PR3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_CAS_PR3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_CAS_PR3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_CAS_PR3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_CAS_PR3_AMBIENT_UPDATE
		BREAK		
		CASE SP_MISSION_CLF_CAS_HEI
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_CAS_HEI_STREAM_IN_DIST, 
								TS_CLF_CAS_HEI_STREAM_OUT_DIST,
								TS_CLF_CAS_HEI_FRIEND_REJECT_DIST,
								TS_CLF_CAS_HEI_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_CAS_HEI_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_CAS_HEI_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_CAS_HEI_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_CAS_HEI_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_CAS_HEI_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_CAS_HEI_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_CAS_HEI_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_CAS_HEI_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_CAS_HEI_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_CAS_HEI_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_CAS_HEI_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_CAS_HEI_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_MIL_DAM
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_MIL_DAM_STREAM_IN_DIST, 
								TS_CLF_MIL_DAM_STREAM_OUT_DIST,
								TS_CLF_MIL_DAM_FRIEND_REJECT_DIST,
								TS_CLF_MIL_DAM_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_MIL_DAM_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_MIL_DAM_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_MIL_DAM_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_MIL_DAM_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_MIL_DAM_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_MIL_DAM_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_MIL_DAM_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_MIL_DAM_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_MIL_DAM_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_MIL_DAM_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_MIL_DAM_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_MIL_DAM_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_MIL_PLA
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_MIL_PLA_STREAM_IN_DIST, 
								TS_CLF_MIL_PLA_STREAM_OUT_DIST,
								TS_CLF_MIL_PLA_FRIEND_REJECT_DIST,
								TS_CLF_MIL_PLA_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_MIL_PLA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_MIL_PLA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_MIL_PLA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_MIL_PLA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_MIL_PLA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_MIL_PLA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_MIL_PLA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_MIL_PLA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_MIL_PLA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_MIL_PLA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_MIL_PLA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_MIL_PLA_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_MIL_RKT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_MIL_RKT_STREAM_IN_DIST, 
								TS_CLF_MIL_RKT_STREAM_OUT_DIST,
								TS_CLF_MIL_RKT_FRIEND_REJECT_DIST,
								TS_CLF_MIL_RKT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_MIL_RKT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_MIL_RKT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_MIL_RKT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_MIL_RKT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_MIL_RKT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_MIL_RKT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_MIL_RKT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_MIL_RKT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_MIL_RKT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_MIL_RKT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_MIL_RKT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_MIL_RKT_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_ASS_POL
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_POL_STREAM_IN_DIST, 
								TS_CLF_ASS_POL_STREAM_OUT_DIST,
								TS_CLF_ASS_POL_FRIEND_REJECT_DIST,
								TS_CLF_ASS_POL_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_POL_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_POL_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_POL_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_POL_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_POL_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_POL_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_POL_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_POL_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_POL_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_POL_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_POL_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_POL_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_ASS_RET
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_RET_STREAM_IN_DIST, 
								TS_CLF_ASS_RET_STREAM_OUT_DIST,
								TS_CLF_ASS_RET_FRIEND_REJECT_DIST,
								TS_CLF_ASS_RET_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_RET_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_RET_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_RET_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_RET_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_RET_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_RET_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_RET_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_RET_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_RET_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_RET_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_RET_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_RET_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_CAB
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_CAB_STREAM_IN_DIST, 
								TS_CLF_ASS_CAB_STREAM_OUT_DIST,
								TS_CLF_ASS_CAB_FRIEND_REJECT_DIST,
								TS_CLF_ASS_CAB_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_CAB_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_CAB_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_CAB_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_CAB_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_CAB_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_CAB_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_CAB_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_CAB_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_CAB_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_CAB_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_CAB_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_CAB_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_GEN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_GEN_STREAM_IN_DIST, 
								TS_CLF_ASS_GEN_STREAM_OUT_DIST,
								TS_CLF_ASS_GEN_FRIEND_REJECT_DIST,
								TS_CLF_ASS_GEN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_GEN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_GEN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_GEN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_GEN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_GEN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_GEN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_GEN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_GEN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_GEN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_GEN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_GEN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_GEN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_SUB
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_SUB_STREAM_IN_DIST, 
								TS_CLF_ASS_SUB_STREAM_OUT_DIST,
								TS_CLF_ASS_SUB_FRIEND_REJECT_DIST,
								TS_CLF_ASS_SUB_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_SUB_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_SUB_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_SUB_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_SUB_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_SUB_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_SUB_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_SUB_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_SUB_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_SUB_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_SUB_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_SUB_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_SUB_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_HEL
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_HEL_STREAM_IN_DIST, 
								TS_CLF_ASS_HEL_STREAM_OUT_DIST,
								TS_CLF_ASS_HEL_FRIEND_REJECT_DIST,
								TS_CLF_ASS_HEL_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_HEL_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_HEL_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_HEL_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_HEL_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_HEL_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_HEL_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_HEL_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_HEL_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_HEL_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_HEL_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_HEL_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_HEL_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_VIN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_VIN_STREAM_IN_DIST, 
								TS_CLF_ASS_VIN_STREAM_OUT_DIST,
								TS_CLF_ASS_VIN_FRIEND_REJECT_DIST,
								TS_CLF_ASS_VIN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_VIN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_VIN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_VIN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_VIN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_VIN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_VIN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_VIN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_VIN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_VIN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_VIN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_VIN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_VIN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_HNT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_HNT_STREAM_IN_DIST, 
								TS_CLF_ASS_HNT_STREAM_OUT_DIST,
								TS_CLF_ASS_HNT_FRIEND_REJECT_DIST,
								TS_CLF_ASS_HNT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_HNT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_HNT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_HNT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_HNT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_HNT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_HNT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_HNT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_HNT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_HNT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_HNT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_HNT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_HNT_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_CLF_ASS_SKY
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_ASS_SKY_STREAM_IN_DIST, 
								TS_CLF_ASS_SKY_STREAM_OUT_DIST,
								TS_CLF_ASS_SKY_FRIEND_REJECT_DIST,
								TS_CLF_ASS_SKY_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_ASS_SKY_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_ASS_SKY_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_ASS_SKY_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_ASS_SKY_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_ASS_SKY_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_ASS_SKY_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_ASS_SKY_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_ASS_SKY_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_ASS_SKY_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_ASS_SKY_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_ASS_SKY_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_ASS_SKY_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_JET_1
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_JET_1_STREAM_IN_DIST, 
								TS_CLF_JET_1_STREAM_OUT_DIST,
								TS_CLF_JET_1_FRIEND_REJECT_DIST,
								TS_CLF_JET_1_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_JET_1_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_JET_1_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_JET_1_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_JET_1_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_JET_1_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_JET_1_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_JET_1_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_JET_1_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_JET_1_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_JET_1_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_JET_1_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_JET_1_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_JET_2
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_JET_2_STREAM_IN_DIST, 
								TS_CLF_JET_2_STREAM_OUT_DIST,
								TS_CLF_JET_2_FRIEND_REJECT_DIST,
								TS_CLF_JET_2_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_JET_2_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_JET_2_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_JET_2_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_JET_2_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_JET_2_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_JET_2_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_JET_2_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_JET_2_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_JET_2_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_JET_2_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_JET_2_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_JET_2_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_JET_3
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_JET_3_STREAM_IN_DIST, 
								TS_CLF_JET_3_STREAM_OUT_DIST,
								TS_CLF_JET_3_FRIEND_REJECT_DIST,
								TS_CLF_JET_3_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_JET_3_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_JET_3_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_JET_3_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_JET_3_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_JET_3_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_JET_3_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_JET_3_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_JET_3_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_JET_3_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_JET_3_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_JET_3_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_JET_3_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_JET_REP
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_JET_REP_STREAM_IN_DIST, 
								TS_CLF_JET_REP_STREAM_OUT_DIST,
								TS_CLF_JET_REP_FRIEND_REJECT_DIST,
								TS_CLF_JET_REP_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_JET_REP_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_JET_REP_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_JET_REP_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_JET_REP_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_JET_REP_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_JET_REP_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_JET_REP_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_JET_REP_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_JET_REP_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_JET_REP_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_JET_REP_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_JET_REP_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_ALEX_GRND
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_ALEX_GRND_STREAM_IN_DIST, 
								TS_CLF_RC_ALEX_GRND_STREAM_OUT_DIST,
								TS_CLF_RC_ALEX_GRND_FRIEND_REJECT_DIST,
								TS_CLF_RC_ALEX_GRND_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_GRND_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_ALEX_GRND_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_ALEX_GRND_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_ALEX_GRND_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_GRND_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_ALEX_GRND_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_GRND_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_ALEX_GRND_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_ALEX_GRND_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_ALEX_GRND_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_GRND_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_ALEX_GRND_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_ALEX_AIR
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_ALEX_AIR_STREAM_IN_DIST, 
								TS_CLF_RC_ALEX_AIR_STREAM_OUT_DIST,
								TS_CLF_RC_ALEX_AIR_FRIEND_REJECT_DIST,
								TS_CLF_RC_ALEX_AIR_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_AIR_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_ALEX_AIR_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_ALEX_AIR_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_ALEX_AIR_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_AIR_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_ALEX_AIR_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_AIR_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_ALEX_AIR_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_ALEX_AIR_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_ALEX_AIR_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_AIR_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_ALEX_AIR_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_ALEX_UNDW
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_ALEX_UNDW_STREAM_IN_DIST, 
								TS_CLF_RC_ALEX_UNDW_STREAM_OUT_DIST,
								TS_CLF_RC_ALEX_UNDW_FRIEND_REJECT_DIST,
								TS_CLF_RC_ALEX_UNDW_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_UNDW_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_ALEX_UNDW_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_ALEX_UNDW_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_ALEX_UNDW_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_UNDW_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_ALEX_UNDW_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_UNDW_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_ALEX_UNDW_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_ALEX_UNDW_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_ALEX_UNDW_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_UNDW_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_ALEX_UNDW_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_ALEX_RWRD
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_ALEX_RWRD_STREAM_IN_DIST, 
								TS_CLF_RC_ALEX_RWRD_STREAM_OUT_DIST,
								TS_CLF_RC_ALEX_RWRD_FRIEND_REJECT_DIST,
								TS_CLF_RC_ALEX_RWRD_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_RWRD_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_ALEX_RWRD_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_ALEX_RWRD_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_ALEX_RWRD_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_RWRD_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_ALEX_RWRD_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_ALEX_RWRD_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_ALEX_RWRD_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_ALEX_RWRD_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_ALEX_RWRD_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_ALEX_RWRD_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_ALEX_RWRD_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_MEL_MONT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_MELODY_MONT_STREAM_IN_DIST, 
								TS_CLF_RC_MELODY_MONT_STREAM_OUT_DIST,
								TS_CLF_RC_MELODY_MONT_FRIEND_REJECT_DIST,
								TS_CLF_RC_MELODY_MONT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_MONT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_MELODY_MONT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_MELODY_MONT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_MELODY_MONT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_MONT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_MELODY_MONT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_MONT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_MELODY_MONT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_MELODY_MONT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_MELODY_MONT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_MONT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_MELODY_MONT_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_MEL_AIRP
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_MELODY_AIRP_STREAM_IN_DIST, 
								TS_CLF_RC_MELODY_AIRP_STREAM_OUT_DIST,
								TS_CLF_RC_MELODY_AIRP_FRIEND_REJECT_DIST,
								TS_CLF_RC_MELODY_AIRP_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_AIRP_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_MELODY_AIRP_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_MELODY_AIRP_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_MELODY_AIRP_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_AIRP_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_MELODY_AIRP_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_AIRP_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_MELODY_AIRP_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_MELODY_AIRP_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_MELODY_AIRP_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_AIRP_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_MELODY_AIRP_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_MEL_WING
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_MELODY_WING_STREAM_IN_DIST, 
								TS_CLF_RC_MELODY_WING_STREAM_OUT_DIST,
								TS_CLF_RC_MELODY_WING_FRIEND_REJECT_DIST,
								TS_CLF_RC_MELODY_WING_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_WING_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_MELODY_WING_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_MELODY_WING_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_MELODY_WING_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_WING_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_MELODY_WING_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_WING_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_MELODY_WING_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_MELODY_WING_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_MELODY_WING_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_WING_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_MELODY_WING_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_MEL_DIVE
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_MELODY_DIVE_STREAM_IN_DIST, 
								TS_CLF_RC_MELODY_DIVE_STREAM_OUT_DIST,
								TS_CLF_RC_MELODY_DIVE_FRIEND_REJECT_DIST,
								TS_CLF_RC_MELODY_DIVE_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_DIVE_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_MELODY_DIVE_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_MELODY_DIVE_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_MELODY_DIVE_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_DIVE_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_MELODY_DIVE_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_MELODY_DIVE_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_MELODY_DIVE_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_MELODY_DIVE_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_MELODY_DIVE_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_MELODY_DIVE_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_MELODY_DIVE_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_AGN_BODY
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_AGNES_BODY_STREAM_IN_DIST, 
								TS_CLF_RC_AGNES_BODY_STREAM_OUT_DIST,
								TS_CLF_RC_AGNES_BODY_FRIEND_REJECT_DIST,
								TS_CLF_RC_AGNES_BODY_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_BODY_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_AGNES_BODY_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_AGNES_BODY_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_AGNES_BODY_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_BODY_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_AGNES_BODY_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_BODY_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_AGNES_BODY_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_AGNES_BODY_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_AGNES_BODY_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_BODY_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_AGNES_BODY_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_AGN_RCPT
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_AGNES_RCPT_STREAM_IN_DIST, 
								TS_CLF_RC_AGNES_RCPT_STREAM_OUT_DIST,
								TS_CLF_RC_AGNES_RCPT_FRIEND_REJECT_DIST,
								TS_CLF_RC_AGNES_RCPT_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_RCPT_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_AGNES_RCPT_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_AGNES_RCPT_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_AGNES_RCPT_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_RCPT_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_AGNES_RCPT_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_RCPT_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_AGNES_RCPT_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_AGNES_RCPT_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_AGNES_RCPT_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_RCPT_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_AGNES_RCPT_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_AGN_ESCP
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_AGNES_ESCP_STREAM_IN_DIST, 
								TS_CLF_RC_AGNES_ESCP_STREAM_OUT_DIST,
								TS_CLF_RC_AGNES_ESCP_FRIEND_REJECT_DIST,
								TS_CLF_RC_AGNES_ESCP_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_ESCP_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_AGNES_ESCP_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_AGNES_ESCP_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_AGNES_ESCP_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_ESCP_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_AGNES_ESCP_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_AGNES_ESCP_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_AGNES_ESCP_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_AGNES_ESCP_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_AGNES_ESCP_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_AGNES_ESCP_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_AGNES_ESCP_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_CLA_HACK
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_CLAIRE_HACK_STREAM_IN_DIST, 
								TS_CLF_RC_CLAIRE_HACK_STREAM_OUT_DIST,
								TS_CLF_RC_CLAIRE_HACK_FRIEND_REJECT_DIST,
								TS_CLF_RC_CLAIRE_HACK_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_HACK_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_CLAIRE_HACK_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_CLAIRE_HACK_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_CLAIRE_HACK_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_HACK_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_CLAIRE_HACK_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_HACK_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_CLAIRE_HACK_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_CLAIRE_HACK_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_CLAIRE_HACK_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_HACK_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_CLAIRE_HACK_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_CLA_DRUG
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_CLAIRE_DRUG_STREAM_IN_DIST, 
								TS_CLF_RC_CLAIRE_DRUG_STREAM_OUT_DIST,
								TS_CLF_RC_CLAIRE_DRUG_FRIEND_REJECT_DIST,
								TS_CLF_RC_CLAIRE_DRUG_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_DRUG_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_CLAIRE_DRUG_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_CLAIRE_DRUG_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_CLAIRE_DRUG_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_DRUG_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_CLAIRE_DRUG_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_DRUG_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_CLAIRE_DRUG_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_CLAIRE_DRUG_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_CLAIRE_DRUG_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_DRUG_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_CLAIRE_DRUG_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_CLF_RC_CLA_FIN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_CLF_RC_CLAIRE_FIN_STREAM_IN_DIST, 
								TS_CLF_RC_CLAIRE_FIN_STREAM_OUT_DIST,
								TS_CLF_RC_CLAIRE_FIN_FRIEND_REJECT_DIST,
								TS_CLF_RC_CLAIRE_FIN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_FIN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_CLF_RC_CLAIRE_FIN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_CLF_RC_CLAIRE_FIN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_CLF_RC_CLAIRE_FIN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_FIN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_CLF_RC_CLAIRE_FIN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_CLF_RC_CLAIRE_FIN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_CLF_RC_CLAIRE_FIN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_CLF_RC_CLAIRE_FIN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_CLF_RC_CLAIRE_FIN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_CLF_RC_CLAIRE_FIN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_CLF_RC_CLAIRE_FIN_AMBIENT_UPDATE
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("SETUP_CLF_TRIGGER_SCENE: No CLF trigger scene data for passed mission. Bug BenR.")
			EXIT
		BREAK
	ENDSWITCH
	
	paramScenePointers.bSceneBound = TRUE
ENDPROC
#endif

#if USE_NRM_DLC
PROC SETUP_SET_NRM_TRIGGER_SCENE(SP_MISSIONS paramMissionID, TRIGGER_SCENE &paramTrigScene, TRIGGER_SCENE_POINTERS &paramScenePointers)

	CPRINTLN(DEBUG_TRIGGER, "Populating trigger scene data for NRM mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(paramMissionID), ".")
	
	SWITCH paramMissionID	
		CASE SP_MISSION_NRM_SUR_START
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_START_STREAM_IN_DIST, 
								TS_NRM_SUR_START_STREAM_OUT_DIST,
								TS_NRM_SUR_START_FRIEND_REJECT_DIST,
								TS_NRM_SUR_START_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_START_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_START_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_START_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_START_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_START_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_START_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_START_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_START_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_START_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_START_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_START_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_START_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_AMANDA
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_AMANDA_STREAM_IN_DIST, 
								TS_NRM_SUR_AMANDA_STREAM_OUT_DIST,
								TS_NRM_SUR_AMANDA_FRIEND_REJECT_DIST,
								TS_NRM_SUR_AMANDA_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_AMANDA_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_AMANDA_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_AMANDA_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_AMANDA_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_AMANDA_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_AMANDA_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_AMANDA_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_AMANDA_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_AMANDA_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_AMANDA_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_AMANDA_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_AMANDA_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_TRACEY
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_TRACEY_STREAM_IN_DIST, 
								TS_NRM_SUR_TRACEY_STREAM_OUT_DIST,
								TS_NRM_SUR_TRACEY_FRIEND_REJECT_DIST,
								TS_NRM_SUR_TRACEY_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_TRACEY_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_TRACEY_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_TRACEY_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_TRACEY_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_TRACEY_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_TRACEY_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_TRACEY_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_TRACEY_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_TRACEY_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_TRACEY_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_TRACEY_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_TRACEY_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_MICHAEL
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_MICHAEL_STREAM_IN_DIST, 
								TS_NRM_SUR_MICHAEL_STREAM_OUT_DIST,
								TS_NRM_SUR_MICHAEL_FRIEND_REJECT_DIST,
								TS_NRM_SUR_MICHAEL_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_MICHAEL_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_MICHAEL_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_MICHAEL_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_MICHAEL_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_MICHAEL_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_MICHAEL_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_MICHAEL_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_MICHAEL_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_MICHAEL_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_MICHAEL_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_MICHAEL_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_MICHAEL_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_HOME
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_HOME_STREAM_IN_DIST, 
								TS_NRM_SUR_HOME_STREAM_OUT_DIST,
								TS_NRM_SUR_HOME_FRIEND_REJECT_DIST,
								TS_NRM_SUR_HOME_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_HOME_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_HOME_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_HOME_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_HOME_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_HOME_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_HOME_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_HOME_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_HOME_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_HOME_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_HOME_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_HOME_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_HOME_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_JIMMY
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_JIMMY_STREAM_IN_DIST, 
								TS_NRM_SUR_JIMMY_STREAM_OUT_DIST,
								TS_NRM_SUR_JIMMY_FRIEND_REJECT_DIST,
								TS_NRM_SUR_JIMMY_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_JIMMY_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_JIMMY_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_JIMMY_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_JIMMY_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_JIMMY_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_JIMMY_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_JIMMY_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_JIMMY_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_JIMMY_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_JIMMY_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_JIMMY_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_JIMMY_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_PARTY
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_PARTY_STREAM_IN_DIST, 
								TS_NRM_SUR_PARTY_STREAM_OUT_DIST,
								TS_NRM_SUR_PARTY_FRIEND_REJECT_DIST,
								TS_NRM_SUR_PARTY_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_PARTY_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_PARTY_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_PARTY_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_PARTY_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_PARTY_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_PARTY_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_PARTY_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_PARTY_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_PARTY_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_PARTY_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_PARTY_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_PARTY_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUR_CURE
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUR_CURE_STREAM_IN_DIST, 
								TS_NRM_SUR_CURE_STREAM_OUT_DIST,
								TS_NRM_SUR_CURE_FRIEND_REJECT_DIST,
								TS_NRM_SUR_CURE_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUR_CURE_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUR_CURE_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUR_CURE_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUR_CURE_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUR_CURE_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUR_CURE_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUR_CURE_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUR_CURE_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUR_CURE_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUR_CURE_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUR_CURE_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUR_CURE_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_RESCUE_ENG
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RESCUE_ENG_STREAM_IN_DIST, 
								TS_NRM_RESCUE_ENG_STREAM_OUT_DIST,
								TS_NRM_RESCUE_ENG_FRIEND_REJECT_DIST,
								TS_NRM_RESCUE_ENG_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RESCUE_ENG_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RESCUE_ENG_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RESCUE_ENG_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RESCUE_ENG_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_ENG_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RESCUE_ENG_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_ENG_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RESCUE_ENG_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RESCUE_ENG_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RESCUE_ENG_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RESCUE_ENG_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RESCUE_ENG_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_RESCUE_MED
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RESCUE_MED_STREAM_IN_DIST, 
								TS_NRM_RESCUE_MED_STREAM_OUT_DIST,
								TS_NRM_RESCUE_MED_FRIEND_REJECT_DIST,
								TS_NRM_RESCUE_MED_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RESCUE_MED_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RESCUE_MED_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RESCUE_MED_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RESCUE_MED_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_MED_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RESCUE_MED_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_MED_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RESCUE_MED_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RESCUE_MED_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RESCUE_MED_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RESCUE_MED_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RESCUE_MED_AMBIENT_UPDATE
		BREAK
		
		CASE SP_MISSION_NRM_RESCUE_GUN
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RESCUE_GUN_STREAM_IN_DIST, 
								TS_NRM_RESCUE_GUN_STREAM_OUT_DIST,
								TS_NRM_RESCUE_GUN_FRIEND_REJECT_DIST,
								TS_NRM_RESCUE_GUN_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RESCUE_GUN_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RESCUE_GUN_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RESCUE_GUN_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RESCUE_GUN_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_GUN_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RESCUE_GUN_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RESCUE_GUN_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RESCUE_GUN_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RESCUE_GUN_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RESCUE_GUN_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RESCUE_GUN_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RESCUE_GUN_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUP_FUEL
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUP_FUEL_STREAM_IN_DIST, 
								TS_NRM_SUP_FUEL_STREAM_OUT_DIST,
								TS_NRM_SUP_FUEL_FRIEND_REJECT_DIST,
								TS_NRM_SUP_FUEL_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUP_FUEL_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUP_FUEL_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUP_FUEL_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUP_FUEL_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUP_FUEL_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUP_FUEL_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUP_FUEL_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUP_FUEL_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUP_FUEL_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUP_FUEL_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUP_FUEL_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUP_FUEL_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUP_AMMO
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUP_AMMO_STREAM_IN_DIST, 
								TS_NRM_SUP_AMMO_STREAM_OUT_DIST,
								TS_NRM_SUP_AMMO_FRIEND_REJECT_DIST,
								TS_NRM_SUP_AMMO_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUP_AMMO_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUP_AMMO_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUP_AMMO_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUP_AMMO_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUP_AMMO_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUP_AMMO_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUP_AMMO_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUP_AMMO_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUP_AMMO_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUP_AMMO_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUP_AMMO_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUP_AMMO_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUP_MEDS
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUP_MEDS_STREAM_IN_DIST, 
								TS_NRM_SUP_MEDS_STREAM_OUT_DIST,
								TS_NRM_SUP_MEDS_FRIEND_REJECT_DIST,
								TS_NRM_SUP_MEDS_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUP_MEDS_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUP_MEDS_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUP_MEDS_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUP_MEDS_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUP_MEDS_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUP_MEDS_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUP_MEDS_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUP_MEDS_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUP_MEDS_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUP_MEDS_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUP_MEDS_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUP_MEDS_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_SUP_FOOD
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_SUP_FOOD_STREAM_IN_DIST, 
								TS_NRM_SUP_FOOD_STREAM_OUT_DIST,
								TS_NRM_SUP_FOOD_FRIEND_REJECT_DIST,
								TS_NRM_SUP_FOOD_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_SUP_FOOD_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_SUP_FOOD_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_SUP_FOOD_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_SUP_FOOD_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_SUP_FOOD_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_SUP_FOOD_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_SUP_FOOD_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_SUP_FOOD_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_SUP_FOOD_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_SUP_FOOD_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_SUP_FOOD_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_SUP_FOOD_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_RADIO_A	
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RADIO_A_STREAM_IN_DIST, 
								TS_NRM_RADIO_A_STREAM_OUT_DIST,
								TS_NRM_RADIO_A_FRIEND_REJECT_DIST,
								TS_NRM_RADIO_A_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RADIO_A_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RADIO_A_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RADIO_A_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RADIO_A_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RADIO_A_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RADIO_A_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RADIO_A_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RADIO_A_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RADIO_A_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RADIO_A_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RADIO_A_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RADIO_A_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_RADIO_B
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RADIO_B_STREAM_IN_DIST, 
								TS_NRM_RADIO_B_STREAM_OUT_DIST,
								TS_NRM_RADIO_B_FRIEND_REJECT_DIST,
								TS_NRM_RADIO_B_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RADIO_B_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RADIO_B_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RADIO_B_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RADIO_B_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RADIO_B_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RADIO_B_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RADIO_B_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RADIO_B_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RADIO_B_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RADIO_B_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RADIO_B_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RADIO_B_AMBIENT_UPDATE
		BREAK
		CASE SP_MISSION_NRM_RADIO_C
			SETUP_TRIGGER_SCENE(paramTrigScene, 
								paramMissionID, 
								TS_NRM_RADIO_C_STREAM_IN_DIST, 
								TS_NRM_RADIO_C_STREAM_OUT_DIST,
								TS_NRM_RADIO_C_FRIEND_REJECT_DIST,
								TS_NRM_RADIO_C_FRIEND_ACCEPT_BITS)
							
			paramScenePointers.RESET_TRIGGER_SCENE				= &TS_NRM_RADIO_C_RESET
			paramScenePointers.REQUEST_TRIGGER_SCENE_ASSETS 	= &TS_NRM_RADIO_C_REQUEST_ASSETS
			paramScenePointers.RELEASE_TRIGGER_SCENE_ASSETS		= &TS_NRM_RADIO_C_RELEASE_ASSETS
			paramScenePointers.HAVE_TRIGGER_SCENE_ASSETS_LOADED = &TS_NRM_RADIO_C_HAVE_ASSETS_LOADED
			paramScenePointers.CREATE_TRIGGER_SCENE 			= &TS_NRM_RADIO_C_CREATE
			paramScenePointers.RELEASE_TRIGGER_SCENE			= &TS_NRM_RADIO_C_RELEASE
			paramScenePointers.DELETE_TRIGGER_SCENE 			= &TS_NRM_RADIO_C_DELETE
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_TRIGGERED	= &TS_NRM_RADIO_C_HAS_BEEN_TRIGGERED
			paramScenePointers.HAS_TRIGGER_SCENE_BEEN_DISRUPTED = &TS_NRM_RADIO_C_HAS_BEEN_DISRUPTED
			paramScenePointers.IS_TRIGGER_SCENE_BLOCKED			= &TS_NRM_RADIO_C_IS_BLOCKED
			paramScenePointers.UPDATE_TRIGGER_SCENE				= &TS_NRM_RADIO_C_UPDATE
			paramScenePointers.AMBIENT_UPDATE_TRIGGER_SCENE		= &TS_NRM_RADIO_C_AMBIENT_UPDATE
		BREAK	
		
		
		DEFAULT
			SCRIPT_ASSERT("SETUP_SET_NRM_TRIGGER_SCENE: No SetD trigger scene data for passed mission. Bug CraigV.")
			EXIT
		BREAK
	ENDSWITCH
	
	paramScenePointers.bSceneBound = TRUE
ENDPROC
#endif
