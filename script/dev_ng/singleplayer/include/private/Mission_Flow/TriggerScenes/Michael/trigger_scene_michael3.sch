//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Michael 3 - Trigger Scene Data							│
//│																				│
//│				Author: Ben Rollinson				Date: 22/10/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│							ped[0] = Dave Norton								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"

CONST_FLOAT 	TS_MICHAEL3_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_MICHAEL3_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_MCH3_TRIGGER_DIST			7.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_MICHAEL3_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_MICHAEL3_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_MICHAEL3_RESET()
	
	g_sTriggerSceneAssets.flag = FALSE

ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_MICHAEL3_REQUEST_ASSETS()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_MICHAEL3_RELEASE_ASSETS()
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_MICHAEL3_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_MICHAEL3_CREATE()
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_MICHAEL3_RELEASE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_MICHAEL3_DELETE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_MICHAEL3_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_3)
	FLOAT	vTriggerDistance 
	VEHICLE_INDEX vehPlayerCar
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
	AND NOT g_bPlayerIsInTaxi
	
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MCH3_TRIGGER_DIST)
		#ENDIF
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			//On vehicle, we want a larger distance trigger
			vTriggerDistance = 1089 // (33 meters)
		ELSE
			vTriggerDistance = TS_MCH3_TRIGGER_DIST*TS_MCH3_TRIGGER_DIST
		ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < vTriggerDistance
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehPlayerCar = GET_PLAYERS_LAST_VEHICLE()
				IF DOES_ENTITY_EXIST(vehPlayerCar) AND NOT IS_ENTITY_DEAD(vehPlayerCar)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 500, 1, 20.0)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_MICHAEL3_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_MICHAEL3_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_MICHAEL3_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_MICHAEL3_AMBIENT_UPDATE()
ENDPROC
