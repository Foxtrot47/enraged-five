//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Michael 2 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│
//│								ped[0]  Denise									│
//│								ped[1]  Friend 1								│
//│								ped[2]  Friend 2								│
//│								ped[3]  Franklin								│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_MICHAEL2_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_MICHAEL2_STREAM_OUT_DIST		175.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_MICHAEL2_TRIGGER_DIST		3.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_MICHAEL2_PIN_IN_DIST			75.0	// Distance from trigger's blip at which the interior pins.
CONST_FLOAT		TS_MICHAEL2_PIN_OUT_DIST		80.0	// Distance from trigger's blip at which the interior unpins.

CONST_FLOAT		TS_MICHAEL2_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_MICHAEL2_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


INT sceneMic2LeadIn

BOOL bPinnedHouse
INTERIOR_INSTANCE_INDEX intHouse

INT iFrontDoor = HASH("DOORHASH_F_HOUSE_SC_F")

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_MICHAEL2_RESET()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_MICHAEL2_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("missmic2leadinmic_2_int")
	REQUEST_ANIM_DICT("cellphone@")
	//mic_2_int_alt1_leadin_denise | mic_2_int_alt1_leadin_f1 | mic_2_int_alt1_leadin_f2 | mic_2_int_alt1_leadin_door
	REQUEST_PLAYER_PED_MODEL(CHAR_FRANKLIN)
	REQUEST_NPC_PED_MODEL(CHAR_DENISE)
	REQUEST_MODEL(CSB_DENISE_FRIEND)
	REQUEST_MODEL(V_ILEV_FA_FRONTDOOR)
	REQUEST_MODEL(PROP_PHONE_ING)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_MICHAEL2_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("missmic2leadinmic_2_int")
	REMOVE_ANIM_DICT("cellphone@")
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FRANKLIN)
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_DENISE)
	SET_MODEL_AS_NO_LONGER_NEEDED(CSB_DENISE_FRIEND)
	SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_FA_FRONTDOOR)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		REMOVE_DOOR_FROM_SYSTEM(iFrontDoor)
	ENDIF
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_MICHAEL2_HAVE_ASSETS_LOADED()
	IF HAS_ANIM_DICT_LOADED("missmic2leadinmic_2_int")
	AND HAS_ANIM_DICT_LOADED("cellphone@")
	AND HAS_PLAYER_PED_MODEL_LOADED(CHAR_FRANKLIN)
	AND HAS_NPC_PED_MODEL_LOADED(CHAR_DENISE)
	AND HAS_MODEL_LOADED(CSB_DENISE_FRIEND)
	AND HAS_MODEL_LOADED(V_ILEV_FA_FRONTDOOR)
	AND HAS_MODEL_LOADED(PROP_PHONE_ING)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_MICHAEL2_CREATE()
	IF intHouse = NULL
		intHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-14.5468, -1437.9185, 30.2015>>, "v_franklins")
	ENDIF
	
	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_1), 40.0, TRUE)
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-13.219224, -1452.647827, 30.549818>> - <<20.0, 20.0, 10.0>>, <<-13.219224, -1452.647827, 30.549818>> + <<20.0, 20.0, 10.0>>)
	
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT_NO_OFFSET(V_ILEV_FA_FRONTDOOR, <<-14.8689, -1441.1821, 31.1932>> + <<-0.00, -0.03, 0.01>>)// + <<-0.23, 0.60, -0.01>>)		+ <<-0.00, -0.03, 0.01>>
	
	SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[0], <<0.0, 0.0, -179.50>>)
	
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_DENISE, <<-14.2667, -1443.4365, 30.1058>>, 162.9857)
	SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[0], 2.0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.ped[1] = CREATE_PED(PEDTYPE_MISSION, CSB_DENISE_FRIEND, <<-15.1030, -1442.7990, 30.1058>>, -108.3768)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_HAIR, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_TORSO, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[1], PED_COMP_LEG, 1, 0)
	SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[1], 2.0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.ped[2] = CREATE_PED(PEDTYPE_MISSION, CSB_DENISE_FRIEND, <<-13.5914, -1443.0289, 30.1058>>, 159.0737)
	SET_PED_LOD_MULTIPLIER(g_sTriggerSceneAssets.ped[2], 2.0)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3], CHAR_FRANKLIN, <<-14.6627, -1452.0387, 29.5395>>, 349.9372, DEFAULT, TRUE)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
		
		g_sTriggerSceneAssets.object[2] = CREATE_OBJECT(PROP_PHONE_ING, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[3]))
		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[2],  g_sTriggerSceneAssets.ped[3], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[3], BONETAG_PH_R_HAND), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, 0.0>>))
		TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[3], "cellphone@", "cellphone_text_in", WALK_BLEND_IN, WALK_BLEND_OUT, DEFAULT, AF_HOLD_LAST_FRAME)
	ENDIF
	
	sceneMic2LeadIn = CREATE_SYNCHRONIZED_SCENE(<<-14.869, -1441.182, 31.193>> + <<-0.00, -0.03, 0.01>>, <<0.0, 0.0, 180.0>>)
	
	PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], sceneMic2LeadIn, "mic_2_int_alt1_leadin_door", "missmic2leadinmic_2_int", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
	
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneMic2LeadIn, "missmic2leadinmic_2_int", "mic_2_int_alt1_leadin_denise", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneMic2LeadIn, "missmic2leadinmic_2_int", "mic_2_int_alt1_leadin_f1", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], sceneMic2LeadIn, "missmic2leadinmic_2_int", "mic_2_int_alt1_leadin_f2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	
	SET_SYNCHRONIZED_SCENE_PHASE(sceneMic2LeadIn, 0.0)
	SET_SYNCHRONIZED_SCENE_RATE(sceneMic2LeadIn, 0.0)
	
	REMOVE_ANIM_DICT("missmic2leadinmic_2_int")
	REMOVE_ANIM_DICT("cellphone@")
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FRANKLIN)
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_DENISE)
	SET_MODEL_AS_NO_LONGER_NEEDED(CSB_DENISE_FRIEND)
	SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_FA_FRONTDOOR)
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_PHONE_ING)
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MICHAEL_2,
												"MIC_2_INT",
												CS_NONE,
												CS_ALL,
												CS_ALL)
	
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_MICHAEL_2, "DENISE", g_sTriggerSceneAssets.ped[0])
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_MICHAEL_2, "Denises_Friend_1", g_sTriggerSceneAssets.ped[1])
	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_MICHAEL_2, "Denises_Friend_2", g_sTriggerSceneAssets.ped[2])
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_MICHAEL2_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[2])
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		REMOVE_DOOR_FROM_SYSTEM(iFrontDoor)
	ENDIF
	
	IF intHouse <> NULL
		IF bPinnedHouse = TRUE
			UNPIN_INTERIOR(intHouse)
			
			bPinnedHouse = FALSE
		ENDIF
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_MICHAEL2_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])	//We can't delete it since it's a map object
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[2])
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
		REMOVE_DOOR_FROM_SYSTEM(iFrontDoor)
	ENDIF
	
	IF intHouse <> NULL
		IF bPinnedHouse = TRUE
			UNPIN_INTERIOR(intHouse)
			
			bPinnedHouse = FALSE
		ENDIF
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_MICHAEL2_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_2, GET_CURRENT_PLAYER_PED_INT())
		
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MICHAEL2_TRIGGER_DIST)
		#ENDIF
		
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		
		//Pin the interior
		IF fDistanceSquaredFromTrigger < (TS_MICHAEL2_PIN_IN_DIST * TS_MICHAEL2_PIN_IN_DIST)
			IF intHouse = NULL
				intHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-14.5468, -1437.9185, 30.2015>>, "v_franklins")
			ELSE
				IF bPinnedHouse = FALSE
					PIN_INTERIOR_IN_MEMORY(intHouse)
					
					IF NOT IS_INTERIOR_READY(intHouse)
						PRINTLN("PINNING INTERIOR...")
					ELSE
						SET_INTERIOR_ACTIVE(intHouse, TRUE)
						
						bPinnedHouse = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF fDistanceSquaredFromTrigger > (TS_MICHAEL2_PIN_OUT_DIST * TS_MICHAEL2_PIN_OUT_DIST)
			IF intHouse <> NULL
				IF bPinnedHouse = TRUE
					UNPIN_INTERIOR(intHouse)
					
					bPinnedHouse = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		//Look for the player locking in when they walk over the mission blip.
		IF NOT g_bPlayerLockedInToTrigger
			VEHICLE_INDEX vehClosest = GET_CLOSEST_VEHICLE(<<-14.23323, -1441.48621, 30.10742>>, 8.75, DUMMY_MODEL_FOR_SCRIPT, ENUM_TO_INT(VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES))
			
			#IF IS_DEBUG_BUILD
				IF DOES_ENTITY_EXIST(vehClosest)
					PRINTLN("vehClosest = ", NATIVE_TO_INT(vehClosest), " [model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClosest)), "]")
				ELSE
					PRINTLN("vehClosest doesn't exist.")
				ENDIF
			#ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehClosest)
			AND NOT IS_AREA_OCCUPIED(<<-14.23323, -1441.48621, 30.10742>> - <<5.0, 5.0, 5.0>>, <<-14.23323, -1441.48621, 30.10742>> + <<5.0, 5.0, 5.0>>, FALSE, TRUE, FALSE, FALSE, FALSE)
				IF ((GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-27.723364, -1451.520264, 28.912764>>, <<-5.230825, -1450.831665, 39.548096>>, 30.0))
				OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-4.701051, -1445.258057, 29.053890>>, <<-27.657207, -1446.865112, 39.652313>>, 20.0)))
				AND NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<-4.701051, -1445.258057, 29.053890>>, <<-27.657207, -1446.865112, 39.652313>>, 20.0)
				AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					
					CLEAR_AREA_OF_PROJECTILES(vTriggerPosition, 100.0)
				ENDIF
			ENDIF
		//Player locked in. 
		ELSE
			IF HAS_THIS_CUTSCENE_LOADED("MIC_2_INT")
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iFrontDoor)
					ADD_DOOR_TO_SYSTEM(iFrontDoor, V_ILEV_FA_FRONTDOOR, <<-14.8689, -1441.1821, 31.1932>>)
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneMic2LeadIn)
					IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
						g_sTriggerSceneAssets.object[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<-14.8689, -1441.1821, 31.1932>>, 1.0, V_ILEV_FA_FRONTDOOR)
					ELSE
						IF GET_SYNCHRONIZED_SCENE_RATE(sceneMic2LeadIn) < 1.0
							SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.object[1], FALSE)
							
							IF DOOR_SYSTEM_GET_OPEN_RATIO(iFrontDoor) <> 1.0
							OR DOOR_SYSTEM_GET_DOOR_STATE(iFrontDoor) != DOORSTATE_FORCE_LOCKED_THIS_FRAME
								DOOR_SYSTEM_SET_OPEN_RATIO(iFrontDoor, 1.0, FALSE, FALSE)
								DOOR_SYSTEM_SET_DOOR_STATE(iFrontDoor, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
							ENDIF
							
							SET_SYNCHRONIZED_SCENE_PHASE(sceneMic2LeadIn, 0.01)
							SET_SYNCHRONIZED_SCENE_RATE(sceneMic2LeadIn, 1.0)
							
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
								TASK_FOLLOW_NAV_MESH_TO_COORD(g_sTriggerSceneAssets.ped[3], <<-14.9856, -1446.9579, 29.6462>>, PEDMOVE_WALK)
								TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[2])
							ENDIF
						ELSE
							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneMic2LeadIn) >= 0.75
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-7.118235, -1445.559326, 29.185593>>, <<-20.304062, -1446.629395, 33.622505>>, 8.0)
				OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-0.967271, -1445.203369, 29.053888>>, <<-30.576401, -1446.576416, 44.353458>>, 28.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_MICHAEL2_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
		
		//Check for scene peds being injured or harmed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[index])
					OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[index])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for scene vehicle being damaged or destroyed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.veh) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
				OR NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(<<-4.701051, -1445.258057, 29.053890>>, <<-27.657207, -1446.865112, 39.652313>>, 20.0)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-4.701051, -1445.258057, 29.053890>>, <<-27.657207, -1446.865112, 39.652313>>, 20.0)
		RETURN TRUE
	ENDIF
	
	VEHICLE_INDEX vehClosest = GET_CLOSEST_VEHICLE(<<-14.23323, -1441.48621, 30.10742>>, 8.75, DUMMY_MODEL_FOR_SCRIPT, ENUM_TO_INT(VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER))
	

	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(vehClosest)
			PRINTLN("vehClosest = ", NATIVE_TO_INT(vehClosest), " [model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClosest)), "]")
		ELSE
			PRINTLN("vehClosest doesn't exist.")
		ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(vehClosest)
	OR IS_AREA_OCCUPIED(<<-14.23323, -1441.48621, 30.10742>> - <<5.0, 5.0, 5.0>>, <<-14.23323, -1441.48621, 30.10742>> + <<5.0, 5.0, 5.0>>, FALSE, TRUE, FALSE, FALSE, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_MICHAEL2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_MICHAEL2_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_MICHAEL2_AMBIENT_UPDATE()
ENDPROC
