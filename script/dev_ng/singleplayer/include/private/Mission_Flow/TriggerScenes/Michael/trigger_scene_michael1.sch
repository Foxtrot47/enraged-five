//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Michael 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								veh[0] = Michael's car							│
//│								ped[0] = Michael								│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "respawn_location_private.sch"


CONST_FLOAT 	TS_MICHAEL1_STREAM_IN_DIST		200.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_MICHAEL1_STREAM_OUT_DIST		225.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_MICHAEL1_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_MICHAEL1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_MICHAEL1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

BOOL			TS_MICHAEL1_MUSIC_PLAYING
BOOL			TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_MICHAEL1_RESET()
	TS_MICHAEL1_MUSIC_PLAYING = FALSE
	TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_MICHAEL1_REQUEST_ASSETS()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			//
		BREAK
		CASE CHAR_TREVOR
			REQUEST_MODEL(PLAYER_ZERO)
			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			REQUEST_ANIM_DICT("missmic1leadinoutmic_1_int")
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_MICHAEL1_RELEASE_ASSETS()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[0])
			TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	REMOVE_ANIM_DICT("missmic1leadinoutmic_1_int")
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_MICHAEL1_HAVE_ASSETS_LOADED()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			RETURN TRUE
		BREAK
		CASE CHAR_TREVOR
			IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, << -804.200, 172.400, 72.850 >>)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			OR NOT HAS_ANIM_DICT_LOADED("missmic1leadinoutmic_1_int")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_MICHAEL1_CREATE()

	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_1), 5.0, TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	ADD_RELATIONSHIP_GROUP("MICHAEL_1_TS_RELGROUP", g_sTriggerSceneAssets.relGroup)

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			//
		BREAK
		CASE CHAR_TREVOR
		
			//only create michael's tailgater when there is no other one in the world
			//check this in case player as trevor is somehow using michael's car
			IF GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL) = 0
		
				//remove vehicle gen car and disable the vehicle gen
				DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
			
				//create Michael's car
				//get information about vehicle gen at Michael's house
				VEHICLE_GEN_DATA_STRUCT sVehicleGenDataStruct
				GET_VEHICLE_GEN_DATA(sVehicleGenDataStruct, VEHGEN_MICHAEL_SAVEHOUSE)
				IF CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[0], CHAR_MICHAEL, sVehicleGenDataStruct.coords, sVehicleGenDataStruct.heading)
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0])
					SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[0], FALSE)
				ENDIF
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
				
			ENDIF
			
			//A dummy Michael has already been created invisible during the loading process.
			//Reveal and configure him and start him playing his anims.
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
				TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)

				TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missmic1leadinoutmic_1_int", "_leadin_michael",
										<< -803.850, 172.265, 71.845 >>, << 0.0, 0.0, 110.0 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
										AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION,
										0, EULER_YXZ, AIK_DISABLE_LEG_IK)
			ENDIF
			SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_TREVOR, SAVEHOUSE_MICHAEL_BH, TRUE)
		BREAK
		
	ENDSWITCH

	TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = FALSE

	//commenting out for B*1878567
	//loading the cutscene will be requested in the update proc, as it needs to happen closer to mission trigger due to streaming on next gen
	//Start preloading intro cutscene for this mission.
	//MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MICHAEL_1, "MIC_1_INT", CS_1|CS_2|CS_3|CS_4, CS_NONE, CS_2|CS_3|CS_4|CS_5)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_MICHAEL1_RELEASE()

	IF TS_MICHAEL1_MUSIC_PLAYING
		IF TRIGGER_MUSIC_EVENT("MIC1_LEFT_HOUSE")
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_LEFT_HOUSE.")
			#ENDIF
		ENDIF
	ENDIF

	//Release Michael's car.
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])

	//Release Michael.
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)

	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_TREVOR, SAVEHOUSE_MICHAEL_BH, FALSE)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = FALSE
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_MICHAEL1_DELETE()
	
	IF TS_MICHAEL1_MUSIC_PLAYING
		IF TRIGGER_MUSIC_EVENT("MIC1_LEFT_HOUSE")
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_LEFT_HOUSE.")
			#ENDIF
		ENDIF
	ENDIF

	//Delete Michael's car.
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])

	//Delete Michael.
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)

	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_TREVOR, SAVEHOUSE_MICHAEL_BH, FALSE)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
	TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = FALSE
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_MICHAEL1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_1, GET_CURRENT_PLAYER_PED_INT())
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MICHAEL1_TRIGGER_DIST)
		#ENDIF
	
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				FLOAT fDistanceSquaredFromTrigger 
				fDistanceSquaredFromTrigger  = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
				IF fDistanceSquaredFromTrigger < (TS_MICHAEL1_TRIGGER_DIST*TS_MICHAEL1_TRIGGER_DIST)
				
					IF TS_MICHAEL1_MUSIC_PLAYING
						TRIGGER_MUSIC_EVENT("MIC1_INTRO_CS_BEGINS")
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_INTRO_CS_BEGINS.")
						#ENDIF
					ENDIF
				
					RETURN TRUE
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
						IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missmic1leadinoutmic_1_int", "_leadin_michael")
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.521179,168.317169,71.334709>>, <<-805.366150,178.339600,74.834709>>, 9.0) //living room
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-808.519714,176.214539,70.834709>>, <<-809.006653,181.628891,75.403091>>, 2.0) //main hall
							OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-806.494019,179.495087,71.153091>>, <<-808.482422,184.822479,75.559669>>, 5.0) //stairs
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_MICHAEL1_HAS_BEEN_DISRUPTED()

	INT i

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		FOR i = 0 TO 3
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[i])
				OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[i])
					
					IF i = 0
						g_bMichaelHasBeenDisrupted = TRUE
						
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": g_bMichaelHasBeenDisrupted = TRUE")
						#ENDIF
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_MICHAEL1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_MICHAEL1_UPDATE()

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		
			IF NOT TS_MICHAEL1_MUSIC_PLAYING
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0) //kitchen
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0) //hall
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0) //kitchen/living room
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0) //living room
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5) //lower staircase
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0) //hall/living room
					IF TRIGGER_MUSIC_EVENT("MIC1_PRE_MISSION_MUSIC")
						TS_MICHAEL1_MUSIC_PLAYING = TRUE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_PRE_MISSION_MUSIC.")
						#ENDIF
					ENDIF
				ENDIF
				
			ELSE
				
				IF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-799.520630,187.521057,71.105469>>, <<-795.681763,177.692230,74.834709>>, 6.0) //kitchen
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-804.679260,183.043533,70.847778>>, <<-816.280396,178.596970,75.153091>>, 6.0) //hall
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-803.700134,185.895218,71.105469>>, <<-800.207031,176.436371,74.834709>>, 4.0) //kitchen/living room
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-801.559814,168.317978,71.334709>>, <<-805.525635,178.113297,74.834709>>, 8.0) //living room
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-811.660278,182.266098,70.653091>>, <<-805.412842,184.612473,76.002815>>, 2.5) //lower staircase
				AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-807.917725,176.369095,70.834709>>, <<-808.785706,178.632828,74.653091>>, 2.0) //hall/living room
					IF TRIGGER_MUSIC_EVENT("MIC1_LEFT_HOUSE")
						TS_MICHAEL1_MUSIC_PLAYING = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": Triggering music event MIC1_LEFT_HOUSE.")
						#ENDIF
					ENDIF
				ENDIF
			
			ENDIF
			
		BREAK
		CASE CHAR_TREVOR
		
			SET_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_SAVEHOUSE)
		
			SET_DOOR_STATE(DOORNAME_M_MANSION_G1, 	DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
		BREAK
	ENDSWITCH
	
	IF NOT TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_1, GET_CURRENT_PLAYER_PED_INT())) <= 30.0*30.0
			//Start preloading intro cutscene for this mission.
			MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MICHAEL_1, "MIC_1_INT", CS_1|CS_2|CS_3|CS_4, CS_NONE, CS_2|CS_3|CS_4|CS_5)
			TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = TRUE
		ENDIF
	ELSE
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_1, GET_CURRENT_PLAYER_PED_INT())) > 100.0*100.0
			//Unload intro cutscene for this mission.
			MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
			TS_MICHAEL1_CUTSCENE_REQUEST_TRIGGERED = FALSE
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_MICHAEL1_AMBIENT_UPDATE()

	

ENDPROC
