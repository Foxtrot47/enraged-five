//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Michael 4 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "blip_control_public.sch"


CONST_FLOAT 	TS_MICHAEL4_STREAM_IN_DIST		180.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_MICHAEL4_STREAM_OUT_DIST		190.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_MICHAEL4_TRIGGER_DIST		2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_MICHAEL4_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_MICHAEL4_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

//BOOL bRejectHelpShown = FALSE


//CONST_INT MOVP_ANTON			0
//CONST_INT MOVP_CAMMAN			1
//CONST_INT MOVP_LAZLOW			2
//
//CONST_INT MOVP_AELLA			3
//CONST_INT MOVP_BENTON			4
//CONST_INT MOVP_MOLLY			5
//CONST_INT MOVP_RUBEN			6
//
//
//CONST_INT MOVP_FAN_A			7
//CONST_INT MOVP_PAP1				8
//CONST_INT MOVP_PAP2				9
//CONST_INT MOVP_PAP3				10
//CONST_INT MOVP_STAR_A			11
//
//CONST_INT MOVP_FEM_A			12
//CONST_INT MOVP_MILTON			13
//CONST_INT MOVP_PAP1_A			14
//CONST_INT MOVP_PAP2_A			15
//CONST_INT MOVP_PAP3_A			16
//
//CONST_INT MOVP_JIMMY			17
//
////X = 300.500
////Y = 203.510
////Z = 103.4
////<<300.5, 203.51, 103.4>>
//INT navBlockArea
//INT iSceneM4LeadIn = 0
//BOOL bJimmyComponentsSet = FALSE


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_MICHAEL4_RESET()
	//bRejectHelpShown = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_MICHAEL4_REQUEST_ASSETS()

//	REQUEST_ANIM_DICT("missmic4premiere")
//	REQUEST_ANIM_DICT("missmic4ig_4")
//	REQUEST_MODEL(IG_LAZLOW)
//	REQUEST_MODEL(IG_JIMMYDISANTO)
//	REQUEST_MODEL(U_M_Y_ANTONB)
//	REQUEST_MODEL(IG_Milton)	
//	REQUEST_MODEL(A_F_Y_SCDressy_01)
//	REQUEST_MODEL(A_M_M_Paparazzi_01)
//	REQUEST_MODEL(S_M_Y_Grip_01)
//	
//	REQUEST_MODEL(Prop_V_Cam_01)
//	REQUEST_MODEL(P_ING_MICROPHONEL_01)
//	REQUEST_MODEL(prop_pap_camera_01)
//	
//	REQUEST_IPL("redCarpet")
	
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_MICHAEL4_RELEASE_ASSETS()
	//REMOVE_IPL("redCarpet")
	
//	REMOVE_ANIM_DICT("missmic4premiere")
//	REMOVE_ANIM_DICT("missmic4ig_4")
//	SET_MODEL_AS_NO_LONGER_NEEDED(IG_LAZLOW)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IG_JIMMYDISANTO)
//	SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_ANTONB)
//	SET_MODEL_AS_NO_LONGER_NEEDED(IG_Milton)
//	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_SCDressy_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_Paparazzi_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_Grip_01)
//	
//	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_V_Cam_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(P_ING_MICROPHONEL_01)
//	SET_MODEL_AS_NO_LONGER_NEEDED(prop_pap_camera_01)
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_MICHAEL4_HAVE_ASSETS_LOADED()

//	IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[MOVP_JIMMY], CHAR_JIMMY, <<295.4207, 185.1796, 103.2333>>)
//		IF NOT bJimmyComponentsSet
//			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[MOVP_JIMMY])
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
//				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,10), 2, 0, 0) //(decl)
//				bJimmyComponentsSet = TRUE
//			ENDIF
//		ENDIF
//		RETURN FALSE
//	ENDIF
//	
//	IF HAS_ANIM_DICT_LOADED("missmic4premiere")
//	AND HAS_ANIM_DICT_LOADED("missmic4ig_4")
//	AND HAS_MODEL_LOADED(IG_LAZLOW)
//	IF HAS_MODEL_LOADED(IG_JIMMYDISANTO)
////	AND HAS_MODEL_LOADED(U_M_Y_ANTONB)
////	AND HAS_MODEL_LOADED(IG_Milton)
////	AND HAS_MODEL_LOADED(A_F_Y_SCDressy_01)
////	AND HAS_MODEL_LOADED(A_M_M_Paparazzi_01)
////	AND HAS_MODEL_LOADED(S_M_Y_Grip_01)
////	AND HAS_MODEL_LOADED(Prop_V_Cam_01)
////	AND HAS_MODEL_LOADED(P_ING_MICROPHONEL_01)
////	AND HAS_MODEL_LOADED(prop_pap_camera_01)
		RETURN TRUE
//	ENDIF
//	
	//RETURN FALSE

RETURN TRUE
	
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_MICHAEL4_CREATE()

//	iSceneM4LeadIn = 0
//	
//	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO, <<-716.5614, -159.6554, 35.9891>>, 96.9674)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], INT_TO_ENUM(PED_COMPONENT,10), 2, 0, 0) //(decl)
//	
//
//	SET_REDUCE_PED_MODEL_BUDGET(TRUE)
//	SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
//	SET_PED_POPULATION_BUDGET(2)
//	SET_VEHICLE_POPULATION_BUDGET(2)
//
//	ADD_RELATIONSHIP_GROUP("Leadin", g_sTriggerSceneAssets.relGroup)
//
//	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
//	SET_PED_NON_CREATION_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>)
//	SET_ROADS_IN_AREA(<<292.424530,172.560867,109.715904>> - <<61.000000,50.250000,8.500000>>, <<292.424530,172.560867,109.715904>> + <<61.000000,50.250000,8.500000>>, FALSE)
//	CLEAR_AREA(<<297.8264, 191.4778, 103.3186>>, 80.0, TRUE)
//		
//	navBlockArea =  ADD_NAVMESH_BLOCKING_OBJECT(<<292.424530,172.560867,109.715904>>, <<61.000000,50.250000,8.500000>>, 0.0)
//	
//
//	//Lazlow interview Anton               
//	g_sTriggerSceneAssets.ped[MOVP_ANTON]  = CREATE_PED(PEDTYPE_MISSION, U_M_Y_ANTONB,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_CAMMAN] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Grip_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_LAZLOW] = CREATE_PED(PEDTYPE_MISSION, IG_LAZLOW,<<300.5, 203.51, 103.4>>)
//	
//	//4stars reacting to paps/crowd
//	g_sTriggerSceneAssets.ped[MOVP_AELLA]  = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_BENTON] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_MOLLY]  = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_RUBEN]  = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	
//	//Actress posing
//	g_sTriggerSceneAssets.ped[MOVP_FAN_A]  = CREATE_PED(PEDTYPE_MISSION, IG_LAZLOW,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP1]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP2]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP3]   = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_STAR_A] = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	
//	//Milton posing
//	g_sTriggerSceneAssets.ped[MOVP_FEM_A]  = CREATE_PED(PEDTYPE_MISSION, A_F_Y_SCDressy_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_MILTON] = CREATE_PED(PEDTYPE_MISSION, IG_Milton,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP1_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP2_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	g_sTriggerSceneAssets.ped[MOVP_PAP3_A] = CREATE_PED(PEDTYPE_MISSION, A_M_M_Paparazzi_01,<<300.5, 203.51, 103.4>>)
//	
//	
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
//	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_MILTON], INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
//	
//	
//	TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[MOVP_JIMMY])
//	bJimmyComponentsSet = FALSE
////	g_sTriggerSceneAssets.ped[MOVP_JIMMY] = CREATE_PED(PEDTYPE_MISSION, IG_JIMMYDISANTO,<<295.4207, 185.1796, 103.2333>>, 141.9553)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,3), 2, 0, 0) //(uppr)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
////	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[MOVP_JIMMY], INT_TO_ENUM(PED_COMPONENT,10), 2, 0, 0) //(decl)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missmic4ig_4", "base", <<295.4207, 185.1796, 104.1333>>, <<0.0, 0.0,  141.9553>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION)
//	
//	//Start preloading intro cutscene for this mission.
//	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_MICHAEL_4,
//												"MIC_4_INT",
//												CS_ALL,
//												CS_NONE,
//												CS_NONE)
//	
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_MICHAEL_4, "Jimmy", g_sTriggerSceneAssets.ped[MOVP_JIMMY])
//
//	
//	//Camera men
//	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(Prop_V_Cam_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0],g_sTriggerSceneAssets.ped[MOVP_CAMMAN], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_CAMMAN], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	
//	g_sTriggerSceneAssets.object[7] = CREATE_OBJECT(P_ING_MICROPHONEL_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[7],g_sTriggerSceneAssets.ped[MOVP_LAZLOW], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_LAZLOW], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	
//	
//	//paps 1
//	g_sTriggerSceneAssets.object[1] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[1],g_sTriggerSceneAssets.ped[MOVP_PAP1], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP1], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	g_sTriggerSceneAssets.object[2] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[2],g_sTriggerSceneAssets.ped[MOVP_PAP2], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP2], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	g_sTriggerSceneAssets.object[3] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[3],g_sTriggerSceneAssets.ped[MOVP_PAP3], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP3], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	
//	//paps 2
//	g_sTriggerSceneAssets.object[4] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[4],g_sTriggerSceneAssets.ped[MOVP_PAP1_A], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP1_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	g_sTriggerSceneAssets.object[5] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[5],g_sTriggerSceneAssets.ped[MOVP_PAP2_A], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP2_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	g_sTriggerSceneAssets.object[6] = CREATE_OBJECT(prop_pap_camera_01, <<300.5, 203.51, 103.4>>)
//	ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[6],g_sTriggerSceneAssets.ped[MOVP_PAP3_A], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[MOVP_PAP3_A], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//	
//	
//	
//	
//	
//	//Lazlow interview Anton  
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_ANTON], "missmic4premiere", "Interview_Short_Anton", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_CAMMAN], "missmic4premiere", "Interview_Short_Camman", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_LAZLOW], "missmic4premiere", "Interview_Short_Lazlow", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//
//
//	
//	//4stars reacting to paps/crowd
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_AELLA], "missmic4premiere", "Prem_4stars_A_Aella", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_BENTON], "missmic4premiere", "Prem_4stars_A_Benton", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_MOLLY], "missmic4premiere", "Prem_4stars_A_Molly", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_RUBEN], "missmic4premiere", "Prem_4stars_A_Ruben", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//
//
//	//Actress posing
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_FAN_A], "missmic4premiere", "Prem_Actress_fan_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP1], "missmic4premiere", "Prem_Actress_pap1_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP2], "missmic4premiere", "Prem_Actress_pap2_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP3], "missmic4premiere", "Prem_Actress_pap3_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_STAR_A], "missmic4premiere", "Prem_Actress_star_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	
//	
//	//Milton posing
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_FEM_A], "missmic4premiere", "Prem_Milton_Fem_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_MILTON], "missmic4premiere", "Prem_Milton_Milton_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//                                                            INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP1_A], "missmic4premiere", "Prem_Milton_Pap1_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP2_A], "missmic4premiere", "Prem_Milton_Pap2_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[MOVP_PAP3_A], "missmic4premiere", "Prem_Milton_Pap3_A", <<300.5, 203.51, 103.4>>, <<0.0, 0.0, 0.0>>,
//															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, 
//                                                            AF_NOT_INTERRUPTABLE | AF_USE_KINEMATIC_PHYSICS | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION, 0.462)
//	
//
//	INT i
//	FOR i = 0 TO MOVP_JIMMY - 1
//		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[i], g_sTriggerSceneAssets.relGroup)
//		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[i], TRUE)
//	ENDFOR
//	
//	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[MOVP_JIMMY], g_sTriggerSceneAssets.relGroup)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_MICHAEL4_RELEASE()

//	INT i
//	FOR i = 0 TO MOVP_JIMMY
//		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
//	ENDFOR
//	
//	FOR i = 0 TO 7
//		TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[i])
//	ENDFOR
//	
//	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
//
//	REMOVE_NAVMESH_BLOCKING_OBJECT(navBlockArea)
//
//	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
//	
//	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
//	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
//
//	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
//	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
//	SET_PED_POPULATION_BUDGET(3)
//	SET_VEHICLE_POPULATION_BUDGET(3)
//
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
//	bJimmyComponentsSet = FALSE
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_MICHAEL4_DELETE()
	
//	INT i
//	FOR i = 0 TO MOVP_JIMMY
//		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
//	ENDFOR
//	
//	FOR i = 0 TO 7
//		TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[i])
//	ENDFOR
//
//	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
//	
//	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
//	
//	REMOVE_NAVMESH_BLOCKING_OBJECT(navBlockArea)
//	
//	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
//	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
//	
//	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
//	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
//	SET_PED_POPULATION_BUDGET(3)
//	SET_VEHICLE_POPULATION_BUDGET(3)
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
//	bJimmyComponentsSet = FALSE
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_MICHAEL4_HAS_BEEN_TRIGGERED()
//	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_4)
//	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		//Render trigger zone debug.
//		#IF IS_DEBUG_BUILD
//			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MICHAEL4_TRIGGER_DIST)
//		#ENDIF
//	
//		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
//		IF fDistanceSquaredFromTrigger < (TS_MICHAEL4_TRIGGER_DIST*TS_MICHAEL4_TRIGGER_DIST)
//		OR (iSceneM4LeadIn > 0 AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<293.256836,179.302856,102.287781>>, <<301.882446,202.810303,107.641953>>, 17.500000))
//		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<297.140503,190.210205,103.063286>>, <<301.466858,202.287949,106.890182>>, 15.000000)
//
//			RETURN TRUE
//			
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
///    
///    				// check if Michael is wearing his tuxedo
////				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
////					RETURN TRUE
////				ELIF NOT bRejectHelpShown
////					SWITCH GET_FLOW_HELP_MESSAGE_STATUS("TRIG_SUIT")
////						CASE FHS_EXPIRED
////							ADD_HELP_TO_FLOW_QUEUE("TRIG_SUIT", FHP_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
////						BREAK
////						CASE FHS_DISPLAYED
////							//Flash the closest high end clothes shop to encourage the player to visit it.
////							IF IS_STATIC_BLIP_CURRENTLY_VISIBLE_TO_SYSTEM(STATIC_BLIP_SHOP_CLOTHES_H_03_MW)
////								IF IS_STATIC_BLIP_CURRENTLY_VISIBLE(STATIC_BLIP_SHOP_CLOTHES_H_03_MW)
////									SET_BLIP_FLASHES(g_GameBlips[STATIC_BLIP_SHOP_CLOTHES_H_03_MW].biBlip, TRUE)
////									SET_BLIP_FLASH_TIMER(g_GameBlips[STATIC_BLIP_SHOP_CLOTHES_H_03_MW].biBlip, 10000)
////								ENDIF
////							ENDIF
////							bRejectHelpShown = TRUE
////						BREAK
////					ENDSWITCH
////				ENDIF
////			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE

	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_MICHAEL_4)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_MICHAEL4_TRIGGER_DIST)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (TS_MICHAEL4_TRIGGER_DIST*TS_MICHAEL4_TRIGGER_DIST)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_MICHAEL4_HAS_BEEN_DISRUPTED()

//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		//Check for Jimmy being harmed.
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[MOVP_JIMMY])
//			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[MOVP_JIMMY])
//				RETURN TRUE
//			ELSE
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[MOVP_JIMMY], PLAYER_PED_ID())
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		//Check for Lazlow being harmed.
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[MOVP_LAZLOW])
//			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[MOVP_LAZLOW])
//				RETURN TRUE
//			ELSE
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[MOVP_LAZLOW], PLAYER_PED_ID())
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		//Check for the camera man being harmed.
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[MOVP_CAMMAN])
//			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[MOVP_CAMMAN])
//				RETURN TRUE
//			ELSE
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[MOVP_CAMMAN], PLAYER_PED_ID())
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		//Check for Anton being harmed.
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[MOVP_ANTON])
//			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[MOVP_ANTON])
//				RETURN TRUE
//			ELSE
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[MOVP_ANTON], PLAYER_PED_ID())
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Check for weapon useage in the general area.
//	IF IS_BULLET_IN_ANGLED_AREA(<<290.376190,197.558456,102.873940>>, <<308.642365,190.877441,107.234367>>, 23.000000)
//	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<290.376190,197.558456,102.873940>>, <<308.642365,190.877441,107.234367>>, 23.000000)
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_MICHAEL4_IS_BLOCKED()
	IF NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_H_01_BH)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_MICHAEL4_UPDATE()

//	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
//	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<293.256836,179.302856,102.287781>>, <<301.882446,202.810303,107.641953>>, 17.500000)
//		UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
//	ENDIF
//	
//	SWITCH iSceneM4LeadIn
//	
//		CASE 0
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<295.291077,184.093262,102.266258>>, <<301.882446,202.810303,107.641953>>, 17.500000)
//				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
//				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
//				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, g_sTriggerSceneAssets.ped[0], "JIMMY")
//				
//				TASK_FOLLOW_TO_OFFSET_OF_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID(), <<-1.5, 0.0, 0.0>>, PEDMOVE_WALK)
//				
//				iSceneM4LeadIn++		
//			ENDIF
//		BREAK
//		
//		CASE 1
//			IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "SOL5AUD", "SOL5_PROUD", CONV_PRIORITY_VERY_HIGH)
//				iSceneM4LeadIn++
//			ENDIF
//		BREAK
//	
//	ENDSWITCH
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_MICHAEL4_AMBIENT_UPDATE()
ENDPROC
