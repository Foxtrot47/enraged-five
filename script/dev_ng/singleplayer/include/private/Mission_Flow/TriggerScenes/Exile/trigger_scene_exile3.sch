//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Template - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_EXILE3_STREAM_IN_DIST		100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_EXILE3_STREAM_OUT_DIST		110.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_EXILE3_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_EXILE3_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_EXILE3_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

ENUM EXILE_3_TRIGGER_BIT_ENUM
	EXILE_3_TRIGGER_BIT_DONE_PATRICIA_GREETING 		= BIT0,
	EXILE_3_TRIGGER_BIT_DONE_MONTHLY_TRAIN_ANIM		= BIT1,
	EXILE_3_TRIGGER_BIT_DONE_MONTHLY_TRAIN_SPEECH 	= BIT2
ENDENUM

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_EXILE3_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_EXILE3_REQUEST_ASSETS()
	REQUEST_ANIM_DICT("MISSEXILE3")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_EXILE3_RELEASE_ASSETS()
	REMOVE_ANIM_DICT("MISSEXILE3")	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_EXILE3_HAVE_ASSETS_LOADED()
	IF HAS_ANIM_DICT_LOADED("MISSEXILE3")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_EXILE3_CREATE()
	g_bAllowAmbientFriendLaunching = FALSE
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_EXILE3_RELEASE()
	g_bAllowAmbientFriendLaunching = TRUE
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_EXILE3_DELETE()
	g_bAllowAmbientFriendLaunching = TRUE	
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_EXILE3_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_EXILE_3)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (TS_EXILE3_TRIGGER_DIST*TS_EXILE3_TRIGGER_DIST)
			RETURN TRUE
		//B* 1798196: Make sure Trevor's door is closed when nearby the trailer
		ELIF fDistanceSquaredFromTrigger < TS_EXILE3_STREAM_IN_DIST * TS_EXILE3_STREAM_IN_DIST	
			IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(GET_HASH_KEY("DOORHASH_T_TRAILER_CS")))>0.1
				SET_DOOR_STATE(DOORNAME_T_TRAILER_CS,DOORSTATE_UNLOCKED)
				DOOR_SYSTEM_SET_OPEN_RATIO(GET_HASH_KEY("DOORHASH_T_TRAILER_CS"), 0.0,TRUE,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_EXILE3_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_EXILE3_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_EXILE3_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_EXILE3_AMBIENT_UPDATE()
ENDPROC
