//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Exile 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_EXILE1_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_EXILE1_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_EXILE1_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_EXILE1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_EXILE1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

//STRING strCutscene1 = "exile_1_intp1_a"
//STRING strCutscene2 = "exile_1_int"

//STRING strDialogueText = "EXL1AUD"*/

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_EXILE1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_EXILE1_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missexile1_cargoplaneleadinoutexile_1_intleadin")
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, Prop_Phone_ING_03)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_PHONE_ING)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_EXILE1_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_EXILE1_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_EXILE1_CREATE()
	
	//Start preloading intro cutscene for this mission.
	//MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_EXILE_1, "exile_1_int", CS_NONE, CS_NONE, CS_NONE)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_EXILE1_RELEASE()
	//MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_EXILE1_DELETE()
	//MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_EXILE1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_EXILE_1)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_EXILE1_TRIGGER_DIST)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		
		IF fDistanceSquaredFromTrigger < (7*7)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)			
		ENDIF		
		
		IF fDistanceSquaredFromTrigger < (TS_EXILE1_TRIGGER_DIST*TS_EXILE1_TRIGGER_DIST)
			SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_EXILE1_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_EXILE1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_EXILE1_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_EXILE1_AMBIENT_UPDATE()
ENDPROC
