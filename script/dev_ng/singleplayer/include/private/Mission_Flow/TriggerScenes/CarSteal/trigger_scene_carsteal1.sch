//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Carsteal 1 - Trigger Scene Data							│
//│																				│
//│								Date: 13/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//│								ped[0]  Devin									│
//│								ped[1]  Molly									│
//│								ped[2]  Driver (Felon)							│
//│								ped[3]  Franklin								│
//│								ped[4]  Trevor									│
//│								veh[0]  Car (Felon)								│
//│								veh[1]  Franklin's Car (F620)					│
//│								veh[2]  Devin's Car (Adder)						│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_CAR1_STREAM_IN_DIST		300.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_CAR1_STREAM_OUT_DIST		335.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_CAR1_TRIGGER_DIST		5.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_CAR1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_CAR1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

SELECTOR_PED_STRUCT sSelectorPeds

INT sceneCar1LeadIn

//Scene state bit flags.
CONST_INT CAR1_DAVE_REACTING				0
CONST_INT CAR1_LOADED_CUT_APPROACH_RIGHT	1
CONST_INT CAR1_LOADED_CUT_APPROACH_LEFT		2

INT iCar1SceneState = 0

BOOL bCar1FocusPush
INT iCar1OutOfVehicle

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_CAR1_RESET()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
	
	g_sTriggerSceneAssets.id = -1
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_CAR1_REQUEST_ASSETS()
	REQUEST_NPC_PED_MODEL(CHAR_DEVIN)								// Only 8 slots in a medium load queue.
	REQUEST_ANIM_DICT("misscarsteal1leadinout@i_fought_the_law") 	// Name too long for load queue.
	
	g_sTriggerSceneAssets.loadQueue.iLastFrame = GET_FRAME_COUNT() 	// Wait until the next frame before making more load requests.
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_MOLLY))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, A_M_Y_Business_01)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, FELON)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, F620)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, ADDER)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "misscarsteal1leadin")
	
	PRINTLN("Requesting Assets")
ENDPROC

/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_CAR1_RELEASE_ASSETS()
	SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_DEVIN)
	REMOVE_ANIM_DICT("misscarsteal1leadinout@i_fought_the_law")
	
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_CAR1_HAVE_ASSETS_LOADED()

	IF NOT HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN FALSE
	ENDIF

	//Used to preload variations on an invisible ped before the scene is created!
	TSPedCompRequest sDevinComponents[3]
	TS_STORE_PED_COMP_REQUEST(sDevinComponents[0], PED_COMP_TORSO, 2, 0)
	TS_STORE_PED_COMP_REQUEST(sDevinComponents[1], PED_COMP_LEG, 2, 0)
	TS_STORE_PED_COMP_REQUEST(sDevinComponents[2], PED_COMP_DECL, 1, 0)
	TSPedCompRequest sBusinessManComponents[3]
	TS_STORE_PED_COMP_REQUEST(sBusinessManComponents[0], PED_COMP_HEAD, 1, 1)
	TS_STORE_PED_COMP_REQUEST(sBusinessManComponents[1], PED_COMP_TORSO, 0, 0)
	TS_STORE_PED_COMP_REQUEST(sBusinessManComponents[2], PED_COMP_LEG, 0, 0)
	TSPedCompRequest sDummyComponents[1]
	sDummyComponents[0].texture = -1
	TSPedPropRequest sMollyProp[1]
	sMollyProp[0].position = ANCHOR_EYES
	sMollyProp[0].prop = 0
	sMollyProp[0].texture = 0
	TSPedPropRequest sDummyProp[1]
	sDummyProp[0].prop = -1
	
	//NB. CHAR_ESTATE_AGENT uses model A_M_Y_BUSINESS_01.
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_DEVIN, <<111.4789, -396.7308, 40.2624>>, sDevinComponents, sDummyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[1], CHAR_MOLLY, <<112.5587, -400.4127, 40.2624>>, sDummyComponents, sMollyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[2], CHAR_ESTATE_AGENT, <<111.4789, -396.7308, 40.2624>>, sBusinessManComponents, sDummyProp)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_TREVOR
			IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_DEVIN, <<111.4789, -396.7308, 40.2624>>, sDevinComponents, sDummyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[1], CHAR_MOLLY, <<112.5587, -400.4127, 40.2624>>, sDummyComponents, sMollyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[2], CHAR_ESTATE_AGENT, <<111.4789, -396.7308, 40.2624>>, sBusinessManComponents, sDummyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[3], CHAR_FRANKLIN, <<111.661, -400.209, 40.243>>)
				RETURN FALSE
			ENDIF
		BREAK
		CASE CHAR_MICHAEL
			IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_DEVIN, <<111.4789, -396.7308, 40.2624>>, sDevinComponents, sDummyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[1], CHAR_MOLLY, <<112.5587, -400.4127, 40.2624>>, sDummyComponents, sMollyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_NPC_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[2], CHAR_ESTATE_AGENT, <<111.4789, -396.7308, 40.2624>>, sBusinessManComponents, sDummyProp)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[3], CHAR_FRANKLIN, <<111.661, -400.209, 40.243>>)
			OR NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[4], CHAR_TREVOR, <<111.7596, -399.2278, 40.2648>>)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT HAS_ANIM_DICT_LOADED("misscarsteal1leadinout@i_fought_the_law")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_CAR1_CREATE()
	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_1), 14.0, TRUE)
	
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<104.747391, -389.131927, 80.369141>> - <<65.0, 100.0, 50.0>>, <<104.747391, -389.131927, 80.369141>> + <<65.0, 100.0, 50.0>>)
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
		SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[0], -140.8511)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[1])
		SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[1], -0.4412)
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	ENDIF
	
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(FELON, <<110.7381, -401.2210, 40.9425>>, -112.0840)
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[2])
		SET_PED_INTO_VEHICLE(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.veh[0])
		TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)
	ENDIF
	
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(F620, <<117.1050, -414.6006, 40.1527>>, 268.8413)
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[1], TRUE)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[1], 43, 43)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[1], 48, 48)
	
	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(ADDER, <<119.2013, -398.4865, 40.1208>>, -171.4098)
	SET_VEHICLE_HAS_STRONG_AXLES(g_sTriggerSceneAssets.veh[2], TRUE)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[2], 0, 0)
	SET_VEHICLE_EXTRA_COLOURS(g_sTriggerSceneAssets.veh[2], 0, 0)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
	OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	AND DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))
		INT sceneDevin = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneDevin, "misscarsteal1leadin", "devon_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneDevin, TRUE)
		
		INT sceneMolly = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneMolly, "misscarsteal1leadin", "molly_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneMolly, TRUE)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		INT sceneDevin = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneDevin, "misscarsteal1leadinout@i_fought_the_law", "leadin_loop_devin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneDevin, TRUE)
		
		INT sceneMolly = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneMolly, "misscarsteal1leadinout@i_fought_the_law", "leadin_loop_molly", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneMolly, TRUE)
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[3])
			SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[3], 148.2)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
			INT sceneFranklin = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3], sceneFranklin, "misscarsteal1leadinout@i_fought_the_law", "leadin_loop_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneFranklin, TRUE)
		ENDIF
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		INT sceneDevin = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneDevin, "misscarsteal1leadinout@i_fought_the_law", "MicArrive_LeadIn_LOOP_DEVIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneDevin, TRUE)
		
		INT sceneMolly = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
		TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneMolly, "misscarsteal1leadinout@i_fought_the_law", "MicArrive_LeadIn_LOOP_MOLLY", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneMolly, TRUE)
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[3])
			SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[3], 148.2)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
			INT sceneFranklin = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3], sceneFranklin, "misscarsteal1leadinout@i_fought_the_law", "MicArrive_LeadIn_LOOP_FRANKLIN", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneFranklin, TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
			TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[4])
			SET_ENTITY_HEADING(g_sTriggerSceneAssets.ped[4], 42.4422)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
			INT sceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[4], sceneTrevor, "misscarsteal1leadinout@i_fought_the_law", "MicArrive_LeadIn_LOOP_TREVOR", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(sceneTrevor, TRUE)
		ENDIF
	ENDIF
	
	REMOVE_ANIM_DICT("misscarsteal1leadin")
	REMOVE_ANIM_DICT("misscarsteal1leadinout@i_fought_the_law")

	SET_MODEL_AS_NO_LONGER_NEEDED(FELON)
	SET_MODEL_AS_NO_LONGER_NEEDED(F620)
	SET_MODEL_AS_NO_LONGER_NEEDED(ADDER)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_CAR1_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[4])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	STOP_GAMEPLAY_HINT(FALSE)
	
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_CAR1_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[2])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[3])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[4])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	STOP_GAMEPLAY_HINT(FALSE)
	
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
	CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_CAR1_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_1)
		
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_CAR1_TRIGGER_DIST)
		#ENDIF
		
		//Load the correct cutscene variation depending on which side the player is closer to.
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fDistFromRight = VDIST2(vPlayerPos, <<108.38885, -396.61292, 40.27208>>)	//Behind
		FLOAT fDistFromLeft = VDIST2(vPlayerPos, <<114.03026, -399.25626, 40.26558>>)	//In Front
		FLOAT fDifference = fDistFromRight - fDistFromLeft
		
		#IF IS_DEBUG_BUILD
		IF g_bTriggerDebugOn
			DRAW_DEBUG_TEXT("Right", <<108.38885, -396.61292, 40.27208>>, 0, 0, 255, 255)
			DRAW_DEBUG_SPHERE(<<108.38885, -396.61292, 40.27208>>, 0.25, 0, 255, 0, 65) 
			DRAW_DEBUG_TEXT("Left", <<114.03026, -399.25626, 40.26558>>, 0, 0, 255, 255)
			DRAW_DEBUG_SPHERE(<<114.03026, -399.25626, 40.26558>>, 0.25, 0, 255, 0, 65) 
		ENDIF
		#ENDIF
		
		//Look for the player locking in when they walk over the mission blip.
		IF NOT g_bPlayerLockedInToTrigger
			IF fDifference > 0.5 //Closer to left.
				IF IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
					MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
					CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
				ENDIF
				
				IF NOT IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
					IF NOT g_bFlowCleanupIntroCutscene
						//Start preloading intro cutscene for this mission.
						MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_CARSTEAL_1,
																	"CAR_1_INT_CONCAT",
																	CS_4,
																	CS_2 | CS_3 | CS_4,
																	CS_3 | CS_4)
						
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "DEVIN", g_sTriggerSceneAssets.ped[0])
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_CARSTEAL_1, "DEVIN", PED_COMP_SPECIAL, 1, 0)
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "MOLLY", g_sTriggerSceneAssets.ped[1])
						
						SET_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
					ENDIF
				ENDIF
			ELIF fDifference < -0.5 //Closer to right.
				IF IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
					MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
					CLEAR_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
				ENDIF
				
				IF NOT IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
					IF NOT g_bFlowCleanupIntroCutscene
						//Start preloading intro cutscene for this mission.
						MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_CARSTEAL_1,
																	"CAR_1_INT_CONCAT",
																	CS_4,
																	CS_1 | CS_2 | CS_3 | CS_4,
																	CS_3 | CS_4)
						
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "DEVIN", g_sTriggerSceneAssets.ped[0])
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_CARSTEAL_1, "DEVIN", PED_COMP_SPECIAL, 1, 0)
						MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "MOLLY", g_sTriggerSceneAssets.ped[1])
						
						SET_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT)
					ENDIF
				ENDIF
			ELIF NOT IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT) AND NOT IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
				IF NOT g_bFlowCleanupIntroCutscene
					//Start preloading intro cutscene for this mission.
					MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_CARSTEAL_1,
																"CAR_1_INT_CONCAT",
																CS_4,
																CS_2 | CS_3 | CS_4,
																CS_3 | CS_4)
					
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "DEVIN", g_sTriggerSceneAssets.ped[0])
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_CARSTEAL_1, "DEVIN", PED_COMP_SPECIAL, 1, 0)
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_CARSTEAL_1, "MOLLY", g_sTriggerSceneAssets.ped[1])
					
					SET_BIT(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_RIGHT) OR IS_BIT_SET(iCar1SceneState, CAR1_LOADED_CUT_APPROACH_LEFT)
				IF ((GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) <> CAM_VIEW_MODE_FIRST_PERSON
				AND ((GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<107.184433, -434.609833, 37.239693>>, <<124.478577, -385.553314, 52.271389>>, 40.0))
				OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<109.346207, -419.774963, 37.358894>>, <<121.540504, -384.988220, 52.587891>>, 32.0))))
				OR (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<111.346207, -404.774963, 37.358894>>, <<118.540504, -383.988220, 52.587891>>, 25.0)))
				AND NOT IS_TIMED_EXPLOSIVE_PROJECTILE_IN_ANGLED_AREA(<<107.184433, -434.609833, 37.239693>>, <<124.478577, -385.553314, 52.271389>>, 40.0)
				AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						iCar1OutOfVehicle = -1
					ENDIF
					
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					
					CLEAR_AREA_OF_PROJECTILES(vTriggerPosition, 100.0)
					
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], 15000)
				ENDIF
			ENDIF
		//Player locked in. 
		ELSE
			IF iCar1OutOfVehicle = -1
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iCar1OutOfVehicle = GET_GAME_TIMER()
			ENDIF
			
			IF NOT bCar1FocusPush
				IF ((GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<96.086937, -384.519012, 39.304409>>, <<131.700592, -409.523163, 46.061520>>, 20.0))
				OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<98.809998, -386.643707, 39.887123>>, <<124.452202, -403.864441, 50.203625>>, 20.0)))
				AND iCar1OutOfVehicle != -1 AND (GET_GAME_TIMER() - iCar1OutOfVehicle) > 1500
					IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), PEDMOVE_WALK, DEFAULT, DEFAULT, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
						SET_GAMEPLAY_COORD_HINT(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), -1, 2500)
						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.35)
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.915)
						ELSE
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(0.015)
						ENDIF
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(-0.02)
						SET_GAMEPLAY_HINT_FOV(30.0)
					ENDIF
					
					g_sTriggerSceneAssets.id = GET_GAME_TIMER()
					
					bCar1FocusPush = TRUE
				ENDIF
			ELSE
				IF IS_GAMEPLAY_HINT_ACTIVE()
					IF IS_PED_RAGDOLL(PLAYER_PED_ID())
						STOP_GAMEPLAY_HINT()
					ENDIF
				ENDIF
				
				//Trigger 3.5 seconds after the focus push.
//				IF (GET_GAME_TIMER() - g_sTriggerSceneAssets.id) > 3500
//					RETURN TRUE
//				ENDIF
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneCar1LeadIn)
				IF HAS_THIS_CUTSCENE_LOADED("CAR_1_INT_CONCAT")
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<98.809998, -386.643707, 39.887123>>, <<124.452202, -403.864441, 50.203625>>, 20.0)
						sceneCar1LeadIn = CREATE_SYNCHRONIZED_SCENE(<<110.0, -399.0, 40.230>>, <<0.0, 0.0, 66.500>>)
						
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], sceneCar1LeadIn, "misscarsteal1leadin", "DEVON_Fra_enters_leadin", SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], sceneCar1LeadIn, "misscarsteal1leadin", "MOLLY_Fra_enters_leadin", SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						
						SET_SYNCHRONIZED_SCENE_PHASE(sceneCar1LeadIn, 0.0)
						
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneCar1LeadIn, FALSE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneCar1LeadIn, TRUE)
						
						ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 3, PLAYER_PED_ID(), "FRANKLIN")
						
						CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "CST3AUD", "CST3_INT_LI", CONV_PRIORITY_MEDIUM)
					ENDIF
				ENDIF
			ENDIF
			
			IF ((GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<103.871902, -393.572815, 37.765583>>, <<117.710983, -400.916565, 50.263367>>, 14.0)
			OR (IS_SYNCHRONIZED_SCENE_RUNNING(sceneCar1LeadIn)
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneCar1LeadIn) >= 0.5)))
			OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<110.284775, -406.437805, 45.264786>>, <<116.966278, -392.045410, 39.711278>>, 20.0))
			OR (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<110.284775, -406.437805, 45.264786>>, <<116.966278, -392.045410, 39.711278>>, 24.0)))
			OR NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<106.038277, -438.193268, 37.253963>>, <<126.994453, -378.399811, 52.260574>>, 50.0)
				RETURN TRUE
			ENDIF
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_CAR1_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
		
		//Check for scene peds being injured or harmed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[index])
					OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[index])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for scene vehicle being damaged or destroyed.
		REPEAT COUNT_OF(g_sTriggerSceneAssets.veh) index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
				OR NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(<<107.184433, -434.609833, 37.239693>>, <<124.478577, -385.553314, 52.271389>>, 40.0)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<107.184433, -434.609833, 37.239693>>, <<124.478577, -385.553314, 52.271389>>, 40.0)
		RETURN TRUE
	ENDIF
	
	VEHICLE_INDEX vehClosest = GET_CLOSEST_VEHICLE(<<114.57894, -399.86444, 40.26400>>, 12.0, DUMMY_MODEL_FOR_SCRIPT, ENUM_TO_INT(VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER | VEHICLE_SEARCH_FLAG_RETURN_HELICOPTORS_ONLY | VEHICLE_SEARCH_FLAG_RETURN_PLANES_ONLY))
	
	IF DOES_ENTITY_EXIST(vehClosest) 
		PRINTLN("vehClosest = ", NATIVE_TO_INT(vehClosest), " [model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClosest)), "]")
		
		IF (IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehClosest)) OR IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehClosest)))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_CAR1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_CAR1_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_CAR1_AMBIENT_UPDATE()
ENDPROC
