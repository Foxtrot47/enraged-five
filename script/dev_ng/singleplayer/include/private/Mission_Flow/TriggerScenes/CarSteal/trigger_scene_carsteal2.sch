//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Carsteal2 - Trigger Scene Data							│
//│																				│
//│				Author: Ben Rollinson				Date: 04/10/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								veh[0] = Chopper								│
//│								ped[0] = Pilot									│
//│								ped[1] = Trevor									│
//╘═════════════════════════════════════════════════════════════════════════════╛


USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "friends_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_CAR2_STREAM_IN_DIST			100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_CAR2_STREAM_OUT_DIST			120.0	// Distance at which the scene is deleted and unloaded.
CONST_FLOAT		TS_CAR2_TRIGGER_DIST_TREVOR		10.0	// Distance from blip at which the mission triggers for Trevor.
CONST_FLOAT		TS_CAR2_TRIGGER_DIST_FRANKLIN	5.0	// Distance from blip at which the mission triggers for Franklin.

CONST_FLOAT		TS_CAR2_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_CAR2_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
///    

PED_INDEX cop[4],civ[3]
INTERIOR_INSTANCE_INDEX int_police
int iCreateSceneFlag
int iWantedLevel

PROC TS_CAR2_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_CAR2_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missheistdockssetup1ig_13@start_idle")
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, S_M_Y_COP_01)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, A_M_Y_GenStreet_02)
	LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(g_sTriggerSceneAssets.loadQueue, "CHHEIST", MISSION_TEXT_SLOT)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_CAR2_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_CAR2_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_CAR2_CREATE()
	iWantedLevel = 0
	SET_CREATE_RANDOM_COPS(FALSE)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_CAR2_RELEASE()
	int i
	repeat count_of(cop) i
		IF DOES_ENTITY_EXIST(cop[i])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(cop[i])
				SET_PED_KEEP_TASK(cop[i],TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(cop[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	repeat count_of(civ) i
		IF DOES_ENTITY_EXIST(civ[i])
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(civ[i])
				SET_PED_KEEP_TASK(civ[i],TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(civ[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_CAR2_DELETE()
	int i
	repeat count_of(cop) i
		IF DOES_ENTITY_EXIST(cop[i])
			DELETE_PED(cop[i])
		ENDIF
	ENDREPEAT
	
	repeat count_of(civ) i
		IF DOES_ENTITY_EXIST(civ[i])
			DELETE_PED(civ[i])
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_CAR2_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_2, GET_CURRENT_PLAYER_PED_INT())

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			#IF IS_DEBUG_BUILD
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_CAR2_TRIGGER_DIST_TREVOR)
			#ENDIF
			IF (IS_PED_ON_FOOT(player_ped_id()) AND fDistanceSquaredFromTrigger < (TS_CAR2_TRIGGER_DIST_TREVOR*TS_CAR2_TRIGGER_DIST_TREVOR))
			OR IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<409.332886,-969.273315,27.566418>>, <<408.970184,-982.836914,32.206459>>, 6.625000)
				IF GET_PLAYER_WANTED_LEVEL(player_id()) > 0
					PRINT_HELP("CHRETURN",3000)		
					IF GET_PLAYER_WANTED_LEVEL(player_id()) > iWantedLevel
						iWantedLevel = GET_PLAYER_WANTED_LEVEL(player_id())									
					ENDIF
				ELSE
					g_sTriggerSceneAssets.ped[0] = cop[0]
					g_sTriggerSceneAssets.ped[1] = cop[1]
					g_sTriggerSceneAssets.ped[2] = cop[2]
					g_sTriggerSceneAssets.ped[6] = cop[3]
					g_sTriggerSceneAssets.ped[3] = civ[0]
					g_sTriggerSceneAssets.ped[4] = civ[1]
					g_sTriggerSceneAssets.ped[5] = civ[2]
				
					RETURN TRUE
				ENDIF
			ENDIF
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			#IF IS_DEBUG_BUILD
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_CAR2_TRIGGER_DIST_FRANKLIN)
			#ENDIF
			IF fDistanceSquaredFromTrigger < (TS_CAR2_TRIGGER_DIST_FRANKLIN*TS_CAR2_TRIGGER_DIST_FRANKLIN)
			
				
				g_sTriggerSceneAssets.ped[0] = cop[0]
				g_sTriggerSceneAssets.ped[1] = cop[1]
				g_sTriggerSceneAssets.ped[2] = cop[2]
				g_sTriggerSceneAssets.ped[6] = cop[3]
				g_sTriggerSceneAssets.ped[3] = civ[0]
				g_sTriggerSceneAssets.ped[4] = civ[1]
				g_sTriggerSceneAssets.ped[5] = civ[2]
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_CAR2_HAS_BEEN_DISRUPTED()
	
	//player creates explosino
	IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)
		entity_index aEnt
		aEnt = GET_OWNER_OF_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE,<<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)
		
		IF GET_PED_INDEX_FROM_ENTITY_INDEX(aEnt) = PLAYER_PED_ID()
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_MAX_WANTED_LEVEL(5)
			SET_PLAYER_WANTED_LEVEL(player_id(),3)
			SET_PLAYER_WANTED_LEVEL_NOW(player_id())
			RETURN TRUE
		ENDIF
	ENDIF
	
	//player shooting near pol dept
	IF IS_PED_SHOOTING(player_ped_id())
		IF IS_ENTITY_IN_ANGLED_AREA( player_ped_id(), <<443.060303,-943.353638,12.574972>>, <<441.947815,-1061.535400,55.027191>>, 130.812500)				
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_MAX_WANTED_LEVEL(5)
			SET_PLAYER_WANTED_LEVEL(player_id(),3)
			SET_PLAYER_WANTED_LEVEL_NOW(player_id())	
			RETURN TRUE
		ENDIF
	ENDIF
	
	int ij
	FOR ij = 0 to 7
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[ij])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[ij])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[ij],PLAYER_PED_ID())								
				OR IS_PED_IN_COMBAT(g_sTriggerSceneAssets.ped[ij])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_CAR2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_CAR2_UPDATE()
/*
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_CAR_STEAL_2, GET_CURRENT_PLAYER_PED_INT())

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition) < 40
				STREAMVOL_ID iStreamFrustum
				iStreamFrustum = STREAMVOL_CREATE_FRUSTUM(<<1393.3628, -2052.5813, 65.4054>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<2.5107, -0.0000, 51.1167>>) ,150,FLAG_MAPDATA,STREAMVOL_LOD_FLAG_HIGH)
			ENDIF
		ENDIF
	ENDIF
*/		

	IF iWantedLevel > 0
		IF GET_PLAYER_WANTED_LEVEL(player_id()) > iWantedLevel
			iWantedLevel = GET_PLAYER_WANTED_LEVEL(player_id())
		ENDIF
		
		SET_PLAYER_WANTED_LEVEL(player_id(),iWantedLevel)
		SET_PLAYER_WANTED_LEVEL_NOW(player_id())
	ENDIF


	SWITCH iCreateSceneFlag
		CASE 0
			int_police = GET_INTERIOR_AT_COORDS(<<441.02, -978.93, 30.69>>)									
			IF IS_VALID_INTERIOR(int_police)
				PIN_INTERIOR_IN_MEMORY(int_police)
				iCreateSceneFlag++
			endif
		BREAK
		
		case 1
			IF IS_INTERIOR_READY(int_police)			
				SET_INTERIOR_ACTIVE(int_police, TRUE)	
				iCreateSceneFlag++
			ENDIF
		BREAK
		
		case 2

			//CLEAR_AREA(<< 1388.0295, -2067.3911, 50.9981 >>,30,TRUE)
			
			ADD_SCENARIO_BLOCKING_AREA(<<442.040558,-976.513245,30.252041>>-<<3.312500,3.500000,1.125000>>,<<442.040558,-976.513245,30.252041>>+<<3.312500,3.500000,1.125000>>)
			ADD_SCENARIO_BLOCKING_AREA(<<454.516663,-990.567993,30.689508>>-<<6.750000,3.500000,1.000000>>,<<454.516663,-990.567993,30.689508>>+<<6.750000,3.500000,1.000000>>) 
					
			CLEAR_AREA_OF_PEDS(<< 454.6653, -990.8345, 29.7694 >>,10.0)
			CLEAR_AREA_OF_PEDS(<<441.65, -976.73, 29.69>>,4.0)
				
			ADD_SCENARIO_BLOCKING_AREA(<<442.42, -986.07, 30.15>>-<<0.5,0.5,1>>,<<442.42, -986.07, 30.15>>+<<0.5,0.5,1>>)
			ADD_SCENARIO_BLOCKING_AREA(<<439.31, -985.99, 30.15>>-<<0.5,0.5,1>>,<<439.31, -985.99, 30.15>>+<<0.5,0.5,1>>)
			
			cop[0] = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<441.0267, -978.2040, 29.6895>>,192) 
			cop[1] = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<< 440.2506, -975.6328, 29.6895 >>,356)
			cop[2] = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<454.1487, -979.8940, 29.6896>>, 105.1729)
			cop[3] = CREATE_PED(PEDTYPE_COP,S_M_Y_COP_01,<<450.2071, -992.9072, 29.6896>>, 316.4481)
		
			
			SET_PED_DEFAULT_COMPONENT_VARIATION(cop[0])
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,0), 2, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(cop[0], INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
			
			SET_PED_DEFAULT_COMPONENT_VARIATION(cop[1])
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(cop[1], INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)

			SET_PED_DEFAULT_COMPONENT_VARIATION(cop[2])
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(cop[2], INT_TO_ENUM(PED_COMPONENT,10), 1, 1, 0) //(decl)
			
		//	SET_PED_DEFAULT_COMPONENT_VARIATION(ped[ped_cop4].id)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(cop[0],FALSE)

			STOP_PED_SPEAKING(cop[0],TRUE)
			STOP_PED_SPEAKING(cop[1],TRUE)
			STOP_PED_SPEAKING(cop[2],TRUE)
			STOP_PED_SPEAKING(cop[3],TRUE)

			GIVE_WEAPON_TO_PED(cop[0],WEAPONTYPE_PISTOL,infinite_ammo)
			GIVE_WEAPON_TO_PED(cop[1],WEAPONTYPE_PISTOL,infinite_ammo)
			GIVE_WEAPON_TO_PED(cop[2],WEAPONTYPE_PISTOL,infinite_ammo)
			GIVE_WEAPON_TO_PED(cop[3],WEAPONTYPE_PISTOL,infinite_ammo)
			TASK_START_SCENARIO_IN_PLACE(cop[0],"WORLD_HUMAN_HANG_OUT_STREET")
			TASK_START_SCENARIO_IN_PLACE(cop[1],"WORLD_HUMAN_CLIPBOARD")
			TASK_START_SCENARIO_IN_PLACE(cop[2],"WORLD_HUMAN_CLIPBOARD")
			

			civ[0] = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<436.9079, -986.8186, 29.6895>>, 71.5386)
			civ[1] = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<443.4680, -981.7770, 29.6895>>,30)
			civ[2] = CREATE_PED(PEDTYPE_CIVMALE,A_M_Y_GenStreet_02,<<444.9140, -988.1146, 29.6895>>, 71.5386)

			SET_PED_DEFAULT_COMPONENT_VARIATION(civ[0])
			SET_PED_COMPONENT_VARIATION(civ[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(civ[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(civ[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			
			SET_PED_DEFAULT_COMPONENT_VARIATION(civ[1])
			SET_PED_COMPONENT_VARIATION(civ[1], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(civ[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(civ[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)

			SET_PED_DEFAULT_COMPONENT_VARIATION(civ[2])
			SET_PED_COMPONENT_VARIATION(civ[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(civ[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(civ[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			
			TASK_START_SCENARIO_IN_PLACE(civ[0],"WORLD_HUMAN_BUM_STANDING")
			TASK_START_SCENARIO_IN_PLACE(civ[1],"WORLD_HUMAN_STAND_IMPATIENT")							
			
			
			INT scenei
			scenei = CREATE_SYNCHRONIZED_SCENE(<< 447.140, -988.574, 29.688 >>, << -0.000, 0.000, -80.150 >>)
			TASK_SYNCHRONIZED_SCENE (cop[3], scenei, "missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_guard1", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
			TASK_SYNCHRONIZED_SCENE (civ[2], scenei, "missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_dockworker", INSTANT_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
			SET_SYNCHRONIZED_SCENE_LOOPED(scenei,true)
												
			
			iCreateSceneFlag++
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_CAR2_AMBIENT_UPDATE()
ENDPROC
