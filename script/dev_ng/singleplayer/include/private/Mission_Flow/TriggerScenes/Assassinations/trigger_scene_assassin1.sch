//╘═════════════════════════════════════════════════════════════════════════════╛
//│						Template - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "friends_public.sch"


CONST_FLOAT 	TS_ASSASSIN1_STREAM_IN_DIST				200.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_ASSASSIN1_STREAM_OUT_DIST			210.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_ASSASSIN1_TRIGGER_DIST				4.75	// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_ASSASSIN1_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_ASSASSIN1_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

//STRUCT DEBUG_POS_DATA
//		BOOL bTurnOnDebugPos
//		VECTOR vDebugVector
//		FLOAT fDebugFloat
//		FLOAT fDebugX
//		FLOAT fDebugY
//		FLOAT fdebugWidth
//		FLOAT fdebugHeight
//		FLOAT fRotateX
//		FLOAT fRotateY
//		FLOAT fRotateZ
//		FLOAT fOffsetX
//		FLOAT fOffsetY
//		FLOAT fOffsetZ
//		FLOAT fFOV
//		ENTITY_INDEX myObject
//		VECTOR myObjectPos
//	ENDSTRUCT
//DEBUG_POS_DATA myDebugData
//
//PROC AddWidgets(DEBUG_POS_DATA& myData)
//	myData = myData
//	START_WIDGET_GROUP("Assassin")
//		ADD_WIDGET_BOOL("turn on positioning", myData.bTurnOnDebugPos)
//		ADD_WIDGET_VECTOR_SLIDER("DebugPosition", myData.vDebugVector, -2000, 2000, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("DebugRotation", myData.fDebugFloat, -360, 360, 1.0)
//		ADD_WIDGET_FLOAT_SLIDER("DebugX", myData.fDebugX, 0, 1, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("DebugY", myData.fDebugY, 0, 1, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("DebugWidth", myData.fdebugWidth, 0, 2, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("DebugHeight", myData.fdebugHeight, 0, 2, 0.01)
//		ADD_WIDGET_FLOAT_SLIDER("RotateX", myData.fRotateX, 0, 360, 1)
//		ADD_WIDGET_FLOAT_SLIDER("RotateY", myData.fRotateY, 0, 360, 1)
//		ADD_WIDGET_FLOAT_SLIDER("RotateZ", myData.fRotateZ, 0, 360, 1)
//		ADD_WIDGET_FLOAT_SLIDER("OffsetX", myData.fOffsetX, -2000, 2000, 0.1)
//		ADD_WIDGET_FLOAT_SLIDER("OffsetY", myData.fOffsetY, -2000, 2000, 0.1)
//		ADD_WIDGET_FLOAT_SLIDER("OffsetZ", myData.fOffsetZ, -2000, 2000, 0.1)
//		ADD_WIDGET_FLOAT_SLIDER("FOV", myData.fFOV, 0, 125, 0.1)
//	STOP_WIDGET_GROUP()
//ENDPROC

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_ASSASSIN1_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_ASSASSIN1_REQUEST_ASSETS()
	
	REQUEST_ANIM_DICT("oddjobs@assassinate@hotel@leaning@")

ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_ASSASSIN1_RELEASE_ASSETS()
	
	REMOVE_ANIM_DICT("oddjobs@assassinate@hotel@leaning@")
	
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_ASSASSIN1_HAVE_ASSETS_LOADED()
	IF HAS_ANIM_DICT_LOADED("oddjobs@assassinate@hotel@leaning@")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_ASSASSIN1_CREATE()

	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-1509.8623, -947.0913, 8.2495>>+<<2,2,2>>,<<-1509.8623, -947.0913, 8.2495>>-<<2,2,2>>)
	CLEAR_AREA(<<-1509.8623, -947.0913, 6.2495>>, 20.0, TRUE)
	
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_ASSASSIN_1, "ASS_INT_2_ALT1", CS_NONE, CS_ALL, CS_NONE)
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_ASSASSIN1_RELEASE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_ASSASSIN1_DELETE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_ASSASSIN1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_ASSASSIN_1)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_BIT_SET(g_sMissionStaticData[SP_MISSION_ASSASSIN_1].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			//Render trigger zone debug.
			#IF IS_DEBUG_BUILD
				DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_ASSASSIN1_TRIGGER_DIST)
			#ENDIF
			
			FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
			IF fDistanceSquaredFromTrigger < (TS_ASSASSIN1_TRIGGER_DIST*TS_ASSASSIN1_TRIGGER_DIST)
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_ASSASSIN1_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_ASSASSIN1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_ASSASSIN1_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_ASSASSIN1_AMBIENT_UPDATE()
ENDPROC
