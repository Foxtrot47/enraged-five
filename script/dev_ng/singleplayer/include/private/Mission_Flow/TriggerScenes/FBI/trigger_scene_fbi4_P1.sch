//╒═════════════════════════════════════════════════════════════════════════════╕
//│					FBI4 Prep 1 - Trigger Scene Data							│
//│																				│
//│								Date: 01/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//│								veh[0]	= GARBAGE TRUCK							│
//│								ped[0]	= BIN MAN								│
//│								ped[1]	= BIN MAN FRIEND						│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "flow_mission_trigger_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI4_P1_STREAM_IN_DIST			250.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4_P1_STREAM_OUT_DIST			270.0	// Distance at which the scene is deleted and unloaded.
CONST_FLOAT		TS_FBI4_P1_TRIGGER_DIST				75.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT		TS_FBI4_P1_FLYING_TRIGGER_DIST		110.0	// Distance from trigger's blip at which the scene triggers when in a flying vehicle.
CONST_FLOAT 	TS_FBI4_P1_BATTLE_BUDDY_CALL_DIST	500.0 	// Distance from trigger's blip at which the player can call in battle buddies.

//Distance at which friends who can't come on the mission will bail from the player group.
CONST_FLOAT		TS_FBI4_P1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4_P1_FRIEND_ACCEPT_BITS	BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

TS_MOVING_BLIP_STRUCT TS_FBI4_P1_MOVING_BLIP

BOOL TS_TRASH_TRIGGERED = FALSE

ENUM MOVER_BLIP_STATES
	MBS_INIT,
	MBS_UPDATE
ENDENUM

MOVER_BLIP_STATES eMoverBlipState = MBS_INIT


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI4_P1_RESET()
	CPRINTLN(DEBUG_MISSION, " = TS_FBI4_P1_RESET()")
	TS_TRASH_TRIGGERED = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4_P1_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TRASH)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, S_M_Y_GARBAGE)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4_P1_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4_P1_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
	AND GET_IS_WAYPOINT_RECORDING_LOADED("FBI4_P1_BlipRoute1")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_ANGLE(VECTOR pos1, VECTOR vLookAt)
	float fAngle
	float dX = vLookAt.x - pos1.x
	float dY = vLookAt.y - pos1.y

	if dY != 0
		fAngle = ATAN2(dX,dY)
	ELSE
		if dX < 0
			fAngle = -90
		ELSE
			fAngle = 90
		ENDIF
	ENDIF

	//flip for world heading system
	fAngle *= -1.0
	
	RETURN fAngle
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4_P1_CREATE()
	CLEAR_AREA(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P1].vCoords[0], 33.437500, TRUE)
	
	VECTOR vNextPoint
	WAYPOINT_RECORDING_GET_COORD( "FBI4_P1_BlipRoute1", TS_FBI4_P1_MOVING_BLIP.iTargetNode+1, vNextPoint)

	//TRUCK
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(TRASH, g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P1].vCoords[0], GET_ANGLE(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P1].vCoords[0], vNextPoint))

	//GARBAGE DUDE
	g_sTriggerSceneAssets.ped[0] = CREATE_PED_INSIDE_VEHICLE(g_sTriggerSceneAssets.veh[0] ,PEDTYPE_MISSION, S_M_Y_GARBAGE)
	
	g_sTriggerSceneAssets.ped[1] = CREATE_PED_INSIDE_VEHICLE(g_sTriggerSceneAssets.veh[0] ,PEDTYPE_MISSION, S_M_Y_GARBAGE, VS_FRONT_RIGHT)
	
	TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0], "FBI4_P1_BlipRoute1", DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, TS_FBI4_P1_MOVING_BLIP.iTargetNode+1)
	CPRINTLN(DEBUG_MISSION, " - TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY")
	TS_FBI4_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, g_sTriggerSceneAssets.veh[0])

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4_P1_RELEASE()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			TRIGGER_SCENE_RELEASE_PED_FLEE_IN_VEHICLE(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[0])
		ELSE
			TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
		ENDIF
	ENDIF

	IF NOT TS_TRASH_TRIGGERED
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, FALSE)
	ENDIF
	
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[1])
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4_P1_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[1])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	IF NOT TS_TRASH_TRIGGERED
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, FALSE)
	ENDIF
	CPRINTLN(DEBUG_MISSION, "TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT")
	TS_FBI4_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, "FBI4_P1_BlipRoute1", 0.5, -1)
ENDPROC

PROC FBIP1_RELEASE_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
				RELEASE_BATTLEBUDDY(hBuddy)
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4_P1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			IF g_bTriggerDebugOn
				if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4_P1_FLYING_TRIGGER_DIST)
				else
					DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4_P1_TRIGGER_DIST)
				ENDIF
			endif
		#ENDIF

		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		if IS_PED_IN_FLYING_VEHICLE(player_ped_id())
			IF fDistanceSquaredFromTrigger < (TS_FBI4_P1_FLYING_TRIGGER_DIST*TS_FBI4_P1_FLYING_TRIGGER_DIST)	
				FBIP1_RELEASE_BATTLE_BUDDIES()
				TS_TRASH_TRIGGERED = TRUE
				CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
				RETURN TRUE
			ENDIF
		else
			IF fDistanceSquaredFromTrigger < (TS_FBI4_P1_TRIGGER_DIST*TS_FBI4_P1_TRIGGER_DIST)
				FBIP1_RELEASE_BATTLE_BUDDIES()
				TS_TRASH_TRIGGERED = TRUE
				CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
				
				RETURN TRUE
			ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				FBIP1_RELEASE_BATTLE_BUDDIES()
				TS_TRASH_TRIGGERED = TRUE
				RETURN IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRASH)
			ENDIF
		endif
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4_P1_HAS_BEEN_DISRUPTED()
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			TS_FBI4_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, "FBI4_P1_BlipRoute1", 0.5, -1)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4_P1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

FUNC BOOL FBIP1_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		
		IF IS_PED_IN_ANY_VEHICLE(hPed)
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(hPed))
			AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(hPed))
			AND NOT IS_ENTITY_AT_COORD(GET_VEHICLE_PED_IS_IN(hPed),  <<-401.4687, -2168.1863, 9.3184>>, <<2,2,2>>)
						
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(hPed))
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				
					IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(hPed), VS_DRIVER) = hPed

						RETURN TRUE
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FBIP1_MONITOR_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
			
				// Does script need to take control of buddy and drive to dest?
				IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
					IF FBIP1_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
						IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
							SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
							CLEAR_PED_TASKS(hBuddy)
						ENDIF
					ENDIF
				ENDIF
			
			ELSE

				// Does script still need to take control of buddy and drive to dest?
				IF FBIP1_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
					
					// Buddy drives truck to destination
					IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
						VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1)
						TASK_VEHICLE_MISSION_COORS_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), vTriggerPosition, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 2.0, 10.0)
					ENDIF
				
				ELSE
					RELEASE_BATTLEBUDDY(hBuddy)
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene. 
PROC TS_FBI4_P1_UPDATE()
	FBIP1_MONITOR_BATTLE_BUDDIES()
	IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
	AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		IF NOT TS_UPDATE_MOVING_BLIP(TS_FBI4_P1_MOVING_BLIP)
			TS_FBI4_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, g_sTriggerSceneAssets.veh[0])
		ENDIF
	ENDIF
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
ENDPROC

/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4_P1_AMBIENT_UPDATE()	
		SWITCH eMoverBlipState
			CASE MBS_INIT
				REQUEST_WAYPOINT_RECORDING("FBI4_P1_BlipRoute1")
				IF GET_IS_WAYPOINT_RECORDING_LOADED("FBI4_P1_BlipRoute1")
					CPRINTLN(DEBUG_MISSION, "g_sTriggerSceneAssets.flag - TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT")
					TS_FBI4_P1_MOVING_BLIP = TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_MISSION_FBI_OFFICERS4_P1, "FBI4_P1_BlipRoute1", 0.5, 0)
					eMoverBlipState = MBS_UPDATE
				ENDIF
			BREAK
			
			CASE MBS_UPDATE 
				IF NOT TS_UPDATE_MOVING_BLIP(TS_FBI4_P1_MOVING_BLIP)
					eMoverBlipState = MBS_INIT
				ENDIF
			BREAK
		ENDSWITCH
ENDPROC
