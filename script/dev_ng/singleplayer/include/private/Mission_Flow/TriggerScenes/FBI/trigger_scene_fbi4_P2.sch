//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Template - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//│					g_sTriggerSceneAssets.veh[0] - Tow Truck					│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "properties_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI4_P2_STREAM_IN_DIST			175.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4_P2_STREAM_OUT_DIST			185.0	// Distance at which the scene is deleted and unloaded	
CONST_FLOAT		TS_FBI4_P2_TRIGGER_DIST				75.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_FBI4_P2_BATTLE_BUDDY_CALL_DIST	500.0 	// Distance from trigger's blip at which the player can call in battle buddies.

CONST_FLOAT		TS_FBI4_P2_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4_P2_FRIEND_ACCEPT_BITS	BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.

VECTOR vAnimOffset = <<0.2,2.2,0>>

//Dan H: Blip index for the car blip
BLIP_INDEX bliTowTruck

BOOL TS_TOW_TRIGGERED = FALSE

PROC TS_FBI4_P2_RESET()
	TS_TOW_TRIGGERED = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4_P2_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TOWTRUCK)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PEYOTE)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, RUINER)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, S_M_M_TRUCKER_01)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, A_F_M_TOURIST_01)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "mini@repair")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4_P2_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4_P2_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4_P2_CREATE()

	//Clear area and create the tow truck.
	VECTOR vTruckPos = g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P2].vCoords[0]
	CLEAR_AREA(vTruckPos, 30.0, TRUE, TRUE)
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(TOWTRUCK, vTruckPos, 83.9445)
	
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(peyote,<<-412.3477, -2176.2617, 9.3184>>, 281.0786)

	g_sTriggerSceneAssets.veh[2] = CREATE_VEHICLE(RUINER, <<-386.5059, -2168.7383, 9.3184>>, 87.2085)
	
	g_sTriggerSceneAssets.ped[0] = CREATE_PED(PEDTYPE_MISSION, S_M_M_Trucker_01, <<-409.72052, -2175.85815, 9.31836>>, 121.7754)
	SET_PED_DEFAULT_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0])
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[1])
	AND DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[2])
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[1] ,SC_DOOR_BONNET, FALSE)//, TRUE)
		SET_VEHICLE_ENGINE_HEALTH(g_sTriggerSceneAssets.veh[1], 100)
		SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[1], 0)
		SET_VEHICLE_DOOR_OPEN(g_sTriggerSceneAssets.veh[1] ,SC_DOOR_FRONT_LEFT, TRUE)//, TRUE), TRUE)
	ENDIF

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[1])
			TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "mini@repair", "fixing_a_ped", GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[1], vAnimOffset), <<0,0,-GET_ENTITY_HEADING(g_sTriggerSceneAssets.veh[1])>>, INSTANT_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_sTriggerSceneAssets.ped[0], TRUE)
		ENDIF
		SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.1)

	ENDIF
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-417.19797, -2155.88574, 8.36266>>, <<-383.49078, -2188.51147, 12.92130>>)
ENDPROC

/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4_P2_RELEASE()
	INT i
	FOR i=0 TO 3
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[i])
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	IF NOT TS_TOW_TRIGGERED 
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
	ENDIF
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4_P2_DELETE()
	INT i
	FOR i=0 TO 3
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[i])
	ENDFOR
	IF NOT TS_TOW_TRIGGERED 
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, FALSE)
	ENDIF
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


PROC FBIP2_RELEASE_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
				RELEASE_BATTLEBUDDY(hBuddy)
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4_P2_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P2)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4_P2_TRIGGER_DIST)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (TS_FBI4_P2_TRIGGER_DIST*TS_FBI4_P2_TRIGGER_DIST)
			FBIP2_RELEASE_BATTLE_BUDDIES()
			TS_TOW_TRIGGERED = TRUE
			//Dan H Only create blip if one doesn't exist already
			IF NOT DOES_BLIP_EXIST(bliTowTruck) 
				bliTowTruck=CREATE_BLIP_FOR_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE)
			ENDIF
			RETURN TRUE
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			FBIP2_RELEASE_BATTLE_BUDDIES()
			TS_TOW_TRIGGERED = TRUE
			RETURN IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4_P2_HAS_BEEN_DISRUPTED()
	//Check for scene vehicles being damaged or destroyed.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4_P2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

FUNC BOOL FBIP2_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		
		IF IS_PED_IN_ANY_VEHICLE(hPed)
			IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(hPed))
			AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(hPed))
			AND NOT IS_ENTITY_AT_COORD(GET_VEHICLE_PED_IS_IN(hPed),  <<-401.4687, -2168.1863, 9.3184>>, <<2,2,2>>)
						
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(hPed))
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				
					IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(hPed), VS_DRIVER) = hPed

						RETURN TRUE
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC FBIP2_MONITOR_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
			
				// Does script need to take control of buddy and drive to dest?
				IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
					IF FBIP2_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
						IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
							SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
							CLEAR_PED_TASKS(hBuddy)
						ENDIF
					ENDIF
				ENDIF
			
			ELSE

				// Does script still need to take control of buddy and drive to dest?
				IF FBIP2_DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
					
					// Buddy drives truck to destination
					IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), <<-401.4687, -2168.1863, 9.3184>>, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 2.0, 10.0)
					ENDIF
				
				ELSE
					RELEASE_BATTLEBUDDY(hBuddy)
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FBI4_P2_UPDATE()
	
	FBIP2_MONITOR_BATTLE_BUDDIES()

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
		SET_PED_CAPSULE(g_sTriggerSceneAssets.ped[0], 0.1)
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4_P2_AMBIENT_UPDATE()

ENDPROC
