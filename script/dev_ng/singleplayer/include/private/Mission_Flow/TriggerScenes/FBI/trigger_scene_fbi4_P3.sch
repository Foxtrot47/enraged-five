//╒═════════════════════════════════════════════════════════════════════════════╕
//│					FBI Officers 4 Prep 3 - Trigger Scene Data					│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			//Keep a key of what the global indexes are used for here.			│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "respawn_location_private.sch"
USING "flow_mission_trigger_public.sch"


CONST_FLOAT 	TS_FBI4_P3_STREAM_IN_DIST			100.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4_P3_STREAM_OUT_DIST			110.0	// Distance at which the scene is deleted and unloaded	
CONST_FLOAT		TS_FBI4_P3_TRIGGER_DIST				10.0	// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FBI4_P3_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4_P3_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

CONST_INT MAX_INVALIDMODS			2

ENUM TRIGGER_STATE	
	TS_PLACE_FIRST_ITEM,
	TS_PLACE_GAS_AFT_CAR,
	TS_PLACE_CAR_AFT_GAS,
	TS_DONE
ENDENUM

ENUM COP_MONITOR
	CM_MONITER,
	CM_INIT_WANTED,
	CM_LOSING_WANTED	
ENDENUM
COP_MONITOR eCopMonitor = CM_MONITER
MODEL_NAMES mnInvalidModels[MAX_INVALIDMODS]
VECTOR vPositionEnteredCar = <<0, 0, 0>>
VECTOR vPositionExitedCar = vPositionEnteredCar
TRIGGER_STATE eTriggerState = TS_PLACE_FIRST_ITEM
BOOL bDisplayedJerrycanHelp = FALSE
VEHICLE_SETUP_STRUCT mDroppedOffCarStruct
VECTOR vCarPosition = <<0,0,0>>
FLOAT fCarHeading
VECTOR vJerryPos = <<0,0,0>>
STRING sUnsuitableVehicleHelp
STRING sTextBlock = "FBIPRAU"			//The Dialogue block for the mission
BOOL bIsCarValid = FALSE
BOOL bISHidingPosValid = FALSE
BOOL bInFIBVehicleBeforeComplete = FALSE
VECTOR vPotentialHidingPos = <<0,0,0>>


#IF IS_DEBUG_BUILD
	PROC UPDATE_DEBUG_PASS()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			IF NOT GET_MISSION_FLAG()
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						GET_VEHICLE_SETUP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), mDroppedOffCarStruct)
						vCarPosition = <<1117.4779, -1241.1418, 19.7933>>
						fCarHeading = 64.7935

						SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE, TRUE)

						g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

						CLEAR_HELP()
						PRINT_HELP("AM_H_FBIC8", DEFAULT_GOD_TEXT_TIME)
						eTriggerState = TS_DONE	
					ENDIF
				ELSE
					PRINT_HELP("AM_H_FBICDEBUG", DEFAULT_GOD_TEXT_TIME)
				ENDIF
			ENDIF
		ENDIF
	ENDPROC

#ENDIF

FUNC BOOL DO_MISSION_FLOW_HELP(STRING HelpText, FlowHelpPriority priority = FHP_HIGH, INT delay = 0, INT expire = 2000, INT display = DEFAULT_HELP_TEXT_TIME)

	SWITCH GET_FLOW_HELP_MESSAGE_STATUS(HelpText)//This vehicle is unsuitable to use for the getaway.
		CASE FHS_EXPIRED
			ADD_HELP_TO_FLOW_QUEUE(HelpText, priority, delay, expire, display)
		BREAK
		CASE FHS_DISPLAYED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the vehicle is a car 
///    Cant use a bike, plane, heli, horse, boat or police vehicle
/// RETURNS:
///    TRUE if the vehicle is none of the above
FUNC BOOL IS_VEHICLE_ROAD_VEHICLE()
	IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
	OR IS_PED_ON_MOUNT(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to make sure the vehicle's model is not on the invalid list
/// PARAMS:
///    Name - The name of the model to check
/// RETURNS:
///    TRUE if the model being checked is on the invalid list
FUNC BOOL DOES_CURRENT_MODEL_EQUAL_INVALID(MODEL_NAMES Name)
	INT i
	FOR i=0 TO (MAX_INVALIDMODS-1)
		IF mnInvalidModels[i] = Name

			IF i = 0
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_1)
				bInFIBVehicleBeforeComplete = TRUE
			ELIF i = 1
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_2)
				bInFIBVehicleBeforeComplete = TRUE
			ELSE
				bInFIBVehicleBeforeComplete = FALSE
			ENDIF

			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the vehicles stats are with in a certain level
///    And performs the DOES_CURRENT_MODEL_EQUAL_INVALID 
///    Acceleration needs to be over 0.165
///    Max est speed needs to be above 31
/// RETURNS:
///    TRUE if all the checks are passed ie model acceleration and top speed
FUNC BOOL IS_VEHICLE_MODEL_SUITABLE()	
	MODEL_NAMES mnTemp = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))

	IF NOT DOES_CURRENT_MODEL_EQUAL_INVALID(mnTemp)
		IF GET_VEHICLE_MODEL_ACCELERATION(mnTemp) > 0.165
		AND GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp) > 31
			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ACCELERATION  == ", GET_VEHICLE_MODEL_ACCELERATION(mnTemp)) 
				CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED  == ", GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp)) 
			#ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF

	sUnsuitableVehicleHelp = "AM_H_FBIC2"
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is in any of the preset vehicle setups for a character
///    eg: Michaels tailgater etc.
/// RETURNS:
///    TRUE if the player is in a vehicle that matches the setup details of
///    one of the player cars
FUNC BOOL IS_PLAYER_IN_ANY_PLAYER_VEHICLE()
	VEHICLE_SETUP_STRUCT Temp
	GET_VEHICLE_SETUP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), Temp)
	
	IF DOES_VEH_SETUP_STRUCT_MATCH_PLAYER_VEHICLE(CHAR_MICHAEL, Temp)
		sUnsuitableVehicleHelp = "AM_H_FBIC2A"
		RETURN TRUE
	ELIF DOES_VEH_SETUP_STRUCT_MATCH_PLAYER_VEHICLE(CHAR_FRANKLIN, Temp)
		sUnsuitableVehicleHelp = "AM_H_FBIC2B"
		RETURN TRUE
	ELIF DOES_VEH_SETUP_STRUCT_MATCH_PLAYER_VEHICLE(CHAR_TREVOR, Temp)
		sUnsuitableVehicleHelp = "AM_H_FBIC2C"
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PASSENGER_IN_TAXI()
	IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT) = PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT) = PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT) = PLAYER_PED_ID()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs checks to determine if the vehicle the player is in could
///    be used in the getaway
/// RETURNS:
///    TRUE if the vehicle the player is in can be used in the getaway
FUNC BOOL DO_VALID_GETAWAY_CAR_CHECK()
	IF IS_VEHICLE_MODEL_SUITABLE()
	AND NOT  IS_PLAYER_IN_ANY_PLAYER_VEHICLE()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CURRENT_VEHICLE_VALID()
	IF IS_VEHICLE_ROAD_VEHICLE()
	AND DO_VALID_GETAWAY_CAR_CHECK()
		bIsCarValid = TRUE 
	ELSE
		bIsCarValid = FALSE
	ENDIF
	
	RETURN bIsCarValid
ENDFUNC

/// PURPOSE:
///    Performs a check to see if the player is trying to park a vehicle within 200m of 
///    a save house
/// RETURNS:
///    TRUE if the player is within range
FUNC BOOL IS_SAFE_HOUSE_TOO_CLOSE()
	SAVEHOUSE_NAME_ENUM eSafeHouse = GET_CLOSEST_SAVEHOUSE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FLOAT dist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_sSavehouses[eSafeHouse].vSpawnCoords)
	IF ( dist <= 100*100)
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "IS_SAFE_HOUSE_TOO_CLOSE() RETURN TRUE  == ", dist) 
		#ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_TO_CLOSE_TO_FBI_LOCS()
	IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<855.72577, -2344.49072, 29.33145>>) <= 230*230) //HIEST location
		PRINT_HELP("AM_H_FBIC6A", DEFAULT_HELP_TEXT_TIME)//This location isn't discrete enough.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "TOO CLOSE TO HIEST location RETURN TRUE")
		#ENDIF
		RETURN TRUE
		
	ELIF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<1383.21021, -2068.01978, 51.87057>>) <= 170*170) //FBI LOT
		PRINT_HELP("AM_H_FBIC6B", DEFAULT_HELP_TEXT_TIME)//This location isn't discrete enough.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "TOO CLOSE TO FBI LOT() RETURN TRUE")
		#ENDIF
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if two entities are at similar heights
/// PARAMS:
///    e1 - the coords of the first entity passed in
///    e2 - the coords of the second entity passed in
/// RETURNS:
///    TRUE is the entities heights are within the defined tolerance
FUNC BOOL ARE_COORDS_AT_SAME_HEIGHT(VECTOR v1, VECTOR v2)
	FLOAT fDiff = ABSF(v1.z - v2.z)
	
	IF fDiff <= 5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks that the vehicle the player is using is alive
///    Can be disabled so that it will simply return true
/// PARAMS:
///    bCheckVehicle - Should we check for vehicle or simply
///    return true
/// RETURNS:
///    TRUE if the player is in a vehicle and the vehicle
///    is valid
FUNC BOOL DO_ALIVE_VALID_CHECKS(BOOL bCheckVehicle )
	IF bCheckVehicle
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs checks to see if the location the player has chosen is valid place for the
///    getaway vehicle/gas to be dumped.
///    Also displays help text telling the player why the hiding place isnt valid
/// PARAMS:
///    bCheckVehicle - Should we check for teh vehicle 
///    this just stops valid checks for the vehicle the player wont be in at this point 
/// RETURNS:
///    TRUE if the hiding position is valid
FUNC BOOL IS_HIDING_POS_VAILD(BOOL bCheckVehicle = TRUE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF DO_ALIVE_VALID_CHECKS(bCheckVehicle )
			//If is player stopped
			IF IS_PED_STOPPED(PLAYER_PED_ID())
				ENTITY_INDEX eiExclution
				Bool bPeds = TRUE
				IF bCheckVehicle
					eiExclution = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					bPeds = FALSE
				ELSE
					eiExclution = PLAYER_PED_ID()
				ENDIF
				IF NOT IS_POSITION_OCCUPIED(vPos, 2, FALSE, TRUE, bPeds, FALSE, FALSE, eiExclution)
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MISSION, "IS_AREA_OCCUPIED ===== FALSE") 
					#ENDIF
					IF bCheckVehicle
						IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_DRIVER)
							PRINT_HELP("AM_H_FBIC5", DEFAULT_HELP_TEXT_TIME)//This location is obstructed.
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MISSION, "IS_ENTRY_POINT_FOR_SEAT_CLEAR ====== FALSE")
							#ENDIF
							RETURN FALSE
						ENDIF

						VECTOR vCarRot = GET_ENTITY_ROTATION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF vCarRot.x >= 10
						OR vCarRot.x <= -10
							PRINT_HELP("AM_H_FBIC5A", DEFAULT_HELP_TEXT_TIME)//This location is too steep..
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== FALSE")
							#ENDIF
							RETURN FALSE
						ELIF vCarRot.y >= 10
						OR vCarRot.y <= -10
							PRINT_HELP("AM_H_FBIC5B", DEFAULT_HELP_TEXT_TIME)//This location is too uneven..
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== FALSE")
							#ENDIF
							RETURN FALSE
						ELSE
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== TRUE ==== ", vCarRot)
							#ENDIF
						ENDIF
					
						IF IS_ENTITY_UPSIDEDOWN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							PRINT_HELP("AM_H_FBIC7", DEFAULT_HELP_TEXT_TIME)//The vehicle is upsidedown.
							#IF IS_DEBUG_BUILD 
								CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_STUCK_ON_ROOF ====== FALSE")
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF

		 			VECTOR vRoadNode
					IF GET_CLOSEST_VEHICLE_NODE(vPos, vRoadNode, NF_NONE)
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_SPHERE(vRoadNode, 4, 255)
						#ENDIF
						IF NOT IS_SAFE_HOUSE_TOO_CLOSE()
							IF NOT IS_PLAYER_TO_CLOSE_TO_FBI_LOCS()
								IF ( VDIST2(vPos, vRoadNode) >= 10*10)
									#IF IS_DEBUG_BUILD 
										CPRINTLN(DEBUG_MISSION, "( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRoadNode) >= 10*10) ======== TRUE ", VDIST2(vPos, vRoadNode)) 	
									#ENDIF
									RETURN TRUE
								ELSE
									#IF IS_DEBUG_BUILD 
										CPRINTLN(DEBUG_MISSION, "(VDIST2(vPos, vRoadNode) >= 10*10 ) ======== FALSE ", VDIST2(vPos, vRoadNode)) 	
									#ENDIF
									
									PRINT_HELP("AM_H_FBIC6", DEFAULT_HELP_TEXT_TIME)//This location isn't discrete enough.s
								ENDIF
							ENDIF
						ELSE
							
							PRINT_HELP("AM_H_FBIC4", DEFAULT_HELP_TEXT_TIME)//This location is too close to a safe house.
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " ==== NO ROAD NODE ") #ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					PRINT_HELP("AM_H_FBIC5", DEFAULT_HELP_TEXT_TIME)//This location is obstructed.
				ENDIF		
			ELSE
				PRINT_HELP("AM_H_FBIC3", DEFAULT_HELP_TEXT_TIME)//The vehicle needs to be at a stop before making the call.
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CONTACT_WATCH(BOOL bOn)
	IF bOn
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			SET_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE("AM_H_FBIC9")
			SET_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE("AM_H_FBIC9")
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE("AM_H_FBIC9")
		ELSE
			SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE("AM_H_FBIC9")
		ENDIF
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			REMOVE_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION()
			REMOVE_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION()
		ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()
		ELSE
			REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI4_P3_RESET()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_CURRENT_VEHICLE_VALID()
				vPositionEnteredCar = GET_ENTITY_COORDS(PLAYER_PED_ID())
				STRING sHelp
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					sHelp = "AM_H_FBIC1A"
				ELSE
					sHelp = "AM_H_FBIC1B"
				ENDIF


				ADD_HELP_TO_FLOW_QUEUE(sHelp, FHP_HIGH, 0, 2000, DEFAULT_GOD_TEXT_TIME)
				
				IF GET_FLOW_HELP_MESSAGE_STATUS(sHelp) = FHS_DISPLAYED //You can use this vehicle as a getaway vehicle. Drop getaway vehicle off at ~BLIP_TRACE_GETAWAY_CAR~.
					g_sTriggerSceneAssets.flag = TRUE
				ENDIF
			ENDIF
		ELSE
			g_sTriggerSceneAssets.flag = FALSE
		ENDIF
	ENDIF

	mnInvalidModels[0] = TRASH
	mnInvalidModels[1] = TOWTRUCK
	bDisplayedJerrycanHelp = FALSE
	SET_DEFAULT_GUNSHOP_WEAPON(WEAPONTYPE_PETROLCAN)
	vPotentialHidingPos = <<0,0,0>>
	bIsHidingPosValid = FALSE
	bIsCarValid = FALSE
	CONTACT_WATCH(FALSE)	
	#IF IS_DEBUG_BUILD
//		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
	#ENDIF
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4_P3_REQUEST_ASSETS()
	IF eTriggerState = TS_PLACE_GAS_AFT_CAR
		REQUEST_MODEL(mDroppedOffCarStruct.eModel)
	ELIF eTriggerState = TS_PLACE_CAR_AFT_GAS
//		REQUEST_MODEL(PROP_JERRYCAN_01A)
	ENDIF
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4_P3_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(mDroppedOffCarStruct.eModel)
		#IF IS_DEBUG_BUILD
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (FALSE)	
		#ENDIF
//	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_JERRYCAN_01A)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4_P3_HAVE_ASSETS_LOADED()
	IF eTriggerState = TS_PLACE_GAS_AFT_CAR
		IF HAS_MODEL_LOADED(mDroppedOffCarStruct.eModel)
			RETURN TRUE
		ENDIF
	ELIF eTriggerState = TS_PLACE_CAR_AFT_GAS
//		IF HAS_MODEL_LOADED(PROP_JERRYCAN_01A)
			RETURN TRUE
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4_P3_CREATE()
	IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>, vCarPosition)
			IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCarPosition) >= 10*10)
			AND eTriggerState = TS_PLACE_GAS_AFT_CAR
				g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(mDroppedOffCarStruct.eModel, vCarPosition, fCarHeading)
				SET_VEHICLE_SETUP(g_sTriggerSceneAssets.veh[0], mDroppedOffCarStruct)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>, vJerryPos)
		IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) >= 10*10)
		AND eTriggerState = TS_PLACE_CAR_AFT_GAS
			g_sTriggerSceneAssets.object[0] = CREATE_PORTABLE_PICKUP ( PICKUP_WEAPON_PETROLCAN, vJerryPos)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4_P3_RELEASE()
	INT iEntityInstanceID
	STRING strEntityScript
	INT i
	FOR i=0 TO 3
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[i])
				strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.veh[i], iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
						SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.veh[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[i])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[i])
				strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[i], iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
						SET_OBJECT_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.object[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	g_sTriggerSceneAssets.flag = FALSE	
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4_P3_DELETE()
	INT iEntityInstanceID
	STRING strEntityScript
	INT i
	FOR i=0 TO 3
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[i])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[i])
				strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.veh[i], iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[i])
							IF eTriggerState = TS_PLACE_GAS_AFT_CAR
								eTriggerState = TS_PLACE_FIRST_ITEM
							ENDIF
							SET_VEHICLE_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.veh[i])
						ELSE
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[i])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[i])
			IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[i])
				strEntityScript = GET_ENTITY_SCRIPT(g_sTriggerSceneAssets.object[i], iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					IF ARE_STRINGS_EQUAL(strEntityScript, GET_THIS_SCRIPT_NAME())
						DELETE_OBJECT(g_sTriggerSceneAssets.object[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	g_sTriggerSceneAssets.flag = FALSE
ENDPROC

PROC UPDATE_ENTERED_POSITION()
	IF NOT ARE_VECTORS_EQUAL(vPositionEnteredCar, <<0,0,0>>)
		IF ARE_VECTORS_ALMOST_EQUAL(vPositionExitedCar, GET_ENTITY_COORDS(PLAYER_PED_ID()), 2.0)
			EXIT
		ENDIF
	ENDIF
	vPositionEnteredCar = GET_ENTITY_COORDS(PLAYER_PED_ID())
ENDPROC

PROC MONITER_HELP_TEXT()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF NOT g_sTriggerSceneAssets.flag
			IF eTriggerState <> TS_PLACE_GAS_AFT_CAR
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_CURRENT_VEHICLE_VALID()
					IF NOT IS_PLAYER_PASSENGER_IN_TAXI()
					
						UPDATE_ENTERED_POSITION()

						STRING sHelp
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							sHelp = "AM_H_FBIC1A"
						ELSE
							sHelp = "AM_H_FBIC1B"
						ENDIF

						IF NOT IS_STRING_NULL_OR_EMPTY(sHelp)
							IF DO_MISSION_FLOW_HELP(sHelp) //You can use this vehicle as a getaway vehicle. Drop getaway vehicle off at ~BLIP_TRACE_GETAWAY_CAR~.
								g_sTriggerSceneAssets.flag = TRUE
							ENDIF
						ENDIF
					ELSE
						bIsCarValid = FALSE
						g_sTriggerSceneAssets.flag = TRUE
					ENDIF
				ELSE
					IF NOT bInFIBVehicleBeforeComplete
						IF NOT IS_STRING_NULL_OR_EMPTY(sUnsuitableVehicleHelp)
							IF DO_MISSION_FLOW_HELP(sUnsuitableVehicleHelp)
								g_sTriggerSceneAssets.flag = TRUE
							ENDIF
						ENDIF
					ELSE
						g_sTriggerSceneAssets.flag = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vPositionExitedCar = GET_ENTITY_COORDS(PLAYER_PED_ID())
				bIsCarValid = FALSE
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//					REMOVE_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION()
//					REMOVE_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION()
//				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//					REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()
//				ELSE
//					REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()
//				ENDIF
				g_sTriggerSceneAssets.flag = FALSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL DO_CONF_CALL(enumCharacterList who, STRING conv)

	IF PLAYER_MAKE_CONFERENCE_CALL(g_sTriggerSceneAssets.conversation, who, CHAR_MICHAEL, sTextBlock, conv, CONV_PRIORITY_VERY_HIGH)
		IF IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBIC3")
			CLEAR_HELP()
		ENDIF
		TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_FBI_OFFICERS4_P3, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_IS_CONTACT_LIST_ON_SCREEN()
	IF IS_PHONE_ONSCREEN()
	AND IS_CONTACTS_LIST_ON_SCREEN()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_HIGHLIGHTING_CONTACT()
	IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
		enumCharacterList eHighlighted = GET_CURRENTLY_HIGHLIGHTED_CONTACT()
		IF eHighlighted = CHAR_MICHAEL
		OR eHighlighted = CHAR_TREVOR
		OR eHighlighted = CHAR_FRANKLIN
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL HAS_CAR_BEEN_PLACED(BOOL &bJerryCan)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF bIsCarValid
					IF eTriggerState = TS_PLACE_CAR_AFT_GAS
						IF NOT bISHidingPosValid
							IF IS_PLAYER_HIGHLIGHTING_CONTACT()
								IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
									bIsHidingPosValid = TRUE
									CONTACT_WATCH(bIsHidingPosValid)
								ELSE
									bIsHidingPosValid = FALSE
									CONTACT_WATCH(bIsHidingPosValid)
								ENDIF
							ENDIF
						ELSE
							IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
								IF DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, GET_ENTITY_COORDS(PLAYER_PED_ID()), 10)
									bJerryCan = TRUE
									INT iVoice
									STRING sVoice
									STRING sConv
									enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
									enumCharacterList eContact
									IF ePlayer = CHAR_MICHAEL
										iVoice = 0
										sVoice = "MICHAEL"
										sConv = "FBI_3_MDRPCA"
										eContact = CHAR_FRANK_TREV_CONF
										
									ELIF ePlayer = CHAR_FRANKLIN
										iVoice = 1
										sVoice = "FRANKLIN"
										sConv = "FBI_3_FDRPCA"
										eContact = CHAR_MIKE_TREV_CONF
									ELIF ePlayer = CHAR_TREVOR
										iVoice = 2
										sVoice = "TREVOR"
										sConv = "FBI_3_TDRPCA"
										eContact = CHAR_MIKE_FRANK_CONF
									ENDIF

									IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									bJerryCan = FALSE
									INT iVoice
									STRING sVoice
									STRING sConv
									enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
									enumCharacterList eContact
									IF ePlayer = CHAR_MICHAEL
										iVoice = 0
										sVoice = "MICHAEL"
										sConv = "FBI_3_MDRPCA"
										eContact = CHAR_FRANK_TREV_CONF
										
									ELIF ePlayer = CHAR_FRANKLIN
										iVoice = 1
										sVoice = "FRANKLIN"
										sConv = "FBI_3_FDRPCA"
										eContact = CHAR_MIKE_TREV_CONF
									ELIF ePlayer = CHAR_TREVOR
										iVoice = 2
										sVoice = "TREVOR"
										sConv = "FBI_3_TDRPCA"
										eContact = CHAR_MIKE_FRANK_CONF
									ENDIF

									IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
										IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT bISHidingPosValid
							IF IS_PLAYER_HIGHLIGHTING_CONTACT()
								IF IS_HIDING_POS_VAILD()
									vPotentialHidingPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
									bIsHidingPosValid = TRUE
									CONTACT_WATCH(bIsHidingPosValid)
								ENDIF
							ENDIF
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPotentialHidingPos) <= 5*5
								IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
									IF DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, GET_ENTITY_COORDS(PLAYER_PED_ID()), 10)
										#IF IS_DEBUG_BUILD 
											CPRINTLN(DEBUG_MISSION, "DOES_PICKUP_OF_TYPE_EXIST_IN_AREA ====== TRUE")
										#ENDIF
										bJerryCan = TRUE
										INT iVoice
										STRING sVoice
										STRING sConv
										enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
										enumCharacterList eContact
										IF ePlayer = CHAR_MICHAEL
											iVoice = 0
											sVoice = "MICHAEL"
											sConv = "FBI_3_MDRPCA"
											eContact = CHAR_FRANK_TREV_CONF
											
										ELIF ePlayer = CHAR_FRANKLIN
											iVoice = 1
											sVoice = "FRANKLIN"
											sConv = "FBI_3_FDRPCA"
											eContact = CHAR_MIKE_TREV_CONF
										ELIF ePlayer = CHAR_TREVOR
											iVoice = 2
											sVoice = "TREVOR"
											sConv = "FBI_3_TDRPCA"
											eContact = CHAR_MIKE_FRANK_CONF
										ENDIF

										IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ELSE
										bJerryCan = FALSE
										INT iVoice
										STRING sVoice
										STRING sConv
										enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
										enumCharacterList eContact
										IF ePlayer = CHAR_MICHAEL
											iVoice = 0
											sVoice = "MICHAEL"
											sConv = "FBI_3_MDRPC"
											eContact = CHAR_FRANK_TREV_CONF
											
										ELIF ePlayer = CHAR_FRANKLIN
											iVoice = 1
											sVoice = "FRANKLIN"
											sConv = "FBI_3_FDRPC"
											eContact = CHAR_MIKE_TREV_CONF
										ELIF ePlayer = CHAR_TREVOR
											iVoice = 2
											sVoice = "TREVOR"
											sConv = "FBI_3_TDRPC"
											eContact = CHAR_MIKE_FRANK_CONF
										ENDIF
										
										IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
											ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
											IF DO_CONF_CALL(eContact, sConv)
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									bIsHidingPosValid = FALSE
									CONTACT_WATCH(bIsHidingPosValid)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_GAS_CAN_BEEN_PLACED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, GET_ENTITY_COORDS(PLAYER_PED_ID()), 10)
			
			IF NOT bDisplayedJerrycanHelp
				IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
					STRING sHelp
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						sHelp = "AM_H_FBIC1E"
					ELSE
						sHelp = "AM_H_FBIC1F"
					ENDIF

					SWITCH GET_FLOW_HELP_MESSAGE_STATUS(sHelp)//This vehicle is unsuitable to use for the getaway.
						CASE FHS_EXPIRED
							ADD_HELP_TO_FLOW_QUEUE(sHelp, FHP_HIGH, 0, FLOW_HELP_NEVER_EXPIRES, DEFAULT_GOD_TEXT_TIME)
						BREAK
						CASE FHS_DISPLAYED
							bDisplayedJerrycanHelp = TRUE
						BREAK
					ENDSWITCH
				ENDIF
			ELSE
				IF eTriggerState = TS_PLACE_GAS_AFT_CAR
					IF IS_PHONE_ONSCREEN()
						CONTACT_WATCH(TRUE)
						INT iVoice
						STRING sVoice
						STRING sConv
						enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
						enumCharacterList eContact
						IF ePlayer = CHAR_MICHAEL
							iVoice = 0
							sVoice = "MICHAEL"
							sConv = "FBI_3_MDRPJA"
							eContact = CHAR_MIKE_TREV_CONF
						ELIF ePlayer = CHAR_FRANKLIN
							iVoice = 1
							sVoice = "FRANKLIN"
							sConv = "FBI_3_FDRPJA"
							eContact = CHAR_MIKE_TREV_CONF
						ELIF ePlayer = CHAR_TREVOR
							iVoice = 2
							sVoice = "TREVOR"
							sConv = "FBI_3_TDRPJA"
							eContact = CHAR_MIKE_FRANK_CONF
						ENDIF
						
						IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
							IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ENDIF
						ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
							IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ENDIF
						ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
							IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= 10*10)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_PHONE_ONSCREEN()
						INT iVoice
						STRING sVoice
						STRING sConv
						enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
						enumCharacterList eContact
						IF ePlayer = CHAR_MICHAEL
							iVoice = 0
							sVoice = "MICHAEL"
							sConv = "FBI_3_MDRPJ"
							eContact = CHAR_MIKE_TREV_CONF
						ELIF ePlayer = CHAR_FRANKLIN
							iVoice = 1
							sVoice = "FRANKLIN"
							sConv = "FBI_3_FDRPJ"
							eContact = CHAR_MIKE_TREV_CONF
						ELIF ePlayer = CHAR_TREVOR
							iVoice = 2
							sVoice = "TREVOR"
							sConv = "FBI_3_TDRPJ"
							eContact = CHAR_MIKE_FRANK_CONF
						ENDIF
						IF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_MICHAEL)
							IF IS_HIDING_POS_VAILD(FALSE)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ELSE
							ENDIF
						ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_TREVOR)
							IF IS_HIDING_POS_VAILD(FALSE)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ELSE
							ENDIF
						ELIF IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION(CHAR_FRANKLIN)
							IF IS_HIDING_POS_VAILD(FALSE)
								ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, iVoice, PLAYER_PED_ID(), sVoice)
								IF DO_CONF_CALL(eContact, sConv)
									RETURN TRUE
								ENDIF
							ELSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bDisplayedJerrycanHelp = FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MONITER_PLAYER_MOVING_CAR()
	IF ( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vJerryPos) <= (TS_FBI4_P3_STREAM_IN_DIST-10)*(TS_FBI4_P3_STREAM_IN_DIST-10))
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0])
				IF ( VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0]), vCarPosition) >= 25*25)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITER_PLAYER_MOVING_CAR == eTriggerState = TS_PLACE_FIRST_ITEM") #ENDIF
					eTriggerState = TS_PLACE_FIRST_ITEM
				ELIF ( VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.veh[0]), vCarPosition) >= 10*10)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITER_PLAYER_MOVING_CAR == HELP") #ENDIF
					IF eTriggerState = TS_PLACE_CAR_AFT_GAS
						PRINT_HELP("AM_H_FBIC10A", DEFAULT_GOD_TEXT_TIME) //Return the car to the ~BLIP_GETAWAY_CAR~.
					ELSE
						PRINT_HELP("AM_H_FBIC10B", DEFAULT_GOD_TEXT_TIME) //Return the car to the ~BLIP_JERRY_CAN~.
					ENDIF
				ELSE
					IF IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBIC10A")
					OR IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBIC10B")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF eTriggerState = TS_PLACE_GAS_AFT_CAR
				eTriggerState = TS_PLACE_FIRST_ITEM
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_PLAYER_MOVING_PETROL()
	IF NOT DOES_PICKUP_OF_TYPE_EXIST_IN_AREA(PICKUP_WEAPON_PETROLCAN, vCarPosition, 10)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_MOVING_PETROL == eTriggerState = TS_PLACE_FIRST_ITEM") #ENDIF
		eTriggerState = TS_PLACE_FIRST_ITEM
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_MOVING_PETROL == HELP") #ENDIF

		IF eTriggerState = TS_PLACE_GAS_AFT_CAR
			PRINT_HELP("AM_H_FBIC11A", DEFAULT_GOD_TEXT_TIME) //Drop some gas at ~BLIP_GETAWAY_CAR~.
		ELSE
			PRINT_HELP("AM_H_FBIC11B", DEFAULT_GOD_TEXT_TIME) //Drop some gas at ~BLIP_JERRY_CAN~.
		ENDIF
	ELSE
		IF IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBIC11A")
		OR IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBIC11B")
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

PROC MONITER_PLAYER_WANTED()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND eTriggerState <> TS_PLACE_GAS_AFT_CAR
		SWITCH eCopMonitor
			CASE CM_MONITER
				IF IS_THIS_PRINT_BEING_DISPLAYED("AM_H_FBICWANT")
					CLEAR_HELP()
				ENDIF

				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					CLEAR_HELP()
					eCopMonitor = CM_INIT_WANTED
				ENDIF
			BREAK
			
			CASE CM_INIT_WANTED
				IF NOT DO_MISSION_FLOW_HELP("AM_H_FBICWANT")
					eCopMonitor = CM_LOSING_WANTED
				ELSE
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
						eCopMonitor = CM_MONITER
					ENDIF
				ENDIF
			BREAK
			
			CASE CM_LOSING_WANTED
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
					eCopMonitor = CM_MONITER
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


PROC UPDATE_TRIGGER_STATE()
	MONITER_PLAYER_WANTED()
	IF eCopMonitor = CM_MONITER
		MONITER_HELP_TEXT()
		BOOL bJerryCanDropAtSameTime
		SWITCH eTriggerState
			CASE TS_PLACE_FIRST_ITEM
				
	//			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "TS_PLACE_FIRST_ITEM") #ENDIF
				IF HAS_CAR_BEEN_PLACED(bJerryCanDropAtSameTime)
					GET_VEHICLE_SETUP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), mDroppedOffCarStruct)
					vCarPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					fCarHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
					vJerryPos = vCarPosition
					SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE, TRUE)
					g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF bJerryCanDropAtSameTime
						eTriggerState = TS_DONE					
					ELSE
						CLEAR_HELP()
						IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PETROLCAN)
							PRINT_HELP("AM_H_FBIC8A", DEFAULT_GOD_TEXT_TIME)
						ELSE
							PRINT_HELP("AM_H_FBIC8B", DEFAULT_GOD_TEXT_TIME)
						ENDIF
						//stat has been removed
						//INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FBI4P3_COLLECTION_TIME)
						eTriggerState = TS_PLACE_GAS_AFT_CAR
					ENDIF
				ELIF HAS_GAS_CAN_BEEN_PLACED()
					vJerryPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					vCarPosition = vJerryPos
					eTriggerState = TS_PLACE_CAR_AFT_GAS
				ENDIF
				
			BREAK

			CASE TS_PLACE_GAS_AFT_CAR
				MONITER_PLAYER_MOVING_CAR()
				IF HAS_GAS_CAN_BEEN_PLACED()
					vJerryPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					eTriggerState = TS_DONE
				ENDIF
			BREAK
			
			CASE TS_PLACE_CAR_AFT_GAS
				MONITOR_PLAYER_MOVING_PETROL()
				IF HAS_CAR_BEEN_PLACED(bJerryCanDropAtSameTime)
				
					GET_VEHICLE_SETUP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), mDroppedOffCarStruct)
					vCarPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
					fCarHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())

					SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE, TRUE)
					g_sTriggerSceneAssets.veh[0] = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
					CLEAR_HELP()
					eTriggerState = TS_DONE
				ENDIF
			BREAK

			CASE TS_DONE
				
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4_P3_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P3)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4_P3_TRIGGER_DIST)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (TS_FBI4_P3_TRIGGER_DIST*TS_FBI4_P3_TRIGGER_DIST)
			RETURN TRUE
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4_P3_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4_P3_IS_BLOCKED()
//	SWITCH eTriggerState
//		CASE TS_PLACE_FIRST_ITEM
//			UPDATE_TRIGGER_STATE()
//			#IF IS_DEBUG_BUILD
//				UPDATE_DEBUG_PASS()
//			#ENDIF
//		BREAK
//
//		CASE TS_PLACE_GAS_AFT_CAR
//			IF DOES_BLIP_EXIST(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P3].biBlip)
//				SET_BLIP_SPRITE(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P3].biBlip, RADAR_TRACE_JERRY_CAN)
//			ENDIF
			RETURN FALSE
//		BREAK
//		
//		CASE TS_PLACE_CAR_AFT_GAS
//			IF DOES_BLIP_EXIST(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P3].biBlip)
//				SET_BLIP_SPRITE(g_GameBlips[STATIC_BLIP_MISSION_FBI_OFFICERS4_P3].biBlip, RADAR_TRACE_GETAWAY_CAR)
//			ENDIF
//			RETURN FALSE
//		BREAK
//
//		CASE TS_DONE
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				RETURN FALSE
//			ENDIF
//		BREAK
//	ENDSWITCH
//	RETURN TRUE
ENDFUNC

/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FBI4_P3_UPDATE()
	//Display a help message to get in one of the firetrucks
	//if the player gets close enough to the one created by the trigger scene.
//	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "TS_FBI4_P3_UPDATE()") #ENDIF
	#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_PASS()
	#ENDIF
	UPDATE_TRIGGER_STATE()
ENDPROC

 
/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4_P3_AMBIENT_UPDATE()
//	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "TS_FBI4_P3_AMBIENT_UPDATE()") #ENDIF
	#IF IS_DEBUG_BUILD
		UPDATE_DEBUG_PASS()
	#ENDIF
	UPDATE_TRIGGER_STATE()
ENDPROC
