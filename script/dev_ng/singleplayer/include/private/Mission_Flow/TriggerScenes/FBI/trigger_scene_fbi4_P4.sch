//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Template - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FBI4_P4_STREAM_IN_DIST			130.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4_P4_STREAM_OUT_DIST			140.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FBI4_P4_TRIGGER_DIST				10.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT 	TS_FBI4_P4_BATTLE_BUDDY_CALL_DIST	200.0 	// Distance from trigger's blip at which the player can call in battle buddies.

CONST_FLOAT		TS_FBI4_P4_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4_P4_FRIEND_ACCEPT_BITS	BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR	//Friends who can trigger the mission with the player.

enumBankAccountName	ePlayerAccount
BOOL bNoMoneyHelp = FALSE

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI4_P4_RESET()
	SET_SHOP_BLIP_NEEDS_UPDATED(CLOTHES_SHOP_A_01_VB)
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4_P4_REQUEST_ASSETS()
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4_P4_RELEASE_ASSETS()
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4_P4_HAVE_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4_P4_CREATE()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			ePlayerAccount = BANK_ACCOUNT_MICHAEL
		BREAK
		
		CASE CHAR_FRANKLIN
			ePlayerAccount = BANK_ACCOUNT_FRANKLIN
		BREAK
		
		CASE CHAR_TREVOR
			ePlayerAccount = BANK_ACCOUNT_TREVOR
		BREAK
	ENDSWITCH
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-1333.43066, -1280.89685, 11.48874>>, <<-1337.81042, -1286.40540, 2.83855>>)
	bNoMoneyHelp = FALSE
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4_P4_RELEASE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4_P4_DELETE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
ENDPROC


FUNC INT GET_REQUIRED_CASH_MASKS()
	INT iCash
	
		iCash = ABSI(GET_ACCOUNT_BALANCE(ePlayerAccount) - 300)
		CPRINTLN(DEBUG_MISSION, "AMOUNT OF CASH NEEDED ====== ", iCash, " AMOUNT OF CASH OWNED ===== ", GET_ACCOUNT_BALANCE(ePlayerAccount))
	RETURN iCash
ENDFUNC

/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4_P4_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_P4)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4_P4_TRIGGER_DIST)
		#ENDIF
		
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (TS_FBI4_P4_TRIGGER_DIST*TS_FBI4_P4_TRIGGER_DIST)
			IF GET_ACCOUNT_BALANCE(ePlayerAccount) >= GET_REQUIRED_CASH_MASKS()
				RETURN TRUE
			ELSE
				IF NOT bNoMoneyHelp
					PRINT_HELP_WITH_NUMBER("AM_H_FBIPCASH", GET_REQUIRED_CASH_MASKS(), DEFAULT_HELP_TEXT_TIME)//~1~ needed to purchase items.
					bNoMoneyHelp = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4_P4_HAS_BEEN_DISRUPTED()
	IF NOT IS_SHOP_OPEN_FOR_BUSINESS(CLOTHES_SHOP_A_01_VB)
	OR IS_PLAYER_KICKING_OFF_IN_SHOP(CLOTHES_SHOP_A_01_VB)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4_P4_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FBI4_P4_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4_P4_AMBIENT_UPDATE()
ENDPROC
