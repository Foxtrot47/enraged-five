//╒═════════════════════════════════════════════════════════════════════════════╕
//│							FBI1 - Trigger Scene Data							│
//│																				│
//│								Date: 03/11/12									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								ped[0]	-	Dave Norton							│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "flow_mission_trigger_public.sch"
USING "script_heist.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI1_STREAM_IN_DIST		85.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI1_STREAM_OUT_DIST		95.0	// Distance at which the scene is deleted and unloaded.

CONST_FLOAT		TS_FBI1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

//Scene state bit flags.
CONST_INT FBI1_DAVE_REACTING				0
CONST_INT FBI1_LOADED_CUT_APPROACH_RIGHT	1
CONST_INT FBI1_LOADED_CUT_APPROACH_LEFT		2

VECTOR vDaveScenePos = << -440.132, 1058.520, 326.690 >>
VECTOR vDaveSceneRot = << 0.000, 0.000, 0.000 >>

INT iDaveScene = 0
INT iSceneState = 0

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI1_RESET()
	iDaveScene = 0
	iSceneState = 0
	g_sTriggerSceneAssets.id = -1
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI1_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_DAVE))
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "MISSFBI1LEADINOUT")
	LOAD_QUEUE_MEDIUM_ADD_WAYPOINT_REC(g_sTriggerSceneAssets.loadQueue, "FIB1_LEADINA")
	LOAD_QUEUE_MEDIUM_ADD_WAYPOINT_REC(g_sTriggerSceneAssets.loadQueue, "FIB1_LEADINB")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI1_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI1_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI1_CREATE()
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<-440.412262-20.000000,1073.884766-15.750000,327.116913-2.000000>>, <<-440.412262+20.000000,1073.884766+15.750000,327.116913+2.000000>>)
	SET_PED_NON_CREATION_AREA(<<-440.412262-20.000000,1073.884766-15.750000,327.116913-2.000000>>, <<-440.412262+20.000000,1073.884766+15.750000,327.116913+2.000000>>)
	g_sTriggerSceneAssets.id = ADD_NAVMESH_BLOCKING_OBJECT(<<-440.412262,1073.884766,327.116913>>,<<35.000000,27.625,3.200000>>, 0)
	//<<20.000000,15.750000,2.000000>>,0.0)
	
	CLEAR_AREA(<<-436.8515, 1059.0400, 326.6815>>, 20.0, TRUE)
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	
	//Create Dave Norton.
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_DAVE, <<-436.8515, 1059.0400, 326.6815>>, 0.0)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DAVE))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	//Start Dave's ambient looping sync scene.
	iDaveScene = CREATE_SYNCHRONIZED_SCENE(vDaveScenePos, vDaveSceneRot)
	SET_SYNCHRONIZED_SCENE_LOOPED(iDaveScene, TRUE)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iDaveScene, "MISSFBI1LEADINOUT", "FBI_1_INT_LEADIN_LOOP_DAVEN", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8, g_sTriggerSceneAssets.ped[0], "DAVE")
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI1_RELEASE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	CLEAR_PED_NON_CREATION_AREA()
	IF g_sTriggerSceneAssets.id != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(g_sTriggerSceneAssets.id)
	ENDIF

	//Make Dave flee the player.
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
	
	
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI1_DELETE()
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	CLEAR_PED_NON_CREATION_AREA()
	IF g_sTriggerSceneAssets.id != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(g_sTriggerSceneAssets.id)
	ENDIF

	//Delete Dave.
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
	
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 8)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI1_HAS_BEEN_TRIGGERED()
	BOOL bTriggered = FALSE

	IF IS_BIT_SET(iSceneState, FBI1_DAVE_REACTING)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iDaveScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iDaveScene) > 0.8
				bTriggered = TRUE
			ENDIF
		ELSE
			bTriggered = TRUE
		ENDIF
		
		IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					FLOAT fDistFromDaveSquared = VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), GET_ENTITY_COORDS(PLAYER_PED_ID()))
					CPRINTLN(DEBUG_TRIGGER, fDistFromDaveSquared)
					IF fDistFromDaveSquared < 25 //5^2
						bTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bTriggered
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
					SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[0], TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(g_sTriggerSceneAssets.ped[0])
			ENDIF
			REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
			TS_FBI1_RELEASE_ASSETS()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI1_HAS_BEEN_DISRUPTED()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BULLET_IN_ANGLED_AREA(<<-448.893616,1064.198975,326.438171>>, <<-433.119720,1061.907349,330.685577>>, 8.500000)
	OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-448.893616,1064.198975,326.438171>>, <<-433.119720,1061.907349,330.685577>>, 8.500000)
		SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FBI1_UPDATE()

	//Check for the player walking near to Dave.
	IF NOT IS_BIT_SET(iSceneState, FBI1_DAVE_REACTING)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_BIT_SET(g_sMissionStaticData[SP_MISSION_FBI_1].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
				//Load the correct cutscene variation depending on which side of Dave the player is closer to.
				VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				FLOAT fDistFromRight = VDIST2(vPlayerPos, <<-443.2148, 1060.2468, 326.6808>>)
				FLOAT fDistFromLeft = VDIST2(vPlayerPos, <<-438.8461, 1059.5204, 326.6808>>)
				FLOAT fDifference = fDistFromRight - fDistFromLeft

				#IF IS_DEBUG_BUILD
					IF g_bTriggerDebugOn
						DRAW_DEBUG_TEXT("Right", <<-443.2148, 1060.2468, 326.6808>>, 0, 0, 255, 255)
						DRAW_DEBUG_SPHERE(<<-443.2148, 1060.2468, 326.6808>>, 0.25, 0, 255, 0, 65) 
						DRAW_DEBUG_TEXT("Left", <<-438.8461, 1059.5204, 326.6808>>, 0, 0, 255, 255)
						DRAW_DEBUG_SPHERE(<<-438.8461, 1059.5204, 326.6808>>, 0.25, 0, 255, 0, 65) 
					ENDIF
				#ENDIF
				
				IF fDifference > 0.5 //Closer to left.
					IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
						CPRINTLN(DEBUG_TRIGGER, "Clearing right cutscene.")
						MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
						CLEAR_BIT(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
					ENDIF
					IF NOT IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
						IF NOT g_bFlowCleanupIntroCutscene
							CPRINTLN(DEBUG_TRIGGER, "Loading left cutscene.")
							MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_1, "FBI_1_INT", CS_1|CS_3|CS_4|CS_5|CS_6)
							SET_BIT(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
						ENDIF
					ENDIF
				ELIF fDifference < -0.5 //Closer to right.
					IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
						CPRINTLN(DEBUG_TRIGGER, "Clearing left cutscene.")
						MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
						CLEAR_BIT(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
					ENDIF
					IF NOT IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
						IF NOT g_bFlowCleanupIntroCutscene
							CPRINTLN(DEBUG_TRIGGER, "Loading right cutscene.")
							MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_1, "FBI_1_INT", CS_2|CS_3|CS_4|CS_5|CS_6)
							SET_BIT(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
						ENDIF
					ENDIF
				ENDIF
				
				//Apply a slowing area.
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-453.364227,1075.772217,326.671295>>, <<-428.489899,1065.004028,331.182129>>, 21.250000)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_DIVE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_AIM)
					WEAPON_TYPE eCurrentWeapon
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
					IF eCurrentWeapon != WEAPONTYPE_UNARMED
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF

				//Check for the player triggering the reaction.
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-453.781982,1067.391235,326.431610>>, <<-440.045990,1060.740967,331.117035>>, 6.750000)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-442.656769,1059.945190,326.432220>>, <<-432.556030,1062.536255,331.242462>>, 6.250000)
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iDaveScene)
								
								//Trigger Dave's reaction to Michael approaching.
								iDaveScene = CREATE_SYNCHRONIZED_SCENE(vDaveScenePos, vDaveSceneRot)
								TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iDaveScene, "MISSFBI1LEADINOUT", "FBI_1_INT_LEADIN_ACTION_DAVEN", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
								SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
								SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								
								//Start the player walking towards Dave on a waypoint recording.
								STRING strWaypointRecording
								IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
									strWaypointRecording = "FIB1_LEADINA"
								ELSE
									strWaypointRecording = "FIB1_LEADINB"
								ENDIF
								
								INT iClosestWaypointNode = 0
								INT iNumberOfNodes = 0
								WAYPOINT_RECORDING_GET_NUM_POINTS(strWaypointRecording, iNumberOfNodes)
								WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(strWaypointRecording, GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosestWaypointNode)
								IF iClosestWaypointNode < (iNumberOfNodes-1)
									iClosestWaypointNode++
								ENDIF
								TASK_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), strWaypointRecording, iClosestWaypointNode, EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_TURN_TO_FACE_WAYPOINT_HEADING_AT_END | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
								
								//Only play in-game dialogue while approaching from the right.
								IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_RIGHT)
									ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "FBI1AUD", "FBI1_INTL", CONV_PRIORITY_VERY_HIGH)
								ENDIF
								
								SET_BIT(iSceneState, FBI1_DAVE_REACTING)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(iSceneState, FBI1_DAVE_REACTING)
					IF NOT IS_GAMEPLAY_HINT_ACTIVE()
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
							SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, 13000)
							SET_GAMEPLAY_HINT_FOV(30.0)
							SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.460)
							SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.000)
							IF IS_BIT_SET(iSceneState, FBI1_LOADED_CUT_APPROACH_LEFT)
								SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.80)
							ELSE
								SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
							ENDIF
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.100)
						ENDIF
					ELSE
						STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI1_AMBIENT_UPDATE()
ENDPROC
