
//╒═════════════════════════════════════════════════════════════════════════════╕
//│							FBI 5 - Trigger Scene Data							│
//│																				│
//│				Author: Ben Rollinson				Date: 05/07/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								ped[0] = Michael								│
//│								ped[1] = Franklin								│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "player_ped_public.sch"
USING "mission_control_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI5_STREAM_IN_DIST		290.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI5_STREAM_OUT_DIST		300.0	// Distance at which the scene is deleted and unloaded.
CONST_FLOAT		TS_FBI5_TRIGGER_DIST		14.00	// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FBI5_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI5_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

CONST_INT		FBI5_LOADED_CUT_APPROACH_FRONT	0
CONST_INT		FBI5_LOADED_CUT_APPROACH_BACK	1

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI5_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI5_REQUEST_ASSETS()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_TREVOR
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
		FALLTHRU
		CASE CHAR_FRANKLIN
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
			LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "MISSFBI5LEADINOUT")
		BREAK
	ENDSWITCH
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, DINGHY)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI5_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI5_HAVE_ASSETS_LOADED()
	RETURN HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI5_CREATE()
	g_sTriggerSceneAssets.id = 0
	INT iFIB5SyncScene
	VECTOR vFIB5ScenePos
	VECTOR vFIB5SceneRot

	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<3773.4, 4393.7, 50.00>>, <<3891.2, 4555.1, -50.00>>)

	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	CLEAR_AREA(<< 3795.0986, 4466.8384, 4.6044 >>, 120.0, TRUE) 	//Dan H: Original radius was 60
	
	//Create the boat.
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(DINGHY, << 3853.4600, 4455.4272, -0.2>>,  266.1028)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[0], 0, 0) 
    SET_VEHICLE_DIRT_LEVEL(g_sTriggerSceneAssets.veh[0], 5.0) 
    SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 3, true)
	SET_VEHICLE_ON_GROUND_PROPERLY(g_sTriggerSceneAssets.veh[0])
	SET_VEHICLE_ENGINE_ON(g_sTriggerSceneAssets.veh[0], FALSE, TRUE)

	//If it's night time force the boat's lights to be on.
	INT iCurrentHour = GET_CLOCK_HOURS()
	IF iCurrentHour < 7 OR iCurrentHour >= 20 
		SET_VEHICLE_LIGHTS(g_sTriggerSceneAssets.veh[0], FORCE_VEHICLE_LIGHTS_ON)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		//Create Michael if we arrive as Franklin.
		CASE CHAR_FRANKLIN
			vFIB5ScenePos = << 3830.185, 4463.323, 2.699 >>
			vFIB5SceneRot = << 0.000, 0.000, -93.000 >>
		
			CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, <<3830.3010, 4463.7432, 1.7318>>, 264.4568, FALSE, TRUE)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
			
			iFIB5SyncScene = CREATE_SYNCHRONIZED_SCENE(vFIB5ScenePos, vFIB5SceneRot)
			SET_SYNCHRONIZED_SCENE_LOOPED(iFIB5SyncScene, TRUE)
			TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iFIB5SyncScene, "MISSFBI5LEADINOUT", "LEADIN_1_MIC", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
		
			ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[0], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, PLAYER_PED_ID(), "FRANKLIN")
		BREAK
		
		//Create Michael and Franklin if we arrive as Trevor.
		CASE CHAR_TREVOR
			vFIB5ScenePos = << 3830.185, 4463.323, 1.697 >>
			vFIB5SceneRot = << 0.000, 0.000, -95.000 >>
			
			CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, <<3830.3010, 4463.7432, 1.7318>>, 264.4568, FALSE, TRUE)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
			
			CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_FRANKLIN, <<3830.1956, 4462.5376, 1.7318>>, 269.7024, FALSE, TRUE)
			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
			
			iFIB5SyncScene = CREATE_SYNCHRONIZED_SCENE(vFIB5ScenePos, vFIB5SceneRot)
			SET_SYNCHRONIZED_SCENE_LOOPED(iFIB5SyncScene, TRUE)
			TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iFIB5SyncScene, "MISSFBI5LEADINOUT", "LEADIN_2_MIC", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
			TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[1], iFIB5SyncScene, "MISSFBI5LEADINOUT", "LEADIN_2_FRA", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT )
		
			ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[0], "MICHAEL")
			ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1, g_sTriggerSceneAssets.ped[1], "FRANKLIN")
		BREAK
	ENDSWITCH
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FRANKLIN)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI5_RELEASE()
	INT index

	//Release scene peds.
	REPEAT 2 index
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
	
	//B* 1990803: Check if hint cam active, deactivate if true
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI5_DELETE()
	INT index

	//Delete scene peds.
	REPEAT 2 index
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0)
	REMOVE_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 1)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI5_HAS_BEEN_TRIGGERED()
	
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS5, GET_CURRENT_PLAYER_PED_INT())
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI5_TRIGGER_DIST)
		#ENDIF
		
		enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
		
		IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			IF NOT (IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID()))
				FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
				
				//Lock in to the trigger sequence when 10m from the trigger point as Franklin or Trevor.
				IF NOT g_bPlayerLockedInToTrigger
					IF ePlayer = CHAR_TREVOR
					OR ePlayer = CHAR_FRANKLIN
						IF fDistanceSquaredFromTrigger < 210.25 //14.5^2
							//Play dialogue on approach for Trevor and Franklin.
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "FBI5AAU", "FBI5_INT_LI", CONV_PRIORITY_VERY_HIGH)
							ELSE
								ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(g_sTriggerSceneAssets.conversation, "FBI5AAU", "FBI5_INT_LIF", CONV_PRIORITY_VERY_HIGH)
							ENDIF
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_GAMEPLAY_HINT_ACTIVE()
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
							SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
							SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, 13000)
							SET_GAMEPLAY_HINT_FOV(30.0)
							SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.460)
							SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.000)
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
							SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.100)
						ENDIF
					ELSE
						STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
					ENDIF
				ENDIF
				
				IF  NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_FRONT)
				AND NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_BACK)
					RETURN FALSE
				ENDIF
			
				IF fDistanceSquaredFromTrigger < (TS_FBI5_TRIGGER_DIST*TS_FBI5_TRIGGER_DIST)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF ePlayer = CHAR_TREVOR
				OR ePlayer = CHAR_FRANKLIN
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3847.273193,4463.729492,4.151820>>,<<16.250000,1.750000,2.500000>>)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<3835.645996,4464.246582,2.944657>>,<<3.750000,14.000000,3.500000>>)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI5_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
	
		//Check for scene peds being injured or harmed.
		REPEAT 2 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
				OR IS_PED_RAGDOLL(g_sTriggerSceneAssets.ped[index])
				OR IS_ENTITY_ON_FIRE(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for the dinghy being damaged.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.veh[0], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_AT_COORD(g_sTriggerSceneAssets.veh[0], <<3853.257080,4455.461426,1.957811>>,<<4.250000,1.750000,2.000000>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
		IF IS_BULLET_IN_ANGLED_AREA(<<3830.110596,4460.512695,1.026169>>, <<3830.214111,4466.375000,4.992479>>, 6.000000)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<3830.110596,4460.512695,1.026169>>, <<3830.214111,4466.375000,4.992479>>, 6.000000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI5_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_FBI5_UPDATE()

	//Load the correct cutscene variation depending on which direction the player approaches from.
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fDistFromFront = VDIST2(vPlayerPos, <<3832.6851, 4463.3345, 1.7214>>)
	FLOAT fDistFromBack = VDIST2(vPlayerPos, <<3827.8816, 4463.0811, 1.9017>>)
	FLOAT fDifference =  fDistFromBack - fDistFromFront

	#IF IS_DEBUG_BUILD
		IF g_bTriggerDebugOn
			DRAW_DEBUG_TEXT("Front", <<3832.6851, 4463.3345, 1.7214>>, 0, 0, 255, 255)
			DRAW_DEBUG_SPHERE(<<3832.6851, 4463.3345, 1.7214>>, 0.25, 0, 255, 0, 65) 
			DRAW_DEBUG_TEXT("Back", <<3827.8816, 4463.0811, 1.9017>>, 0, 0, 255, 255)
			DRAW_DEBUG_SPHERE(<<3827.8816, 4463.0811, 1.9017>>, 0.25, 0, 255, 0, 65) 
		ENDIF
	#ENDIF
	
	IF fDifference > 0.5 //Closer to front.
		IF IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_BACK)
			CPRINTLN(DEBUG_TRIGGER, "Clearing BACK cutscene.")
			MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
			CLEAR_BIT(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_BACK)
		ENDIF
		IF NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_FRONT)
			IF NOT g_bFlowCleanupIntroCutscene
				CPRINTLN(DEBUG_TRIGGER, "Loading FRONT cutscene.")
			
				INT iStartHour = g_sMissionStaticData[SP_MISSION_FBI_5].startHour
				INT iEndHour = g_sMissionStaticData[SP_MISSION_FBI_5].endHour
				IF IS_TIME_BETWEEN_THESE_HOURS(iStartHour, iEndHour)
					//Correct hours.
					MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_5,
																"FBI_5_INT",
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_8,
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_8,
																CS_3|CS_4|CS_5|CS_6|CS_8)
					
					fbi5a_perform_tods = FALSE
				ELSE
					//Out of hours.
					MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_5,
																"FBI_5_INT",
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
																CS_3|CS_4|CS_5|CS_6|CS_7|CS_8)
					fbi5a_perform_tods = TRUE
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",PLAYER_PED_ID())
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",g_sTriggerSceneAssets.ped[0])
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Franklin",PLAYER_PED_ID())	
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",g_sTriggerSceneAssets.ped[0])
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Franklin",g_sTriggerSceneAssets.ped[1])						
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Trevor",PLAYER_PED_ID())
				ENDIF	
				SET_BIT(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_FRONT)
			ENDIF
		ENDIF
		
	ELIF fDifference < -0.5 //Closer to back.
		IF IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_FRONT)
			CPRINTLN(DEBUG_TRIGGER, "Clearing FRONT cutscene.")
			MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
			CLEAR_BIT(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_FRONT)
		ENDIF
		IF NOT IS_BIT_SET(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_BACK)
			IF NOT g_bFlowCleanupIntroCutscene
				CPRINTLN(DEBUG_TRIGGER, "Loading BACK cutscene.")
				
				INT iStartHour = g_sMissionStaticData[SP_MISSION_FBI_5].startHour
				INT iEndHour = g_sMissionStaticData[SP_MISSION_FBI_5].endHour
				IF IS_TIME_BETWEEN_THESE_HOURS(iStartHour, iEndHour)
					//Correct hours.
					MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_5,
																"FBI_5_INT",
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_8,
																CS_2|CS_3|CS_4|CS_5|CS_6|CS_8,
																CS_4|CS_5|CS_6|CS_8)
																
					fbi5a_perform_tods = FALSE
				ELSE
					//Out of hours.
					MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_5,
																"FBI_5_INT",
																CS_1|CS_2|CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
																CS_2|CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
																CS_4|CS_5|CS_6|CS_7|CS_8)
																
					fbi5a_perform_tods = TRUE
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",PLAYER_PED_ID())
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",g_sTriggerSceneAssets.ped[0])
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Franklin",PLAYER_PED_ID())	
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Michael",g_sTriggerSceneAssets.ped[0])
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Franklin",g_sTriggerSceneAssets.ped[1])						
					MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_5,"Trevor",PLAYER_PED_ID())
				ENDIF	
				
				SET_BIT(g_sTriggerSceneAssets.id, FBI5_LOADED_CUT_APPROACH_BACK)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI5_AMBIENT_UPDATE()
ENDPROC
