
//╒═════════════════════════════════════════════════════════════════════════════╕
//│							FBI 4 - Trigger Scene Data							│
//│																				│
//│				Author: Ben Rollinson				Date: 05/07/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								veh[0] = 	Trash Truck							│
//│								veh[1] = 	Towing Truck						│
//│								veh[2] = 	Trevor's Truck						│
//│								ped[0] = 	Michael								│
//│								ped[1] = 	Franklin							│	
//│								object[0] = Heist bag							│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "player_ped_public.sch"
USING "commands_audio.sch"
USING "flow_public_game.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI4_STREAM_IN_DIST		250.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4_STREAM_OUT_DIST		260.0	// Distance at which the scene is deleted and unloaded.
CONST_FLOAT		TS_FBI4_TRIGGER_DIST		6.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FBI4_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI4_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TRASH)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TOWTRUCK)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, P_LD_HEIST_BAG_S_1)
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, TOWTRUCK)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "MISSFBI4LEADINOUTFBI_4_MCS_3")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


PROC TS_FBI4_CREATE_MICHAEL()
	CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, <<1377.1262, -2082.3298, 50.9983>>, 55.3305, FALSE, TRUE)
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
	SET_PED_COMP_ITEM_CURRENT_SP(g_sTriggerSceneAssets.ped[0], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	VECTOR vCutscenePos = << 1376.840, -2082.661, 51.050 >>
	VECTOR vCutsceneRot = << 0.000, 0.000, 117.000 >>
	INT iCutscene = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[0], iCutscene, "MISSFBI4LEADINOUTFBI_4_MCS_3", "_LEADIN_LOOP_MICHAEL", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
	SET_SYNCHRONIZED_SCENE_LOOPED(iCutscene, TRUE)
	
	//Create Micahel's heist bag.
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<1377.2223, -2082.2046, 51.4064>>)
	SET_ENTITY_HEADING(g_sTriggerSceneAssets.object[0], 20.96)
	vCutscenePos = << 1376.690, -2082.761, 51.050 >>
	vCutsceneRot = << 0.000, 0.000, 117.000 >>
	iCutscene = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], iCutscene, "_LEADIN_LOOP_HEIST_BAG","MISSFBI4LEADINOUTFBI_4_MCS_3" , INSTANT_BLEND_IN )
ENDPROC


PROC TS_FBI4_CREATE_MICHAEL_AND_FRANKLIN()
	CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, <<1373.2456, -2076.6614, 50.9983>>, 107.6638, FALSE, TRUE)
	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
	SET_PED_COMP_ITEM_CURRENT_SP(g_sTriggerSceneAssets.ped[0], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	
	CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_FRANKLIN, <<1372.5920, -2077.2300, 50.9983>>, 310.9398, FALSE, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	SET_PED_COMP_ITEM_CURRENT_SP(g_sTriggerSceneAssets.ped[1], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	
	//Create Micahel's heist bag.
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<1377.2223, -2082.2046, 51.4064>>)
	SET_ENTITY_HEADING(g_sTriggerSceneAssets.object[0], 20.96)

	VECTOR vCutscenePos = << 1376.690, -2082.761, 51.050 >>
	VECTOR vCutsceneRot = << 0.000, 0.000, 117.000 >>
	INT iCutscene = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
	PLAY_SYNCHRONIZED_ENTITY_ANIM(g_sTriggerSceneAssets.object[0], iCutscene, "_LEADIN_LOOP_HEIST_BAG","MISSFBI4LEADINOUTFBI_4_MCS_3" , INSTANT_BLEND_IN )
ENDPROC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4_CREATE()
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	CLEAR_AREA(<<1382.6375,-2073.6797,50.9988>>, 40.0, TRUE)
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1505.3, -2052.0, 100.00>>, <<1252.8, -2034.0, -100.00>>)

	//Create trash truck.
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(TRASH, <<1380.4200, -2072.7695, 51.7607>>, 37.5000)
	SET_MODEL_AS_NO_LONGER_NEEDED(TRASH)
	SET_SIREN_CAN_BE_CONTROLLED_BY_AUDIO(g_sTriggerSceneAssets.veh[0], FALSE)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED)
	
	//Create tow truck.
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(TOWTRUCK, <<1377.1045, -2076.2000, 52.0000>>, 37.5000)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(g_sTriggerSceneAssets.veh[1], true)
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[1], VEHICLELOCK_LOCKED)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[1], 0, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(TOWTRUCK)
	
	//Create Trevor's truck.
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF IS_PLAYER_PED_IN_PERSONAL_VEHICLE (PLAYER_PED_ID())
			g_sTriggerSceneAssets.veh[2] = GET_PLAYERS_LAST_VEHICLE()
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], FALSE, TRUE)
			SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[2], VEHICLELOCK_LOCKED)
		ELSE
			CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[2], CHAR_TREVOR, <<1385.0901,-2070.5645,51.9675>>, 37.5000)
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR))
	
	//Setup the scene for any required player chracters.
	SWITCH GET_CURRENT_PLAYER_PED_ENUM() 
		CASE CHAR_TREVOR
			TS_FBI4_CREATE_MICHAEL_AND_FRANKLIN()
		BREAK
		CASE CHAR_FRANKLIN
			TS_FBI4_CREATE_MICHAEL()
			SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FRANKLIN)
		BREAK
		CASE CHAR_MICHAEL
			SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
			SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_FRANKLIN)
		BREAK
	ENDSWITCH
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_4,
												"FBI_4_MCS_3_CONCAT",
												CS_ALL,
												CS_2|CS_3|CS_4,
												CS_3|CS_4)
												
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])							
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_4, "MICHAEL", g_sTriggerSceneAssets.ped[0])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])							
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_4, "FRANKLIN", g_sTriggerSceneAssets.ped[1])
	ENDIF
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4_RELEASE()
	INT index

	//Release scene vehicles.
	REPEAT 3 index
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[index])
	ENDREPEAT
	
	//Release scene peds.
	REPEAT 2 index
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	//Release the heist bag.
	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4_DELETE()
	INT index

	//Delete scene vehicles.
	REPEAT 3 index
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[index])
	ENDREPEAT
	
	//Delete scene peds.
	REPEAT 2 index
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT
	
	//Delete the heist bag.
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
	
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4_HAS_BEEN_TRIGGERED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
		enumCharacterList eCurrentPlayer = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(eCurrentPlayer) 
			SWITCH eCurrentPlayer
				CASE CHAR_MICHAEL
					IF NOT g_bPlayerLockedInToTrigger
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1382.588623,-2072.575439,50.748219>>, <<1368.920166,-2084.042480,55.998219>>, 18.000000)
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1381.248657,-2077.387207,50.748219>>, <<1372.042603,-2085.124756,55.998215>>, 8.750000)
						RETURN TRUE
					ENDIF
				BREAK
				CASE CHAR_FRANKLIN
					IF NOT g_bPlayerLockedInToTrigger
						//Apply a slowing area.
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1386.697876,-2067.857422,50.748146>>, <<1367.672607,-2083.884766,56.248146>>, 24.500000)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_DIVE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_AIM)
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1384.968872,-2069.659912,50.748146>>, <<1365.263306,-2086.169189,55.998142>>, 22.250000)
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1378.200806,-2076.921387,50.748215>>, <<1369.652222,-2084.022461,55.998219>>, 13.000000)
						RETURN TRUE
					ENDIF
				BREAK
				CASE CHAR_TREVOR
					IF NOT g_bPlayerLockedInToTrigger
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1386.697876,-2067.857422,50.748146>>, <<1367.672607,-2083.884766,56.248146>>, 24.500000)
							SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
								REQUEST_PED_VISIBILITY_TRACKING(g_sTriggerSceneAssets.ped[0])
							ENDIF
						ENDIF
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1379.521240,-2075.928955,50.748219>>, <<1370.848389,-2082.984619,56.248219>>, 13.750000)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		//Do a pull-in cam focusing on Michael.
		IF g_bPlayerLockedInToTrigger
			SWITCH eCurrentPlayer
				CASE CHAR_TREVOR
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF IS_TRACKED_PED_VISIBLE(g_sTriggerSceneAssets.ped[0])
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
									SEQUENCE_INDEX SequenceIndex
									CLEAR_SEQUENCE_TASK(SequenceIndex)
									OPEN_SEQUENCE_TASK(SequenceIndex)
										TASK_LEAVE_ANY_VEHICLE(NULL, 500)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), 1.0)
									CLOSE_SEQUENCE_TASK(SequenceIndex)
									TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
									CLEAR_SEQUENCE_TASK(SequenceIndex)
								ENDIF
							ELSE
								IF NOT IS_GAMEPLAY_HINT_ACTIVE()
									IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
											TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), 1.0)
										ENDIF
										SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
										SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[0], <<0,0,0>>, TRUE, 13000)
										SET_GAMEPLAY_HINT_FOV(30.0)
										SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(0.460)
										SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(0.000)
										SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(-0.02)
										SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(0.100)
									ENDIF
								ELSE
									STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
								ENDIF
							ENDIF
						ELSE
							STOP_GAMEPLAY_HINT()
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
			
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		INT index
	
		//Check for scene peds being injured or harmed.
		REPEAT 5 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN TRUE
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for scene vehicles being damaged or destroyed.
		REPEAT 3 index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[index])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_FRANKLIN
				IF IS_BULLET_IN_ANGLED_AREA(<<1378.638550,-2081.848877,50.498257>>, <<1376.107666,-2084.014648,53.498257>>, 3.000000)
				OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<1378.638550,-2081.848877,50.498257>>, <<1376.107666,-2084.014648,53.498257>>, 3.000000)
					RETURN TRUE
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF IS_BULLET_IN_ANGLED_AREA(<<1373.769287,-2076.116455,50.498257>>, <<1371.826294,-2077.558838,53.498257>>, 2.500000)
				OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<1373.769287,-2076.116455,50.498257>>, <<1371.826294,-2077.558838,53.498257>>, 2.500000)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4_IS_BLOCKED()
	IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P1_M_DONE)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P1_F_DONE)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P1_T_DONE)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P2_M_DONE)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P2_F_DONE)
	OR IS_COMMUNICATION_REGISTERED(CALL_FIB4_P2_T_DONE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_FBI4_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4_AMBIENT_UPDATE()
ENDPROC
