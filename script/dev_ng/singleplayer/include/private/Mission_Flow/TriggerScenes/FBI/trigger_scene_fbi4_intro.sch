
//╒═════════════════════════════════════════════════════════════════════════════╕
//│						FBI 4 Intro - Trigger Scene Data						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								ped[0] = Andreas								│
//│								ped[1] = Dave									│
//│								ped[2] = Steve									│
//│								ped[3] = Michael								│
//│								ped[4] = Trevor									│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "player_ped_public.sch"
USING "commands_audio.sch"
USING "flow_mission_trigger_public.sch"
USING "load_queue_public.sch"


CONST_FLOAT 	TS_FBI4I_STREAM_IN_DIST			220.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FBI4I_STREAM_OUT_DIST		230.0	// Distance at which the scene is deleted and unloaded.
CONST_FLOAT		TS_FBI4I_TRIGGER_DIST			8.0	// Distance from trigger's blip at which the scene triggers.
CONST_FLOAT		TS_FBI4I_CONTACT_DIST			75.0

CONST_FLOAT		TS_FBI4I_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FBI4I_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

FLOAT fHintFov = 30.000
FLOAT fHintFollow = 0.380
FLOAT fHintPitchOrbit = 0.000
FLOAT fHintSide = 0.095
FLOAT fHintVert = 0.050

BOOL bSetPedsReact = FALSE

INT iTriggerTimer = -1
INT iStartDialogueTimer = -1

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID Widg
	BOOL bStopTriggerFireing = FALSE
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(Widg)
			DELETE_WIDGET_GROUP(Widg)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Sets up getaway vehicle widget
	PROC SET_UP_RAG_WIDGETS()
		CLEANUP_OBJECT_WIDGETS()

		Widg = START_WIDGET_GROUP("Trigger")			
			START_WIDGET_GROUP("Hint cam tweaker")
				ADD_WIDGET_BOOL("Stop trigger firing ", bStopTriggerFireing)

				ADD_WIDGET_FLOAT_SLIDER("fHintFov", fHintFov, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("HintFollow", fHintFollow, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fHintPitchOrbit", fHintPitchOrbit, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fHintSide", fHintSide, -99999, 99999, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fHintVert", fHintVert, -99999, 99999, 0.01)
			STOP_WIDGET_GROUP()

		STOP_WIDGET_GROUP()
	ENDPROC

#ENDIF

ENUM TS_FBI4I_LEAD_IN_STATE
	TS_FBI4I_LIS_WAIT,
	TS_FBI4I_LIS_CONTACT,
	TS_FBI4I_LIS_LOCKED,
	TS_FBI4I_LIS_TRIGGERED
ENDENUM

TS_FBI4I_LEAD_IN_STATE TS_FBI4I_eLeadinState = TS_FBI4I_LIS_WAIT
INT TS_FBI4I_LEAD_IN_iTrevScene
/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FBI4I_RESET()
	g_sTriggerSceneAssets.flag = FALSE
	TS_FBI4I_eLeadinState = TS_FBI4I_LIS_WAIT
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FBI4I_REQUEST_ASSETS()
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_ANDREAS))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_DAVE))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, GET_NPC_PED_MODEL(CHAR_STEVE))
	LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_PLAYER_PHONE_01)
	LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missfbi4leadinoutfbi_4_int")
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FBI4I_RELEASE_ASSETS()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FBI4I_HAVE_ASSETS_LOADED()
	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC TS_FBI4I_CREATE_MICHAEL(BOOL bSetupForFrank)
	STRING sAnim
	IF bSetupForFrank
		sAnim = "fbi_4_int_fra_idle_mic"
	ELSE
		sAnim = "fbi_4_int_trv_idle_mic"
	ENDIF
	CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[3], CHAR_MICHAEL, <<1982.1981, 3818.9526, 31.4232>>, 215.9222, FALSE, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.relGroup)
	TASK_SYNCHRONIZED_SCENE (g_sTriggerSceneAssets.ped[3], g_sTriggerSceneAssets.id, "missfbi4leadinoutfbi_4_int", sAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//	WHILE NOT CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[0], CHAR_MICHAEL, <<1388.0695, -2057.9080, 50.9983>>, 263.5418)
//		WAIT(0)
//	ENDWHILE
ENDPROC


PROC TS_FBI4I_CREATE_TREVOR()
	CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[4], CHAR_TREVOR, <<1982.1981, 3818.9526, 31.4232>>, 215.9222, FALSE, TRUE)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[4], g_sTriggerSceneAssets.relGroup)
	TS_FBI4I_LEAD_IN_iTrevScene = CREATE_SYNCHRONIZED_SCENE(<< 1391.657, -2073.055, 51.624 >>, << -0.000, 0.000, 40.320 >>)
	TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[4], TS_FBI4I_LEAD_IN_iTrevScene, "missfbi4leadinoutfbi_4_int", "fbi_4_int_fra_idle_trv", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	SET_SYNCHRONIZED_SCENE_LOOPED(TS_FBI4I_LEAD_IN_iTrevScene, TRUE)
	WHILE NOT CREATE_PLAYER_VEHICLE(g_sTriggerSceneAssets.veh[1], CHAR_TREVOR, <<1382.499878,-2068.988037,51.967495>>, 13.785377)
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
			SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[1], VEHICLELOCK_LOCKED)
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_AGENTS(BOOL bCreatePhone = FALSE)
	//Create Andreas.
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_ANDREAS, <<1390.1227, -2070.5527, 51.0074>>)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_ANDREAS))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
	//Create Dave.
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_DAVE, <<1392.2704, -2071.4844, 51.0172>>)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_DAVE))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], g_sTriggerSceneAssets.relGroup)
	IF bCreatePhone
		g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_PLAYER_PHONE_01, GET_PED_BONE_COORDS(g_sTriggerSceneAssets.ped[1], BONETAG_PH_R_HAND, <<0,0,0>>))
		ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[1], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[1], BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
	ENDIF
	//Create Steve.
	CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[2], CHAR_STEVE, <<1393.0402, -2069.2317, 51.0114>>)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_TORSO, 1, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[2], PED_COMP_DECL, 1, 0)
	SET_PED_PROP_INDEX(g_sTriggerSceneAssets.ped[2], ANCHOR_EYES, 0)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_STEVE))
	TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[2], g_sTriggerSceneAssets.relGroup)

ENDPROC

PROC ASSIGN_AGENT_SCENE(enumCharacterList ePlayer)
	VECTOR vCutscenePos
	VECTOR vCutsceneRot
	INT	iCutscene
	IF ePlayer = CHAR_MICHAEL
		vCutscenePos = << 1390.95569, -2072.54346,51.989666 >>
		vCutsceneRot = << -0.000, 0.000, 40.320 >>
		g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
		iCutscene = g_sTriggerSceneAssets.id
	ELSE
		vCutscenePos = << 1391.657, -2073.055, 51.624 >>
		vCutsceneRot = << -0.000, 0.000, 40.320 >>
		g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
		iCutscene = g_sTriggerSceneAssets.id
	ENDIF
	STRING sDict
	STRING sAndreasAnim
	STRING sDaveAnim
	STRING sSteveAnim

	sDict = "missfbi4leadinoutfbi_4_int"

	IF ePlayer = CHAR_MICHAEL
		sAndreasAnim = "AGENTS_IDLE_A_ANDREAS"
		sDaveAnim = "AGENTS_IDLE_A_DAVE"
		sSteveAnim = "AGENTS_IDLE_B_STEVE"

	ELIF ePlayer = CHAR_FRANKLIN
		
		sAndreasAnim = "fbi_4_int_fra_idle_andreas"
		sDaveAnim = "fbi_4_int_fra_idle_dave"
		sSteveAnim = "fbi_4_int_fra_idle_steve"

	ELIF ePlayer = CHAR_TREVOR
		sAndreasAnim = "fbi_4_int_trv_idle_andreas"
		sDaveAnim = "fbi_4_int_trv_idle_dave"
		sSteveAnim = "fbi_4_int_trv_idle_steve"

	ENDIF
	
	
	//Start synchronized scene on Andreas.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], iCutscene, sDict, sAndreasAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iCutscene, TRUE)
		ENDIF
	ENDIF
	
	//Start synchronized scene on Dave.
	IF ePlayer = CHAR_MICHAEL
		vCutscenePos = <<1392.979, -2071.985, 52.010>>
		vCutsceneRot = << 0.000, 0.000, 26.000 >>
		iCutscene = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
	ENDIF

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], iCutscene, sDict, sDaveAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iCutscene, TRUE)
		ENDIF
	ENDIF

		//Start synchronized scene on Steve.
	IF ePlayer = CHAR_MICHAEL
		vCutscenePos = << 1393.279, -2069.735, 52.010 >>
		vCutsceneRot = << 0.000, 0.000, 33.000 >>
		iCutscene = CREATE_SYNCHRONIZED_SCENE(vCutscenePos, vCutsceneRot)
	ENDIF

	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], iCutscene, sDict, sSteveAnim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iCutscene, TRUE)
		ENDIF
	ENDIF

ENDPROC


PROC CREATE_LEAD_IN_SCENE_FOR_MICHAEL(enumCharacterList ePlayer)
	ASSIGN_AGENT_SCENE(ePlayer)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
ENDPROC

PROC CREATE_LEAD_IN_SCENE_FOR_FRANKLIN(enumCharacterList ePlayer)
	ASSIGN_AGENT_SCENE(ePlayer)
	TS_FBI4I_CREATE_MICHAEL(TRUE)
	TS_FBI4I_CREATE_TREVOR()
ENDPROC

PROC CREATE_LEAD_IN_SCENE_FOR_TREVOR(enumCharacterList ePlayer)
	ASSIGN_AGENT_SCENE(ePlayer)
	TS_FBI4I_CREATE_MICHAEL(FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
ENDPROC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FBI4I_CREATE()
	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
	CLEAR_AREA(<<1382.6375,-2073.6797,50.9988>>, 40.0, TRUE)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	g_sTriggerSceneAssets.scenarioBlocking = ADD_SCENARIO_BLOCKING_AREA(<<1257.3236, -2170.5015, 40.5466>>, <<1510.1860, -1965.2948, 75.6541>>)

	IF ePlayer = CHAR_MICHAEL
		CREATE_AGENTS(TRUE)
	ELSE
		CREATE_AGENTS()
	ENDIF

	SWITCH ePlayer
		CASE CHAR_MICHAEL
			CREATE_LEAD_IN_SCENE_FOR_MICHAEL(ePlayer)
		BREAK

		CASE CHAR_FRANKLIN
			CREATE_LEAD_IN_SCENE_FOR_FRANKLIN(ePlayer)
		BREAK

		CASE CHAR_TREVOR
			CREATE_LEAD_IN_SCENE_FOR_TREVOR(ePlayer)
		BREAK
	
	ENDSWITCH
	
	//Setup the scene for any required player chracters.
//	SWITCH ePlayer 
//		CASE CHAR_FRANKLIN
//			TS_FBI4I_CREATE_MICHAEL()
//			TS_FBI4I_CREATE_TREVOR()
//		BREAK
//		CASE CHAR_TREVOR
//			TS_FBI4I_CREATE_MICHAEL()
//			
//		BREAK
//		CASE CHAR_MICHAEL
//			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
//			SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
//		BREAK
//	ENDSWITCH


	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FBI_4_INTRO,
												"FBI_4_INT",
												CS_ALL,
												CS_4|CS_5|CS_6|CS_7,
												CS_2|CS_3|CS_4|CS_5|CS_6|CS_7)
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_4_INTRO, "steve_FBI", g_sTriggerSceneAssets.ped[2])
		MISSION_FLOW_SET_INTRO_CUTSCENE_PROP(SP_MISSION_FBI_4_INTRO, "steve_FBI", ANCHOR_EYES, 0, 0)
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_4_INTRO, "michael", g_sTriggerSceneAssets.ped[3])
	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FBI_4_INTRO, "trevor", g_sTriggerSceneAssets.ped[4])
	ENDIF

	#IF IS_DEBUG_BUILD
		SET_UP_RAG_WIDGETS()
	#ENDIF

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FBI4I_RELEASE()
	INT index

	//Release scene peds.
	REPEAT 5 index
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT

//	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])

	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FBI4I_DELETE()
	INT index

	//Delete scene peds.
	REPEAT 5 index
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT

//	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])

	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	REMOVE_SCENARIO_BLOCKING_AREA(g_sTriggerSceneAssets.scenarioBlocking)
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FBI4I_HAS_BEEN_TRIGGERED()
	IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM()) 
		IF IS_BIT_SET(g_sMissionStaticData[SP_MISSION_FBI_4_INTRO].triggerCharBitset, GET_CURRENT_PLAYER_PED_INT())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//B* 2005563: Block the player from diving into the scene with a plane
				IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
					VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_I)
					//Render trigger zone debug.
					#IF IS_DEBUG_BUILD
						DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FBI4I_TRIGGER_DIST)
					#ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1411.47339, -2050.92847, 49.99822>>, <<1352.34534, -2098.83472, 80.99826>>, 30)
						IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
							RETURN TRUE
						ENDIF
					ENDIF

					FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)

					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF fDistanceSquaredFromTrigger < (2*2)				
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							RETURN TRUE
						ENDIF
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF fDistanceSquaredFromTrigger < (4*4)				
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							RETURN TRUE
						ENDIF
					ELSE
						IF fDistanceSquaredFromTrigger < (6*6)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							RETURN TRUE
						ENDIF
					ENDIF

					IF TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED
						CPRINTLN(DEBUG_MISSION, "TRIGGERING BECAUSE TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED")					
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC


PROC MAKE_ALL_TRIGGER_PEDS_FLEE()
	INT index
	REPEAT 5 index
		IF g_sTriggerSceneAssets.ped[index] != PLAYER_PED_ID()
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					TASK_SMART_FLEE_PED(g_sTriggerSceneAssets.ped[index], PLAYER_PED_ID(), 500, -1)
					SET_PED_KEEP_TASK(g_sTriggerSceneAssets.ped[index], TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC RESET_AFTER_DISRUPT()
	STOP_GAMEPLAY_HINT()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	CLEAR_PED_TASKS(PLAYER_PED_ID())
ENDPROC

/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FBI4I_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())

		//Check for scene peds being injured or harmed.
		IF IS_BULLET_IN_AREA(<<1393.96460, -2070.81079, 50.99826>>, 50, FALSE)
		OR IS_PROJECTILE_IN_AREA(<<1402.25439, -2045.80835, 50>>, <<1377.43286, -2106.00684, 100>>)
			MAKE_ALL_TRIGGER_PEDS_FLEE()
			RESET_AFTER_DISRUPT()
			RETURN TRUE
		ELSE
			INT index
			REPEAT 5 index
				IF g_sTriggerSceneAssets.ped[index] != PLAYER_PED_ID()
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
						IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(g_sTriggerSceneAssets.ped[index])
								MAKE_ALL_TRIGGER_PEDS_FLEE()
								RESET_AFTER_DISRUPT()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			VEHICLE_INDEX veh = GET_PLAYERS_LAST_VEHICLE()
			VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_I)

			IF DOES_ENTITY_EXIST(veh)
			AND IS_VEHICLE_DRIVEABLE(veh)
				FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(veh), vTriggerPosition)
				IF fDistanceSquaredFromTrigger < (8*8)
					MAKE_ALL_TRIGGER_PEDS_FLEE()
					RESET_AFTER_DISRUPT()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FBI4I_IS_BLOCKED()
	RETURN FALSE
ENDFUNC

PROC AGENTS_REACT(INT i, enumCharacterList ePlayer)
	STRING sAndreasAnim
	STRING sDaveAnim
	STRING sSteveAnim
	
	IF ePlayer = CHAR_MICHAEL
		EXIT

	ELIF ePlayer = CHAR_FRANKLIN
		sAndreasAnim = "fbi_4_int_fra_react_andreas"
		sDaveAnim = "fbi_4_int_fra_react_dave"
		sSteveAnim = "fbi_4_int_fra_react_steve"

	ELIF ePlayer = CHAR_TREVOR
		sAndreasAnim = "fbi_4_int_trv_react_andreas"
		sDaveAnim = "fbi_4_int_trv_react_dave"
		sSteveAnim = "fbi_4_int_trv_react_steve"

	ENDIF
	//Andreas
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[0], i, "missfbi4leadinoutfbi_4_int", sAndreasAnim, WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF

	//Dave
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[1], i, "missfbi4leadinoutfbi_4_int", sDaveAnim, WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF

	//Steve
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[2], i, "missfbi4leadinoutfbi_4_int", sSteveAnim, WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF

ENDPROC

PROC MICHAEL_REACT(INT i, STRING sAnim)
	//Andreas
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[3], i, "missfbi4leadinoutfbi_4_int", sAnim, WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF
ENDPROC

PROC TREVOR_REACT(INT i)
	//Andreas
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
		IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[4])
			TASK_SYNCHRONIZED_SCENE(g_sTriggerSceneAssets.ped[4], i, "missfbi4leadinoutfbi_4_int", "fbi_4_int_fra_react_trv", WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF
ENDPROC


PROC TS_FBI4I_WAIT_CONTACT()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_I)
	FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)

	IF fDistanceSquaredFromTrigger < (TS_FBI4I_CONTACT_DIST*TS_FBI4I_CONTACT_DIST)
		CPRINTLN(DEBUG_MISSION, "TS_FBI4I_eLeadinState = LIS_CONTACT")
		TS_FBI4I_eLeadinState = TS_FBI4I_LIS_CONTACT
	ENDIF
ENDPROC

FUNC BOOL TS_FBI4I_ARE_SCENE_PEDS_ALIVE()
	INT index
	REPEAT 5 index
		IF g_sTriggerSceneAssets.ped[index] != PLAYER_PED_ID()
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[index])
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

PROC HANDLE_HINT_CAM()
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			IF TS_FBI4I_ARE_SCENE_PEDS_ALIVE()
				SET_GAMEPLAY_ENTITY_HINT(g_sTriggerSceneAssets.ped[1], <<0,0,0>>, TRUE, 60000)
				SET_GAMEPLAY_HINT_FOV(fHintFov)
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(TRUE)
				iTriggerTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				SET_GAMEPLAY_HINT_FOV(fHintFov)
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR(fHintFollow)
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET(fHintPitchOrbit)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET(fHintSide)
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET(fHintVert)
			#ENDIF
			STOP_GAMEPLAY_HINT_BEING_CANCELLED_THIS_UPDATE(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC TS_FBI4I_CONTACT()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FBI_OFFICERS4_I)
	FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
	FLOAT fDist = 20.08
	FLOAT fDistPlus = 20.08
	
	IF ePlayer != CHAR_MICHAEL
		IF NOT g_sTriggerSceneAssets.flag
			IF fDistanceSquaredFromTrigger < ((fDist+fDist)*(fDist+fDist))
				STRING sConv
				IF ePlayer = CHAR_TREVOR
					ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, g_sTriggerSceneAssets.ped[3], "MICHAEL")
					ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 3, g_sTriggerSceneAssets.ped[2], "STEVE")
					sConv = "fbi4_INT_LIT"
				ELSE
					ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 2, g_sTriggerSceneAssets.ped[4], "TREVOR")
					iStartDialogueTimer = GET_GAME_TIMER()
					sConv = "fbi4_INT_LIF"
				ENDIF
				g_sTriggerSceneAssets.flag = CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "HeatAud", sConv, CONV_PRIORITY_VERY_HIGH)
			ENDIF
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED
				EXIT
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1403.80029, -2085.14624, 50.99826>>, <<1383.05603, -2060.08057, 90.99826>>, 24.0)
					IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						HANDLE_HINT_CAM()
						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							CPRINTLN(DEBUG_MISSION, "TS_FBI4I_eLeadinState  = LIS_LOCKED")
							TS_FBI4I_eLeadinState = TS_FBI4I_LIS_LOCKED
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH)
	//						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),PEDMOVEBLENDRATIO_WALK, 4500)
							TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						ENDIF
					ENDIF
				ELIF fDistanceSquaredFromTrigger > ((fDistPlus+fDistPlus)*(fDistPlus+fDistPlus))
					g_sTriggerSceneAssets.flag = FALSE
					KILL_ANY_CONVERSATION()
				ENDIF
			ENDIF
		ENDIF
	ELSE


		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1403.80029, -2085.14624, 50.99826>>, <<1383.05603, -2061.08057, 90.99826>>, 23.0)
			IF NOT IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
				HANDLE_HINT_CAM()
				SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					CPRINTLN(DEBUG_MISSION, "TS_FBI4I_eLeadinState  = LIS_LOCKED")
					TS_FBI4I_eLeadinState = TS_FBI4I_LIS_LOCKED
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_REENABLE_CONTROL_ON_DEATH)
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(),PEDMOVEBLENDRATIO_WALK, 4500 )
					TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), vTriggerPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TS_FBI4I_LOCKED()
	HANDLE_HINT_CAM()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	#IF IS_DEBUG_BUILD
		IF bStopTriggerFireing
			g_bPlayerLockedInToTrigger = FALSE	//This is fricking stooopid		
			EXIT
		ELSE
			g_bPlayerLockedInToTrigger = TRUE //works though and debug only
		ENDIF
	#ENDIF
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1403.80029, -2085.14624, 50.99826>>, <<1387.05176, -2065.73267, 90.99826>>, 23.0)
//	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1393.80066, -2073.45337, 50.99826>>, <<1384.63721, -2060.77612, 90.99826>>, 11.0)
		IF NOT bSetPedsReact
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				g_sTriggerSceneAssets.id = CREATE_SYNCHRONIZED_SCENE(<<1391.657, -2073.055, 51.624>>, << -0.000, 0.000, 40.320 >>)
				SET_SYNCHRONIZED_SCENE_LOOPED(g_sTriggerSceneAssets.id, FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(g_sTriggerSceneAssets.id, TRUE)
				AGENTS_REACT(g_sTriggerSceneAssets.id, ePlayer)


				IF ePlayer = CHAR_TREVOR
					MICHAEL_REACT(g_sTriggerSceneAssets.id, "fbi_4_int_trv_react_mic")
				ELIF ePlayer = CHAR_FRANKLIN
					MICHAEL_REACT(g_sTriggerSceneAssets.id, "fbi_4_int_fra_react_mic")
					//TREVOR_REACT(g_sTriggerSceneAssets.id)
				ENDIF

//				KILL_ANY_CONVERSATION() //[MF] Commenting out as fix for B* 1740586
				bSetPedsReact = TRUE
			ELSE
				bSetPedsReact = TRUE
			ENDIF
		ELSE
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF (GET_GAME_TIMER() - iStartDialogueTimer) > 10300
						TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() - iStartDialogueTimer) > 5300
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED
					ENDIF
				ENDIF
			ELSE
				IF (GET_GAME_TIMER() - iTriggerTimer) > 2000
					CPRINTLN(DEBUG_MISSION, "TS_FBI4I_eLeadinState = LIS_TRIGGERED")
					TS_FBI4I_eLeadinState = TS_FBI4I_LIS_TRIGGERED
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.  
PROC TS_FBI4I_UPDATE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SWITCH TS_FBI4I_eLeadinState
			CASE TS_FBI4I_LIS_WAIT
				TS_FBI4I_WAIT_CONTACT()
			BREAK
			
			CASE TS_FBI4I_LIS_CONTACT
				TS_FBI4I_CONTACT()
			BREAK
			
			CASE TS_FBI4I_LIS_LOCKED
				TS_FBI4I_LOCKED()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FBI4I_AMBIENT_UPDATE()
ENDPROC
