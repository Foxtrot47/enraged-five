//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Family 1 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_FAMILY1_STREAM_IN_DIST		75.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FAMILY1_STREAM_OUT_DIST		100.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FAMILY1_TRIGGER_DIST			3.0		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FAMILY1_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FAMILY1_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FAMILY1_RESET()
	g_sTriggerSceneAssets.flag = FALSE
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FAMILY1_REQUEST_ASSETS()
//	REQUEST_ANIM_DICT("missfam1leadinoutfamily_1_intp1_3")
//	REQUEST_MODEL(P_TUMBLER_02_s1)
//	REQUEST_MODEL(PROP_CIGAR_02)
//	REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FAMILY1_RELEASE_ASSETS()
//	REMOVE_ANIM_DICT("missfam1leadinoutfamily_1_intp1_3")
//	SET_MODEL_AS_NO_LONGER_NEEDED(P_TUMBLER_02_s1)
//	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CIGAR_02)
//	SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FAMILY1_HAVE_ASSETS_LOADED()
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		IF NOT TRIGGER_SCENE_PRELOAD_DUMMY_PLAYER_WITH_VARIATIONS(g_sTriggerSceneAssets.ped[0], CHAR_MICHAEL, << -780.647, 187.402, 72.810 >>, COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1)
//		OR NOT HAS_ANIM_DICT_LOADED("missfam1leadinoutfamily_1_intp1_3")
//		OR NOT HAS_MODEL_LOADED(P_TUMBLER_02_s1)
//		OR NOT HAS_MODEL_LOADED(PROP_CIGAR_02)
//			RETURN FALSE
//		ENDIF
//	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FAMILY1_CREATE()

//	ADD_RELATIONSHIP_GROUP("Player Group", g_sTriggerSceneAssets.relGroup)
//
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		/* START SYNCHRONIZED SCENE -  */
//		//VECTOR scenePosition = << -780.647, 187.402, 72.810 >>
//		//VECTOR sceneRotation = << 0.000, 0.000, 109.440 >>
//		//INT sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//		//TASK_SYNCHRONIZED_SCENE (ENTER_PED_HERE, sceneId, "missfam1leadinoutfamily_1_intp1_3", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//		
//		//A dummy Michael has already been created invisible during the loading process.
//		//Reveal him and start him playing his anims.
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//			TRIGGER_SCENE_REVEAL_DUMMY_PED(g_sTriggerSceneAssets.ped[0])
//			TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.relGroup)
//			SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
//			FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], TRUE)
//			TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missfam1leadinoutfamily_1_intp1_3", "base", << -780.647, 187.402, 72.90 >>, << 0.000, 0.000, 109.440 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_OVERRIDE_PHYSICS)
//			
//			g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(P_TUMBLER_02_s1, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]))
//			g_sTriggerSceneAssets.object[1] = CREATE_OBJECT(PROP_CIGAR_02, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]))
//			ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[0], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//			ATTACH_ENTITY_TO_ENTITY(g_sTriggerSceneAssets.object[1], g_sTriggerSceneAssets.ped[0], GET_PED_BONE_INDEX(g_sTriggerSceneAssets.ped[0], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//			STOP_PED_SPEAKING(g_sTriggerSceneAssets.ped[0], TRUE)
//		ENDIF
//	ENDIF
//
//	//Start preloading intro cutscene for this mission.
//	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FAMILY_1,		
//												"FAMILY_1_INT",			
//												CS_ALL,								
//												CS_2|CS_3|CS_4|CS_5|CS_6|CS_7|CS_8,
//												CS_NONE)
//
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FAMILY_1, "Michael", PLAYER_ZERO, OUTFIT_P0_SHIRT_SHORTS_1)
//
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FAMILY_1, "Franklin", PLAYER_ONE, OUTFIT_P1_WHITE_SHIRT_JEANS)
//	ENDIF
	
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FAMILY1_RELEASE()
//	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[0])
//	TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[1])
//	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
//		
//	REMOVE_ANIM_DICT("missfam1leadinoutfamily_1_intp1_3")
//	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FAMILY1_DELETE()
//	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
//	TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
//	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
//		
//	REMOVE_ANIM_DICT("missfam1leadinoutfamily_1_intp1_3")
//	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FAMILY1_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_1, GET_CURRENT_PLAYER_PED_INT())
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FAMILY1_TRIGGER_DIST)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		
		IF fDistanceSquaredFromTrigger < (TS_FAMILY1_TRIGGER_DIST*TS_FAMILY1_TRIGGER_DIST)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-814.722473,176.353958,74.653091>>, <<-816.431702,181.587433,71.001167>>, 2.000000)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-814.722473,176.353958,74.653091>>, <<-816.431702,181.587433,71.001167>>, 2.000000)
				g_sTriggerSceneAssets.flag = TRUE
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_L))
                	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.025, FALSE, TRUE)
                	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
            	ENDIF
            	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
            		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.025, FALSE, TRUE)
                	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
            	ENDIF
			ELSE
				g_sTriggerSceneAssets.flag = FALSE
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FAMILY1_HAS_BEEN_DISRUPTED()

//	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//		IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
//			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(g_sTriggerSceneAssets.ped[0], PLAYER_PED_ID())
//			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0]), 15.0)
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FAMILY1_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FAMILY1_UPDATE()
	
//	//IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
//		//IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
//			//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[0])) < 20.0
//			IF NOT g_bPlayerLockedInToTrigger
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//					//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-793.464294,184.525650,71.457512>>, <<-769.445496,184.852295,73.639839>>, 16.750000)
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-794.771240,185.166229,70.734795>>, <<-768.758179,186.472931,74.716957>>, 16.500000)
//						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
//					ENDIF
//				ENDIF
//				
//				VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_1, GET_CURRENT_PLAYER_PED_INT())
//				FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
//				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//					IF fDistanceSquaredFromTrigger < (TS_FAMILY1_TRIGGER_DIST * 2)
//						SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
//					ENDIF
//				ENDIF
//			ENDIF
//		
//		//ENDIF
//	//ENDIF

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FAMILY1_AMBIENT_UPDATE()
ENDPROC
