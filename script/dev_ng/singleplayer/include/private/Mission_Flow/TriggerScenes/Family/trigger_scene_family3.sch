//+-----------------------------------------------------------------------------+
//¦						Family 3 - Trigger Scene Data							¦
//¦																				¦
//¦				Author: Ben Rollinson				Date: 08/07/12				¦
//¦-----------------------------------------------------------------------------¦
//¦								ped[0] 	  = Tennis coach						¦
//¦								veh[0] 	  = Pickup truck						¦
//¦								veh[1] 	  = Tennis coach's car					¦
//¦								object[0] = Tennis Racket_1						¦
//¦								object[1] = Tennis Racket_2						¦
//+-----------------------------------------------------------------------------+

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "vehicle_gen_public.sch"
USING "load_queue_public.sch"

CONST_FLOAT 	TS_FAMILY3_STREAM_IN_DIST			100.0								// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FAMILY3_STREAM_OUT_DIST			120.0								// Distance at which the scene is deleted and unloaded.

CONST_FLOAT		TS_FAMILY3_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FAMILY3_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.

INT				TS_FAMILY3_TENNIS_COACH_PATH		= -1
INT				TS_FAMILY3_TENNIS_COACH_PROGRESS	= 0

BOOL			TS_FAMILY3_CONVERSATION_STARTED

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FAMILY3_RESET()

	TS_FAMILY3_CONVERSATION_STARTED		= FALSE
	TS_FAMILY3_TENNIS_COACH_PATH		= -1
	TS_FAMILY3_TENNIS_COACH_PROGRESS	= 0
	
ENDPROC

/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FAMILY3_REQUEST_ASSETS()

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BLISTA)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BISON2)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_TENNIS_RACK_01)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_TENNIS_RACK_01B)
		BREAK
		CASE CHAR_FRANKLIN
			LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "get_up@directional@movement@from_knees@panic")
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BLISTA)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, BISON2)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, IG_TENNISCOACH)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_TENNIS_RACK_01)
			LOAD_QUEUE_MEDIUM_ADD_MODEL(g_sTriggerSceneAssets.loadQueue, PROP_TENNIS_RACK_01B)
			LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(g_sTriggerSceneAssets.loadQueue, "missfam3leadinoutfam_3_int")
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FAMILY3_RELEASE_ASSETS()
	RELEASE_SCRIPT_AUDIO_BANK()
	CLEANUP_LOAD_QUEUE_MEDIUM(g_sTriggerSceneAssets.loadQueue)
ENDPROC

/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FAMILY3_HAVE_ASSETS_LOADED()

	IF HAS_LOAD_QUEUE_MEDIUM_LOADED(g_sTriggerSceneAssets.loadQueue)
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC TS_FAMILY3_SETUP_PED_FOR_TRIGGER_SCENE(PED_INDEX PedIndex)
	
	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
															 			
			SET_PED_AS_ENEMY(PedIndex, FALSE)													//set ped properties
			SET_PED_CAN_BE_TARGETTED(PedIndex, FALSE)
			SET_PED_SUFFERS_CRITICAL_HITS(PedIndex, FALSE)
			SET_ENTITY_IS_TARGET_PRIORITY(PedIndex, FALSE)
			SET_PED_CAN_RAGDOLL(PedIndex, FALSE)
			SET_RAGDOLL_BLOCKING_FLAGS(PedIndex, RBF_PLAYER_IMPACT)
			SET_PED_DIES_WHEN_INJURED(PedIndex, FALSE)
			SET_PED_CAN_EVASIVE_DIVE(PedIndex, FALSE)
			
			SET_PED_CONFIG_FLAG(PedIndex, PCF_CannotBeTargeted, TRUE)							 //ped config flags
			SET_PED_CONFIG_FLAG(PedIndex, PCF_DisableExplosionReactions, TRUE)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(PedIndex, g_sTriggerSceneAssets.relGroup)			 //ped rel group hash
			
		    SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedIndex, TRUE)								 //block events
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FAMILY3_CREATE()
	CLEAR_AREA(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_3), 30.0, TRUE)
	
	//remove vehicle gen car and disable the vehicle gen
	//DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MICHAEL_SAVEHOUSE)
	
	REMOVE_RELATIONSHIP_GROUP(g_sTriggerSceneAssets.relGroup)
	ADD_RELATIONSHIP_GROUP("FAMILY_3_TS_RELGROUP", g_sTriggerSceneAssets.relGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, g_sTriggerSceneAssets.relGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, g_sTriggerSceneAssets.relGroup)
	
	//create tennis coach
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_FRANKLIN
			
			//create Tennis coach
			IF CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_TENNIS_COACH, <<-811.6234, 167.6178, 75.7124>>, 116.2182)
				
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_TORSO, 	1, 0)
				SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_LEG, 	1, 0)
			
				TS_FAMILY3_SETUP_PED_FOR_TRIGGER_SCENE(g_sTriggerSceneAssets.ped[0])
				
				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, NULL, "MICHAEL")
				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 2, NULL, "AMANDA")
				ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 3, g_sTriggerSceneAssets.ped[0], "TENNISCOACH")
				
				SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[0], FALSE)
				SET_ENTITY_INVINCIBLE(g_sTriggerSceneAssets.ped[0], TRUE)
				SET_PED_PATH_AVOID_FIRE(g_sTriggerSceneAssets.ped[0], FALSE)
				SET_ENTITY_PROOFS(g_sTriggerSceneAssets.ped[0], TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_TENNISCOACH)
				
			ENDIF
			
		BREAK
	ENDSWITCH	

	//create truck
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(BISON2, << -827.0914, 176.4763, 69.9637 >>, 148.6507)
	SET_VEHICLE_EXTRA(g_sTriggerSceneAssets.veh[0], 5, FALSE)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[0], 132, 0)
	SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0])
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[0], VEHICLELOCK_LOCKED)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[0], FALSE)	//block the car from being attached to tow truck
	SET_VEHICLE_CAN_LEAK_PETROL(g_sTriggerSceneAssets.veh[0], FALSE)		//block the car from leaking oil and petrol, see B*1910599
	SET_VEHICLE_CAN_LEAK_OIL(g_sTriggerSceneAssets.veh[0], FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(BISON2)
	
	//create Tennis Coach's car
	g_sTriggerSceneAssets.veh[1] = CREATE_VEHICLE(BLISTA, <<-826.2942, 158.1811, 68.3918>>, 84.0848)
	SET_VEHICLE_COLOURS(g_sTriggerSceneAssets.veh[1], 43, 43)
	SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1])
	SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[1], VEHICLELOCK_LOCKED)
	SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(g_sTriggerSceneAssets.veh[1], FALSE)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(g_sTriggerSceneAssets.veh[1], FALSE)	//block the car from being attached to tow truck
	SET_VEHICLE_CAN_LEAK_PETROL(g_sTriggerSceneAssets.veh[1], FALSE)		//block the car from leaking oil and petrol, see B*1910599
	SET_VEHICLE_CAN_LEAK_OIL(g_sTriggerSceneAssets.veh[1], FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(BLISTA)
	
	//create tennis rackets
	g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_TENNIS_RACK_01, <<-818.0380, 179.2600, 71.2520>>)
	SET_ENTITY_COORDS_NO_OFFSET(g_sTriggerSceneAssets.object[0], <<-818.0380, 179.2600, 71.2520>>)
	SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[0], <<-20.5200, -74.0200, 14.3000>>)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[0], TRUE)
	
	g_sTriggerSceneAssets.object[1] = CREATE_OBJECT(PROP_TENNIS_RACK_01B, <<-818.1280, 179.1527, 71.2620>>)
	SET_ENTITY_COORDS_NO_OFFSET(g_sTriggerSceneAssets.object[1], <<-818.1280, 179.1527, 71.2620>>)
	SET_ENTITY_ROTATION(g_sTriggerSceneAssets.object[1], <<-39.2400, -75.9600, 16.2000>>)
	FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[1], TRUE)
			
	SET_MODEL_AS_NO_LONGER_NEEDED(PROP_TENNIS_RACK_01)
	
	//Start preloading intro cutscene for this mission.
	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FAMILY_3, "FAMILY_3_INT", CS_ALL, CS_4|CS_5, CS_NONE)
												
	//request variations depending on current player ped for the other player ped								
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()

		CASE CHAR_MICHAEL
		
			//MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FAMILY_3, "Michael", PLAYER_PED_ID())
			
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FAMILY_3, "Franklin", PLAYER_ONE, OUTFIT_P1_GREEN_SHIRT_JEANS)
			
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "TennisCoach", 	PED_COMP_TORSO, 	1, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "TennisCoach", 	PED_COMP_LEG, 		1, 0)
			
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_HAIR, 		1, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_TORSO, 	2, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_LEG, 		2, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_DECL, 		1, 0)
			
		BREAK
		
		CASE CHAR_FRANKLIN
		
			//MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_PED(SP_MISSION_FAMILY_3, "Franklin", PLAYER_PED_ID())
			
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FAMILY_3, "Michael", PLAYER_ZERO, OUTFIT_P0_POLOSHIRT_JEANS_1)
			
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_HAIR, 		1, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_TORSO, 	2, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_LEG, 		2, 0)
			MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATION(SP_MISSION_FAMILY_3, "Amanda", 		PED_COMP_DECL, 		1, 0)
			
		BREAK
	
	ENDSWITCH		
	
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FAMILY3_RELEASE()
	INT index
	//Release all scene peds
	REPEAT 1 index
		TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT

	//Release all scene vehicles.
	REPEAT 3 index
		TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[index])
	ENDREPEAT
	
	//Release all scene objects
	REPEAT 2 index
		TRIGGER_SCENE_RELEASE_OBJECT(g_sTriggerSceneAssets.object[index])
	ENDREPEAT
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door DOORHASH_M_MANSION_BEDROOM from system.")
		#ENDIF
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FAMILY3_DELETE()
	INT index
	//Delete all scene peds
	REPEAT 1 index
		TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[index])
	ENDREPEAT

	//Delete all scene vehicles.
	REPEAT 3 index
		TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[index])
	ENDREPEAT
	
	//Delete all scene objects
	REPEAT 2 index
		TRIGGER_SCENE_DELETE_OBJECT(g_sTriggerSceneAssets.object[index])
	ENDREPEAT
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), DOORSTATE_UNLOCKED, FALSE, TRUE)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		REMOVE_DOOR_FROM_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Removing door DOORHASH_M_MANSION_BEDROOM from system.")
		#ENDIF
	ENDIF
	
	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FAMILY3_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_3, GET_CURRENT_PLAYER_PED_INT())

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, LOCATE_SIZE_MISSION_TRIGGER)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		
	
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		
			CASE CHAR_MICHAEL
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-818.509583,181.256790,70.514099>>, <<-816.144043,175.071472,75.211678>>, 6.0)
					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
				ENDIF
				
				IF fDistanceSquaredFromTrigger < (LOCATE_SIZE_MISSION_TRIGGER*LOCATE_SIZE_MISSION_TRIGGER)
				
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-817.376404,179.819931,71.127319>>, <<-816.115845,176.531265,74.727318>>, 3.5) //increase the width to allow for the cutscene to trigger earlier
				
						IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), DOORSTATE_UNLOCKED, FALSE, TRUE)
						ENDIF

						IF TS_FAMILY3_CONVERSATION_STARTED
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
								CLEAR_PED_TASKS(g_sTriggerSceneAssets.ped[0])
							ENDIF
						ENDIF
						
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						
						RETURN TRUE
				
					ENDIF
				ENDIF
			
			BREAK
			
			CASE CHAR_FRANKLIN
			
				IF fDistanceSquaredFromTrigger < (LOCATE_SIZE_MISSION_TRIGGER*LOCATE_SIZE_MISSION_TRIGGER)
			
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
						DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), DOORSTATE_UNLOCKED, FALSE, TRUE)
					ENDIF

					IF TS_FAMILY3_CONVERSATION_STARTED
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
							CLEAR_PED_TASKS(g_sTriggerSceneAssets.ped[0])
						ENDIF
					ENDIF
					
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					
					RETURN TRUE
					
				ENDIF
			
			BREAK
			
		ENDSWITCH

	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FAMILY3_HAS_BEEN_DISRUPTED()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//player cannot disrupt this scene
		//all vehicles will be recreated during intro cutscene if player damaged any of the vehicles
		//no peds are involved in trigger scene
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FAMILY3_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene. 
PROC TS_FAMILY3_UPDATE()

	//SET_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MICHAEL_SAVEHOUSE)

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
		
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
				ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), V_ILEV_MM_DOORW, <<-809.281,177.855,76.890>>, FALSE, FALSE)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Adding door DOORHASH_M_MANSION_BEDROOM to system.")
				#ENDIF
			ENDIF
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM))
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), 0.0, FALSE, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_BEDROOM), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
			ENDIF
		
			IF NOT TS_FAMILY3_CONVERSATION_STARTED
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -809.742432, 177.312668, 76.940727 >>, << 0.5, 0.5, 1.2 >>)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							ADD_PED_FOR_DIALOGUE(g_sTriggerSceneAssets.conversation, 0, PLAYER_PED_ID(), "MICHAEL")
							IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "FAM3AUD", "FAM3_INTLID", CONV_PRIORITY_MEDIUM)
								TS_FAMILY3_CONVERSATION_STARTED = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CHAR_FRANKLIN
		
			
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
				
					REQUEST_SCRIPT_AUDIO_BANK("FAMILY3_2")
				
					SWITCH TS_FAMILY3_TENNIS_COACH_PROGRESS
					
						CASE 0
						
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-827.604431,186.856827,67.198853>>, <<-817.856567,162.987122,75.108208>>, 12.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-829.089478,166.362167,66.578064>>, <<-818.642212,176.902878,75.027519>>, 16.0)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-833.989807,178.300598,67.987930>>, <<-822.476074,176.727783,75.278336>>, 12.7)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-819.062073,189.457870,69.491692>>, <<-817.643616,176.622314,75.227425>>, 6.0)
								
									TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "missfam3leadinoutfam_3_int", "_leadout_coach",
															<< -817.395, 179.250, 71.200 >>, << 0.0, 0.0, -115.0 >>,
															INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET
															| AF_OVERRIDE_PHYSICS | AF_HOLD_LAST_FRAME, 0, EULER_YXZ, AIK_DISABLE_LEG_IK)
									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(g_sTriggerSceneAssets.ped[0])
									SET_PED_PATH_AVOID_FIRE(g_sTriggerSceneAssets.ped[0], FALSE)
									SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[0], TRUE)
									CLEAR_ROOM_FOR_ENTITY(g_sTriggerSceneAssets.ped[0])
									
									IF REQUEST_SCRIPT_AUDIO_BANK("FAMILY3_2")
										PLAY_SOUND_FROM_ENTITY(-1, "FAMILY3_COACH_OUT_WINDOW_MASTER", g_sTriggerSceneAssets.ped[0])
									ENDIF
									
									SET_VEHICLE_DOORS_LOCKED(g_sTriggerSceneAssets.veh[1], VEHICLELOCK_UNLOCKED)
									
									SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
									//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), << -816.99, 178.10, 71.23 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 3, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
									
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
									ENDIF
									
									TS_FAMILY3_TENNIS_COACH_PROGRESS++
									
								ENDIF
							ENDIF
							
						
						BREAK
						
						CASE 1
						
							IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "missfam3leadinoutfam_3_int", "_leadout_coach")
							
								IF NOT IS_PED_HEADTRACKING_PED(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0])
									TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), g_sTriggerSceneAssets.ped[0], INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
								ENDIF
							
								IF NOT TS_FAMILY3_CONVERSATION_STARTED
									IF GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "missfam3leadinoutfam_3_int", "_leadout_coach") > 0.25
										IF CREATE_CONVERSATION(g_sTriggerSceneAssets.conversation, "FAM3AUD", "FAM3_GROUND", CONV_PRIORITY_MEDIUM)
											TS_FAMILY3_CONVERSATION_STARTED = TRUE
										ENDIF
									ENDIF
								ENDIF
								
								IF GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "missfam3leadinoutfam_3_int", "_leadout_coach") >= 0.95

									IF 	NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-818.002319,165.636063,72.426025>>,<<8.000000,4.000000,4.000000>>)
									AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-818.002319,165.636063,72.426025>>,<<8.000000,4.000000,4.000000>>)
									
										TS_FAMILY3_TENNIS_COACH_PATH = 0
										
										TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_180",
													   WALK_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME | AF_TAG_SYNC_OUT)
									
									ELIF 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-831.520691,167.633026,66.517761>>, <<-826.665894,178.933685,74.305206>>, 8.000000)
									AND 	NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-833.469238,164.453995,65.800903>>, <<-831.257263,168.378433,72.682831>>, 10.000000)
									
										TS_FAMILY3_TENNIS_COACH_PATH = 1
										
										TASK_PLAY_ANIM(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_0",
													   WALK_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME | AF_TAG_SYNC_OUT)
									
									ENDIF
									
									#IF IS_DEBUG_BUILD
										PRINTLN(GET_THIS_SCRIPT_NAME(), ": Tennis coach path is ", TS_FAMILY3_TENNIS_COACH_PATH, ".")
									#ENDIF
									
									TS_FAMILY3_TENNIS_COACH_PROGRESS++
								
								ENDIF
								
							ENDIF

						BREAK
						
						CASE 2
						
							SWITCH TS_FAMILY3_TENNIS_COACH_PATH
							
								CASE 0
								
									IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_180")
										IF GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_180") >= 0.525

											SEQUENCE_INDEX SequenceIndex
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -815.77, 163.63, 70.34 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
												IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
													IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[1], VS_DRIVER)
														TASK_ENTER_VEHICLE(NULL, g_sTriggerSceneAssets.veh[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY)
													ELSE
														TASK_ENTER_VEHICLE(NULL, g_sTriggerSceneAssets.veh[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN)
													ENDIF
												ELSE
													TASK_SMART_FLEE_COORD(NULL, << -815.77, 163.63, 70.34 >>, 100.0, INFINITE_TASK_TIME)
												ENDIF
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[0], SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											
											FORCE_PED_MOTION_STATE(g_sTriggerSceneAssets.ped[0], MS_ON_FOOT_RUN, FALSE)
											SET_PED_PATH_AVOID_FIRE(g_sTriggerSceneAssets.ped[0], FALSE)
											
											TS_FAMILY3_TENNIS_COACH_PROGRESS++
							
										ENDIF
									ENDIF
								
								BREAK

								CASE 1
								
									IF IS_ENTITY_PLAYING_ANIM(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_0")
										IF GET_ENTITY_ANIM_CURRENT_TIME(g_sTriggerSceneAssets.ped[0], "get_up@directional@movement@from_knees@panic", "get_up_0") >= 0.525

											SEQUENCE_INDEX SequenceIndex
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											OPEN_SEQUENCE_TASK(SequenceIndex)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -831.35, 165.95, 68.27 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
												IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
													IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(g_sTriggerSceneAssets.ped[0], g_sTriggerSceneAssets.veh[1], VS_DRIVER)
														TASK_ENTER_VEHICLE(NULL, g_sTriggerSceneAssets.veh[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY)
													ELSE
														TASK_ENTER_VEHICLE(NULL, g_sTriggerSceneAssets.veh[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN)
													ENDIF
												ELSE
													TASK_SMART_FLEE_COORD(NULL, << -831.35, 165.95, 68.27 >>, 100.0, INFINITE_TASK_TIME)
												ENDIF
											CLOSE_SEQUENCE_TASK(SequenceIndex)
											TASK_PERFORM_SEQUENCE(g_sTriggerSceneAssets.ped[0], SequenceIndex)
											CLEAR_SEQUENCE_TASK(SequenceIndex)
											
											FORCE_PED_MOTION_STATE(g_sTriggerSceneAssets.ped[0], MS_ON_FOOT_RUN, FALSE)
											SET_PED_PATH_AVOID_FIRE(g_sTriggerSceneAssets.ped[0], FALSE)
											
											TS_FAMILY3_TENNIS_COACH_PROGRESS++
							
										ENDIF
									ENDIF
								
								BREAK
								
							ENDSWITCH
						
						BREAK
					
					ENDSWITCH
				
				ENDIF
			ENDIF
		
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FAMILY3_AMBIENT_UPDATE()
ENDPROC
