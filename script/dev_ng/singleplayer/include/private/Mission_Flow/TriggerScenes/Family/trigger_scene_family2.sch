//╒═════════════════════════════════════════════════════════════════════════════╕
//│						Family 2 - Trigger Scene Data							│
//│																				│
//│								Date: 08/01/13									│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│			Keep a key of what the global indexes are used for here.			│	
//╘═════════════════════════════════════════════════════════════════════════════╛

// Create entities using the global entity indexes found in g_sTriggerSceneAssets
// This means your mission script will be able to reference the indexes and grab
// ownership later.

USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"
USING "locates_public.sch"


CONST_FLOAT 	TS_FAMILY2_STREAM_IN_DIST		50.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_FAMILY2_STREAM_OUT_DIST		75.0	// Distance at which the scene is deleted and unloaded.	
CONST_FLOAT		TS_FAMILY2_TRIGGER_DIST			2.5		// Distance from trigger's blip at which the scene triggers.

CONST_FLOAT		TS_FAMILY2_FRIEND_REJECT_DIST	TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_FAMILY2_FRIEND_ACCEPT_BITS	BIT_NOBODY							//Friends who can trigger the mission with the player.

/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_FAMILY2_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_FAMILY2_REQUEST_ASSETS()
	REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_JIMMY))
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_FAMILY2_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_JIMMY))
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_FAMILY2_HAVE_ASSETS_LOADED()
	RETURN HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_JIMMY))
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_FAMILY2_CREATE()

	BOOL bAssetsReady = FALSE
	
	WHILE NOT CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_JIMMY, <<-805.7495, 168.2395, 75.7504>>, 84.5596)
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.ped[0], TRUE)
		SET_ENTITY_COLLISION(g_sTriggerSceneAssets.ped[0], FALSE)
		SET_ENTITY_VISIBLE(g_sTriggerSceneAssets.ped[0], FALSE)
		SET_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0], PED_COMP_BERD, 	0, 0)
		SET_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0], PED_COMP_TORSO, 	0, 0)
		SET_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0], PED_COMP_LEG, 		0, 0)
		SET_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0], PED_COMP_FEET, 	0, 0)
		SET_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0], PED_COMP_DECL, 	0, 0)
	ENDIF
	
	WHILE NOT bAssetsReady
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(g_sTriggerSceneAssets.ped[0])
			bAssetsReady = TRUE
		ENDIF
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
	AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_BERD, 	0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_TORSO, 	0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_LEG,		0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_FEET, 	0, 0)
		SET_PED_COMPONENT_VARIATION(g_sTriggerSceneAssets.ped[0], PED_COMP_DECL, 	0, 0)
		RELEASE_PED_PRELOAD_VARIATION_DATA(g_sTriggerSceneAssets.ped[0])
	ENDIF
	
	//Start preloading intro cutscene for this mission.
//	MISSION_FLOW_REQUEST_MISSION_INTRO_CUTSCENE(SP_MISSION_FAMILY_2,
//												"FAMILY_2_INT",
//												CS_ALL,
//												CS_NONE,
//												CS_NONE)
												
//	MISSION_FLOW_SET_INTRO_CUTSCENE_VARIATIONS_FROM_OUTFIT(SP_MISSION_FAMILY_2, "Michael", GET_PLAYER_PED_MODEL(CHAR_MICHAEL), OUTFIT_P0_YOGA_FLIP_FLOPS)

ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_FAMILY2_RELEASE()
	TRIGGER_SCENE_RELEASE_PED_FLEE_PLAYER(g_sTriggerSceneAssets.ped[0])
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_FAMILY2_DELETE()
	TRIGGER_SCENE_DELETE_PED(g_sTriggerSceneAssets.ped[0])
//	MISSION_FLOW_CLEANUP_MISSION_INTRO_CUTSCENE()
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_FAMILY2_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_2)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, TS_FAMILY2_TRIGGER_DIST)
		#ENDIF
		
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (3.5*3.5)
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTriggerPosition, <<1,1,g_vAnyMeansLocate.z>>, TRUE)
		
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						WAIT(0)
					ENDWHILE
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			AND NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				IF GET_PED_DRAWABLE_VARIATION(g_sTriggerSceneAssets.ped[0]	,	 PED_COMP_BERD) 	= 	0
				AND GET_PED_DRAWABLE_VARIATION(g_sTriggerSceneAssets.ped[0]	,	 PED_COMP_TORSO) 	= 	0
				AND GET_PED_DRAWABLE_VARIATION(g_sTriggerSceneAssets.ped[0]	,	 PED_COMP_LEG) 		= 	0
				AND GET_PED_DRAWABLE_VARIATION(g_sTriggerSceneAssets.ped[0]	,	 PED_COMP_FEET)	 	= 	0
				AND GET_PED_DRAWABLE_VARIATION(g_sTriggerSceneAssets.ped[0]	,	 PED_COMP_DECL) 	= 	0
					SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
					RETURN TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_FAMILY2_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_FAMILY2_IS_BLOCKED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_FAMILY2_UPDATE()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-815.594543,182.771591,66.904427>>, <<-828.966370,178.474289,74.645470>>, 12.750000)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_FAMILY2_AMBIENT_UPDATE()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-815.594543,182.771591,66.904427>>, <<-828.966370,178.474289,74.645470>>, 12.750000)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
		ENDIF
	ENDIF
	
ENDPROC
