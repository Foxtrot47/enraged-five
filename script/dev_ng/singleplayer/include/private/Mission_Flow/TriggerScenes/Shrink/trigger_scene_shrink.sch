//╒═════════════════════════════════════════════════════════════════════════════╕
//│					Shrink 1/2/5 - Trigger Scene Data							│
//│																				│
//│				Author: Ben Rollinson				Date: 08/07/12				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│								veh[0] = Shrink's car							│
//╘═════════════════════════════════════════════════════════════════════════════╛
	
USING "rage_builtins.sch"
USING "globals.sch"
USING "trigger_scene_private.sch"
USING "script_heist.sch"


CONST_FLOAT 	TS_SHRINK_STREAM_IN_DIST		150.0	// Distance at which the scene loads and is created.
CONST_FLOAT		TS_SHRINK_STREAM_OUT_DIST		190.0	// Distance at which the scene is deleted and unloaded.

CONST_FLOAT		TS_SHRINK_FRIEND_REJECT_DIST		TS_DEFAULT_FRIEND_REJECT_DISTANCE	//Distance friends will bail from player group.
CONST_INT		TS_SHRINK_FRIEND_ACCEPT_BITS		BIT_NOBODY							//Friends who can trigger the mission with the player.


/// PURPOSE: 	Use to set any variables declared in this header to the state they should
///    			be in the first time the trigger scene logic is run in the gameflow.
PROC TS_SHRINK_RESET()
ENDPROC


/// PURPOSE: 	Loads the assets required to create the scene. Runs when the player
/// 			enters the load range of the scene.
PROC TS_SHRINK_REQUEST_ASSETS()
	REQUEST_MODEL(COMET2)
ENDPROC


/// PURPOSE: 	Releases the scenes loaded assets from memory. Runs when the player leaves
///    			the unload range of the scene or when the flow tells the scene to cleanup.
PROC TS_SHRINK_RELEASE_ASSETS()
	SET_MODEL_AS_NO_LONGER_NEEDED(COMET2)
ENDPROC


/// PURPOSE: 	Checks that assets required to create the scene are in memory. Runs while the
/// 			player is in load range of the scene and assets haven't loaded yet.
FUNC BOOL TS_SHRINK_HAVE_ASSETS_LOADED()
	IF HAS_MODEL_LOADED(COMET2)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: 	Creates entities for the scene. Runs while the player is in load range of the
///    			scene as soon as all required assets have loaded.
PROC TS_SHRINK_CREATE()
	CLEAR_AREA(<<-1894.9221, -565.0509, 11.3552>>, 3.0, TRUE)
	
	//Create Shrink's car.
	g_sTriggerSceneAssets.veh[0] = CREATE_VEHICLE(COMET2, <<-1894.9221, -565.0509, 11.3552>>, 49.63)
	SET_MODEL_AS_NO_LONGER_NEEDED(COMET2)
	SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0])
	SET_VEHICLE_COLOUR_COMBINATION(g_sTriggerSceneAssets.veh[0], 6)
ENDPROC


/// PURPOSE:	Set created entities in the scene as no longer needed and set them to disperse
/// 			in a natural manner. Used to clean up a scene while it is visible to the player. 
///    			Runs when the player distrupts a scene before triggering it.
PROC TS_SHRINK_RELEASE()
	TRIGGER_SCENE_RELEASE_VEHICLE(g_sTriggerSceneAssets.veh[0])
ENDPROC


/// PURPOSE:	Delete created entities in the scene from the world. Runs when the player leaves
///    			the unload range of the scene.
PROC TS_SHRINK_DELETE()
	TRIGGER_SCENE_DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
ENDPROC


/// PURPOSE:	Custom logic to decide whether or not conditions have been met to trigger the 
///    			mission linked to the trigger. By default runs every frame that a scene 
///    			is created. Once this returns TRUE the mission script will launch next frame.
FUNC BOOL TS_SHRINK_HAS_BEEN_TRIGGERED()
	VECTOR vTriggerPosition = GET_STATIC_BLIP_POSITION(STATIC_BLIP_MINIGAME_SHRINK)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Render trigger zone debug.
		#IF IS_DEBUG_BUILD
			DEBUG_DRAW_TRIGGER_SCENE_TRIGGER(vTriggerPosition, LOCATE_SIZE_MISSION_TRIGGER)
		#ENDIF
	
		FLOAT fDistanceSquaredFromTrigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTriggerPosition)
		IF fDistanceSquaredFromTrigger < (LOCATE_SIZE_MISSION_TRIGGER*LOCATE_SIZE_MISSION_TRIGGER)
		
			//Does the player have enough money to trigger the Shrink mission?
			INT iRequiredCash
			STRING strHelpMoneyLabel
			IF IS_MISSION_AVAILABLE(SP_MISSION_SHRINK_1)
				iRequiredCash = 500
				strHelpMoneyLabel = "AM_H_SHRCOST1"
			ELIF IS_MISSION_AVAILABLE(SP_MISSION_SHRINK_2)
				iRequiredCash = 1000
				strHelpMoneyLabel = "AM_H_SHRCOST2"
			ELIF IS_MISSION_AVAILABLE(SP_MISSION_SHRINK_5)
				iRequiredCash = 2000
				strHelpMoneyLabel = "AM_H_SHRCOST5"
			ELSE
				SCRIPT_ASSERT("TS_SHRINK_HAS_BEEN_TRIGGERED: Could not determine which Shrink mission was available.")
			ENDIF
			
			IF GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL) >= iRequiredCash
				//Player has enough, fire the mission.
				RETURN TRUE
			ENDIF

			//Player doesn't have enough, display help to inform them to get more.
			SWITCH GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SHRMON")
				CASE FHS_EXPIRED
					ADD_HELP_WITH_SUBSTRING_TO_FLOW_QUEUE("AM_H_SHRMON", strHelpMoneyLabel, FHP_VERY_HIGH, 0, 1000, DEFAULT_HELP_TEXT_TIME, BIT_MICHAEL)
				BREAK
			ENDSWITCH
			
		ELIF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_SHRMON") = FHS_DISPLAYED
			g_txtFlowHelpLastDisplayed = ""
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether or not the player has disrupted entites in
///    			the scene enough to cause the trigger to deactivate. This check is run every
///    			frame the scene is created. If it returns TRUE the RELEASE function will run
///    			and the player must leave the area for the trigger to reactivate.
FUNC BOOL TS_SHRINK_HAS_BEEN_DISRUPTED()
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Custom logic to decide whether loading, creation of, and triggering of the
///    			scene should be blocked this frame. Useful if the mission trigger needs to
///    			be disabled for periods of time determined by logic within this trigger
///    			script.
FUNC BOOL TS_SHRINK_IS_BLOCKED()
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_SHRINK_5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	General update procedure for the scene. Runs every frame that the scene
///    			is created. Can be used to manage behaviour of entites in the scene.   
PROC TS_SHRINK_UPDATE()
ENDPROC


/// PURPOSE:	General update procedure for the trigger that runs every frame that the
///    			scene hasn't been created. Useful if we want custom behvaiour on the scene's
///    			position or blip.
PROC TS_SHRINK_AMBIENT_UPDATE()
ENDPROC
