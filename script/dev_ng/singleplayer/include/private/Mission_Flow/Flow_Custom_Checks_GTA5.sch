//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 09/10/12			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  		Flow Custom Check Definitions						│
//│																				│
//│		DESCRIPTION: Contains custom checks functions that the flow can			│
//│		query using the FLOW_DO_CUSTOM_CHECK_JUMPS command.						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "flow_commands_enums_GAME.sch"
USING "vector_id_public.sch"
USING "friend_flow_public.sch"
USING "flow_mission_data_public.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ Debug  ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
	FUNC STRING GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(FLOW_CHECK_IDS paramCheckID)
		SWITCH paramCheckID
			CASE FLOW_CHECK_TRUE							RETURN "CHECK_TRUE"								BREAK
			CASE FLOW_CHECK_TREVOR2_UNLOCK					RETURN "TREVOR2_UNLOCK"							BREAK
			CASE FLOW_CHECK_MIKE_EVENT_AMANDA				RETURN "MIKE_EVENT_AMANDA"						BREAK
			CASE FLOW_CHECK_MIKE_EVENT_TRACY				RETURN "MIKE_EVENT_TRACY"						BREAK
			CASE FLOW_CHECK_MIKE_EVENT_JIMMY				RETURN "MIKE_EVENT_JIMMY"						BREAK
			CASE FLOW_CHECK_FBI4_PREP3						RETURN "FBI4_PREP3"								BREAK
			CASE FLOW_CHECK_FBI4_PREP3_NOT_DONE				RETURN "FBI4_PREP3_NOT_DONE"					BREAK
			CASE FLOW_CHECK_RICKIE_USED						RETURN "RICKIE_USED"							BREAK
			CASE FLOW_CHECK_FAMILY2_UNLOCK					RETURN "FAMILY2_UNLOCK"							BREAK
			CASE FLOW_CHECK_FINH_GETAWAY_VEH_NOT_SETUP		RETURN "FINH_GETAWAY_VEH_NOT_SETUP"				BREAK
			CASE FLOW_CHECK_AH_GETAWAY_VEH_SETUP			RETURN "AH_GETAWAY_VEH_SETUP"					BREAK
			CASE FLOW_CHECK_AH_GETAWAY_VEH_NOT_SETUP		RETURN "AH_GETAWAY_VEH_NOT_SETU"				BREAK
			CASE FLOW_CHECK_JEWEL_P2A_UNLOCKED				RETURN "JEWEL_P2A_UNLOCKED"						BREAK
			CASE FLOW_CHECK_FRAN_DONE_LAM_ACTIVITY			RETURN "FRAN_DONE_LAM_ACTIVITY"					BREAK
			CASE FLOW_CHECK_TREV_DONE_LAM_ACTIVITY			RETURN "TREV_DONE_LAM_ACTIVITY"					BREAK
			CASE FLOW_CHECK_PORT_LS_B_BRANCH_CHOSEN			RETURN "PORT_LS_B_BRANCH_CHOSEN"				BREAK
			CASE FLOW_CHECK_PORT_LS_SETUP_DONE				RETURN "PORT_LS_SETUP_DONE"						BREAK
			CASE FLOW_CHECK_SOLOMON3_UNLOCK					RETURN "SOLOMON_3_UNLOCK"						BREAK
			CASE FLOW_CHECK_EXILE2_COMPLETE					RETURN "EXILE2_COMPLETE"						BREAK
			CASE FLOW_CHECK_RC_HUNTING2_COMPLETE			RETURN "RC_HUNTING2_COMPLETE"					BREAK
			CASE FLOW_CHECK_PATRICIA_CALL1_FINISHED			RETURN "PATRICIA_CALL1_FINISHED"				BREAK
			CASE FLOW_CHECK_CAN_UNLOCK_FRIEND_LAMAR			RETURN "FLOW_CHECK_CAN_UNLOCK_FRIEND_LAMAR"		BREAK
			CASE FLOW_CHECK_CAN_CALL_FRIEND_JIMMY			RETURN "FLOW_CHECK_CAN_CALL_FRIEND_JIMMY"		BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_MICHAEL				RETURN "FLOW_CHECK_TEXT_FRIEND_MICHAEL"			BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_FRANKLIN			RETURN "FLOW_CHECK_TEXT_FRIEND_FRANKLIN"		BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_TREVOR				RETURN "FLOW_CHECK_TEXT_FRIEND_TREVOR"			BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_LAMAR				RETURN "FLOW_CHECK_TEXT_FRIEND_LAMAR"			BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_JIMMY				RETURN "FLOW_CHECK_TEXT_FRIEND_JIMMY"			BREAK
			CASE FLOW_CHECK_TEXT_FRIEND_AMANDA				RETURN "FLOW_CHECK_TEXT_FRIEND_AMANDA"			BREAK
		ENDSWITCH
		RETURN "INVALID!"
	ENDFUNC
#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Custom Function Defintions  ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//Default FLOW_CHECK_ID function.
FUNC BOOL DEFAULT_CHECK()
	RETURN TRUE
ENDFUNC


//Runs with FLOW_CHECK_TREVOR2_UNLOCK.
FUNC BOOL DO_TREVOR2_UNLOCK_CHECK()
	IF g_iTimeStartedMissionUnlockCheck = -1
		g_iTimeStartedMissionUnlockCheck = GET_GAME_TIMER()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), VID_LOCATION_TREVOR_TRAILER_500M)
			CPRINTLN(DEBUG_FLOW, "Player moved more than 500m from trailer. Passing Trevor2 unlock check.")
			g_iTimeStartedMissionUnlockCheck = -1
			RETURN TRUE
		ENDIF
		IF (GET_GAME_TIMER() - g_iTimeStartedMissionUnlockCheck) > 240000
			IF NOT IS_PED_AT_VECTOR_ID(PLAYER_PED_ID(), VID_LOCATION_TREVOR_TRAILER_180M)
				CPRINTLN(DEBUG_FLOW, "3 mins passed and player moved more than 180m from trailer. Passing Trevor2 unlock check.")
				g_iTimeStartedMissionUnlockCheck = -1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_MIKE_EVENT_AMANDA.
FUNC BOOL DO_CHECK_MIKE_EVENT_AMANDA()
	IF HAS_CHAR_ARRANGED_FRIEND_ACTIVITY_WITH_ANYONE(CHAR_MICHAEL)
		CPRINTLN(DEBUG_FRIENDS, "DO_CHECK_MIKE_EVENT_AMANDA() - blocked ME event by friend activity")
		RETURN FALSE
	ENDIF
	INT iClockHour = GET_CLOCK_HOURS()
	IF iClockHour < 6 OR iClockHour > 14
		RETURN FALSE
	ENDIF	
	RETURN TRUE
ENDFUNC

//Runs with FLOW_CHECK_MIKE_EVENT_TRACY.
FUNC BOOL DO_CHECK_MIKE_EVENT_TRACY()
	IF HAS_CHAR_ARRANGED_FRIEND_ACTIVITY_WITH_ANYONE(CHAR_MICHAEL)
		CPRINTLN(DEBUG_FRIENDS, "DO_CHECK_MIKE_EVENT_TRACY() - blocked ME event by friend activity")
		RETURN FALSE
	ENDIF
	if IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_4)
		RETURN FALSE
	endif
	RETURN TRUE
ENDFUNC

//Runs with FLOW_CHECK_MIKE_EVENT_JIMMY.
FUNC BOOL DO_CHECK_MIKE_EVENT_JIMMY()
	IF HAS_CHAR_ARRANGED_FRIEND_ACTIVITY_WITH_ANYONE(CHAR_MICHAEL)
		CPRINTLN(DEBUG_FRIENDS, "DO_CHECK_MIKE_EVENT_JIMMY() - blocked ME event by friend activity")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

//Runs with FLOW_CHECK_FBI4_PREP3.
FUNC BOOL DO_FBI4_PREP3_COMPLETED_CHECK()
	RETURN g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED]
ENDFUNC


//Runs with FLOW_CHECK_FBI4_PREP3_NOT_DONE.
FUNC BOOL DO_FBI4_PREP3_NOT_COMPLETED_CHECK()
	RETURN NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED]
ENDFUNC


//Runs with FLOW_CHECK_RICKIE_USED.
FUNC BOOL DO_RICKIE_USED_CHECK()
	INT iChoiceIndex
	INT iCrewIndex
	REPEAT MAX_HEIST_CHOICES iChoiceIndex
		REPEAT g_sHeistChoiceData[iChoiceIndex].iCrewSize iCrewIndex
			BOOL bCompleted = FALSE
			SWITCH iChoiceIndex
				CASE HEIST_CHOICE_JEWEL_STEALTH		
				CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_2)
				BREAK
				CASE HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A)
				BREAK
				CASE HEIST_CHOICE_DOCKS_DEEP_SEA	
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2B)
				BREAK	
				CASE HEIST_CHOICE_RURAL_NO_TANK		
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
				BREAK
				CASE HEIST_CHOICE_AGENCY_FIRETRUCK
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_3A)
				BREAK	
				CASE HEIST_CHOICE_AGENCY_HELICOPTER
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_3B)
				BREAK	
				CASE HEIST_CHOICE_FINALE_TRAFFCONT
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2A)
				BREAK	
				CASE HEIST_CHOICE_FINALE_HELI
					bCompleted = GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2B)
				BREAK	
			ENDSWITCH
			
			IF bCompleted
			AND g_savedGlobals.sHeistData.eSelectedCrew[iChoiceIndex][iCrewIndex] = CM_HACKER_B_RICKIE_UNLOCK
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_FAMILY2_UNLOCK
FUNC BOOL DO_FAMILY2_UNLOCK_CHECK()

	if not GET_MISSION_COMPLETE_STATE(SP_MISSION_FRANKLIN_0)
		RETURN FALSE
	endif
	
	//Is the player more than 200m from the Family2 mission blip?
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF VDIST2(GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_FAMILY_2), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 40000 //200^2
			CPRINTLN(DEBUG_FLOW, "DO_FAMILY2_UNLOCK_CHECK: Family2 unlocked due to 200m leave area check.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF g_iTimeStartedMissionUnlockCheck = -1
		g_iTimeStartedMissionUnlockCheck = GET_GAME_TIMER()
	ENDIF
	
	IF (GET_GAME_TIMER() - g_iTimeStartedMissionUnlockCheck) > 30000
		CPRINTLN(DEBUG_FLOW, "DO_FAMILY2_UNLOCK_CHECK: Family2 unlocked due to 30 second timeout check.")
		g_iTimeStartedMissionUnlockCheck = -1
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_FINH_GETAWAY_VEH_NOT_SETUP
FUNC BOOL DO_FINH_GETAWAY_VEH_NOT_SETUP_CHECK()
	RETURN NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_FINALE_PREPE_DONE]
ENDFUNC


//Runs with FLOW_CHECK_AH_GETAWAY_VEH_SETUP
FUNC BOOL DO_AH_GETAWAY_VEH_SETUP_CHECK()
	RETURN g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_AGENCY_PREP_2_DONE]
ENDFUNC

//Runs with FLOW_CHECK_AH_GETAWAY_VEH_NOT_SETUP
FUNC BOOL DO_AH_GETAWAY_VEH_NOT_SETUP_CHECK()
	RETURN NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_AGENCY_PREP_2_DONE]
ENDFUNC


//Runs with FLOW_CHECK_JEWEL_P2A_UNLOCKED
FUNC BOOL DO_JEWEL_P2A_UNLOCKED_CHECK()
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_2A)
		RETURN TRUE
	ELIF IS_MISSION_AVAILABLE(SP_HEIST_JEWELRY_PREP_2A)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_FRAN_DONE_LAM_ACTIVITY
FUNC BOOL DO_FRAN_DONE_LAM_ACTIVITY_CHECK()
	RETURN g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_FRAN_DONE_ACTIVITY_WITH_LAMAR]
ENDFUNC


//Runs with FLOW_CHECK_FRAN_NOT_DONE_LAM_ACTIVITY
FUNC BOOL DO_FRAN_NOT_DONE_LAM_ACTIVITY_CHECK()
	RETURN NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_FRAN_DONE_ACTIVITY_WITH_LAMAR]
ENDFUNC


//Runs with FLOW_CHECK_PORT_LS_B_BRANCH_CHOSEN
FUNC BOOL DO_PORT_LS_B_BRANCH_CHOSEN_CHECK()
	IF g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_DOCKS] = HEIST_CHOICE_DOCKS_DEEP_SEA
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_PORT_LS_SETUP_DONE
FUNC BOOL DO_PORT_LS_SETUP_DONE_CHECK()
	RETURN g_savedGlobals.sFlow.missionSavedData[SP_HEIST_DOCKS_1].completed
ENDFUNC


//Runs with FLOW_CHECK_SOLOMON3_UNLOCK
FUNC BOOL DO_SOLOMON3_UNLOCK_CHECK()
	IF g_iTimeStartedMissionUnlockCheck = -1
		g_iTimeStartedMissionUnlockCheck = GET_GAME_TIMER()
	ENDIF
	
	IF (GET_GAME_TIMER() - g_iTimeStartedMissionUnlockCheck) > 240000
		CPRINTLN(DEBUG_FLOW, "4 mins passed. Passing Solomon3 unlock check.")
		g_iTimeStartedMissionUnlockCheck = -1
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		FLOAT fDistanceSquaredFromSol3Trigger = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_STATIC_BLIP_POSITION(STATIC_BLIP_MISSION_SOLOMON))
		IF fDistanceSquaredFromSol3Trigger > 22500 //150^2
			CPRINTLN(DEBUG_FLOW, "Player moved more than 150m from Solomon3 trig position. Passing Solomon3 unlock check.")
			g_iTimeStartedMissionUnlockCheck = -1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//Runs with FLOW_CHECK_EXILE2_COMPLETE
FUNC BOOL DO_EXILE2_COMPLETE_CHECK()
	RETURN GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_2)
ENDFUNC


//Runs with FLOW_CHECK_RC_HUNTING2_COMPLETE
FUNC BOOL DO_RC_HUNTING2_COMPLETE_CHECK()
	RETURN IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_HUNTING_2].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
ENDFUNC


//Runs with FLOW_CHECK_PATRICIA_CALL1_FINISHED
FUNC BOOL DO_PATRICIA_CALL1_FINISHED_CHECK()
	INT index
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = CALL_PATRICIA_1
			RETURN FALSE
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC

//Runs with FLOW_CHECK_CAN_UNLOCK_FRIEND_LAMAR
FUNC BOOL DO_CAN_UNLOCK_FRIEND_LAMAR_CHECK()

	// Don't add friend if on activity
	IF IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))
		RETURN FALSE
	ENDIF
	
	// Don't add friend if their ped exists
	INT iIndex = ENUM_TO_INT(FR_LAMAR) - NUM_OF_PLAYABLE_PEDS
	IF iIndex < NUM_OF_NPC_FRIENDS
		IF DOES_ENTITY_EXIST(g_pGlobalFriends[iIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_CAN_CALL_FRIEND_JIMMY
FUNC BOOL DO_CAN_CALL_FRIEND_JIMMY_CHECK()

	// Might not be able to call friend if on activity
	IF IS_THREAD_ACTIVE(INT_TO_NATIVE(THREADID, g_SavedGlobals.sFriendsData.g_FriendScriptThread))
		RETURN FALSE
	ENDIF
	
	// Can't call friend if they exist
	INT iIndex = ENUM_TO_INT(FR_JIMMY) - NUM_OF_PLAYABLE_PEDS
	IF iIndex < NUM_OF_NPC_FRIENDS
		IF DOES_ENTITY_EXIST(g_pGlobalFriends[iIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Can't call Jimmy if asleep
	INT iAwakeHour = 14
	INT iSleepHour = 5
	INT iCurrentHour = GET_CLOCK_HOURS()
	
	IF iAwakeHour < iSleepHour
		IF iCurrentHour < iAwakeHour
		OR iCurrentHour >= iSleepHour
			RETURN FALSE
		ENDIF
	ELSE
		IF iCurrentHour < iAwakeHour
		AND iCurrentHour >= iSleepHour
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Can't call if no connection
	enumFriendConnection eConnection = GET_CONNECTION_FROM_FRIENDS(GET_FRIEND_FROM_CHAR(GET_CURRENT_PLAYER_PED_ENUM()), FR_JIMMY)
	IF eConnection >= MAX_FRIEND_CONNECTIONS
		RETURN FALSE
	ELSE
	
		// Can't call if not in contact wait
		IF g_FriendConnectState[eConnection].state != FC_STATE_ContactWait
			RETURN FALSE
		ENDIF
		
		// Can't call if blocked
		IF g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].blockBits != 0
			RETURN FALSE
		ENDIF
		
		// Can't call if hungout recently
		IF IS_TIMER_STARTED(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].lastContactTimer)
		AND GET_TIMER_IN_SECONDS(g_SavedGlobals.sFriendsData.g_FriendConnectData[eConnection].lastContactTimer) < 5.0*60.0
			RETURN FALSE
		ENDIF
	
	ENDIF

	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_MICHAEL
FUNC BOOL DO_TEXT_FRIEND_MICHAEL_CHECK()

	// Don't send text if sender exists
	IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_FRANKLIN
FUNC BOOL DO_TEXT_FRIEND_FRANKLIN_CHECK()

	// Don't send text if sender exists
	IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_TREVOR
FUNC BOOL DO_TEXT_FRIEND_TREVOR_CHECK()

	// Don't send text if sender exists
	IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_LAMAR
FUNC BOOL DO_TEXT_FRIEND_LAMAR_CHECK()

	// Don't send text if sender exists
	INT iIndex = ENUM_TO_INT(FR_LAMAR) - NUM_OF_PLAYABLE_PEDS
	IF iIndex >= 0 AND iIndex < NUM_OF_NPC_FRIENDS
		IF DOES_ENTITY_EXIST(g_pGlobalFriends[iIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_JIMMY
FUNC BOOL DO_TEXT_FRIEND_JIMMY_CHECK()

	// Don't send text if sender exists
	INT iIndex = ENUM_TO_INT(FR_JIMMY) - NUM_OF_PLAYABLE_PEDS
	IF iIndex >= 0 AND iIndex < NUM_OF_NPC_FRIENDS
		IF DOES_ENTITY_EXIST(g_pGlobalFriends[iIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

//Runs with FLOW_CHECK_TEXT_FRIEND_AMANDA
FUNC BOOL DO_TEXT_FRIEND_AMANDA_CHECK()

	// Don't send text if sender exists
	INT iIndex = ENUM_TO_INT(FR_AMANDA) - NUM_OF_PLAYABLE_PEDS
	IF iIndex >= 0 AND iIndex < NUM_OF_NPC_FRIENDS
		IF DOES_ENTITY_EXIST(g_pGlobalFriends[iIndex])
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ Function Binds  ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC GET_FLOW_CHECK_FUNCTION(FLOW_CHECK_IDS paramCheckID, PerformCustomFlowCheck &returnFunction)
	SWITCH paramCheckID
		CASE FLOW_CHECK_TRUE
			returnFunction = &DEFAULT_CHECK
		BREAK
	
		CASE FLOW_CHECK_TREVOR2_UNLOCK
			returnFunction = &DO_TREVOR2_UNLOCK_CHECK
		BREAK
		
		CASE FLOW_CHECK_MIKE_EVENT_AMANDA			
			returnFunction = &DO_CHECK_MIKE_EVENT_AMANDA	
		BREAK
		
		CASE FLOW_CHECK_MIKE_EVENT_TRACY			
			returnFunction = &DO_CHECK_MIKE_EVENT_TRACY	
		BREAK
		
		CASE FLOW_CHECK_MIKE_EVENT_JIMMY			
			returnFunction = &DO_CHECK_MIKE_EVENT_JIMMY
		BREAK
		
		CASE FLOW_CHECK_FBI4_PREP3						
			returnFunction = &DO_FBI4_PREP3_COMPLETED_CHECK
		BREAK
		
		CASE FLOW_CHECK_FBI4_PREP3_NOT_DONE
			returnFunction = &DO_FBI4_PREP3_NOT_COMPLETED_CHECK
		BREAK

		CASE FLOW_CHECK_RICKIE_USED
			returnFunction = &DO_RICKIE_USED_CHECK
		BREAK
		
		CASE FLOW_CHECK_FAMILY2_UNLOCK
			returnFunction = &DO_FAMILY2_UNLOCK_CHECK
		BREAK
		
		CASE FLOW_CHECK_FINH_GETAWAY_VEH_NOT_SETUP
			returnFunction = &DO_FINH_GETAWAY_VEH_NOT_SETUP_CHECK
		BREAK
		
		CASE FLOW_CHECK_AH_GETAWAY_VEH_SETUP
			returnFunction = &DO_AH_GETAWAY_VEH_SETUP_CHECK
		BREAK
		
		CASE FLOW_CHECK_AH_GETAWAY_VEH_NOT_SETUP
			returnFunction = &DO_AH_GETAWAY_VEH_NOT_SETUP_CHECK
		BREAK
		
		CASE FLOW_CHECK_JEWEL_P2A_UNLOCKED
			returnFunction = &DO_JEWEL_P2A_UNLOCKED_CHECK
		BREAK
		
		CASE FLOW_CHECK_FRAN_DONE_LAM_ACTIVITY
			returnFunction = &DO_FRAN_DONE_LAM_ACTIVITY_CHECK
		BREAK
		
		CASE FLOW_CHECK_FRAN_NOT_DONE_LAM_ACTIVITY
			returnFunction = &DO_FRAN_NOT_DONE_LAM_ACTIVITY_CHECK
		BREAK
		
		CASE FLOW_CHECK_PORT_LS_B_BRANCH_CHOSEN
			returnFunction = &DO_PORT_LS_B_BRANCH_CHOSEN_CHECK
		BREAK
		
		CASE FLOW_CHECK_PORT_LS_SETUP_DONE
			returnFunction = &DO_PORT_LS_SETUP_DONE_CHECK
		BREAK
		
		CASE FLOW_CHECK_SOLOMON3_UNLOCK
			returnFunction = &DO_SOLOMON3_UNLOCK_CHECK
		BREAK
		
		CASE FLOW_CHECK_EXILE2_COMPLETE
			returnFunction = &DO_EXILE2_COMPLETE_CHECK
		BREAK
		
		CASE FLOW_CHECK_RC_HUNTING2_COMPLETE
			returnFunction = &DO_RC_HUNTING2_COMPLETE_CHECK
		BREAK
		
		CASE FLOW_CHECK_PATRICIA_CALL1_FINISHED
			returnFunction = &DO_PATRICIA_CALL1_FINISHED_CHECK
		BREAK
		
		CASE FLOW_CHECK_CAN_UNLOCK_FRIEND_LAMAR
			returnFunction = &DO_CAN_UNLOCK_FRIEND_LAMAR_CHECK
		BREAK
		
		CASE FLOW_CHECK_CAN_CALL_FRIEND_JIMMY
			returnFunction = &DO_CAN_CALL_FRIEND_JIMMY_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_MICHAEL
			returnFunction = &DO_TEXT_FRIEND_MICHAEL_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_FRANKLIN
			returnFunction = &DO_TEXT_FRIEND_FRANKLIN_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_TREVOR
			returnFunction = &DO_TEXT_FRIEND_TREVOR_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_LAMAR
			returnFunction = &DO_TEXT_FRIEND_LAMAR_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_JIMMY
			returnFunction = &DO_TEXT_FRIEND_JIMMY_CHECK
		BREAK
		
		CASE FLOW_CHECK_TEXT_FRIEND_AMANDA
			returnFunction = &DO_TEXT_FRIEND_AMANDA_CHECK
		BREAK
		
		//!!! Add new FLOW_CHECK_ID cases here !!!
		
		DEFAULT
			SCRIPT_ASSERT("GET_FLOW_CHECK_FUNCTION: There was no valid function defined for the passed FLOW_CHECK_ID. Tell BenR.")
			returnFunction = &DEFAULT_CHECK
		BREAK
	ENDSWITCH
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Public Interface  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL DO_CUSTOM_FLOW_CHECK(FLOW_CHECK_IDS paramCheckID)
	IF paramCheckID = FLOW_CHECK_NONE OR paramCheckID = MAX_FLOW_CHECK_IDS
		SCRIPT_ASSERT("FLOW_CHECK_NONE and MAX_FLOW_CHECK_IDS are not valid IDs to check.")
		RETURN FALSE
	ENDIF

	PerformCustomFlowCheck fpCustomFlowCheck
	GET_FLOW_CHECK_FUNCTION(paramCheckID, fpCustomFlowCheck)
	RETURN CALL fpCustomFlowCheck()
ENDFUNC
