//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 20/11/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  	GTA5 Mission Data Common Functions						│
//│																				│
//│		DESCRIPTION: A header containing all commonGTA5 specific mission 		│
//│		and flow data population procedures.									│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "blip_control_public.sch"
USING "Flow_Mission_Data_Public.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════╡ Data Setup and Storage Procedures  ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// PURPOSE: All the Blip Control System functions to setup initial mission blip data for a single blip.
//
// INPUT PARAMS:		paramBlipID		Static Blip ID being setup
//						paramCoords		Coordinates for the blip
//						paramCategory	The category of the blip being setup
//						paramMapName	Text label that defines the name that will appear for this blip when hovered over on the map.
//						paramSprite		The sprite the blip will use. Leave as default to CS_ALLow the category to set the sprite automatically.
//
PROC Setup_Mission_Blip(STATIC_BLIP_NAME_ENUM paramBlipID, VECTOR paramCoords, STRING paramMapName, BLIP_SPRITE paramSprite = RADAR_TRACE_OBJECTIVE)

	SET_STATIC_BLIP_ACTIVE_STATE(paramBlipID, FALSE)
	SET_STATIC_BLIP_APPEAR_ON_MAP(paramBlipID, TRUE)
	SET_STATIC_BLIP_APPEAR_IN_RADAR(paramBlipID, TRUE)
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(paramBlipID, TRUE)
	
	SET_STATIC_BLIP_POSITION(paramBlipID, paramCoords)
	SET_STATIC_BLIP_CATEGORY(paramBlipID, STATIC_BLIP_CATEGORY_MISSION)
	
	IF NOT ARE_STRINGS_EQUAL(paramMapName, "")
		SET_STATIC_BLIP_NAME(paramBlipID, paramMapName)
	ENDIF

	IF paramSprite <> RADAR_TRACE_OBJECTIVE
		SET_STATIC_BLIP_ICON(paramBlipID, paramSprite)
	ENDIF

ENDPROC


PROC Setup_Character_Specific_Mission_Blip(STATIC_BLIP_NAME_ENUM paramBlipID, VECTOR paramMikeCoords, BLIP_SPRITE paramMikeSprite,  VECTOR paramFranCoords, BLIP_SPRITE paramFranSprite, VECTOR paramTrevCoords, BLIP_SPRITE paramTrevSprite, STRING paramMapName)

	SET_STATIC_BLIP_ACTIVE_STATE(paramBlipID, FALSE)
	SET_STATIC_BLIP_APPEAR_ON_MAP(paramBlipID, TRUE)
	SET_STATIC_BLIP_APPEAR_IN_RADAR(paramBlipID, TRUE)
	SET_STATIC_BLIP_APPEAR_EDGE_RADAR(paramBlipID, TRUE)
	SET_STATIC_BLIP_CATEGORY(paramBlipID, STATIC_BLIP_CATEGORY_MISSION)
	SET_STATIC_BLIP_MULTIMODE(paramBlipID, paramMikeCoords, paramMikeSprite, paramFranCoords, paramFranSprite, paramTrevCoords, paramTrevSprite)
 
	IF NOT ARE_STRINGS_EQUAL(paramMapName, "")
		SET_STATIC_BLIP_NAME(paramBlipID, paramMapName)
	ENDIF

ENDPROC


#IF IS_DEBUG_BUILD
PROC Set_Mission_Debug_Data(SP_MISSIONS paramMission, JUMP_LABEL_IDS paramLaunchLabel, JUMP_LABEL_IDS paramBeforeLaunchLabel, BOOL paramDoLaunchWarp, VECTOR paramWarpPos, INT paramBitsCharBlockDebugLaunch = 0)

	g_sMissionDebugData[paramMission].launchLabel = paramLaunchLabel
	g_sMissionDebugData[paramMission].beforeLaunchLabel = paramBeforeLaunchLabel
	g_sMissionDebugData[paramMission].doLaunchWarp = paramDoLaunchWarp
	g_sMissionDebugData[paramMission].warpPosition = paramWarpPos
	g_sMissionDebugData[paramMission].charBitsBlockDebugLaunch = paramBitsCharBlockDebugLaunch
	
ENDPROC
#ENDIF


PROC Set_Mission_Data(SP_MISSIONS paramMission, STRING paramScriptName, STRING paramStatID, STATIC_BLIP_NAME_ENUM paramBlip, INT paramCharTriggerFlags, INT paramCharFriendFlags, INT paramStartHour, INT paramEndHour, INT paramMFFlags)

	g_sMissionStaticData[paramMission].scriptName			= paramScriptName
	g_sMissionStaticData[paramMission].scriptHash			= GET_HASH_KEY(paramScriptName)
	g_sMissionStaticData[paramMission].statID 				= paramStatID
	g_sMissionStaticData[paramMission].triggerCharBitset 	= paramCharTriggerFlags
	g_sMissionStaticData[paramMission].friendCharBitset 	= paramCharFriendFlags
	g_sMissionStaticData[paramMission].blip 				= paramBlip
	g_sMissionStaticData[paramMission].startHour 			= paramStartHour
	g_sMissionStaticData[paramMission].endHour 				= paramEndHour
	g_sMissionStaticData[paramMission].settingsBitset		= paramMFFlags
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(paramMFFlags, MF_INDEX_HAS_LEADIN)
			IF IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_M)
			OR IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_F)
			OR IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_T)
				SCRIPT_ASSERT("SET_MISSION_DATA: Tried to flag a mission to do a lead-in timelapse when the mission isn't flagged to have a lead-in. This will break the mission trigger.")
			ENDIF
		ENDIF
	#ENDIF

ENDPROC
PROC Set_Mission_Data_HASH(SP_MISSIONS paramMission, INT paramScriptNameHash, STRING paramStatID, STATIC_BLIP_NAME_ENUM paramBlip, INT paramCharTriggerFlags, INT paramCharFriendFlags, INT paramStartHour, INT paramEndHour, INT paramMFFlags)
	
	g_sMissionStaticData[paramMission].scriptName			= ""
	g_sMissionStaticData[paramMission].scriptHash			= paramScriptNameHash
	g_sMissionStaticData[paramMission].statID 				= paramStatID
	g_sMissionStaticData[paramMission].triggerCharBitset 	= paramCharTriggerFlags
	g_sMissionStaticData[paramMission].friendCharBitset 	= paramCharFriendFlags
	g_sMissionStaticData[paramMission].blip 				= paramBlip
	g_sMissionStaticData[paramMission].startHour 			= paramStartHour
	g_sMissionStaticData[paramMission].endHour 				= paramEndHour
	g_sMissionStaticData[paramMission].settingsBitset		= paramMFFlags
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(paramMFFlags, MF_INDEX_HAS_LEADIN)
			IF IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_M)
			OR IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_F)
			OR IS_BIT_SET(paramMFFlags, MF_INDEX_DO_TRIGGER_TOD_T)
				SCRIPT_ASSERT("SET_MISSION_DATA: Tried to flag a mission to do a lead-in timelapse when the mission isn't flagged to have a lead-in. This will break the mission trigger.")
			ENDIF
		ENDIF
	#ENDIF

ENDPROC

PROC Set_Extra_Mission_Flags(SP_MISSIONS paramMission, INT paramDLCFlags)
	g_iExtraMissionFlags[paramMission] = paramDLCFlags
ENDPROC


//Mission settings bit flags. Must match up with MF_INDEX_* constants in flow_globals.sch
CONST_INT	MF_NONE						0
CONST_INT   MF_NO_SAVE					BIT0		// The game won't autosave after this mission.
CONST_INT   MF_NO_FAIL					BIT1		// No fail screen will display when failing this mission.
CONST_INT   MF_NO_REPLAY				BIT2		// No replay will be offered on failing this mission.
CONST_INT   MF_NO_TITLE					BIT3		// No title will be displayed as this mission starts.
CONST_INT	MF_NO_PASS					BIT4		// No mission passed audio or popup at the end of this mission.
CONST_INT   MF_NO_REPEAT				BIT5		// Once the mission is completed it won't be available to repreat from the front-end.
CONST_INT   MF_NO_COMP_PERC     		BIT6		// This mission does not count towards 100% completion.
CONST_INT	MF_NO_COMP_ORDER			BIT7		// This mission does not count in flow completion order tracking.
CONST_INT	MF_DO_TRIGGER_TOD_M			BIT8		// This mission will time of day skip to meet time restrictions before any lead-in scene is created for Michael.
CONST_INT	MF_DO_TRIGGER_TOD_F			BIT9		// This mission will time of day skip to meet time restrictions before any lead-in scene is created for Franklin.
CONST_INT	MF_DO_TRIGGER_TOD_T			BIT10		// This mission will time of day skip to meet time restrictions before any lead-in scene is created for Trevor.
CONST_INT	MF_BLIP_IS_HIDDEN			BIT11		// The blip for this mission is never shown on the map.
CONST_INT	MF_NO_SWITCH_COUNT			BIT12		// This mission doesn't count to the available mission counter when bringing up the switch HUD.
CONST_INT	MF_HAS_LEADIN				BIT13		// This mission creates ambient off-mission assets at its trigger location.
CONST_INT	MF_IS_HEIST					BIT14		// This mission is a heist finale mission.
CONST_INT	MF_IS_PREP					BIT15		// This mission is a heist prep mission.
CONST_INT	MF_NO_SHITSKIP				BIT16		// This mission does not allow the player to skip past sections of gameplay if they repeatedly fail.
CONST_INT	MF_FORCE_RESTART			BIT17		// This mission can not be exited out of when failing. The player must replay on fail.
CONST_INT	MF_HIDE_PHONE_ON_START		BIT18		// This mission will force the phone off-screen as it launches.
CONST_INT	MF_MUST_LAUNCH_PRIORITY		BIT19		// This mission applies to the candidate controller with the must launch priority.
CONST_INT	MF_HIGH_MEMORY_LEADIN		BIT20		// The leadin for this mission has particularly high memory and should cause minigame launchers with ambient assets to clean up.
CONST_INT	MF_SEAMLESS_TRIGGER			BIT21		// This mission triggers seamlessly. Probably from a phonecall.
CONST_INT	MF_NO_LEADIN_TIMEOUT		BIT22		// This mission's leadin will not trigger automatically after the player is locked in for a number of seconds.
CONST_INT	MF_DO_TOD_AUTO_START		BIT23		// If this missions time of day trigger is hit it automatically starts the mission instead of doing a timelapse cutscene.
CONST_INT	MF_NO_VEH_EXIT				BIT24		// The player won't be forced out of their vehicle as they hit the mission trigger.
CONST_INT	MF_NO_VEH_STOP				BIT25		// The player's vehicle won't be forced to halt when they hit the mission trigger.
CONST_INT	MF_NO_SAVE_ON_LEAD_IN		BIT26		// The player won't be allowed to use savegame bed if the mission lead-in is active
CONST_INT	MF_NO_LEAVE_AREA_CHECK		BIT27		// Allows the mission's trigger to come online while the player is standing close to it.
CONST_INT	MF_FORCE_CORONA_M			BIT28		// A corona will always display for this trigger for Michael.
CONST_INT	MF_PRESTREAM_FULLY_OFF		BIT29		// When loading the intro cutscene for this mission ensure all map pre-streaming is fully turned off.
CONST_INT	MF_MID_MISSION_TOD			BIT30		// This mission's TOD skip is done during the mission, rather than at the start.


//Mission flow extra config flags. Starts a new bitset. Must match up with MF_EXTRA_INDEX_* constants in x:\gta5\script\dev_ng\singleplayer\include\globals\dlc\globals_sp_dlc.sch
CONST_INT MF_EXTRA_NONE					0
CONST_INT MF_EXTRA_FORCE_TRIGGER_CORONA	BIT0		//Forces a corona to display at the mission trigger even if the trigger has a full lead-in scene.
CONST_INT MF_EXTRA_TEMP_ZERO_LEAVE_AREA	BIT1		//Next time the trigger for this mission is created it will have zero sized leave area distance. On scene creation this will be cleared.
CONST_INT MF_EXTRA_PRESTREAM_ON			BIT2		// When loading the intro cutscene for this mission ensure all map pre-streaming is turned on. Overrides MF_PRESTREAM_FULLY_OFF.

