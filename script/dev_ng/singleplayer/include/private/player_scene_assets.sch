USING "flow_public_game.sch" 
USING "rage_builtins.sch"
USING "tv_control_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_assets.sch										//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

// *******************************************************************************************
//	SCENE TIMETABLE
// *******************************************************************************************

PROC InitialiseBuddy(PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene, enumCharacterList eSceneBuddy,
		SCRIPT_TASK_NAME eLoopTask, SCRIPT_TASK_NAME eOutTask,
		VECTOR vSceneBuddyCoordOffset, FLOAT fSceneBuddyHeadOffset)
		
	sPassedScene.eSceneBuddy			= eSceneBuddy
	
	sPassedScene.eBuddyLoopTask			= eLoopTask
	sPassedScene.eBuddyOutTask			= eOutTask
	
	sPassedScene.vSceneBuddyCoordOffset	= vSceneBuddyCoordOffset
	sPassedScene.fSceneBuddyHeadOffset	= fSceneBuddyHeadOffset
	
ENDPROC
PROC BlankBuddy(PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	InitialiseBuddy(sPassedScene, NO_CHARACTER,
			SCRIPT_TASK_ANY, SCRIPT_TASK_ANY,
			<<0,0,0>>, 0)
	
ENDPROC


/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL SETUP_PLAYER_TIMETABLE_FOR_SCENE(PED_SCENE_STRUCT sPedScene, PLAYER_TIMETABLE_SCENE_STRUCT &sPassedScene)
	sPassedScene.bDescentOnly = FALSE
	
	SWITCH sPedScene.eScene
		CASE PR_SCENE_DEAD
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 sDead
			sDead = "Dead eScene for player timetable setup: "
			sDead += Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene)
			
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sDead)PRINTNL()
			SCRIPT_ASSERT(sDead)
			#ENDIF
			
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_ANY
			sPassedScene.eOutTask = SCRIPT_TASK_ANY
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_0_noVehicle
			
			RETURN FALSE
		BREAK
		CASE PR_SCENE_HOSPITAL
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_0_noVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_OVERRIDE
			sPassedScene.sScene = g_sOverrideScene[CHAR_MICHAEL]
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[CHAR_MICHAEL]
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_OVERRIDE
			sPassedScene.sScene = g_sOverrideScene[CHAR_FRANKLIN]
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[CHAR_FRANKLIN]
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_OVERRIDE
			sPassedScene.sScene = g_sOverrideScene[CHAR_TREVOR]
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[CHAR_TREVOR]
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_DEFAULT
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[sPedScene.ePed]
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_DEFAULT
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[sPedScene.ePed]
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DEFAULT
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = g_ePlayerLastVehState[sPedScene.ePed]
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_PHONECALL_ARM3
		CASE PR_SCENE_Fa_PHONECALL_FAM1
		CASE PR_SCENE_Fa_PHONECALL_FAM3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_USE_MOBILE_PHONE
			sPassedScene.eOutTask = SCRIPT_TASK_USE_MOBILE_PHONE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_ARM3
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = g_ePlayerLastVehState[sPedScene.ePed]
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
////			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
////					SCRIPT_TASK_START_SCENARIO_AT_POSITION, SCRIPT_TASK_START_SCENARIO_AT_POSITION,
////					<<112.7937, -1287.2606, 28.5529>>	- <<115.1569, -1286.6840, 28.2613>>,
////					-1.05								- 111.0000)
//			BlankBuddy(sPassedScene)
//			
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Fa_FAMILY1
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			
//			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
//					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
//					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
//					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
//			
//			sPassedScene.eVehState = PTVS_2_playerInVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_FBI1
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FAMILY3
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FBI2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL4
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_USE_MOBILE_PHONE
			sPassedScene.eOutTask = SCRIPT_TASK_USE_MOBILE_PHONE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FINALE2intro
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2intro
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FAMILY1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_LEAVE_VEHICLE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
					<<0,0,0>>, 0)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI4intro
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FBI4intro
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_FBI5
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_2_playerInVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Ma_FBI3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI4
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI5
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_FAMILY4_a
//		CASE PR_SCENE_Ma_FAMILY4_b
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Ta_FAMILY4
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALEC
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY3B
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ma_FRANKLIN2
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FRANKLIN2
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FBI1end
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Ma_MARTIN1
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_2_playerInVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Ta_MARTIN1
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			InitialiseBuddy(sPassedScene, CHAR_PATRICIA,
//					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
//					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
//					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Ta_CARSTEAL1
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_CARSTEAL1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_CARSTEAL2
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_RURAL2A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_RC_MRSP2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_Fa_NICE2B
//			sPedScene.eScene = PR_SCENE_F_TRAFFIC_a
//			IF SETUP_PLAYER_TIME""TABLE_FOR_SCENE(sPedScene, sPassedScene)
//				sPassedScene.sScene.eScene = PR_SCENE_Fa_NICE2B
//				RETURN TRUE
//			ENDIF
//		BREAK
//		CASE PR_SCENE_Ta_NICE2B
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1a
//		CASE PR_SCENE_FTa_FRANKLIN1b
		CASE PR_SCENE_FTa_FRANKLIN1c
		CASE PR_SCENE_FTa_FRANKLIN1d
		CASE PR_SCENE_FTa_FRANKLIN1e
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER
			sPassedScene.eOutTask = SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_Ma_FRANKLIN2
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			
//			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
//					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
//					<<0,0,0>>, 0)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Ta_FRANKLIN2
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_2_playerInVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_Ma_EXILE2
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_Fa_EXILE2
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene) // See B*1287746 the Chop script now creates Chop in the car
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_EXILE3
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_EXILE3
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_ENTER_VEHICLE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_MICHAEL3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_MICHAEL3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_DOCKS2A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_DOCKS2A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_DOCKS2B
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FINALE1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCY3A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FINALE2B
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ta_FINALE2B
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_JERKOFF
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		#ENDIF
		CASE PR_SCENE_T_HEADINSINK
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_MD_FBI2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			
			InitialiseBuddy(sPassedScene, CHAR_DAVE,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_MD_FRANKLIN2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_BEDROOM
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-0.7035, -0.0282, 0.0000>>, 0.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-3.0300, -0.7200, 0.0800>>, 168.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE1_a
		CASE PR_SCENE_M2_SAVEHOUSE1_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_play_anim, SCRIPT_TASK_play_anim,
					<<-14.1564, 8.1640, 2.6407>>+<<-2.1600, 0.0300, -1.0000>>, -66.0000+-56.0000)
			
			IF (sPedScene.eScene = PR_SCENE_M2_SAVEHOUSE1_b)
				BlankBuddy(sPassedScene)
			ENDIF
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WASHFACE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WAKEUPSCREAM
//		CASE PR_SCENE_M4_HOUSEBED_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_HOUSEBED
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WAKESUPSCARED
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M4_WATCHINGTV
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_KIDS_TV
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_POOLSIDE_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_POOLSIDE_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
//		CASE PR_SCENE_M_OFFICE			RETURN false		BREAK
		CASE PR_SCENE_M2_CARSLEEP_a
		CASE PR_SCENE_M2_CARSLEEP_b
		CASE PR_SCENE_M6_CARSLEEP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_CANAL_a
		CASE PR_SCENE_M_CANAL_b
		CASE PR_SCENE_M_CANAL_c
		CASE PR_SCENE_M_PIER_b
		CASE PR_SCENE_M2_SMOKINGGOLF
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			IF (sPedScene.eScene = PR_SCENE_M_CANAL_b)
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF	
			IF (sPedScene.eScene = PR_SCENE_M_PIER_b)
				sPassedScene.eVehState = PTVS_0_noVehicle
			ENDIF
			IF (sPedScene.eScene = PR_SCENE_M2_SMOKINGGOLF)
				BlankBuddy(sPassedScene)
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF	
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_LUNCH_a
//		CASE PR_SCENE_M7_LUNCH_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			IF (sPedScene.eScene = PR_SCENE_M2_LUNCH_a)
				InitialiseBuddy(sPassedScene, CHAR_AMANDA,
						SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE, 		//"s""i""t", "plyr_sit_LOOP",
						<<-1.7100, 1.0500, 0.0000>>, -138.0000)
			ENDIF
//			IF (sPedScene.eScene = PR_SCENE_M7_LUNCH_b)
//				InitialiseBuddy(sPassedScene, CHAR_AMANDA,
//						SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE, 		//"s""i""t", "plyr_sit_LOOP",
//						<<-1.7100, 1.0500, 0.0000>>, -138.0000)
//			ENDIF
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_EXITRESTAURANT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_LUNCH_b
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M_BAR_a
//		CASE PR_SCENE_M_BAR_b
		CASE PR_SCENE_M6_LIQUORSTORE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_VWOODPARK_a
		CASE PR_SCENE_M_VWOODPARK_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_RURAL1
		CASE PR_SCENE_M_PARKEDHILLS_a
		CASE PR_SCENE_M_PARKEDHILLS_b
		CASE PR_SCENE_M4_PARKEDBEACH
		CASE PR_SCENE_M6_PARKEDHILLS_a
		CASE PR_SCENE_M6_PARKEDHILLS_b
		CASE PR_SCENE_M6_PARKEDHILLS_c
		CASE PR_SCENE_M6_PARKEDHILLS_d
		CASE PR_SCENE_M6_PARKEDHILLS_e
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_DRIVING_a
		CASE PR_SCENE_M2_DRIVING_b
		CASE PR_SCENE_M6_DRIVING_a
		CASE PR_SCENE_M6_DRIVING_b
		CASE PR_SCENE_M6_DRIVING_c
		CASE PR_SCENE_M6_DRIVING_d
		CASE PR_SCENE_M6_DRIVING_e
		CASE PR_SCENE_M6_DRIVING_f
		CASE PR_SCENE_M6_DRIVING_g
		CASE PR_SCENE_M6_DRIVING_h
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_RONBORING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_RON,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.9800, 0.8017, -0.4200>>, -180.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M2_CYCLING_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_CYCLING_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_CYCLING_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_PHARMACY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_0_noVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M_TRAFFIC_a
		CASE PR_SCENE_M_TRAFFIC_b
		CASE PR_SCENE_M_TRAFFIC_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_WIFEEXITSCAR
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			InitialiseBuddy(sPassedScene, CHAR_TRACEY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_LEAVE_VEHICLE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFDAU_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			InitialiseBuddy(sPassedScene, CHAR_TRACEY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_LEAVE_VEHICLE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_LEAVE_VEHICLE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_DROPOFFSON_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_LEAVE_VEHICLE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_PIER_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_MARINA
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_ARGUEWITHWIFE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-2.0400, -0.1500, 0.0000>>, 14.4000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_HOOKERMOTEL
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_COFFEE_a
		CASE PR_SCENE_M_COFFEE_b
		CASE PR_SCENE_M_COFFEE_c
		CASE PR_SCENE_M4_CINEMA
		CASE PR_SCENE_M7_COFFEE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M_BENCHCALL_a
		CASE PR_SCENE_M_BENCHCALL_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_ONPHONE
		CASE PR_SCENE_M6_DEPRESSED
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_MORNING_a
//		CASE PR_SCENE_M6_MORNING_b
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_HOUSETV_a
//		CASE PR_SCENE_M6_HOUSETV_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M6_SUNBATHING
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_DRINKINGBEER
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M6_BOATING
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M6_PILOTSCHOOL
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
//			//
//			//
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_0_noVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_M6_TRIATHLON
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
//			//
//			//
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_0_noVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_M7_RESTAURANT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-1.7100, 1.0500, 0.0000>>, -138.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.1779, 2.4276, 0.0000>>, 162.1022)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BYESOLOMON_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.3427, 1.1592, 1.0351>>, 139.9120)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_REJECTENTRY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_HOOKERS
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITBARBER
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EXITFANCYSHOP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_FAKEYOGA
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			#IF USE_TU_CHANGES
				//#1603258
				BlankBuddy(sPassedScene)
			#ENDIF
			
			#IF NOT USE_TU_CHANGES
				InitialiseBuddy(sPassedScene, CHAR_AMANDA,
						SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_STAND_STILL,
						<<-4.2466, -5.8385, 1.0000>>, -83.6963)
			#ENDIF

			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_GETSREADY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-3.0300, -0.7200, 0.0800>>, 168.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_PARKEDHILLS
//			sPedScene.eScene = PR_SCENE_M_PARKEDHILLS_a
//			IF SETUP_PLAYER_TIME""TABLE_FOR_SCENE(sPedScene, sPassedScene)
//				sPassedScene.sScene.eScene = PR_SCENE_M7_parkedhills
//				RETURN TRUE
//			ENDIF
//		BREAK
		CASE PR_SCENE_M7_READSCRIPT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_EMPLOYEECONVO
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TALKTOGUARD
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOT_JIMMY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
						SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_SYNCHRONIZED_SCENE,
						<<-0.8472, 0.1550, 0.0000>>, 152.5560)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_TV
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
						SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
						<<-0.3577, -0.8968, -0.0003>>, -0.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_OPENDOORFORAMA
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_AMANDA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_JIMMY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0,0,0>>, 0)
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_TRACEYEXITSCAR
//		CASE PR_SCENE_M_HOOKERCAR

			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_TRACEY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0,0,0>>, 0)
			
//			IF sPedScene.eScene = PR_SCENE_M_HOOKERCAR
//				sPassedScene.eSceneBuddy = CHAR_BLANK_ENTRY
//			ENDIF
			
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_BIKINGJIMMY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_S_FAMILY4
			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_0_noVehicle
			
			RETURN TRUE
		BREAK
	ENDSWITCH
	SWITCH sPedScene.eScene
		CASE PR_SCENE_F0_SH_ASLEEP
		CASE PR_SCENE_F1_SH_ASLEEP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_NAPPING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_GETTINGREADY
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_READING
		CASE PR_SCENE_F1_SH_READING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.0, 1.0, 0.0>>, 0)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			IF g_bMagDemoActive
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF
						
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<4.2943, -6.7858, 0.0000>>, -133.5332)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			IF g_bMagDemoActive
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF
						
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			IF g_bMagDemoActive
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			IF g_bMagDemoActive
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
		CASE PR_SCENE_F1_GARBAGE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_THROW_CUP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_GYM
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F1_POOLSIDE_a
		CASE PR_SCENE_F1_POOLSIDE_b
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_CLEANINGAPT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONCELL
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_SNACKING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_IRONING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F0_WATCHINGTV
		CASE PR_SCENE_F1_WATCHINGTV
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_TANISHAFIGHT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_TANISHA,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_NEWHOUSE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_WALKCHOP
//		CASE PR_SCENE_F1_WALKCHOP
		CASE PR_SCENE_F_WALKCHOP_a
		CASE PR_SCENE_F_WALKCHOP_b
		CASE PR_SCENE_F0_PLAYCHOP
		CASE PR_SCENE_F1_PLAYCHOP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<3.4381, -0.8269, 0.0000>>, -87.6612)
					
			IF (sPedScene.eScene <> PR_SCENE_F1_PLAYCHOP)
				sPassedScene.vSceneBuddyCoordOffset	= <<3.4381, -0.8269, 0.0000>>
				sPassedScene.fSceneBuddyHeadOffset	= -87.6612
			ENDIF
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_AGENCY1
		CASE PR_SCENE_F_TRAFFIC_a
		CASE PR_SCENE_F_TRAFFIC_b
		CASE PR_SCENE_F_TRAFFIC_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_AGENCYprep1
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F0_BIKE
		CASE PR_SCENE_F1_BIKE
		CASE PR_SCENE_F_BIKE_c
		CASE PR_SCENE_F_BIKE_d
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_CLEANCAR
		CASE PR_SCENE_F1_CLEANCAR
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		BREAK
		CASE PR_SCENE_F1_BYETAXI
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<3.4381, -0.8269, 0.0000>>, -87.6612)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_LAMGRAFF
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
//					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
//					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
//					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
//
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F_CLUB
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<-0.7949, -0.5080, 0.0000>>, 0.0000)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_CS_CHECKSHOE
		CASE PR_SCENE_F_CS_WIPEHANDS
		CASE PR_SCENE_F_CS_WIPERIGHT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_BAR_a_01
		CASE PR_SCENE_F_BAR_b_01
		CASE PR_SCENE_F_BAR_c_02
		CASE PR_SCENE_F_BAR_d_02
		CASE PR_SCENE_F_BAR_e_01
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_TAUNT
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P1
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.2900, 1.4100, 0.0000>>, 57.5200)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_LAMTAUNT_P2
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
//					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
//					<<-2.5964, -1.9707, 1.0000>>, 180.0000)
//			
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.2900, 1.4100, 0.0000>>, 57.5200)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_P5
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.5504, -0.2322, 0.0000>>, -9.9741)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_LAMTAUNT_NIGHT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_LAMAR,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.2300, 0.6000, -1.0000>>, 34.5600)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_S_EXILE2
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_0_noVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_F_S_AGENCY_2A_a
		CASE PR_SCENE_F_S_AGENCY_2A_b
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_F_S_FBI1end
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_F_S_AGENCY_2B
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_2_playerInVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_F_S_AGENCY_2C
//			sPassedScene.sScene = sPedScene
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		
		
//		CASE PR_SCENE_T_STRIPCLUB_a
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
//			
//			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
//					SCRIPT_TASK_play_anim, SCRIPT_TASK_play_anim,
//					<<-0.7949, -0.5080, 0.0000>>, 0.0000)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_SC_MOCKLAPDANCE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_BAR
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.5033, 1.2118, 0.0000>>, -78.0449)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.5033, 1.2118, 0.0000>>, -78.0449)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_STRIPCLUB_out
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CN_CHATEAU_b
		CASE PR_SCENE_T_CN_CHATEAU_c
		CASE PR_SCENE_T_CR_CHATEAU_d
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T_PUKEINTOFOUNT
		CASE PR_SCENE_T_CN_PARK_b
//		CASE PR_SCENE_T_CN_PARK_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SMOKEMETH
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DOCKS_a
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_SWEATSHOP
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_ALLEYDRUNK
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_ISLAND
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_ALLEYDRUNK
		CASE PR_SCENE_T_CN_WAKETRASH_b
		CASE PR_SCENE_T_CR_WAKEBEACH
		CASE PR_SCENE_T_CN_WAKEBARN
		CASE PR_SCENE_T_CN_WAKETRAIN
		CASE PR_SCENE_T_CR_WAKEROOFTOP
		CASE PR_SCENE_T_CN_WAKEMOUNTAIN
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			BlankBuddy(sPassedScene)
			
			IF (sPedScene.eScene = PR_SCENE_T_CN_WAKETRASH_b)
				sPassedScene.eVehState = PTVS_1_playerWithVehicle
			ENDIF
			IF (sPedScene.eScene = PR_SCENE_T_CN_WAKETRAIN)
				sPassedScene.eVehState = PTVS_0_noVehicle
			ENDIF
			IF (sPedScene.eScene = PR_SCENE_T_CN_WAKEBARN)
				InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
						SCRIPT_TASK_STAND_STILL, SCRIPT_TASK_GO_STRAIGHT_TO_COORD,
						<<-2.4166, -0.1500, 0>>, -148.7555)
			ENDIF
			IF (sPedScene.eScene = PR_SCENE_T_CR_WAKEBEACH)
				InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
						SCRIPT_TASK_GO_STRAIGHT_TO_COORD, SCRIPT_TASK_GO_STRAIGHT_TO_COORD,
						<<-5.7915, -11.0439, 0.6700>>, -72.5890)
			ENDIF
			
			RETURN TRUE
		BREAK

		CASE PR_SCENE_T_FIGHTBBUILD
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SCARETRAMP
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_GARDEN
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_DUMPSTER
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_FUNERAL
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<GET_RANDOM_FLOAT_IN_RANGE(-5, 5), GET_RANDOM_FLOAT_IN_RANGE(-5, 5), 0.0000>>,
					GET_RANDOM_FLOAT_IN_RANGE(-180, 180))
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_CHASECAR_a
		CASE PR_SCENE_T_CN_CHASECAR_b
		CASE PR_SCENE_T_CR_CHASEBIKE
		CASE PR_SCENE_T_CR_CHASESCOOTER
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_POLICE_a
		CASE PR_SCENE_T_CN_POLICE_b
		CASE PR_SCENE_T_CN_POLICE_c
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_LINGERIE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_CR_MACHINE
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_CR_BRIDGEDROP
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FIGHTBAR_a
		CASE PR_SCENE_T_FIGHTBAR_b
		CASE PR_SCENE_T_FIGHTBAR_c
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_YELLATDOORMAN
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FIGHTYAUCLUB_b
		CASE PR_SCENE_T_FIGHTCASINO
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_RUDEATCAFE
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_RAND_TEMPLE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_UNDERPIER
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_KONEIGHBOUR
			sPassedScene.sScene = sPedScene
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<< 0.0, 0.0, 0.0 >>, -0.0)
					
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_DRUNKHOWLING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_play_anim, SCRIPT_TASK_play_anim,
					<<-3.2640, -14.7895, 14.1815>>, -0.6311)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_DRUNKHOWLING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.0, 0.0, 0.0>>, 0.0)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.0, 0.0, 0.0>>, 0.0)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.2400, 0.3300, -0.5000>>, -11.8800)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.2400, 0.3300, -0.5000>>, -11.8800)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_BEAR
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.4100, 1.3500, 0.0000>>, 168.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<1.7491, 0.6423, 0.0000>>, 166.4790)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDPINEAPPLE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			InitialiseBuddy(sPassedScene, CHAR_FLOYD,
					SCRIPT_TASK_SYNCHRONIZED_SCENE, SCRIPT_TASK_SYNCHRONIZED_SCENE,
					<<0.7200, -0.6900, -0.0>>, 172.4400)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_T6_SMOKECRYSTAL
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T6_EVENING
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
//			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T6_METHLAB
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_play_anim
			sPassedScene.eOutTask = SCRIPT_TASK_play_anim
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_HUNTING1
		CASE PR_SCENE_T6_HUNTING2
		CASE PR_SCENE_T6_HUNTING3
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_TRAF_AIR
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_T6_DISPOSEBODY_A
//			sPassedScene.sScene = sPedScene
//			
//			sPassedScene.eLoopTask = SCRIPT_TASK_STAND_STILL
//			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
//			
//			BlankBuddy(sPassedScene)
//			sPassedScene.eVehState = PTVS_1_playerWithVehicle
//			
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_T6_DIGGING
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			sPassedScene.eOutTask = SCRIPT_TASK_SYNCHRONIZED_SCENE
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CN_PIER
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_AIM_GUN_AT_ENTITY
			sPassedScene.eOutTask = SCRIPT_TASK_AIM_GUN_AT_ENTITY
			
			InitialiseBuddy(sPassedScene, CHAR_BLANK_ENTRY,
					SCRIPT_TASK_COWER, SCRIPT_TASK_HANDS_UP,
					<<-9.5443, -3.1910, -0.3000>>, 180.0000)
			
			sPassedScene.eVehState = PTVS_1_playerWithVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_LAKE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_GO_STRAIGHT_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_STAND_STILL
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_0_noVehicle
			
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLYING_PLANE
			sPassedScene.sScene = sPedScene
			
			sPassedScene.eLoopTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			sPassedScene.eOutTask = SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
			
			BlankBuddy(sPassedScene)
			sPassedScene.eVehState = PTVS_2_playerInVehicle
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for player timetable setup: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	sPassedScene.sScene = sPedScene
	sPassedScene.eLoopTask = SCRIPT_TASK_ANY
	sPassedScene.eOutTask = SCRIPT_TASK_ANY
	BlankBuddy(sPassedScene)
	sPassedScene.eVehState = PTVS_0_noVehicle
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL SETUP_BLANK_BUDDY_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene,
		PED_TYPE &PedType, MODEL_NAMES &model)
	PedType = PEDTYPE_MISSION
	
	SWITCH eScene
		CASE PR_SCENE_Ma_FAMILY1
			model = A_M_M_EastSA_02		RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_BAR
			model = S_F_Y_BARTENDER_01	RETURN TRUE
		BREAK
		CASE PR_SCENE_T_SC_CHASE
			model = S_M_M_BOUNCER_01	RETURN TRUE
		BREAK
//		CASE PR_SCENE_T_THROW_FOOD
//			model = S_M_M_BOUNCER_01	RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_CN_WAKEBARN
			model = A_C_COW				RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_WAKEBEACH
			model = A_F_M_FatCult_01	RETURN TRUE
		BREAK
//		CASE PR_SCENE_M_HOOKERCAR
		CASE PR_SCENE_M7_HOOKERS
			model = S_F_Y_HOOKER_01		RETURN TRUE
		BREAK
//		CASE PR_SCENE_M7_TALKTOGUARD
//			model = S_M_M_Security_01	RETURN TRUE
//		BREAK
//		CASE PR_SCENE_T_STRIPCLUB_a
//			model = A_M_M_Business_01	RETURN TRUE
//		BREAK
		CASE PR_SCENE_T_DRUNKHOWLING
			model = A_F_Y_BevHills_01	RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CR_BLOCK_CAMERA
			model = A_F_M_TOURIST_01	RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			model = S_M_O_BUSKER_01		RETURN TRUE
		BREAK
		CASE PR_SCENE_T_CN_PIER			FALLTHRU
			model = G_M_Y_LOST_01		RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_CLUB			FALLTHRU
		CASE PR_SCENE_F1_BYETAXI
			model = A_F_Y_BevHills_02	RETURN TRUE
		BREAK
		//CASE PR_SCENE_Fa_EXILE2			FALLTHRU // See B*1287746 the Chop script now creates Chop in the car
		CASE PR_SCENE_F0_WALKCHOP		FALLTHRU
		CASE PR_SCENE_F0_PLAYCHOP		FALLTHRU
//		CASE PR_SCENE_F1_WALKCHOP		FALLTHRU
		CASE PR_SCENE_F1_PLAYCHOP		FALLTHRU
		CASE PR_SCENE_F_WALKCHOP_a		FALLTHRU
		CASE PR_SCENE_F_WALKCHOP_b
			model = GET_CHOP_MODEL()	RETURN TRUE
		BREAK
		CASE PR_SCENE_F_KUSH_DOC_a
			model = A_F_M_BEACH_01		RETURN TRUE
		CASE PR_SCENE_F_MD_KUSH_DOC
			model = A_M_Y_BeachVesp_01	RETURN TRUE	//#829176
		CASE PR_SCENE_T_KONEIGHBOUR
			model = A_M_Y_BevHills_02	RETURN TRUE
			
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "invalid eScene for SETUP_BLANK_BUDDY_FOR_SCENE: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
//	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	PedType = PEDTYPE_MISSION
	model = DUMMY_MODEL_FOR_SCRIPT
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_AUDIO_BANK_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tBankName, TEXT_LABEL_63 &tBankBName)
	
	tBankBName = ""
	
	SWITCH eScene
		CASE PR_SCENE_M_COFFEE_a
		CASE PR_SCENE_M_COFFEE_b
		CASE PR_SCENE_M_COFFEE_c
		CASE PR_SCENE_M4_CINEMA
		CASE PR_SCENE_M7_COFFEE
			tBankName = "PRM_COFFEE"					RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_LOUNGECHAIRS
			tBankName = "PRM7_LOUNGECHAIRS"				RETURN TRUE
		BREAK
		CASE PR_SCENE_M7_KIDS_GAMING
			tBankName = "PRM7_KIDS_GAMING"				RETURN TRUE
		BREAK
		CASE PR_SCENE_M4_WATCHINGTV
			tBankName = "SAFEHOUSE_MICHAEL_SIT_SOFA"	RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDSAVEHOUSE
			tBankName = "SAFEHOUSE_MICHAEL_SIT_SOFA"	RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_ASLEEP
		CASE PR_SCENE_F1_SH_ASLEEP
			tBankName = "SAFEHOUSE_FRANKLIN_SOFA"		RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_SH_PUSHUP_a
		CASE PR_SCENE_F0_SH_PUSHUP_b
		CASE PR_SCENE_F1_SH_PUSHUP
			tBankName = "FRANKLIN_PUSHUPS"				RETURN TRUE
		BREAK
		CASE PR_SCENE_F0_GARBAGE
		CASE PR_SCENE_F1_GARBAGE
			tBankName = "PRF0_GARBAGE"					RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_WATCHINGTV
			tBankName = "PRF1_WATCHINGTV"				RETURN TRUE
		BREAK
		CASE PR_SCENE_F1_IRONING
			tBankName = "PRF1_IRONING"					RETURN TRUE
		BREAK
		CASE PR_SCENE_F_HIT_CUP_HAND
			tBankName = "PRF_HIT_CUP_HAND"				RETURN TRUE
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE PR_SCENE_T_SHIT
			tBankName = "TREVOR_GET_OFF_TOILET"			RETURN TRUE		BREAK
		CASE PR_SCENE_T_JERKOFF
			tBankName = "T_JERKOFF"						RETURN TRUE		BREAK
		#ENDIF
		CASE PR_SCENE_F1_SNACKING
			tBankName = "PRF1_SNACKING"					RETURN TRUE
		BREAK
		CASE PR_SCENE_T_FLOYDCRYING_A
		CASE PR_SCENE_T_FLOYDCRYING_E0
		CASE PR_SCENE_T_FLOYDCRYING_E1
		CASE PR_SCENE_T_FLOYDCRYING_E2
		CASE PR_SCENE_T_FLOYDCRYING_E3
			tBankName = "SAFEHOUSE_FRANKLIN_SOFA"		RETURN TRUE
		BREAK
		CASE PR_SCENE_T_GUITARBEATDOWN
			tBankName = "PRT_GUITARBEATDOWN"
			tBankBName = "PRT_GUITARBEATDOWN_MUSIC"		RETURN TRUE
		BREAK
		CASE PR_SCENE_T_NAKED_ISLAND
			tBankName = "PRT_NAKED_ISLAND"				RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_FLUSHESFOOT
			tBankName = "PRT6_FLUSHESFOOT_01"
			tBankBName = "PRT6_FLUSHESFOOT_02"
			RETURN TRUE
		BREAK
		CASE PR_SCENE_T6_DIGGING
			tBankName = "T6_DIGGING"					RETURN TRUE		BREAK
		CASE PR_SCENE_T_HEADINSINK
			tBankName = "PRT_HEADSINK"					RETURN TRUE		BREAK
		CASE PR_SCENE_T_FLOYD_DOLL
			tBankName = "PRT_FLOYD_DOLL"				RETURN TRUE		BREAK
//		CASE PR_SCENE_T6_BLOWSHITUP
//			tBankName = "TREVOR_BLOWING_SHIT_UP"		RETURN TRUE		BREAK
		
		CASE PR_SCENE_T_STRIPCLUB_out
			tBankName = "PRT_STRIPCLUB_out"				RETURN TRUE		BREAK
		CASE PR_SCENE_T_THROW_FOOD
			tBankName = "PRT_THROW_FOOD"				RETURN TRUE		BREAK
		CASE PR_SCENE_T_GARBAGE_FOOD
			tBankName = "PRT_GARBAGE_FOOD"				RETURN TRUE		BREAK
			
			
		CASE PR_SCENE_T_CR_BRIDGEDROP
			tBankName = "PRT_CR_BRIDGEDROP"				RETURN TRUE		BREAK
		CASE PR_SCENE_M2_BEDROOM
			tBankName = "PRM2_BEDROOM"					RETURN TRUE		BREAK
			
		CASE PR_SCENE_M7_DROPPINGOFFJMY
			tBankName = "PRM7_DROPPINGOFFJMY"			RETURN TRUE		BREAK
		CASE PR_SCENE_M7_WIFETENNIS
			tBankName = "PRM7_WIFETENNIS"				RETURN TRUE		BREAK
		CASE PR_SCENE_M6_RONBORING
			tBankName = "PRM6_RONBORING"				RETURN TRUE		BREAK
			
		CASE PR_SCENE_T_DOCKS_a
		CASE PR_SCENE_T_DOCKS_b
		CASE PR_SCENE_T_DOCKS_c
		CASE PR_SCENE_T_DOCKS_d
			tBankName = "PRT_DOCKS"						RETURN TRUE		BREAK
		CASE PR_SCENE_T_FLOYDSPOON_A
		CASE PR_SCENE_T_FLOYDSPOON_A2
		CASE PR_SCENE_T_FLOYDSPOON_B
		CASE PR_SCENE_T_FLOYDSPOON_B2
			tBankName = "PRT_FLOYDSPOON"				RETURN TRUE		BREAK
		CASE PR_SCENE_T_SMOKEMETH 
			tBankName = "PRT_SMOKEMETH"					RETURN TRUE		BREAK
		CASE PR_SCENE_T_ESCORTED_OUT
			tBankName = "PRT_ESCORTED_OUT"				RETURN TRUE		BREAK
//		CASE PR_SCENE_M4_DOORSTUMBLE
//			tBankName = "PRM4_DOORSTUMBLE"				RETURN TRUE		BREAK
		CASE PR_SCENE_T_FIGHTBBUILD
			tBankName = "PRT_FIGHTBBUILD"				RETURN TRUE		BREAK
		
		CASE PR_SCENE_T_ANNOYSUNBATHERS
			tBankName = "PRT_ANNOYSUNBATHERS"			RETURN TRUE		BREAK
		CASE PR_SCENE_T_NAKED_BRIDGE
			tBankName = "PRT_NAKED_BRIDGE"				RETURN TRUE		BREAK
		
		CASE PR_SCENE_F0_SH_READING
		CASE PR_SCENE_F1_SH_READING
			tBankName = "PRF0_SH_READING"				RETURN TRUE		BREAK
		CASE PR_SCENE_F1_ONLAPTOP
			tBankName = "PRF1_ONLAPTOP"					RETURN TRUE		BREAK
		
		CASE PR_SCENE_T_SC_BAR
			tBankName = "PRT_SC_BAR"					RETURN TRUE		BREAK
		CASE PR_SCENE_M4_WASHFACE
			tBankName = "PRM4_WASHFACE"					RETURN TRUE		BREAK
		CASE PR_SCENE_M7_ROUNDTABLE
			tBankName = "PRM7_ROUNDTABLE"				RETURN TRUE		BREAK

	ENDSWITCH
	
	tBankName = ""
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_SYNCH_AUDIO_EVENT_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tAudioEvent, INT &iStartOffsetMs)
	SWITCH eScene
//		CASE PR_SCENE_T_SHIT			//#808597
//			tAudioEvent = "TRV_IG_4"	iStartOffsetMs = 0	RETURN TRUE
//		BREAK
		CASE PR_SCENE_F0_TANISHAFIGHT	//#1563952
			tAudioEvent = "FRAS_IG_19"		iStartOffsetMs = 0	RETURN TRUE	BREAK
		CASE PR_SCENE_F_MD_KUSH_DOC
			tAudioEvent = "WEED_EXCHANGE"	iStartOffsetMs = 0	RETURN TRUE	BREAK
		
	ENDSWITCH
	
	tAudioEvent = ""
	iStartOffsetMs = 0
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_AUDIO_SCENE_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &tSceneName)
	SWITCH eScene
		CASE PR_SCENE_F0_TANISHAFIGHT	//#1795963
			tSceneName = "PRF0_TANISHAFIGHT_SCENE"		RETURN TRUE	BREAK
		
	ENDSWITCH
	
	tSceneName = ""
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_INTERIOR_ENTITY_SET_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &entitySetName, BOOL &bActivateSet)
	SWITCH eScene
		CASE PR_SCENE_F1_ONLAPTOP
			entitySetName = "showhome_only"
			bActivateSet = FALSE	//TRUE
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_BEDROOM		FALLTHRU
		CASE PR_SCENE_M2_SAVEHOUSE0_b	FALLTHRU
		CASE PR_SCENE_M4_WAKEUPSCREAM	FALLTHRU
		CASE PR_SCENE_M4_WAKESUPSCARED	FALLTHRU
		CASE PR_SCENE_M7_GETSREADY
			entitySetName = "V_Michael_bed_messy"
			bActivateSet = TRUE
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	entitySetName = ""
	bActivateSet = FALSE
	RETURN FALSE
ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_TIMECYCLE_MOD_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL &TCmod)
	SWITCH eScene
		CASE PR_SCENE_M4_WAKEUPSCREAM
		CASE PR_SCENE_M4_WAKESUPSCARED
			TCmod = "sleeping"
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	TCmod = ""
	RETURN FALSE
ENDFUNC

///// PURPOSE: 
//FUNC BOOL SETUP_PTFX_FOR_TIMETABLE_EXIT_SCENE(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_63 &fxName)
//	SWITCH eScene
//		CASE PR_SCENE_T6_DIGGING
//			fxName = "scr_pts_digging"
//			RETURN TRUE
//		BREAK
//		
//	ENDSWITCH
//	
//	fxName = ""
//	RETURN FALSE
//ENDFUNC

/// PURPOSE: 
FUNC BOOL SETUP_SYNCH_SHAKE_EVENT_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene, INT &Duration, INT &MinFrequency, INT &MaxFrequency)
	SWITCH eScene
		CASE PR_SCENE_T_CR_BRIDGEDROP
			Duration = 050
			
			MinFrequency = 025
			MaxFrequency = 150
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	Duration = 0
	MinFrequency = 0
	MinFrequency = 0
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_FORCE_STEP_TYPE_FOR_SCENE(PED_REQUEST_SCENE_ENUM eScene)
	
	
/*
SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), TRUE, 20, 0 ) when Michael is in bed
SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), TRUE, 0, 1) when Michael is coming out of the wardrobe 
SET_FORCE_STEP_TYPE(PLAYER_PED_ID(), FALSE, 0,0 ) when the scene finishes.
*/
	
	SWITCH eScene
		CASE PR_SCENE_M2_BEDROOM
			RETURN TRUE
		BREAK
		CASE PR_SCENE_M2_SAVEHOUSE0_b
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
FUNC BOOL Preload_SpecialMissionAssets(PED_REQUEST_SCENE_ENUM eScene)
#if USE_CLF_DLC
	eScene = eScene
#endif
#if USE_NRM_DLC
	eScene = eScene
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH eScene
		CASE PR_SCENE_F_S_EXILE2
//			MODEL_NAMES franklin_model
//			franklin_model = GET_PLAYER_PED_MODEL(CHAR_FRANKLIN)
			
			MODEL_NAMES gang_enemy_0_model
			gang_enemy_0_model = a_m_m_hillbilly_02
			
			MODEL_NAMES chop_model
			chop_model = GET_CHOP_MODEL()
			
			MODEL_NAMES franklins_car_model
			franklins_car_model = BALLER
			
			MODEL_NAMES gang_car_model
			gang_car_model = dubsta
			
//			REQUEST_MODEL(franklin_model)
			REQUEST_MODEL(franklins_car_model)
			REQUEST_MODEL(gang_car_model)
			REQUEST_MODEL(gang_enemy_0_model)
			REQUEST_MODEL(chop_model)
			REQUEST_MODEL(radi)
			REQUEST_MODEL(sabregt)	//tampa)
//			REQUEST_PTFX_ASSET()  	//script player_timetable_scene has no particle asset set up - can't request the asset
			REQUEST_VEHICLE_RECORDING(003, "lkexcile2")
			REQUEST_VEHICLE_RECORDING(004, "lkexcile2")
			REQUEST_VEHICLE_RECORDING(005, "lkexile2")
			REQUEST_VEHICLE_RECORDING(001, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(002, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(003, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(004, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(005, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(006, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(007, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(008, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(009, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(010, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(011, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(012, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(013, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(014, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(015, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(016, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(017, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(018, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(019, "lkexcile2_chase")
			REQUEST_VEHICLE_RECORDING(100, "lkexcile2_chase")
			request_anim_dict("missexile2")
			request_anim_dict("missexile2switch")
			request_anim_dict("missswitch")		
			request_anim_dict("shake_cam_all@")
			
			
			MISSION_FLOW_FORCE_TRIGGER_MISSION(SP_MISSION_EXILE_2)
			
		//	INCRAMENT_Pause_Outro_Count()		//for "SP_MISSION_EXILE_2"
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	#ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the coords and heading of the ped we are going to hotswap to for the specified scene.
#if USE_CLF_DLC
FUNC BOOL Request_SpecialMissionAssetsCLF(PED_SCENE_STRUCT sPedScene)

	sPedScene = sPedScene
	g_bFlowCleanupIntroCutscene = TRUE //Clean up any flow pre-loaded cutscenes as we change player character on an ambient switch.
									
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sInvalid
		sInvalid = "eScene requires no special assets: "
		sInvalid += Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene)	
		PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	#ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL Request_SpecialMissionAssetsNRM(PED_SCENE_STRUCT sPedScene)

	sPedScene = sPedScene
	g_bFlowCleanupIntroCutscene = TRUE //Clean up any flow pre-loaded cutscenes as we change player character on an ambient switch.
									
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sInvalid
		sInvalid = "eScene requires no special assets: "
		sInvalid += Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene)	
		PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
	#ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
FUNC BOOL Request_SpecialMissionAssets(PED_SCENE_STRUCT sPedScene)
#if USE_CLF_DLC
	return Request_SpecialMissionAssetsCLF(sPedScene)
#endif
#if USE_NRM_DLC
	return Request_SpecialMissionAssetsNRM(sPedScene)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH sPedScene.eScene
		CASE PR_SCENE_Fa_PHONECALL_ARM3	FALLTHRU
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	FALLTHRU
		CASE PR_SCENE_Fa_PHONECALL_FAM1	FALLTHRU
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	FALLTHRU
		CASE PR_SCENE_Fa_PHONECALL_FAM3	//FALLTHRU
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3
			
			// only update the flow flag if gameflow is active
			IF (g_savedGlobals.sFlow.isGameflowActive)
				
				// if one of Franklins post-Armenian3 stripclip switches is selected, set the "allow stripclub" bit to TRUE
				IF NOT (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB)))
					SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB), TRUE)
				ENDIF
				
//				MAKE_AUTOSAVE_REQUEST()
			ENDIF
			g_bFlowCleanupIntroCutscene = TRUE	//Clean up any flow pre-loaded cutscenes as we change player character on an ambient switch.
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_S_EXILE2
			Preload_SpecialMissionAssets(sPedScene.eScene)
			
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("GENERIC_PED_SCENE - g_iPauseOnOutro: \"")
			PRINTINT(g_iPauseOnOutro)
			PRINTSTRING(" for \"")
			PRINTSTRING(Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene))
			PRINTSTRING("\"")
			PRINTNL()
			#ENDIF
			g_bFlowCleanupIntroCutscene = TRUE	//Clean up any flow pre-loaded cutscenes as we change player character on an ambient switch.
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_F_S_AGENCY_2A_a
		CASE PR_SCENE_F_S_AGENCY_2A_b
			MISSION_FLOW_FORCE_TRIGGER_MISSION(SP_HEIST_AGENCY_2)
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_M_S_FAMILY4
			MISSION_FLOW_FORCE_TRIGGER_MISSION(SP_MISSION_FAMILY_4)
			RETURN TRUE
		BREAK

	ENDSWITCH
	
	g_bFlowCleanupIntroCutscene = TRUE //Clean up any flow pre-loaded cutscenes as we change player character on an ambient switch.
									
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 sInvalid
	sInvalid = "eScene requires no special assets: "
	sInvalid += Get_String_From_Ped_Request_Scene_Enum(sPedScene.eScene)
	
	PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(": ")PRINTSTRING(sInvalid)PRINTNL()
//	SCRIPT_ASSERT(sInvalid)
	#ENDIF
	
	RETURN FALSE
#endif
#endif
ENDFUNC



FUNC BOOL CONTROL_PLAYER_WATCHING_TV(PED_REQUEST_SCENE_ENUM eScene, TEXT_LABEL_31 &RoomName, FLOAT &fTurnOffTVPhase, TV_LOCATION &eRoomTVLocation, TVCHANNELTYPE &eTVChannelType, TV_CHANNEL_PLAYLIST &eTVPlaylist)
	
	eTVChannelType = TVCHANNELTYPE_CHANNEL_1
	eTVPlaylist = TV_PLAYLIST_STD_CNT
	
	SWITCH eScene
		CASE PR_SCENE_M2_SAVEHOUSE1_a	RoomName = "v_michael_g_lounge"		fTurnOffTVPhase = 1.0		eRoomTVLocation = TV_LOC_MICHAEL_PROJECTOR	RETURN TRUE BREAK
		CASE PR_SCENE_M2_KIDS_TV		RoomName = "v_michael_g_lounge"		fTurnOffTVPhase = 1.0		eRoomTVLocation = TV_LOC_MICHAEL_PROJECTOR	RETURN TRUE BREAK
		CASE PR_SCENE_M4_WATCHINGTV		RoomName = "v_michael_g_lounge"		fTurnOffTVPhase = 0.35		eRoomTVLocation = TV_LOC_MICHAEL_PROJECTOR	RETURN TRUE BREAK
		CASE PR_SCENE_M6_HOUSETV_a		RoomName = "v_TrailerRm" 			fTurnOffTVPhase = -1.0		eRoomTVLocation = TV_LOC_TREVOR_TRAILER		RETURN TRUE BREAK
//		CASE PR_SCENE_M6_HOUSETV_b		RoomName = "v_TrailerBedRm" 		fTurnOffTVPhase = 1.0		eRoomTVLocation = TV_LOC_TREVOR_TRAILER		RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_TV		RoomName = "v_michael_g_lounge" 	fTurnOffTVPhase = -1.0		eRoomTVLocation = TV_LOC_MICHAEL_PROJECTOR	RETURN TRUE BREAK
		CASE PR_SCENE_M7_KIDS_GAMING	RoomName = "v_michael_g_lounge" 	fTurnOffTVPhase = -1.0		eRoomTVLocation = TV_LOC_JIMMY_BEDROOM		eTVChannelType = TVCHANNELTYPE_CHANNEL_SPECIAL	eTVPlaylist = TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER	RETURN TRUE BREAK
		
//		CASE PR_SCENE_F0_WATCHINGTV		RoomName = "v_57_FrontRM"			fTurnOffTVPhase = 0.20		eRoomTVLocation = TV_LOC_NONE				RETURN TRUE BREAK
		CASE PR_SCENE_F1_WATCHINGTV		RoomName = "loungeB"				fTurnOffTVPhase = 0.20		eRoomTVLocation = TV_LOC_FRANKLIN_VINEWOOD	RETURN TRUE BREAK
		
		CASE PR_SCENE_T_FLOYDSAVEHOUSE	RoomName = "rm_Lounge"				fTurnOffTVPhase = 0.35		eRoomTVLocation = TV_LOC_TREVOR_VENICE		RETURN TRUE BREAK
		
	ENDSWITCH
	
	eRoomTVLocation = TV_LOC_NONE
	RoomName = ""
	fTurnOffTVPhase = -1
	eTVChannelType = TVCHANNELTYPE_CHANNEL_NONE
	eTVPlaylist = TV_PLAYLIST_NONE
	
	RETURN FALSE
ENDFUNC
