USING "RC_helper_functions.sch"
USING "RC_threat_public.sch"
//USING "commands_task.sch"

ENUM DELIVER_PED_TYPE
	DRIVER = 0,
	PASSENGER,
	
	MAX_DELIVERY_PEDS
ENDENUM

ENUM DELIVER_PED_CONTROL_STATES
	DPCS_DELIVERING,
	DPCS_REACTING
ENDENUM

ENUM DELIVER_PED_DELIVERY_STATES
	DPDS_NULL,
	DPDS_WALKING_TO_VAN_DOOR,
	DPDS_ENTER_VAN,
	DPDS_WAITING_IN_VAN,
	DPDS_DRIVE_TO_DELIVERY_POINT,
	DPDS_BREAKING,
	DPDS_PULLINGOVER,
	DPDS_WAIT_PASSENGER_DELIVER,
	DPDS_EXIT_VAN_DELIVER,
	DPDS_MAKE_DELIVERY,
	DPDS_WANDERING
ENDENUM

ENUM SUB_DELIVERY_STATES
	SDS_NULL = 0,
	SDS_WALKING_TO_BAG,
	SDS_PICK_UP_BIN_BAG,
	SDS_WALK_TO_BACK_OF_TRUCK,
	SDS_THROW_BAG_IN_TO_TRUCK,
	SDS_DROP_BAG
ENDENUM

ENUM DELIVER_PED_REACTION_STATES
	DPRS_NULL,
	DPRS_EXIT_VAN_FLEE,
	DPRS_BEING_JACKED,
	DPRS_WAIT_RING_COPS,
	DPRS_RING_COPS,
	DPRS_WAIT_REACT_TO_PLAYER_FLEE,
	DPRS_REACT_TO_PLAYER_INITAL_FLEE,
	DPRS_REACT_TO_PLAYER_FLEE,
	DPRS_FLEE_AFTER_RINGING_COPS,
	DPRS_REACT_TO_PLAYER_ANGER,
	DPRS_WAIT_REACT_TO_PLAYER_COWER,
	DPRS_WAIT_REACT_SEARCH,
	DPRS_REACT_TO_PLAYER_COWER,
	DPRS_REACT_TO_PLAYER_BLOCKING_DOOR,
	DPRS_REACT_TO_PLAYER_BLOCKING_BINS,
	DPRS_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK,
	DPRS_REACT_TO_PLAYER_BLOCKING_FRONT_OF_TRUCK,
	DPRS_REACT_TO_PLAYER_BUMP,
	DPRS_WAIT_FLIM_BLOCKING,
	DPRS_WAIT_FLIM_DEAD,
	DPRS_REACT_FLIM,
	DPRS_WAIT_REACT_BRAWL,
	DPRS_REACT_BRAWL,
	DPRS_REACT_SEARCH
	
ENDENUM

REL_GROUP_HASH HASH_ENEMIES			//Relationship hash for all ranchers


//PASSENGER_PED_STATES ePrevPassengerPed = PPS_NULL

//PERCEPTION CONSTANTS
CONST_FLOAT MAX_VISION_RANGE			20.0			//The distance a ped can identify that the player is a threat
CONST_FLOAT MAX_VISION_ARC_RANGE		170.0			//the vision arc that the player needs to be in before they are spotted

//DEILIVERY PED
CONST_INT MAX_DELIVERIES				21
INT iNumDeliveries = MAX_DELIVERIES


STRUCT PED_STRUCT
	PED_INDEX 						id
	INT								iReactionTimer
	INT								iReactionDelay
	INT								iQuickReactionDelay
	BLIP_INDEX						blip
	DELIVER_PED_CONTROL_STATES		eControlState = DPCS_DELIVERING
	DELIVER_PED_REACTION_STATES		eReactionState = DPRS_NULL
	DELIVER_PED_DELIVERY_STATES		eDeliverState = DPDS_NULL
	DELIVER_PED_DELIVERY_STATES		ePrevDeliverState = DPDS_NULL
	SUB_DELIVERY_STATES				eDeliverSubState = SDS_NULL
	DELIVER_PED_TYPE				eDeliverPedType
ENDSTRUCT

STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX		id
	BLIP_INDEX			blip
ENDSTRUCT

STRUCT DELIVERY_POINT
	VECTOR 				vDriveTo
	VECTOR				vParkingVec
	FLOAT				fDriveToTargetDist
	FLOAT				fDriveToStraightDist
	FLOAT				fParkHeading
	BOOL				bDoActionAtDeliveryPoint	= TRUE
	PARK_TYPE			eParkType					= PARK_TYPE_PULL_OVER
	VECTOR				vBlockingAreaCentre
	VECTOR				vBlockingAreaSize
	
	OBJECT_INDEX 		oiObject
	BOOL 				bDelivered 					= FALSE
	VECTOR				vPedSpawn
	FLOAT				fPedHeading
	MODEL_NAMES			pedModel
	BOOL				bSpawned 					= FALSE
ENDSTRUCT

STRUCT SHAPE_TEST_STRUCT
	SHAPETEST_INDEX ShapeTest
	SHAPETEST_TYPE	eShapeTestType
	//Input
	VECTOR			vStartPos
	VECTOR			vEndPos
	VECTOR			vSize 					//For box
	VECTOR			vEulerRot	 			//For box
	EULER_ROT_ORDER eEulerOrder = EULER_XYZ //For box
	FLOAT			fRadius
	BOOL			bRequested
	ENTITY_INDEX	EntityIndex 			//For bounding box test
	ENTITY_INDEX	HitEntity 				//For who we hit
	//Results
	BOOL			bResultIsNew
	VECTOR			vHitPos
	VECTOR			vHitNormal
	BOOL			bIsHit
ENDSTRUCT

SHAPE_TEST_STRUCT mGotoBlockingShape

PROC RESET_SHAPE_TEST()
	mGotoBlockingShape.ShapeTest = NULL
	mGotoBlockingShape.bIsHit  = FALSE
	mGotoBlockingShape.HitEntity = NULL
ENDPROC

VEHICLE_STRUCT mRoutedVehicle
PED_STRUCT mPed[MAX_DELIVERY_PEDS]
DELIVERY_POINT mDeliveryPoint[MAX_DELIVERIES]

FLOAT fTruckBootOpenRatio = -1.0
INT iDeliveryIndex
BOOL bIsLooped = FALSE
BOOL bIsSetRoute = FALSE
BOOL bPassengerDoDeliveries = FALSE
BOOL bCheckSeenPedDie = FALSE
BOOL bWantedLevelSet = FALSE

VECTOR vSearchPos = <<0,0,0>>

INT iPlayerBlockingTruckTimer = -1
///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD
//	BLIP_INDEX DebugBlip
	BOOL bShowDeliveryDebugText = TRUE
	WIDGET_GROUP_ID DeliverswidgetGroup
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDeliveryDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDeliveryDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDeliveryDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_DELIVERY_RAGE_WIDGETS()
		DeliverswidgetGroup = START_WIDGET_GROUP("Delivery Header file")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDeliveryDebugText)
			STOP_WIDGET_GROUP()			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_DELIVERY_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(DeliverswidgetGroup)
			DELETE_WIDGET_GROUP(DeliverswidgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_DELIVERY_RAG_WIDGETS()
	
	ENDPROC
#ENDIF

/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_PED_UNINJURED(pedindex)
				#IF IS_DEBUG_BUILD SK_PRINT("PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(pedindex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			objectindex = CREATE_OBJECT(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
				SET_ENTITY_HEADING(objectindex, dir)
				ACTIVATE_PHYSICS(objectindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(objectindex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_ENTITY_HEADING(vehicleindex, dir)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a ped inside a vehicle
/// PARAMS:
///    pedindex - The index to write the newly created ped back to (ref)
///    Car - The vehicle to create the ped in
///    modelName - The model of the ped to create
///    seat - the seat to create the ped in 
///    bUnloadAfterSpawn - should we unload the ped model after creation
/// RETURNS:
///    TRUE if the ped was spawned correctly
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	IF IS_VEHICLE_OK(Car)
		IF NOT DOES_ENTITY_EXIST(pedindex)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the heading between to vectors
/// PARAMS:
///    V1 - First vector
///    V2 - Second vector
/// RETURNS:
///    FLOAT -  the heading between the two
FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x,V2.y-V1.y)
ENDFUNC


/// PURPOSE:
///    Picks a new delivery point at random
/// RETURNS:
///    returns the index of the new delivery point
FUNC INT PICK_RANDOM_DELIVERY_POINT()
	INT i = GET_RANDOM_INT_IN_RANGE(0, iNumDeliveries-1)
	
	WHILE mDeliveryPoint[i].bDelivered
		i = GET_RANDOM_INT_IN_RANGE(0, iNumDeliveries-1)
	ENDWHILE

	RETURN i
ENDFUNC

PROC CHOOSE_NEXT_DELIVERY_POINT()
	INT i = 0
	IF bIsSetRoute
		//Find an index that hasnt had a delivery
		WHILE mDeliveryPoint[iDeliveryIndex].bDelivered
		AND i <> -1
			iDeliveryIndex++
			IF iDeliveryIndex >= iNumDeliveries-1
				i = -1
				iDeliveryIndex = iNumDeliveries-1
			ENDIF
		ENDWHILE

		IF iDeliveryIndex = iNumDeliveries-1
			IF bIsLooped
				iDeliveryIndex = 0
				FOR i=0 TO (iNumDeliveries-1)
					mDeliveryPoint[i].bDelivered = FALSE
				ENDFOR
			ELSE
				iDeliveryIndex = iNumDeliveries-1
				EXIT
			ENDIF
		ENDIF
	ELSE
		INT iCount
		iCount = 0
		FOR i=0 TO (iNumDeliveries-1)
			IF mDeliveryPoint[i].bDelivered
				iCount++
			ENDIF
		ENDFOR
		
		IF iCount = iNumDeliveries-1
			iDeliveryIndex = iNumDeliveries-1
		ELIF iCount = iNumDeliveries
			IF bIsLooped
				FOR i=0 TO (iNumDeliveries-1)
					mDeliveryPoint[i].bDelivered = FALSE
				ENDFOR
				iDeliveryIndex = PICK_RANDOM_DELIVERY_POINT()
			ELSE
				iDeliveryIndex = -1
				EXIT
			ENDIF
		ELSE
			iDeliveryIndex = PICK_RANDOM_DELIVERY_POINT()
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_CLOSEST_DELIVERY_POINT()
	INT iClosestID
	VECTOR vCurrentPos
	INT i
	#IF USE_TU_CHANGES
		INT iTestID
	#ENDIF
	FLOAT fDistances[MAX_DELIVERIES]
	IF IS_VEHICLE_OK(mRoutedVehicle.id)
		vCurrentPos = GET_ENTITY_COORDS(mRoutedVehicle.id)
	
		FOR i=0 TO (iNumDeliveries-1)
			FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(vCurrentPos, mDeliveryPoint[i].vDriveTo)
			IF NOT IS_VECTOR_ZERO(mDeliveryPoint[i].vDriveTo)
			AND fDist > 25
				fDistances[i] = fDist
			ELSE
				fDistances[i] = -1.0
			ENDIF
		ENDFOR
		
		FLOAT lowestFloat = fDistances[0]
		FOR i = 0 TO (iNumDeliveries-1)
			IF fDistances[i] != -1.0
				IF fDistances[i] < lowestFloat
					lowestFloat = fDistances[i]
					iClosestID = i
				ENDIF
			ENDIF
		ENDFOR
		
		
		FLOAT dot = DOT_PRODUCT((mDeliveryPoint[iClosestID].vDriveTo- vCurrentPos), GET_ENTITY_FORWARD_VECTOR(mRoutedVehicle.id))
		#IF NOT USE_TU_CHANGES
			#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("dot FROM CURRENT TO CLOSEST  ========== ", dot) #ENDIF
			//Check the point isnt behind us
			INT p = GET_GAME_TIMER()
			WHILE dot < 0 
			AND (GET_GAME_TIMER() - p) < 100
				iClosestID++
				IF iClosestID >= MAX_DELIVERIES
					iClosestID = 0
				ENDIF
				
				vCurrentPos = GET_ENTITY_COORDS(mRoutedVehicle.id)
				dot = DOT_PRODUCT((mDeliveryPoint[iClosestID].vDriveTo- vCurrentPos), GET_ENTITY_FORWARD_VECTOR(mRoutedVehicle.id))
				p = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("FROM WHILE dot FROM CURRENT TO CLOSEST  ========== ", dot)#ENDIF
			ENDWHILE
		#ENDIF
		
		
		#IF USE_TU_CHANGES
			iTestID = iClosestID
			
			INT p = 0
			WHILE dot < 0 
			AND p < 40
				iTestID++
				IF iTestID >= MAX_DELIVERIES
					iTestID = 0
				ENDIF
				
				vCurrentPos = GET_ENTITY_COORDS(mRoutedVehicle.id)
				dot = DOT_PRODUCT((mDeliveryPoint[iTestID].vDriveTo- vCurrentPos), GET_ENTITY_FORWARD_VECTOR(mRoutedVehicle.id))
				p++
				#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("FROM WHILE dot FROM CURRENT TO CLOSEST  ========== ", dot)#ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Current test ID  ========== ", iTestID)#ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT_INT("TIME  ========== ", p)#ENDIF
			ENDWHILE

			IF p >= 40
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Current test ID  ========== ", iTestID)#ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Current test ID  ========== ", p)
				SCRIPT_ASSERT("COULDN't FIND DELIVERY ID.")#ENDIF
				iTestID = iNumDeliveries-1
			ENDIF
			
		#ENDIF
			
	ENDIF
	
	#IF USE_TU_CHANGES
		iClosestID = iTestID
	#ENDIF
	
	RETURN iClosestID
ENDFUNC


/// PURPOSE:
///    Removes a previous car gen blocking area
///    picks a new delivery point at randomm unless we are on the last point
///    sets up a new car gen blocking area at the new delivery point
PROC DO_DELIVERY_POINT_SETUP(BOOL FirstTimeActivate = FALSE)
	IF iDeliveryIndex != -1
		IF NOT IS_VECTOR_ZERO(mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre)
		AND NOT IS_VECTOR_ZERO(mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize)
		// Remove previous car gen blocking
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(
				mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre - mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize,
				mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre + mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize,
				TRUE)
		ENDIF
	ENDIF
	
	IF NOT FirstTimeActivate
		CHOOSE_NEXT_DELIVERY_POINT()
	ELSE
		iDeliveryIndex = GET_CLOSEST_DELIVERY_POINT()
	ENDIF
		
	IF iDeliveryIndex != -1
		#IF IS_DEBUG_BUILD SK_PRINT_INT("iDeliveryIndex ==== ", iDeliveryIndex) #ENDIF
		IF NOT IS_VECTOR_ZERO(mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre)
		AND NOT IS_VECTOR_ZERO(mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize)
		// Add vehicle gen blocking
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(
				mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre - mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize,
				mDeliveryPoint[iDeliveryIndex].vBlockingAreaCentre + mDeliveryPoint[iDeliveryIndex].vBlockingAreaSize,
				FALSE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Tells the ped to drive to a delivery point and park if mDeliveryPoint[iDeliveryIndex].bDoActionAtDeliveryPoint
///    is set
///    If all delivery points have been visited the ped is told to wander in the vehicle
///    can add so specific behaviour to this point 
PROC TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT(BOOL FirstTimeActivate = FALSE)
	IF NOT IS_VEHICLE_SEAT_FREE(mRoutedVehicle.id, VS_FRONT_RIGHT)
		DO_DELIVERY_POINT_SETUP(FirstTimeActivate)
		IF iDeliveryIndex <> iNumDeliveries-1
			IF iDeliveryIndex = 4
				TASK_VEHICLE_GOTO_NAVMESH(mPed[DRIVER].id, mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vDriveTo, 15, DRIVINGMODE_STOPFORCARS|DF_UseSwitchedOffNodes, mDeliveryPoint[iDeliveryIndex].fDriveToTargetDist)
//				TASK_VEHICLE_DRIVE_TO_COORD(mPed[DRIVER].id, mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vDriveTo, 15, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS|DF_UseSwitchedOffNodes, mDeliveryPoint[iDeliveryIndex].fDriveToTargetDist, mDeliveryPoint[iDeliveryIndex].fDriveToStraightDist)//B*1397821
			ELSE
				TASK_VEHICLE_DRIVE_TO_COORD(mPed[DRIVER].id, mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vDriveTo, 15, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_STOPFORCARS|DF_UseSwitchedOffNodes, mDeliveryPoint[iDeliveryIndex].fDriveToTargetDist, mDeliveryPoint[iDeliveryIndex].fDriveToStraightDist)		
			ENDIF
			
			#IF IS_DEBUG_BUILD SK_PRINT("TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT - eDeliveryPed = DPS_DRIVE_TO_DELIVERY_POINT") #ENDIF
			mPed[DRIVER].ePrevDeliverState = mPed[DRIVER].eDeliverState
			mPed[DRIVER].eDeliverState = DPDS_DRIVE_TO_DELIVERY_POINT
		ELSE
			TASK_VEHICLE_DRIVE_WANDER(mPed[DRIVER].id, mRoutedVehicle.id, 15, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
			mPed[DRIVER].ePrevDeliverState = mPed[DRIVER].eDeliverState
			mPed[DRIVER].eDeliverState = DPDS_WANDERING
		ENDIF
	ELSE
		mPed[DRIVER].ePrevDeliverState = mPed[DRIVER].eDeliverState
		mPed[DRIVER].eDeliverState = DPDS_WAIT_PASSENGER_DELIVER
	ENDIF
ENDPROC

PROC TELL_PED_TO_MAKE_DELIVERY(PED_STRUCT &ped)
	IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE)
		IF mDeliveryPoint[iDeliveryIndex].pedModel <> DUMMY_MODEL_FOR_SCRIPT
			IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
				TASK_GOTO_ENTITY_OFFSET( ped.id, mDeliveryPoint[iDeliveryIndex].oiObject, DEFAULT_TIME_NEVER_WARP, 0.0, 180.0, PEDMOVEBLENDRATIO_WALK)
	//			TASK_GO_TO_ENTITY(ped.id, mDeliveryPoint[iDeliveryIndex].oiObject, DEFAULT_TIME_NEVER_WARP, 0.1, PEDMOVEBLENDRATIO_WALK, 1)
	//			TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, mDeliveryPoint[iDeliveryIndex].vPedSpawn, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 2, ENAV_DEFAULT)
			ENDIF
		ELSE
			TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, mDeliveryPoint[iDeliveryIndex].vPedSpawn, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, -1, ENAV_DEFAULT, GET_HEADING_FROM_COORDS_TO_COORDS(mDeliveryPoint[iDeliveryIndex].vPedSpawn, GET_ENTITY_COORDS(ped.id)))
		ENDIF
		
		IF IS_VEHICLE_OK(mRoutedVehicle.id)
			SET_VEHICLE_DOOR_OPEN(mRoutedVehicle.id, SC_DOOR_BOOT)
		ENDIF
		
		ped.ePrevDeliverState = ped.eDeliverState
		ped.eDeliverState = DPDS_MAKE_DELIVERY
		ped.eDeliverSubState = SDS_WALKING_TO_BAG
	ENDIF
ENDPROC

/// PURPOSE:
///    Finds a road node 100m in front and 5m to the right of a vehicle 
///    and then looks for a point by the side of the road from that road node
///    also makes sure there isnt already a vehicle or an object in a 2m radius from the point
/// PARAMS:
///    vh - the vehicle to find the point from
/// RETURNS:
///    A position at the side of the road about 100m in front of the vehicle 
///    Will return vSafeVec if any of the checks failed
FUNC VECTOR FIND_PARKING_SPOT()
//		#IF IS_DEBUG_BUILD SAFE_REMOVE_BLIP(DebugBlip) #ENDIF
		VECTOR vTestParkPos = <<0,0,0>>
		
//		IF GET_CLOSEST_VEHICLE_NODE(vTestPos, vTestParkPos)
			IF GET_POSITION_BY_SIDE_OF_ROAD(mDeliveryPoint[iDeliveryIndex].vDriveTo, 0, vTestParkPos)
				IF NOT IS_ANY_VEHICLE_NEAR_POINT(vTestParkPos, 2)
				AND NOT IS_ANY_OBJECT_NEAR_POINT(vTestParkPos, 2)
//					#IF IS_DEBUG_BUILD DebugBlip = CREATE_BLIP_FOR_COORD(vTestParkPos) #ENDIF
					RETURN vTestParkPos
				ENDIF
			ENDIF
//		ENDIF
	
	RETURN <<0,0,0>>
	
ENDFUNC

PROC TELL_DRIVER_PED_PULLOVER(PED_STRUCT &ped)
	IF IS_VEHICLE_OK(mRoutedVehicle.id)
//		VECTOR vTempParkingPos = FIND_PARKING_SPOT()
//
//		IF ARE_VECTORS_EQUAL(vTempParkingPos, <<0,0,0>>)
//			vTempParkingPos = GET_ENTITY_COORDS(ped.id)
//		ENDIF	//GET_ENTITY_HEADING(mRoutedVehicle.id)

		SEQUENCE_INDEX Seq
		OPEN_SEQUENCE_TASK(Seq)
			TASK_VEHICLE_DRIVE_TO_COORD(NULL, mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vParkingVec, 8, DRIVINGSTYLE_ACCURATE,TRASH, DRIVINGMODE_AVOIDCARS, 10, 15)
			TASK_VEHICLE_PARK(NULL, mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vParkingVec, mDeliveryPoint[iDeliveryIndex].fParkHeading, mDeliveryPoint[iDeliveryIndex].eParkType, 5, TRUE)
		CLOSE_SEQUENCE_TASK(Seq)
		TASK_PERFORM_SEQUENCE(ped.id, Seq)
		CLEAR_SEQUENCE_TASK(Seq)

		#IF IS_DEBUG_BUILD SK_PRINT("ped.ePrevDeliverState ===== DPDS_DRIVE_TO_DELIVERY_POINT") #ENDIF
		ped.eDeliverState = DPDS_PULLINGOVER
	ENDIF
	
ENDPROC

/// PURPOSE:
///    
PROC ROUTE_POINT_MANAGER()
	INT i
	FOR i=0 TO(iNumDeliveries-1)
		IF mDeliveryPoint[i].pedModel <> DUMMY_MODEL_FOR_SCRIPT
			IF mDeliveryPoint[i].bSpawned
				//Manage the ped here
				IF i != iDeliveryIndex
				AND IS_ENTITY_ALIVE(mDeliveryPoint[i].oiObject)
				AND GET_DISTANCE_BETWEEN_ENTITIES(mDeliveryPoint[i].oiObject, PLAYER_PED_ID()) > 200
					DELETE_OBJECT(mDeliveryPoint[i].oiObject)	
					mDeliveryPoint[i].bSpawned = FALSE
				ENDIF
			ELSE
				IF mDeliveryPoint[i].pedModel <> DUMMY_MODEL_FOR_SCRIPT
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), mDeliveryPoint[i].vDriveTo, 100)
					OR IS_ENTITY_IN_RANGE_COORDS(mPed[DRIVER].id, mDeliveryPoint[i].vDriveTo, 120)
						mDeliveryPoint[i].bSpawned = SPAWN_OBJECT(mDeliveryPoint[i].oiObject, mDeliveryPoint[i].pedModel, mDeliveryPoint[i].vPedSpawn, mDeliveryPoint[i].fPedHeading)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

FUNC VECTOR GET_ENTRYPOINT_AREA_OFFSET(BOOL bDriver = FALSE)
	VECTOR vPos

	INT i 
	IF NOT bDriver
		i = GET_ENTITY_BONE_INDEX_BY_NAME(mRoutedVehicle.id, "wheel_rf")
	ELSE
		i = GET_ENTITY_BONE_INDEX_BY_NAME(mRoutedVehicle.id, "wheel_lf")
	ENDIF
	vPos = GET_WORLD_POSITION_OF_ENTITY_BONE(mRoutedVehicle.id, i)
	vPos = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mRoutedVehicle.id, vPos)
	
	IF NOT bDriver
		vPos += <<1, 0, 0.5>>
	ELSE
		vPos += <<-1, 0, 0.5>>
	ENDIF

	
	RETURN vPos
	
ENDFUNC


/// PURPOSE:
///    Tells the driver ped to enter the route vehicle
PROC ACTIVATE_DRIVER_PED()
	IF NOT IS_PED_IN_VEHICLE(mPed[DRIVER].id, mRoutedVehicle.id)
		TASK_FOLLOW_NAV_MESH_TO_COORD(mPed[DRIVER].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(TRUE)), 1, -1)
		mPed[DRIVER].eDeliverState = DPDS_WALKING_TO_VAN_DOOR
	ELSE
		TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT(TRUE)
	ENDIF
	
ENDPROC

PROC REACTIVATE_PED(PED_STRUCT &ped)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, HASH_ENEMIES, RELGROUPHASH_PLAYER)
	SWITCH ped.ePrevDeliverState
		CASE DPDS_DRIVE_TO_DELIVERY_POINT
			#IF IS_DEBUG_BUILD SK_PRINT("REACTIVATE_PED ped.ePrevDeliverState ===== DPDS_DRIVE_TO_DELIVERY_POINT") #ENDIF
			
		BREAK
		
		CASE DPDS_WANDERING
			#IF IS_DEBUG_BUILD SK_PRINT(" REACTIVATE_PED ped.ePrevDeliverState ===== DPDS_WANDERING") #ENDIF
		BREAK

		CASE DPDS_EXIT_VAN_DELIVER
			#IF IS_DEBUG_BUILD SK_PRINT("REACTIVATE_PED ped.ePrevDeliverState ===== DPDS_EXIT_VAN_DELIVER") #ENDIF
			ACTIVATE_DRIVER_PED()
		BREAK
		
		CASE DPDS_WAITING_IN_VAN
			#IF IS_DEBUG_BUILD SK_PRINT("REACTIVATE_PED ped.ePrevDeliverState ===== DPDS_WAITING_IN_VAN") #ENDIF
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PERFORM_SEQUENCE)
				IF iDeliveryIndex <= MAX_DELIVERIES-1
				AND iDeliveryIndex != -1
					mDeliveryPoint[iDeliveryIndex].bDelivered = TRUE
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
					#IF IS_DEBUG_BUILD SK_PRINT(" REACTIVATE_PED Tasking ped to walk vehicle") #ENDIF
					IF ped.eDeliverPedType = DRIVER
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(TRUE)), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET()), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ENDIF
				ENDIF
			ENDIF

		BREAK

		CASE DPDS_ENTER_VAN
			IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				IF ped.eDeliverPedType = DRIVER
					TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(TRUE)), 1, -1)
					ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
				ELSE
					TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET()), 1, -1)
					ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD SK_PRINT(" REACTIVATE_PED ped.ePrevDeliverState ===== DPDS_ENTER_VAN") #ENDIF
		BREAK
		
		CASE DPDS_MAKE_DELIVERY
			IF NOT mDeliveryPoint[iDeliveryIndex].bDelivered
				IF ped.eDeliverSubState = SDS_WALKING_TO_BAG
					TELL_PED_TO_MAKE_DELIVERY(ped)
				ELSE
					IF ped.eDeliverPedType = DRIVER
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(TRUE)), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET()), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
					IF ped.eDeliverPedType = DRIVER
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(TRUE)), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET()), 1, -1)
						ped.eDeliverState = DPDS_WALKING_TO_VAN_DOOR
					ENDIF
				ENDIF
			ENDIF
		BREAK

		DEFAULT
			
		BREAK

	ENDSWITCH
	#IF IS_DEBUG_BUILD SK_PRINT_INT("REACTIVATE_PED ped.ePrevDeliverState ===== DEFAULT ", ENUM_TO_INT(ped.ePrevDeliverState)) #ENDIF
	ped.iReactionTimer = GET_GAME_TIMER()
	ped.eControlState = DPCS_DELIVERING
ENDPROC

/// PURPOSE:
///     Checks to see if the mRoutedVehicle car has burst tyres
/// RETURNS:
///    TRUE if the mRoutedVehicle car has burst tyres.
FUNC BOOL HAS_PLAYER_BURST_ROUTECAR_TYRES()
	INT i
	INT iDamagedTyres = 0 
	
	FOR i = (ENUM_TO_INT(SC_WHEEL_CAR_FRONT_LEFT)) TO (ENUM_TO_INT(SC_WHEEL_CAR_REAR_RIGHT))
		IF IS_VEHICLE_TYRE_BURST(mRoutedVehicle.id, INT_TO_ENUM(SC_WHEEL_LIST, i))
			iDamagedTyres++
		ENDIF
	ENDFOR
	IF iDamagedTyres >= 3
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Monitors the hunters car for damage and plays hunter dialogue if appropriate.
FUNC BOOL MONITER_PLAYER_DAMAGING_ROUTE_CAR()
	IF IS_VEHICLE_OK(mRoutedVehicle.id)
	AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mRoutedVehicle.id, PLAYER_PED_ID())
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mRoutedVehicle.id)
			IF HAS_PLAYER_BURST_ROUTECAR_TYRES()
			OR GET_ENTITY_HEALTH(mRoutedVehicle.id) < 851
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC 

PROC TELL_PED_DROP_BIN_BAG(PED_STRUCT &ped)
	IF ped.eDeliverState = DPDS_MAKE_DELIVERY
		IF ped.eDeliverSubState >= SDS_PICK_UP_BIN_BAG
			IF NOT IS_PED_INJURED(ped.id)
				#IF IS_DEBUG_BUILD SK_PRINT("Playing break out bag drop anim") #ENDIF
				RESET_PED_MOVEMENT_CLIPSET(ped.id)
				RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
				TASK_PLAY_ANIM(ped.id, "missfbi4prepp1", "_bag_drop_garbage_man", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_SECONDARY|AF_UPPERBODY)
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC TELL_PED_RING_COPS(PED_STRUCT &ped)
	IF ped.eReactionState != DPRS_WAIT_RING_COPS
	AND ped.eReactionState !=  DPRS_RING_COPS
		TELL_PED_DROP_BIN_BAG(ped)
		ped.iReactionTimer = GET_GAME_TIMER()
		ped.eReactionState = DPRS_WAIT_RING_COPS
		ped.eControlState = DPCS_REACTING
		#IF IS_DEBUG_BUILD SK_PRINT("TELL_PED_RING_COPS()") #ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Driver is told to head over to either the spawned ped or
///    the point the ped would of been spawned at if DUMMY_MODEL_FOR_SCRIPT
///    was set as the peds model
PROC TELL_PASSENGER_PED_TO_MAKE_DELIVERY()
	TELL_PED_TO_MAKE_DELIVERY(mPed[PASSENGER])
	//EPREVPASSENGERPED = ePassengerPed
	#IF IS_DEBUG_BUILD SK_PRINT("TELL_PASSENGER_PED_TO_MAKE_DELIVERY - ePassengerPed = PPS_MAKE_DELIVERY") #ENDIF
ENDPROC


FUNC DELIVER_PED_TYPE GET_THE_OTHER_GUY(DELIVER_PED_TYPE type)
	IF type = DRIVER
		RETURN PASSENGER
	ENDIF
	
	RETURN DRIVER
ENDFUNC

PROC PICK_FLEE_REACTION(PED_STRUCT &ped, BOOL bForceFlee = FALSE)
	TELL_PED_DROP_BIN_BAG(ped)

	IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
	OR bForceFlee
	OR ped.eReactionState = DPRS_REACT_SEARCH
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), ped.id, 10)
			#IF IS_DEBUG_BUILD SK_PRINT("PICK_FLEE_REACTION = TGS_FLEE") #ENDIF
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_SMART_FLEE_PED)
				ped.eReactionState = DPRS_WAIT_REACT_TO_PLAYER_FLEE
				ped.eControlState = DPCS_REACTING
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		ELSE
			ped.eReactionState = DPRS_WAIT_REACT_TO_PLAYER_COWER
			ped.eControlState = DPCS_REACTING
			ped.iReactionTimer = GET_GAME_TIMER()
		ENDIF
	ELSE
		IF ped.eControlState <> DPCS_REACTING
			#IF IS_DEBUG_BUILD SK_PRINT("PICK_FLEE_REACTION = TGS_ATTACK") #ENDIF
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_COMBAT)
			AND NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_MELEE)
				ped.eControlState = DPCS_REACTING
				ped.eReactionState = DPRS_WAIT_REACT_BRAWL
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_BEHIND_UNARMED_STEALTH(PED_STRUCT &ped)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ped.id, <<0,-2,0>>), <<2,3,2>>)
	AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
	AND GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_BUMPED_PED(PED_INDEX ped)
	IF IS_ENTITY_IN_RANGE_ENTITY(ped, PLAYER_PED_ID(), 6)
		IF NOT IS_PED_IN_ANY_VEHICLE(ped)
			IF IS_ENTITY_TOUCHING_ENTITY(ped, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC START_SHAPETEST (SHAPE_TEST_STRUCT &lShapeTest, ENTITY_INDEX ExcludeEntityIndex)

	SWITCH lShapeTest.eShapeTestType

		CASE SHAPETEST_INVALID

		BREAK
		
		CASE SHAPETEST_LOSPROBE
			lShapeTest.ShapeTest = START_SHAPE_TEST_LOS_PROBE(lShapeTest.vStartPos, lShapeTest.vEndPos, SCRIPT_INCLUDE_ALL, ExcludeEntityIndex)
		BREAK
	
		// Since nobody was using async sphere tests we disabled them
		//CASE SHAPETEST_SPHERE
			//lShapeTest.ShapeTest = START_SHAPE_TEST_SPHERE(lShapeTest.vStartPos, lShapeTest.fRadius, SCRIPT_INCLUDE_ALL)
		//BREAK
		
		CASE SHAPETEST_BOUND
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOUND(lShapeTest.EntityIndex, SCRIPT_INCLUDE_ALL)
		BREAK
		
		CASE SHAPETEST_BOUNDINGBOX
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOUNDING_BOX(lShapeTest.EntityIndex, SCRIPT_INCLUDE_ALL)
		BREAK

		CASE SHAPETEST_BOX
			lShapeTest.ShapeTest = START_SHAPE_TEST_BOX(lShapeTest.vStartPos, lShapeTest.vSize, lShapeTest.vEulerRot, lShapeTest.eEulerOrder, SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED, ExcludeEntityIndex)
		BREAK
		
		CASE SHAPETEST_CAPSULE
			lShapeTest.ShapeTest = START_SHAPE_TEST_CAPSULE(lShapeTest.vStartPos, lShapeTest.vEndPos, lShapeTest.fRadius, SCRIPT_INCLUDE_ALL, ExcludeEntityIndex)
		BREAK
	
	ENDSWITCH
	
	lShapeTest.bRequested = (lShapeTest.ShapeTest <> NULL)
	
ENDPROC

//Keep renewing the shape test (call this every frame)
PROC UPDATE_PERSISTENT_SHAPETEST (SHAPE_TEST_STRUCT &lShapeTest, ENTITY_INDEX ExcludeEntityIndex)
	
	INT 		 iHit 		// int as bool
	
	SHAPETEST_STATUS Status = GET_SHAPE_TEST_RESULT(lShapeTest.ShapeTest, iHit, lShapeTest.vHitPos, lShapeTest.vHitNormal, lShapeTest.HitEntity)
	
	lShapeTest.bResultIsNew = (SHAPETEST_STATUS_RESULTS_READY = Status)
	
	//Convert the int as bool to a bool
	IF lShapeTest.bResultIsNew
		IF 0 = iHit
			lShapeTest.bIsHit = FALSE
		ELSE
			lShapeTest.bIsHit = TRUE
		ENDIF
	ENDIF
	
	//Try to keep all shape tests requested at all times
	IF Status <> SHAPETEST_STATUS_RESULTS_NOTREADY //Result was just returned or request was nonexistent
		START_SHAPETEST (lShapeTest, ExcludeEntityIndex)
	ENDIF

ENDPROC

PROC INIT_GOTO_BLOCKING_SHAPE_TEST(VECTOR vSize, VECTOR vEulerRot)
	mGotoBlockingShape.vSize = vSize
	mGotoBlockingShape.vEulerRot = vEulerRot
	mGotoBlockingShape.eShapeTestType = SHAPETEST_BOX
ENDPROC

FUNC BOOL IS_GOTO_BLOCKED(VECTOR vPos, ENTITY_INDEX ent, VECTOR vSize)
	mGotoBlockingShape.vStartPos = vPos
	mGotoBlockingShape.vSize = vSize
	UPDATE_PERSISTENT_SHAPETEST(mGotoBlockingShape, ent)
	#IF IS_DEBUG_BUILD
//		DRAW_DEBUG_SPHERE(vPos, vSize.x, 0, 0, 255, 150)
	#ENDIF
	IF mGotoBlockingShape.bIsHit
//		CPRINTLN(DEBUG_MISSION, "Shape test hit something")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_ENTITY_A_VEHICLE(mGotoBlockingShape.HitEntity)
				IF IS_ENTITY_ALIVE(mGotoBlockingShape.HitEntity)
					IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mGotoBlockingShape.HitEntity) = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
						CPRINTLN(DEBUG_MISSION, "Player in vehicle blocking go to")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_A_PED(mGotoBlockingShape.HitEntity)
				IF IS_ENTITY_ALIVE(mGotoBlockingShape.HitEntity)
					IF GET_PED_INDEX_FROM_ENTITY_INDEX(mGotoBlockingShape.HitEntity) = PLAYER_PED_ID()
						CPRINTLN(DEBUG_MISSION, "Player on foot blocking go to")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_PLAYER_BLOCKING_TRUCK_PROGRESSION(PED_STRUCT &ped)
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0, 10, -2>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0,3,2>>), 5)
	AND IS_PED_STOPPED(ped.id)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Has the player pointed a gun at the ped or has the player fired near the ped
PROC MONITER_PLAYER_THREATEN(PED_STRUCT &ped, DELIVER_PED_TYPE type, BOOL bForceMonitor = FALSE)
	IF ped.eControlState <> DPCS_REACTING 
	OR bForceMonitor
		IF NOT IS_PED_UNINJURED(mPed[GET_THE_OTHER_GUY(type)].id)
			IF NOT bCheckSeenPedDie
				IF DOES_ENTITY_EXIST(mPed[GET_THE_OTHER_GUY(type)].id)
					IF IS_PED_IN_PED_VIEW_CONE(ped.id, mPed[GET_THE_OTHER_GUY(type)].id, 140)
					OR IS_ENTITY_IN_RANGE_ENTITY(mPed[GET_THE_OTHER_GUY(type)].id, ped.id, 3)
						PICK_FLEE_REACTION(ped) 
						bCheckSeenPedDie = TRUE
						CPRINTLN(DEBUG_MISSION, "Seen other guy is dead react = ", type)
						EXIT
					ELSE
						vSearchPos = GET_ENTITY_COORDS(mPed[GET_THE_OTHER_GUY(type)].id)
						ped.iReactionTimer = GET_GAME_TIMER()
						ped.ePrevDeliverState = ped.eDeliverState
						ped.eControlState = DPCS_REACTING
						ped.eReactionState = DPRS_WAIT_REACT_SEARCH
						CPRINTLN(DEBUG_MISSION, "entering search = ", type)
					ENDIF
				ELSE
					PICK_FLEE_REACTION(ped)	
				ENDIF
			ENDIF
		ELSE
			IF mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_REACT_BRAWL
				IF IS_PED_IN_PED_VIEW_CONE(ped.id, mPed[GET_THE_OTHER_GUY(type)].id, 140)
				AND IS_ENTITY_IN_RANGE_ENTITY(mPed[GET_THE_OTHER_GUY(type)].id, PLAYER_PED_ID(), 15)
					ped.eControlState = DPCS_REACTING
					ped.eReactionState = DPRS_WAIT_REACT_BRAWL
					CPRINTLN(DEBUG_MISSION, "ped can see other ped brawling with player  = ", type)
				ENDIF
			ELIF mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_WAIT_REACT_TO_PLAYER_FLEE
			OR mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_REACT_TO_PLAYER_FLEE
				IF IS_ENTITY_IN_RANGE_ENTITY(mPed[GET_THE_OTHER_GUY(type)].id, ped.id, 10)
					PICK_FLEE_REACTION(ped)
				ENDIF
			ENDIF
		ENDIF

		IF HAS_PLAYER_BUMPED_PED(ped.id)
			TELL_PED_DROP_BIN_BAG(ped)
			CPRINTLN(DEBUG_MISSION, "React brawl ", type)

			ped.iReactionTimer = GET_GAME_TIMER()
			ped.ePrevDeliverState = ped.eDeliverState
			ped.eControlState = DPCS_REACTING
			ped.eReactionState = DPRS_WAIT_REACT_BRAWL
		ENDIF
		
		IF ped.eDeliverPedType = DRIVER
		AND ped.eDeliverState = DPDS_DRIVE_TO_DELIVERY_POINT
			IF IS_PLAYER_BLOCKING_TRUCK_PROGRESSION(ped)	
				IF IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
					TASK_DRIVE_BY(ped.id, PLAYER_PED_ID(), NULL, <<0,0,0>>, 10, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
					SET_HORN_PERMANENTLY_ON_TIME(mRoutedVehicle.id, 2000)
					
					iPlayerBlockingTruckTimer = GET_GAME_TIMER()
					ped.iReactionTimer = GET_GAME_TIMER()
					ped.eReactionState= DPRS_REACT_TO_PLAYER_BLOCKING_FRONT_OF_TRUCK
					ped.eControlState = DPCS_REACTING
				ENDIF
			ENDIF
		ENDIF
		IF HAS_PLAYER_THREATENED_PED(ped.id, TRUE, 70, 170, FALSE, TRUE)
		AND NOT IS_PLAYER_BEHIND_UNARMED_STEALTH(ped)
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
				IF IS_VEHICLE_OK(mRoutedVehicle.id)
					IF IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
						IF GET_ENTITY_SPEED(mRoutedVehicle.id) > 5
							IF type = DRIVER
								#IF IS_DEBUG_BUILD SK_PRINT("FLEE 1") #ENDIF
								TASK_VEHICLE_MISSION_PED_TARGET(ped.id, mRoutedVehicle.id, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, 300, 2)
								ped.eReactionState = DPRS_REACT_TO_PLAYER_FLEE
								TASK_COWER(mPed[GET_THE_OTHER_GUY(type)].id)
								mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_WAIT_REACT_TO_PLAYER_COWER
								mPed[GET_THE_OTHER_GUY(type)].eControlState = DPCS_REACTING
								ped.eControlState = DPCS_REACTING
								ped.iReactionTimer = GET_GAME_TIMER()
								mPed[GET_THE_OTHER_GUY(type)].iReactionTimer = GET_GAME_TIMER()
							ELSE
								#IF IS_DEBUG_BUILD SK_PRINT("FLEE 2") #ENDIF
								TASK_COWER(ped.id)
								ped.eReactionState = DPRS_REACT_TO_PLAYER_COWER
								TASK_VEHICLE_MISSION_PED_TARGET(mPed[GET_THE_OTHER_GUY(type)].id, mRoutedVehicle.id, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, 300, 2)
								mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_WAIT_REACT_TO_PLAYER_FLEE

								mPed[GET_THE_OTHER_GUY(type)].eControlState = DPCS_REACTING
								ped.eControlState = DPCS_REACTING
								ped.iReactionTimer = GET_GAME_TIMER()
								mPed[GET_THE_OTHER_GUY(type)].iReactionTimer = GET_GAME_TIMER()
							ENDIF
						ELSE

							PICK_FLEE_REACTION(ped)
							IF DOES_ENTITY_EXIST(mPed[GET_THE_OTHER_GUY(type)].id)
								IF IS_PED_IN_PED_VIEW_CONE(mPed[GET_THE_OTHER_GUY(type)].id, PLAYER_PED_ID())

									PICK_FLEE_REACTION(mPed[GET_THE_OTHER_GUY(type)])
									CPRINTLN(DEBUG_MISSION, "other guy can see the player threaten the = ", type)
								ENDIF
							ENDIF
						ENDIF
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped.id)
					ELSE

						PICK_FLEE_REACTION(ped)
						IF DOES_ENTITY_EXIST(mPed[GET_THE_OTHER_GUY(type)].id)
							IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(type)].id, ped.id)

								PICK_FLEE_REACTION(mPed[GET_THE_OTHER_GUY(type)])
								CPRINTLN(DEBUG_MISSION, "other ped can see the player threatening and not in a car = ", type)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
	
				ped.iReactionTimer = GET_GAME_TIMER()
				ped.ePrevDeliverState = ped.eDeliverState
				ped.eControlState = DPCS_REACTING
				ped.eReactionState = DPRS_WAIT_REACT_BRAWL
				CPRINTLN(DEBUG_MISSION, "ped threatened by player without gun = ", type)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped.id)
				IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(type)].id, ped.id)
					mPed[GET_THE_OTHER_GUY(type)].iReactionTimer = GET_GAME_TIMER()
					mPed[GET_THE_OTHER_GUY(type)].ePrevDeliverState = ped.eDeliverState
					mPed[GET_THE_OTHER_GUY(type)].eControlState = DPCS_REACTING
					mPed[GET_THE_OTHER_GUY(type)].eReactionState = DPRS_WAIT_REACT_BRAWL
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mPed[GET_THE_OTHER_GUY(type)].id)
					CPRINTLN(DEBUG_MISSION, "ped can see other ped being threatened without gun = ", type)
				ENDIF
			ENDIF
			ped.eControlState = DPCS_REACTING
		ELIF IS_PED_BEING_JACKED(ped.id)
			TELL_PED_DROP_BIN_BAG(ped)
			TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 500, -1, FALSE, TRUE)
			ped.eReactionState = DPRS_BEING_JACKED
			ped.eControlState = DPCS_REACTING
			ped.iReactionTimer = GET_GAME_TIMER()
			CPRINTLN(DEBUG_MISSION, "Ped is being jacked = ", type)
		ENDIF
		
		IF IS_PED_RAGDOLL(ped.id)
			IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
				IF IS_ENTITY_ATTACHED(mDeliveryPoint[iDeliveryIndex].oiObject)
					DETACH_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, TRUE, FALSE)
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(mDeliveryPoint[iDeliveryIndex].oiObject, APPLY_TYPE_IMPULSE, GET_ENTITY_FORWARD_VECTOR(ped.id),0, TRUE, TRUE)
					CPRINTLN(DEBUG_MISSION, "Detaching the bin bag = ", type)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ped.eControlState <> DPCS_REACTING
		IF IS_PED_UNINJURED(mPed[GET_THE_OTHER_GUY(type)].id)
			IF IS_PED_BEING_JACKED(mPed[GET_THE_OTHER_GUY(type)].id)
				TELL_PED_DROP_BIN_BAG(ped)
				CPRINTLN(DEBUG_MISSION, "other guy is being jacked = ", type)
				TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 120, -1)
//				TASK_LEAVE_ANY_VEHICLE(ped.id, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				ped.iReactionTimer = GET_GAME_TIMER()
				ped.eReactionState = DPRS_REACT_TO_PLAYER_FLEE
				ped.eControlState = DPCS_REACTING
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is in the 
///    truck while the other ped isn't in (normally the driver)
///    and if they start to drive off
PROC MONITOR_PLAYER_DRIVING_OFF_IN_VAN(PED_STRUCT &ped)
	IF IS_VEHICLE_OK(mRoutedVehicle.id)
	AND (IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id)
	AND NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id))
		IF GET_ENTITY_SPEED(mRoutedVehicle.id) > 0
			ped.iReactionTimer = GET_GAME_TIMER()
			ped.ePrevDeliverState = ped.eDeliverState
			ped.eControlState = DPCS_REACTING
			ped.eReactionState = DPRS_WAIT_REACT_BRAWL
			CPRINTLN(DEBUG_MISSION, "MONITOR_PLAYER_DRIVING_OFF_IN_VAN(), DPRS_WAIT_REACT_BRAWL")
		ELSE
			CPRINTLN(DEBUG_MISSION, "GET_ENTITY_SPEED(mRoutedVehicle.id)", GET_ENTITY_SPEED(mRoutedVehicle.id))
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id)
				CPRINTLN(DEBUG_MISSION, "IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id)")
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				CPRINTLN(DEBUG_MISSION, "NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)")
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the play damages the delivery van 
///    If they do the ped gets out and turns to the player and plays an angry gesture
///    if the damage is done while the player is armed and shooting then the ped flees
///    [TODO] Need to check for bullet/explosion damage
PROC MONITER_PLAYER_BUMPING_VAN(PED_STRUCT &ped)
	IF ped.eControlState <> DPCS_REACTING
		IF IS_VEHICLE_OK(mRoutedVehicle.id)
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_DRIVE_BY)
			AND IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mRoutedVehicle.id, 10)
					IF IS_ENTITY_TOUCHING_ENTITY(mRoutedVehicle.id, PLAYER_PED_ID())
//						#IF IS_DEBUG_BUILD SK_PRINT("MONITER_PLAYER_BUMPING_VAN - eDeliveryPed = DPS_REACT_TO_PLAYER_ANGER") #ENDIF
						
						TASK_DRIVE_BY(ped.id, PLAYER_PED_ID(), NULL, <<0,0,0>>, 10, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
						ped.iReactionTimer = GET_GAME_TIMER()
						ped.eReactionState= DPRS_REACT_TO_PLAYER_ANGER
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TELL_PED_REACT_PLAYER_BLOCKING_DOOR(PED_STRUCT &ped)
	#IF IS_DEBUG_BUILD SK_PRINT("ped.eReactionState ===== DPRS_REACT_TO_PLAYER_BLOCKING_DOOR") #ENDIF
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_ENEMIES, RELGROUPHASH_PLAYER)
	TELL_PED_DROP_BIN_BAG(ped)
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(ped.id)
			TASK_PUT_PED_DIRECTLY_INTO_MELEE(ped.id, PLAYER_PED_ID(), NORMAL_BLEND_IN, -1, 10)
		ELSE
			SEQUENCE_INDEX Seq 
			OPEN_SEQUENCE_TASK(Seq)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(Seq)
			TASK_PERFORM_SEQUENCE(ped.id, Seq)
			CLEAR_SEQUENCE_TASK(Seq)
		ENDIF
	ELSE
		SEQUENCE_INDEX Seq 
		OPEN_SEQUENCE_TASK(Seq)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
		CLOSE_SEQUENCE_TASK(Seq)
		TASK_PERFORM_SEQUENCE(ped.id, Seq)
		CLEAR_SEQUENCE_TASK(Seq)
	ENDIF
	RESET_PED_MOVEMENT_CLIPSET(ped.id)
	RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
	RESET_SHAPE_TEST()
	ped.iReactionTimer = GET_GAME_TIMER()
	ped.ePrevDeliverState = ped.eDeliverState
	ped.eControlState = DPCS_REACTING
	ped.eReactionState = DPRS_REACT_TO_PLAYER_BLOCKING_DOOR
ENDPROC

PROC TELL_PED_REACT_TO_PLAYER_BLOCKING_BINS(PED_STRUCT &ped)
	#IF IS_DEBUG_BUILD SK_PRINT("ped.eReactionState ===== DPRS_REACT_TO_PLAYER_BLOCKING_BINS") #ENDIF
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_ENEMIES, RELGROUPHASH_PLAYER)
	TELL_PED_DROP_BIN_BAG(ped)

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(ped.id)
			TASK_PUT_PED_DIRECTLY_INTO_MELEE(ped.id, PLAYER_PED_ID(), NORMAL_BLEND_IN, -1, 10)
		ELSE
			SEQUENCE_INDEX Seq 
			OPEN_SEQUENCE_TASK(Seq)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(Seq)
			TASK_PERFORM_SEQUENCE(ped.id, Seq)
			CLEAR_SEQUENCE_TASK(Seq)
		ENDIF
	ELSE
		SEQUENCE_INDEX Seq 
		OPEN_SEQUENCE_TASK(Seq)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
		CLOSE_SEQUENCE_TASK(Seq)
		TASK_PERFORM_SEQUENCE(ped.id, Seq)
		CLEAR_SEQUENCE_TASK(Seq)
	ENDIF
	RESET_PED_MOVEMENT_CLIPSET(ped.id)
	RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
	RESET_SHAPE_TEST()
	IF NOT DOES_BLIP_EXIST(ped.blip)
		ped.blip = CREATE_BLIP_FOR_PED(ped.id, TRUE)
	ENDIF
	ped.iReactionTimer = GET_GAME_TIMER()
	ped.ePrevDeliverState = ped.eDeliverState
	ped.eControlState = DPCS_REACTING
	ped.eReactionState = DPRS_REACT_TO_PLAYER_BLOCKING_BINS
ENDPROC

PROC TELL_PED_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK(PED_STRUCT &ped)
	#IF IS_DEBUG_BUILD SK_PRINT("ped.eReactionState ===== DPRS_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK") #ENDIF
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, HASH_ENEMIES, RELGROUPHASH_PLAYER)
	TELL_PED_DROP_BIN_BAG(ped)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(ped.id)
			TASK_PUT_PED_DIRECTLY_INTO_MELEE(ped.id, PLAYER_PED_ID(), NORMAL_BLEND_IN, -1, 10)
		ELSE
			SEQUENCE_INDEX Seq 
			OPEN_SEQUENCE_TASK(Seq)
				TASK_LEAVE_ANY_VEHICLE(NULL)
				TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
			CLOSE_SEQUENCE_TASK(Seq)
			TASK_PERFORM_SEQUENCE(ped.id, Seq)
			CLEAR_SEQUENCE_TASK(Seq)
		ENDIF
	ELSE
		SEQUENCE_INDEX Seq 
		OPEN_SEQUENCE_TASK(Seq)
			TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
			TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
		CLOSE_SEQUENCE_TASK(Seq)
		TASK_PERFORM_SEQUENCE(ped.id, Seq)
		CLEAR_SEQUENCE_TASK(Seq)
	ENDIF
	RESET_PED_MOVEMENT_CLIPSET(ped.id)
	RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
	RESET_SHAPE_TEST()
	IF NOT DOES_BLIP_EXIST(ped.blip)
		ped.blip = CREATE_BLIP_FOR_PED(ped.id, TRUE)
	ENDIF
	ped.iReactionTimer = GET_GAME_TIMER()
	ped.ePrevDeliverState = ped.eDeliverState
	ped.eControlState = DPCS_REACTING
	ped.eReactionState = DPRS_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK
ENDPROC

PROC OPEN_TRUCK_BOOT()
	IF fTruckBootOpenRatio != 1.0
		IF IS_VEHICLE_OK(mRoutedVehicle.id)
				SET_VEHICLE_DOOR_CONTROL(mRoutedVehicle.id, SC_DOOR_BOOT, DT_DOOR_INTACT, fTruckBootOpenRatio)
				fTruckBootOpenRatio = fTruckBootOpenRatio +@ 0.1

//				#IF IS_DEBUG_BUILD
//					SK_PRINT_FLOAT("OPEN_TRUCK_BOOT = ", fTruckBootOpenRatio)
//				#ENDIF

			IF fTruckBootOpenRatio >= 1.0
				SET_VEHICLE_DOOR_OPEN(mRoutedVehicle.id, SC_DOOR_BOOT)
				fTruckBootOpenRatio = 1.0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLOSE_TRUCK_BOOT()
	IF fTruckBootOpenRatio != 0.0
		SET_VEHICLE_DOOR_CONTROL(mRoutedVehicle.id, SC_DOOR_BOOT, DT_DOOR_INTACT, fTruckBootOpenRatio)
		fTruckBootOpenRatio = fTruckBootOpenRatio -@ 0.1

//		#IF IS_DEBUG_BUILD
////			SK_PRINT_FLOAT("CLOSE_TRUCK_BOOT = ", fTruckBootOpenRatio)
//		#ENDIF

		IF fTruckBootOpenRatio <= 0.0
			SET_VEHICLE_DOOR_SHUT(mRoutedVehicle.id, SC_DOOR_BOOT, FALSE)
			fTruckBootOpenRatio = 0.0
		ENDIF
	ENDIF
ENDPROC

PROC DELIVERY_SUBSTATES(PED_STRUCT &ped)

	IF ped.eDeliverSubState >= SDS_WALKING_TO_BAG
	AND ped.eDeliverSubState != SDS_NULL
//		OPEN_TRUCK_BOOT()
	ENDIF

	SWITCH ped.eDeliverSubState
		CASE SDS_WALKING_TO_BAG
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
				SEQUENCE_INDEX Seq
				OPEN_SEQUENCE_TASK(Seq)
					IF GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(ped.id), mDeliveryPoint[iDeliveryIndex].vPedSpawn) > 1.5
						TASK_TURN_PED_TO_FACE_COORD(NULL, mDeliveryPoint[iDeliveryIndex].vPedSpawn)
					ENDIF
					TASK_PLAY_ANIM(NULL, "missfbi4prepp1", "_bag_pickup_garbage_man", NORMAL_BLEND_IN, FAST_BLEND_OUT, -1, AF_USE_MOVER_EXTRACTION|AF_EXTRACT_INITIAL_OFFSET)
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(ped.id, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				#IF IS_DEBUG_BUILD SK_PRINT("Finished getting out of vehicle picking up bin bag") #ENDIF
				ped.eDeliverSubState = SDS_PICK_UP_BIN_BAG
			ELSE
				IF IS_GOTO_BLOCKED(mDeliveryPoint[iDeliveryIndex].vPedSpawn, ped.id, <<1,1,8>>)
					TELL_PED_REACT_TO_PLAYER_BLOCKING_BINS(ped)
					EXIT
				ENDIF
			ENDIF
			SET_PED_CAPSULE(ped.id, 0.2)
		BREAK
	
		CASE SDS_PICK_UP_BIN_BAG
			IF IsPedPerformingTask(ped.id, SCRIPT_TASK_PERFORM_SEQUENCE)

				IF IS_ENTITY_PLAYING_ANIM(ped.id, "missfbi4prepp1", "_bag_pickup_garbage_man")
					IF GET_ENTITY_ANIM_CURRENT_TIME(ped.id, "missfbi4prepp1", "_bag_pickup_garbage_man") >= 0.333
						SET_PED_MOVEMENT_CLIPSET(ped.id, "missfbi4prepp1_garbageman", INSTANT_BLEND_DURATION)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id, "missfbi4prepp1_garbageman")
						IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
						AND NOT IS_ENTITY_ATTACHED(mDeliveryPoint[iDeliveryIndex].oiObject)
							#IF IS_DEBUG_BUILD SK_PRINT("Attaching bin bag to Right hand") #ENDIF
    						ATTACH_ENTITY_TO_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, ped.id, GET_PED_BONE_INDEX(ped.id, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF IS_GOTO_BLOCKED(mDeliveryPoint[iDeliveryIndex].vPedSpawn, ped.id, <<1,1,8>>)
					TELL_PED_REACT_TO_PLAYER_BLOCKING_BINS(ped)
					EXIT
				ENDIF
			ELSE
				IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					#IF IS_DEBUG_BUILD SK_PRINT("bag picked up changing substate to walk to back of truck") #ENDIF
					IF IS_VEHICLE_OK(mRoutedVehicle.id)
//						TASK_PLAY_ANIM(ped.id, "missfbi4prepp1", "_bag_walk_garbage_man", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_LOOPING)
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0,-4.8,0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, GET_ENTITY_HEADING(mRoutedVehicle.id))
					ENDIF
					
					ped.eDeliverSubState = SDS_WALK_TO_BACK_OF_TRUCK
				ENDIF
			ENDIF
			SET_PED_CAPSULE(ped.id, 0.2)

		BREAK

		CASE SDS_WALK_TO_BACK_OF_TRUCK
			IF IS_VEHICLE_OK(mRoutedVehicle.id)
				IF NOT IS_GOTO_BLOCKED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0, -5.5, 0>>), mRoutedVehicle.id, <<1.9,1.9,8>>)
					IF IS_ENTITY_AT_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0, -5.5, 0>>), <<0.9,0.9,2>>)
						IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							#IF IS_DEBUG_BUILD SK_PRINT("Reached back of truck start throw bag seq") #ENDIF
	//						CLEAR_PED_SECONDARY_TASK(ped.id)
							SEQUENCE_INDEX Seq
							OPEN_SEQUENCE_TASK(Seq)
								IF GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(ped.id), GET_ENTITY_COORDS(mRoutedVehicle.id)) > 3.0
									TASK_ACHIEVE_HEADING(NULL, GET_ENTITY_HEADING(mRoutedVehicle.id))
								ENDIF
								TASK_PLAY_ANIM(NULL, "missfbi4prepp1", "_bag_throw_garbage_man")
							CLOSE_SEQUENCE_TASK(Seq)
							TASK_PERFORM_SEQUENCE(ped.id, Seq)
							CLEAR_SEQUENCE_TASK(Seq)
							#IF IS_DEBUG_BUILD SK_PRINT("Clearing alt walk anim override") #ENDIF
							ped.eDeliverSubState = SDS_THROW_BAG_IN_TO_TRUCK
						ENDIF
					ELSE
						IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)	
							TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0,-4.8,0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, GET_ENTITY_HEADING(mRoutedVehicle.id))
						ENDIF
					ENDIF
				ELSE
					TELL_PED_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK(ped)
				ENDIF
			ENDIF
		BREAK

		CASE SDS_THROW_BAG_IN_TO_TRUCK
			IF NOT IS_GOTO_BLOCKED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<0, -5.5, 0>>), ped.id, <<1.9,1.9,8>>)
				IF IsPedPerformingTask(ped.id, SCRIPT_TASK_PERFORM_SEQUENCE)
					IF IS_ENTITY_PLAYING_ANIM(ped.id, "missfbi4prepp1", "_bag_throw_garbage_man")
						IF GET_ENTITY_ANIM_CURRENT_TIME(ped.id, "missfbi4prepp1", "_bag_throw_garbage_man") >= 0.391
							IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
								IF IS_ENTITY_ATTACHED(mDeliveryPoint[iDeliveryIndex].oiObject)
									RESET_PED_MOVEMENT_CLIPSET(ped.id)
									RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
									#IF IS_DEBUG_BUILD SK_PRINT("Detaching bin bag") #ENDIF
									DETACH_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, TRUE, FALSE)
									SET_OBJECT_PHYSICS_PARAMS(mDeliveryPoint[iDeliveryIndex].oiObject, 2, -1, <<-1,-1,-1>>, <<-1,-1,-1>>)
									APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(mDeliveryPoint[iDeliveryIndex].oiObject, APPLY_TYPE_IMPULSE, GET_ENTITY_FORWARD_VECTOR(ped.id),0, TRUE, TRUE)
								ENDIF
								IF IS_ENTITY_TOUCHING_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, mRoutedVehicle.id)
									SAFE_DELETE_OBJECT(mDeliveryPoint[iDeliveryIndex].oiObject)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT("Reactivating ped") #ENDIF
					ped.eDeliverSubState = SDS_NULL
					IF IS_VEHICLE_OK(mRoutedVehicle.id)
						SET_VEHICLE_DOOR_SHUT(mRoutedVehicle.id, SC_DOOR_BOOT, FALSE)
						SAFE_DELETE_OBJECT(mDeliveryPoint[iDeliveryIndex].oiObject)
					ENDIF

					REACTIVATE_PED(ped)
				ENDIF
			ELSE
				TELL_PED_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK(ped)
			ENDIF
		BREAK
		
		CASE SDS_DROP_BAG
			
		BREAK
	ENDSWITCH

ENDPROC

PROC MONITOR_DRIVER_DOOR_BLOCKED(PED_STRUCT &ped)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX Veh
		Veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(Veh)
			IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped.id, Veh, VS_DRIVER)
				ped.eReactionState = DPRS_WAIT_FLIM_BLOCKING
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC WAIT_PED_AT_VAN_DOOR(PED_STRUCT &ped, BOOL bDriver = FALSE)
	IF IS_GOTO_BLOCKED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(bDriver)), mRoutedVehicle.id, <<1.9,1.9,8>>)
		IF IS_ENTITY_IN_ANGLED_AREA(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<5, 0, -2>>), (GET_ENTITY_COORDS(mRoutedVehicle.id)+ <<0,0,2>>), 9)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<5, 0, -2>>), (GET_ENTITY_COORDS(mRoutedVehicle.id)+ <<0,0,2>>), 9)
			TELL_PED_REACT_PLAYER_BLOCKING_DOOR(ped)
		ELSE
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_GO_TO_ENTITY)
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<5, 0, -2>>), (GET_ENTITY_COORDS(mRoutedVehicle.id)+ <<0,0,2>>), 9)
				CLEAR_PED_TASKS(ped.id)
				TASK_GO_TO_ENTITY(ped.id, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 4, PEDMOVEBLENDRATIO_WALK)
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_IN_RANGE_COORDS(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, GET_ENTRYPOINT_AREA_OFFSET(bDriver)), 5)
			#IF IS_DEBUG_BUILD SK_PRINT(" WAIT_PED_AT_VAN_DOOR Tasking ped to enter vehicle") #ENDIF
			VEHICLE_SEAT v = VS_DRIVER
			IF NOT bDriver
				v = VS_FRONT_RIGHT
			ENDIF

			TASK_ENTER_VEHICLE(ped.id, mRoutedVehicle.id, -1, v, PEDMOVEBLENDRATIO_WALK, ECF_DONT_JACK_ANYONE|ECF_RESUME_IF_INTERRUPTED)
			//EPREVPASSENGERPED = ePassengerPed
			ped.eDeliverState = DPDS_ENTER_VAN
		ENDIF
	ENDIF
	
ENDPROC

PROC DO_DELIVERIES(PED_STRUCT &ped)
	IF ped.eDeliverSubState = SDS_NULL
	AND ped.eDeliverPedType = PASSENGER
//		CLOSE_TRUCK_BOOT()
	ENDIF

	SWITCH ped.eDeliverState

		CASE DPDS_WALKING_TO_VAN_DOOR
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id, TRUE)

				PICK_FLEE_REACTION(ped)
				#IF IS_DEBUG_BUILD SK_PRINT("Player is in vehicle Picked flee reaction") #ENDIF
				EXIT
			ENDIF

			IF ped.eDeliverPedType = DRIVER
				WAIT_PED_AT_VAN_DOOR(ped, TRUE)
			ELSE
				WAIT_PED_AT_VAN_DOOR(ped, FALSE)
			ENDIF			
		BREAK

		CASE DPDS_ENTER_VAN
			IF IS_PED_SITTING_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_ENTER_VEHICLE)
					IF ped.eDeliverPedType = DRIVER
						TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT()
					ELSE
						ped.eDeliverState = DPDS_WAITING_IN_VAN
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id, TRUE)

					PICK_FLEE_REACTION(ped)
					#IF IS_DEBUG_BUILD SK_PRINT("Player is in vehicle Picked flee reaction") #ENDIF
					EXIT
				ENDIF
			ENDIF
		BREAK

		CASE DPDS_WAITING_IN_VAN
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mRoutedVehicle.id, TRUE)

				PICK_FLEE_REACTION(ped)
				#IF IS_DEBUG_BUILD SK_PRINT("Player is in vehicle Picked flee reaction") #ENDIF
				EXIT
			ENDIF

			IF bPassengerDoDeliveries
				IF iDeliveryIndex <= MAX_DELIVERIES-1
				AND iDeliveryIndex != -1
					IF NOT mDeliveryPoint[iDeliveryIndex].bDelivered
						IF IS_ENTITY_IN_RANGE_COORDS(ped.id, mDeliveryPoint[iDeliveryIndex].vDriveTo, 20)
							#IF IS_DEBUG_BUILD SK_PRINT("ped.ePrevDeliverState ===== DPDS_WAITING_IN_VAN") #ENDIF
							IF mDeliveryPoint[iDeliveryIndex].bDoActionAtDeliveryPoint
							AND	IS_VEHICLE_STOPPED(mRoutedVehicle.id)
								TELL_PASSENGER_PED_TO_MAKE_DELIVERY()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					ped.eDeliverState = DPDS_WANDERING
				ENDIF
			ENDIF
		BREAK
		
		CASE DPDS_DRIVE_TO_DELIVERY_POINT
			#IF IS_DEBUG_BUILD
				IF bShowDeliveryDebugText
//					DRAW_DEBUG_SPHERE(mDeliveryPoint[iDeliveryIndex].vDriveTo, 25, 0, 255, 0, 100)
				ENDIF
			#ENDIF
			IF IS_ENTITY_IN_RANGE_COORDS(ped.id, mDeliveryPoint[iDeliveryIndex].vDriveTo, 25)
				#IF IS_DEBUG_BUILD SK_PRINT("ped.ePrevDeliverState ===== DPDS_DRIVE_TO_DELIVERY_POINT") #ENDIF
				IF mDeliveryPoint[iDeliveryIndex].bDoActionAtDeliveryPoint
					TELL_DRIVER_PED_PULLOVER(ped)
//					ped.eDeliverState = DPDS_BREAKING
				ELSE
					mDeliveryPoint[iDeliveryIndex].bDelivered = TRUE
					TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT()
				ENDIF
			ENDIF
		BREAK
		
		CASE DPDS_BREAKING
			IF IS_VEHICLE_OK(mRoutedVehicle.id)
				BRING_VEHICLE_TO_HALT(mRoutedVehicle.id, 25, 10)
				FLOAT fSpeed
				fSpeed = GET_ENTITY_SPEED(mRoutedVehicle.id)
				#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("fSpeed = = = = = ", fSpeed) #ENDIF
				IF fSpeed < 8
					#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("fSpeed < 8 --- fSpeed = = = = = ", fSpeed) #ENDIF
					TELL_DRIVER_PED_PULLOVER(ped)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE DPDS_PULLINGOVER
			#IF IS_DEBUG_BUILD
				IF bShowDeliveryDebugText
//					DRAW_DEBUG_SPHERE(mDeliveryPoint[iDeliveryIndex].vParkingVec, 5, 255, 0, 0, 100)
				ENDIF
			#ENDIF

			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PERFORM_SEQUENCE)
			AND IS_VEHICLE_STOPPED(mRoutedVehicle.id)
//			AND IS_ENTITY_IN_RANGE_COORDS(mRoutedVehicle.id, mDeliveryPoint[iDeliveryIndex].vParkingVec, 5)
//					#IF IS_DEBUG_BUILD SAFE_REMOVE_BLIP(DebugBlip) #ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT("ped.ePrevDeliverState ===== DPDS_PULLINGOVER") #ENDIF
				IF bPassengerDoDeliveries
					ped.ePrevDeliverState = ped.eDeliverState
					ped.eDeliverState = DPDS_WAIT_PASSENGER_DELIVER
				ELSE
					TASK_LEAVE_ANY_VEHICLE(ped.id)
					ped.ePrevDeliverState = ped.eDeliverState
					ped.eDeliverState = DPDS_EXIT_VAN_DELIVER
				ENDIF
			ENDIF
		BREAK
		
		CASE DPDS_WAIT_PASSENGER_DELIVER
			IF IS_PED_UNINJURED(mPed[PASSENGER].id)
				IF IS_PED_IN_VEHICLE(mPed[PASSENGER].id, mRoutedVehicle.id)
				AND mPed[PASSENGER].eDeliverState = DPDS_WAITING_IN_VAN
					#IF IS_DEBUG_BUILD SK_PRINT("ped.ePrevDeliverState ===== DPDS_EXIT_VAN_DELIVER") #ENDIF
					TELL_DRIVER_PED_DRIVE_TO_DELIVERY_POINT()
				ENDIF
			ENDIF
		BREAK
		
		CASE DPDS_EXIT_VAN_DELIVER
			IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				TELL_PED_TO_MAKE_DELIVERY(ped)
			ENDIF
			
		BREAK
		
		CASE DPDS_MAKE_DELIVERY
			DELIVERY_SUBSTATES(ped)
			
//			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PERFORM_SEQUENCE)
////				mDeliveryPoint[iDeliveryIndex].bDelivered = TRUE
////				REACTIVATE_PED(ped)
//			ELSE
//				IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iReactionDelay
//					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), mDeliveryPoint[iDeliveryIndex].vPedSpawn, <<2,2,2>>)
//						TELL_PED_REACT_TO_PLAYER_BLOCKING_BINS(ped)
//					ENDIF
//				ENDIF
//			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC DO_REACTIONS(PED_STRUCT &ped)
	SWITCH ped.eReactionState
		CASE DPRS_WAIT_REACT_TO_PLAYER_FLEE
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iQuickReactionDelay
				IF IsPedPerformingTask(ped.id, SCRIPT_TASK_COMBAT)
					CLEAR_PED_TASKS(ped.id)
				ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT("Setting state to initial flee state") #ENDIF
				TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 30, -1, FALSE)
				ped.eReactionState = DPRS_REACT_TO_PLAYER_INITAL_FLEE
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		
		CASE DPRS_WAIT_REACT_BRAWL
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				IF CAN_PED_SEE_PED(ped.id, PLAYER_PED_ID())

					PICK_FLEE_REACTION(ped, TRUE)
					#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 1") #ENDIF
					EXIT
				ENDIF
			ENDIF
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iQuickReactionDelay
				IF NOT IS_PED_IN_ANY_VEHICLE(ped.id)
					TASK_PUT_PED_DIRECTLY_INTO_MELEE(ped.id, PLAYER_PED_ID(), NORMAL_BLEND_IN, -1, 10)
				ELSE
					SEQUENCE_INDEX Seq 
					OPEN_SEQUENCE_TASK(Seq)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(Seq)
					TASK_PERFORM_SEQUENCE(ped.id, Seq)
					CLEAR_SEQUENCE_TASK(Seq)
				ENDIF
				IF NOT DOES_BLIP_EXIST(ped.blip)
					ped.blip = CREATE_BLIP_FOR_PED(ped.id, TRUE)
				ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT("setting combat task") #ENDIF
				ped.eReactionState = DPRS_REACT_BRAWL
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		CASE DPRS_WAIT_REACT_TO_PLAYER_COWER
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iQuickReactionDelay
				IF IS_PED_IN_ANY_VEHICLE(ped.id)
					SET_PED_RESET_FLAG(ped.id, PRF_PanicInVehicle, TRUE)
					ped.eReactionState = DPRS_REACT_TO_PLAYER_COWER
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT("PICK_FLEE_REACTION = TGS_COWER") #ENDIF
					IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_COWER)
						IF IsPedPerformingTask(ped.id, SCRIPT_TASK_COMBAT)
							CLEAR_PED_TASKS(ped.id)
						ENDIF

						TASK_COWER(ped.id)
						ped.eReactionState = DPRS_REACT_TO_PLAYER_COWER
						ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_WAIT_RING_COPS
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iQuickReactionDelay
				TASK_USE_MOBILE_PHONE(ped.id, TRUE)
				TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 500, -1, FALSE, TRUE)
				CPRINTLN(DEBUG_MISSION, "ped Will now ring cops = ", ped.eDeliverPedType)
				ped.iReactionTimer = GET_GAME_TIMER()
				ped.eReactionState = DPRS_RING_COPS
				ped.eControlState = DPCS_REACTING
			ENDIF
		BREAK
		
		CASE DPRS_WAIT_REACT_SEARCH
			MONITOR_PLAYER_DRIVING_OFF_IN_VAN(ped)
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > 5000
				IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					IF DOES_ENTITY_EXIST(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id)
						TASK_FOLLOW_NAV_MESH_TO_COORD(ped.id, vSearchPos, PEDMOVEBLENDRATIO_WALK,DEFAULT_TIME_NEVER_WARP, 2)
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_MISSION, "ped Will search for friend = ", ped.eDeliverPedType)
				ped.iReactionTimer = GET_GAME_TIMER()
				ped.eReactionState = DPRS_REACT_SEARCH
				ped.eControlState = DPCS_REACTING
			ENDIF
		BREAK
		
		CASE DPRS_WAIT_FLIM_BLOCKING
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PLAY_ANIM)
				CPRINTLN(DEBUG_MISSION, "ped Will now film player = ", ped.eDeliverPedType)
				SEQUENCE_INDEX Seq
				OPEN_SEQUENCE_TASK(Seq)
					IF GET_HEADING_BETWEEN_VECTORS(GET_ENTITY_COORDS(ped.id), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 1.5
						TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF
					TASK_START_SCENARIO_IN_PLACE( NULL, "WORLD_HUMAN_MOBILE_FILM_SHOCKING", 6000, TRUE )
				CLOSE_SEQUENCE_TASK(Seq)
				TASK_PERFORM_SEQUENCE(ped.id, Seq)
				CLEAR_SEQUENCE_TASK(Seq)
				ped.eControlState = DPCS_REACTING
				ped.eReactionState = DPRS_REACT_FLIM
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
		CASE DPRS_WAIT_FLIM_DEAD
			MONITOR_PLAYER_DRIVING_OFF_IN_VAN(ped)
			IF NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_PLAY_ANIM)
				CPRINTLN(DEBUG_MISSION, "ped Will now film dead ped = ", ped.eDeliverPedType)
				TASK_START_SCENARIO_IN_PLACE( ped.id, "WORLD_HUMAN_MOBILE_FILM_SHOCKING", 6000, TRUE )
				ped.eControlState = DPCS_REACTING
				ped.eReactionState = DPRS_REACT_FLIM
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
		BREAK

		CASE DPRS_EXIT_VAN_FLEE
			IF NOT IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id)
				TELL_PED_RING_COPS(ped)
			ENDIF
		BREAK
		
		CASE DPRS_BEING_JACKED
			IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(ped.id)
			AND (GET_GAME_TIMER() - ped.iReactionTimer) > 3000
//				AND NOT GET_IS_TASK_ACTIVE(mPed[DRIVER].id, CODE_TASK_EXIT_VEHICLE)
				TELL_PED_RING_COPS(ped)
			ENDIF
		BREAK
		 
		CASE DPRS_RING_COPS
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > 8000
				TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 500, -1)
				IF NOT bWantedLevelSet
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						bWantedLevelSet = TRUE
						CPRINTLN(DEBUG_MISSION, "Giving player 2 star wanted script = ", GET_THIS_SCRIPT_NAME())
					ENDIF
				ENDIF
				TASK_USE_MOBILE_PHONE(ped.id, FALSE)
				ped.iReactionTimer = GET_GAME_TIMER()
				ped.eReactionState = DPRS_FLEE_AFTER_RINGING_COPS
			ELSE
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 1
					TASK_USE_MOBILE_PHONE(ped.id, FALSE)
					ped.iReactionTimer = GET_GAME_TIMER()
					ped.eReactionState = DPRS_FLEE_AFTER_RINGING_COPS
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_REACT_TO_PLAYER_INITAL_FLEE
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iReactionDelay
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					#IF IS_DEBUG_BUILD SK_PRINT("ped out of range set to normal flee state") #ENDIF
					SAFE_REMOVE_BLIP(ped.blip)
					ped.eReactionState = DPRS_REACT_TO_PLAYER_FLEE
					ped.iReactionTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_REACT_TO_PLAYER_FLEE
			IF IS_PED_IN_VEHICLE(ped.id, mRoutedVehicle.id, TRUE)
				IF ped.eReactionState <> DPRS_RING_COPS
				AND ped.eReactionState <> DPRS_EXIT_VAN_FLEE
					IF MONITER_PLAYER_DAMAGING_ROUTE_CAR()
						IF IS_PED_UNINJURED(ped.id)
							CLEAR_PED_TASKS(ped.id)
							TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 500, -1, FALSE, TRUE)
							ped.eReactionState = DPRS_EXIT_VAN_FLEE
						ENDIF
					ENDIF
				ENDIF

				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 120)
					CPRINTLN(DEBUG_MISSION, "Released one of the delivery peds = ", ped.eDeliverPedType)
					SAFE_RELEASE_PED(ped.id)
				ENDIF

			ELSE
				TELL_PED_RING_COPS(ped)
			ENDIF
		BREAK
		
		CASE DPRS_FLEE_AFTER_RINGING_COPS
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 120)
				CPRINTLN(DEBUG_MISSION, "Released one of the delivery peds = ", ped.eDeliverPedType)		
				SAFE_RELEASE_PED(ped.id)
			ENDIF
		BREAK
		
		CASE DPRS_REACT_TO_PLAYER_ANGER
			IF IS_PED_BEING_JACKED(ped.id)

				PICK_FLEE_REACTION(ped, TRUE)	
				EXIT
			ENDIF
			
			IF (GET_GAME_TIMER() - ped.iReactionTimer) > ped.iReactionDelay
			OR NOT IsPedPerformingTask(ped.id, SCRIPT_TASK_DRIVE_BY)

				REACTIVATE_PED(ped)
			ENDIF
		BREAK
		
		CASE DPRS_REACT_TO_PLAYER_COWER
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay
				TASK_SMART_FLEE_PED(ped.id, PLAYER_PED_ID(), 500, -1, FALSE, TRUE)
				ped.eReactionState = DPRS_WAIT_REACT_TO_PLAYER_FLEE
			ELSE
				SET_PED_RESET_FLAG(ped.id, PRF_PanicInVehicle, TRUE)
			ENDIF
		BREAK

		CASE DPRS_REACT_TO_PLAYER_BLOCKING_DOOR
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay
				//Line
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id, ped.id)
						PICK_FLEE_REACTION(ped)
						#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 2") #ENDIF
					ELSE
						MONITOR_DRIVER_DOOR_BLOCKED(ped)
						ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE DPRS_REACT_TO_PLAYER_BLOCKING_BAC_OF_TRUCK
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay
				//Line
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id, ped.id)

						PICK_FLEE_REACTION(ped)
						#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 5") #ENDIF
					ELSE
						MONITOR_DRIVER_DOOR_BLOCKED(ped)
						ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE DPRS_REACT_TO_PLAYER_BLOCKING_FRONT_OF_TRUCK
			IF IS_PED_BEING_JACKED(ped.id)

				PICK_FLEE_REACTION(ped, TRUE)	
				EXIT
			ENDIF
			IF (GET_GAME_TIMER() - iPlayerBlockingTruckTimer) >= 9000
				//Line
				IF NOT IS_PLAYER_BLOCKING_TRUCK_PROGRESSION(ped)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					ped.iReactionTimer = GET_GAME_TIMER()
					ped.eControlState = DPCS_REACTING
					ped.eReactionState = DPRS_REACT_TO_PLAYER_BLOCKING_FRONT_OF_TRUCK
				ENDIF
			ELIF (GET_GAME_TIMER() - iPlayerBlockingTruckTimer) >= 5000
			AND (GET_GAME_TIMER() - iPlayerBlockingTruckTimer) <= 5500
				IF IS_PED_IN_ANY_VEHICLE(ped.id)
					IF IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(ped.id)
						CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(ped.id)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE DPRS_REACT_TO_PLAYER_BLOCKING_BINS
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id, ped.id)
						PICK_FLEE_REACTION(ped)
						#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 4") #ENDIF
					ELSE
					MONITOR_DRIVER_DOOR_BLOCKED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_REACT_TO_PLAYER_BUMP
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ELSE
					IF CAN_PED_SEE_PED(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id, ped.id)

						PICK_FLEE_REACTION(ped)
						#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 5") #ENDIF
					ELSE
						MONITOR_DRIVER_DOOR_BLOCKED(ped)
						ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_REACT_FLIM
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF CAN_PED_SEE_PED(ped.id, PLAYER_PED_ID())

					PICK_FLEE_REACTION(ped, TRUE)
					#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 6") #ENDIF
					EXIT
				ENDIF
			ENDIF
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= 6000
				IF NOT IS_ENTITY_IN_ANGLED_AREA(ped.id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mRoutedVehicle.id, <<5, 0, -2>>), (GET_ENTITY_COORDS(mRoutedVehicle.id)+ <<0,0,2>>), 9)

					REACTIVATE_PED(ped)
				ELSE
					TELL_PED_RING_COPS(ped)
					TELL_PED_RING_COPS(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)])
				ENDIF
				ped.iReactionTimer = GET_GAME_TIMER()
			ENDIF
			MONITOR_PLAYER_DRIVING_OFF_IN_VAN(ped)
		BREAK
		
		CASE DPRS_REACT_BRAWL
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
				IF CAN_PED_SEE_PED(ped.id, PLAYER_PED_ID())

					PICK_FLEE_REACTION(ped, TRUE)
					#IF IS_DEBUG_BUILD SK_PRINT("player is armed flee 7") #ENDIF
					EXIT
				ENDIF
			ENDIF
//			SET_PED_MIN_MOVE_BLEND_RATIO(ped.id, PEDMOVEBLENDRATIO_RUN)
			
			IF (GET_GAME_TIMER() - ped.iReactionTimer) >= ped.iReactionDelay			
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(ped.id, PLAYER_PED_ID(), 15)
					//Line

					REACTIVATE_PED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
					EXIT
				ELSE
					MONITOR_DRIVER_DOOR_BLOCKED(ped)
					ped.iReactionTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		BREAK
		
		CASE DPRS_REACT_SEARCH
			MONITER_PLAYER_THREATEN(ped, ped.eDeliverPedType, TRUE)
			MONITOR_PLAYER_DRIVING_OFF_IN_VAN(ped)
//			IF DOES_ENTITY_EXIST(mPed[GET_THE_OTHER_GUY(ped.eDeliverPedType)].id)
				IF IS_ENTITY_IN_RANGE_COORDS(ped.id, vSearchPos, 2)
					IF IS_POSITION_IN_PED_VIEW_CONE(ped.id, vSearchPos)
						CLEAR_PED_TASKS(ped.id)
						ped.eReactionState = DPRS_WAIT_FLIM_DEAD
						ped.eControlState = DPCS_REACTING
						ped.iReactionTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
//			ELSE
//				TASK_VEHICLE_DRIVE_WANDER(ped.id, mRoutedVehicle.id, 20, DRIVINGMODE_STOPFORCARS_STRICT)
//				ped.iReactionTimer = GET_GAME_TIMER()
//				ped.eControlState = DPCS_DELIVERING
//				ped.eDeliverState = DPDS_WANDERING
//			ENDIF
		BREAK
		
	
	ENDSWITCH
ENDPROC


PROC DELIVERY_PED_STATE(PED_STRUCT &ped, DELIVER_PED_TYPE type)
	IF IS_PED_UNINJURED(ped.id)
		MONITER_PLAYER_THREATEN(ped, type)
		IF IS_ENTITY_PLAYING_ANIM(ped.id, "missfbi4prepp1", "_bag_drop_garbage_man")
			IF GET_ENTITY_ANIM_CURRENT_TIME(ped.id, "missfbi4prepp1", "_bag_drop_garbage_man") >= 0.172
//				CPRINTLN(DEBUG_MISSION, "ped is playing drop garbage = ", type)
				IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
				AND IS_ENTITY_ATTACHED(mDeliveryPoint[iDeliveryIndex].oiObject)
					RESET_PED_MOVEMENT_CLIPSET(ped.id)
					RESET_PED_WEAPON_MOVEMENT_CLIPSET(ped.id)
					IF iDeliveryIndex <= MAX_DELIVERIES-1
					AND iDeliveryIndex != -1
						mDeliveryPoint[iDeliveryIndex].bDelivered = TRUE
						ped.eDeliverSubState = SDS_NULL
					ENDIF
					CPRINTLN(DEBUG_MISSION, "Detaching bin bag = ", type)
					DETACH_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, TRUE, FALSE)
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(mDeliveryPoint[iDeliveryIndex].oiObject, APPLY_TYPE_IMPULSE, GET_ENTITY_FORWARD_VECTOR(ped.id),0, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		SWITCH ped.eControlState
			CASE DPCS_DELIVERING
				IF type = DRIVER
					ROUTE_POINT_MANAGER()
					MONITER_PLAYER_BUMPING_VAN(ped)
				ENDIF
				DO_DELIVERIES(ped)
			BREAK

			CASE DPCS_REACTING
				DO_REACTIONS(ped)
			BREAK
		ENDSWITCH
	ELSE
		IF IS_ENTITY_ALIVE(mDeliveryPoint[iDeliveryIndex].oiObject)
			IF IS_ENTITY_ATTACHED(mDeliveryPoint[iDeliveryIndex].oiObject)
				DETACH_ENTITY(mDeliveryPoint[iDeliveryIndex].oiObject, TRUE, FALSE)
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(mDeliveryPoint[iDeliveryIndex].oiObject, APPLY_TYPE_IMPULSE, GET_ENTITY_FORWARD_VECTOR(ped.id),0, TRUE, TRUE)
				CPRINTLN(DEBUG_MISSION, "ped dead detach bag")
			ENDIF
		ENDIF

		SAFE_REMOVE_BLIP(ped.blip)
		IF DOES_ENTITY_EXIST(ped.id)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), ped.id, 150)
			AND NOT bCheckSeenPedDie
				CPRINTLN(DEBUG_MISSION, "Released one of the dead delivery peds = ", ped.eDeliverPedType)
				SAFE_RELEASE_PED(ped.id)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_ROUTE_MANAGER()
	INT i
	FOR i= ENUM_TO_INT(DRIVER) TO (ENUM_TO_INT(MAX_DELIVERY_PEDS)-1)
		DELIVERY_PED_STATE(mPed[i], INT_TO_ENUM(DELIVER_PED_TYPE, i))
	ENDFOR
ENDPROC

/// PURPOSE:
///    Fills a new delivery point with a position and info for spawning a ped that the Driver ped will walk over to
///    If the Model is set to DUMMY_MODEL_FOR_SCRIPT then no ped will be spawned but the Driver ped will still get out of the vehicle and 
///    head to the point the ped would of been
/// PARAMS:
///    vpos - The position of the point to drive to 
///    vPedPos - the position the ped should be spawned at or where the driver will walk to 
///    fPedDir - the direction the ped should face when spawned 
///    mod - the model that should be used to spawn the ped
/// RETURNS:
///    A new instance of a DELIVERY_POINT
FUNC DELIVERY_POINT FILL_DELIVERY_POINT_DEST_PED(VECTOR vpos, VECTOR vPedPos, FLOAT fPedDir, MODEL_NAMES mod)
	DELIVERY_POINT mTemp
	
	mTemp.vDriveTo = vpos
	mTemp.vPedSpawn = vPedPos
	mTemp.fPedHeading = fPedDir
	mTemp.pedModel = mod
	RETURN mTemp
ENDFUNC

/// PURPOSE:
///    Fills in a delivery point with all the needed data for the Vehicle to 
///    park when reaching the delivery point
/// PARAMS:
///    DelPoint - The Delivery point to populate
///    UseParkTask - Should we use the park task or just coast to a stop (peds apply breaks when they dont have a task in a car
///    DriveToTargetDist - The distance at which the vehicle has reached its destination 
///    DriveToStraightDist - The disatance before the vehicle will use straight line pathing
///    ParkHeading - If the vehicle's heading isn't within this amount of the Direction param, the vehicle will back up and try to straighten itself out
///    ParkType - The type of parking TO perform
///    BlockingAreaCentre - The point we should block veh gens 
///    BlockingAreaSize - the size of the area we should block 
PROC FILL_DELIVERY_POINT_PARKING(DELIVERY_POINT &DelPoint, VECTOR parkingpos, BOOL UseParkTask, FLOAT DriveToTargetDist,	FLOAT DriveToStraightDist, FLOAT ParkHeading, PARK_TYPE ParkType, VECTOR	BlockingAreaCentre, VECTOR BlockingAreaSize)
	DelPoint.vParkingVec = parkingpos
	DelPoint.fDriveToTargetDist = DriveToTargetDist
	DelPoint.fDriveToStraightDist = DriveToStraightDist
	DelPoint.fParkHeading = ParkHeading
	DelPoint.bDoActionAtDeliveryPoint = UseParkTask
	DelPoint.eParkType = ParkType
	DelPoint.vBlockingAreaCentre = BlockingAreaCentre
	DelPoint.vBlockingAreaSize = BlockingAreaSize
ENDPROC

PROC REGISTER_PED_AS_DRIVER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		mPed[DRIVER].id = ped
	ELSE
		SCRIPT_ASSERT("Ped was invaild when trying to register as a delivery driver")
	ENDIF
ENDPROC

PROC REGISTER_PED_AS_PASSENGER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		mPed[PASSENGER].id = ped
	ELSE
		SCRIPT_ASSERT("Ped was invaild when trying to register as a passenger")
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a Vehicle as the Route Vehicle
/// PARAMS:
///    vhe - 
PROC REGISTER_VEHICLE(VEHICLE_INDEX vhe)
	IF IS_VEHICLE_OK(vhe)
		mRoutedVehicle.id = vhe
	ELSE
		SCRIPT_ASSERT("Ped was invaild when trying to register as a delivery vehicle")
	ENDIF
ENDPROC

/// PURPOSE:
///    Releases the ped and the vehicle
PROC RELEASE_DELIVERY_ENTITIES()
	SAFE_REMOVE_BLIP(mPed[DRIVER].blip)
	SAFE_REMOVE_BLIP(mPed[PASSENGER].blip)
	SAFE_RELEASE_VEHICLE(mRoutedVehicle.id)
	SAFE_RELEASE_PED(mPed[DRIVER].id)
	SAFE_RELEASE_PED(mPed[PASSENGER].id)
	SAFE_RELEASE_OBJECT(mDeliveryPoint[iDeliveryIndex].oiObject)
	REMOVE_RELATIONSHIP_GROUP(HASH_ENEMIES)
	REMOVE_ANIM_DICT("missfbi4prepp1")
	REMOVE_ANIM_SET("missfbi4prepp1_garbageman")
ENDPROC

/// PURPOSE:
///    Starts the Delivery peds state machine
/// PARAMS:
///    NumDeliveries - 
///    IsSetRoute - 
///    IsLooped - 
///    DoActionAtDeliveryPoint - 
PROC ACTIVATE_DELIVERY_MANAGER(INT NumDeliveries = MAX_DELIVERIES, BOOL IsSetRoute = FALSE, BOOL IsLooped = FALSE, BOOL PassengerDoDeliveries = FALSE)
	iNumDeliveries = NumDeliveries
	bIsSetRoute = IsSetRoute
	bIsLooped = IsLooped
	bPassengerDoDeliveries = PassengerDoDeliveries
	mPed[DRIVER].iReactionTimer = 0
	mPed[PASSENGER].iReactionTimer = 0
	INT i
	
	FOR i=0 TO (iNumDeliveries-1)
		mDeliveryPoint[i].bDelivered = FALSE
	ENDFOR
	fTruckBootOpenRatio = 0
	ACTIVATE_DRIVER_PED()
	mPed[PASSENGER].eDeliverState = DPDS_WAITING_IN_VAN
	mPed[PASSENGER].eControlState = DPCS_DELIVERING
	mPed[PASSENGER].eDeliverPedType = PASSENGER
	
	mPed[PASSENGER].iReactionDelay = 1500
	mPed[PASSENGER].iQuickReactionDelay = 500

	mPed[DRIVER].eControlState = DPCS_DELIVERING
	mPed[DRIVER].eDeliverPedType = DRIVER
	
	mPed[DRIVER].iReactionDelay = 1000
	mPed[DRIVER].iQuickReactionDelay = 100
	
	ADD_RELATIONSHIP_GROUP("Enemies", HASH_ENEMIES)
	SET_PED_RELATIONSHIP_GROUP_HASH(mPed[DRIVER].id, HASH_ENEMIES)
	SET_PED_RELATIONSHIP_GROUP_HASH(mPed[PASSENGER].id, HASH_ENEMIES)

	SET_ENTITY_IS_TARGET_PRIORITY(mPed[DRIVER].id, TRUE)
	SET_ENTITY_IS_TARGET_PRIORITY(mPed[PASSENGER].id, TRUE)
	
	INIT_GOTO_BLOCKING_SHAPE_TEST(<<1.9,1.9,8>>, <<0,0,0>>)
ENDPROC

/// PURPOSE:
///    Loads Anims required for ped reactions or behaviours
///    
/// PARAMS:
///    bWait - If TRUE we wait in a while loop (holting the script) till the 
///    anims are loaded
/// RETURNS:
///    TRUE when all anims are loaded 
FUNC BOOL LOAD_DELIVERY_ANIMS(BOOL bWait = FALSE)

	REQUEST_ANIM_DICT("missfbi4prepp1")
	REQUEST_ANIM_SET("missfbi4prepp1_garbageman")
	IF bWait
		WHILE NOT HAS_ANIM_DICT_LOADED("missfbi4prepp1")
		AND NOT HAS_ANIM_SET_LOADED("missfbi4prepp1_garbageman")
			WAIT(0)
		ENDWHILE
		RETURN TRUE
	ELSE
		IF HAS_ANIM_DICT_LOADED("missfbi4prepp1")
		AND HAS_ANIM_SET_LOADED("missfbi4prepp1_garbageman")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
