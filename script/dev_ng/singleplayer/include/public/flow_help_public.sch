//╒═════════════════════════════════════════════════════════════════════════════╕
//│               Author:  Ben Rollinson                  Date: 09/02/12        │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│                                                                             │
//│                           Flow Help Text Public Header                      │
//│                                                                             │
//│               The public interface for the flow help text controller.       │
//│                                                                             │
//╘═════════════════════════════════════════════════════════════════════════════╛  

USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_help_private.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Help Queue Management  ╞═══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

ENUM FlowHelpStatus
    FHS_QUEUED,
    FHS_DISPLAYED,
    FHS_EXPIRED
ENDENUM


PROC ADD_HELP_TO_FLOW_QUEUE(	STRING paramHelpLabel, 
								FlowHelpPriority paramPriority = FHP_LOW, 
								INT paramDelay = 0, 
								INT paramExpiration = 
								FLOW_HELP_NEVER_EXPIRES, 
								INT paramDuration = DEFAULT_HELP_TEXT_TIME, 
								INT paramCharBitset = 7, 
								CC_CodeID paramCodeIDStart = CID_BLANK, 
								CC_CodeID paramCodeIDDisplayed = CID_BLANK, 
								INT paramSettings = FHF_NONE)
							
    PRIVATE_Add_Help_To_Flow_Queue(paramHelpLabel, "", paramPriority, paramDelay, paramExpiration, paramDuration, paramCharBitset, paramCodeIDStart, paramCodeIDDisplayed, paramSettings)
ENDPROC


PROC ADD_HELP_WITH_SUBSTRING_TO_FLOW_QUEUE(	STRING paramHelpLabel, 
											STRING paramSubstringLabel, 
											FlowHelpPriority paramPriority = FHP_LOW, 
											INT paramDelay = 0, 
											INT paramExpiration = FLOW_HELP_NEVER_EXPIRES, 
											INT paramDuration = DEFAULT_HELP_TEXT_TIME, 
											INT paramCharBitset = 7, 
											CC_CodeID paramCodeIDStart = CID_BLANK, 
											CC_CodeID paramCodeIDDisplayed = CID_BLANK,
											INT paramSettings = FHF_NONE)
											
    PRIVATE_Add_Help_To_Flow_Queue(paramHelpLabel, paramSubstringLabel, paramPriority, paramDelay, paramExpiration, paramDuration, paramCharBitset, paramCodeIDStart, paramCodeIDDisplayed, paramSettings)
ENDPROC


PROC REMOVE_HELP_FROM_FLOW_QUEUE(STRING paramHelpLabel, BOOL paramIfDisplayedClear = TRUE)

    CPRINTLN(DEBUG_FLOW_HELP, "Script [", GET_THIS_SCRIPT_NAME(), "] attempting to remove ", paramHelpLabel, " from the help queue...")
    
    IF g_bFlowHelpDisplaying
    AND paramIfDisplayedClear       
        IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(paramHelpLabel)
		and not IS_HELP_MESSAGE_FADING_OUT()
            CPRINTLN(DEBUG_FLOW_HELP, "Help being removed is displayed. Clearing it. ")
            CLEAR_HELP(FALSE)
        ENDIF           
    ENDIF
    
    INT i, j
	
	#if USE_CLF_DLC	
	    REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount i        
	        //displayed are no longer in a queue
	        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobalsClifford.sFlowHelp.sHelpQueue[i].tHelpText)          
	            CPRINTLN(DEBUG_FLOW_HELP, "removing Help: ",paramHelpLabel," from queue.")
	            //Shift all queue entries above this down one position.
	            FOR j = i TO (g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount-2)
	                PRIVATE_Copy_Queued_Help_From_Index_To_Index(j, j+1)
	            ENDFOR
	            PRIVATE_Clear_Queued_Help_At_Index(g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount-1)
	            g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount--           
	            //Check if queue priority has changed after removing an item.
	            PRIVATE_Update_Flow_Help_Queue_Priority_Level()
	            EXIT
	        ENDIF
	    ENDREPEAT
	#endif 
	
	#if USE_NRM_DLC
	    REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount i        
	        //displayed are no longer in a queue
	        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobalsnorman.sFlowHelp.sHelpQueue[i].tHelpText)          
	            CPRINTLN(DEBUG_FLOW_HELP, "removing Help: ",paramHelpLabel," from queue.")
	            //Shift all queue entries above this down one position.
	            FOR j = i TO (g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount-2)
	                PRIVATE_Copy_Queued_Help_From_Index_To_Index(j, j+1)
	            ENDFOR
	            PRIVATE_Clear_Queued_Help_At_Index(g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount-1)
	            g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount--           
	            //Check if queue priority has changed after removing an item.
	            PRIVATE_Update_Flow_Help_Queue_Priority_Level()
	            EXIT
	        ENDIF
	    ENDREPEAT
	#endif

	#if NOT USE_SP_DLC
	    REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount i        
	        //displayed are no longer in a queue
	        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobals.sFlowHelp.sHelpQueue[i].tHelpText)          
	            CPRINTLN(DEBUG_FLOW_HELP, "removing Help: ",paramHelpLabel," from queue.")
	            //Shift all queue entries above this down one position.
	            FOR j = i TO (g_savedGlobals.sFlowHelp.iFlowHelpCount-2)
	                PRIVATE_Copy_Queued_Help_From_Index_To_Index(j, j+1)
	            ENDFOR
	            PRIVATE_Clear_Queued_Help_At_Index(g_savedGlobals.sFlowHelp.iFlowHelpCount-1)
	            g_savedGlobals.sFlowHelp.iFlowHelpCount--           
	            //Check if queue priority has changed after removing an item.
	            PRIVATE_Update_Flow_Help_Queue_Priority_Level()
	            EXIT
	        ENDIF
	    ENDREPEAT
	#endif   
	
    CPRINTLN(DEBUG_FLOW_HELP, " Failed. No matching help was queued.")
ENDPROC


FUNC BOOL IS_FLOW_HELP_QUEUE_EMPTY()

#if USE_CLF_DLC
	IF g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount > 0
		RETURN FALSE
    ELSE
        RETURN TRUE
    ENDIF
#endif

#if USE_NRM_DLC
	IF g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount > 0
		RETURN FALSE
    ELSE
        RETURN TRUE
    ENDIF
#endif

#if NOT USE_SP_DLC
    IF g_savedGlobals.sFlowHelp.iFlowHelpCount > 0
		RETURN FALSE
    ELSE
        RETURN TRUE
    ENDIF
#endif
        
ENDFUNC


FUNC BOOL IS_FLOW_HELP_MESSAGE_QUEUED(STRING paramHelpLabel)
    INT index
	
#if USE_CLF_DLC	
    REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount index
        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobalsClifford.sFlowHelp.sHelpQueue[index].tHelpText)
            RETURN TRUE
        ENDIF
    ENDREPEAT	
    RETURN FALSE
#endif	

#if USE_NRM_DLC
    REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount index
        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobalsnorman.sFlowHelp.sHelpQueue[index].tHelpText)
            RETURN TRUE
        ENDIF
    ENDREPEAT	
    RETURN FALSE
#endif

#if NOT USE_SP_DLC
    REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount index
        IF ARE_STRINGS_EQUAL(paramHelpLabel, g_savedGlobals.sFlowHelp.sHelpQueue[index].tHelpText)
            RETURN TRUE
        ENDIF
    ENDREPEAT	
    RETURN FALSE
#endif

ENDFUNC


FUNC FlowHelpStatus GET_FLOW_HELP_MESSAGE_STATUS(STRING paramHelpLabel)

    //Is the help message registered as the last displayed?
    IF ARE_STRINGS_EQUAL(paramHelpLabel, g_txtFlowHelpLastDisplayed)
        RETURN FHS_DISPLAYED
    ENDIF
        
    //Is the help message still waiting in the queue?
    IF IS_FLOW_HELP_MESSAGE_QUEUED(paramHelpLabel)
        RETURN FHS_QUEUED
    ENDIF
     
    //The help messaged either expired or was never queued.
    RETURN FHS_EXPIRED
	
ENDFUNC


PROC PAUSE_FLOW_HELP_QUEUE(BOOL paramPause)
    #IF IS_DEBUG_BUILD
        IF paramPause
            CPRINTLN(DEBUG_FLOW_HELP, "Help text queue paused by script [", GET_THIS_SCRIPT_NAME(), "].")
        ELSE
            CPRINTLN(DEBUG_FLOW_HELP, "Help text queue unpaused by script [", GET_THIS_SCRIPT_NAME(), "].")
            IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
                CWARNINGLN(DEBUG_FLOW_HELP, "Flow help queue unpaused while on mission.")
            ENDIF
        ENDIF
    #ENDIF
    
    //Add a short delay after unpausing to improve pacing.
    IF NOT paramPause
        g_iFlowHelpDelayTime = GET_GAME_TIMER() + FLOW_HELP_DELAY_BETWEEN_MESSAGES
    ENDIF
	
    g_bFlowHelpPaused = paramPause
ENDPROC


PROC CLEAR_FLOW_HELP_MESSAGE_DELAY()
    g_iFlowHelpDelayTime = GET_GAME_TIMER()
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Flow Help Messages - GTAV ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF NOT USE_SP_DLC 

ENUM FlowHelpMessage
    FHM_FIRST_MISSION_PASS,
    FHM_FIRST_STAT_INVALIDATED,
    FHM_FIRST_CALL_RINGING,
    FHM_FIRST_CALL_ONGOING,
    FHM_FIRST_TEXT_RECEIVED,
    FHM_FIRST_RC_BLIP,
    FHM_FIRST_PROPERTY_MISSION,
    FHM_SELECTOR_MISSIONS_A,
    FHM_SELECTOR_MISSIONS_B,
    FHM_SELECTOR_NO_SWAP_A,
    FHM_SELECTOR_NO_SWAP_B,
    FHM_SELECTOR_NO_SWAP_C,
    FHM_SELECTOR_NO_SWAP_EXILE,
    FHM_SELECTOR_WANTED_SWAP,
    FHM_BAILBOND_HELP_FIRST_MESSAGE,
    FHM_SONAR_COLLECT_LOAD_TRACKIFY,
    FHM_MOVING_MISSION_BLIPS,
    FHM_DOWNLOAD_CHOP_APP,
    FHM_RURAL_PREP1_BLIP,
    FHM_TAXI_PHONE_INTRODUCED,
    FHM_SAVE_ANYWHERE_ON_MISSION,
    FHM_STEALTH_TAKEDOWN,
    FHM_TATTOO_SHOPS_UNLOCKED,
    FHM_CLIMB_HELP,
    FHM_LADDER_HELP,
    FHM_RC_MRS_PHILIPS_FIND_VAN, // To fix B*1218064
    FHM_FIRST_CALL_DECISION,
    FHM_CONVERTIBLE_ROOF,
    FHM_HEADLIGHTS,
    FHM_NO_STAMINA,
    FHM_MULTI_WEAPONS_IN_SLOT,
    FHM_VEHICLE_RADIO_A,
    FHM_VEHICLE_RADIO_B,
    FHM_LETTER_INFO_DISPLAYED,
    FHM_EPSILON_WEBSITE,
    FHM_NEAR_FIRST_RCM_AS_WRONG_CHAR,
    FHM_FIRST_TRACT_CLUE,
    FHM_FIRST_EMAIL_WITH_LINK,
    FHM_FIRST_EMAIL_SCROLL,
    FHM_JOSH1_FOR_SALE,
    FHM_RAMPAGES_UNLOCKED,
    FHM_DINGHY_HELP1,
    FHM_DINGHY_HELP2,
    FHM_DINGHY_HELP3,
    FHM_DINGHY_HELP4,
    FHM_HEIST_CREW_UNLOCKED,
    FHM_VEHICLE_ROLLED,
    FHM_OBTAINED_AUTO_PARACHUTE,
    FHM_FRIEND_ACTIVITY_HELP,
    FHM_ENTERED_TRAIN_IN_GROUP,
    FHM_FIRST_RE_ENCOUNTER,
    FHM_RE_STAT_BOOSTS,
    FHM_PROG_AR,
    FHM_SOCIAL_CLUB_LINK,
    FHM_AFFECT_STOCKS,
    FHM_BARRY3_REMINDER_HELP,
    FHM_HEIST_CREW_SKILLUP,
    FHM_BLIMP_UNLOCK,
    FHM_RC_TONYA1_REMINDER,
	FHM_PIMENU_ACTIVATE,
	FHM_SNIPER_ZOOMED_MOVEMENT,
	FHM_DRIVEBY_CONTROL,
	FHM_TRI_3_UNLOCKED_IN_EXILE,
	FHM_PLAYER_LOGGED_FOR_STOCKS,
	FHM_FIRST_PERSON_HELP_REMINDER_A,
	FHM_FIRST_PERSON_HELP_REMINDER_B,
	FHM_ROCKSTAR_EDITOR_INTRO1,
	FHM_ROCKSTAR_EDITOR_INTRO2,
	FHM_DIRECTOR_MODE_STORY,
	FHM_DIRECTOR_MODE_HEISTS,
	FHM_DIRECTOR_MODE_SPECIAL,
	FHM_DIRECTOR_MODE_ANIMALS,
	FHM_ROCKSTAR_EDITOR_REC,
	FHM_ROCKSTAR_EDITOR_ACTION_MODE//,
	//FHM_HYDRAULICS_HELP
ENDENUM

#IF IS_DEBUG_BUILD
    FUNC STRING GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(FlowHelpMessage paramHelp)
    
        SWITCH paramHelp
            CASE FHM_FIRST_MISSION_PASS             RETURN "FIRST_MISSION_PASS"             BREAK
            CASE FHM_FIRST_STAT_INVALIDATED         RETURN "FIRST_STAT_INVALIDATED"         BREAK
            CASE FHM_FIRST_CALL_RINGING             RETURN "FIRST_CALL_RINGING"             BREAK
            CASE FHM_FIRST_CALL_ONGOING             RETURN "FIRST_CALL_ONGOING"             BREAK
            CASE FHM_FIRST_TEXT_RECEIVED            RETURN "FIRST_TEXT_RECEIVED"            BREAK
            CASE FHM_FIRST_RC_BLIP                  RETURN "FIRST_RC_BLIP"                  BREAK
            CASE FHM_FIRST_PROPERTY_MISSION         RETURN "FIRST_PROPERTY_MISSION"         BREAK           
            CASE FHM_SELECTOR_MISSIONS_A            RETURN "SELECTOR_MISSIONS_A"            BREAK
            CASE FHM_SELECTOR_MISSIONS_B            RETURN "SELECTOR_MISSIONS_B"            BREAK
            CASE FHM_SELECTOR_NO_SWAP_A             RETURN "SELECTOR_NO_SWAP_A"             BREAK
            CASE FHM_SELECTOR_NO_SWAP_B             RETURN "SELECTOR_NO_SWAP_B"             BREAK
            CASE FHM_SELECTOR_NO_SWAP_C             RETURN "SELECTOR_NO_SWAP_C"             BREAK
            CASE FHM_SELECTOR_NO_SWAP_EXILE         RETURN "FHM_SELECTOR_NO_SWAP_EXILE"     BREAK
            CASE FHM_SELECTOR_WANTED_SWAP           RETURN "SELECTOR_WANTED_SWAP"           BREAK
            CASE FHM_BAILBOND_HELP_FIRST_MESSAGE    RETURN "BAILBOND_HELP_FIRST_MESSAGE"    BREAK
            CASE FHM_SONAR_COLLECT_LOAD_TRACKIFY    RETURN "SUBM_HELP1"                     BREAK
            CASE FHM_MOVING_MISSION_BLIPS           RETURN "AH_H_JHP2A_ASS"                 BREAK
            CASE FHM_DOWNLOAD_CHOP_APP              RETURN "FC_APPHLP"                      BREAK
            CASE FHM_RURAL_PREP1_BLIP               RETURN "AM_H_PREP4"                     BREAK
            CASE FHM_TAXI_PHONE_INTRODUCED          RETURN "TAXI_PHONE_INTRODUCED"          BREAK
            CASE FHM_SAVE_ANYWHERE_ON_MISSION       RETURN "SAVE_ANYWHERE_ON_MISSION"       BREAK
            CASE FHM_STEALTH_TAKEDOWN               RETURN "AH_H_TAKEDOWN"                  BREAK
            CASE FHM_TATTOO_SHOPS_UNLOCKED          RETURN "TATTOO_SHOPS_UNLOCKED"          BREAK
            CASE FHM_CLIMB_HELP                     RETURN "CLIMB_HELP"                     BREAK
            CASE FHM_LADDER_HELP                    RETURN "LADDER_HELP"                    BREAK
            CASE FHM_RC_MRS_PHILIPS_FIND_VAN        RETURN "MRSP_01_HELP"                   BREAK
            CASE FHM_FIRST_CALL_DECISION            RETURN "FIRST_CALL_DECISION"            BREAK
            CASE FHM_CONVERTIBLE_ROOF               RETURN "CONVERTIBLE_ROOF"               BREAK
            CASE FHM_HEADLIGHTS                     RETURN "HEADLIGHTS"                     BREAK
            CASE FHM_NO_STAMINA                     RETURN "NO_STAMINA"                     BREAK
            CASE FHM_MULTI_WEAPONS_IN_SLOT          RETURN "MULTI_WEAPONS_IN_SLOT"          BREAK
            CASE FHM_VEHICLE_RADIO_A                RETURN "VEHICLE_RADIO_A"                BREAK
            CASE FHM_VEHICLE_RADIO_B                RETURN "VEHICLE_RADIO_B"                BREAK
            CASE FHM_LETTER_INFO_DISPLAYED          RETURN "LETTERS_FIRST"                  BREAK
            CASE FHM_EPSILON_WEBSITE                RETURN "EPSILON_WEBSITE"                BREAK
            CASE FHM_NEAR_FIRST_RCM_AS_WRONG_CHAR   RETURN "NEAR_FIRST_RCM_AS_WRONG_CHAR"   BREAK
            CASE FHM_FIRST_TRACT_CLUE               RETURN "FIRST_TRACT_CLUE"               BREAK
            CASE FHM_FIRST_EMAIL_WITH_LINK          RETURN "FIRST_EMAIL_WITH_LINK"          BREAK
            CASE FHM_FIRST_EMAIL_SCROLL             RETURN "FIRST_EMAIL_SCROLL"             BREAK
            CASE FHM_JOSH1_FOR_SALE                 RETURN "JOSH1_FOR_SALE"                 BREAK
            CASE FHM_RAMPAGES_UNLOCKED              RETURN "RAMPAGES_UNLOCKED"              BREAK
            CASE FHM_DINGHY_HELP1                   RETURN "DINGHY_HELP1"                   BREAK
            CASE FHM_DINGHY_HELP2                   RETURN "DINGHY_HELP2"                   BREAK
            CASE FHM_DINGHY_HELP3                   RETURN "DINGHY_HELP3"                   BREAK
            CASE FHM_DINGHY_HELP4                   RETURN "DINGHY_HELP4"                   BREAK
            CASE FHM_HEIST_CREW_UNLOCKED            RETURN "HEIST_CREW_UNLOCKED"            BREAK
            CASE FHM_VEHICLE_ROLLED                 RETURN "VEHICLE_ROLLED"                 BREAK
            CASE FHM_FRIEND_ACTIVITY_HELP           RETURN "FHM_FRIEND_ACTIVITY_HELP"       BREAK
            CASE FHM_OBTAINED_AUTO_PARACHUTE        RETURN "FHM_OBTAINED_AUTO_PARACHUTE"    BREAK
            CASE FHM_ENTERED_TRAIN_IN_GROUP         RETURN "ENTERED_TRAIN_IN_GROUP"         BREAK
            CASE FHM_FIRST_RE_ENCOUNTER             RETURN "FIRST_RE_ENCOUNTER"             BREAK
            CASE FHM_RE_STAT_BOOSTS                 RETURN "RE_STAT_BOOSTS"                 BREAK
            CASE FHM_PROG_AR                        RETURN "PROG_AR"                        BREAK
            CASE FHM_SOCIAL_CLUB_LINK               RETURN "SOCIAL_CLUB_LINK"               BREAK
            CASE FHM_AFFECT_STOCKS                  RETURN "AFFECT_STOCKS"                  BREAK
            CASE FHM_BARRY3_REMINDER_HELP           RETURN "BARRY3_REMINDER_HELP"           BREAK
            CASE FHM_HEIST_CREW_SKILLUP             RETURN "HEIST_CREW_SKILLUP"             BREAK
            CASE FHM_BLIMP_UNLOCK                   RETURN "BLIMP_UNLOCK"                   BREAK
            CASE FHM_RC_TONYA1_REMINDER             RETURN "RC_TONYA1_REMINDER"             BREAK
			CASE FHM_PIMENU_ACTIVATE             	RETURN "PI_MENU_ACTIVATE"             	BREAK
			CASE FHM_SNIPER_ZOOMED_MOVEMENT			RETURN "FHM_SNIPER_ZOOMED_MOVEMENT"		BREAK
			CASE FHM_DRIVEBY_CONTROL				RETURN "FHM_DRIVEBY_CONTROL"			BREAK
			CASE FHM_TRI_3_UNLOCKED_IN_EXILE		RETURN "TRI_3_UNLOCKED_IN_EXILE"		BREAK
			CASE FHM_PLAYER_LOGGED_FOR_STOCKS		RETURN "PLAYER_NOT_LOGGED_FOR_STOCKS"	BREAK
			CASE FHM_FIRST_PERSON_HELP_REMINDER_A	RETURN "FIRST_PERSON_HELP_REMINDER_A"	BREAK
			CASE FHM_FIRST_PERSON_HELP_REMINDER_B	RETURN "FIRST_PERSON_HELP_REMINDER_B"	BREAK
			CASE FHM_ROCKSTAR_EDITOR_INTRO1			RETURN "ROCKSTAR_EDITOR_INTRO1"			BREAK
			CASE FHM_ROCKSTAR_EDITOR_INTRO2			RETURN "ROCKSTAR_EDITOR_INTRO2"			BREAK
			CASE FHM_DIRECTOR_MODE_STORY			RETURN "DIRECTOR_MODE_STORY"			BREAK
			CASE FHM_DIRECTOR_MODE_HEISTS			RETURN "DIRECTOR_MODE_HEISTS"			BREAK
			CASE FHM_DIRECTOR_MODE_SPECIAL			RETURN "DIRECTOR_MODE_SPECIAL"			BREAK
			CASE FHM_DIRECTOR_MODE_ANIMALS			RETURN "DIRECTOR_MODE_ANIMALS"			BREAK
			CASE FHM_ROCKSTAR_EDITOR_REC			RETURN "ROCKSTAR_EDITOR_REC"			BREAK
			CASE FHM_ROCKSTAR_EDITOR_ACTION_MODE	RETURN "ROCKSTAR_EDITOR_ACTION_MODE"	BREAK
			//CASE FHM_HYDRAULICS_HELP				RETURN "HYDRAULICS_HELP"				BREAK
        ENDSWITCH
        
        SCRIPT_ASSERT("GET_FLOW_HELP_MESSAGE_DISPLAY_STRING: FlowHelpMessage passed did not have a display string defined. Bug BenR.")
        RETURN "ERROR"
    ENDFUNC
#ENDIF

#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Flow Help Messages - CLF  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF USE_CLF_DLC

ENUM FlowHelpMessage
    FHM_VEHICLE_ROLLED,
    FHM_ENTERED_TRAIN_IN_GROUP,
    FHM_SOCIAL_CLUB_LINK,
	FHM_JP_ACTIVATED,
	FHM_JP_TOGGLE
ENDENUM

#IF IS_DEBUG_BUILD
    FUNC STRING GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(FlowHelpMessage paramHelp)
    
        SWITCH paramHelp
            CASE FHM_VEHICLE_ROLLED                 RETURN "VEHICLE_ROLLED"                 BREAK
            CASE FHM_ENTERED_TRAIN_IN_GROUP         RETURN "ENTERED_TRAIN_IN_GROUP"         BREAK
            CASE FHM_SOCIAL_CLUB_LINK               RETURN "SOCIAL_CLUB_LINK"               BREAK
			CASE FHM_JP_ACTIVATED					RETURN "FHM_JP_ACTIVATED"				BREAK
			CASE FHM_JP_TOGGLE 						RETURN "FHM_JP_TOGGLE"					BREAK
        ENDSWITCH
        
        SCRIPT_ASSERT("GET_FLOW_HELP_MESSAGE_DISPLAY_STRING: FlowHelpMessage passed did not have a display string defined. Bug BenR.")
        RETURN "ERROR"
    ENDFUNC
#ENDIF

#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ Flow Help Messages - NRM ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF USE_NRM_DLC

ENUM FlowHelpMessage
    FHM_VEHICLE_ROLLED,
    FHM_ENTERED_TRAIN_IN_GROUP,
    FHM_SOCIAL_CLUB_LINK
ENDENUM

#IF IS_DEBUG_BUILD
    FUNC STRING GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(FlowHelpMessage paramHelp)
    
        SWITCH paramHelp
            CASE FHM_VEHICLE_ROLLED                 RETURN "VEHICLE_ROLLED"                 BREAK
            CASE FHM_ENTERED_TRAIN_IN_GROUP         RETURN "ENTERED_TRAIN_IN_GROUP"         BREAK
            CASE FHM_SOCIAL_CLUB_LINK               RETURN "SOCIAL_CLUB_LINK"               BREAK
        ENDSWITCH
        
        SCRIPT_ASSERT("GET_FLOW_HELP_MESSAGE_DISPLAY_STRING: FlowHelpMessage passed did not have a display string defined. Bug BenR.")
        RETURN "ERROR"
    ENDFUNC
#ENDIF

#ENDIF


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════╡ One Time Help Message Management  ╞═════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FlowHelpMessage paramHelp)
    INT iBitIndex = ENUM_TO_INT(paramHelp)
    INT iBitsetIndex = 0
    WHILE iBitIndex > 31
        iBitIndex -= 32
        iBitsetIndex++
    ENDWHILE

    IF iBitsetIndex < FLOW_HELP_BITSET_COUNT
	
	#if USE_CLF_DLC
		SET_BIT(g_savedGlobalsClifford.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
	#endif
	
	#if USE_NRM_DLC
		SET_BIT(g_savedGlobalsnorman.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
	#endif
	
	#if NOT USE_SP_DLC
		SET_BIT(g_savedGlobals.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
	#endif	
	
        #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FLOW,"One time help message ", 
                                GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(paramHelp), 
                                " flagged as displayed by ", 
                                GET_THIS_SCRIPT_NAME(), ".")
        #ENDIF
    ELSE
        SCRIPT_ASSERT("SET_ONE_TIME_HELP_MESSAGE_DISPLAYED: Too many help messages registered to fit in saved bitsets. Increase FLOW_HELP_BITSET_COUNT?")
    ENDIF
ENDPROC


PROC CLEAR_ONE_TIME_HELP_MESSAGE_DISPLAYED(FlowHelpMessage paramHelp)
    INT iBitIndex = ENUM_TO_INT(paramHelp)
    INT iBitsetIndex = 0
    WHILE iBitIndex > 31
        iBitIndex -= 32
        iBitsetIndex++
    ENDWHILE

    IF iBitsetIndex < FLOW_HELP_BITSET_COUNT
		
		#if USE_CLF_DLC
			CLEAR_BIT(g_savedGlobalsClifford.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
		#endif
		
		#if USE_NRM_DLC
			CLEAR_BIT(g_savedGlobalsnorman.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
		#endif
		
		#if NOT USE_SP_DLC
			CLEAR_BIT(g_savedGlobals.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
		#endif
		
        CLEAR_BIT(g_savedGlobals.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
		
        #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FLOW,"One time help message ", 
                                GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(paramHelp), 
                                " cleared as displayed by ", 
                                GET_THIS_SCRIPT_NAME(), ".")
        #ENDIF
    ELSE
		#IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FLOW,"Tried to set one time help message ", 
                                GET_FLOW_HELP_MESSAGE_DISPLAY_STRING(paramHelp), 
                                " as displayed but there wasn't enough saved bitsets.")
        #ENDIF
        SCRIPT_ASSERT("CLEAR_ONE_TIME_HELP_MESSAGE_DISPLAYED: Too many help messages registered to fit in saved bitsets. Increase FLOW_HELP_BITSET_COUNT?")
    ENDIF
ENDPROC


FUNC BOOL HAS_ONE_TIME_HELP_DISPLAYED(FlowHelpMessage paramHelp)
    INT iBitIndex = ENUM_TO_INT(paramHelp)
    INT iBitsetIndex = 0
    WHILE iBitIndex > 31
        iBitIndex -= 32
        iBitsetIndex++
    ENDWHILE
    
    IF iBitsetIndex < FLOW_HELP_BITSET_COUNT
	
		#IF USE_CLF_DLC
			IF g_bLoadedClifford
	       		RETURN IS_BIT_SET(g_savedGlobalsClifford.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
			ENDIF
		#ENDIF	
		
		#IF USE_NRM_DLC
			IF g_bLoadedNorman
	        	RETURN IS_BIT_SET(g_savedGlobalsnorman.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
			ENDIF
		#ENDIF	
		
	    RETURN IS_BIT_SET(g_savedGlobals.sFlowHelp.iHelpDisplayedBitset[iBitsetIndex], iBitIndex)
    ENDIF

    SCRIPT_ASSERT("HAS_ONE_TIME_HELP_DISPLAYED: Too many help messages registered to fit in saved bitsets. Increase FLOW_HELP_BITSET_COUNT?")
    RETURN FALSE
ENDFUNC


// Keep requeueing a help message in the flow help queue until it displays. Once sucessful flag an FlowHelpMessage
// enum as being displayed so the help won't display again for this player.
PROC QUEUE_ONE_TIME_HELP_UNTIL_DISPLAYED(FlowHelpMessage paramMessage, STRING paramHelpLabel, INT paramCharBitset = 7)

	IF NOT HAS_ONE_TIME_HELP_DISPLAYED(paramMessage)
		SWITCH GET_FLOW_HELP_MESSAGE_STATUS(paramHelpLabel)
			CASE FHS_EXPIRED
				ADD_HELP_TO_FLOW_QUEUE(paramHelpLabel, FHP_MEDIUM, 0, 2000, DEFAULT_HELP_TEXT_TIME, paramCharBitset)
			BREAK
			CASE FHS_DISPLAYED
				SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(paramMessage)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC


PROC Clean_Non_Save_Help_Text_On_Startup()
    CPRINTLN(DEBUG_FLOW_HELP, "Checking for none save help to clear on SP startup.")    
    INT index
	
#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sFlowHelp.iFlowHelpCount index
        IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[index].iSettingsFlags, FHF_INDEX_SAVED)
            CPRINTLN(DEBUG_FLOW_HELP, "Removing none save help text ", g_savedGlobalsClifford.sFlowHelp.sHelpQueue[index].tHelpText, " from the help queue.")           
            REMOVE_HELP_FROM_FLOW_QUEUE(g_savedGlobalsClifford.sFlowHelp.sHelpQueue[index].tHelpText)
        ENDIF
    ENDREPEAT
#endif


#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sFlowHelp.iFlowHelpCount index
        IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[index].iSettingsFlags, FHF_INDEX_SAVED)
            CPRINTLN(DEBUG_FLOW_HELP, "Removing none save help text ", g_savedGlobalsnorman.sFlowHelp.sHelpQueue[index].tHelpText, " from the help queue.")           
            REMOVE_HELP_FROM_FLOW_QUEUE(g_savedGlobalsnorman.sFlowHelp.sHelpQueue[index].tHelpText)
        ENDIF
    ENDREPEAT
#endif

#if NOT USE_SP_DLC
    REPEAT g_savedGlobals.sFlowHelp.iFlowHelpCount index
        IF NOT IS_BIT_SET(g_savedGlobals.sFlowHelp.sHelpQueue[index].iSettingsFlags, FHF_INDEX_SAVED)
            CPRINTLN(DEBUG_FLOW_HELP, "Removing none save help text ", g_savedGlobals.sFlowHelp.sHelpQueue[index].tHelpText, " from the help queue.")           
            REMOVE_HELP_FROM_FLOW_QUEUE(g_savedGlobals.sFlowHelp.sHelpQueue[index].tHelpText)
        ENDIF
    ENDREPEAT
#endif	

ENDPROC
