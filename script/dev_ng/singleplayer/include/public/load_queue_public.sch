//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		 Load Queue Public Header							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			23/06/14												│
//│		DESCRIPTION: 	The public interface for queuing up load				│
//│						requests to be scheduled over multiple frames.			│								
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "load_queue_private.sch"


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Small Load Queue Commands  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Add a model to a small length load queue to be loaded in sequence with 
///    		 a number of other load requests across multiple frames.  
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramModel - The model enum to be queued and loaded.
///    
PROC LOAD_QUEUE_SMALL_ADD_MODEL(LoadQueueSmall &paramQueue, MODEL_NAMES paramModel)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL")
ENDPROC


/// PURPOSE: Remove a model that is in a small length load queue. Choose to unload
///    		 it from memory as we are doing this.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramModel - The model enum to be removed from the queue.
///    paramSetAsNoLongerNeeded - Unload the model from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_MODEL(LoadQueueSmall &paramQueue, MODEL_NAMES paramModel, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an animation dictionary to a small length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///		paramDictName - The name of the animation dictionary to be queued and loaded.
///    
PROC LOAD_QUEUE_SMALL_ADD_ANIM_DICT(LoadQueueSmall &paramQueue, STRING paramDictName)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName)
ENDPROC


/// PURPOSE: Remove an animation dictionary that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///  	paramDictName - The name of the animation dictionary to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the anim dict from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_ANIM_DICT(LoadQueueSmall &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a clip set to a small length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramClipSetName - The name of the clip set to be queued and loaded.
///    
PROC LOAD_QUEUE_SMALL_ADD_CLIP_SET(LoadQueueSmall &paramQueue, STRING paramClipSetName)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName)
ENDPROC


/// PURPOSE: Remove a clip set that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///  	paramClipSetName - The name of the clip set to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the clip set from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_CLIP_SET(LoadQueueSmall &paramQueue, STRING paramClipSetName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a texture dictionary to a small length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramDictName - The name of the texture dictionary be queued and loaded.
///    paramPriority - Should this load be given extra priority.
///    
PROC LOAD_QUEUE_SMALL_ADD_TEXT_DICT(LoadQueueSmall &paramQueue, STRING paramDictName, BOOL paramPriority = FALSE)
	INT iSettings
	IF paramPriority		
		SET_BIT(iSettings, BIT_LQD_PRIORITY_TD)
	ENDIF
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, iSettings)
ENDPROC


/// PURPOSE: Remove a texture dictionary that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///  	paramDictName - The name of the texture dictionary be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the texture dictionary from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_TEXT_DICT(LoadQueueSmall &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a vehicle recording to a small length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramFileNo - The number of the vehicle recording file.
///    paramRecName - The root name of the vehicle recording file.
///    
PROC LOAD_QUEUE_SMALL_ADD_VEH_REC(LoadQueueSmall &paramQueue, INT paramFileNo, STRING paramRecName)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName)
ENDPROC


/// PURPOSE: Remove a vehicle recording that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///   	paramFileNo - The number of the vehicle recording file.
///    	paramRecName - The root name of the vehicle recording file.
///    	paramSetAsNoLongerNeeded - Unload the vehicle recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_VEH_REC(LoadQueueSmall &paramQueue, INT paramFileNo, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a waypoint recording to a small length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramRecName - The name of the waypoint recording to be queued and loaded.
///    
PROC LOAD_QUEUE_SMALL_ADD_WAYPOINT_REC(LoadQueueSmall &paramQueue, STRING paramRecName)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName)
ENDPROC


/// PURPOSE: Remove a waypoint recording that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///    	paramRecName - The name of the waypoint recording to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the waypoint recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_WAYPOINT_REC(LoadQueueSmall &paramQueue, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an audio bank to a small length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramBankName - The name of the audio bank to be queued and loaded.
///    paramOverNetwork - Should this load be made over the nextwork.
///    
PROC LOAD_QUEUE_SMALL_ADD_AUDIO_BANK(LoadQueueSmall &paramQueue, STRING paramBankName, BOOL paramOverNetwork = FALSE)
	INT iSettings
	IF paramOverNetwork		
		SET_BIT(iSettings, BIT_LQD_OVER_NETWORK)
	ENDIF
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, iSettings)
ENDPROC


/// PURPOSE: Remove an audio bank that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///    	paramBankName - The name of the audio bank to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the audio bank from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_AUDIO_BANK(LoadQueueSmall &paramQueue, STRING paramBankName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a script to a small length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramScriptHash - The hash of the script name to be queued and loaded.
///    
PROC LOAD_QUEUE_SMALL_ADD_SCRIPT(LoadQueueSmall &paramQueue, INT paramScriptHash)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL")
ENDPROC


/// PURPOSE: Remove a script that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///    	paramScriptHash - The hash of the script name to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the script from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_SCRIPT(LoadQueueSmall &paramQueue, INT paramScriptHash, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a text block to a small length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramBlock - The text block to be loaded.
///    paramSlot - The text slot to load into.
///    paramClearBriefOnRelease - Should the briefs for text in this block be cleared when we unload the block.
///    
PROC LOAD_QUEUE_SMALL_ADD_ADDITIONAL_TEXT(LoadQueueSmall &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramClearBriefOnRelease = FALSE)
	INT iSettings
	IF paramClearBriefOnRelease		
		SET_BIT(iSettings, BIT_LQD_CLEAR_BRIEF)
	ENDIF
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, iSettings)
ENDPROC


/// PURPOSE: Remove a text block that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///    	paramBlock - The text block to be removed.
///   	paramSlot - The text slot to be removed.
///    	paramSetAsNoLongerNeeded - Unload the text block from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_ADDITIONAL_TEXT(LoadQueueSmall &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a PTFX asset to a small length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Note the script
///    		 needs to have been pre-allocated a PTFX asset for this to do anyting.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    
PROC LOAD_QUEUE_SMALL_ADD_PTFX_ASSET(LoadQueueSmall &paramQueue)
	PRIVATE_Load_Queue_Small_Add_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL")
ENDPROC


/// PURPOSE: Remove a PTFX asset that is in a small length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a small load queue data struct.
///    	paramSetAsNoLongerNeeded - Unload the PTFX asset from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_SMALL_REMOVE_PTFX_ASSET(LoadQueueSmall &paramQueue, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Small_Remove_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Query if all requests in a small length load queue have finished loading.
///    		 Note: Returns TRUE if the queue is empty.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
/// RETURNS: TRUE if all items in the queue have loaded or the queue is empty.
///    
FUNC BOOL HAS_LOAD_QUEUE_SMALL_LOADED(LoadQueueSmall &paramQueue)
	//If the queue isn't flagged as loading then return true immediately.
	IF NOT paramQueue.bLoading
		RETURN TRUE
	ENDIF
	
	//If the queue is flagged as loading start checking for items that haven't 
	//been requested or haven't finished loading.
	INT i
	REPEAT LOAD_QUEUE_SIZE_SMALL i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
				RETURN FALSE
			ENDIF
			IF NOT PRIVATE_Has_Queued_Load_Loaded(paramQueue.sQueue[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//All items have been requested and have loaded. 
	//Flag the queue as no longer loading a return we are finished.
	paramQueue.bLoading = FALSE
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Small queue finished loading.")
	RETURN TRUE
ENDFUNC


/// PURPOSE: Releases all items from memory that are listed in a small length load queue.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    
PROC SET_LOAD_QUEUE_SMALL_AS_NO_LONGER_NEEDED(LoadQueueSmall &paramQueue)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue small as no longer needed.")

	INT i
	REPEAT LOAD_QUEUE_SIZE_SMALL i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[i])
		ENDIF
	ENDREPEAT
	
	paramQueue.bLoading = TRUE
ENDPROC


/// PURPOSE: Manages a small load queue data struct ensuring that requests queued in
///    		 a sequence of one request per frame.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    
PROC UPDATE_LOAD_QUEUE_SMALL(LoadQueueSmall &paramQueue)
	IF paramQueue.bLoading
		IF GET_FRAME_COUNT() >= (paramQueue.iLastFrame + paramQueue.iFrameDelay)
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_PAUSED_GAME))
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED)) // The game has been paused. Don't allow this to deadlock the load queues.
			INT i
			REPEAT LOAD_QUEUE_SIZE_SMALL i
				IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
					IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
						PRIVATE_Make_Queued_Load(paramQueue.sQueue[i])
						paramQueue.iLastFrame = GET_FRAME_COUNT()
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Resets all data stored in a small length load queue data struct. This
///    		 command will also set all queued items as no longer needed to safeguard
///    		 against them being left in memory.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramLeaveInMemory - Only clear the data out of the struct. Don't unload any loaded assets at the same time.
///  
PROC CLEANUP_LOAD_QUEUE_SMALL(LoadQueueSmall &paramQueue, BOOL paramLeaveInMemory = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Cleaning up load queue small.")

	IF NOT paramLeaveInMemory
		SET_LOAD_QUEUE_SMALL_AS_NO_LONGER_NEEDED(paramQueue)
	ENDIF

	INT i
	REPEAT LOAD_QUEUE_SIZE_SMALL i
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[i])
	ENDREPEAT
	
	paramQueue.bLoading = FALSE
	paramQueue.iLastFrame = -1
ENDPROC


/// PURPOSE: Set how many frames to wait in between each queued load request for a small
///    		 length load queue.
/// PARAMS:
///    paramQueue - An instance of a small load queue data struct.
///    paramFrameDelay - The number of frames to schedule in between making each queued load request.
///    
PROC SET_LOAD_QUEUE_SMALL_FRAME_DELAY(LoadQueueSmall &paramQueue, INT paramFrameDelay)
	CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue small frame delay of ", paramFrameDelay, ".")
	IF paramFrameDelay > 0
		paramQueue.iFrameDelay = paramFrameDelay
	ELSE
		SCRIPT_ASSERT("SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY: Tried to set invalid frame delay of less than 1. Bug BenR.")
	ENDIF
ENDPROC




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ Medium Load Queue Commands  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Add a model to a medium length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.  
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramModel - The model enum to be queued and loaded.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_MODEL(LoadQueueMedium &paramQueue, MODEL_NAMES paramModel)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL")
ENDPROC


/// PURPOSE: Remove a model that is in a medium length load queue. Choose to unload
///    		 it from memory as we are doing this.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramModel - The model enum to be removed from the queue.
///    paramSetAsNoLongerNeeded - Unload the model from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_MODEL(LoadQueueMedium &paramQueue, MODEL_NAMES paramModel, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an animation dictionary to a medium length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramDictName - The name of the animation dictionary to be queued and loaded.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_ANIM_DICT(LoadQueueMedium &paramQueue, STRING paramDictName)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName)
ENDPROC


/// PURPOSE: Remove an animation dictionary that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///  	paramDictName - The name of the animation dictionary to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the anim dict from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_ANIM_DICT(LoadQueueMedium &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a clip set to a medium length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramClipSetName - The name of the clip set to be queued and loaded.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_CLIP_SET(LoadQueueMedium &paramQueue, STRING paramClipSetName)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName)
ENDPROC


/// PURPOSE: Remove a clip set that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///  	paramClipSetName - The name of the clip set to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the clip set from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_CLIP_SET(LoadQueueMedium &paramQueue, STRING paramClipSetName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a texture dictionary to a medium length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramDictName - The name of the texture dictionary be queued and loaded.
///    paramPriority - Should this load be given extra priority.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_TEXT_DICT(LoadQueueMedium &paramQueue, STRING paramDictName, BOOL paramPriority = FALSE)
	INT iSettings
	IF paramPriority		
		SET_BIT(iSettings, BIT_LQD_PRIORITY_TD)
	ENDIF
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, iSettings)
ENDPROC


/// PURPOSE: Remove a texture dictionary that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///  	paramDictName - The name of the texture dictionary be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the texture dictionary from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_TEXT_DICT(LoadQueueMedium &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a vehicle recording to a medium length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramFileNo - The number of the vehicle recording file.
///    paramRecName - The root name of the vehicle recording file.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_VEH_REC(LoadQueueMedium &paramQueue, INT paramFileNo, STRING paramRecName)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName)
ENDPROC


/// PURPOSE: Remove a vehicle recording that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///   	paramFileNo - The number of the vehicle recording file.
///    	paramRecName - The root name of the vehicle recording file.
///    	paramSetAsNoLongerNeeded - Unload the vehicle recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_VEH_REC(LoadQueueMedium &paramQueue, INT paramFileNo, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a waypoint recording to a medium length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramRecName - The name of the waypoint recording to be queued and loaded.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_WAYPOINT_REC(LoadQueueMedium &paramQueue, STRING paramRecName)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName)
ENDPROC


/// PURPOSE: Remove a waypoint recording that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///    	paramRecName - The name of the waypoint recording to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the waypoint recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_WAYPOINT_REC(LoadQueueMedium &paramQueue, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an audio bank to a medium length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramBankName - The name of the audio bank to be queued and loaded.
///    paramOverNetwork - Should this load be made over the nextwork.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_AUDIO_BANK(LoadQueueMedium &paramQueue, STRING paramBankName, BOOL paramOverNetwork = FALSE)
	INT iSettings
	IF paramOverNetwork		
		SET_BIT(iSettings, BIT_LQD_OVER_NETWORK)
	ENDIF
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, iSettings)
ENDPROC


/// PURPOSE: Remove an audio bank that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///    	paramBankName - The name of the audio bank to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the audio bank from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_AUDIO_BANK(LoadQueueMedium &paramQueue, STRING paramBankName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a script to a medium length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramScriptHash - The hash of the script name to be queued and loaded.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_SCRIPT(LoadQueueMedium &paramQueue, INT paramScriptHash)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL")
ENDPROC


/// PURPOSE: Remove a script that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///    	paramScriptHash - The hash of the script name to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the script from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_SCRIPT(LoadQueueMedium &paramQueue, INT paramScriptHash, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a text block to a medium length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramBlock - The text block to be loaded.
///    paramSlot - The text slot to load into.
///    paramClearBriefOnRelease - Should the briefs for text in this block be cleared when we unload the block.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_ADDITIONAL_TEXT(LoadQueueMedium &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramClearBriefOnRelease = FALSE)
	INT iSettings
	IF paramClearBriefOnRelease		
		SET_BIT(iSettings, BIT_LQD_CLEAR_BRIEF)
	ENDIF
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, iSettings)
ENDPROC


/// PURPOSE: Remove a text block that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///    	paramBlock - The text block to be removed.
///   	paramSlot - The text slot to be removed.
///    	paramSetAsNoLongerNeeded - Unload the text block from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_ADDITIONAL_TEXT(LoadQueueMedium &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a PTFX asset to a medium length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Note the script
///    		 needs to have been pre-allocated a PTFX asset for this to do anyting.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    
PROC LOAD_QUEUE_MEDIUM_ADD_PTFX_ASSET(LoadQueueMedium &paramQueue)
	PRIVATE_Load_Queue_Medium_Add_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL")
ENDPROC


/// PURPOSE: Remove a PTFX asset that is in a medium length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a medium load queue data struct.
///    	paramSetAsNoLongerNeeded - Unload the PTFX asset from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_MEDIUM_REMOVE_PTFX_ASSET(LoadQueueMedium &paramQueue, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Medium_Remove_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Query if all requests in a medium length load queue have finished loading.
///    		 Note: Returns TRUE if the queue is empty.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
/// RETURNS: TRUE if all items in the queue have loaded or the queue is empty.
///    
FUNC BOOL HAS_LOAD_QUEUE_MEDIUM_LOADED(LoadQueueMedium &paramQueue)
	//If the queue isn't flagged as loading then return true immediately.
	IF NOT paramQueue.bLoading
		RETURN TRUE
	ENDIF
	
	//If the queue is flagged as loading start checking for items that haven't 
	//been requested or haven't finished loading.
	INT i
	REPEAT LOAD_QUEUE_SIZE_MEDIUM i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
				RETURN FALSE
			ENDIF
			IF NOT PRIVATE_Has_Queued_Load_Loaded(paramQueue.sQueue[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//All items have been requested and have loaded. 
	//Flag the queue as no longer loading a return we are finished.
	paramQueue.bLoading = FALSE
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Medium queue finished loading.")
	RETURN TRUE
ENDFUNC


/// PURPOSE: Releases all items from memory that are listed in a medium length load queue.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
/// 
PROC SET_LOAD_QUEUE_MEDIUM_AS_NO_LONGER_NEEDED(LoadQueueMedium &paramQueue)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue medium as no longer needed.")

	INT i
	REPEAT LOAD_QUEUE_SIZE_MEDIUM i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[i])
		ENDIF
	ENDREPEAT
	
	paramQueue.bLoading = TRUE
ENDPROC


/// PURPOSE: Manages a medium load queue data struct ensuring that requests queued in
///    		 a sequence of one request per frame.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    
PROC UPDATE_LOAD_QUEUE_MEDIUM(LoadQueueMedium &paramQueue)
	IF paramQueue.bLoading
		IF GET_FRAME_COUNT() >= (paramQueue.iLastFrame + paramQueue.iFrameDelay)
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_PAUSED_GAME)) 
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED))	// The game has been paused. Don't allow this to deadlock the load queues.
			INT i
			REPEAT LOAD_QUEUE_SIZE_MEDIUM i
				IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
					IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
						PRIVATE_Make_Queued_Load(paramQueue.sQueue[i])
						paramQueue.iLastFrame = GET_FRAME_COUNT()
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Resets all data stored in a medium length load queue data struct. This
///    		 command will also set all queued items as no longer needed to safeguard
///    		 against them being left in memory.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramLeaveInMemory - Only clear the data out of the struct. Don't unload any loaded assets at the same time.
///  
PROC CLEANUP_LOAD_QUEUE_MEDIUM(LoadQueueMedium &paramQueue, BOOL paramLeaveInMemory = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Cleaning up load queue medium.")
	
	IF NOT paramLeaveInMemory
		SET_LOAD_QUEUE_MEDIUM_AS_NO_LONGER_NEEDED(paramQueue)
	ENDIF
	
	INT i
	REPEAT LOAD_QUEUE_SIZE_MEDIUM i
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[i])
	ENDREPEAT
	
	paramQueue.bLoading = FALSE
	paramQueue.iLastFrame = -1
ENDPROC


/// PURPOSE: Set how many frames to wait in between each queued load request for a medium
///    		 length load queue.
/// PARAMS:
///    paramQueue - An instance of a medium load queue data struct.
///    paramFrameDelay - The number of frames to schedule in between making each queued load request.
///    
PROC SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY(LoadQueueMedium &paramQueue, INT paramFrameDelay)
	CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue medium frame delay of ", paramFrameDelay, ".")
	IF paramFrameDelay > 0
		paramQueue.iFrameDelay = paramFrameDelay
	ELSE
		SCRIPT_ASSERT("SET_LOAD_QUEUE_MEDIUM_FRAME_DELAY: Tried to set invalid frame delay of less than 1. Bug BenR.")
	ENDIF
ENDPROC




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Large Load Queue Commands  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Add a model to a large length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.  
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramModel - The model enum to be queued and loaded.
///    
PROC LOAD_QUEUE_LARGE_ADD_MODEL(LoadQueueLarge &paramQueue, MODEL_NAMES paramModel)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL")
ENDPROC


/// PURPOSE: Remove a model that is in a large length load queue. Choose to unload
///    		 it from memory as we are doing this.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramModel - The model enum to be removed from the queue.
///   
PROC LOAD_QUEUE_LARGE_REMOVE_MODEL(LoadQueueLarge &paramQueue, MODEL_NAMES paramModel, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an animation dictionary to a large length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramDictName - The name of the animation dictionary to be queued and loaded.
///    
PROC LOAD_QUEUE_LARGE_ADD_ANIM_DICT(LoadQueueLarge &paramQueue, STRING paramDictName)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName)
ENDPROC


/// PURPOSE: Remove an animation dictionary that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///  	paramDictName - The name of the animation dictionary to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the anim dict from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_ANIM_DICT(LoadQueueLarge &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_ANIM_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a clip set to a large length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramClipSetName - The name of the clip set to be queued and loaded.
///    
PROC LOAD_QUEUE_LARGE_ADD_CLIP_SET(LoadQueueLarge &paramQueue, STRING paramClipSetName)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName)
ENDPROC


/// PURPOSE: Remove a clip set that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///  	paramClipSetName - The name of the clip set to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the clip set from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_CLIP_SET(LoadQueueLarge &paramQueue, STRING paramClipSetName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_CLIP_SET, -1, paramClipSetName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a texture dictionary to a large length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramDictName - The name of the texture dictionary be queued and loaded.
///    paramPriority - Should this load be given extra priority.
///    
PROC LOAD_QUEUE_LARGE_ADD_TEXT_DICT(LoadQueueLarge &paramQueue, STRING paramDictName, BOOL paramPriority = FALSE)
	INT iSettings
	IF paramPriority		
		SET_BIT(iSettings, BIT_LQD_PRIORITY_TD)
	ENDIF
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, iSettings)
ENDPROC


/// PURPOSE: Remove a texture dictionary that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///  	paramDictName - The name of the texture dictionary be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the texture dictionary from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_TEXT_DICT(LoadQueueLarge &paramQueue, STRING paramDictName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_TEXT_DICT, -1, paramDictName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a vehicle recording to a large length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramFileNo - The number of the vehicle recording file.
///    paramRecName - The root name of the vehicle recording file.
///    
PROC LOAD_QUEUE_LARGE_ADD_VEH_REC(LoadQueueLarge &paramQueue, INT paramFileNo, STRING paramRecName)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName)
ENDPROC


/// PURPOSE: Remove a vehicle recording that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///   	paramFileNo - The number of the vehicle recording file.
///    	paramRecName - The root name of the vehicle recording file.
///    	paramSetAsNoLongerNeeded - Unload the vehicle recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_VEH_REC(LoadQueueLarge &paramQueue, INT paramFileNo, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_VEH_REC, paramFileNo, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a waypoint recording to a large length load queue to be loaded in  
///    		 sequence with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramRecName - The name of the waypoint recording to be queued and loaded.
///    
PROC LOAD_QUEUE_LARGE_ADD_WAYPOINT_REC(LoadQueueLarge &paramQueue, STRING paramRecName)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName)
ENDPROC


/// PURPOSE: Remove a waypoint recording that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///    	paramRecName - The name of the waypoint recording to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the waypoint recording from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_WAYPOINT_REC(LoadQueueLarge &paramQueue, STRING paramRecName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_WAYPOINT_REC, -1, paramRecName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add an audio bank to a large length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramBankName - The name of the audio bank to be queued and loaded.
///    paramOverNetwork - Should this load be made over the nextwork.
///    
PROC LOAD_QUEUE_LARGE_ADD_AUDIO_BANK(LoadQueueLarge &paramQueue, STRING paramBankName, BOOL paramOverNetwork = FALSE)
	INT iSettings
	IF paramOverNetwork		
		SET_BIT(iSettings, BIT_LQD_OVER_NETWORK)
	ENDIF
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, iSettings)
ENDPROC


/// PURPOSE: Remove an audio bank that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///    	paramBankName - The name of the audio bank to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the audio bank from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_AUDIO_BANK(LoadQueueLarge &paramQueue, STRING paramBankName, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_AUDIO_BANK, -1, paramBankName, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a script to a large length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramScriptHash - The hash of the script name to be queued and loaded.
///    
PROC LOAD_QUEUE_LARGE_ADD_SCRIPT(LoadQueueLarge &paramQueue, INT paramScriptHash)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL")
ENDPROC


/// PURPOSE: Remove a script that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///    	paramScriptHash - The hash of the script name to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the script from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_SCRIPT(LoadQueueLarge &paramQueue, INT paramScriptHash, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a text block to a large length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramBlock - The text block to be loaded.
///    paramSlot - The text slot to load into.
///    paramClearBriefOnRelease - Should the briefs for text in this block be cleared when we unload the block.
///    
PROC LOAD_QUEUE_LARGE_ADD_ADDITIONAL_TEXT(LoadQueueLarge &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramClearBriefOnRelease = FALSE)
	INT iSettings
	IF paramClearBriefOnRelease		
		SET_BIT(iSettings, BIT_LQD_CLEAR_BRIEF)
	ENDIF
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, iSettings)
ENDPROC


/// PURPOSE: Remove a text block that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///    	paramBlock - The text block to be removed.
///   	paramSlot - The text slot to be removed.
///    	paramSetAsNoLongerNeeded - Unload the text block from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_ADDITIONAL_TEXT(LoadQueueLarge &paramQueue, STRING paramBlock, TEXT_BLOCK_SLOTS paramSlot, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_ADDIT_TEXT, ENUM_TO_INT(paramSlot), paramBlock, paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a PTFX asset to a large length load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Note the script
///    		 needs to have been pre-allocated a PTFX asset for this to do anyting.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    
PROC LOAD_QUEUE_LARGE_ADD_PTFX_ASSET(LoadQueueLarge &paramQueue)
	PRIVATE_Load_Queue_Large_Add_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL")
ENDPROC


/// PURPOSE: Remove a PTFX asset that is in a large length load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a large load queue data struct.
///    	paramSetAsNoLongerNeeded - Unload the PTFX asset from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LARGE_REMOVE_PTFX_ASSET(LoadQueueLarge &paramQueue, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Large_Remove_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Query if all requests in a large length load queue have finished loading.
///    		 Note: Returns TRUE if the queue is empty.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
/// RETURNS: TRUE if all items in the queue have loaded or the queue is empty.
///    
FUNC BOOL HAS_LOAD_QUEUE_LARGE_LOADED(LoadQueueLarge &paramQueue)
	//If the queue isn't flagged as loading then return true immediately.
	IF NOT paramQueue.bLoading
		RETURN TRUE
	ENDIF
	
	//If the queue is flagged as loading start checking for items that haven't 
	//been requested or haven't finished loading.
	INT i
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
				RETURN FALSE
			ENDIF
			IF NOT PRIVATE_Has_Queued_Load_Loaded(paramQueue.sQueue[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//All items have been requested and have loaded. 
	//Flag the queue as no longer loading a return we are finished.
	paramQueue.bLoading = FALSE
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Large queue finished loading.")
	RETURN TRUE
ENDFUNC


/// PURPOSE: Releases all items from memory that are listed in a large length load queue.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
/// 
PROC SET_LOAD_QUEUE_LARGE_AS_NO_LONGER_NEEDED(LoadQueueLarge &paramQueue)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue large as no longer needed.")

	INT i
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			PRIVATE_Set_Queued_Load_As_No_Longer_Needed(paramQueue.sQueue[i])
		ENDIF
	ENDREPEAT
	
	paramQueue.bLoading = TRUE
ENDPROC


/// PURPOSE: Manages a large load queue data struct ensuring that requests queued in
///    		 a sequence of one request per frame.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    
PROC UPDATE_LOAD_QUEUE_LARGE(LoadQueueLarge &paramQueue)
	IF paramQueue.bLoading
		IF GET_FRAME_COUNT() >= (paramQueue.iLastFrame + paramQueue.iFrameDelay)
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_PAUSED_GAME)) 
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED))	// The game has been paused. Don't allow this to deadlock the load queues.

			INT i
			REPEAT LOAD_QUEUE_SIZE_LARGE i
				IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
					IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
						PRIVATE_Make_Queued_Load(paramQueue.sQueue[i])
						paramQueue.iLastFrame = GET_FRAME_COUNT()
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Resets all data stored in a large length load queue data struct. This
///    		 command will also set all queued items as no longer needed to safeguard
///    		 against them being left in memory.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramLeaveInMemory - Only clear the data out of the struct. Don't unload any loaded assets at the same time.
///  
PROC CLEANUP_LOAD_QUEUE_LARGE(LoadQueueLarge &paramQueue, BOOL paramLeaveInMemory = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Cleaning up load queue large.")
	
	IF NOT paramLeaveInMemory
		SET_LOAD_QUEUE_LARGE_AS_NO_LONGER_NEEDED(paramQueue)
	ENDIF

	INT i
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		PRIVATE_Clean_Up_Load_Queue_Data(paramQueue.sQueue[i])
	ENDREPEAT
	
	paramQueue.bLoading = FALSE
	paramQueue.iLastFrame = -1
	paramQueue.iFrameDelay = 1
ENDPROC


/// PURPOSE: Set how many frames to wait in between each queued load request for a large
///    		 length load queue.
/// PARAMS:
///    paramQueue - An instance of a large load queue data struct.
///    paramFrameDelay - The number of frames to schedule in between making each queued load request.
///    
PROC SET_LOAD_QUEUE_LARGE_FRAME_DELAY(LoadQueueLarge &paramQueue, INT paramFrameDelay)
	CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue large frame delay of ", paramFrameDelay, ".")
	IF paramFrameDelay > 0
		paramQueue.iFrameDelay = paramFrameDelay
	ELSE
		SCRIPT_ASSERT("SET_LOAD_QUEUE_LARGE_FRAME_DELAY: Tried to set invalid frame delay of less than 1. Bug BenR.")
	ENDIF
ENDPROC




//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ Light Load Queue Commands  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Add a model to a light load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Light
///    		 load queues can't hold load requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    paramModel - The model enum to be queued and loaded.
///    
PROC LOAD_QUEUE_LIGHT_ADD_MODEL(LoadQueueLight &paramQueue, MODEL_NAMES paramModel)
	PRIVATE_Load_Queue_Light_Add_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel))
ENDPROC


/// PURPOSE: Remove a model that is in a light load queue. Choose to unload
///    		 it from memory as we are doing this.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    paramModel - The model enum to be removed from the queue.
///   
PROC LOAD_QUEUE_LIGHT_REMOVE_MODEL(LoadQueueLight &paramQueue, MODEL_NAMES paramModel, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Light_Remove_Data(paramQueue, LQT_MODEL, ENUM_TO_INT(paramModel), "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a script to a light load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Light
///    		 load queues can't hold load requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    paramScriptHash - The hash of the script name to be queued and loaded.
///    
PROC LOAD_QUEUE_LIGHT_ADD_SCRIPT(LoadQueueLight &paramQueue, INT paramScriptHash)
	PRIVATE_Load_Queue_Light_Add_Data(paramQueue, LQT_SCRIPT, paramScriptHash)
ENDPROC


/// PURPOSE: Remove a script that is in a light load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a light load queue data struct.
///    	paramScriptHash - The hash of the script name to be removed from the queue.
///    	paramSetAsNoLongerNeeded - Unload the script from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LIGHT_REMOVE_SCRIPT(LoadQueueLight &paramQueue, INT paramScriptHash, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Light_Remove_Data(paramQueue, LQT_SCRIPT, paramScriptHash, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Add a PTFX asset to a light load queue to be loaded in sequence 
///    		 with a number of other load requests across multiple frames. Note the script
///    		 needs to have been pre-allocated a PTFX asset for this to do anyting. Light
///    		 load queues can't hold load requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    
PROC LOAD_QUEUE_LIGHT_ADD_PTFX_ASSET(LoadQueueLight &paramQueue)
	PRIVATE_Load_Queue_Light_Add_Data(paramQueue, LQT_PTFX_ASSET, -1)
ENDPROC


/// PURPOSE: Remove a PTFX asset that is in a light load queue. Choose 
///    		 to unload it from memory as we are doing this.
/// PARAMS:
///		paramQueue - An instance of a light load queue data struct.
///    	paramSetAsNoLongerNeeded - Unload the PTFX asset from memory as we remove it from the queue?
///   
PROC LOAD_QUEUE_LIGHT_REMOVE_PTFX_ASSET(LoadQueueLight &paramQueue, BOOL paramSetAsNoLongerNeeded = FALSE)
	PRIVATE_Load_Queue_Light_Remove_Data(paramQueue, LQT_PTFX_ASSET, -1, "NULL", paramSetAsNoLongerNeeded)
ENDPROC


/// PURPOSE: Query if all requests in a light load queue have finished loading.
///    		 Note: Returns TRUE if the queue is empty. Light load queues can't 
///    		 hold load requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
/// RETURNS: TRUE if all items in the queue have loaded or the queue is empty.
///    
FUNC BOOL HAS_LOAD_QUEUE_LIGHT_LOADED(LoadQueueLight &paramQueue)
	//If the queue isn't flagged as loading then return true immediately.
	IF NOT paramQueue.bLoading
		RETURN TRUE
	ENDIF
	
	//If the queue is flagged as loading start checking for items that haven't 
	//been requested or haven't finished loading.
	INT i
	REPEAT LOAD_QUEUE_SIZE_LIGHT i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
				RETURN FALSE
			ENDIF
			IF NOT PRIVATE_Has_Queued_Load_Loaded_Light(paramQueue.sQueue[i])
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	//All items have been requested and have loaded. 
	//Flag the queue as no longer loading a return we are finished.
	paramQueue.bLoading = FALSE
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Light queue finished loading.")
	RETURN TRUE
ENDFUNC


/// PURPOSE: Releases all items from memory that are listed in a light load queue. 
///    		 Light load queues can't hold load requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
/// 
PROC SET_LOAD_QUEUE_LIGHT_AS_NO_LONGER_NEEDED(LoadQueueLight &paramQueue)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue light as no longer needed.")

	INT i
	REPEAT LOAD_QUEUE_SIZE_LIGHT i
		IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
			PRIVATE_Set_Queued_Load_As_No_Longer_Needed_Light(paramQueue.sQueue[i])
		ENDIF
	ENDREPEAT
	
	paramQueue.bLoading = TRUE
ENDPROC


/// PURPOSE: Manages a light load queue data struct ensuring that requests queued in
///    		 a sequence of one request per frame. Light load queues can't hold load 
///    		 requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    
PROC UPDATE_LOAD_QUEUE_LIGHT(LoadQueueLight &paramQueue)
	IF paramQueue.bLoading
		IF GET_FRAME_COUNT() >= (paramQueue.iLastFrame + paramQueue.iFrameDelay)
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_PAUSED_GAME)) 
		OR IS_BIT_SET(g_replay.iReplayBits, ENUM_TO_INT(RB_REPLAY_SETUP_STARTED))	// The game has been paused. Don't allow this to deadlock the load queues.

			INT i
			REPEAT LOAD_QUEUE_SIZE_LIGHT i
				IF IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_USED)
					IF NOT IS_BIT_SET(paramQueue.sQueue[i].iState, BIT_LQD_REQUESTED)
						PRIVATE_Make_Queued_Load_Light(paramQueue.sQueue[i])
						paramQueue.iLastFrame = GET_FRAME_COUNT()
						EXIT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Resets all data stored in a light load queue data struct. This
///    		 command will also set all queued items as no longer needed to safeguard
///    		 against them being left in memory. Light load queues can't hold load 
///    		 requests that use textal data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    paramLeaveInMemory - Only clear the data out of the struct. Don't unload any loaded assets at the same time.
///  
PROC CLEANUP_LOAD_QUEUE_LIGHT(LoadQueueLight &paramQueue, BOOL paramLeaveInMemory = FALSE)
	CPRINTLN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Cleaning up load queue light.")
	
	IF NOT paramLeaveInMemory
		SET_LOAD_QUEUE_LIGHT_AS_NO_LONGER_NEEDED(paramQueue)
	ENDIF

	INT i
	REPEAT LOAD_QUEUE_SIZE_LARGE i
		PRIVATE_Clean_Up_Load_Queue_Data_Light(paramQueue.sQueue[i])
	ENDREPEAT
	
	paramQueue.bLoading = FALSE
	paramQueue.iLastFrame = -1
	paramQueue.iFrameDelay = 1
ENDPROC


/// PURPOSE: Set how many frames to wait in between each queued load request for a light
///    		 length load queue. Light load queues can't hold load requests that use textal 
///    		 data.
/// PARAMS:
///    paramQueue - An instance of a light load queue data struct.
///    paramFrameDelay - The number of frames to schedule in between making each queued load request.
///    
PROC SET_LOAD_QUEUE_LIGHT_FRAME_DELAY(LoadQueueLight &paramQueue, INT paramFrameDelay)
	CDEBUG1LN(DEBUG_LOAD_QUEUE, "<", GET_THIS_SCRIPT_NAME(), "> Setting load queue light frame delay of ", paramFrameDelay, ".")
	IF paramFrameDelay > 0
		paramQueue.iFrameDelay = paramFrameDelay
	ELSE
		SCRIPT_ASSERT("SET_LOAD_QUEUE_LIGHT_FRAME_DELAY: Tried to set invalid frame delay of less than 1. Bug BenR.")
	ENDIF
ENDPROC
