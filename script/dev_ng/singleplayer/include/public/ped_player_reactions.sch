USING "Commands_entity.sch"
USING "Commands_ped.sch"
USING "script_ped.sch"
USING "commands_fire.sch"
USING "dialogue_public.sch"
USING "rGeneral_include.sch"
USING "usefulCommands.sch"
USING "commands_event.sch"

int iNextArrayToFill

bool bAlertEveryone
bool bHearExplosion
bool bWarningDialoguePLaying
bool bPlayerSeen
bool bAPedIsReacting
int iNextAlertCheckPed
int iWarningDialogueTriggerTime
int highestPriorityDialogue=0, highestPrioritySpeaker = -1
int pedRespondingToAlert = -1
int alertBulletFlag,alertBuddyFlag,alertSeenFlag
int iCheckDefAreasCounter

int reactionDialogueFlag
int lastDialogue,lastSpeaker

float fDefAreaSize
vector vActionCheckCore
float fActionCheckRange

ENUM ALERT_MESSAGE
	ALERT_NONE,
	ALERT_HEAR_PLAYER,	
	ALERT_NEARBY_PED_REACTION,	
	ALERT_EXPLOSION,
	ALERT_SOMEONE_ALERTING_EVERYONE,
	ALERT_DEAD_BODY_SEEN,
	ALERT_PLAYER_SEEN,
	ALERT_BULLET_IMPACT,		
	ALERT_SEEKING,	
	ALERT_PLAYER_FOUND,
	ALERT_COMBAT
ENDENUM

ENUM enum_state	
	state_NONE,
	STATE_SPAWN,
	STATE_RESTART_AMBIENT,
	STATE_AMBIENT,
	STATE_REACT,
	STATE_SEEK,
	STATE_COMBAT,
	STATE_FLEE	
ENDENUM

struct structCheckState
	ped_index id
	ALERT_MESSAGE alertMessage
	ALERT_MESSAGE lastAlertMessage
	enum_state state
	int actionFlag
	int subFlag
	bool bSetToAttackClose //sets if this ped will move to a defensive area close to the player or not. Used to avoid crowding
	bool nearbyreactingPed
	bool canRespondToHearingPlayer
	bool shortDetectionRange = TRUE
	int pedOfInterest
	int timer
	vector vpos
	vector vPosB
	float floatA
	float investigateRange
	bool scriptControlledCombat
	bool scriptReactionFirst
	int nearbyPedA=-1,nearbyPedB=-1
endstruct

struct diaData
	STRING sPedNameForDialogue
	STRING sDialogueToPlay
	int iDiaIndex
endstruct

bool bPlayerMakingNoiseInVehicle
bool bBlockUnseenReactionDialogue
bool bEnableReactionDialogue = TRUE
ped_index pedInSlot[10]
structPedsForConversation CONVSTRUCT
string rConvBlock

int irConvoLastAttmpetTime,	irConvoAttempts

#if is_debug_build
bool ShowSpecifcPedData
int pedDataToShow
#endif
func PED_INDEX ped_in_slot(int slot)
	return pedInSlot[slot]
ENDFUNC

PROC rADD_PED_FOR_DIALOGUE_EXTRA(int speakerIndex, ped_index speaker, string speakerLabel)
	//may be a problem if  aped gets added multiple times in different slots. Fix below:
	
	int iJ
	repeat count_of(pedInSlot) iJ
		if pedInSlot[iJ] = speaker
			//there should never be more than one situation for this
			REMOVE_PED_FOR_DIALOGUE(CONVSTRUCT,iJ)
			pedInSlot[iJ] = null
		endif
	endrepeat
			
	if pedInSlot[speakerIndex] != null
		REMOVE_PED_FOR_DIALOGUE(CONVSTRUCT,speakerIndex)
	ENDIF
	
	//if speakerIndex = 0
		//SCRIPT_ASSERT("Use a non-zero speaker index") //cos this function won't work otherwise
	//ENDIF
	
	ADD_PED_FOR_DIALOGUE(CONVSTRUCT,speakerIndex,speaker, speakerLabel)
	pedInSlot[speakerIndex] = speaker

ENDPROC



PROC REMOVE_INACTIVE_SPEAKERS(string thisConv, int iSp1,int iSp2, int iSp3, int iSp4)
	cprintln(debug_trevor3, isp1," ",isp2," ",isp3," ",isp4)
	int i
	IF IS_STRING_NULL_OR_EMPTY(remlastConv)
	OR NOT ARE_STRINGS_EQUAL(remlastConv,thisConv)
		remlastConv = thisConv
		FOR i = 0 to 15
			IF CONVSTRUCT.PedInfo[i].ActiveInConversation
				IF i != iSp1
				AND i != iSp2
				AND i != iSp3
				AND i != isp4
					CPRINTLN(DEBUG_TREVOR3,"remove pexd at dialogue: ",I," label: ",CONVSTRUCT.PedInfo[i].VoiceID)
					REMOVE_PED_FOR_DIALOGUE(CONVSTRUCT,i)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC


func bool rCREATE_CONVERSATION_EXTRA(string Label, int speakerOne, ped_index pedSpeakerOne, string speakerOneLabel, int speakerTwo=-1, ped_index pedSpeakerTwo=null, string speakerTwoLabel = null, int speakerThree=-1, ped_index pedSpeakerThree=null, string speakerThreeLabel = null, int speakerFour=-1, ped_index pedSpeakerFour=null, string speakerFourLabel = null, enumConversationPriority convPriority = CONV_PRIORITY_HIGH)	
		
		REMOVE_INACTIVE_SPEAKERS(Label,speakerOne,speakerTwo,speakerThree,speakerFour)
				
		rADD_PED_FOR_DIALOGUE_EXTRA(speakerOne,pedSpeakerOne,speakerOneLabel)
	
		if speakerTwo != -1
			rADD_PED_FOR_DIALOGUE_EXTRA(speakerTwo,pedSpeakerTwo,speakerTwoLabel)
		ENDIF
		if speakerThree != -1
			rADD_PED_FOR_DIALOGUE_EXTRA(speakerThree,pedSpeakerThree,speakerThreeLabel)
		ENDIF
		if speakerFour != -1
			rADD_PED_FOR_DIALOGUE_EXTRA(speakerFour,pedSpeakerFour,speakerFourLabel)
		ENDIF
		
		
		
		
		IF NOT IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()
			IF NOT IS_MESSAGE_BEING_DISPLAYED()			
				if CREATE_CONVERSATION(CONVSTRUCT, rConvBlock, Label, convPriority)		
					irConvoAttempts = 0
					return true
				ELSE //backup method in case dialogue just fails to start
					IF irConvoLastAttmpetTime != GET_GAME_TIMER()
						irConvoAttempts++
						irConvoLastAttmpetTime = GET_GAME_TIMER()
						IF irConvoAttempts >= 10
							irConvoAttempts = 0
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF	
				
			else 
				return true //abort
			endif
		else
			return true //abort
		endif
	return FALSE
ENDFUNC

SEQUENCE_INDEX aSequence

proc startSeq()
	OPEN_SEQUENCE_TASK(aSequence)
ENDPROC



proc endSeq(PED_INDEX &aPed, bool MakeRepeat=FALSE)
	
	if MakeRepeat
		SET_SEQUENCE_TO_REPEAT(aSequence,REPEAT_FOREVER)
	ENDIF
	CLOSE_SEQUENCE_TASK(aSequence)
	if not IS_PED_DEAD_OR_DYING(aPed)
		TASK_PERFORM_SEQUENCE(aPed,aSequence)		
	ENDIF
	CLEAR_SEQUENCE_TASK(aSequence)
ENDPROC

func bool IS_CONV_ROOT_PLAYING(string conv_root_to_check)
	TEXT_LABEL_23 txt
	txt = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

	IF ARE_STRINGS_EQUAL(txt,conv_root_to_check)
		RETURN TRUE		
	ENDIF
	RETURN FALSE
endfunc

FUNC float GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(ENTITY_INDEX ent, vector lookAtCoord)
	if DOES_ENTITY_EXIST(ent)
		if not IS_ENTITY_DEAD(ent)
			float fHeading = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(ent,FALSE),lookAtCoord)
			RETURN fHeading-GET_ENTITY_HEADING(ent)	
		endif
	ELSE
		SCRIPT_ASSERT("GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(), entity does not exist.")
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC float GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(ENTITY_INDEX ent, ENTITY_INDEX entLookAt)
	if DOES_ENTITY_EXIST(ent) and DOES_ENTITY_EXIST(entLookAt)
		RETURN GET_RELATIVE_ANGLE_FROM_ENTITY_TO_COORD(ent,GET_ENTITY_COORDS(entLookAt,FALSE))
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC INT DO_TURN_IN_PANIC()

	int i 
	int iLAst=-1
	int iRand
	int iTime,iTotaltime
	CPRINTLN(debug_trevor3,"REACT: DO_TURN_IN_PANIC()")
	TASK_PLAY_ANIM(null,"misschinese2_bank1", "reaction_forward_big_intro_a",SLOW_BLEND_IN,-2,1400)
	for i = 0 to 3
		iRand = GET_RANDOM_INT_IN_RANGE(0,3)
		iTime = GET_RANDOM_INT_IN_RANGE(1100,1800)
		itotalTime += iTime
		IF iRand = iLast
			iRand += 1
			IF iRand > 2
				iRand = 0
			ENDIF
		ENDIF
		iLast=iRand
		SWITCH iRand
			CASE 0
				iRand = GET_RANDOM_INT_IN_RANGE(0,4)
				SWITCH iRAnd
					CASE 0
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@left", "reaction_left_big_intro_a",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 1
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@left", "reaction_left_big_intro_b",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 2
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@left", "reaction_left_big_intro_c",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
				ENDSWITCH
			BREAK
			CASE 1
				iRand = GET_RANDOM_INT_IN_RANGE(0,4)
				SWITCH iRAnd
					CASE 0
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@right", "reaction_right_big_intro_a",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 1
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@right", "reaction_right_big_intro_b",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 2
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@right", "reaction_right_big_intro_c",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
				ENDSWITCH
			BREAK
			CASE 2
				iRand = GET_RANDOM_INT_IN_RANGE(0,4)
				SWITCH iRAnd
					CASE 0
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@backward", "reaction_backward_big_intro_a",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 1
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@backward", "reaction_backward_big_intro_b",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
					CASE 2
						TASK_PLAY_ANIM(null,"reaction@male_stand@big_intro@backward", "reaction_backward_big_intro_c",SLOW_BLEND_IN,-2,iTime)//,AF_NOT_INTERRUPTABLE)											
					BREAK
				ENDSWITCH
			BREAK
			
		ENDSWITCH
	ENDFOR
	RETURN iTotaltime	
	
ENDFUNC

FUNC BOOL DO_TURN_TO_NEARBY_DEAD_PED(int iD, structCheckState& peds[], bool isInSequence = TRUe)
	int iJ
	float fRange = 1000
	float thisRange
	int iClosest = -1
	IF NOT IS_PED_DEAD_OR_DYING(peds[iD].id)
		REPEAT COUNT_OF(peds) ij
			IF IS_PED_DEAD_OR_DYING(peds[iJ].id)
			AND DOES_ENTITY_EXIST(peds[iJ].id)
				thisRange = GET_DISTANCE_BETWEEN_ENTITIES(peds[ij].id,peds[iD].id)
				IF thisRange < fRange
					fRange = thisRange
					iClosest = iJ
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iClosest = -1
			RETURN FALSE
		ELSE	
			peds[iD].floatA = GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(peds[iD].id,peds[iClosest].id)
			IF ABSF(peds[iD].floatA) > 20
				IF isInSequence
					TASK_TURN_PED_TO_FACE_ENTITY(null,peds[iClosest].id,0)							
				ELSE
					TASK_TURN_PED_TO_FACE_ENTITY(peds[iD].id,peds[iClosest].id,0)
				ENDIF
				peds[iD].floatA = GET_HEADING_FROM_ENTITIES(peds[iD].id,peds[iClosest].id)
				RETURN TRUE
			ENDIF		
		ENDIF
			
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT MINMAX(float fValue,float minValue,float maxValue)
	IF fValue < minValue
		RETURN minValue
	ENDIF
	
	IF fValue > maxValue
		RETURN maxValue
	ENDIF
	RETURN fValue
ENDFUNC

FUNC BOOL IS_PED_PLAYING_REACTION_ANIM(ped_index pedID)
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","alert_gunshot")
		RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","buddy_shot_b")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","reaction_forward_big_intro_a")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","alert_body")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","react_forward_small_intro_a")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","react_backward_small_intro_a")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","react_left_small_intro_a")
			RETURN TRUE
	ENDIF
	IF IS_ENTITY_PLAYING_ANIM(pedID,"misschinese2_bank1","react_right_small_intro_a")
			RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_REACTION_ARRAY_ENTRY(ped_index pedID,structCheckState& zped[])
	int i
	REPEAT COUNT_OF(zped) i
		IF zped[i].id = pedID
			RETURN i
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC setReactDialogue(diaData &dataToSet,STRING dialogueConvRoot, int iDiaPedEntry, STRING diaPedName)
	dataToSet.sPedNameForDialogue = diaPedName
	dataToSet.sDialogueToPlay = dialogueConvRoot
	dataToSet.iDiaIndex = iDiaPedEntry
ENDPROC

FUNC BOOL DO_PLAYER_NOISE_REACTION(int pedID, structCheckState& zped[], STRING returnToScenario, diaData dHearPlayer, diaData dHearPlayerAgain, diaData dPlayerNotSeen, bool bAlertEveryoneIfNotSeen)
	

	SWITCH zPED[pedID].subFlag
	
		case 0 //reaction for hearing player	
			IF NOT ARE_STRINGS_EQUAL("chi2_hear",dHearPlayer.sDialogueToPlay)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_CONV_ROOT_PLAYING(dHearPlayer.sDialogueToPlay)
						cprintln(debug_trevor3,"ped_reaction script kill dialogue 1")
						KILL_FACE_TO_FACE_CONVERSATION()			
					ENDIF
				ENDIF
				IF rCREATE_CONVERSATION_EXTRA(dHearPlayer.sDialogueToPlay,dHearPlayer.iDiaIndex,zPED[pedID].id,dHearPlayer.sPedNameForDialogue)
					zPED[pedID].vpos = GET_ENTITY_COORDS(zPED[pedID].id)
					zPED[pedID].floatA = GET_ENTITY_HEADING(zPED[pedID].id)
					zPED[pedID].subFlag = 0	
					zPED[pedID].subFlag++
					zPED[pedID].timer = GET_GAME_TIMER() + 1000							
				ENDIF
			ELSE
				zPED[pedID].vpos = GET_ENTITY_COORDS(zPED[pedID].id)
				zPED[pedID].floatA = GET_ENTITY_HEADING(zPED[pedID].id)			
				zPED[pedID].subFlag++
			ENDIF
		BREAK
		CASE 1
			
			IF GET_GAME_TIMER() > zPED[pedID].timer
				TASK_TURN_PED_TO_FACE_COORD(zped[pedID].id,GET_ENTITY_COORDS(player_ped_id()),5000)
				zPED[pedID].subFlag++
				zPED[pedID].timer = GET_GAME_TIMER() + 5000
			ENDIF
		BREAK
		CASE 2		
			IF NOT ARE_STRINGS_EQUAL("chi2_hear",dHearPlayer.sDialogueToPlay)			
				IF (CAN_PED_HEAR_PLAYER(player_id(),zPED[pedID].id) OR bPlayerMakingNoiseInVehicle=TRUE)
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zped[pedID].id,zPED[pedID].vpos) < zPED[pedID].investigateRange			
					IF GET_GAME_TIMER() > zPED[pedID].timer - 3000
						TASK_FOLLOW_NAV_MESH_TO_COORD(zped[pedID].id,GET_ENTITY_COORDS(player_ped_id()),pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.1) 					
						zPED[pedID].vposB = GET_ENTITY_COORDS(player_ped_id())
						zPED[pedID].subFlag=5
					ENDIF
				ELSE
					IF GET_GAME_TIMER() > zPED[pedID].timer						
						IF rCREATE_CONVERSATION_EXTRA(dPlayerNotSeen.sDialogueToPlay,dPlayerNotSeen.iDiaIndex,zPED[pedID].id,dPlayerNotSeen.sPedNameForDialogue)
							IF bAlertEveryoneIfNotSeen
							//	RETURN TRUE
							ENDIF
							zPED[pedID].subFlag++
						ENDIF									
					ENDIF
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > zPED[pedID].timer
					TASK_ACHIEVE_HEADING(zped[pedID].id,zPED[pedID].floatA,2000)				
					zPED[pedID].subFlag=4
				ENDIF
			ENDIF
		BREAK
		CASE 3
			startSeq()
			TASK_FOLLOW_NAV_MESH_TO_COORD(null,zPED[pedID].vpos,pedmove_walk,DEFAULT_TIME_BEFORE_WARP,0.2,ENAV_DONT_ADJUST_TARGET_POSITION)
			TASK_ACHIEVE_HEADING(null,zPED[pedID].floatA,2000)
			endseq(zped[pedID].id)
			zPED[pedID].timer = GET_GAME_TIMER() + 3000
			zPED[pedID].subFlag++
		BREAK
		CASE 4
			//IF GET_GAME_TIMER() > zPED[pedID].subFlag
			IF GET_SCRIPT_TASK_STATUS(zped[pedID].id,SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK	
			OR (ARE_STRINGS_EQUAL("chi2_hear",dHearPlayer.sDialogueToPlay) AND GET_SCRIPT_TASK_STATUS(zped[pedID].id,SCRIPT_TASK_ACHIEVE_HEADING) = FINISHED_TASK)
				IF NOT ARE_STRINGS_EQUAL(returnToScenario,"")
					TASK_START_SCENARIO_IN_PLACE(zPED[pedID].id,returnToScenario,0,TRUE)					
				ENDIf

				zPED[pedID].actionFlag = 0
				zPED[pedID].subFlag = 0
				zPED[pedID].alertMessage = ALERT_NONE
				zPED[pedID].lastalertMessage = ALERT_NONE
				IF ARE_STRINGS_EQUAL("chi2_hear",dHearPlayer.sDialogueToPlay)
					zPed[pedID].state = STATE_AMBIENT
				ELSE
					zPed[pedID].state = STATE_RESTART_AMBIENT
				ENDIF
				pedRespondingToAlert = -1
			ENDIF
		BREAK
		case 5
			IF rCREATE_CONVERSATION_EXTRA(dHearPlayer.sDialogueToPlay,dHearPlayerAgain.iDiaIndex,zPED[pedID].id,dHearPlayer.sPedNameForDialogue)
				zPED[pedID].subFlag++										
			ENDIF
		BREAK
		CASE 6			
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(zPED[pedID].id,zPED[pedID].vposB) < 2.0
			OR GET_SCRIPT_TASK_STATUS(zped[pedID].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) = FINISHED_TASK	
				IF NOT IS_PED_INJURED(zPED[pedID].id)
					CLEAR_PED_TASKS(zPED[pedID].id)
					startSeq()
						TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[pedID].id,<<-5,3,0>>),2500)
						TASK_LOOK_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(zped[pedID].id,<<5,3,0>>),2500)
					endseq(zped[pedID].id)
					zPED[pedID].timer = GET_GAME_TIMER() + 3000
					zPED[pedID].subFlag=2			
				ENDIF
			ENDIF
		break	
		
		
	endswitch
	RETURN FALSE
ENDFUNC

PROC TERMINATE_REACTIONS()
	UNBLOCK_DECISION_MAKER_EVENT(DECISION_MAKER_DEFAULT, EVENT_INJURED_CRY_FOR_HELP)
ENDPROC

PROC RESET_REACT_ARRAY(structCheckState& peds[],structPedsForConversation &convo, string thisConvBlock, float DefAreaSize, vector vCenterOfAction, float fReactionRange, enum_state defaultState=STATE_AMBIENT)
	int i
	bAPedIsReacting=bAPedIsReacting
	fDefAreaSize = DefAreaSize
	vActionCheckCore = vCenterOfAction
	fActionCheckRange = fReactionRange
	CONVSTRUCT = convo
	rConvBlock = thisConvBlock
	REPEAT COUNT_OF(peds) i
		peds[i].alertMessage = ALERT_NONE
		peds[i].lastAlertMessage = ALERT_NONE
		peds[i].state = defaultState
		peds[i].actionFlag = 0
		peds[i].subFlag = 0
		peds[i].nearbyreactingPed = FALSE
		peds[i].canRespondToHearingPlayer = FALSE
		peds[i].pedOfInterest = 0
		peds[i].timer = 0
		peds[i].vpos = <<0,0,0>>
		peds[i].vPosB = <<0,0,0>>
		peds[i].floatA = 0						
	ENDREPEAT
	
	bAlertEveryone = FALSE
	bHearExplosion = FALSE
	bWarningDialoguePLaying = FALSE
	bPlayerSeen = FALSE
	bAPedIsReacting = FALSE
	bEnableReactionDialogue = TRUE
	bBlockUnseenReactionDialogue = FALSE
	
	iNextAlertCheckPed = 0

	highestPriorityDialogue=0
	highestPrioritySpeaker = -1
	pedRespondingToAlert = -1
	alertBulletFlag = 0
	alertBuddyFlag = 0
	alertSeenFlag = 0
	iCheckDefAreasCounter = 0

	reactionDialogueFlag = 0
	lastDialogue = 0
	lastSpeaker = 0

	BLOCK_DECISION_MAKER_EVENT(DECISION_MAKER_DEFAULT, EVENT_INJURED_CRY_FOR_HELP)
	
	iNextArrayToFill = 0
ENDPROC

PROC ADD_PED_TO_REACT_ARRAY(structCheckState& pedCheckState[], ped_index pedID, bool canHearPlayer, bool scriptControlledCombat = FALSE, bool scriptControlledResponse = FALSE)
	pedCheckState[iNextArrayToFill].id = pedID
	pedCheckState[iNextArrayToFill].canRespondToHearingPlayer = canHearPlayer
	pedCheckState[iNextArrayToFill].scriptControlledCombat = scriptControlledCombat
	pedCheckState[iNextArrayToFill].scriptReactionFirst = scriptControlledResponse
	iNextArrayToFill++	
ENDPROC

PROC ADD_PED_TO_REACT_ARRAY_AT_ENTRY(structCheckState& pedCheckState[], int index, ped_index pedID, bool canHearPlayer, bool scriptControlledCombat = FALSE, bool scriptControlledResponse = FALSE)
	pedCheckState[index].id = pedID
	pedCheckState[index].canRespondToHearingPlayer = canHearPlayer
	pedCheckState[index].scriptControlledCombat = scriptControlledCombat
	pedCheckState[index].scriptReactionFirst = scriptControlledResponse
	IF index > iNextArrayToFill
		iNextArrayToFill = index+1
	ENDIF
	//cprintln(debug_trevor3,"index = ",index," pedID = ",native_to_int(pedCheckState[index].id))
ENDPROC

PROC REMOVE_PED_FROM_REACT_ARRAY(structCheckState& pedCheckState[], int index)
		pedCheckState[index].alertMessage = ALERT_NONE
		pedCheckState[index].lastAlertMessage = ALERT_NONE
		pedCheckState[index].state = state_NONE
		pedCheckState[index].actionFlag = 0
		pedCheckState[index].subFlag = 0
		pedCheckState[index].nearbyreactingPed = FALSE
		pedCheckState[index].canRespondToHearingPlayer = FALSE
		pedCheckState[index].pedOfInterest = 0
		pedCheckState[index].timer = 0
		pedCheckState[index].vpos = <<0,0,0>>
		pedCheckState[index].vPosB = <<0,0,0>>
		pedCheckState[index].floatA = 0
		pedCheckState[index].id = null
ENDPROC

bool bPedJustKilled
int lastLivingCount

PROC DO_ALERT_CHECKS(structCheckState& pedCheckState[],diaData dAlertAllSeen,diaData dAlertAllunSeen, diaData dSeePlayer, diaData dFoundPlayer)
	int i
	//check if any peds reacting - if true, find peds close to that one and inform them of a nearby reacting ped
	
	IF NOT bAlertEveryone = TRUE
		bPedJustKilled = FALSE
		if IS_PED_SHOOTING(PLAYER_PED_ID())
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_ATTACK)
			INT iLivingCount
			REPEAT COUNT_OF(pedCheckState) i
				IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)
					iLivingCount++
				ENDIF
			ENDREPEAT
			IF lastLivingCount = 0
				lastLivingCount = iLivingCount
			ENDIF
			
			IF iLivingCount < lastLivingCount
				lastLivingCount = iLivingCount
				bPedJustKilled = TRUE
			ENDIF
		endif
	
		int iCheckCount = 4
		WHILE iCheckCount > 0	
			IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[iNextAlertCheckPed].id)
				IF pedCheckState[iNextAlertCheckPed].state > STATE_AMBIENT
				AND pedCheckState[iNextAlertCheckPed].alertMessage > ALERT_HEAR_Player
					REPEAT COUNT_OF(pedCheckState) i	
						IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)
						
							IF i != iNextAlertCheckPed
								IF pedCheckState[i].state <= STATE_REACT
									IF GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,pedCheckState[iNextAlertCheckPed].id) < 8
										IF (GET_INTERIOR_FROM_ENTITY(pedCheckState[iNextAlertCheckPed].id) != NULL AND GET_INTERIOR_FROM_ENTITY(pedCheckState[i].id) != NULL)
										OR (GET_INTERIOR_FROM_ENTITY(pedCheckState[iNextAlertCheckPed].id) = NULL AND GET_INTERIOR_FROM_ENTITY(pedCheckState[i].id) = NULL)
											vector v1,v2
											v1 = GET_ENTITY_COORDS(pedCheckState[i].id)
											v2 = GET_ENTITY_COORDS(pedCheckState[iNextAlertCheckPed].id,false)
											if absf(v1.z - v2.z) < 2
												pedCheckState[i].nearbyreactingPed = TRUE					
												pedCheckState[i].pedOfInterest = iNextAlertCheckPed
											endif
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					float closeA,closeB, thisClose, fRelAngle
					closeA = 10
					closeB = 11
					pedCheckState[iNextAlertCheckPed].nearbyPedA = -1
					pedCheckState[iNextAlertCheckPed].nearbyPedB = -1
					REPEAT COUNT_OF(pedCheckState) i	
						IF i != iNextAlertCheckPed
							IF DOES_ENTITY_EXIST(pedCheckState[i].id)
								thisClose = GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,pedCheckState[iNextAlertCheckPed].id)
								IF thisClose < 10
									
									fRelAngle =  GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(pedCheckState[iNextAlertCheckPed].id,pedCheckState[i].id)
									IF fRelAngle > -90 and fRelAngle < 90
										IF thisClose < closeA
											pedCheckState[iNextAlertCheckPed].nearbyPedB = pedCheckState[iNextAlertCheckPed].nearbyPedA
											pedCheckState[iNextAlertCheckPed].nearbyPedA = i
											closeB = closeA
											closeA = thisClose
										ELIF thisClose < closeB
											closeB = thisClose
											pedCheckState[iNextAlertCheckPed].nearbyPedB = i
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					//IF iNextAlertCheckPed = 4 or iNextAlertCheckPed = 5
					//	//cprintln(DEBUG_TREVOR3,"nearby a: ",pedCheckState[iNextAlertCheckPed].nearbyPedA,"nearby b: ",pedCheckState[iNextAlertCheckPed].nearbyPedB)
					//ENDIF
				ENDIF
			ENDIF
			iNextAlertCheckPed++
			If iNextAlertCheckPed >= count_of(pedCheckState)
				iNextAlertCheckPed = 0
			ENDIF
			
			iCheckCount--			
		ENDWHILE
	ENDIF
	
	vehicle_index vehTemp
	bool bPlayerInChopper
	IF IS_PED_IN_ANY_HELI(player_ped_id())
		vehTemp = GET_VEHICLE_PED_IS_IN(player_ped_id())
		IF IS_VEHICLE_DRIVEABLE(vehtemp)
			IF GET_IS_VEHICLE_ENGINE_RUNNING(vehTemp)
				bPlayerInChopper = TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE,vActionCheckCore,fActionCheckRange)	
		bHearExplosion = TRUE
	ENDIF		
	
	
	IF NOT bWarningDialoguePLaying
		IF IS_CONV_ROOT_PLAYING(dAlertAllSeen.sDialogueToPlay)		
		OR IS_CONV_ROOT_PLAYING(dAlertAllunSeen.sDialogueToPlay)
			iWarningDialogueTriggerTime = GET_GAME_TIMER() + 1000
			bWarningDialoguePLaying=TRUE
		ENDIF		
		IF IS_CONV_ROOT_PLAYING(dSeePlayer.sDialogueToPlay)
		OR IS_CONV_ROOT_PLAYING(dFoundPlayer.sDialogueToPlay)
			iWarningDialogueTriggerTime = GET_GAME_TIMER() + 1000
			bWarningDialoguePLaying=TRUE
		ENDIF
	ELSE
		IF NOT IS_CONV_ROOT_PLAYING(dAlertAllSeen.sDialogueToPlay)	
		AND NOT IS_CONV_ROOT_PLAYING(dAlertAllunSeen.sDialogueToPlay)
		AND NOT IS_CONV_ROOT_PLAYING(dSeePlayer.sDialogueToPlay)
		AND NOT IS_CONV_ROOT_PLAYING(dFoundPlayer.sDialogueToPlay)
			bWarningDialoguePLaying=FALSE
		ELSE
			IF GET_GAME_TIMER() > iWarningDialogueTriggerTime
				IF IS_CONV_ROOT_PLAYING(dAlertAllSeen.sDialogueToPlay)	
				OR IS_CONV_ROOT_PLAYING(dAlertAllunSeen.sDialogueToPlay)	
					bAlertEveryone = TRUE
				ENDIF
				IF IS_CONV_ROOT_PLAYING(dSeePlayer.sDialogueToPlay)	
				OR IS_CONV_ROOT_PLAYING(dFoundPlayer.sDialogueToPlay)	
					bPlayerSeen = TRUE
					//bAlertEveryone = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	highestPriorityDialogue = 0
	
	int iClosestPedHearingPlayer = -1
	float fclosestHearingRange = 13
	
	//iClosePedCount=0
	bAPedIsReacting = FALSE
	
	vehicle_index playerVeh
	IF IS_PED_IN_ANY_VEHICLE(player_ped_id())		
		playerVeh = GET_VEHICLE_PED_IS_USING(player_ped_id())						
	ENDIf
	IF pedRespondingToAlert = -1
		bPlayerMakingNoiseInVehicle=FALSE
	ENDIF
	
	REPEAT COUNT_OF(pedCheckState) i
		IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)	
			
			IF pedCheckState[i].state != STATE_FLEE
				//combat range check count deals with player crowding
				/*
				IF pedCheckState[i].state >= STATE_SEEK
					IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),pedCheckState[i].id) < 40
						iClosePedCount++
					ENDIF
				ENDIF
				*/
			
				pedCheckState[i].lastAlertMessage = pedCheckState[i].AlertMessage
				pedCheckState[i].AlertMessage = ALERT_NONE
				IF pedCheckState[i].state <= STATE_REACT
					//hear player
					
					IF pedRespondingToAlert = -1
					
						IF IS_VEHICLE_DRIVEABLE(playerVeh)
							IF GET_IS_VEHICLE_ENGINE_RUNNING(playerVeh)
								bPlayerMakingNoiseInVehicle=TRUE
								fclosestHearingRange = 20
								
							ENDIF
							IF IS_VEHICLE_SIREN_ON(playerVeh)
								bPlayerMakingNoiseInVehicle=TRUE
								fclosestHearingRange = 50
							ENDIF
						
						ENDIF
						
						IF CAN_PED_HEAR_PLAYER(player_id(),pedCheckState[i].id)																	
						OR (IS_VEHICLE_DRIVEABLE(playerVeh) AND GET_IS_VEHICLE_ENGINE_RUNNING(playerVeh))
				
							IF GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),pedCheckState[i].id) < fclosestHearingRange																												
					
								IF 	pedCheckState[i].canRespondToHearingPlayer
									IF GET_INTERIOR_FROM_ENTITY(player_ped_id()) = GET_INTERIOR_FROM_ENTITY(pedCheckState[i].id)
									
									
										fclosestHearingRange = GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),pedCheckState[i].id)
										iClosestPedHearingPlayer = i
										pedCheckState[i].alertMessage = ALERT_HEAR_PLAYER
										pedCheckState[i].investigateRange = fclosestHearingRange + 5
									ENDIF
								ENDIF												
							ENDIF
						ENDIF
					ENDIF
				
					//see/hear nearby ped seeking or reacting
					IF pedCheckState[i].nearbyreactingPed
						pedCheckState[i].nearbyreactingPed=FALSE
						pedCheckState[i].alertMessage = ALERT_NEARBY_PED_REACTION
					ENDIF

					IF bHearExplosion
						pedCheckState[i].alertMessage = ALERT_EXPLOSION
					ENDIF
					
					//Heard Warning
					IF bAlertEveryone
						pedCheckState[i].alertMessage = ALERT_SOMEONE_ALERTING_EVERYONE
					ENDIF
					
					
					
					//hear bullet	
					IF bPedJustKilled
						//cprintln(debug_trevor3,"PEd just killed: ",get_Game_timer())
					ENDIf
					
					IF NOT bPedJustKilled //to stop a ped reacting to another ped being shot nearby.
					AND NOT IS_PED_RAGDOLL(pedCheckState[i].id)
						IF IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_SHOT_FIRED_BULLET_IMPACT)			
							pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
							//cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 1")
						ENDIF
						
						IF HAS_PED_RECEIVED_EVENT(pedCheckState[i].id,EVENT_SHOT_FIRED_BULLET_IMPACT)
							pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
							IF IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_DAMAGE)
								//cprintln(debug_Trevor3,"Im damaged")
							ENDIF
							//cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 2")
						ENDIF
						
						IF pedCheckState[i].shortDetectionRange
							IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(pedCheckState[i].id),4.0,TRUE)
							
								pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
								cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 3a: ",get_Game_timer())
							ENDIF
						ELSE
							IF HAS_BULLET_IMPACTED_IN_AREA(GET_ENTITY_COORDS(pedCheckState[i].id),6.0,TRUE)
								pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
								cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 3b")
							ENDIF
						ENDIF
														
						IF IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_SHOT_FIRED_WHIZZED_BY)
							pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
							cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 4")
						ENDIF
						
						IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedCheckState[i].id),1.0)
							pedCheckState[i].alertMessage = ALERT_BULLET_IMPACT
							cprintln(debug_Trevor3,"ALERT_BULLET_IMPACT 5")
						ENDIF
					ENDIF
					
					//see dead ped
					//if IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_DEAD)										
					//OR HAS_PED_RECEIVED_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_DEAD)
					
					
					IF pedCheckState[i].state < STATE_REACT
						IF pedCheckState[i].nearbyPedA != -1
							IF DOES_ENTITY_EXIST(pedCheckState[pedCheckState[i].nearbyPedA].id)
								IF IS_PED_DEAD_OR_DYING(pedCheckState[pedCheckState[i].nearbyPedA].id)
									IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedCheckState[i].id,pedCheckState[pedCheckState[i].nearbyPedA].id)
										pedCheckState[i].alertMessage = ALERT_DEAD_BODY_SEEN									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF pedCheckState[i].nearbyPedB != -1
							IF DOES_ENTITY_EXIST(pedCheckState[pedCheckState[i].nearbyPedB].id)
								IF IS_PED_DEAD_OR_DYING(pedCheckState[pedCheckState[i].nearbyPedB].id)
									IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedCheckState[i].id,pedCheckState[pedCheckState[i].nearbyPedB].id)
										pedCheckState[i].alertMessage = ALERT_DEAD_BODY_SEEN
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF pedCheckState[i].state <= STATE_SEEK
				
					//in combat / seeking
					If pedCheckState[i].alertMessage = ALERT_NONE
						IF pedCheckState[i].state > STATE_REACT
						OR IS_PED_IN_COMBAT(pedCheckState[i].id)
							pedCheckState[i].alertMessage = ALERT_SEEKING
						ENDIF
					ENDIF
				
					//see player far
					/*
					if IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_HATE)
						cprintln(debug_trevor3,"ALERT A ",i)
					ENDIF
					
					IF IS_PED_IN_COMBAT(pedCheckState[i].id)
						IF CAN_PED_IN_COMBAT_SEE_TARGET(pedCheckState[i].id,PLAYER_PED_ID())
							cprintln(debug_trevor3,"ALERT B ",i)
						ENDIF
					ENDIF
					
					IF (IS_PED_PLAYING_REACTION_ANIM(pedCheckState[i].id) AND HAS_PED_RECEIVED_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_HATE))
						cprintln(debug_trevor3,"ALERT C ",i)
					ENDIF	*/
					
					if IS_PED_RESPONDING_TO_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_HATE)
					or HAS_PED_RECEIVED_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_HATE)
					OR (IS_PED_IN_COMBAT(pedCheckState[i].id) AND CAN_PED_IN_COMBAT_SEE_TARGET(pedCheckState[i].id,PLAYER_PED_ID()))
					OR (IS_PED_PLAYING_REACTION_ANIM(pedCheckState[i].id) AND HAS_PED_RECEIVED_EVENT(pedCheckState[i].id,EVENT_ACQUAINTANCE_PED_HATE))
						IF pedCheckState[i].state > STATE_REACT
							//cprintln(debug_trevor3,"Found you!!!")
							pedCheckState[i].alertMessage = ALERT_PLAYER_FOUND
						ELSE
							//cprintln(debug_trevor3,"See you!!!")
							pedCheckState[i].alertMessage = ALERT_PLAYER_SEEN
						ENDIF
					ENDIF				
				ENDIF
				
				IF bPlayerInChopper
				AND GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),pedCheckState[i].id) < 50
					//cprintln(debug_trevor3,"See you chopper!!!")
					pedCheckState[i].alertMessage = ALERT_PLAYER_SEEN
				ENDIF
				
				IF pedCheckState[i].state <= STATE_COMBAT
					IF bPlayerSeen
						pedCheckState[i].alertMessage = ALERT_COMBAT
					ENDIF
				ENDIF
				
				//ensures only the closest ped hearing the player will react			
				IF pedCheckState[i].alertMessage <= pedCheckState[i].lastAlertMessage
					pedCheckState[i].alertMessage = pedCheckState[i].lastAlertMessage				
				ELSE							
					pedCheckState[i].state = STATE_REACT
					pedCheckState[i].actionFlag = 0				
					pedCheckState[i].subFlag = 0
				ENDIF			
				
				IF enum_to_int(pedCheckState[i].alertMessage) > highestPriorityDialogue
					highestPriorityDialogue = enum_to_int(pedCheckState[i].alertMessage)
					highestPrioritySpeaker = i
					//highestPriorityTriggerTime = GET_GAME_TIMER()
				ENDIF
				
				/*
				IF pedCheckState[i].alertMessage != ALERT_NONE
					IF pedCheckState[i].state != STATE_REACT
						pedCheckState[i].state = STATE_REACT
						pedCheckState[i].actionFlag = 0
						pedCheckState[i].subFlag = -1
					ENDIF
				ENDIF*/
			ELSE
				pedCheckState[i].state = state_NONE
				pedCheckState[i].alertMessage = ALERT_NONE
			ENDIF
			
			IF pedCheckState[i].state > STATE_AMBIENT
				bAPedIsReacting = TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT	
	
	//check if any ped has an alert state and set a bool accordingly...
	
	IF pedRespondingToAlert = -1
		IF iClosestPedHearingPlayer != -1
			IF ALERT_HEAR_PLAYER  > pedCheckState[iClosestPedHearingPlayer].alertMessage
				pedCheckState[iClosestPedHearingPlayer].alertMessage = ALERT_HEAR_PLAYER
				
				pedRespondingToAlert = iClosestPedHearingPlayer
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_DEAD_OR_DYING(pedCheckState[pedRespondingToAlert].id)
			pedRespondingToAlert = -1
		ENDIF
	ENDIF
ENDPROC

PROC DO_REACTIONS(structCheckState& pedCheckState[],vector& vDefAreas[], diaData dHearPlayer,diaData dHearPlayerAgain, diaData dLostPlayer) // diaData dSeePlayer, diaData dHearBullet
	int i
	float fFloatA
	REPEAT COUNT_OF(pedCheckState) i
		IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)
		AND NOT IS_PED_BEING_STEALTH_KILLED(pedCheckState[i].id)	
				IF NOT pedCheckState[i].scriptReactionFirst
					SWITCH pedCheckState[i].state
						CASE STATE_REACT
							IF IS_PED_USING_SCENARIO(pedCheckState[i].id,"PROP_HUMAN_SEAT_CHAIR")
							OR IS_PED_USING_SCENARIO(pedCheckState[i].id,"PROP_HUMAN_SEAT_ARMCHAIR")
							OR GET_SCRIPT_TASK_STATUS(pedCheckState[i].id,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
								//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCheckState[i].id,true)
								cprintln(debug_trevor3,"HIT sync scene")
								IF NOT IS_PED_INJURED(pedCheckState[i].id)
									CLEAR_PED_TASKS(pedCheckState[i].id)	
								ENDIF
								IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)								
									SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedCheckState[i].id)
								ENDIF
							ELIF GET_SCRIPT_TASK_STATUS(pedCheckState[i].id,SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) = finished_task
							
								SWITCH pedCheckState[i].actionFlag
									CASE 0	
										//cprintln(debug_trevor3,"HIT A")
										IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[i].id)												
											//CPRINTLN(debug_trevor3,"Play immediate scenario exit")
											
											SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedCheckState[i].id)
										ENDIF
									
										IF NOT IS_PED_USING_SCENARIO(pedCheckState[i].id,"PROP_HUMAN_SEAT_CHAIR")
										AND NOT IS_PED_USING_SCENARIO(pedCheckState[i].id,"PROP_HUMAN_SEAT_ARMCHAIR")
											//cprintln(debug_trevor3,"HIT B")
											IF IS_PED_IN_ANY_VEHICLE(pedCheckState[i].id)
												cprintln(debug_trevor3,"HIT C")
												vehicle_index vehTemp
												vehTemp = GET_VEHICLE_PED_IS_USING(pedCheckState[i].id)
												IF IS_VEHICLE_DRIVEABLE(vehtemp)
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTemp)
														cprintln(debug_trevor3,"HIT D")
														vector vSpeed
														vSpeed = GET_ENTITY_VELOCITY(vehTemp)
														STOP_PLAYBACK_RECORDED_VEHICLE(vehtemp)
														SET_ENTITY_VELOCITY(vehtemp,vSpeed)		
														TASK_LEAVE_ANY_VEHICLE(pedCheckState[i].id)
													ENDIF
												ENDIF
											ENDIF
											
											IF NOT IS_PED_IN_ANY_VEHICLE(pedCheckState[i].id)
												SWITCH pedCheckState[i].alertMessage
													CASE ALERT_NEARBY_PED_REACTION
														SWITCH pedCheckState[i].subFlag
															CASE 0																	
																IF pedCheckState[i].pedOfInterest >= 0
																	IF DOES_ENTITY_EXIST(pedCheckState[pedCheckState[i].pedOfInterest].id)
																		fFloatA = GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(pedCheckState[i].ID,pedCheckState[pedCheckState[i].pedOfInterest].id)
																		IF fFloatA > -45 and fFloatA < 45
																			pedCheckState[i].subFlag = 1
																		ELIF fFloatA > -135 AND fFloatA <= -45
																			pedCheckState[i].subFlag = 2
																		ELIF fFloatA >= 45 AND fFloatA <= 135
																			pedCheckState[i].subFlag = 3
																		ELSE
																			pedCheckState[i].subFlag = 4
																		ENDIF
																		
																		pedCheckState[i].timer = get_game_timer() + 500
																	ELSE
																		pedCheckState[i].alertMessage = ALERT_NONE
																	ENDIF
																ENDIF										
															BREAK
														
															CASE 1
																IF get_game_timer() > pedCheckState[i].timer
																	startSeq()
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																	TASK_PLAY_ANIM(null,"misschinese2_bank1", "react_forward_small_intro_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].actionFlag++
																ENDIF
															BREAK
															CASE 2
																IF get_game_timer() > pedCheckState[i].timer
																	startSeq()
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																	TASK_PLAY_ANIM(null,"misschinese2_bank1", "react_left_small_intro_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].actionFlag++
																ENDIF
															BREAK
															CASE 3
																IF get_game_timer() > pedCheckState[i].timer
																	startSeq()
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																	TASK_PLAY_ANIM(null,"misschinese2_bank1", "react_right_small_intro_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].actionFlag++
																ENDIF
															BREAK
															CASE 4
																IF get_game_timer() > pedCheckState[i].timer
																	startSeq()
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																	TASK_PLAY_ANIM(null,"misschinese2_bank1", "react_backward_small_intro_a",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].actionFlag++
																ENDIF
															BREAK
														ENDSWITCH
														
													BREAK
													CASE ALERT_BULLET_IMPACT	
														//CPRINTLN(debug_trevor3,"REACT: ALERT BULLET IMPACT: subflag = ",pedCheckState[i].subFlag)
														IF pedCheckState[i].subFlag < 2
														//	CPRINTLN(debug_trevor3,"Clear tasks")
															CLEAR_PED_TASKS(pedCheckState[i].id)
															pedCheckState[i].subFlag = alertBulletFlag
															alertBulletFlag++ //makes sure subsequent peds eon't play the same reaction anim
															IF alertBulletFlag = 2
																alertBulletFlag = 0
															ENDIF
														ENDIF
														
														
														
														SWITCH pedCheckState[i].subFlag
														
															CASE 0
																	SET_CURRENT_PED_WEAPON(pedCheckState[i].id,GET_BEST_PED_WEAPON(pedCheckState[i].id),TRUE)
																	startSeq()					
																	IF NOT IS_PED_IN_ANY_VEHICLE(pedCheckState[i].id,true)
																		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																		TASK_PLAY_ANIM(null,"misschinese2_bank1", "buddy_shot_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)																											
																		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																	ELSE
																		pedCheckState[i].actionFlag++
																	ENDIF
																	TASK_COMBAT_HATED_TARGETS_AROUND_PED(null,200)
																	
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].subFlag = GET_GAME_TIMER() + 3000
																	//pedCheckState[i].actionFlag++
																	cprintln(debug_Trevor3,"BULLET IMPACT ANIM")
															BREAK
															CASE 1
																int itimer
																	startSeq()
																		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,true)
																		itimer = DO_TURN_IN_PANIC()
																		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,false)
																		TASK_COMBAT_HATED_TARGETS_AROUND_PED(null,200)
																	endseq(pedCheckState[i].id)
																	pedCheckState[i].subFlag = GET_GAME_TIMER() + itimer
																	//pedCheckState[i].actionFlag++
																
															BREAK		
															DEFAULT
																cprintln(debug_Trevor3,"Is playing buller anim?")
																IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b")
																	cprintln(debug_Trevor3,"Yes")
																	IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b") < 0.2
																		cprintln(debug_Trevor3,"Update time")
																		SET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b",0.2)
																	ENDIF
																ENDIF
																IF GET_GAME_TIMER() > pedCheckState[i].subFlag
																	pedCheckState[i].actionFlag++
																ENDIF
															BREAK
														ENDSWITCH																										
													BREAK
													
													CASE ALERT_DEAD_BODY_SEEN
													
														IF pedCheckState[i].subFlag <= 2
															pedCheckState[i].subFlag = alertBuddyFlag
															alertBuddyFlag++
															IF alertBuddyFlag = 2
																alertBuddyFlag = 0
															ENDIF
														
														ENDIF
													
														IF NOT IS_PED_IN_ANY_VEHICLE(pedCheckState[i].id,true)
															SWITCH pedCheckState[i].subFlag
																CASE 0											
																	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCheckState[i].id,TRUE)													
																	TASK_PLAY_ANIM(pedCheckState[i].id,"misschinese2_bank1", "alert_gunshot",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_NOT_INTERRUPTABLE)
																	pedCheckState[i].subFlag=3
																BREAK
																CASE 1
																	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCheckState[i].id,TRUE)													
																	TASK_PLAY_ANIM(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_NOT_INTERRUPTABLE)
																	SET_CURRENT_PED_WEAPON(pedCheckState[i].id,GET_BEST_PED_WEAPON(pedCheckState[i].id),TRUE)
																	pedCheckState[i].subFlag=3
																BREAK
																CASE 3 //reacted
																	IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1", "alert_gunshot")
																		IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1", "alert_gunshot") > 0.95			
																			IF DO_TURN_TO_NEARBY_DEAD_PED(i,pedCheckState,FALSE)
																				pedCheckState[i].subFlag=4
																			ELSE
																				TASK_PLAY_ANIM(pedCheckState[i].id,"reaction@male_stand@big_variations@b", "react_big_variations_d",SLOW_BLEND_IN,-2,GET_RANDOM_INT_IN_RANGE(4000,6000),AF_NOT_INTERRUPTABLE)
																				pedCheckState[i].subFlag=6
																			ENDIF
																		ENDIF
																	ENDIF
																	IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b")
																		IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1", "buddy_shot_b") > 0.95			
																			IF DO_TURN_TO_NEARBY_DEAD_PED(i,pedCheckState,FALSE)
																				pedCheckState[i].subFlag=5
																			ELSE
																				TASK_PLAY_ANIM(pedCheckState[i].id,"reaction@male_stand@big_variations@b", "react_big_variations_e",SLOW_BLEND_IN,-2,GET_RANDOM_INT_IN_RANGE(4000,6000),AF_NOT_INTERRUPTABLE)
																				pedCheckState[i].subFlag=6
																			ENDIF
																		ENDIF
																	ENDIF
																BREAK
																case 4
																	IF ABSF(GET_ENTITY_HEADING(pedCheckState[i].id) - pedCheckState[i].floatA) < 10
																		TASK_PLAY_ANIM(pedCheckState[i].id,"reaction@male_stand@big_variations@b", "react_big_variations_d",SLOW_BLEND_IN,-2,GET_RANDOM_INT_IN_RANGE(6000,9000),AF_NOT_INTERRUPTABLE)
																		pedCheckState[i].subFlag=6
																	ENDIF
																break
																case 5
																	IF ABSF(GET_ENTITY_HEADING(pedCheckState[i].id) - pedCheckState[i].floatA) < 10
																		TASK_PLAY_ANIM(pedCheckState[i].id,"reaction@male_stand@big_variations@b", "react_big_variations_f",SLOW_BLEND_IN,-2,GET_RANDOM_INT_IN_RANGE(6000,9000),AF_NOT_INTERRUPTABLE)
																		pedCheckState[i].subFlag=6
																	ENDIF
																break
																case 6
																	IF GET_SCRIPT_TASK_STATUS(pedCheckState[i].id,SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
																		pedCheckState[i].subFlag = 0
																		pedCheckState[i].actionFlag++
																	ENDIF
																break
															ENDSWITCH
														ELSE
															pedCheckState[i].subFlag = 0
															pedCheckState[i].actionFlag++
														ENDIF
													BREAK

													CASE ALERT_SOMEONE_ALERTING_EVERYONE
													
														SWITCH pedCheckState[i].subFlag
															CASE 0
																pedCheckState[i].subFlag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
															BREAK
														
															DEFAULT
																IF GET_GAME_TIMER() > pedCheckState[i].subFlag
																	pedCheckState[i].state = STATE_SEEK
																	pedCheckState[i].alertMessage = ALERT_NONE
																	pedCheckState[i].actionFlag=0
																ENDIF
															BREAK
														ENDSWITCH
													BREAK
																

													CASE ALERT_SEEKING
														IF pedCheckState[i].state != STATE_COMBAT
														AND pedCheckState[i].state != STATE_SEEK
															pedCheckState[i].state = STATE_SEEK
															pedCheckState[i].actionFlag=0
														ENDIF
														//pedCheckState[i].alertMessage = ALERT_NONE //don't uncomment as the ped will lost their seek status
														
											
													BREAK
													
													CASE ALERT_COMBAT
														IF pedCheckState[i].state != STATE_COMBAT
															pedCheckState[i].state = STATE_COMBAT
															//pedCheckState[i].alertMessage = ALERT_NONE //don't uncomment as the ped will lost their seek status
															pedCheckState[i].actionFlag=0
														ENDIF
											
													BREAK

													CASE ALERT_EXPLOSION
														SWITCH pedCheckState[i].subFlag
															CASE 0
																pedCheckState[i].subFlag = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0,2000)
															BREAK
															
															DEFAULT
																IF GET_GAME_TIMER() > pedCheckState[i].subFlag
																	IF pedCheckState[i].state != STATE_COMBAT
																	AND pedCheckState[i].state != STATE_SEEK
																		pedCheckState[i].state = STATE_SEEK	
																		pedCheckState[i].alertMessage = ALERT_NONE
																		pedCheckState[i].actionFlag=0
																	ENDIF
																ENDIF
															BREAK
														ENDSWITCH
													BREAK
													
													CASE ALERT_HEAR_PLAYER
														pedRespondingToAlert = i													
														IF DO_PLAYER_NOISE_REACTION(i,pedCheckState,"",dHearPlayer,dHearPlayerAgain,dLostPlayer,FALSE)
															//if true, it means the ped will alert buddies if the player is seen
															pedCheckState[i].state = STATE_SEEK	
															pedCheckState[i].alertMessage = ALERT_NONE
															pedCheckState[i].actionFlag=0
														ENDIF
													BREAK
													
													CASE ALERT_PLAYER_SEEN
														SWITCH pedCheckState[i].subFlag
															CASE 0
																
																IF alertSeenFlag = 0	
																	IF NOT IS_PED_IN_ANY_VEHICLE(pedCheckState[i].id,true)
																		vector vh1,vh2
																		vh1 = GET_ENTITY_COORDS(pedCheckState[i].id)
																		vh2 = GET_ENTITY_COORDS(player_ped_id())
																		float fVAng
																		fVAng = atan((vh2.z-vh1.z)/GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,player_ped_id(),false))
																		IF fVAng > 20
																			pedCheckState[i].subFlag = 1
																		ELIF fVAng < -20
																			pedCheckState[i].subFlag = 2
																		ELSE
																			pedCheckState[i].subFlag = 3
																		ENDIF
																	ELSE
																		pedCheckState[i].state = STATE_COMBAT
																		pedCheckState[i].alertMessage = ALERT_COMBAT
																		pedCheckState[i].actionFlag=0
																	ENDIF
																	alertSeenFlag++
																
																ELSE
																	pedCheckState[i].state = STATE_COMBAT
																	pedCheckState[i].alertMessage = ALERT_COMBAT
																	pedCheckState[i].actionFlag=0
																ENDIF
															BREAK
															CASE 1
																
																startSeq()
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,TRUE)
																TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),1000)
																TASK_PLAY_ANIM(null,"misschinese2_bank1", "spot_high",SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,FALSE)
																endseq(pedCheckState[i].id)
																pedCheckState[i].subFlag = 4
															BREAK
															CASE 2
																startSeq()
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,TRUE)
																TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),1000)
																TASK_PLAY_ANIM(null,"misschinese2_bank1", "spot_low",SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)//,AF_NOT_INTERRUPTABLE)
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,FALSE)
																endseq(pedCheckState[i].id)
																pedCheckState[i].subFlag = 4
															BREAK
															CASE 3
																startSeq()
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,TRUE)
																TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),1000)
																TASK_PLAY_ANIM(null,"misschinese2_bank1", "spot_mid",SLOW_BLEND_IN,NORMAL_BLEND_OUT,-1)//)//,AF_NOT_INTERRUPTABLE)
																TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(null,FALSE)
																endseq(pedCheckState[i].id)
																pedCheckState[i].subFlag = 4
															BREAK
															CASE 4
																IF bPlayerSeen //ensure the seen dialogue plays out
																	IF absf(GET_RELATIVE_ANGLE_FROM_ENTITY_TO_ENTITY(pedCheckState[i].id,player_ped_id())) < 20
																		IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","spot_high")
																			IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","spot_high") > MINMAX(((GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,player_ped_id())+5)/35.0),0.27,0.9)
																				pedCheckState[i].state = STATE_COMBAT
																				pedCheckState[i].alertMessage = ALERT_COMBAT
																				pedCheckState[i].actionFlag=0
																			ENDIF
																		ENDIF
																		IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","spot_mid")
																			IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","spot_mid") > MINMAX(((GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,player_ped_id())+5)/35.0),0.27,0.9)
																				pedCheckState[i].state = STATE_COMBAT
																				pedCheckState[i].alertMessage = ALERT_COMBAT
																				pedCheckState[i].actionFlag=0
																			ENDIF
																		ENDIF
																		IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","spot_low")
																			IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","spot_low") > MINMAX(((GET_DISTANCE_BETWEEN_ENTITIES(pedCheckState[i].id,player_ped_id())+5)/35.0),0.27,0.9)
																				pedCheckState[i].state = STATE_COMBAT
																				pedCheckState[i].alertMessage = ALERT_COMBAT
																				pedCheckState[i].actionFlag=0
																			ENDIF
																		ENDIF
																	ELSE
																		pedCheckState[i].state = STATE_COMBAT
																		pedCheckState[i].alertMessage = ALERT_COMBAT
																		pedCheckState[i].actionFlag=0
																	ENDIF		
																ENDIF
															BREAK
														ENDSWITCH
																	
																		
														
													BREAK	
												ENDSWITCH
										
								
										ELSE 
											pedCheckState[i].actionFlag++
											
										ENDIF
									ENDIF
								BREAK
								CASE 1
									IF NOT pedCheckState[i].scriptReactionFirst
									
								
									//IF GET_SCRIPT_TASK_STATUS(pedCheckState[i].id,SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
										bool animPlaying
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","alert_gunshot")
											animPlaying=true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","alert_gunshot") > 0.9								
												animPlaying=false
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","buddy_shot_b")
											animPlaying = true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","buddy_shot_b") > 0.9
												animPlaying = false																
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","reaction_forward_big_intro_a")
											animPlaying = true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","reaction_forward_big_intro_a") > 0.9
												animPlaying = false																		
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","alert_body")
											animPlaying = true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","alert_body") > 0.9
												animPlaying = false																		
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","react_forward_small_intro_a")
											animPlaying = true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","react_forward_small_intro_a") > 0.9
												animPlaying=false																		
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","react_backward_small_intro_a")
											animPlaying=true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","react_backward_small_intro_a") > 0.9
												animPlaying=false																	
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","react_left_small_intro_a")
											animPlaying=true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","react_left_small_intro_a") > 0.9
												animPlaying=false																		
											ENDIF
										ENDIF
										IF IS_ENTITY_PLAYING_ANIM(pedCheckState[i].id,"misschinese2_bank1","react_right_small_intro_a")
											animPlaying=true
											IF GET_ENTITY_ANIM_CURRENT_TIME(pedCheckState[i].id,"misschinese2_bank1","react_right_small_intro_a") > 0.9
												animPlaying=false								
											ENDIF
										ENDIF
										if not animPlaying
											pedCheckState[i].actionFlag=2
											pedCheckState[i].subFlag = GET_GAME_TIMER() + 2000
											TASK_COMBAT_PED(pedCheckState[i].id,player_ped_id())
											SET_ENTITY_LOAD_COLLISION_FLAG(pedCheckState[i].id,TRUE)
											SET_PED_TARGET_LOSS_RESPONSE(pedCheckState[i].id,TLR_NEVER_LOSE_TARGET)		
										endif
									ENDIF								
								BREAK
								CASE 2
									IF GET_GAME_TIMER() > pedCheckState[i].subFlag
										pedCheckState[i].state = STATE_SEEK
										pedCheckState[i].alertMessage = ALERT_NONE
										pedCheckState[i].actionFlag=0
									ENDIF
								BREAK
								
							ENDSWITCH
						
							ENDIF
						BREAK
						CASE STATE_SEEK
						FALLTHRU
						CASE STATE_COMBAT
							IF IS_PED_USING_SCENARIO(pedCheckState[i].id,"PROP_HUMAN_SEAT_CHAIR")
							OR IS_PED_USING_ANY_SCENARIO(pedCheckState[i].id)
								IF NOT IS_PED_INJURED(pedCheckState[i].id) //added for bug 1695190
									CLEAR_PED_TASKS(pedCheckState[i].id)		
								ENDIF								
							ELSE								
								SWITCH pedCheckState[i].actionFlag
									CASE 0										
										pedCheckState[i].timer = 0
										IF i = 12
										OR i = 13 
										OR i = 14										
											cprintln(debug_trevor3,"In TO COMBAT ",i)					
										ENDIF
										SET_CURRENT_PED_WEAPON(pedCheckState[i].id,GET_BEST_PED_WEAPON(pedCheckState[i].id),TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCheckState[i].id,false)
									//	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedCheckState[i].id)
										SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(pedCheckState[i].id)
										SET_PED_COMBAT_ATTRIBUTES(pedCheckState[i].id,CA_CAN_CHARGE,TRUE)
										SET_PED_FLEE_ATTRIBUTES(pedCheckState[i].id,FA_NEVER_FLEE,TRUE)
										pedCheckState[i].actionFlag++
										
										//CLEAR_PED_TASKS(pedCheckState[i].id)						
									BREAK
									CASE 1
										if not pedCheckState[i].scriptControlledCombat
											vDefAreas[0] = vDefAreas[0]
											iCheckDefAreasCounter=iCheckDefAreasCounter
											fDefAreaSize=fDefAreaSize
										/*	IF GET_GAME_TIMER() > pedCheckState[i].timer
												
												//check how many peds are set to be close to the player and how many far away.
												int iC
												int iClosePeds
												iClosePeds = 0
												REPEAT COUNT_OF(pedCheckState) iC
													IF iC != i
														IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[iC].id)
															IF pedCheckState[iC].bSetToAttackClose
																iclosePeds++
															ENDIF
														ENDIF
													ENDIF
												ENDREPEAT
												
												IF iClosePeds < 3
													pedCheckState[i].bSetToAttackClose = TRUE
												ENDIF
											
												float fAngPlayer,fAngDef, fAngDiff
												fAngPlayer = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(pedCheckState[i].id),GET_ENTITY_COORDS(player_ped_id()),FALSE)
												int iDefCount
												iDefCount = GET_RANDOM_INT_IN_RANGE(0,10)
												
												int iChosen,iTotalChecks
												iTotalChecks = 0
												iChosen = -1
												
												bool bAbortSearch
												bAbortSearch = FALSE
												
												WHILE iDefCount >= 0
												AND bAbortSearch = FALSE
												
													
													fAngDef = GET_HEADING_FROM_COORDS(GET_ENTITY_COORDS(pedCheckState[i].id),vDefAreas[iCheckDefAreasCounter],FALSE)
													fAngDiff = absf(fAngDef - fAngPlayer)
												//	//cprintln(debug_trevor3,"fAngPlayer = ",fAngPlayer," fAngDef = ",fAngDef," fAngDiff = ",fAngDiff)
													If fAngDiff > 180
														fAngDiff -= 360
														fAngDiff = absf(fAngDiff)
													ENDIF
													
												
												
													
													IF fAngDiff < 70													
														IF (pedCheckState[i].bSetToAttackClose
														and GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vDefAreas[iCheckDefAreasCounter]) < 40)
														OR (pedCheckState[i].bSetToAttackClose = FALSE AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vDefAreas[iCheckDefAreasCounter]) > 40 AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vDefAreas[iCheckDefAreasCounter]) < 90)
															IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedCheckState[i].id,vDefAreas[iCheckDefAreasCounter]) < 50
																iDefCount--													
																iChosen = iCheckDefAreasCounter																							
															ENDIF
														ENDIF
													ENDIF
													
													iCheckDefAreasCounter++
													iTotalChecks++
													IF iCheckDefAreasCounter >= COUNT_OF(vDefAreas)
														iCheckDefAreasCounter=0
													ENDIF
													
													
													
													IF iTotalChecks >= COUNT_OF(vDefAreas)
														bAbortSearch = TRUE								
													ENDIF
													
												ENDWHILE
											
												IF iChosen != -1														
													SET_PED_SPHERE_DEFENSIVE_AREA(pedCheckState[i].id,vDefAreas[iChosen],fDefAreaSize)																												
												ENDIF
												*/
												TASK_COMBAT_PED(pedCheckState[i].id,player_ped_id())
												SET_ENTITY_LOAD_COLLISION_FLAG(pedCheckState[i].id,TRUE)
												SET_PED_TARGET_LOSS_RESPONSE(pedCheckState[i].id,TLR_NEVER_LOSE_TARGET)	
												pedCheckState[i].actionFlag++
												pedCheckState[i].timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(20000,35000)
										//	ENDIF
										endif
									BREAK
								ENDSWITCH
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			
		ENDIF
	ENDREPEAT
ENDPROC

PROC DO_DIALOGUE(structCheckState& pedCheckState[], diaData dHearBullet, diaData dSeePlayer, diaData dFoundPlayer, diaData dAlertAllseen,diaData dAlertAllunseen , diaData dDeadBody, diaData dExplosion)
	IF IS_GAMEPLAY_CAM_RENDERING()
		IF bEnableReactionDialogue
			IF highestPriorityDialogue > lastDialogue
			OR lastSpeaker != highestPrioritySpeaker
			OR (highestPriorityDialogue < lastDialogue AND highestPriorityDialogue = enum_to_int(ALERT_SEEKING))
			OR (highestPriorityDialogue = enum_to_int(ALERT_COMBAT) AND bAlertEveryone = FALSE)
				reactionDialogueFlag = 1
				lastDialogue = highestPriorityDialogue
				lastSpeaker = highestPrioritySpeaker
			ENDIF	
				
			SWITCH reactionDialogueFlag
				CASE 1
					IF highestPrioritySpeaker > -1
						IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[highestPrioritySpeaker].id)		
							SWITCH int_to_enum(ALERT_MESSAGE,highestPriorityDialogue)							
								
								CASE ALERT_SEEKING
									IF NOT bAlertEveryone
										IF NOT bPlayerSeen
											IF NOT bBlockUnseenReactionDialogue
												IF NOT IS_CONV_ROOT_PLAYING(dAlertAllunseen.sDialogueToPlay)
													cprintln(debug_trevor3,"ped_reaction script kill dialogue 2")
													KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
													IF rCREATE_CONVERSATION_EXTRA(dAlertAllunseen.sDialogueToPlay,dAlertAllunseen.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dAlertAllunseen.sPedNameForDialogue)																													
														//cprintln(DEBUG_TREVOR3,"a) dAlertAllunseen")
														reactionDialogueFlag=2
													ENDIF
												ENDIF
											ELSE
												reactionDialogueFlag=2
											ENDIF
										else
											IF NOT IS_CONV_ROOT_PLAYING(dAlertAllseen.sDialogueToPlay)
												cprintln(debug_trevor3,"ped_reaction script kill dialogue 3")
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
												IF rCREATE_CONVERSATION_EXTRA(dAlertAllseen.sDialogueToPlay,dAlertAllseen.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dAlertAllseen.sPedNameForDialogue)																													
													//cprintln(DEBUG_TREVOR3,"b) dAlertAllunseen")
													reactionDialogueFlag=2
												ENDIF
											ENDIF
										endif
									ELSE
										reactionDialogueFlag=2
									ENDIF
								BREAK
						
								CASE ALERT_BULLET_IMPACT
									cprintln(debug_trevor3,"ped_reaction script kill dialogue 4")
									bBlockUnseenReactionDialogue=FALSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF rCREATE_CONVERSATION_EXTRA(dHearBullet.sDialogueToPlay,dHearBullet.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dHearBullet.sPedNameForDialogue)													
										//cprintln(DEBUG_TREVOR3,"c) dHearBullet")
										//SET_CONDITION_STATE(COND_ENEMY_PED_REACTING,TRUE)
										reactionDialogueFlag=2
									ENDIF
								BREAK
							
								
								CASE ALERT_DEAD_BODY_SEEN
									cprintln(debug_trevor3,"ped_reaction script kill dialogue 5")
									bBlockUnseenReactionDialogue = FALSE
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF rCREATE_CONVERSATION_EXTRA(dDeadBody.sDialogueToPlay,dDeadBody.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dDeadBody.sPedNameForDialogue)
										//cprintln(DEBUG_TREVOR3,"d) dDeadBody")
										//SET_CONDITION_STATE(COND_ENEMY_PED_REACTING,TRUE)
										reactionDialogueFlag=2
									ENDIF
								BREAK
								CASE ALERT_EXPLOSION										
									IF NOT bAlertEveryone	
									AND NOT bBlockUnseenReactionDialogue
										cprintln(debug_trevor3,"ped_reaction script kill dialogue 6")
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										
										IF rCREATE_CONVERSATION_EXTRA(dExplosion.sDialogueToPlay,dExplosion.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dExplosion.sPedNameForDialogue)										
											//cprintln(DEBUG_TREVOR3,"e) dExplosion")
											reactionDialogueFlag=2
										ENDIF									
									ELSE
										reactionDialogueFlag=2
									ENDIF										
								BREAK
								
								CASE ALERT_PLAYER_SEEN										
									IF NOT bPlayerSeen											
										IF NOT IS_CONV_ROOT_PLAYING(dSeePlayer.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dFoundPlayer.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dAlertAllseen.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dAlertAllunseen.sDialogueToPlay)
											bBlockUnseenReactionDialogue=FALSE
											cprintln(debug_trevor3,"ped_reaction script kill dialogue 7")
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											IF rCREATE_CONVERSATION_EXTRA(dSeePlayer.sDialogueToPlay,dSeePlayer.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dSeePlayer.sPedNameForDialogue)											
												reactionDialogueFlag=2
											ENDIF	
										ENDIF
									else
										reactionDialogueFlag=2
									endif
									
								BREAK		
								
								CASE ALERT_COMBAT
								FALLTHRU
								CASE ALERT_PLAYER_FOUND
									IF NOT bPlayerSeen											
										IF NOT IS_CONV_ROOT_PLAYING(dSeePlayer.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dFoundPlayer.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dAlertAllseen.sDialogueToPlay)
										AND NOT IS_CONV_ROOT_PLAYING(dAlertAllunseen.sDialogueToPlay)
											cprintln(debug_trevor3,"ped_reaction script kill dialogue 8")
											bBlockUnseenReactionDialogue=FALSE
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											IF rCREATE_CONVERSATION_EXTRA(dFoundPlayer.sDialogueToPlay,dFoundPlayer.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dFoundPlayer.sPedNameForDialogue)
												//cprintln(DEBUG_TREVOR3,"g) dFoundPlayer")
												reactionDialogueFlag=2
											ENDIF	
										ENDIF
									else
										IF NOT bAlertEveryone
										
											IF rCREATE_CONVERSATION_EXTRA(dalertAllSeen.sDialogueToPlay,dalertAllSeen.iDiaIndex,pedCheckState[highestPrioritySpeaker].id,dalertAllSeen.sPedNameForDialogue)											
												bBlockUnseenReactionDialogue = FALSE
												//cprintln(DEBUG_TREVOR3,"h) dalertAllSeen")
												reactionDialogueFlag=2
											ENDIF
										ENDIF
										reactionDialogueFlag=2
									endif
								BREAK
								
							ENDSWITCH
						ELSE
							reactionDialogueFlag = 2
						ENDIF
					ELSE
						reactionDialogueFlag = 2
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						reactionDialogueFlag = 0			
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

ENDPROC

PROC handlePedReactions(structCheckState& pedCheckState[], vector& vDefAreas[], diaData dHearPlayer,diaData dHearPlayerAgain, diaData dLostPlayer, diaData dHearBullet, diaData dSeePlayer, diaData dFoundPlayer, diaData dAlertAllSeen,diaData dAlertAllUnSeen , diaData dDeadBody, diaData dExplosion)
	IF iNextArrayToFill > 0
		DO_ALERT_CHECKS(pedCheckState,dAlertAllSeen,dAlertAllUnSeen,dSeePlayer,dFoundPlayer)
		DO_REACTIONS(pedCheckState,vDefAreas,dHearPlayer,dHearPlayerAgain,dLostPlayer)
		DO_DIALOGUE(pedCheckState,dHearBullet,dSeePlayer,dFoundPlayer, dAlertAllSeen,dAlertAllUnSeen,dDeadBody,dExplosion)
	ENDIF
ENDPROC

#if is_debug_build

	bool bShowAlerts
	WIDGET_GROUP_ID newWidget

	FUNC STRING GET_ALERTED_STRING(structCheckState& pedCheckState[], int iPed, bool bShowLastAlert=FALSE)
		ALERT_MESSAGE checkAlert
		IF bShowLastAlert = FALSE
			checkAlert = pedCheckState[iPed].alertMessage
		ELSE
			checkAlert = pedCheckState[iPed].lastAlertMessage
		ENDIF
		switch checkAlert
			case ALERT_NONE RETURN "ALERT_NONE"
			case ALERT_NEARBY_PED_REACTION RETURN "ALERT_NEARBY_PED_REACTION"
			case ALERT_BULLET_IMPACT RETURN "ALERT_BULLET_IMPACT"
			case ALERT_DEAD_BODY_SEEN RETURN "ALERT_DEAD_BODY_SEEN"	
			case ALERT_EXPLOSION RETURN "ALERT_EXPLOSION"
			case ALERT_PLAYER_SEEN RETURN "ALERT_PLAYER_SEEN"		
			case ALERT_HEAR_PLAYER  RETURN "ALERT_HEAR_PLAYER"
			case ALERT_SEEKING  RETURN "ALERT_SEEKING"	
			case ALERT_SOMEONE_ALERTING_EVERYONE  RETURN "ALERT_SOMEONE_ALERTING_EVERYONE"
			case ALERT_PLAYER_FOUND RETURN "ALERT_PLAYER_FOUND"
			CASE ALERT_COMBAT RETURN "ALERT_COMBAT"
		ENDSWITCH
		return ""
	ENDFUNC
	
	FUNC STRING GET_STATE_STRING(structCheckState& pedCheckState[], int iPED)
		SWITCH pedCheckState[iPed].state
			CASE STATE_AMBIENT
				RETURN("STATE_AMBIENT")
			BREAK
			CASE STATE_COMBAT
				RETURN("STATE_COMBAT")
			BREAK
			CASE STATE_REACT
				RETURN("STATE_REACT")
			BREAK
			CASE STATE_SEEK
				RETURN("STATE_SEEK")
			BREAK
			CASE STATE_SPAWN
				RETURN("STATE_SPAWN")
			BREAK
		ENDSWITCH
		return("")
	ENDFUNC
	
	PROC pedReactionsDebug(structCheckState& pedCheckState[], WIDGET_GROUP_ID &thisWidget)
		int iR
		IF DOES_WIDGET_GROUP_EXIST(thisWidget)
			IF NOT DOES_WIDGET_GROUP_EXIST(newWidget)
				//cprintln(DEBUG_TREVOR3,"PEd reactions version 1.0")
				SET_CURRENT_WIDGET_GROUP(thisWidget)
				newWidget = START_WIDGET_GROUP("Ped Reactions")
					ADD_WIDGET_BOOL("bShowAlerts",bShowAlerts)
					ADD_WIDGET_BOOL("Show ped data",ShowSpecifcPedData)
					ADD_WIDGET_INT_SLIDER("PEd to show",pedDataToShow,0,COUNT_OF(pedCheckState),1)
					

					START_WIDGET_GROUP("Dialogue")
						ADD_WIDGET_INT_READ_ONLY("highestPriorityDialogue",highestPriorityDialogue)
						ADD_WIDGET_INT_READ_ONLY("lastDialogue",lastDialogue)
						ADD_WIDGET_INT_READ_ONLY("reactionDialogueFlag",reactionDialogueFlag)
						ADD_WIDGET_INT_READ_ONLY("highestPrioritySpeaker",highestPrioritySpeaker)					
						ADD_WIDGET_BOOL("bWarningDialoguePLaying",bWarningDialoguePLaying)
						ADD_WIDGET_BOOL("bAPedIsReacting",bAPedIsReacting)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()			
				CLEAR_CURRENT_WIDGET_GROUP(thisWidget)
			ENDIF
		ENDIF
		/*
		peds[i].alertMessage = ALERT_NONE
		peds[i].lastAlertMessage = ALERT_NONE
		peds[i].state = STATE_AMBIENT
		peds[i].actionFlag = 0
		peds[i].subFlag = 0
		peds[i].nearbyreactingPed = FALSE
		peds[i].canRespondToHearingPlayer = FALSE
		peds[i].pedOfInterest = 0
		peds[i].timer = 0
		peds[i].vpos = <<0,0,0>>
		peds[i].vPosB = <<0,0,0>>
		peds[i].floatA = 0
		*/
		IF bShowAlerts
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			DRAW_RECT_FROM_CORNER(0.03,0.08,0.3,0.66,0,0,0,180)
			float fx,fy
			
			TEXT_LABEL_63 txtlbl
			
			REPEAT COUNT_OF(pedCheckState) iR
				txtlbl = ""
				txtlbl += GET_ALERTED_STRING(pedCheckState,iR) 
				txtlbl += " "
				txtlbl += GET_STATE_STRING(pedCheckState,iR)
				txtlbl += " "
				txtlbl += pedCheckState[iR].actionFlag
				txtlbl += " "
				txtlbl += pedCheckState[iR].subFlag
				txtlbl += " "
				IF pedCheckState[iR].scriptReactionFirst
					txtlbl += " TRUE"
				ELSE
					txtlbl += " FALSE"
				ENDIF
		
				
				SET_TEXT_SCALE(0.3,0.3)
				IF IS_PED_DEAD_OR_DYING(pedCheckState[iR].id)
					SET_TEXT_COLOUR(255,0,0,255)
				ELSE
					SET_TEXT_COLOUR(255,255,255,255)
				ENDIF
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.04,0.1+(iR*0.02),"STRING",txtlbl)
				SET_TEXT_COLOUR(255,255,255,255)
				//DISPLAY_TEXT_WITH_LITERAL_STRING(0.04,0.1+(iR*0.02),"STRING",GET_ALERTED_STRING(pedCheckState,iR))
				//SET_TEXT_SCALE(0.3,0.3)
				//DISPLAY_TEXT_WITH_LITERAL_STRING(0.19,0.1+(iR*0.02),"STRING",GET_STATE_STRING(pedCheckState,iR))
				//SET_TEXT_SCALE(0.3,0.3)
				//showIntOnScreen(0.28,0.1+(iR*0.02),pedCheckState[iR].actionFlag)
				//showIntOnScreen(0.3,0.1+(iR*0.02),pedCheckState[iR].subFlag)
				IF DOES_ENTITY_EXIST(pedCheckState[iR].id)
					IF GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(pedCheckState[iR].id,FALSE),fx,fy)
						DRAW_DEBUG_LINE_2D(<<0.312,0.12+(iR*0.02),0>>,<<fx,fy,0>>)
						DRAW_DEBUG_LINE_2D(<<0.312,0.12+(iR*0.02),0>>,<<0,0.12+(iR*0.02),0>>)
					ENDIF
				ENDIF
			ENDREPEAT
			SET_TEXT_SCALE(0.3,0.3)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.04,0.65,"STRING","bAlertEveryone: ")
			SET_TEXT_SCALE(0.3,0.3)
			IF bAlertEveryone
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.65,"STRING","YES")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.65,"STRING","NO")
			ENDIF
			
			SET_TEXT_SCALE(0.3,0.3)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.04,0.67,"STRING","bPlayerSeen: ")
			SET_TEXT_SCALE(0.3,0.3)
			IF bPlayerSeen
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.67,"STRING","YES")
			ELSE
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.11,0.67,"STRING","NO")
			ENDIF
		ENDIF
		
		IF ShowSpecifcPedData
			cprintln(debug_Trevor3,"Alert message: ",GET_ALERTED_STRING(pedCheckState,pedDataToShow)," last: ",GET_ALERTED_STRING(pedCheckState,pedDataToShow,true)," state: ",GET_STATE_STRING(pedCheckState,pedDataToShow)," action flag: ",pedCheckState[pedDataToShow].actionFlag," sub flag: ",pedCheckState[pedDataToShow].subFlag)
			IF NOT IS_PED_DEAD_OR_DYING(pedCheckState[pedDataToShow].id)
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedCheckState[pedDataToShow].id)+<<0,0,1>>,0.3)
			ENDIF
		ENDIF
	ENDPROC
#endif

