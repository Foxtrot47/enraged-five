//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   buddy_head_track_public.sch                                 //
//      AUTHOR          :   Rob Bray                                              		//
//      DESCRIPTION     :   Handles buddies head tracking the player            		//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

// make a buddy head track you if you are entering vehicle
PROC HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		VEHICLE_INDEX enteringVehicle = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(enteringVehicle)
			PED_INDEX buddyPed = GET_PED_IN_VEHICLE_SEAT(enteringVehicle, VS_FRONT_RIGHT)
			IF NOT IS_PED_INJURED(buddyPed)
				IF buddyPed <> PLAYER_PED_ID()
					IF IS_ENTITY_A_MISSION_ENTITY(buddyPed)
						IF NOT IS_PED_HEADTRACKING_ENTITY(buddyPed, PLAYER_PED_ID())
							TASK_LOOK_AT_ENTITY(buddyPed, PLAYER_PED_ID(), 2000, SLF_WHILE_NOT_IN_FOV)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// cancel buddy permanent head track
PROC CANCEL_BUDDY_PERMANENT_HEAD_TRACK(PED_INDEX buddyPed)
	IF NOT IS_PED_INJURED(buddyPed)
		IF IS_PED_HEADTRACKING_ENTITY(buddyPed, PLAYER_PED_ID())
			TASK_CLEAR_LOOK_AT(buddyPed)
		ENDIF
	ENDIF
ENDPROC

// make a buddy head track you permanently
PROC HANDLE_BUDDY_PERMANENT_HEAD_TRACK(PED_INDEX buddyPed, BOOL bOnFootOnly = TRUE)
	IF NOT IS_PED_INJURED(buddyPed)
		BOOL bInSameVehicle = FALSE
		
		IF bOnFootOnly
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_SITTING_IN_ANY_VEHICLE(buddyPed)
				IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) = GET_VEHICLE_PED_IS_IN(buddyPed)
					bInSameVehicle = TRUE
				ENDIF
			ENDIF
		ELSE
			bInSameVehicle = FALSE
		ENDIF
	
		IF NOT IS_PED_HEADTRACKING_ENTITY(buddyPed, PLAYER_PED_ID())
			IF NOT bInSameVehicle
				TASK_LOOK_AT_ENTITY(buddyPed, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV | SLF_SLOW_TURN_RATE)
			ENDIF
		ELSE
			IF bInSameVehicle
				IF NOT IS_PED_IN_CURRENT_CONVERSATION(buddyPed)
					TASK_CLEAR_LOOK_AT(buddyPed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// make a buddy react to a wanted level by setting them into action mode
PROC SET_BUDDY_ACTION_MODE(PED_INDEX buddyPed, BOOL bSet, BOOL &bBuddyActionModeTrack, INT &iBuddyActionModeDelay)
	IF NOT IS_PED_INJURED(buddyPed)
		IF bSet
			SET_PED_USING_ACTION_MODE(buddyPed, TRUE, -1, "DEFAULT_ACTION")
			iBuddyActionModeDelay = 0
			bBuddyActionModeTrack = TRUE
		ELSE
			SET_PED_USING_ACTION_MODE(buddyPed, FALSE)
			iBuddyActionModeDelay = 0
			bBuddyActionModeTrack = FALSE
		ENDIF
	ENDIF
ENDPROC

// handle buddy action mode when wanted
PROC HANDLE_BUDDY_ACTION_MODE_WHILE_WANTED(PED_INDEX buddyPed, BOOL &bBuddyActionModeTrack, INT &iBuddyActionModeDelay)
	IF NOT IS_PED_INJURED(buddyPed)
		IF bBuddyActionModeTrack
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF iBuddyActionModeDelay = 0
					iBuddyActionModeDelay = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(200,800)
				ELSE
					IF GET_GAME_TIMER() >= iBuddyActionModeDelay
						SET_BUDDY_ACTION_MODE(buddyPed, FALSE, bBuddyActionModeTrack, iBuddyActionModeDelay)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				IF iBuddyActionModeDelay = 0
					iBuddyActionModeDelay = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000,4000)
				ELSE
					IF GET_GAME_TIMER() >= iBuddyActionModeDelay
						SET_BUDDY_ACTION_MODE(buddyPed, TRUE, bBuddyActionModeTrack, iBuddyActionModeDelay)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC
