USING "ambience_run_checks.sch"
USING "family_private.sch"
USING "family_flow_public.sch"

///public interface for family scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FAMILY PUBLIC RUNNING FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL IS_FAMILY_SCENE_ALLOWED_TO_RUN(enumCharacterList ePed)
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_PLAYER_PLAYING", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
	OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY_FRIENDS)
	OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY_PREP)
	OR IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR))
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			IF GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE-", GET_MISSION_TYPE_DEBUG_STRING(g_OnMissionState), "]	// switch not short range")
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE-", GET_MISSION_TYPE_DEBUG_STRING(g_OnMissionState), "]	// not switching")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_MISSION_TRIGGER_ACTIVE()
	
	#IF IS_DEBUG_BUILD
	AND NOT g_bUpdatedFamilyEvents
	AND NOT g_bDrawDebugFamilyStuff
	#ENDIF
	
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			IF GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_MISSION_TRIGGER_ACTIVE", "]	// switch not short range")
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_MISSION_TRIGGER_ACTIVE", "]	// not switching")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF (GET_MISSION_FLAG())
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "GET_MISSION_FLAG()", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF (GET_RANDOM_EVENT_FLAG() AND g_bRandomEventActive)
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "(GET_RANDOM_EVENT_FLAG() AND g_bRandomEventActive)", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_THE_CURRENT_PLAYER_PED(ePed)
//		RETURN FALSE
	ENDIF
	
	#IF NOT USE_CLF_DLC
	#IF NOT USE_NRM_DLC
	
	#IF IS_DEBUG_BUILD
	IF NOT g_bUpdatedFamilyEvents
	AND NOT g_bDrawDebugFamilyStuff
	#ENDIF
		IF NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "(STRAND_PROLOGUE() not activated)", "]")
			#ENDIF
			
			RETURN FALSE
		ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	#ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(enumCharacterList ePed, VECTOR vFamilySceneCoord, FLOAT fDistMult = 1.0)
	IF NOT IS_FAMILY_SCENE_ALLOWED_TO_RUN(ePed)
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "IS_FAMILY_SCENE_ALLOWED_TO_RUN", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vFamilySceneCoord) >= (fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT*fDistMult)
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "GET_DISTANCE_BETWEEN_COORDS", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF g_bSavebedCompleteSuccessfully
		
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> family scene NOT allowed to run[", "g_bSavebedCompleteSuccessfully", "]")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	FAMILY PUBLIC FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(enumFamilyMember eFamilyMember)
	
	
	IF g_flowUnsaved.bFlowControllerBusy
		WHILE g_flowUnsaved.bFlowControllerBusy
	
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(", Get_String_From_FamilyMember(eFamilyMember), ") - bFlowControllerBusy...")
			#ENDIF
			
			WAIT(0)
		ENDWHILE
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(", Get_String_From_FamilyMember(eFamilyMember), ")", " [g_eLastMissionPassed: ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(g_eLastMissionPassed, TRUE), ", ", g_iLastMissionPassedGameTime - GET_GAME_TIMER(), "]")
	#ENDIF
	
	IF g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
		
		#IF IS_DEBUG_BUILD
		IF g_bUpdatedFamilyEvents
		AND g_bDebugForceCreateFamily
			IF (g_eDebugSelectedMember <> eFamilyMember)
				CPRINTLN(DEBUG_FAMILY, "g_eDebugSelectedMember <> ", Get_String_From_FamilyMember(eFamilyMember), " while g_bDebugForceCreateFamily")
				
				RETURN FALSE
			ENDIF
		ENDIF
		#ENDIF
		
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS")
		#ENDIF
		
		enumFamilyEvents eDesiredFamilyEvent
		
		IF PRIVATE_Get_Desired_FamilyMember_Event(eFamilyMember, eDesiredFamilyEvent)

			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eDesiredFamilyEvent), ") = TRUE, set current event to desired event", " [GET_CLOCK_HOURS:", GET_CLOCK_HOURS(), ":00]")
			#ENDIF
			
			PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, eDesiredFamilyEvent)
			RETURN TRUE
		
		else
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "PRIVATE_Get_Desired_FamilyMember_Event(", Get_String_From_FamilyMember(eFamilyMember), ", ", Get_String_From_FamilyEvent(eDesiredFamilyEvent), ") = FALSE, DONT set current event to desired event", " [GET_CLOCK_HOURS:", GET_CLOCK_HOURS(), "]")
			#ENDIF
			
		ENDIF
	
	else
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
		#ENDIF
	
	ENDIF
	
	RETURN FALSE
ENDFUNC



// *******************************************************************************************
//	FAMILY PUBLIC FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL IS_EVENT_VALID_FOR_FAMILY_MEMBER(enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)

	SWITCH eFamilyEvent
	
		CASE FE_M_FAMILY_on_laptops
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_FAMILY_MIC4_locked_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M7_FAMILY_finished_breakfast
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_finished_pizza
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_FAMILY_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_SON_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_SON_gaming_loop
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_SON_gaming_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M7_SON_gaming_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_SON_rapping_in_the_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Borrows_sisters_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_watching_porn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_in_room_asks_for_munchies
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_phone_calls_in_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_on_ecstasy_AND_friendly
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_A
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_B
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_C
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_Fighting_with_sister_D
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_smoking_weed_in_a_bong
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_SON_raids_fridge_for_food
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_jumping_jacks
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_gaming
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_going_for_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_coming_back_from_a_bike_ride
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_on_laptop_looking_for_jobs
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_SON_watching_TV
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_SON_watching_TV_with_tracey
			SWITCH eFamilyMember
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
				BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M2_DAUGHTER_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_workout_with_mp3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Going_out_in_her_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_walks_to_room_music
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_dancing_practice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_purges_in_the_bathroom
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_on_phone_to_friends
		CASE FE_M_DAUGHTER_on_phone_LOCKED
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_shower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_sober
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_watching_TV_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_screaming_at_dad
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sex_sounds_from_room
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_crying_over_a_guy
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_Coming_home_drunk
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_DAUGHTER_couchsleep
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_studying_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_studying_does_nails
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_DAUGHTER_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_WIFE_screams_at_mexmaid
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_in_face_mask
		CASE FE_M7_WIFE_in_face_mask
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_playing_tennis
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_doing_yoga
		CASE FE_M7_WIFE_doing_yoga
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_nails_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_WIFE_leaving_in_car_v2
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_WIFE_MD_leaving_in_car_v3
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_leaving_in_car
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_with_shopping_bags_enter
		CASE FE_M7_WIFE_with_shopping_bags_enter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_idle
//		CASE FE_M7_WIFE_with_shopping_bags_idle
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M2_WIFE_with_shopping_bags_exit
//		CASE FE_M7_WIFE_with_shopping_bags_exit
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_gets_drink_in_kitchen
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_sunbathing
		CASE FE_M7_WIFE_sunbathing
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_getting_botox_done
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_WIFE_passed_out_SOFA
		CASE FE_M7_WIFE_passed_out_SOFA
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_WIFE_screaming_at_son_P1
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_WIFE_screaming_at_son_P2
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_screaming_at_son_P3
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
				CASE FM_MICHAEL_SON			RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_screaming_at_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_phones_man_OR_therapist
		CASE FE_M7_WIFE_phones_man_OR_therapist
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_WIFE_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_M2_WIFE_using_vibrator
		CASE FE_M_WIFE_using_vibrator_END
		CASE FE_M7_WIFE_using_vibrator
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		CASE FE_M_WIFE_passed_out_BED
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M2_WIFE_sleeping
		CASE FE_M7_WIFE_sleeping
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_Making_juice
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_WIFE_shopping_with_daughter
			SWITCH eFamilyMember
				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
				CASE FM_MICHAEL_DAUGHTER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M7_WIFE_shopping_with_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M7_WIFE_on_phone
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_WIFE		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		
//		CASE FE_M_MEXMAID_cooking_for_son
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M2_MEXMAID_cleans_booze_pot_other
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M7_MEXMAID_cleans_booze_pot_other
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
		CASE FE_M2_MEXMAID_clean_surface_a
		CASE FE_M2_MEXMAID_clean_surface_b
		CASE FE_M2_MEXMAID_clean_surface_c
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
		CASE FE_M7_MEXMAID_clean_surface
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
		CASE FE_M2_MEXMAID_clean_window
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
		CASE FE_M7_MEXMAID_clean_window
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
//		CASE FE_M_MEXMAID_MIC4_clean_surface
//			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
//		BREAK
		CASE FE_M_MEXMAID_MIC4_clean_window
			RETURN IS_EVENT_VALID_FOR_FAMILY_MEMBER(eFamilyMember, FE_M2_MEXMAID_cleans_booze_pot_other)
		BREAK
		CASE FE_M_MEXMAID_does_the_dishes
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_MEXMAID_makes_calls
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_M_MEXMAID_watching_TV
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_MEXMAID_stealing_stuff
		CASE FE_M_MEXMAID_stealing_stuff_caught
			SWITCH eFamilyMember
				CASE FM_MICHAEL_MEXMAID		RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_M_GARDENER_with_leaf_blower
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_planting_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_trimming_hedges
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_cleaning_pool
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_mowing_lawn
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_watering_flowers
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_M_GARDENER_spraying_for_weeds
//			SWITCH eFamilyMember
//				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_M_GARDENER_on_phone
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_M_GARDENER_smoking_weed
			SWITCH eFamilyMember
				CASE FM_MICHAEL_GARDENER	RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		
//		CASE FE_M_MICHAEL_MIC2_washing_face
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MICHAEL	RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_F_AUNT_pelvic_floor_exercises
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_in_face_mask
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_watching_TV
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_F_AUNT_returned_to_aunts
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_AUNT  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR  RETURN TRUE BREAK
//				CASE FM_FRANKLIN_STRETCH  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR  RETURN TRUE BREAK
//				CASE FM_FRANKLIN_STRETCH  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR  RETURN TRUE BREAK
//				CASE FM_FRANKLIN_STRETCH  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			SWITCH eFamilyMember
//				CASE FM_FRANKLIN_LAMAR  RETURN TRUE BREAK
//				CASE FM_FRANKLIN_STRETCH  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering
			SWITCH eFamilyMember
				CASE FM_FRANKLIN_LAMAR  RETURN TRUE BREAK
				CASE FM_FRANKLIN_STRETCH  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RON_monitoring_police_frequency
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RON_stares_through_binoculars
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_sunbathing
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_drinking_beer
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
				CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
				CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_smoking_crystal
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		#ENDIF 
		//CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
		//	SWITCH eFamilyMember
		//		CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
		//		CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
		//	ENDSWITCH
		//BREAK
		CASE FE_T0_TREVOR_blowing_shit_up
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_TREVOR  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_trying_to_pick_up_signals
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_RONEX_doing_target_practice
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_RONEX_conspiracies_boring_Michael
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_RON  RETURN TRUE BREAK
//				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_WIFE  RETURN TRUE BREAK
				CASE FM_TREVOR_0_MICHAEL  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar
			SWITCH eFamilyMember
				CASE FM_TREVOR_0_MOTHER  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T0_MOTHER_something_b
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MOTHER  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
//		CASE FE_T0_MOTHER_something_c
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_0_MOTHER  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		
		CASE FE_T1_FLOYD_cleaning
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_cries_on_sofa
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_pineapple
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		CASE FE_T1_FLOYD_is_sleeping
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
//		CASE FE_T1_FLOYD_with_wade_post_trevor3
//			SWITCH eFamilyMember
//				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
//				CASE FM_TREVOR_1_WADE  RETURN TRUE BREAK
//			ENDSWITCH
//		BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1
			SWITCH eFamilyMember
				CASE FM_TREVOR_1_FLOYD  RETURN TRUE BREAK
				CASE FM_TREVOR_1_WADE  RETURN TRUE BREAK
			ENDSWITCH
		BREAK
		
		
		CASE FE_ANY_wander_family_event
		CASE FE_ANY_find_family_event
			RETURN TRUE
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	RETURN FALSE	BREAK
		CASE NO_FAMILY_EVENTS
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL RELEASE_PED_TO_FAMILY_SCENE(PED_INDEX PedIndex)
	
//	BREAK_ON_NATIVE_COMMAND("CLEAR_AREA", FALSE)
//	BREAK_ON_NATIVE_COMMAND("SET_PED_AS_NO_LONGER_NEEDED", FALSE)
//	BREAK_ON_NATIVE_COMMAND("DELETE_PED", FALSE)
	
	IF NOT DOES_ENTITY_EXIST(PedIndex)
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> PedIndex doesn't exist for RELEASE_PED_TO_FAMILY_SCENE()")
		#ENDIF
		
		SCRIPT_ASSERT("PedIndex doesn't exist for RELEASE_PED_TO_FAMILY_SCENE()")
		RETURN FALSE
	ENDIF
	
	enumFamilyMember	eFamilyMember = GET_enumFamilyMember_from_ped(PedIndex)
	
	IF DOES_ENTITY_EXIST(g_pScene_buddy)
	
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_pScene_buddy exists in RELEASE_PED_TO_FAMILY_SCENE[", Get_String_From_FamilyMember(eFamilyMember), "]")
		#ENDIF
		
		IF DOES_ENTITY_EXIST(g_pScene_extra_buddy)
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_pScene_extra_buddy exists in RELEASE_PED_TO_FAMILY_SCENE[", Get_String_From_FamilyMember(eFamilyMember), "]")
			#ENDIF
			
			SCRIPT_ASSERT("g_pScene_extra_buddy exists in RELEASE_PED_TO_FAMILY_SCENE()")
			RETURN FALSE
		ENDIF
		
		g_eCurrentFamilyEvent[eFamilyMember]	= FAMILY_MEMBER_BUSY
//		g_eSceneBuddyEvents						= FE_ANY_find_family_event
		
		g_pScene_extra_buddy = PedIndex
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> RELEASE_PED_TO_FAMILY_SCENE extra[", Get_String_From_FamilyMember(eFamilyMember), "]")
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	g_eCurrentFamilyEvent[eFamilyMember]	= FAMILY_MEMBER_BUSY
	g_eSceneBuddyEvents						= FE_ANY_find_family_event
	
	g_pScene_buddy = PedIndex
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> RELEASE_PED_TO_FAMILY_SCENE[", Get_String_From_FamilyMember(eFamilyMember), "]")
	#ENDIF
	
	
	RETURN TRUE
ENDFUNC
