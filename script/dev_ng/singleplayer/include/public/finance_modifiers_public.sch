USING "globals.sch"
USING "finance_enums.sch"
USING "commands_network.sch"
USING "commands_audio.sch"


PROC BAWSAQ_CAR_MODIFIED(MODEL_NAMES vehicleEdited)
	g_bStockMarketCarModified = TRUE
	g_eStockMarketCarModified = vehicleEdited
ENDPROC


/// PURPOSE:
///    Increments a stock system modifier by the passed value
///    Stock system modifiers are values that have their rate of change 
///    watched by the stock system,  this rate of change is used to create
///    price movements.
PROC BAWSAQ_INCREMENT_MODIFIER(BSMF_TYPES toAlter,INT byValue = 1)
	CDEBUG1LN(DEBUG_STOCKS, "Incrementing stock marked modifier for ", toAlter," by ", byValue, ".")

	IF byValue < 1
		CDEBUG1LN(DEBUG_STOCKS, "Invalid increment value passed.")
		SCRIPT_ASSERT("BAWSAQ_INCREMENT_MODIFIER: Invalid increment value passed.")
		EXIT
	ENDIF

	IF g_BS_Modifiers[toAlter].bReadFromProfile
		CDEBUG1LN(DEBUG_STOCKS, "Modifier is flagged to be read from profile. Manual increment skipped.")
		EXIT
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG1LN(DEBUG_STOCKS, "Network game is in progress. Stock market shouldn't update in MP. Increment skipped.")
		EXIT
	ENDIF

	// If online then set stat g_BS_Modifiers[toAlter].
	IF g_BS_Modifiers[toAlter].bOnline
		CDEBUG1LN(DEBUG_BAWSAQ, "Stat is online. Reading and updating modifier stat.")
	
		INT iStatValue
		STAT_GET_INT(g_BS_Modifiers[toAlter].uploadTo, iStatValue)
		CDEBUG1LN(DEBUG_BAWSAQ, "Stat current value is ", iStatValue, ".")

		iStatValue += byValue
		STAT_SET_INT(g_BS_Modifiers[toAlter].uploadTo, iStatValue)
		CDEBUG1LN(DEBUG_BAWSAQ, "Incremented stat value to ", iStatValue, ".")
	ENDIF
	
	// If offline then update data.
	//g_BS_Modifiers[toAlter].iChangeThisUpdate += byValue
ENDPROC


ENUM RADIO_STATION_COMPANY
	RADIO_CO_NONE,
	RADIO_CO_WEAZEL,
	RADIO_CO_CNT,
	RADIO_CO_RA1,
	RADIO_CO_RA2
ENDENUM 


/// PURPOSE:
///    Arbitrary radio station company ownership definition
///    Thanks for dropping the damn ball on this one new media.
/// PARAMS:
///    i - 
/// RETURNS:
///    
FUNC RADIO_STATION_COMPANY GET_RADIO_STATION_CO_BY_INT(INT i)
	SWITCH i 
		CASE 1
		CASE 2
		CASE 3
		CASE 4
			RETURN RADIO_CO_RA1
		BREAK
			
		CASE 5
		CASE 6
		CASE 7
		CASE 8
			RETURN RADIO_CO_RA2
		BREAK
			
		CASE 9
		CASE 10
		CASE 11
		CASE 12
			RETURN RADIO_CO_WEAZEL
		BREAK
			
		CASE 13
		CASE 14
		CASE 15
		CASE 16
			RETURN RADIO_CO_CNT
		BREAK
	ENDSWITCH
	
	RETURN RADIO_CO_NONE
ENDFUNC


/// PURPOSE:
///    Zit song ID use for given stations.
PROC STOCK_MARKET_MUSIC_ZIT_STATION_CHECK()
 	BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_ZITPOP_FOR_ZIT)	
	
	INT s = GET_PLAYER_RADIO_STATION_INDEX()
	SWITCH GET_RADIO_STATION_CO_BY_INT(s)
		CASE RADIO_CO_WEAZEL
			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_ZITIT_FOR_WZL)
			BREAK
		CASE RADIO_CO_CNT
			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_ZITIT_FOR_CNT)
			BREAK
		DEFAULT
			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_ZITIT_FOR_ZIT)
			BREAK
	ENDSWITCH
ENDPROC
