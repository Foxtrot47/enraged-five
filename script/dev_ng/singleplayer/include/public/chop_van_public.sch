//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   chop_van_public.sch                                   						//
//      AUTHOR          :   Rob Bray                                                    				//
//      DESCRIPTION     :   Common functions for Chop riding in van										//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_vehicle.sch" 
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_physics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_audio.sch"
USING "script_player.sch"

ENUM CHOP_LEAN_IN_VAN_STATE_ENUM
	CHOP_LEAN_IN_VAN_STATE_CENTERED = 0,
	CHOP_LEAN_IN_VAN_STATE_FORWARD,
	CHOP_LEAN_IN_VAN_STATE_BACK,
	CHOP_LEAN_IN_VAN_STATE_LEFT,
	CHOP_LEAN_IN_VAN_STATE_RIGHT,
	CHOP_LEAN_IN_VAN_STATE_BARK
ENDENUM

CONST_FLOAT CHOP_ANIM_BLEND_IN 0.8
CONST_FLOAT CHOP_ANIM_BLEND_OUT -0.8
CONST_FLOAT CHOP_GROWL_ROT 206.0

// get chop in van dictionary
FUNC STRING GET_CHOP_IN_VAN_ANIM_DICT()
	RETURN "MISSCHOP_VEHICLE@BACK_OF_VAN"
ENDFUNC

// handle chop lean animations
PROC HANDLE_CHOP_LEAN_IN_VAN(PED_INDEX chopPed, VEHICLE_INDEX vanVehicle, CHOP_LEAN_IN_VAN_STATE_ENUM &chopState, FLOAT &fCachedSpeed, INT &iNextAnimTime, INT &iNextBarkTime, BOOL bAllowBarkAnim = FALSE)
	IF NOT IS_PED_INJURED(chopPed)
	AND IS_VEHICLE_DRIVEABLE(vanVehicle)
		VECTOR vVanRotationVelocity = GET_ENTITY_ROTATION_VELOCITY(vanVehicle)
		VECTOR vVanVelocity = GET_ENTITY_SPEED_VECTOR(vanVehicle, TRUE)
		FLOAT fVanSpeed = GET_ENTITY_SPEED(vanVehicle)
		//PRINTFLOAT(fVanSpeed) PRINTNL()
		//PRINTVECTOR(vVanVelocity) PRINTNL()
		FLOAT fVanSpeedDiff = vVanVelocity.y - fCachedSpeed
		fVanSpeedDiff /= GET_FRAME_TIME()
		//PRINTFLOAT(fVanSpeedDiff) PRINTNL()
		
		IF vVanVelocity.y < 0
			vVanRotationVelocity.z *= -1
		ENDIF
		
		CHOP_LEAN_IN_VAN_STATE_ENUM newChopState
		
		IF vVanRotationVelocity.z < -0.5
			IF fVanSpeed >= 6.4
				newChopState = CHOP_LEAN_IN_VAN_STATE_LEFT
			ENDIF
		ELIF vVanRotationVelocity.z > 0.5
			IF fVanSpeed >= 6.4
				newChopState = CHOP_LEAN_IN_VAN_STATE_RIGHT
			ENDIF
		ELIF fVanSpeedDiff > 2.0
			newChopState = CHOP_LEAN_IN_VAN_STATE_BACK
		ELIF fVanSpeedDiff < -8.0
			newChopState = CHOP_LEAN_IN_VAN_STATE_FORWARD
		ELSE
			IF bAllowBarkAnim
			AND fVanSpeed <= 3
			AND (GET_GAME_TIMER() >= iNextBarkTime OR chopState = CHOP_LEAN_IN_VAN_STATE_BARK)
				newChopState = CHOP_LEAN_IN_VAN_STATE_BARK
			ELSE
				newChopState = CHOP_LEAN_IN_VAN_STATE_CENTERED
			ENDIF
		ENDIF		
		
		/*
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
			newChopState = CHOP_LEAN_IN_VAN_STATE_LEFT
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			newChopState = CHOP_LEAN_IN_VAN_STATE_RIGHT
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
			newChopState = CHOP_LEAN_IN_VAN_STATE_BACK
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
			newChopState = CHOP_LEAN_IN_VAN_STATE_FORWARD
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD5)
			newChopState = CHOP_LEAN_IN_VAN_STATE_CENTERED
		ELSE
			newChopState = chopState
		ENDIF	
		*/
		
		IF newChopState <> chopState
		AND GET_GAME_TIMER() >= iNextAnimTime
			//DETACH_entity(chopPed)
			STRING sAnim
			SWITCH newChopState
				CASE CHOP_LEAN_IN_VAN_STATE_CENTERED
					sAnim = "CHOP_SIT_LOOP"
				BREAK
				CASE CHOP_LEAN_IN_VAN_STATE_FORWARD
					sAnim = "CHOP_LEAN_FORWARDS_LOOP"
				BREAK
				CASE CHOP_LEAN_IN_VAN_STATE_BACK
					sAnim = "CHOP_LEAN_BACK_LOOP"
				BREAK
				CASE CHOP_LEAN_IN_VAN_STATE_LEFT
					sAnim = "CHOP_LEAN_RIGHT_LOOP"
				BREAK
				CASE CHOP_LEAN_IN_VAN_STATE_RIGHT
					sAnim = "CHOP_LEAN_LEFT_LOOP"
				BREAK
				CASE CHOP_LEAN_IN_VAN_STATE_BARK
					sAnim = "CHOP_BARK"
				BREAK
			ENDSWITCH
			
			iNextAnimTime = GET_GAME_TIMER() + 500
			
			IF newChopState = CHOP_LEAN_IN_VAN_STATE_BARK
				iNextBarkTime = GET_GAME_TIMER() + 3000
			ENDIF
			
			TASK_PLAY_ANIM(chopPed,  GET_CHOP_IN_VAN_ANIM_DICT(), sAnim, CHOP_ANIM_BLEND_IN, CHOP_ANIM_BLEND_OUT, -1, AF_LOOPING)
			chopState = newChopState
		ENDIF
		
		fCachedSpeed = vVanVelocity.y
	ENDIF
ENDPROC

// attach chop to van
PROC ATTACH_CHOP_TO_VAN(PED_INDEX chopPed, VEHICLE_INDEX vanVehicle, FLOAT fHeading, BOOL bFromGetIn = FALSE, BOOL bOverrideAttachPos = FALSE, FLOAT fOverrideAttachX = 0.0, FLOAT fOverrideAttachY = 0.0, FLOAT fOverrideAttachZ = 0.0, FLOAT fOverrideAttachRot = 0.0)
	IF NOT IS_PED_INJURED(chopPed)
	AND IS_VEHICLE_DRIVEABLE(vanVehicle)
		
		VECTOR vAttach
		FLOAT fAttachHeading
		IF NOT bOverrideAttachPos
			IF NOT bFromGetIn 
				vAttach = <<0,-0.5,0.3>>
			ELSE
				vAttach = <<0.129,-1.522,0.3>>
			ENDIF
			fAttachHeading = fHeading
		ELSE
			vAttach = <<fOverrideAttachX, fOverrideAttachY, fOverrideAttachZ>>
			fAttachHeading = fOverrideAttachRot
		ENDIF
		
		ATTACH_ENTITY_TO_ENTITY(chopPed, vanVehicle, 0, vAttach, <<0,0,fAttachHeading>>, FALSE, FALSE, FALSE, TRUE)
	ENDIF
ENDPROC

// set chop in van
PROC SET_CHOP_INTO_VAN(PED_INDEX chopPed, VEHICLE_INDEX vanVehicle, CHOP_LEAN_IN_VAN_STATE_ENUM &chopState, FLOAT &fCachedVanSpeed, BOOL bGuard = FALSE, BOOL bInstant = TRUE, BOOL bFromGetIn = FALSE, BOOL bOverrideAttachPos = FALSE, FLOAT fOverrideAttachX = 0.0, FLOAT fOverrideAttachY = 0.0, FLOAT fOverrideAttachZ = 0.0, FLOAT fOverrideAttachRot = 0.0)
	IF NOT IS_PED_INJURED(chopPed)
	AND IS_VEHICLE_DRIVEABLE(vanVehicle)
		IF bInstant
			CLEAR_PED_TASKS_IMMEDIATELY(chopPed)
		ELSE
			//CLEAR_PED_TASKS(chopPed)
		ENDIF
		
		STRING sAnim
		FLOAT fBlendIn
		FLOAT fHeading
		IF bGuard
			sAnim = "CHOP_GROWL"
			fHeading = CHOP_GROWL_ROT
		ELSE
			sAnim = "CHOP_SIT_LOOP"
			IF NOT bFromGetIn
				fHeading = 0.0
			ELSE
				fHeading = 11.96
			ENDIF
		ENDIF
		
		IF bInstant
			fBlendIn = INSTANT_BLEND_IN
		ELSE
			fBlendIn = SLOW_BLEND_IN
		ENDIF
		
		chopState = CHOP_LEAN_IN_VAN_STATE_CENTERED
		fCachedVanSpeed = 0
		
		ATTACH_CHOP_TO_VAN(chopPed, vanVehicle, fHeading, bFromGetIn, bOverrideAttachPos, fOverrideAttachX, fOverrideAttachY, fOverrideAttachZ, fOverrideAttachRot)
		TASK_PLAY_ANIM(chopPed,  GET_CHOP_IN_VAN_ANIM_DICT(), sAnim, fBlendIn, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET)
		IF bInstant
			FORCE_PED_AI_AND_ANIMATION_UPDATE(chopPed)
		ENDIF
	ENDIF
ENDPROC
