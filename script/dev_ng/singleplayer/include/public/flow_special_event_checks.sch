//╒═════════════════════════════════════════════════════════════════════════════╕
//│				 GTA5 Specific Flow Special Event Check Header					│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			22/11/10												│
//│		DESCRIPTION: 	A header containing any special script needed to 		│
//│						perform special event checks in the GTA5 flow.			│									
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_public_core_override.sch"
USING "ped_component_public.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ Character Appearance Checks  ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//Check to see if Franklin has changed his clothes after Lamar has asked him to.

FUNC BOOL FLOWEVENT_HAS_FRANKLIN_CHANGED_CLOTHES_SINCE_LAMAR_REQUEST()
	//Is the request active?
	IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_ACTIVE)
		//Is the player currently Franklin?
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			//Has Franklin's original core outift ID been saved?
			IF g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID = -1
				SCRIPT_ASSERT("FLOWEVENT_HAS_FRANKLIN_CHANGED_CLOTHES_SINCE_LEAAR_REQUEST: Franklin outfit request is active but no original outfit has been recorded. Ask BenR.")
				RETURN FALSE
			ENDIF
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				//Is Franklin's outfit when leaving the wardrobe the same as the outfit Lamar told him to change?
				IF GET_CORE_OUTFIT_ID_FROM_PED(PLAYER_PED_ID()) = g_savedGlobals.sPlayerData.iFranklinOriginalOutfitID
					Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_COMPLETE, FALSE)
				ELSE
					Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_COMPLETE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		RETURN Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_OUTFIT, BITS_CHARACTER_REQUEST_COMPLETE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

