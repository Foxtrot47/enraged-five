//INCLUDE FILES
USING "rage_builtins.sch"
USING "globals.sch"

#IF IS_DEBUG_BUILD

USING "commands_script.sch"
USING "commands_xml.sch"
USING "script_bugstar.sch"
USING "flow_mission_data_public.sch"
USING "cellphone_public.sch"

//CONST_FLOATs

CONST_FLOAT MENU_ITEM_LINE_HIGHT		0.0300		// amount to move down for each new line
CONST_FLOAT DISPLAY_AT_X 				0.0510		// X coordionate of the menu item
CONST_FLOAT DISPLAY_AT_Y 				0.1250		// y coordionate of the menu item
CONST_FLOAT SKIP_MENU_BAR_MIN_POS 		0.1100		//minimum position of the slidebar
CONST_FLOAT SKIP_MENU_BAR_MAX_POS 		0.9000		//Maximum position of the slidebar
CONST_FLOAT X_SCALE 					0.2000		// Text scale X
CONST_FLOAT Y_SCALE						0.2850		// Text scale Y
CONST_FLOAT BORDER_HEIGHT				0.0030		// height of the border
CONST_FLOAT TOP_MENU_HEIGHT				0.0600		//Height of the top part of the menu
CONST_FLOAT SKIP_MENU_MAX_HEIGHT 		0.7940		//Maximum hight of the skip menu
CONST_FLOAT SKIP_MENU_MENU_TITLE_HEIGHT 0.154		//Hight oif the title of the menu
CONST_FLOAT SKIP_MENU_MENU_TITLE_BORDER_HEIGHT 0.215//Hight of the border of the title
FLOAT SKIP_MENU_MOVE_DOWN_BY 		= 0.024		//Moves the whole menu down by this

//CONST_INTs
CONST_INT SKIP_MENU_MAX_STRING_DISPLAY 	28			//Max strings that can be displayed at once
CONST_INT SKIP_MENU_MOVE_POINT			13			//point that the menu moves at
CONST_INT SKIP_MENU_MOVE_DELAY			100			//the delay timer for moving the menu if the button if held down.
//BOOLS
BOOL bDrawMenu						//should the menu be drawn
INT iSelectedMenu
INT iButtonPressedTimer
INT iThisPSkipTimer

ENUM SKIP_MODE
	SM_J_SKIP,
	SM_P_SKIP,
	SM_Z_SKIP
ENDENUM

SKIP_MODE smLastSkipMethod

//STRUCT that holds information about the menu
STRUCT MissionStageMenuTextStruct
	TEXT_LABEL_63 sTxtLabel 	
	BOOL bSelectable = TRUE
ENDSTRUCT

//PURPOSE: Returns True is a string is not Null and sets the iSelected stage to the non NULL value. 
FUNC BOOL RETURN_NON_NULL_STRING(BOOL bDown,MissionStageMenuTextStruct &StructPassed[],INT &iSelected)
	INT i
	INT MenuLength = COUNT_OF(StructPassed)	//length of the menu
	//go through the menu to find the next non-null value
	REPEAT  MenuLength i
		IF bDown//if the menu is moving down
			iSelected+=1
			IF iSelected > (MenuLength-1)
				iSelected = 0
			ENDIF
		ELSE//if the menu is moving up
			iSelected-=1
			IF iSelected<0
				iSelected = (MenuLength -1)
			ENDIF
		ENDIF
		//If the string is not NULL then return true		
		IF NOT IS_STRING_NULL(StructPassed[iSelected].sTxtLabel)
		AND StructPassed[iSelected].bSelectable = TRUE
		
			RETURN TRUE
			
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC	
//PURPOSE: Returns True is a string is not Null and sets the iSelected stage to the non NULL value. 
FUNC BOOL RETURN_FIRST_NON_NULL_STRING(MissionStageMenuTextStruct &StructPassed[],INT &iSelected)
	INT i
	INT MenuLength = COUNT_OF(StructPassed)	//length of the menu
	//go through the menu to find the next non-null value
	REPEAT  MenuLength i

		//If the string is not NULL then return true		
		IF NOT IS_STRING_NULL(StructPassed[iSelected].sTxtLabel)
		AND StructPassed[iSelected].bSelectable = TRUE
		
			RETURN TRUE
			
		ENDIF
		//move down to next
		iSelected+=1
		IF iSelected > (MenuLength-1)
			iSelected = 0
		ENDIF
		
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC	
//PURPOSE:Draws the slider bar if needed
PROC SKIP_MENU_DO_SLIDE_BAR(INT MenuLength, FLOAT fPanelY, FLOAT fMenuHeight, FLOAT fSlidePos= 0.2490)
	FLOAT fScrollBarHeight 
	FLOAT fTotalHeight 
	FLOAT fScrollBarScale
	FLOAT fBlockHeight
	FLOAT fPosition
	FLOAT fPositionScale
	FLOAT fBlockPos
	INT iMenuStringStart
	//Height of the scroll bar
	fScrollBarHeight = (SKIP_MENU_BAR_MAX_POS+SKIP_MENU_MOVE_DOWN_BY) - (SKIP_MENU_BAR_MIN_POS+SKIP_MENU_MOVE_DOWN_BY)
	// Find out the total height that the items cover
	fTotalHeight = MenuLength * MENU_ITEM_LINE_HIGHT
	// Work out the scale between the scroll bar and total item height
	fScrollBarScale = fScrollBarHeight / fTotalHeight
	// Scale the scroll block
	fBlockHeight = fScrollBarHeight * fScrollBarScale
	//work out the menu start
	IF iSelectedMenu > SKIP_MENU_MOVE_POINT
		iMenuStringStart = iSelectedMenu - SKIP_MENU_MOVE_POINT
		IF iMenuStringStart > MenuLength - SKIP_MENU_MAX_STRING_DISPLAY
			iMenuStringStart = MenuLength - SKIP_MENU_MAX_STRING_DISPLAY
		ENDIF
	ENDIF
	// Grab the position of the item
	fPosition = TO_FLOAT(iMenuStringStart)*MENU_ITEM_LINE_HIGHT
	// Work out the scale of the item in percentage form
	fPositionScale = fPosition / fTotalHeight
	//work out the final position of the scroll block
	fBlockPos = fPanelY - (fScrollBarHeight * 0.5) + (fBlockHeight * 0.5) + (fScrollBarHeight * fPositionScale)
	//draw the shit
	DRAW_RECT(fSlidePos,fPanelY, 0.0050, fMenuHeight, 0, 0, 0, 255)
	DRAW_RECT(fSlidePos,fBlockPos, 0.0050, fBlockHeight, 100, 100, 100, 255)
ENDPROC
//PURPOSE:Draws the slider bar if needed
PROC SKIP_MENU_DO_SELECTED_BOX(INT MenuLength, FLOAT fMenuWidth = 0.2100, FLOAT fMenuX =  0.1460, FLOAT fRowHeightMultiplier = 1.0)
	INT iMenuStringStart
	IF MenuLength > (SKIP_MENU_MAX_STRING_DISPLAY + 1)//26 is the maximum strings that you can have
		IF iSelectedMenu > SKIP_MENU_MOVE_POINT
			iMenuStringStart = SKIP_MENU_MOVE_POINT
			IF iSelectedMenu > MenuLength - SKIP_MENU_MOVE_POINT
				iMenuStringStart = SKIP_MENU_MAX_STRING_DISPLAY - (MenuLength - iSelectedMenu)
			ENDIF
			IF iMenuStringStart > SKIP_MENU_MAX_STRING_DISPLAY
				iMenuStringStart = SKIP_MENU_MAX_STRING_DISPLAY
			ENDIF
		ELSE
			iMenuStringStart = iSelectedMenu
		ENDIF
	ELSE
		iMenuStringStart = iSelectedMenu
	ENDIF
	//item selected box
	DRAW_RECT(fMenuX, (DISPLAY_AT_Y+SKIP_MENU_MOVE_DOWN_BY+0.01)+(iMenuStringStart*MENU_ITEM_LINE_HIGHT*fRowHeightMultiplier), fMenuWidth, 0.0200, 127, 206, 255, 150)
ENDPROC

CONST_INT MAX_BUG_NUMBERS 10

STRUCT TAGGED_BUGS_STRUCT
	INT iBugNumber[MAX_BUG_NUMBERS]
	INT iBugDeveloper[MAX_BUG_NUMBERS]
	INT iDev
	INT iBug
	BOOL bXML_loaded, bQuery_failed, bWait_for_z_release
	INT iStage
	INT iBugstarID
	TEXT_LABEL_7 sMissionID
ENDSTRUCT
TAGGED_BUGS_STRUCT sTaggedBugs

FUNC BOOL DO_LOAD_XML()

	BOOL bFinished = FALSE
	TEXT_LABEL_63 tlTemp
	
	SWITCH sTaggedBugs.iStage
		// Wait for failed flag to be cleared
		CASE 0
			IF NOT sTaggedBugs.bQuery_failed
				sTaggedBugs.iStage++
			ENDIF
		BREAK
		
		// Obtain missionID for current mission
		CASE 1
			// Attempt to start a new query if we haven't done so already
			IF NOT HAS_BUGSTAR_QUERY_STARTED()
				IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
					// Start a new query
					TEXT_LABEL_63 tlQuery
					tlQuery = "MissionsV2?fields=ScriptName,MissionId"
					
					PRINTLN("DO_LOAD_XML() - Starting query: ", tlQuery)
					
					START_BUGSTAR_QUERY(tlQuery)
					SET_BUGSTAR_QUERY_STARTED()
					
					// Reset some data
					sTaggedBugs.sMissionID = ""
					sTaggedBugs.iBug = 0
					sTaggedBugs.iBugstarID = -1
					INT i
					REPEAT MAX_BUG_NUMBERS i
						sTaggedBugs.iBugNumber[i] = 0
						sTaggedBugs.iBugDeveloper[i] = 0
					ENDREPEAT
					sTaggedBugs.iDev = 0
				ENDIF
			ENDIF
			
			IF HAS_BUGSTAR_QUERY_STARTED()
				IF NOT IS_BUGSTAR_QUERY_PENDING()
					IF (IS_BUGSTAR_QUERY_SUCCESSFUL() AND NOT IS_BUTTON_PRESSED(PAD1, DPADDOWN))
						IF(CREATE_XML_FROM_BUGSTAR_QUERY())
							
							// Integers used for the FOR loops going through all the nodes and attributes
							INT eachNode
							TEXT_LABEL_63 tlNodeString, tlScript
							BOOL bFoundScript
							
							INT iNodeCount
							iNodeCount = GET_NUMBER_OF_XML_NODES()
							
							// Loop through all the nodes
							FOR eachNode = 0 TO (iNodeCount-1)
							
								IF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("scriptName")
									tlNodeString = GET_STRING_FROM_XML_NODE()
									tlScript = GET_THIS_SCRIPT_NAME()
									tlScript += ".sc"
									
									IF (GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_KEY(tlNodeString)) // without .sc
									OR (GET_HASH_KEY(tlScript) = GET_HASH_KEY(tlNodeString)) // with .sc
										bFoundScript = TRUE
									ENDIF
									
								ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("missionID")
									IF bFoundScript
										sTaggedBugs.sMissionID = GET_STRING_FROM_XML_NODE()
										eachNode = iNodeCount // bail
									ENDIF
								ENDIF
								
								// Tell the script to goto the next node in the xml file
								GET_NEXT_XML_NODE()
							ENDFOR
							
							DELETE_XML_FILE()
						ELSE
							PRINTLN("DO_LOAD_XML() - Query failed 1")
							sTaggedBugs.bQuery_failed = TRUE
							sTaggedBugs.iStage = 0
							bFinished = TRUE
						ENDIF
					ENDIF
					
					// Query finished pending and we will have processed a successful query so end
					END_BUGSTAR_QUERY()
					SET_BUGSTAR_QUERY_FINISHED()
					
					// Now get the bugstarID
					IF GET_HASH_KEY(sTaggedBugs.sMissionID) != 0
						PRINTLN("DO_LOAD_XML() - Looking up bugstarID for ", sTaggedBugs.sMissionID)
						sTaggedBugs.iStage++
					ELSE
						PRINTLN("DO_LOAD_XML() - Unable to find mission id")
						
						sTaggedBugs.iStage = 0
						
						// Attempt made to load data so return true.
						bFinished = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Obtain bugstarID using missionID
		CASE 2
			// Attempt to start a new query if we haven't done so already
			IF NOT HAS_BUGSTAR_QUERY_STARTED()
				IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
					// Start a new query
					TEXT_LABEL_7 tlScriptID
					TEXT_LABEL_63 tlQuery
					tlScriptID = GET_SP_MISSION_ID_LABEL_FOR_SCRIPT_NAME(GET_THIS_SCRIPT_NAME(), TRUE)
					
					tlQuery = "Missions/Summary/"
					tlQuery += tlScriptID
					
					PRINTLN("DO_LOAD_XML() - Starting query: ", tlQuery)
					
					START_BUGSTAR_QUERY(tlQuery)
					SET_BUGSTAR_QUERY_STARTED()
					
					// Reset some data
					sTaggedBugs.iBug = 0
					sTaggedBugs.iBugstarID = -1
					INT i
					REPEAT MAX_BUG_NUMBERS i
						sTaggedBugs.iBugNumber[i] = 0
						sTaggedBugs.iBugDeveloper[i] = 0
					ENDREPEAT
					sTaggedBugs.iDev = 0
				ENDIF
			ENDIF
			
			IF HAS_BUGSTAR_QUERY_STARTED()
				IF NOT IS_BUGSTAR_QUERY_PENDING()
					IF (IS_BUGSTAR_QUERY_SUCCESSFUL() AND NOT IS_BUTTON_PRESSED(PAD1, DPADDOWN))
						IF(CREATE_XML_FROM_BUGSTAR_QUERY())
							
							// Integers used for the FOR loops going through all the nodes and attributes
							INT eachNode, eachAttribute
							
							INT iNodeCount
							iNodeCount = GET_NUMBER_OF_XML_NODES()
							
							
							// Loop through all the nodes
							FOR eachNode = 0 TO (iNodeCount-1)
								SWITCH GET_HASH_KEY(GET_XML_NODE_NAME())
								
									// 'bugstarItem'
									CASE DM_HASH_ITEM_BUGSTAR
									
										// Check the node has some attributes
										IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
										
											// Loop through all the attributes
											FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
										
												// Convert the attribute name to a hash key like we did with the nodes so we can use a switch statement
												SWITCH GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute))

													// Grab the 'bugstarMissionId'
													CASE DM_HASH_BUG_BUGSTAR_ID
														sTaggedBugs.iBugstarID = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
														eachNode = (iNodeCount)
														eachAttribute = (GET_NUMBER_OF_XML_NODE_ATTRIBUTES())
														PRINTLN("DO_LOAD_XML() - BugstarID for '", GET_THIS_SCRIPT_NAME(), "' is ", sTaggedBugs.iBugstarID)
													BREAK
													
												ENDSWITCH
												
											ENDFOR
											
										ENDIF
										
									BREAK
									
								ENDSWITCH
								
								// Tell the script to goto the next node in the xml file
								GET_NEXT_XML_NODE()
							ENDFOR
							
							DELETE_XML_FILE()
						ELSE
							PRINTLN("DO_LOAD_XML() - Query failed 2")
							sTaggedBugs.bQuery_failed = TRUE
							sTaggedBugs.iStage = 0
							bFinished = TRUE
						ENDIF
					ENDIF
					
					// Query finished pending and we will have processed a successful query so end
					END_BUGSTAR_QUERY()
					SET_BUGSTAR_QUERY_FINISHED()
					
					
					// Now get the bugs
					IF sTaggedBugs.iBugstarID != -1
						PRINTLN("DO_LOAD_XML() - Looking for bugs with the UpdateFlag set to true")
						sTaggedBugs.iStage++
					ELSE
						PRINTLN("DO_LOAD_XML() - Unable to find bugstar mission id")
						
						sTaggedBugs.iStage = 0
						
						// Attempt made to load data so return true.
						bFinished = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Obtain bug summary for bugs with the 'Update' tickbox ticked
		CASE 3
			// Attempt to start a new query if we haven't done so already
			IF NOT HAS_BUGSTAR_QUERY_STARTED()
				IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
					// Start a new query
					TEXT_LABEL_63 tlQuery
					tlQuery = "MissionsV2/"
					tlQuery += sTaggedBugs.iBugstarID
					tlQuery += "/Bugs?fields=Id,Summary,UpdateFlag,Developer"
					
					PRINTLN("DO_LOAD_XML() - Starting query: ", tlQuery)
					
					START_BUGSTAR_QUERY(tlQuery)
					SET_BUGSTAR_QUERY_STARTED()
				ENDIF
			ENDIF
			
			IF HAS_BUGSTAR_QUERY_STARTED()
				IF NOT IS_BUGSTAR_QUERY_PENDING()
					IF (IS_BUGSTAR_QUERY_SUCCESSFUL())
						IF(CREATE_XML_FROM_BUGSTAR_QUERY())
							// Loop through all the nodes
							INT eachNode
							
							INT iNodeCount
							iNodeCount = GET_NUMBER_OF_XML_NODES()
							
							FOR eachNode = 0 TO (iNodeCount-1)
								IF sTaggedBugs.iBug < MAX_BUG_NUMBERS
									IF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("Id")
										tlTemp = GET_STRING_FROM_XML_NODE()
										STRING_TO_INT(tlTemp, sTaggedBugs.iBugNumber[sTaggedBugs.iBug])
									ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("Summary")
										tlTemp = "BSS_"
										tlTemp += sTaggedBugs.iBug
										ADD_DEBUG_STRING(tlTemp, GET_STRING_FROM_XML_NODE())
									ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("Developer")
										tlTemp = GET_STRING_FROM_XML_NODE()
										STRING_TO_INT(tlTemp, sTaggedBugs.iBugDeveloper[sTaggedBugs.iBug])
									ELIF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("UpdateFlag")
										IF (GET_HASH_KEY(GET_STRING_FROM_XML_NODE()) = GET_HASH_KEY("true"))
											// The UpdateFlag node comes last so once we hit it move on to the next bug
											PRINTSTRING("DO_LOAD_XML() - Found bug - ")PRINTINT(sTaggedBugs.iBugNumber[sTaggedBugs.iBug])PRINTNL()
											sTaggedBugs.iBug++
										ENDIF
									ENDIF
								ENDIF
								
								// Tell the script to goto the next node in the xml file
								GET_NEXT_XML_NODE()
							ENDFOR
							
							DELETE_XML_FILE()
						ENDIF
					ELSE
						PRINTLN("DO_LOAD_XML() - Query failed 3")
						sTaggedBugs.bQuery_failed = TRUE
						sTaggedBugs.iStage = 0
						bFinished = TRUE
					ENDIF
					
					// Query finished pending and we will have processed a successful query so end
					END_BUGSTAR_QUERY()
					SET_BUGSTAR_QUERY_FINISHED()
					
					IF NOT bFinished
						// Now get the LBCH tagged bugs
						PRINTLN("DO_LOAD_XML() - Looking for bugs with the LBCH tag set to true")
						sTaggedBugs.iStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Obtain list of bugs with the LBCH tag.
		CASE 4
			// Attempt to start a new query if we haven't done so already
			IF NOT HAS_BUGSTAR_QUERY_STARTED()
				IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
					// Start a new query
					TEXT_LABEL_63 tlQuery
					tlQuery = "Missions/TaggedBugs/LBCH"
					PRINTLN("DO_LOAD_XML() - Starting query: ", tlQuery)
					
					START_BUGSTAR_QUERY(tlQuery)
					SET_BUGSTAR_QUERY_STARTED()
				ENDIF
			ENDIF
			
			IF HAS_BUGSTAR_QUERY_STARTED()
				IF NOT IS_BUGSTAR_QUERY_PENDING()
					IF (IS_BUGSTAR_QUERY_SUCCESSFUL() AND NOT IS_BUTTON_PRESSED(PAD1, DPADDOWN))
						IF(CREATE_XML_FROM_BUGSTAR_QUERY())
						
							INT iNodeCount
							iNodeCount = GET_NUMBER_OF_XML_NODES()
							
							IF iNodeCount <> 0
					        	// Integers used for the FOR loops going through all the nodes and attributes
					        	INT eachNode, eachAttribute
							   	TEXT_LABEL_31 MissionName
								MissionName = GET_THIS_SCRIPT_NAME()
							   	MissionName += ".sc"
								
								BOOL bExitBeforeNextMission
					        	// Loop through all the nodes
					        	FOR eachNode = 0 TO (iNodeCount-1)
								
									IF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "mission")
										IF bExitBeforeNextMission = FALSE
											IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
												FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
													IF ARE_STRINGS_EQUAL(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute), "file")
														IF ARE_STRINGS_EQUAL(GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute), MissionName)
															bExitBeforeNextMission = TRUE
															//PRINTSTRING("Found mission")PRINTNL()
														ENDIF
													ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute), "id")
														//Don't Do anything yet
													ENDIF
												ENDFOR
											ENDIF
										ELSE
											eachNode = iNodeCount
										ENDIF
											
									ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_NAME(), "taggedBug")
										
										IF bExitBeforeNextMission
											IF GET_NUMBER_OF_XML_NODE_ATTRIBUTES() <> 0
												FOR eachAttribute = 0 TO (GET_NUMBER_OF_XML_NODE_ATTRIBUTES()-1)
													IF sTaggedBugs.iBug < MAX_BUG_NUMBERS
														IF ARE_STRINGS_EQUAL(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute), "id")
															sTaggedBugs.iBugNumber[sTaggedBugs.iBug] = GET_INT_FROM_XML_NODE_ATTRIBUTE(eachAttribute)
															PRINTSTRING("DO_LOAD_XML() - Found bug - ")PRINTINT(sTaggedBugs.iBugNumber[sTaggedBugs.iBug])PRINTNL()
														ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute), "summary")
															tlTemp = "BSS_"
															tlTemp += sTaggedBugs.iBug
															ADD_DEBUG_STRING(tlTemp, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
														ELIF ARE_STRINGS_EQUAL(GET_XML_NODE_ATTRIBUTE_NAME(eachAttribute), "developer")
															tlTemp = "BSD_"
															tlTemp += sTaggedBugs.iBug
															ADD_DEBUG_STRING(tlTemp, GET_STRING_FROM_XML_NODE_ATTRIBUTE(eachAttribute))
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											sTaggedBugs.iBug++
										ENDIF
									ENDIF
											
									// Tell the script to goto the next node in the xml file
									GET_NEXT_XML_NODE()
								
								// End of for loop going through the nodes
								ENDFOR
								
							ENDIF
							
							// Make sure the xml file is cleanup up
							DELETE_XML_FILE()
							
						ENDIF
					ENDIF
					
					// Query finished pending and we will have processed a successful query so end
					END_BUGSTAR_QUERY()
					SET_BUGSTAR_QUERY_FINISHED()
					
					IF sTaggedBugs.iBug > 0
						// Now get the developer names for bugs found
						PRINTLN("DO_LOAD_XML() - Grabbing the developer names for the bugs we just found")
						sTaggedBugs.iStage++
					ELSE
						sTaggedBugs.iStage = 0
						
						// Attempt made to load data so return true.
						bFinished = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Now get all the developer names, https://rsgedibgs1.rockstar.t2.corp:8443/BugstarRestService-1.0/rest/Projects/1546/Users/30
		CASE 5
			IF sTaggedBugs.iDev < sTaggedBugs.iBug
			
				// Attempt to start a new query if we haven't done so already
				IF NOT HAS_BUGSTAR_QUERY_STARTED()
					IF IS_BUGSTAR_QUERY_SAFE_TO_START(TRUE)
						// Start a new query
						TEXT_LABEL_63 tlQuery
						tlQuery = "Users/"
						tlQuery += sTaggedBugs.iBugDeveloper[sTaggedBugs.iDev]
						PRINTLN("DO_LOAD_XML() - Starting query: ", tlQuery)
						
						START_BUGSTAR_QUERY(tlQuery)
						SET_BUGSTAR_QUERY_STARTED()
					ENDIF
				ENDIF
				
				IF HAS_BUGSTAR_QUERY_STARTED()
					IF NOT IS_BUGSTAR_QUERY_PENDING()
						IF (IS_BUGSTAR_QUERY_SUCCESSFUL() AND NOT IS_BUTTON_PRESSED(PAD1, DPADDOWN))
							IF(CREATE_XML_FROM_BUGSTAR_QUERY())
								// Loop through all the nodes
								INT eachNode
								
								INT iNodeCount
								iNodeCount = GET_NUMBER_OF_XML_NODES()
								
								FOR eachNode = 0 TO (iNodeCount-1)
									IF GET_HASH_KEY(GET_XML_NODE_NAME()) = GET_HASH_KEY("friendlyName")
										tlTemp = "BSD_"
										tlTemp += sTaggedBugs.iDev
										ADD_DEBUG_STRING(tlTemp, GET_STRING_FROM_XML_NODE())
									ENDIF
									
									// Tell the script to goto the next node in the xml file
									GET_NEXT_XML_NODE()
								ENDFOR
								
								DELETE_XML_FILE()
							ENDIF
						ENDIF
						
						// Query finished pending and we will have processed a successful query so end
						END_BUGSTAR_QUERY()
						SET_BUGSTAR_QUERY_FINISHED()
						
						// Now get the next developer name
						sTaggedBugs.iDev++
					ENDIF
				ENDIF
			ELSE
				// Now get the LBCH tagged bugs
				PRINTLN("DO_LOAD_XML() - Finished grabbing all data")
				sTaggedBugs.iStage = 0
				
				// Attempt made to load data so return true.
				bFinished = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (bFinished)
ENDFUNC



PROC DRAW_LBD_LIST()

	FLOAT fBodyTextX = 0.685
	FLOAT fBodyTextY = 0.137
	FLOAT fBodyTextH = 0.035
	FLOAT fMenuBodyFinalH = 0.055
	
	FLOAT fMenuX = 0.675
	FLOAT fMenuY = 0.075
	FLOAT fMenuW = 0.250
	FLOAT fMenuH = 0.050
	
	FLOAT fTitleTextX = fMenuX+(fMenuW*0.5)
	FLOAT fTitleTextY = 0.075
	
	TEXT_LABEL_15 tlSummary //, tlDev
	INT iLineNumber
	INt iLineCount
	INT i
	FLOAT fTempTextY
	
	IF sTaggedBugs.bXML_loaded
		IF sTaggedBugs.iBug > 0
			fTempTextY = 0.0
			REPEAT sTaggedBugs.iBug i
				IF sTaggedBugs.iBugNumber[i] != 0
				
					//tlDev = "BSD_" tlDev += i
					tlSummary = "BSS_" tlSummary += i
					
					SET_TEXT_SCALE(0.345, 0.345)
					SET_TEXT_COLOUR(255, 255, 255, 255)
					SET_TEXT_CENTRE(FALSE)
					SET_TEXT_WRAP(fBodyTextX, fTitleTextX+(fMenuW*0.5)-(fBodyTextX-(fTitleTextX-(fMenuW*0.5))))
					SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
					SET_TEXT_EDGE(0, 0, 0, 0, 0)
					BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING("DM_BUG_CH")
						ADD_TEXT_COMPONENT_INTEGER(sTaggedBugs.iBugNumber[i])
						//ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlDev)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlSummary)
					iLineCount = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fBodyTextX, fBodyTextY+fTempTextY)
					fTempTextY +=  (fBodyTextH * iLineCount)
				ENDIF
			ENDREPEAT
		ENDIF
		IF fTempTextY > fMenuBodyFinalH
			fMenuBodyFinalH = fTempTextY
		ENDIF
	ENDIF
	
	// Title background
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+(fMenuH*0.5), 				fMenuW, 			fMenuH, 10, 10, 10, 200)
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+(fMenuH*0.5), 				fMenuW-0.005, 		fMenuH-0.01, 50, 50, 50, 200)
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+(fMenuH*0.5), 				fMenuW-0.005-0.003,	fMenuH-0.01-0.005, 0, 0, 0, 255)
	
	// Body background
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+fMenuH+(fMenuBodyFinalH*0.5), 	fMenuW, 			fMenuBodyFinalH, 10, 10, 10, 200)
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+fMenuH+(fMenuBodyFinalH*0.5), 	fMenuW-0.005, 		fMenuBodyFinalH-0.01, 50, 50, 50, 200)
	DRAW_RECT(fMenuX+(fMenuW*0.5), 	fMenuY+fMenuH+(fMenuBodyFinalH*0.5), 	fMenuW-0.005-0.003,	fMenuBodyFinalH-0.01-0.005, 0, 0, 0, 255)
	
	//Display the menu title		
	SET_TEXT_SCALE(0.575, 0.575)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
	SET_TEXT_EDGE(0, 0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fTitleTextX, fTitleTextY, "STRING", "LBCH Tagged Bugs")
	
	// Display items
	IF sTaggedBugs.bQuery_failed
		SET_TEXT_SCALE(0.345, 0.345)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_CENTRE(FALSE)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fBodyTextX, fBodyTextY, "STRING", "Bugstar query timed out")
	ELIF sTaggedBugs.bXML_loaded
		
		fTempTextY = 0.0
		
		IF sTaggedBugs.iBug > 0
			REPEAT sTaggedBugs.iBug i
				IF sTaggedBugs.iBugNumber[i] != 0
				
					//tlDev = "BSD_" tlDev += i
					tlSummary = "BSS_" tlSummary += i
					
					SET_TEXT_SCALE(0.345, 0.345)
					SET_TEXT_COLOUR(255, 255, 255, 255)
					SET_TEXT_CENTRE(FALSE)
					SET_TEXT_WRAP(fBodyTextX, fTitleTextX+(fMenuW*0.5)-(fBodyTextX-(fTitleTextX-(fMenuW*0.5))))
					SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
					SET_TEXT_EDGE(0, 0, 0, 0, 0)
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("DM_BUG_CH") //~1~ - ~a~ // ~r~~1~~w~ - ~a~~n~~a~
						ADD_TEXT_COMPONENT_INTEGER(sTaggedBugs.iBugNumber[i])
						//ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlDev)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlSummary)
					END_TEXT_COMMAND_DISPLAY_TEXT(fBodyTextX, fBodyTextY+fTempTextY)
					
					SET_TEXT_SCALE(0.345, 0.345)
					SET_TEXT_COLOUR(255, 255, 255, 255)
					SET_TEXT_CENTRE(FALSE)
					SET_TEXT_WRAP(fBodyTextX, fTitleTextX+(fMenuW*0.5)-(fBodyTextX-(fTitleTextX-(fMenuW*0.5))))
					SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
					SET_TEXT_EDGE(0, 0, 0, 0, 0)
					BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING("DM_BUG_CH")
						ADD_TEXT_COMPONENT_INTEGER(sTaggedBugs.iBugNumber[i])
						//ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlDev)
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(tlSummary)
					iLineCount = END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(fBodyTextX, fBodyTextY+fTempTextY)
					fTempTextY +=  (fBodyTextH * iLineCount)
					
					iLineNumber++
				ENDIF
			ENDREPEAT
		ENDIF
		
		//if there are no bugs
		IF iLineNumber = 0
			SET_TEXT_SCALE(0.345, 0.345)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_CENTRE(FALSE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
			SET_TEXT_EDGE(0, 0, 0, 0, 0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(fBodyTextX, fBodyTextY, "STRING", "No tagged bugs to display")
		ENDIF
	ELSE
		SET_TEXT_SCALE(0.345, 0.345)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_CENTRE(FALSE)
		SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		SET_TEXT_EDGE(0, 0, 0, 0, 0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(fBodyTextX, fBodyTextY, "STRING", "Loading tagged bugs...")
	ENDIF
ENDPROC

INT iMenuDisplayTimer
BOOL bInitialDrawn
PROC DO_LBD_LIST()

	IF NOT g_debugMenuControl.bUseBugstarQuery
	OR g_bugstarQuery.bQueriesDisabled
		EXIT
	ENDIF

	//if the menu is set to be displayed
	//IF g_bDisplayLBDBugSummary
		//load the XML
		IF (sTaggedBugs.bXML_loaded = FALSE AND bDrawMenu = TRUE)
		OR (sTaggedBugs.bXML_loaded = FALSE AND g_bDisplayLBDBugSummary = TRUE)
			IF DO_LOAD_XML()
				sTaggedBugs.bXML_loaded = TRUE
				iMenuDisplayTimer = GET_GAME_TIMER() + 7500
				
				// Add an extra 10 seconds if we have bugs to display
				IF sTaggedBugs.iBug > 1
					iMenuDisplayTimer += 15000
				ENDIF
			ENDIF
		ENDIF
		
		//once loaded display the menu if needed. 
		IF (bInitialDrawn = FALSE AND g_bDisplayLBDBugSummary)
		OR bDrawMenu
			DRAW_LBD_LIST()
		ENDIF
		
		// cancel the timer if time is up or we are displaying
		IF sTaggedBugs.bXML_loaded 
			IF (GET_GAME_TIMER() > iMenuDisplayTimer)
			OR bDrawMenu
				bInitialDrawn = TRUE
			ENDIF
		ENDIF
		
		//cancel if a skip button has been pressed
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_M)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bInitialDrawn = TRUE
		ENDIF	
		
	//ENDIF
ENDPROC


//PURPOSE: Draws a menu of current mission stages, Returns true when a stage has been selected
FUNC BOOL LAUNCH_MISSION_STAGE_MENU(MissionStageMenuTextStruct &StructPassed[], INT &iReturnStage, INT iSelectedPassed = 0, BOOL bDoJPSkips = FALSE,STRING MenuTitlePassed = NULL, 
									BOOL bDoCHeckMenu = TRUE,BOOL bDoNewJPSkipReset = FALSE, KEY_NUMBER knKeyPassed = KEY_Z, KEYBOARD_MODIFIER keyboardModifier = KEYBOARD_MODIFIER_NONE, 
									BOOL bUseReturns = TRUE, BOOL bSkipNonSelectableStages = FALSE, FLOAT fRowHeightMultiplier = 1.0)
	
	//Draw the LBD menu
	IF 	bDoCHeckMenu
	AND IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
	AND NOT g_bMagDemoActive
		DO_LBD_LIST()
	ENDIF
	
	//STRING sTHreadName = GET_THIS_SCRIPT_NAME()
	//checks to see if the menu should be drawn
	IF bDrawMenu = FALSE
		IF IS_DEBUG_KEY_JUST_PRESSED(knKeyPassed, keyboardModifier, "launch Z menu")	
			bDrawMenu = TRUE
			iSelectedMenu = iSelectedPassed
			
			// If we failed to get the Bugstar data before, try again
			IF sTaggedBugs.bQuery_failed
				sTaggedBugs.iStage = 0
				sTaggedBugs.bXML_loaded = FALSE
				sTaggedBugs.bQuery_failed = FALSE
			ENDIF
		ENDIF
	ELSE
	
		// block cellphone
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
		iSelectMissionStageDrawTime = GET_GAME_TIMER()
	
	//makes the mission debug menu
		INT i									//for the repeat
		INT MenuLength = COUNT_OF(StructPassed)	//length of the menu
		FLOAT fMenuHeight						//height of the menu for drawing the border
		IF IS_STRING_NULL(MenuTitlePassed)		//if the menu title has not been set then set one to a default
			MenuTitlePassed = "Mission Stages"
		ENDIF

		//make sure it's a non NULL String that is selected to start with
		RETURN_FIRST_NON_NULL_STRING(StructPassed,iSelectedMenu)
		
		IF bDrawMenu = TRUE//stays in here untill you quit out of the menu. 
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			// Move down menu
			IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_DOWN)
				IF GET_GAME_TIMER() > iButtonPressedTimer 
					//Move DOWN and check if the string is NULL or we've gone longer than the menu Length go back to the start
					RETURN_NON_NULL_STRING(TRUE,StructPassed,iSelectedMenu)
					iButtonPressedTimer = GET_GAME_TIMER() + SKIP_MENU_MOVE_DELAY
				ENDIF
			ENDIF
			
			// Move up menu
			IF IS_BUTTON_PRESSED(PAD1, DPADUP)
			OR IS_KEYBOARD_KEY_PRESSED(KEY_UP)
				IF GET_GAME_TIMER() > iButtonPressedTimer 
					//Move UP and check if the string is NULL or we've gone less than one go to the menu end
					RETURN_NON_NULL_STRING(FALSE,StructPassed,iSelectedMenu)
					iButtonPressedTimer = GET_GAME_TIMER() + SKIP_MENU_MOVE_DELAY
				ENDIF			
			ENDIF			
			
			//draw the menu background and borders
			fMenuHeight = (MenuLength*MENU_ITEM_LINE_HIGHT+TOP_MENU_HEIGHT)
			IF fMenuHeight > SKIP_MENU_MAX_HEIGHT
				fMenuHeight  = SKIP_MENU_MAX_HEIGHT
			ENDIF 			
			//Main background
			DRAW_RECT(0.1460, ((SKIP_MENU_MENU_TITLE_HEIGHT*0.5)+(fMenuHeight*0.5))+SKIP_MENU_MOVE_DOWN_BY, 0.2300, fMenuHeight+0.0890, 10, 10, 10, 180)
			//items border
			FLOAT fPanelY = ((SKIP_MENU_MENU_TITLE_BORDER_HEIGHT*0.5)+(fMenuHeight*0.5))+SKIP_MENU_MOVE_DOWN_BY
			DRAW_RECT(0.1460, fPanelY, 0.2160, fMenuHeight +0.0120, 50, 50, 50, 255)
			//items background
			DRAW_RECT(0.1460, fPanelY, 0.2100, fMenuHeight, 0, 0, 0, 255)
			//draw the item selected box
			SKIP_MENU_DO_SELECTED_BOX(MenuLength, DEFAULT, DEFAULT, fRowHeightMultiplier)
			//Display the slide bar if needed,
			IF MenuLength > SKIP_MENU_MAX_STRING_DISPLAY
				SKIP_MENU_DO_SLIDE_BAR(MenuLength, fPanelY, fMenuHeight)
			ENDIF
			//Display the menu title border
			DRAW_RECT(0.1460, 0.0690+SKIP_MENU_MOVE_DOWN_BY, 0.2160, 0.0600, 50, 50, 50, 255)
			DRAW_RECT(0.1460, 0.0690+SKIP_MENU_MOVE_DOWN_BY, 0.2100, 0.0500, 1, 0, 0, 255)
			//Display the menu title		
			SET_TEXT_SCALE(0.6800, 0.6800)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_CENTRE(TRUE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)
			SET_TEXT_EDGE(0, 0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.1460, 0.0450+SKIP_MENU_MOVE_DOWN_BY, "STRING", MenuTitlePassed)
			
			INT iMenuStringStart = 0
			FOR i = 0 TO (SKIP_MENU_MAX_STRING_DISPLAY -1)
				IF i < MenuLength
					//26 is the maximum strings that you can have
					IF MenuLength > (SKIP_MENU_MAX_STRING_DISPLAY + 1)//26 is the maximum strings that you can have
						IF iSelectedMenu > SKIP_MENU_MOVE_POINT
							iMenuStringStart = iSelectedMenu - SKIP_MENU_MOVE_POINT
							IF iMenuStringStart > MenuLength - SKIP_MENU_MAX_STRING_DISPLAY
								iMenuStringStart = MenuLength - SKIP_MENU_MAX_STRING_DISPLAY
							ENDIF
						ENDIF
					ENDIF
					//If the string is <> to null then display it to avoid asserts of the menu array passed is too big.
					IF NOT IS_STRING_NULL(StructPassed[i+iMenuStringStart].sTxtLabel)
						IF StructPassed[i+iMenuStringStart].bSelectable = TRUE
							SET_TEXT_SCALE(X_SCALE * fRowHeightMultiplier, Y_SCALE * fRowHeightMultiplier)
							SET_TEXT_COLOUR(255, 255, 255, 255)
						ELSE
							SET_TEXT_SCALE(X_SCALE * fRowHeightMultiplier, (Y_SCALE+(0.5*Y_SCALE)) * fRowHeightMultiplier)
							SET_TEXT_COLOUR(110, 110, 110, 255)
						ENDIF
						SET_TEXT_EDGE(1, 0, 0, 0, 255)
						DISPLAY_TEXT_WITH_LITERAL_STRING(DISPLAY_AT_X, DISPLAY_AT_Y+SKIP_MENU_MOVE_DOWN_BY + (MENU_ITEM_LINE_HIGHT * fRowHeightMultiplier * i), "STRING", StructPassed[i+iMenuStringStart].sTxtLabel)
					ENDIF
				ENDIF
			ENDFOR
			//to quit out if KEY_Z is pressed again
			IF IS_DEBUG_KEY_JUST_PRESSED(knKeyPassed, keyboardModifier, "launch Z menu")
				sTaggedBugs.bWait_for_z_release = TRUE
			ENDIF
			IF sTaggedBugs.bWait_for_z_release
				IF NOT IS_DEBUG_KEY_JUST_PRESSED(knKeyPassed, keyboardModifier, "launch Z menu")
					sTaggedBugs.bWait_for_z_release = FALSE
					//stop drawing the menu
					bDrawMenu = FALSE
				ENDIF
			ENDIF
			
			//to quit out if one of the main debug keys are pressed
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_M)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)			
			OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)			
				//stop drawing the menu
				bDrawMenu = FALSE
			ENDIF	
			
			//to select
			IF bUseReturns
				IF IS_BUTTON_PRESSED(PAD1, CROSS)
				OR IS_KEYBOARD_KEY_PRESSED(KEY_RETURN)
					//set the return value
					iReturnStage = iSelectedMenu
					//stop drawing the menu
					bDrawMenu = FALSE
					RETURN (TRUE)
				ENDIF
			ELSE
				iReturnStage = iSelectedMenu
			ENDIF
		ENDIF	
	ENDIF
	
	
	
	//control the J and P Skips oly if the bool bDoJPSkips is set
	smLastSkipMethod = SM_Z_SKIP //Assume Z skip was pressed if not triggered by P or J.
	IF bDoJPSkips = TRUE
		//set the current stage
		iReturnStage = iSelectedPassed
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_P))
				WAIT (0)
			ENDWHILE		
			smLastSkipMethod = SM_P_SKIP

			iReturnStage--
			IF bSkipNonSelectableStages
				iReturnStage--
				WHILE NOT StructPassed[iReturnStage].bSelectable
					iReturnStage--
					// can't skip backward as no selectable menu item was found before the current selected item
					IF iReturnStage < 0
						iReturnStage = iSelectedMenu	
					ENDIF
				ENDWHILE
			ENDIF
			IF iReturnStage < 0
				iReturnStage = 0
			ENDIF
			
			RETURN (TRUE)
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_J))
				WAIT (0)
			ENDWHILE		
			smLastSkipMethod = SM_J_SKIP

			iReturnStage++
			
			IF bSkipNonSelectableStages
				WHILE NOT StructPassed[iReturnStage].bSelectable
					iReturnStage++
					// can't skip forward as no selectable menu item was found past the current selected item
					IF iReturnStage > COUNT_OF(StructPassed)
						iReturnStage = iSelectedMenu	
					ENDIF
				ENDWHILE
			ENDIF
			
			IF iReturnStage > COUNT_OF(StructPassed)
				iReturnStage = COUNT_OF(StructPassed)
			ENDIF
			
			RETURN (TRUE)
		ENDIF
	ENDIF
	
	SWITCH smLastSkipMethod
	
//		CASE SM_Z_SKIP
//			PRINTLN(("Z MENU: Last skip method was **Z**"))
//		BREAK
	
		CASE SM_J_SKIP
			PRINTLN(("Z MENU: Last skip method was **J**"))
		BREAK
		CASE SM_P_SKIP
			PRINTLN(("Z MENU: Last skip method was **P**"))
		BREAK
		
	ENDSWITCH
	
	IF bDoNewJPSkipReset = TRUE
		//set the current stage
		iReturnStage = iSelectedPassed
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_P))
				WAIT (0)
			ENDWHILE
			
			INT current_time 
			current_time = GET_GAME_TIMER()
			
			IF ((current_time - iThisPSkipTimer) < 5000) 
			
				iReturnStage--
			
				IF bSkipNonSelectableStages
					WHILE NOT StructPassed[iReturnStage].bSelectable
						iReturnStage--
						// can't skip as no selectable menu item was found before the current selected item
						IF iReturnStage < 0
							iReturnStage = iSelectedMenu	
						ENDIF
					ENDWHILE
				ENDIF
				
			ENDIF
			
			iThisPSkipTimer = GET_GAME_TIMER()
		
			IF iReturnStage < 0
				iReturnStage = 0
			ENDIF
			
			RETURN (TRUE)
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			WHILE (IS_KEYBOARD_KEY_PRESSED(KEY_J))
				WAIT (0)
			ENDWHILE		
			
			iReturnStage++
			IF bSkipNonSelectableStages
				WHILE NOT StructPassed[iReturnStage].bSelectable
					iReturnStage++
					// can't skip as no selectable menu item was found past the current selected item
					IF iReturnStage > COUNT_OF(StructPassed)
						iReturnStage = iSelectedMenu	
					ENDIF
				ENDWHILE
			ENDIF
			
			IF iReturnStage > COUNT_OF(StructPassed)
				iReturnStage = COUNT_OF(StructPassed)
			ENDIF
			RETURN (TRUE)
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC
	FUNC  TEXT_LABEL_63 GET_FMMC_STRING(INT i)
		TEXT_LABEL_63 tl63Return
		IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName)
			SWITCH g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].iType
				CASE FMMC_TYPE_MISSION		
					tl63Return = "Mission        - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return				
				CASE FMMC_TYPE_SURVIVAL		
					tl63Return = "Horde        - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return
				CASE FMMC_TYPE_GANGHIDEOUT		
					tl63Return = "Ganghide Out   - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return
				CASE FMMC_TYPE_BASE_JUMP
					tl63Return = "BaseJump     - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return
//				CASE FMMC_TYPE_TRIATHLON		
//					tl63Return = "Triathlon     - " 
//					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
//					RETURN tl63Return
				CASE FMMC_TYPE_DEATHMATCH	
					tl63Return = "Deathmatch   - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return
				CASE FMMC_TYPE_RACE			
					tl63Return = "Race              - " 
					tl63Return += g_FMMC_HEADER_STRUCT.sMyMissionHeaderVars[i].tlMissionName
					RETURN tl63Return
			ENDSWITCH
		ENDIF
		
		tl63Return = "Slot "
		tl63Return +=i
		RETURN tl63Return
	
	ENDFUNC

	PROC SET_UP_FMMC_SKIP_NAMES(MissionStageMenuTextStruct &SkipMenuStruct[], BOOL bUseChrissNames = FALSE, BOOL BuseFMMCstruct = FALSE)
	IF bUseChrissNames = FALSE
		IF BuseFMMCstruct
			INT i
			FOR i = 0 TO (COUNT_OF(SkipMenuStruct) -1)
				SkipMenuStruct[i].sTxtLabel 		= GET_FMMC_STRING(i)
			ENDFOR
		ELSE
			SkipMenuStruct[0].sTxtLabel 		= "Default"
			SkipMenuStruct[1].sTxtLabel 		= "Slot 1"
			SkipMenuStruct[2].sTxtLabel 		= "Slot 2"
			SkipMenuStruct[3].sTxtLabel 		= "Slot 3"
			SkipMenuStruct[4].sTxtLabel 		= "Slot 4"
			SkipMenuStruct[5].sTxtLabel 		= "Slot 5"
			SkipMenuStruct[6].sTxtLabel 		= "Slot 6"
			SkipMenuStruct[7].sTxtLabel 		= "Slot 7"
			SkipMenuStruct[8].sTxtLabel 		= "Slot 8"
			SkipMenuStruct[9].sTxtLabel 		= "Slot 9"
			SkipMenuStruct[10].sTxtLabel 		= "Slot 10"
			SkipMenuStruct[11].sTxtLabel 		= "Slot 11"
			SkipMenuStruct[12].sTxtLabel 		= "Slot 12"
			SkipMenuStruct[13].sTxtLabel 		= "Slot 13"
			SkipMenuStruct[14].sTxtLabel 		= "Slot 14"
			SkipMenuStruct[15].sTxtLabel 		= "Slot 15"
			SkipMenuStruct[16].sTxtLabel 		= "Slot 16"
			SkipMenuStruct[17].sTxtLabel 		= "Slot 17"
			SkipMenuStruct[18].sTxtLabel 		= "Slot 18"
			SkipMenuStruct[19].sTxtLabel 		= "Slot 19"
			SkipMenuStruct[20].sTxtLabel 		= "Slot 20"
			SkipMenuStruct[21].sTxtLabel 		= "Slot 21"
			SkipMenuStruct[22].sTxtLabel 		= "Slot 22"
			SkipMenuStruct[23].sTxtLabel 		= "Slot 23"
			SkipMenuStruct[24].sTxtLabel 		= "Slot 24"
			SkipMenuStruct[25].sTxtLabel 		= "Slot 25"
			SkipMenuStruct[26].sTxtLabel 		= "Slot 26"
			SkipMenuStruct[27].sTxtLabel 		= "Slot 27"
			SkipMenuStruct[28].sTxtLabel 		= "Slot 28"
			SkipMenuStruct[29].sTxtLabel 		= "Slot 29"
			SkipMenuStruct[30].sTxtLabel 		= "Slot 30"
		ENDIF
	ELSE
		SkipMenuStruct[0].sTxtLabel 		= "Default"
		SkipMenuStruct[1].sTxtLabel 		= "1  - Villa"           
		SkipMenuStruct[2].sTxtLabel 		= "2  - Observatory"     
		SkipMenuStruct[3].sTxtLabel 		= "3  - Golf Course"     
		SkipMenuStruct[4].sTxtLabel 		= "4  - Dam"             
		SkipMenuStruct[5].sTxtLabel 		= "5  - Docks 2"         
		SkipMenuStruct[6].sTxtLabel 		= "6  - Reservoir"       
		SkipMenuStruct[7].sTxtLabel 		= "7  - Motel"           
		SkipMenuStruct[8].sTxtLabel 		= "8  - Fire Station"    
		SkipMenuStruct[9].sTxtLabel 		= "9  - Forest"          
		SkipMenuStruct[10].sTxtLabel 		= "10 - Biolab"          
		SkipMenuStruct[11].sTxtLabel 		= "11 - Movie Set"       
		SkipMenuStruct[12].sTxtLabel 		= "12 - Pier"            
		SkipMenuStruct[13].sTxtLabel 		= "13 -"
		SkipMenuStruct[14].sTxtLabel 		= "14 -"
		SkipMenuStruct[15].sTxtLabel 		= "15 -"
		SkipMenuStruct[16].sTxtLabel 		= "16 -"
		SkipMenuStruct[17].sTxtLabel 		= "17 -"
		SkipMenuStruct[18].sTxtLabel 		= "18 -"
		SkipMenuStruct[19].sTxtLabel 		= "19 -"
		SkipMenuStruct[20].sTxtLabel 		= "20 -"
		SkipMenuStruct[21].sTxtLabel 		= "21 -"
		SkipMenuStruct[22].sTxtLabel 		= "22 -"
		SkipMenuStruct[23].sTxtLabel 		= "23 -"
		SkipMenuStruct[24].sTxtLabel 		= "24 -"
		SkipMenuStruct[25].sTxtLabel 		= "25 -"
		SkipMenuStruct[26].sTxtLabel 		= "26 -"
		SkipMenuStruct[27].sTxtLabel 		= "27 -"
		SkipMenuStruct[28].sTxtLabel 		= "28 -"
		SkipMenuStruct[29].sTxtLabel 		= "29 -"
		SkipMenuStruct[30].sTxtLabel 		= "30 -"
	ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    This helps to translate missions stages to their corresponding index in the z skip menu. 
	///    This allows the user to have non-selectable entries in their z-menu (for the likes of cutscene names, breakers, etc) 
	///    Without breaking the skip logic.
	///    *NOTE* Use the param in the LAUNCH_MISSION_STAGE_MENU() call for this to work correctly. This will make J/P skips with the z-menu skip over
	///    the non-selectable stages.
	/// PARAMS:
	///    iStageToTranslate - The current stage your mission is at, or the stage you wish to get the corresponding z-menu entry for.
	///    iCorrespondingMenuIndex - This is the resulting index in the z-menu struct of the specified stage.
	/// RETURNS:
	///    Returns TRUE if it finds a corresponding entry, will only ever return false if it reaches the end of the menu without finding an entry.
	FUNC BOOL TRANSLATE_TO_Z_MENU(MissionStageMenuTextStruct &SkipMenuStruct[], INT iStageToTranslate, INT &iCorrespondingMenuIndex)

		INT iSelectableIndexCounter = 0
		INT i

		FOR i = 0 TO COUNT_OF(SkipMenuStruct)
		
			IF SkipMenuStruct[i].bSelectable
				IF iSelectableIndexCounter = iStageToTranslate
					iCorrespondingMenuIndex = i	
					RETURN TRUE
				ELSE
					iSelectableIndexCounter++
				ENDIF			
			ENDIF
		
		ENDFOR
		
		
		RETURN FALSE

	ENDFUNC

	/// PURPOSE:
	///    This helps to translate the z menu index into a mission stage taking into account stages that 
	///    are non-selectable that would otherwise throw out the ordering.
	///    This allows the user to have non-selectable entries in their z-menu (for the likes of cutscene names, breakers, etc) 
	///    Without breaking the skip logic.
	///    *NOTE* Use the param in the LAUNCH_MISSION_STAGE_MENU() call for this to work correctly. This will make J/P skips with the z-menu skip over
	///    the non-selectable stages.
	/// PARAMS:
	///    iZMenuIndex - The z menu index you wish to translate to a mission stage. This will be 1 to 1 if you don't have any non-selectable entries
	/// RETURNS:
	///    Returns the corresponding stage index. Again this will be 1 to 1 if you have no non-serlectable entries
	FUNC INT TRANSLATE_FROM_Z_MENU(MissionStageMenuTextStruct &SkipMenuStruct[], INT iZMenuIndex)
		
		INT i
		INT iNonSelectableCounter = 0
		
		FOR i = 0 TO iZMenuIndex
		
			CDEBUG3LN(DEBUG_MISSION, "i: ", i)
			IF NOT SkipMenuStruct[i].bSelectable
				iNonSelectableCounter++
			ENDIF
		
		ENDFOR
		
		RETURN iZMenuIndex - iNonSelectableCounter

	ENDFUNC


	
#ENDIF	


