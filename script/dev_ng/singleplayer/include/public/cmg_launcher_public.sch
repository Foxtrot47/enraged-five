USING "cmg_support_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_launcher_public.sch
//	Sam Hackett
//
//	For functions that are needed by both the launcher (GTA) and minigames (GTA or BOB)
//	- Grabbing entities on minigame start
//	- Returning entities on minigame end
//
//	See also:
//		cmg_launcher_private.sch
//		bjack_sp.sc
//		poker_sp.sc
//		roulette_sp.sc
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


FUNC BOOL CLIFFORD_MG_GET_TABLE_MODEL(CMG_TYPE eMgType, MODEL_NAMES& eModel)	// Also done in-line
	SWITCH (eMgType)
		CASE CMG_TYPE_Blackjack
			eModel = INT_TO_ENUM( MODEL_NAMES, HASH("agt_prop_casino_blckjack_01") )
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Poker
			eModel = INT_TO_ENUM( MODEL_NAMES, HASH("agt_prop_casino_poker_01") )
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Roulette
			eModel = INT_TO_ENUM( MODEL_NAMES, HASH("agt_prop_casino_roulette_01") )
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Slot
			eModel = INT_TO_ENUM( MODEL_NAMES, HASH("agt_prop_casino_slot_01a") )
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Dancing
			// not triggered from model
			RETURN TRUE
		BREAK
		DEFAULT	// ERROR
			CPRINTLN(DEBUG_MINIGAME,	"GET_CLIFFORD_MG_TABLE_MODEL() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eMgType))
			SCRIPT_ASSERT(				"GET_CLIFFORD_MG_TABLE_MODEL() Invalid minigame type")
		BREAK
	ENDSWITCH

	eModel = DUMMY_MODEL_FOR_SCRIPT
	RETURN FALSE
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// CLIFFORD TABLE /////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC INT CLIFFORD_MG_SEAT_COUNT(CMG_TYPE eMgType)
	SWITCH eMgType
		CASE CMG_TYPE_Blackjack		RETURN 4		BREAK
		CASE CMG_TYPE_Poker			RETURN 5		BREAK
		CASE CMG_TYPE_Roulette		RETURN 4		BREAK
		CASE CMG_TYPE_Slot			RETURN 1		BREAK
		CASE CMG_TYPE_Dancing		RETURN 1		BREAK
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MINIGAME, "CLIFFORD_MG_SEAT_COUNT() - Unknown minigame type: ", eMgType)
	SCRIPT_ASSERT("CLIFFORD_MG_SEAT_COUNT() - Unknown minigame type")
	RETURN 0
ENDFUNC

FUNC BOOL CLIFFORD_MG_HAS_DEALER(CMG_TYPE eMgType)
	SWITCH eMgType
		CASE CMG_TYPE_Blackjack		FALLTHRU
		CASE CMG_TYPE_Poker			FALLTHRU
		CASE CMG_TYPE_Roulette
			RETURN TRUE
		BREAK
		CASE CMG_TYPE_Slot
			RETURN FALSE
		BREAK
		CASE CMG_TYPE_Dancing
			RETURN FALSE
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_MINIGAME, "CLIFFORD_MG_GET_TYPE_HAS_DEALER() - Unknown minigame type: ", eMgType)
			SCRIPT_ASSERT("CLIFFORD_MG_GET_TYPE_HAS_DEALER() - Unknown minigame type")
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC BOOL CLIFFORD_MG_GET_TABLE_OBJECT(CMG_TYPE eMgType, VECTOR vTablePos, OBJECT_INDEX& hTable, BOOL bFreezeOnDetect = TRUE)

	// Get table model
	MODEL_NAMES eTableModel
	IF NOT CLIFFORD_MG_GET_TABLE_MODEL(eMgType, eTableModel)
		RETURN FALSE
	ELSE

		// Check area for table
		hTable = GET_CLOSEST_OBJECT_OF_TYPE(vTablePos, 1.0, eTableModel, FALSE)
		IF DOES_ENTITY_EXIST(hTable)
		OR eMgType = CMG_TYPE_Dancing

			// Freeze table position
			IF bFreezeOnDetect
				IF DOES_ENTITY_EXIST(hTable)
					FREEZE_ENTITY_POSITION(hTable, TRUE)
				ENDIF
			ENDIF
			
			RETURN TRUE
		
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// CLIFFORD SEAT POS //////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL CLIFFORD_MG_GET_SEAT_POS(CMG_TYPE eMgType, OBJECT_INDEX hTable, INT iSeatID, VECTOR& vPos, FLOAT& fRot, BOOL bReturnAsOffset = FALSE)

	VECTOR	vSeatPosOffset
	FLOAT	fSeatRotOffset
	
	// Set default values (in case of error)
	IF NOT bReturnAsOffset
		vPos = GET_ENTITY_COORDS(hTable)
		fRot = GET_ENTITY_HEADING(hTable)
	ELSE
		vPos = << 0.0, 0.0, 0.0 >>
		fRot = 0.0
	ENDIF

	SWITCH eMgType
		
		CASE CMG_TYPE_Blackjack
			SWITCH iSeatID	// Seat height = 0.64m
				CASE 0
					vSeatPosOffset = <<1.195679, 0.162567, 0.997757>> //+ <<-0.400000, 0.100000, 0.150000>>
					fSeatRotOffset = 77.046165
				BREAK
				CASE 1
					vSeatPosOffset = <<0.605591, -0.564148, 0.997452>> //+ <<-0.170000, 0.300000, 0.150000>>
					fSeatRotOffset = 28.437315
				BREAK
				CASE 2
					vSeatPosOffset = <<-0.509766, -0.470917, 0.997467>> //+ <<0.090000, 0.170000, 0.150000>>
					fSeatRotOffset = 330.080383
				BREAK
				CASE 3
					vSeatPosOffset = <<-1.196228, 0.182098, 0.997620>> //+ <<0.380000, 0.070000, 0.150000>>
					fSeatRotOffset = 284.755676
				BREAK
				DEFAULT	// ERROR
					CPRINTLN(DEBUG_MINIGAME,	"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type \"", ENUM_NAME_CMG_TYPE(eMgType), "\" does not support seat ID ", iSeatID)
					SCRIPT_ASSERT(				"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type does not support given seat ID")
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CMG_TYPE_Poker
			SWITCH iSeatID
				CASE 0
					vSeatPosOffset = <<1.642639, -0.151947, 0.997543>>
					fSeatRotOffset = 90.571457
				BREAK
				CASE 1
					vSeatPosOffset = <<1.064819, -1.080872, 0.997253>>
					fSeatRotOffset = 19.304749
				BREAK
				CASE 2
					vSeatPosOffset = <<0.077698, -1.023041, 0.997375>>
					fSeatRotOffset = 359.610809
				BREAK
				CASE 3
					vSeatPosOffset = <<-0.908508, -0.904968, 0.997208>>
					fSeatRotOffset = 342.092499
				BREAK
				CASE 4
					vSeatPosOffset = <<-1.682190, -0.226624, 0.997726>>
					fSeatRotOffset = 267.673615
				BREAK
				DEFAULT	// ERROR
					CPRINTLN(DEBUG_MINIGAME,	"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type \"", ENUM_NAME_CMG_TYPE(eMgType), "\" does not support seat ID ", iSeatID)
					SCRIPT_ASSERT(				"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type does not support given seat ID")
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK

		CASE CMG_TYPE_Roulette
			SWITCH iSeatID
				CASE 0
					vSeatPosOffset = <<-0.197754, -0.962830, 0.997253>>
					fSeatRotOffset = 359.645233
				BREAK
				CASE 1
					vSeatPosOffset = <<0.810059, -0.850616, 0.997665>>
					fSeatRotOffset = 359.177429
				BREAK
				CASE 2
					vSeatPosOffset = <<1.513367, -0.122192, 0.997360>>
					fSeatRotOffset = 89.345108
				BREAK
				CASE 3
					vSeatPosOffset = <<0.783203, 0.732941, 0.997421>>
					fSeatRotOffset = 178.491562
				BREAK
				DEFAULT	// ERROR
					CPRINTLN(DEBUG_MINIGAME,	"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type \"", ENUM_NAME_CMG_TYPE(eMgType), "\" does not support seat ID ", iSeatID)
					SCRIPT_ASSERT(				"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type does not support given seat ID")
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT	// ERROR
			CPRINTLN(DEBUG_MINIGAME,	"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type \"", ENUM_NAME_CMG_TYPE(eMgType), "\" not supported")
			SCRIPT_ASSERT(				"CLIFFORD_MG_GET_SEAT_OFFSET() - Table type not supported")
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	// Apply offset (if required)
	IF NOT bReturnAsOffset
		vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, vSeatPosOffset) 
		fRot = fRot + fSeatRotOffset
	ELSE
		vPos = vSeatPosOffset
		fRot = fSeatRotOffset
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL CLIFFORD_MG_GET_SEAT_PED(CMG_TYPE eMgType, OBJECT_INDEX hTable, INT iSeatID, PED_INDEX& hPed, FLOAT fRadius = 0.5, BOOL bSetAsMissionEntity = FALSE)

	VECTOR	vSeatPos
	FLOAT	fSeatRot
	IF CLIFFORD_MG_GET_SEAT_POS(eMgType, hTable, iSeatID, vSeatPos, fSeatRot)
		
		// THIS HAS TO BE CALLED BEFORE CALLING THIS OR SCENARIO PEDS WON'T GET RETURNED
		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		IF GET_CLOSEST_PED(vSeatPos, fRadius, TRUE, TRUE, hPed, FALSE, TRUE)//GET_RANDOM_PED_AT_COORD(vSeatPos, <<1,1,1>>)
		
			IF NOT IS_PED_INJURED(hPed)
				
				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_USING_ANY_SCENARIO(hPed))
				
					IF bSetAsMissionEntity
						SET_ENTITY_AS_MISSION_ENTITY(hPed, TRUE, TRUE)
					ENDIF
					RETURN TRUE
					
				ENDIF
			
			ENDIF
		
		ENDIF

	ENDIF
	
	hPed = NULL
	RETURN FALSE

ENDFUNC

FUNC BOOL CLIFFORD_MG_GET_SEAT_PED_FROM_ORIGIN(CMG_TYPE eMgType, VECTOR vOriginPos, FLOAT fOriginRot, INT iSeatID, PED_INDEX& hPed, FLOAT fRadius = 0.5, BOOL bSetAsMissionEntity = FALSE)

	VECTOR	vOffsetPos
	FLOAT	fOffsetRot
	IF CLIFFORD_MG_GET_SEAT_POS(eMgType, NULL, iSeatID, vOffsetPos, fOffsetRot, TRUE)
		
		VECTOR vSeatPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOriginPos, fOriginRot, vOffsetPos)
		
		// THIS HAS TO BE CALLED BEFORE CALLING THIS OR SCENARIO PEDS WON'T GET RETURNED
		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		IF GET_CLOSEST_PED(vSeatPos, fRadius, TRUE, TRUE, hPed, FALSE, TRUE)//GET_RANDOM_PED_AT_COORD(vSeatPos, <<1,1,1>>)
		
			IF NOT IS_PED_INJURED(hPed)
				
				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_USING_ANY_SCENARIO(hPed))
				
					IF bSetAsMissionEntity
						SET_ENTITY_AS_MISSION_ENTITY(hPed, TRUE, TRUE)
					ENDIF
					RETURN TRUE
					
				ENDIF
			
			ENDIF
		
		ENDIF

	ENDIF
	
	hPed = NULL
	RETURN FALSE

ENDFUNC

FUNC BOOL CLIFFORD_MG_IS_SEAT_PED(CMG_TYPE eMgType, OBJECT_INDEX hTable, INT iSeatID, PED_INDEX hPed, FLOAT fRadius = 0.5)

	VECTOR	vSeatPos
	FLOAT	fSeatRot
	IF CLIFFORD_MG_GET_SEAT_POS(eMgType, hTable, iSeatID, vSeatPos, fSeatRot)
	
		IF NOT IS_PED_INJURED(hPed)

			IF IS_ENTITY_AT_COORD(hPed, <<fRadius, fRadius, fRadius>>)
		
				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_USING_ANY_SCENARIO(hPed))
					RETURN TRUE
				ENDIF
			ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////// CLIFFORD DEALER POS ////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL CLIFFORD_MG_GET_DEALER_POS(CMG_TYPE eMgType, OBJECT_INDEX hTable, VECTOR& vPos, FLOAT& fRot, BOOL bReturnAsOffset = FALSE)

	VECTOR	vSeatPosOffset
	FLOAT	fSeatRotOffset
	
	// Set default values (in case of error)
	IF NOT bReturnAsOffset
		vPos = GET_ENTITY_COORDS(hTable)
		fRot = GET_ENTITY_HEADING(hTable)
	ELSE
		vPos = << 0.0, 0.0, 0.0 >>
		fRot = 0.0
	ENDIF

	SWITCH eMgType
		
		CASE CMG_TYPE_Blackjack
			vSeatPosOffset = <<0.003174, 0.785645, 0.997299>>
			fSeatRotOffset = 180.034927
		BREAK
		
		CASE CMG_TYPE_Poker
			vSeatPosOffset = <<-0.014343, 0.707825, 0.996964>>
			fSeatRotOffset = 178.043274
		BREAK

		CASE CMG_TYPE_Roulette
			vSeatPosOffset = <<-0.292236, 0.855560, 0.997147>>
			fSeatRotOffset = 180.648087
		BREAK

		DEFAULT	// ERROR
			CPRINTLN(DEBUG_MINIGAME,	"CLIFFORD_MG_GET_DEALER_POS() - Table type \"", ENUM_NAME_CMG_TYPE(eMgType), "\" not supported")
			SCRIPT_ASSERT(				"CLIFFORD_MG_GET_DEALER_POS() - Table type not supported")
			RETURN FALSE
		BREAK

	ENDSWITCH
	
	// Apply offset (if required)
	IF NOT bReturnAsOffset
		vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fRot, vSeatPosOffset) 
		fRot = fRot + fSeatRotOffset
	ELSE
		vPos = vSeatPosOffset
		fRot = fSeatRotOffset
	ENDIF

	RETURN TRUE

ENDFUNC

FUNC BOOL CLIFFORD_MG_GET_DEALER_PED(CMG_TYPE eMgType, OBJECT_INDEX hTable, PED_INDEX& hPed, BOOL bSetAsMissionEntity = FALSE)

	VECTOR	vPos
	FLOAT	fRot
	IF CLIFFORD_MG_GET_DEALER_POS(eMgType, hTable, vPos, fRot)
	
		IF GET_CLOSEST_PED(vPos, 0.5, TRUE, TRUE, hPed, FALSE, TRUE)//GET_RANDOM_PED_AT_COORD(vSeatPos, <<1,1,1>>)
		
			IF NOT IS_PED_INJURED(hPed)

				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_ACTIVE_IN_SCENARIO(hPed))
				
					IF bSetAsMissionEntity
						SET_ENTITY_AS_MISSION_ENTITY(hPed, TRUE, TRUE)
					ENDIF
					RETURN TRUE
				
				ENDIF
			ENDIF
		
		ENDIF

	ENDIF
	
	hPed = NULL
	RETURN FALSE

ENDFUNC

FUNC BOOL CLIFFORD_MG_GET_DEALER_PED_FROM_ORIGIN(CMG_TYPE eMgType, VECTOR vOriginPos, FLOAT fOriginRot, PED_INDEX& hPed, FLOAT fRadius = 0.5, BOOL bSetAsMissionEntity = FALSE)

	VECTOR	vOffsetPos
	FLOAT	fOffsetRot
	IF CLIFFORD_MG_GET_DEALER_POS(eMgType, NULL, vOffsetPos, fOffsetRot, TRUE)
		
		VECTOR vSeatPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vOriginPos, fOriginRot, vOffsetPos)
		
		// THIS HAS TO BE CALLED BEFORE CALLING THIS OR SCENARIO PEDS WON'T GET RETURNED
		SET_SCENARIO_PEDS_TO_BE_RETURNED_BY_NEXT_COMMAND(TRUE)
		IF GET_CLOSEST_PED(vSeatPos, fRadius, TRUE, TRUE, hPed, FALSE, TRUE)//GET_RANDOM_PED_AT_COORD(vSeatPos, <<1,1,1>>)
		
			IF NOT IS_PED_INJURED(hPed)
				
				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_USING_ANY_SCENARIO(hPed))
				
					IF bSetAsMissionEntity
						SET_ENTITY_AS_MISSION_ENTITY(hPed, TRUE, TRUE)
					ENDIF
					RETURN TRUE
					
				ENDIF
			
			ENDIF
		
		ENDIF

	ENDIF
	
	hPed = NULL
	RETURN FALSE

ENDFUNC

FUNC BOOL CLIFFORD_MG_IS_DEALER_PED(CMG_TYPE eMgType, OBJECT_INDEX hTable, PED_INDEX hPed)

	VECTOR	vPos
	FLOAT	fRot
	IF CLIFFORD_MG_GET_DEALER_POS(eMgType, hTable, vPos, fRot)
	
		IF NOT IS_PED_INJURED(hPed)

			IF IS_ENTITY_AT_COORD(hPed, <<0.5, 0.5, 0.5>>)
		
				SCRIPTTASKSTATUS eStatusA = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_PLAY_ANIM)
				SCRIPTTASKSTATUS eStatusB = GET_SCRIPT_TASK_STATUS(hPed, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				IF (eStatusA = PERFORMING_TASK) OR (eStatusA = WAITING_TO_START_TASK)
				OR (eStatusB = PERFORMING_TASK) OR (eStatusB = WAITING_TO_START_TASK)
				OR (IS_PED_ACTIVE_IN_SCENARIO(hPed))
				
					RETURN TRUE
				
				ENDIF
			
			ENDIF
		
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

