USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "finance_modifiers_public.sch"
USING "drunk_private.sch"

#IF IS_DEBUG_BUILD
USING "player_ped_public.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	drunk_public.sch
//		AUTHOR			:	Keith / Alwyn
//		DESCRIPTION		:	The public interface into the drunk control routines.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_FLOAT		MAX_DRUNK_CAMERA_STRENGTH	5.0

ENUM SMOKING_EFFECT
	SMOKING_EFFECT_STONED,
	#IF FEATURE_MUSIC_STUDIO
	SMOKING_EFFECT_SHORT_TRIP
	#ENDIF
ENDENUM

// -----------------------------------------------------------------------------------------
//		Public Drunk Request routines
// -----------------------------------------------------------------------------------------

/// PURPOSE:
///    
/// RETURNS:
///    
FUNC BOOL Should_Drunk_Controller_Be_Running()
	
	IF (g_drunkCameraActive)
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugDrunkInfo
			CDEBUG3LN(DEBUG_DRUNK, "<RUNCHECK-PASS> g_drunkCameraActive ")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF DOES_CAM_EXIST(g_drunkCameraIndex)
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugDrunkInfo
			CDEBUG3LN(DEBUG_DRUNK, "<RUNCHECK-PASS> DOES_CAM_EXIST(g_drunkCameraIndex) ")
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("drunk")) > 0
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugDrunkInfo
			CDEBUG3LN(DEBUG_DRUNK, "<RUNCHECK-PASS> number of drunk threads = ", GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("drunk")))
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF g_drunkRequestsPending > 0
		#IF IS_DEBUG_BUILD
		IF g_bDrawDebugDrunkInfo
			CDEBUG3LN(DEBUG_DRUNK, "<RUNCHECK-PASS> g_drunkRequestsPending = ", g_drunkRequestsPending)
		ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	

	#IF IS_DEBUG_BUILD
	IF g_bDrawDebugDrunkInfo
		CDEBUG3LN(DEBUG_DRUNK, "<RUNCHECK-FAIL> g_drunkRequestsPending = ", g_drunkRequestsPending)
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------
//		Public Drunk Request routines
// -----------------------------------------------------------------------------------------

// PURPOSE: Clear out one slot in the Drunk request array
//
// INPUT PARAMS:	paramArrayIndex		Index into the drunk requests array
PROC Clear_One_Drunk_Request(INT paramArrayIndex)
	
	IF (paramArrayindex < 0)
	OR (paramArrayIndex >= MAX_NUMBER_OF_DRUNK_REQUESTS)
		SCRIPT_ASSERT("Clear_One_Drunk_Request: array index out of bounds")
		EXIT
	ENDIF
	
	IF NOT (g_drunkRequests[paramArrayIndex].ped = NULL)
		IF (g_drunkRequests[paramArrayIndex].ped = PLAYER_PED_ID())
			g_playerRequestedToBeDrunk = FALSE
		ENDIF
	ENDIF

	g_drunkRequests[paramArrayIndex].status			= DS_NO_STATUS
	g_drunkRequests[paramArrayIndex].ped			= NULL
	g_drunkRequests[paramArrayIndex].ragdoll_msec	= DRUNK_LEVEL_NOT_DRUNK
	g_drunkRequests[paramArrayIndex].overall_msec	= DRUNK_LEVEL_NOT_DRUNK
	g_drunkRequests[paramArrayIndex].vehicle		= NULL
	
	
	DEBUG_PRINTCALLSTACK()
	
	g_drunkRequestsPending--
	
	IF g_drunkRequestsPending < 0
		g_drunkRequestsPending = 0
	ENDIF
	
ENDPROC


// PURPOSE: Clear out all slots in the Drunk request array
PROC Clear_All_Drunk_Requests()

	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_REQUESTS tempLoop
		Clear_One_Drunk_Request(tempLoop)
	ENDREPEAT

ENDPROC






// -----------------------------------------------------------------------------------------
//		Public Drunk Ped routines
// -----------------------------------------------------------------------------------------


/// PURPOSE:
///    Returns true if the timecycle modifier for the ped being 
///    either drunk or high is active.
FUNC BOOL Is_Drunk_Or_High_Cam_Effect_Active()
	RETURN g_drunkCameraActive
	AND NOT IS_STRING_NULL_OR_EMPTY(g_drunkCameraTimeCycleName)
ENDFUNC


// PURPOSE: Get the array index in the Drunk ped array that contains this Unique Drunk Ped ID
//
// INPUT PARAMS:	paramUniqueID		Unique Drunk Ped ID being searched for
// RETURN VALUE:	INT					Array Index associated with this Ped ID, or UNKNOWN_DRUNK_ARRAY_INDEX
FUNC INT Get_Drunk_Ped_Array_Index_For_This_Unique_Drunk_Ped_ID(INT paramUniqueID)
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS tempLoop
		IF (g_drunkPeds[tempLoop].uniqueID = paramUniqueID)
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE: Clear one slot in the Drunk Ped array
//
// INPUT PARAMS:	paramArrayIndex		Index into the drunk peds array
PROC Clear_One_Drunk_Ped(INT paramArrayIndex)
	
	IF (paramArrayindex < 0)
	OR (paramArrayIndex >= MAX_NUMBER_OF_DRUNK_PEDS)
		SCRIPT_ASSERT("Clear_One_Drunk_Ped: array index out of bounds")
		EXIT
	ENDIF

	g_drunkPeds[paramArrayIndex].uniqueID		= NO_UNIQUE_DRUNK_PED_ID
	g_drunkPeds[paramArrayIndex].myPedIndex		= NULL
	g_drunkPeds[paramArrayIndex].eDrunkLevel	= DL_NO_LEVEL
	g_drunkPeds[paramArrayIndex].iAlcoholHit	= 0
	g_drunkPeds[paramArrayIndex].iWeedHit		= 0
	
ENDPROC


// PURPOSE: Clear all slots in the Drunk Ped array
PROC Clear_All_Drunk_Peds()

	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS tempLoop
		Clear_One_Drunk_Ped(tempLoop)
	ENDREPEAT

ENDPROC


// NOTES:	The intention of this function is to tidy up after drunk peds where the 'drunk' script has terminated.
//			This function will not terminate the 'drunk' script since it assumes it has already terminated.
//
// PURPOSE: To clear all Drunk Ped details for this Unique Drunk Ped ID.
//			Details include the Drunk Ped details and any notices directly to or from this Drunk Ped ID.
//			(more... Alt-G)
//
// INPUT PARAMS:	paramUniqueID		Unique Drunk Ped ID of ped that is no longer drunk
PROC Clear_All_Drunk_Ped_Details_For_This_ID(INT paramUniqueID)

	Clear_All_Notices_With_This_Unique_Drunk_Ped_ID(paramUniqueID)
	
	INT drunkPedIndex = Get_Drunk_Ped_Array_Index_For_This_Unique_Drunk_Ped_ID(paramUniqueID)
	IF (drunkPedIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		EXIT
	ENDIF
	
	Clear_One_Drunk_Ped(drunkPedIndex)

ENDPROC


// PURPOSE: Clear out all variables
PROC Reset_All_Drunk_Variables_And_Terminate_All_Unneeded_Scripts()

	// Initial set up
	// ...clear out all the drunk requests
	Clear_All_Drunk_Requests()
	
	// ...clear out all the drunk peds
	Clear_All_Drunk_Peds()
	
	// ...clear out all the drunk notices
	Clear_All_Drunk_Notices()
	
	// ...reset drunk camera variables
	Reset_Drunk_Camera_Variables()
	
	// Reset Player Is Drunk variable
	g_playerIsDrunk = FALSE
	g_playerRequestedToBeDrunk = FALSE
	
	g_drunkRequestsPending = 0
	
	TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("drunk")
			
ENDPROC


// PURPOSE:	Forces all drunk peds to become sober
PROC Force_All_Drunk_Peds_To_Become_Sober()
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Force_All_Drunk_Peds_To_Become_Sober()")
	
	// Make all drunk peds sober?
	INT drunkPedArrayIndex	= 0
	INT uniqueDrunkPedID	= 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS drunkPedArrayIndex
		uniqueDrunkPedID = g_drunkPeds[drunkPedArrayIndex].uniqueID
		IF NOT (uniqueDrunkPedID = NO_UNIQUE_DRUNK_PED_ID)
			// ...found a drunk ped, so send a 'Become Sober' notice
			Post_Drunk_Notice(UNIQUE_ID_PUBLIC_FUNCTIONS, uniqueDrunkPedID, DNID_BECOME_SOBER)
		ENDIF
	ENDREPEAT
	
	// Ped isn't drunk, so check if it has been requested to be drunk
	INT drunkRequestArrayIndex = 0
	REPEAT MAX_NUMBER_OF_DRUNK_REQUESTS drunkRequestArrayIndex
		IF (g_drunkRequests[drunkRequestArrayIndex].status = DS_REQUEST_SCRIPT)
			// Clear this drunk request array index
			Clear_One_Drunk_Request(drunkRequestArrayIndex)
		ENDIF
	ENDREPEAT

ENDPROC





// -----------------------------------------------------------------------------------------
//		Public Unique Ped ID routines
// -----------------------------------------------------------------------------------------

// PURPOSE:	To get the already created Unique Drunk Ped ID for a ped
//
// INPUT PARAMS:	paramPed		Ped whose unique ID is being searched for
// RETURN VALUE:	INT				Unique ID if the ped is drunk, or NO_UNIQUE_DRUNK_PED_ID
FUNC INT Get_Peds_Unique_Drunk_Ped_ID(PED_INDEX paramPed)
	
	IF (paramPed = NULL)
		RETURN NO_UNIQUE_DRUNK_PED_ID
	ENDIF
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS tempLoop
		IF (paramPed = g_drunkPeds[tempLoop].myPedIndex)
			RETURN (g_drunkPeds[tempLoop].uniqueID)
		ENDIF
	ENDREPEAT
	
	RETURN NO_UNIQUE_DRUNK_PED_ID
	
ENDFUNC





// -----------------------------------------------------------------------------------------
//		Drunk Level Ped ID routines
// -----------------------------------------------------------------------------------------

// PURPOSE:	To get the already created Unique Drunk Ped ID for a ped
//
// INPUT PARAMS:	paramPed		Ped whose unique ID is being searched for
// RETURN VALUE:	INT				Unique ID if the ped is drunk, or NO_UNIQUE_DRUNK_PED_ID
FUNC g_eDrunkLevel Get_Peds_Drunk_Level(PED_INDEX paramPed)
	
	
	IF (paramPed = NULL)
		RETURN DL_NO_LEVEL
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramPed))
		RETURN DL_NO_LEVEL
	ENDIF
	
	// Ignore duplicate requests
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramPed)
	INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
	IF (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		RETURN DL_NO_LEVEL
	ENDIF
	
	RETURN g_drunkPeds[arrayIndex].eDrunkLevel
ENDFUNC

// PURPOSE:	To get the already created Unique Drunk Ped ID for a ped
//
// INPUT PARAMS:	paramPed		Ped whose unique ID is being searched for
// RETURN VALUE:	INT				Unique ID if the ped is drunk, or NO_UNIQUE_DRUNK_PED_ID
FUNC STRING Get_Drunk_Level_Moveclip(g_eDrunkLevel thisDrunkLevel)
	SWITCH thisDrunkLevel
		CASE DL_verydrunk
			RETURN "move_m@drunk@verydrunk"
		BREAK
		CASE DL_slightlydrunk
			RETURN "move_m@drunk@slightlydrunk"
		BREAK
		CASE DL_moderatedrunk
			RETURN "move_m@drunk@moderatedrunk"
		BREAK
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

// -----------------------------------------------------------------------------------------
//		Drunk Level Ped ID routines
// -----------------------------------------------------------------------------------------

FUNC INT Get_Peds_Drunk_Alcohol_Hit_Count(PED_INDEX paramPed)
	IF (paramPed = NULL)
		RETURN 0
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramPed))
		RETURN 0
	ENDIF
	
	// Ignore duplicate requests
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramPed)
	INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
	IF (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		RETURN ENUM_TO_INT(DL_NO_LEVEL)
	ENDIF
	
	RETURN g_drunkPeds[arrayIndex].iAlcoholHit
ENDFUNC

FUNC INT Get_Peds_Drunk_Weed_Hit_Count(PED_INDEX paramPed)
	IF (paramPed = NULL)
		RETURN 0
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramPed))
		RETURN 0
	ENDIF
	
	// Ignore duplicate requests
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramPed)
	INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
	IF (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		RETURN ENUM_TO_INT(DL_NO_LEVEL)
	ENDIF
	
	RETURN g_drunkPeds[arrayIndex].iWeedHit
ENDFUNC

// -----------------------------------------------------------------------------------------
//		Public Drunk control routines
// -----------------------------------------------------------------------------------------

// PURPOSE: Find the array index of the ped in the Drunk Ped array.
//			This doesn't check the drunk requests, only the existing drunks.
//
// INPUT PARAMS:	paramPed			The ped index of the ped being checked
// RETURN VALUE:	INT					The Drunk Ped array index, or UNKNOWN_DRUNK_ARRAY_INDEX
FUNC INT Get_Drunk_Ped_Array_Index_For_This_Ped_Index(PED_INDEX paramPed)
	
	IF (paramPed = NULL)
		RETURN UNKNOWN_DRUNK_ARRAY_INDEX
	ENDIF
	
	INT tempLoop = 0
	REPEAT MAX_NUMBER_OF_DRUNK_PEDS tempLoop
		IF NOT (g_drunkPeds[tempLoop].uniqueID = NO_UNIQUE_DRUNK_PED_ID)
			IF (paramPed = g_drunkPeds[tempLoop].myPedIndex)
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN UNKNOWN_DRUNK_ARRAY_INDEX

ENDFUNC


// PURPOSE: Checks the Drunk Ped array to see if a ped is drunk.
//			This doesn't check the drunk requests, only the existing drunks.
//
// INPUT PARAMS:	paramPed			The ped index of the ped being checked
// RETURN VALUE:	BOOL				TRUE if the ped is drunk, otherwise FALSE
FUNC BOOL Is_Ped_Drunk(PED_INDEX paramPed)
	
	IF (paramPed = NULL)
		RETURN FALSE
	ENDIF
	
	IF (Get_Drunk_Ped_Array_Index_For_This_Ped_Index(paramPed) = UNKNOWN_DRUNK_ARRAY_INDEX)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


// PURPOSE: Requests that the ped becomes drunk
//
// INPUT PARAMS:	paramPed			The ped index for the ped that is about to become drunk
//					paramRagdollMsec	Number of Msec the ped should ragdoll for
// RETURN VALUE:	BOOL				TRUE if the ped has successfully become drunk, otherwise FALSE
FUNC BOOL Make_Ped_Drunk(PED_INDEX paramPed, INT paramRagdoll_msec, BOOL ignoreParamTimeCheck = FALSE)
	
	IF (paramPed = NULL)
		RETURN FALSE
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramPed))
		RETURN FALSE
	ENDIF
	
	// Ignore duplicate requests
	// Check if the drunk ped is already drunk
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramPed)
	
	IF NOT (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
		// ...ped is already drunk
		RETURN TRUE
	ENDIF
	
	// Ped isn't already drunk, so check if the ped has been requested to be drunk
	INT arrayIndex = Get_Peds_Drunk_Request_Array_Index(paramPed)
	IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		// ...ped has already been requested to be drunk
		RETURN TRUE
	ENDIF
	
	
	// Ignore illegal drunk time parameters
	IF NOT ignoreParamTimeCheck
		IF (paramRagdoll_msec = DRUNK_LEVEL_NOT_DRUNK)
		OR (paramRagdoll_msec < 0)
			SCRIPT_ASSERT("Attempt to Make_Ped_Drunk but ragdoll msec requested is NOT_DRUNK or a negative value")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Request this ped to become drunk
	INT freeSlot = Get_An_Empty_Drunk_Request_Slot()
	IF (freeSlot = UNKNOWN_DRUNK_ARRAY_INDEX)
		CPRINTLN(DEBUG_DRUNK, "All drunk request slots are full - Need to increase MAX_NUMBER_OF_DRUNK_REQUESTS")
		RETURN FALSE
	ENDIF
	
	
	// Store the details
	// NOTE: Initially store the overall msec to be the same as the ragdolling msec - a separate command can change this
	g_drunkRequests[freeSlot].status		= DS_REQUEST_SCRIPT
	g_drunkRequests[freeSlot].ped			= paramPed
	g_drunkRequests[freeSlot].ragdoll_msec	= paramRagdoll_msec
	g_drunkRequests[freeSlot].overall_msec	= paramRagdoll_msec
	g_drunkRequests[freeSlot].vehicle		= NULL
	
	// Set the 'player requested to be drunk' flag
	IF (paramPed = PLAYER_PED_ID())
		g_playerRequestedToBeDrunk = TRUE
	ENDIF
	
	g_drunkRequestsPending++
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tlDrunkPedName = ""
	IF (paramPed = PLAYER_PED_ID())
		tlDrunkPedName = "player "
	ENDIF
	
	MODEL_NAMES paramPedModel = GET_ENTITY_MODEL(paramPed)
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(paramPed)
	IF ePed <> NO_CHARACTER
		tlDrunkPedName += GET_PLAYER_PED_STRING(ePed)
	ELIF (paramPedModel = MP_M_FREEMODE_01)
		tlDrunkPedName += "MP_M_FREEMODE_01"
	ELIF (paramPedModel = MP_F_FREEMODE_01)
		tlDrunkPedName += "MP_F_FREEMODE_01"
	ELSE
		tlDrunkPedName += GET_MODEL_NAME_FOR_DEBUG(paramPedModel)
	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Make_Ped_Drunk(", tlDrunkPedName, ", ", paramRagdoll_msec, ") //", g_drunkRequestsPending)
	#ENDIF
	
	RETURN TRUE
ENDFUNC


// PURPOSE: Requests that the ped becomes drunk
//
// INPUT PARAMS:	paramPed			The ped index for the ped that is about to become drunk
//					paramRagdollMsec	Number of Msec the ped should ragdoll for
// RETURN VALUE:	BOOL				TRUE if the ped has successfully become drunk, otherwise FALSE
FUNC BOOL Make_Ped_Drunk_Constant(PED_INDEX paramPed)
	RETURN Make_Ped_Drunk(paramPed, DRUNK_LEVEL_CONSTANT, TRUE)
ENDFUNC


// NOTES:	A notice gets posted for the drunk ped informing it to become sober. When read, the associated
//			drunk script will terminate. This will cause it to stop sending 'Still Drunk' messages to the
//			control script and the control script will tidy up any other details associated with that
//			drunk ped.
// 
// PURPOSE:	Makes a ped instantly sober - doesn't affect camera effects.
//			(more... Alt_G)
//
// INPUT PARAMS:	paramPed			The ped index of the ped that must become sober.
PROC Make_Ped_Sober(PED_INDEX paramPed)
	
	IF (paramPed = NULL)
		EXIT
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramPed))
		EXIT
	ENDIF
	
	// Is the ped drunk?
	INT drunkPedArrayIndex = Get_Drunk_Ped_Array_Index_For_This_Ped_Index(paramPed)
	IF NOT (drunkPedArrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		// ...the ped does exist and is drunk, so send a 'Become Sober' notice
		INT uniqueDrunkPedID = g_drunkPeds[drunkPedArrayIndex].uniqueID
		Post_Drunk_Notice(UNIQUE_ID_PUBLIC_FUNCTIONS, uniqueDrunkPedID, DNID_BECOME_SOBER)
		EXIT
	ENDIF
	
	
	// Ped isn't drunk, so check if it has been requested to be drunk
	INT drunkRequestArrayIndex = Get_Peds_Drunk_Request_Array_Index(paramPed)
	
	IF (drunkRequestArrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
		EXIT
	ENDIF
	
	// Clear this drunk request array index
	Clear_One_Drunk_Request(drunkRequestArrayIndex)
	
	#IF IS_DEBUG_BUILD
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(paramPed)
	IF ePed <> NO_CHARACTER
		CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Make_Ped_Sober(", GET_PLAYER_PED_STRING(ePed), ") //", g_drunkRequestsPending)
	ELSE
		CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Make_Ped_Sober(", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(paramPed)), ") //", g_drunkRequestsPending)
	ENDIF
	#ENDIF

ENDPROC

// PURPOSE:	Extend the drunk time for other drunk effects like steering bias (0 to cancel extension)
//			Doesn't apply to ragdolling
// 
// INPUT PARAMS:	paramDrunkPed		The Drunk Ped having time extended for other effects
//					paramTime_msec		The extended time in milliseconds
PROC Extend_Overall_Drunk_Time(PED_INDEX paramDrunkPed, INT paramTime_msec)

	IF (paramDrunkPed = NULL)
		EXIT
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramDrunkPed))
		EXIT
	ENDIF
	
	
	// Ignore illegal time parameters
	IF (paramTime_msec < 0)
		SCRIPT_ASSERT("Extend_Overall_Drunk_Time(): time msec requested is a negative value")
		EXIT
	ENDIF

	
	// Check if the drunk ped is already drunk
	INT drunkPedUniqueID	= Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
	INT arrayIndex			= UNKNOWN_DRUNK_ARRAY_INDEX
	
	IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
		// ...drunk ped isn't already drunk, so check if the ped has been requested to be drunk
		arrayIndex = Get_Peds_Drunk_Request_Array_Index(paramDrunkPed)
		IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
			// ...ped has been requested to be drunk, so add the extended time parameter
			g_drunkRequests[arrayIndex].overall_msec += paramTime_msec
			g_drunkRequests[arrayIndex].ragdoll_msec += paramTime_msec
		ENDIF
		
		EXIT
	ENDIF
	
	// Drunk Ped is already drunk, so update the overall time details
	Post_Drunk_Notice_With_Int(UNIQUE_ID_PUBLIC_FUNCTIONS, drunkPedUniqueID, DNID_EXTENDED_TIME, paramTime_msec)
	
ENDPROC


PROC Force_Cleanup_Drunk_Ped(PED_INDEX paramDrunkPed)
	RESET_PED_MOVEMENT_CLIPSET(paramDrunkPed)
	CLEAR_PED_ALTERNATE_MOVEMENT_ANIM(paramDrunkPed, AAT_IDLE)
	
	SET_PED_RESET_FLAG(paramDrunkPed, PRF_DisableActionMode, FALSE)
	SET_PED_CAN_PLAY_AMBIENT_ANIMS(paramDrunkPed, TRUE)
	SET_PED_FLEE_ATTRIBUTES(paramDrunkPed, FA_DISABLE_AMBIENT_CLIPS, FALSE)
	
	SET_PED_IS_DRUNK(paramDrunkPed, FALSE)
ENDPROC

// PURPOSE:	Activate the drunk camera for a set time
// 
// INPUT PARAMS:	paramTime_msec		The time in milliseconds that the drunk camera should be active for
//					paramTimeout		The time in milliseconds that the drunk camera should start returning to normal
//					paramStrength		The strength of the camera effect (between 0.0 to 1.0) [default = 0.3]
//					amplitudeScalar		The strength of the camera effect (between 0.0 to 1.0) [default = 0.3]
PROC Activate_Drunk_Camera(INT paramTime_msec, INT paramTimeout_msec = DRUNK_DEFAULT_TIMEOUT_msec,
		FLOAT paramStrength = 0.3, FLOAT amplitudeScalar = 1.0, CAMERA_INDEX scriptCamIndex = NULL,
		BOOL ignoreParamTimeCheck = FALSE)

	IF (g_bMagDemoActive)
		EXIT
	ENDIF
	
	IF (g_drunkCameraActive)
		EXIT
	ENDIF
	
	IF NOT ignoreParamTimeCheck
		IF (paramTime_msec < 0)
			SCRIPT_ASSERT("Activate_Drunk_Camera: Camera activated with a negative activation time")
			EXIT
		ENDIF
	ENDIF
	
	IF (paramStrength < 0.0)
	OR (paramStrength > MAX_DRUNK_CAMERA_STRENGTH)
		SCRIPT_ASSERT("Activate_Drunk_Camera: The camera shake strength is outside acceptable boundaries")
		EXIT
	ENDIF
	IF NOT IS_GAMEPLAY_CAM_SHAKING()
		SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", amplitudeScalar)
	ENDIF
	IF NOT IS_CINEMATIC_CAM_SHAKING()
		SHAKE_CINEMATIC_CAM("DRUNK_SHAKE", amplitudeScalar * g_drunkenCinematicCamMultiplier)
	ENDIF
	
	IF DOES_CAM_EXIST(scriptCamIndex)
		SHAKE_CAM(scriptCamIndex, "DRUNK_SHAKE", amplitudeScalar)
		g_drunkCameraIndex = scriptCamIndex
	ELSE
		g_drunkCameraIndex = NULL
	ENDIF
	g_drunkCameraActive = TRUE
	
	INT gameTimer = GET_GAME_TIMER()
	g_drunkCameraTimeout = gameTimer + paramTime_msec
	IF ignoreParamTimeCheck
		IF (paramTime_msec = DRUNK_LEVEL_CONSTANT)
			g_drunkCameraTimeout = DRUNK_LEVEL_CONSTANT
		ENDIF
	ENDIF
	
	g_drunkCameraTimeoutDuration = paramTimeout_msec
	g_drunkCameraMotionBlur = paramStrength
	g_drunkCameraDesiredAmplitudeScalar = amplitudeScalar
	g_drunkCameraActualAmplitudeScalar = amplitudeScalar
	
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Activate_Drunk_Camera(paramTime:", paramTime_msec, "ms, paramTimeout:", paramTimeout_msec, "ms, paramStrength:", paramStrength, ", amplitudeScalar:", amplitudeScalar, ", ignoreParamTimeCheck:", GET_STRING_FROM_BOOL(ignoreParamTimeCheck), ", scriptCamIndex:", NATIVE_TO_INT(scriptCamIndex), ")")
	#ENDIF
	
ENDPROC


// PURPOSE:	Activate the drunk camera indefinately
// 
// INPUT PARAMS:	paramTime_msec		The time in milliseconds that the drunk camera should be active for
//					paramStrength		The strength of the camera effect (between 0.0 to 1.0) [default = 0.3]
//					amplitudeScalar		The strength of the camera effect (between 0.0 to 1.0) [default = 0.3]
PROC Activate_Drunk_Camera_Constant(INT TimeoutDuration = DRUNK_DEFAULT_TIMEOUT_msec, FLOAT paramStrength = 0.3, FLOAT amplitudeScalar = 1.0, CAMERA_INDEX scriptCamIndex = NULL)
	Activate_Drunk_Camera(DRUNK_LEVEL_CONSTANT, TimeoutDuration, paramStrength, amplitudeScalar, scriptCamIndex, true)
	
ENDPROC


PROC Update_Drunk_Camera_ParamTime(INT paramTime_msec)

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Update_Drunk_Camera_ParamTime(", paramTime_msec, ")")
	#ENDIF
	
	INT gameTimer = GET_GAME_TIMER()
	g_drunkCameraTimeout = gameTimer + paramTime_msec
	
ENDPROC


PROC Update_Drunk_Camera_ParamStrength(FLOAT paramStrength)

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
	IF (paramStrength < 0.0)
	OR (paramStrength > MAX_DRUNK_CAMERA_STRENGTH)
		SCRIPT_ASSERT("Update_Drunk_Camera_ParamStrength: The camera shake strength is outside acceptable boundaries")
		EXIT
	ENDIF
	
	g_drunkCameraMotionBlur = paramStrength
	
ENDPROC


/// PURPOSE:
///    sets a desired amplitude for the drunk camera to try and reach
/// PARAMS:
///    amplitudeScalar - 
PROC Update_Drunk_Camera_ParamDesiredAmplitude(FLOAT amplitudeScalar)

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
	g_drunkCameraDesiredAmplitudeScalar = amplitudeScalar
	
ENDPROC

/// PURPOSE:
///    instantly updates the drunk camera amplitude
/// PARAMS:
///    amplitudeScalar - 
PROC Update_Drunk_Camera_ParamAmplitude(FLOAT amplitudeScalar)

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
	g_drunkCameraDesiredAmplitudeScalar = amplitudeScalar
	g_drunkCameraActualAmplitudeScalar = amplitudeScalar
	
ENDPROC


FUNC FLOAT Get_Drunk_Cam_Timeout_Multiplier()

	INT gameTimer = GET_GAME_TIMER()
	
	// timeout multiplier
	FLOAT fDrunkCamTimeoutMult = 1.0
	INT iDrunkCameraTimeoutRemaining = g_drunkCameraTimeout - gameTimer
	IF iDrunkCameraTimeoutRemaining <= g_drunkCameraTimeoutDuration
		IF (g_drunkCameraTimeout <> DRUNK_LEVEL_CONSTANT)
			fDrunkCamTimeoutMult = TO_FLOAT(iDrunkCameraTimeoutRemaining) / TO_FLOAT(g_drunkCameraTimeoutDuration)
		ENDIF
	ENDIF
	
	RETURN fDrunkCamTimeoutMult
ENDFUNC
FUNC FLOAT Get_Drunk_Cam_Hit_Count_Multiplier()
	FLOAT fDrunkCamHitMult = 0.1
	INT iPlayerDrunkAlcoholHitCount = Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID())
	INT iPlayerDrunkWeedHitCount = Get_Peds_Drunk_Weed_Hit_Count(PLAYER_PED_ID())
	
	IF (iPlayerDrunkWeedHitCount = 0)
		IF (iPlayerDrunkAlcoholHitCount = 0)
			fDrunkCamHitMult = 0.1
		ELSE
			fDrunkCamHitMult = TO_FLOAT(iPlayerDrunkAlcoholHitCount) / TO_FLOAT(10)
			
			IF (fDrunkCamHitMult > 1.0) fDrunkCamHitMult = 1.0 ENDIF
		ENDIF
	ELSE
		IF (iPlayerDrunkWeedHitCount = 0)
			fDrunkCamHitMult = 0.1
		ELSE
			fDrunkCamHitMult = TO_FLOAT(iPlayerDrunkWeedHitCount) / TO_FLOAT(5)
			
			IF (fDrunkCamHitMult > 1.0) fDrunkCamHitMult = 1.0 ENDIF
		ENDIF
	ENDIF
	
	RETURN fDrunkCamHitMult
ENDFUNC

PROC Request_Drunk_Audio_Scene(STRING SceneName)
	IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneScript)
		IF ARE_STRINGS_EQUAL(g_drunkAudioSceneName, SceneName)
		AND ARE_STRINGS_EQUAL(g_drunkAudioSceneScript, GET_THIS_SCRIPT_NAME())
			CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Request_Drunk_Audio_Scene(\"", SceneName, "\") with script request \"", g_drunkAudioSceneName, "\" already from ", g_drunkAudioSceneScript, ".sc")
			EXIT
		ENDIF
		
		CASSERTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Request_Drunk_Audio_Scene(\"", SceneName, "\") with an existing script request \"", g_drunkAudioSceneName, "\" from ", g_drunkAudioSceneScript, ".sc")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Request_Drunk_Audio_Scene(\"", SceneName, "\")")
	
	g_drunkAudioSceneName = SceneName
	g_drunkAudioSceneScript = GET_THIS_SCRIPT_NAME()
ENDPROC

// PURPOSE:	Causes the drunk camera effect to come to an immediate end
PROC Quit_Drunk_Camera_Immediately(BOOL bClearTimecycles = TRUE)
	CPRINTLN(DEBUG_DRUNK, "<", GET_THIS_SCRIPT_NAME(), "> Quit_Drunk_Camera_Immediately()")
	DEBUG_PRINTCALLSTACK()

//	IF NOT (g_drunkCameraActive)
//		EXIT
//	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<DRUNK CAM> Quit immediately called by \"", GET_THIS_SCRIPT_NAME(), ".sc\", bClearTimecycles: ", bClearTimecycles)
	
	SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(0.0)
	SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)
	
	SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(0.0)
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	
	SET_CINEMATIC_CAM_SHAKE_AMPLITUDE(0.0)
	STOP_CINEMATIC_CAM_SHAKING(TRUE)
	
	SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_NORMAL)
	
	IF IS_AUDIO_SCENE_ACTIVE("SAFEHOUSE_STONED_MICHAEL")
		STOP_AUDIO_SCENE("SAFEHOUSE_STONED_MICHAEL")
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneName)
		IF IS_AUDIO_SCENE_ACTIVE(g_drunkAudioSceneName)
			STOP_AUDIO_SCENE(g_drunkAudioSceneName)
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(g_drunkCameraIndex)
		IF IS_CAM_SHAKING(g_drunkCameraIndex)
			SET_CAM_SHAKE_AMPLITUDE(g_drunkCameraIndex, 0.0)
			STOP_CAM_SHAKING(g_drunkCameraIndex, TRUE)
		ENDIF
	ENDIF
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		CPRINTLN(DEBUG_DRUNK, "<DRUNK QUIT> someone started a global script cam shake, nuke it on killing drunk cam!!")
		STOP_SCRIPT_GLOBAL_SHAKING()
	ENDIF
	
	IF bClearTimecycles
	//	Dave W. DOn't want to do this if the GTAO passive mode cutscene is running
	//	Graeme - If you don't want to clear the timecycle modifier then call Quit_Drunk_Camera_Immediately_Without_Clearing_Time_Cycle() instead
		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
		OR GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() != -1
			CPRINTLN(DEBUG_DRUNK, "<DRUNK QUIT> CLEAR TIME MODIFIER (TCM: ", GET_TIMECYCLE_MODIFIER_INDEX(), ", TCTM: ", GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(), ")")
			CLEAR_TIMECYCLE_MODIFIER()
		ELIF IS_PLAYER_SWITCH_IN_PROGRESS()
			CPRINTLN(DEBUG_DRUNK, "<DRUNK QUIT> CLEAR TIME MODIFIER (TCM: ", GET_TIMECYCLE_MODIFIER_INDEX(), ", TCTM: ", GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(), ", switch in progress)")
			CLEAR_TIMECYCLE_MODIFIER()
		ELSE
			CPRINTLN(DEBUG_DRUNK, "<DRUNK QUIT> Don't clear time modifier (TCM: ", GET_TIMECYCLE_MODIFIER_INDEX(), ", TCTM: ", GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(), ")")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_DRUNK, "<DRUNK QUIT> bClearTimecycles set to false")
	ENDIF
	
	g_drunkCameraTimeCycleModifier = 0
	g_drunkCameraTimeCycleName = ""
	
	g_drunkAudioSceneName = ""
	g_drunkAudioSceneScript = ""
	
	Reset_Drunk_Camera_Variables()

ENDPROC

PROC Quit_Drunk_Camera_Immediately_Without_Clearing_Time_Cycle()
	Quit_Drunk_Camera_Immediately(FALSE)
ENDPROC


// PURPOSE:	Causes the drunk camera effect to gradually stop
// 
// INPUT PARAMS:	paramTime_msec			The time in milliseconds over which to quit drunk camera [default = 30000]
PROC Quit_Drunk_Camera_Gradually(INT paramTime_msec = DRUNK_DEFAULT_TIMEOUT_msec)

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
//	SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(0.0)
//	SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(0.0)
	
	// Update the drunk camera variables
	INT gameTimer = GET_GAME_TIMER()
	g_drunkCameraTimeout			= gameTimer + paramTime_msec
	g_drunkCameraTimeoutDuration	= paramTime_msec

ENDPROC


// PURPOSE:	Sobers up all drunk peds and quit drunk camera
PROC Force_All_Drunk_Peds_To_Become_Sober_And_Quit_Camera()

	// Make all peds sober
	Force_All_Drunk_Peds_To_Become_Sober()
	
	// Clear camera drunk effects
	Quit_Drunk_Camera_Immediately()

ENDPROC

FUNC STRING GET_SMOKING_EFFECT_STRING(SMOKING_EFFECT eSmokingEffect)
	SWITCH eSmokingEffect
		CASE SMOKING_EFFECT_STONED		RETURN "STONED"
		#IF FEATURE_MUSIC_STUDIO
		CASE SMOKING_EFFECT_SHORT_TRIP	RETURN "FixerShortTrip"
		#ENDIF
	ENDSWITCH
	
	RETURN "INVALID_SMOKING_EFFECT"
ENDFUNC

PROC Player_Takes_Alcohol_Hit(PED_INDEX paramDrunkPed, CAMERA_INDEX scriptCamIndex = NULL, INT iHitCount = 1)

	IF (paramDrunkPed = NULL)
		EXIT
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramDrunkPed))
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_PEYOTE)
		SET_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_DRANK_PEYOTE)
	ENDIF
	
	IF (Get_Peds_Drunk_Alcohol_Hit_Count(paramDrunkPed) + iHitCount) > 15
		iHitCount = (15 - Get_Peds_Drunk_Alcohol_Hit_Count(paramDrunkPed))
	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<ALCOHOL HIT> Called by \"", GET_THIS_SCRIPT_NAME(), ".sc\"")
	CPRINTLN(DEBUG_DRUNK, "<ALCOHOL HIT> Hit Count: ", iHitCount)
	
	CONST_INT	iCONST_ALCOHOL_HIT_TIMEOUT_msec		20000
	INT			ALCOHOL_HIT_TIMEOUT_msec			= iCONST_ALCOHOL_HIT_TIMEOUT_msec
	IF NETWORK_IS_GAME_IN_PROGRESS()
		ALCOHOL_HIT_TIMEOUT_msec *= 3
	ELSE
		IF ARE_STRINGS_EQUAL(GET_THIS_SCRIPT_NAME(), "ob_drinking_shots")
			ALCOHOL_HIT_TIMEOUT_msec *= 2
		ENDIF
	ENDIF
	
	IF NOT Is_Ped_Drunk(paramDrunkPed)
		Make_Ped_Drunk(paramDrunkPed, ALCOHOL_HIT_TIMEOUT_msec)
		Activate_Drunk_Camera(ALCOHOL_HIT_TIMEOUT_msec,
				ALCOHOL_HIT_TIMEOUT_msec,				//INT paramTimeout_msec = DRUNK_DEFAULT_TIMEOUT_msec,
				0.3,									//FLOAT paramStrength = 0.3,
				Get_Drunk_Cam_Hit_Count_Multiplier(),	//FLOAT amplitudeScalar = 1.0)
				scriptCamIndex)
		
		INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
		IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
			INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
			IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
				g_drunkPeds[arrayIndex].iAlcoholHit += iHitCount
//				SCRIPT_ASSERT("g_drunkPeds[arrayIndex].iAlcoholHit 111")
			ENDIF
		ENDIF
	ELSE
		
		INT drunkPedUniqueID	= Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
		INT arrayIndex			= UNKNOWN_DRUNK_ARRAY_INDEX
		
		IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
			arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
			IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
//				g_drunkRequests[arrayIndex].overall_msec += ALCOHOL_HIT_TIMEOUT_msec
//				g_drunkRequests[arrayIndex].ragdoll_msec += ALCOHOL_HIT_TIMEOUT_msec
				g_drunkPeds[arrayIndex].iAlcoholHit += iHitCount
//				SCRIPT_ASSERT("g_drunkPeds[arrayIndex].iAlcoholHit 222")
			ENDIF
		ENDIF
		
		IF g_drunkCameraTimeout = DRUNK_LEVEL_NOT_DRUNK
		OR g_drunkCameraTimeout <= GET_GAME_TIMER()
			Activate_Drunk_Camera(ALCOHOL_HIT_TIMEOUT_msec,
					ALCOHOL_HIT_TIMEOUT_msec,				//INT paramTimeout_msec = DRUNK_DEFAULT_TIMEOUT_msec,
					0.3,									//FLOAT paramStrength = 0.3,
					Get_Drunk_Cam_Hit_Count_Multiplier(),	//FLOAT amplitudeScalar = 1.0)
					scriptCamIndex)
		ELSE
			g_drunkCameraTimeout += ALCOHOL_HIT_TIMEOUT_msec
			Extend_Overall_Drunk_Time(paramDrunkPed, ALCOHOL_HIT_TIMEOUT_msec)
		ENDIF
	ENDIF
	
	IF Get_Peds_Drunk_Alcohol_Hit_Count(paramDrunkPed) >= 2
		g_drunkCameraTimeCycleModifier = 0.25
		IF IS_STRING_NULL_OR_EMPTY(g_drunkCameraTimeCycleName)
			g_drunkCameraTimeCycleName = "DRUNK"
		ENDIF
	ENDIF
	
//	// Check if the drunk ped is already drunk
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
	IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
		INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
		IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
//			g_drunkPeds[arrayIndex].iAlcoholHit++
//			
//			SCRIPT_ASSERT("g_drunkPeds[arrayIndex].iAlcoholHit 333")
		ENDIF
		
		EXIT
	ENDIF
	
	Post_Drunk_Notice_With_Ped_Index(UNIQUE_ID_PUBLIC_FUNCTIONS, drunkPedUniqueID, DNID_HIT_ALCOHOL, paramDrunkPed, iHitCount)
	
ENDPROC

PROC RESET_DRUNK_WEED_EFFECT_STRENGTH()
	g_drunkCameraEffectStrength = 1.0
	PRINTLN("[RESET_DRUNK_WEED_EFFECT_STRENGTH] - set to: ", g_drunkCameraEffectStrength)
ENDPROC

PROC SET_DRUNK_WEED_EFFECT_STRENGTH(FLOAT fStrength)
	PRINTLN("[SET_DRUNK_WEED_EFFECT_STRENGTH] - setting from: ", g_drunkCameraEffectStrength, " to: ", fStrength)
	g_drunkCameraEffectStrength = fStrength
ENDPROC

PROC Player_Takes_Weed_Hit(PED_INDEX paramDrunkPed, CAMERA_INDEX scriptCamIndex = NULL, INT iHitCount = 1, SMOKING_EFFECT eSmokingEffect = SMOKING_EFFECT_STONED,INT ioverridingduration = -1)

	IF (paramDrunkPed = NULL)
		EXIT
	ENDIF
	
	IF NOT (DOES_ENTITY_EXIST(paramDrunkPed))
		EXIT
	ENDIF
	
	IF (Get_Peds_Drunk_Weed_Hit_Count(paramDrunkPed) + iHitCount) > 10
		iHitCount = (10 - Get_Peds_Drunk_Weed_Hit_Count(paramDrunkPed))
	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<WEED HIT> Called by \"", GET_THIS_SCRIPT_NAME(), "\"")
	DEBUG_PRINTCALLSTACK()
	
	INT	iCONST_WEED_HIT_TIMEOUT_msec		= 20000
	IF ioverridingduration !=-1
		iCONST_WEED_HIT_TIMEOUT_msec = ioverridingduration
	ENDIF
	
	INT			WEED_HIT_TIMEOUT_msec				= iCONST_WEED_HIT_TIMEOUT_msec
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND ioverridingduration =-1
		WEED_HIT_TIMEOUT_msec *= 3
	ENDIF
	
	IF NOT Is_Ped_Drunk(paramDrunkPed)
		Make_Ped_Drunk(paramDrunkPed, Weed_HIT_TIMEOUT_msec)
		Activate_Drunk_Camera(Weed_HIT_TIMEOUT_msec,
				Weed_HIT_TIMEOUT_msec,					//INT paramTimeout_msec = DRUNK_DEFAULT_TIMEOUT_msec,
				0.3,									//FLOAT paramStrength = 0.3,
				Get_Drunk_Cam_Hit_Count_Multiplier(),	//FLOAT amplitudeScalar = 1.0)
				scriptCamIndex)
		
		INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
		IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
			INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
			IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
				g_drunkPeds[arrayIndex].iWeedHit += iHitCount
			ENDIF
		ENDIF
	ELSE
		
		INT drunkPedUniqueID	= Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
		INT arrayIndex			= UNKNOWN_DRUNK_ARRAY_INDEX
		
		IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
			arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
			IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
//				g_drunkRequests[arrayIndex].overall_msec += Weed_HIT_TIMEOUT_msec
//				g_drunkRequests[arrayIndex].ragdoll_msec += Weed_HIT_TIMEOUT_msec
				
				g_drunkPeds[arrayIndex].iWeedHit += iHitCount
			ENDIF
		ENDIF
		
		IF g_drunkCameraTimeout = DRUNK_LEVEL_NOT_DRUNK
		OR g_drunkCameraTimeout <= GET_GAME_TIMER()
			Activate_Drunk_Camera(Weed_HIT_TIMEOUT_msec,
					Weed_HIT_TIMEOUT_msec,					//INT paramTimeout_msec = DRUNK_DEFAULT_TIMEOUT_msec,
					0.3,									//FLOAT paramStrength = 0.3,
					Get_Drunk_Cam_Hit_Count_Multiplier(),	//FLOAT amplitudeScalar = 1.0)
					scriptCamIndex)
		ELSE
			g_drunkCameraTimeout += WEED_HIT_TIMEOUT_msec
			Extend_Overall_Drunk_Time(paramDrunkPed, WEED_HIT_TIMEOUT_msec)
		ENDIF
	ENDIF
	
	g_drunkCameraTimeCycleModifier = 0.25
	g_drunkCameraTimeCycleName = GET_SMOKING_EFFECT_STRING(eSmokingEffect)
	
	// Check if the drunk ped is already drunk
	INT drunkPedUniqueID = Get_Peds_Unique_Drunk_Ped_ID(paramDrunkPed)
	IF (drunkPedUniqueID = NO_UNIQUE_DRUNK_PED_ID)
		INT arrayIndex = Get_Peds_Drunk_Ped_Array_Index(drunkPedUniqueID)
		IF NOT (arrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
//			g_drunkPeds[arrayIndex].iWeedHit++
			
			//SCRIPT_ASSERT("g_drunkPeds[arrayIndex].iWeedHit 111")
		ENDIF
		
		EXIT
	ENDIF
	
	Post_Drunk_Notice_With_Ped_Index(UNIQUE_ID_PUBLIC_FUNCTIONS, drunkPedUniqueID, DNID_HIT_Weed, paramDrunkPed, iHitCount)
	
ENDPROC
// -----------------------------------------------------------------------------------------
//		Public Drunk modifier routines
// -----------------------------------------------------------------------------------------

// PURPOSE:	Increment the BAWSAQ drunk modifier stat for pubs and clubs visited
PROC BawsaqIncrementDrunkModifier_PUBCLUBVISIT()
	////BAWSAQ_INCREMENT_MODIFIER(BSMF_PUBCLUBVISIT)
ENDPROC

// PURPOSE:	Increment the BAWSAQ drunk modifier stat for times drunk
PROC BawsaqIncrementDrunkModifier_TIMESDRUNK()
	////BAWSAQ_INCREMENT_MODIFIER(BSMF_TIMESDRUNK)
ENDPROC

// PURPOSE:	Increment the BAWSAQ drunk modifier stat for friends taken to the pub
PROC BawsaqIncrementDrunkModifier_FRNDTOPUB()
	////BAWSAQ_INCREMENT_MODIFIER(BSMF_FRNDTOPUB)
ENDPROC

// PURPOSE:	Increment the BAWSAQ drunk modifier stat for number of drunk crimes
PROC BawsaqIncrementDrunkModifier_DRNKCRIME()
	////BAWSAQ_INCREMENT_MODIFIER(BSMF_DRNKCRIME)
ENDPROC

// PURPOSE:	Increment the BAWSAQ drunk modifier stat for number of hangovers (E2 - blackout and wake up elsewhere)
PROC BawsaqIncrementDrunkModifier_HANGOVERS()
	////BAWSAQ_INCREMENT_MODIFIER(BSMF_HANGOVERS)
ENDPROC
