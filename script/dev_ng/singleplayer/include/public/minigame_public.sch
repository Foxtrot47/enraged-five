//minigame_public.sch

USING "globals.sch"
USING "minigame_blip_support.sch"
USING "blip_enums.sch"
USING "flow_public_core.sch"

INT	g_iLoadedMiniGamesCount
STRUCTTIMER tmrMinigameLoadTimeout

CONST_INT MINIGAME_LOAD_RANGE 35
CONST_FLOAT MINIGAME_LOAD_TIMEOUT 8.0
#if USE_CLF_DLC
FUNC INT GET_NUM_MINIGAMES_IN_RANGECLF(VECTOR vPos)
	INT iRetVal, iIndex
	FLOAT fDist2
			
	iRetVal = 0
	
	PRINTLN("Checking offroad races in range")
	//check offroad
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_CLF_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_OFFROAD_RACES))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE5 ) TO ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE12 )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("offroad race is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking base jumps in range")
	//check basejumping
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_CLF_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_BASEJUMPING))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR ) TO ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("base jump is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking to see if stunt plane time trials in range")
	//check stunt planes
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_CLF_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES))
		iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR )
		fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
		
		IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
			PRINTLN("stunt plane time trials is in range")
			++iRetVal
		ENDIF
	ENDIF
	
	PRINTLN("we are reporting ", iRetVal, " mini games in range")
	RETURN iRetVal

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC INT GET_NUM_MINIGAMES_IN_RANGENRM(VECTOR vPos)
	INT iRetVal, iIndex
	FLOAT fDist2
			
	iRetVal = 0
	
	PRINTLN("Checking offroad races in range")
	//check offroad
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_NRM_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_OFFROAD_RACES))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE5 ) TO ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE12 )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("offroad race is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking base jumps in range")
	//check basejumping
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_NRM_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_BASEJUMPING))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR ) TO ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("base jump is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking to see if stunt plane time trials in range")
	//check stunt planes
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_NRM_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES))
		iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR )
		fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
		
		IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
			PRINTLN("stunt plane time trials is in range")
			++iRetVal
		ENDIF
	ENDIF
	
	PRINTLN("we are reporting ", iRetVal, " mini games in range")
	RETURN iRetVal

ENDFUNC
#endif
FUNC INT GET_NUM_MINIGAMES_IN_RANGE(VECTOR vPos)
#if USE_CLF_DLC
	return GET_NUM_MINIGAMES_IN_RANGECLF(vpos)
#endif
#if USE_NRM_DLC
	return GET_NUM_MINIGAMES_IN_RANGENRM(vpos)
#endif	
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	INT iRetVal, iIndex
	FLOAT fDist2
			
	iRetVal = 0
	
	PRINTLN("Checking offroad races in range")
	//check offroad
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_OFFROAD_RACES))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE5 ) TO ENUM_TO_INT( STATIC_BLIP_MINIGAME_OFFROAD_RACE12 )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("offroad race is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking base jumps in range")
	//check basejumping
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_BASEJUMPING))
		FOR iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR ) TO ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_ROCK_CLIFF )
			fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
			
			IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
				PRINTLN("base jump is in range")
				++iRetVal
			ENDIF
		ENDFOR
	ENDIF
	
	PRINTLN("Checking to see if stunt plane time trials in range")
	//check stunt planes
	IF GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STUNT_PLANES))
		iIndex = ENUM_TO_INT( STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR )
		fDist2 = VDIST2( GET_STATIC_BLIP_POSITION( INT_TO_ENUM( STATIC_BLIP_NAME_ENUM, iIndex )), vPos )
		
		IF fDist2 <= (MINIGAME_LOAD_RANGE * MINIGAME_LOAD_RANGE)
			PRINTLN("stunt plane time trials is in range")
			++iRetVal
		ENDIF
	ENDIF
	
	PRINTLN("we are reporting ", iRetVal, " mini games in range")
	RETURN iRetVal
#endif
#endif
ENDFUNC

FUNC BOOL ARE_MINIGAMES_IN_RANGE_LOADED( VECTOR vPos )
	INT iMiniGamesInRange = GET_NUM_MINIGAMES_IN_RANGE( vPos )
	
	// Reset the minigame launched data
	IF NOT IS_TIMER_STARTED( tmrMinigameLoadTimeout )
		g_iLoadedMiniGamesCount = 0
		START_TIMER_AT( tmrMinigameLoadTimeout, 0.0 )
	ENDIF
	
	IF iMiniGamesInRange = g_iLoadedMiniGamesCount
		OR GET_TIMER_IN_SECONDS( tmrMinigameLoadTimeout ) > MINIGAME_LOAD_TIMEOUT
		PRINTLN("iMiniGamesInRange is ", iMiniGamesInRange, " and g_iLoadedMiniGamesCount is ", g_iLoadedMiniGamesCount, " after about ", GET_TIMER_IN_SECONDS( tmrMinigameLoadTimeout), " seconds")
		CANCEL_TIMER( tmrMinigameLoadTimeout )
		g_iLoadedMiniGamesCount = 0
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC INCREMENT_MINIGAMES_LOADED_COUNT()
	PRINTLN("Incrementing g_iLoadedMiniGamesCount")
	g_iLoadedMiniGamesCount++
ENDPROC

//EOF

