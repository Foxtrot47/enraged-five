
USING "friends_public.sch"

///public interface for friend activity scripts
///    alwyn.roberts@rockstarnorth.com
///    

// *******************************************************************************************
//	FRIEND INITIALISATION PUBLIC ACCESSORS AND MUTATORS
// *******************************************************************************************

FUNC BOOL canStartActivity(enumActivityLocation eLoc)
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - activity not launched")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(FRIEND_A_PED_ID())
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - friend A ped injured")
		RETURN FALSE
	ENDIF

	IF NOT IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - friend A ped not in players group")
		RETURN FALSE
	ENDIF

	IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())
		IF NOT IS_PED_GROUP_MEMBER(FRIEND_B_PED_ID(), PLAYER_GROUP_ID())
//			CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - friend B ped exists but is not in players group")
			RETURN FALSE
		ENDIF
	ENDIF

	IF NOT IS_STATIC_BLIP_CURRENTLY_VISIBLE(g_ActivityLocations[eLoc].sprite)
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - activity blip not enabled")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL hasActivityStarted(enumActivityLocation eLoc)
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - activity not launched")
	ELSE
		IF eLoc <> NO_ACTIVITY_LOCATION
		AND eLoc = g_eCurrentActivityLoc
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL hasAnyActivityStarted()
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
//		CPRINTLN(DEBUG_FRIENDS, "canStartActivity(", GetLabel_enumActivityLocation(eLoc), ") - activity not launched")
	ELSE
		IF g_eCurrentActivityLoc < MAX_ACTIVITY_LOCATIONS
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    thisActivityLocation - 
PROC startActivity(enumActivityLocation thisActivityLocation, BOOL bPedsMustBeInGroup = TRUE)
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
			CPRINTLN(DEBUG_FRIENDS, "startActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - activity session not launched")

			SCRIPT_ASSERT("Cannot start a friend activity - activity session not launched")
		ELSE
			CPRINTLN(DEBUG_FRIENDS, "startActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ")")
		ENDIF
	#ENDIF
	
	IF IS_PED_INJURED(FRIEND_A_PED_ID())
		SCRIPT_ASSERT("Cannot start a friend activity - friend A is injured/doesn't exist")
		EXIT
	ENDIF
	IF bPedsMustBeInGroup
		IF NOT IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
			SCRIPT_ASSERT("Cannot start a friend activity - friend A isnt in players group")
			EXIT
		ENDIF
		IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())
			IF NOT IS_PED_GROUP_MEMBER(FRIEND_B_PED_ID(), PLAYER_GROUP_ID())
				SCRIPT_ASSERT("Cannot start a friend activity - friend B exists but is not in players group")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF (g_eCurrentActivityLoc = NO_ACTIVITY_LOCATION)
		g_ePreviousActivityResult	= NO_ACTIVITY_RESULT
		g_eCurrentActivityLoc		= thisActivityLocation
	
	ELSE
		SCRIPT_ASSERT("startActivity() - invalid thisActivityLocation")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    thisActivityLocation - 
PROC finishActivity(enumActivityLocation thisActivityLocation, enumActivityResult thisActivityResult)
	
	#IF IS_DEBUG_BUILD
		// Ensure in friend hangout mode
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
			CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - activity not launched??")
			
			SCRIPT_ASSERT("cannot finish a friend activity if activity not launched!")
		ELSE
			// Ensure friend A (if exists) has been returned to players group
			IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())
				IF NOT IS_PED_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
					SCRIPT_ASSERT("finishActivity() - Friend A not returned to players group at end of minigame")
				ENDIF
			ENDIF
			
			// Ensure friend B (if exists) has been returned to players group
			IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())
				IF NOT IS_PED_GROUP_MEMBER(FRIEND_B_PED_ID(), PLAYER_GROUP_ID())
					SCRIPT_ASSERT("finishActivity() - Friend B not returned to players group at end of minigame")
				ENDIF
			ENDIF

			// Report finish result
			SWITCH thisActivityResult
				CASE AR_playerWon
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player won!")
				BREAK
				CASE AR_playerDraw
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player draw!")
				BREAK
				CASE AR_buddyA_won
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player lost to friend A!")
				BREAK
				CASE AR_buddyB_won
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player lost to friend B!")
				BREAK
				CASE AR_playerQuit
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player quit!")
				BREAK
				
				CASE AR_buddyA_attacked
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - buddy A attacked")
				BREAK
				CASE AR_buddyB_attacked
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - buddy B attacked")
				BREAK
				CASE AR_buddyAll_attacked
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - buddy (all) attacked")
				BREAK
				CASE AR_buddy_injured
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - buddy injured??")
				BREAK
				CASE AR_deatharrest
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - player deatharrest??")
				BREAK
				
				DEFAULT
					CPRINTLN(DEBUG_FRIENDS, "finishActivity(", GetLabel_enumActivityLocation(thisActivityLocation), ") - returns DEFAULT? use a valid enumActivityResult please")
					SCRIPT_ASSERT("minigame script returns DEFAULT?")
				BREAK
				
			ENDSWITCH
			
		ENDIF
	#ENDIF
	
	IF (g_eCurrentActivityLoc = thisActivityLocation)
		g_ePreviousActivityLoc		= g_eCurrentActivityLoc
		g_ePreviousActivityResult	= thisActivityResult
		
		g_eCurrentActivityLoc		= NO_ACTIVITY_LOCATION
	ELSE
		SCRIPT_ASSERT("invalid thisActivityLocation in finishActivity()")
	ENDIF
	
ENDPROC
