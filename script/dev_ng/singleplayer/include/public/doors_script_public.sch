//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║				Author: Michael Wadelin				Date: 14/01/2014		 	║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║																				║
//║ 	A door controller framework to help with managing the opening and 		║
//║		closing of doors on mission.											║
//║																				║
//╚═════════════════════════════════════════════════════════════════════════════╝

USING "rage_builtins.sch"
USING "commands_script.sch"
USING "commands_object.sch"


CONST_FLOAT			MISSION_DOORS_KEEP_CURRENT_RATIO 		99.9


STRUCT MISSION_DOOR_STRUCT
	INT				iHash
	BOOL			bChangingState
	FLOAT			fDesiredOpenRatio
	FLOAT			fStartOpenRatio
	BOOL			bDesiredLockState
	INT				iInterpTime
	INT				iTimeStamp
	BOOL			bExistingDoor
ENDSTRUCT


//PURPOSE: Adds a new door to the doors array
PROC MISSION_DOORS_ADD_DOOR( MISSION_DOOR_STRUCT &sDoor, INT iHash, VECTOR vCoord, MODEL_NAMES eModel, BOOL bResetDoor = FALSE )

	sDoor.iHash	= iHash
	
	INT iExistingDoorHash
	
	IF sDoor.iHash != 0
		
		IF NOT DOOR_SYSTEM_FIND_EXISTING_DOOR( vCoord, eModel, iExistingDoorHash )
	
			IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM( sDoor.iHash )
				
				ADD_DOOR_TO_SYSTEM( sDoor.iHash, eModel, vCoord, FALSE )
			
				CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_ADD_DOOR() - new door added - hash: ", sDoor.iHash )
			
			ELSE
			
				CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_ADD_DOOR() - existing door found under the same name - hash: ", sDoor.iHash )
			
			ENDIF
		
		ELSE
			sDoor.iHash 			= iExistingDoorHash
			sDoor.bExistingDoor 	= TRUE
			CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_ADD_DOOR() - existing door found under another name - override hash: ", iExistingDoorHash )
		ENDIF
		
		IF bResetDoor
			DOOR_SYSTEM_SET_OPEN_RATIO( sDoor.iHash, 0.0, FALSE, TRUE )
			DOOR_SYSTEM_SET_DOOR_STATE( sDoor.iHash, DOORSTATE_FORCE_CLOSED_THIS_FRAME )
			DOOR_SYSTEM_SET_DOOR_STATE( sDoor.iHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME )
			CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_ADD_DOOR() - has been reset" )	
		ENDIF
	
	ELSE
		CPRINTLN( DEBUG_MIKE_UTIL, "[doors_script_public] MISSION_DOORS_ADD_DOOR() - invalid hash: ", sDoor.iHash )
	ENDIF	
	
ENDPROC


PROC MISSION_DOORS_CLEANUP_DOOR( MISSION_DOOR_STRUCT &sDoor )
	
	IF sDoor.iHash != 0
	
		IF NOT sDoor.bExistingDoor
		
			IF IS_DOOR_REGISTERED_WITH_SYSTEM( sDoor.iHash )
				
				REMOVE_DOOR_FROM_SYSTEM( sDoor.iHash )
			
				CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_CLEANUP_DOOR() - door removed - hash: ", sDoor.iHash )
			
			ELSE
				CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_CLEANUP_DOOR() - door not found in door system - hash: ", sDoor.iHash )
			ENDIF
		
		ELSE
			CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_CLEANUP_DOOR() - door not added by this script, do not remove - hash: ", sDoor.iHash )
		ENDIF
		
	ELSE
		CPRINTLN( DEBUG_MIKE, "[doors_script_public] MISSION_DOORS_CLEANUP_DOOR() - not a valid hash - hash: ", sDoor.iHash )
	ENDIF

	sDoor.iHash 			= 0
	sDoor.bChangingState 	= FALSE
	sDoor.fDesiredOpenRatio	= 0
	sDoor.fStartOpenRatio	= 0
	sDoor.bDesiredLockState	= FALSE
	sDoor.iTimeStamp		= 0
	sDoor.iInterpTime		= 0
	sDoor.bExistingDoor		= FALSE

ENDPROC


//PURPOSE: Changes the state of the door
PROC MISSION_DOORS_SET_DOOR_STATE( MISSION_DOOR_STRUCT &sDoor, BOOL lockState, FLOAT openRatio = MISSION_DOORS_KEEP_CURRENT_RATIO, INT iTime = 0 )
	
	IF sDoor.iHash != 0
		// No update over time, do NOW!
		IF iTime <= 0
			IF openRatio != MISSION_DOORS_KEEP_CURRENT_RATIO
				DOOR_SYSTEM_SET_OPEN_RATIO( sDoor.iHash, openRatio, FALSE, TRUE )
			ENDIF
			
			IF lockState
				DOOR_SYSTEM_SET_DOOR_STATE( sDoor.iHash, DOORSTATE_LOCKED, FALSE, TRUE )
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE( sDoor.iHash, DOORSTATE_UNLOCKED, FALSE, TRUE )
			ENDIF
			
			sDoor.iTimeStamp = 0
			sDoor.iInterpTime = 0
			
		// Needs updating over time, mark for frame by frame update
		ELIF openRatio != MISSION_DOORS_KEEP_CURRENT_RATIO 
			sDoor.bChangingState = TRUE
			sDoor.bDesiredLockState = lockState
			sDoor.fDesiredOpenRatio = openRatio
			
			sDoor.iTimeStamp = GET_GAME_TIMER()
			sDoor.iInterpTime = iTime
			
			sDoor.fStartOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO( sDoor.iHash )
		ELSE
			SCRIPT_ASSERT( "DOOR_SYSTEM: CANNOT OPEN DOOR OVER TIME TO KEEP THE SAME DOOR RATIO, POINTLESS!" )
		ENDIF
	ENDIF
	
ENDPROC


//PURPOSE: Obtains the state of the door
PROC MISSION_DOORS_GET_DOOR_STATE_AND_RATIO( MISSION_DOOR_STRUCT &sDoor, BOOL &bLockState, FLOAT &fOpenRatio )
	IF sDoor.iHash != 0
	
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO( sDoor.iHash )
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE( sDoor.iHash )
		
		IF eDoorState = DOORSTATE_LOCKED
		OR eDoorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
		OR eDoorState = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
			bLockState = TRUE
		ELSE
			bLockState = FALSE
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Manage locking and opening of doors
PROC MISSION_DOORS_UPDATE( MISSION_DOOR_STRUCT &sDoors[] )
	INT i
	
	REPEAT COUNT_OF( sDoors ) i
	
		IF sDoors[i].iHash != 0
		AND sDoors[i].bChangingState
		
			BOOL bLockState = FALSE
			FLOAT fOpenRatio = 0.0
			MISSION_DOORS_GET_DOOR_STATE_AND_RATIO( sDoors[i], bLockState, fOpenRatio)
	
			// If state does not match then update to new state
			IF bLockState != sDoors[i].bDesiredLockState
			OR fOpenRatio != sDoors[i].fDesiredOpenRatio

				IF sDoors[i].iInterpTime > 0
				AND sDoors[i].iTimeStamp > 0

					FLOAT fInterpProg = CLAMP( (GET_GAME_TIMER() - sDoors[i].iTimeStamp )/ TO_FLOAT( sDoors[i].iInterpTime ), 0.0, 1.0 )

					IF fInterpProg != 1.0
						
						// change in progress
						fOpenRatio = sDoors[i].fStartOpenRatio + ( fInterpProg * ( sDoors[i].fDesiredOpenRatio - sDoors[i].fStartOpenRatio ) )
						bLockState = TRUE
						
					ELSE
						
						// door finished
						bLockState = sDoors[i].bDesiredLockState
						fOpenRatio = sDoors[i].fDesiredOpenRatio
						sDoors[i].bChangingState = FALSE

						sDoors[i].iInterpTime		= 0
						sDoors[i].iTimeStamp 		= 0
						sDoors[i].bChangingState 	= FALSE
					ENDIF
				ELSE
//					SCRIPT_ASSERT("DOOR SYSTEM: DOOR NOT INTERP TIME SET!")
					// Set to the desired values as no time was present
					IF sDoors[i].bDesiredLockState
						bLockState = TRUE
					ELSE
						bLockState = FALSE
					ENDIF
					fOpenRatio = sDoors[i].fDesiredOpenRatio
					
					sDoors[i].iInterpTime		= 0
					sDoors[i].iTimeStamp 		= 0
					sDoors[i].bChangingState 	= FALSE
				ENDIF
				
				IF fOpenRatio != MISSION_DOORS_KEEP_CURRENT_RATIO
					DOOR_SYSTEM_SET_OPEN_RATIO( sDoors[i].iHash, fOpenRatio, FALSE, TRUE )
				ENDIF
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE( sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE )
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE( sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE )
				ENDIF
			ELSE
				
				DOOR_SYSTEM_SET_OPEN_RATIO( sDoors[i].iHash, fOpenRatio, FALSE, TRUE )
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE( sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE )
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE( sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE )
				ENDIF
			
				// door already in the desired stage
				sDoors[i].iInterpTime		= 0
				sDoors[i].iTimeStamp 		= 0
				sDoors[i].bChangingState 	= FALSE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
ENDPROC
