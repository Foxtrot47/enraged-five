
//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	player_scene_post_mission.sch								//
//		AUTHOR          :	Alwyn Roberts												//
//		DESCRIPTION		:	Contains the players timetable and procs to set up the		//
//							scenes for each slot in the timetable.						//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "player_scene_vehicle.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SCENE TIMETABLE                                                                                                                        	   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL GetPlaceholderCoordPostMission(PED_REQUEST_SCENE_ENUM eScene, VECTOR vCoordFrom, VECTOR vCoordTo, FLOAT fPercentDist,
		VECTOR &vLastKnownCoords, FLOAT &fLastKnownHead)
	VECTOR vCoordDiff = vCoordto - vCoordfrom
	
	fLastKnownHead = GET_HEADING_FROM_VECTOR_2D(-vCoorddiff.x, -vCoorddiff.y) + GET_RANDOM_FLOAT_IN_RANGE(5, 5)
	
	fPercentDist *= GET_RANDOM_FLOAT_IN_RANGE(0.95, 1.05)
	
	vLastKnownCoords = vCoordto - (vCoorddiff * fPercentDist)
	vLastKnownCoords += <<GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1), 0>>
	
//	#IF IS_DEBUG_BUILD
//	OPEN_DEBUG_FILE()
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("		CASE ")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_Ped_Request_Scene_Enum(eScene))SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("			vLastKnownCoords = ")SAVE_VECTOR_TO_DEBUG_FILE(vLastKnownCoords)SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("			fLastKnownHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fLastKnownHead)SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("			RETURN TRUE	BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_STRING_TO_DEBUG_FILE("		BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
//	SAVE_NEWLINE_TO_DEBUG_FILE()
//	CLOSE_DEBUG_FILE()
//	#ENDIF
	
	eScene = eScene
	RETURN TRUE
ENDFUNC

FUNC BOOL GetLastKnownPedInfoPostMission(PED_REQUEST_SCENE_ENUM eScene, VECTOR &vLastKnownCoords, FLOAT &fLastKnownHead)
	SWITCH eScene
		
		CASE PR_SCENE_Fa_STRIPCLUB_ARM3	vLastKnownCoords = <<115.1569, -1286.6840, 28.2613>>		fLastKnownHead = 111.0000	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_PHONECALL_ARM3	vLastKnownCoords = <<-90.0089, -1324.1947, 28.3203>>		fLastKnownHead = 194.1887	RETURN TRUE	BREAK
//		CASE PR_SCENE_Ma_ARM3			vLastKnownCoords = <<-69.9962, -1107.7679, 25.0764>>		fLastKnownHead = -20.8093	RETURN TRUE	BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM1	RETURN GetLastKnownPedInfoPostMission(PR_SCENE_Fa_STRIPCLUB_ARM3, vLastKnownCoords, fLastKnownHead)	BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM1	RETURN GetLastKnownPedInfoPostMission(PR_SCENE_Fa_PHONECALL_ARM3, vLastKnownCoords, fLastKnownHead)	BREAK
//		CASE PR_SCENE_Fa_STRIPCLUB_FAM3	RETURN GetLastKnownPedInfoPostMission(PR_SCENE_Fa_STRIPCLUB_ARM3, vLastKnownCoords, fLastKnownHead)	BREAK
		CASE PR_SCENE_Fa_PHONECALL_FAM3	RETURN GetLastKnownPedInfoPostMission(PR_SCENE_Fa_PHONECALL_ARM3, vLastKnownCoords, fLastKnownHead)	BREAK
		CASE PR_SCENE_Fa_FAMILY3		vLastKnownCoords = <<-807.2979, -48.4004, 36.8173>>			fLastKnownHead = 201.6328	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_FBI1			vLastKnownCoords = <<1432.3402, -1887.3832, 70.5768>>		fLastKnownHead = 350.0509	RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FBI2
			vLastKnownCoords = <<1666.2040, 1967.2504, 143.3213>>
			fLastKnownHead = 0.7896
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Ma_FAMILY1		vLastKnownCoords = <<-1440.2200, -127.0200, 50.0000>>		fLastKnownHead = 42.0000	RETURN TRUE BREAK	//1373502
		CASE PR_SCENE_Fa_FBI4intro		vLastKnownCoords = <<135.0550, -1759.6396, 27.8957>>		fLastKnownHead = -129.000	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FBI4intro		vLastKnownCoords = <<687.6992, -1744.0299, 28.3624>>		fLastKnownHead = 267.1409	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FBI3								//FIB 3 - Michael in car near FIB building as if dropped off Dave.
			vLastKnownCoords = <<56.5117, -744.6122, 43.1356>>
			fLastKnownHead = 340.0526
			RETURN TRUE
		BREAK
		CASE PR_SCENE_Fa_FBI4								//FIB 4 - Franklin on way home from burning vehicle location.
			vLastKnownCoords = <<506.4850, -1884.9670, 24.7640>>
			fLastKnownHead = 22.9566
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Fa_FBI5			vLastKnownCoords = << 1555.9575, 953.6136, 77.2063 >>		fLastKnownHead = 152.8118	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FBI5			vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		
		CASE PR_SCENE_Ta_FAMILY4		vLastKnownCoords =  <<220.7200, -64.4177, 68.2922>>			fLastKnownHead = 250.4535-360 RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_FINALEC		vLastKnownCoords =  << 2048.0701, 3840.8401, 34.2238>>		fLastKnownHead = 119.6030	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_AGENCY1		vLastKnownCoords = <<-464.2200, -1592.9800, 38.7300>>		fLastKnownHead = 168.0000 	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_AGENCYprep1
			vLastKnownCoords =  <<744.7900+0.0186, -465.8600-0.0114, 36.6399>>			
			fLastKnownHead =  51.7279
			RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_AGENCY3B		vLastKnownCoords = <<-9.0000, 508.1000, 173.6278>>			fLastKnownHead = 151.2504	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_CARSTEAL1		vLastKnownCoords = <<72.2278, -1464.6798, 28.2915>>			fLastKnownHead = 156.8827	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_AGENCY2		vLastKnownCoords = <<763.0000, -906.0000, 24.2312>>			fLastKnownHead = 7.2736		RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_CARSTEAL1		vLastKnownCoords = <<257.9167, -1120.7855, 28.3684>>		fLastKnownHead = 97.2736	RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_CARSTEAL2		vLastKnownCoords = << 422.5858, -978.6332, 69.7073 >>		fLastKnownHead = 4			RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_FBI2			vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FBI4			vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_DOCKS2B		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FAMILY6		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FINALEprepD	vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ma_FAMILY6		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_MARTIN1		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ma_TREVOR3		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_TREVOR3		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ma_FRANKLIN2		vLastKnownCoords = << 294.8521, 882.9366, 197.8527 >>		fLastKnownHead = 162.6930	RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_FRANKLIN2		vLastKnownCoords = <<-1771.8015, 794.4316, 138.4211>>		fLastKnownHead = 128.9946	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FBI1end		vLastKnownCoords = <<1495.5953, -1848.8207, 70.2075>>		fLastKnownHead = 32.2721	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_RURAL2A		vLastKnownCoords = <<2897.5544, 4032.2410, 50.1419>>		fLastKnownHead = 192.8091	RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_RURAL2A		vLastKnownCoords = << 1973.355, 3818.204, 32.005 >>			fLastKnownHead = 32.000		RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_RC_MRSP2		vLastKnownCoords = << 1973.355, 3818.204, 32.005 >>			fLastKnownHead = 32.000		RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_RURAL1			vLastKnownCoords = <<1397.0000, 3725.8000, 33.0673>>		fLastKnownHead = -3.7534	RETURN TRUE	BREAK
		
		CASE PR_SCENE_FTa_FRANKLIN1a
			vLastKnownCoords = <<798.4536, -2975.3408, 4.0205>>+<<0,0,1>>
			fLastKnownHead = 90.0000
			RETURN TRUE
		BREAK
//		CASE PR_SCENE_FTa_FRANKLIN1b
//			vLastKnownCoords = <<733.2842, -3017.2427, 8.3068>>
//			fLastKnownHead = 114.8398
//			RETURN TRUE
//		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1c
			vLastKnownCoords = <<709.0244, -2916.4788, 5.0589>>
			fLastKnownHead = 355.3260
			RETURN TRUE
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1d
			vLastKnownCoords = <<643.5248, -2917.3250, 5.1337>>
			fLastKnownHead = 334.1068
			RETURN TRUE
		BREAK
		CASE PR_SCENE_FTa_FRANKLIN1e
			vLastKnownCoords = <<595.2742, -2819.1826, 5.0559>>
			fLastKnownHead = 46.8853
			RETURN TRUE
		BREAK
		
		CASE PR_SCENE_Ma_EXILE2			vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_EXILE2			vLastKnownCoords = <<314.4171, 965.2070, 208.4024>>		fLastKnownHead = 165.9421	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_EXILE3
			vLastKnownCoords = <<3321.5369, 4975.4546, 25.9097>>
			fLastKnownHead = 221.2280
			RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_EXILE3			vLastKnownCoords = <<-111.1318, 6316.4790, 30.4904>>	fLastKnownHead = 42.00+180	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_MICHAEL3		vLastKnownCoords = <<-731.3261, 106.6800, 54.7169>>		fLastKnownHead = 98.9764	RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_MICHAEL3		vLastKnownCoords = <<-1257.5000, -526.9999, 30.2361>>	fLastKnownHead = 220.9554	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_DOCKS2A		vLastKnownCoords = <<736.9869, -2050.6780, 28.2718>>	fLastKnownHead = 83.9922	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_DOCKS2A		vLastKnownCoords = <<262.5499, -2540.1504, 4.8433>>		fLastKnownHead = -64.1366	RETURN TRUE BREAK
		CASE PR_SCENE_Fa_FINALE1		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_CARSTEAL4		vLastKnownCoords = <<-315.7789, 6201.3550, 30.4322>>	fLastKnownHead = 127.7547	RETURN TRUE BREAK
		CASE PR_SCENE_Fa_FINALE2intro	vLastKnownCoords = <<118.0988, -1264.9160, 32.3637>>	fLastKnownHead = -63.0000	RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FINALE2intro	vLastKnownCoords = <<37.5988, -1351.5203, 28.2954>>		fLastKnownHead = 90.0339	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_DOCKS2B		vLastKnownCoords = <<-558.2693, 261.1167, 82.0700>>		fLastKnownHead = 84.6231	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FINALE1		vLastKnownCoords = <<-196.9999, 507.9999, 132.4770>>	fLastKnownHead = 99.6049	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_AGENCY3A		vLastKnownCoords = <<1312.0100, -1645.8700, 51.2000>>	fLastKnownHead = 120.0000	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FINALE2A		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FINALE2A		vLastKnownCoords = << -818.7374, 6.4824, 41.2432 >>		fLastKnownHead = 211.8223	RETURN TRUE	BREAK
		CASE PR_SCENE_Ma_FINALE2B		vLastKnownCoords = <<2091.2583, 4714.8521, 40.1936>>	fLastKnownHead = 136.0867	RETURN TRUE	BREAK
		CASE PR_SCENE_Ta_FINALE1		vLastKnownCoords = <<1762.5900, 3247.2119, 40.7350>>	fLastKnownHead = 27.0648	RETURN TRUE BREAK
		CASE PR_SCENE_Ta_FINALE2B		vLastKnownCoords = <<1764.0129, 3252.9021, 40.7350>>	fLastKnownHead = 27.0648	RETURN TRUE	BREAK
		CASE PR_SCENE_Fa_FINALEA		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_FINALEB		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Ma_FINALEC		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		CASE PR_SCENE_Fa_FINALEC		vLastKnownCoords = <<0,0,0>> fLastKnownHead = 0 RETURN TRUE BREAK
		
		DEFAULT
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
#if USE_CLF_DLC
FUNC BOOL UpdatePostMissionInfoclf(PED_INFO_STRUCT &sInfo,enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eScene)
	
	g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed]	= eScene
	
	sInfo.sLastTimeActive[ePed]									= GET_CURRENT_TIMEOFDAY()
	
	IF NOT GetLastKnownPedInfoPostMission(eScene, sInfo.vLastKnownCoords[ePed], sInfo.fLastKnownHead[ePed])
		SCRIPT_ASSERT("a post mission switch is missing from GetLastKnownPedInfoPostMission()")
		RETURN FALSE
	ENDIF
	
	IF NOT ARE_VECTORS_EQUAL(sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
		IF NOT ARE_VECTORS_EQUAL(g_sPedSceneData[eScene].vCreateCoords, <<0,0,0>>)
			PED_VEH_DATA_STRUCT sVehData
			VECTOR vVehCoordsOffset
			FLOAT fVehHeadOffset
			VECTOR vDriveOffset
			FLOAT fDriveSpeed
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, eScene,
					sVehData, vVehCoordsOffset, fVehHeadOffset,
					vDriveOffset, fDriveSpeed)
					
				#IF IS_DEBUG_BUILD
				PRINTSTRING("update g_sPlayerLastVeh[")
				PRINTINT(ENUM_TO_INT(ePed))
				PRINTSTRING("] = ")
				IF (sVehData.model <> DUMMY_MODEL_FOR_SCRIPT)
					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(sVehData.model))
				ELSE
					PRINTSTRING("NULL")
				ENDIF
				PRINTNL()
				#ENDIF
				
				g_sPlayerLastVeh[ePed] = sVehData
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTSTRING("ignore g_sPlayerLastVeh[")
			PRINTINT(ENUM_TO_INT(ePed))
			PRINTSTRING("] no veh")
			PRINTNL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTSTRING("ignore g_sPlayerLastVeh[")
		PRINTINT(ENUM_TO_INT(ePed))
		PRINTSTRING("] null vector")
		PRINTNL()
		#ENDIF		
	ENDIF
	sInfo.iLastKnownRoomKey[ePed]								= 0
	sInfo.vLastKnownVelocity[ePed]								= <<0,0,0>>
	sInfo.iLastKnownWantedLevel[ePed]							= 0
	
	RETURN TRUE
ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL UpdatePostMissionInfoNRM(PED_INFO_STRUCT &sInfo,enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eScene)
	
	g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed]	= eScene
	
	sInfo.sLastTimeActive[ePed]									= GET_CURRENT_TIMEOFDAY()
	
	IF NOT GetLastKnownPedInfoPostMission(eScene, sInfo.vLastKnownCoords[ePed], sInfo.fLastKnownHead[ePed])
		SCRIPT_ASSERT("a post mission switch is missing from GetLastKnownPedInfoPostMission()")
		RETURN FALSE
	ENDIF
	
	//
	IF NOT ARE_VECTORS_EQUAL(sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
		IF NOT ARE_VECTORS_EQUAL(g_sPedSceneData[eScene].vCreateCoords, <<0,0,0>>)
			PED_VEH_DATA_STRUCT sVehData
			VECTOR vVehCoordsOffset
			FLOAT fVehHeadOffset
			VECTOR vDriveOffset
			FLOAT fDriveSpeed
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, eScene,
					sVehData, vVehCoordsOffset, fVehHeadOffset,
					vDriveOffset, fDriveSpeed)
					
				#IF IS_DEBUG_BUILD
				PRINTSTRING("update g_sPlayerLastVeh[")
				PRINTINT(ENUM_TO_INT(ePed))
				PRINTSTRING("] = ")
				IF (sVehData.model <> DUMMY_MODEL_FOR_SCRIPT)
					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(sVehData.model))
				ELSE
					PRINTSTRING("NULL")
				ENDIF
				PRINTNL()
				#ENDIF
				
				g_sPlayerLastVeh[ePed] = sVehData
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTSTRING("ignore g_sPlayerLastVeh[")
			PRINTINT(ENUM_TO_INT(ePed))
			PRINTSTRING("] no veh")
			PRINTNL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTSTRING("ignore g_sPlayerLastVeh[")
		PRINTINT(ENUM_TO_INT(ePed))
		PRINTSTRING("] null vector")
		PRINTNL()
		#ENDIF
		
	ENDIF
	//
	
	sInfo.iLastKnownRoomKey[ePed]								= 0
	sInfo.vLastKnownVelocity[ePed]								= <<0,0,0>>
	sInfo.iLastKnownWantedLevel[ePed]							= 0
	
	RETURN TRUE
ENDFUNC
#endif

FUNC BOOL UpdatePostMissionInfo(PED_INFO_STRUCT &sInfo,	enumCharacterList ePed, PED_REQUEST_SCENE_ENUM eScene)
#if USE_CLF_DLC
	return UpdatePostMissionInfoclf(sInfo,ePed,eScene)
#endif
#if USE_NRM_DLC
	return UpdatePostMissionInfoNRM(sInfo,ePed,eScene)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	// dont update a post-mission switch if the post-arm 3 stripclub scene hasnt been seen
	IF NOT (IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_MINIGAME_ACTIVE],
			ENUM_TO_INT(MINIGAME_STRIPCLUB)))
		
		PED_REQUEST_SCENE_ENUM eOldScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
		IF (eOldScene = PR_SCENE_Fa_STRIPCLUB_ARM3)
//		OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM1)
//		OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM3)
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("don't UpdatePostMissionInfo(")
			PRINTINT(ENUM_TO_INT(eScene))
			PRINTSTRING(")	//")
			PRINTINT(ENUM_TO_INT(eOldScene))
			PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
		IF (eOldScene = PR_SCENE_Fa_PHONECALL_ARM3)
		OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM1)
		OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM3)
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("don't UpdatePostMissionInfo(")
			PRINTINT(ENUM_TO_INT(eScene))
			PRINTSTRING(")	//")
			PRINTINT(ENUM_TO_INT(eOldScene))
			PRINTNL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]	= eScene
	
	sInfo.sLastTimeActive[ePed]									= GET_CURRENT_TIMEOFDAY()
	
	IF NOT GetLastKnownPedInfoPostMission(eScene, sInfo.vLastKnownCoords[ePed], sInfo.fLastKnownHead[ePed])
		SCRIPT_ASSERT("a post mission switch is missing from GetLastKnownPedInfoPostMission()")
		RETURN FALSE
	ENDIF
	
	//
	IF NOT ARE_VECTORS_EQUAL(sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
		IF NOT ARE_VECTORS_EQUAL(g_sPedSceneData[eScene].vCreateCoords, <<0,0,0>>)
			PED_VEH_DATA_STRUCT sVehData
			VECTOR vVehCoordsOffset
			FLOAT fVehHeadOffset
			VECTOR vDriveOffset
			FLOAT fDriveSpeed
			IF GET_PLAYER_VEH_POSITION_FOR_SCENE(ePed, eScene,
					sVehData, vVehCoordsOffset, fVehHeadOffset,
					vDriveOffset, fDriveSpeed)
					
				#IF IS_DEBUG_BUILD
				PRINTSTRING("update g_sPlayerLastVeh[")
				PRINTINT(ENUM_TO_INT(ePed))
				PRINTSTRING("] = ")
				IF (sVehData.model <> DUMMY_MODEL_FOR_SCRIPT)
					PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(sVehData.model))
				ELSE
					PRINTSTRING("NULL")
				ENDIF
				PRINTNL()
				#ENDIF
				
				g_sPlayerLastVeh[ePed] = sVehData
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTSTRING("ignore g_sPlayerLastVeh[")
			PRINTINT(ENUM_TO_INT(ePed))
			PRINTSTRING("] no veh")
			PRINTNL()
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTSTRING("ignore g_sPlayerLastVeh[")
		PRINTINT(ENUM_TO_INT(ePed))
		PRINTSTRING("] null vector")
		PRINTNL()
		#ENDIF
		
	ENDIF
	//
	
	sInfo.iLastKnownRoomKey[ePed]								= 0
	sInfo.vLastKnownVelocity[ePed]								= <<0,0,0>>
	sInfo.iLastKnownWantedLevel[ePed]							= 0
	
	RETURN TRUE
#endif
#endif
ENDFUNC

#if USE_CLF_DLC
PROC UpdateLastKnownPedInfoPostMissionclf(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)
	SWITCH eMissionID
		
		CASE SP_MISSION_CLF_TRAIN		
			UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_T_DEFAULT) 
		BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			IF eMissionID != SP_MISSION_NONE
				INT iFriendCharBitset, BIT_CURRENT
				iFriendCharBitset = g_sMissionStaticData[eMissionID].friendCharBitset
				SWITCH g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed
					CASE CHAR_MICHAEL
						BIT_CURRENT = BIT_MICHAEL
					BREAK
					CASE CHAR_FRANKLIN
						BIT_CURRENT = BIT_FRANKLIN
					BREAK
					CASE CHAR_TREVOR
						BIT_CURRENT = BIT_TREVOR
					BREAK
				ENDSWITCH
				
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_MICHAEL)
					IF (BIT_CURRENT <> BIT_MICHAEL)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_MICHAEL")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_FRANKLIN)
					IF (BIT_CURRENT <> BIT_FRANKLIN)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_FRANKLIN")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_TREVOR)
					IF (BIT_CURRENT <> BIT_TREVOR)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_TREVOR")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC UpdateLastKnownPedInfoPostMissionNRM(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)
	SWITCH eMissionID
		
		CASE SP_MISSION_NRM_SUR_START
			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_M_DEFAULT) 
		BREAK
		CASE SP_MISSION_NRM_SUR_AMANDA	
		CASE SP_MISSION_NRM_SUR_TRACEY	
		CASE SP_MISSION_NRM_SUR_MICHAEL		
		CASE SP_MISSION_NRM_SUR_HOME	
		CASE SP_MISSION_NRM_SUR_JIMMY	
		CASE SP_MISSION_NRM_SUR_PARTY	
		CASE SP_MISSION_NRM_SUR_CURE	
		CASE SP_MISSION_NRM_RESCUE_ENG
		CASE SP_MISSION_NRM_RESCUE_MED
		CASE SP_MISSION_NRM_RESCUE_GUN
		CASE SP_MISSION_NRM_SUP_FUEL
		CASE SP_MISSION_NRM_SUP_AMMO
		CASE SP_MISSION_NRM_SUP_MEDS
		CASE SP_MISSION_NRM_SUP_FOOD
		CASE SP_MISSION_NRM_RADIO_A
		CASE SP_MISSION_NRM_RADIO_B
		CASE SP_MISSION_NRM_RADIO_C
			//do noithing
		BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			IF eMissionID != SP_MISSION_NONE
				INT iFriendCharBitset, BIT_CURRENT
				iFriendCharBitset = g_sMissionStaticData[eMissionID].friendCharBitset
				SWITCH g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed
					CASE CHAR_MICHAEL
						BIT_CURRENT = BIT_MICHAEL
					BREAK
					CASE CHAR_FRANKLIN
						BIT_CURRENT = BIT_FRANKLIN
					BREAK
					CASE CHAR_TREVOR
						BIT_CURRENT = BIT_TREVOR
					BREAK
				ENDSWITCH
				
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_MICHAEL)
					IF (BIT_CURRENT <> BIT_MICHAEL)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_MICHAEL")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_FRANKLIN)
					IF (BIT_CURRENT <> BIT_FRANKLIN)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_FRANKLIN")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_TREVOR)
					IF (BIT_CURRENT <> BIT_TREVOR)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_TREVOR")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			
		BREAK
	ENDSWITCH
	
ENDPROC
#ENDIF
PROC UpdateLastKnownPedInfoPostMission(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)
#if USE_CLF_DLC
	UpdateLastKnownPedInfoPostMissionclf(sInfo,eMissionID)
#ENDIF
#if USE_NRM_DLC
	UpdateLastKnownPedInfoPostMissionNRM(sInfo,eMissionID)
#ENDIF
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH eMissionID
		
		CASE SP_MISSION_FAMILY_1		UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FAMILY1) BREAK
		CASE SP_MISSION_FAMILY_3		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FAMILY3) BREAK
		CASE SP_MISSION_FBI_1			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FBI1) BREAK
		CASE SP_MISSION_FBI_2			UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FBI2)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FBI2) BREAK
		
		CASE SP_MISSION_FBI_4_INTRO		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FBI4intro)
										UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FBI4intro) BREAK
		CASE SP_MISSION_FBI_5			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FBI5)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FBI5) BREAK
		CASE SP_MISSION_FBI_3			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FBI3) BREAK
		CASE SP_MISSION_FAMILY_4		UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FAMILY4) BREAK
		CASE SP_HEIST_AGENCY_1			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_AGENCY1) BREAK
		CASE SP_HEIST_AGENCY_PREP_1		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_AGENCYprep1) BREAK
		CASE SP_HEIST_AGENCY_3B			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_AGENCY3B) BREAK
		CASE SP_MISSION_CARSTEAL_1		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_CARSTEAL1)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_CARSTEAL1) BREAK
		CASE SP_HEIST_AGENCY_2			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_AGENCY2) BREAK
		CASE SP_MISSION_CARSTEAL_2		UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_CARSTEAL2) BREAK
		CASE SP_MISSION_FBI_4			UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FBI4)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FBI4) BREAK
//		CASE SP_MISSION_FAMILY_6		UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FAMILY6) BREAK
		CASE SP_HEIST_FINALE_PREP_D		UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FINALEprepD) BREAK
		CASE SP_MISSION_MARTIN_1		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_MARTIN1) BREAK
		CASE SP_MISSION_TREVOR_3		UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_TREVOR3) 
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_TREVOR3) BREAK
		CASE SP_HEIST_RURAL_1			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_RURAL1) BREAK
		CASE SP_MISSION_FRANKLIN_2		UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FRANKLIN2)	g_sPlayerLastVeh[CHAR_MICHAEL].model = DUMMY_MODEL_FOR_SCRIPT
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FRANKLIN2)	g_sPlayerLastVeh[CHAR_TREVOR].model = DUMMY_MODEL_FOR_SCRIPT	BREAK
//		CASE SP_MISSION_FBI_1_END		UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FBI1end) BREAK
		CASE SP_MISSION_FRANKLIN_1		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_FTa_FRANKLIN1a)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_FTa_FRANKLIN1a) BREAK
		CASE SP_MISSION_EXILE_2			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_EXILE2)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_EXILE2) BREAK
		CASE SP_MISSION_EXILE_3			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_EXILE3) BREAK
//		CASE SP_MISSION_MICHAEL_2		UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_MICHAEL2) BREAK
		CASE SP_MISSION_MICHAEL_3		UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_MICHAEL3)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_MICHAEL3) BREAK
		CASE SP_HEIST_DOCKS_2A			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_DOCKS2A)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_DOCKS2A) BREAK
		CASE SP_HEIST_FINALE_1			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FINALE1)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FINALE1)
										UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FINALE1) BREAK
		CASE SP_HEIST_DOCKS_2B			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_DOCKS2B)
										UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_DOCKS2B) BREAK
		CASE SP_HEIST_AGENCY_3A			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_AGENCY3A) BREAK
		CASE SP_HEIST_FINALE_2A			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FINALE2A) 
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FINALE2A) BREAK
		CASE SP_HEIST_FINALE_2B			UpdatePostMissionInfo(sInfo, CHAR_MICHAEL,	PR_SCENE_Ma_FINALE2B) 
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FINALE2B) BREAK
		CASE SP_HEIST_RURAL_2			UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_RURAL2A)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_RURAL2A) BREAK
		CASE SP_MISSION_CARSTEAL_4		UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_CARSTEAL4) BREAK
		CASE SP_HEIST_FINALE_2_INTRO	UpdatePostMissionInfo(sInfo, CHAR_FRANKLIN,	PR_SCENE_Fa_FINALE2intro)
										UpdatePostMissionInfo(sInfo, CHAR_TREVOR,	PR_SCENE_Ta_FINALE2intro) BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			IF eMissionID != SP_MISSION_NONE
				INT iFriendCharBitset, BIT_CURRENT
				iFriendCharBitset = g_sMissionStaticData[eMissionID].friendCharBitset
				SWITCH g_savedGlobals.sPlayerData.sInfo.eCurrentPed
					CASE CHAR_MICHAEL
						BIT_CURRENT = BIT_MICHAEL
					BREAK
					CASE CHAR_FRANKLIN
						BIT_CURRENT = BIT_FRANKLIN
					BREAK
					CASE CHAR_TREVOR
						BIT_CURRENT = BIT_TREVOR
					BREAK
				ENDSWITCH
				
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_MICHAEL)
					IF (BIT_CURRENT <> BIT_MICHAEL)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_MICHAEL")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_FRANKLIN)
					IF (BIT_CURRENT <> BIT_FRANKLIN)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_FRANKLIN")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
				IF IS_BITMASK_SET(iFriendCharBitset, BIT_TREVOR)
					IF (BIT_CURRENT <> BIT_TREVOR)
						//
						SAVE_STRING_TO_DEBUG_FILE("BIT_TREVOR")
						SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			
		BREAK
	ENDSWITCH
#ENDIF
#ENDIF
ENDPROC
// for use in MISSION_FLOW_MISSION_STARTING()
PROC RESET_POST_MISSION_SWITCHES()
	
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	
	enumCharacterList eChar
	REPEAT NUM_OF_PLAYABLE_PEDS eChar
		PED_REQUEST_SCENE_ENUM ePlayerLastScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eChar]
		IF GetLastKnownPedInfoPostMission(ePlayerLastScene, vLastKnownCoords, fLastKnownHead)
			IF  (ePlayerLastScene <> PR_SCENE_Fa_PHONECALL_ARM3)
			AND (ePlayerLastScene <> PR_SCENE_Fa_STRIPCLUB_ARM3)	// 
//			AND (ePlayerLastScene <> PR_SCENE_Ma_ARM3)				// #504817
			AND (ePlayerLastScene <> PR_SCENE_Fa_PHONECALL_FAM1)	// 		
//			AND (ePlayerLastScene <> PR_SCENE_Fa_STRIPCLUB_FAM1)	// 		
			AND (ePlayerLastScene <> PR_SCENE_Fa_PHONECALL_FAM3)	// 		
//			AND (ePlayerLastScene <> PR_SCENE_Fa_STRIPCLUB_FAM3)	// 		
			
				#IF IS_DEBUG_BUILD
				PRINTSTRING("RESET_POST_MISSION_SWITCHES(")
				PRINTSTRING("reset ped \"")
				PRINTINT(ENUM_TO_INT(eChar))
				PRINTSTRING("\" post-mission scene PR_SCENE_")
				PRINTINT(ENUM_TO_INT(ePlayerLastScene))
				PRINTSTRING(")")
				PRINTNL()
				#ENDIF
				
				g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[eChar] = PR_SCENE_INVALID
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

