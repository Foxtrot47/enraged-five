//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	properties_private.sch										//
//		AUTHOR			:	Rob Bray													//
//		DESCRIPTION		:	Functions dealing with property purchases	 				//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "flow_mission_data_public.sch"
USING "blip_control_public.sch"
USING "mission_control_public.sch"
USING "cellphone_public.sch"
USING "code_control_public.sch"

#if not USE_CLF_DLC
#if not USE_NRM_DLC
USING "properties_private.sch"
#endif
#endif


// get the current owner of a property
FUNC enumCharacterList GET_CURRENT_PROPERTY_OWNER(PROPERTY_ENUM thisProperty)
	RETURN g_savedGlobals.sPropertyData.propertyOwnershipData[thisProperty].charOwner
ENDFUNC

// suppress the loading and creation of property for sale signs
PROC SUPPRESS_PROPERTY_FOR_SALE_SIGNS(BOOL bSuppress)
	IF bSuppress
		SET_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
	ELSE
		CLEAR_BITMASK_AS_ENUM(g_PropertySystemData.iSystemBit, PROPERTY_SYSTEM_BIT_SUPPRESS_FOR_SALE_SIGNS)
	ENDIF
ENDPROC

// sets the result of a property management mission on script cleanup
PROC SET_PROPERTY_MANAGEMENT_RESULT(PROPERTY_MANAGEMENT_RESULT_ENUM thisResult)
	g_PropertySystemData.currentPropertyManagementResult = thisResult
ENDPROC

// get the current property management property
FUNC PROPERTY_ENUM GET_CURRENT_PROPERTY_MANAGEMENT_PROPERTY()
	RETURN g_PropertySystemData.currentPropertyManagementProperty
ENDFUNC

// get the current property management mission 
FUNC PROPERTY_MANAGEMENT_MISSION_ENUM GET_CURRENT_PROPERTY_MANAGEMENT_MISSION()
	RETURN g_PropertySystemData.currentPropertyManagementMission
ENDFUNC

// get the current property management variation, converted to a delivery variation
FUNC DELIVERY_MISSION_VARIATION_ENUM GET_CURRENT_DELIVERY_VARIATION()
	RETURN CONVERT_INT_TO_DELIVERY_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// get the current property management variation, converted to a defend variation
FUNC DEFEND_MISSION_VARIATION_ENUM GET_CURRENT_DEFEND_VARIATION()
	RETURN CONVERT_INT_TO_DEFEND_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// get the current property management variation, converted to a stolen variation
FUNC RECOVER_STOLEN_MISSION_VARIATION_ENUM GET_CURRENT_RECOVER_STOLEN_VARIATION()
	RETURN CONVERT_INT_TO_RECOVER_STOLEN_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// get the current property management variation, converted to a gang attack variation
FUNC GANG_ATTACK_MISSION_VARIATION_ENUM GET_CURRENT_GANG_ATTACK_VARIATION()
	RETURN CONVERT_INT_TO_GANG_ATTACK_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// get the current property management variation, converted to a defend variation
FUNC HOSE_DOWN_PROTESTORS_MISSION_VARIATION_ENUM GET_CURRENT_HOSE_DOWN_PROTESTORS_VARIATION()
	RETURN CONVERT_INT_TO_HOSE_DOWN_PROTESTORS_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// get the current property management variation, converted to a defend variation
FUNC PLANE_PROMOTION_MISSION_VARIATION_ENUM GET_CURRENT_PLANE_PROMOTION_VARIATION()
	RETURN CONVERT_INT_TO_PLANE_PROMOTION_VARIATION(g_PropertySystemData.currentPropertyManagementProperty, g_PropertySystemData.iCurrentPropertyManagementVariation)
ENDFUNC

// gets a property management vehicle and converts it to a script entity
FUNC VEHICLE_INDEX GET_PROPERTY_MANAGEMENT_EVENT_VEHICLE(INT iThisVehicleConst)
	IF IS_VEHICLE_DRIVEABLE(g_PropertySystemData.managementEventVehicle[iThisVehicleConst])
		SET_ENTITY_AS_MISSION_ENTITY(g_PropertySystemData.managementEventVehicle[iThisVehicleConst], TRUE, TRUE)
		RETURN g_PropertySystemData.managementEventVehicle[iThisVehicleConst]
	ENDIF
	
	RETURN NULL
ENDFUNC

// put all of a dead character's properties back on the market...
PROC PUT_DEAD_CHARACTERS_PROPERTIES_BACK_ON_MARKET(enumCharacterList deadChar)
	INT i
	
	REPEAT COUNT_OF(PROPERTY_ENUM) i 
		IF g_savedGlobals.sPropertyData.propertyOwnershipData[i].charOwner = deadChar
			g_savedGlobals.sPropertyData.propertyOwnershipData[i].charOwner = NO_CHARACTER
			SET_BITMASK_AS_ENUM(g_savedGlobals.sPropertyData.propertyOwnershipData[i].iBit, PROPERTY_BIT_SENT_EMAIL)
		ENDIF
	ENDREPEAT
ENDPROC

// override fail reason for property mission
PROC OVERRIDE_PROPERTY_MISSION_FAIL_REASON(PROPERTY_MISSION_FAIL_REASON_OVERRIDE_ENUM failReason)
	g_PropertySystemData.failReasonOverride = failReason
ENDPROC

// override pass reason for property mission
PROC OVERRIDE_PROPERTY_MISSION_PASS_REASON(PROPERTY_MISSION_PASS_REASON_OVERRIDE_ENUM passReason)
	g_PropertySystemData.passReasonOverride = passReason
ENDPROC

// cancel out property streamvol / new load scene
PROC CANCEL_PROPERTY_MAP_LOADS()
	NEW_LOAD_SCENE_STOP()	// reordered call to come before streamvol_delete for B*2061680
	IF STREAMVOL_IS_VALID(g_PropertySystemData.streamVolume)
		STREAMVOL_DELETE(g_PropertySystemData.streamVolume)
		CPRINTLN(DEBUG_PROPERTY, GET_THIS_SCRIPT_NAME(), " : CANCEL_PROPERTY_MAP_LOADS() deleted g_PropertySystemData.streamVolume")
	ENDIF
ENDPROC

DEBUGONLY FUNC STRING GET_PROPERTY_NAME_FOR_DEBUG(PROPERTY_ENUM eProperty)

	SWITCH eProperty
		CASE PROPERTY_CAR_SCRAP_YARD
			RETURN "PROPERTY_CAR_SCRAP_YARD"
		BREAK
	ENDSWITCH
	
	RETURN "INVALID_PROPERTY_NAME"

ENDFUNC

