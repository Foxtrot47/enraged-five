//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   clubs_public.sch                                         	//
//      AUTHOR          :   Rob Bray                                                    //
//      DESCRIPTION     :   Common functions for gun and strip clubs					//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

// has the player equipped a weapon?
FUNC BOOL PLAYER_HAS_WEAPON_EQUIPPED()
	WEAPON_TYPE playerWeapon
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWeapon)
	IF playerWeapon = WEAPONTYPE_INVALID
	OR playerWeapon = WEAPONTYPE_UNARMED
	OR playerWeapon = WEAPONTYPE_ELECTRIC_FENCE
	OR playerWeapon = GADGETTYPE_PARACHUTE
	OR playerWeapon = WEAPONTYPE_OBJECT
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

// is the player doing something that should send the club aggro if they're spotted?
FUNC BOOL IS_PLAYER_DOING_AGGRO_ACTIVITY_TO_PED(PED_INDEX ped)
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped, PLAYER_PED_ID()) 
		IF IS_PED_INJURED(ped)
			#IF IS_DEBUG_BUILD
			PRINTSTRING("HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY has been trigger on model: ")		
			PRINTNL()
			#ENDIF
			RETURN TRUE	
		ENDIF
	ELIF PLAYER_HAS_WEAPON_EQUIPPED()
		// aiming with a weapon
		IF IS_PLAYER_TARGETTING_ENTITY(GET_PLAYER_INDEX(), ped)
		OR IS_PLAYER_FREE_AIMING_AT_ENTITY(GET_PLAYER_INDEX(), ped)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
