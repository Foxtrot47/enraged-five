// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	carsteal_public.sch
//		AUTHOR			:	Matt Booton
//		DESCRIPTION		:	Contains functions that are global across missions in the
//							Car Steal stand.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
USING "commands_entity.sch"
USING "commands_vehicle.sch"
USING "commands_debug.sch"

/// PURPOSE:
///    Creates one of the six cars stolen in the Car Steal strand. Use this to ensure the cars remain consistent across missions 
///    (e.g. they keep the same colours, number plates, etc). The function works the same as CREATE_VEHICLE, so it'll assume you've
///    already requested the model.
/// PARAMS:
///    model - The model name of the car: the command will assert if this is not one of the six models used in the Car Steal strand.
/// RETURNS:
///    The vehicle index of the created car.
FUNC VEHICLE_INDEX CREATE_CAR_STEAL_STRAND_CAR(MODEL_NAMES model, VECTOR vPos, FLOAT fHeading = 0.0)
	VEHICLE_INDEX veh
	IF model = MONROE
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Race Yellow
		SET_VEHICLE_COLOURS(veh, 89, 89)
		SET_VEHICLE_EXTRA_COLOURS(veh, 88, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, " FA5T66 ")
	ELIF model = CHEETAH
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Dark Blue
		SET_VEHICLE_COLOURS(veh, 62, 62)
		SET_VEHICLE_EXTRA_COLOURS(veh, 68, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, " SDTM1YP")
	ELIF model = STINGER
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Red
		SET_VEHICLE_COLOURS(veh, 27, 27)
		SET_VEHICLE_EXTRA_COLOURS(veh, 36, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, "ALPHADOG")
		SET_VEHICLE_EXTRA(veh, 1, TRUE)
	ELIF model = JB700
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Dark Steel
		SET_VEHICLE_COLOURS(veh, 3, 3)
		SET_VEHICLE_EXTRA_COLOURS(veh, 3, 3)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, "  4G3NT")
	ELIF model = ENTITYXF
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Orange
		SET_VEHICLE_COLOURS(veh, 38, 38)
		SET_VEHICLE_EXTRA_COLOURS(veh, 37, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, " MKB652 ")
	ELIF model = ZTYPE
		veh = CREATE_VEHICLE(model, vPos, fHeading)
		// Black
		SET_VEHICLE_COLOURS(veh, 0, 0)
		SET_VEHICLE_EXTRA_COLOURS(veh, 10, 0)
		SET_VEHICLE_NUMBER_PLATE_TEXT(veh, " V1NTAG3")
	ELSE
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("CREATE_CAR_STEAL_STRAND_CAR: The specified model is not a Car Steal strand model.")
		#ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(veh)
		SET_VEHICLE_DIRT_LEVEL(veh, 0.0)
		SET_VEHICLE_AUTOMATICALLY_ATTACHES(veh, FALSE)
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(veh, FALSE)
	ENDIF
	
	RETURN veh
ENDFUNC
