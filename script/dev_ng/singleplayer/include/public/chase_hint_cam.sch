USING "rage_builtins.sch"
using "commands_pad.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "cellphone_public.sch"
using "menu_public.sch"

// Allows the player to press CIRCLE while in a vehicle to look at the chase target.
// Barry

CONST_INT	iCHASE_HINT_INFINITE_DWELL_TIME				-1		
CONST_INT	iCHASE_HINT_INTERP_IN_TIME					1000	//1500	//2500	
CONST_INT	iCHASE_HINT_INTERP_OUT_TIME					1000	//1500	//2500	
CONST_FLOAT	fCHASE_HINT_CAM_FOV							40.0

CONST_INT	iCHASE_HINT_HELP_DISPLAY_MAX					3

//bool bHint_toggleOn

ENUM enumHintCamStartMethod
	HCSM_0 = 0,
	HCSM_1_forced,
	HCSM_2_pressed,
	HCSM_3_hold,
	
	HCSM_none = -1
ENDENUM
//CV hint cam states
ENUM eHINT_CAM_STATE

	hint_OFF,
	hint_CHECK,
	hint_HELD,
	hint_TOGGLE,
	hint_FORCED

endenum
ENUM eHINT_TYPE
	hType_ANY,
	hType_ON_FOOT,
	hTYPE_VEH
endenum
STRUCT CHASE_HINT_CAM_STRUCT
	BOOL 		bHintInterpingBack 			= FALSE
	BOOL 		bHintInterpingIn 			= FALSE
	INT			iForceChaseHintCamGameTime 	= -1
	STRING 		sDisplayedHintHelp 			= NULL
	//CV
	int iInputCheckTimer 					= 0
	eHINT_CAM_STATE	eHintCamMethod			= hint_off
	eHINT_TYPE 		ehType					= hType_ANY
	
	bool 			bHint_toggleOn
	INT 			iInterpInTimer 			= 0
	
	INT 			iInterpInLength			= iCHASE_HINT_INTERP_IN_TIME
	INT 			iInterpOutLength		= iCHASE_HINT_INTERP_OUT_TIME
	
	bool			bPostFXActive			= False
ENDSTRUCT

// *******************************************************************************************
//	BAZZAMATAZZ PROCEDURES AND FUNCTIONS
// *******************************************************************************************
// PURPOSE: Returns TRUE if it's safe to output chase hint cam help text.
FUNC BOOL SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bool Bcheck_player_control = true,bool bCheck_currentCam = true, bool bSpecialCamera = false)	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] SAFE_TO_PRINT_CHASE_HINT_CAM_HELP - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(Bcheck_player_control))
	ENDIF
	#ENDIF
	
	if Bcheck_player_control
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": IS_PLAYER_CONTROL_ON - false")
			RETURN FALSE
		ENDIF
	endif
	
	if bSpecialCamera
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - TRUE bSpecialCamera is TRUE")
		RETURN TRUE
	endif
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": IS_PLAYER_SWITCH_IN_PROGRESS - false")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": IS_PHONE_ONSCREEN - false")
		RETURN FALSE
	ENDIF
	
	if IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": IS_CUSTOM_MENU_ON_SCREEN - false")
		return false
	endif
	
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": NETWORK_TEXT_CHAT_IS_TYPING - false")
		RETURN FALSE
	ENDIF
	
	IF g_bBrowserVisible
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": g_bBrowserVisible - false")
		RETURN FALSE
	ENDIF
	
	if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Appinternet")) > 0 			// internet check	
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": Appinternet > 0  - false")
		return false
	endif
	if g_bInATM				// ATM check
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": g_bInATM - false")	
		return false
	endif
	
	
	
	if bCheck_currentCam
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)

			IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_CINEMATIC
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE CAM_VIEW_MODE_CONTEXT_IN_BOAT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					RETURN FALSE
				ENDIF
			ELIF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_CINEMATIC
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE CAM_VIEW_MODE_CONTEXT_IN_HELI is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					RETURN FALSE
				ENDIF
			ELIF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_CINEMATIC
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					RETURN FALSE
				ENDIF
			ELIF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_CINEMATIC
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					RETURN FALSE
				ENDIF
			ELIF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_CINEMATIC
				OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE CAM_VIEW_MODE_CONTEXT_ON_BIKE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					RETURN FALSE
				ENDIF
			ELIF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_CINEMATIC
			  OR GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE GET_FOLLOW_VEHICLE_CAM_VIEW_MODE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
				RETURN FALSE
			ENDIF
			
			IF IS_GAMEPLAY_CAM_LOOKING_BEHIND()		
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE IS_GAMEPLAY_CAM_LOOKING_BEHIND is TRUE")
				RETURN FALSE
			ENDIF
			
		ENDIF
	endif
	
	IF WVM_FLOW_IS_CURRENT_MISSION_WVM_MISSION()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_STOCKPILE(g_FMMC_STRUCT.iAdversaryModeType)
	OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX iVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			VEHICLE_SEAT sVehicle = GET_SEAT_PED_IS_IN(PLAYER_PED_ID())
			IF IS_TURRET_SEAT(iVehicle,sVehicle)
			OR (GET_ENTITY_MODEL(iVehicle) = APC AND sVehicle != VS_DRIVER)
			OR (GET_ENTITY_MODEL(iVehicle) = AKULA AND sVehicle != VS_DRIVER)
			OR (GET_ENTITY_MODEL(iVehicle) = RIOT2 AND sVehicle = VS_FRONT_RIGHT AND HAS_VEHICLE_GOT_MOD(iVehicle, MOD_ROOF) AND GET_VEHICLE_MOD(iVehicle, MOD_ROOF) != -1)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE IN A TURRET SEAT DURING MP MISSION")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bBlockHintCamUsageInMPMission
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE g_bBlockHintCamUsageInMPMissionis set")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": SAFE_TO_PRINT_CHASE_HINT_CAM_HELP() - FALSE IS_PLAYER_IN_ANY_SIMPLE_INTERIOR is TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_USING_MISSION_SPECIFIC_HINT_CAM_IN_FM(BOOL bSet)
	IF MPGlobalsAmbience.bUsingMissionSpecificHintCamInFreemode != bSet
		MPGlobalsAmbience.bUsingMissionSpecificHintCamInFreemode = bSet
		
		#IF IS_DEBUG_BUILD
		IF bSet
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_USING_MISSION_SPECIFIC_HINT_CAM_IN_FM - bUsingMissionSpecificHintCamInFreemode = TRUE")
		ELSE
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": SET_USING_MISSION_SPECIFIC_HINT_CAM_IN_FM - bUsingMissionSpecificHintCamInFreemode = FALSE")
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL USING_MISSION_SPECIFIC_HINT_CAM_IN_FM()
	RETURN MPGlobalsAmbience.bUsingMissionSpecificHintCamInFreemode
ENDFUNC

// PURPOSE: Call this to cleanup the hintcam when you're done with it.
PROC KILL_CHASE_HINT_CAM(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, STRING sHintHelp = NULL,bool bImmediately = false)
	
	
	#IF IS_DEBUG_BUILD
	IF (MPGlobalsAmbience.bDisableKillChaseHintCam)
		EXIT
	ENDIF
	#ENDIF	
	
	
	//Don't want to kill the HInt Cam if the Kill Strip hint cam is running
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_TRIP_SKIP_IN_PROGRESS)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		//PD moved this in here so it only plays if the hint was active.
		STOP_GAMEPLAY_HINT(bImmediately)
		ANIMPOSTFX_STOP("FocusIn")
		STOP_AUDIO_SCENE("HINT_CAM_SCENE")
		if thisChaseHintCamStruct.bPostFXActive 
			ANIMPOSTFX_PLAY("FocusOut",0,false)
			PLAY_SOUND_FRONTEND(-1, "FocusOut", "HintCamSounds")
			thisChaseHintCamStruct.bPostFXActive = FALSE
		endif				
	ENDIF
	

	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)		//803233
	
	thisChaseHintCamStruct.bHintInterpingIn				= FALSE
	thisChaseHintCamStruct.bHintInterpingBack			= FALSE
	thisChaseHintCamStruct.iForceChaseHintCamGameTime	= -1
	thisChaseHintCamStruct.iInterpInTimer				= 0
	thisChaseHintCamStruct.eHintCamMethod				= hint_off
	thisChaseHintCamStruct.ehType						= hType_ANY
	
	#if IS_DEBUG_BUILD
		g_bDrawLiteralSceneString = false	// turn off debug
	#endif
	
	STRING sThisHintHelp = sHintHelp
	IF IS_STRING_NULL(sThisHintHelp)
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			sThisHintHelp = "CMN_HINT"
		ELSE
			sThisHintHelp = "FM_IHELP_HNT"
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL(thisChaseHintCamStruct.sDisplayedHintHelp)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(thisChaseHintCamStruct.sDisplayedHintHelp)
//			#if IS_DEBUG_BUILD
//				println("[CHASE HINT CAM] KILL CHASE CAM(clear help) : IS_THIS_HELP_MESSAGE_BEING_DISPLAYED: sDisplayedHintHelp",thisChaseHintCamStruct.sDisplayedHintHelp)
//			#endif
			CLEAR_HELP()
		ENDIF
	ENDIF
	IF NOT IS_STRING_NULL(sThisHintHelp)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
//			#if IS_DEBUG_BUILD
//				println("[CHASE HINT CAM] KILL CHASE CAM(clear help) : IS_THIS_HELP_MESSAGE_BEING_DISPLAYED: sThisHintHelp",sThisHintHelp)
//			#endif
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_CHASE_HINT_CAM(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, BOOL bForce)
	IF bForce
		thisChaseHintCamStruct.iForceChaseHintCamGameTime	= GET_GAME_TIMER()
	ELSE
		thisChaseHintCamStruct.iForceChaseHintCamGameTime	= -1
	ENDIF
ENDPROC

// set custom interp times for chase hint cam
PROC SET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, INT iTime)
	thisChaseHintCamStruct.iInterpInLength = iTime
ENDPROC

PROC SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, INT iTime)
	thisChaseHintCamStruct.iInterpOutLength = iTime
ENDPROC

PROC RESET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct)
	thisChaseHintCamStruct.iInterpInLength = iCHASE_HINT_INTERP_IN_TIME
ENDPROC

PROC RESET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct)
	thisChaseHintCamStruct.iInterpOutLength = iCHASE_HINT_INTERP_OUT_TIME
ENDPROC

// *******************************************************************************************
//	ALWYNS PROCEDURES AND FUNCTIONS
// *******************************************************************************************
//#NO_WIKI
FUNC BOOL PRIVATE_IsChaseHintCamButtonPressedOnFoot(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			
			IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) AND (GET_GAME_TIMER() > iDisplayingReplayRecordUI)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_IsChaseHintCamButtonJustPressedOnFoot(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) AND (GET_GAME_TIMER() > iDisplayingReplayRecordUI)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_IsChaseHintCamButtonJustReleasedOnFoot(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_IsChaseHintCamButtonPressedInVehicle(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		
		IF IS_FOLLOW_VEHICLE_CAM_ACTIVE()
			IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) AND (GET_GAME_TIMER() > iDisplayingReplayRecordUI)
				SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_IsChaseHintCamButtonJustPressedInVehicle(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] PRIVATE_IsChaseHintCamButtonJustPressedInVehicle - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(bCheck_player_control))
	ENDIF
	#ENDIF
	
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		
		IF IS_FOLLOW_VEHICLE_CAM_ACTIVE()
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) AND (GET_GAME_TIMER() > iDisplayingReplayRecordUI)
				SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_IsChaseHintCamButtonJustReleasedInVehicle(bool bCheck_player_control = true,bool bCheck_currentCam = true,bool bSpecialCamera = false)
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		
		IF IS_FOLLOW_VEHICLE_CAM_ACTIVE()
			IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
FUNC BOOL PRIVATE_isEntityAlive(ENTITY_INDEX ENTITY)
  	IF DOES_ENTITY_EXIST(ENTITY)
		IF IS_ENTITY_A_VEHICLE(ENTITY)
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(ENTITY))
				RETURN TRUE 
			ENDIF
		ELIF IS_ENTITY_A_PED(ENTITY)
			IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(ENTITY))
	          	RETURN TRUE 
	    	ENDIF
		ELIF IS_ENTITY_AN_OBJECT(ENTITY)
			return TRue	
		ENDIF           
	ENDIF
      RETURN FALSE
ENDFUNC
PROC PRIVATE_StartCoordChaseHintCam(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VECTOR vHintAt, HINT_TYPE Type = HINTTYPE_DEFAULT)
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)	
	
	Int interpin 	= thisChaseHintCamStruct.iInterpInLength
	int interpout 	= thisChaseHintCamStruct.iInterpOutLength
	
	if type = HINTTYPE_VEHICLE_HIGH_ZOOM
		if interpin < 1500
			interpin = 1500
		endif
		if interpout < 1500
			interpout = 1500
		endif
	endif
	
	SET_GAMEPLAY_COORD_HINT(vHintAt,iCHASE_HINT_INFINITE_DWELL_TIME,
			interpIn,	
			interpOut,	
			Type)
	
	SCRIPT_LOOK_FLAG LookFlags = SLF_WHILE_NOT_IN_FOV	//SLF_DEFAULT
	SCRIPT_LOOK_PRIORITY priority = SLF_LOOKAT_HIGH		//SLF_LOOKAT_MEDIUM
	TASK_LOOK_AT_COORD(PLAYER_PED_ID(), vHintAt, iCHASE_HINT_INFINITE_DWELL_TIME, LookFlags, priority)
	
//	SET_TRANSITION_TIMECYCLE_MODIFIER("Hint_cam", TO_FLOAT(thisChaseHintCamStruct.iInterpInLength) / 1000.0)
	
	ANIMPOSTFX_PLAY("FocusIn", 0, FALSE)
	START_AUDIO_SCENE("HINT_CAM_SCENE")
	PLAY_SOUND_FRONTEND(-1, "FocusIn", "HintCamSounds")
	
	thisChaseHintCamStruct.bPostFXActive = TRUE
	thisChaseHintCamStruct.iInterpInTimer = GET_GAME_TIMER()
	thisChaseHintCamStruct.bHintInterpingIn = TRUE
	thisChaseHintCamStruct.bHintInterpingBack = FALSE
	
ENDPROC
PROC PRIVATE_StartEntityChaseHintCamWithOffset(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, VECTOR vHint_offset, HINT_TYPE Type = HINTTYPE_DEFAULT)
	CDEBUG1LN(DEBUG_NET_GUN_TURRET, "PRIVATE_StartEntityChaseHintCamWithOffset")
	IF g_bBombuskaExitCamTimerOn = TRUE
		CDEBUG1LN(DEBUG_NET_GUN_TURRET, GET_THIS_SCRIPT_NAME(), ":SHOULD_CONTROL_CHASE_HINT_CAM: g_bBombuskaExitCamTimerOn = false, EXITING")	
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(chaseEntity)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("chase hint cam entity is dead!")
		#ENDIF
		
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct)
	ENDIF
	
	IF ARE_VECTORS_EQUAL(vHint_offset, <<0,0,0>>)
		IF IS_ENTITY_A_PED(chaseEntity)
			PED_INDEX chasePed = GET_PED_INDEX_FROM_ENTITY_INDEX(chaseEntity)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(chasePed)
				IF IS_PED_A_PLAYER(chasePed)
					IF !IS_PLAYER_FEMALE()
						vHint_offset = <<0.0, 0.0, 1.0>>
					ENDIF
				ELSE
					IF IS_PED_MALE(chasePed)
						vHint_offset = <<0.0, 0.0, 1.0>>
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	
	Int interpin 	= thisChaseHintCamStruct.iInterpInLength
	int interpout 	= thisChaseHintCamStruct.iInterpOutLength
	
	if type = HINTTYPE_VEHICLE_HIGH_ZOOM
		if interpin < 1500
			interpin = 1500
		endif
		if interpout < 1500
			interpout = 1500
		endif
	endif
	
	SET_GAMEPLAY_ENTITY_HINT(chaseEntity, vHint_offset, TRUE,
			iCHASE_HINT_INFINITE_DWELL_TIME,	//INT iDwellTime = DEFAULT_DWELL_TIME,
			interpin,			//INT iInterpTo = DEFAULT_INTERP_IN_TIME,
			interpout,		//INT iInterpFrom = DEFAULT_INTERP_OUT_TIME,
			Type)
	
	SCRIPT_LOOK_FLAG LookFlags = SLF_WHILE_NOT_IN_FOV	//SLF_DEFAULT
	SCRIPT_LOOK_PRIORITY priority = SLF_LOOKAT_HIGH		//SLF_LOOKAT_MEDIUM
	TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), chaseEntity, iCHASE_HINT_INFINITE_DWELL_TIME, LookFlags, priority)
	
//	SET_TRANSITION_TIMECYCLE_MODIFIER("Hint_cam", TO_FLOAT(thisChaseHintCamStruct.iInterpInLength) / 1000.0)
	
	ANIMPOSTFX_PLAY("FocusIn", 0, FALSE)
	START_AUDIO_SCENE("HINT_CAM_SCENE")
	PLAY_SOUND_FRONTEND(-1, "FocusIn", "HintCamSounds")
	thisChaseHintCamStruct.bPostFXActive = TRUE
	thisChaseHintCamStruct.iInterpInTimer = GET_GAME_TIMER()
	thisChaseHintCamStruct.bHintInterpingIn = TRUE
	thisChaseHintCamStruct.bHintInterpingBack = FALSE
	
ENDPROC

FUNC BOOL PRIVATE_chaseHintCamStillSet(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct)
	IF thisChaseHintCamStruct.iForceChaseHintCamGameTime > 0
		
		//CONST_INT iCHASE_HINT_RESET_FORCE_TIME	(thisChaseHintCamStruct.iInterpOutLength / 2)
		INT iCHASE_HINT_RESET_FORCE_TIME = (thisChaseHintCamStruct.iInterpOutLength / 2)
	
		IF (thisChaseHintCamStruct.iForceChaseHintCamGameTime+iCHASE_HINT_RESET_FORCE_TIME) > GET_GAME_TIMER()
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_StopChaseHintCam(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct)
	
	if PRIVATE_isEntityAlive(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	endif
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
		STOP_GAMEPLAY_HINT()
		STOP_AUDIO_SCENE("HINT_CAM_SCENE")
		ANIMPOSTFX_STOP("FocusIn")		
		if thisChaseHintCamStruct.bPostFXActive
			ANIMPOSTFX_PLAY("FocusOut", 0, FALSE)
			PLAY_SOUND_FRONTEND(-1, "FocusOut", "HintCamSounds")
			thisChaseHintCamStruct.bPostFXActive = FALSE
		endif		
	endif
	
	thisChaseHintCamStruct.iForceChaseHintCamGameTime	= -1
	thisChaseHintCamStruct.bHintInterpingBack = TRUE
ENDPROC

// *******************************************************************************************
//	REDUNDANT CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************

PROC StartVehicleChaseHintCam(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle)
	PRIVATE_StartEntityChaseHintCamWithOffset(thisChaseHintCamStruct, chaseVehicle, <<0,0,0>>)
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("StartVehicleChaseHintCam to be removed, please use a relevent hint cam command e.g. CONTROL_VEHICLE_CHASE_HINT_CAM()")
	#endif
ENDPROC

FUNC INT ChaseHelpTextDisplayed(BOOL bIncrament)

	#if USE_CLF_DLC
		SWITCH g_OnMissionState
			CASE MISSION_TYPE_STORY
			CASE MISSION_TYPE_STORY_FRIENDS			
				IF bIncrament
					g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_mission++
				ENDIF
				RETURN g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_mission			
			BREAK			
			CASE MISSION_TYPE_RANDOM_EVENT
			CASE MISSION_TYPE_OFF_MISSION
			
				IF bIncrament
					g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_RE++
				ENDIF
				RETURN g_savedGlobalsClifford.sAmbient.iChaseHelpTextDisplayed_RE
				
			BREAK
			
			DEFAULT
			BREAK
		ENDSWITCH		
	#ENDIF
	#if USE_NRM_DLC
		SWITCH g_OnMissionState
			CASE MISSION_TYPE_STORY
			CASE MISSION_TYPE_STORY_FRIENDS			
				IF bIncrament
					g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_mission++
				ENDIF
				RETURN g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_mission			
			BREAK
			
			CASE MISSION_TYPE_RANDOM_EVENT
			CASE MISSION_TYPE_OFF_MISSION
			
				IF bIncrament
					g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_RE++
				ENDIF
				RETURN g_savedGlobalsnorman.sAmbient.iChaseHelpTextDisplayed_RE
				
			BREAK
			
			DEFAULT
			BREAK
		ENDSWITCH
	#ENDIF
	
	#if not USE_SP_DLC
		SWITCH g_OnMissionState
			CASE MISSION_TYPE_STORY
			CASE MISSION_TYPE_STORY_FRIENDS			
				IF bIncrament
					g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_mission++
				ENDIF
				RETURN g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_mission			
			BREAK
			CASE MISSION_TYPE_RANDOM_CHAR
			
				IF bIncrament
					g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RC++
				ENDIF
				RETURN g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RC
				
			BREAK
			CASE MISSION_TYPE_RANDOM_EVENT
			CASE MISSION_TYPE_OFF_MISSION
			
				IF bIncrament
					g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RE++
				ENDIF
				RETURN g_savedGlobals.sAmbient.iChaseHelpTextDisplayed_RE
				
			BREAK
			
			DEFAULT
			BREAK
		ENDSWITCH
	#ENDIF
	
	
	RETURN iCHASE_HINT_HELP_DISPLAY_MAX
ENDFUNC
	
FUNC BOOL ShouldDisplayChaseHelpText(string sHintHelp = NULL)
	
	#IF IS_DEBUG_BUILD
		#if USE_CLF_DLC
			IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
				RETURN FALSE
			ENDIF
		#ENDIF
		#if USE_NRM_DLC
			IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
				RETURN FALSE
			ENDIF
		#ENDIF
		#if not USE_SP_DLC
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				RETURN FALSE
			ENDIF
		#ENDIF
	#ENDIF
	
	IF NOT SAFE_TO_PRINT_CHASE_HINT_CAM_HELP()
		IF( NOT IS_STRING_NULL_OR_EMPTY(sHintHelp)
		and IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHintHelp))
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMN_HINT")
		 	CLEAR_HELP()		
		Endif
		RETURN FALSE
	ENDIF
	
	SWITCH g_OnMissionState
		CASE MISSION_TYPE_STORY
		CASE MISSION_TYPE_STORY_FRIENDS
			IF ChaseHelpTextDisplayed(FALSE) < iCHASE_HINT_HELP_DISPLAY_MAX
				
				RETURN TRUE
			ENDIF
		BREAK
		CASE MISSION_TYPE_RANDOM_CHAR
			IF ChaseHelpTextDisplayed(FALSE) < 1
				RETURN TRUE
			ENDIF
		BREAK
		CASE MISSION_TYPE_RANDOM_EVENT
		CASE MISSION_TYPE_OFF_MISSION
			IF ChaseHelpTextDisplayed(FALSE) < 1
				RETURN TRUE
			ENDIF
		BREAK
		
		DEFAULT
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	CHASE HINT CAM HELP PROCEDURES AND FUNCTIONS
// *******************************************************************************************

PROC DEBUG_RenderChaseHintCam(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct)
	IF NOT g_flowUnsaved.bShowMissionFlowDebugScreen
		EXIT
	ENDIF
	
	IF NOT g_bDrawLiteralSceneString
		EXIT
	ENDIF
	
	CONST_FLOAT fTEXT_0_y_offset	0.4450
	CONST_FLOAT fTEXT_1_y_offset	0.4808
	CONST_FLOAT fTEXT_2_y_offset	0.5165
	CONST_FLOAT fTEXT_3_y_offset	0.5522
	
	TEXT_LABEL_63 tHintCamInfo = "eHintCamMethod: "
	SWITCH thisChaseHintCamStruct.eHintCamMethod
		CASE hint_CHECK		tHintCamInfo += "CHECK" BREAK
		CASE hint_TOGGLE	tHintCamInfo += "TOGGLE" BREAK
		CASE hint_HELD		tHintCamInfo += "HELD" 	 BREAK
		CASE hint_FORCED	tHintCamInfo += "FORCED" BREAK

		DEFAULT
			tHintCamInfo += ENUM_TO_INT(thisChaseHintCamStruct.eHintCamMethod)
		BREAK
	ENDSWITCH
	
	text_label_63 tHintTypeInfo = "Hint Type: "
	switch thisChaseHintCamStruct.ehType
	case hType_ANY 		tHintTypeInfo += "ANY"	break
	case hType_ON_FOOT	tHintTypeInfo += "FOOT"	break
	case hTYPE_VEH		tHintTypeInfo += "VEH"	break
	DEFAULT
		tHintTypeInfo += ENUM_TO_INT(thisChaseHintCamStruct.ehType)
	BREAK
	endswitch
	
	TEXT_LABEL_63 tHintCamHelp = thisChaseHintCamStruct.sDisplayedHintHelp
	tHintCamHelp += ": "
	tHintCamHelp += ChaseHelpTextDisplayed(FALSE)
	
	IF thisChaseHintCamStruct.eHintCamMethod = hint_OFF
		IF NOT thisChaseHintCamStruct.bHintInterpingBack AND NOT thisChaseHintCamStruct.bHintInterpingIn
			INT R = 255, G = 000, B = 000		//red
			DRAW_RECT(0.5,0.5, 0.1,0.1, R,G,B,064)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_0_y_offset,"STRING","HINT off, not interping")
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_1_y_offset,"STRING", tHintCamInfo)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(thisChaseHintCamStruct.sDisplayedHintHelp)
				SET_TEXT_SCALE(0.5,0.5)
				SET_TEXT_COLOUR(R,G,B,255)
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_2_y_offset,"STRING", tHintCamHelp)
			ENDIF
		ELSE
			INT R = 000, G = 255, B = 255		//cyan (sky blue)
			DRAW_RECT(0.5,0.5, 0.1,0.1, R,G,B,064)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			
			TEXT_LABEL_63 str = "HINT off, interping: "
			
			IF thisChaseHintCamStruct.bHintInterpingBack
				str += " back "
			ENDIF
			IF thisChaseHintCamStruct.bHintInterpingIn
				str += " in "
			ENDIF
			
			INT iInterpInTime = ABSI((thisChaseHintCamStruct.iInterpInTimer + thisChaseHintCamStruct.iInterpInLength) - GET_GAME_TIMER())
			IF (iInterpInTime < 1000)	str += "0" ENDIF
			IF (iInterpInTime < 100)	str += "0" ENDIF
			IF (iInterpInTime < 10)		str += "0" ENDIF
			str += iInterpInTime
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_0_y_offset,"STRING",str)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_1_y_offset,"STRING", tHintCamInfo)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(thisChaseHintCamStruct.sDisplayedHintHelp)
				SET_TEXT_SCALE(0.5,0.5)
				SET_TEXT_COLOUR(R,G,B,255)
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_2_y_offset,"STRING", tHintCamHelp)
			ENDIF
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_3_y_offset,"STRING",tHintTypeInfo)
			
		ENDIF
	ELSE
		IF NOT thisChaseHintCamStruct.bHintInterpingBack AND NOT thisChaseHintCamStruct.bHintInterpingIn
			INT R = 000, G = 255, B = 000		//green
			DRAW_RECT(0.5,0.5, 0.1,0.1, R,G,B,064)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_0_y_offset,"STRING","HINT on, not interping")
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_1_y_offset,"STRING", tHintCamInfo)			
			
			IF NOT IS_STRING_NULL_OR_EMPTY(thisChaseHintCamStruct.sDisplayedHintHelp)
				SET_TEXT_SCALE(0.5,0.5)
				SET_TEXT_COLOUR(R,G,B,255)
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_2_y_offset,"STRING", tHintCamHelp)
			ENDIF
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_3_y_offset,"STRING",tHintTypeInfo)
			
		ELSE
			INT R = 255, G = 255, B = 000		//yellow
			DRAW_RECT(0.5,0.5, 0.1,0.1, R,G,B,064)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			
			TEXT_LABEL_63 str = "HINT on, interping: "
			
			IF thisChaseHintCamStruct.bHintInterpingBack
				str += " back "
			ENDIF
			IF thisChaseHintCamStruct.bHintInterpingIn
				str += " in "
			ENDIF
			
			INT iInterpInTime = ABSI((thisChaseHintCamStruct.iInterpInTimer + thisChaseHintCamStruct.iInterpInLength) - GET_GAME_TIMER())
			IF (iInterpInTime < 1000)	str += "0" ENDIF
			IF (iInterpInTime < 100)	str += "0" ENDIF
			IF (iInterpInTime < 10)		str += "0" ENDIF
			str += iInterpInTime
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_0_y_offset,"STRING",str)
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_1_y_offset,"STRING", tHintCamInfo)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(thisChaseHintCamStruct.sDisplayedHintHelp)
				SET_TEXT_SCALE(0.5,0.5)
				SET_TEXT_COLOUR(R,G,B,255)
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_2_y_offset,"STRING", tHintCamHelp)
			ENDIF
			
			SET_TEXT_SCALE(0.5,0.5)
			SET_TEXT_COLOUR(R,G,B,255)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5,fTEXT_3_y_offset,"STRING",tHintTypeInfo)
			
		ENDIF
	ENDIF
	
ENDPROC

#ENDIF

// *******************************************************************************************
//	ENTITY CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************
/// PURPOSE: Returns toggle state of hint cam 
///    
/// PARAMS:
///    thisChaseHintCamStruct - local hint cam struct
///    bCheck_player_control - Set's if it should return false when player control is disabled.
/// RETURNS:
///    
FUNC BOOL SHOULD_CONTROL_CHASE_HINT_CAM(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct,bool bCheck_player_control = true,bool bCheck_currentCam = true, bool bSpecialCamera = false)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] SHOULD_CONTROL_CHASE_HINT_CAM - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(bCheck_player_control))
	ENDIF
	#ENDIF
	
//	#IF FEATURE_SMUGGLER
//	IF g_bBombuskaExitCamTimerOn = TRUE
//		PRINTLN(GET_THIS_SCRIPT_NAME(), ":SHOULD_CONTROL_CHASE_HINT_CAM: g_bBombuskaExitCamTimerOn - false")	
//		RETURN FALSE
//	ENDIF
//	#ENDIF
	
	// check to see if the hint cam has finished interping in 
	IF thisChaseHintCamStruct.bHintInterpingIn //AND IS_GAMEPLAY_HINT_ACTIVE()
		IF GET_GAME_TIMER() >= thisChaseHintCamStruct.iInterpInTimer + thisChaseHintCamStruct.iInterpInLength
			thisChaseHintCamStruct.bHintInterpingIn = FALSE
		ENDIF
	ENDIF
	
	switch thisChaseHintCamStruct.eHintCamMethod
//============================= SET HINT OFF/ON ===============================	
		case hint_OFF
			thisChaseHintCamStruct.bHint_toggleOn = false
			
			if 	 thisChaseHintCamStruct.ehType = hType_ANY			
				//In a vehicle
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
					IF PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
						thisChaseHintCamStruct.eHintCamMethod 	= hint_CHECK
						thisChaseHintCamStruct.bHint_toggleOn 	= true
					endif
				//On foot		
				else
					IF PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
						thisChaseHintCamStruct.eHintCamMethod 	= hint_CHECK
						thisChaseHintCamStruct.bHint_toggleOn 	= true
					endif
				endif			
			elif thisChaseHintCamStruct.ehType = hType_ON_FOOT
				IF PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
					thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
					thisChaseHintCamStruct.eHintCamMethod 	= hint_CHECK
					thisChaseHintCamStruct.bHint_toggleOn 	= true
				endif
			elif thisChaseHintCamStruct.ehType = hTYPE_VEH
				IF PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
					thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
					thisChaseHintCamStruct.eHintCamMethod 	= hint_CHECK
					thisChaseHintCamStruct.bHint_toggleOn 	= true
				endif
			endif								
			//forced
			if PRIVATE_chaseHintCamStillSet(thisChaseHintCamStruct)
				thisChaseHintCamStruct.bHint_toggleOn 	= true
				thisChaseHintCamStruct.eHintCamMethod = hint_FORCED
			endif
		break	
//=========================== CHECK TOGGLE/HELD ===============================		
		case hint_CHECK // check			
	// button was tapped toggle
			if get_game_timer() - thisChaseHintCamStruct.iInputCheckTimer <= 500 //toggle
				if 	 thisChaseHintCamStruct.ehType = hType_ANY	
					//In a vehicle			
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
						IF not PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
							thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
							thisChaseHintCamStruct.eHintCamMethod 	= hint_TOGGLE
						endif
					//On foot	
					else
						IF not PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
							thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
							thisChaseHintCamStruct.eHintCamMethod 	= hint_TOGGLE
						endif
					endif
				elif  thisChaseHintCamStruct.ehType = hType_ON_FOOT	
					IF not PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
						thisChaseHintCamStruct.eHintCamMethod 	= hint_TOGGLE
					endif
				elif  thisChaseHintCamStruct.ehType = hTYPE_VEH
					IF not PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.iInputCheckTimer = get_game_timer()
						thisChaseHintCamStruct.eHintCamMethod 	= hint_TOGGLE
					endif
				endif
			else				
	//held down check if no longer held
				thisChaseHintCamStruct.eHintCamMethod = hint_HELD
			endif				
		break	
//=========================== HELD ===============================			
		case hint_HELD
			
			if thisChaseHintCamStruct.ehType = hType_ANY	
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
					IF not PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.eHintCamMethod = hint_OFF
					endif
				else
					IF not PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.eHintCamMethod = hint_OFF
					endif
				endif
				
			elif thisChaseHintCamStruct.ehType = hType_ON_FOOT
				IF not PRIVATE_IsChaseHintCamButtonPressedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
				or IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
					thisChaseHintCamStruct.eHintCamMethod = hint_OFF
				endif
				
			elif thisChaseHintCamStruct.ehType = hTYPE_VEH
				IF not IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
				or GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)	
					thisChaseHintCamStruct.eHintCamMethod = hint_OFF
				else
					IF not PRIVATE_IsChaseHintCamButtonPressedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
						thisChaseHintCamStruct.eHintCamMethod = hint_OFF
					endif
				endif
			endif
		break	
//=========================== TOGGLE ===============================		
		case hint_TOGGLE
			if get_game_timer() - thisChaseHintCamStruct.iInputCheckTimer > 500 
				if thisChaseHintCamStruct.ehType = hType_ANY	
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
						if PRIVATE_IsChaseHintCamButtonJustReleasedInVehicle(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
							thisChaseHintCamStruct.eHintCamMethod = hint_OFF
						endif
					else
						if PRIVATE_IsChaseHintCamButtonJustReleasedOnFoot(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
							thisChaseHintCamStruct.eHintCamMethod = hint_OFF
						endif
					endif				
				elif thisChaseHintCamStruct.ehType = hType_ON_FOOT
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
					or PRIVATE_IsChaseHintCamButtonJustReleasedOnFoot(bCheck_player_control,bCheck_currentCam, bSpecialCamera)
						thisChaseHintCamStruct.eHintCamMethod = hint_OFF
					endif
				elif thisChaseHintCamStruct.ehType = hType_veh	
					IF not IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
					or GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)	
						thisChaseHintCamStruct.eHintCamMethod = hint_OFF
					else
						IF PRIVATE_IsChaseHintCamButtonJustReleasedInVehicle(bCheck_player_control,bCheck_currentCam, bSpecialCamera)
							thisChaseHintCamStruct.eHintCamMethod = hint_OFF
						endif
					endif
				endif
			endif
		break
//=========================== FORCED ===============================		
		case hint_FORCED
			if not PRIVATE_chaseHintCamStillSet(thisChaseHintCamStruct)
				thisChaseHintCamStruct.eHintCamMethod = hint_OFF
			endif
		break
	endswitch

	IF not SAFE_TO_PRINT_CHASE_HINT_CAM_HELP(bCheck_player_control,bCheck_currentCam,bSpecialCamera)
		thisChaseHintCamStruct.eHintCamMethod = hint_OFF
		thisChaseHintCamStruct.bHint_toggleOn = false
	ENDIF	
		
	IF thisChaseHintCamStruct.bHint_toggleOn
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		RETURN TRUE
	else		
		RETURN FALSE		
	endif
		
RETURN FALSE	
ENDFUNC

// *******************************************************************************************
//	ENTITY CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************

PROC CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, VECTOR vHint_offset, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(bCheckPlayerControl))
	ENDIF
	#ENDIF
	
	// check to see if the hint cam has finished interping in 
	IF thisChaseHintCamStruct.bHintInterpingIn AND IS_GAMEPLAY_HINT_ACTIVE()
		IF GET_GAME_TIMER() >= thisChaseHintCamStruct.iInterpInTimer + thisChaseHintCamStruct.iInterpInLength
			thisChaseHintCamStruct.bHintInterpingIn = FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_RenderChaseHintCam(thisChaseHintCamStruct)
	#ENDIF
	
	STRING sThisHintHelp = sHintHelp
	IF IS_STRING_NULL(sThisHintHelp)
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			sThisHintHelp = "CMN_HINT"
		ELSE
			sThisHintHelp = "FM_IHELP_HNT"
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	ENDIF
	
	if PRIVATE_isEntityAlive(chaseEntity)
	and IS_ENTITY_VISIBLE(chaseEntity)//check for MP if the entity is invisible then break out
		bool bVisible = false
		
		if IS_ENTITY_A_PED(chaseEntity)
			REQUEST_PED_VISIBILITY_TRACKING(GET_PED_INDEX_FROM_ENTITY_INDEX(chaseEntity))
			REQUEST_PED_VEHICLE_VISIBILITY_TRACKING(GET_PED_INDEX_FROM_ENTITY_INDEX(chaseEntity),true)
			if IS_TRACKED_PED_VISIBLE(GET_PED_INDEX_FROM_ENTITY_INDEX(chaseEntity))
				bVisible = true
			endif
		elif IS_ENTITY_A_VEHICLE(chaseEntity)
			TRACK_VEHICLE_VISIBILITY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(chaseEntity))
			if IS_VEHICLE_VISIBLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(chaseEntity))
				bVisible = true
			endif
		elif IS_ENTITY_AN_OBJECT(chaseEntity)
			TRACK_OBJECT_VISIBILITY(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(chaseEntity))
			if IS_OBJECT_VISIBLE(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(chaseEntity))
				bVisible = true
			endif
		endif
		
		
					
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()		
			IF SHOULD_CONTROL_CHASE_HINT_CAM(thisChaseHintCamStruct, bCheckPlayerControl, bCheck_currentCam )
				PRIVATE_StartEntityChaseHintCamWithOffset(thisChaseHintCamStruct, chaseEntity, vHint_offset, Type)
			ENDIF
			
			IF thisChaseHintCamStruct.bHintInterpingBack
				thisChaseHintCamStruct.bHintInterpingBack = FALSE
			ELSE
				if thisChaseHintCamStruct.ehType = hTYPE_VEH
					IF ShouldDisplayChaseHelpText(sThisHintHelp)
						IF IS_STRING_NULL(thisChaseHintCamStruct.sDisplayedHintHelp)
						AND NOT IS_STRING_NULL(sThisHintHelp)
						and IS_PED_IN_ANY_VEHICLE(player_ped_id())
							IF bVisible
							and not IS_HELP_MESSAGE_BEING_DISPLAYED()
							and bShowHint
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
								 	PRINT_HELP(sThisHintHelp)
									thisChaseHintCamStruct.sDisplayedHintHelp = sThisHintHelp
									if ARE_STRINGS_EQUAL("CMN_HINT",sThisHintHelp)
										ChaseHelpTextDisplayed(TRUE)
									endif							
								ENDIF
							ENDIF
						ENDIF
					endif
				else
					IF ShouldDisplayChaseHelpText(sThisHintHelp)
						IF IS_STRING_NULL(thisChaseHintCamStruct.sDisplayedHintHelp)
						AND NOT IS_STRING_NULL(sThisHintHelp)
							
							IF IS_ENTITY_ON_SCREEN(chaseEntity)
							and bVisible
							and not IS_HELP_MESSAGE_BEING_DISPLAYED()
							and bShowHint
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
								 	PRINT_HELP(sThisHintHelp)
									thisChaseHintCamStruct.sDisplayedHintHelp = sThisHintHelp								
									if ARE_STRINGS_EQUAL("CMN_HINT",sThisHintHelp)
										ChaseHelpTextDisplayed(TRUE)
									endif
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				endif
			ENDIF
		ELSE
			IF NOT IS_STRING_NULL(sHintHelp)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHintHelp)
				 	CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
				IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_BOAT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_HELI is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_ON_BIKE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_CINEMATIC
				  OR GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET() - KILL_CHASE_HINT_CAM GET_FOLLOW_VEHICLE_CAM_VIEW_MODE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
				ENDIF				
			ENDIF
					
			
			IF NOT SHOULD_CONTROL_CHASE_HINT_CAM(thisChaseHintCamStruct,bCheckPlayerControl,bCheck_currentCam )
				IF NOT thisChaseHintCamStruct.bHintInterpingBack
				AND NOT thisChaseHintCamStruct.bHintInterpingIn
				AND NOT PRIVATE_chaseHintCamStillSet(thisChaseHintCamStruct)
					PRIVATE_StopChaseHintCam(thisChaseHintCamStruct)					
				ENDIF
			ENDIF
		ENDIF
	else
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp)
	endif
	
	
ENDPROC
PROC CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	thisChaseHintCamStruct.ehType = hType_ANY
	CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET(thisChaseHintCamStruct, chaseEntity, <<0,0,0>>, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// PURPOSE: Hintcams toward vehicles, Player does not need to be in a vehicle.
//          If he is in a vehicle it uses cinematic cam button, if not it uses the context button.
PROC CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT_WITH_OFFSET(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, VECTOR vHint_offset, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct)
	ENDIF
	thisChaseHintCamStruct.ehType = hType_ON_FOOT
	CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET(thisChaseHintCamStruct, chaseEntity, vHint_offset, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC
PROC CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	
	CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT_WITH_OFFSET(thisChaseHintCamStruct, chaseEntity, <<0,0,0>>, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// PURPOSE: Hintcams toward vehicles, Player does not need to be in a vehicle.
//          If he is in a vehicle it uses cinematic cam button, if not it uses the context button.
PROC CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, VECTOR vHint_offset, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct)
	ENDIF
	thisChaseHintCamStruct.ehType = hTYPE_VEH
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(bCheckPlayerControl))
	ENDIF
	#ENDIF
	
	CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS_WITH_OFFSET(thisChaseHintCamStruct, chaseEntity, vHint_offset, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC
PROC CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, ENTITY_INDEX chaseEntity, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FocusControlDebug")
		PRINTLN("[FOCUS_NIGHTMARE] CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE - SHOULD_CHECK_FOR_PLAYER_CONTROL_FOR_FOCUS_CAM = ", GET_STRING_FROM_BOOL(bCheckPlayerControl))
	ENDIF
	#ENDIF
	CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET(thisChaseHintCamStruct, chaseEntity, <<0,0,0>>, sHintHelp, Type,bCheckPlayerControl,bShowHint,bCheck_currentCam )
ENDPROC

// *******************************************************************************************
//	PED CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************

PROC CONTROL_PED_CHASE_HINT_CAM_ON_FOOT(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, PED_INDEX chasePed, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT(thisChaseHintCamStruct, chasePed, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

PROC CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, PED_INDEX chasePed, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(thisChaseHintCamStruct, chasePed, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

PROC CONTROL_PED_CHASE_HINT_CAM_ANY_MEANS(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, PED_INDEX chasePed, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS(thisChaseHintCamStruct, chasePed, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// *******************************************************************************************
//	VEHICLE CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************
PROC CONTROL_VEHICLE_CHASE_HINT_CAM_ON_FOOT_WITH_OFFSET(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle, VECTOR vHint_offset, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_ON_FOOT_WITH_OFFSET(thisChaseHintCamStruct, chaseVehicle, vHint_offset, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC
PROC CONTROL_VEHICLE_CHASE_HINT_CAM_ON_FOOT(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_VEHICLE_CHASE_HINT_CAM_ON_FOOT_WITH_OFFSET(thisChaseHintCamStruct, chaseVehicle, <<0,0,0>>, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

PROC CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle, VECTOR vHint_offset, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET(thisChaseHintCamStruct, chaseVehicle, vHint_offset, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC
PROC CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE_WITH_OFFSET(thisChaseHintCamStruct, chaseVehicle, <<0,0,0>>, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

PROC CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VEHICLE_INDEX chaseVehicle, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)
	CONTROL_ENTITY_CHASE_HINT_CAM_ANY_MEANS(thisChaseHintCamStruct, chaseVehicle, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// *******************************************************************************************
//	COORD CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************
PROC CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VECTOR vHintAt, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,bool bCheck_currentCam = true)

	// check to see if the hint cam has finished interping in 
	IF thisChaseHintCamStruct.bHintInterpingIn AND IS_GAMEPLAY_HINT_ACTIVE()
		IF GET_GAME_TIMER() >= thisChaseHintCamStruct.iInterpInTimer + thisChaseHintCamStruct.iInterpInLength
			thisChaseHintCamStruct.bHintInterpingIn = FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_RenderChaseHintCam(thisChaseHintCamStruct)
	#ENDIF
	
	STRING sThisHintHelp = sHintHelp
	
	IF IS_STRING_NULL(sThisHintHelp)
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			sThisHintHelp = "CMN_HINT"		
		ELSE
			sThisHintHelp = "FM_IHELP_HNT"
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	ENDIF
	
	IF NOT IS_GAMEPLAY_HINT_ACTIVE()
		
		IF SHOULD_CONTROL_CHASE_HINT_CAM(thisChaseHintCamStruct,bCheckPlayerControl, bCheck_currentCam )
			PRIVATE_StartCoordChaseHintCam(thisChaseHintCamStruct, vHintAt, Type)
		ENDIF
		
		IF thisChaseHintCamStruct.bHintInterpingBack
			thisChaseHintCamStruct.bHintInterpingBack = FALSE
		ELSE
			if thisChaseHintCamStruct.ehType = hTYPE_VEH
				IF ShouldDisplayChaseHelpText(sThisHintHelp)
					IF IS_STRING_NULL(thisChaseHintCamStruct.sDisplayedHintHelp)				
					AND NOT IS_STRING_NULL(sThisHintHelp)
					and IS_PED_IN_ANY_VEHICLE(player_ped_id())
						IF IS_SPHERE_VISIBLE(vHintAt,1)
						and not IS_HELP_MESSAGE_BEING_DISPLAYED()
						and bShowHint
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
							 	PRINT_HELP(sThisHintHelp)
								thisChaseHintCamStruct.sDisplayedHintHelp = sThisHintHelp
								if ARE_STRINGS_EQUAL("CMN_HINT",sThisHintHelp)
									ChaseHelpTextDisplayed(TRUE)
								endif							
							ENDIF
						ENDIF
					ENDIF
				endif
			else
				IF ShouldDisplayChaseHelpText(sThisHintHelp)
					IF IS_STRING_NULL(thisChaseHintCamStruct.sDisplayedHintHelp)
					AND NOT IS_STRING_NULL(sThisHintHelp)					
						IF IS_SPHERE_VISIBLE(vHintAt,1)
						and not IS_HELP_MESSAGE_BEING_DISPLAYED()
						and bShowHint
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
							 	PRINT_HELP(sThisHintHelp)
								thisChaseHintCamStruct.sDisplayedHintHelp = sThisHintHelp
								if ARE_STRINGS_EQUAL("CMN_HINT",sThisHintHelp)
									ChaseHelpTextDisplayed(TRUE)
								endif
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			endif
		ENDIF
	ELSE
		IF NOT IS_STRING_NULL(sThisHintHelp)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sThisHintHelp)
			and IS_HELP_MESSAGE_BEING_DISPLAYED()
			 	CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
				IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_BOAT) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_BOAT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_HELI) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_HELI is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_AIRCRAFT is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_IN_SUBMARINE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_CINEMATIC
					OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM CAM_VIEW_MODE_CONTEXT_ON_BIKE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
						KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
					ENDIF
				ELIF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_CINEMATIC
				  OR GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS() - KILL_CHASE_HINT_CAM GET_FOLLOW_VEHICLE_CAM_VIEW_MODE is CAM_VIEW_MODE_FIRST_PERSON OR CAM_VIEW_MODE_CINEMATIC")
					KILL_CHASE_HINT_CAM(thisChaseHintCamStruct,sThisHintHelp,true)
				ENDIF				
			ENDIF
		
		IF NOT SHOULD_CONTROL_CHASE_HINT_CAM(thisChaseHintCamStruct,bCheckPlayerControl, bCheck_currentCam )
			IF NOT thisChaseHintCamStruct.bHintInterpingBack
			AND NOT thisChaseHintCamStruct.bHintInterpingIn
			AND NOT PRIVATE_chaseHintCamStillSet(thisChaseHintCamStruct)
				PRIVATE_StopChaseHintCam(thisChaseHintCamStruct)				
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CONTROL_COORD_CHASE_HINT_CAM_ON_FOOT(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VECTOR vHintAt, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,BOOL  bCheck_currentCam = TRUE)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct)
	ENDIF
	thisChaseHintCamStruct.ehType = hType_ON_FOOT
	CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(thisChaseHintCamStruct, vHintAt, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// PURPOSE: Hintcams toward coords, Player does not need to be in a vehicle.
//          If he is in a vehicle it uses cinematic cam button, if not it uses the context button.
PROC CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VECTOR vHintAt, STRING sHintHelp = NULL, HINT_TYPE Type = HINTTYPE_DEFAULT,Bool bCheckPlayerControl = true, bool bShowHint = true,BOOL  bCheck_currentCam = TRUE)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),true)
		KILL_CHASE_HINT_CAM(thisChaseHintCamStruct)
	ENDIF
	thisChaseHintCamStruct.ehType = hTYPE_VEH
	CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(thisChaseHintCamStruct, vHintAt, sHintHelp, Type,bCheckPlayerControl,bShowHint, bCheck_currentCam )
ENDPROC

// *******************************************************************************************
//	RACE CHASE HINT CAM PROCEDURES AND FUNCTIONS
// *******************************************************************************************

// PURPOSE: Call this to kill an active hint came so it updates to a new target(for races).
//			Using this instead of KILL_CHASE_HINT_CAM as K_C_H_C causes a flash to the cinematic camera before 
//			setting itself to the new hint_cam coords during a race while the user holds the button down BUG: 210659
PROC KILL_RACE_HINT_CAM(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, STRING sHintHelp = NULL)
	KILL_CHASE_HINT_CAM(thisChaseHintCamStruct, sHintHelp)
ENDPROC

// PURPOSE: Call this every frame that you'd like the hintcam to be usable.
PROC CONTROL_RACE_HINT_CAM(CHASE_HINT_CAM_STRUCT &thisChaseHintCamStruct, VECTOR vHintAt)
	CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(thisChaseHintCamStruct, vHintAt)
ENDPROC
