
///public interface for family flow scripts
///    alwyn.roberts@rockstarnorth.com

USING "rage_builtins.sch"
USING "globals.sch"


// *******************************************************************************************
//	FAMILY CURRENT EVENT PUBLIC FUNCTIONS
// *******************************************************************************************
FUNC enumFamilyEvents Get_Current_Event_For_FamilyMember(enumFamilyMember eFamilyMember)
	RETURN g_eCurrentFamilyEvent[eFamilyMember]
ENDFUNC
PROC Set_Current_Event_For_FamilyMember(enumFamilyMember eFamilyMember, enumFamilyEvents eCurrentFamilyEvents)
	g_eCurrentFamilyEvent[eFamilyMember] = eCurrentFamilyEvents
ENDPROC

// *******************************************************************************************
//	FAMILY PREVIOUS EVENT PUBLIC FUNCTIONS
// *******************************************************************************************
FUNC enumFamilyEvents Get_Previous_Event_For_FamilyMember(enumFamilyMember eFamilyMember)
	RETURN g_savedGlobals.sFamilyData.ePreviousFamilyEvent[eFamilyMember]
ENDFUNC
PROC Update_Previous_Event_For_FamilyMember(enumFamilyMember eFamilyMember)
	
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
		CASE FE_ANY_find_family_event
		CASE FE_ANY_wander_family_event
		CASE MAX_FAMILY_EVENTS
		CASE FAMILY_MEMBER_BUSY
		CASE NO_FAMILY_EVENTS
			//
		BREAK
		
		DEFAULT
			g_savedGlobals.sFamilyData.ePreviousFamilyEvent[eFamilyMember] = g_eCurrentFamilyEvent[eFamilyMember]
		BREAK
	ENDSWITCH
ENDPROC

// *******************************************************************************************
//	FAMILY PUBLIC FLOW FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    
/// RETURNS:
///    
PROC UpdateMissionPassedFamilyEvents(SP_MISSIONS paramMissionID)
#if USE_CLF_DLC
paramMissionID = paramMissionID
#endif
#if USE_NRM_DLC
paramMissionID = paramMissionID
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	SWITCH paramMissionID
		CASE SP_MISSION_ARMENIAN_2
			Set_Current_Event_For_FamilyMember(FM_FRANKLIN_LAMAR,FAMILY_MEMBER_BUSY)
			Set_Current_Event_For_FamilyMember(FM_FRANKLIN_STRETCH,FAMILY_MEMBER_BUSY)
		BREAK
		CASE SP_MISSION_FAMILY_3	//#440403
			Set_Current_Event_For_FamilyMember(FM_MICHAEL_GARDENER,FAMILY_MEMBER_BUSY)
		BREAK
		CASE SP_MISSION_LAMAR		//#414659
			Set_Current_Event_For_FamilyMember(FM_FRANKLIN_LAMAR,FAMILY_MEMBER_BUSY)
			Set_Current_Event_For_FamilyMember(FM_FRANKLIN_STRETCH,FAMILY_MEMBER_BUSY)
		BREAK
		CASE SP_MISSION_TREVOR_2	//#414664
			Set_Current_Event_For_FamilyMember(FM_TREVOR_0_RON,FAMILY_MEMBER_BUSY)
		BREAK
	ENDSWITCH
#endif
#endif
ENDPROC

