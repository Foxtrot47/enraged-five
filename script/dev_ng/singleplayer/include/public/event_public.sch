//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	event_controller.sc											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Functions to tell if a specific event has occured this 		//
//							frame.														//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////


USING "rage_builtins.sch"
USING "globals.sch"


///////////////////////////////////////////////////////////////////////////////////////////
///    
///    VEHICLE DAMAGED EVENT
///    
/// PURPOSE: Returns TRUE if a vehicle damaged event has occured
FUNC BOOL Has_Vehicle_Been_Damaged()
	RETURN (g_sEventData.iVehicleDamagedEvents > 0)
ENDFUNC

/// PURPOSE: Returns the number of vehicle damaged events that have occured
FUNC INT Get_Number_Of_Vehicle_Damaged_Events()
	RETURN g_sEventData.iVehicleDamagedEvents
ENDFUNC

/// PURPOSE: Returns the ENTITY_INDEX of a damaged vehicle
///    NOTE: iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE
FUNC ENTITY_INDEX Get_Index_Of_Damaged_Vehicle(INT iEventSlot)
	#IF IS_DEBUG_BUILD
		IF (iEventSlot < 0 OR iEventSlot >= MAX_EVENTS_STORED_PER_TYPE)
			SCRIPT_ASSERT("Get_Index_Of_Damaged_Vehicle() iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE")
		ENDIF
	#ENDIF
	RETURN g_sEventData.eidVehicleDamaged[iEventSlot]
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////////
///    
///    VEHICLE DESTROYED EVENT
///    
/// PURPOSE: Returns TRUE if a vehicle destroyed event has occured
FUNC BOOL Has_Vehicle_Been_Destroyed()
	RETURN (g_sEventData.iVehicleDestroyedEvents > 0)
ENDFUNC

/// PURPOSE: Returns the number of vehicle destroyed events that have occured
FUNC INT Get_Number_Of_Vehicle_Destroyed_Events()
	RETURN g_sEventData.iVehicleDestroyedEvents
ENDFUNC

/// PURPOSE: Returns the ENTITY_INDEX of the last vehicle destroyed by the player
///    NOTE: iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE
FUNC ENTITY_INDEX Get_Index_Of_Destroyed_Vehicle(INT iEventSlot)
	#IF IS_DEBUG_BUILD
		IF (iEventSlot < 0 OR iEventSlot >= MAX_EVENTS_STORED_PER_TYPE)
			SCRIPT_ASSERT("Get_Index_Of_Destroyed_Vehicle() iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE")
		ENDIF
	#ENDIF
	RETURN g_sEventData.eidVehicleDestroyed[iEventSlot]
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////////
///    
///    PED INJURED EVENT
///    
/// PURPOSE: Returns TRUE if a ped injured event has occured
FUNC BOOL Has_Ped_Been_Injured()
	RETURN (g_sEventData.iPedInjuredEvents > 0)
ENDFUNC

/// PURPOSE: Returns the number of ped injured events that have occured
FUNC INT Get_Number_Of_Ped_Injured_Events()
	RETURN g_sEventData.iPedInjuredEvents
ENDFUNC

/// PURPOSE: Returns the ENTITY_INDEX of the last ped injured by the player
///    NOTE: iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE
FUNC ENTITY_INDEX Get_Index_Of_Injured_Ped(INT iEventSlot)
	#IF IS_DEBUG_BUILD
		IF (iEventSlot < 0 OR iEventSlot >= MAX_EVENTS_STORED_PER_TYPE)
			SCRIPT_ASSERT("Get_Index_Of_Injured_Ped() iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE")
		ENDIF
	#ENDIF
	RETURN g_sEventData.eidPedInjured[iEventSlot]
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////////
///    
///    PED KILLED EVENT
///    
/// PURPOSE: Returns TRUE if a ped killed event has occured
FUNC BOOL Has_Ped_Been_Killed()
	RETURN (g_sEventData.iPedKilledEvents > 0)
ENDFUNC

/// PURPOSE: Returns the number of ped killed events that have occured
FUNC INT Get_Number_Of_Ped_Killed_Events()
	RETURN g_sEventData.iPedKilledEvents
ENDFUNC

/// PURPOSE: Returns the ENTITY_INDEX of the last ped killed by the player
///    NOTE: iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE
FUNC ENTITY_INDEX Get_Index_Of_Killed_Ped(INT iEventSlot)
	#IF IS_DEBUG_BUILD
		IF (iEventSlot < 0 OR iEventSlot >= MAX_EVENTS_STORED_PER_TYPE)
			SCRIPT_ASSERT("Get_Index_Of_Killed_Ped() iEventSlot must be >= 0 and < MAX_EVENTS_STORED_PER_TYPE")
		ENDIF
	#ENDIF
	RETURN g_sEventData.eidPedKilled[iEventSlot]
ENDFUNC

