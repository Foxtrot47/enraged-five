
/////////
///    Functions and datatypes concerning the display of the end screen described at
///        https://devstar.rockstargames.com/wiki/index.php/MISSION_BOX
///    
///    
///    
///    
///    
///    Ak
//////////

  
USING "rage_builtins.sch"
USING "globals.sch"
USING "menu_public.sch"
USING "shared_hud_displays.sch"
USING "screenDisplayState.sch"

//	ESC = End Screen Constant

//##########	POSITIONING VARS	############
CONST_FLOAT ESC_LINE_Y_ADVANCE 				25.0	//Advance distance between stat entries? 0.0347 = 25 pixel advance
CONST_FLOAT ESC_LINE_Y_GAP 					2.0		//Border thickness 0.00278 = 2 pixel advance
CONST_FLOAT ESC_LINE_Y_ADVANCE_HEADER 		30.0	//Vertical length of the header/footer for stats? 0.04722222192 = 34 pixel advance
CONST_FLOAT ESC_1ST_LINE_UP					2.0		//How much the 1st white line moves up
CONST_FLOAT ESC_1ST_ELEM_GAP				0.3		//How much the 1st element is below the line
CONST_FLOAT ESC_1ST_ELEM_EXTRA_GAP			0.2		//Extra spacing if there are elements in the list
CONST_FLOAT ESC_COMPLETION_GAP				0.2		//Spacing before the completion box
CONST_FLOAT ESC_HALF_WIDTH 					0.1125	//Half the width of the stats screen
CONST_FLOAT ESC_INDENT						0.006	//Left/right screen indent?
CONST_FLOAT ESC_Y_POSITION 					0.3		//2725 //Distance between header and stats, should be about 0.2625 after scaling
CONST_FLOAT ESC_X_PIXEL 					0.00078125//0.00052083333 //
CONST_FLOAT ESC_Y_PIXEL 					0.00138888888//0.00092592592 //	
CONST_FLOAT ESC_Y_PIXEL_LINE 				0.00092592592	
CONST_FLOAT ESC_SOCIAL_BULLSHIT_TAB_Y 		0.138888888//0.092592592 //

CONST_INT MEDAL_PIX_SIDE 16

//##########	TIMING VARS	############
CONST_INT	ESC_MOVE_UP_TIME 600
CONST_INT 	ESC_BLEND_TIME 1500
CONST_INT	ESC_DEFAULT_TIME	15000		//Default time until animating out
CONST_INT	ESC_ROLL_UP_GRACE 333		//Time to roll-up stats before it stops rendering
CONST_INT	ESC_MOVE_UP_GRACE 666		//Time after start when it begins moving the text up


//##########	ASPECT RATIO VARS	############
const_float AR4_3 1.3333333
const_float AR16_9 1.7777777
const_float AR16_10 1.6

//##################################	FUNCTIONS	###########################################

//Gets a modifier float based on the screen aspect ratio
FUNC FLOAT GET_ASPECT_RATIO_MODIFIER()
	FLOAT fMod = 1.0
	IF IS_PC_VERSION()
//		IF ABSF(GET_ASPECT_RATIO(FALSE) - AR16_10) <0.1 //16:10, most UI errors
//			RETURN 1.04
//		ELIF ABSF(GET_ASPECT_RATIO(FALSE) - AR4_3) <0.1
//			RETURN 1.10
//		ELSE
//			RETURN 1.0
//		ENDIF

		//At 16:9 (1.777) things look normal widescreen (low vertical spacing)
		//At 4:3 (1.333) things should have bigger vertical spacing
		//Therefore, Increase vertical spacing smoothly between 
		//1.0   - 1.03 for ARs of 
		//1.777 - 1.333  => fmod = 1.0 + (1.777-AR)/0.444 *.03
		//fMod -= (AR16_9 - GET_ASPECT_RATIO(FALSE)) / (AR16_9-AR4_3) * 0.03
	ENDIF

//	if GET_FRAME_COUNT() % 5 = 0
//		CPRINTLN(debug_mission_stats,"AR multiplier ",fMod)
//	ENDIF
	RETURN fMod
ENDFUNC	

FUNC FLOAT PIXEL_X(FLOAT PIXELSIZE)
	RETURN PIXELSIZE * ESC_X_PIXEL
ENDFUNC

FUNC FLOAT PIXEL_Y(FLOAT PIXELSIZE)	
	RETURN PIXELSIZE * ESC_Y_PIXEL //* GET_ASPECT_RATIO_MODIFIER()
ENDFUNC

FUNC FLOAT PIXEL_Y_LINE(FLOAT PIXELSIZE)	
	RETURN PIXELSIZE * ESC_Y_PIXEL_LINE //* GET_ASPECT_RATIO_MODIFIER()
ENDFUNC

FUNC FLOAT Get_PT_scale(FLOAT PTSIZE)
	RETURN PTSIZE * 0.025// * GET_ASPECT_RATIO_MODIFIER()
ENDFUNC


PROC END_SCREEN_WORK_OUT_WIDTHS(END_SCREEN_DATASET &esd)

	//SET_TEXT_WRAP(fontLeft,fontRight)
	
	FLOAT width = 0.0
	
	/*
	INT iTitleSubelements
	END_SCREEN_TITLE_SUB_ELEMENT_TYPE titleSubElementType[MAX_END_SCREEN_TITLE_SUB_ELEMENTS]
	TEXT_LABEL_63 titleSubstrings[MAX_END_SCREEN_TITLE_SUBSTRINGS]
	INT iTitleSubInts[MAX_END_SCREEN_TITLE_SUBINTEGERS]
	*/
	
	
	SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
	SET_TEXT_SCALE(1.0, Get_PT_scale(16))
	
	IF esd.iTitleSubelements = 0
		IF  esd.titleIsUserName
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")	
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.title)
			width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
		ELSE
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(esd.title)
			width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
		ENDIF
	ELSE
		BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")	
		INT iIntCount = 0
		INT iStrCount = 0
		INT i = 0
		REPEAT esd.iTitleSubelements i
			SWITCH esd.titleSubElementType[i]
				CASE ESTSET_INT
					ADD_TEXT_COMPONENT_INTEGER(esd.iTitleSubInts[iIntCount])
					++iIntCount
					BREAK
				CASE ESTSET_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(esd.titleSubstrings[iStrCount] )
					++iStrCount
					BREAK
				CASE ESTSET_LITERAL_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.titleSubstrings[iStrCount])
					++iStrCount
					BREAK
			ENDSWITCH
		ENDREPEAT
		width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
	ENDIF
	
	
	IF width > (ESC_HALF_WIDTH*2 - ESC_INDENT*2)
		esd.fTrueHalfWidth = (width/2.0) + ESC_INDENT*2
	ENDIF
	
	
	//check the width of all the elements
	//check the width of the completion line
	
	
ENDPROC


PROC PRIVATE_ENDSCREEN_DRAW_ORDER()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	ENDIF
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)

ENDPROC

PROC PRIVATE_RENDER_ENDSCREEN_LABEL(STRING tl, FLOAT x, FLOAT y,BOOL c = FALSE, BOOL isPlayerName = FALSE, TEXT_FONTS TextFont = FONT_STANDARD)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(TextFont)
	PRIVATE_ENDSCREEN_DRAW_ORDER()

	IF isPlayerName
		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	ELSE
		BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl)
	ENDIF
	//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC

PROC PRIVATE_RENDER_ENDSCREEN_LABEL_WITH_LITERAL_STRING(STRING tl,STRING tlSubString, FLOAT x, FLOAT y,BOOL c = FALSE, TEXT_FONTS TextFont = FONT_STANDARD)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(TextFont)
	PRIVATE_ENDSCREEN_DRAW_ORDER()

	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlSubString)

	//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC

PROC PRIVATE_RENDER_ENDSCREEN_MISSION_LABEL(STRING tl, FLOAT x, FLOAT y,TEXT_FONTS TextFont = FONT_STANDARD)
	SET_TEXT_CENTRE(TRUE)
	SET_TEXT_FONT(TextFont)
	PRIVATE_ENDSCREEN_DRAW_ORDER()
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC

PROC PRIVATE_RENDER_ENDSCREEN_VALUE(INT valueA, INT valueB, 
									FLOAT x, FLOAT y, 
									STRING base,
									END_SCREEN_ELEMENT_FORMATTING format)
												
	HUD_COLOURS hucu = HUD_COLOUR_WHITE
	
	//set the string value if needed
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_ENDSCREEN_DRAW_ORDER()
	
	//add any needed sub values
	
	
	//do backing effects 
	FLOAT width = 0.0
	FLOAT xw = 8*	ESC_X_PIXEL
	FLOAT yh = 16*	ESC_Y_PIXEL
	INT r = 93
	INT g = 182
	INT b = 229
	
	IF format = ESEF_DOLLAR_VALUE_REDBACK
		r = 194
		g = 80
		b = 80
	ENDIF
	
	SWITCH format
		//CASE ESEF_DOLLAR_VALUE
		CASE ESEF_DOLLAR_VALUE_REDBACK  
		CASE ESEF_DOLLAR_VALUE_BLUEBACK
			SET_TEXT_SCALE(1.0, Get_PT_scale(18))//CUSTOM_MENU_TEXT_SCALE_Y*1.2857)
			SET_TEXT_FONT(FONT_CONDENSED)
			IF valueA < 0
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ESMINDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(-1*valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
				width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(FALSE)
			ELSE
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ESDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
				width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(FALSE)
			ENDIF
		
			width -= (width % ESC_X_PIXEL) //trim the sub pixel amount
			
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Left",
			x-(width), //+ (ESC_X_PIXEL*4),
			y+yh*0.6 + ESC_Y_PIXEL*2,
			xw,yh,
			0.0,
			r,g,b,255)
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Centre",
			x-width*0.5 - (ESC_X_PIXEL*2),
			y+yh*0.6 + ESC_Y_PIXEL*2,
			width - xw*0.5,yh,
			0.0,
			r,g,b,255)
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Right",
			x- (ESC_X_PIXEL*4),
			y+yh*0.6+ ESC_Y_PIXEL*2,
			xw,yh,
			0.0,
			r,g,b,255)		
			
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		BREAK	
	ENDSWITCH
	
	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
	//start the string
	SWITCH format 
		CASE ESEF_RAW_PERCENT
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("PERCENTAGE")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			
			BREAK
		CASE ESEF_RAW_INTEGER
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_NUM")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			
			BREAK
		CASE ESEF_FRACTION
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_TWO_NUM")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		
		CASE ESEF_DOLLAR_VALUE_REDBACK
		CASE ESEF_DOLLAR_VALUE_BLUEBACK
			SET_TEXT_SCALE(1.0, Get_PT_scale(18))//CUSTOM_MENU_TEXT_SCALE_Y*1.2857)
		FALLTHRU
		CASE ESEF_DOLLAR_VALUE
			
			IF valueA < 0
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ESMINDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(-1*valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
			ELSE
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ESDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
			ENDIF
			
			BREAK
		CASE ESEF_RAW_STRING
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			BREAK
		CASE ESEF_RAW_USERNAME
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(base)
			BREAK
		CASE ESEF_TIME_H_M_S
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_HOURS | TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)
			BREAK
		CASE ESEF_TIME_M_S
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS )
			BREAK
		
		CASE ESEF_TIME_M_S_MS
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS | TIME_FORMAT_MILLISECONDS | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			BREAK			
		CASE ESEF_TIME_M_S_MS_WITH_PERIOD //1506472
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS | TIME_FORMAT_MILLISECONDS | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			BREAK
		CASE ESEF_DISTANCE_VALUE_METERS
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("AHD_DIST")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			BREAK
		CASE ESEF_SINGLE_NUMBER_SUB
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		CASE ESEF_DOUBLE_NUMBER_SUB_LEFT
		CASE ESEF_DOUBLE_NUMBER_SUB
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		CASE ESEF_LEFT_AND_RIGHT_SUBSTRINGS
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		CASE ESEF_TWO_STRINGS
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
		BREAK
		//CASE ESEF_LEFT_SIDE_INT_SUBSTRING //this only applys a substring to name
	ENDSWITCH
	
	IF format != ESEF_LEFT_SIDE_INT_SUBSTRING
		IF format = ESEF_DOLLAR_VALUE_REDBACK
		OR format = ESEF_DOLLAR_VALUE_BLUEBACK
			END_TEXT_COMMAND_DISPLAY_TEXT(x- ESC_X_PIXEL*4, y)
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		ELSE
			END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
		ENDIF
	ENDIF

ENDPROC

PROC PRIVATE_RENDER_ENSCREEN_ELEMENT(END_SCREEN_DATASET &esd, INT iNth, FLOAT yat, FLOAT fontLeft,FLOAT fontRight,INT EndScreenAlpha)
	
	//END_SCREEN_ELEMENT_FORMATTING 
	//TEXT_LABEL ElementName[iNth]
	//TEXT_LABEL ElementText[iNth]
	//INT ElementValA[iNth]
	//INT ElementValB[iNth]
	//END_SCREEN_CHECK_MARK_STATUS ElementCheck[iNth]
	//BOOL ElementInvalidation[iNth]
		//IF esd.ElementInvalidation[iNth]
		//	SET_TEXT_COLOUR(10,10,10,255)
		//ELSE
		INT txt_R,txt_G,txt_B,txt_A
		GET_HUD_COLOUR(HUD_COLOUR_WHITE,txt_R,txt_G,txt_B,txt_A)
		SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
		//ENDIF
		
		
		
		SET_TEXT_WRAP(fontLeft,fontRight)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		//render the name
		SET_TEXT_CENTRE(FALSE)
		SET_TEXT_FONT(FONT_STANDARD)
		PRIVATE_ENDSCREEN_DRAW_ORDER()
		
		
		
		IF esd.ElementIsPlayerName[iNth]
		BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.ElementName[iNth])
		ELSE
		
		
		BEGIN_TEXT_COMMAND_DISPLAY_TEXT(esd.ElementName[iNth])
			IF esd.ElementFormat[iNth] = ESEF_LEFT_AND_RIGHT_SUBSTRINGS
			OR esd.ElementFormat[iNth] = ESEF_LEFT_SIDE_INT_SUBSTRING
				ADD_TEXT_COMPONENT_INTEGER(esd.ElementValA[iNth])
			ENDIF
		ENDIF
		//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
		//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
		END_TEXT_COMMAND_DISPLAY_TEXT(fontleft, yat)


		


		FLOAT TRUEright = fontRight
		FLOAT ficonw
		FLOAT ficonh 
		//FLOAT ficonwh 
		//FLOAT ficonhh 
		
		
		
		SWITCH esd.ElementCheck[iNth]
			CASE ESCM_NO_MARK
				BREAK
			CASE ESCM_UNCHECKED
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_EMPTY,
					FALSE,TRUE,
					ficonw,ficonh)
				
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
					
				//render the unchecked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_EMPTY,FALSE), 
				(fontRight)- ESC_INDENT, yat +PIXEL_Y(ESC_LINE_Y_GAP)+ (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, EndScreenAlpha)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
			CASE ESCM_CHECKED
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_TICK,
					FALSE,TRUE,
					ficonw,ficonh)
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
				//render the checked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_TICK,FALSE), 
				(fontRight)- ESC_INDENT , yat +PIXEL_Y(ESC_LINE_Y_GAP)+ (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, EndScreenAlpha)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
			CASE ESCM_INVALIDATED
			
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_CROSS,
					FALSE,TRUE,
					ficonw,ficonh)
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
				//render the checked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_CROSS,FALSE), 
				(fontRight)- ESC_INDENT , yat +PIXEL_Y(ESC_LINE_Y_GAP) + (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, EndScreenAlpha)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
				
		ENDSWITCH
		
		
		IF esd.ElementFormat[iNth] = ESEF_NAME_ONLY
			EXIT
		ENDIF
		//render the value
		
		
		IF esd.ElementFormat[iNth] = ESEF_DOUBLE_NUMBER_SUB_LEFT
			SET_TEXT_JUSTIFICATION(FONT_LEFT)
		ELSE
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
		ENDIF
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		
		IF esd.ElementFormat[iNth] = ESEF_DOLLAR_VALUE_BLUEBACK
		OR esd.ElementFormat[iNth] = ESEF_DOLLAR_VALUE_REDBACK
			SET_TEXT_WRAP(fontLeft,TRUEright - ESC_X_PIXEL*3)
		ELSE
			SET_TEXT_WRAP(fontLeft,TRUEright + ESC_X_PIXEL*2)
		ENDIF
		SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
	
	
		PRIVATE_RENDER_ENDSCREEN_VALUE(
		esd.ElementValA[iNth], esd.ElementValB[iNth], 
		fontRight, yat, 
		esd.ElementText[iNth],
		esd.ElementFormat[iNth])
		//esd.ElementInvalidation[iNth])

		//CPRINTLN(DEBUG_MISSION_STATS, "PRIVATE_RENDER_ENSCREEN_ELEMENT: <eleminvalidation> invalidation for ", iNth, " is ", esd.ElementInvalidation[iNth])
		
	
ENDPROC


PROC FORCE_ENDSCREEN_ANIM_OUT(END_SCREEN_DATASET &esd)
	esd.bShowSkipperPrompt = FALSE
	esd.bHoldOnEnd = FALSE
	esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
ENDPROC

FUNC FLOAT GET_RENDERED_TEXT_WIDTH(STRING str)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(str)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE) / 2
ENDFUNC

FUNC FLOAT GET_RENDERED_TEXT_WIDTH_LITERAL_STRING(STRING str,STRING str2)
	BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(str)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(str2)
	RETURN END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE) / 2
ENDFUNC

//FUNC CONTROL_ACTION GET_RENDER_ENDSCREEN_ACCEPT_BUTTON()
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		RETURN INPUT_CONTEXT
//	ENDIF
//	
//	RETURN INPUT_FRONTEND_ACCEPT
//ENDFUNC
//
//FUNC CONTROL_ACTION GET_RENDER_ENDSCREEN_EXPAND_BUTTON()
//	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//		RETURN INPUT_JUMP
//	ENDIF
//	
//	RETURN INPUT_FRONTEND_X
//ENDFUNC

PROC ENDSCREEN_ADD_SKIP_BUTTONS(END_SCREEN_DATASET &esd, BOOL bAddExpandButton)

	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "CLEAR_ALL")
	END_SCALEFORM_MOVIE_METHOD()
	
#IF NOT USE_TU_CHANGES
	//Removing this fixes #1813267.
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_CLEAR_SPACE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
#ENDIF

	// Turn on PC clickable instructional buttons
	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "TOGGLE_MOUSE_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

	//Add accept button
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP")
		
		// Clickable
		IF IS_PC_VERSION()
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(INPUT_FRONTEND_ENDSCREEN_ACCEPT))
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	//Add expand button (if required)
	IF bAddExpandButton
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_XPAND")
			
			// Clickable
			IF IS_PC_VERSION()
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(INPUT_FRONTEND_ENDSCREEN_EXPAND))
			ENDIF
			
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
		
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "DRAW_INSTRUCTIONAL_BUTTONS")
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

PROC ENDSCREEN_ADD_VOTE_BUTTONS(END_SCREEN_DATASET &esd)

	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "CLEAR_ALL")
	END_SCALEFORM_MOVIE_METHOD()
	
#IF NOT USE_TU_CHANGES
	//Removing this fixes #1813267.
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_CLEAR_SPACE")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
#ENDIF

	// Turn on PC clickable instructional buttons
	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "TOGGLE_MOUSE_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

	//up
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_TU")
	END_SCALEFORM_MOVIE_METHOD()
	//down 
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_TD")
	END_SCALEFORM_MOVIE_METHOD()
	
	//cancel
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_AB")
	END_SCALEFORM_MOVIE_METHOD()
			
	BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "DRAW_INSTRUCTIONAL_BUTTONS")
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

/// PURPOSE:
//assumes that the scaleform is already loaded
//call at your own risk    
/// PARAMS:
///    esd - 
///    
///    
PROC RAW_ENDSCREEN_DRAW(END_SCREEN_DATASET &esd, FLOAT skipperMult = 1.0, BOOL shortShard = FALSE)
	
	Int EndScreenAlpha 	= round(esd.fFadeOutMult * 255)
	
	FLOAT yat = GET_ASPECT_RATIO_MODIFIER()*0.25//(ESC_Y_POSITION - 0.15)
	
//	CPRINTLN(debug_luke,"")	
//	CPRINTLN(debug_luke,"Started drawing at ",yat," mult:",esd.fScrollOutMult)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(esd.splash)	
		IF esd.fTitleUpMult >= 0.0//10
			IF !esd.bStartedFailformAnim
//				CPRINTLN(debug_luke,"Set-up 1st stage screen")				
				BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "SHOW_MISSION_PASSED_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(esd.passed_splash_message) 	//BIG_TEXT "Mission Passed"
					
					IF esd.CompletionType = ESC_MULTI_COMPLETION
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(esd.title)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(esd.title) 					//MESSAGE_TEXT optional, not used
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(150) 							//alpha of shard (MP)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100) 							//alpha of shard
					ENDIF
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) 						//unknown needs to be here...
					
					IF esd.CompletionType = ESC_MULTI_COMPLETION
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(esd.iElements - 1) 			//NUMBER OF STATS LINES (FOR SETTING BACKGROUND HEIGHT) 
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(esd.iElements) 				//NUMBER OF STATS LINES (FOR SETTING BACKGROUND HEIGHT) 
					ENDIF
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(shortShard)		 			//For when there is no medal text and 2nd white line
					
					IF esd.CompletionType = ESC_MULTI_COMPLETION
					OR esd.CompletionType = ESC_STRING_COMPLETION
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_BLACK))//Manually setting the shard colour
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_MENU_HIGHLIGHT))//Manually setting the shard colour
					ENDIF
				END_SCALEFORM_MOVIE_METHOD()	
				esd.bStartedFailformAnim = TRUE
			ENDIF
			
			//Call Transition up to move the message to its original position ONLY if pass screen has elements
			IF esd.iElements > 0 AND !esd.bStartedMoveUp AND esd.IGameTimerReplacement > ESC_MOVE_UP_TIME
				BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "TRANSITION_UP")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.15)						//Transition timing
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)						   //Prevent auto-expansion 
				END_SCALEFORM_MOVIE_METHOD()
				esd.bStartedMoveUp = TRUE
			ENDIF

			
		ENDIF
		
		PRIVATE_ENDSCREEN_DRAW_ORDER()
		
		//B* 1825616: Move the pass screen lower on Armenian_1 (due to help text box)
//		IF ARE_STRINGS_EQUAL(esd.title, "M_ARM1")
//			yAt += 0.05 * GET_ASPECT_RATIO_MODIFIER()
//		ENDIF
		
		//		CPRINTLN(debug_luke,"Still drawing at ",yat)
		//Draw splash title scaleform positioned/based on aspect ratio
//		DRAW_SCALEFORM_MOVIE(esd.splash,0.5,yat,AR16_9/ GET_ASPECT_RATIO(FALSE),1,255,255,255,255)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(esd.splash,255,255,255,255)
		
	ENDIF
	
//	CPRINTLN(debug_luke, "End screen blend in ", esd.fBlendInProgress, " sum ",esd.fBlendInTargetYSum, " tmult =  ", esd.fTitleUpMult," M alpha: ",EndScreenAlpha)
	
	FLOAT currentH = (esd.fBlendInTargetYSum*esd.fBlendInProgress)*(1.0 - esd.fScrollOutMult)
	FLOAT socialH = 0.0

	IF esd.bVoteModeEnabled
		socialH = ((ESC_SOCIAL_BULLSHIT_TAB_Y + PIXEL_Y(ESC_LINE_Y_GAP*2))*esd.fVoteBarProg)*(1.0 - esd.fScrollOutMult)
		currentH +=  3*socialH
	ENDIF
	
	IF esd.fScrollOutMult != 0.0
		FLOAT min =  0//PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)//+ PIXEL_Y(ESC_LINE_Y_GAP*2) // title box 
		IF currentH < min
		 	currentH = min
		ENDIF
	ELSE
		FLOAT min = 0// PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) - PIXEL_Y(ESC_LINE_Y_GAP)// Completion box 
		IF esd.fTitleUpMult >= 0.975
			IF currentH < min
			 	currentH = min
			ENDIF
		ENDIF
	ENDIF

	yat 	= ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER()
//	IF ARE_STRINGS_EQUAL(esd.title,"M_ARM1")
//			yAt += 0.05 * GET_ASPECT_RATIO_MODIFIER()
//	ENDIF
	
	IF esd.bCenterMessageMode
		yat = 0.5
	ENDIF
	
	FLOAT w = esd.fTrueHalfWidth *2
	
	IF esd.CompletionType != ESC_MULTI_COMPLETION
		//B* 1918481: increase width if title text is too long
		FLOAT textW = GET_RENDERED_TEXT_WIDTH(esd.title)
		if w < textW
			w = textW + 3*ESC_INDENT
		ENDIF
		
		//B* 2092218: IF on a small aspect ratio, increase width of the box
		IF GET_ASPECT_RATIO(FALSE) < 1.4
			w *= 1.3
		ENDIF
		
		//B* 2023018: If completion space is too small, increase width
		IF NOT IS_STRING_NULL_OR_EMPTY(esd.CompletionLiteralString)
			textW = GET_RENDERED_TEXT_WIDTH_LITERAL_STRING(esd.CompletionResultString,esd.CompletionLiteralString)
		ELSE
			textW = GET_RENDERED_TEXT_WIDTH(esd.CompletionResultString)
		ENDIF
		FLOAT medalOffset = (END_SCREEN_CURSIVE_COMP_X_OFFSET+0.05) / GET_ASPECT_RATIO_MODIFIER()/2.5
		
		//B*-2173541: correction for 800x600 PC res
		IF (esd.CompletionType = ESC_FRACTION_COMPLETION OR esd.CompletionType = ESC_PERCENTAGE_COMPLETION)
		AND esd.CompletionMedalState != ESMS_NO_MEDAL
			IF w < textW + 2.6*medalOffset
				w = textW + 2.6*medalOffset
			ENDIF
		ELIF esd.CompletionType = ESC_CASH_COMPLETION // 3207525 (Only used in MP)
			IF w < textW + 2.6*medalOffset
				w = textW + 2.6*medalOffset
			ENDIF
		ELSE
			IF w < textW + 1.9*medalOffset
				w = textW + 2*medalOffset
			ENDIF
		ENDIF
	ENDIF
	
	FLOAT fontLeft 	= (0.499 - w/2) + ESC_INDENT // request both moved slightly to the left
	FLOAT fontRight = (0.499 + w/2) - ESC_INDENT // request both moved slightly to the left
	
	// Moved these back to here, wanted to only set them exclusive if endscreen is managing the button prompts, but everything else is written expecting us to set them exclusive whether we're managing the buttons or not
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
	
	// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)

	//show the "press button to continue prompt if needed
	IF esd.bShowSkipperPrompt OR esd.bVoteModeEnabled
		IF (esd.iEndScreenDisplayFinish-((ESC_DEFAULT_TIME-1000)*skipperMult)) < esd.IGameTimerReplacement	//normal mode: show buttons 14 seconds before end screen finishes
		OR (esd.bVoteModeEnabled AND esd.fBlendInProgress > 0.95 AND (esd.iEndScreenDisplayFinish-(ESC_DEFAULT_TIME *2/3)) < esd.IGameTimerReplacement) //vote mode
			IF esd.bVoteModeEnabled
				//timing
				
				IF esd.iVoteTimer < 0// init
					esd.iVoteTimer *= -1		
					esd.iVoteTimer = esd.IGameTimerReplacement + esd.iVoteTimer
				ENDIf
				IF esd.iVoteTimer > 0
					IF (esd.iVoteTimer - esd.IGameTimerReplacement) > 0
						DRAW_GENERIC_TIMER(esd.iVoteTimer - esd.IGameTimerReplacement ,"TIMER_TIME")
					ELSE
						esd.iVoteTimer = 0
						esd.eVoteResult = ESSRV_TIMEOUT
						esd.bVoteModeEnabled = FALSE
						esd.bHoldOnEnd = FALSE
						esd.bShowSkipperPrompt = FALSE
						esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
						esd.iVoteTimer = 0
					ENDIF
				ENDIF
				
			
			
				//scroll out effect
				IF esd.fVoteBarProg < 1.0
					esd.fVoteBarProg += 0.0 +@ (1.0/0.166) 
					IF esd.fVoteBarProg > 1.0
						esd.fVoteBarProg = 1.0
					ENDIF
				ENDIF
			ENDIF
			
		
			//SET_TEXT_WRAP(fontLeft,fontRight)
			//SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			//SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			//PRIVATE_RENDER_ENDSCREEN_LABEL("MTPHPERCONT", fontLeft, yat+currentH)//
			
			IF IS_SCREEN_FADED_OUT()
				HIDE_LOADING_ON_FADE_THIS_FRAME()
			ENDIF
			IF esd.button != NULL AND ( esd.fScrollOutMult < 0.10) 
			AND (esd.IGameTimerReplacement <= esd.iEndScreenDisplayFinish)	//Don't render buttons when screen animates out
				//B* disable street names while the end screen is rendering
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME) 
			    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(esd.button,255,255,255,EndScreenAlpha)
			ENDIF
			
			
			IF esd.bVoteModeEnabled
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_ALTERNATE)
			
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_THUMBS_UP	
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_THUMBS_DOWN
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF
				
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_ABSTAIN
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF

			ELSE
				IF esd.bShowSkipperPrompt
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME) 
                    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_ALTERNATE)
					
//					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)	// Don't need to disable these now endscreen has it's own inputs
//					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)

					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,			INPUT_FRONTEND_ENDSCREEN_ACCEPT)
					OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
						PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
						esd.bShowSkipperPrompt = FALSE
						esd.bHoldOnEnd = FALSE
						esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
						PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	
	//Positioning/alpha variables
	FLOAT fHDiff
	FLOAT fadeMult

//==================================================== TITLE BOX ===========================================================	
//	PRIVATE_ENDSCREEN_DRAW_ORDER()
//	INT r,g,b,a
//	GET_HUD_COLOUR(HUD_COLOUR_PAUSE_BG,r,g,b,a)
//	a = round(255 * 0.73) //73% black
//	#IF NOT IS_NEXTGEN_BUILD
//	DRAW_RECT(0.5, yat+(currentH*0.5),
//			  w, currentH,
//			  r,g,b,round(esd.fFadeOutMult*a))
//	#ENDIF
//	#IF IS_NEXTGEN_BUILD
//	DRAW_SPRITE("MPHud", "MissionPassedGradient",
//			  0.5, yat+(currentH*0.5),
//			  w, currentH,
//			  0, r,g,b,round(esd.fFadeOutMult*a))
//	#ENDIF
	
//==================================================== WHITE LINE ===========================================================	

//	IF currentH >= PIXEL_Y(ESC_LINE_Y_GAP)
////		draw whiteline top
//		PRIVATE_ENDSCREEN_DRAW_ORDER()
//		
////		#IF NOT IS_NEXTGEN_BUILD
////		DRAW_RECT(0.5, yat-(PIXEL_Y(ESC_LINE_Y_GAP-0.5)-ESC_Y_PIXEL),
////				  w, PIXEL_Y(ESC_LINE_Y_GAP),
////				  255,255,255,EndScreenAlpha)
////		#ENDIF
////		#IF IS_NEXTGEN_BUILD
////		DRAW_SPRITE("MPHud", "MissionPassedGradient",
////				  0.5,yat-(PIXEL_Y(ESC_LINE_Y_GAP-0.5)-ESC_Y_PIXEL),
////				  w,PIXEL_Y(ESC_LINE_Y_GAP),
////				  0,
////				  255,255,255,EndScreenAlpha)
////		#ENDIF
//	ELSE	
//		EXIT
//	ENDIF
//	yat += PIXEL_Y(ESC_LINE_Y_GAP)
	
//==================================================== TITLE BOX ===========================================================	

	//draw the title string
	INT txt_R,txt_G,txt_B,txt_A
	GET_HUD_COLOUR(HUD_COLOUR_WHITE,txt_R,txt_G,txt_B,txt_A)
	SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
	SET_TEXT_WRAP(fontLeft,fontRight)
	SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
//	#IF NOT IS_NEXTGEN_BUILD
//		SET_TEXT_SCALE(1.0, Get_PT_scale(16)) //17.36
//	#ENDIF
//	#IF IS_NEXTGEN_BUILD
//		SET_TEXT_SCALE(1.0, 0.6)//END_SCREEN_CURSIVE_TITLE_SCALE)
//	#ENDIF
	#IF NOT IS_NEXTGEN_BUILD
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
	#ENDIF
	#IF IS_NEXTGEN_BUILD
		SET_TEXT_SCALE(1.0,  0.4)//END_SCREEN_CURSIVE_COMP_SCALE)
	#ENDIF
	
	yat = yat-PIXEL_Y(6)
	//0.00092592592 1/1080
	
//	
//							//	IS THE USERNAME BEING USED
//	IF esd.titleIsUserName
//		#IF NOT IS_NEXTGEN_BUILD
//			PRIVATE_RENDER_ENDSCREEN_MISSION_LABEL(esd.title,0.5,yat)			//+PIXEL_Y(ESC_LINE_Y_GAP)
//		#ENDIF
//		#IF IS_NEXTGEN_BUILD
//			PRIVATE_RENDER_ENDSCREEN_MISSION_LABEL(esd.title,0.5,yat)			//+PIXEL_Y(ESC_LINE_Y_GAP)
//		#ENDIF
		
//	yat = yat+PIXEL_Y(5)
	
//	ELSE
//							//	  DISPLAY MISSION TITLE
//							
//		IF esd.iTitleSubelements = 0
//			#IF NOT IS_NEXTGEN_BUILD
//			PRIVATE_RENDER_ENDSCREEN_LABEL(esd.title,0.5,yat+ESC_Y_PIXEL,TRUE,DEFAULT)				//+PIXEL_Y(ESC_LINE_Y_GAP)
//			#ENDIF
//			#IF IS_NEXTGEN_BUILD
//			PRIVATE_RENDER_ENDSCREEN_LABEL(esd.title,0.5,
//					yat+ESC_Y_PIXEL+END_SCREEN_CURSIVE_TITLE_Y_OFFSET,
//					TRUE,DEFAULT,FONT_CURSIVE)	//+PIXEL_Y(ESC_LINE_Y_GAP)
//			#ENDIF
//		ELSE
//		
//							//	   TITLE WITH ELEMENTS
//			SET_TEXT_CENTRE(TRUE)
//			#IF NOT IS_NEXTGEN_BUILD
//			SET_TEXT_FONT(FONT_STANDARD)
//			#ENDIF
//			#IF IS_NEXTGEN_BUILD
//			SET_TEXT_FONT(FONT_CURSIVE)
//			#ENDIF
//			PRIVATE_ENDSCREEN_DRAW_ORDER()
//			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(esd.title)
//			INT iIntCount = 0
//			INT iStrCount = 0
//			INT i = 0
//			REPEAT esd.iTitleSubelements i
//				SWITCH esd.titleSubElementType[i]
//					CASE ESTSET_INT
//						ADD_TEXT_COMPONENT_INTEGER(esd.iTitleSubInts[iIntCount])
//						++iIntCount
//						BREAK
//					CASE ESTSET_STRING
//						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(esd.titleSubstrings[iStrCount] )
//						++iStrCount
//						BREAK
//					CASE ESTSET_LITERAL_STRING
//						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.titleSubstrings[iStrCount])
//						++iStrCount
//						BREAK
//				ENDSWITCH
//			ENDREPEAT
//			END_TEXT_COMMAND_DISPLAY_TEXT(0.5,yat+ESC_Y_PIXEL)//yat+PIXEL_Y(1.85))
//		ENDIF
//	ENDIF
	
//===========================================================================================================================
//==================================================== WHITE LINE ===========================================================	
	yat += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)-(PIXEL_Y(ESC_LINE_Y_GAP*ESC_1ST_LINE_UP))
//	CPRINTLN(debug_dan,"Curr/Yat LINE:",currentH,"/",yAt,"/",PIXEL_Y(ESC_LINE_Y_GAP*15))
	fHDiff = currentH - PIXEL_Y(ESC_LINE_Y_GAP*14)
	IF fHDiff >= 0
		//draw whiteline top
		fadeMult = CLAMP(fHDiff/(0.6*PIXEL_Y(ESC_LINE_Y_ADVANCE)),0,1.0)
		PRIVATE_ENDSCREEN_DRAW_ORDER()
		#IF IS_NEXTGEN_BUILD
		DRAW_RECT(0.5,
				  yat-(PIXEL_Y(ESC_LINE_Y_GAP-0.5)-ESC_Y_PIXEL),
				  w,
				  PIXEL_Y_LINE(1), //PIXEL_Y(1),
				  txt_R,txt_G,txt_B,ROUND(fadeMult*txt_A))
//		DRAW_SPRITE("MPHud", "MissionPassedGradient",
//				  0.5,
//				  yat+(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
//				  w,
//				  PIXEL_Y(ESC_LINE_Y_GAP),
//				  0,
//				  txt_R,txt_G,txt_B,EndScreenAlpha)
		#ENDIF
	ELSE	
		EXIT
	ENDIF
//===========================================================================================================================	
//=================================================== ELEMENTS BOX ==========================================================
	yat += PIXEL_Y(ESC_LINE_Y_GAP*ESC_1ST_ELEM_GAP)
//	CPRINTLN(debug_luke,"Curr/Yat elem box:",currentH,"/",yAt)
	IF esd.iElements > 0 
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*ESC_1ST_ELEM_EXTRA_GAP) //6.25 pxls
	ENDIF
	
	
	INT i = 0
	REPEAT esd.iElements i
//		CPRINTLN(debug_dan,"currH: ",currentH," yAt: ",yAt," -",ESC_Y_POSITION* GET_ASPECT_RATIO_MODIFIER())
		fHDiff = currentH - (yat-ESC_Y_POSITION* GET_ASPECT_RATIO_MODIFIER())
		IF fHDiff >= 0
			//slight fade-out if this is the last element to render
			fadeMult = CLAMP(fHDiff/(0.8*PIXEL_Y(ESC_LINE_Y_ADVANCE)),0,1.0)
//			CPRINTLN(debug_dan,"drawing stat ",i," at alpha ",fadeMult)
			//draw each of the content lines
			 PRIVATE_RENDER_ENSCREEN_ELEMENT(esd, i, yat+PIXEL_Y(ESC_LINE_Y_GAP), fontLeft, fontRight,ROUND(EndScreenAlpha * fadeMult))
		ELSE
			EXIT
		ENDIF
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE)
		
		IF esd.CompletionType = ESC_MULTI_COMPLETION
			IF i = esd.iMidIndex - 1
				yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*ESC_1ST_ELEM_EXTRA_GAP)//6.25 pxls
		//		CPRINTLN(debug_dan,"Curr/Yat LINE:",currentH,"/",yAt - ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
				fHDiff = currentH - (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
				IF fHDiff >= 0 
					yat += PIXEL_Y(ESC_LINE_Y_GAP)//y at the bottom of the white line
					fadeMult = CLAMP(fHDiff/(0.6*PIXEL_Y(ESC_LINE_Y_ADVANCE)),0,1.0)
					//draw whiteline before end
					PRIVATE_ENDSCREEN_DRAW_ORDER()
					#IF IS_NEXTGEN_BUILD
					DRAW_RECT(0.5,
							  yat+(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
							  w,
							  PIXEL_Y_LINE(1), //PIXEL_Y(1),
							  txt_R,txt_G,txt_B,ROUND(fadeMult*txt_A))
					#ENDIF
					
					yat += PIXEL_Y(ESC_LINE_Y_GAP*ESC_1ST_ELEM_GAP)
				//	CPRINTLN(debug_luke,"Curr/Yat elem box:",currentH,"/",yAt)
					IF esd.iElements > 0 
						yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*ESC_1ST_ELEM_EXTRA_GAP) //6.25 pxls
					ENDIF
				ELSE
					CWARNINGLN(DEBUG_MISSION_STATS, "exit??? ", currentH, " >= ", (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER()))
		//			EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF esd.CompletionType = ESC_MULTI_COMPLETION
		EXIT
	ENDIF
	
	
//===========================================================================================================================			
//==================================================== WHITE LINE ===========================================================	
	IF esd.iElements  > 0// AND esd.bShowCompletion
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*ESC_1ST_ELEM_EXTRA_GAP)//6.25 pxls
//		CPRINTLN(debug_dan,"Curr/Yat LINE:",currentH,"/",yAt - ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
		fHDiff = currentH - (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
		IF fHDiff >= 0 
			yat += PIXEL_Y(ESC_LINE_Y_GAP)//y at the bottom of the white line
			fadeMult = CLAMP(fHDiff/(0.6*PIXEL_Y(ESC_LINE_Y_ADVANCE)),0,1.0)
			//draw whiteline before end
			PRIVATE_ENDSCREEN_DRAW_ORDER()
			#IF IS_NEXTGEN_BUILD
			DRAW_RECT(0.5,
					  yat+(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
					  w,
					  PIXEL_Y_LINE(1), //PIXEL_Y(1),
					  txt_R,txt_G,txt_B,ROUND(fadeMult*txt_A))
			#ENDIF
		ELSE
			CWARNINGLN(DEBUG_MISSION_STATS, "exit??? ", currentH, " >= ", (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER()))
//			EXIT
		ENDIF
			
		
	ENDIF
//===========================================================================================================================
//================================================= COMPLETION BOX ==========================================================
	IF esd.bShowCompletion
//		CPRINTLN(debug_dan,"Curr/Yat box:",currentH,"/",yAt - ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*ESC_COMPLETION_GAP)
		fHDiff = currentH - (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
		IF fHDiff >=0
			fadeMult = CLAMP(fHDiff/(0.8*PIXEL_Y(ESC_LINE_Y_ADVANCE)),0,1.0)
	//		CPRINTLN(debug_luke,"Curr/Yat completion:",currentH,"/",yAt)
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,ROUND(fadeMult * EndScreenAlpha))
			
			//Medal dimensions
			FLOAT ficonw, ficonh
			GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_EMPTY,FALSE,TRUE,ficonw,ficonh)
			
			FLOAT completionLeft = fontLeft, completionRight = fontRight
				
			#IF IS_NEXTGEN_BUILD
			IF GET_CURRENT_LANGUAGE() = LANGUAGE_ENGLISH
				completionLeft = fontLeft + (END_SCREEN_CURSIVE_COMP_X_OFFSET / GET_ASPECT_RATIO_MODIFIER()/2.5)
				completionRight = fontRight - (END_SCREEN_CURSIVE_COMP_X_OFFSET/ GET_ASPECT_RATIO_MODIFIER()/2.5)
				
				IF esd.CompletionType = ESC_FRACTION_COMPLETION
					completionLeft = fontLeft + ((END_SCREEN_CURSIVE_COMP_X_OFFSET+0.05) / GET_ASPECT_RATIO_MODIFIER()/2.5)
					completionRight = fontRight - ((END_SCREEN_CURSIVE_COMP_X_OFFSET+0.05) / GET_ASPECT_RATIO_MODIFIER()/2.5)
				ENDIF
			ENDIF
			#ENDIF
			
			//If no medal awarded, move slightly right to keep centred
			IF esd.CompletionMedalState = ESMS_NO_MEDAL
				completionLeft += (ficonw*0.28 + ESC_INDENT)/2
				completionRight += (ficonw*0.28 + ESC_INDENT)/2
			ENDIF
			
			//TODO Draw completion line here
			IF esd.CompletionType != ESC_STRING_COMPLETION
				SET_TEXT_WRAP(completionLeft,completionRight)
				SET_TEXT_JUSTIFICATION(FONT_LEFT)
			ELSE
				completionLeft = 0.5
				SET_TEXT_JUSTIFICATION(FONT_CENTRE)
			ENDIF
			#IF NOT IS_NEXTGEN_BUILD
				SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
				PRIVATE_RENDER_ENDSCREEN_LABEL(esd.CompletionResultString,completionLeft,yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
			#ENDIF
			#IF IS_NEXTGEN_BUILD
				SET_TEXT_SCALE(1.0, 0.4)//END_SCREEN_CURSIVE_COMP_SCALE)
				IF IS_STRING_NULL_OR_EMPTY(esd.CompletionLiteralString)
					PRIVATE_RENDER_ENDSCREEN_LABEL(esd.CompletionResultString,completionLeft,yat+PIXEL_Y(ESC_LINE_Y_GAP*2), DEFAULT, DEFAULT)//4pxls after white 
				ELSE
					PRIVATE_RENDER_ENDSCREEN_LABEL_WITH_LITERAL_STRING(esd.CompletionResultString,esd.CompletionLiteralString,completionLeft,yat+PIXEL_Y(ESC_LINE_Y_GAP*2), DEFAULT, DEFAULT)//4pxls after white line
				ENDIF
			#ENDIF
			
			//and percentage if needed

			SET_TEXT_WRAP(completionLeft,completionRight)
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			#IF NOT IS_NEXTGEN_BUILD
				SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
			#ENDIF
			#IF IS_NEXTGEN_BUILD
				SET_TEXT_SCALE(1.0,  0.4)//END_SCREEN_CURSIVE_COMP_SCALE)
			#ENDIF
			SET_TEXT_CENTRE(FALSE)
			#IF NOT IS_NEXTGEN_BUILD
				SET_TEXT_FONT(FONT_STANDARD)
			#ENDIF
			#IF IS_NEXTGEN_BUILD
				//SET_TEXT_FONT(FONT_CURSIVE)
			#ENDIF
			PRIVATE_ENDSCREEN_DRAW_ORDER()
			
			FLOAT medalRight = completionRight
			
			#IF NOT IS_NEXTGEN_BUILD
			TEXT_LABEL medaldictionary = "CommonMenu"
			TEXT_LABEL medalsprite = "Common_Medal"
			#ENDIF
			#IF IS_NEXTGEN_BUILD
			TEXT_LABEL medaldictionary = "MPHud"
			TEXT_LABEL_31 medalsprite = "MissionPassedMedal"
			#ENDIF
			
			medalRight -= ficonw*0.28 + ESC_INDENT//ESC_X_PIXEL*(MEDAL_PIX_SIDE/1.5)

			SET_TEXT_WRAP(completionLeft,medalRight)
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,ROUND(fadeMult * EndScreenAlpha))
			
			SWITCH esd.CompletionType
				CASE ESC_PERCENTAGE_COMPLETION
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("PERCENTAGE")
					ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
					END_TEXT_COMMAND_DISPLAY_TEXT(completionLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
					BREAK
					
				CASE ESC_FRACTION_COMPLETION
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_TWO_NUM")
					ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
					ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueB)
					END_TEXT_COMMAND_DISPLAY_TEXT(completionLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
					BREAK
				
				CASE ESC_XP_COMPLETION
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("MTPHPER_XPNO")
					ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
					END_TEXT_COMMAND_DISPLAY_TEXT(completionLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
					BREAK
				
				CASE ESC_CASH_COMPLETION
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ESDOLLA")
				//	ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
					ADD_TEXT_COMPONENT_FORMATTED_INTEGER(esd.iCompletionValueA, INTEGER_FORMAT_COMMA_SEPARATORS)
					END_TEXT_COMMAND_DISPLAY_TEXT(completionLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
					BREAK
			ENDSWITCH
			
			REQUEST_STREAMED_TEXTURE_DICT(medaldictionary)

			IF esd.CompletionMedalState != ESMS_NO_MEDAL
			AND HAS_STREAMED_TEXTURE_DICT_LOADED(medaldictionary)
					INT mr = 255
					INT mg = 255
					INT mb = 255
					INT ma = EndScreenAlpha
					SWITCH esd.CompletionMedalState
						CASE ESMS_BRONZE
							GET_HUD_COLOUR(HUD_COLOUR_BRONZE,mr,mg,mb,ma)
							BREAK
						CASE ESMS_GOLD
							GET_HUD_COLOUR(HUD_COLOUR_GOLD,mr,mg,mb,ma)
							BREAK
						CASE ESMS_SILVER
							GET_HUD_COLOUR(HUD_COLOUR_SILVER,mr,mg,mb,ma)
							BREAK
					ENDSWITCH
					
					//FLOAT fmedalwh = ESC_X_PIXEL*(MEDAL_PIX_SIDE/3)
					FLOAT fmedalhh = ESC_Y_PIXEL*(MEDAL_PIX_SIDE/3)
					
					FLOAT fmedalw = ESC_X_PIXEL*MEDAL_PIX_SIDE*2//twice the size for new texture
					FLOAT fmedalh = ESC_Y_PIXEL*MEDAL_PIX_SIDE*2//twice the size for new texture
					FLOAT fmedalcx = (completionRight+PIXEL_X(4))- ESC_INDENT
					FLOAT fmedalcy = yat + (PIXEL_Y(END_SCREEN_MEDAL_Y_OFFSET)) + fmedalhh
					
					//Move medal left if no completion text
					IF esd.CompletionType = INT_TO_ENUM(END_SCREEN_COMPLETION_TYPE,-1)
						fmedalCx -= ESC_INDENT*6
					ENDIF
					
					fmedalw *= END_SCREEN_MEDAL_SCALE
					fmedalh *= END_SCREEN_MEDAL_SCALE
					
					DRAW_SPRITE(medaldictionary, medalsprite, 
							fmedalCx,//(completionRight) - fmedalwh*1.1, 
							fmedalCy, //fmedalhh+
							fmedalw, fmedalh, 
							0, 
							mr, mg, mb, ROUND(fadeMult * EndScreenAlpha))
			ENDIF
			
	//===========================================================================================================================
	//==================================================== WHITE LINE ===========================================================	
			yat += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)-(ESC_LINE_Y_GAP)
	//		CPRINTLN(debug_luke,"Curr/Yat line3:",currentH,"/",yAt)
	//		IF currentH >= (yat-ESC_Y_POSITION * GET_ASPECT_RATIO_MODIFIER())
	//			//draw whiteline before end
	//			PRIVATE_ENDSCREEN_DRAW_ORDER()
	//			#IF NOT IS_NEXTGEN_BUILD
	//			DRAW_RECT(0.5,
	//					  yat,//-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
	//					  w,
	//					  PIXEL_Y(ESC_LINE_Y_GAP),
	//					  255,255,255,EndScreenAlpha)
	//			#ENDIF
	//			#IF IS_NEXTGEN_BUILD
	////			DRAW_SPRITE("MPHud", "MissionPassedGradient",
	////					  0.5,
	////					  yat,//-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
	////					  w,
	////					  PIXEL_Y(ESC_LINE_Y_GAP),
	////					  0,
	////					  255,255,255,EndScreenAlpha)
	//			#ENDIF
	//		ELSE
	//			EXIT 
	//		ENDIF
		ENDIF
	ENDIF
//	CPRINTLN(debug_dan,"")

//===========================================================================================================================
//===================================================== VOTE BOX ============================================================
	
//	IF esd.bVoteModeEnabled AND socialH > 0.0
//		FLOAT remainder = socialH
//		remainder -= socialH*0.08
//		yat += socialH*0.08
//		//[ES_HELP_SOC1:MISHSTA]
//		//Let the ~y~Social Club Community~s~ know if you
//		
//		//draw the title string
//		IF esd.fVoteBarProg > 0.08
//			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
//			SET_TEXT_WRAP(fontLeft,fontRight)
//			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
//			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
//			PRIVATE_RENDER_ENDSCREEN_LABEL("ES_HELP_SOC1",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
//		ENDIF
//		
//		
//		remainder -= socialH*0.2
//		yat += socialH*0.2
//		//[ES_HELP_SOC2:MISHSTA]
//		//enjoyed this race. Vote here:
//		
//		//draw the title string
//		IF esd.fVoteBarProg > 0.28
//			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
//			SET_TEXT_WRAP(fontLeft,fontRight)
//			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
//			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
//			if 	 esd.eVoteGameType = ESMGT_RACE
//				PRIVATE_RENDER_ENDSCREEN_LABEL("ES_HELP_SOC2",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
//			elif esd.eVoteGameType = ESMGT_DEATHMATCH
//				PRIVATE_RENDER_ENDSCREEN_LABEL("ES_HELP_SOC2b",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
//			elif esd.eVoteGameType = ESMGT_MISSION
//				PRIVATE_RENDER_ENDSCREEN_LABEL("ES_HELP_SOC2c",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
//			endif
//		ENDIF
//		
//		remainder -= socialH*0.3
//		yat += socialH*0.3
//		//[ES_HELP_SOC3:MISHSTA]
//		//Like / Dislike
//		
//		//draw the title string
//		IF esd.fVoteBarProg > 0.58
//			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
//			SET_TEXT_WRAP(fontLeft,fontRight)
//			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
//			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
//			PRIVATE_RENDER_ENDSCREEN_LABEL("ES_HELP_SOC3",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
//			//draw thumbs 
//			FLOAT fThumbw = ESC_X_PIXEL * MEDAL_PIX_SIDE * 3
//			FLOAT fThumbh = ESC_Y_PIXEL * MEDAL_PIX_SIDE * 3
//			FLOAT fThumbhh = ESC_Y_PIXEL* (MEDAL_PIX_SIDE/3)
//			
//			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
//			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ES_HELP_SOC3")
//			FLOAT wx = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
//			
//			
//			//left thumb
//			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb",	
//			0.5 - (wx*0.5) - (fThumbw*0.6),
//			yat + (PIXEL_Y(9)) + fThumbhh,
//			fThumbw, fThumbh, 0,255, 255, 255, EndScreenAlpha)
//			
//			//right thumb
//			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb", 	
//			0.5+ (wx*0.5) + (fThumbw*0.6),	
//			yat + (PIXEL_Y(9)) + fThumbhh,
//			fThumbw, fThumbh, 180,255, 255, 255, EndScreenAlpha)
//			
//		ENDIF
//		
//		IF remainder > 0
//			yat += remainder
//		ENDIF
//		
//		
//	ENDIF
//===========================================================================================================================
//==================================================== WHITE LINE ===========================================================
//	IF currentH >= (yat-ESC_Y_POSITION* GET_ASPECT_RATIO_MODIFIER())
//		//draw whiteline before end
//		PRIVATE_ENDSCREEN_DRAW_ORDER()
//		#IF NOT IS_NEXTGEN_BUILD
//		DRAW_RECT(0.5,
//				  yat, //-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
//				  w,
//				  PIXEL_Y(ESC_LINE_Y_GAP),
//				  255,255,255,EndScreenAlpha)
//		#ENDIF
//		#IF IS_NEXTGEN_BUILD
////		DRAW_SPRITE("MPHud", "MissionPassedGradient",
////				  0.5,
////				  yat, //-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
////				  w,
////				  PIXEL_Y(ESC_LINE_Y_GAP),
////				  0,
////				  255,255,255,EndScreenAlpha)
////		#ENDIF
//	ELSE
//		EXIT 
//	ENDIF
//===========================================================================================================================	

ENDPROC
Proc INIT_ENDSCREEN_VALUES(END_SCREEN_DATASET &esd)

	esd.fFadeOutMult 			= 1.0
	esd.iFadeOutSplashTimer 	= 0	
	esd.fVoteBarProg 			= 0.0	
	esd.iEndScreenDisplayFinish = 0
	esd.fTitleUpMult 			= 0.0	
	esd.fScrollOutMult 			= 0.0
	esd.fBlendInProgress 		= 0.0
	esd.fBlendInTargetYSum 		= 0.0 //the precalculated target height
	esd.bTransitionOutCalled	= FALSE
	esd.bDoneFlash 				= FALSE
	esd.IGameTimerReplacement 	= 0 
	esd.bNoLoadingScreenEnabled = FALSE
	esd.bStatsExpanded 			= FALSE
	esd.bAlwaysShowStats		= FALSE
	 
	esd.fTrueHalfWidth = ESC_HALF_WIDTH
	 
	esd.bStartedFailformAnim 	= FALSE
	esd.bStartedMoveUp			= FALSE
	
	esd.iLastFrameRendered		= 0
	esd.bLastRender				= FALSE
	
	esd.fCircMult				= 1
	
ENDPROC
PROC RESET_ENDSCREEN(END_SCREEN_DATASET &esd)
	//SCALEFORM_INDEX splash
	INIT_ENDSCREEN_VALUES(esd)
	
	esd.iVoteTimer 				= 0
	esd.iTitleSubelements 		= 0
	esd.iElements 				= 0
	esd.bVoteModeEnabled 		= FALSE	
	esd.eVoteResult 			= ESSRV_UNSET
		
ENDPROC

/// PURPOSE:
///    CALL THIS BEFORE PREPARE IF YOU USE IT
PROC ENDSCREEN_CONFIGURE_VOTE_PANEL(END_SCREEN_DATASET &esd,INT iVoteDurationSeconds,END_SCREEN_MP_GAME_TYPE gameType = ESMGT_RACE)
	
	esd.bVoteModeEnabled 	= TRUE
	esd.fVoteBarProg 		= 0.0
	esd.eVoteResult 		= ESSRV_UNSET
	esd.iVoteTimer 			= -iVoteDurationSeconds*1000
	esd.bHoldOnEnd 			= TRUE
	esd.eVoteGameType 		= gameType
ENDPROC

FUNC END_SCREEN_SOCIAL_RETURN_VALUE GET_ENDSCREEN_SOCIAL_RETURN_VALUE(END_SCREEN_DATASET &esd)
	RETURN esd.eVoteResult
ENDFUNC

PROC DEBUG_ENDSCREEN_DRAW(END_SCREEN_DATASET &esd)
UNUSED_PARAMETER(esd)
#IF IS_DEBUG_BUILD
	TEXT_LABEL_23 tlTime
	Float yAt = 0.6, xAt = 0.7

	//Draw end screen timer
	tlTime ="Time: "
	tlTime+= esd.IGameTimerReplacement
	tlTime+= "/"
	tlTime+= esd.iEndScreenDisplayFinish
	SET_TEXT_SCALE(0.7,0.7)
	DISPLAY_TEXT_WITH_LITERAL_STRING(xAt,yAt,"STRING",tlTime)
	yAt +=0.05
	
	//Draw blend-out
	tlTime ="Blend-in: "
	tlTime+= ROUND(esd.fBlendInProgress*100)
	SET_TEXT_SCALE(0.7,0.7)
	DISPLAY_TEXT_WITH_LITERAL_STRING(xAt,yAt,"STRING",tlTime)
	yAt +=0.05
	
	//Draw circ mult
	tlTime ="Circ mult: "
	tlTime+= ROUND(esd.fCircMult*100)
	SET_TEXT_SCALE(0.7,0.7)
	DISPLAY_TEXT_WITH_LITERAL_STRING(xAt,yAt,"STRING",tlTime)
	yAt +=0.05
#ENDIF
ENDPROC

//populate
//prepare
//render
//shutdown


/// PURPOSE:
///    render the data in the heist endscreen structure
/// PARAMS:
///    esd - data handle
///    bBlock - if true will block and re-render until done
///    	otherwise will return state until done
///    skipperMultiplier - How much pressing A to skip will acceperate the screen timer
///    shortShard - shorter version of the shard (for no stat pass screens)
///    bIgnoreOwnTimer - Won't stop rendering while this is true - makes stop time = current + 1 sec each time it's set
///   
/// RETURNS:
///    True when render complete
FUNC BOOL RENDER_ENDSCREEN(END_SCREEN_DATASET &esd, BOOL bBlockUntilDone = FALSE,FLOAT skipperMultiplier = 1.0, 
							BOOL shortShard = FALSE, BOOL bIgnoreOwnTimer = FALSE, BOOL bAllowReexpand = FALSE)
	
	//Only render once a frame, return FALSE to avoid thinking the screen has stopped rendering naturally
	IF GET_FRAME_COUNT() = esd.iLastFrameRendered
		CPRINTLN(debug_mission_stats,"Already rendered this frame (",esd.iLastFrameRendered,"), avoiding double draw")
		RETURN esd.bLastRender
	ENDIF
	esd.iLastFrameRendered = GET_FRAME_COUNT()
	
//	DEBUG_ENDSCREEN_DRAW(esd)
	
	// PT Fix 1558760
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PED_DEAD_OR_DYING(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			esd.bLastRender = TRUE
			RETURN TRUE // bail out
		ENDIF
		
		IF IS_PED_BEING_ARRESTED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			esd.bLastRender = TRUE
			RETURN TRUE // bail out
		ENDIF
	ENDIF	
	
	IF NOT esd.bNoLoadingScreenEnabled
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			SET_NO_LOADING_SCREEN(TRUE)
			esd.bNoLoadingScreenEnabled = TRUE
		ENDIF
	ENDIF
	
	//DRAW_GENERIC_TIMER
	//DRAW_TIMER_HUD
	
	//Before disabling the special ability toggle, first clear any active abilities.
	#IF USE_TU_CHANGES
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					CPRINTLN(DEBUG_MISSION_STATS, "RENDER_ENDSCREEN: Clearing player special ability as end screen is displaying.")
					SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME) 
    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF GET_ARE_CAMERA_CONTROLS_DISABLED()							// B*2277553 - Now checking for camera off, not player control off
		OR (IS_SCREEN_FADED_OUT() AND NOT IS_SCREEN_FADING_IN())
			SET_MOUSE_CURSOR_THIS_FRAME()
		ENDIF
	ENDIF
	
	//Stop the comms controller queue timers from ticking while the end screen is drawing.
	g_bPauseCommsQueuesThisFrame = TRUE
	
	IF NOT esd.bDoneFlash
		SWITCH GET_PLAYER_PED_ENUM(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			CASE CHAR_FRANKLIN
				ANIMPOSTFX_PLAY("SuccessFranklin",1000,FALSE)
				BREAK
			CASE CHAR_TREVOR
				ANIMPOSTFX_PLAY("SuccessTrevor",1000,FALSE)
				BREAK
			DEFAULT//CASE CHAR_MICHAEL or MP
				ANIMPOSTFX_PLAY("SuccessMichael",1000,FALSE)
				BREAK
		ENDSWITCH
		esd.bDoneFlash = TRUE
	ENDIF
	
	
	
	IF esd.iEndScreenDisplayFinish = 0
		esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement + FLOOR(ESC_DEFAULT_TIME *skipperMultiplier)
		CPRINTLN(DEBUG_MISSION_STATS, "RENDER_ENDSCREEN: first call on dataset, setting timer")
	ENDIF
	//Update own timer to never stop rendering
	IF bIgnoreOwnTimer AND esd.IGameTimerReplacement >= esd.iEndScreenDisplayFinish - ESC_BLEND_TIME
		esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement + 2*ESC_BLEND_TIME
	ENDIF
	
	IF esd.fBlendInTargetYSum = 0.0 //recalcuate the height
		esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) // Title box should = 33pxls
		esd.fBlendInTargetYSum += esd.iElements* PIXEL_Y(ESC_LINE_Y_ADVANCE)
		IF esd.iElements > 0
		
			esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE*0.5)
		
		ENDIF
		IF esd.bShowCompletion
			esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) - PIXEL_Y(ESC_LINE_Y_GAP)//completion
		ENDIF 
	ENDIF
	
	BOOL rendering = TRUE
	
	WHILE rendering
		//Set the 'end screen on' global boolean to true
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
		
		//render the data
		esd.IGameTimerReplacement += round(0 +@ 1000)
		
		RAW_ENDSCREEN_DRAW(esd,skipperMultiplier,shortShard)
		
		#IF NOT IS_NEXTGEN_BUILD
		CONST_FLOAT	fCONST_TitleUpDelta		0.15
		CONST_FLOAT	fCONST_FadeOutDelta		0.81
		CONST_FLOAT	fCONST_ScrollOutDelta	0.14
		#ENDIF			
		#IF IS_NEXTGEN_BUILD
		CONST_FLOAT	fCONST_TitleUpDelta		0.225
		CONST_FLOAT	fCONST_FadeOutDelta		1.215
		CONST_FLOAT	fCONST_ScrollOutDelta	0.30
		//Circular interp constants
		//		Stats scroll speed increases in both expand and contract
		//		Contract is slower (as per shard anim) so it's multiplied locally
		CONST_FLOAT fCONST_CircInterpDelta	0.07
		CONST_FLOAT fCONST_CircMultMin		0.75
		CONST_FLOAT fCONST_CircMultMax		1.15
		#ENDIF			

		
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish + ESC_MOVE_UP_GRACE - ESC_DEFAULT_TIME*skipperMultiplier //text move up time
			IF esd.fTitleUpMult < 1.0
				esd.fTitleUpMult += 0.0 +@ (1.0/fCONST_TitleUpDelta)
			ENDIF
		ENDIF
		esd.fTitleUpMult= CLAMP(esd.fTitleUpMult,0.0,1.0)
		
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish -  ESC_ROLL_UP_GRACE
			IF NOT esd.bHoldOnEnd
				IF esd.bStatsExpanded
					esd.bStatsExpanded = FALSE
					esd.bAlwaysShowStats = FALSE
					esd.fCircMult = fCONST_CircMultMin 		//Reset interp multiplier
					BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash,"ROLL_UP_BACKGROUND")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				esd.fFadeOutMult -= 0.0 +@ (1.0/fCONST_FadeOutDelta)
			endif
		ENDIF
		esd.fFadeOutMult = CLAMP(esd.fFadeOutMult,0.0,1.0)
		
		IF esd.fFadeOutMult <= 0.70
		and not esd.bTransitionOutCalled
		AND (esd.splash != NULL)
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "TRANSITION_OUT")	
			END_SCALEFORM_MOVIE_METHOD()
			esd.iFadeOutSplashTimer = esd.IGameTimerReplacement	
			esd.bTransitionOutCalled = true
		endif
		
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish - ESC_ROLL_UP_GRACE  
			IF NOT esd.bHoldOnEnd
				IF esd.fScrollOutMult < 1.0
					esd.fScrollOutMult += 0.0 +@ (1.0/fCONST_ScrollOutDelta) 
				ENDIF
			ENDIF
		ENDIF
		esd.fScrollOutMult = CLAMP(esd.fScrollOutMult,0.0,1.0)
		
		// If control method has changed, update buttons
		IF esd.bShowSkipperPrompt
			IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				IF HAS_SCALEFORM_MOVIE_LOADED(esd.button)
					IF NOT esd.bVoteModeEnabled
						ENDSCREEN_ADD_SKIP_BUTTONS(esd, (NOT esd.bStatsExpanded) AND (esd.iElements > 0))
//					ELSE
//						ENDSCREEN_ADD_VOTE_BUTTONS(esd)	// Not sure if the vote screen exists anymore, so haven't been able to test this bit. It should in theory be fine though if voting screen needs to be made to refresh sprites.
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//update the blend
		//Check to see if stats should be expanded, only if not transitioning out
		// we need to check the disabled control as on PC expand is jump
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND) 
		AND esd.iEndScreenDisplayFinish > esd.IGameTimerReplacement+ESC_ROLL_UP_GRACE
			IF NOT esd.bAlwaysShowStats AND esd.iElements != 0 AND HAS_SCALEFORM_MOVIE_LOADED(esd.button) 
			AND esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish + 1165 - 15000*skipperMultiplier //grace period for stats dropdown
				//Check whether to expand or contract
				IF NOT esd.bStatsExpanded
					BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash,"ROLL_DOWN_BACKGROUND")
					END_SCALEFORM_MOVIE_METHOD()
					esd.bStatsExpanded = TRUE
					esd.fCircMult = fCONST_CircMultMin 		//Reset interp multiplier
					//Increase the timer in case it is close to closing-out
					IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish - 5000
						esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement + 5000
					ENDIF
				ELIF bAllowReexpand		//Only if allowed, default is FALSE
					BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash,"ROLL_UP_BACKGROUND")
					END_SCALEFORM_MOVIE_METHOD()
					esd.bStatsExpanded = FALSE
					esd.fCircMult = fCONST_CircMultMin		//Reset interp multiplier
				ENDIF
				
				//Re-draw buttons with "expand" removed
				ENDSCREEN_ADD_SKIP_BUTTONS(esd, (NOT esd.bStatsExpanded) AND (esd.iElements > 0))
				
			ENDIF
		ENDIF

		//Expand stats multiplier (goes up when expanding, down when not
		IF (esd.bStatsExpanded OR esd.bAlwaysShowStats) AND esd.iElements != 0
			IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish + 1165 - 15000*skipperMultiplier // grace period
				IF esd.bAlwaysShowStats AND NOT esd.bStatsExpanded
					esd.bStatsExpanded = TRUE
					esd.fCircMult = fCONST_CircMultMin		//Reset interp multiplier
					BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash,"ROLL_DOWN_BACKGROUND")
					END_SCALEFORM_MOVIE_METHOD()					
				ENDIF
				//Clamp-increase the blendinprogress
				esd.fBlendInProgress = CLAMP(esd.fBlendInProgress+@ (1.0/fCONST_ScrollOutDelta*esd.fCircMult),0.0,1.0)
				esd.fCircMult = CLAMP(esd.fCircMult + fconst_circInterpDelta,fCONST_CircMultMin,fCONST_CircMultMax)
			ENDIF
	 	ELSE
			//Clamp-decrease the blendinprogress
			esd.fBlendInProgress = CLAMP(esd.fBlendInProgress-@ (1.0/fCONST_ScrollOutDelta*esd.fCircMult*0.01),0.0,1.0)
			esd.fCircMult = CLAMP(esd.fCircMult + fconst_circInterpDelta,fCONST_CircMultMin,fCONST_CircMultMax)
		ENDIF
		//check to see if done
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish
		
			IF esd.bHoldOnEnd
				//display x to continue button if need be
				//wait for button press
					//if so then rendering = false
				IF NOT esd.bVoteModeEnabled					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_ACCEPT)
//							rendering  = FALSE							
						esd.bHoldOnEnd = FALSE
					ENDIF
				ENDIF
			ELSE
				IF esd.IGameTimerReplacement - esd.iFadeOutSplashTimer > 1000
				and esd.bTransitionOutCalled
					rendering = FALSE			
				endif
			ENDIF		
		endif
		
		//Store last frame's rendering result (negated, returns true when done rendering)
		esd.bLastRender = !rendering
		
		IF bBlockUntilDone
			WAIT(0)
		ELSE
			//Set the 'end screen on' global boolean to false
			IF !rendering
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			ENDIF
			RETURN !rendering
		ENDIF
		
		
	ENDWHILE
	//made it out! the screen process is complete.
	CPRINTLN(DEBUG_MISSION_STATS, "RENDER_ENDSCREEN: process complete")
	
	//Set the 'end screen on' global boolean to false
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	RETURN TRUE
ENDFUNC


//Force pass screen to transition out by simulating Accept being pressed
PROC ENDSCREEN_START_TRANSITION_OUT(END_SCREEN_DATASET &esd)
	//Only do it if it is the case
	IF esd.bHoldOnEnd OR esd.IGameTimerReplacement <= esd.iEndScreenDisplayFinish
		esd.bHoldOnEnd = FALSE
		esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement -1
	ENDIF
ENDPROC



//Population calls 
PROC SET_ENDSCREEN_DATASET_HEADER(END_SCREEN_DATASET &esd,
								  STRING splash,
								  STRING title,
								  BOOL bTitleIsUserName = FALSE)
	//
	
	esd.passed_splash_message = splash
	esd.title = title

	
	esd.titleIsUserName = bTitleIsUserName
	esd.eSplashType = ESST_REGULAR
ENDPROC

PROC SET_ENDSCREEN_DATASET_SPLASH_WITH_SUBINTS(END_SCREEN_DATASET &esd,
								  STRING splash,
								  INT ValueA,
								  INT ValueB = 0,
								  BOOL UseBoth = FALSE)
	//
	esd.passed_splash_message = splash
	esd.iSplashSubA = ValueA
	esd.iSplashSubB = ValueB
	IF UseBoth
		esd.eSplashType = ESST_TWO_SUB
	ELSE
		esd.eSplashType = ESST_ONE_SUB
	ENDIF
ENDPROC



PROC SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_LITERAL_STRING(END_SCREEN_DATASET &esd,STRING str)
	//
	esd.iTitleSubelements = 1
	esd.titleSubElementType[0] = ESTSET_LITERAL_STRING
	esd.titleSubstrings[0] = str
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_STRING(END_SCREEN_DATASET &esd,STRING str)
	//
	esd.iTitleSubelements = 1
	esd.titleSubElementType[0] = ESTSET_STRING
	esd.titleSubstrings[0] = str
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_INT(END_SCREEN_DATASET &esd,
								  INT value)
	//
	esd.iTitleSubelements = 1
	esd.titleSubElementType[0] = ESTSET_INT
	esd.iTitleSubInts[0] = value
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SUB_INT_STRING_INT(END_SCREEN_DATASET &esd,
								  INT valueA,STRING str,INT valueB)
	//
	esd.iTitleSubelements = 3
	esd.titleSubElementType[0] = ESTSET_INT
	esd.titleSubElementType[1] = ESTSET_STRING
	esd.titleSubElementType[2] = ESTSET_INT
	
	esd.iTitleSubInts[0] = valueA
	esd.iTitleSubInts[1] = valueB

	esd.titleSubstrings[0] = str
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SUB_INT_STRING(END_SCREEN_DATASET &esd,
								  INT value,STRING str)
	//
	esd.iTitleSubelements = 2
	esd.titleSubElementType[0] = ESTSET_INT
	esd.titleSubElementType[1] = ESTSET_STRING
	
	esd.iTitleSubInts[0] = value

	esd.titleSubstrings[0] = str
ENDPROC



PROC ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(END_SCREEN_DATASET &esd,
										   END_SCREEN_ELEMENT_FORMATTING type, 
										   STRING name,
										   STRING text,
										   INT valueA,
										   INT valueB,
										   END_SCREEN_CHECK_MARK_STATUS checkState,
										   BOOL bNameIsPlayerName = FALSE)
										   
	//
	IF esd.iElements = END_SCREEN_MAX_ELEMENTS
		EXIT	
	ENDIF
	INT i = esd.iElements
	esd.ElementFormat[i] = type
	esd.ElementName[i] = name
	esd.ElementText[i] = text
	esd.ElementValA[i] = valueA
	esd.ElementValB[i] = valueB
	esd.ElementCheck[i] = checkState
	//esd.ElementInvalidation[i] = bInvalidated
	CDEBUG1LN(DEBUG_MISSION_STATS,GET_THIS_SCRIPT_NAME(),"-> ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT: name ",name, " val A:",valueA," val B:",valueB," check:",ENUM_TO_INT(checkState))
	esd.ElementIsPlayerName[i] = bNameIsPlayerName
	++esd.iElements
ENDPROC



PROC SET_ENDSCREEN_COMPLETION_LINE_STATE(END_SCREEN_DATASET &esd,
										 BOOL bShow,
										 STRING resultString,
										 INT valueA,
										 INT valueB,
										 END_SCREEN_COMPLETION_TYPE type = ESC_PERCENTAGE_COMPLETION,
										 END_SCREEN_MEDAL_STATUS medal = ESMS_NO_MEDAL
										  , INT iMidIndex = 0 )
	//
	esd.bShowCompletion = bShow
	esd.CompletionResultString = resultString
	esd.CompletionLiteralString = ""
	esd.iCompletionValueA = valueA
	esd.iCompletionValueB = valueB
	esd.CompletionType = type
	 
	esd.CompletionMedalState = medal
	
	esd.iMidIndex = iMidIndex
ENDPROC

PROC SET_ENDSCREEN_COMPLETION_LINE_STATE_WITH_LITERAL_STRING(END_SCREEN_DATASET &esd,
										 BOOL bShow,
										 STRING resultString,
										 STRING literalString,
										 INT valueA,
										 INT valueB,
										 END_SCREEN_COMPLETION_TYPE type = ESC_PERCENTAGE_COMPLETION,
										 END_SCREEN_MEDAL_STATUS medal = ESMS_NO_MEDAL
										  , INT iMidIndex = 0 )
	//
	esd.bShowCompletion = bShow
	esd.CompletionResultString = resultString
	esd.CompletionLiteralString = literalString

	esd.iCompletionValueA = valueA
	esd.iCompletionValueB = valueB
	esd.CompletionType = type
	 
	esd.CompletionMedalState = medal
	
	esd.iMidIndex = iMidIndex
ENDPROC


/// PURPOSE:
///    Prep the scaleform part of the end screen, call this after you have finished calling populate
///    functions on it.
///    If not set to block returns if it has completely prepped the scaleform
/// RETURNS:
///    
FUNC BOOL ENDSCREEN_PREPARE(END_SCREEN_DATASET &esd, BOOL bBlockUntilLoaded = TRUE, BOOL bCenterMessageMode = FALSE)
	
	//esd.bShowCompletion = FALSE
// 	ENDSCREEN_CONFIGURE_VOTE_PANEL(esd,30)
	esd.bCenterMessageMode = bCenterMessageMode
	INIT_ENDSCREEN_VALUES(esd) // prep values

	END_SCREEN_WORK_OUT_WIDTHS(esd)
	
	//Check if this is a race, enable always expanding stats if yes
	IF ARE_STRINGS_EQUAL(esd.CompletionResultString,"SPR_RESULT") OR (ARE_STRINGS_EQUAL(esd.CompletionResultString,"") AND esd.iElements > 0)
		esd.bAlwaysShowStats = TRUE
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REQUEST_STREAMED_TEXTURE_DICT("MPHud") // 3209804
	ENDIF
	
	IF esd.splash = NULL
		//do the request
		REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
		REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
		REQUEST_STREAMED_TEXTURE_DICT("MPHud")
		//IF bCenterMessageMode
			//esd.splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_CENTERED")
		//ELSE
			esd.splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
		//ENDIF
		esd.bStartedFailformAnim = FALSE
		esd.bstartedMoveUp = FALSE
		CPRINTLN(DEBUG_MISSION_STATS, "<ENDSCREEN_PREPARE> Requested CommonMenu / MPLeaderboard / MPHud")
	ENDIF
	
	// Speirs added to fix PT 1575220
//	IF esd.bShowSkipperPrompt AND esd.button = NULL 
		esd.button = REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")
		CPRINTLN(DEBUG_MISSION_STATS, "<ENDSCREEN_PREPARE> Requested instructional button scaleform. ID: ",  NATIVE_TO_INT(esd.button), ".")
//	ENDIF

	IF bBlockUntilLoaded
		#IF IS_DEBUG_BUILD
		INT iWaitCount = 0
		#ENDIF
		
		WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(esd.splash))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu"))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard"))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud"))
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MISSION_STATS, "<ENDSCREEN_PREPARE> Waiting for scaleform movie and textures to load (iWaitCount: ", iWaitCount, ").")
			iWaitCount++
			#ENDIF
			
			WAIT(0)
		ENDWHILE
		
		IF esd.bShowSkipperPrompt OR esd.bVoteModeEnabled
			WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(esd.button)
				CPRINTLN(DEBUG_MISSION_STATS, "<ENDSCREEN_PREPARE> Waiting for button movie.")
				WAIT(0)
			ENDWHILE
		ENDIF
		
	ELSE
	
		IF (NOT HAS_SCALEFORM_MOVIE_LOADED(esd.splash))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu"))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard"))
		OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPHud"))
			CPRINTLN(DEBUG_MISSION_STATS, "ENDSCREEN_PREPARE: Movie or textures not loaded in none blocking mode, returning false")
			
			RETURN FALSE
		ENDIF
		
		IF esd.bShowSkipperPrompt
			IF  NOT HAS_SCALEFORM_MOVIE_LOADED(esd.button)
				CPRINTLN(DEBUG_MISSION_STATS, "<ENDSCREEN_PREPARE> Waiting for button movie.")
				RETURN FALSE
			ENDIF
		ENDIF
		
	
	ENDIF

//	IF esd.bCenterMessageMode
//	
//		BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "SHOW_CENTERED_MP_MESSAGE")
//	ELSE
//
//		BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "SHOW_CENTERED_MP_MESSAGE")
//	ENDIF
//	
//	
//	
//	IF esd.eSplashType = ESST_REGULAR
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(esd.passed_splash_message)
//	ELSE
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(esd.passed_splash_message)
//			ADD_TEXT_COMPONENT_INTEGER(esd.iSplashSubA)
//			IF esd.eSplashType = ESST_TWO_SUB
//				ADD_TEXT_COMPONENT_INTEGER(esd.iSplashSubB)
//			ENDIF
//		END_TEXT_COMMAND_SCALEFORM_STRING()
//	ENDIF
//	
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")//esd.title) //this will need to be hidden
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)//alpha
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)//anim
//	END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
	
	

	
	
	IF esd.bShowSkipperPrompt
		IF esd.bVoteModeEnabled //NOT USED?? 11/09/2014 lukeAustin. No voting in SP. Hangover from old script.

			ENDSCREEN_ADD_VOTE_BUTTONS(esd)
		
		ELIF esd.iElements != 0
		
			ENDSCREEN_ADD_SKIP_BUTTONS(esd, TRUE)
			
		ELSE
		
			ENDSCREEN_ADD_SKIP_BUTTONS(esd, FALSE)

		ENDIF
			
	ENDIF
	
	g_bResultScreenPrepared= TRUE
	CPRINTLN(DEBUG_MISSION_STATS, "ENDSCREEN_PREPARE: Complete")
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    call to release your end screen scaleform elements
/// PARAMS:
///    esd - 
PROC ENDSCREEN_SHUTDOWN(END_SCREEN_DATASET &esd, BOOL bResetButton = FALSE)
	CPRINTLN(DEBUG_MISSION_STATS, "ENDSCREEN_SHUTDOWN: Marking dataset scaleform as no longer needed")
	IF esd.splash != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(esd.splash)
		esd.splash = NULL
	ENDIF
	
	IF (esd.bShowSkipperPrompt
	OR bResetButton)
	AND esd.button != NULL
		//2285274 - Other scripts may keep this scaleform in memory for other uses.
		//By default turn off mouse input in case they don't need it.
		IF IS_PC_VERSION()
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "TOGGLE_MOUSE_BUTTONS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	
		CPRINTLN(DEBUG_MISSION_STATS, "ENDSCREEN_SHUTDOWN: Set instructional button scaleform as no longer needed.")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(esd.button)
		esd.button = NULL
	ENDIF
	
	IF esd.bNoLoadingScreenEnabled
		SET_NO_LOADING_SCREEN(FALSE)
		esd.bNoLoadingScreenEnabled = FALSE
	ENDIF

	//1321416
	IF NOT g_bEndScreenSuppressFadeIn
		IF NOT IS_PLAYER_DEAD(GET_PLAYER_INDEX())//1454085
		IF NOT g_flowUnsaved.bUpdatingGameflow
			IF IS_SCREEN_FADED_OUT() AND (NOT IS_REPEAT_PLAY_ACTIVE())//1416830
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		ENDIF
	ENDIF
	
	//1925793: Mark stat screen as not displaying, just to be safe
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)

ENDPROC
