
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_fire.sch"
USING "commands_script.sch"
USING "commands_object.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "script_player.sch"
USING "script_ped.sch"
using "rgeneral_include.sch"

STRUCT TRIGGER_BOX
	VECTOR			vMin
	VECTOR			vMax
	FLOAT			flWidth
	BOOL			bHasTriggeredOnce
ENDSTRUCT


FUNC TRIGGER_BOX CREATE_TRIGGER_BOX(VECTOR vMin, VECTOR vMax, FLOAT flWidth)
	TRIGGER_BOX tbRetVal
      
	tbRetVal.vMin = vMin
	tbRetVal.vMax = vMax
	tbRetVal.flWidth = flWidth
	tbRetVal.bHasTriggeredOnce = FALSE
	
	RETURN tbRetVal
ENDFUNC

FUNC BOOL IS_ENTITY_IN_TRIGGER_BOX(TRIGGER_BOX &trigger, ENTITY_INDEX thisEntity)
	IF IS_ENTITY_OK(thisEntity)
		IF IS_ENTITY_IN_ANGLED_AREA(thisEntity, trigger.vMin, trigger.vMax, trigger.flWidth)
			trigger.bHasTriggeredOnce = TRUE
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_TRIGGER_BOX_FIRST_TIME(TRIGGER_BOX &trigger, ENTITY_INDEX thisEntity)
	IF trigger.bHasTriggeredOnce
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_OK(thisEntity)
		IF IS_ENTITY_IN_ANGLED_AREA(thisEntity, trigger.vMin, trigger.vMax, trigger.flWidth)
			trigger.bHasTriggeredOnce = TRUE
			
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TRIGGER_BOX(TRIGGER_BOX &trigger)
	RETURN IS_ENTITY_IN_TRIGGER_BOX(trigger, PLAYER_PED_ID())
ENDFUNC

// can we find a way to use globals for this?
FUNC BOOL IS_ANY_BUDDY_IN_TRIGGER_BOX(TRIGGER_BOX &trigger, PED_INDEX ped1, PED_INDEX ped2 = NULL, PED_INDEX ped3 = NULL, PED_INDEX ped4 = NULL)
	IF IS_ENTITY_IN_TRIGGER_BOX(trigger, ped1)
	OR IS_ENTITY_IN_TRIGGER_BOX(trigger, ped2)
	OR IS_ENTITY_IN_TRIGGER_BOX(trigger, ped3)
	OR IS_ENTITY_IN_TRIGGER_BOX(trigger, ped4)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_TRIGGER_BOX_BEEN_TRIGGERED(TRIGGER_BOX &trigger)
	RETURN trigger.bHasTriggeredOnce
ENDFUNC

PROC RESET_TRIGGER_BOX(TRIGGER_BOX &trigger)
	trigger.bHasTriggeredOnce = FALSE
ENDPROC

PROC SET_PED_TRIGGER_BOX_DEFENSIVE_AREA(PED_INDEX thisPed, TRIGGER_BOX thisTrigger, BOOL bUseCenterAsGoToPosition = FALSE, BOOL bApplyToSecondaryDefensiveArea = FALSE)
	IF IS_ENTITY_OK(thisPed)
		SET_PED_ANGLED_DEFENSIVE_AREA(thisPed, thisTrigger.vMin, thisTrigger.vMax, thisTrigger.flWidth, bUseCenterAsGoToPosition, bApplyToSecondaryDefensiveArea)
	ENDIF
ENDPROC
