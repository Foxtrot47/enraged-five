USING "rage_builtins.sch"
USING "globals.sch"


STRUCT HEADING_PUSH_TRIGGER_DATA
	//setting data
	ENTITY_INDEX triggererEntity
	ENTITY_INDEX targetEntity
	FLOAT fTriggerRange
	FLOAT fTriggerAngle
	FLOAT fTriggerSpeedLimit
	INT iTriggerTime
	
	
	
	
	//internal data
	INT iTriggerTimeCounter
ENDSTRUCT



  
FUNC BOOL UPDATE_HEADING_PUSH_TRIGGER(HEADING_PUSH_TRIGGER_DATA &myDataIn,INT timeDeltaMS)

	//if any of the conditions fail then reset trigger time


	//check both entities exsist	
	
	
	//check they're alive


	//check range
	IF NOT (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(myDataIn.triggererEntity),
								       GET_ENTITY_COORDS(myDataIn.targetEntity)) < myDataIn.fTriggerRange)
		myDataIn.iTriggerTimeCounter = 0
		RETURN FALSE
	ENDIF
	
	//check speed //player max on foot is around 2.0
	/*
	PRINTSTRING("UPDATE_HEADING_PUSH_TRIGGER: speed ")
	PRINTFLOAT(GET_ENTITY_SPEED(myDataIn.triggererEntity))
	PRINTSTRING(" limit ")
	PRINTFLOAT(myDataIn.fTriggerSpeedLimit)
	PRINTNL()
	*/
	
	IF NOT (GET_ENTITY_SPEED(myDataIn.triggererEntity) < myDataIn.fTriggerSpeedLimit)
		myDataIn.iTriggerTimeCounter = 0
		RETURN FALSE
	ENDIF
	
	//check heading
	FLOAT triggerHeading =  GET_ENTITY_HEADING(myDataIn.targetEntity)
	FLOAT entityHeading = GET_ENTITY_HEADING(myDataIn.triggererEntity)

	FLOAT trans = (entityHeading - triggerHeading)
	
	IF trans < -180
		trans = 360 + trans
	ENDIF
	/*
	PRINTLN("UPDATE_HEADING_PUSH_TRIGGER: Trigger heading ", triggerHeading, 
	" entity heading ", entityHeading, 
	" diff ", trans,
	" threshold +/-", myDataIn.fTriggerAngle)
	*/
	
	
	
	

	IF NOT((trans < myDataIn.fTriggerAngle) AND (trans > (-myDataIn.fTriggerAngle)))
		myDataIn.iTriggerTimeCounter = 0
		RETURN FALSE
	ENDIF
	
	
	
	
	//do timing check
	IF (myDataIn.iTriggerTimeCounter > myDataIn.iTriggerTime)
		//fire event and reset struct
		
		RETURN TRUE	
	ELSE
		//update delta and return
		myDataIn.iTriggerTimeCounter += timeDeltaMS
		RETURN FALSE
	ENDIF
	
	
	
	RETURN FALSE
ENDFUNC
















































