USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

ENUM enumVEHGEN_GARAGE_SCENE_Pans
	VEHGEN_GARAGE_SCENE_PAN_enterPed = 0,
	VEHGEN_GARAGE_SCENE_PAN_enterVeh,
	VEHGEN_GARAGE_SCENE_PAN_exitPedOpen,
	VEHGEN_GARAGE_SCENE_PAN_exitVeh,
	VEHGEN_GARAGE_SCENE_PAN_MAX
ENDENUM

ENUM enumVEHGEN_GARAGE_SCENE_Cuts
	VEHGEN_GARAGE_SCENE_CUT_null = 0,
	VEHGEN_GARAGE_SCENE_CUT_MAX
ENDENUM

ENUM enumVEHGEN_GARAGE_SCENE_Markers
	VEHGEN_GARAGE_SCENE_MARKER_enterVeh = 0,
	VEHGEN_GARAGE_SCENE_MARKER_exitVeh,
	VEHGEN_GARAGE_SCENE_MARKER_MAX
ENDENUM

ENUM enumVEHGEN_GARAGE_SCENE_Placers
	VEHGEN_GARAGE_SCENE_PLACER_enterPed = 0,
	VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen,
	VEHGEN_GARAGE_SCENE_PLACER_exitVeh,
	VEHGEN_GARAGE_SCENE_PLACER_MAX
ENDENUM

ENUM enumVEHGEN_GARAGE_SCENE_Points
	VEHGEN_GARAGE_SCENE_POINT_enterVeh = 0,
	VEHGEN_GARAGE_SCENE_POINT_exitVeh,
//	VEHGEN_GARAGE_SCENE_POINT_garageDoor,
	VEHGEN_GARAGE_SCENE_POINT_MAX
ENDENUM

STRUCT STRUCT_VEHGEN_GARAGE_SCENE
	structSceneTool_Pan		mPans[VEHGEN_GARAGE_SCENE_PAN_MAX]
	FLOAT					fHold[VEHGEN_GARAGE_SCENE_PAN_MAX]
	#IF IS_DEBUG_BUILD
	BOOL					bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_MAX]
	#ENDIF
	
	structSceneTool_Marker	mMarkers[VEHGEN_GARAGE_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[VEHGEN_GARAGE_SCENE_PLACER_MAX]
	structSceneTool_Point	mPoints[VEHGEN_GARAGE_SCENE_POINT_MAX]
ENDSTRUCT

FUNC BOOL Private_GET_GARAGE_VEHICLE_WARP_DATA(VEHICLE_GEN_NAME_ENUM eName, STRUCT_VEHGEN_GARAGE_SCENE& scene)
	
	//VEHGEN_GARAGE_SCENE_TYPE_exitPedOpen
	scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].vPos = <<194.6740, -1026.9840, -100.0000>>
	scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitPedOpen].fRot = 285.0000
	
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].mStart.vPos = <<198.9908, -1025.9564, -98.8981>>
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].mStart.vRot = <<-4.5679, -0.0266, 110.7275>>
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].mEnd.vPos = <<198.6016, -1026.1030, -98.9313>>
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].mEnd.vRot = <<-5.3499, -0.0266, 110.7275>>
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fFov = 25.2218
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fShake = 0.2000
	scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen].fDuration = 3.5000
	scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen] = 0.0000
	
	#IF IS_DEBUG_BUILD
//	scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitPedOpen] = TRUE
	#ENDIF
	
	SWITCH eName
		CASE VEHGEN_WEB_CAR_MICHAEL
			//VEHGEN_GARAGE_SCENE_TYPE_enterPed
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos = <<-84.4891, 95.2463, 72.7397>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vRot = <<-1.0236, 0.0007, -126.7439>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vPos = <<-84.1568, 94.9229, 72.7318>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vRot = <<-1.0236, 0.0007, -127.1499>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fFov = 25.1687
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration = 3.5000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed] = 0.0
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos = <<-62.5434, 80.2917, 70.5644>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot = 345.0000
			
			//VEHGEN_GARAGE_SCENE_TYPE_enterVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vPos = <<-61.0750, 81.2555, 84.5867>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vRot = <<-69.9271, 0.0000, 103.8920>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vPos = <<-60.6284, 81.2036, 84.5867>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vRot = <<-73.4239, -0.0000, 108.7693>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fFov = 29.9579
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration = 3.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = 1.0
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos = <<-60.3859, 78.4352, 70.5249>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos = <<-67.0129, 81.9471, 70.5249>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot = <<0.0000, 0.0000, -117.9206>>
			
			//VEHGEN_GARAGE_SCENE_TYPE_exitVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vPos = <<-61.8119, 77.3506, 80.3259>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vRot = <<-57.7470, 0.0000, 34.1968>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vPos = <<-61.8119, 77.3506, 80.3259>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vRot = <<-48.0008, 0.0000, 49.6137>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fFov = 30.0324
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration = 3.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = 1.0
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos = <<-62.6272, 79.7534, 70.6161>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot = <<0.0000, 0.0000, 65.0000>>
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos = <<-79.1059, 87.1091, 70.5157>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos = <<-88.4000, 70.8000, 72.0000>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot = 150.0000
			
//			//VEHGEN_GARAGE_SCENE_garageDoor
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos = <<-72.6200, -1821.0740, 30.9024>>
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot = <<90.0000, 0.0000, -129.1705>>
			
			#IF IS_DEBUG_BUILD
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterPed] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = TRUE
			#ENDIF
			RETURN TRUE
		BREAK
		CASE VEHGEN_WEB_CAR_FRANKLIN
			//VEHGEN_GARAGE_SCENE_TYPE_enterPed
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos = <<-66.6121, -1842.1222, 26.6941>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vRot = <<3.4131, -0.0009, 17.9566>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vPos = <<-66.5137, -1841.6156, 26.7211>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vRot = <<3.1669, -0.0009, 18.0384>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fFov = 25.1856
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration = 3.5000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed] = 0.0000
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos = <<-72.5431, -1820.4470, 25.9424>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot = 140.0000
			
			//VEHGEN_GARAGE_SCENE_TYPE_enterVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vPos = <<-72.7256, -1823.2828, 41.0951>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vRot = <<-81.9335, 0.0000, -58.1415>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vPos = <<-72.9027, -1822.9979, 41.0951>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vRot = <<-80.6657, 0.0000, -29.6554>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fFov = 29.9864
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration = 3.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = 1.0000
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos = <<-76.7939, -1816.7700, 26.6473>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos = <<-69.2183, -1823.2582, 25.9421>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot = <<0.0000, 0.0000, 49.4214>>
			
			//VEHGEN_GARAGE_SCENE_TYPE_exitVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vPos = <<-70.9492, -1820.9363, 45.3628>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vRot = <<-85.5171, 0.0000, 68.3257>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vPos = <<-70.9836, -1821.0228, 45.3628>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vRot = <<-89.4995, 0.0000, 68.3257>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fFov = 28.4803
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration = 4.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = 1.0000
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos = <<-75.3386, -1818.2485, 26.0241>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot = <<0.0000, 0.0000, -132.7699>>
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos = <<-63.9880, -1829.3903, 26.4230>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos = <<-49.7380, -1830.1475, 25.5591>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot = 317.3589
			
//			//VEHGEN_GARAGE_SCENE_garageDoor
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos = <<-72.6200, -1821.0740, 30.9024>>
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot = <<90.0000, 0.0000, -129.1705>>
			
			#IF IS_DEBUG_BUILD
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterPed] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = TRUE
			#ENDIF
			RETURN TRUE
		BREAK
		CASE VEHGEN_WEB_CAR_TREVOR
			//VEHGEN_GARAGE_SCENE_TYPE_enterPed
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos = <<-237.0740, -1170.3906, 23.4790>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vRot = <<7.6190, 0.0000, -69.7398>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vPos = <<-237.2166, -1169.6716, 23.4931>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vRot = <<7.6190, 0.0000, -71.3391>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fFov = 40.0015
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration = 3.5000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterPed] = 0.0000
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos = <<-218.0289, -1162.3920, 22.0242>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot = 15.0000
			
			//VEHGEN_GARAGE_SCENE_TYPE_enterVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vPos = <<-219.5507, -1159.9506, 36.8399>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mStart.vRot = <<-81.7188, -0.0000, 170.4291>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vPos = <<-219.1442, -1160.0190, 36.8399>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].mEnd.vRot = <<-80.5726, -0.0000, -168.9920>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fFov = 30.0319
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh].fDuration = 4.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = 1.0000
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos = <<-211.6438, -1162.4067, 22.0234>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos = <<-221.6438, -1162.4067, 22.0234>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot = <<0.0000, 0.0000, -90.0000>>
			
			//VEHGEN_GARAGE_SCENE_TYPE_exitVeh
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vPos = <<-218.5983, -1159.0609, 31.4452>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mStart.vRot = <<-62.4689, 0.0000, -142.2907>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vPos = <<-218.6533, -1159.0671, 31.4471>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].mEnd.vRot = <<-62.4689, 0.0000, -172.3491>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fFov = 30.0000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fShake = 0.2000
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_exitVeh].fDuration = 3.0000
			scene.fHold[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = 1.0000
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos = <<-214.5676, -1162.6580, 21.9591>>
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot = <<0.0000, 0.0000, 90.0000>>
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitVeh].vPos = <<-227.7619, -1162.8480, 22.0085>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos = <<-221.6000, -1156.3000, 22.6000>>
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot = 0.0000
			
//			//VEHGEN_GARAGE_SCENE_garageDoor
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos = <<-218.0171, -1162.3920, 24.5204>>
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot = <<90.0000, 0.0000, 90.0000>>
			
			#IF IS_DEBUG_BUILD
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterPed] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = TRUE
//			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = TRUE
			#ENDIF
			RETURN TRUE
		BREAK
		
		#IF IS_DEBUG_BUILD
		DEFAULT
			PURCHASABLE_GARAGE_DATA_STRUCT sData
			GET_PURCHASABLE_GARAGE_DATA(sData, eName)
			
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos = sData.vGarageExitCoords[1]
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot = sData.fGarageExitHeading[1] + 180.0
			IF scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot > 360.0
				scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot -= 360.0
			ENDIF
			
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos, scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot, <<0,-10,2>>)
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vRot = <<0, -0.0000, scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vPos = scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vPos + <<2,2,0>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mEnd.vRot = scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].mStart.vRot + <<0,0,0>>
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fFov = 50
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fDuration = 7.0            //7.0
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed].fShake = 0.2
			
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos = scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z = scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].fRot
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos = scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_enterPed].vPos
			scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = scene.mPans[VEHGEN_GARAGE_SCENE_PAN_enterPed]
			
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos = sData.vGarageExitCoords[0]
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z = sData.fGarageExitHeading[0] + 180.0
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterveh].vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos, scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z, <<0,10.0,0>>)
			IF scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z > 180.0
				scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z -= 360.0
			ENDIF
			
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos = scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_enterVeh].vPos
			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z = scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vRot.z+180
			scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitveh].vPos = scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_enterVeh].vPos
			IF scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z > 180.0
				scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z -= 360.0
			ENDIF
			
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vPos = ((scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos+scene.mMarkers[VEHGEN_GARAGE_SCENE_MARKER_exitveh].vPos)/2.0)+<<0,0,4>>
//			scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot = <<90,0,scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z>>
//			IF scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot.z > 180.0
//				scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_garageDoor].vRot.z -= 360.0
//			ENDIF
			
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].vPos = scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vPos
			scene.mPlacers[VEHGEN_GARAGE_SCENE_PLACER_exitVeh].fRot = scene.mPoints[VEHGEN_GARAGE_SCENE_POINT_exitVeh].vRot.z
			
			#IF IS_DEBUG_BUILD
			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterPed] = TRUE
			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_enterVeh] = TRUE
			scene.bPlaceholder[VEHGEN_GARAGE_SCENE_PAN_exitVeh] = TRUE
			#ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
USING "vehgen_garage_scene_debug.sch"
#ENDIF
