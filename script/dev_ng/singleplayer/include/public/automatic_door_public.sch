
//╒═════════════════════════════════════════════════════════════════════════════╕
//│					 		Automatic Door Public Header						│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			20/07/11												│
//│		DESCRIPTION: 	Public commands for interacting with the automatic		│
//│						door system.											│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "commands_entity.sch"
USING "commands_script.sch"
USING "rage_builtins.sch"
USING "globals.sch"


#IF IS_DEBUG_BUILD

FUNC STRING PED_MODEL_NAME(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(ped)
		RETURN GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ped))
	ELSE
		RETURN "*NO MODEL*"
	ENDIF
ENDFUNC


FUNC STRING GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(AUTOMATIC_DOOR_ENUM paramAutoDoorID)
	SWITCH paramAutoDoorID
		CASE AUTODOOR_DOCKS_FRONT_GATE_IN		RETURN "DOCKS_FRONT_GATE_IN"		BREAK
		CASE AUTODOOR_DOCKS_FRONT_GATE_OUT		RETURN "DOCKS_FRONT_GATE_OUT"		BREAK
		CASE AUTODOOR_DOCKS_BACK_GATE_IN		RETURN "DOCKS_BACK_GATE_IN"			BREAK
		CASE AUTODOOR_DOCKS_BACK_GATE_OUT		RETURN "DOCKS_BACK_GATE_OUT"		BREAK
		CASE AUTODOOR_MICHAEL_MANSION_GATE		RETURN "MICHAEL_MANSION_GATE"		BREAK
		CASE AUTODOOR_MIL_DOCKS_GATE_IN			RETURN "MIL_DOCKS_GATE_IN"			BREAK
		CASE AUTODOOR_MIL_DOCKS_GATE_OUT		RETURN "MIL_DOCKS_GATE_OUT"			BREAK
		CASE AUTODOOR_MIL_BASE_GATE_IN			RETURN "MIL_BASE_GATE_IN"			BREAK
		CASE AUTODOOR_MIL_BASE_GATE_OUT			RETURN "MIL_BASE_GATE_OUT"			BREAK
		CASE AUTODOOR_CULT_GATE_LEFT			RETURN "CULT_GATE_LEFT"				BREAK
		CASE AUTODOOR_CULT_GATE_RIGHT			RETURN "CULT_GATE_RIGHT"			BREAK
		CASE AUTODOOR_DTOWN_VINEWOOD_GARAGE		RETURN "DTOWN_VINEWOOD_GARAGE"		BREAK
		CASE AUTODOOR_FRAN_HILLS_GARAGE			RETURN "FRAN_HILLS_GARAGE"			BREAK
		CASE AUTODOOR_SC1_COP_CARPARK			RETURN "SC1_COP_CARPARK"			BREAK
		CASE AUTODOOR_DEVIN_GATE_L				RETURN "DEVIN_GATE_L"				BREAK
		CASE AUTODOOR_DEVIN_GATE_R				RETURN "DEVIN_GATE_R"				BREAK
		CASE AUTODOOR_AIRPORT_BARRIER_OUT		RETURN "AIRPORT_BARRIER_OUT"		BREAK
		CASE AUTODOOR_AIRPORT_BARRIER_IN		RETURN "AIRPORT_BARRIER_IN"			BREAK
		CASE AUTODOOR_IMPOUND_L					RETURN "IMPOUND_L"					BREAK
		CASE AUTODOOR_IMPOUND_R					RETURN "IMPOUND_R"					BREAK
		CASE AUTODOOR_MIL_BASE_BARRIER_IN		RETURN "MIL_BASE_BARRIER_IN"		BREAK
		CASE AUTODOOR_MIL_BASE_BARRIER_OUT		RETURN "MIL_BASE_BARRIER_OUT"		BREAK
		CASE AUTODOOR_AIRPORT_ALT_GATES_L		RETURN "AIRPORT_ALT_GATES_L"		BREAK
		CASE AUTODOOR_AIRPORT_ALT_GATES_R		RETURN "AIRPORT_ALT_GATES_R"		BREAK
		CASE AUTODOOR_HAYES_GARAGE				RETURN "HAYES_GARAGE"				BREAK
		CASE AUTODOOR_DRAINSERVICE_L			RETURN "DRAINSERVICE_L"				BREAK
		CASE AUTODOOR_DRAINSERVICE_R			RETURN "DRAINSERVICE_R"				BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_AUTOMATIC_DOOR_DEBUG_STRING: No debug string assigned for passed autodoor enum.")
	RETURN "ERROR!"
ENDFUNC


FUNC STRING GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_INDEX(INT paramAutoDoorIndex)
	RETURN GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(INT_TO_ENUM(AUTOMATIC_DOOR_ENUM, paramAutoDoorIndex))
ENDFUNC

#ENDIF


FUNC BOOL IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTOMATIC_DOOR_ENUM eDoor, PED_INDEX pedToCheck)
	INT index
	REPEAT g_sAutoDoorData[eDoor].registeredPedCount index
		IF pedToCheck = g_sAutoDoorData[eDoor].registeredPed[index]
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


PROC REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTOMATIC_DOOR_ENUM eDoor, PED_INDEX pedToRegister)

	CDEBUG1LN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " registering ped[", NATIVE_TO_INT(pedToRegister), "|", PED_MODEL_NAME(pedToRegister), "] with door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), ".")
	
	IF NOT DOES_ENTITY_EXIST(pedToRegister)
		SCRIPT_ASSERT("REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: Ped doesn't exist.")
		CDEBUG1LN(DEBUG_DOOR, "Ped doesn't exist.")
		EXIT
	ENDIF
	
	IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(eDoor, pedToRegister)
		CDEBUG1LN(DEBUG_DOOR, "Ped already registered.")
		EXIT
	ENDIF
	
	IF g_sAutoDoorData[eDoor].registeredPedCount < MAX_AUTODOOR_REGISTERED_PEDS
		CDEBUG1LN(DEBUG_DOOR, "Ped registered sucessfully.")
		g_sAutoDoorData[eDoor].registeredPed[g_sAutoDoorData[eDoor].registeredPedCount] = pedToRegister
		g_sAutoDoorData[eDoor].registeredPedCount++
	ELSE
	
		PRINTLN("Max registered peds limit has been reached for door ", eDoor)
		
		// Find if there is a free slot somewhere..
		INT iRegisteredPed
		PED_INDEX pedRegistered
		BOOL bDeadPedFound = FALSE
		REPEAT MAX_AUTODOOR_REGISTERED_PEDS iRegisteredPed
			pedRegistered = g_sAutoDoorData[eDoor].registeredPed[iRegisteredPed]
			
			IF NOT DOES_ENTITY_EXIST(pedRegistered)
			OR IS_PED_INJURED(pedRegistered)
				PRINTLN("REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: We have found a dead ped in the array of registered peds. Updating ped in slot ", iRegisteredPed)
				g_sAutoDoorData[eDoor].registeredPed[iRegisteredPed] = pedToRegister
				bDeadPedFound = TRUE
				// Bail out of the loop.
				iRegisteredPed = MAX_AUTODOOR_REGISTERED_PEDS+1
			ENDIF
		ENDREPEAT
		
		IF NOT bDeadPedFound
			PRINTLN("REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: Failed to find a dead ped, unable to register ped with index ", NATIVE_TO_INT(pedToRegister))
			SCRIPT_ASSERT("REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: Max registered peds limit has been reached, failed to find slot")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_DOOR, "Peds registered with door ", GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), ":")
		INT iPed
		REPEAT g_sAutoDoorData[eDoor].registeredPedCount iPed
				CDEBUG1LN(DEBUG_DOOR, iPed, " ped[", NATIVE_TO_INT(g_sAutoDoorData[eDoor].registeredPed[iPed]), "|", PED_MODEL_NAME(g_sAutoDoorData[eDoor].registeredPed[iPed]), "]")
		ENDREPEAT
	#ENDIF

ENDPROC


PROC UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTOMATIC_DOOR_ENUM eDoor, PED_INDEX pedToUnregister, BOOL pedToUnregisterIsNonExistent = FALSE)
	
	CDEBUG1LN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " unregistering ped[", NATIVE_TO_INT(pedToUnregister), "|", PED_MODEL_NAME(pedToUnregister), "] with door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), ".")

	IF NOT pedToUnregisterIsNonExistent
		IF NOT DOES_ENTITY_EXIST(pedToUnregister)
			SCRIPT_ASSERT("UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: Ped doesn't exist.")
			CDEBUG1LN(DEBUG_DOOR, "Ped doesn't exist.")
			EXIT
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_DOOR, "UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR: Ped doesn't exist.")
	ENDIF
	
	IF g_sAutoDoorData[eDoor].registeredPedCount = 0
		CDEBUG1LN(DEBUG_DOOR, "No peds registered to unregister.")
		EXIT
	ENDIF

	INT index
	BOOL bPedFound = FALSE
	REPEAT g_sAutoDoorData[eDoor].registeredPedCount index
		IF bPedFound
			//Ped being removed was registered in a slot before this.
			//Shift registered ped down one index.
			g_sAutoDoorData[eDoor].registeredPed[index-1] = g_sAutoDoorData[eDoor].registeredPed[index]
			g_sAutoDoorData[eDoor].registeredPed[index] = NULL
		ELSE
			IF pedToUnregister = g_sAutoDoorData[eDoor].registeredPed[index]
				g_sAutoDoorData[eDoor].registeredPed[index] = NULL
				bPedFound = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF bPedFound
		CDEBUG1LN(DEBUG_DOOR, "Ped unregistered sucessfully.")
		g_sAutoDoorData[eDoor].registeredPedCount--
	ELSE
		CDEBUG1LN(DEBUG_DOOR, "Ped not registered.")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT pedToUnregisterIsNonExistent
			CDEBUG1LN(DEBUG_DOOR, "Peds registered with door ", GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), ":")
			INT iPed
			REPEAT g_sAutoDoorData[eDoor].registeredPedCount iPed
					CDEBUG1LN(DEBUG_DOOR, iPed, " ped[", NATIVE_TO_INT(g_sAutoDoorData[eDoor].registeredPed[iPed]), "|", PED_MODEL_NAME(g_sAutoDoorData[eDoor].registeredPed[iPed]), "]")
			ENDREPEAT
		ENDIF
	#ENDIF
	
ENDPROC


PROC LOCK_AUTOMATIC_DOOR_OPEN(AUTOMATIC_DOOR_ENUM eDoor, BOOL lockDoorOpen, BOOL snapToPosition = TRUE)
	IF lockDoorOpen
		CPRINTLN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " is flagging door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), " to be locked open.")
		SET_BIT(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_LOCK_OPEN)
		
		IF snapToPosition
			IF IS_BIT_SET(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_REVERSE_OPEN_RATIO)
				g_sAutoDoorData[eDoor].currentOpenRatio = -1.0
			ELSE
				g_sAutoDoorData[eDoor].currentOpenRatio = 1.0
			ENDIF
			DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[eDoor].doorID, g_sAutoDoorData[eDoor].currentOpenRatio, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[eDoor].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
		ELSE
			// let the building controller script pick it up
		ENDIF
	ELSE
		CPRINTLN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " is unflagging door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), " to be locked open.")
		CLEAR_BIT(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_LOCK_OPEN)
		
		IF snapToPosition
			g_sAutoDoorData[eDoor].currentOpenRatio = 0.0
			DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[eDoor].doorID, g_sAutoDoorData[eDoor].currentOpenRatio, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[eDoor].doorID, DOORSTATE_LOCKED, FALSE, TRUE)
		ELSE
			// let the building controller script pick it up
		ENDIF
	ENDIF
ENDPROC


PROC FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTOMATIC_DOOR_ENUM eDoor, BOOL bForceOpen)
	IF bForceOpen
		CPRINTLN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " is flagging door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), " to slide open autonomously.")
		SET_BIT(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_ALWAYS_OPEN)
	ELSE
		CPRINTLN(DEBUG_DOOR, GET_THIS_SCRIPT_NAME(), " is unflagging door ",  GET_AUTOMATIC_DOOR_DEBUG_STRING_FROM_ID(eDoor), " to slide open automatically.")
		CLEAR_BIT(g_sAutoDoorData[eDoor].settingsBitset, BIT_AUTODOOR_ALWAYS_OPEN)
	ENDIF
ENDPROC

