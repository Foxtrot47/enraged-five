USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_stats.sch"
USING "stats_enums.sch"
USING "shared_debug_text.sch"
USING "finance_modifiers_public.sch"
USING "completionpercentage_public.sch"
USING "script_clock.sch"
USING "commands_socialclub.sch"
USING "commands_stats.sch"
USING "flow_help_public.sch"
USING "achievement_public.sch"
USING "net_system_activity_feed.sch"


CONST_INT	BAWSAQ_ONLINE_SYNC_TIMER				120000	//Every 2 minutes. The frequency at which we attempt to read online stock values for BAWSAQ.
CONST_INT	STOCK_MARKET_LOG_UPDATE_MINS			480		//In-game minutes between each log update. 480 mins = 8 hours = 3 updates per day.
CONST_INT	STOCK_MARKET_TICK_UPDATES_PER_LOG		16		//The number of simulation tick updates in between each log update.
CONST_INT	STOCK_MARKET_LOG_UPDATES_PER_DAY		3		//The number of log tick updates each in-game day.


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   finance_control_public.sch
//      AUTHOR          :   Ak
//      DESCRIPTION     :   Interfaces allowing transactions involving the player's account/s 
//                          and the stock market
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/// PURPOSE:
///    Helper function for reading stock totals from player portfolio
/// PARAMS:
///    iChar - char id
///    entry - portfolio index
/// RETURNS:
///    Amount of shares owned by character.
FUNC INT GET_OWNED_TOTAL_FROM_PORTFOLIO(INT iChar, INT entry)
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		RETURN 0
	ENDIF

	SWITCH iChar
		CASE 0		RETURN g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry]			BREAK
		CASE 1		RETURN g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry]		BREAK
		CASE 2		RETURN g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry]		BREAK
	ENDSWITCH
	
	// Invalid char.
	RETURN 0
ENDFUNC


/// PURPOSE:
///    Searches for the total owned in the specific company by the player
/// PARAMS:
///    iChar - 
///    companyIndex - 
FUNC INT GET_OWNED_TOTAL_FROM_PORTFOLIO_BY_COMPANY(INT iChar, INT companyIndex)
	
	INT i
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		SWITCH iChar
			//Michael.
			CASE 0
				IF g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =  companyIndex
					IF g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i] > 0
						RETURN g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i]
					ENDIF
				ENDIF
			BREAK
			
			//Franklin.
			CASE 1
				IF g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] =  companyIndex
					IF g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i] > 0
						RETURN g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i]
					ENDIF
				ENDIF
			BREAK
			
			//Trevor.
			CASE 2 
				IF g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] =  companyIndex
					IF g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i] > 0
						RETURN g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i]
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	RETURN 0
ENDFUNC


/// PURPOSE:
///    Updates the owned totals for a specific player character. 
///    This is so that the social club can track current owned values.
PROC UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(INT paramCharIndex)
	CDEBUG1LN(DEBUG_BAWSAQ, "Updating character ", paramCharIndex, "'s owned stats for social club.")

	INT i
	REPEAT BS_CO_TOTAL_LISTINGS i
		IF g_BS_Listings[i].bOnlineStock
			INT si = g_BS_Listings[i].StatIndex
			IF si > -1
				INT iStocksOwned = GET_OWNED_TOTAL_FROM_PORTFOLIO_BY_COMPANY(paramCharIndex, i)
				
				IF g_BS_Listings[i].fCurrentPrice <= 0
					iStocksOwned = 0 // Currently pretend that the stocks owned is 0, because the current price is a bad value
					CDEBUG1LN(DEBUG_BAWSAQ, "Invalid current price for index: ", i, " Currently pretend that the stocks owned is 0, because the current price is a bad value")				
				ENDIF				
				CDEBUG1LN(DEBUG_BAWSAQ, "Setting stocks owned to ", iStocksOwned, " for SC stat with hash ", g_BS_OIndexData[si].OnlineCharOwned[paramCharIndex], ".")
				STAT_SET_INT(g_BS_OIndexData[si].OnlineCharOwned[paramCharIndex], iStocksOwned)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Updates the owned totals for all player characters. 
///    This is so that the social club can track current owned values.
PROC UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS()
	CDEBUG1LN(DEBUG_BAWSAQ, "Updating portfolio owned stats for all characters.")

	UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(0)
	UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(1)
	UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(2)
ENDPROC


/// PURPOSE:
///    Gets company index for the given player portfolio index
/// PARAMS:
///    iChar - 
///    entry - 
/// RETURNS:
///    
FUNC BAWSAQ_COMPANIES GET_COMPANY_INDEX_FROM_PORTFOLIO(INT iChar, INT entry)
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		RETURN BS_CO_SHT
	ENDIF
	
	SWITCH iChar
		CASE 0
			RETURN INT_TO_ENUM(BAWSAQ_COMPANIES, g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[entry])
		CASE 1
			RETURN INT_TO_ENUM(BAWSAQ_COMPANIES,g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[entry])
		CASE 2
			RETURN INT_TO_ENUM(BAWSAQ_COMPANIES,g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[entry])
	ENDSWITCH
	
	// Invalid char.
	RETURN BS_CO_SHT
ENDFUNC

 
/// PURPOSE:
///    Sets company index for the given player at the given portfolio index.
PROC SET_COMPANY_INDEX_FROM_PORTFOLIO(INT iChar, INT entry, BAWSAQ_COMPANIES bsco)
	//MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		EXIT
	ENDIF
	
	SWITCH iChar
		CASE 0
			g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[entry] = ENUM_TO_INT(bsco)
			BREAK
		CASE 1
			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[entry] = ENUM_TO_INT(bsco)
			BREAK
		CASE 2
			g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[entry] = ENUM_TO_INT(bsco)
			BREAK
	ENDSWITCH
	
	//invalid char
	EXIT
ENDPROC


/// PURPOSE:
///    REturns how much of the given stock the player owns
/// PARAMS:
///    coindex -  the company to look for
/// RETURNS:
///    the amount of stock owned of that company by the player
FUNC INT GET_PLAYER_AMOUNT_OF_STOCK_OWNED( BAWSAQ_COMPANIES coindex, BAWSAQ_TRADERS trader) 
	INT i = 0
	CPRINTLN(DEBUG_STOCKS, "GET_PLAYER_AMOUNT_OF_STOCK_OWNED: company - ", coindex, "  trader - ", trader, ".")
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		IF GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(trader),i) = coindex
			IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i) > 0
				RETURN GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i)
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN 0
ENDFUNC




/// PURPOSE:
///    Sets the invested value for the given share in the player's portfolio
/// PARAMS:
///    iChar - 
///    entry - 
///    val - 
///    increment - 
PROC SET_INVESTED_TOTAL_FROM_PORTFOLIO(INT iChar,INT entry, FLOAT val, BOOL increment = FALSE)
	//MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		EXIT
	ENDIF
	
	IF increment
		SWITCH iChar
			CASE 0
				g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] += val
				IF g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] = 0
				ENDIF
				
				BREAK
			CASE 1
				g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] += val 
				IF g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] = 0
				ENDIF
				BREAK
			CASE 2
				g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] += val
				IF g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] = 0
				ENDIF
				BREAK
		ENDSWITCH
	ELSE
		SWITCH iChar
			CASE 0
				g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] = val
				IF g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry] = 0
				ENDIF
				BREAK
			CASE 1
				g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] = val 
				IF g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry] = 0
				ENDIF
				BREAK
			CASE 2
				g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] = val
				IF g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] < 0
					g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry] = 0
				ENDIF
				BREAK
		ENDSWITCH
	ENDIF
	//invalid char
	EXIT

ENDPROC


/// PURPOSE:
///    Sets the owned value for the given portfolio entry
/// PARAMS:
///    iChar - 
///    entry - 
///    ownedval - 
///    increment - 
PROC SET_OWNED_TOTAL_FROM_PORTFOLIO(INT iChar,INT entry, INT ownedval, BOOL increment = false)

	//MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		EXIT
	ENDIF
	
	
	BAWSAQ_COMPANIES stock_name
	SWITCH iChar
		CASE 0
			//g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry] += ownedval
			stock_name = INT_TO_ENUM( BAWSAQ_COMPANIES, g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[entry])
			BREAK
		CASE 1
			//g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry] += ownedval
			stock_name = INT_TO_ENUM( BAWSAQ_COMPANIES,g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[entry])
			BREAK
		CASE 2 
			//g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry] += ownedval
			stock_name = INT_TO_ENUM( BAWSAQ_COMPANIES,g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[entry])
			BREAK
	ENDSWITCH
	
	IF g_BS_Listings[stock_name].fCurrentPrice <= 0
		CPRINTLN(DEBUG_STOCKS, "Bailed changing owned total for char ", iChar, "'s portfolio index ", entry, " due to stock having invalid current price.")
		EXIT
	ENDIF
	
	INT nVal = 0

	IF increment
		SWITCH iChar
			CASE 0
				g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry] += ownedval
				nVal = g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry]
				BREAK
			CASE 1
				g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry] += ownedval
				nVal = g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry]
				BREAK
			CASE 2 
				g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry] += ownedval
				nVal = g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry]
				BREAK
		ENDSWITCH
	ELSE
		SWITCH iChar
			CASE 0
				g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry] = ownedval
				nVal = g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[entry]
				BREAK
			CASE 1
				g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry] = ownedval
				nVal = g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[entry]
				BREAK
			CASE 2 
				g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry] = ownedval
				nVal = g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[entry]
				BREAK
		ENDSWITCH
	ENDIF
	
	//fix for 1612899
	IF g_BS_Listings[stock_name].bOnlineStock
		INT si = g_BS_Listings[stock_name].StatIndex
		IF si > -1
			STAT_SET_INT(g_BS_OIndexData[si].OnlineCharOwned[iChar] , nVal)
		ENDIF
	ENDIF

	//invalid char
	EXIT
ENDPROC


/// PURPOSE:
///    Snapshots the bank and stock system state to a buffer
///    Used on repeat play and checkpoints
/// PARAMS:
///    bCheckpoint - checkpoint or from start
PROC COPY_BANK_TRANSLOGS_CLF(BOOL bCheckpoint = true)
	CPRINTLN(DEBUG_STOCKS, "Snapshotting stock and bank state to be used for repeat play and checkpoint restoration...")
	
	INT i = 0
	INT j = 0
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint 			= g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint 	= g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint 		= g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0] 			= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
		g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0] 				= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0] 					= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_OWNED[i]

		g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0] 		= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0] 			= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0] 				= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_OWNED[i]

		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0] 			= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0] 				= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0] 					= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_OWNED[i]
		
		IF NOT bCheckpoint
			g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1] 		= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1] 			= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1] 				= g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_OWNED[i]

			g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1] 	= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1] 		= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1] 			= g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_OWNED[i]

			g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1] 		= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1] 			= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1] 				= g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_OWNED[i]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Snapshotting finished.")
ENDPROC


/// PURPOSE:
///    Snapshots the bank and stock system state to a buffer
///    Used on repeat play and checkpoints
/// PARAMS:
///    bCheckpoint - checkpoint or from start
PROC COPY_BANK_TRANSLOGS_NRM(BOOL bCheckpoint = true)
	CPRINTLN(DEBUG_STOCKS, "Snapshotting stock and bank state to be used for repeat play and checkpoint restoration...")
	
	INT i = 0
	INT j = 0
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint 			= g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint 	= g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint 		= g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0] 			= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
		g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0] 				= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0] 					= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_OWNED[i]

		g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0] 		= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0] 			= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0] 				= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_OWNED[i]

		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0] 			= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0] 				= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0] 					= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_OWNED[i]
		
		IF NOT bCheckpoint
			g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1] 		= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1] 			= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1] 				= g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_OWNED[i]

			g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1] 	= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1] 		= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1] 			= g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_OWNED[i]

			g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1] 		= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1] 			= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1] 				= g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_OWNED[i]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Snapshotting finished.")
ENDPROC


/// PURPOSE:
///    Snapshots the bank and stock system state to a buffer
///    Used on repeat play and checkpoints
/// PARAMS:
///    bCheckpoint - checkpoint or from start
PROC COPY_BANK_TRANSLOGS(BOOL bCheckpoint = true)
	
	#if USE_CLF_DLC
		if g_bLoadedClifford
			COPY_BANK_TRANSLOGS_CLF()
			exit
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			COPY_BANK_TRANSLOGS_NRM()
			EXIT
		ENDIF
	#ENDIF
	
	CPRINTLN(DEBUG_STOCKS, "Snapshotting stock and bank state to be used for repeat play and checkpoint restoration...")
	
	INT i = 0
	INT j = 0
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint = 			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint = 	g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint = 		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0] = 		g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
		g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0] = 			g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0] = 				g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i]

		g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0] = 	g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0] = 		g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0] = 			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i]

		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0] = 	g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0] = 		g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[i]
		g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0] = 			g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i]
		
		IF NOT bCheckpoint
			g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1] = 		g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] 
			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1] = 			g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1] = 				g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i]

			g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1] = 	g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1] = 		g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1] = 			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i]

			g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1] = 	g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1] = 		g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[i]
			g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1] = 			g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Snapshotting finished.")
ENDPROC


/// PURPOSE:
///    Restores the bank and stock system state from a buffer.
///    Used on repeat play and checkpoints 
PROC PASTE_BANK_TRANSLOGS_CLF(BOOL bCheckpoint = TRUE)
	CPRINTLN(DEBUG_STOCKS, "Restoring stock and bank state snapshot after repeat play or checkpoint load...")
	
	INT i, j
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType = 			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource = 	g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint
			g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree = 		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint
		ENDREPEAT
	ENDREPEAT

	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		IF bCheckpoint
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0]
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0]

			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0]
			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0]

			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0]
			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0]
		ELSE
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1]
			g_savedGlobalsClifford.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1]

			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1]
			g_savedGlobalsClifford.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1]

			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1]
			g_savedGlobalsClifford.sFinanceData.TREVOR_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Restoring of stock and bank state snapshot finished.")
ENDPROC


/// PURPOSE:
///    Restores the bank and stock system state from a buffer.
///    Used on repeat play and checkpoints 
PROC PASTE_BANK_TRANSLOGS_NRM(BOOL bCheckpoint = TRUE)
	CPRINTLN(DEBUG_STOCKS, "Restoring stock and bank state snapshot after repeat play or checkpoint load...")
	
	INT i, j
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType = 			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource = 	g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint
			g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree = 		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint
		ENDREPEAT
	ENDREPEAT

	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		IF bCheckpoint
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0]
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0]

			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0]
			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0]

			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0]
			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0]
		ELSE
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1]
			g_savedGlobalsnorman.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1]

			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1]
			g_savedGlobalsnorman.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1]

			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1]
			g_savedGlobalsnorman.sFinanceData.TREVOR_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Restoring of stock and bank state snapshot finished.")
ENDPROC


/// PURPOSE:
///    Restores the bank and stock system state from a buffer.
///    Used on repeat play and checkpoints 
PROC PASTE_BANK_TRANSLOGS(BOOL bCheckpoint = TRUE)
	#IF USE_CLF_DLC
		IF g_bLoadedClifford
			PASTE_BANK_TRANSLOGS_CLF()
			EXIT
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman
			PASTE_BANK_TRANSLOGS_NRM()
			EXIT
		ENDIF
	#ENDIF

	CPRINTLN(DEBUG_STOCKS, "Restoring stock and bank state snapshot after repeat play or checkpoint load...")
	
	INT i, j
	REPEAT MAX_BANK_ACCOUNTS i
		REPEAT MAX_BANK_ACCOUNT_LOG_ENTRIES j
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eType = 		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eTypeCheckpoint
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSource = 	g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].eBaacSourceCheckpoint
			g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegree = 		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[j].iDegreeCheckpoint
		ENDREPEAT
	ENDREPEAT

	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		IF bCheckpoint
			g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][0]
			g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][0]

			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][0]
			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][0]

			g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][0]
			g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][0]
			g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][0]
		ELSE
			g_savedGlobals.sFinanceData.MIKE_SHARES_COMPANY_INDEX[i] =		g_sPortfolioSnapshot.MIKE_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[i] = 			g_sPortfolioSnapshot.MIKE_SHARES_INVESTED[i][1]
			g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i] = 				g_sPortfolioSnapshot.MIKE_SHARES_OWNED[i][1]

			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.FRANKLIN_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.FRANKLIN_SHARES_INVESTED[i][1]
			g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.FRANKLIN_SHARES_OWNED[i][1]

			g_savedGlobals.sFinanceData.TREVOR_SHARES_COMPANY_INDEX[i] = 	g_sPortfolioSnapshot.TREVOR_SHARES_COMPANY_INDEX[i][1]
			g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[i] = 		g_sPortfolioSnapshot.TREVOR_SHARES_INVESTED[i][1]
			g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i] = 			g_sPortfolioSnapshot.TREVOR_SHARES_OWNED[i][1]
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "...Restoring of stock and bank state snapshot finished.")
ENDPROC


/// PURPOSE:
///    This exists because circular includes
/// PARAMS:
///    paramMissionID -   
FUNC BOOL LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSIONS paramMissionID)
#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sFlow.missionSavedData[paramMissionID].completed
#endif
#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sFlow.missionSavedData[paramMissionID].completed
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	RETURN g_savedGlobals.sFlow.missionSavedData[paramMissionID].completed
#endif
#endif
ENDFUNC


// Market filter flags.
CONST_INT MFFILTER_POSITIVE 					0 
CONST_INT MFFILTER_RESET_POST_EXILE3 			1 //full effect then raise to half after exile three
CONST_INT MFFILTER_TIMESCALE_WEEKS 				2 //if not weeks then in days
CONST_INT MFFILTER_DURATION_1 					3 
CONST_INT MFFILTER_DURATION_2 					4 
CONST_INT MFFILTER_DURATION_4 					5 
CONST_INT MFFILTER_RAMP_UP 						6 
CONST_INT MFFILTER_RAMP_UP_ENDSLOW 				7 
CONST_INT MFFILTER_RAMP_UP_PROPORTION_HALF 		8 
CONST_INT MFFILTER_RAMP_UP_PROPORTION_QUARTER 	9 
CONST_INT MFFILTER_RAMP_UP_PROPORTION_EIGHTH 	10 
CONST_INT MFFILTER_RAMP_DOWN 					11 
CONST_INT MFFILTER_RAMP_DOWN_STARTSLOW 			12 
CONST_INT MFFILTER_RAMP_DOWN_PROPORTION_HALF 	13 
CONST_INT MFFILTER_RAMP_DOWN_PROPORTION_QUARTER	14 
CONST_INT MFFILTER_RAMP_DOWN_PROPORTION_EIGHTH 	15 
CONST_INT MFFILTER_SCALE_50_PERC 				16 
CONST_INT MFFILTER_SCALE_25_PERC 				17 
CONST_INT MFFILTER_SCALE_10_PERC 				18 
CONST_INT MFFILTER_SCALE_33_PERC 				19 
CONST_INT MFFILTER_DURATION_8 					20 


/// PURPOSE:
///    Takes and duration and flag bitset, returns a scaling value.
FUNC FLOAT GET_BS_FILTER_FROM_DURATION_FLAGS(INT iDurationRemaining, INT iFlags)

	FLOAT actionScale = 0.0

	IF iDurationRemaining < 1 
		RETURN 1.0 // No effect.
	ENDIF
	BOOL bPositive = IS_BIT_SET(iFlags,MFFILTER_POSITIVE)
		
	// INT iSec,iMINs,iHour,iDay,iMonths,iYears
	// 2 min -> 1 hour
	// Minutes to hours, div 2
	FLOAT fGameDurationLeft = TO_FLOAT(iDurationRemaining) //(iDurationRemaining*10)/2
	
	//Used to be units, now means days
	INT iTotalUnits = 0
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_1)
		iTotalUnits += 1
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_2)
		iTotalUnits += 2
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_4)
		iTotalUnits += 4
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_8)
		iTotalUnits += 8
	ENDIF
	
	//Used to be 8640000 (1000*60*60*24), was changed to units
	FLOAT fGameDurationTotal = TO_FLOAT(STOCK_MARKET_LOG_UPDATES_PER_DAY * iTotalUnits)
	IF IS_BIT_SET(iFlags,MFFILTER_TIMESCALE_WEEKS)
		fGameDurationTotal *= 7
	ENDIF
	
	IF IS_BIT_SET(iFlags,MFFILTER_RESET_POST_EXILE3)
		//IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED]
		//	RETURN 1.0 //effect over
		//ENDIF
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
			RETURN 1.0	//No effect
		ENDIF
		#ENDIF
		#ENDIF
	ELSE
		IF fGameDurationTotal < 1
			RETURN 1.0 // No effect.
		ENDIF
	ENDIF
	
	FLOAT processDelta = 1.0 - (fGameDurationLeft/fGameDurationTotal) // Flipped because duration remaining is counting down.
	CDEBUG1LN(debug_stocks,"Filter duration Left/Total: ",fGameDurationLeft,"/",fGameDurationTotal," Delta: ",processDelta)

	IF IS_BIT_SET(iFlags,MFFILTER_SCALE_50_PERC)
		actionScale -= 0.50
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_SCALE_25_PERC)
		actionScale -= 0.25
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_SCALE_10_PERC)
		actionScale -= 0.10
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_SCALE_33_PERC)
		actionScale -= 0.33
	ENDIF
	IF bPositive
		actionScale = -actionScale
	ENDIF

	IF NOT IS_BIT_SET(iFlags,MFFILTER_RESET_POST_EXILE3)
	
		// Check if it's in it's ramp up period and modify the actionscale.
		IF IS_BIT_SET(iFlags,MFFILTER_RAMP_UP)
			FLOAT upprop = 0.0
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_UP_PROPORTION_HALF)
				upprop += 0.50
			ENDIF
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_UP_PROPORTION_QUARTER)
				upprop += 0.25
			ENDIF
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_UP_PROPORTION_EIGHTH)
				upprop += 0.125
			ENDIF

			IF processDelta < upprop
				FLOAT upprog = processDelta/upprop
				IF IS_BIT_SET(iFlags,MFFILTER_RAMP_UP_ENDSLOW)
					upprog = 1.0 - upprog
					upprog *= upprog
					upprog = 1.0 - upprog
				ENDIF
				actionScale *= upprog
			ENDIF
			
		ENDIF
		
		// Check if it's in it's ramp down period and modify the actionscale.
		IF IS_BIT_SET(iFlags,MFFILTER_RAMP_DOWN)
			FLOAT downprop = 0.0
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_DOWN_PROPORTION_HALF)
				downprop += 0.50
			ENDIF
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_DOWN_PROPORTION_QUARTER)
				downprop += 0.25
			ENDIF
			IF IS_BIT_SET(iFlags,MFFILTER_RAMP_DOWN_PROPORTION_EIGHTH)
				downprop += 0.125
			ENDIF
			FLOAT fromend = 1.0 - downprop
			
			// Modify.
			IF processDelta > fromend
				FLOAT downprog = (processDelta-fromend)/downprop
				IF IS_BIT_SET(iFlags,MFFILTER_RAMP_DOWN_STARTSLOW)
					downprog *= downprog
				ENDIF
				actionScale *= (1.0 - downprog)
			ENDIF
		ENDIF
		
	ENDIF
	
	actionScale = 1.0 + actionScale
	
	RETURN actionScale
ENDFUNC


/// PURPOSE:
///    Obtains current price filter scaling value for the given company.  
FUNC FLOAT GET_BS_PRICE_FILTER(BAWSAQ_COMPANIES b)
#if USE_CLF_DLC
	b = b
	RETURN 1.0
#ENDIF
#if USE_NRM_DLC
	b = b
	RETURN 1.0
#ENDIF

#if not USE_CLF_DLC
#if not USE_NRM_DLC	
	FLOAT FilterScale  = 1.0
	
	// Apply flow modifiers to filter. These are permenant effects based on progress
	// of Merryweather.
	SWITCH b
		CASE BS_CO_LFI		
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED]
				FilterScale = 0.5
			ENDIF		
		BREAK
	
		CASE BS_CO_MER // Merryweather.
			
			// Kill Mike. Huge boost!
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_B)
				FilterScale *= 3.0
			ELSE
				// Port of LS Heists 2A and 2B.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A) 
				OR LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2B)
					FilterScale *= 0.95
				ENDIF
				// Exile 1.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
					FilterScale *= 0.98
				ENDIF
				// Exile 3.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_3)
					FilterScale *= 0.98
				ENDIF
				// Mike 3.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_3)
					FilterScale *= 0.98
				ENDIF

				// Kill trevor // minor drop.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_A)
					FilterScale *= 0.98
				ENDIF
				
				// Save both.
				IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_C2)
					FilterScale *= 0.50
				ENDIF
				
			ENDIF
		BREAK
		
		CASE BS_CO_RIM
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
				FilterScale *= 0.50
			ENDIF
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_CARSTEAL_3)
				FilterScale *= 0.95
			ENDIF
		BREAK
		
		CASE BS_CO_GRU
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_2)
					FilterScale *= 0.90
				ENDIF
			ENDIF
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS)
					FilterScale *= 0.90
				ENDIF
			ENDIF
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2A)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS)
					FilterScale *= 0.90
				ENDIF
			ENDIF
			
		BREAK
		
		CASE BS_CO_VAG
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_2)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
					FilterScale *= 0.70
				ENDIF
			ENDIF
		BREAK
		
		CASE BS_CO_AUG
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS)
				FilterScale *= 2.0
			ENDIF
		BREAK
		
		CASE BS_CO_CLK
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_2)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_FINALE_CREDITS)
					FilterScale *= 0.6
				ENDIF
			ENDIF
		BREAK		
		
		CASE BS_CO_FUS
			IF LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_SOLOMON_3)
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
					FilterScale *= 0.5
				ENDIF
			ENDIF
			BREAK
	ENDSWITCH

	IF g_savedGlobals.sFinanceData.iFiltersRegistered < 1
		RETURN FilterScale
	ENDIF
	
	INT iReg = -1
	INT i = 0
	REPEAT MAX_SP_FINANCE_FILTERS i
		IF g_savedGlobals.sFinanceData.FilteredBind[i] = b
			iReg = i
		ENDIF
	ENDREPEAT
	
	IF iReg > -1
		FilterScale = GET_BS_FILTER_FROM_DURATION_FLAGS(g_savedGlobals.sFinanceData.iFilterDurationRemaining[iReg],g_savedGlobals.sFinanceData.iFilterFlags[iReg])
		
		CDEBUG3LN(DEBUG_STOCKS, "GET_BS_FILTER_FROM_DURATION_FLAGS returned ", FilterScale, " for index ", b)
	ENDIF
	
	IF FilterScale > 1.0
		FLOAT rem = FilterScale - 1.0
		//rem*=GET_PRICE_RISE_SCALER_FOR_ASSET(b)	
		INT bi = ENUM_TO_INT(b)
		INT ito1 = GET_OWNED_TOTAL_FROM_PORTFOLIO(0,bi)
		INT ito2 = GET_OWNED_TOTAL_FROM_PORTFOLIO(1,bi)
		INT ito3 = GET_OWNED_TOTAL_FROM_PORTFOLIO(2,bi)
		
		IF (ito1+ito2+ito3) > 0
			INT cashsum = g_BankAccounts[0].iBalance + g_BankAccounts[1].iBalance + g_BankAccounts[2].iBalance
		
			REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
				IF g_savedGlobals.sFinanceData.MIKE_SHARES_OWNED[i] > 0
					cashsum += floor(g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[i])
				ENDIF
				IF g_savedGlobals.sFinanceData.FRANKLIN_SHARES_OWNED[i] > 0
					cashsum += floor(g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[i])
				ENDIF
				IF g_savedGlobals.sFinanceData.TREVOR_SHARES_OWNED[i] > 0
					cashsum += floor(g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[i])
				ENDIF
			ENDREPEAT

			INT minEffect = 700000000//   700 000 000
			INT maxEffect = 1000000000//1 000 000 000

			IF cashsum > maxEffect
				rem = 0.0
			ELSE
				IF cashsum < minEffect
					rem = 1.0
				ELSE	
					FLOAT degree = TO_FLOAT(cashsum - minEffect)
					FLOAT range = TO_FLOAT(maxEffect- minEffect)
					rem *= (1.0-(degree/range))
				ENDIF
			ENDIF	
			
		ENDIF
		
		FilterScale = 1.0 + rem
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF FilterScale != 1.0
			CPRINTLN(DEBUG_STOCKS, "GET_BS_PRICE_FILTER: Company ", b, " stock multiplied by ", FilterScale)
		ENDIF
	#ENDIF
	
	RETURN FilterScale
	
#ENDIF
#ENDIF
ENDFUNC


/// PURPOSE:
///    Gets the current simulated price for a stock. This is for internal use and isn't what 
///    the player should ever see.
/// PARAMS:
///    index - 
/// RETURNS:
///    
FUNC FLOAT GET_STOCK_PRICE(INT index)
	//see if this price is in the finance price transform list
	BAWSAQ_COMPANIES b = INT_TO_ENUM(BAWSAQ_COMPANIES, index)
	
	FLOAT retprice = (g_BS_Listings[index].fCurrentPrice * GET_BS_PRICE_FILTER(b))

	// Strip off everything after two decimal places
	RETURN TO_FLOAT(FLOOR(retprice*100))/100.0
ENDFUNC


/// PURPOSE:
///    Gets the current stock price as it appears in the log
/// RETURNS:
///    The stock price as it appears at the previous carret index (as the carret was incremented just after update)
FUNC FLOAT GET_CURRENT_STOCK_PRICE(INT iCompany)
	FLOAT fStockPrice

	//Get online data if an online stock
	IF g_BS_Listings[iCompany].bOnlineStock
		fStockPrice = GET_STOCK_PRICE(iCompany)
	ELSE
		//Get last logged value if an offline stock
		INT iLastLoggedPrice = g_BS_Listings[iCompany].iLogIndexCaret - 1
		IF iLastLoggedPrice < 0 
			iLastLoggedPrice = MAX_STOCK_PRICE_LOG_ENTRIES - 1
		ENDIF
		fStockPrice = g_BS_Listings[iCompany].LoggedPrices[iLastLoggedPrice]
	ENDIF
	
	CDEBUG2LN(DEBUG_STOCKS, "Current stock price for company ", iCompany, " is $", fStockPrice, ".")
	RETURN fStockPrice
ENDFUNC


/// PURPOSE:
///    Gets amount of funds invested in the given portfolio index.
FUNC FLOAT GET_INVESTED_TOTAL_FROM_PORTFOLIO(INT iChar, INT entry)
	//MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER
	IF entry < 0 OR entry > MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER-1
		RETURN 0.0
	ENDIF
	
	FLOAT fCurrentPrice = GET_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(iChar, entry)))
	
	IF fCurrentPrice > 0
	
		SWITCH iChar
			CASE 0
				RETURN g_savedGlobals.sFinanceData.MIKE_SHARES_INVESTED[entry]
			CASE 1
				RETURN g_savedGlobals.sFinanceData.FRANKLIN_SHARES_INVESTED[entry]
			CASE 2
				RETURN g_savedGlobals.sFinanceData.TREVOR_SHARES_INVESTED[entry]
		ENDSWITCH
		
	ENDIF
	
	//invalid char
	RETURN 0.0

ENDFUNC
 
 
/// PURPOSE:
///    Gets the total value of the given player character's portfolio
/// PARAMS:
///    trader - 
/// RETURNS:
///    
 FUNC FLOAT GET_CURRENT_PLAYER_PORTFOLIO_INVESTED(BAWSAQ_TRADERS trader)
 	FLOAT sum = 0
	INT i = 0
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		//g_BS_Portfolios[trader][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)	
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i) > 0
			sum += GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i) 
		ENDIF
	ENDREPEAT
	CPRINTLN(DEBUG_STOCKS, "GET_CURRENT_PLAYER_PORTFOLIO_INVESTED: ", sum)
	RETURN sum
 ENDFUNC

/// PURPOSE:
///    Gets the total value of the player char porfolio.
 FUNC FLOAT GET_CURRENT_PLAYER_PORTFOLIO_VALUE(BAWSAQ_TRADERS trader)
 	FLOAT sum = 0
	
	INT i
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		//g_BS_Portfolios[trader][iGET_COMPANY_INDEX_FROM_PORTFOLIO(,)	
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i) > 0
			sum += GET_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(trader),i)))*GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i)
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "GET_CURRENT_PLAYER_PORTFOLIO_VALUE: ", sum)
	RETURN sum
 ENDFUNC
 
 
/// PURPOSE:
///    Gets the total value of the player based on the last logged values of stocks.
 FUNC FLOAT GET_LOGGED_PLAYER_PORTFOLIO_VALUE(BAWSAQ_TRADERS trader)
 	CDEBUG1LN(DEBUG_STOCKS, "Starting to calculate portfolio total for trader ", ENUM_TO_INT(trader), "...")
 
 	FLOAT fPortfolioTotal = 0
	
	INT i
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		INT iOwnedTotal = GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(trader),i)
		IF iOwnedTotal > 0
			CDEBUG1LN(DEBUG_STOCKS, "Current total is $", fPortfolioTotal, ".")
			
			INT iStock = ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(trader),i))
			FLOAT fCurrentStockPrice = GET_CURRENT_STOCK_PRICE(iStock)
			IF fCurrentStockPrice > 0
			
				FLOAT fOwnedValue = fCurrentStockPrice * TO_FLOAT(iOwnedTotal)
			
				CDEBUG1LN(DEBUG_STOCKS, "Found ", iOwnedTotal, " owned in stock ", iStock, " worth $", GET_CURRENT_STOCK_PRICE(iStock), " each for a total of $", fOwnedValue, ".")
			
				fPortfolioTotal += fOwnedValue
			ELSE
				CDEBUG1LN(DEBUG_STOCKS, "Excluded stock ", iStock, " due to invalid current price of ", fCurrentStockPrice, ".")
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_STOCKS, "GET_LOGGED_PLAYER_PORTFOLIO_VALUE: ", fPortfolioTotal)
	RETURN fPortfolioTotal
 ENDFUNC


/// PURPOSE:
///    Obtains a scaling value used on rising price updates that is effected by the player's total
///    owned assets.
///    This is applied to price rises to attenuate them to zero on LCN for owned stock for players
///    who have total assets approaching 1000 million.  
FUNC FLOAT GET_PRICE_RISE_SCALER_FOR_ASSET(BAWSAQ_COMPANIES company)
	
	IF g_BS_Listings[company].bOnlineStock
		RETURN 1.0//not filtered
	ENDIF
	
	INT ito1 = GET_OWNED_TOTAL_FROM_PORTFOLIO(0,ENUM_TO_INT(company))
	INT ito2 = GET_OWNED_TOTAL_FROM_PORTFOLIO(1,ENUM_TO_INT(company))
	INT ito3 = GET_OWNED_TOTAL_FROM_PORTFOLIO(2,ENUM_TO_INT(company))
		
	IF (ito1+ito2+ito3) < 1
		RETURN 1.0 //not filtering price rises for unowned assets 
	ENDIF
	INT maxEffect = 1000000000//1 000 000 000
	INT cashsum = g_BankAccounts[0].iBalance + g_BankAccounts[1].iBalance + g_BankAccounts[2].iBalance
	
	IF g_BankAccounts[0].iBalance > maxEffect
		RETURN 0.0
	ENDIF
	IF g_BankAccounts[1].iBalance > maxEffect
		RETURN 0.0
	ENDIF
	IF g_BankAccounts[2].iBalance > maxEffect
		RETURN 0.0
	ENDIF
	
	
	FLOAT val = 0.0
	INT i
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		INT t = GET_OWNED_TOTAL_FROM_PORTFOLIO(0,i)
		IF  t > 0
			val += GET_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(0,i)))*t
		ENDIF
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(1,i) > 0
			val += GET_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(1,i)))*t
		ENDIF
		IF GET_OWNED_TOTAL_FROM_PORTFOLIO(2,i) > 0
			val += GET_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(2,i)))*t
		ENDIF
	ENDREPEAT
	
	
	INT sum = FLOOR(val)+cashsum
	//2 147 483 647
	// finally check if the price needs to be filtered
	
	INT minEffect = 700000000//   700 000 000
	
	
	IF sum > maxEffect
		CPRINTLN(DEBUG_STOCKS, "GET_PRICE_RISE_SCALER_FOR_ASSET: asset ", company, " scaler 0 earlybail" )
		RETURN 0.0
	ENDIF
	
	IF sum < minEffect
		CPRINTLN(DEBUG_STOCKS, "GET_PRICE_RISE_SCALER_FOR_ASSET: asset ", company, " scaler 1 earlybail" )
		RETURN 1.0
	ENDIF
	

	FLOAT degree = TO_FLOAT(sum - minEffect)
	FLOAT range = TO_FLOAT(maxEffect- minEffect)
	
	IF !(range > 0.0)
		CPRINTLN(DEBUG_STOCKS, "GET_PRICE_RISE_SCALER_FOR_ASSET: asset ", company, " scaler ", 1.0-(degree/range) )
		RETURN 1.0
	ENDIF
	
	CPRINTLN(DEBUG_STOCKS, "GET_PRICE_RISE_SCALER_FOR_ASSET: asset ", company, " scaler ",1.0-(degree/range) )

	RETURN 1.0-(degree/range)
ENDFUNC


/// PURPOSE:
///    Increment stock system stats that relate to money spent on mods.
/// PARAMS:
///    amount - 
PROC STOCK_MARKET_VEHICLE_MOD_MONEY_SPENT(INT amount)
	BAWSAQ_INCREMENT_MODIFIER(  BSMF_SM_GAMONSP, amount ) // money on all else but mods
	BAWSAQ_INCREMENT_MODIFIER( BSMF_SM_VECMOD_FOR_AUG , amount )
	BAWSAQ_INCREMENT_MODIFIER( BSMF_SM_VECMOD_FOR_LSC , amount )
ENDPROC


/// PURPOSE:
///    Log this price update for all LCN stocks.
PROC LCN_INTERNAL_SNAPSHOT_LISTING_TO_LOG(BAWSAQ_COMPANIES company, BOOL bSkipFilters = FALSE)
	IF g_BS_Listings[company].bOnlineStock 
		EXIT //online stocks read thier logs from history functions.
	ENDIF
	
	CDEBUG3LN(DEBUG_LCN, "Writing log for LCN company ", ENUM_TO_INT(company), ". ", PICK_STRING(bSkipFilters, "Skipping filters.", "Not skipping filters."))
	
	FLOAT currentprice = g_BS_Listings[company].fCurrentPrice
	
	IF !bSkipFilters
		currentprice = GET_STOCK_PRICE(ENUM_TO_INT(company))
	ENDIF
	
	IF currentprice = 0
		SCRIPT_ASSERT("LCN_INTERNAL_SNAPSHOT_LISTING_TO_LOG: Current price is zero. Bug *Default Levels*.")
		EXIT
	ENDIF
	
    g_BS_Listings[company].LoggedPrices[g_BS_Listings[company].iLogIndexCaret] = currentprice

	IF currentprice > g_BS_Listings[company].fLoggedHighValue
		g_BS_Listings[company].fLoggedHighValue = currentprice
	ENDIF
	IF currentprice < g_BS_Listings[company].fLoggedLowValue
		g_BS_Listings[company].fLoggedLowValue = currentprice
	ENDIF
    g_BS_Listings[company].iLogIndexCaret += 1
    
    IF g_BS_Listings[company].iLogIndexCaret >= MAX_STOCK_PRICE_LOG_ENTRIES
        g_BS_Listings[company].iLogIndexCaret = 0 
    ENDIF
	
	FLOAT ave = 0.0
	INT i
	REPEAT MAX_STOCK_PRICE_LOG_ENTRIES i
		ave += g_BS_Listings[company].LoggedPrices[i]
	ENDREPEAT
	ave /= TO_FLOAT(MAX_STOCK_PRICE_LOG_ENTRIES)
	g_BS_Listings[company].fLoggedPriceChangeFromWeeklyAverage = currentprice -  ave
	g_BS_Listings[company].fLoggedPriceChangeInPercent = (g_BS_Listings[company].fLoggedPriceChangeFromWeeklyAverage/ave)*100.0	
	
	 
ENDPROC 


/// PURPOSE:
///    Attempt to read all the current prices out of profile stats if we can.
PROC REQUEST_BAWSAQ_STOCK_PRICE_VALUES()
	INT iGameTime = GET_GAME_TIMER()
	CPRINTLN(DEBUG_BAWSAQ, "Requstiong BAWSAQ stock price sync. Time last synced = ", g_iBrowserStockTimeLastSynch, " GameTime = ", iGameTime)

	IF iGameTime > (g_iBrowserStockTimeLastSynch + BAWSAQ_ONLINE_SYNC_TIMER)
	OR g_iBrowserStockTimeLastSynch = -1
		CPRINTLN(DEBUG_BAWSAQ, "...enough time has passed attempting to request sync from server.")
	
		IF NOT NETWORK_IS_SIGNED_ONLINE()
			CDEBUG1LN(DEBUG_BAWSAQ, "Sync failed: Game is not online.")
			g_bInvalidOnlinePricesRead = TRUE
			EXIT
		ENDIF
		
		IF NOT NETWORK_HAS_VALID_ROS_CREDENTIALS()
			CDEBUG1LN(DEBUG_BAWSAQ, "Sync failed: The network does not have valid ROS credentials.")
			g_bInvalidOnlinePricesRead = TRUE
			EXIT
		ENDIF
		
		IF NOT g_bBrowserStockPriceSynchInProgress
			CDEBUG2LN(DEBUG_BAWSAQ, "Sync not in progress.")
			IF STAT_COMMUNITY_START_SYNCH()
				g_iBrowserStockTimeLastSynch = iGameTime  //cooldown on stock market triggered reads
				g_bBrowserStockPriceSynchInProgress = TRUE
				CDEBUG2LN(DEBUG_BAWSAQ, "Sync started sucessfully. Scheduling.")
			ENDIF
		ENDIF
#IF IS_DEBUG_BUILD
	ELSE
#ENDIF
		CPRINTLN(DEBUG_BAWSAQ, "...not enough time has passed. Skipping sync.")
	ENDIF
ENDPROC


/// PURPOSE:
///  Read the community stats that represent the prices for online stocks
///  place the prices in the buffers, report prices of 0 or lower as invalid.
/// RETURNS:
///    Number of invalid prices read
FUNC INT BAWSAQ_COPY_ONLINE_PRICES_OUT_OF_STATS()
	CPRINTLN(DEBUG_BAWSAQ, "Starting to read community stats...")

	IF (NOT NETWORK_IS_SIGNED_ONLINE())
	OR (NOT NETWORK_HAS_VALID_ROS_CREDENTIALS())
		CPRINTLN(DEBUG_STOCKS, "BAWSAQ_COPY_ONLINE_PRICES_OUT_OF_STATS called while not signed in.")
		RETURN 1
	ENDIF
	
	
	INT i = 0 
	INT iInvalidPrices = 0
	REPEAT BS_CO_TOTAL_LISTINGS i
		IF g_BS_Listings[i].bOnlineStock
			IF g_BS_Listings[i].StatIndex > -1
				STATSENUM e = g_BS_OIndexData[g_BS_Listings[i].StatIndex].OnlinePriceStat
				
				CDEBUG1LN(DEBUG_BAWSAQ,"Stock ",i," ",g_BS_Listings[i].sLongName)
				STAT_GET_FLOAT(e, g_BS_Listings[i].fCurrentPrice)
				CDEBUG1LN(DEBUG_BAWSAQ,"Current price: ",g_BS_Listings[i].fCurrentPrice)
				IF (g_BS_Listings[i].fCurrentPrice > 0)
					FLOAT ave = 0.0
					STAT_COMMUNITY_GET_HISTORY(e, 0, g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage)
					CDEBUG1LN(DEBUG_BAWSAQ,"Logged price change: ",g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage)
					g_BS_Listings[i].fLoggedHighValue = g_BS_Listings[i].fCurrentPrice
					g_BS_Listings[i].fLoggedLowValue = g_BS_Listings[i].fCurrentPrice
					ave += g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage
					
					//g_BS_Listings[i].fLastPriceChange = g_BS_Listings[i].fCurrentPrice - g_BS_Listings[i].fLastPriceChange
					//g_BS_Listings[i].fLastPriceChangeInPercent = (g_BS_Listings[i].fLastPriceChange/g_BS_Listings[i].fCurrentPrice)*100.0
					
					INT j
					REPEAT 4 j
						FLOAT f = 0
						STAT_COMMUNITY_GET_HISTORY(e, j+1, f)
						CDEBUG1LN(DEBUG_BAWSAQ,"History price #",j,": ",f)
						IF f > g_BS_Listings[i].fLoggedHighValue
							g_BS_Listings[i].fLoggedHighValue = f
						ENDIF
						
						IF f < g_BS_Listings[i].fLoggedLowValue
							g_BS_Listings[i].fLoggedLowValue = f
						ENDIF
						ave += f
					ENDREPEAT	
					
					ave /= 5.0
					CDEBUG1LN(DEBUG_BAWSAQ,"Avg is ",ave)
					FLOAT trueprice = GET_STOCK_PRICE(i)
					CDEBUG1LN(DEBUG_BAWSAQ,"True price: ",trueprice)
					IF trueprice > g_BS_Listings[i].fLoggedHighValue
						g_BS_Listings[i].fLoggedHighValue = trueprice
					ENDIF
					IF trueprice < g_BS_Listings[i].fLoggedLowValue
						g_BS_Listings[i].fLoggedLowValue = trueprice
					ENDIF
					CDEBUG1LN(DEBUG_BAWSAQ,"Logged min/max: ",g_BS_Listings[i].fLoggedLowValue,"/",g_BS_Listings[i].fLoggedHighValue)
					
					g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage = trueprice - ave
					//% change is calculated based on average, not current price (avoids <-100% changes)
					g_BS_Listings[i].fLoggedPriceChangeInPercent = (g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage/ave) * 100.0		
					CDEBUG1LN(DEBUG_BAWSAQ,"Avg change and %: ",g_BS_Listings[i].fLoggedPriceChangeFromWeeklyAverage,"/",g_BS_Listings[i].fLoggedPriceChangeInPercent,"%")
					
					CDEBUG1LN(DEBUG_BAWSAQ,"")
				ELSE
					++iInvalidPrices
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_BAWSAQ, "...Community stats finished reading. ", iInvalidPrices, " invalid stats found.")
	RETURN iInvalidPrices
ENDFUNC


/// PURPOSE:
///    waits for a STAT_COMMUNITY_SYNCH_IS_PENDING to finish then update the prices
PROC BAWSAQ_PROCESS_STOCK_PRICE_SYNCH()
	IF NOT NETWORK_IS_SIGNED_ONLINE()
		IF g_bBrowserStockPriceSynchInProgress
			CPRINTLN(DEBUG_BAWSAQ, "The game isn't online. BAWSAQ sync could not complete. Checking again in ", BAWSAQ_ONLINE_SYNC_TIMER, "ms.")
			g_bBrowserStockPriceSynchInProgress = FALSE
			g_iBrowserStockTimeLastSynch = GET_GAME_TIMER()
			g_bInvalidOnlinePricesRead = TRUE
		ENDIF
		EXIT
	ENDIF
	
	IF g_bBrowserStockPriceSynchInProgress
		CDEBUG3LN(DEBUG_BAWSAQ, "Stat sync is processing...")
		
		IF NOT STAT_COMMUNITY_SYNCH_IS_PENDING()
			INT iInvalidPrices = BAWSAQ_COPY_ONLINE_PRICES_OUT_OF_STATS()
			g_bBrowserStockPriceSynchInProgress = FALSE
			
			IF iInvalidPrices < 5
				CPRINTLN(DEBUG_BAWSAQ, "Stat sync was successful with no invalid values found.")
				g_bInvalidOnlinePricesRead = FALSE
			ELSE
				CPRINTLN(DEBUG_BAWSAQ, "Stat sync returned invalid values.")
				//g_bInvalidOnlinePricesRead = TRUE
			ENDIF
			
#IF IS_DEBUG_BUILD
		ELSE
			CDEBUG3LN(DEBUG_BAWSAQ, "Stat sync is pending. Waiting on server.")
#ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    checks if the savegame has the given lifeinvader coupon
/// PARAMS:
///    c - 
/// RETURNS:
///    
FUNC BOOL DOES_SAVE_HAVE_COUPON_CLF(COUPON_TYPE c) 
#IF USE_TU_CHANGES
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_BIT_SET(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ELSE
		RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	RETURN IS_BIT_SET(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
#ENDIF	//	NOT USE_TU_CHANGES
ENDFUNC
FUNC BOOL DOES_SAVE_HAVE_COUPON_NRM(COUPON_TYPE c) 
#IF USE_TU_CHANGES
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_BIT_SET(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ELSE
		RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	RETURN IS_BIT_SET(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
#ENDIF	//	NOT USE_TU_CHANGES
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL IS_THIS_HSW_MOD_COUPON(COUPON_TYPE c)
	SWITCH c
		CASE COUPON_HSW_MOD1	
		CASE COUPON_HSW_MOD2	
		CASE COUPON_HSW_MOD3	
		CASE COUPON_HSW_MOD4	
		CASE COUPON_HSW_MOD5
		CASE COUPON_HSW_MOD6	
		CASE COUPON_HSW_MOD7
		CASE COUPON_HSW_MOD8
		CASE COUPON_HSW_MOD9
		CASE COUPON_HSW_MOD10
		CASE COUPON_HSW_MOD11
		CASE COUPON_HSW_MOD12
		CASE COUPON_HSW_MOD13
		CASE COUPON_HSW_MOD14
		CASE COUPON_HSW_MOD15
		CASE COUPON_HSW_MOD16
		CASE COUPON_HSW_MOD17
		CASE COUPON_HSW_MOD18
		CASE COUPON_HSW_MOD19
		CASE COUPON_HSW_MOD20
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC MP_INT_STATS GET_HSW_MOD_DISCOUNT_STAT(COUPON_TYPE c)
	SWITCH c
		CASE COUPON_HSW_MOD1
			RETURN MP_STAT_DISCOUNT_HSW_MOD1
		CASE COUPON_HSW_MOD2	
			RETURN MP_STAT_DISCOUNT_HSW_MOD2
		CASE COUPON_HSW_MOD3
			RETURN MP_STAT_DISCOUNT_HSW_MOD3
		CASE COUPON_HSW_MOD4
			RETURN MP_STAT_DISCOUNT_HSW_MOD4
		CASE COUPON_HSW_MOD5
			RETURN MP_STAT_DISCOUNT_HSW_MOD5
		CASE COUPON_HSW_MOD6	
			RETURN MP_STAT_DISCOUNT_HSW_MOD6
		CASE COUPON_HSW_MOD7
			RETURN MP_STAT_DISCOUNT_HSW_MOD7
		CASE COUPON_HSW_MOD8
			RETURN MP_STAT_DISCOUNT_HSW_MOD8
		CASE COUPON_HSW_MOD9
			RETURN MP_STAT_DISCOUNT_HSW_MOD9
		CASE COUPON_HSW_MOD10
			RETURN MP_STAT_DISCOUNT_HSW_MOD10
		CASE COUPON_HSW_MOD11
			RETURN MP_STAT_DISCOUNT_HSW_MOD11
		CASE COUPON_HSW_MOD12
			RETURN MP_STAT_DISCOUNT_HSW_MOD12
		CASE COUPON_HSW_MOD13
			RETURN MP_STAT_DISCOUNT_HSW_MOD13
		CASE COUPON_HSW_MOD14
			RETURN MP_STAT_DISCOUNT_HSW_MOD14
		CASE COUPON_HSW_MOD15
			RETURN MP_STAT_DISCOUNT_HSW_MOD15
		CASE COUPON_HSW_MOD16
			RETURN MP_STAT_DISCOUNT_HSW_MOD16
		CASE COUPON_HSW_MOD17
			RETURN MP_STAT_DISCOUNT_HSW_MOD17
		CASE COUPON_HSW_MOD18
			RETURN MP_STAT_DISCOUNT_HSW_MOD18
		CASE COUPON_HSW_MOD19
			RETURN MP_STAT_DISCOUNT_HSW_MOD19
		CASE COUPON_HSW_MOD20
			RETURN MP_STAT_DISCOUNT_HSW_MOD20
		BREAK
	ENDSWITCH
	
	RETURN MP_STAT_DISCOUNT_HSW_MOD1
ENDFUNC

FUNC BOOL IS_THIS_HSW_UPGRADE_COUPON(COUPON_TYPE c)
	SWITCH c
		CASE COUPON_HSW_UPGRADE	
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC MPPLY_INT_STATS GET_HSW_UPGRADE_DISCOUNT_STAT(COUPON_TYPE c)
	SWITCH c
		CASE COUPON_HSW_UPGRADE
			RETURN MPPLY_DISCOUNT_HSW_UPGRADE
	ENDSWITCH
	
	RETURN MPPLY_DISCOUNT_HSW_UPGRADE
ENDFUNC
#ENDIF


FUNC BOOL DOES_SAVE_HAVE_COUPON(COUPON_TYPE c) 
#if USE_CLF_DLC
	return DOES_SAVE_HAVE_COUPON_CLF(c)
#endif
#if USE_NRM_DLC
	return DOES_SAVE_HAVE_COUPON_NRM(c)
#endif
	
	IF (c = COUPON_CAR_XMAS2017)
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2017)
	ENDIF
	IF (c = COUPON_CAR_XMAS2018)
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2018)
	ENDIF
	IF (c = COUPON_HELI_XMAS2018)
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_HELI_XMAS2018)
	ENDIF
	IF (c = COUPON_CAR2_XMAS2018)
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR2_XMAS2018)
	ENDIF
	IF (c = COUPON_CASINO_PLANE_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_PLANE_SITE)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (c = COUPON_CASINO_BOAT_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BOAT_SITE)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE2)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE2)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (c = COUPON_CASINO_MIL_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_MIL_SITE)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	IF (c = COUPON_CASINO_BIKE_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BIKE_SITE)
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_HSW_MOD_COUPON(c)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		INT iCouponPosixTime = GET_MP_INT_CHARACTER_STAT(GET_HSW_MOD_DISCOUNT_STAT(c))
		
		IF iCouponPosixTime = 0
		OR iCurrentPosixTime >= iCouponPosixTime
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF IS_THIS_HSW_UPGRADE_COUPON(c)
		INT iCouponPosixTime = GET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(c))
		
		IF iCouponPosixTime <= 0
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF (c = COUPON_CAR_GEN9_MIGRATION)
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_GEN9_MIGRATION)
	ENDIF 
	#ENDIF

#IF USE_TU_CHANGES
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ELSE
		RETURN IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	RETURN IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
#ENDIF	//	NOT USE_TU_CHANGES
ENDFUNC

/// PURPOSE:
///    Gets the display name for the given coupons
/// PARAMS:
///    c - 
/// RETURNS:
///    
FUNC STRING GET_COUPON_STRING(COUPON_TYPE c)
	
	//this doesn't work
	//STRING coupons[MAX_COUPONS] = {"COUP_HAIRC","COUP_TATTOO","COUP_WARSTOCK",
	//"COUP_MOSPORT","COUP_ELITAS","COUP_MEDSPENS","COUP_SPRUNK","COUP_RESPRAY"	}
	
	//RETURN coupons[c]
	SWITCH c
		CASE COUPON_HAIRCUT
			RETURN "COUP_HAIRC"
		CASE COUPON_TATOO
			RETURN "COUP_TATTOO"
		CASE COUPON_MIL_SITE
			RETURN "COUP_WARSTOCK"
		CASE COUPON_CAR_SITE
			RETURN "COUP_MOSPORT"
		CASE COUPON_PLANE_SITE
			RETURN "COUP_ELITAS"
		CASE COUPON_MEDICAL
			RETURN "COUP_MEDSPENS"
		CASE COUPON_SPRUNK
			RETURN "COUP_SPRUNK"
		CASE COUPON_SPRAY
			RETURN "COUP_RESPRAY"
		CASE COUPON_CAR_XMAS2017
			RETURN "COUP_XMAS2017"
		CASE COUPON_CAR_XMAS2018
			RETURN "COUP_CAR_XMAS2018"
		CASE COUPON_HELI_XMAS2018
			RETURN "COUP_HELI_XMAS2018"
		CASE COUPON_CAR2_XMAS2018
			RETURN "COUP_CAR2_XMAS2018"
		CASE COUPON_CASINO_PLANE_SITE
			RETURN "COUP_CAS_ELITAS"
		CASE COUPON_CASINO_BOAT_SITE
			RETURN "COUP_CAS_DOCKTEASE"
		CASE COUPON_CASINO_CAR_SITE
			RETURN "COUP_CAS_MOSPORT"
		CASE COUPON_CASINO_CAR_SITE2
			RETURN "COUP_CAS_SSASA"
		CASE COUPON_CASINO_MIL_SITE
			RETURN "COUP_CAS_WARSTOCK"
		CASE COUPON_CASINO_BIKE_SITE
			RETURN "COUP_CAS_PANDM"
		BREAK
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE COUPON_CAR_GEN9_MIGRATION
			RETURN "COUPON_CAR_GEN9_MIGRATION"
		BREAK
		#ENDIF
		DEFAULT
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_THIS_HSW_MOD_COUPON(c)
				RETURN "HSW_COUP"
			ENDIF
			
			IF IS_THIS_HSW_UPGRADE_COUPON(c)
				RETURN "HSWU_COUP"
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH


	RETURN ""
ENDFUNC


/// PURPOSE:
///    Redeems the coupon of this type
/// PARAMS:
///    c - 
PROC REDEEM_COUPON_CLF(COUPON_TYPE c)
	//mark the coupon as redeemed and pop the feed message
	//Andrew,
	//Steven T has added the lifeinvader contact in CL 1354085
	//+[CELL_E_276]
	//+Lifeinvader
	//
	//And I've just preocessed the latest contact pic updates and submitted. Should all bein the next build.
	
#IF USE_TU_CHANGES
	BOOL bClearedBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ENDIF
	IF bClearedBit
		//CHAR_LIFEINVADER
		PRINTLN("<OBTAIN_COUPON> Redeemed coupon notification for ",c)
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	IF IS_BIT_SET(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		//CHAR_LIFEINVADER
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")

		
		
		CLEAR_BIT(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	NOT USE_TU_CHANGES
ENDPROC


/// PURPOSE:
///    Redeems the coupon of this type
/// PARAMS:
///    c - 
PROC REDEEM_COUPON_NRM(COUPON_TYPE c)
	//mark the coupon as redeemed and pop the feed message
	//Andrew,
	//Steven T has added the lifeinvader contact in CL 1354085
	//+[CELL_E_276]
	//+Lifeinvader
	//
	//And I've just preocessed the latest contact pic updates and submitted. Should all bein the next build.
	
#IF USE_TU_CHANGES
	BOOL bClearedBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ENDIF
	IF bClearedBit
		//CHAR_LIFEINVADER
		PRINTLN("<OBTAIN_COUPON> Redeemed coupon notification for ",c)
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	IF IS_BIT_SET(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		//CHAR_LIFEINVADER
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")

		
		
		CLEAR_BIT(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	NOT USE_TU_CHANGES
ENDPROC


/// PURPOSE:
///    Redeems the coupon of this type
/// PARAMS:
///    c - 
PROC REDEEM_COUPON(COUPON_TYPE c)
	//mark the coupon as redeemed and pop the feed message
	//Andrew,
	//Steven T has added the lifeinvader contact in CL 1354085
	//+[CELL_E_276]
	//+Lifeinvader
	//
	//And I've just preocessed the latest contact pic updates and submitted. Should all bein the next build.
	
	#if USE_CLF_DLC
		REDEEM_COUPON_CLF(c)
		exit
	#endif
	#if USE_NRM_DLC
		REDEEM_COUPON_NRM(c)
		exit
	#endif
	
	IF (c = COUPON_CAR_XMAS2017)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2017, FALSE)
		EXIT
	ENDIF
	
	IF (c = COUPON_CAR_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2018, FALSE)
		EXIT
	ENDIF
	IF (c = COUPON_HELI_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_HELI_XMAS2018, FALSE)
		EXIT
	ENDIF
	IF (c = COUPON_CAR2_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR2_XMAS2018, FALSE)
		EXIT
	ENDIF
	
	IF (c = COUPON_CASINO_PLANE_SITE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_PLANE_SITE, 0)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_BOAT_SITE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BOAT_SITE, 0)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE, 0)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE2)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE2, 0)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_MIL_SITE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_MIL_SITE, 0)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_BIKE_SITE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BIKE_SITE, 0)
		EXIT
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_HSW_MOD_COUPON(c)
		SET_MP_INT_CHARACTER_STAT(GET_HSW_MOD_DISCOUNT_STAT(c), 0)
		EXIT
	ENDIF
	
	IF IS_THIS_HSW_UPGRADE_COUPON(c)
		INT iCurrentNumCoupon = GET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(c))
		SET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(c), iCurrentNumCoupon - 1)
		EXIT
	ENDIF
	
	IF (c = COUPON_CAR_GEN9_MIGRATION)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_GEN9_MIGRATION, FALSE)
		PRINTLN("[REDEEM_COUPON] COUPON_CAR_GEN9_MIGRATION")
		EXIT
	ENDIF
	#ENDIF
	
#IF USE_TU_CHANGES
	BOOL bClearedBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		OR IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
			bClearedBit = TRUE
			CLEAR_BIT(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ENDIF
	IF bClearedBit
		//CHAR_LIFEINVADER
		PRINTLN("<REDEEM_COUPON> Redeemed coupon notification for \"", GET_COUPON_STRING(c), "\"")
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
#ENDIF	//	USE_TU_CHANGES

#IF NOT USE_TU_CHANGES
	IF IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		//CHAR_LIFEINVADER
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_RED")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")

		
		
		CLEAR_BIT(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
	ENDIF
#ENDIF	//	NOT USE_TU_CHANGES
ENDPROC


/// PURPOSE:
///    Awards the given coupon to the save game.
/// PARAMS:
///    c - 
PROC OBTAIN_COUPON_CLF(COUPON_TYPE c)
	BOOL bSetBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_BIT_SET(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bSetBit = TRUE
			SET_BIT(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
			bSetBit = TRUE
			SET_BIT(g_savedGlobalsClifford.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
		ENDIF
	ENDIF
	IF bSetBit
		//CHAR_LIFEINVADER
		PRINTLN("<OBTAIN_COUPON> Displaying coupon notification for ",c)
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_STR")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
ENDPROC

/// PURPOSE:
///    Awards the given coupon to the save game.
/// PARAMS:
///    c - 
PROC OBTAIN_COUPON_NRM(COUPON_TYPE c)
	BOOL bSetBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_BIT_SET(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bSetBit = TRUE
			SET_BIT(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
			bSetBit = TRUE
			SET_BIT(g_savedGlobalsnorman.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
		ENDIF
	ENDIF
	IF bSetBit
		//CHAR_LIFEINVADER
		PRINTLN("<OBTAIN_COUPON> Displaying coupon notification for ",c)
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_STR")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
PROC PRINT_HSW_UPGRADE_COUPON_TICKER()
	THEFEED_SHOW()
	BEGIN_TEXT_COMMAND_THEFEED_POST("PIM_DV_HSW")	
	END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("CAS_LW_RDISC", ENUM_TO_INT(ICON_TYPE_DISCOUNT), "PIM_DV_HSW", TRUE)
ENDPROC

PROC PRINT_HSW_MOD_COUPON_TICKER()
	THEFEED_SHOW()
	BEGIN_TEXT_COMMAND_THEFEED_POST("PIM_DV_HSM")	
	END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("CAS_LW_RDISC", ENUM_TO_INT(ICON_TYPE_DISCOUNT), "PIM_DV_HSM", TRUE)
ENDPROC
#ENDIF


/// PURPOSE:
///    Awards the given coupon to the save game.
/// PARAMS:
///    c - 
PROC OBTAIN_COUPON(COUPON_TYPE c)
	#if use_CLF_dlc
		OBTAIN_COUPON_CLF(c)
		exit
	#endif
	#if USE_NRM_DLC
		OBTAIN_COUPON_NRM(c)
		exit
	#endif
	
	IF (c = COUPON_CAR_XMAS2017)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2017, TRUE)
		EXIT
	ENDIF
	
	IF (c = COUPON_CAR_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_XMAS2018, TRUE)
		EXIT
	ENDIF
	
	IF (c = COUPON_HELI_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_HELI_XMAS2018, TRUE)
		EXIT
	ENDIF
	IF (c = COUPON_CAR2_XMAS2018)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR2_XMAS2018, TRUE)
		EXIT
	ENDIF
	
	CONST_INT ciCOUPON_CASINO_LIFETIME_POSIX	(48*60)	// 48 minutes
	IF (c = COUPON_CASINO_PLANE_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_PLANE_SITE, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_BOAT_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BOAT_SITE, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_CAR_SITE2)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_CAR_SITE2, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_MIL_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_MIL_SITE, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	IF (c = COUPON_CASINO_BIKE_SITE)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_BIKE_SITE, iCurrentPosixTime+ciCOUPON_CASINO_LIFETIME_POSIX)
		EXIT
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_HSW_MOD_COUPON(c)
		INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
		SET_MP_INT_CHARACTER_STAT(GET_HSW_MOD_DISCOUNT_STAT(c), iCurrentPosixTime + g_sMPTunables.iHSW_MOD_COUPON_LIFETIME_POSIX)
		PRINT_HSW_MOD_COUPON_TICKER()
		EXIT
	ENDIF
	
	IF IS_THIS_HSW_UPGRADE_COUPON(c)
		INT iCurrentNumCoupon = GET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(c))
		IF iCurrentNumCoupon < g_sMPTunables.iHSW_UPGRADE_COUPON_LIMIT
			SET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(c), iCurrentNumCoupon + 1)
			PRINT_HSW_UPGRADE_COUPON_TICKER()
		ELSE
			PRINTLN("[OBTAIN_COUPON] Player already has max number of HSW upgrade coupon")
		ENDIF
		EXIT
	ENDIF
		
	IF (c = COUPON_CAR_GEN9_MIGRATION)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PLAYER_HAS_COUPON_CAR_GEN9_MIGRATION, TRUE)
		PRINTLN("[OBTAIN_COUPON] COUPON_CAR_GEN9_MIGRATION")
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bSetBit = FALSE
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			bSetBit = TRUE
			SET_BIT(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
		OR NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
			bSetBit = TRUE
			SET_BIT(g_savedGlobals.sFinanceData.iSaveCoupons,ENUM_TO_INT(c)) 
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSaveCoupons,ENUM_TO_INT(c))
		ENDIF
	ENDIF
	IF bSetBit
		//CHAR_LIFEINVADER
		PRINTLN("<OBTAIN_COUPON> Displaying coupon notification for \"", GET_COUPON_STRING(c), "\"")
		TEXT_LABEL_63 ll = "CHAR_LIFEINVADER"
		BEGIN_TEXT_COMMAND_THEFEED_POST("COUP_STR")
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_COUPON_STRING(c))
		END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(ll,ll,TRUE,TEXT_ICON_BLANK, "")
	ENDIF
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL DOES_SAVE_HAVE_ANY_HSW_MOD_COUPON()
	COUPON_TYPE couponToCheck
	INT iStart = ENUM_TO_INT(COUPON_HSW_MOD1) 
	INT iEnd = iStart + (g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1)
	INT i
	
	FOR i = iStart TO iEnd
		couponToCheck = INT_TO_ENUM(COUPON_TYPE, i)
		IF DOES_SAVE_HAVE_COUPON(couponToCheck)
			RETURN TRUE
		ENDIF	
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_AVAILABLE_HSW_MOD_COUPON()
	COUPON_TYPE couponToCheck
	INT iStart = ENUM_TO_INT(COUPON_HSW_MOD1) 
	INT iEnd = iStart + (g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1)
	INT i, iNumCoupon
	
	FOR i = iStart TO iEnd
		couponToCheck = INT_TO_ENUM(COUPON_TYPE, i)
		IF DOES_SAVE_HAVE_COUPON(couponToCheck)
			iNumCoupon++
		ENDIF	
	ENDFOR
	
	RETURN iNumCoupon
ENDFUNC

DEBUGONLY FUNC STRING GET_DEBUG_HSW_MOD_COUPON_NAME(COUPON_TYPE couponToCheck)
	SWITCH couponToCheck
		CASE COUPON_HSW_MOD1			RETURN	"COUPON_HSW_MOD1"	
		CASE COUPON_HSW_MOD2			RETURN	"COUPON_HSW_MOD2"	
		CASE COUPON_HSW_MOD3			RETURN	"COUPON_HSW_MOD3"	
		CASE COUPON_HSW_MOD4			RETURN	"COUPON_HSW_MOD4"	
		CASE COUPON_HSW_MOD5			RETURN	"COUPON_HSW_MOD5"
		CASE COUPON_HSW_MOD6			RETURN	"COUPON_HSW_MOD6"	
		CASE COUPON_HSW_MOD7			RETURN	"COUPON_HSW_MOD7"
		CASE COUPON_HSW_MOD8			RETURN	"COUPON_HSW_MOD8"
		CASE COUPON_HSW_MOD9			RETURN	"COUPON_HSW_MOD9"
		CASE COUPON_HSW_MOD10			RETURN	"COUPON_HSW_MOD10"
		CASE COUPON_HSW_MOD11			RETURN	"COUPON_HSW_MOD11"
		CASE COUPON_HSW_MOD12			RETURN	"COUPON_HSW_MOD12"
		CASE COUPON_HSW_MOD13			RETURN	"COUPON_HSW_MOD13"
		CASE COUPON_HSW_MOD14			RETURN	"COUPON_HSW_MOD14"
		CASE COUPON_HSW_MOD15			RETURN	"COUPON_HSW_MOD15"
		CASE COUPON_HSW_MOD16			RETURN	"COUPON_HSW_MOD16"
		CASE COUPON_HSW_MOD17			RETURN	"COUPON_HSW_MOD17"
		CASE COUPON_HSW_MOD18			RETURN	"COUPON_HSW_MOD18"
		CASE COUPON_HSW_MOD19			RETURN	"COUPON_HSW_MOD19"
		CASE COUPON_HSW_MOD20			RETURN	"COUPON_HSW_MOD20"
	ENDSWITCH	
	
	RETURN "INVALID COUPON_TYPE"
ENDFUNC

PROC OBTAIN_HSW_MOD_COUPON()
	COUPON_TYPE couponToCheck
	INT iStart = ENUM_TO_INT(COUPON_HSW_MOD1) 
	INT iEnd = iStart + (g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1)
	INT i
	
	FOR i = iStart TO iEnd
		couponToCheck = INT_TO_ENUM(COUPON_TYPE, i)
		IF NOT DOES_SAVE_HAVE_COUPON(couponToCheck)
			OBTAIN_COUPON(couponToCheck)
			PRINTLN("[OBTAIN_HSW_MOD_COUPON] ", GET_DEBUG_HSW_MOD_COUPON_NAME(couponToCheck))
			BREAKLOOP
		ENDIF	
		
		PRINTLN("[OBTAIN_HSW_MOD_COUPON] No free coupon available")
	ENDFOR
ENDPROC

FUNC COUPON_TYPE GET_NEXT_AVAILABLE_HSW_MOD_COUPON()
	COUPON_TYPE nextCoupon
	INT couponToCheck[MAX_NUM_HSW_MOD_COUPON]
	INT iStart = ENUM_TO_INT(COUPON_HSW_MOD1) 
	INT iEnd = iStart + (g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1)
	INT i, iArrayIndex
	
	// Add them all to array
	FOR i = iStart TO iEnd
		IF  DOES_SAVE_HAVE_COUPON(INT_TO_ENUM(COUPON_TYPE, i))
			couponToCheck[iArrayIndex] = GET_MP_INT_CHARACTER_STAT(GET_HSW_MOD_DISCOUNT_STAT(INT_TO_ENUM(COUPON_TYPE, i)))
		ENDIF	
		iArrayIndex++
	ENDFOR
	
	// Find which is going to be exipre soon
	INT min = HIGHEST_INT
	i = 0
	iArrayIndex = 0
	FOR i = 0 TO g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1 
		IF couponToCheck[i] <= min
		AND couponToCheck[i] > 0
			min = couponToCheck[i]
			iArrayIndex = i
			PRINTLN("[REDEEM_HSW_MOD_COUPON] min: ", min, " iArrayIndex: ", iArrayIndex, " i: ", i) 
		ENDIF
	ENDFOR 
	
	INT iCouponToRedeem = ENUM_TO_INT(COUPON_HSW_MOD1) + iArrayIndex
	nextCoupon = INT_TO_ENUM(COUPON_TYPE, iCouponToRedeem)
	
	RETURN nextCoupon
ENDFUNC

PROC REDEEM_HSW_MOD_COUPON()
	COUPON_TYPE nextCoupon = GET_NEXT_AVAILABLE_HSW_MOD_COUPON()
	PRINTLN("[REDEEM_HSW_MOD_COUPON] ", GET_DEBUG_HSW_MOD_COUPON_NAME(nextCoupon))
	REDEEM_COUPON(nextCoupon)
ENDPROC

FUNC INT GET_NUM_AVAILABLE_HSW_UPGRADE_COUPON()
	RETURN  GET_MP_INT_PLAYER_STAT(GET_HSW_UPGRADE_DISCOUNT_STAT(COUPON_HSW_UPGRADE))
ENDFUNC

FUNC BOOL DOES_SAVE_HAVE_ANY_HSW_UPGRADE_COUPON()
	RETURN GET_NUM_AVAILABLE_HSW_UPGRADE_COUPON() > 0
ENDFUNC
#ENDIF

//// PURPOSE:
 ///   Checks the SC inbox for coupons
PROC UPDATE_COUPONS()
	INT mess = SC_INBOX_GET_TOTAL_NUM_MESSAGES()
	IF mess > 0
		INT i = 0
		REPEAT mess i
			TEXT_LABEL_63 type = SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(i)
			IF GET_HASH_KEY(type) = HASH("coupon")
				IF NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(i)
					INT iCouponIndex = 0
					IF SC_INBOX_MESSAGE_GET_DATA_INT(i,"i",iCouponIndex)
						COUPON_TYPE ct = INT_TO_ENUM(COUPON_TYPE,iCouponIndex)
						if !g_bLoadedClifford
							IF NOT DOES_SAVE_HAVE_COUPON(ct)
							AND NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(i)
								OBTAIN_COUPON(ct)
							ENDIF	
						else
							IF NOT DOES_SAVE_HAVE_COUPON_CLF(ct)
							AND NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(i)
								OBTAIN_COUPON_CLF(ct)
							ENDIF	
						endif					
					ENDIF
					SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(i)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC


/*
Vom Feuer (VOM)
Shrewsbury (SHR)
Hawk & Little (HAL)
//depot/gta5/art/Models/Props/Weapons/ZZZ_WeaponDocs/GTA_Weapon_Clearance_Condensed.xlsx
*/
ENUM BAWSAQ_INTERNAL_WEAPON_COMPANY
	WEPCO_VOM_FEUER,//US
	WEPCO_SHREWSBURY,//Slavshit
	WEPCO_HAWK_AND_LITTLE,//Swiss
	WEPCO_NONE
ENDENUM


/// PURPOSE:
///    Takes a weapon type returns the company it's assigned to
/// PARAMS:
///    mn - 
/// RETURNS:
///    
FUNC BAWSAQ_INTERNAL_WEAPON_COMPANY WEAPON_MODEL_PARSE(WEAPON_TYPE mn)

	SWITCH mn//if these enums change feel free to comment out any of this list and send an email about it to whoever cares

		CASE WEAPONTYPE_STICKYBOMB
		CASE WEAPONTYPE_STUNGUN
		CASE WEAPONTYPE_GRENADELAUNCHER
		CASE WEAPONTYPE_HEAVYSNIPER
		CASE WEAPONTYPE_CARBINERIFLE
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
		CASE WEAPONTYPE_SMG
		CASE WEAPONTYPE_ASSAULTSHOTGUN
		CASE WEAPONTYPE_APPISTOL
			RETURN WEPCO_VOM_FEUER
		BREAK	
		
		
		CASE WEAPONTYPE_SMOKEGRENADE
		CASE WEAPONTYPE_REMOTESNIPER
		CASE WEAPONTYPE_RPG	
		CASE WEAPONTYPE_MG	
		CASE WEAPONTYPE_ADVANCEDRIFLE	
		CASE WEAPONTYPE_MICROSMG
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
		CASE WEAPONTYPE_PISTOL
			RETURN WEPCO_SHREWSBURY
		BREAK
			
			
			
		CASE WEAPONTYPE_GRENADE	
		CASE WEAPONTYPE_MINIGUN
		CASE WEAPONTYPE_SNIPERRIFLE
		CASE WEAPONTYPE_COMBATMG	
		CASE WEAPONTYPE_ASSAULTRIFLE
		CASE WEAPONTYPE_PUMPSHOTGUN
		CASE WEAPONTYPE_COMBATPISTOL
			RETURN WEPCO_HAWK_AND_LITTLE
		BREAK
			
		DEFAULT 
			PRINTSTRING("WEAPON_MODEL_PARSE unknown weapon: ")
			PRINTINT(ENUM_TO_INT(mn))
			PRINTNL()
			RETURN WEPCO_NONE
		BREAK
			
	ENDSWITCH

	RETURN WEPCO_NONE
ENDFUNC


PROC INFORM_STOCK_SYS_WEAPON_BOUGHT(WEAPON_TYPE mn)
	SWITCH WEAPON_MODEL_PARSE(mn)
		//BSMF_SM_WEPBUY_FOR_SHR,// WeaponTypeBought : used by Shewsbury
		//BSMF_SM_WEPBUY_FOR_HAL,// WeaponTypeBought : used by HawkAndLittle
		CASE WEPCO_SHREWSBURY
				BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_WEPBUY_FOR_SHR)
			BREAK
		CASE WEPCO_HAWK_AND_LITTLE
				BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_WEPBUY_FOR_HAL)
			BREAK
	ENDSWITCH
ENDPROC




// *****************************************************************************************
// ********************************Account management functions*****************************

/// PURPOSE:
///    Called by DO_BANK_ACCOUNT_ACTION to update the stats
PROC BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(enumBankAccountName acc)
	CDEBUG1LN(DEBUG_FINANCE, "Pushing script account value to stats...") 

    INT passingStruct
//    printDebugString("Updating bank stats for account")
//    PRINTNL()
    passingStruct = g_BankAccounts[acc].iBalance
    SWITCH acc
        CASE BANK_ACCOUNT_MICHAEL
			CDEBUG1LN(DEBUG_FINANCE, "Setting SP0_TOTAL_CASH to $", passingStruct, ".") 
            STAT_SET_INT(SP0_TOTAL_CASH, passingStruct)
        BREAK
        CASE BANK_ACCOUNT_FRANKLIN
			CDEBUG1LN(DEBUG_FINANCE, "Setting SP1_TOTAL_CASH to $", passingStruct, ".") 
            STAT_SET_INT(SP1_TOTAL_CASH, passingStruct)
        BREAK
        CASE BANK_ACCOUNT_TREVOR
			CDEBUG1LN(DEBUG_FINANCE, "Setting SP2_TOTAL_CASH to $", passingStruct, ".") 
            STAT_SET_INT(SP2_TOTAL_CASH, passingStruct)
        BREAK
    ENDSWITCH
ENDPROC


/// PURPOSE:
///    Checks for direct modification of the bank account stats, and makes the system match if needed
PROC BANK_CHECK_FOR_CODE_DEPOSITS()
	INT receivingStruct
	CDEBUG1LN(DEBUG_FINANCE, "Checking to see if script bank balances match stats...")

	IF NETWORK_IS_SIGNED_IN()
	    STAT_GET_INT(SP0_TOTAL_CASH, receivingStruct)
	    IF NOT (g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = receivingStruct)
			CDEBUG1LN(DEBUG_FINANCE, "Updating BANK_ACCOUNT_MICHAEL to match stat SP0_TOTAL_CASH. Script: $", g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance, " Stat: $", receivingStruct, ".")
	        g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance =  receivingStruct
	    ENDIF
	    
	    STAT_GET_INT(SP1_TOTAL_CASH, receivingStruct)
	    IF NOT (g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = receivingStruct)
	        CDEBUG1LN(DEBUG_FINANCE, "Updating BANK_ACCOUNT_FRANKLIN to match stat SP1_TOTAL_CASH. Script: $", g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance, " Stat: $", receivingStruct, ".")
	        g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance =  receivingStruct
	    ENDIF
	    
	    STAT_GET_INT(SP2_TOTAL_CASH, receivingStruct)
	    IF NOT (g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = receivingStruct)
			CDEBUG1LN(DEBUG_FINANCE, "Updating BANK_ACCOUNT_TREVOR to match stat SP2_TOTAL_CASH. Script: $", g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance, " Stat: $", receivingStruct, ".")
	        g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance =  receivingStruct
	    ENDIF
#IF IS_DEBUG_BUILD
	ELSE
		CDEBUG1LN(DEBUG_FINANCE, "The player was not signed in to a profile. Could not retrieve stat values.")
#ENDIF	
	ENDIF
	
ENDPROC


PROC QUICK_INCREMENT_INT_STAT(STATSENUM e, INT amount)
	INT istat
	STAT_GET_INT(e,istat)
	istat += amount
	STAT_SET_INT(e,istat)
ENDPROC


/// PURPOSE:
///    Gets the bank action scource string for the given enumerator
/// PARAMS:
///    baacin - The account action source to get the name of
/// RETURNS:
///    
FUNC STRING GET_BAAC_STRING_TAG(BANK_ACCOUNT_ACTION_SOURCE_BAAC baacin)

    SWITCH baacin
		// Mission character.
		CASE BAAC_SIMEON					RETURN "ACCNA_SIM" 				BREAK
		CASE BAAC_LESTER					RETURN "ACCNA_LES" 				BREAK
		CASE BAAC_AMANDA					RETURN "ACCNA_AMA" 				BREAK
		CASE BAAC_JIMMY						RETURN "ACCNA_JIM" 				BREAK
		CASE BAAC_TRACEY					RETURN "ACCNA_TRA" 				BREAK
		CASE BAAC_OSCAR						RETURN "ACCNA_OSC" 				BREAK
		CASE BAAC_ABIGAIL					RETURN "ACCNA_ABI" 				BREAK
		CASE BAAC_RE_BURIAL					RETURN "ACCNA_BUR" 				BREAK
		CASE BAAC_MICHAEL					RETURN "ACCNA_MIKE" 			BREAK
		CASE BAAC_SHRINK					RETURN "ACCNA_DRFR" 			BREAK
		CASE BAAC_STRIP_CLUB				RETURN "ACCNA_STRP" 			BREAK
		CASE BAAC_MRSPOKE					RETURN "ACCNA_MRSPOKE" 			BREAK
		CASE BAAC_LOS_SANTOS_GOLF_CLUB		RETURN "ACCNA_GOL_CLU" 			BREAK	
			
		// Company
		CASE BAAC_CLUCKING_BELL				RETURN "ACCNA_CBELL" 			BREAK
		CASE BAAC_WHIZZ_PHONE				RETURN "ACCNA_WHIZZ" 			BREAK
		CASE BAAC_MADAM_CHONGS				RETURN "ACCNA_MCHON" 			BREAK
		CASE BAAC_DS_CLOTHING				RETURN "ACCNA_DSACH" 			BREAK
		CASE BAAC_LOS_SANTOS_HOSPITAL		RETURN "ACCNA_LSANH" 			BREAK
		CASE BAAC_CRAPKIA					RETURN "ACCNA_CRAPKI" 			BREAK
		CASE BAAC_VINE_CLEAN				RETURN "ACCNA_VCLEAN" 			BREAK
		CASE BAAC_CANDYSUXX					RETURN "ACCNA_CSUX" 			BREAK
		CASE BAAC_VINEWOOD_BEAUTY			RETURN "ACCNA_VBEU" 			BREAK
		CASE BAAC_AMMUNATION				RETURN "ACCNA_ANAT" 			BREAK
		
		// Bars
		CASE BAAC_BAHAMAMAMAS				RETURN "ACCNA_BAHAMA" 			BREAK
		CASE BAAC_BAY_BAR					RETURN "ACCNA_BAR_BY" 			BREAK
		CASE BAAC_BIKER_BAR					RETURN "ACCNA_BAR_BI" 			BREAK
		CASE BAAC_HIMEN_BAR					RETURN "ACCNA_BAR_HI" 			BREAK
		CASE BAAC_MOJITOS_BAR				RETURN "ACCNA_BAR_MO" 			BREAK
		CASE BAAC_SHENANIGANS_BAR			RETURN "ACCNA_BAR_SH" 			BREAK
		CASE BAAC_SINGLETONS_BAR			RETURN "ACCNA_BAR_SI" 			BREAK

		// Services
		CASE BAAC_TAXI						RETURN "ACCNA_TAXI" 			BREAK
		CASE BAAC_DRUG_TRAFFICKING			RETURN "ACCNA_DTRAF" 			BREAK
		CASE BAAC_CAR_REPO					RETURN "ACCNA_REPO" 			BREAK
		CASE BAAC_HUNTING					RETURN "ACCNA_HUNT" 			BREAK
		CASE BAAC_SHOOTING_RANGE			RETURN "ACCNA_RANGE" 			BREAK
		CASE BAAC_RACES						RETURN "ACCNA_RACES" 			BREAK
		
		// Other/misc.
		CASE BAAC_EPSILON_SITE_DONATION		RETURN "ACCNA_EPS_ST" 			BREAK
		CASE BAAC_EPSILON_ROBES_DONATION	RETURN "ACCNA_EPS_RB" 			BREAK
		
		// Stock brokerage.
        CASE BAAC_BROKERAGE_PAYMENT			RETURN "ACCNA_BROKERA" 			BREAK     
			
		// Vehicle websites
		CASE BAAC_MOTORSPORT_SITE			RETURN "ACCNA_CARSITE" 			BREAK
		CASE BAAC_ARMY_SITE					RETURN "ACCNA_ARMYSITE" 		BREAK
		CASE BAAC_PLANE_SITE				RETURN "ACCNA_PLANESITE" 		BREAK
		CASE BAAC_BOAT_SITE					RETURN "ACCNA_BOATSITE" 		BREAK
		CASE BAAC_BIKE_SITE 				RETURN "ACCNA_BIKESITE" 		BREAK
		CASE BAAC_SUPERAUTO_SITE 			RETURN "ACCNA_AUTOSITE" 		BREAK
		CASE BAAC_LOWRIDER_SITE 			RETURN "ACCNA_LOSSSITE" 		BREAK
		CASE BAAC_ARENA_SITE 				RETURN "ACCNA_ARENASITE" 		BREAK
			
		// Misc
		CASE BAAC_CASH_DEPOSIT				RETURN "ACCNA_CASHDEP" 			BREAK
		CASE BAAC_BAIL_BONDS				RETURN "ACCNA_BAILBONDS" 		BREAK
		CASE BAAC_HEIST_OFFSHORE_ACCOUNT	RETURN "ACCNA_HOFFSHORE" 		BREAK
		CASE BAAC_SNACKS					RETURN "ACCNA_SNACK" 			BREAK

		// Barbers & Hairdressers
		CASE BAAC_HAIRDO_SHOP_01_BH			RETURN "S_H_01"  				BREAK		
		CASE BAAC_HAIRDO_SHOP_02_SC			RETURN "S_H_02" 				BREAK 		
		CASE BAAC_HAIRDO_SHOP_03_V			RETURN "S_H_03" 				BREAK 		
		CASE BAAC_HAIRDO_SHOP_04_SS			RETURN "S_H_04" 				BREAK 		
		CASE BAAC_HAIRDO_SHOP_05_MP			RETURN "S_H_05"  				BREAK		
		CASE BAAC_HAIRDO_SHOP_06_HW			RETURN "S_H_06" 				BREAK 		
		CASE BAAC_HAIRDO_SHOP_07_PB			RETURN "S_H_07"  				BREAK		
		
		// Clothes shops
		CASE BAAC_CLOTHES_SHOP_L_01_SC		RETURN "S_CL_01" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_02_GS		RETURN "S_CL_02" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_03_DT		RETURN "S_CL_03" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_04_CS		RETURN "S_CL_04" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_05_GSD		RETURN "S_CL_05" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_06_VC		RETURN "S_CL_06" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_L_07_PB		RETURN "S_CL_07" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_M_01_SM		RETURN "S_CM_01" 				BREAK 
		CASE BAAC_CLOTHES_SHOP_M_03_H		RETURN "S_CM_03" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_M_04_HW		RETURN "S_CM_04" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_M_05_GOH		RETURN "S_CM_05" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_H_01_BH		RETURN "S_CH_01" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_H_02_B		RETURN "S_CH_02" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_H_03_MW		RETURN "S_CH_03" 				BREAK 		
		CASE BAAC_CLOTHES_SHOP_A_01_VB		RETURN "S_CA_01" 				BREAK 	
		
		// Tattoo parlour
		CASE BAAC_TATTOO_PARLOUR_01_HW		RETURN "S_T_01" 				BREAK 		
		CASE BAAC_TATTOO_PARLOUR_02_SS		RETURN "S_T_02" 				BREAK 		
		CASE BAAC_TATTOO_PARLOUR_03_PB		RETURN "S_T_03" 				BREAK 		
		CASE BAAC_TATTOO_PARLOUR_04_VC		RETURN "S_T_04" 				BREAK 		
		CASE BAAC_TATTOO_PARLOUR_05_ELS		RETURN "S_T_05" 				BREAK 		
		CASE BAAC_TATTOO_PARLOUR_06_GOH		RETURN "S_T_06" 				BREAK 
		
		// AmmuNation
		CASE BAAC_GUN_SHOP_01_DT			RETURN "S_G_01" 				BREAK 		
		CASE BAAC_GUN_SHOP_02_SS			RETURN "S_G_02" 				BREAK 		
		CASE BAAC_GUN_SHOP_03_HW			RETURN "S_G_03" 				BREAK 		
		CASE BAAC_GUN_SHOP_04_ELS			RETURN "S_G_04" 				BREAK 		
		CASE BAAC_GUN_SHOP_05_PB			RETURN "S_G_05" 				BREAK 		
		CASE BAAC_GUN_SHOP_06_LS			RETURN "S_G_06" 				BREAK 		
		CASE BAAC_GUN_SHOP_07_MW			RETURN "S_G_07" 				BREAK 		
		CASE BAAC_GUN_SHOP_08_CS			RETURN "S_G_08" 				BREAK 		
		CASE BAAC_GUN_SHOP_09_GOH			RETURN "S_G_09" 				BREAK 		
		CASE BAAC_GUN_SHOP_10_VWH			RETURN "S_G_10" 				BREAK 		
		CASE BAAC_GUN_SHOP_11_ID1			RETURN "S_G_11" 				BREAK
		
		// Car mod
		CASE BAAC_CARMOD_SHOP_01_AP			RETURN "S_MO_01"				BREAK 		
		CASE BAAC_CARMOD_SHOP_05_ID2		RETURN "S_MO_05"				BREAK 		
		CASE BAAC_CARMOD_SHOP_06_BT1		RETURN "S_MO_06"				BREAK 		
		CASE BAAC_CARMOD_SHOP_07_CS1		RETURN "S_MO_07"				BREAK 		
		CASE BAAC_CARMOD_SHOP_08_CS6		RETURN "S_MO_08"				BREAK 		
		CASE BAAC_CARMOD_SHOP_SUPERMOD		RETURN "S_MO_09"				BREAK
		
		// Properties
		CASE BAAC_PROP_TOWING				RETURN "ACCNA_TOWING"			BREAK	// Towing Impound (Franklin)
		CASE BAAC_PROP_TAXI					RETURN "ACCNA_TAXI_LOT"			BREAK	// Taxi Lot (all)
		CASE BAAC_PROP_ARMS					RETURN "ACCNA_ARMS"				BREAK	// Arms Trafficking (Trevor)
		CASE BAAC_PROP_SONAR				RETURN "ACCNA_SONAR"			BREAK	// Sonar Collections (All)
		CASE BAAC_PROP_CARMOD				RETURN "ACCNA_CARMOD"			BREAK	// Car Mod Shop (Franklin)
		CASE BAAC_PROP_VCINEMA				RETURN "ACCNA_VCINEMA"			BREAK	// Vinewood Cinema (Michael)
		CASE BAAC_PROP_DCINEMA				RETURN "ACCNA_DCINEMA"			BREAK	// Downtown Cinema (Michael)
		CASE BAAC_PROP_MCINEMA				RETURN "ACCNA_MCINEMA"			BREAK	// Morningwood Cinema (Michael)
		CASE BAAC_PROP_GOLF					RETURN "ACCNA_GOLF"				BREAK	// Golf club (All)
		CASE BAAC_PROP_CSCRAP				RETURN "ACCNA_CSCRAP"			BREAK	// Car Scrap Yard (Franklin)
		CASE BAAC_PROP_SMOKE				RETURN "ACCNA_SMOKE"			BREAK	// Smoke on the Water Weed Shop (Franklin)
		CASE BAAC_PROP_BAR_TEQUILA			RETURN "ACCNA_TEQUILA"			BREAK	// Tequi-la-la Bar (All)
		CASE BAAC_PROP_BAR_PITCHERS			RETURN "ACCNA_PITCHERS"			BREAK	// Pitchers (All)
		CASE BAAC_PROP_BAR_HEN				RETURN "ACCNA_HEN"				BREAK	// The Hen House (all)
		CASE BAAC_PROP_BAR_HOOKIES			RETURN "ACCNA_HOOKIES"			BREAK	// Hookies Bar (all)
		CASE BAAC_PROP_MARINA				RETURN "ACCNA_MARINA"			BREAK
		CASE BAAC_PROP_HANGAR				RETURN "ACCNA_HANGAR"			BREAK
		CASE BAAC_PROP_HELIPAD				RETURN "ACCNA_HELIPAD"			BREAK
		CASE BAAC_PROP_GARAGE				RETURN "ACCNA_GARAGE"			BREAK
	
		// Police Stations
		CASE BAAC_POLICE_STATION_VB			RETURN "ACCNA_PD_VB"			BREAK	// Vespucci Beach
		CASE BAAC_POLICE_STATION_SC			RETURN "ACCNA_PD_SC"			BREAK	// South Central
		CASE BAAC_POLICE_STATION_DT			RETURN "ACCNA_PD_DT"			BREAK	// Downtown
		CASE BAAC_POLICE_STATION_RH			RETURN "ACCNA_PD_RH"			BREAK	// Rockford Hills
		CASE BAAC_POLICE_STATION_SS			RETURN "ACCNA_PD_SS"			BREAK	// Sandy Shores
		CASE BAAC_POLICE_STATION_PB			RETURN "ACCNA_PD_PB"			BREAK	// Paleto Bay
		CASE BAAC_POLICE_STATION_HW			RETURN "ACCNA_PD_HW"			BREAK	// Vinewood (#1289759)
		
		//Hospitals
		CASE BAAC_HOSPITAL_RH				RETURN "ACCNA_H_RH"				BREAK	// Rockford Hills
		CASE BAAC_HOSPITAL_SC				RETURN "ACCNA_H_SC"				BREAK	// South Central
		CASE BAAC_HOSPITAL_DT				RETURN "ACCNA_H_DT"				BREAK	// Downtown
		CASE BAAC_HOSPITAL_SS				RETURN "ACCNA_H_SS"				BREAK	// Sandy Shores
		CASE BAAC_HOSPITAL_PB				RETURN "ACCNA_H_PB"				BREAK	// Paleto Bay
			
		// Websites
		CASE BAAC_CONSITE					RETURN "ACCNA_CONSIT"			BREAK
		CASE BAAC_REALITY_MILL				RETURN "ACCNA_TRMSITE"			BREAK
				
		// MP
		CASE BAAC_DYNASTY_PROPERTY			RETURN "ACCNA_DYNPROP"			BREAK

		// Debug non-logged transaction.
		#IF IS_DEBUG_BUILD
		CASE BAAC_DEFAULT_DEBUG				RETURN "DEBUG_DONOTLOG"	BREAK 
       	CASE BAAC_UNLOGGED_SMALL_ACTION		RETURN "THISSHOULDNOTBELOGGED"	BREAK
		#ENDIF
		
    ENDSWITCH
	
	CPRINTLN(DEBUG_FINANCE, "Unset BAAC_ string index ", baacin, ".")
	SCRIPT_ASSERT("Unset BAAC_ enum string, see log for index.")
	
    RETURN ""
ENDFUNC


/// PURPOSE:
///    Carries out an account action.
/// PARAMS:
///    accountname - 	The account to act upon
///    actiontype - 	The action to take
///    actionsource - 	The character/buisiness making the transaction, used for logging
///    actiondegree - 	The degree of change of this action
///    bForceAction - 	Allows the action to go through despite safety checks failing. 
///    					For example debiting an account without enough money.
FUNC BOOL DO_BANK_ACCOUNT_ACTION(enumBankAccountName accountname, BANK_ACCOUNT_ACTION_TYPE actiontype, BANK_ACCOUNT_ACTION_SOURCE_BAAC actionsource, INT actiondegree, BOOL bForceAction = FALSE)
	
	#IF IS_DEBUG_BUILD
		BOOL bCrediting = TRUE
		IF actiontype = BAA_DEBIT
			bCrediting = FALSE
		ENDIF
		CPRINTLN(DEBUG_FINANCE, "Attempting bank account action... AccountID '", ENUM_TO_INT(accountname),"' ", PICK_STRING(bCrediting, "credited", "debited"), " by $", actiondegree, ".")
	#ENDIF


	// First check current code state to prevent bad things happening on load with this command in the flow.
	BANK_CHECK_FOR_CODE_DEPOSITS()	
	
	#IF IS_DEBUG_BUILD
	IF ((!g_bLoadedClifford and !g_bLoadedNorman and GET_HASH_OF_THIS_SCRIPT_NAME() != HASH("debug"))
	OR (g_bLoadedClifford  and GET_HASH_OF_THIS_SCRIPT_NAME() != HASH("debugCLF"))
	OR (g_bLoadedNorman  and GET_HASH_OF_THIS_SCRIPT_NAME() != HASH("debugNRM")))
	
		IF actionsource = BAAC_DEFAULT_DEBUG
			SCRIPT_ASSERT("Do not use BAAC_DEFAULT_DEBUG, please add a transcation entry or use BAAC_UNLOGGED_SMALL_ACTION if you do not want your transaction to appear in the log.")
		ELSE
			IF actionsource != BAAC_UNLOGGED_SMALL_ACTION
				IF NOT DOES_TEXT_LABEL_EXIST(GET_BAAC_STRING_TAG(actionsource))
					SCRIPT_ASSERT("Bank account action source enum with a missing string called from this script OR the text files are out of date. Please add an entry for this enum to GET_BAAC_STRING_TAG")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF	
	
  	IF actiondegree < 1 //I'm tired of the assert, it is dumb anyway
		CDEBUG1LN(DEBUG_FINANCE, "Action failed as we are not operating on a positive value.")
		RETURN FALSE
	ENDIF
		
	FLOAT fDiscount = 1.0
	COUPON_TYPE ct
	
	SWITCH actiontype
		CASE BAA_DEBIT
			SWITCH accountname
				CASE BANK_ACCOUNT_MICHAEL
					BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_MONSP_FOR_BOL)
					QUICK_INCREMENT_INT_STAT(SP0_MONEY_TOTAL_SPENT, actiondegree)
				BREAK
				CASE BANK_ACCOUNT_FRANKLIN
					QUICK_INCREMENT_INT_STAT(SP1_MONEY_TOTAL_SPENT, actiondegree)
				BREAK
				CASE BANK_ACCOUNT_TREVOR
					QUICK_INCREMENT_INT_STAT(SP2_MONEY_TOTAL_SPENT, actiondegree)
				BREAK
			ENDSWITCH
		
			CHECK_THATS_A_LOT_OF_CHEDDAR_ACHIEVEMENT()
			
			SWITCH actionsource
				CASE BAAC_HOSPITAL_DT
				CASE BAAC_HOSPITAL_PB
				CASE BAAC_HOSPITAL_RH
				CASE BAAC_HOSPITAL_SC
				CASE BAAC_HOSPITAL_SS 
					IF DOES_SAVE_HAVE_COUPON(COUPON_MEDICAL) 	
						fDiscount = 0.9
						ct=COUPON_MEDICAL
					ENDIF 
				BREAK			
			
				CASE BAAC_TATTOO_PARLOUR_01_HW 		// Tattoo - Vinewood
				CASE BAAC_TATTOO_PARLOUR_02_SS 		// Tattoo - Sandy Shores
				CASE BAAC_TATTOO_PARLOUR_03_PB 		// Tattoo - Paleto Bay
				CASE BAAC_TATTOO_PARLOUR_04_VC 		// Tattoo - Vespucci Canals
				CASE BAAC_TATTOO_PARLOUR_05_ELS 		// Tattoo - East Los Santos
				CASE BAAC_TATTOO_PARLOUR_06_GOH 		// Tattoo - Great Ocean Highway
					SWITCH accountname
						CASE BANK_ACCOUNT_MICHAEL
							QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_ON_TATTOOS, actiondegree)
							BREAK
						CASE BANK_ACCOUNT_FRANKLIN
							QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_ON_TATTOOS, actiondegree)
							BREAK
						CASE BANK_ACCOUNT_TREVOR
							QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_ON_TATTOOS, actiondegree)
							BREAK
					ENDSWITCH
					
					IF DOES_SAVE_HAVE_COUPON(COUPON_TATOO) 					
						fDiscount = 0.0
						ct=COUPON_TATOO
					ENDIF 
				BREAK
					
				CASE BAAC_TAXI
					SWITCH accountname
						CASE BANK_ACCOUNT_MICHAEL
							QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_ON_TAXIS,actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing SP0_MONEY_SPENT_ON_TAXIS stat for by ", actiondegree )
						BREAK
						CASE BANK_ACCOUNT_FRANKLIN
							QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_ON_TAXIS,actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing SP1_MONEY_SPENT_ON_TAXIS stat for by ", actiondegree )
						BREAK
						CASE BANK_ACCOUNT_TREVOR
							QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_ON_TAXIS,actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing SP2_MONEY_SPENT_ON_TAXIS stat for by ", actiondegree )
						BREAK
					ENDSWITCH
				BREAK
					
				CASE BAAC_STRIP_CLUB
					SWITCH accountname
						CASE BANK_ACCOUNT_MICHAEL
							QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_IN_STRIP_CLUBS, actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing finance stat for mike by ", actiondegree )
						BREAK
						CASE BANK_ACCOUNT_FRANKLIN
							QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_IN_STRIP_CLUBS, actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing finance stat for franklin by ", actiondegree )
						BREAK
						CASE BANK_ACCOUNT_TREVOR
							QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_IN_STRIP_CLUBS, actiondegree)
							CPRINTLN(DEBUG_FINANCE, "Incrementing finance stat for trevor by ", actiondegree )
						BREAK
					ENDSWITCH
				BREAK
																
				CASE BAAC_PROP_TOWING//Towing Impound (Franklin)
				CASE BAAC_PROP_TAXI//Taxi Lot (all)
				CASE BAAC_PROP_ARMS//Arms Trafficking (Trevor)
				CASE BAAC_PROP_SONAR//Sonar Collections (All)
				CASE BAAC_PROP_VCINEMA//	Vinewood Cinema (Michael)
				CASE BAAC_PROP_DCINEMA//	Downtown Cinema (Michael)
				CASE BAAC_PROP_MCINEMA//	Morningwood Cinema (Michael)
				CASE BAAC_PROP_GOLF//	Golf club (All)
				CASE BAAC_PROP_CSCRAP//	Car Scrap Yard (Franklin)
				CASE BAAC_PROP_SMOKE//	Smoke on the Water Weed Shop (Franklin)
				CASE BAAC_PROP_BAR_TEQUILA//	Tequi-la-la Bar (All)
				CASE BAAC_PROP_BAR_PITCHERS//	Pitchers (All)
				CASE BAAC_PROP_BAR_HEN//	The Hen House (all)
				CASE BAAC_PROP_BAR_HOOKIES//	Hookies Bar (all)
					SWITCH accountname
						CASE BANK_ACCOUNT_MICHAEL
							QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_PROPERTY, actiondegree)
						BREAK
						CASE BANK_ACCOUNT_FRANKLIN
							QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_PROPERTY, actiondegree)
						BREAK
						CASE BANK_ACCOUNT_TREVOR
							QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_PROPERTY, actiondegree)
						BREAK
					ENDSWITCH
				BREAK
				
				DEFAULT
					SWITCH(GET_HASH_OF_THIS_SCRIPT_NAME())
						CASE HASH("clothes_shop_sp")
							SWITCH accountname
								CASE BANK_ACCOUNT_MICHAEL
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_CLOTHES stat for Mike by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_IN_CLOTHES, actiondegree)
									
								BREAK
								CASE BANK_ACCOUNT_FRANKLIN
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_CLOTHES stat for Franklin by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_IN_CLOTHES,actiondegree)
								BREAK
								CASE BANK_ACCOUNT_TREVOR
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_CLOTHES stat for Trevor by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_IN_CLOTHES,actiondegree)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE HASH("hairdo_shop_sp")
							SWITCH accountname
								CASE BANK_ACCOUNT_MICHAEL
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_ON_HAIRDOS stat for Mike by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_ON_HAIRDOS, actiondegree)
								BREAK
								
								CASE BANK_ACCOUNT_FRANKLIN
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_ON_HAIRDOS stat for Franklin by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_ON_HAIRDOS, actiondegree)
								BREAK
								
								CASE BANK_ACCOUNT_TREVOR
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_ON_HAIRDOS stat for Trevor by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_ON_HAIRDOS, actiondegree)
								BREAK
							ENDSWITCH
							
							IF DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT) 					
								fDiscount = 0.0
								ct=COUPON_HAIRCUT
							ENDIF 
						BREAK
						
						CASE HASH("gunclub_shop")
							SWITCH accountname
								CASE BANK_ACCOUNT_MICHAEL
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_BUYING_GUNS stat for Mike by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_IN_BUYING_GUNS, actiondegree)
								BREAK
								CASE BANK_ACCOUNT_FRANKLIN
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_BUYING_GUNS stat for Franklin by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_IN_BUYING_GUNS, actiondegree)
									
								BREAK
								CASE BANK_ACCOUNT_TREVOR
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_IN_BUYING_GUNS stat for Trevor by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_IN_BUYING_GUNS, actiondegree)
								BREAK
							ENDSWITCH
						BREAK
						
						CASE HASH("carmod_shop")
							SWITCH accountname
								CASE BANK_ACCOUNT_MICHAEL
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_CAR_MODS stat for Mike by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_CAR_MODS, actiondegree)
								BREAK
								CASE BANK_ACCOUNT_FRANKLIN
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_CAR_MODS stat for Franklin by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_CAR_MODS, actiondegree)
								BREAK
								CASE BANK_ACCOUNT_TREVOR
									CPRINTLN(DEBUG_FINANCE, "Incrementing SPENT_CAR_MODS stat for Trevor by $", actiondegree ,".")
									QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_CAR_MODS, actiondegree)
								BREAK
							ENDSWITCH
							STOCK_MARKET_VEHICLE_MOD_MONEY_SPENT(actiondegree)
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		
		CASE BAA_CREDIT
			SWITCH accountname
				CASE BANK_ACCOUNT_MICHAEL
					BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_MONUP_FOR_BOL, actiondegree)
				BREAK
				CASE BANK_ACCOUNT_FRANKLIN
					BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_MONUP_FOR_MAZ, actiondegree)
				BREAK
				CASE BANK_ACCOUNT_TREVOR
					BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_MONUP_FOR_FLC, actiondegree)
				BREAK
			ENDSWITCH

			BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_MONUP_FOR_SHK, actiondegree)
		BREAK
	ENDSWITCH
	

    INT i = ENUM_TO_INT(accountname)

	actiondegree = FLOOR(fDiscount*TO_FLOAT(actiondegree))
	CDEBUG1LN(DEBUG_FINANCE, "Applying discounts to action degree. Discount multiplier was ", fDiscount, "x. New action degree is $", actiondegree, ".")
	
    //TODO overflow check
    //carry out the action
	INT prebalance = 0
	INT actionpostdegree = actiondegree
	IF fDiscount = 0.0	
		REDEEM_COUPON(ct)		
		RETURN TRUE
	ELIF fDiscount != 1.0
		REDEEM_COUPON(ct)	
	ENDIF

	INT sum = (g_BankAccounts[i].iBalance + actiondegree)
    SWITCH actiontype
        CASE BAA_CREDIT
			CPRINTLN(DEBUG_FINANCE, "Undertaking bank account action... AccountID '", i,"' credited by $", actiondegree, ".")
			
			IF g_BankAccounts[i].iBalance >= 0
			AND actiondegree > 0
				IF sum <= 0
					g_BankAccounts[i].iBalance = 2147483647
					CDEBUG1LN(DEBUG_FINANCE, "Credit action has overrun the max int size. Setting bank balance to max int value.")
					SCRIPT_ASSERT("Account action too extreme, setting to cap.")					
				ELSE
					g_BankAccounts[i].iBalance += actiondegree
				ENDIF
			ENDIF
			
			SWITCH accountname
				CASE BANK_ACCOUNT_MICHAEL
					QUICK_INCREMENT_INT_STAT(SP0_TOTAL_CASH_EARNED,actiondegree)
				BREAK
				CASE BANK_ACCOUNT_FRANKLIN
					QUICK_INCREMENT_INT_STAT(SP1_TOTAL_CASH_EARNED,actiondegree)
				BREAK
				CASE BANK_ACCOUNT_TREVOR
					QUICK_INCREMENT_INT_STAT(SP2_TOTAL_CASH_EARNED,actiondegree)
				BREAK
			ENDSWITCH
        BREAK
        CASE BAA_DEBIT
			CPRINTLN(DEBUG_FINANCE, "Undertaking bank account action... AccountID '", i, "' debited by $", actiondegree, ".")
		
			IF NOT bForceAction
				IF (g_BankAccounts[i].iBalance - actiondegree) < 0
					CDEBUG1LN(DEBUG_FINANCE, "Debit action would leave bank balance negative and has not been forced. Action failed.")
					RETURN FALSE
				ENDIF
			ENDIF
			
			prebalance = g_BankAccounts[i].iBalance
            g_BankAccounts[i].iBalance -= actiondegree
			
			IF bForceAction			
				//set new actionpostdegree total if it hit zero from it's old total
				actionpostdegree = prebalance
			ENDIF
        BREAK
    ENDSWITCH
    
    //log the action
    IF actionsource = BAAC_UNLOGGED_SMALL_ACTION
        //unlocked action, check its below the threshold
        IF actionpostdegree > 20
          CPRINTLN(DEBUG_FINANCE, "!!BAAC_UNLOGGED_SMALL_ACTION!! + greater than threshold value, logged action should be used instead!!")
        ENDIF
    ELSE		
		#if USE_CLF_DLC	
	        g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eType = actiontype
	        g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eBaacSource = actionsource
	        g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].iDegree = actiondegree        
	        g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogActions++
	        g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint++		
	        IF g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint > (MAX_BANK_ACCOUNT_LOG_ENTRIES-1)
	            g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint = 0
	        ENDIF
		#endif
		#if USE_NRM_DLC
	        g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eType = actiontype
	        g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eBaacSource = actionsource
	        g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].iDegree = actiondegree        
	        g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogActions++
	        g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint++		
	        IF g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint > (MAX_BANK_ACCOUNT_LOG_ENTRIES-1)
	            g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint = 0
	        ENDIF
		#endif
		
		#if not USE_CLF_DLC		
		#if not USE_NRM_DLC
	        g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eType = actiontype
	        g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].eBaacSource = actionsource
	        g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint].iDegree = actiondegree        
	        g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogActions++
	        g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint++		
	        IF g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint > (MAX_BANK_ACCOUNT_LOG_ENTRIES-1)
	            g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[i].iLogIndexPoint = 0
	        ENDIF
		#endif	
		#endif
    ENDIF
    
    BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(accountname)
	
	IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
		CPRINTLN(DEBUG_FINANCE, "Off mission finance action. Lodging system state")
		#if USE_CLF_DLC	
			COPY_BANK_TRANSLOGS_CLF(FALSE)
		#endif
		#if USE_NRM_DLC
			COPY_BANK_TRANSLOGS_NRM(FALSE)
		#endif
		#if not USE_CLF_DLC	
		#if not USE_NRM_DLC
			COPY_BANK_TRANSLOGS(FALSE)
		#endif
		#endif
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC DO_POLICE_FINANCE_PENALTY(enumCharacterList charSheetID, INT iCash,POLICE_STATION_NAME_ENUM place)
	
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = MAX_ACCOUNTS)
        //stupid value, fail
            SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to DO_POLICE_FINANCE_PENALTY, fail")
            EXIT
    ENDIF
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = NO_ACCOUNT)
        //no account fail and assert
        SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to DO_POLICE_FINANCE_PENALTY, fail")
            EXIT
    ENDIF
    
    //
	enumBankAccountName account = GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID)

	SWITCH account
		CASE BANK_ACCOUNT_MICHAEL
			QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_IN_COP_BRIBES,iCash)
		BREAK
		CASE BANK_ACCOUNT_FRANKLIN
			QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_IN_COP_BRIBES,iCash)
		BREAK
		CASE BANK_ACCOUNT_TREVOR
			QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_IN_COP_BRIBES,iCash)
		BREAK
	ENDSWITCH
	SWITCH place
		CASE POLICE_STATION_VB // Vespucci Beach
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_VB,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_SC		// South Central
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_SC,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_DT		// Downtown
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_DT,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_RH		// Rockford Hills
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_RH,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_SS		// Sandy Shores
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_SS,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_PB		// Paleto Bay
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_PB,iCash,TRUE)
			BREAK
		CASE POLICE_STATION_HW		// Paleto Bay
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_POLICE_STATION_HW,iCash,TRUE)
			BREAK
		DEFAULT
			 SCRIPT_ASSERT("DO_POLICE_FINANCE_PENALTY passed an unknown location")
			EXIT
	ENDSWITCH


ENDPROC


PROC DO_HOSPITAL_FINANCE_PENALTY(enumCharacterList charSheetID, INT iCash,HOSPITAL_NAME_ENUM place)
	PRINTLN("<RESPAWN> DO_HOSPITAL_FINANCE_PENALTY called with cash value ", iCash)
	
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to DO_HOSPITAL_FINANCE_PENALTY, fail")
        EXIT
    ENDIF
	
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = NO_ACCOUNT)
        SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to DO_HOSPITAL_FINANCE_PENALTY, fail")
        EXIT
    ENDIF
    
	enumBankAccountName account = GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID)
	SWITCH account
		CASE BANK_ACCOUNT_MICHAEL
			QUICK_INCREMENT_INT_STAT(SP0_MONEY_SPENT_ON_HEALTHCARE,iCash)
			BREAK
		CASE BANK_ACCOUNT_FRANKLIN
			QUICK_INCREMENT_INT_STAT(SP1_MONEY_SPENT_ON_HEALTHCARE,iCash)
			BREAK
		CASE BANK_ACCOUNT_TREVOR
			QUICK_INCREMENT_INT_STAT(SP2_MONEY_SPENT_ON_HEALTHCARE,iCash)
			BREAK
	ENDSWITCH

	SWITCH place
		CASE HOSPITAL_RH 		// Rockford Hills
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_HOSPITAL_RH,iCash,TRUE)
			BREAK
		CASE HOSPITAL_SC 		// South Central
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_HOSPITAL_SC,iCash,TRUE)
			BREAK
		CASE HOSPITAL_DT 		// Downtown
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_HOSPITAL_DT,iCash,TRUE)
			BREAK
		CASE HOSPITAL_SS 		// Sandy Shores
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_HOSPITAL_SS,iCash,TRUE)
			BREAK
		CASE HOSPITAL_PB			// Paleto Bay
			DO_BANK_ACCOUNT_ACTION(account,BAA_DEBIT,BAAC_HOSPITAL_PB,iCash,TRUE)
			BREAK
		DEFAULT
			SCRIPT_ASSERT("DO_HOSPITAL_FINANCE_PENALTY passed an unknown location")
			EXIT
			BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Used by snapshot system to undo bank actions in repeat play and replay controller systems
/// PARAMS:
///    iAccount - which bank account we are altering
PROC UNDO_LAST_BANK_ACCOUNT_ACTION(INT iAccount)
	CPRINTLN(DEBUG_FINANCE, "Undoing last bank action for account ", iAccount, ".")

	// Update the log index point
	g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint--
	IF g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint < 0
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint = MAX_BANK_ACCOUNT_LOG_ENTRIES-1
	ENDIF
	
	CPRINTLN(DEBUG_REPLAY, "Undo bank action number ", g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint, "for account ", iAccount)
	
	// undo the action
	BANK_ACCOUNT_ACTION_TYPE actiontype = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint].eType
	INT actiondegree = g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint].iDegree
	
	SWITCH actiontype
        CASE BAA_CREDIT
			CPRINTLN(DEBUG_REPLAY, "Undo credit action, removing cash: ", actiondegree)
            g_BankAccounts[iAccount].iBalance -= actiondegree
        BREAK
        CASE BAA_DEBIT
			CPRINTLN(DEBUG_REPLAY, "Undo debit action, adding cash: ", actiondegree)
            g_BankAccounts[iAccount].iBalance += actiondegree
        BREAK
    ENDSWITCH
	
	// reset the log for this action
    g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint].iDegree = 0
	g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].LogEntries[g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogIndexPoint].eBaacSource = BAAC_DEFAULT_DEBUG
	g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[iAccount].iLogActions--
	
	// update stats
	BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(INT_TO_ENUM(enumBankAccountName, iAccount))
ENDPROC


/// PURPOSE:
///    Used to reset player accounts to the values needed for the new game
PROC BANK_INITIALISE_STARTING_BALANCES(BOOL force = FALSE)
	CPRINTLN(DEBUG_FINANCE, "Initialising starting balances for player characters.")

	// Ensure script and stats are in sync.
	BANK_CHECK_FOR_CODE_DEPOSITS() 
	
#IF USE_CLF_DLC
	IF (NOT g_savedGlobalsClifford.sFinanceData.bFirstTimeBankInit)
	AND LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_CLF_TRAIN) //prevent the initial launch obliterating the log set
	
		CPRINTLN(DEBUG_FINANCE, "First time bank init detected for CLF playthrough. Setting up fresh bank states.")

		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 4000
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogActions = 0
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 4000
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogActions = 0
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 4000
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogActions = 0
		g_savedGlobalsClifford.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogIndexPoint = 0
		
		
		// So the following transactions will go through.
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_TREVOR)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_MICHAEL)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_FRANKLIN)
		
		// TODO Need new bank history for the DLC pack.
		
//		//trev
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 22300)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 678)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 45200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_MADAM_CHONGS, 200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 12)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 14)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_LOS_SANTOS_HOSPITAL, 280)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 30200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 185)
//		
//		//franklin
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_WHIZZ_PHONE ,45)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_VINE_CLEAN ,400)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CANDYSUXX ,19)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_DS_CLOTHING ,149)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
//		
//		//mike
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_DS_CLOTHING ,2313)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_WHIZZ_PHONE ,20)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CRAPKIA ,5633)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_VINEWOOD_BEAUTY ,700)
		
		// Zero out the stat. #1412074
		STAT_SET_INT(SP0_MONEY_TOTAL_SPENT, 0)
		STAT_SET_INT(SP1_MONEY_TOTAL_SPENT, 0)
		STAT_SET_INT(SP2_MONEY_TOTAL_SPENT, 0)
				
		// Set the initial totals. (PLACEHOLDER)
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 1000000
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 1000000
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 1000000
		g_savedGlobalsClifford.sFinanceData.bFirstTimeBankInit = TRUE
	ENDIF
#ENDIF
#IF USE_NRM_DLC
	IF (NOT g_savedGlobalsnorman.sFinanceData.bFirstTimeBankInit)
	AND LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_NRM_SUR_START) //prevent the initial launch obliterating the log set
		
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 1000000
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogActions = 0
		//g_BankAccounts[BANK_ACCOUNT_MICHAEL].iLogActionsSinceLastCheck = 0
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 1000000
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogActions = 0
		//g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iLogActionsSinceLastCheck = 0
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 1000000
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogActions = 0
		//g_BankAccounts[BANK_ACCOUNT_TREVOR].iLogActionsSinceLastCheck = 0
		g_savedGlobalsnorman.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogIndexPoint = 0
		
		// So the following transactions will go through.
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_TREVOR)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_MICHAEL)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_FRANKLIN)
		
		// TODO Need new bank history for the DLC pack.
		
//		//trev
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 22300)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 678)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 45200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_MADAM_CHONGS, 200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 12)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 14)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_LOS_SANTOS_HOSPITAL, 280)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 30200)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 185)
//		
//		//franklin
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_WHIZZ_PHONE ,45)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_VINE_CLEAN ,400)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CANDYSUXX ,19)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_DS_CLOTHING ,149)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
//		
//		//mike
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_DS_CLOTHING ,2313)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_WHIZZ_PHONE ,20)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CRAPKIA ,5633)
//		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_VINEWOOD_BEAUTY ,700)
		
		// Zero out the stat. #1412074
		STAT_SET_INT(SP0_MONEY_TOTAL_SPENT,0)
		STAT_SET_INT(SP1_MONEY_TOTAL_SPENT,0)
		STAT_SET_INT(SP2_MONEY_TOTAL_SPENT,0)
				
		// Set the initial totals. (PLACEHOLDER)
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 1000000
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 1000000
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 1000000
		g_savedGlobalsnorman.sFinanceData.bFirstTimeBankInit = TRUE
	ENDIF
#ENDIF


#IF NOT USE_CLF_DLC
#if not USE_NRM_DLC
	IF (NOT g_savedGlobals.sFinanceData.bFirstTimeBankInit)
	AND LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_PROLOGUE) //prevent the initial launch obliterating the log set
	
		CPRINTLN(DEBUG_FINANCE, "First time bank init detected for GTAV playthrough. Setting up fresh bank states.")
		
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 4000
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogActions = 0
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_MICHAEL].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 4000
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogActions = 0
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_FRANKLIN].iLogIndexPoint = 0
		
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 4000
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogActions = 0
		g_savedGlobals.sFinanceData.PLAYER_ACCOUNT_LOGS[BANK_ACCOUNT_TREVOR].iLogIndexPoint = 0
		
		// So the following transactions will go through.
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_TREVOR)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_MICHAEL)
		BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_FRANKLIN)
		
		//trev
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 22300)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 678)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 45200)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_MADAM_CHONGS, 200)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 12)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_CLUCKING_BELL, 14)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_LOS_SANTOS_HOSPITAL, 280)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_CREDIT,BAAC_OSCAR, 30200)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_TREVOR,BAA_DEBIT,BAAC_AMMUNATION, 185)
		
		/*
		can we have some payments in from Excelsior Motoring,
		and Premium Deluxe Motor Sports

		Outgoings - Weekend MBA Course - $500

		New Boiler from Plumbing company (get from Stu Petri) $4,000
		Car tuning from company that you go to in Fam 1
		Downpayment on car - 3,000, burger shot
		cluckin bell
		Astro Theatres - $40,
		Bahama Mama's West - $500
		*/
		
		//franklin
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_WHIZZ_PHONE ,45)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_VINE_CLEAN ,400)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CANDYSUXX ,19)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_DS_CLOTHING ,149)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_FRANKLIN,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
		
		//mike
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CLUCKING_BELL ,19)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_DS_CLOTHING ,2313)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_WHIZZ_PHONE ,20)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_CRAPKIA ,5633)
		DO_BANK_ACCOUNT_ACTION(BANK_ACCOUNT_MICHAEL,BAA_DEBIT,BAAC_VINEWOOD_BEAUTY ,700)
		
		// Zero out the stat. #1412074
		STAT_SET_INT(SP0_MONEY_TOTAL_SPENT,0)
		STAT_SET_INT(SP1_MONEY_TOTAL_SPENT,0)
		STAT_SET_INT(SP2_MONEY_TOTAL_SPENT,0)
		
		/*
		//intialisation companies, 
		BAAC_CLUCKING_BELL,
		BAAC_WHIZZ_PHONE,
		BAAC_MADAM_CHONGS,
		BAAC_DS_CLOTHING,
		BAAC_LOS_SANTOS_HOSPITAL,
		BAAC_CRAPKIA,
		BAAC_VINE_CLEAN,
		BAAC_CANDYSUXX,
		BAAC_VINEWOOD_BEAUTY,
		BAAC_AMMUNATION,
		*/

		// Set the initial totals.
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 10666
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 3085
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 1178
		g_savedGlobals.sFinanceData.bFirstTimeBankInit = TRUE
	ENDIF
#ENDIF
#ENDIF	
	
	// Forced when doing a gameflow reset.
	IF force
		CPRINTLN(DEBUG_FINANCE, "Forcing bank balances to starting defaults for a gameflow reset.")
		g_BankAccounts[BANK_ACCOUNT_TREVOR].iBalance = 108654
		g_BankAccounts[BANK_ACCOUNT_MICHAEL].iBalance = 7860
		g_BankAccounts[BANK_ACCOUNT_FRANKLIN].iBalance = 78
	ENDIF
	
	BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_TREVOR)
	BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_MICHAEL)
	BANK_FORCE_UPDATE_LINKED_ACCOUNT_STATS(BANK_ACCOUNT_FRANKLIN)
ENDPROC


/// PURPOSE:
///    Returns the current balance of the given account.
FUNC INT GET_ACCOUNT_BALANCE(enumBankAccountName accountname)
	CDEBUG3LN(DEBUG_FINANCE, "Get account balance for account ", ENUM_TO_INT(accountname), ". Found $", g_BankAccounts[ENUM_TO_INT(accountname)].iBalance, ".")
    RETURN g_BankAccounts[ENUM_TO_INT(accountname)].iBalance
ENDFUNC


/// PURPOSE:
///    Gets the account action source for a given shop name.
FUNC BANK_ACCOUNT_ACTION_SOURCE_BAAC GET_BAAC_FROM_SHOP_ENUM(SHOP_NAME_ENUM e)
 	INT base = ENUM_TO_INT(e) - ENUM_TO_INT(HAIRDO_SHOP_01_BH)
 	INT ish = 	ENUM_TO_INT(BAAC_HAIRDO_SHOP_01_BH)
 	RETURN INT_TO_ENUM(BANK_ACCOUNT_ACTION_SOURCE_BAAC,base+ish)
ENDFUNC


/// PURPOSE:
///    Adds credit to the account of a given charsheet. Clifford DLC specific.
PROC CREDIT_BANK_ACCOUNT_CLF(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount, BOOL bReward = FALSE, BOOL bRCReward = FALSE)

    IF (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
    IF (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = NO_ACCOUNT)
        SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
	
    DO_BANK_ACCOUNT_ACTION(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account, BAA_CREDIT, description, amount)

	IF bReward
		INT istat = 0
		STATSENUM target 
		IF bRCReward
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH				
		ELSE
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_MISSIONS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_MISSIONS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_MISSIONS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH	
		ENDIF
		STAT_GET_INT(target,istat)
		istat += amount
		STAT_SET_INT(target,istat)
	ENDIF
ENDPROC


/// PURPOSE:
///    Adds credit to the account of a given charsheet. Norman DLC specific.
PROC CREDIT_BANK_ACCOUNT_NRM(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount, BOOL bReward = FALSE, BOOL bRCReward = FALSE)

    IF (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
    IF (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = NO_ACCOUNT)
        SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
	
    DO_BANK_ACCOUNT_ACTION(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account, BAA_CREDIT, description, amount)

	IF bReward
		INT istat = 0
		STATSENUM target 
		IF bRCReward
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH				
		ELSE
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_MISSIONS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_MISSIONS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_MISSIONS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH	
		ENDIF
		STAT_GET_INT(target,istat)
		istat += amount
		STAT_SET_INT(target,istat)
	ENDIF
ENDPROC


/// PURPOSE:
///    Adds credit to the account of a given charsheet.
PROC CREDIT_BANK_ACCOUNT(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount, BOOL bReward = FALSE, BOOL bRCReward = FALSE)
	CPRINTLN(DEBUG_FINANCE, "Crediting bank account. Owner:", ENUM_TO_INT(charSheetID), " Source:", GET_BAAC_STRING_TAG(description), " Amount:$", amount, ".")

	#IF USE_CLF_DLC
		IF g_bLoadedClifford
			CREDIT_BANK_ACCOUNT_CLF(charSheetID, description, amount, bReward, bRCReward)
			EXIT
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman 
			CREDIT_BANK_ACCOUNT_NRM(charSheetID, description, amount, bReward, bRCReward)
			EXIT
		ENDIF
	#ENDIF

    //DO_BANK_ACCOUNT_ACTION(GET_ACCOUNT_NAME_FROM_CHARSHEET(charSheetID),BAA_CREDIT,description,amount)
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = MAX_ACCOUNTS)
        //stupid value, fail
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = NO_ACCOUNT)
        //no account fail and assert
        SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail")
        EXIT
    ENDIF
	
    DO_BANK_ACCOUNT_ACTION(GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID), BAA_CREDIT,description,amount)

	IF bReward
		INT istat = 0
		STATSENUM target 
		IF bRCReward
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_RANDOM_PEDS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH				
		ELSE
			SWITCH charSheetID
				CASE CHAR_MICHAEL
					target = SP0_MONEY_MADE_FROM_MISSIONS
					BREAK 
				CASE CHAR_FRANKLIN
					target = SP1_MONEY_MADE_FROM_MISSIONS
					BREAK
				CASE CHAR_TREVOR
					target = SP2_MONEY_MADE_FROM_MISSIONS
					BREAK
				DEFAULT
					EXIT
			ENDSWITCH	
		ENDIF
		STAT_GET_INT(target,istat)
		istat += amount
		STAT_SET_INT(target,istat)
	ENDIF
ENDPROC


/// PURPOSE:
///     Debits the account of a given charsheet. Clifford DLC specific.
FUNC BOOL DEBIT_BANK_ACCOUNT_CLF(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount)

    IF (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail.")
    	RETURN FALSE
    ENDIF
    IF (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = NO_ACCOUNT)
    	SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail.")
    	RETURN FALSE
    ENDIF
    
    RETURN DO_BANK_ACCOUNT_ACTION(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[charSheetID].bank_account, BAA_DEBIT, description, amount)
ENDFUNC


/// PURPOSE:
///     Debits the account of a given charsheet. Norman DLC specific.
FUNC BOOL DEBIT_BANK_ACCOUNT_NRM(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount)

    IF (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail.")
    	RETURN FALSE
    ENDIF
    IF (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account = NO_ACCOUNT)
    	SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail.")
    	RETURN FALSE
    ENDIF
    
    RETURN DO_BANK_ACCOUNT_ACTION(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[charSheetID].bank_account, BAA_DEBIT, description, amount)
ENDFUNC

/// PURPOSE:
///    Debits the account of a given charsheet.
FUNC BOOL DEBIT_BANK_ACCOUNT(enumCharacterList charSheetID, BANK_ACCOUNT_ACTION_SOURCE_BAAC description, INT amount)
	CPRINTLN(DEBUG_FINANCE, "Debiting bank account. Owner:", ENUM_TO_INT(charSheetID), " Source:", GET_BAAC_STRING_TAG(description), " Amount:$", amount, ".")

	#IF USE_CLF_DLC
		IF g_bLoadedClifford
			RETURN DEBIT_BANK_ACCOUNT_CLF(charSheetID, description, amount)
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman
			RETURN DEBIT_BANK_ACCOUNT_NRM(charSheetID, description, amount)
		ENDIF
	#ENDIF

    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = MAX_ACCOUNTS)
        SCRIPT_ASSERT("charSheetID that has account set to MAX_ACCOUNTS passed to CREDIT_BANK_ACCOUNT, fail.")
        RETURN FALSE
    ENDIF
    IF (GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID) = NO_ACCOUNT)
    	SCRIPT_ASSERT("charSheetID that has account set to NO_ACCOUNT passed to CREDIT_BANK_ACCOUNT, fail.")
    	RETURN FALSE
    ENDIF
    
    RETURN DO_BANK_ACCOUNT_ACTION(GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(charSheetID), BAA_DEBIT, description, amount)
ENDFUNC


/// PURPOSE:
///    Gets the cash total a given character sheet has in their account, if they have one. 
FUNC INT GET_TOTAL_CASH(enumCharacterList charSheetID)
	CDEBUG1LN(DEBUG_FINANCE, "Looking up total player cash for character ", ENUM_TO_INT(charSheetID), "...")

	#IF IS_DEBUG_BUILD
		IF g_bInMultiplayer
			SCRIPT_ASSERT("GET_TOTAL_CASH was called in MP. This command is SP only.")
			RETURN 0
		ENDIF
	#ENDIF
	
    INT receivingStruct
    SWITCH charSheetID
        CASE CHAR_MICHAEL
            STAT_GET_INT(SP0_TOTAL_CASH, receivingStruct)
			CDEBUG1LN(DEBUG_FINANCE, "Mike has $", receivingStruct, ".")
            RETURN receivingStruct
        CASE CHAR_FRANKLIN
            STAT_GET_INT(SP1_TOTAL_CASH, receivingStruct)
			CDEBUG1LN(DEBUG_FINANCE, "Franklin has $", receivingStruct, ".")
            RETURN receivingStruct
        CASE CHAR_TREVOR
            STAT_GET_INT(SP2_TOTAL_CASH, receivingStruct)
			CDEBUG1LN(DEBUG_FINANCE, "Trevor has $", receivingStruct, ".")
            RETURN receivingStruct
    ENDSWITCH
	
    SCRIPT_ASSERT("GET_TOTAL_CASH: Enum doesn't have an account.")
    RETURN 0
ENDFUNC


/// PURPOSE:
///    Compares the cash total in the specified char sheet's bank account (if it has one) with compareValue.
FUNC BOOL IS_TOTAL_CASH_GREATER(enumCharacterList charSheetID, INT compareValue)
    IF GET_TOTAL_CASH(charSheetID) > compareValue
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF
ENDFUNC


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


PROC ACTIVATE_FINANCE_FILTER(BAWSAQ_COMPANIES bsco,INT iFlags, BOOL bSupressHelp = FALSE)
	
	//Search for filters applying to this CO
	INT iReg = -1
	INT i = 0
	INT iFreeReg = -1
	REPEAT MAX_SP_FINANCE_FILTERS i
		IF g_savedGlobals.sFinanceData.iFilterDurationRemaining[i] > 0
			IF g_savedGlobals.sFinanceData.FilteredBind[i] = bsco
				iReg = i
			ENDIF
		ELSE
			iFreeReg = i
		ENDIF
	ENDREPEAT
	
	IF (iReg != -1) OR (iFreeReg = -1)
		CPRINTLN(DEBUG_STOCKS, "ACTIVATE_FINANCE_FILTER: cannot register, no space or already registered")
		EXIT
	ENDIF
	
	INT iDuration = 0
	
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_1)
		iDuration += 1
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_2)
		iDuration += 2
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_4)
		iDuration += 4
	ENDIF
	IF IS_BIT_SET(iFlags,MFFILTER_DURATION_8)
		iDuration += 8
	ENDIF
	IF iDuration = 0 AND (NOT IS_BIT_SET(iFlags,MFFILTER_RESET_POST_EXILE3))
		CPRINTLN(DEBUG_STOCKS, "ACTIVATE_FINANCE_FILTER: Duration of 0 is invalid")
		EXIT
	ENDIF
	
	//CHANGED no longer represents time but number of days
	//iDuration *= ((1000*60)*60)*24//convert to days in MS
	iDuration *= STOCK_MARKET_LOG_UPDATES_PER_DAY	//3 log updates per day
	

	IF IS_BIT_SET(iFlags,MFFILTER_TIMESCALE_WEEKS)
		iDuration *= 7
	ENDIF
	
	++g_savedGlobals.sFinanceData.iFiltersRegistered
	g_savedGlobals.sFinanceData.iFilterFlags[iFreeReg] = iFlags
	g_savedGlobals.sFinanceData.FilteredBind[iFreeReg] = bsco
	g_savedGlobals.sFinanceData.iFilterDurationRemaining[iFreeReg] = iDuration
	
	CDEBUG1LN(debug_stocks,"Activated filter for ",ENUM_TO_INT(bsco)," with duration", iDuration)
	
	//Attempt to display a flow help message if the "Affect Stock" help hasn't been seen before.
	IF NOT bSupressHelp
	
		#IF NOT USE_CLF_DLC
		#IF NOT USE_NRM_DLC
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_AFFECT_STOCKS)
			
			#IF IS_DEBUG_BUILD
				//Don't queue help while debug launching.
				IF NOT g_flowUnsaved.bUpdatingGameflow
			#ENDIF
			
				CPRINTLN(DEBUG_STOCKS, "ACTIVATE_FINANCE_FILTER: Attempting to display flow help message about affecting stock prices.")
				ADD_HELP_TO_FLOW_QUEUE("AM_H_STOCKS", FHP_MEDIUM, 0, 40000, DEFAULT_HELP_TEXT_TIME, 7, CID_BLANK, CID_AFFECT_STOCKS_HELP_DISPLAYED)
			
			#IF IS_DEBUG_BUILD
				ENDIF
			#ENDIF
			
			ENDIF
		#ENDIF
		#ENDIF

	ENDIF
	LCN_INTERNAL_SNAPSHOT_LISTING_TO_LOG(bsco)
	CPRINTLN(DEBUG_STOCKS, "ACTIVATE_FINANCE_FILTER: Registered effect ", bsco, " for duration ", iDuration )
ENDPROC


#IF IS_DEBUG_BUILD
	PROC DEBUG_BS_DURATION_PRICE_TEST()
		FLOAT price = 50.0
		INT iflags = 0
		
		INT iDurationCountdown = ((1000*60)*60)*24//one day in hours
		INT iDurationCountStep = 1000*60 // minute intervals	
		SET_BIT(iflags,MFFILTER_DURATION_1)
		SET_BIT(iflags,MFFILTER_SCALE_50_PERC)

		CDEBUG1LN(DEBUG_STOCKS, "Test 1 constant effect, base ", price , " duration 1, 50 perc")
		WHILE iDurationCountdown > -1
			FLOAT mod = GET_BS_FILTER_FROM_DURATION_FLAGS(iDurationCountdown,iflags)
			CDEBUG1LN(DEBUG_STOCKS, "Time: ", iDurationCountdown , " price: ", price*mod)
			iDurationCountdown -= iDurationCountStep
			//WAIT(0)
		ENDWHILE

		SET_BIT(iflags,MFFILTER_RAMP_DOWN)
		SET_BIT(iflags,MFFILTER_RAMP_UP)
		SET_BIT(iflags,MFFILTER_RAMP_DOWN_PROPORTION_QUARTER)
		SET_BIT(iflags,MFFILTER_RAMP_UP_PROPORTION_QUARTER)
		iDurationCountdown = ((1000*60)*60)*24//one day in hours
		iDurationCountStep = 1000*60 // minute intervals		
		CDEBUG1LN(DEBUG_STOCKS, "Test 2 ramp up and down, base ", price , " duration 1, 50 perc")
		WHILE iDurationCountdown > -1
			FLOAT mod = GET_BS_FILTER_FROM_DURATION_FLAGS(iDurationCountdown,iflags)
			CDEBUG1LN(DEBUG_STOCKS, "Time: ", iDurationCountdown , " price: ", price*mod)
			iDurationCountdown -= iDurationCountStep
			//WAIT(0)
		ENDWHILE

		SET_BIT(iflags,MFFILTER_RAMP_UP_ENDSLOW)
		SET_BIT(iflags,MFFILTER_RAMP_DOWN_STARTSLOW)
		iDurationCountdown = ((1000*60)*60)*24//one day in hours
		iDurationCountStep = 1000*60 // minute intervals		
		CDEBUG1LN(DEBUG_STOCKS, "Test 3 ramp up and down, start and end slow, base ", price , " duration 1, 50 perc")
		WHILE iDurationCountdown > -1
			FLOAT mod = GET_BS_FILTER_FROM_DURATION_FLAGS(iDurationCountdown,iflags)
			CDEBUG1LN(DEBUG_STOCKS, "Time: ", iDurationCountdown , " price: ", price*mod)
			iDurationCountdown -= iDurationCountStep
			//WAIT(0)
		ENDWHILE
	ENDPROC
	
	FUNC STRING DEBUG_GET_STRING_FROM_SPEND_CATEGORIES(SPEND_CATEGORIES eSpend)
		SWITCH eSpend
			CASE MONEY_SPENT_CONTACT_SERVICE	RETURN "CONTACT_SERVICE" BREAK
			CASE MONEY_SPENT_PROPERTY_UTIL 		RETURN "PROPERTY_UTIL" BREAK
			CASE MONEY_SPENT_JOB_ACTIVITY	 	RETURN "JOB_ACTIVITY" BREAK
			CASE MONEY_SPENT_BETTING	 		RETURN "BETTING" BREAK
			CASE MONEY_SPENT_STYLE_ENT			RETURN "STYLE_ENT" BREAK
			CASE MONEY_SPENT_HEALTHCARE			RETURN "HEALTHCARE" BREAK
			CASE MONEY_SPENT_FROM_DEBUG			RETURN "FROM_DEBUG" BREAK
			CASE MONEY_SPENT_DROPPED_STOLEN		RETURN "DROPPED_STOLEN" BREAK
			CASE MONEY_SPENT_VEH_MAINTENANCE	RETURN "VEH_MAINTENANCE" BREAK
			CASE MONEY_SPENT_HOLDUPS			RETURN "HOLDUPS" BREAK
			CASE MONEY_SPENT_PASSIVEMODE		RETURN "PASSIVEMODE" BREAK
			CASE MONEY_SPENT_BANKINTEREST		RETURN "BANKINTEREST" BREAK
			
			CASE MONEY_SPENT_WEAPON_ARMOR		RETURN "WEAPON_ARMOR" BREAK
			CASE MONEY_SPENT_ROCKSTAR_AWARD		RETURN "ROCKSTAR_AWARD" BREAK
			CASE MONEY_SPENT_NOCOPS				RETURN "NOCOPS" BREAK
			#IF IS_NEXTGEN_BUILD
			CASE MONEY_SPEND_DEDUCT				RETURN "DEDUCT" BREAK
			#ENDIF
			
			DEFAULT
				IF eSpend = INT_TO_ENUM(SPEND_CATEGORIES, ENUM_TO_INT(MONEY_EARN_REFUND))
					RETURN "E:MONEY_EARN_REFUND"
				ENDIF
			BREAK
		ENDSWITCH
		
		RETURN ""
	ENDFUNC
	FUNC STRING DEBUG_GET_STRING_FROM_EARN_CATEGORIES(EARN_CATEGORIES eEarn)
		SWITCH eEarn
			CASE MONEY_EARN_JOBS				RETURN "JOBS" BREAK
			CASE MONEY_EARN_SELLING_VEH 		RETURN "SELLING_VEH" BREAK
			CASE MONEY_EARN_BETTING	 			RETURN "BETTING" BREAK
			CASE MONEY_EARN_GOOD_SPORT	 		RETURN "GOOD_SPORT" BREAK
			CASE MONEY_EARN_PICKED_UP			RETURN "PICKED_UP" BREAK
			CASE MONEY_EARN_SHARED				RETURN "SHARED" BREAK
			CASE MONEY_EARN_JOBSHARED			RETURN "JOBSHARED" BREAK
			CASE MONEY_EARN_ROCKSTAR_AWARD		RETURN "ROCKSTAR_AWARD" BREAK
			CASE MONEY_EARN_REFUND				RETURN "REFUND" BREAK
			CASE MONEY_EARN_BANK_INT			RETURN "BANK_INT" BREAK
			CASE MONEY_EARN_BANK_INTEREST		RETURN "BANK_INTEREST" BREAK
			CASE MONEY_EARN_FROM_JOB_BONUS		RETURN "FROM_JOB_BONUS" BREAK
			CASE MONEY_EARN_FROM_HEIST_JOB		RETURN "FROM_HEIST_JOB" BREAK
			
			DEFAULT
				IF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_PROPERTY_UTIL))
					RETURN "S:PROPERTY_UTIL"
				ELIF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_STYLE_ENT))
					RETURN "S:STYLE_ENT"
				ELIF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_CONTACT_SERVICE))
					RETURN "S:STYLE_ENT"
				ELIF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_HEALTHCARE))
					RETURN "S:HEALTHCARE"
				ELIF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_VEH_MAINTENANCE))
					RETURN "S:VEH_MAINTENANCE"
				ELIF eEarn = INT_TO_ENUM(EARN_CATEGORIES, ENUM_TO_INT(MONEY_SPENT_JOB_ACTIVITY))
					RETURN "S:JOB_ACTIVITY"
				ENDIF
			BREAK
		ENDSWITCH
		
		RETURN ""
	ENDFUNC
#ENDIF


PROC DECREMENT_STOCK_FILTERS_DURATIONS()
	CPRINTLN(DEBUG_STOCKS, "Updating active stock filters...")

	IF g_savedGlobals.sFinanceData.iFiltersRegistered < 1
		CPRINTLN(DEBUG_STOCKS, "No filters are registered. Exiting.")
		EXIT
	ENDIF
	
	INT i
	REPEAT MAX_SP_FINANCE_FILTERS i
		IF NOT IS_BIT_SET(g_savedGlobals.sFinanceData.iFilterFlags[i], MFFILTER_RESET_POST_EXILE3)
			IF g_savedGlobals.sFinanceData.iFilterDurationRemaining[i] > 0
				//Filter update duration is measured in log updates
				g_savedGlobals.sFinanceData.iFilterDurationRemaining[i] -= 1
				
				CPRINTLN(DEBUG_STOCKS, "Filter index ",i," duration remaining ", g_savedGlobals.sFinanceData.iFilterDurationRemaining[i])
				
				IF g_savedGlobals.sFinanceData.iFilterDurationRemaining[i] < 1
					g_savedGlobals.sFinanceData.iFilterDurationRemaining[i] = 0
					--g_savedGlobals.sFinanceData.iFiltersRegistered
					CDEBUG1LN(DEBUG_STOCKS, "Filter index ", i, " expired. Unregistered filter.")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	CPRINTLN(DEBUG_STOCKS, "...Filter updates finished.")
ENDPROC


FUNC INT GET_FREE_OR_OWNED_BAWSAQ_PORTFOLIO_INDEX(BAWSAQ_TRADERS save_to, BAWSAQ_COMPANIES stock_name)
	INT foundindex = -1
    BOOL foundBoughtAlready = FALSE
	
	INT i
    REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
        IF (GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to),i) = 0) AND (NOT foundBoughtAlready)
            foundindex = i
        ENDIF
        
        IF GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(save_to),i) = stock_name AND (GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to),i) > 0)
            foundBoughtAlready = TRUE
            foundindex = i
        ENDIF
    ENDREPEAT
	
	RETURN foundindex
ENDFUNC


PROC AWARD_LCN_SHARES(BAWSAQ_TRADERS save_to, BAWSAQ_COMPANIES stock_name, INT value)
	INT foundIndex = GET_FREE_OR_OWNED_BAWSAQ_PORTFOLIO_INDEX(save_to,stock_name)
	
	IF foundIndex = -1
		CPRINTLN(DEBUG_STOCKS, "AWARD_LCN_SHARES: Cannot award shares no space")
		EXIT
	ENDIF
	
	FLOAT fprice = GET_STOCK_PRICE(ENUM_TO_INT(stock_name))
	IF g_BS_Listings[stock_name].bOnlineStock
		CASSERTLN(DEBUG_STOCKS, "AWARD_LCN_SHARES called on an online listing ", ENUM_TO_INT(stock_name), ". This command should only be used with LCN stocks.")
		EXIT
	ENDIF
	
	INT totalgift = CEIL(TO_FLOAT(value)/fprice)
	
	SET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(save_to),foundindex,stock_name)
	SET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to),foundindex,totalgift,TRUE) 
ENDPROC


/// PARAMS:
///    buy_with - The account name to use funds from
///    save_to - The portfolio the bought stocks will be added to
///    stock_name - the type of stock to buy
///    number_of_shares - the amount of shares of given stock to buy
PROC BUY_STOCKS(enumBankAccountName buy_with, BAWSAQ_TRADERS save_to, BAWSAQ_COMPANIES stock_name, INT number_of_shares, FLOAT share_price)
    CPRINTLN(DEBUG_STOCKS, "Attemping to buy online stocks. Trader:", save_to, " Company:", stock_name, " NumberOfShares:", number_of_shares, " SharePrice:", share_price)

    // Find free stock portfolio index, or find already owned index.
    INT foundindex = GET_FREE_OR_OWNED_BAWSAQ_PORTFOLIO_INDEX(save_to, stock_name)
	
    IF foundindex = -1
		CPRINTLN(DEBUG_STOCKS, "Failed: There were no free portfolio indexes in this character's saved globals.")
		g_bBSWebsiteNoSpaceTrigger = TRUE
        EXIT
    ENDIF
   	g_bBSWebsiteNoSpaceTrigger = FALSE
	
	CDEBUG1LN(DEBUG_STOCKS, "Found portfolio index ", foundindex, " to save the stock data in.")
	
	// Check if we need to set up a fresh portfolio for these stocks.
	IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to), foundindex) = 0
		CDEBUG1LN(DEBUG_STOCKS, "The player doesn't own any stocks for this company. Cleaning out the portfolio index for new company.")
		SET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to), foundindex, 0.0)
	ENDIF
	
	// Safeguard against players spending more than they have.
	INT bat = CEIL(share_price)
	BOOL bForceBankAction = FALSE
	IF bat > g_BankAccounts[buy_with].iBalance
		CDEBUG1LN(DEBUG_STOCKS, "Purchase price is above the player's bank balance. Simply using max bank balance of $", g_BankAccounts[buy_with].iBalance, " instead.")
		bat = g_BankAccounts[buy_with].iBalance
		bForceBankAction = TRUE
	ENDIF
	
    IF NOT DO_BANK_ACCOUNT_ACTION(buy_with, BAA_DEBIT,BAAC_BROKERAGE_PAYMENT, bat, bForceBankAction)
		SCRIPT_ASSERT("BUY_STOCKS: Action failed because DO_BANK_ACCOUNT_ACTION failed.")
		EXIT
	ENDIF
	CDEBUG1LN(DEBUG_STOCKS, "Bank transaction completed.")
    
   	SET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to), foundindex, number_of_shares, TRUE)
    SET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to), foundindex, share_price, TRUE)
   	SET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(save_to), foundindex, stock_name) 
	g_savedGlobals.sFinanceData.iProfitLoss -= CEIL(share_price)  
	CDEBUG1LN(DEBUG_STOCKS, "Profit tracking for PURE_ALPHA_ACHIEVEMENT is now ", g_savedGlobals.sFinanceData.iProfitLoss, ".")
	
	//If the player invests more than $1 million dollars at once push
	//a message to the PS4 feed.
	IF share_price > 1000000
		REQUEST_SYSTEM_ACTIVITY_TYPE_STOCKMARKET_INVESTED()
	ENDIF
	
	//Can't have this run and trigger a save on mission due to 1589060.
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_STOCKS)

	IF number_of_shares < 1
		EXIT
	ENDIF
	
	IF g_BS_Listings[stock_name].bOnlineStock
		INT si = g_BS_Listings[stock_name].StatIndex
		IF si > -1
			STAT_SET_INT(g_BS_OIndexData[si].OnlineCharOwned[save_to],
			GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(save_to),foundindex))
		ENDIF
	ENDIF
    
	//Check for big investments to inform friends about.
	IF share_price > 1000000.0
		CDEBUG1LN(DEBUG_STOCKS, "Presence event triggered for purchase over $1000000.")
		PRESENCE_EVENT_UPDATESTAT_INT (SP0_CROUCHED_AND_SHOT, 10)
	ENDIF
	
	//If we've changed a BAWSAQ stock, update social club with the amount of stocks the character owns.
	IF g_BS_Listings[stock_name].bOnlineStock
		CDEBUG1LN(DEBUG_STOCKS, "Stock is a BAWSAQ stock. Updating social club stats.")
		UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(ENUM_TO_INT(save_to))
	ENDIF
	
	IF g_OnMissionState = MISSION_TYPE_OFF_MISSION
		CDEBUG1LN(DEBUG_STOCKS, "We're off-mission. Logging latest bank actions.")
		COPY_BANK_TRANSLOGS()
	ENDIF
	
    CPRINTLN(DEBUG_STOCKS, "Stock purchase complete.")
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    profits_to - the account name to place the proceeds in
///    sell_from - the portfolio to sell the given stock from
///    stock_name - the type of stock to sell
///    number_of_shares - the amount of shares to sell
PROC SELL_STOCKS(enumBankAccountName profits_to, BAWSAQ_TRADERS sell_from, BAWSAQ_COMPANIES stock_name, INT number_of_shares, FLOAT share_price)
    CPRINTLN(DEBUG_STOCKS, "Attemping to sell online stocks. Trader: ", sell_from, " Company:", stock_name, " NumberOfShares:", number_of_shares, " SharePrice:", share_price)
	
    // Does the portfolio selected have any of the needed stocks?
    INT foundindex = -1
    INT i = 0
    REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
        IF GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), i) = stock_name
            IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), i) > 0
                foundindex = i
            ENDIF
        ENDIF
    ENDREPEAT
	
    IF foundindex = -1
		CPRINTLN(DEBUG_STOCKS, "Failed: The character doesn't own any of the stock they are trying to sell.")
        SCRIPT_ASSERT("Player is trying to sell a stock they don't own any of. Bug *Default Levels*.")
        EXIT
    ENDIF
    
	INT iOwned = GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),foundindex)
	CDEBUG1LN(DEBUG_STOCKS, "The player owns ", iOwned, " of the stock.")
	
	FLOAT fInvestment = GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), foundindex)
	CDEBUG1LN(DEBUG_STOCKS, "The invested value of these stocks is ", fInvestment, ".")
	
    //Does we have too little of this stock to make the sale?
    IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),foundindex) < number_of_shares
		CPRINTLN(DEBUG_STOCKS, "Failed: The character doesn't own enough of the stock they are trying to sell.")
        SCRIPT_ASSERT("Script trying to sell more shares than the portfolio actually has!")
        EXIT
    ENDIF
    
    // Perform the sale.
   	IF NOT DO_BANK_ACCOUNT_ACTION(profits_to, BAA_CREDIT,BAAC_BROKERAGE_PAYMENT, FLOOR(share_price))
		SCRIPT_ASSERT("SELL_STOCKS: Action failed because DO_BANK_ACCOUNT_ACTION failed.")
	 	EXIT
	ENDIF
	CDEBUG1LN(DEBUG_STOCKS, "Bank transaction completed.")
    
	FLOAT winloss = share_price - ((GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), foundindex)/GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),foundindex)) * number_of_shares)
	CDEBUG1LN(DEBUG_STOCKS, "The character's profits from this sale over their initial investment is ", winloss, ".")
	
	//If the player has lost more than $50000 in this trade push a message to
	//the PS4 activity feed.
	IF winloss <= -50000
		REQUEST_SYSTEM_ACTIVITY_TYPE_STOCKMARKET_LOSS()
	ENDIF
	
    //Fix the money invested total if there are any shares left, or clear it if there are none left.
    SET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), foundindex, -share_price ,TRUE) 
    SET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), foundindex, -number_of_shares, TRUE) 
	g_savedGlobals.sFinanceData.iProfitLoss += FLOOR(share_price)
	
	CDEBUG1LN(DEBUG_STOCKS, "Profit tracking for PURE_ALPHA_ACHIEVEMENT is now ", g_savedGlobals.sFinanceData.iProfitLoss, ".")
	
	// If the acheivement hasn't already been met check if the latest change will trigger it.
	IF !CHECK_TRADING_PURE_ALPHA_ACHIEVEMENT()		
		FLOAT rat = TO_FLOAT(number_of_shares)/TO_FLOAT(iOwned) //selling x proportion of owned ratio
		FLOAT fAdjustedInvestment = fInvestment*rat
		IF share_price > fAdjustedInvestment
			CHECK_TRADING_PURE_ALPHA_ACHIEVEMENT(TRUE)	
		ENDIF
	ENDIF
    
    //Increment the volume for BAWSAQ online tracking.
	IF g_BS_Listings[stock_name].bOnlineStock
		INT si = g_BS_Listings[stock_name].StatIndex
		IF si > -1
			CDEBUG1LN(DEBUG_BAWSAQ, "Stock is a BAWSAQ stock. Updating stat for server to track.")
			STAT_SET_INT(g_BS_OIndexData[si].OnlineCharOwned[sell_from], GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), foundindex))
		ENDIF
	ENDIF
	
	//Check if the profit or loss was large enough to tell the player's friends about.
	IF winloss > 100000
		CDEBUG1LN(DEBUG_STOCKS, "The player has made an investment profit of over $100000. Triggering friend broadcast.")
		PRESENCE_EVENT_UPDATESTAT_INT(SP0_CROUCHED_AND_SHOT, 20)
	ELIF winloss < 0
		IF winloss < -100000
			CDEBUG1LN(DEBUG_STOCKS, "The player has made an investment loss of over $100000. Triggering friend broadcast.")
			PRESENCE_EVENT_UPDATESTAT_INT (SP0_CROUCHED_AND_SHOT, 30)
		ENDIF
	ENDIF
	
	//If we've changed a BAWSAQ stock, update social club with the amount of stocks the character owns.
	IF g_BS_Listings[stock_name].bOnlineStock
		CDEBUG1LN(DEBUG_BAWSAQ, "Stock is a BAWSAQ stock. Updating social club stats.")
		UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(ENUM_TO_INT(sell_from))
	ENDIF
	
	CPRINTLN(DEBUG_STOCKS, "Stock sale complete.")
ENDPROC


PROC SELL_ALL_STOCKS(enumBankAccountName profits_to, BAWSAQ_TRADERS sell_from)
	CPRINTLN(DEBUG_STOCKS, "Attemping to sell all online stocks. Trader: ", sell_from, ".")
	
	#IF IS_DEBUG_BUILD
		IF g_iStockSellAllQuote > 0
			CPRINTLN(DEBUG_STOCKS, "Price for sale quoted at $", g_iStockSellAllQuote, ".")
		ENDIF
	#ENDIF

	INT iOwned = 0
	INT iTypes = 0
	FLOAT fRawVal = 0
	
	FLOAT fSumProf = 0
	
	FLOAT fIncOnly = 0
	FLOAT fFallOnly = 0
	
	BOOL bAProfitWasMade = FALSE
	
	INT i
    REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
        IF GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),i) > 0
		
			FLOAT price = GET_CURRENT_STOCK_PRICE(ENUM_TO_INT(GET_COMPANY_INDEX_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),i)))
			
			IF price > 0
			
				INT iOwnedForCompany = GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from), i)
				iOwned += iOwnedForCompany
				
				++iTypes
			
			
				FLOAT thisraw = GET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),i) * price
				FLOAT inve = GET_INVESTED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),i)
				
				#IF IS_DEBUG_BUILD
					IF iOwnedForCompany > 0
						CDEBUG1LN(DEBUG_STOCKS, "Found ", iOwned, " stocks for company ", i, " worth ", thisraw, " owned by this character.")
					ENDIF
				#ENDIF
				
				fRawVal+= thisraw
				FLOAT proval = thisraw - inve
				
				IF( proval > 0)
					fIncOnly += proval
				ELSE
					fFallOnly += (-(proval))
				ENDIF
				fSumProf += proval
				
				IF !bAProfitWasMade AND proval > 0
					bAProfitWasMade = TRUE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_STOCKS, "Excluded selling stock at portfolio index ", i, " due to invalid current price of $", price, ".")
			ENDIF
        ENDIF
    ENDREPEAT
	
	CDEBUG1LN(DEBUG_STOCKS, "Found a total of ", iOwned, " stocks worth ", fRawVal, " for this character.")
	
    IF iOwned = 0
        CPRINTLN(DEBUG_STOCKS, "Failed: Cannot sell everything when the player owns nothing.")
		SCRIPT_ASSERT("SELL_ALL_STOCKS:  Cannot sell everything when the player owns nothing.")
        EXIT
    ENDIF
	
	// Work out the wealth raise value before we do the transaction and clear up the owned totals
	FLOAT fCurrfunds = TO_FLOAT(GET_ACCOUNT_BALANCE(profits_to))
	FLOAT fNewFunds = fRawVal + fCurrfunds
	IF fCurrfunds < 1.0
		fCurrfunds = 1.0
	ENDIF
	g_fLastStockSellAll_NewWealthPerc = (fNewFunds/fCurrfunds)*100.0
	g_savedGlobals.sFinanceData.iProfitLoss += FLOOR(fRawVal)
	
	CDEBUG1LN(DEBUG_STOCKS, "Profit tracking for PURE_ALPHA_ACHIEVEMENT is now ", g_savedGlobals.sFinanceData.iProfitLoss, ".")
	IF !CHECK_TRADING_PURE_ALPHA_ACHIEVEMENT()
		IF bAProfitWasMade
			CHECK_TRADING_PURE_ALPHA_ACHIEVEMENT(true)
		ENDIF
	ENDIF

	IF g_iStockSellAllQuote = 0
		CDEBUG1LN(DEBUG_STOCKS, "No quote set, using calculated value.")
		g_iStockSellAllQuote = FLOOR(fRawVal)
	ENDIF
	
	IF g_iStockSellAllQuote = 0
		CPRINTLN(DEBUG_STOCKS, "Failed: Cannot sell everything when its worth nothing. This shouldn't happen.")
		SCRIPT_ASSERT("SELL_ALL_STOCKS:  Cannot sell everything when its worth nothing.")
		EXIT
	ENDIF
	
	// Now do the transaction and zero out the owned stocks.
	IF NOT DO_BANK_ACCOUNT_ACTION(profits_to,BAA_CREDIT,BAAC_BROKERAGE_PAYMENT,g_iStockSellAllQuote)
		SCRIPT_ASSERT("SELL_ALL_STOCKS: Action failed because DO_BANK_ACCOUNT_ACTION failed.")
	 	EXIT
	ENDIF
	CDEBUG1LN(DEBUG_STOCKS, "Bank transaction completed.")
	
	g_iStockSellAllQuote = 0
	i = 0
	REPEAT MAX_BAWSAQ_PORTFOLIO_ENTRIES_PER_TRADER i
		SET_OWNED_TOTAL_FROM_PORTFOLIO(ENUM_TO_INT(sell_from),i,0)
	ENDREPEAT

	// Made it this far then success, log values and bail.
	g_iLastStockSellAll_Totshares = iOwned
	g_iLastStockSellAll_Cotot = iTypes
	g_fLastStockSellAll_Valover = fIncOnly
	g_fLastStockSellAll_Valunder = fFallOnly
	g_fLastStockSellAll_Totval = fRawVal
	g_fLastStockSellAll_Profval = fSumProf
	CDEBUG1LN(DEBUG_STOCKS, "Sale values logged.")
	
	//We've sold a variety of stocks. Some might be BAWSAQ so update Social Club with owned stock values.
	CDEBUG1LN(DEBUG_BAWSAQ, "Updating social club stats in case we just sold BAWSAQ stocks.")
	UPDATE_SOCIAL_CLUB_BAWSAQ_PORTFOLIO_STATS_FOR_CHARACTER(ENUM_TO_INT(sell_from))

	CPRINTLN(DEBUG_STOCKS, "All stocks sold sucessfully.")
ENDPROC


PROC FORCE_STOCK_LOG_UPDATE(INT iNumberOfUpdates = 1)
	CPRINTLN(DEBUG_STOCKS, "Script ", GET_THIS_SCRIPT_NAME(), " has requested an instant log update from the stock controller.")
	g_iForceStockLogUpdate = iNumberOfUpdates
ENDPROC


/// PURPOSE:
///		Find the highest value a stock has held in its logged history.
FUNC FLOAT GET_HIGH_VALUE_FOR_COMPANY(BAWSAQ_COMPANIES target)
	FLOAT ret = 0
	
	INT i
	REPEAT MAX_STOCK_PRICE_LOG_ENTRIES i
		IF g_BS_Listings[target].LoggedPrices[i] > 0.0
			IF ret < g_BS_Listings[target].LoggedPrices[i]
				ret = g_BS_Listings[target].LoggedPrices[i]
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN ret
ENDFUNC


/// PURPOSE:
///		Find the lowest value a stock has held in its logged history.
FUNC FLOAT GET_LOW_VALUE_FOR_COMPANY(BAWSAQ_COMPANIES target)
	FLOAT ret = 3.402823E+38 //Max float.
	
	INT i
	REPEAT MAX_STOCK_PRICE_LOG_ENTRIES i
		IF g_BS_Listings[target].LoggedPrices[i] > 0.0
			IF ret > g_BS_Listings[target].LoggedPrices[i]
				ret = g_BS_Listings[target].LoggedPrices[i]
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN ret
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///    this stuff should probably be moved to a private header


PROC PUSH_PRICE_TRIGGER(BAWSAQ_COMPANIES forCompany,
						STRING header, STRING content,
						eSTOCK_MARKET_NEWS_STORY_PRICE_TRIGGER_TYPE type,
						eSTOCK_STORY_CATEGORY category = eSS_main)
	
	IF g_iPRICE_CARET = PRICE_TRIGGER_STACK_TOTAL
		SCRIPT_ASSERT("PUSH_PRICE_TRIGGER: no space left to add price triggers")
		EXIT
	ENDIF

	g_BS_PriceTriggers[g_iPRICE_CARET].t = type

	g_BS_PriceTriggers[g_iPRICE_CARET].s.eCat = category
	g_BS_PriceTriggers[g_iPRICE_CARET].s.tHeader = header
	g_BS_PriceTriggers[g_iPRICE_CARET].s.tSummary = content
	g_BS_PriceTriggers[g_iPRICE_CARET].s.storyID = g_iSTORY_ID_CARET
	
	g_BS_PriceTriggers[g_iPRICE_CARET].c = forCompany
	
	++g_iPRICE_CARET
	++g_iSTORY_ID_CARET
ENDPROC


PROC PUSH_MODIFIER_TRIGGER(BSMF_TYPES forModifier,
						   STRING header, STRING content,
						   eSTOCK_MARKET_NEWS_STORY_MODIFIER_TRIGGER_TYPE type,
						   eSTOCK_STORY_CATEGORY category = eSS_main)
	
	IF g_iMODIFIER_CARET = g_iMODIFIER_CARET
		SCRIPT_ASSERT("PUSH_MODIFIER_TRIGGER: no space left to add price triggers")
		EXIT
	ENDIF

	g_BS_ModifierTrigger[g_iMODIFIER_CARET].t = type
	
	g_BS_ModifierTrigger[g_iMODIFIER_CARET].s.eCat = category
	g_BS_ModifierTrigger[g_iMODIFIER_CARET].s.tHeader = header
	g_BS_ModifierTrigger[g_iMODIFIER_CARET].s.tSummary = content
	g_BS_ModifierTrigger[g_iMODIFIER_CARET].s.storyID = g_iSTORY_ID_CARET
	
	
	g_BS_ModifierTrigger[g_iMODIFIER_CARET].m = forModifier
	
	++g_iMODIFIER_CARET
	++g_iSTORY_ID_CARET
ENDPROC


FUNC BOOL STACKIFY_STORY(STOCK_MARKET_NEWS_STORY &sto, INT force_category = -1)
	#IF IS_DEBUG_BUILD
	IF g_bForceBawsaqNews
		CPRINTLN(DEBUG_STOCKS, "Ignore trying to stackify story ID ", sto.storyID, ".")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	CDEBUG1LN(DEBUG_STOCKS, "Trying to stackify story ID ", sto.storyID, ".")

	// Find a slot with lower freshness and plop in the story.
	eSTOCK_STORY_CATEGORY c = sto.eCat
	
	IF force_category != -1
		c = INT_TO_ENUM(eSTOCK_STORY_CATEGORY,force_category)
	ENDIF
	
	INT i = 0
	
	REPEAT BAWSAQ_STORIES_PER_CATEGORY i
		IF g_BS_NewsStack[c][i].storyID = sto.storyID
			CDEBUG1LN(DEBUG_STOCKS, "Failed. The story was already on the stack.")
			sto.freshness += 0.05 //each time a story is triggered it gets less fresh
			RETURN FALSE//this story is already on the stack, don't add
		ENDIF
	ENDREPEAT
	
	i = 0
	
	FLOAT f = 0.0
	INT lowestindex = -1
	REPEAT BAWSAQ_STORIES_PER_CATEGORY i
		
		IF g_BS_NewsStack[c][i].freshness > f
			lowestindex = i
			f = g_BS_NewsStack[c][i].freshness
			
		ENDIF
		
	ENDREPEAT
	
	IF lowestindex = -1
		RETURN FALSE
	ENDIF
	
	//This is fresher than this story, add and exit.
	IF sto.freshness <= g_BS_NewsStack[c][lowestindex].freshness
		sto.freshness += 1.0 // Each time a story is triggered it gets less fresh.
		
		g_BS_NewsStack[c][lowestindex].freshness = sto.freshness
		g_BS_NewsStack[c][lowestindex].tHeader = sto.tHeader
		g_BS_NewsStack[c][lowestindex].tSummary = sto.tSummary
		g_BS_NewsStack[c][lowestindex].storyID = sto.storyID
		g_BS_NewsStack[c][lowestindex].eCat = sto.eCat
		
		CDEBUG1LN(DEBUG_STOCKS, "Placed story \"", sto.tHeader, "\" in news stack at ", c ," - ", lowestindex )
		RETURN TRUE
	ENDIF

	CDEBUG1LN(DEBUG_STOCKS, "Failed. Every story was fresher than this story! There were no valid slots on the stack.")
	RETURN FALSE
ENDFUNC


//Move these init funcs to finance private
PROC INITIALISE_MARKET_NEWS_STORIES()
	g_iPRICE_CARET = 0
	g_iSTORY_ID_CARET = 0
	
	CDEBUG1LN(DEBUG_STOCKS, "Clearing all news strings to the default.")
	INT i, j
	REPEAT eSS_Max_Categories i
		REPEAT BAWSAQ_STORIES_PER_CATEGORY j
			g_BS_NewsStack[i][j].tHeader = "BSNEWS_DEFAULT" 
			g_BS_NewsStack[i][j].tSummary = "BSNEWS_DEFAULT" 
		ENDREPEAT
	ENDREPEAT
	
	
	
	//Set initial story strings.
	//PRICE_STORY_SUSTAINED_RISE: 		Sustained price increase over 10 ticks with end price greater than 50%+ of starting.
	//PRICE_STORY_SUSTAINED_FALL: 		Sustained price fall over 10 ticks with end price lower than 50%- of starting.
	//PRICE_STORY_RAPID_FLUCTUATION:	At least 5 price reversals in the last 10 ticks combined with at least a 20% price change on average, with finishing price within 20%+/- of start.
	
	CDEBUG1LN(DEBUG_STOCKS, "Setting initial story strings for all offline stocks.")
	
	// eCola// Offline
	PUSH_PRICE_TRIGGER(BS_CO_ECL,"ECL_P_SR_H","ECL_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_ECL,"ECL_P_SF_H","ECL_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_ECL,"ECL_P_RF_H","ECL_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// BurgerShot// Offline
	PUSH_PRICE_TRIGGER(BS_CO_BGR,"BGR_P_SR_H","BGR_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_BGR,"BGR_P_SF_H","BGR_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	//PUSH_PRICE_TRIGGER(BS_CO_BGR,"BGR_P_RF_H","BGR_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	 // CluckingBell// Offline
	PUSH_PRICE_TRIGGER(BS_CO_CLK,"CLK_P_SR_H","CLK_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_CLK,"CLK_P_SF_H","CLK_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_CLK,"CLK_P_RF_H","CLK_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// BeanMachine// Offline
	PUSH_PRICE_TRIGGER(BS_CO_BEN,"BEN_P_SR_H","BEN_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_BEN,"BEN_P_SF_H","BEN_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_BEN,"BEN_P_RF_H","BEN_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// Fleeca// Offline
	PUSH_PRICE_TRIGGER(BS_CO_FLC,"FLC_P_SR_H","FLC_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_FLC,"FLC_P_SF_H","FLC_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_FLC,"FLC_P_RF_H","FLC_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// Prolaps// Offline
	PUSH_PRICE_TRIGGER(BS_CO_PRO,"PRO_P_SR_H","PRO_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_PRO,"PRO_P_SF_H","PRO_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_PRO,"PRO_P_RF_H","PRO_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// VanillaUnicorn// Offline
	PUSH_PRICE_TRIGGER(BS_CO_UNI,"UNI_P_SR_H","UNI_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_UNI,"UNI_P_SF_H","UNI_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_UNI,"UNI_P_RF_H","UNI_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// Krapea// Offline
	PUSH_PRICE_TRIGGER(BS_CO_KRP,"KRP_P_SR_H","KRP_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_KRP,"KRP_P_SF_H","KRP_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_KRP,"KRP_P_RF_H","KRP_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// FlyUS// Offline
	PUSH_PRICE_TRIGGER(BS_CO_FUS,"FUS_P_SR_H","FUS_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_FUS,"FUS_P_SF_H","FUS_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_FUS,"FUS_P_RF_H","FUS_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// GoPostal// Offline
	PUSH_PRICE_TRIGGER(BS_CO_GOP,"GOP_P_SR_H","GOP_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_GOP,"GOP_P_SF_H","GOP_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_GOP,"GOP_P_RF_H","GOP_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// Banner// Offline
	PUSH_PRICE_TRIGGER(BS_CO_BAN,"BAN_P_SR_H","BAN_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_BAN,"BAN_P_SF_H","BAN_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_BAN,"BAN_P_RF_H","BAN_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// MaxRena// Offline
	PUSH_PRICE_TRIGGER(BS_CO_MAX,"MAX_P_SR_H","MAX_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_MAX,"MAX_P_SF_H","MAX_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_MAX,"MAX_P_RF_H","MAX_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// GastroBans// Offline
	PUSH_PRICE_TRIGGER(BS_CO_GAS,"GAS_P_SR_H","GAS_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_GAS,"GAS_P_SF_H","GAS_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_GAS,"GAS_P_RF_H","GAS_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// GruppeSechs// Offline
	PUSH_PRICE_TRIGGER(BS_CO_GRU,"GRU_P_SR_H","GRU_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_GRU,"GRU_P_SF_H","GRU_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_GRU,"GRU_P_RF_H","GRU_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	// Pump&Run// Offline
	PUSH_PRICE_TRIGGER(BS_CO_PMP,"PMP_P_SR_H","PMP_P_SR_C",PRICE_STORY_SUSTAINED_RISE)
	PUSH_PRICE_TRIGGER(BS_CO_PMP,"PMP_P_SF_H","PMP_P_SF_C",PRICE_STORY_SUSTAINED_FALL)
	PUSH_PRICE_TRIGGER(BS_CO_PMP,"PMP_P_RF_H","PMP_P_RF_C",PRICE_STORY_RAPID_FLUCTUATION)
	
	CDEBUG3LN(DEBUG_STOCKS, "g_iPRICE_CARET = ", g_iPRICE_CARET, ".")
	
	// Finally randomly populate the initial news list.
	CDEBUG1LN(DEBUG_STOCKS, "Populating the initial news list with random stories.")
	INT r = GET_RANDOM_INT_IN_RANGE(0,(g_iPRICE_CARET-1))
	REPEAT eSS_Max_Categories i
		REPEAT BAWSAQ_STORIES_PER_CATEGORY j
			g_BS_NewsStack[i][j].freshness = 5000
		
			WHILE NOT STACKIFY_STORY(g_BS_PriceTriggers[r].s, i)
				r = GET_RANDOM_INT_IN_RANGE(0,(g_iPRICE_CARET-1))
				CDEBUG3LN(DEBUG_STOCKS, "New random news story failed to stack.")
				WAIT(2000)
			ENDWHILE
			
			g_BS_PriceTriggers[r].s.freshness = 0.0
			r = GET_RANDOM_INT_IN_RANGE(0,(g_iPRICE_CARET-1))
			
			CDEBUG3LN(DEBUG_STOCKS, "New random news story sucessfully stacked.")
		ENDREPEAT
	ENDREPEAT
ENDPROC

PROC INIT_GIFT_COUPONS()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF	
	#IF FEATURE_GEN9_EXCLUSIVE
	INT iNumHSWUpgradeGift = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_HSW_UPG_GIFT_NEW)
	IF iNumHSWUpgradeGift > 0
		INT iNumCurrentHSWUpgradeCoupon = GET_NUM_AVAILABLE_HSW_UPGRADE_COUPON()
		iNumCurrentHSWUpgradeCoupon += iNumHSWUpgradeGift
		IF iNumCurrentHSWUpgradeCoupon <= g_sMPTunables.iHSW_UPGRADE_COUPON_LIMIT
			SET_MP_INT_PLAYER_STAT(MPPLY_DISCOUNT_HSW_UPGRADE, iNumCurrentHSWUpgradeCoupon)
		ELSE
			PRINTLN("INIT_GIFT_COUPONS Don't give HSW voucher as we hit the limit: ", iNumCurrentHSWUpgradeCoupon)
		ENDIF
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_HSW_UPG_GIFT_NEW, 0)
		PRINTLN("INIT_GIFT_COUPONS MPPLY_DISCOUNT_HSW_UPGRADE: ", iNumCurrentHSWUpgradeCoupon)
	ENDIF
	#ENDIF
ENDPROC

FUNC BOOL HAS_PRICE_STORY_TRIGGER_FIRED(INT triggerindex)
	//TODO integer check here
	BAWSAQ_COMPANIES co  = g_BS_PriceTriggers[triggerindex].c
	
	FLOAT current = g_BS_Listings[co].fCurrentPrice

	//g_BS_Listings[g_BS_PriceTriggers[i].c].iLogIndexCaret
	//MAX_STOCK_PRICE_LOG_ENTRIES
	//g_BS_Listings[g_BS_PriceTriggers[i].c].LoggedPrices
	INT frontindex = g_BS_Listings[co].iLogIndexCaret
	WHILE frontindex > (MAX_STOCK_PRICE_LOG_ENTRIES-1)
		frontindex = frontindex - MAX_STOCK_PRICE_LOG_ENTRIES
	ENDWHILE
	INT backindex = (frontindex - 10)
	WHILE backindex < 0
		backindex = MAX_STOCK_PRICE_LOG_ENTRIES + backindex
	ENDWHILE
	FLOAT from = g_BS_Listings[co].LoggedPrices[backindex]
	
	//make sure current price in log matches current
	g_BS_Listings[co].LoggedPrices[g_BS_Listings[co].iLogIndexCaret] = GET_STOCK_PRICE(ENUM_TO_INT(co))
	
	
	INT switchbacks = 0
	INT rises = 0
	INT falls = 0
	
	
	//look over the range from backindex to frontindex and count the spikes
	INT f = backindex + 1

	IF f = MAX_STOCK_PRICE_LOG_ENTRIES
		f = 0
	ENDIF

	
	INT i = 0
	INT l = f
	INT c = 0
	INT n = 0
	
	
	FLOAT risingaverage
	//look over the range from backindex to frontindex and find the average increase
	FLOAT fallingaverage
	//look over the range from backindex to frontindex and find the average decrease
	
	// l -> c -> n
	
	REPEAT 8 i
		c = l+1
		IF c = MAX_STOCK_PRICE_LOG_ENTRIES
		 c = 0
		ENDIF
		n = c+1
		IF n = MAX_STOCK_PRICE_LOG_ENTRIES
			n = 0
		ENDIF
		
		
		IF g_BS_Listings[co].LoggedPrices[l] < g_BS_Listings[co].LoggedPrices[c]
			++rises
			risingaverage += g_BS_Listings[co].LoggedPrices[c] - g_BS_Listings[co].LoggedPrices[l]
		ELSE
			++falls		
			fallingaverage += g_BS_Listings[co].LoggedPrices[l] - g_BS_Listings[co].LoggedPrices[c]
		ENDIF
		
		
		//switchback check
		IF (g_BS_Listings[co].LoggedPrices[l] < g_BS_Listings[co].LoggedPrices[c])
			AND (g_BS_Listings[co].LoggedPrices[n] < g_BS_Listings[co].LoggedPrices[c])
			//peak
			++switchbacks
		ENDIF
		IF (g_BS_Listings[co].LoggedPrices[l] > g_BS_Listings[co].LoggedPrices[c])
			AND (g_BS_Listings[co].LoggedPrices[n] > g_BS_Listings[co].LoggedPrices[c])
			//gutter
			++switchbacks
		ENDIF
		
		++l
		IF l = MAX_STOCK_PRICE_LOG_ENTRIES
			l = 0
		ENDIF
	ENDREPEAT
	
	
	IF rises < 0 
		risingaverage = risingaverage/rises
	ENDIF
	
	IF falls > 0
		fallingaverage = fallingaverage/falls
	ENDIF
	
	
	FLOAT diffdeg = current - from
	IF diffdeg < 0
		diffdeg *= -1
	ENDIF
	
	//FLOAT avtdiff = risingaverage - fallingaverage
	
	SWITCH g_BS_PriceTriggers[triggerindex].t
		CASE PRICE_STORY_SUSTAINED_RISE
			//for the last 9 logs what percentage is an increase?
			//if increase percentage is over X% of the last 9 ticks
			IF rises < 6
				RETURN FALSE
			ENDIF

			//and the average movement of those increases is greater than 4% of the price from 10 ticks ago
			IF risingaverage < (from*0.04)
				RETURN FALSE
			ENDIF
			
			CDEBUG1LN(DEBUG_STOCKS, "PRICE_STORY_SUSTAINED_RISE story trigger fired.")
			CDEBUG2LN(DEBUG_STOCKS, "(", from, " - > ", current, ")")
			CDEBUG2LN(DEBUG_STOCKS, "Diff:", diffdeg, " Switches:", switchbacks , " Rises:", rises , " Falls:", falls , " Rising avt:", risingaverage ," Falling avt:", fallingaverage)
			RETURN TRUE
		BREAK
		
		CASE PRICE_STORY_SUSTAINED_FALL
			IF falls < 6
				RETURN FALSE
			ENDIF


			IF fallingaverage < (from*0.04)
				RETURN FALSE
			ENDIF
			
			CDEBUG1LN(DEBUG_STOCKS, "PRICE_STORY_SUSTAINED_FALL story trigger fired.")
			CDEBUG2LN(DEBUG_STOCKS, "(", from, " - > ", current, ")")
			CDEBUG2LN(DEBUG_STOCKS, "Diff:", diffdeg, " Switches:", switchbacks , " Rises:", rises , " Falls:", falls , " Rising avt:", risingaverage ," Falling avt:", fallingaverage)
			RETURN TRUE
		BREAK
		
		CASE PRICE_STORY_RAPID_FLUCTUATION
			//for the last 10 ticks is the number of turnbacks > 4
			//is the price at the end of this window within x% of it's starting price?
			IF switchbacks < 7
				RETURN FALSE
			ENDIF
			
			IF diffdeg > (from*0.01)
				RETURN FALSE
			ENDIF
			
			CDEBUG1LN(DEBUG_STOCKS, "PRICE_STORY_RAPID_FLUCTUATION story trigger fired.")
			CDEBUG2LN(DEBUG_STOCKS, "(", from, " - > ", current, ")")
			CDEBUG2LN(DEBUG_STOCKS, "Diff:", diffdeg, " Switches:", switchbacks , " Rises:", rises , " Falls:", falls , " Rising avt:", risingaverage ," Falling avt:", fallingaverage)
			RETURN TRUE
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC


ENUM BAWSAQ_INTERNAL_CAR_COMPANIES
    BSICC_HVY,
    BSICC_MAI,
    BSICC_BRU,
    BSICC_VAP,
    BSICC_BFA,
    BSICC_HJK,
    BSICC_SHT,
    BSICC_UMA,
    BSICC_NONE
ENDENUM


FUNC BAWSAQ_INTERNAL_CAR_COMPANIES GET_MAKE_OF_VEHICLE(MODEL_NAMES mn)
	SWITCH mn
		//SM_VAPID_DESTROYED
        CASE BENSON
		CASE BOBCATXL
		CASE BULLET
		CASE DOMINATOR
		CASE MINIVAN
		CASE PEYOTE
		CASE RADI
		CASE SADLER
		CASE SADLER2
		CASE SANDKING
		CASE SANDKING2
		CASE SPEEDO
		CASE SPEEDO2
		CASE STANIER
        	RETURN BSICC_VAP
		BREAK
        
         //SM_BRUTE_DESTROYED
		CASE BOXVILLE
		CASE CAMPER
		CASE PONY
		CASE PONY2
		CASE STOCKADE
		CASE STOCKADE3
		CASE TIPTRUCK
        	RETURN BSICC_BRU
		BREAK
        
        //SM_SCHYSTER_DESTROYED
        CASE FUSILADE
        	RETURN BSICC_SHT
		BREAK
        
        //SM_MAIBATSU_DESTROYED
        CASE MULE
		CASE MULE2
		CASE PENUMBRA
		CASE SANCHEZ
		CASE SANCHEZ2
       		RETURN BSICC_MAI
		BREAK
  
        //SM_BFA_DESTROYED
        CASE BFINJECTION
		CASE DUNE
		CASE SURFER
		CASE SURFER2
        	RETURN BSICC_BFA
		BREAK
        
        //SM_HVY_DESTROYED
        CASE BARRACKS2
		CASE BIFF
		CASE BULLDOZER
		CASE CUTTER
		CASE DUMP
		CASE FORKLIFT
		CASE MIXER
		CASE MIXER2
        	RETURN BSICC_HVY
		BREAK
			  
		//SM_UBERMACHT_DESTROYED	  
        CASE ORACLE
        CASE ORACLE2
		CASE SENTINEL
		CASE SENTINEL2
		CASE ZION
		CASE ZION2
        	RETURN BSICC_UMA
		BREAK
	ENDSWITCH

	RETURN BSICC_NONE
ENDFUNC


FUNC BOOL IS_PLAYER_USING_ATM()
	RETURN g_bInATM
ENDFUNC


