//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   menu_cursor_3d_public.sch                                   		    	//
//      AUTHOR          :   Stephen Robertson                                              				//
//      DESCRIPTION     :   Common functions for handling mouse cursors in scripted menus in 3D 		//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "commands_graphics.sch"
USING "rc_area_public.sch"

/// PURPOSE:
///    Checks if the cursor is inside a 2D menu item on a menu rendered inside the game world.
/// PARAMS:
///    fItemOriginX - X Origin of the menu item inside the 3D menu, 0 to 1
///    fItemOriginY - Y Origin of the menu item inside the 3D menu, 0 to 1
///    fItemWidth - Width of menu item, 0 to 1
///    fItemHeight - Height of menu item, 0 to 1
///    v3DMenuOrigin - Origin of 3D menu in world coords
///    f3DMenuHeading - Heading of 3D menu 
///    v3DMenuSize - Size of 3D menu in world coords.
/// RETURNS:
///    TRUE if the cursor is inside the menu item.
///    
FUNC BOOL IS_CURSOR_INSIDE_3D_MENU_ITEM( FLOAT fItemOriginX, FLOAT fItemOriginY, FLOAT fItemWidth, FLOAT fItemHeight, VECTOR v3DMenuOrigin, FLOAT f3DMenuHeading, VECTOR v3DMenuSize  )

	VECTOR vMin			// Top left corner
	VECTOR vMax			// Bottom right corner
	
	
	VECTOR vPoly3D[4], vPoly2D[4]//, vBox[4]
	TEST_POLY sTestPoly
						
	vMin.x = v3DMenuSize.x * fItemOriginX
	vMin.y = v3DMenuSize.y * fItemOriginY
	
	vMax.x = vMin.x + (v3DMenuSize.x * fItemWidth)
	vMax.y = vMin.y + (v3DMenuSize.y * fItemHeight)		
	
	vPoly3D[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, <<-vMin.x, 0.0, -vMin.y >> )
	vPoly3D[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, <<-vMax.x, 0.0, -vMin.y >> )
	vPoly3D[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, <<-vMax.x,0.0 ,-vMax.y>>)
	vPoly3D[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, << -vMin.x, 0.0, -vMax.y>> )
		
	// RIGHT SIDE OFF SCREEN
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[0], vPoly2D[0].x, vPoly2D[0].y)
	OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[1], vPoly2D[1].x, vPoly2D[1].y)

		vPoly3D[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, <<  0, 0, -vMin.y >>  )
		vPoly3D[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, <<  0, 0, -vMax.y >>  )

		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[0], vPoly2D[0].x, vPoly2D[0].y)
		OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[1], vPoly2D[1].x, vPoly2D[1].y)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	// LEFT SIDE OFF SCREEN
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[2], vPoly2D[2].x, vPoly2D[2].y)
	OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[3], vPoly2D[3].x, vPoly2D[3].y)

		vPoly3D[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, << 0, 0, vMax.y >>  )
		vPoly3D[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v3DMenuOrigin, f3DMenuHeading, << 0, 0, vMin.y >>  )

		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[2], vPoly2D[2].x, vPoly2D[2].y)
		OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[3], vPoly2D[3].x, vPoly2D[3].y)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//	
//	DRAW_DEBUG_LINE_2D(vPoly2D[0], vPoly2D[1])
//	DRAW_DEBUG_LINE_2D(vPoly2D[1], vPoly2D[2])
//	DRAW_DEBUG_LINE_2D(vPoly2D[2], vPoly2D[3])
//	DRAW_DEBUG_LINE_2D(vPoly2D[3], vPoly2D[0])
	
	//DRAW_DEBUG_POLY_2D( vPoly2D[0], vPoly2D[1], vPoly2D[2],255,255,255,16 )
	//DRAW_DEBUG_POLY_2D( vPoly2D[2], vPoly2D[3], vPoly2D[0],255,255,255,16 )
	
	// Add a polygon for this object
	OPEN_TEST_POLY(sTestPoly)
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[0])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[1])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[2])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[3])
	CLOSE_TEST_POLY(sTestPoly)

	// Check if the point is inside the poly.
	IF IS_POINT_IN_POLY_2D(sTestPoly, <<g_fMenuCursorX, g_fMenuCursorY, 0>>)
//		DRAW_DEBUG_POLY_2D( vPoly2D[0], vPoly2D[1], vPoly2D[2],255,255,255,16 )
//		DRAW_DEBUG_POLY_2D( vPoly2D[2], vPoly2D[3], vPoly2D[0],255,255,255,16 )
//		DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", 1 )
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks if the cursor is inside the bounding box of an entity.
/// PARAMS:
///    iEntity - 
/// RETURNS:
///    
FUNC BOOL IS_CURSOR_INSIDE_ENTITY_BOUNDS( ENTITY_INDEX iEntity)

	FLOAT fHeading
	VECTOR vPos, vSizeMin, vSizeMax //, vSize
	VECTOR vPoly3D[4], vPoly2D[4]//, vBox[4]
	TEST_POLY sTestPoly
	
	IF NOT DOES_ENTITY_EXIST(iEntity)
		RETURN FALSE
	ENDIF
	
	vPos = GET_ENTITY_COORDS(iEntity)
	fHeading = GET_ENTITY_HEADING(iEntity)
	
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(iEntity), vSizeMin, vSizeMax )
	
	vPoly3D[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vSizeMin.x, 0, vSizeMin.z >>  )	
	vPoly3D[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vSizeMin.x, 0, vSizeMax.z >>  )
	vPoly3D[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vSizeMax.x, 0, vSizeMax.z >>  )	
	vPoly3D[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vSizeMax.x, 0, vSizeMin.z >>  )	

	// RIGHT SIDE OFF SCREEN
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[0], vPoly2D[0].x, vPoly2D[0].y)
	OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[1], vPoly2D[1].x, vPoly2D[1].y)

		vPoly3D[0] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<  0, 0, vSizeMin.z >>  )
		vPoly3D[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<  0, 0, vSizeMax.z >>  )

		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[0], vPoly2D[0].x, vPoly2D[0].y)
		OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[1], vPoly2D[1].x, vPoly2D[1].y)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	// LEFT SIDE OFF SCREEN
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[2], vPoly2D[2].x, vPoly2D[2].y)
	OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[3], vPoly2D[3].x, vPoly2D[3].y)

		vPoly3D[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << 0, 0, vSizeMax.z >>  )	
		vPoly3D[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << 0, 0, vSizeMin.z >>  )	

		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[2], vPoly2D[2].x, vPoly2D[2].y)
		OR NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPoly3D[3], vPoly2D[3].x, vPoly2D[3].y)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	/*
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	DRAW_DEBUG_LINE_2D(vPoly2D[0], vPoly2D[1])
	DRAW_DEBUG_LINE_2D(vPoly2D[1], vPoly2D[2])
	DRAW_DEBUG_LINE_2D(vPoly2D[2], vPoly2D[3])
	DRAW_DEBUG_LINE_2D(vPoly2D[3], vPoly2D[0])
	*/
	
	// Add a polygon for this object
	OPEN_TEST_POLY(sTestPoly)
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[0])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[1])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[2])
	ADD_TEST_POLY_VERT(sTestPoly, vPoly2D[3])
	CLOSE_TEST_POLY(sTestPoly)

	// Check if the point is inside the poly.
	IF IS_POINT_IN_POLY_2D(sTestPoly, <<g_fMenuCursorX, g_fMenuCursorY, 0>>)
		//DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.1, "NUMBER", 1 )
		RETURN TRUE
	ELSE
		// Sphere check at centre for smaller thinner objects
		FLOAT fX,fY
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fX, fY)
			IF VDIST( <<fX, Fy, 0.0>>, <<g_fMenuCursorX, g_fMenuCursorY, 0.0 >> ) < 0.05
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Checks if the cursor is inside a sphere of given radius
/// PARAMS:
///    iEntity - 
/// RETURNS:
///    
FUNC BOOL IS_CURSOR_INSIDE_SPHERE( VECTOR vPos, FLOAT fRadius)

	VECTOR center2D
	VECTOR edge2D
	VECTOR tempEdge2D
	VECTOR cursorPos
	FLOAT ang
	INT i
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN FALSE
	ENDIF
		
	cursorPos.x = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	cursorPos.y = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)

	// workout center
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, center2D.x, center2D.y)
		RETURN FALSE
	ENDIF
	
	// check external point - could probably use 6 points for this instead and calculate magnitude between 2 points
	REPEAT 8 i
		ang = TO_FLOAT(i) * 45.0
		IF GET_SCREEN_COORD_FROM_WORLD_COORD(vPos + <<SIN(ang) * fRadius, COS(ang) * fRadius, 0>>, tempEdge2D.x, tempEdge2D.y)
			edge2D = tempEdge2D
			i = 999
		ENDIF
	ENDREPEAT
	
	IF IS_VECTOR_ZERO(edge2D)
		RETURN FALSE
	ENDIF
	
	VECTOR v
	FLOAT centerToMouseMag
	FLOAT centerToEdgeMag
	
	// get magitudes from center to cursor point and center to edge point
	v = cursorPos - center2D
	centerToMouseMag = (v.x * v.x + v.y * v.y + v.z * v.z)
	
	v = edge2D - center2D
	centerToEdgeMag = (v.x * v.x + v.y * v.y + v.z * v.z)
	
	RETURN centerToMouseMag <= centerToEdgeMag
ENDFUNC
