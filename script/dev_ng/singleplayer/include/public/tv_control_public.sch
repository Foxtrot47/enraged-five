USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_audio.sch"
USING "globals.sch"
USING "flow_mission_data_public.sch"

ENUM TV_CHANNEL_PLAYLIST
	TV_PLAYLIST_NONE=0,
	
	TV_PLAYLIST_STD_CNT,
	TV_PLAYLIST_STD_WEAZEL,
	
	TV_PLAYLIST_LO_CNT,
	TV_PLAYLIST_LO_WEAZEL,
	
	TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER,
	TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER_CUTSCENE,
	
	TV_PLAYLIST_SPECIAL_WORKOUT,
	TV_PLAYLIST_SPECIAL_INVADER,
	TV_PLAYLIST_SPECIAL_INVADER_EXP,
	TV_PLAYLIST_SPECIAL_PLSH1_INTRO,
	TV_PLAYLIST_SPECIAL_LEST1_INTRO_FAME_OR_SHAME,
	TV_PLAYLIST_STD_WEAZEL_FOS_EP2,
	
	TV_PLAYLIST_SPECIAL_MP_WEAZEL,
	TV_PLAYLIST_SPECIAL_MP_CCTV,

	TV_PLAYLIST_END
ENDENUM

BOOL bBlockTVChannelControl, bBlockTVVolumeControl
INT iTVControlTimer
BOOL bAllowTVSoundFX = TRUE

FUNC STRING GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_CHANNEL_PLAYLIST tvPlaylist)
	STRING sReturn
	SWITCH tvPlaylist
		CASE TV_PLAYLIST_STD_CNT
			sReturn = "PL_STD_CNT"
		BREAK
		CASE TV_PLAYLIST_STD_WEAZEL
			sReturn = "PL_STD_WZL"
		BREAK
		CASE TV_PLAYLIST_LO_CNT
			sReturn = "PL_LO_CNT"
		BREAK
		
		CASE TV_PLAYLIST_LO_WEAZEL
			sReturn = "PL_LO_WZL"
		BREAK
				
		CASE TV_PLAYLIST_SPECIAL_WORKOUT
			sReturn = "PL_SP_WORKOUT"
		BREAK
		CASE TV_PLAYLIST_SPECIAL_INVADER
			sReturn = "PL_SP_INV"
		BREAK
		CASE TV_PLAYLIST_SPECIAL_INVADER_EXP
			sReturn = "PL_SP_INV_EXP"
		BREAK
		CASE TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER
			sReturn = "PL_LO_RS"
		BREAK
		
		CASE TV_PLAYLIST_LO_RIGHTEOUS_SLAUGHTER_CUTSCENE
			sReturn = "PL_LO_RS_CUTSCENE"
		BREAK
		
		CASE TV_PLAYLIST_SPECIAL_PLSH1_INTRO
			sReturn = "PL_SP_PLSH1_INTRO"
		BREAK
		
		CASE TV_PLAYLIST_SPECIAL_LEST1_INTRO_FAME_OR_SHAME
			sReturn = "PL_LES1_FAME_OR_SHAME"
		BREAK
		
		CASE TV_PLAYLIST_STD_WEAZEL_FOS_EP2
			sReturn = "PL_STD_WZL_FOS_EP2"
		BREAK
		
		CASE TV_PLAYLIST_SPECIAL_MP_WEAZEL
			sReturn = "PL_MP_WEAZEL"
		BREAK
		
		CASE TV_PLAYLIST_SPECIAL_MP_CCTV
			sReturn = "PL_MP_CCTV"
		BREAK
		
	ENDSWITCH
	
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> GET_XML_PLAYLIST_FOR_TV_PLAYLIST() - PLAYLIST STRING = ", sReturn)
	
	RETURN sReturn
ENDFUNC

FUNC INT SETUP_CUSTOM_TV_SCREEN( OBJECT_INDEX objectTV )

	INT iRenderTarget
	
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		REGISTER_NAMED_RENDERTARGET("tvscreen")
		LINK_NAMED_RENDERTARGET(GET_ENTITY_MODEL(objectTV))
		IF NOT IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(objectTV))
			RELEASE_NAMED_RENDERTARGET("tvscreen")			
		ENDIF
	ENDIF
	
	iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID("tvscreen")
	SET_TEXT_RENDER_ID(iRenderTarget)
		
	RETURN iRenderTarget
	
ENDFUNC

PROC DRAW_TO_CUSTOM_TV_SCREEN( OBJECT_INDEX objectTV, INT iRenderTarget )

    SET_TEXT_RENDER_ID(iRenderTarget)
	DRAW_TV_CHANNEL(0.5,0.5,1.0,1.0,0.0,255,255,255,255)
	
	IF GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_NONE
		ATTACH_TV_AUDIO_TO_ENTITY(objectTV)
	ENDIF
	
ENDPROC


PROC CLEANUP_CUSTOM_TV_SCREEN( OBJECT_INDEX objectTV, INT iRenderTarget )

	IF NOT (iRenderTarget = -1)
		SET_TEXT_RENDER_ID(iRenderTarget)
		
		IF DOES_ENTITY_EXIST(objectTV)
			
			IF GET_ENTITY_MODEL(objectTV) = V_ILEV_MM_SCREEN2
				
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)  // Clears the projector render target to white.
				
				IF IS_NAMED_RENDERTARGET_REGISTERED("big_disp")
					RELEASE_NAMED_RENDERTARGET("big_disp")
				ENDIF
			
			ELSE
				IF GET_ENTITY_MODEL(objectTV) = V_ILEV_MM_SCRE_OFF
					//DRAW_RECT(0.5, 0.5, 1.0, 1.0, 255, 255, 255, 255)  // Clears the projector render target to white.
				ELSE
					DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)  // Clears the render target to black.
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
			RELEASE_NAMED_RENDERTARGET("tvscreen")
		ENDIF
		
	ENDIF


ENDPROC


FUNC BOOL IS_THIS_TV_AVAILABLE_FOR_USE( TV_LOCATION eTVLocation )
	
	IF eTVLocation != TV_LOC_NONE
		IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bAvailableForUse
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_THIS_TV_ON( TV_LOCATION eTVLocation )
	
	IF eTVLocation != TV_LOC_NONE
		IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn
			RETURN TRUE
		ENDIF	
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_THIS_TV_FORCED_ON(TV_LOCATION eTVLocation)
	
	IF eTVLocation != TV_LOC_NONE
		IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC RESTORE_STANDARD_CHANNELS()

	SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_1, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_STD_CNT))
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
		SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_STD_WEAZEL_FOS_EP2))
	ELSE
		SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_STD_WEAZEL))
	ENDIF
	#endif
	#endif
	
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> RESTORE_STANDARD_CHANNELS() - FOS EP2")
	
ENDPROC

PROC PLAY_TV_CHANNEL_WITH_PLAYLIST(TV_LOCATION eTVLocation, TVCHANNELTYPE tvChannel, TV_CHANNEL_PLAYLIST tvPlaylist, BOOL bFromStart=FALSE)
	
	IF eTVLocation != TV_LOC_NONE
	
		IF NOT IS_THIS_TV_AVAILABLE_FOR_USE(eTVlocation)
			SCRIPT_ASSERT("PLAY_TV_CHANNEL_WITH_PLAYLIST() - TV script has not initialised! Please use IS_THIS_TV_AVAILABLE_FOR_USE() before attempting TV playback. ")
			EXIT
		ENDIF
		
		SET_TV_CHANNEL_PLAYLIST(tvChannel, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(tvPlaylist), bFromStart)
		SET_TV_CHANNEL(tvChannel)
	
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(tvChannel)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(tvPlaylist)
	ENDIF
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> PLAY_TV_CHANNEL_WITH_PLAYLIST() = ")
ENDPROC

PROC PLAY_TV_CHANNEL(TV_LOCATION eTVLocation, TVCHANNELTYPE tvChannel )

	IF  tvChannel <> TVCHANNELTYPE_CHANNEL_SCRIPT
	AND tvChannel <> TVCHANNELTYPE_CHANNEL_SPECIAL
		
		IF eTVLocation != TV_LOC_NONE
			SET_TV_CHANNEL(tvChannel)
			g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(tvChannel)
		ENDIF
	ENDIF
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> PLAY_TV_CHANNEL()")
ENDPROC

PROC FORCE_STOP_TV(TV_LOCATION eTVLocation)

	IF eTVlocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = TRUE
	ENDIF
	
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> FORCE_STOP_TV() = ")

ENDPROC


FUNC TV_LOCATION GET_TV_ENUM_FOR_INTERIOR_ROOM( TEXT_LABEL_31 RoomName )

	IF IS_STRING_NULL_OR_EMPTY(RoomName)
		RETURN TV_LOC_NONE
	ENDIF

	IF ARE_STRINGS_EQUAL(RoomName, "V_Michael_G_Lounge")
		RETURN TV_LOC_MICHAEL_PROJECTOR
	ENDIF

	IF ARE_STRINGS_EQUAL(RoomName, "V_Michael_1_Son" )
		RETURN TV_LOC_JIMMY_BEDROOM
	ENDIF
	
	RETURN TV_LOC_NONE
	
ENDFUNC

PROC START_AMBIENT_TV_PLAYBACK( TV_LOCATION eTVLocation, TVCHANNELTYPE eTVChannelType = TVCHANNELTYPE_CHANNEL_1, TV_CHANNEL_PLAYLIST eTVPlaylist = TV_PLAYLIST_STD_CNT, BOOL bAllowPlayerControl = FALSE, BOOL bPlayFromBeginning = FALSE )
	
	IF eTVLocation != TV_LOC_NONE
	
		IF NOT IS_THIS_TV_AVAILABLE_FOR_USE(eTVlocation)
			SCRIPT_ASSERT("START_AMBIENT_TV_PLAYBACK() - TV script has not initialised! Please use IS_THIS_TV_AVAILABLE_FOR_USE() before attempting to start TV playback. ")
			EXIT
		ENDIF
		
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV = TRUE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelType = ENUM_TO_INT(eTVChannelType)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].iTVChannelPlaylist = ENUM_TO_INT(eTVPlaylist)
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bStartPlaylistFromBeginning = bPlayFromBeginning
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = bAllowPlayerControl
		
		// If the TV is already on (even if player controlled) change the channel and playlist immediately.
		IF IS_THIS_TV_ON(eTVLocation)
			SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
			SET_TV_CHANNEL_PLAYLIST(eTVChannelType, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(eTVPlaylist), bPlayFromBeginning)
			SET_TV_CHANNEL(eTVChannelType)
		ENDIF
	
	ENDIF
	
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> START_AMBIENT_TV_PLAYBACK() - TV ID: ", ENUM_TO_INT(eTVLocation) )
	g_sTVPlaybackStartedByThisScript = GET_THIS_SCRIPT_NAME()
ENDPROC

PROC STOP_AMBIENT_TV_PLAYBACK(TV_LOCATION eTVLocation)
	
	IF eTVLocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = TRUE
	ENDIF
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> STOP_AMBIENT_TV_PLAYBACK()")
ENDPROC

PROC STOP_TV_PLAYBACK(BOOL bRestoreStandardChannels = TRUE, TV_LOCATION eTVLocation = TV_LOC_NONE )
	
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	IF bRestoreStandardChannels
		RESTORE_STANDARD_CHANNELS()
	ENDIF
	
	IF eTVLocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStopTV = TRUE
	ENDIF
	
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> STOP_TV_PLAYBACK() for TV started by ", g_sTVPlaybackStartedByThisScript)
	g_sTVPlaybackStartedByThisScript = "NULL"
ENDPROC

FUNC BOOL IS_TV_SCRIPT_AVAILABLE_FOR_USE(TV_LOCATION eTVLocation)
	
	IF eTVLocation != TV_LOC_NONE
		IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bAvailableForUse = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DISABLE_TV_CONTROLS( TV_LOCATION eTVLocation, BOOL bDisable )

	IF eTVLocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled = bDisable
	ENDIF

ENDPROC

FUNC BOOL IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL(BOOL bIgnoreTimer = FALSE)
	INT iDeadzone = 64
	INT iAxis
	iAxis = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X ) -127
	IF bIgnoreTimer
	OR TIMERA() > 300 
		//change the channels.
		IF iAxis > 0+iDeadzone
		OR iAxis < 0-iDeadzone
			SETTIMERA(0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOCATION eTVLocation)

	IF eTVLocation != TV_LOC_NONE
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartAmbientTV = FALSE
		//g_TVStruct[ENUM_TO_INT(eTVLocation)].bForceStartTV = FALSE
		IF g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsTVOn
		AND g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = FALSE
			g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsPlayerControlled = TRUE
		ENDIF
		g_TVStruct[ENUM_TO_INT(eTVLocation)].bTVControlsDisabled = FALSE
	ENDIF
	
ENDPROC

PROC RUN_TV_TRANSPORT_CONTROLS(BOOL bAllowChannelChange=TRUE, BOOL bAllowVolumeChange=TRUE)
	INT iDeadzone = 64
	INT iAxis
	FLOAT fTVVolume
	IF bAllowChannelChange
		IF IS_INPUT_ATTEMPTING_TO_CHANGE_CHANNEL()
			IF NOT bBlockTVChannelControl
			
		
				IF REQUEST_AMBIENT_AUDIO_BANK( "SAFEHOUSE_MICHAEL_SIT_SOFA" )
					PLAY_SOUND_FRONTEND(-1, "MICHAEL_SOFA_TV_CHANGE_CHANNEL_MASTER") 
				ENDIF
			
				IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_1
					SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
					PRINTLN(GET_THIS_SCRIPT_NAME(), " - RUN_TV_TRANSPORT_CONTROLS - SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)")
				ELSE
					SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)	
					PRINTLN(GET_THIS_SCRIPT_NAME(), " - RUN_TV_TRANSPORT_CONTROLS - SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)")
				ENDIF
				bBlockTVChannelControl = TRUE
			ENDIF
		ELSE
			IF bBlockTVChannelControl
				bBlockTVChannelControl = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF bAllowVolumeChange
		iAxis = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_SCRIPT_LEFT_AXIS_Y) - 127
		//PRINTLN(GET_THIS_SCRIPT_NAME(), " - iAxis = ", iAxis)
//		// Movie Volume in dB -24 to 0.
		IF NOT bBlockTVVolumeControl
		
			IF iAxis > 0+iDeadzone
					
				fTVVolume = GET_TV_VOLUME()
				fTVVolume = (fTVVolume - 0.5)
				
				IF fTVVolume < -36.0
					fTVVolume = -36.0
				ENDIF
				
				SET_TV_VOLUME(fTVVolume)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - RUN_TV_TRANSPORT_CONTROLS - SET_TV_VOLUME = ", fTVVolume)
				iTVControlTimer = GET_GAME_TIMER()
				bBlockTVVolumeControl = TRUE
				
			ENDIF
			
			//turn movie volume down.
			IF iAxis < 0-iDeadzone
				fTVVolume = GET_TV_VOLUME()
				
				fTVVolume = (fTVVolume + 0.5)

				IF fTVVolume > 0.0
					fTVVolume = 0.0
				ENDIF
				
				SET_TV_VOLUME(fTVVolume)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - RUN_TV_TRANSPORT_CONTROLS - SET_TV_VOLUME = ", fTVVolume)
				iTVControlTimer = GET_GAME_TIMER()
				bBlockTVVolumeControl = TRUE
				
			ENDIF
			
			IF iAxis < 0+iDeadzone
			AND iAxis > 0-iDeadzone
				bAllowTVSoundFX = TRUE
			ELSE
				IF bAllowTVSoundFX
					
					IF REQUEST_AMBIENT_AUDIO_BANK( "SAFEHOUSE_MICHAEL_SIT_SOFA" )
						IF fTVVolume != -36.0 AND fTVVolume != 0
							PLAY_SOUND_FRONTEND(-1, "MICHAEL_SOFA_REMOTE_CLICK_VOLUME_MASTER")
						ENDIF
						bAllowTVSoundFX = FALSE
					ENDIF
					
				ENDIF
			ENDIF
			
			
		ENDIF
		IF bBlockTVVolumeControl
			IF iAxis = 0
			OR GET_GAME_TIMER() > iTVControlTimer + 24
				bBlockTVVolumeControl = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC SET_TV_INDESTRUCTIBLE( TV_LOCATION eTVLocation, BOOL bIsIndestructible )

	PRINTLN(GET_THIS_SCRIPT_NAME(), " - SET_TV_INDESTRUCTIBLE - TV: ", eTVLocation, " Status: ", bIsIndestructible)
	g_TVStruct[ENUM_TO_INT(eTVLocation)].bIsIndestructible = bIsIndestructible

ENDPROC

