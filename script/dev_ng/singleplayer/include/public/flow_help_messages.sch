//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 28/03/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  GTA V - General Help Message Triggering					│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"

USING "flow_help_public.sch"
USING "flow_public_game.sch"
USING "Mission_Control_Public.sch"
USING "code_control_public.sch"


TYPEDEF FUNC BOOL HelpCheck()

CONST_INT	NUM_CONDITION_PASSES_BEFORE_DISPLAY		5


STRUCT FlowHelpMessageVars
	INT iMessageToCheckThisFrame = 0
	INT iConditionPassCounter = 0
	HelpCheck fpHelpCheck
ENDSTRUCT

/*
    GET_CURRENT_WEBPAGE_ID now no longer resets it's value.
	It's just a get, but these functions are used all over
*/
FUNC INT GET_CURRENT_WEBSITE_ID_WRAPPED() // END_SCALEFORM_MOVIE_METHOD_INT()
	INT raw =  GET_CURRENT_WEBSITE_ID()
	RETURN raw
ENDFUNC


FUNC BOOL CHECK_CONVERTIBLE_ROOF()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehPlayer))
							IF IS_VEHICLE_A_CONVERTIBLE(vehPlayer)
							AND GET_ENTITY_MODEL(vehPlayer) != VOLTIC
								CONVERTIBLE_ROOF_STATE eState = GET_CONVERTIBLE_ROOF_STATE(vehPlayer)
								IF eState = CRS_RAISED
									IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_NO_STAMINA()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_PLAYER_SPRINT_TIME_REMAINING(PLAYER_ID()) < 5.0
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL DOES_PLAYER_HAVE_MULTIPLE_WEAPONS_IN_A_WEAPON_WHEEL_GROUP()
	INT i, iWepGroupIndex
	WEAPON_SLOT eWeaponSlot
	WEAPON_TYPE eWeaponType
	WEAPON_GROUP eWeaponGroup
	INT iWeaponsInGroupCount[NUM_PLAYER_PED_WEAPON_GROUPS]

	FOR i = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS) - 1)
	
		eWeaponSlot = GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(i)
		IF eWeaponSlot != WEAPONSLOT_INVALID
			eWeaponType = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(i))
			IF eWeaponType != WEAPONTYPE_INVALID			
				eWeaponGroup = GET_WEAPONTYPE_GROUP(eWeaponType)
				//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - eWeaponType is valid : ", eWeaponType, " returned weapon group is :", eWeaponGroup, " i = ", i)
				IF eWeaponGroup != WEAPONGROUP_INVALID
				
					//unable to get the weapongroup for unarmed so have to set this based of the weapontype
					IF eWeaponType = WEAPONTYPE_UNARMED
						//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - eWeaponType was unarmed, :", eWeaponType, "set group to WEAPONGROUP_MELEE : ", WEAPONGROUP_MELEE, " i = ", i)
						eWeaponGroup = WEAPONGROUP_MELEE
					ENDIF
					
					// merge some groups together which appear in the same wheel_ group
					SWITCH eWeaponGroup
						CASE WEAPONGROUP_FIREEXTINGUISHER
							FALLTHRU
						CASE WEAPONGROUP_PETROLCAN
							eWeaponGroup = WEAPONGROUP_THROWN
							//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - current eWeaponGroup ", eWeaponGroup, " merged to  rWEAPONGROUP_THROWN : ", WEAPONGROUP_THROWN, " i = ", i)
						BREAK
						CASE WEAPONGROUP_STUNGUN
							eWeaponGroup = WEAPONGROUP_PISTOL
							//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - current eWeaponGroup ", eWeaponGroup, " merged to  WEAPONGROUP_PISTOL : ", WEAPONGROUP_PISTOL, " i = ", i)
						BREAK
						CASE WEAPONGROUP_MG
							eWeaponGroup = WEAPONGROUP_SMG
							//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - current eWeaponGroup ", eWeaponGroup, " merged to  WEAPONGROUP_SMG : ", WEAPONGROUP_SMG, " i = ", i)
						BREAK
					ENDSWITCH
					
					iWepGroupIndex = GET_INT_FROM_PLAYER_PED_WEAPON_GROUP(eWeaponGroup)			
					
					// check the returned weapon group index is valid
					IF iWepGroupIndex <> -1					
						
						iWeaponsInGroupCount[iWepGroupIndex]++
		
						// check if the weapon group now has more than one entry
						IF iWeaponsInGroupCount[iWepGroupIndex] > 1							
							//CDEBUG2LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - DOES_PLAYER_HAVE_MULTIPLE_WEAPONS_IN_A_WEAPON_WHEEL_GROUP - return TRUE, for group : ", iWepGroupIndex, " i = ", i, " Frame Count : ", GET_FRAME_COUNT())
							RETURN TRUE
						ENDIF
					ELSE
						//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - iWepGroupIndex isn't valid : ", iWepGroupIndex, " i = ", i)
					ENDIF
				ELSE
					//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - wep group invalid : ", eWeaponGroup, " i = ", i)
				ENDIF
			ELSE
				//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - wep type invalid : ", eWeaponType, " i = ", i)
			ENDIF	
		ELSE
			//CDEBUG3LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - wep slot invalid : ", eWeaponSlot, " i = ", i)
		ENDIF
	ENDFOR
	
	//CDEBUG2LN(DEBUG_FLOW_HELP, "multiWeaponInSlotWheelHelp.sc - DOES_PLAYER_HAVE_MULTIPLE_WEAPONS_IN_A_WEAPON_WHEEL_GROUP - return FALSE", " Frame Count : ", GET_FRAME_COUNT())
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_MULTI_WEAPONS_IN_SLOT()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				// detect when the weapon wheel is on screen
				IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
					IF NOT IS_PHONE_ONSCREEN()
					AND IS_PED_ON_FOOT(PLAYER_PED_ID())	// Fix B*1061199
						IF DOES_PLAYER_HAVE_MULTIPLE_WEAPONS_IN_A_WEAPON_WHEEL_GROUP()
							RETURN TRUE
						ENDIF
					ENDIF						
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_VEHICLE_RADIO_HELP_A()
	IF IS_PLAYER_PLAYING(PLAYER_ID()) AND (NOT IS_CURRENTLY_ON_MISSION_TO_TYPE())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
					IF GET_PED_IN_VEHICLE_SEAT(vehPlayer) = PLAYER_PED_ID()
						IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
							IF DOES_PLAYER_VEH_HAVE_RADIO()
								//Don't let this help text show if the Simeon call is still queued.
								IF NOT IS_COMMUNICATION_REGISTERED(CALL_ARM2_UNLOCK)
								and not IS_COMMUNICATION_REGISTERED(CALL_CARDMG_ARM2_UNLOCK)
									IF NOT IS_PHONE_ONSCREEN()
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_VEHICLE_RADIO_HELP_B()
	IF HAS_ONE_TIME_HELP_DISPLAYED(FHM_VEHICLE_RADIO_A)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						IF GET_PED_IN_VEHICLE_SEAT(vehPlayer) = PLAYER_PED_ID()
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								IF DOES_PLAYER_VEH_HAVE_RADIO()
									IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_RADIO_STATIONS)
										//Don't let this help text show if the Simeon call is still queued.
										IF NOT IS_COMMUNICATION_REGISTERED(CALL_ARM2_UNLOCK)
										and not IS_COMMUNICATION_REGISTERED(CALL_CARDMG_ARM2_UNLOCK)
											IF NOT IS_PHONE_ONSCREEN()
												RETURN TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_VEHICLE_ROLLED()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				MODEL_NAMES eModel = GET_ENTITY_MODEL(vehPlayer)
				IF NOT IS_THIS_MODEL_A_BOAT(eModel)
				AND NOT IS_THIS_MODEL_A_HELI(eModel)
				AND NOT IS_THIS_MODEL_A_PLANE(eModel)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
						IF GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = PLAYER_PED_ID()
							IF NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								IF IS_ENTITY_UPSIDEDOWN(vehPlayer)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL CHECK_OBTAINED_AUTO_PARACHUTE()
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			if GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_AIR_VEHICLE_PARACHUTE_UNLOCKED)
				VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					MODEL_NAMES eModel = GET_ENTITY_MODEL(vehPlayer)
					IF  IS_THIS_MODEL_A_HELI(eModel)
					or  IS_THIS_MODEL_A_PLANE(eModel)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							IF GET_PED_IN_VEHICLE_SEAT(vehPlayer, VS_DRIVER) = PLAYER_PED_ID()
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			endif	
		endif
	endif
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_PLAYER_ENTERED_TRAIN_IN_GROUP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			GROUP_INDEX groupPlayer = GET_PLAYER_GROUP(PLAYER_ID())
			INT iLeaders, iFollowers
			GET_GROUP_SIZE(groupPlayer, iLeaders, iFollowers)
			IF iLeaders > 1 OR iFollowers > 0
				IF IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_PLAYER_USING_PROG_AR()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				WEAPON_TYPE eWeapon
				IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eWeapon)
					IF eWeapon = WEAPONTYPE_DLC_PROGRAMMABLEAR
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_SOCIAL_CLUB_LINK()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_GAME_LINKED_TO_SOCIAL_CLUB(TRUE, TRUE)
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_2)
					RETURN TRUE
				ENDIF
			ELSE
				// Linked to social club so stop the checks.
				REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_SOCIAL", FALSE)
				SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_SOCIAL_CLUB_LINK)
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_FRIEND_ACTIVITY_HELP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF g_savedGlobals.sFriendsData.g_bHelpDoneCanPhoneFriend
			OR GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_1)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_ZOOMED_MOVEMENT_HELP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
						WEAPON_TYPE eCurrentWeapon
						IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon)
						AND eCurrentWeapon != WEAPONTYPE_INVALID
						AND eCurrentWeapon != WEAPONTYPE_UNARMED
						AND eCurrentWeapon != WEAPONTYPE_OBJECT
							IF GET_WEAPONTYPE_GROUP(eCurrentWeapon) = WEAPONGROUP_SNIPER			
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_DRIVEBY_CONTROL_HELP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
						VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
						IF IS_VEHICLE_DRIVEABLE(vehPlayer) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayer)
							MODEL_NAMES eModel = GET_ENTITY_MODEL(vehPlayer)
							IF IS_THIS_MODEL_A_CAR(eModel)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_TRI_3_UNLOCKED_WHILE_EXILED()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			//Check if player after exile
			IF  GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
			AND (g_savedGlobals.sTriathlonData.eCurrentRaceUnlocked = TRIATHLON_RACE_IRONMAN)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_PLAYER_LOGGED_FOR_STOCKS()
	IF NOT IS_PLAYER_ONLINE() 
	//Numbers for Stock exchange sites
	AND (GET_CURRENT_WEBSITE_ID_WRAPPED() = 4)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_FIRST_PERSON_HELP_REMINDER_A()
	IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
	
		IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW)	
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_HAS_USED_FP_VIEW, TRUE)
		ENDIF
	
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
			IF g_iFlowHelpFirstPersonTimer = -1
				g_iFlowHelpFirstPersonTimer = GET_GAME_TIMER()
			ELIF (GET_GAME_TIMER() - g_iFlowHelpFirstPersonTimer) > 10000
				IF IS_FLOW_HELP_QUEUE_EMPTY()
				OR IS_FLOW_HELP_MESSAGE_QUEUED("AM_H_FP1")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF g_iFlowHelpFirstPersonTimer != -1
		g_iFlowHelpFirstPersonTimer = -1
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_FIRST_PERSON_HELP_REMINDER_B()
	IF HAS_ONE_TIME_HELP_DISPLAYED(FHM_FIRST_PERSON_HELP_REMINDER_A)
		IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
			IF g_iFlowHelpFirstPersonTimer = -1
				g_iFlowHelpFirstPersonTimer = GET_GAME_TIMER()
			ELIF (GET_GAME_TIMER() - g_iFlowHelpFirstPersonTimer) > 120000
				IF IS_FLOW_HELP_QUEUE_EMPTY()
				OR IS_FLOW_HELP_MESSAGE_QUEUED("AM_H_FP2")
					g_iFlowHelpFirstPersonTimer = -1
					RETURN TRUE
				ENDIF
			ENDIF
		ELIF g_iFlowHelpFirstPersonTimer != -1
			g_iFlowHelpFirstPersonTimer = -1
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_ROCKSTAR_EDITOR_INTRO_HELP()
	#IF USE_REPLAY_RECORDING_TRIGGERS
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_ROCKSTAR_EDITOR_INTRO_HELP2()
	#IF USE_REPLAY_RECORDING_TRIGGERS
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
		and HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_INTRO1)
		and IS_SELECTOR_ONSCREEN()
		and not HAS_ONE_TIME_HELP_DISPLAYED(FHM_ROCKSTAR_EDITOR_INTRO2)
			RETURN TRUE
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_HYDRAULICS_HELP()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				//IF GET_ENTITY_MODEL(vehPlayer) = BUCCANEER2
				IF IS_VEHICLE_A_SUPERMOD_MODEL(GET_ENTITY_MODEL(vehPlayer), SUPERMOD_FLAG_HAS_HYDRAULICS)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_OTH_MESSAGE_REPEATING(FlowHelpMessage fhm)
	//Check a list of one-time flow help messages that should be repeated
	IF fhm = FHM_PLAYER_LOGGED_FOR_STOCKS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PROCESS_HELP_MESSAGE(STRING strHelp, FlowHelpMessage eFlowHelpMessage, FlowHelpMessageVars &sVars, BOOL bDisplayOnMission = FALSE, INT iQueueDelay = 0, INT iHelpDuration = DEFAULT_HELP_TEXT_TIME, FlowHelpPriority fhPriority = FHP_MEDIUM, CC_CodeID eCodeID = CID_BLANK)
	IF NOT HAS_ONE_TIME_HELP_DISPLAYED(eFlowHelpMessage)
		IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
			IF CALL sVars.fpHelpCheck()
				sVars.iConditionPassCounter++
				IF sVars.iConditionPassCounter >= NUM_CONDITION_PASSES_BEFORE_DISPLAY
					SWITCH GET_FLOW_HELP_MESSAGE_STATUS(strHelp)
						CASE FHS_EXPIRED
							ADD_HELP_TO_FLOW_QUEUE(strHelp, fhPriority, iQueueDelay, 2500, iHelpDuration, 7, CID_BLANK, eCodeID)
						BREAK
						CASE FHS_DISPLAYED
							IF NOT IS_OTH_MESSAGE_REPEATING(eFlowHelpMessage)
								SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(eFlowHelpMessage)
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
				RETURN TRUE
			ELSE
				sVars.iConditionPassCounter = 0
				IF IS_FLOW_HELP_MESSAGE_QUEUED(strHelp)
					REMOVE_HELP_FROM_FLOW_QUEUE(strHelp)
				ENDIF
			ENDIF
		ELIF bDisplayOnMission
			IF CALL sVars.fpHelpCheck()
				sVars.iConditionPassCounter++
				IF sVars.iConditionPassCounter >= NUM_CONDITION_PASSES_BEFORE_DISPLAY
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT IS_CUTSCENE_PLAYING()
							IF IS_SCREEN_FADED_IN()
								IF NOT IS_WARNING_MESSAGE_ACTIVE()
									IF g_sPlayerPedRequest.eState != PR_STATE_PROCESSING
										PRINT_HELP(strHelp)
										SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(eFlowHelpMessage)
										IF eCodeID != CID_BLANK
											Execute_Code_ID(eCodeID)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				RETURN TRUE
			ELSE
				sVars.iConditionPassCounter = 0
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_SP_HELP_MESSAGES(FlowHelpMessageVars &sVars)

	//Don't process help messages during a debug flow launch.
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			EXIT
		ENDIF
	#ENDIF

#IF FEATURE_SP_DLC_DIRECTOR_MODE
	CONST_INT 	NUM_HELP_MESSAGES	19
#ENDIF
#IF NOT FEATURE_SP_DLC_DIRECTOR_MODE
	CONST_INT 	NUM_HELP_MESSAGES	17
#ENDIF

	BOOL 		bProcessing 		= FALSE
	
	SWITCH sVars.iMessageToCheckThisFrame
		CASE 0	
			sVars.fpHelpCheck = &CHECK_CONVERTIBLE_ROOF
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_CNVRT", FHM_CONVERTIBLE_ROOF, sVars)		
		BREAK
		CASE 1
			sVars.fpHelpCheck = &CHECK_NO_STAMINA
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_NOSTAM", FHM_NO_STAMINA, sVars)		
		BREAK
		CASE 2	
			sVars.fpHelpCheck = &CHECK_MULTI_WEAPONS_IN_SLOT
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_MULTWEP", FHM_MULTI_WEAPONS_IN_SLOT, sVars)		
		BREAK
		CASE 3	
			sVars.fpHelpCheck = &CHECK_VEHICLE_RADIO_HELP_A
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_RADIO1", FHM_VEHICLE_RADIO_A, sVars)		
		BREAK
		CASE 4	
			sVars.fpHelpCheck = &CHECK_VEHICLE_RADIO_HELP_B
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_RADIO2", FHM_VEHICLE_RADIO_B, sVars)		
		BREAK
		CASE 5	
			sVars.fpHelpCheck = &CHECK_VEHICLE_ROLLED
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_VROLL", FHM_VEHICLE_ROLLED, sVars)		
		BREAK
		CASE 6	
			sVars.fpHelpCheck = &CHECK_PLAYER_ENTERED_TRAIN_IN_GROUP
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_GTRA", FHM_ENTERED_TRAIN_IN_GROUP, sVars, TRUE)		
		BREAK
		CASE 7	
			sVars.fpHelpCheck = &CHECK_PLAYER_USING_PROG_AR
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_PROGAR", FHM_PROG_AR, sVars)		
		BREAK
		CASE 8	
			sVars.fpHelpCheck = &CHECK_SOCIAL_CLUB_LINK
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_SOCIAL", FHM_SOCIAL_CLUB_LINK, sVars, FALSE, 10000, 15000)
		BREAK
		CASE 9
			sVars.fpHelpCheck = &CHECK_OBTAINED_AUTO_PARACHUTE
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_PARA", FHM_OBTAINED_AUTO_PARACHUTE, sVars)	
		BREAK
		CASE 10
			sVars.fpHelpCheck = &CHECK_FRIEND_ACTIVITY_HELP
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_FFRND", FHM_FRIEND_ACTIVITY_HELP, sVars)	
		BREAK
		CASE 11
			sVars.fpHelpCheck = &CHECK_ZOOMED_MOVEMENT_HELP
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_ZOOMOVE", FHM_SNIPER_ZOOMED_MOVEMENT, sVars)	
		BREAK
		CASE 12
			sVars.fpHelpCheck = &CHECK_DRIVEBY_CONTROL_HELP
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_DRIVEBY", FHM_DRIVEBY_CONTROL, sVars)	
		BREAK
		CASE 13		//Triathlon 3 unlocked while in exile
			sVars.fpHelpCheck = &CHECK_TRI_3_UNLOCKED_WHILE_EXILED
			bProcessing = PROCESS_HELP_MESSAGE("TRI_NEWRC",FHM_TRI_3_UNLOCKED_IN_EXILE,sVars)
		BREAK
		CASE 14		//Player needs to log in to access stocks
			sVars.fpHelpCheck = &CHECK_PLAYER_LOGGED_FOR_STOCKS
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_LOGSTOCKS",FHM_PLAYER_LOGGED_FOR_STOCKS, sVars, FALSE, 0, DEFAULT_HELP_TEXT_TIME, FHP_VERY_HIGH)
		BREAK
		CASE 15
			sVars.fpHelpCheck = &CHECK_FIRST_PERSON_HELP_REMINDER_A
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_FP1", FHM_FIRST_PERSON_HELP_REMINDER_A, sVars)
		BREAK
		CASE 16
			sVars.fpHelpCheck = &CHECK_FIRST_PERSON_HELP_REMINDER_B
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_FP2", FHM_FIRST_PERSON_HELP_REMINDER_B, sVars)
		BREAK
		
#IF FEATURE_SP_DLC_DIRECTOR_MODE
		CASE 17
			sVars.fpHelpCheck = &CHECK_ROCKSTAR_EDITOR_INTRO_HELP	
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_EDIT_1", FHM_ROCKSTAR_EDITOR_INTRO1, sVars)
		BREAK
		CASE 18
			sVars.fpHelpCheck = &CHECK_ROCKSTAR_EDITOR_INTRO_HELP2	
			bProcessing = PROCESS_HELP_MESSAGE("AM_H_EDIT_2", FHM_ROCKSTAR_EDITOR_INTRO2, sVars,FALSE, 0, 7500)
		BREAK
#ENDIF		
//#IF FEATURE_LOWRIDER_CONTENT
//		CASE 19
//			sVars.fpHelpCheck = &CHECK_HYDRAULICS_HELP
//			bProcessing = PROCESS_HELP_MESSAGE("AM_H_HYDR", FHM_HYDRAULICS_HELP, sVars)
//		BREAK
//#ENDIF		
		
	ENDSWITCH
	
	IF NOT bProcessing
		sVars.iMessageToCheckThisFrame++
		IF sVars.iMessageToCheckThisFrame = NUM_HELP_MESSAGES
			sVars.iMessageToCheckThisFrame = 0
		ENDIF
	ENDIF
ENDPROC
