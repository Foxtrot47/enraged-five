USING "globals.sch"

//dead ped system is dead

/*

//REGISTER_NEARBY_DEAD_PED // peddex and charsheet
FUNC BOOL REGISTER_NEARBY_DEAD_PED(PED_INDEX piIn, enumCharacterList charsheet)
	//make sure the ped exists
	IF NOT DOES_PED_EXIST(piIn)
		RETURN FALSE
	ENDIF
	//make sure the ped is dead
	IF NOT IS_PED_DEAD(piIn)
		RETURN FALSE
	ENDIF
		
	//is the charsheet valid
	IF charsheet = NO_CHARACTER
		RETURN FALSE
	ENDIF
	//awesome, register it
		//find a free index
		INT i = 0
		REPEAT MAX_NEARBY_DEAD_PEDS i
			IF g_DEAD_PED_REGISTER[i].bUsed = FALSE
				//register
				g_DEAD_PED_REGISTER[i].bUsed = TRUE
				g_DEAD_PED_REGISTER[i].piStoredIndex = piIn
				g_DEAD_PED_REGISTER[i].eCharSheet = charsheet	
				g_iDeadPedsRegistered++
				//return 
				RETURN TRUE
			ENDIF			
		ENDREPEAT
		//ran out of space, assert
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_DEAD_PEDS_REGISTERED()
 	IF	g_iDeadPedsRegistered > 0
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//IS_NEARBY_DEAD_PED_CLEAR // charsheet
FUNC BOOL IS_NEARBY_DEAD_PED_CLEAR(enumCharacterList charsheet)

	//is it in the list
	INT i = 0
	REPEAT MAX_NEARBY_DEAD_PEDS i
		IF g_DEAD_PED_REGISTER[i].bUsed = TRUE
			RETURN FALSE//its in the list, so its not been cleared yet
		ENDIF
	ENDREPEAT

	//not in the list, either cleared out or
	RETURN FALSE
ENDFUNC

//FORCE_CLEAR_DEAD_PEDS // 
PROC FORCE_CLEAR_DEAD_PEDS()

//go through the list and force remove any that still exist
	INT i = 0
	REPEAT MAX_NEARBY_DEAD_PEDS i
		IF g_DEAD_PED_REGISTER[i].bUsed = TRUE
			g_DEAD_PED_REGISTER[i].bUsed = FALSE
			
			//TODO, attempt to forcibly remove the ped from the world
			IF DOES_PED_EXIST()
				REMOVE_PED_ELEGANTLY(g_DEAD_PED_REGISTER[i].piStoredIndex)
			ENDIF
				
			g_iDeadPedsRegistered--
		ENDIF
	ENDREPEAT
	g_iDeadPedsRegistered = 0
ENDPROC

//DELETE_DEAD_PEDS_OF_TYPE //charsheet

PROC DELETE_DEAD_PEDS_OF_TYPE(enumCharacterList charsheet)
//is it in the list?
//is the ped still there?
//force remove it
//go through the list and force remove any that still exist
	INT i = 0
	REPEAT MAX_NEARBY_DEAD_PEDS i
		IF g_DEAD_PED_REGISTER[i].bUsed = TRUE
			IF g_DEAD_PED_REGISTER[i].eCharSheet = charsheet
				g_DEAD_PED_REGISTER[i].bUsed = FALSE
			
				//TODO, attempt to forcibly remove the ped from the world
				IF DOES_PED_EXIST()
					REMOVE_PED_ELEGANTLY(g_DEAD_PED_REGISTER[i].piStoredIndex)
				ENDIF
				
				g_iDeadPedsRegistered--
				
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC



*/






