//╒═════════════════════════════════════════════════════════════════════════════╕
//│					  Off-mission Cutscene Control Public Heade					│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			04/04/12												│
//│		DESCRIPTION: 	Public interface for handling off-mission pre-loading	│
//│						of cutscenes.											│									
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"


PROC REGISTER_OFFMISSION_CUTSCENE_REQUEST(INT &paramRequestID, OFFMISSION_CUTSCENE_TYPE paramType)
	CPRINTLN(DEBUG_FLOW_CUTS, "Script ", GET_THIS_SCRIPT_NAME(), " is registering an off-mission cutscene request of priority ", ENUM_TO_INT(paramType), ".")
	IF g_iOffMissionCutsceneRequestCount < MAX_OFFMISSION_CUTSCENE_REQUESTS
		g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount].iID = 0
		g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount].eType = paramType
		
		//Find the first free ID to give to this new request.
		BOOL bFoundNewID = FALSE
		WHILE NOT bFoundNewID
			INT iRequestIndex
			bFoundNewID = TRUE
			//Does current ID clash with an ID in the list?
			REPEAT g_iOffMissionCutsceneRequestCount iRequestIndex
				IF g_sOffMissionCutsceneRequests[iRequestIndex].iID = g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount].iID
					g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount].iID++
					bFoundNewID = FALSE
				ENDIF
			ENDREPEAT
		ENDWHILE
		paramRequestID = g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount].iID
		g_iOffMissionCutsceneRequestCount++
		g_bOffMissionCutsceneChange = TRUE
		
		CPRINTLN(DEBUG_FLOW_CUTS, "Successfully registered new request: ID=", paramRequestID, " Type=", ENUM_TO_INT(paramType), ".")
	ELSE
		SCRIPT_ASSERT("REGISTER_OFFMISSION_CUTSCENE_REQUEST: Cutscene request list is full. Does MAX_OFFMISSION_CUTSCENE_REQUESTS need increasing? Bug BenR.")
		CPRINTLN(DEBUG_FLOW_CUTS, "Register request denied as the controller has too many requests already registered.")
		paramRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
	ENDIF
ENDPROC


PROC SET_OFFMISSION_CUTSCENE_ACTIVE(INT paramRequestID, BOOL paramActive)
	//Is this a valid request ID?
	IF paramRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
		SCRIPT_ASSERT("SET_OFFMISSION_CUTSCENE_ACTIVE: Invalid request ID.")
		EXIT
	ENDIF
	
	IF paramActive
		IF g_iOffMissionCutsceneRequestAllowed = paramRequestID
			CPRINTLN(DEBUG_FLOW_CUTS, "Request ID ", paramRequestID, " was flagged as having an active cutscene.")
			g_iOffMissionCutsceneRequestActive = paramRequestID
		ELSE
			SCRIPT_ASSERT("SET_OFFMISSION_CUTSCENE_ACTIVE: Tried to set a cutscene request that hasn't been given permission to load to active.")
		ENDIF
	ELSE
		IF g_iOffMissionCutsceneRequestActive = paramRequestID
			CPRINTLN(DEBUG_FLOW_CUTS, "Request ID ", paramRequestID, " was unflagged as having an active cutscene.")
			g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
		ELSE
			SCRIPT_ASSERT("SET_OFFMISSION_CUTSCENE_ACTIVE: Tried to set a request that wasn't active to inactive.")
		ENDIF
	ENDIF
ENDPROC


PROC END_OFFMISSION_CUTSCENE_REQUEST(INT &paramRequestID)
	//Is this a valid request ID?
	IF paramRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
		SCRIPT_ASSERT("END_OFFMISSION_CUTSCENE_REQUEST: Invalid request ID.")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_FLOW_CUTS, "Script ", GET_THIS_SCRIPT_NAME(), " ending off-mission cutscene request ", paramRequestID, ".")
	
	//Is the ending ID flagged as loaded. If so unflag it.
	IF g_iOffMissionCutsceneRequestActive = paramRequestID
		SET_OFFMISSION_CUTSCENE_ACTIVE(paramRequestID, FALSE)
	ENDIF
	
	//Is the ending ID currently being given priority? If so unflag it.
	IF g_iOffMissionCutsceneRequestAllowed = paramRequestID
		g_iOffMissionCutsceneRequestAllowed = NULL_OFFMISSION_CUTSCENE_REQUEST
	ENDIF
	
	//Find request with this ID in the registered list.
	BOOL bFoundIndex = FALSE
	INT iRequestIndex = 0
	WHILE (NOT bFoundIndex) AND (iRequestIndex < g_iOffMissionCutsceneRequestCount)
		IF g_sOffMissionCutsceneRequests[iRequestIndex].iID = paramRequestID
			bFoundIndex = TRUE
		ELSE
			iRequestIndex++
		ENDIF
	ENDWHILE
	
	//Was the ID registered?
	IF NOT bFoundIndex
		CPRINTLN(DEBUG_FLOW_CUTS, "Tried to end a cutscene request that was not registered. Did the cutscene control thread clean up ahead of this script?")
		EXIT
	ENDIF
	
	//ID registered. Remove it from the list.
	INT iTempIndex
	FOR iTempIndex = iRequestIndex TO (g_iOffMissionCutsceneRequestCount-2)
		g_sOffMissionCutsceneRequests[iTempIndex].iID = g_sOffMissionCutsceneRequests[iTempIndex+1].iID
		g_sOffMissionCutsceneRequests[iTempIndex].eType = g_sOffMissionCutsceneRequests[iTempIndex+1].eType
	ENDFOR
	g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount-1].iID = NULL_OFFMISSION_CUTSCENE_REQUEST
	g_sOffMissionCutsceneRequests[g_iOffMissionCutsceneRequestCount-1].eType = OCT_MUST_LOAD
	g_iOffMissionCutsceneRequestCount--
	g_bOffMissionCutsceneChange = TRUE
	CPRINTLN(DEBUG_FLOW_CUTS, "", paramRequestID, " found and unregistered successfully.")

	g_iOffMissionCutsceneUnloadedFrame = GET_FRAME_COUNT()

	paramRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
ENDPROC


FUNC OFFMISSION_CUTSCENE_ACTION GET_OFFMISSION_CUTSCENE_REQURIED_ACTION(INT paramRequestID)
	IF g_iOffMissionCutsceneRequestAllowed = paramRequestID
		//This ID is allowed to load. Is any request already active?
		IF g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
			//Allow a frame's grace period since the last cutscene cleaned up to ensure we
			//don't confuse the code systems.
			IF g_iOffMissionCutsceneUnloadedFrame < GET_FRAME_COUNT()
				//Nothing loaded. Go ahead and load.
				RETURN OCA_LOAD
			ENDIF
		ENDIF
	ELSE
		//This ID isn't allowed to load. Is this ID flagged as loaded?
		IF g_iOffMissionCutsceneRequestActive = paramRequestID
			//The ID is loaded. Need to unload.
			RETURN OCA_UNLOAD
		ENDIF
	ENDIF
	//No action to perform at the moment.
	RETURN OCA_WAIT
ENDFUNC
