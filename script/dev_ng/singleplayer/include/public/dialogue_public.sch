USING "rage_builtins.sch"
USING "globals.sch"
USING "dialogue_private.sch"


//PLEASE READ!


//Dialogue and Phonecall public functions for use by other scripts to automatically play phone conversations and, soon, normal dialogue.

//Please read the comments before each function and check the GTA5 scripting wiki for more detailed information. Any ambiguity, don't hesitate to ask. 

//The main phonecall creation publics have been created as functions as they were in GTA IV. This is handy as it allows anyone using them
//to check if the phonecall has actually launched or not. Whilst you don't have to check the return value, it is much safer to check if a
//phonecall creation function returns TRUE so that you know your phonecall has launched successfully. The creation functions will return
//FALSE if a phonecall of the same or higher priority is currently ongoing at the same time you try to launch yours.

//An assert will fire if you have too many labels in your conversation. This can be easily remedied. See me if it happens.

// AUTHOR: Steve Taylor
                            


//DO NOT CALL! This procedure is used by some public functions and is here for convenience of reference.
PROC Assign_Standard_Dialogue_Holder_Parameters (structPedsForConversation &AssignPedStruct, enumCharacterList AssignWhichChar, STRING AssignWhichBlockOfTextToLoad,  enumSubtitlesState AssignDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState AssignAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE) 


    PRINTLN("Stack size before copying ped for convo struct ", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE())
    g_ConversationPedsHolderStruct = AssignPedStruct    //This might need to be locked to prevent the holder getting overwritten on very, very rare occasions. 
                                                        //It would mean dialogue requests are polling the system at exactly the same time.
                                                        //CheckCurrentConversationStatus would need to unlock before every true/false return. 
                                                        //If found to be locked here, then any request of CREATE_CONVERSATION and similar would need to return FALSE.


    g_TheContactInvolvedinCallHolder = AssignWhichChar
    g_BlockToLoadHolder = AssignWhichBlockOfTextToLoad

    g_CloneConversationHolder = cloneConversation

    IF AssignDisplaySubtitles = DISPLAY_SUBTITLES
    
        g_DisplaySubtitlesHolder = TRUE

        g_AssignDisplaySubtitlesBuffer = DISPLAY_SUBTITLES
    
    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Not displaying subtitles!")
            cdPrintnl()
        #endif

        g_DisplaySubtitlesHolder = FALSE


        g_AssignDisplaySubtitlesBuffer = DO_NOT_DISPLAY_SUBTITLES

    ENDIF


    IF AssignAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN

        g_AddToBriefScreenHolder = TRUE

        g_AssignAddtoBriefScreenBuffer = DO_ADD_TO_BRIEF_SCREEN

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Not adding subs to brief screen!")
            cdPrintnl()
        #endif

        g_AddToBriefScreenHolder = FALSE

        g_AssignAddtoBriefScreenBuffer = DO_NOT_ADD_TO_BRIEF_SCREEN

    ENDIF

ENDPROC




//DO NOT CALL!
PROC Passed_Priority_Cap_Warning()


    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("DIALOG_PUB - Passed priority for face-to-face conversation was higher than MEDIUM, but hard cap is in force.")  
        cdPrintnl()
        cdPrintstring("DIALOG_PUB - Setting conversation priority to MEDIUM. Use lower priorities if you need to interrupt a conversation")
        cdPrintnl()
    #endif


ENDPROC





//DO NOT CALL! This procedure is used by some public functions and is here for convenience of reference.
PROC Construct_Multipart_Conversation_Roots_and_Specifics (INT Passed_Num_of_Multiparts, STRING &ConstructSegmentLabel[],  STRING &ConstructSpecificLabel[],
                                                            STRING FirstPassedRoot = NULL, STRING FirstPassedSpecific = NULL,
                                                            STRING SecondPassedRoot = NULL, STRING SecondPassedSpecific = NULL,
                                                            STRING ThirdPassedRoot = NULL, STRING ThirdPassedSpecific = NULL,
                                                            STRING FourthPassedRoot = NULL, STRING FourthPassedSpecific = NULL,
                                                            STRING FifthPassedRoot = NULL, STRING FifthPassedSpecific = NULL,
                                                            STRING SixthPassedRoot = NULL, STRING SixthPassedSpecific = NULL,
                                                            STRING SeventhPassedRoot = NULL, STRING SeventhPassedSpecific = NULL)


    g_TotalMultiPartsHolder = Passed_Num_of_Multiparts

     
    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMPChatCallHolder = FALSE


    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_PlayingFromLineHolder = FALSE


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif



    ConstructSegmentLabel[0] = FirstPassedRoot
    ConstructSpecificLabel[0] = FirstPassedSpecific

    ConstructSegmentLabel[1] = SecondPassedRoot
    ConstructSpecificLabel[1] = SecondPassedSpecific

    ConstructSegmentLabel[2] = ThirdPassedRoot
    ConstructSpecificLabel[2] = ThirdPassedSpecific

    ConstructSegmentLabel[3] = FourthPassedRoot
    ConstructSpecificLabel[3] = FourthPassedSpecific

    ConstructSegmentLabel[4] = FifthPassedRoot
    ConstructSpecificLabel[4] = FifthPassedSpecific

    ConstructSegmentLabel[5] = SixthPassedRoot
    ConstructSpecificLabel[5] = SixthPassedSpecific

    ConstructSegmentLabel[6] = SeventhPassedRoot
    ConstructSpecificLabel[6] = SeventhPassedSpecific

  

ENDPROC








//If you have an alphanumeric speaker or listener designation in dstar, use this to convert it to an int when assigning a ped for dialogue
//e.g ADD_PED_FOR_DIALOGUE (myscripted_speech, ConvertSingleCharacter("D"), My_ped_index, "That_Ped_Voice")
 
FUNC INT ConvertSingleCharacter (STRING passedSingleCharacter)




    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "0")
        RETURN 0
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "1")
        RETURN 1
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "2")
        RETURN 2
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "3")
        RETURN 3
    ENDIF


    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "4")
        RETURN 4
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "5")
        RETURN 5
    ENDIF

    
    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "6")
        RETURN 6
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "7")
        RETURN 7
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "8")
        RETURN 8
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "9")
        RETURN 9
    ENDIF
  





    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "A")
        RETURN 10
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "B")
        RETURN 11
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "C")
        RETURN 12
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "D")
        RETURN 13
    ENDIF


    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "E")
        RETURN 14
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "F")
        RETURN 15
    ENDIF

    
    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "G")
        RETURN 16
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "H")
        RETURN 17
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "I")
        RETURN 18
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "J")
        RETURN 19
    ENDIF
  


    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "K")
        RETURN 20
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "L")
        RETURN 21
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "M")
        RETURN 22
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "N")
        RETURN 23
    ENDIF
  

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "O")
        RETURN 24
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "P")
        RETURN 25
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "Q")
        RETURN 26
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "R")
        RETURN 27
    ENDIF

    
    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "S")
        RETURN 28
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "T")
        RETURN 29
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "U")
        RETURN 30
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "V")
        RETURN 31
    ENDIF


    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "W")
        RETURN 32
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "X")
        RETURN 33
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "Y")
        RETURN 34
    ENDIF

    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "Z")
        RETURN 35
    ENDIF









    
    IF ARE_STRINGS_EQUAL (passedSingleCharacter, "z") //error character


        #IF IS_DEBUG_BUILD

            SCRIPT_ASSERT ("SL assert 1. A missing or invalid speaker listener value has been detected. Use 0-9 and A-Z only.")

        #endif

        RETURN -1

    ELSE


        #IF IS_DEBUG_BUILD

            SCRIPT_ASSERT ("SL assert 2. A missing or invalid speaker listener value has been detected. Use 0-9 and A-Z only.")

        #endif

        RETURN -1


    ENDIF
    



ENDFUNC

















//Steve Taylor: Ok.
// DJ Jones 19 Mar 2012
//  Pulled these globals out of Construct_Multipart_Phonecall_Roots_and_Specifics so that
//  PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES can set them without modifying strings.
PROC Set_Multipart_Phonecall_Globals(INT Passed_Num_of_Multiparts)

    g_TotalMultiPartsHolder = Passed_Num_of_Multiparts
    g_IsThisConversationForPhoneHolder = TRUE //this conversation is a phonecall.
    g_IsThisAnMPChatCallHolder = FALSE
    g_IsThisAnMpJobOfferHolder = FALSE
    g_PlayingSingleLineHolder = FALSE
    g_PlayingFromLineHolder = FALSE


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif

ENDPROC






PROC Construct_Multipart_Phonecall_Roots_and_Specifics(INT Passed_Num_of_Multiparts, STRING &ConstructSegmentLabel[],  STRING &ConstructSpecificLabel[],
                                                            STRING FirstPassedRoot = NULL, STRING FirstPassedSpecific = NULL,
                                                            STRING SecondPassedRoot = NULL, STRING SecondPassedSpecific = NULL,
                                                            STRING ThirdPassedRoot = NULL, STRING ThirdPassedSpecific = NULL,
                                                            STRING FourthPassedRoot = NULL, STRING FourthPassedSpecific = NULL,
                                                            STRING FifthPassedRoot = NULL, STRING FifthPassedSpecific = NULL,
                                                            STRING SixthPassedRoot = NULL, STRING SixthPassedSpecific = NULL,
                                                            STRING SeventhPassedRoot = NULL, STRING SeventhPassedSpecific = NULL)

    
    Set_Multipart_Phonecall_Globals(Passed_Num_of_Multiparts)

    ConstructSegmentLabel[0] = FirstPassedRoot
    ConstructSpecificLabel[0] = FirstPassedSpecific

    ConstructSegmentLabel[1] = SecondPassedRoot
    ConstructSpecificLabel[1] = SecondPassedSpecific

    ConstructSegmentLabel[2] = ThirdPassedRoot
    ConstructSpecificLabel[2] = ThirdPassedSpecific

    ConstructSegmentLabel[3] = FourthPassedRoot
    ConstructSpecificLabel[3] = FourthPassedSpecific

    ConstructSegmentLabel[4] = FifthPassedRoot
    ConstructSpecificLabel[4] = FifthPassedSpecific

    ConstructSegmentLabel[5] = SixthPassedRoot
    ConstructSpecificLabel[5] = SixthPassedSpecific

    ConstructSegmentLabel[6] = SeventhPassedRoot
    ConstructSpecificLabel[6] = SeventhPassedSpecific

  

ENDPROC
















//After declaring a local instance of the peds speech struct, e.g structPedsForConversation MyLocalPedStruct, use this procedure to add a ped for dialogue.
//The native script conversation commands require an int identifier, a ped index and a voice id. Passing these in here will automate the process for multiple lines.

//Parameter list is as follows:

//YourLocalPedStruct - the name of the struct instance you initially declared. MyLocalPedStruct for example.

//YourNumberID - an int which is currently between 0 and 9 which directly corresponds to the speaker listener columns in DialogueStar / the old dialogue sheets.
//The current player should always be assigned to 0. You should check which character is currently being controlled as the player before adding the ped for dialogue so the voices match up.

//N.B I might be able to put in failsafe stuff in the future to make sure that the player's voice matches the current controlled character.


//YourPedIndex - the ped index of the character that you created. IMPORTANT! If your ped physically exists in the gameworld, i.e for an in-car conversation, then make sure you add the ped for
//dialogue AFTER you have created the ped in your script, not just after you declare the index. Similarly, if your ped does not physically exist, i.e for a phonecall, then pass in NULL
//as the ped_index and make sure the AudioType is set to FRONTEND for that speaker's lines.

//YourVoiceID - a string that refers to the audio voice id of that particular ped.

//PedPlayAmbientAnims - defines, if necessary, whether this ped can play ambient anims. By default this is set to FALSE.





PROC ADD_PED_FOR_DIALOGUE (structPedsForConversation &YourLocalPedStruct, INT YourNumberID, PED_INDEX YourPedIndex,
                             STRING YourVoiceID, BOOL PedPlayAmbientAnims = FALSE, BOOL PedCanUseAutoLookAt = TRUE)



    IF YourLocalPedStruct.PedInfo[YourNumberID].ActiveInConversation = TRUE

        #IF IS_DEBUG_BUILD
            cdPrintstring("DIALOG_PUB - Overwriting ped at position ")
            cdPrintint(YourNumberID) 
            cdPrintnl()
        #endif

    ENDIF


    YourLocalPedStruct.PedInfo[YourNumberID].Index = YourPedIndex
    YourLocalPedStruct.PedInfo[YourNumberID].VoiceId = YourVoiceID


    YourLocalPedStruct.PedInfo[YourNumberID].ActiveInConversation = TRUE
    YourLocalPedStruct.PedInfo[YourNumberID].PlayAmbientAnims = PedPlayAmbientAnims

    YourLocalPedStruct.PedInfo[YourNumberID].CanUseAutoLookAt = PedCanUseAutoLookAt




    //Disable these for multiplayer 225450 , 1135424
    IF NOT g_bInMultiplayer

        IF NOT IS_PED_INJURED (YourPedIndex)        
            IF YourLocalPedStruct.PedInfo[YourNumberID].PlayAmbientAnims = FALSE
                SET_PED_CAN_PLAY_AMBIENT_ANIMS (YourPedIndex, FALSE)
            ELSE
               SET_PED_CAN_PLAY_AMBIENT_ANIMS (YourPedIndex, TRUE) 
            ENDIF
        ENDIF




        IF NOT IS_PED_INJURED (YourPedIndex)
            IF YourLocalPedStruct.PedInfo[YourNumberID].CanUseAutoLookAt = FALSE
                SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT (YourPedIndex, FALSE)
            ELSE
                SET_PED_CAN_USE_AUTO_CONVERSATION_LOOKAT (YourPedIndex, TRUE)
            ENDIF
        ENDIF

            
    ENDIF


 

    #if IS_DEBUG_BUILD
        cdPrintstring("DIALOG_PUB - ADD_PED_FOR_DIALOGUE called. Adding ped at position  ")
        cdPrintint(YourNumberID) 
        cdPrintstring(" with voice ")
        cdPrintstring(YourVoiceID)
        cdPrintstring(" who has a PED INDEX native int of ")
        cdPrintint (NATIVE_TO_INT (YourLocalPedStruct.PedInfo[YourNumberID].Index))
        cdPrintnl()
    #endif


ENDPROC





//If you've created a speaker who is a NULL ped, usually that will just play FRONTEND. However you may wish to have the voice emanate from a specific locations in the game world.
//Pass in the speakernumber of the ped and a vector to do this. Currently only supporting one per conversation but can add others on request.
//Once you call this on a ped, it will be embedded within your local dialogue struct and apply to every subsequent conversation until you forcibly remove it with the
//REMOVE_VOICE_LOCATION_FOR_NULL_PED command.

PROC SET_VOICE_LOCATION_FOR_NULL_PED (structPedsForConversation &YourLocalPedStruct, INT YourSpeakerNumberID, VECTOR YourVoiceLocation)

    YourLocalPedStruct.NullPed_Number_for_VoicePlacement = YourSpeakerNumberID
    YourLocalPedStruct.NullPed_Vector_for_VoicePlacement = YourVoiceLocation
    
    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("DIALOG_PUB - SET_VOICE_LOCATION_FOR_NULL_PED called. SpeakerNumber passed was ")
        cdPrintint(YourSpeakerNumberID) 
        cdPrintnl()
    #endif

ENDPROC




PROC REMOVE_VOICE_LOCATION_FOR_NULL_PED (structPedsForConversation &YourLocalPedStruct, INT YourSpeakerNumberID)

    YourLocalPedStruct.NullPed_Number_for_VoicePlacement = -99
    YourLocalPedStruct.NullPed_Vector_for_VoicePlacement = <<0.0, 0.0, 0.0>>

    //Need this in case of expansion
    IF YourSpeakerNumberID < 0
        SCRIPT_ASSERT ("Passed in invalid speaker number for REMOVE_VOICE_LOCATION...")
    ENDIF



    #if IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring("DIALOG_PUB - REMOVE_VOICE_LOCATION_FOR_NULL_PED called. SpeakerNumber passed was ")
        cdPrintint(YourSpeakerNumberID)
        cdPrintnl()
    #endif


ENDPROC







//This will remove a ped from your dialogue struct. If your ped did exist in the gameworld they may perhaps be killed during the course of the mission.
//If a you try to add a gameworld ped, i.e not just a disembodied voice, who no longer exists the game will throw up an assert.

PROC REMOVE_PED_FOR_DIALOGUE (structPedsForConversation &YourLocalPedStruct, INT YourNumberID)

    
    IF YourLocalPedStruct.PedInfo[YourNumberID].ActiveInConversation = TRUE

    

        YourLocalPedStruct.PedInfo[YourNumberID].ActiveInConversation = FALSE


    
        #IF IS_DEBUG_BUILD
            cdPrintstring("DIALOG_PUB - REMOVE_PED_FOR_DIALOGUE called. Ped at position ")
            cdPrintint(YourNumberID) 
            cdPrintstring(" has been removed from your dialogue struct.")
            cdPrintnl()
        #endif

    
    
    ELSE


        #IF IS_DEBUG_BUILD
            cdPrintstring("DIALOG_PUB - REMOVE_PED_FOR_DIALOGUE called but this ped is already inactive in conversation ")
            cdPrintint(YourNumberID) 
            cdPrintnl()
        #endif



    ENDIF


ENDPROC

















//Wiki-ed
//This function forces a character to call the player's cellphone and wait for him to answer before playing an automated phonecall conversation.

//USER INFO:

//WhichChar should be a character listed in the character sheet - check charsheet_globals.sch or your script editor for the latest list of characters. 
//The name of the character is cross referenced and this is displayed on the phone.

//WhichBlockOfTextToLoad is the text block you'd normally load into the game's additional text slot for dialogue. It's in the top left of the XLS.
//The text is loaded automatically by this function so you don't have to worry about it.


//WhichRootlabel is the name of the text segment / conversation root you wish to play from Dialogue Star. It can be a group of one or many lines. The lines are manipulated 
//by me to produce a conversation from one initial Root label. Think of it as the root of a conversation.

//Priority is self-explanatory. Lower priority calls can be interrupted. Please use CONV_PRIORITY_VERY_HIGH for just now...


//BOOL ShouldDisplaySubtitles and BOOL ShouldAddToBriefScreen are self-explanatory. Forget about these unless you want to specifically alter the default behaviour.


//The BOOL IsThisInboundForCellphone can be safely left out of your function call. It defaults to TRUE and should only be set to FALSE in exceptional circumstances as this 
//prevents the "waiting on you to answer" screen from displaying.


//Example usage: CHAR_CALL_PLAYER_CELLPHONE(CHAR_LESTER, "HST1_CALL3A", CONV_PRIORITY_VERY_HIGH) 







FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, 
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = TRUE)




    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.
    
    g_IsEmergencyServicesCallHolder = FALSE


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif

    

    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC
















//Human to Human Chat Calls - These functions specifically go through the dialogue handling system.



//This will launch an incoming call from the chosen player name, specified in the first string parameter.
FUNC BOOL INITIATE_INCOMING_CHAT_CALL (STRING WhichChatCallerString, enumConversationPriority PassedConversationPriority = CONV_PRIORITY_NON_CRITICAL_CALL, 
                                        BOOL IsThisInboundForCellphone = TRUE)




    g_ChatCallerStringHolder = WhichChatCallerString




    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = TRUE


    #if IS_DEBUG_BUILD

            cdPrintString ("CHAT_CALL assignment 21 - HOLDER has been set to TRUE")
            cdPrintnl()
            cdPrintnl()
  
    #endif




    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.
    
    g_IsEmergencyServicesCallHolder = FALSE


    
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif



    RETURN CheckCurrentConversationStatus ("DUMMY", PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC






//This will safely terminate any incoming chat call once it has been accepted. Prior to this Hang_up_and_put_away_phone() should be used.
PROC SET_INCOMING_CHAT_CALL_SETUP_AS_FINISHED()

    IF g_IsThisAnMPChatCall 

        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_PUB - Request to SET_INCOMING_CHAT_CALL_SETUP_AS_FINISHED. Will remove phone and free dialogue system. ")
            cdPrintnl()
            
            cdPrintString ("g_IsThisAnMPChatCall assigned to false from SET_INCOMING_CHAT_CALL_SETUP_AS_FINISHED()...  2111525")
            cdPrintnl()
            
        #endif

        g_IsThisAnMPChatCall = FALSE

    ELSE

        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_PUB - Request to SET__INCOMING_CHAT_CALL_SETUP_AS_FINISHED but no chat call is currently set up.")
            cdPrintnl()
            
        #endif

    ENDIF

ENDPROC













//Wiki-ed
//The same as above but forces the player to answer the call.

FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                                    enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                    BOOL IsThisInboundForCellphone = TRUE)

    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = TRUE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.
    
    g_IsEmergencyServicesCallHolder = FALSE

    
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC










//Wiki-ed
//Create an outbound phonecall - the phone can be forced onscreen by default which brings up the appropriate calling / talking screens for that contact.

//By not forcing the phone onscreen, this function can be used in conjunction with IS_CALLING_CONTACT to play an appropriate answerphone message or
//flow phonecall by any checking script. If the player is pro-actively calling a contact, the phone must already be in hand and onscreen

FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.

    g_IsEmergencyServicesCallHolder = FALSE
  

    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


   
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif



    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC







FUNC BOOL PLAYER_INITIATE_CHAT_CALL (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.


    //Critical! This must be specified as a chat call.
    g_IsThisAnMPChatCallHolder = TRUE


    #if IS_DEBUG_BUILD

            cdPrintString ("CHAT_CALL assignment 23 - HOLDER has been set to TRUE")
            cdPrintnl()
            cdPrintnl()
  
    #endif


    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.

    g_IsEmergencyServicesCallHolder = FALSE
  

    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


   
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif



    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC














// Shouldn't be used by other scripts. Here for the convenience of the PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES public functions.

PROC Player_Call_Char_Multipart_Setup()


    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_IsThisAnMPChatCallHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.

    g_IsEmergencyServicesCallHolder = FALSE
  

    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif

ENDPROC





//Create an outbound phonecall - the phone can be forced onscreen by default which brings up the appropriate calling / talking screens for that contact.
//This allows the phonecall to be constructed from single lines from different conversation blocks from the same dstar page. They must share the same AUD block!
FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (2, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC








//Special case for friend activity phonecalls.

FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_MULTIPART_WITH_2_LINES_WITH_2_LINE_REPLIES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        STRING QuestionGodTextLabel,  
                                        STRING FirstRootYesReplyLabel, STRING FirstSpecificYesReplyLabel,
                                        STRING SecondRootYesReplyLabel, STRING SecondSpecificYesReplyLabel,
                                        STRING FirstRootNoReplyLabel, STRING FirstSpecificNoReplyLabel,
                                        STRING SecondRootNoReplyLabel, STRING SecondSpecificNoReplyLabel,

                                        enumConversationPriority PassedConversationPriority,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = TRUE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()


    //Overwrite certain parts of Player_Call_Char_Multipart_Setup for this special case.

    g_CallRequiringPlayerResponseHolder = TRUE
    g_QuestionGodLabelHolder = QuestionGodTextLabel
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (2, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel)





    g_TotalReplyMultipartsHolder = 2

    //May optimise this by re-using parts of existing Multipart[30] array near end of development. Here for proof of concept in the interim.                                      
    g_Multipart_YesSegmentLabelHolder[0] =  FirstRootYesReplyLabel
    g_Multipart_YesSpecificLabelHolder[0] = FirstSpecificYesReplyLabel


    g_Multipart_YesSegmentLabelHolder[1] =  SecondRootYesReplyLabel
    g_Multipart_YesSpecificLabelHolder[1] = SecondSpecificYesReplyLabel




    g_Multipart_NoSegmentLabelHolder [0] =  FirstRootNoReplyLabel
    g_Multipart_NoSpecificLabelHolder[0] =  FirstSpecificNoReplyLabel
    

    g_Multipart_NoSegmentLabelHolder [1] =  SecondRootNoReplyLabel
    g_Multipart_NoSpecificLabelHolder[1] =  SecondSpecificNoReplyLabel
    




    




    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC



















FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_3_LINES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (3, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel )



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC













FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_4_LINES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                        STRING FourthRootLabel, STRING FourthSpecificLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (4, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel, FourthRootLabel, FourthSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC





// Need to set up the roots and specifics as string arrays, but globals are modified as in other multipart cellphone calls.
FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES (INT iLines, structPedsForConversation &YourPedStruct, enumCharacterList WhichChar,
                                        STRING WhichBlockOfTextToLoad, STRING& RootLabels[], STRING& SpecificLabels[],
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES,
                                        enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 
    
    Player_Call_Char_Multipart_Setup()
    
    IF ShouldPhoneBeForcedOnScreen = TRUE
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
    ELSE
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE
    ENDIF
    
    Set_Multipart_Phonecall_Globals(iLines)
    
    RETURN CheckCurrentConversationStatusForMultipart(RootLabels, SpecificLabels, PassedConversationPriority, IsThisInboundForCellphone)
ENDFUNC









//This allows an outgoing call of N number of lines to wait for a response once the initial N batch of lines have concluded. See bug 685516.
//You must specify the QuestionGodTextLabel so that a question will appear on the phone prompting a response. This can be loaded as part of your mission text block.
//This function should be used in conjunction with SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES. 
//Example usage of a 10 line multipart call that waits for a response to CELL_291's question and plays a 6 line YES or a 7 line NO branch:

//IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE (10, MyLocalPedStruct, CHAR_DR_FRIEDLANDER, "RECREAU", TestRoots, TestLabels, "CELL_291", CONV_PRIORITY_VERY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
//                
//  SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES (6, 7, TestYesRoots, TestYesSpecifics, TestNoRoots, TestNoSpecifics)
//
//ENDIF

FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE (INT iLines, structPedsForConversation &YourPedStruct, enumCharacterList WhichChar,
                                        STRING WhichBlockOfTextToLoad, STRING& RootLabels[], STRING& SpecificLabels[], STRING QuestionGodTextLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES,
                                        enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 
    
    Player_Call_Char_Multipart_Setup()
    
    IF ShouldPhoneBeForcedOnScreen = TRUE
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
    ELSE
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE
    ENDIF
    
    Set_Multipart_Phonecall_Globals(iLines)


    g_CallRequiringPlayerResponseHolder = TRUE

    g_QuestionGodLabelHolder = QuestionGodTextLabel



    g_TotalReplyMultipartsHolder = 3 //Makes sure dialogue handler is primed to expect a multipart reply that is not a two-line friend activity special case.

    
    RETURN CheckCurrentConversationStatusForMultipart(RootLabels, SpecificLabels, PassedConversationPriority, IsThisInboundForCellphone)

ENDFUNC







//Used in conjunction with PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE.
//Example usage of a 10 line multipart call that waits for a response to CELL_291's question and plays a 6 line YES or a 7 line NO branch:
//IF PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_N_LINES_REQUIRES_RESPONSE (10, MyLocalPedStruct, CHAR_DR_FRIEDLANDER, "RECREAU", TestRoots, TestLabels, "CELL_291", CONV_PRIORITY_VERY_HIGH, TRUE, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
//                
//  SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES (6, 7, TestYesRoots, TestYesSpecifics, TestNoRoots, TestNoSpecifics)
//
//ENDIF


 FUNC BOOL SET_UP_MULTIPART_WITH_N_LINES_YES_NO_BRANCHES (INT iYesLines, INT iNoLines, STRING& RootYesLabels[], STRING& SpecificYesLabels[], STRING& RootNoLabels[], STRING& SpecificNoLabels[])




    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_UNIVERSAL_REPLIES_FOR_N_LINES_HAS_BEEN_SET)


        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_PUB g_BS_UNIVERSAL_REPLIES_FOR_N_LINES has already been set for this conversation phase.")
            cdPrintnl()
            
        #endif

        RETURN FALSE

    
    ELSE


        //Set critical bit. This is cleared in dialogue_private's common cleanup.
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_UNIVERSAL_REPLIES_FOR_N_LINES_HAS_BEEN_SET)
        
        


        g_i_Total_UniversalYesReplies = iYesLines

        INT tempYIndex = 0

                              
        WHILE tempYIndex < (iYesLines + 1)
        
            g_Multipart_Universal_YesRoots[tempYIndex] = RootYesLabels[tempYindex]
            g_Multipart_Universal_YesSpecifics[tempYIndex] = SpecificYesLabels[tempYindex]

            tempYIndex ++

        ENDWHILE

                
                
        g_i_Total_UniversalNoReplies = iNoLines

        INT tempNIndex = 0

                              
        WHILE tempNIndex < (iNoLines + 1)
        
            g_Multipart_Universal_NoRoots[tempNIndex] = RootNoLabels[tempNindex]
            g_Multipart_Universal_NoSpecifics[tempNIndex] = SpecificNoLabels[tempNindex]

            tempNIndex ++

        ENDWHILE



        
        #if IS_DEBUG_BUILD

            cdPrintstring ("DIALOG_PUB g_BS_UNIVERSAL_REPLIES_FOR_N_LINES yes and no branches have been set.")
            cdPrintnl()

            PRINTLN("\nDIALOG_PUB [", GET_THIS_SCRIPT_NAME(), "] has set universal yes no branch replies.")
        
        #endif

        RETURN TRUE



    ENDIF


ENDFUNC





/*
 PROC SET_UP_MULTIPARTS_WITH_N_LINES_NO_BRANCH (INT iLines, STRING& RootYesLabels[], STRING& SpecificYesLabels)


    //Change to function and check for universal bit here


    //Would set universal bit here - it would then be cleaned up in dialogue common cleanup.


    g_i_Total_UniversalNoReplies = iNoLines

    INT tempNIndex = 0

                          
    WHILE tempNIndex < (iLines + 1)
    
        g_Multipart_Universal_NoRoots[tempNIndex] = RootNoLabels[tempNindex]
        g_Multipart_Universal_NoSpecifics[tempNIndex] = RootNoLabels[tempNindex]

        tempInt ++

    ENDWHILE


ENDPROC
*/








//TEXT_LABEL_23 g_Multipart_UniversalYes[30]
//TEXT_LABEL_23 g_Multipart_UniversalNo[30]

























 //Wkik-ed                                                                                            

//Allows the user to pass in all the details required for a call to the 911 operator. Note the default behaviour of this function has changed slightly, the phone is not forced in hand.

//WhichRootLabel refers to the text from the dialogue sheet that describes "The operator answering your call and asking what service you require"

//STRING QuestionGodTextLabel should be a god text label from the .gxt file which poses a short question to be displayed on the phone. It is not part of the conversation or dialogue.
//e.g "Which service?" 

//The phone system then offers a onscreen choice of Ambulance ( Paramedics ), Fire or Police. The appropriate label is then used to select the correct response call from the dialogue sheet.


FUNC BOOL PLAYER_CALL_EMERGENCY_SERVICES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                            STRING QuestionGodTextLabel, STRING AmbRootLabel, STRING FireRootLabel, STRING PoliceRootLabel,
                                            BOOL ShouldPhoneBeForcedOnScreen = FALSE, 
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                            BOOL IsThisInboundForCellphone = FALSE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE       

    g_IsEmergencyServicesCallHolder = TRUE

    g_IsThisAnMPChatCallHolder = FALSE


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


    g_AmbulanceSegmentLabelHolder = AmbRootLabel
   
    g_FireSegmentLabelHolder = FireRootLabel

    g_PoliceSegmentLabelHolder = PoliceRootLabel

    g_QuestionGodLabelHolder = QuestionGodTextLabel



    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC







//Wiki-ed

//Creates a conference call with two contacts involved, the second parameter should also be taken from the charsheet. Both these names will appear on the phone's "calling" screen during a phonecall.

FUNC BOOL PLAYER_MAKE_CONFERENCE_CALL (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, enumCharacterList WhichSecondChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, 
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)

    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_AdditionalContactInvolvedinCallHolder = WhichSecondChar


    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsConferenceCallHolder = TRUE //this is a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.

    g_IsEmergencyServicesCallHolder = FALSE
  

    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC











//Wiki-ed

//Same as CHAR_CALL_PLAYER_CELLPHONE but the call will wait on a yes / no response from the player after the first conversation Root
//before playing the next appropriate segment based on the player's choice

//STRING WhichBlockOfTextToLoad is the text block you'd normally load into the game's additional text slot for dialogue. It's in the top left of the XLS.
//The text is loaded automatically by this function so you don't have to worry about it.


//STRING WhichRootLabel STRING YesRootLabel, STRING NoRootLabel are all Root labels from the XLS sheet. These are manipulated by me to 
//produce the conversation.  WhichRoot label is the first conversation which ends on a question. The YesRootLabel is the conversation branch 
//that would be followed if the player selects "YES" on the phone, and NoRootLabel is the negative answer branch.

//STRING QuestionGodTextLabel should be a god text label from the .gxt file which poses a short question to be displayed on the phone. It is not part 
//of the conversation and is not dialogue.

//e.g If you have a branching YES / NO conversation that finished the initial Root with something like:

//"Hey Boss, do you want me to get pics and put the screws on this guy?"

//then you should have a god text label kept in the same block and text file as your other mission god text which says something like:

//"Blackmail architect?"

//As this is intended to be displayed on the phone, please keep the question very concise ( two to three words max ) and remember the question mark.



FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, STRING YesRootLabel, STRING NoRootLabel, 
                                                    enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                    BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 


    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE


    g_YesSegmentLabelHolder = YesRootLabel
   
    g_NoSegmentLabelHolder = NoRootLabel

    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC




//Wiki-ed
//The same as above but forces the player to answer the call.

FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_REPLIES_FORCE_ANSWER (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, STRING YesRootLabel, STRING NoRootLabel, 
                                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                                BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = TRUE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE


    g_YesSegmentLabelHolder = YesRootLabel
   
    g_NoSegmentLabelHolder = NoRootLabel

    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC

















                             
//Legacy. Please use CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_AUTO_PICKUP
/*
FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_FORCE_ANSWER (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, 
                                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                                BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMpJobOfferHolder = TRUE

    g_PlayingSingleLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE



    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 



    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC

*/




FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_AUTO_PICKUP (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, 
                                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                                BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = TRUE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE



    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC





FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER  (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, 
                                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                                BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = TRUE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE



    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC




FUNC BOOL CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_FORCE_ANSWER  (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, 
                                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                                BOOL IsThisInboundForCellphone = TRUE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = TRUE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = TRUE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE

    g_IsEmergencyServicesCallHolder = FALSE



    g_QuestionGodLabelHolder = QuestionGodTextLabel
   
 
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC






//Wiki-ed

//Same as PLAYER_CALL_CHAR_CELLPHONE but the call will wait on a yes / no response from the player after the first conversation segment before playing 
//the next appropriate segment.

//STRING WhichRootLabel STRING YesRootLabel, STRING NoRootLabel are all Root labels from the XLS sheet. These are manipulated by me to produce the conversation.

//STRING QuestionGodTextLabel should be a god text label from the .gxt file which poses a short question to be displayed on the phone. It is not part of the conversation or dialogue.
//e.g Blackmail Architect? See the above function for more info.

FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_WITH_REPLIES (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority, STRING QuestionGodTextLabel, STRING YesRootLabel, STRING NoRootLabel, 
                                                    BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                                    enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                                    BOOL IsThisInboundForCellphone = FALSE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE
    
    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF

    
    g_CallRequiringPlayerResponseHolder = TRUE
    g_IsEmergencyServicesCallHolder = FALSE

    g_YesSegmentLabelHolder = YesRootLabel
   
    g_NoSegmentLabelHolder = NoRootLabel

    g_QuestionGodLabelHolder = QuestionGodTextLabel



    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC







PROC SET_DYNAMIC_BRANCH_FOR_ONGOING_CALL (STRING WhichBlockOfTextToLoad, STRING WhichRootLabel)


    IF IS_MOBILE_PHONE_CALL_ONGOING()

        g_PrepareCallForDynamicBranch = TRUE
        g_DynamicBlockToLoad  = WhichBlockOfTextToLoad
        g_DynamicBranch_Root = WhichRootLabel


        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Dynamic branch has been set via public script function with root ")
            cdPrintstring(g_DynamicBranch_Root)
            cdPrintnl()
        #endif


    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - WARNING! Dynamic branch can only be set during an ongoing call.")
            cdPrintnl()
        #endif

    ENDIF


ENDPROC



 
PROC PLAY_DYNAMIC_BRANCH (bool b_FinishCurrentLine = TRUE)

    

    STOP_SCRIPTED_CONVERSATION (b_FinishCurrentLine)


    IF b_FinishCurrentLine
    
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Dynamic branch will play after current line concludes. ")
            cdPrintstring(g_DynamicBranch_Root)
            cdPrintnl()
        #endif


    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Dynamic branch will play immediately. Not finishing current line.")
            cdPrintnl()
        #endif

    ENDIF





ENDPROC




PROC CANCEL_DYNAMIC_BRANCH_FOR_ONGOING_CALL()

    
    IF g_PrepareCallForDynamicBranch = TRUE

        g_PrepareCallForDynamicBranch = FALSE


        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Dynamic branch cancelled!")
            cdPrintnl()
        #endif

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Dynamic branch not currently set, no cancel action required.")
            cdPrintnl()
        #endif

    ENDIF


ENDPROC




FUNC BOOL HAS_DYNAMIC_BRANCH_BEEN_SET()


    IF g_PrepareCallForDynamicBranch = TRUE
    
        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF


ENDFUNC





//Wiki-ed

//Returns TRUE if a cellphone call has finished. You can call this in your scripts after successfully launching a phonecall, i.e any "create phonecall" function returns TRUE,
//in order to tell whether or not your call has concluded or not.
FUNC BOOL HAS_CELLPHONE_CALL_FINISHED()

    IF g_ConversationStatus = CONV_STATE_FREE
    
        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC











//Wiki-ed
//"Interrupted" function This will return TRUE in the following circumstances for the immediately preceding call that has gotten through to the phone:
// - if an incoming call is rejected, i.e you choose not to answer.
// - if any call, outgoing or incoming, has connected, i.e you are in conversation and then you hang up before the conversation concludes naturally.
// - if an incoming call is waiting for an answer or in connected conversation and the player enters water or is damaged in any way.*
// - if an outgoing call is in connected conversation and the player enters water or is damaged in any way*
///* it's proposed that Franklin has a waterproof phone so the water clause may not apply to him when he's under player control.

//Important! WAS_LAST_CELLPHONE_CALL_INTERRUPTED will return FALSE again when the following happens.
// - Any other incoming call gets through to the cellphone. As soon as the phone rings at the player's end, it will revert to FALSE.
// - An outgoing call reaches connected conversation. Simply dialling a number on the phone and waiting for connection will not clear a TRUE state. This prevents the player 
//  from quickly wiping the interrupted flag.



FUNC BOOL WAS_LAST_CELLPHONE_CALL_INTERRUPTED()

    IF g_LastCellphoneCallInterrupted = TRUE
    OR  g_LastInboundCallRejected = TRUE  //classing a call that has been actively or automatically rejected as interrupted also.
    
        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC






//Wiki-ed.
//Check this after an inbound cellphone call has been successfully launched i.e returns true and the player phone is ringing.
//If you just want to make sure the phone call is answered you can use this.
//Alternatively you may want to use the FORCE_ANSWER variants.
FUNC BOOL CHECK_CELLPHONE_LAST_CALL_REJECTED()

    IF g_LastInboundCallRejected
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC


//This specifically checks for the phonecall being hung up by the joypad during the dialogue portion of a phonecall. It's not the same as rejected. 
//"Rejected" means the player has chosen not to answer at all. So you may want to use a combination of both these functions to make sure the call is answered 
//and not deliberately hung up during it. Accidental interruptions will not make this return true!
  
FUNC BOOL WAS_LAST_PHONECALL_HUNG_UP_BY_JOYPAD()

    IF g_LastCallHungupDuringCallViaJoypad
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC




//This will specifically check for the phone being in ATTEMPTING or ONGOING call states but script has called HANG_UP_AND_PUT_AWAY_PHONE().
//The bit is cleared the next time a phonecall occurs. 
FUNC BOOL WAS_LAST_PHONECALL_ABORTED_BY_SCRIPT_HANG_UP()

    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_LAST_CALL_ABORTED_DUE_TO_SCRIPT_HANGUP)
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC







//This will disable the user's ability to hang up a call. For safety's sake you must call this with TRUE after your CHAR_CALL... or PLAYER_CALL... returns true.
//The hangup is re-enabled internally by me just before any requested phonecall signals to the asking script that it can play. ( via CheckCurrentConversationStatus )
PROC DISABLE_HANGUP_FOR_THIS_CALL (BOOL bDisableHangup = FALSE)


    IF bDisableHangup = TRUE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Disabled hangup for this call!")
            cdPrintnl()
        #endif

        SET_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Enabled hangup for this call.")
            cdPrintnl()
        #endif


        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP)

    ENDIF
 
ENDPROC







 //This will disable the user's ability to hang up a call. For safety's sake you must call this with TRUE after your CHAR_CALL... or PLAYER_CALL... returns true.
//The hangup is re-enabled internally by me just before any requested phonecall signals to the asking script that it can play. ( via CheckCurrentConversationStatus )
PROC KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU(BOOL bKeepActiveOnPause = FALSE)


    IF bKeepActiveOnPause = TRUE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Set KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU for this call.")
            cdPrintnl()
        #endif

        SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU)

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Removed KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU bit flag.")
            cdPrintnl()
        #endif


        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU)

    ENDIF
 
ENDPROC













     

//If a WITH_REPLIES phone call is in the question state where it is waiting on a positive or negative response from the player, this will return TRUE.
//This can be used in conjunction with FORCE_NEGATIVE_RESPONSE to allow you to determine when the "wait for response" period started and allow you to timeout.

FUNC BOOL IS_CELLPHONE_CALL_WITH_REPLIES_WAITING_ON_USER_INPUT()

    IF IS_BIT_SET (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_WITH_REPLIES_WAITING_ON_USER_INPUT)

        IF g_CallRequiringPlayerResponse = TRUE

            RETURN TRUE

        ELSE

            RETURN FALSE

        ENDIF
    
    ELSE
    
        RETURN FALSE
    
    ENDIF


ENDFUNC




//This will force a negative reply to any CALL_WITH_REPLIES. Should be used only as a timeout feature for when the user has not actively responded.

PROC FORCE_NEGATIVE_RESPONSE_TO_CALL_WITH_REPLIES (BOOL bForceNegative = FALSE)


    IF bForceNegative = TRUE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Forced negative reply for this call!")
            cdPrintnl()
        #endif

        SET_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CALL_WITH_REPLIES_NEGATIVE) 

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Cleared negative reply  for this call.")
            cdPrintnl()
        #endif


        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CALL_WITH_REPLIES_NEGATIVE) 

    ENDIF
 
ENDPROC








//Useful function to return the text label of the currently playing line of a conversation or phonecall. You can use this to tell when a certain line is playing by
//comparing the result with your chosen label via native command ARE_STRINGS_EQUAL. Won't work with multipart conversations.

FUNC TEXT_LABEL_23 GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()

    
    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus = CONV_STATE_PLAYING

        INT ReturnInt
        
        ReturnInt = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()

        
        //Temp!
//        #if IS_DEBUG_BUILD
//        IF ReturnInt > -1
//
//            cdPrintstring("GCSCL is ")
//            cdPrintInt(ReturnInt)
//            cdPrintnl()
//
//            DISPLAY_TEXT_WITH_LITERAL_STRING (0.3, 0.20, "STRING", g_ConversationLabels[ReturnInt])
//
//        ENDIF
//        #endif


        ReturnInt = ReturnInt + g_DialogueAdjustment
    

        //Temp!
//        #if IS_DEBUG_BUILD
//        IF ReturnInt > -1
//
//            cdPrintstring("Adjusted int is ")
//            cdPrintInt(ReturnInt)
//            cdPrintnl()
//
//            DISPLAY_TEXT_WITH_LITERAL_STRING (0.3, 0.25, "STRING", g_ConversationLabels[ReturnInt])
//        
//        ENDIF
//        #endif
    



        IF ReturnInt > -1 //need to check this to prevent potential array overrun if any delay takes place.
            //DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.20, "STRING", g_ConversationLabels[ReturnInt])

 
             RETURN g_ConversationLabels[ReturnInt]
             
        ELSE
                   
            RETURN ReturnNull

        ENDIF

    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 




//Very similar to the above, but takes into account procedural speech line numbering (such as SFX_LOCATION and SFX_CODENAME used in MP Cop despatch )

FUNC TEXT_LABEL_23 GET_CURRENTLY_PLAYING_STANDARD_PROCEDURAL_CONVERSATION_LABEL()

    
    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus = CONV_STATE_PLAYING

        INT ReturnInt
        
        ReturnInt = GET_CURRENT_UNRESOLVED_SCRIPTED_CONVERSATION_LINE()

        
        //Temp!
//        #if IS_DEBUG_BUILD
//        IF ReturnInt > -1
//
//            cdPrintstring("GCSCL is ")
//            cdPrintInt(ReturnInt)
//            cdPrintnl()
//
//            DISPLAY_TEXT_WITH_LITERAL_STRING (0.3, 0.20, "STRING", g_ConversationLabels[ReturnInt])
//
//        ENDIF
//        #endif


        ReturnInt = ReturnInt + g_DialogueAdjustment
    

        //Temp!
//        #if IS_DEBUG_BUILD
//        IF ReturnInt > -1
//
//            cdPrintstring("Adjusted int is ")
//            cdPrintInt(ReturnInt)
//            cdPrintnl()
//
//            DISPLAY_TEXT_WITH_LITERAL_STRING (0.3, 0.25, "STRING", g_ConversationLabels[ReturnInt])
//        
//        ENDIF
//        #endif
    



        IF ReturnInt > -1 //need to check this to prevent potential array overrun if any delay takes place.
            //DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.20, "STRING", g_ConversationLabels[ReturnInt])

 
             RETURN g_ConversationLabels[ReturnInt]
             
        ELSE
                   
            RETURN ReturnNull

        ENDIF

    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 









//Similar to GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL except it will return the NEXT valid subtitle label. So if a conversation has reached label 4, this will pass back label 5.
//If the next label should happen to be a audio sfx pause, then the next label will be checked. This is handy for interrupting a conversation, then resuming at a later point via
//fellow public function CREATE_CONVERSATION_FROM_SPECIFIC_LINE

FUNC TEXT_LABEL_23 GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()

    
    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus = CONV_STATE_PLAYING

        INT ReturnInt, AdjustedInt
        
        ReturnInt = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()

        
        /* Comment in to debug...
        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring("DIALOG_PUB_TEST... Resumption Label Return Int is:")
            cdPrintint (ReturnInt)
            cdPrintstring(" g_DialogueAdjustment is:")
            cdPrintint (g_DialogueAdjustment)
            cdPrintnl()

        #endif
        */
        

        
        ReturnInt = ReturnInt + g_DialogueAdjustment


        AdjustedInt = (ReturnInt + 1) //Get the next line label...




        IF AdjustedInt > -1 AND ReturnInt > -1 //need to check this to prevent potential array overrun if any delay takes place.
            //DISPLAY_TEXT_WITH_LITERAL_STRING (0.4, 0.20, "STRING", g_ConversationLabels[ReturnInt])

            IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[AdjustedInt])

                
                #if IS_DEBUG_BUILD
                    cdPrintstring("DIALOG_PUB... Resumption Label is:")
                    cdPrintstring(g_ConversationLabels[AdjustedInt])
                    cdPrintnl()
                #endif



                RETURN g_ConversationLabels[AdjustedInt] //Next subtitle label must exist, so can't be last line and can't be SFX pause. Safe to return.

            ELSE

                INT tempIndex = AdjustedInt

                    WHILE tempIndex < constConversationLabels

                        IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[tempIndex])
    
                            #if IS_DEBUG_BUILD
                                cdPrintnl()
                                cdPrintstring("DIALOG_PUB... Checked resumption label was audio SFX or pause, validated next actual subtitle label at ")
                                cdPrintint (tempIndex)
                                cdPrintstring(" which  is:")
                                cdPrintstring(g_ConversationLabels[tempIndex])
                                cdPrintnl()

                                cdPrintnl()
                            #endif

                           
                           
                           RETURN g_ConversationLabels[tempIndex]

                           tempIndex = constConversationLabels  
       
                        ENDIF
                    
                        tempIndex ++

                    ENDWHILE
                


                //If we haven't found and returned another valid subtitle label by this point we can assume the conversation was already on the last line...
                #if IS_DEBUG_BUILD
                    cdPrintstring("DIALOG_PUB... Warning! Was already playing last line of current conversation when future resumption label was grabbed.")
                    cdPrintnl()
                    cdPrintstring("DIALOG_PUB... Returned null label. Empty conversation will cue if resumed.")
                    cdPrintnl()
                #endif

                

                //RETURN g_ConversationLabels[ReturnInt] //Just return the last played line with a warning, probably have a null check in the future. Want to see how often it happens.
                
                RETURN ReturnNull


            ENDIF


            RETURN g_ConversationLabels[ReturnInt]

        ELSE
                   
            RETURN ReturnNull

        ENDIF

    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 




















//This will return the speaker value for any standard conversation line that is currently playing. Will not work for multipart conversations as they do not use a single SL label.
//If the currently playing line is an audio or SFX pause, then this will return -1. The speaker value is returned as the corresponding int of any alphanumeric speaker designation.
//"A" will return 10, "B" returns 11. Please see ConvertSingleCharacter for full list.

FUNC INT GET_SPEAKER_INT_FOR_CURRENT_STANDARD_CONVERSATION_LINE()

    

    
    IF  g_ConversationStatus = CONV_STATE_PLAYING

        INT ReturnInt
        
        ReturnInt = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()

        

        
        ReturnInt = ReturnInt + g_DialogueAdjustment





        IF ReturnInt > -1 //need to check this to prevent potential array overrun if any delay takes place.
          

            IF DOES_TEXT_LABEL_EXIST (g_ConversationLabels[ReturnInt])

                

                //Subtitle label must exist, so can't be SFX pause. Safe to extrapolate speaker int from SL string


     
                TEXT_LABEL_23 ThisSpeakListenLabel

                STRING componentCharacter


                ThisSpeakListenLabel = g_ConversationData.ConversationSegmentToGrab //Grab from global.
                ThisSpeakListenLabel += "SL"


                componentCharacter = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME (GET_FILENAME_FOR_AUDIO_CONVERSATION (ThisSpeakListenLabel), (ReturnInt * 3), ((ReturnInt * 3)+ 1))

                /*
                #if IS_DEBUG_BUILD
                    cdPrintstring("DIALOG_PUB... Current subtitle label is:")
                    cdPrintstring(g_ConversationLabels[ReturnInt])
                    cdPrintstring(" and speaker component is ")
                    cdPrintstring(componentCharacter)
                    cdPrintnl()
                #endif
                */


                RETURN ConvertSingleCharacter (componentCharacter) //Returned character from string extraction may be alphanumeric. e.g "A". Easier to make this into an int.


            ELSE

           

                //If we haven't found a subtitle label, we can assume the line was an sfx pause.
                /*
                #if IS_DEBUG_BUILD
                    cdPrintstring("DIALOG_PUB... Warning! Tried to grab speaker int of currently playing line but line playing is audio sfx or pause.")
                    cdPrintnl()
                    cdPrintstring("DIALOG_PUB... Returned -1 as a result.")
                    cdPrintnl()
                #endif
                */
                

                
                RETURN -1


            ENDIF




   
        ELSE
                   
            RETURN -2 //GET_CURRENT_SCRIPTED_CONVERSATION_LINE failsafe. Deliberately not -1, so that differentiation of return results can take place. - 1 best used for SFX pause.

        ENDIF





    ELSE

        RETURN -3 //Wasn't playing a conversation when function was called!

    ENDIF





ENDFUNC 








































//Can be used to get the root of a standard conversation that is currently playing. Handy for detecting when a random conversation is playing.
//Will not work with multipart conversations, as there is no singled defined root for those complex conversations.
FUNC TEXT_LABEL_23 GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()


    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus = CONV_STATE_PLAYING

        RETURN g_ConversationData.ConversationSegmentToGrab
       
    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 


//Can be used to get the root of a standard conversation that is currently playing or building. Handy for detecting when a random conversation is playing.
//Will not work with multipart conversations, as there is no singled defined root for those complex conversations.
FUNC TEXT_LABEL_23 GET_CURRENTLY_PLAYING_OR_BUILDING_STANDARD_CONVERSATION_ROOT()


    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus = CONV_STATE_PLAYING
	OR g_ConversationStatus = CONV_STATE_BUILDING

        RETURN g_ConversationData.ConversationSegmentToGrab
       
    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 

//Can be used to get the root of a standard conversation that is at any stage of being used. Handy for detecting when a random conversation is playing.
//Will not work with multipart conversations, as there is no singled defined root for those complex conversations.
FUNC TEXT_LABEL_23 GET_CURRENTLY_IN_USE_STANDARD_CONVERSATION_ROOT()


    TEXT_LABEL_23 ReturnNull = "NULL"

    
    IF g_ConversationStatus != CONV_STATE_FREE

        RETURN g_ConversationData.ConversationSegmentToGrab
       
    ELSE

        RETURN ReturnNull

    ENDIF

ENDFUNC 



//This will get the root of a standard conversation that is playing OR is waiting to play, i.e the conversation is building, paused or a phonecall is waiting to commence.  
FUNC TEXT_LABEL_23 GET_LAST_PASSED_STANDARD_CONVERSATION_ROOT()


    RETURN g_ConversationData.ConversationSegmentToGrab

       
ENDFUNC 










//Wiki-ed
//Returns true if a normal face to face conversation can be started and then plays that conversation. Use for in-car conversations etc.    

//WhichBlockOfTextToLoad is the text block you'd normally load into the game's additional text slot for dialogue. It's in the top left of the XLS.
//The text is loaded automatically by this function so you don't have to worry about it.
//Which Rootlabel is the name of the text Root you wish to play from the XLS. It can be a group of one or many lines. The lines are manipulated 
//by me to produce a conversation from one initial Root label. 
//Priority is self-explanatory. Lower priority calls can be interrupted. For conversations, use one of the following priorities:
/*
    CONV_PRIORITY_AMBIENT_LOW,
    CONV_PRIORITY_AMBIENT_MEDIUM,
    CONV_PRIORITY_AMBIENT_HIGH,

    CONV_PRIORITY_VERY_LOW,
    CONV_PRIORITY_LOW,
    CONV_PRIORITY_MEDIUM
*/


                                                          
FUNC BOOL CREATE_CONVERSATION (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)

    
   

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation) 

    
    
    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES

            PassedConversationPriority = CONV_PRIORITY_MEDIUM

            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
    

    

 
    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMpJobOfferHolder = FALSE

    g_IsThisAnMPChatCallHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif



      
    
    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC












FUNC BOOL PRELOAD_CONVERSATION (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE, INT PassedAutoPlayTime = 0)

    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation) 

    
    
    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    

#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES

            PassedConversationPriority = CONV_PRIORITY_MEDIUM

            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
    

    

 
    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMpJobOfferHolder = FALSE

    g_IsThisAnMPChatCallHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = TRUE

    g_PlayingFromLineHolder = FALSE

    g_i_PreloadAutoPlayTimeHolder  = PassedAutoPlayTime


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif  
    

    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC




PROC BEGIN_PRELOADED_CONVERSATION()


    START_PRELOADED_CONVERSATION()

    g_IsThisFaceToFacePreloaded = FALSE //Make sure dialogue handler will now allow the conversation to be paused.

    #if IS_DEBUG_BUILD

        //Make sure this always displays regardless of phone debug text widget toggle.
        PRINTLN("\nCELLPHONE_PUB - Preloaded conversation begun from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif


ENDPROC










//This should only be used for non-critical conversation lines. The dialogue handler now has a buffer for one conversation which it will repeatedly try to play if no subsequent
//conversations are queued or a kill_conversation is called. This command adds a standard CREATE_CONVERSATION request only. See Steve T if you need something else.
//Notes:
//Dialogue Handler will stop trying to play the buffered conversation after a few seconds have lapsed, any further KILL command has been received or any other conversation successfully plays.
PROC ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)




    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    
    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM

            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
    

   
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("DIALOG_PUB - Conversation added to buffer.")
            cdPrintnl()
        #endif


    SET_BIT (BitSet_DialogueHandler, g_BS_STANDARD_CONVERSATION_BUFFER_ACTIVE) 

    g_BufferConvPriority = PassedConversationPriority
    g_BufferSegmentToGrab = WhichRootLabel



ENDPROC






//Wiki-ed
//To play a single line you must also pass in the full text label of your specific line as a fourth parameter.
//e.g PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "H3AUD", "HEIST3_VA", "HEIST3_VA_1", CONV_PRIORITY_AMBIENT_HIGH)

FUNC BOOL PLAY_SINGLE_LINE_FROM_CONVERSATION (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)




    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 




    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
        
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF


    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = TRUE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE



    g_SpecificLabelHolder = WhichSpecificLabel


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif

    
    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC






//Allows a conversation to be cued from a specific label, rather that starting at _1.
//e.g CREATE_CONVERSATION_FROM_SPECIFIC_LINE (MyLocalPedStruct, "RBS1AUD", "RBS1_DRV2", "RBS1_DRV2_6", CONV_PRIORITY_MEDIUM) 

//This would start the conversation RBS1_DRV2  from label RBS1_DRV2_6 and then continue to play each subsequent line as 
//normal until the end of the conversation or it is killed by any other means.


FUNC BOOL CREATE_CONVERSATION_FROM_SPECIFIC_LINE (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)




    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 




    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM

#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES

            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES
    
    ENDIF



    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = TRUE


      

    g_SpecificLabelHolder = WhichSpecificLabel


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = FALSE
    #endif

    
    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC



















//Lets you know if a conversation is currently playing or in the building state. Handy for knowing when the conversation system is completely free. 
//The native command IS_SCRIPTED_CONVERSATION_ONGOING can be a little late in returning true when precision timings are required.
//This will return true for a phonecall or face-to-face conversation


FUNC BOOL IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

    IF g_ConversationStatus <> CONV_STATE_FREE
    OR IS_SCRIPTED_CONVERSATION_ONGOING()  //caters for last line being finished by audio after a kill even if conv_state if free.
    

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC






//Wiki_ed
//Let's script pause any ongoing face-to-face conversation only. This is not to be used for phonecalls.
//Pass in TRUE to pause any anygoing conversation, FALSE to restart any previously paused conversation.
//NB: Be aware that any high priority conversation, e.g a phonecall, will take precedence over any paused conversation. Higher priority calls to the system will result 
//in the paused conversation being cleaned up.

PROC PAUSE_FACE_TO_FACE_CONVERSATION (BOOL ShouldConversationBePaused)



    #if IS_DEBUG_BUILD

        IF ShouldConversationBePaused 

            cdPrintnl()                                             
            PRINTSTRING ("DIALOG_PUB - Pause face to face conversation called with TRUE")
            PRINTNL()
            
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN ("\nDIALOG_PUB - Conversation paused from [", GET_THIS_SCRIPT_NAME(), "] through public function.")


        ELSE

            cdPrintnl()                                             
            PRINTSTRING ("DIALOG_PUB - Pause face to face conversation called with FALSE")
            PRINTNL()

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN ("\nDIALOG_PUB - Conversation restarted from [", GET_THIS_SCRIPT_NAME(), "] through public function.")


        ENDIF

    #endif


    g_ConversationPaused = ShouldConversationBePaused

ENDPROC





FUNC BOOL IS_FACE_TO_FACE_CONVERSATION_PAUSED()


    IF g_ConversationPaused = TRUE 
    
        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF


ENDFUNC





//This will return TRUE when a cellphone conversation is actually playing, not just in preloaded state. The native command IS_MOBILE_PHONE_CALL_ONGOING will
//return TRUE when a phone call is preloaded or playing.

FUNC BOOL IS_CELLPHONE_CONVERSATION_PLAYING()


    IF g_ConversationStatus = CONV_STATE_PLAYING
        IF IS_MOBILE_PHONE_CALL_ONGOING()

            RETURN TRUE

        ELSE

            RETURN FALSE

        ENDIF

    ELSE
    
        RETURN FALSE
    
    ENDIF


ENDFUNC





PROC PAUSE_CELLPHONE_CONVERSATION (BOOL ShouldConversationBePaused)



    #if IS_DEBUG_BUILD

        IF ShouldConversationBePaused 

            cdPrintnl()                                             
            PRINTSTRING ("DIALOG_PUB - Pause cellphone conversation called with TRUE")
            PRINTNL()
            
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN ("\nDIALOG_PUB - Cellphone conversation paused from [", GET_THIS_SCRIPT_NAME(), "] through public function.")


        ELSE

            cdPrintnl()                                             
            PRINTSTRING ("DIALOG_PUB - Pause cellphone conversation called with FALSE")
            PRINTNL()

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN ("\nDIALOG_PUB - Cellphone conversation restarted from [", GET_THIS_SCRIPT_NAME(), "] through public function.")


        ENDIF

    #endif



    IF IS_MOBILE_PHONE_CALL_ONGOING()

        g_ConversationPaused = ShouldConversationBePaused

        g_PauseRestartHasComeFromDedicatedCellphoneFunction = TRUE

    ELSE
        
        #if IS_DEBUG_BUILD

            PRINTSTRING ("DIALOG_PUB - PAUSE_CELLPHONE_CONVERSATION called, but no cellphone call ongoing. No action taken.")
            PRINTNL()

        #endif

    ENDIF


ENDPROC





FUNC BOOL IS_CELLPHONE_CONVERSATION_PAUSED()


    IF IS_MOBILE_PHONE_CALL_ONGOING()

        IF g_ConversationPaused = TRUE 
        
            RETURN TRUE

        ELSE

            RETURN FALSE

        ENDIF
    
    ELSE

        #if IS_DEBUG_BUILD

            PRINTSTRING ("DIALOG_PUB - IS_CELLPHONE_CONVERSATION_PAUSED check called, but no cellphone call ongoing.")
            PRINTNL()

        #endif

        RETURN FALSE

    ENDIF

ENDFUNC












//This will return TRUE if no face-to-face or cellphone conversation is taking place or has recently finished. 

FUNC BOOL IS_CONVERSATION_STATUS_FREE()

     
    INT CurrentTimer

    CurrentTimer = GET_GAME_TIMER()
    
     
    IF g_ConversationStatus = CONV_STATE_FREE
    
        IF CurrentTimer - g_Timestamp_of_last_ConvStateFree > 3000  //Three seconds

            IF IS_SCRIPTED_CONVERSATION_ONGOING() = FALSE

                RETURN TRUE
            
            ELSE

                RETURN FALSE

            ENDIF


        ELSE

            RETURN FALSE

        ENDIF
        
    ELSE

        RETURN FALSE        

    ENDIF



ENDFUNC





//Tbis will prevent dialogue handler from blocking or killing any dialogue should the player enter or become submerged in water.
//WARNING! Please remember to call this with false when required AND in your cleanup. A bool widget within Script \ Cellphone_Debug acts as a signifier of usage.
PROC ALLOW_DIALOGUE_IN_WATER (BOOL AllowDialogue)


    IF AllowDialogue 
    
        g_AllowUnderwaterDialogue = TRUE


        #if IS_DEBUG_BUILD
        
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nDIALOGUE_PUB - Underwater dialogue switched ON [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        
            cdPrintnl()                                             
            cdPrintstring("DIALOG_PUB - Allowing underwater dialogue. g_AllowUnderwaterDialogue set to TRUE")
            cdPrintnl()
        #endif

    ELSE

        g_AllowUnderwaterDialogue = FALSE


        #if IS_DEBUG_BUILD

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nDIALOGUE_PUB - Underwater dialogue switched OFF [", GET_THIS_SCRIPT_NAME(), "] through public function.")


            cdPrintnl()                                             
            cdPrintstring("DIALOG_PUB - Preventing underwater dialogue. g_AllowUnderwaterDialogue set to FALSE")
            cdPrintnl()
        #endif



    ENDIF

ENDPROC


















//The next three procs will allow you to terminate any ongoing phonecall, face to face conversation or both at once. USE WITH CAUTION AND DON'T CALL EVERY FRAME!


//Wiki-ed
//This will terminate any phonecall or face-to-face conversation currently ongoing. Will finish last line of a face-to-face, but instantly terminate and disconnect a phone call. 
PROC KILL_ANY_CONVERSATION()

    #if IS_DEBUG_BUILD

        PRINTLN("\nDIALOGUE_PUB - KILL_ANY_CONVERSATION called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif


    BitSet_DialogueHandler = 0
    KillAllConversations()

ENDPROC



//Wiki-ed
//This will terminate any phonecall conversation currently ongoing 
PROC KILL_PHONE_CONVERSATION()

    #if IS_DEBUG_BUILD

        PRINTLN("\nDIALOGUE_PUB - KILL_PHONE_CONVERSATION called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif


    BitSet_DialogueHandler = 0
    KillPhoneConversation()

ENDPROC



//Wiki-ed
 //This will terminate any face-to-face conversation currently ongoing, will finish the current line.
PROC KILL_FACE_TO_FACE_CONVERSATION()
    
    #if IS_DEBUG_BUILD

        PRINTLN("\nDIALOGUE_PUB - KILL_FACE_TO_FACE_CONVERSATION called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif


    BitSet_DialogueHandler = 0
    KillFaceToFaceConversation()

ENDPROC


//Only use in special circumstances. It will look and sound weird otherwise!
PROC KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

    #if IS_DEBUG_BUILD

        PRINTLN("\nDIALOGUE_PUB - KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif

    BitSet_DialogueHandler = 0
    KillFaceToFaceConversationImmediately()

ENDPROC






 //Wiki-ed
//Returns the answer stored for the user's response to a cellphone prompt. The enum state is reset to NO_ANSWER_STORED when a new phonecall is 
//successfully cued. Example use:
//  IF CHECK_RESPONSE_TO_CELLPHONE_PROMPT() = RESPONDED_YES
//      Do your stuff...
//  ENDIF

//Make sure you do this every frame after you cue your phonecall, as the response store will be reset to EMPTY when the phonecall naturally finishes or is hung up.

/*

ENUM enumCellphonePromptResponse

    RESPONSE_STORE_EMPTY,
    RESPONDED_YES,
    RESPONDED_NO

ENDENUM

*/


FUNC enumCellphonePromptResponse CHECK_RESPONSE_TO_CELLPHONE_PROMPT()

        RETURN g_CellphonePromptResponse
         
ENDFUNC





//Wiki-ed
//This will clear the response held to any cellphone YES / NO prompt. You are unlikely to need this but the functionality is provided if need be.

PROC CLEAR_RESPONSE_TO_CELLPHONE_PROMPT()

     g_CellphonePromptResponse = RESPONSE_STORE_EMPTY 

ENDPROC






FUNC enumCellphonePromptResponse CHECK_RESPONSE_TO_MP_JOB_OFFER_PROMPT()

        RETURN g_CellphoneJobOfferResponse
         
ENDFUNC





//Wiki-ed
//This will clear the response held to any cellphone YES / NO prompt. You are unlikely to need this but the functionality is provided if need be.

PROC CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT()

     g_CellphoneJobOfferResponse = RESPONSE_STORE_EMPTY 

ENDPROC





























//Multipart conversations. 

//Multipart conversations allow you to pick and choose specific lines from different conversation roots and compile them into one seamless conversation.
//The conversations must share the same subtitle block however and speaker / listener values. Essentially, if all your conversations are on the same page in dialoguestar,
//then you will be able to create a multipart conversation.
//
//For example:

//CREATE_MULTIPART_CONVERSATION_WITH_3_LINES (MyLocalPedStruct, "ARM1AUD", "ARM1_HAIL", "ARM1_HAIL_1", "ARM1_HOME", "ARM1_HOME_3", "ARM1_RACE", "ARM1_RACE_2", CONV_PRIORITY_VERY_HIGH)



//Build a multipart conversation that consists of two lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_2_LINES (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
                
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
                                                                                                            
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (2, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)




ENDFUNC










//Build a multipart conversation that consists of three lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_3_LINES (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                            STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
                                                                                                                
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (3, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)




ENDFUNC












//Build a multipart conversation that consists of four lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_4_LINES (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                            STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                            STRING FourthRootLabel, STRING FourthSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
            
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF

                                                                                                                
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (4, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel, FourthRootLabel, FourthSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)



ENDFUNC




























//Build a multipart conversation that consists of five lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_5_LINES (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                            STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                            STRING FourthRootLabel, STRING FourthSpecificLabel,
                                            STRING FifthRootLabel, STRING FifthSpecificLabel,
                                
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF

    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (5, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel, FourthRootLabel, FourthSpecificLabel,
                                                             FifthRootLabel, FifthSpecificLabel)



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)




ENDFUNC





//____________________________________________________________________________________________________________________________________________________________________________
//
//
//                              New public functions to allow legacy Dialogue from V work in DLC missions. 
//
//              These force the system to look at whatever_language.rpf rather than whatever_languageDLC.rpf even when a DLC mission is running.
//
//____________________________________________________________________________________________________________________________________________________________________________



//  Plan for new functions to allow legacy GTAV dialogue to play in SP ( and eventually MP ) DLC missions that are tagged as such by the flow:
//  Will need to have g_Holder_ForceDLC_DialogueRequest_to_use_Legacy_Source and g_ForceDLC_DialogueRequest_to_use_Legacy_Source
//  Each new Force function will need to set the holder flag to TRUE, all legacy functions will need to set this to false using USE_TU_CHANGES.
//  FUNC BOOL CheckCurrentConversationStatus will copy the holder into the main var when the other stuff goes in, again using TU changes. 
//  Common Dialogue Cleanup will need to set the holder and the main var back to false.





 FUNC BOOL CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN, BOOL cloneConversation = FALSE)

    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen, cloneConversation) 

    
    
    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES

            PassedConversationPriority = CONV_PRIORITY_MEDIUM

            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
    

    

 
    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMpJobOfferHolder = FALSE

    g_IsThisAnMPChatCallHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif



      
    
    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC





FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = FALSE  //necessary so that any higher priority call doesn't set itself up as requiring a response if the interrupted call did.

    g_IsEmergencyServicesCallHolder = FALSE
  

    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


   
    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical
    #endif



    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC








//Create an outbound phonecall - the phone can be forced onscreen by default which brings up the appropriate calling / talking screens for that contact.
//This allows the phonecall to be constructed from single lines from different conversation blocks from the same dstar page. They must share the same AUD block!
FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()



    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (2, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel)




    #if USE_TU_CHANGES

        g_Holder_ForceDLC_DialogueRequest = TRUE    //Critical! Player_Call_Char_Multipart_Setup  and Construct_Multipart_Phonecall_Roots 
                                                    //defaults g_Holder_ForceDLC_DialogueRequest to FALSE, we need to make this TRUE.
    #endif


    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC








//Same as 2 line variant above but supports an extra line.
FUNC BOOL PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_3_LINES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, 
                                        STRING FirstRootLabel, STRING FirstSpecificLabel,
                                        STRING SecondRootLabel, STRING SecondSpecificLabel,
                                        STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                        enumConversationPriority PassedConversationPriority,
                                        BOOL ShouldPhoneBeForcedOnScreen = TRUE,
                                        enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                        BOOL IsThisInboundForCellphone = FALSE)
                                    

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    Player_Call_Char_Multipart_Setup()



    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF
  



    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Phonecall_Roots_and_Specifics (3, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel )




     
    #if USE_TU_CHANGES

        g_Holder_ForceDLC_DialogueRequest = TRUE    //Critical! Player_Call_Char_Multipart_Setup  and Construct_Multipart_Phonecall_Roots 
                                                    //defaults g_Holder_ForceDLC_DialogueRequest to FALSE, we need to make this TRUE.
    #endif


    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, IsThisInboundForCellphone)

    //RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC









FUNC BOOL PLAYER_CALL_EMERGENCY_SERVICES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, enumCharacterList WhichChar, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, enumConversationPriority PassedConversationPriority,
                                            STRING QuestionGodTextLabel, STRING AmbRootLabel, STRING FireRootLabel, STRING PoliceRootLabel,
                                            BOOL ShouldPhoneBeForcedOnScreen = FALSE, 
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN,
                                            BOOL IsThisInboundForCellphone = FALSE)


    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, WhichChar, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    
    g_ForcePlayerAnswerHolder = FALSE

    g_IsThisConversationForPhoneHolder = TRUE //yes, this is a phonecall conversation.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = FALSE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE

    g_IsConferenceCallHolder = FALSE //this is not a conference call.

    g_CallRequiringPlayerResponseHolder = TRUE       

    g_IsEmergencyServicesCallHolder = TRUE

    g_IsThisAnMPChatCallHolder = FALSE


    IF ShouldPhoneBeForcedOnScreen = TRUE
    
        g_ShouldPhoneBeForcedOnScreenHolder = TRUE
        
    ELSE
         
        g_ShouldPhoneBeForcedOnScreenHolder = FALSE

    ENDIF


    g_AmbulanceSegmentLabelHolder = AmbRootLabel
   
    g_FireSegmentLabelHolder = FireRootLabel

    g_PoliceSegmentLabelHolder = PoliceRootLabel

    g_QuestionGodLabelHolder = QuestionGodTextLabel



    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif


    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, IsThisInboundForCellphone)


ENDFUNC




FUNC BOOL PLAY_SINGLE_LINE_FROM_CONVERSATION_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, STRING WhichRootLabel, STRING WhichSpecificLabel, enumConversationPriority PassedConversationPriority,
                                                enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)


           

    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 




    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
        
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF


    g_IsThisConversationForPhoneHolder = FALSE //this conversation is not a phonecall.

    g_IsThisAnMPChatCallHolder = FALSE

    g_IsThisAnMpJobOfferHolder = FALSE

    g_PlayingSingleLineHolder = TRUE

    g_IsThisFaceToFacePreloadedHolder = FALSE

    g_PlayingFromLineHolder = FALSE



    g_SpecificLabelHolder = WhichSpecificLabel


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif

    
    RETURN CheckCurrentConversationStatus (WhichRootLabel, PassedConversationPriority, FALSE)


ENDFUNC

 //Build a multipart conversation that consists of two lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_2_LINES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
                
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
                                                                                                            
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (2, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel)


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif




    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)




ENDFUNC







//Build a multipart conversation that consists of three lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_3_LINES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                            STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
        
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF
                                                                                                                
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (3, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel)


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)




ENDFUNC








//Build a multipart conversation that consists of four lines.
FUNC BOOL CREATE_MULTIPART_CONVERSATION_WITH_4_LINES_USING_V_CONTENT_IN_DLC (structPedsForConversation &YourPedStruct, STRING WhichBlockOfTextToLoad, 

                                            STRING FirstRootLabel, STRING FirstSpecificLabel,
                                            STRING SecondRootLabel, STRING SecondSpecificLabel,
                                            STRING ThirdRootLabel, STRING ThirdSpecificLabel,
                                            STRING FourthRootLabel, STRING FourthSpecificLabel,
                                                                       
                                            enumConversationPriority PassedConversationPriority,
                                            enumSubtitlesState ShouldDisplaySubtitles = DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen = DO_ADD_TO_BRIEF_SCREEN)



    
    
    
    Assign_Standard_Dialogue_Holder_Parameters (YourPedStruct, NO_CHARACTER, WhichBlockOfTextToLoad, ShouldDisplaySubtitles, ShouldAddToBriefScreen) 

    

    //Force priority to be no higher than medium so that critical phonecalls take precedence over this face to face conversation.
    IF PassedConversationPriority > CONV_PRIORITY_MEDIUM
    
#IF USE_TU_CHANGES
        IF PassedConversationPriority < CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT //Need to allow certain MP face to face conversations use this.
#ENDIF  //  USE_TU_CHANGES
            
            PassedConversationPriority = CONV_PRIORITY_MEDIUM
            
            #if IS_DEBUG_BUILD

                Passed_Priority_Cap_Warning()

            #endif

#IF USE_TU_CHANGES
        ENDIF
#ENDIF  //  USE_TU_CHANGES

    ENDIF

                                                                                                                
    
    STRING ConstructSegmentLabel[10]
    STRING ConstructSpecificLabel[10]


    Construct_Multipart_Conversation_Roots_and_Specifics (4, ConstructSegmentLabel, ConstructSpecificLabel,
                                                             FirstRootLabel, FirstSpecificLabel, SecondRootLabel, SecondSpecificLabel,
                                                             ThirdRootLabel, ThirdSpecificLabel, FourthRootLabel, FourthSpecificLabel)


    #if USE_TU_CHANGES
        g_Holder_ForceDLC_DialogueRequest = TRUE //Critical!
    #endif



    RETURN CheckCurrentConversationStatusForMultipart (ConstructSegmentLabel, ConstructSpecificLabel, PassedConversationPriority, FALSE)



ENDFUNC



