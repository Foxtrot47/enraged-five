
////////////////////////////////////////////////////////////
///    Public cheat control commands and enums
///    
///    Stephen Robertson LDS.

USING "rage_builtins.sch"
USING "globals.sch"

ENUM CHEATS_BITSET_ENUM

	CHEAT_TYPE_SUPER_JUMP = 0,		
	CHEAT_TYPE_SLIDEY_CARS,				
	CHEAT_TYPE_FAST_RUN,					
	CHEAT_TYPE_FAST_SWIM,					
	CHEAT_TYPE_GIVE_WEAPONS,				
	CHEAT_TYPE_ADVANCE_WEATHER,				
	CHEAT_TYPE_GIVE_HEALTH_ARMOR,				
	CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE,		
	CHEAT_TYPE_WANTED_LEVEL_UP,				
	CHEAT_TYPE_WANTED_LEVEL_DOWN,			
	CHEAT_TYPE_GIVE_PARACHUTE,					
	CHEAT_TYPE_BANG_BANG,				
	CHEAT_TYPE_FLAMING_BULLETS,			
	CHEAT_TYPE_EXPLOSIVE_MELEE,			
	CHEAT_TYPE_0_GRAVITY,				
	CHEAT_TYPE_INVINCIBILITY,			
	CHEAT_TYPE_SLOWMO,					
	CHEAT_TYPE_SKYFALL,					
	CHEAT_TYPE_DRUNK,					
	CHEAT_TYPE_AIM_SLOWMO,	
	CHEAT_TYPE_SPAWN_VEHICLE,	// Any vehicle spawn cheat
	CHEAT_TYPE_OFF_MISSION,		// Any cheat that can only be triggered off-mission
	CHEAT_TYPE_GENERAL,			// Any cheat that can be triggered at any time
	CHEAT_TYPE_ALL,				// All cheats
	CHEAT_TYPE_MAX
	
ENDENUM


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Returns a string for the cheat name
/// PARAMS:
///    eCheatEnum - The cheat to get the name for.
///    
FUNC STRING GET_CHEAT_NAME_FROM_ENUM( CHEATS_BITSET_ENUM eCheatEnum )

	SWITCH eCheatEnum
	
		CASE CHEAT_TYPE_SUPER_JUMP
			RETURN "SUPER JUMP"
		CASE CHEAT_TYPE_SLIDEY_CARS
			RETURN "SLIDEY CARS"
		CASE CHEAT_TYPE_FAST_RUN
			RETURN "FAST RUN"
		CASE CHEAT_TYPE_FAST_SWIM
			RETURN "FAST SWIM"
		CASE CHEAT_TYPE_GIVE_WEAPONS	
			RETURN "GIVE_WEAPONS"
		CASE CHEAT_TYPE_ADVANCE_WEATHER
			RETURN "ADVANCE WEATHER"
		CASE CHEAT_TYPE_GIVE_HEALTH_ARMOR
			RETURN "HEALTH & ARMOR"
		CASE CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE
			RETURN "ABILITY_RECHARGE"
		CASE CHEAT_TYPE_WANTED_LEVEL_UP		
			RETURN "WANTED UP"
		CASE CHEAT_TYPE_WANTED_LEVEL_DOWN	
			RETURN "WANTED DOWN"
		CASE CHEAT_TYPE_GIVE_PARACHUTE
			RETURN "GIVE PARACHUTE"
		CASE CHEAT_TYPE_BANG_BANG
			RETURN "BANG BANG"
		CASE CHEAT_TYPE_FLAMING_BULLETS
			RETURN "FLAMING BULLETS"
		CASE CHEAT_TYPE_EXPLOSIVE_MELEE
			RETURN "EXPLOSIVE MELEE"
		CASE CHEAT_TYPE_0_GRAVITY
			RETURN "0 GRAVITY"
		CASE CHEAT_TYPE_INVINCIBILITY
			RETURN "INVINCIBILITY"
		CASE CHEAT_TYPE_SLOWMO	
			RETURN "SLOWMO"
		CASE CHEAT_TYPE_SKYFALL
			RETURN "SKYFALL"
		CASE CHEAT_TYPE_DRUNK	
			RETURN "DRUNK"
		CASE CHEAT_TYPE_AIM_SLOWMO
			RETURN "AIM SLOWMO"
		CASE CHEAT_TYPE_SPAWN_VEHICLE
			RETURN "SPAWN VEHICLE"
		CASE CHEAT_TYPE_OFF_MISSION
			RETURN "OFF MISSION CHEATS"
		CASE CHEAT_TYPE_GENERAL
			RETURN "GENERAL CHEATS"
		CASE CHEAT_TYPE_ALL	
			RETURN "ALL CHEATS"
		
	ENDSWITCH
	
	RETURN "NULL"
	
ENDFUNC

#ENDIF

/// PURPOSE:
///    Triggers a cheat - NOTE This bypasses the cellphone onscreen check so should only be triggered via the phone cheat system!
/// PARAMS:
///    eCheatEnum - 
///    eVehicle - 
PROC TRIGGER_CHEAT( CHEATS_BITSET_ENUM eCheatEnum, MODEL_NAMES eVehicle = DUMMY_MODEL_FOR_SCRIPT )

	#IF IS_DEBUG_BUILD
		CPRINTLN( DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> TRIGGERED THIS CHEAT: ", GET_CHEAT_NAME_FROM_ENUM( eCheatEnum ))
	#ENDIF
	
	SET_BIT( g_iCheatTriggerBitset, ENUM_TO_INT(eCheatEnum) )
	g_eCheatVehicleModelName = eVehicle

ENDPROC

/// PURPOSE:
///    Disables / Enables the specified cheat
/// PARAMS:
///    eCheatEnum - The cheat to disable/enable
///    bDisable - TRUE to disable the cheat, FALSE to enable.
PROC DISABLE_CHEAT( CHEATS_BITSET_ENUM eCheatEnum, BOOL bDisable )

	IF bDisable
		
		#IF IS_DEBUG_BUILD
			CPRINTLN( DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> DISABLED THIS CHEAT: ", GET_CHEAT_NAME_FROM_ENUM( eCheatEnum ))
		#ENDIF
		SET_BIT( g_iBitsetCheatsCurrentlyDisabled, ENUM_TO_INT(eCheatEnum) )
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN( DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> ENABLED THIS CHEAT: ", GET_CHEAT_NAME_FROM_ENUM( eCheatEnum ))
		#ENDIF
		CLEAR_BIT( g_iBitsetCheatsCurrentlyDisabled, ENUM_TO_INT(eCheatEnum) )	
	ENDIF

ENDPROC

/// PURPOSE:
///    TRUE if a cheat is disabled this frame
/// PARAMS:
///    eCheatEnum - The cheat to check
/// RETURNS:
///    
FUNC BOOL IS_CHEAT_DISABLED( CHEATS_BITSET_ENUM eCheatEnum )

	IF IS_BIT_SET( g_iBitsetCheatsCurrentlyDisabled, ENUM_TO_INT(eCheatEnum))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Returns TRUE if the specified cheat is currently active
///    Note: Only cheats with a long duration will register this for any length of time.
/// PARAMS:
///    eCheatEnum - The cheat to check
/// RETURNS:
///    
FUNC BOOL IS_CHEAT_ACTIVE( CHEATS_BITSET_ENUM eCheatEnum )

	IF IS_BIT_SET( g_iBitsetCheatsCurrentlyActive, ENUM_TO_INT(eCheatEnum))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Returns TRUE if the specified cheat has been used this session.
///    A session is from when the cheats script starts until it shuts down.
/// PARAMS:
///    eCheatEnum - The cheat to be checked.
/// RETURNS:
///    
FUNC BOOL HAS_CHEAT_BEEN_USED_THIS_SESSION( CHEATS_BITSET_ENUM eCheatEnum )

	IF IS_BIT_SET( g_iBitsetCheatsUsedThisSession, ENUM_TO_INT(eCheatEnum))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

