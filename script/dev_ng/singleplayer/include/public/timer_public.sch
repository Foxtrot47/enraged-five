USING "timer_private.sch"

/// PURPOSE:
///		Returns TRUE if TIMER t has been started. 
/// PARAMS:
///		t - Timer.
/// RETURNS:
///		TRUE if TIMER t has been started. 
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC BOOL IS_TIMER_STARTED(structtimer &t)
	RETURN IS_BIT_SET( t.TimerBits, ENUM_TO_INT( TIMER_STARTED ) )
ENDFUNC

/// PURPOSE:
///    Returns TRUE if TIMER t has been paused. 
/// PARAMS:
///    t - Timer.
/// RETURNS:
///    TRUE if TIMER t has been paused. 
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC BOOL IS_TIMER_PAUSED(structTimer &t)
	RETURN IS_BIT_SET( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
ENDFUNC

/// PURPOSE:
///		Cancels/Stops TIMER t.  Note: TIMER_DO_WHEN_READY and TIMER_DO_ONCE_WHEN_READY can be called on a CANCELLED TIMER.
/// PARAMS:
///		t - Timer.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC CANCEL_TIMER(structTimer &t)
	t.StartTime = 0.0
	t.PauseTime = 0.0
	t.TimerBits = 0
ENDPROC

/// PURPOSE:
///    Sets the given timer to use either GET_GAME_TIMER or GET_NETWORK_TIME when online.
///    This has no effect on offline use.
/// PARAMS:
///    t - the timer
///    bForceUseGameTimer - TRUE if you want to use GET_GAME_TIMER all the time, FALSE if you want the standard functionality where it uses GET_NETWORK_TIME when online.
PROC FORCE_TIMER_TO_USE_GAME_TIME(structtimer &t, BOOL bForceUseGameTimer)
	IF bForceUseGameTimer
		SET_BIT(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))
		CDEBUG1LN(DEBUG_SYSTEM, "FORCE_TIMER_TO_USE_GAME_TIME set to TRUE")
	ELSE
		CLEAR_BIT(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))
		CDEBUG1LN(DEBUG_SYSTEM, "FORCE_TIMER_TO_USE_GAME_TIME set to FALSE")
	ENDIF
	
	IF IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))
		PRINTLN("[TimerBroken] SET")
	ELSE
		PRINTLN("[TimerBroken] NOT SET")
	ENDIF
ENDPROC

// *******************************************************************************************
//	SET TIMER PROCEDURES
// *******************************************************************************************

/// PURPOSE:
///		
/// PARAMS:
///		t - Timer.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC RESTART_TIMER_AT(structTimer &t, FLOAT Start_Time)
	t.StartTime = GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) - Start_Time
	
	SET_BIT( t.TimerBits, ENUM_TO_INT( TIMER_STARTED ) )
	CLEAR_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
	t.PauseTime = 0.0
ENDPROC

/// PURPOSE:
///    Starts TIMER t at 0.0 seconds. Completely ingnorant of whether the timer has already been started / paused.
/// PARAMS:
///    t - Timer.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC RESTART_TIMER_NOW(structTimer &t)
	RESTART_TIMER_AT( t, 0.0 )
ENDPROC

/// PURPOSE:
///    Starts TIMER t at Start_Time seconds. Ignores command if TIMER is already started.
/// PARAMS:
///    t - Timer.
///    Start_Time - seconds to set the timer to start before now.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC START_TIMER_AT(structTimer &t, FLOAT Start_Time)
	IF NOT IS_TIMER_STARTED(t)
		RESTART_TIMER_AT( t, Start_Time )
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts TIMER t at 0.0 seconds. Ignores command if TIMER is already started.
/// PARAMS:
///    t - Timer.
///    Start_Time - seconds to set the timer to start before now.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC START_TIMER_NOW(structTimer &t)
	IF NOT IS_TIMER_STARTED(t)
		RESTART_TIMER_NOW( t )
	ENDIF
ENDPROC

/// PURPOSE:
///		
/// PARAMS:
///		t - Timer.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC START_TIMER_NOW_SAFE(structTimer &t)
	IF NOT IS_TIMER_STARTED(t)
		START_TIMER_NOW(t)
	ELSE
		RESTART_TIMER_NOW(t)
	ENDIF
ENDPROC

/// PURPOSE:
///		Returns value of TIMER t in seconds. Throws an error if the timer has not been started.
/// PARAMS:
///		t - Timer.
/// RETURNS:
///		
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC FLOAT GET_TIMER_IN_SECONDS(structTimer &t)
	PRINTLN("[TimerBroken] GET_TIMER_IN_SECONDS" )
	IF IS_TIMER_STARTED(t)
		IF IS_TIMER_PAUSED(t)
			PRINTLN("[TimerBroken] IS_TIMER_PAUSED ", t.PauseTime )
			RETURN t.PauseTime
		ELSE
			PRINTLN("[TimerBroken] GetGameTimerSeconds ", t.StartTime )
			RETURN (GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) - t.StartTime)
		ENDIF
	ELSE
		PRINTLN("[TimerBroken] IS_TIMER_STARTED : NO" )
		SCRIPT_ASSERT("ERROR: Trying to get value of TIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to get value of TIMER that has not been started! Returning 0.0 ")PRINTNL()
		RETURN t.StartTime
	ENDIF
ENDFUNC

/// PURPOSE:
///		Returns value of TIMER t in seconds, WITHOUT throwing an assert if the timer is not started.
/// PARAMS:
///		t - Timer.
/// RETURNS:
///		
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC FLOAT GET_TIMER_IN_SECONDS_SAFE(structTimer &t)
	IF IS_TIMER_STARTED(t)
		IF IS_TIMER_PAUSED(t)
			RETURN t.PauseTime
		ELSE
			RETURN (GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) - t.StartTime)
		ENDIF
	ELSE
		RETURN 0.0
	ENDIF
ENDFUNC

// *******************************************************************************************
//	SET TIMER FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    Returns TRUE when TIMER t is larger than timeToWait seconds.
///    This can be called with an unstarted/cancelled TIMER.
/// PARAMS:
///    t - Timer.
///    timeToWait - seconds to wait for the timer to be ready
/// RETURNS:
///		
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
FUNC BOOL TIMER_DO_WHEN_READY(structTimer &t, FLOAT timeToWait)
	IF IS_TIMER_STARTED(t)
		IF GET_TIMER_IN_SECONDS(t) > timeToWait
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE when TIMER t is larger than timeToWait seconds, and then CANCELs/stops the timer.
///    This is effectively like a Timer Oneshot! Although if Timer t is re-started, then this could become true again.
///    This can be called with an unstarted/cancelled TIMER.
/// PARAMS:
///    t - Timer.
///    timeToWait - seconds to wait for the timer to be ready
/// RETURNS:
///		
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
FUNC BOOL TIMER_DO_ONCE_WHEN_READY(structTimer &t, FLOAT timeToWait)
	IF TIMER_DO_WHEN_READY(t, timeToWait)
		CANCEL_TIMER(t)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	PAUSE TIMER PROCEDURES
// *******************************************************************************************

/// PURPOSE:
///		Pauses Timer t. Throws an error if the Timer has not been started.
/// PARAMS:
///		t - Timer.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC PAUSE_TIMER(structTimer &t)
	PRINTLN("[TimerBroken] PAUSE_TIMER")
	IF IS_TIMER_STARTED(t)
		IF NOT IS_TIMER_PAUSED(t)
			t.PauseTime = GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) - t.StartTime
			SET_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
			PRINTLN("[TimerBroken] PAUSE_TIMER ", t.PauseTime)
		ENDIF
	ELSE
		SCRIPT_ASSERT("ERROR: Trying to PAUSE a TIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to PAUSE a TIMER that has not been started! IGNORING COMMAND.")PRINTNL()
	ENDIF
ENDPROC

/// PURPOSE:
///		Unpauses Timer t. Throws an error if the Timer has not been started.
/// PARAMS:
///		t - Timer.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC UNPAUSE_TIMER(structTimer &t)
	IF IS_TIMER_STARTED(t)
		IF IS_TIMER_PAUSED(t)
			t.StartTime = GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) - t.PauseTime
			t.PauseTime = 0.0
			CLEAR_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
		ENDIF
	ELSE
		SCRIPT_ASSERT("ERROR: Trying to UNPAUSE a TIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to UNPAUSE a TIMER that has not been started! IGNORING COMMAND.")PRINTNL()	
	ENDIF
ENDPROC

/// Purpose:
///    Adjust the time of a running timer instantly
/// PARAMS:
///    t - Timer.  offset - Offset float.
/// AUTHOR: troy.schram@rockstarsandiego.com
PROC ADJUST_TIMER(structTimer &t, FLOAT offset)
	PRINTLN("[TimerBroken] ADJUST_TIMER ", offset)
	IF IS_TIMER_STARTED(t)
		RESTART_TIMER_AT( t, (GET_TIMER_IN_SECONDS(t) + offset))
	ELSE
		PRINTSTRING("WARNING: Trying to ADJUST a TIMER that has not been started! IGNORING COMMAND.")PRINTNL()	
	ENDIF
ENDPROC



/// PURPOSE:
///		Returns TRUE if COUNTDOWNTIMER t has been started. 
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// RETURNS:
///		TRUE if COUNTDOWNTIMER t has been started. 
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC BOOL IS_COUNTDOWNTIMER_STARTED(structTimer &t)
	RETURN IS_TIMER_STARTED(t)
ENDFUNC

/// PURPOSE:
///    Returns TRUE if COUNTDOWNTIMER t has been paused. 
/// PARAMS:
///    t - COUNTDOWNTIMER.
/// RETURNS:
///    TRUE if COUNTDOWNTIMER t has been paused. 
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC BOOL IS_COUNTDOWNTIMER_PAUSED(structTimer &t)
	RETURN IS_TIMER_PAUSED(t)
ENDFUNC

/// PURPOSE:
///		Cancels/Stops COUNTDOWNTIMER t.  Note: COUNTDOWNTIMER_DO_WHEN_READY and COUNTDOWNTIMER_DO_ONCE_WHEN_READY can be called on a CANCELLED COUNTDOWNTIMER.
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC CANCEL_COUNTDOWNTIMER(structTimer &t)
	CANCEL_TIMER(t)
ENDPROC

// *******************************************************************************************
//	SET COUNTDOWNTIMER PROCEDURES
// *******************************************************************************************

/// PURPOSE:
///		
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC RESTART_COUNTDOWNTIMER_AT(structTimer &t, FLOAT Start_Time)
	
//	PRINTSTRING(" * restart_countdowntimer_at(")
//	PRINTFLOAT(GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))))
//	PRINTSTRING(" + ")
//	PRINTFLOAT(Start_Time)
//	PRINTSTRING(") = ")
//	PRINTFLOAT(GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) + Start_Time)
//	PRINTNL()
	
	t.StartTime = GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))) + Start_Time
	
	SET_BIT( t.TimerBits, ENUM_TO_INT( TIMER_STARTED ) )
	CLEAR_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
	t.PauseTime = 0.0
ENDPROC

/// PURPOSE:
///    Starts COUNTDOWNTIMER t at 0.0 seconds. Completely ingnorant of whether the COUNTDOWNTIMER has already been started / paused.
/// PARAMS:
///    t - COUNTDOWNTIMER.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC RESTART_COUNTDOWNTIMER_NOW(structTimer &t)
	RESTART_COUNTDOWNTIMER_AT( t, 0.0 )
ENDPROC

/// PURPOSE:
///    Starts COUNTDOWNTIMER t at Start_Time seconds. Ignores command if COUNTDOWNTIMER is already started.
/// PARAMS:
///    t - COUNTDOWNTIMER.
///    Start_Time - seconds to set the COUNTDOWNTIMER to start before now.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC START_COUNTDOWNTIMER_AT(structTimer &t, FLOAT Start_Time)
	IF NOT IS_COUNTDOWNTIMER_STARTED(t)
		RESTART_COUNTDOWNTIMER_AT( t, Start_Time )
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts COUNTDOWNTIMER t at 0.0 seconds. Ignores command if COUNTDOWNTIMER is already started.
/// PARAMS:
///    t - COUNTDOWNTIMER.
///    Start_Time - seconds to set the COUNTDOWNTIMER to start before now.
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
PROC START_COUNTDOWNTIMER_NOW(structTimer &t)
	IF NOT IS_COUNTDOWNTIMER_STARTED(t)
		RESTART_COUNTDOWNTIMER_NOW( t )
	ENDIF
ENDPROC

/// PURPOSE:
///		
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC START_COUNTDOWNTIMER_NOW_SAFE(structTimer &t)
	IF NOT IS_COUNTDOWNTIMER_STARTED(t)
		START_COUNTDOWNTIMER_NOW(t)
	ELSE
		RESTART_COUNTDOWNTIMER_NOW(t)
	ENDIF
ENDPROC

/// PURPOSE:
///		Returns value of COUNTDOWNTIMER t in seconds. Throws an error if the COUNTDOWNTIMER has not been started.
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// RETURNS:
///		
/// AUTHOR:
///    alwyn.roberts@rockstarnorth.com
FUNC FLOAT GET_COUNTDOWNTIMER_IN_SECONDS(structTimer &t)
	IF IS_COUNTDOWNTIMER_STARTED(t)
		IF IS_COUNTDOWNTIMER_PAUSED(t)
			RETURN t.PauseTime
		ELSE
			
//			PRINTSTRING(" ** get_countdowntimer_in_seconds(")
//			PRINTFLOAT(t.StartTime)
//			PRINTSTRING(" - ")
//			PRINTFLOAT(GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))))
//			PRINTSTRING(") = ")
//			PRINTFLOAT(t.StartTime - GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))))
//			PRINTNL()
			
			RETURN (t.StartTime - GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER))))
		ENDIF
	ELSE
		SCRIPT_ASSERT("ERROR: Trying to get value of COUNTDOWNTIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to get value of COUNTDOWNTIMER that has not been started! Returning 0.0 ")PRINTNL()
		RETURN t.StartTime
	ENDIF
ENDFUNC

// *******************************************************************************************
//	SET COUNTDOWNTIMER FUNCTIONS
// *******************************************************************************************

/// PURPOSE:
///    Returns TRUE when COUNTDOWNTIMER t is less then than timeToWait seconds.
///    This can be called with an unstarted/cancelled COUNTDOWNTIMER.
/// PARAMS:
///    t - COUNTDOWNTIMER.
///    timeToWait - seconds to wait for the COUNTDOWNTIMER to be ready
/// RETURNS:
///		
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
FUNC BOOL COUNTDOWNTIMER_DO_WHEN_READY(structTimer &t, FLOAT timeToWait = 0.0)
	IF IS_COUNTDOWNTIMER_STARTED(t)
		IF GET_COUNTDOWNTIMER_IN_SECONDS(t) < timeToWait
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns TRUE when COUNTDOWNTIMER t is larger than timeToWait seconds, and then CANCELs/stops the COUNTDOWNTIMER.
///    This is effectively like a COUNTDOWNTIMER Oneshot! Although if COUNTDOWNTIMER t is re-started, then this could become true again.
///    This can be called with an unstarted/cancelled COUNTDOWNTIMER.
/// PARAMS:
///    t - COUNTDOWNTIMER.
///    timeToWait - seconds to wait for the COUNTDOWNTIMER to be ready
/// RETURNS:
///		
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
FUNC BOOL COUNTDOWNTIMER_DO_ONCE_WHEN_READY(structTimer &t, FLOAT timeToWait = 0.0)
	IF COUNTDOWNTIMER_DO_WHEN_READY(t, timeToWait)
		CANCEL_COUNTDOWNTIMER(t)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	PAUSE COUNTDOWNTIMER PROCEDURES
// *******************************************************************************************

/// PURPOSE:
///		Pauses COUNTDOWNTIMER t. Throws an error if the COUNTDOWNTIMER has not been started.
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC PAUSE_COUNTDOWNTIMER(structTimer &t)
	IF IS_COUNTDOWNTIMER_STARTED(t)
		IF NOT IS_COUNTDOWNTIMER_PAUSED(t)
			t.PauseTime = t.StartTime - GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER)))
			SET_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
		ENDIF
	ELSE
		SCRIPT_ASSERT("ERROR: Trying to PAUSE a COUNTDOWNTIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to PAUSE a COUNTDOWNTIMER that has not been started! IGNORING COMMAND.")PRINTNL()
	ENDIF
ENDPROC

/// PURPOSE:
///		Unpauses COUNTDOWNTIMER t. Throws an error if the COUNTDOWNTIMER has not been started.
/// PARAMS:
///		t - COUNTDOWNTIMER.
/// AUTHOR:	alwyn.roberts@rockstarnorth.com
PROC UNPAUSE_COUNTDOWNTIMER(structTimer &t)
	IF IS_COUNTDOWNTIMER_STARTED(t)
		IF IS_COUNTDOWNTIMER_PAUSED(t)
			t.StartTime = t.PauseTime - GetGameTimerSeconds(IS_BIT_SET(t.TimerBits, ENUM_TO_INT(TIMER_FORCE_GAME_TIMER)))
			t.PauseTime = 0.0
			CLEAR_BIT( t.TimerBits, ENUM_TO_INT( TIMER_PAUSED ) )
		ENDIF
	ELSE
		SCRIPT_ASSERT("ERROR: Trying to UNPAUSE a COUNTDOWNTIMER that has not been started!")
		PRINTSTRING("WARNING: Trying to UNPAUSE a COUNTDOWNTIMER that has not been started! IGNORING COMMAND.")PRINTNL()	
	ENDIF
ENDPROC
