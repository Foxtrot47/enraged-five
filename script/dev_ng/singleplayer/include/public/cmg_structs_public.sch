USING "rage_builtins.sch"
USING "globals.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_structs_public.sch
//	Sam Hackett
//
//	This mirrors the bob file: minigame_handler_structs.sch
//
//	"CMG_TYPE" is our version of bob's "MINIGAME_TYPES" enum.
//	These are the datatypes.
//
//	See also:
//		cmg_support_public.sch
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// These types must match BOB generic_minigame versions


ENUM CMG_TYPE
	CMG_TYPE_Blackjack = 0,
	CMG_TYPE_Poker,
	CMG_TYPE_Roulette,
	CMG_TYPE_Slot,
	CMG_TYPE_Dancing,
	
	CMG_TYPE_MAX,
	CMG_TYPE_INVALID
ENDENUM

STRUCT CLIFFORD_MG_LAUNCH_PARAMS
	CMG_TYPE			minigameType
	VECTOR				vLaunchLocation
	FLOAT				fBuyinAmountDollars
	INT					iMultiplayerInstanceID
ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
