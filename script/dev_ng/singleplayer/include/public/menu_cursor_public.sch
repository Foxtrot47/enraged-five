//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   menu_cursor_public.sch                                   					//
//      AUTHOR          :   Stephen Robertson                                              				//
//      DESCRIPTION     :   Common functions for handling mouse cursors in scripted menus				//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "menu_globals_tu.sch"

USING "commands_hud.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_debug.sch"
USING "commands_graphics.sch"

CONST_FLOAT PI_MOUSE_CAMERA_TRIGGER_SIZE_LR	0.05
CONST_FLOAT PI_MOUSE_CAMERA_TRIGGER_SIZE_UD	0.03
CONST_FLOAT PI_MOUSE_CAMERA_ROTATION_SPEED	70.0

CONST_FLOAT ASPECT_16_9 1.7777777

ENUM MOUSE_SLIDER_TYPE

	MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_OPACITY,
	MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_FAMILY_RESEMBLANCE,
	MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_SKIN_TONE,
	MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_STATS,
	MOUSE_SLIDER_TYPE_HAIRDO_SHOP_OPACITY

ENDENUM

BOOL bMouseVisible = FALSE
BOOL bScriptCursorStyleControl = FALSE


#IF IS_DEBUG_BUILD
	
	BOOL	bCursorMenuSetup					= FALSE
	BOOL	bDrawCursorMenuDebugLines			= FALSE 
	BOOL	bCursorMenuWidgetsCreated			= FALSE
		
	// Basic widget struct

	WIDGET_GROUP_ID debugWidgetGroupID
	FLOAT			debugWidgetMenuXOrigin
	FLOAT			debugWidgetMenuYOrigin
	FLOAT			debugWidgetMenuItemWidth
	FLOAT			debugWidgetMenuItemHeight
	INT				debugWidgetMenuItemNum

	
	/// PURPOSE: Create  widget for cheats
	PROC SETUP_CURSOR_MENU_DEBUG_WIDGETS()
	
		debugWidgetMenuXOrigin = 0.1
		debugWidgetMenuYOrigin = 0.1
		debugWidgetMenuItemWidth = 0.1
		debugWidgetMenuItemHeight = 0.05
		debugWidgetMenuItemNum = 10

		debugWidgetGroupID = START_WIDGET_GROUP("Menu cursor conversion helper")
		ADD_WIDGET_FLOAT_SLIDER("Menu X Origin", debugWidgetMenuXOrigin, 0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Menu Y Origin", debugWidgetMenuYOrigin, 0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Menu Item Width", debugWidgetMenuItemWidth, 0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Menu Item Height", debugWidgetMenuItemHeight, 0, 1.0, 0.001)
		ADD_WIDGET_INT_SLIDER("Number of menu items", debugWidgetMenuItemNum, 1, 20, 1)
		
		// Stop main widget group
		STOP_WIDGET_GROUP()
		
		bCursorMenuWidgetsCreated = TRUE
		
	ENDPROC
	
	PROC CLEANUP_CURSOR_MENU_WIDGETS()
	
		IF DOES_WIDGET_GROUP_EXIST(debugWidgetGroupID)
			DELETE_WIDGET_GROUP(debugWidgetGroupID)
		ENDIF

	ENDPROC
	
	
	PROC DEBUG_CREATE_HEIST_HIT_AREA( VECTOR vPos, FLOAT fHeading, VECTOR vSize )
	
		VECTOR vMin
		VECTOR vMax
		
		VECTOR vTopLeft
		VECTOR vTopRight
		VECTOR vBottomRight
		VECTOR vBottomLeft
		
		IF NOT bCursorMenuWidgetsCreated
			SETUP_CURSOR_MENU_DEBUG_WIDGETS()
		ENDIF
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
		vSize.x = vSize.x * 0.60
		
		vMin.x = vSize.x * debugWidgetMenuXOrigin
		vMin.y = vSize.y * debugWidgetMenuYOrigin
		
		vMax.x = vMin.x + (vSize.x * debugWidgetMenuItemWidth)
		vMax.y = vMin.y + (vSize.y * debugWidgetMenuItemHeight)		
		
		vTopLeft =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<-vMin.x, 0.0, -vMin.y >> )
		vTopRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<-vMax.x, 0.0, -vMin.y >> )
		vBottomRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, <<-vMax.x,0.0 ,-vMax.y>>)
		vBottomLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << -vMin.x, 0.0, -vMax.y>> )
		
		DRAW_DEBUG_LINE( vTopLeft, vTopRight, 255,0,0)
		DRAW_DEBUG_LINE( vTopRight, vBottomRight, 0,255,0)
		DRAW_DEBUG_LINE( vBottomRight, vBottomLeft, 255,255,0)
		DRAW_DEBUG_LINE( vBottomLeft, vTopLeft, 128,255,0)
		
		
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.1, "NUMBER", vTopLeft.x, 2 )
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.2, 0.1, "NUMBER", vTopLeft.z, 2 )
		
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.1,0.2, "NUMBER", vPos.x, 2 )
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.2,0.2, "NUMBER", vPos.z, 2 )
		
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.1, 0.4, "NUMBER", vMin.x, 2 )
		SET_TEXT_SCALE(0.5, 0.5)
		DISPLAY_TEXT_WITH_FLOAT( 0.2, 0.4, "NUMBER", vMin.y, 2 )
		
		//DRAW_DEBUG_SPHERE( vTopLeft, 0.1, 255, 0, 0 )
		//DRAW_DEBUG_SPHERE( vTopRight, 0.1, 0, 255, 0 )
		//DRAW_DEBUG_SPHERE( vBottomRight, 0.1, 0, 0, 255 )
		//DRAW_DEBUG_SPHERE( vBottomLeft, 0.1, 255, 255, 255 )
	
	ENDPROC
	
#ENDIF

PROC TURN_MOUSE_CURSOR_OFF(BOOL bForceOff = FALSE)

	IF bMouseVisible = TRUE
	OR bForceOff
		IF IS_PC_VERSION()
			SET_MOUSE_CURSOR_VISIBLE(FALSE)
			bMouseVisible = FALSE
		ENDIF
	ENDIF

ENDPROC


PROC TURN_MOUSE_CURSOR_ON()
	
	IF bMouseVisible = FALSE
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			SET_MOUSE_CURSOR_VISIBLE(TRUE)
			bMouseVisible = TRUE
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept in menus where cellphone controls are used, e.g. PI Menu.
/// RETURNS:
///    TRUE if only the cellphone accept button has been pressed.
///    
FUNC BOOL IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
//		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_)
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		
			RETURN TRUE

		ENDIF
	
	ELSE
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept in menus where cellphone controls are used, e.g. PI Menu.
/// RETURNS:
///    TRUE if only the cellphone accept button has been pressed.
///    
FUNC BOOL IS_CELLPHONE_ACCEPT_PRESSED_EXCLUDING_MOUSE()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
		AND NOT IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND NOT IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
//		AND NOT IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_)
		
			RETURN TRUE

		ENDIF
	
	ELSE
	
		IF IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept in menus where cellphone controls are used, e.g. PI Menu.
/// RETURNS:
///    TRUE if only the cellphone accept button has been pressed.
///    
FUNC BOOL IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
//		AND NOT IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_)
		
			RETURN TRUE

		ENDIF
	
	ELSE
	
		IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept in menus where cellphone controls are used, e.g. PI Menu.
/// RETURNS:
///    TRUE if only the cellphone cancel button has been released.
///    
FUNC BOOL IS_CELLPHONE_CANCEL_JUST_RELEASED_EXCLUDING_MOUSE()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
		AND NOT IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND NOT IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
//		AND NOT IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_)
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		
			RETURN TRUE

		ENDIF
	
	ELSE
	
		IF IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept in menus where cellphone controls are used, e.g. PI Menu.
/// RETURNS:
///    TRUE if only the cellphone accept button has been pressed.
///    
FUNC BOOL IS_CELLPHONE_ACCEPT_ONLY_JUST_RELEASED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF NOT IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		
			RETURN TRUE

		ENDIF
	
	ELSE
	
		IF IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept.
/// RETURNS:
///    TRUE if the cursor accept button has been pressed.
FUNC BOOL IS_CURSOR_ACCEPT_ONLY_JUST_RELEASED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		//AND IS_DISABLED_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
			RETURN TRUE
		ENDIF
	
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept.
/// RETURNS:
///    TRUE if the cursor accept button has held
FUNC BOOL IS_CURSOR_ACCEPT_HELD()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets around a conflict between cellphone accept and  mouse accept.
/// RETURNS:
///    TRUE if the cursor accept button has held
FUNC BOOL IS_CURSOR_ACCEPT_JUST_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Updates the menu mouse globals so the mouse coords and mouse movement distance update.
///   
PROC UPDATE_MENU_CURSOR_GLOBALS()
	
	g_fMenuCursorXPrev = g_fMenuCursorX
	g_fMenuCursorYPrev = g_fMenuCursorY
	
	g_fMenuCursorX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	g_fMenuCursorY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	g_fMenuCursorXMoveDistance = g_fMenuCursorX - g_fMenuCursorXPrev
	g_fMenuCursorYMoveDistance = g_fMenuCursorY - g_fMenuCursorYPrev
			
ENDPROC

PROC RESET_MOUSE_POINTER_FOR_PAUSE_OR_ALERT()

	IF IS_PC_VERSION()
	
		IF IS_PAUSE_MENU_ACTIVE()
		OR IS_WARNING_MESSAGE_ACTIVE()
			IF bScriptCursorStyleControl = TRUE
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
				bScriptCursorStyleControl = FALSE
			ENDIF
		ELSE
			bScriptCursorStyleControl = TRUE
		ENDIF
	
	ENDIF
	
ENDPROC



/// PURPOSE:
///    Handles changing the menu cursor style, depending on the menu results and if camera movement is allowed
/// PARAMS:
///    bAllowCamera - TRUE if the cursor changes to allow camera movement.
/// RETURNS:
///    TRUE if ACCEPT is being pressed whilst in camera grab mode.
///    
FUNC BOOL HANDLE_MENU_CURSOR(BOOL bAllowDragCamera = TRUE, INT iCurrentItem = -1, BOOL bDisplayCursor = TRUE)

	UNUSED_PARAMETER(iCurrentItem)
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_WARNING_MESSAGE_ACTIVE()
		RETURN FALSE
	ENDIF
	
	// B* 2577444 - Added a param to allow scripts to display or cursor, so the carmod shop can hide the cursor when moving the cam in fps cam.
	// As this defaults to show the cursor it should work exactly the same as before if the param isn't specified.
	IF bDisplayCursor
		SET_MOUSE_CURSOR_THIS_FRAME()
	ENDIF
	
	// If currently doing camera drag...
	IF g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM

		// Set grabing-hand cursor
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_GRAB)				// Mid-drag: Grabbing hand

		// Check if drag has ended
		IF bAllowDragCamera AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN TRUE
		ELSE
			g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
			RETURN FALSE
		ENDIF
	
	// If can click on current item...
	ELIF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
	OR g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
	OR g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
	OR IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()

		// Set default/middle-finger cursor
//		IF g_iMenuCursorItem = iCurrentItem
//			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)	// Can click through: Middle finger
//		ELSE
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)				// Default: normal arrow
//		ENDIF
		
		RETURN FALSE
		
	// If can start drag...
	ELIF g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
	AND bAllowDragCamera
	
		// Check if drag has started
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_GRAB)			// Drag just started: Grabbing hand
			g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM
			RETURN TRUE
		ELSE
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_OPEN)			// Drag can start: Open hand
			RETURN FALSE
		ENDIF
		
	// Otherwise, can't do anything...
	ELSE
		
//		// Set dimmed cursor
//		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_DIMMED)				// Can't click anything: Dimmed arrow
//		RETURN FALSE

		// We're trying out using the non-dimmed cursor here too, rather than the dimmed one
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)					// Can't click anything: Normal arrow
		RETURN FALSE

	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Handles camera movement when using mouse and keyboard in the PI menus
FUNC BOOL HANDLE_PI_MENU_MOUSE_CAMERA(BOOL bUseDragCam, BOOL &bFirstTime )

	FLOAT fAmount
	FLOAT fValue
	
	// Inventory - has a shop style grab camera in face-cam
	IF bUseDragCam
	
		HANDLE_MENU_CURSOR(TRUE)
		
		IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) 		// Need this to prevent conflicts between the mouse and cursor accept buttons.
			IF g_fMenuCursorX < 1.0
				RETURN TRUE
			ENDIF
		ENDIF
	
	ELSE
	
		//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fAmount, 3 )
			
		// Main menu has a left/right push camera.
		
		HANDLE_MENU_CURSOR(FALSE)
		
		// Prevents dodgy camera rotations if the cursor starts off at the edges when the menu initialises
		IF bFirstTime
		
			IF g_fMenuCursorX > (1.0 -PI_MOUSE_CAMERA_TRIGGER_SIZE_LR)
			OR g_fMenuCursorX < PI_MOUSE_CAMERA_TRIGGER_SIZE_LR
			OR g_fMenuCursorY > (1.0 - PI_MOUSE_CAMERA_TRIGGER_SIZE_UD)
			OR g_fMenuCursorY < PI_MOUSE_CAMERA_TRIGGER_SIZE_UD
				RETURN FALSE
			ELSE
				bFirstTime = FALSE
			ENDIF
	
		ENDIF
		
		// Fix for B* 2480157 - Disable mouse camera movement if ped is in a vehicle, as if the vehicle is tilted the mouse camera goes crazy.
		// SET_GAMEPLAY_CAM_RELATIVE_HEADING gives completely different values if the ped is upside down.
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
		ENDIF

		IF g_iMenuCursorItem = MENU_CURSOR_NO_ITEM
		OR g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE	// Note - "NO_CAMERA_MOVE" refers to blocking the drag camera, not the push camera
		
			// Horizontal mouse rotation
			IF g_fMenuCursorX > (1.0 - (PI_MOUSE_CAMERA_TRIGGER_SIZE_LR * 0.75))
			
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_RIGHT)

				fAmount = PI_MOUSE_CAMERA_TRIGGER_SIZE_LR - (1.0 -  g_fMenuCursorX)
				
				IF fAmount > PI_MOUSE_CAMERA_TRIGGER_SIZE_LR
					fAmount = PI_MOUSE_CAMERA_TRIGGER_SIZE_LR
				ENDIF
				
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fAmount, 3 )
			
				fValue = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.2, "NUMBER", fValue, 3 )
				
				fValue = fValue - (PI_MOUSE_CAMERA_ROTATION_SPEED * fAmount)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(fValue)
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.3, "NUMBER", fValue, 3 )
			
			ELIF g_fMenuCursorX < (PI_MOUSE_CAMERA_TRIGGER_SIZE_LR * 0.75)
			
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_LEFT)

				fAmount = (PI_MOUSE_CAMERA_TRIGGER_SIZE_LR - g_fMenuCursorX)
				
				IF fAmount > PI_MOUSE_CAMERA_TRIGGER_SIZE_LR
					fAmount = PI_MOUSE_CAMERA_TRIGGER_SIZE_LR
				ENDIF
				
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fAmount, 3 )
			
				fValue = GET_GAMEPLAY_CAM_RELATIVE_HEADING()
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.2, "NUMBER", fValue, 3 )
				fValue = fValue + (PI_MOUSE_CAMERA_ROTATION_SPEED * fAmount)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(fValue)
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.3, "NUMBER", fValue, 3 )
				
			// For some reason pitch doesn't work. 
			ELIF g_fMenuCursorY > (1.0 - (PI_MOUSE_CAMERA_TRIGGER_SIZE_UD * 0.75))
			
				//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_UP)

				//fValue = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fValue, 3 )
				//fValue = fValue + 0.0001
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(fValue)
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.2, "NUMBER", fValue, 3 )
			
			ELIF g_fMenuCursorY < (PI_MOUSE_CAMERA_TRIGGER_SIZE_UD * 0.75)
			
				//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_DOWN)

				//fValue = GET_GAMEPLAY_CAM_RELATIVE_PITCH()
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.1, "NUMBER", fValue, 3 )
				
				//fValue = fValue - 0.00001
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(fValue)
				//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.2, "NUMBER", fValue, 3 )
				
			ENDIF
		
		ENDIF
			
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Generic function to handle non-shop script based menus. Locates a menu on screen and defines its extents and number of elements.
///    IMPORTANT: Setting up requires default safe-zone size 0.9 to be set in the settings.
/// PARAMS:
///    fMenuXOrigin - 			X Origin of menu
///    fMenuYOrigin - 			Y Origin of menu
///    fMenuItemWidth - 		Width of menu
///    fMenuItemHeight - 		Height of menu item
///    fMenuItemSelectHeight - 	Height of menu item selection
///    iMenuItemNum - 			Number of menu items displayed in the menu.
/// RETURNS:
///    Index of higlighted menu item (Starting at 0 for the top item)
FUNC INT GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU( FLOAT fMenuXOrigin, FLOAT fMenuYOrigin, FLOAT fMenuItemWidth, FLOAT fMenuItemHeight, FLOAT fMenuItemSelectHeight, INT iMenuItemNum, INT iAlpha = 128, BOOL bAdjustForSafeZone = FALSE, BOOL bAdjustForWidescreen = TRUE )

	FLOAT fMenuXMax
	FLOAT fMenuYMax
	
	FLOAT fMouseX
	FLOAT fMouseY
	
	FLOAT fCursorYMenuCoord
	
	FLOAT fMenuXOriginUnadjusted
	FLOAT fMenuYOriginUnadjusted
	
	INT iHighlightedItem = ENUM_TO_INT(MENU_CURSOR_NO_ITEM)
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN MENU_CURSOR_NO_ITEM
	ENDIF
	
	// Disable alternate pause which is bound to Esc, so we can use Esc to go back.
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	
	SET_MOUSE_CURSOR_THIS_FRAME()
	
	#IF IS_DEBUG_BUILD
	IF bCursorMenuSetup
		fMenuXOrigin = debugWidgetMenuXOrigin
		fMenuYOrigin = debugWidgetMenuYOrigin
		fMenuItemWidth = debugWidgetMenuItemWidth		
		fMenuItemHeight = debugWidgetMenuItemHeight
		iMenuItemNum = debugWidgetMenuItemNum
	ENDIF
	#ENDIF
	
	// Adjust for 4:3
	IF bAdjustForWidescreen
		IF NOT GET_IS_WIDESCREEN()
			fMenuXOrigin = ((fMenuXOrigin - 0.5) * 1.33) + 0.5
			fMenuItemWidth = (fMenuItemWidth * 1.33)
		ENDIF
	ENDIF
	
	IF bAdjustForSafeZone
		// Set up safe-zone adjust
		SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
		SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
		
		// Store highlight coords un-adjusted, as the safe-zone will automatically move the highlight to the correct coords if needed.
		fMenuXOriginUnadjusted = fMenuXOrigin
		fMenuYOriginUnadjusted = fMenuYOrigin

		// Adjust menu coords for safe-zone, so the mouse detection works.
		GET_SCRIPT_GFX_ALIGN_POSITION( fMenuXOrigin, fMenuYOrigin, fMenuXOrigin, fMenuYOrigin )
		
		// Reset the safe-zone for safety as we have multiple exit points later.
		RESET_SCRIPT_GFX_ALIGN()
		
	ENDIF
			
	// Work out extents of menu.
	fMenuXMax = fMenuXOrigin + fMenuItemWidth
	fMenuYMax = fMenuYOrigin + (fMenuItemHeight * iMenuItemNum)
	
	
	///////////////////////////////////////////////
	///     DEBUG - DRAW DEBUG LINES OVER MENU
	///     

	#IF IS_DEBUG_BUILD
		
		IF bCursorMenuSetup
			IF NOT bCursorMenuWidgetsCreated
				SETUP_CURSOR_MENU_DEBUG_WIDGETS()
			ENDIF
		ENDIF
		
		IF bDrawCursorMenuDebugLines
			INT	i
			FLOAT fMenuItemY
			
			SET_TEXT_SCALE(0.2, 0.2)
					
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				
			DRAW_DEBUG_LINE_2D(<<fMenuXOrigin, fMenuYOrigin,0.0>>,<<fMenuXMax, fMenuYOrigin,0.0>>)
			DRAW_DEBUG_LINE_2D(<<fMenuXMax, fMenuYOrigin,0.0>>,<<fMenuXMax, fMenuYMax,0.0>>)
			DRAW_DEBUG_LINE_2D(<<fMenuXMax, fMenuYMax,0.0>>,<<fMenuXOrigin, fMenuYMax,0.0>>)
			DRAW_DEBUG_LINE_2D(<<fMenuXOrigin, fMenuYMax,0.0>>,<<fMenuXOrigin, fMenuYOrigin,0.0>>)
			
			i = 0 
			WHILE i <  iMenuItemNum
			
				fMenuItemY = fMenuYOrigin + (i * fMenuItemHeight)
				DRAW_DEBUG_LINE_2D(<<fMenuXOrigin, fMenuItemY,0.0>>,<<fMenuXMax, fMenuItemY,0.0>>)
				
				++ i

			ENDWHILE
			
		ENDIF
	
	#ENDIF
	
		
	///////////////////////////////////////////
	

	//GET_MOUSE_POSITION(fMouseX, fMouseY)
	fMouseX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
	fMouseY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	//PRINTLN("Mouse: ", fMouseX, " ", fMouseY)
	
	// Check if mouse is in the bounding box for the menu
	IF 	fMouseX >= fMenuXOrigin		
	AND fMouseX <= fMenuXMax 
	AND fMouseY >= fMenuYOrigin
	AND fMouseY <= fMenuYMax
					
		// Adjust relative Y coord so it's inside the menu detection box.
		fCursorYMenuCoord = fMouseY - fMenuYOrigin
		iHighlightedItem = FLOOR(fCursorYMenuCoord / fMenuItemHeight )
				
		// Set up safe-zone adjust
		IF bAdjustForSafeZone
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(-0.05, -0.05, 0.0, 0.0)
			DRAW_RECT_FROM_CORNER(fMenuXOriginUnadjusted, fMenuYOriginUnadjusted + (iHighlightedItem * fMenuItemHeight ),fMenuItemWidth, fMenuItemSelectHeight, 255, 255, 255, iAlpha )
			RESET_SCRIPT_GFX_ALIGN()	
		ELSE
			DRAW_RECT_FROM_CORNER(fMenuXOrigin, fMenuYOrigin + (iHighlightedItem * fMenuItemHeight ),fMenuItemWidth, fMenuItemSelectHeight, 255, 255, 255, iAlpha )
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF bDrawCursorMenuDebugLines
				SET_TEXT_SCALE(0.8, 0.8)
				DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.5, "NUMBER", iHighlightedItem )
			ENDIF
		#ENDIF
			
		RETURN iHighlightedItem

	ENDIF
	
	RETURN MENU_CURSOR_NO_ITEM

ENDFUNC

/// PURPOSE:
///   Helper function to test if the menu on-screen scroll down button is pressed. Also checks mousewheel.
/// PARAMS:
///    bAutoRepeatReset - The menus auto-repeat bool so you can hold down the button to auto-repeat scrolling.
///    bCheckDisabledControl - Will check even if control is disabled
///    bIgnoreMouseWheel - Ignores the mouse wheel movement (used for creators as zoom is on mouse wheel in certain modes)
/// RETURNS:
///    TRUE if scroll down is being activated
FUNC BOOL IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(BOOL bAutoRepeatReset = FALSE, BOOL bCheckDisabledControl = FALSE, BOOL bIgnoreMouseWheel = FALSE)

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) AND bAutoRepeatReset
			OR IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF (bIgnoreMouseWheel = FALSE)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN) AND bCheckDisabledControl)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///   Helper function to test if the menu on-screen scroll up button is pressed. Also checks mousewheel.
/// PARAMS:
///    bAutoRepeatReset - The menus auto-repeat bool so you can hold down the button to auto-repeat scrolling.
///    bCheckDisabledControl - Will check even if control is disabled
///    bIgnoreMouseWheel - Ignores the mouse wheel movement (used for creators as zoom is on mouse wheel in certain modes)
/// RETURNS:
///    TRUE if scroll up is activated
FUNC BOOL IS_MENU_CURSOR_SCROLL_UP_PRESSED(BOOL bAutoRepeatReset = FALSE, BOOL bCheckDisabledControl = FALSE, BOOL bIgnoreMouseWheel = FALSE)

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		
		IF g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) AND bAutoRepeatReset
			OR IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF (bIgnoreMouseWheel = FALSE)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			OR (IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) AND bCheckDisabledControl)
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor accept is pressed whilse inside a menu.
/// RETURNS:
///    TRUE if menu cursor accept is being pressed whilst a valid menu item is highlighted
FUNC BOOL IS_MENU_CURSOR_ACCEPT_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor accept is pressed whilse inside a menu.
/// RETURNS:
///    TRUE if menu cursor accept is being pressed whilst a valid menu item is highlighted
FUNC BOOL IS_MENU_CURSOR_ACCEPT_HELD()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor accept is pressed whilse inside a menu.
/// RETURNS:
///    TRUE if menu cursor accept is being pressed whilst a valid menu item is highlighted
FUNC BOOL IS_MENU_CURSOR_ACCEPT_RELEASED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
			IF IS_CONTROL_JUST_RELEASED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor cancel is pressed
/// RETURNS:
///    TRUE if cancel has been pressed
FUNC BOOL IS_MENU_CURSOR_CANCEL_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor accept is pressed on the 'up' icon button
/// RETURNS:
///    TRUE if cursor accept is pressed on the 'up' icon button
FUNC BOOL IS_MENU_CURSOR_UP_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem = -2
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Helper function to test if cursor accept is pressed on the 'down' icon button
/// RETURNS:
///    TRUE if cursor accept is pressed on the 'down' icon button
FUNC BOOL IS_MENU_CURSOR_DOWN_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF g_iMenuCursorItem = -3
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Adjusts an x coord or width for menus based on the pause menu, which scale horizontally to keep their aspect ratio.
///    The coord needs to be one defined originally for 16:9
/// PARAMS:
///    fX - The input coord.
/// RETURNS:
///    The adjusted coord
FUNC FLOAT ADJUST_X_COORD_FOR_ASPECT_RATIO( FLOAT fX )

	fX *= ASPECT_16_9 / GET_ASPECT_RATIO(FALSE)
	
	RETURN fX

ENDFUNC

/// PURPOSE:
///    Handles sliders in scripted menus
/// PARAMS:
///    eMouseSlider - 
///    iIndex - 
///    fMinValue - 
///    fMaxValue - 
/// RETURNS:
///    
FUNC FLOAT HANDLE_MOUSE_SLIDER( MOUSE_SLIDER_TYPE eMouseSlider, FLOAT fX = 0.06, FLOAT fY = 0.596, FLOAT fWidth = 0.12, FLOAT fMinValue = 0.0, FLOAT fMaxValue = 100.0 )
	
		
	FLOAT fXMax, fYMax
	FLOAT fYSpacing
	FLOAT fCursorSliderCoord
	FLOAT fSliderValue
	
	//FLOAT fFudgeFactor = 1.0
	FLOAT fMinClamp = fMinValue
	FLOAT fMaxClamp = fMaxValue
		
	FLOAT fBorder = 0.0
	
	//FLOAT fWidthAdjust = 1.0
	
	FLOAT fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X)// + 0.004 // Fudge for middle finger
	FLOAT fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	//FLOAT fHeight
	
	VECTOR vPauseMenuOrigin
	
	//SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// Don't do anything if the camera is being dragged.
	IF g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM
		RETURN -1.0
	ENDIF
	
	SWITCH eMouseSlider
	
		CASE MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_OPACITY
		
			vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
			
			//DISPLAY_TEXT_WITH_FLOAT(0.8, 0.1, "NUMBER", vPauseMenuOrigin.x, 3 )
			//DISPLAY_TEXT_WITH_FLOAT(0.8, 0.2, "NUMBER", vPauseMenuOrigin.y, 3 )
		
			//IF NOT GET_IS_WIDESCREEN()
				//fXAdjust = 0.0289
			//fWidthAdjust = ADJUST_X_COORD_FOR_ASPECT_RATIO(1.25)
			//ENDIF
						
			fX = vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.005)//+ //fXAdjust
			fY = vPauseMenuOrigin.y + 0.518
			
			fWidth = ADJUST_X_COORD_FOR_ASPECT_RATIO(0.21)
						
			fYSpacing = 0.025
			
			//fFudgeFactor = 1.01
			fMinClamp = fMinValue + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.05)
			fMaxClamp = fMaxValue - ADJUST_X_COORD_FOR_ASPECT_RATIO(0.05)
			
			fBorder = ADJUST_X_COORD_FOR_ASPECT_RATIO(0.005)
					
			///GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU(fX, fY, fWidth, fHeight, fHeight, 1 )
			//DRAW_RECT_FROM_CORNER( fX, fY, fWidth, fHeight, 255,0,0,128 )
		
		BREAK
		
		CASE MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_FAMILY_RESEMBLANCE
		
			vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
						
			fX = vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.145)
			fY = vPauseMenuOrigin.y + 0.417
			
			fWidth = ADJUST_X_COORD_FOR_ASPECT_RATIO(0.042)
			//fHeight = 0.025
			fYSpacing = 0.025
			
			//fFudgeFactor = 1.0
			fMinClamp = fMinValue 
			fMaxClamp = fMaxValue
			
			fBorder = fWidth / 2
								
			//GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU(fX, fY, fWidth, fHeight, fHeight, 1 )
			//DRAW_RECT_FROM_CORNER( fX, fY + 0.05, fWidth, fHeight, 0,255,0,128 )
			
		BREAK
		
		CASE MOUSE_SLIDER_TYPE_CHARACTER_CREATOR_SKIN_TONE
		
			vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
	
		
			fX = vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.145)
			fY = vPauseMenuOrigin.y + 0.457
			
			fWidth = ADJUST_X_COORD_FOR_ASPECT_RATIO(0.042)
			//fHeight = 0.025
			fYSpacing = 0.025
			
			//fFudgeFactor = 1.01
			fMinClamp = fMinValue
			fMaxClamp = fMaxValue
			
			fBorder = fWidth / 2
								
			//GET_CURSOR_HIGHLIGHTED_ITEM_IN_CUSTOM_MENU(fX, fY, fWidth, fHeight, fHeight, 1 )
			//DRAW_RECT_FROM_CORNER( fX, fY, fWidth, fHeight, 0,255,0,128 )
			
		BREAK
		
		CASE MOUSE_SLIDER_TYPE_HAIRDO_SHOP_OPACITY
		
			fWidth *= 0.95 // Shrink bar down so it fits properly
			fy += 0.035 // Move slider down
			fx += (fWidth * 0.025) // Nudge bar across
	
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP) 
			SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
	
			GET_SCRIPT_GFX_ALIGN_POSITION(fX, fY, fX, fY)
		
			RESET_SCRIPT_GFX_ALIGN()
			
			fYSpacing = 0.025
			
			//fFudgeFactor = 1.0
			fMinClamp = fMinValue
			fMaxClamp = fMaxValue
			
			fBorder = 0.005
			
			//DRAW_RECT_FROM_CORNER( fX, fY, fWidth, fYspacing, 0,255, 0,255 )
			
		BREAK
		
	ENDSWITCH
			
	fXMax = fX + fWidth
	fYMax = fY + fYSpacing
	
	
	//DRAW_RECT_FROM_CORNER( fX, fY, fWidth, fYspacing, 255,0,0,128 )
	
	
	IF fMouseY >= fY
	AND fMouseY <= fYMax
	
		IF 	fMouseX >= fX
		AND fMouseX <= fXMax
	
			fCursorSliderCoord = (fMouseX - fX) / fWidth
			
			fSliderValue = fMinValue + ((fMaxValue - fMinValue) * fCursorSliderCoord)
			
			//DISPLAY_TEXT_WITH_FLOAT( 0.8, 0.1, "NUMBER", fCursorSliderCoord, 3 )	
			//DISPLAY_TEXT_WITH_FLOAT( 0.8, 0.2, "NUMBER", fSliderValue, 3 )
			
			//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
			
			IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				// Clamping minimum and maximum values as otherwise you can't get 0 or 100%
				fSliderValue = CLAMP(fSliderValue, fMinClamp, fMaxClamp) 
				RETURN fSliderValue
			ENDIF		
		ENDIF
					
		// HEnsures we can get 0 or 100% at the extreme edges of the slider.
		IF fBorder > 0.0
		
			//DRAW_RECT_FROM_CORNER( fX-fBorder, fY, fBorder, fYspacing, 0,0,255,255 )
			//DRAW_RECT_FROM_CORNER( fXMax, fY, fBorder, fYspacing, 0,0,255,255 )
		
			IF fMouseX > fX - fBorder
			AND fMouseX < fX
			
				//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
			
				IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					RETURN fMinValue
				ENDIF
			ENDIF
		
			IF fMouseX < fXMax + fBorder
			AND fMouseX > fXMax
			
				//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)	
			
				IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
					RETURN fMaxValue
				ENDIF
			ENDIF
		
		ENDIF
	
	
	ENDIF
	
	RETURN -1.0
	
ENDFUNC



/// PURPOSE:
///    Notifies if mouse cursor is inside a rectangular area on screen.
/// PARAMS:
///    fX - 
///    fY - 
///    fWidth - 
///    fHeight - 
///    fMouseX - 
///    fMouseY - 
/// RETURNS:
///    
FUNC BOOL IS_CURSOR_IN_AREA( FLOAT fX, FLOAT fY, FLOAT fWidth, FLOAT fHeight, FLOAT fMouseX = -1.0, FLOAT fMouseY = -1.0)
	
	IF fMouseX < 0 OR fMouseY < 0
	
		fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X)
		fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	ENDIF
	
	// Debug
//	FORCE_SCRIPTED_GFX_WHEN_FRONTEND_ACTIVE(TRUE) 
//	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
//	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
//	DRAW_RECT_FROM_CORNER( fX, fY, fWidth, fHeight, 255, 0, 0,128 )

	IF fMouseX > fX
	AND fMouseY > fY
	AND fMouseX < (fX + fWidth)
	AND fMouseY < (fY + fHeight)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	

ENDFUNC


FUNC BOOL IS_MOUSE_IN_PED_COLOUR_BAR_AREA(BOOL bIsCharacterCreator, BOOL bOpacityIncluded, BOOL bAspectAdjust = TRUE)

	FLOAT fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	FLOAT fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
	
	VECTOR vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
	
	FLOAT fYAdjust = 0.0
	//FLOAT fWidthAdjust = 1.0
	
	IF bOpacityIncluded = FALSE
		fYAdjust = 0.069
	ENDIF
	
	IF NOT bIsCharacterCreator
		// Stub
	ENDIF

	IF bAspectAdjust
	//AND NOT GET_IS_WIDESCREEN()
	//	fWidthAdjust = 1.25
	ENDIF
	
	// In entire bar area
	IF IS_CURSOR_IN_AREA( vPauseMenuOrigin.x, vPauseMenuOrigin.y + 0.556 - fYAdjust, ADJUST_X_COORD_FOR_ASPECT_RATIO(0.223) , 0.101, fMouseX, fMouseY )
	AND PAUSE_MENU_GET_HAIR_COLOUR_INDEX() = -1 
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Checks if the mouse cursor is inside the colour bar seen in the character creator.
/// PARAMS:
///    bIsCharacterCreator - 
///    bOpacityIncluded - 
///    bAspectAdjust - 
/// RETURNS:
///    
FUNC INT GET_MOUSE_PED_COMPONENT_COLOUR_INCREMENT( BOOL bIsCharacterCreator, BOOL bOpacityIncluded, BOOL bAspectAdjust = TRUE )
	
	FLOAT fMouseX = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_X )
	FLOAT fMouseY = GET_CONTROL_NORMAL( FRONTEND_CONTROL, INPUT_CURSOR_Y )
	
	FLOAT fYAdjust = 0.0
	//FLOAT fXAdjust = 0.0
	
	VECTOR vPauseMenuOrigin = GET_PAUSE_MENU_POSITION()
	vPauseMenuOrigin.y +=  0.57
	
	IF bOpacityIncluded = FALSE
		fYAdjust = 0.069
	ENDIF
	
	IF NOT bIsCharacterCreator
		// Stub
	ENDIF
	
	IF bAspectAdjust
	//AND NOT GET_IS_WIDESCREEN()
	//	fXAdjust = 0.0303
	ENDIF
				
	// Left arrow
	IF IS_CURSOR_IN_AREA( vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.007), vPauseMenuOrigin.y - fYAdjust, ADJUST_X_COORD_FOR_ASPECT_RATIO(0.011), 0.018, fMouseX, fMouseY )
		//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		RETURN -1
	ENDIF
	
	IF bAspectAdjust
	//AND NOT GET_IS_WIDESCREEN()
	//	fXAdjust = 0.079
	ENDIF
	
	// Right arrow
	//IF IS_CURSOR_IN_AREA( 0.287 + fXAdjust, 0.651 - fYAdjust, 0.011, 0.018, fMouseX, fMouseY )
	IF IS_CURSOR_IN_AREA( vPauseMenuOrigin.x + ADJUST_X_COORD_FOR_ASPECT_RATIO(0.210), vPauseMenuOrigin.y - fYAdjust, ADJUST_X_COORD_FOR_ASPECT_RATIO(0.011), 0.018, fMouseX, fMouseY )
		//SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		RETURN 1
	ENDIF
		
	RETURN 0
	
ENDFUNC

