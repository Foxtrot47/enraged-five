//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   chop_cam_public.sch                                   						//
//      AUTHOR          :   Rob Bray                                                    				//
//      DESCRIPTION     :   Common functions for chop cam												//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_vehicle.sch" 
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_physics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_audio.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "mission_stat_public.sch"
USING "player_ped_public.sch"

CONST_INT CHOP_VIEW_CAM_INTERP_TIME 0
CONST_FLOAT CHOP_SWITCH_DISTANCE 70.0

ENUM CHOP_VIEW_CAM_STATE_ENUM
	CHOP_VIEW_CAM_DISABLED = 0,
	CHOP_VIEW_CAM_TO_CHOP,
	CHOP_VIEW_CAM_ON_CHOP,
	CHOP_VIEW_CAM_TO_PLAYER
ENDENUM

ENUM CHOP_MISSION_ENUM
	CHOP_MISSION_UNSPECIFIED = 0,
	CHOP_MISSION_FRANKLIN0,
	CHOP_MISSION_FRANKLIN1,
	CHOP_MISSION_EXILE2
ENDENUM

ENUM CHOP_CAM_AUDIO_STATE_ENUM
	CHOP_CAM_AUDIO_STATE_NOT_STARTED = 0,
	CHOP_CAM_AUDIO_STATE_STREAMING,
	CHOP_CAM_AUDIO_STATE_PLAYING
ENDENUM

STRUCT CHOP_VIEW_CAM_DATA
	// state
	CHOP_VIEW_CAM_STATE_ENUM state 
	SELECTOR_SLOTS_ENUM selectorSlot = SELECTOR_PED_MICHAEL
	CHOP_MISSION_ENUM chopMission 
	
	// chop ped
	PED_INDEX chopPed

	// camera stuff
	CAMERA_INDEX chopCam
	INT iFinishAutoInterpTime
	FLOAT fCamYAttach
	FLOAT fCamZAttach
	
	BOOL bNeverTrevor
	
	BOOL bClearPendingTask
	
	// audio stuff
	CHOP_CAM_AUDIO_STATE_ENUM audioState
	INT iAudioVariation
	INT iCamSoundID = -1
	
	VECTOR vLastPTFXPos
	
	// OBSOLETE! REMOVE!
	PTFX_ID scentPTFX
ENDSTRUCT

// request chop cam sfx
FUNC BOOL REQUEST_CHOP_CAM_SFX()
	RETURN TRUE
	
	/*
	IF REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_A_01")
	AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_A_02")
	AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_B_01")
	AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_B_02")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	*/
ENDFUNC

// release chop cam sfx
PROC RELEASE_CHOP_CAM_SFX(CHOP_VIEW_CAM_DATA &chopViewCamData, BOOL bDumpSoundID = TRUE)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_A_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_A_02")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_B_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_B_02")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_C_01")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("CHOP_CAM_C_02")
	
	chopViewCamData.audioState = CHOP_CAM_AUDIO_STATE_NOT_STARTED
	
	IF bDumpSoundID
		IF chopViewCamData.iCamSoundID <> -1
			RELEASE_SOUND_ID(chopViewCamData.iCamSoundID)
			chopViewCamData.iCamSoundID = -1
		ENDIF
	ENDIF
ENDPROC

// fake selector icon to player
PROC FAKE_SELECTOR_IS_PLAYER(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds, BOOL bFake)
	INT i 
	IF bFake
		REPEAT 3 i 
			SELECTOR_SLOTS_ENUM thisSelector = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, i)
			IF thisSelector <> chopViewCamData.selectorSlot
				IF IS_SELECTOR_PED_AVAILABLE_IN_FLOW(thisSelector)
					SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, thisSelector, SELECTOR_STATE_AVAILABLE)
				ENDIF
			ELSE
				SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, thisSelector, SELECTOR_STATE_CURRENT)
			ENDIF
		ENDREPEAT
	ELSE
		REPEAT 3 i 
			SELECTOR_SLOTS_ENUM thisSelector = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, i)
			SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, thisSelector, SELECTOR_STATE_DEFAULT)
		ENDREPEAT
	ENDIF
	
	IF chopViewCamData.bNeverTrevor
		//IF IS_SELECTOR_PED_AVAILABLE_IN_FLOW(SELECTOR_PED_TREVOR)
		//	SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_STATE_UNAVAILABLE)
		//ELSE
		SET_SELECTOR_PED_FAKE_STATE(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_STATE_DEFAULT)
		//ENDIF
	ENDIF	
ENDPROC

// set chop hint
PROC SET_CHOP_SELECT_HINT(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds, BOOL bHintToChop)
	/*
	INT i 
	REPEAT 3 i 
		SELECTOR_SLOTS_ENUM thisSelector = INT_TO_ENUM(SELECTOR_SLOTS_ENUM, i)
		IF thisSelector <> chopViewCamData.selectorSlot
			SET_SELECTOR_PED_HINT(sSelectorPeds, thisSelector, NOT bHintToChop)
		ELSE
			SET_SELECTOR_PED_HINT(sSelectorPeds, thisSelector, bHintToChop)
		ENDIF
	ENDREPEAT	
	*/
	
	SET_SELECTOR_PED_HINT(sSelectorPeds, chopViewCamData.selectorSlot, bHintToChop)
ENDPROC

// common disable chop cam functions
PROC COMMON_DISABLE_CHOP_VIEW_CAM(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds)
	CLEAR_TIMECYCLE_MODIFIER()
	
	IF ANIMPOSTFX_IS_RUNNING("ChopVision")
		ANIMPOSTFX_STOP("ChopVision")
	ENDIF
	
	FAKE_SELECTOR_IS_PLAYER(chopViewCamData, sSelectorPeds, FALSE)
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(FALSE)
	
	IF chopViewCamData.iCamSoundID <> -1
		IF NOT HAS_SOUND_FINISHED(chopViewCamData.iCamSoundID)
			STOP_SOUND(chopViewCamData.iCamSoundID)
		ENDIF	
	ENDIF
	RELEASE_CHOP_CAM_SFX(chopViewCamData, FALSE)
	
	SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, -1)
	
	// allow to speak
	IF NOT IS_PED_INJURED(chopViewCamData.chopPed)
		STOP_PED_SPEAKING(chopViewCamData.chopPed, FALSE)
	ENDIF
	
	// set visible
	IF DOES_ENTITY_EXIST(chopViewCamData.chopPed)
		IF IS_ENTITY_DEAD(chopViewCamData.chopPed)
		OR NOT IS_ENTITY_DEAD(chopViewCamData.chopPed)
			SET_ENTITY_VISIBLE(chopViewCamData.chopPed, TRUE)
		ENDIF
	ENDIF
	
	// MISSION SPECIFIC STUFF
	SWITCH chopViewCamData.chopMission
		CASE CHOP_MISSION_FRANKLIN0
			// Franklin0 Chop
			INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
		BREAK
	ENDSWITCH
ENDPROC

// disable mission specific player tasks
PROC DISABLE_CHOP_VIEW_CAM_PLAYER_TASKS(CHOP_VIEW_CAM_DATA &chopViewCamData)
	SWITCH chopViewCamData.chopMission
		CASE CHOP_MISSION_FRANKLIN0		
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) <> FINISHED_TASK
			OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) <> FINISHED_TASK
			OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> FINISHED_TASK
			OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_TO_ENTITY) <> FINISHED_TASK
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_TO_OFFSET_OF_ENTITY) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
					chopViewCamData.bClearPendingTask = TRUE
				ENDIF
			
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// set chop view cam on
PROC SET_CHOP_VIEW_CAM_ON_CHOP(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds, BOOL bHandleCams = TRUE, BOOL bDoInterp = FALSE, INT iInterpTime = DEFAULT_INTERP_TO_FROM_GAME)
	FAKE_SELECTOR_IS_PLAYER(chopViewCamData, sSelectorPeds, TRUE)
	SET_TIMECYCLE_MODIFIER("chop")	
	SET_CAM_ACTIVE(chopViewCamData.chopCam, TRUE)
	
	IF bHandleCams
		RENDER_SCRIPT_CAMS(TRUE, bDoInterp, iInterpTime)
		IF bDoInterp
			chopViewCamData.iFinishAutoInterpTime = GET_GAME_TIMER() + iInterpTime
		ENDIF
	ENDIF
	sSelectorPeds.eNewSelectorPed = NUMBER_OF_SELECTOR_PEDS
	IF chopViewCamData.chopMission = CHOP_MISSION_FRANKLIN0
		INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START()
	ENDIF	
	
	ANIMPOSTFX_PLAY("ChopVision", 0, FALSE)

	SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 1)
	
	// prevent chop speaking
	IF NOT IS_PED_INJURED(chopViewCamData.chopPed)
		SET_ENTITY_VISIBLE(chopViewCamData.chopPed, FALSE)
		STOP_PED_SPEAKING(chopViewCamData.chopPed, TRUE)
	ENDIF
	
	chopViewCamData.state = CHOP_VIEW_CAM_ON_CHOP
ENDPROC

// set chop view cam interp to chop
PROC SET_CHOP_VIEW_CAM_TO_CHOP(CHOP_VIEW_CAM_DATA &chopViewCamData, BOOL bHandleControl = TRUE, BOOL bHandleHUD = TRUE, BOOL bHandleCams = TRUE, BOOL bDoInterp = FALSE, INT iInterpTime = DEFAULT_INTERP_TO_FROM_GAME, BOOL bUseCodeSwitch = FALSE, BOOL bOverrideType = FALSE, SWITCH_TYPE overrideType = SWITCH_TYPE_SHORT)
	IF bHandleControl
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	IF bHandleHUD
		DISPLAY_HUD(FALSE)
		DISPLAY_RADAR(FALSE)
	ENDIF
	IF bHandleCams
		RENDER_SCRIPT_CAMS(TRUE, bDoInterp, iInterpTime)
		IF bDoInterp
			chopViewCamData.iFinishAutoInterpTime = GET_GAME_TIMER() + iInterpTime
		ENDIF
	ENDIF
	
	IF bUseCodeSwitch
		SWITCH_FLAGS eSwitchFlags = INT_TO_ENUM(SWITCH_FLAGS, 0)
		SWITCH_TYPE switchType
		
		IF NOT bOverrideType
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(chopViewCamData.chopPed), GET_ENTITY_COORDS(PLAYER_PED_ID())) < CHOP_SWITCH_DISTANCE
				switchType = SWITCH_TYPE_SHORT
			ELSE
				switchType = SWITCH_TYPE_AUTO
				eSwitchFlags = SWITCH_FLAG_SKIP_OUTRO 
			ENDIF	
		ELSE
			switchType = overrideType
		ENDIF
		
		START_PLAYER_SWITCH(PLAYER_PED_ID(), chopViewCamData.chopPed, eSwitchFlags, switchType)
	ENDIF
	
	HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE(TRUE)
	
	chopViewCamData.state = CHOP_VIEW_CAM_TO_CHOP
ENDPROC

// set chop view cam interp to player
PROC SET_CHOP_VIEW_CAM_TO_PLAYER(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds, BOOL bSetGameplayCamRelative = TRUE, BOOL bUseCodeSwitch = FALSE, BOOL bClearPTFX = FALSE, BOOL bOverrideType = FALSE, SWITCH_TYPE overrideType = SWITCH_TYPE_SHORT)
	IF bSetGameplayCamRelative
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	
	IF bClearPTFX
		REMOVE_PARTICLE_FX_IN_RANGE(chopViewCamData.vLastPTFXPos, 2)
	ENDIF
	
	IF bUseCodeSwitch
		SWITCH_FLAGS eSwitchFlags = INT_TO_ENUM(SWITCH_FLAGS, 0)
		SWITCH_TYPE switchType
		IF NOT bOverrideType
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(chopViewCamData.chopPed), GET_ENTITY_COORDS(PLAYER_PED_ID())) < CHOP_SWITCH_DISTANCE
				switchType = SWITCH_TYPE_SHORT
			ELSE
				switchType = SWITCH_TYPE_AUTO
				eSwitchFlags = SWITCH_FLAG_SKIP_INTRO
			ENDIF
		ELSE
			switchType = overrideType
		ENDIF
		
		START_PLAYER_SWITCH(chopViewCamData.chopPed, PLAYER_PED_ID(), eSwitchFlags, switchType)	

		IF switchType = SWITCH_TYPE_SHORT
			COMMON_DISABLE_CHOP_VIEW_CAM(chopViewCamData, sSelectorPeds)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
	ELSE	
		COMMON_DISABLE_CHOP_VIEW_CAM(chopViewCamData, sSelectorPeds)
	ENDIF
	
	chopViewCamData.state = CHOP_VIEW_CAM_TO_PLAYER
ENDPROC

// set chop view cam interp to chop
PROC SET_CHOP_VIEW_CAM_DISABLED(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds, BOOL bResetNewSelectorPed = FALSE, BOOL bHandleControl = TRUE, BOOL bHandleHUD = TRUE, BOOL bHandleCams = TRUE, BOOL bInterpToGameCam = FALSE, INT iInterpTime = DEFAULT_INTERP_TO_FROM_GAME)
	COMMON_DISABLE_CHOP_VIEW_CAM(chopViewCamData, sSelectorPeds)
	DISABLE_CHOP_VIEW_CAM_PLAYER_TASKS(chopViewCamData)
	IF bResetNewSelectorPed
		sSelectorPeds.eNewSelectorPed = NUMBER_OF_SELECTOR_PEDS		
	ENDIF
	IF bHandleControl
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF bHandleHUD
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
	ENDIF
	IF bHandleCams
		RENDER_SCRIPT_CAMS(FALSE, bInterpToGameCam, iInterpTime)
		IF bInterpToGameCam
			chopViewCamData.iFinishAutoInterpTime = GET_GAME_TIMER() + iInterpTime
		ENDIF
	ENDIF
	
	chopViewCamData.state = CHOP_VIEW_CAM_DISABLED
ENDPROC

CONST_FLOAT CHOP_CAM_Y -0.15
CONST_FLOAT CHOP_CAM_Z 0.05

// set chop view cam to first person
PROC SET_CHOP_VIEW_CAM_FIRST_PERSON(CHOP_VIEW_CAM_DATA &chopViewCamData)
	IF NOT IS_PED_INJURED(chopViewCamData.chopPed)
		chopViewCamData.fCamYAttach = CHOP_CAM_Y
		chopViewCamData.fCamZAttach = CHOP_CAM_Z
		ATTACH_CAM_TO_PED_BONE(chopViewCamData.chopCam, chopViewCamData.chopPed, BONETAG_HEAD, <<0,chopViewCamData.fCamYAttach,chopViewCamData.fCamZAttach>>)
		SET_CAM_NEAR_CLIP(chopViewCamData.chopCam, 0.101)
		POINT_CAM_AT_ENTITY(chopViewCamData.chopCam, chopViewCamData.chopPed,<<0.0, 10.0, chopViewCamData.fCamZAttach>>)
		SET_CAM_FOV(chopViewCamData.chopCam, 96.76)
		SET_CAM_DOF_PLANES(chopViewCamData.chopCam, 0.0, 0.0, 100000.0, 100000.0)
	ENDIF
ENDPROC

// set chop cam handle water
PROC SET_CHOP_VIEW_CAM_HANDLE_WATER(CHOP_VIEW_CAM_DATA &chopViewCamData)
	IF NOT IS_PED_INJURED(chopViewCamData.chopPed)
	AND DOES_CAM_EXIST(chopViewCamData.chopCam)
		IF IS_ENTITY_IN_WATER(chopViewCamData.chopPed)
			chopViewCamData.fCamZAttach = chopViewCamData.fCamZAttach +@ 0.1
			IF chopViewCamData.fCamZAttach >= 0.32
				chopViewCamData.fCamZAttach = 0.32
			ENDIF
		ELSE
			chopViewCamData.fCamZAttach = chopViewCamData.fCamZAttach -@ 0.1
			IF chopViewCamData.fCamZAttach <= CHOP_CAM_Z
				chopViewCamData.fCamZAttach = CHOP_CAM_Z
			ENDIF			
		ENDIF
		
		ATTACH_CAM_TO_PED_BONE(chopViewCamData.chopCam, chopViewCamData.chopPed, BONETAG_HEAD, <<0,chopViewCamData.fCamYAttach,chopViewCamData.fCamZAttach>>)
	ENDIF
ENDPROC

// set chop cam pull back
PROC SET_CHOP_VIEW_CAM_CUSTOM_ATTACH(CHOP_VIEW_CAM_DATA &chopViewCamData, FLOAT fYAttach, FLOAT fZAttach)
	IF NOT IS_PED_INJURED(chopViewCamData.chopPed)
	AND DOES_CAM_EXIST(chopViewCamData.chopCam)
		chopViewCamData.fCamYAttach = fYAttach
		chopViewCamData.fCamZAttach = fZAttach

		ATTACH_CAM_TO_PED_BONE(chopViewCamData.chopCam, chopViewCamData.chopPed, BONETAG_HEAD, <<0,chopViewCamData.fCamYAttach,chopViewCamData.fCamZAttach>>)
	ENDIF
ENDPROC

// create chop cam
PROC CREATE_CHOP_CAM(CHOP_VIEW_CAM_DATA &chopViewCamData)
	IF NOT DOES_CAM_EXIST(chopViewCamData.chopCam)
		chopViewCamData.chopCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		IF chopViewCamData.iCamSoundID <> -1
			IF NOT HAS_SOUND_FINISHED(chopViewCamData.iCamSoundID)
				STOP_SOUND(chopViewCamData.iCamSoundID)
			ENDIF	
			RELEASE_SOUND_ID(chopViewCamData.iCamSoundID)
			chopViewCamData.iCamSoundID = -1
		ENDIF
		chopViewCamData.iCamSoundID = GET_SOUND_ID()
		chopViewCamData.audioState = CHOP_CAM_AUDIO_STATE_NOT_STARTED
		chopViewCamData.iAudioVariation = GET_RANDOM_INT_IN_RANGE(0,3)
		SET_CHOP_VIEW_CAM_FIRST_PERSON(chopViewCamData)
	ENDIF
ENDPROC

// handle chop cam audio
PROC HANDLE_CHOP_CAM_AUDIO(CHOP_VIEW_CAM_DATA &chopViewCamData)
	SWITCH chopViewCamData.audioState
		CASE CHOP_CAM_AUDIO_STATE_NOT_STARTED
			IF chopViewCamData.state = CHOP_VIEW_CAM_ON_CHOP
				chopViewCamData.audioState = CHOP_CAM_AUDIO_STATE_STREAMING
			ENDIF
		BREAK
		CASE CHOP_CAM_AUDIO_STATE_STREAMING
			BOOL bStreamed
			bStreamed = FALSE
			SWITCH chopViewCamData.iAudioVariation
				CASE 0
					IF REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_A_01")
					AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_A_02")
						bStreamed = TRUE
					ENDIF
				BREAK
				CASE 1
					IF REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_B_01")
					AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_B_02")
						bStreamed = TRUE
					ENDIF
				BREAK
				CASE 2
					IF REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_C_01")
					AND REQUEST_SCRIPT_AUDIO_BANK("CHOP_CAM_C_02")
						bStreamed = TRUE
					ENDIF
				BREAK
			ENDSWITCH
			IF bStreamed
				IF chopViewCamData.iCamSoundID <> -1
					IF HAS_SOUND_FINISHED(chopViewCamData.iCamSoundID)			
						SWITCH chopViewCamData.iAudioVariation
							CASE 0
								PLAY_SOUND_FRONTEND(chopViewCamData.iCamSoundID, "CHOP_CAM_A")
								PRINTSTRING("play chop sound A") PRINTNL()
							BREAK
							CASE 1
								PLAY_SOUND_FRONTEND(chopViewCamData.iCamSoundID, "CHOP_CAM_B")
								PRINTSTRING("play chop sound B") PRINTNL()
							BREAK
							CASE 2
								PLAY_SOUND_FRONTEND(chopViewCamData.iCamSoundID, "CHOP_CAM_C")
								PRINTSTRING("play chop sound C") PRINTNL()
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				
				chopViewCamData.iAudioVariation++
				IF chopViewCamData.iAudioVariation > 2
					chopViewCamData.iAudioVariation = 0
				ENDIF
				chopViewCamData.audioState = CHOP_CAM_AUDIO_STATE_PLAYING
			ENDIF
		BREAK
		CASE CHOP_CAM_AUDIO_STATE_PLAYING
		BREAK
	ENDSWITCH
ENDPROC

// has player selected chop?
FUNC BOOL IS_CHOP_SELECTED(CHOP_VIEW_CAM_DATA &chopViewCamData, SELECTOR_PED_STRUCT &sSelectorPeds)
	IF sSelectorPeds.eNewSelectorPed = chopViewCamData.selectorSlot
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// set hidof when in chop cam - 1824903
PROC HANDLE_CHOP_CAM_HIDOF(CHOP_VIEW_CAM_DATA &chopViewCamData)
//	printstring("here1") printnl()
	IF chopViewCamData.state = CHOP_VIEW_CAM_ON_CHOP
	//	printstring("here2") printnl()
		SET_USE_HI_DOF()
	ENDIF
ENDPROC
