///////////////////////////////////////////////////////////////////////////
///    INSTRUCTIONAL BUTTON ICONS PUBLIC HEADER
///    
///    Script interface for new instructional button icon functionality.
///    
//////////////////////////////////////////////////////////////////////////
///    
///    
///    
///  

/// Instructional button functions - gets a string containing the instructional button(s) depending on the input passed in.
  
/// Special gamepad icons

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Left Stick Rotate icon.
FUNC TEXT_LABEL_63 GET_ICON_LSTICK_ROTATE_STRING()
      TEXT_LABEL_63 label = "b_20"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Right Stick Rotate icon.
FUNC TEXT_LABEL_63 GET_ICON_RSTICK_ROTATE_STRING()
      TEXT_LABEL_63 label = "b_29"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Sixaxis Drive icon.
FUNC TEXT_LABEL_63 GET_ICON_SIXAXIS_DRIVE_STRING()
      TEXT_LABEL_63 label = "b_40"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Sixaxis Pitch icon.
FUNC TEXT_LABEL_63 GET_ICON_SIXAXIS_PITCH_STRING()
      TEXT_LABEL_63 label = "b_41"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Sixaxis Reload icon.
FUNC TEXT_LABEL_63 GET_ICON_SIXAXIS_RELOAD_STRING()
      TEXT_LABEL_63 label = "b_42"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Sixaxis Roll icon.
FUNC TEXT_LABEL_63 GET_ICON_SIXAXIS_ROLL_STRING()
      TEXT_LABEL_63 label = "b_43"
      return label
ENDFUNC

//INFO:
// PURPOSE:  Returns the TEXT_LABEL_63 that represents the Spinner icon.
FUNC TEXT_LABEL_63 GET_ICON_SPINNER_STRING()
      TEXT_LABEL_63 label = "b_44"
      return label
ENDFUNC

// Instructional button wrapper functions

//INFO:
// PURPOSE: Returns the TEXT_LABEL_63 of the instructional button(s) represented by the passed in control action.
FUNC TEXT_LABEL_63 GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(CONTROL_TYPE control, CONTROL_ACTION action, BOOL allowXOSwap = TRUE)

	TEXT_LABEL_63 sText = GET_CONTROL_INSTRUCTIONAL_BUTTONS_STATIC_STRING(control,action, allowXOSwap)

	RETURN sText
		
ENDFUNC

//INFO:
// PURPOSE: Returns the TEXT_LABEL_63 of the instructional button(s) represented by the passed in control action group
FUNC TEXT_LABEL_63 GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(CONTROL_TYPE control, CONTROL_ACTION_GROUP actionGroup, BOOL allowXOSwap = TRUE)

	TEXT_LABEL_63 sText = GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STATIC_STRING( control, actionGroup, allowXOSwap)
	
	RETURN sText
		
ENDFUNC
