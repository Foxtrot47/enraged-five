USING "globals.sch"
USING "flow_mission_data_public.sch"
USING "finance_modifiers_public.sch"
USING "player_ped_public.sch"
USING "commands_hud.sch"
USING "end_screen.sch"
USING "screenDisplayState.sch"
USING "achievement_public.sch"
USING "end_screen.sch"
USING "dlc_mission_stat_private.sch"

#IF USE_CLF_DLC
USING "mission_stat_generated_private_CLF.sch"
#ENDIF
#IF NOT USE_CLF_DLC
USING "mission_stat_generated_private.sch"
#ENDIF



#IF IS_DEBUG_BUILD
FUNC STRING SAFE_GET_MISSION_STAT_NAME(ENUM_MISSION_STATS e)
	IF e = UNSET_MISSION_STAT_ENUM
		RETURN "UNSET_MISSION_STAT_ENUM"
	ENDIF
	
	IF NOT g_MissionStatTrackingPrototypes[e].bHidden
		RETURN GET_MISSION_STAT_NAME(e)
	ELSE
		TEXT_LABEL str = "MS_UNKN_"
		str += ENUM_TO_INT(e)
		
		#IF IS_DEBUG_BUILD
		RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
		#ENDIF
		#IF NOT IS_DEBUG_BUILD
		RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(str)
		#ENDIF
	ENDIF
ENDFUNC
#ENDIF

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// complete medals
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC FLOAT GET_MISSION_STAT_PERCENTAGE(INT visiblesucceeded,INT visibleattempted, BOOL criticalInvalidated = FALSE)
	IF criticalInvalidated
		RETURN 0.0
	ENDIF
	IF visiblesucceeded = visibleattempted
		RETURN 100.0
	ENDIF

	FLOAT uploadRatio = TO_FLOAT(visiblesucceeded)/TO_FLOAT(visibleattempted)
	FLOAT fUploadPercentage = (50.0*uploadRatio) + 50.0

	fUploadPercentage = TO_FLOAT(CEIL(fUploadPercentage))
	IF fUploadPercentage > 100.0
		fUploadPercentage = 100.0
	ENDIF
	
	RETURN fUploadPercentage	
ENDFUNC

FUNC INT GET_STAT_MEDAL_VALUE(FLOAT percentage)
	IF percentage = 0
		RETURN 0
	ENDIF

	IF percentage = 100.0
		RETURN 3//gold
	ELIF percentage > 50.0
		RETURN 2//silver
	ENDIF
	RETURN 1//bronze
ENDFUNC

/// PURPOSE:
///    Gets the hud colour of the medal passed in so it can be displayed in the pause menu
/// PARAMS:
///    iMedal - int of medal (1=gold, 2=silver, 3=bronze)
/// RETURNS:
///    HUD_COLOURS of medal
FUNC HUD_COLOURS GET_MEDAL_HUD_COLOUR(INT iMedal)
		
	HUD_COLOURS eMedalColour = HUD_COLOUR_BRONZE
	
	SWITCH iMedal
		CASE 3
			eMedalColour = HUD_COLOUR_GOLD
		BREAK
		CASE 2
			eMedalColour = HUD_COLOUR_SILVER
		BREAK
		CASE 1
			eMedalColour = HUD_COLOUR_BRONZE
		BREAK
	ENDSWITCH
	RETURN eMedalColour
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// accuracy stats
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    
FUNC INT GET_MISSION_STAT_STORED_VALUE(STATSENUM e)
	
	IF ENUM_TO_INT(e) = 0
		CPRINTLN(DEBUG_MISSION_STATS, "GET_MISSION_STAT_STORED_VALUE:<STATSET> stat unset")
		RETURN -1
	ENDIF
	
	INT InStat
	IF STAT_GET_INT(e,InStat)
		CPRINTLN(DEBUG_MISSION_STATS, "GET_MISSION_STAT_STORED_VALUE: for stat hash ", e , " got value ", InStat)
		RETURN InStat-1	//value offset so 0 is meaningful as unset
	ELSE
		RETURN -1
	ENDIF
ENDFUNC

/// PURPOSE:
///    please note that this is used only to read the accuracy values
///    mission stats are read with GET_MISSION_STAT_STORED_VALUE
/// PARAMS:
///    e - 
/// RETURNS:
///    
FUNC INT RETRIEVE_INT_STAT(STATSENUM e)
	INT istat
	STAT_GET_INT(e,istat)
	RETURN istat

ENDFUNC



PROC SET_MISSION_STAT_STORED_VALUE(INT value, ENUM_MISSION_STATS s)
	STATSENUM e = g_MissionStatTrackingPrototypes[s].statname
	IF ENUM_TO_INT(e) = 0
		CERRORLN(DEBUG_MISSION_STATS, "SET_MISSION_STAT_STORED_VALUE: enum conversion failed for ",g_MissionStatTrackingPrototypes[s].statname)
		EXIT
	ENDIF
	
	g_MissionStatTrackingPrototypes[s].currentvalue = value
	
	INT InStat
	InStat = value+1//value offset when stored so zero serverside is -1 or unset client side
	CPRINTLN(DEBUG_MISSION_STATS,"SET_MISSION_STAT_STORED_VALUE: Setting mission stat ",GET_MISSION_STAT_NAME(s)," to ",inStat)
	STAT_SET_INT(e,InStat)
	
	IF IS_REPEAT_PLAY_ACTIVE() AND g_bMissionStatSystemBuildingReplayStats
		//CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Adding value and index to replay stat buffer incase this is a replay, ", e, " : ", value, " : ", s)
		ADD_REPLAY_STAT_VALUE(ENUM_TO_INT(e))
		ADD_REPLAY_STAT_VALUE(value)
		ADD_REPLAY_STAT_VALUE(ENUM_TO_INT(s))
	ENDIF
ENDPROC



PROC RESET_MISSION_STATS_ACCURACY_STATE()

	g_bMissionStatAccuracyTallied = FALSE
	//reset the accuracy tracking
	//player one
	CPRINTLN(DEBUG_MISSION_STATS, "Reset accuracy metrics:")
	
	//INT g_iMissionStatsSP0Hits = 0
	//INT g_iMissionStatsSP0Shots = 0
	//INT g_iMissionStatsSP1Hits = 0
	//INT g_iMissionStatsSP1Shots = 0
	//INT g_iMissionStatsSP2Hits = 0
	//INT g_iMissionStatsSP2Shots = 0
	
	g_iMissionStatsSP0Shots = RETRIEVE_INT_STAT(SP0_SHOTS)
	g_iMissionStatsSP0Hits = RETRIEVE_INT_STAT(SP0_HITS)

	g_iMissionStatsSP1Shots = RETRIEVE_INT_STAT(SP1_SHOTS)
	g_iMissionStatsSP1Hits = RETRIEVE_INT_STAT(SP1_HITS)
	
	g_iMissionStatsSP2Shots = RETRIEVE_INT_STAT(SP2_SHOTS)
	g_iMissionStatsSP2Hits = RETRIEVE_INT_STAT(SP2_HITS)
	
	CPRINTLN(DEBUG_MISSION_STATS, "	Sp 0 hits/shots : ", g_iMissionStatsSP0Hits, "/", g_iMissionStatsSP0Shots)
	CPRINTLN(DEBUG_MISSION_STATS, "	Sp 1 hits/shots : ", g_iMissionStatsSP1Hits, "/", g_iMissionStatsSP1Shots)
	CPRINTLN(DEBUG_MISSION_STATS, "	Sp 2 hits/shots : ", g_iMissionStatsSP2Hits, "/", g_iMissionStatsSP2Shots)
	
	//SCRIPT_ASSERT("Accuracy metrics start")
ENDPROC



FUNC STRING GET_STRING_FROM_MISSION_STAT_TYPES(ENUM_MISSION_STAT_TYPES eType)
	SWITCH eType
		CASE MISSION_STAT_TYPE_UNSET							RETURN "UNSET"
		
		CASE MISSION_STAT_TYPE_TOTALTIME						RETURN "TOTALTIME"
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE					RETURN "ACTION_CAM_USE"
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL						RETURN "UNIQUE_BOOL"
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER					RETURN "WINDOWED_TIMER"
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD	RETURN "SINGLE_ENTITY_SPEED_THRESHOLD"
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD	RETURN "SINGLE_ENTITY_DAMAGE_THRESHOLD"
		CASE MISSION_STAT_TYPE_HEADSHOTS						RETURN "HEADSHOTS"
		CASE MISSION_STAT_TYPE_FRACTION							RETURN "FRACTION"
		CASE MISSION_STAT_TYPE_ACCURACY		 					RETURN "ACCURACY"
		CASE MISSION_STAT_TYPE_PURE_COUNT						RETURN "PURE_COUNT"
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE			RETURN "PURE_COUNT_PERCENTAGE"
		CASE MISSION_STAT_TYPE_FINANCE_DIRECT		 			RETURN "FINANCE_DIRECT"
		CASE MISSION_STAT_TYPE_FINANCE_TABLE					RETURN "FINANCE_TABLE"
		CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE				RETURN "PURE_COUNT_DISTANCE"
		CASE MISSION_STAT_TYPE_BULLETS_FIRED					RETURN "BULLETS_FIRED"
		CASE MISSION_STAT_TYPE_INNOCENTS_KILLED	 				RETURN "INNOCENTS_KILLED"
		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE				RETURN "SPECIAL_ABILITY_USE"
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str = "UNKNOWN_"
	str += ENUM_TO_INT(eType)
	
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
	#ENDIF
	#IF NOT IS_DEBUG_BUILD
	RETURN ""
	#ENDIF
ENDFUNC

//#IF USE_ASSASSIN_DLC
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//new assassination mission stat control////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//FUNC STRING GET_DLC_ASSASSINATION_TITLE_STRING(INT assassinationIndex)
//	
//	IF assassinationIndex = -1
//		RETURN "INVALID"
//	ENDIF
//	
//	SWITCH assassinationIndex
//		CASE 0	RETURN "M_DApol" BREAK
//		CASE 1	RETURN "M_DAret" BREAK
//		CASE 2	RETURN "M_DAcab" BREAK
//		CASE 3	RETURN "M_DAgen" BREAK
//		CASE 4	RETURN "M_DAsub" BREAK
//		
//		CASE 5	RETURN "M_DAdrg" BREAK
//		CASE 6	RETURN "M_DAhel" BREAK
//		CASE 7	RETURN "M_DAbar" BREAK
//		CASE 8	RETURN "M_DAstr" BREAK
//		CASE 9	RETURN "M_DAvin" BREAK
//		
//		CASE 10	RETURN "M_DAhnt" BREAK
//		CASE 11	RETURN "M_DAsky" BREAK
//	ENDSWITCH
//	
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_63 str = "M_INVALID_"
//	str += g_savedGlobalsDLC.iDLCAssassinationStage
//	str += " - "
//	str += assassinationIndex
//	
//	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
//	#ENDIF
//	#IF NOT IS_DEBUG_BUILD
//	RETURN ""
//	#ENDIF
//ENDFUNC






//FUNC BOOL GET_FORMATTING_TYPE_FOR_DLC_STAT(ENUM_MISSION_STAT_TYPES type, END_SCREEN_ELEMENT_FORMATTING &eFormatting)
//	CONST_INT iPLAYER_SINGLE_ENTITY_DAMAGE_THRESHOLD	180
//	
//	eFormatting = ESEF_NAME_ONLY
//	SWITCH type
////		CASE MISSION_STAT_TYPE_UNSET							eFormatting = UNSET"
////		
//		CASE MISSION_STAT_TYPE_TOTALTIME						eFormatting = ESEF_TIME_M_S BREAK
////		CASE MISSION_STAT_TYPE_ACTION_CAM_USE					eFormatting = ACTION_CAM_USE BREAK
//		CASE MISSION_STAT_TYPE_UNIQUE_BOOL						eFormatting = ESEF_NAME_ONLY BREAK
//		CASE MISSION_STAT_TYPE_WINDOWED_TIMER					eFormatting = ESEF_TIME_M_S BREAK
////		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD	eFormatting = SINGLE_ENTITY_SPEED_THRESHOLD BREAK
//		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD	eFormatting = ESEF_NAME_ONLY BREAK
//		CASE MISSION_STAT_TYPE_HEADSHOTS						eFormatting = ESEF_FRACTION BREAK
//		CASE MISSION_STAT_TYPE_FRACTION							eFormatting = ESEF_FRACTION BREAK
//		CASE MISSION_STAT_TYPE_ACCURACY		 					eFormatting = ESEF_RAW_PERCENT BREAK
//		CASE MISSION_STAT_TYPE_PURE_COUNT						eFormatting = ESEF_RAW_INTEGER BREAK
//		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE			eFormatting = ESEF_RAW_PERCENT BREAK
////		CASE MISSION_STAT_TYPE_FINANCE_DIRECT		 			eFormatting = FINANCE_DIRECT BREAK
////		CASE MISSION_STAT_TYPE_FINANCE_TABLE					eFormatting = FINANCE_TABLE BREAK
////		CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE				eFormatting = PURE_COUNT_DISTANCE BREAK
////		CASE MISSION_STAT_TYPE_BULLETS_FIRED					eFormatting = BULLETS_FIRED BREAK
////		CASE MISSION_STAT_TYPE_INNOCENTS_KILLED	 				eFormatting = INNOCENTS_KILLED BREAK
////		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE				eFormatting = SPECIAL_ABILITY_USE BREAK
//		
//		DEFAULT
//			TEXT_LABEL_63 str
//			str  = "type "
//			str += GET_STRING_FROM_MISSION_STAT_TYPES(type)
//			str += " has unknown formatting"
//			
//			CPRINTLN(DEBUG_MISSION_STATS, str)
//			SCRIPT_ASSERT(str)
//				
//			RETURN FALSE
//		BREAK
//	ENDSWITCH
//	
//	RETURN TRUE
//ENDFUNC


//FUNC BOOL GET_END_SCREEN_CHECK_MARK_FOR_DLC_STAT(DLC_ASSASSINATION_STATCONFIG &config, DLC_ASSASSINATION_STATS stat, INT iTarget, END_SCREEN_CHECK_MARK_STATUS &eESCM, INT &visibleSucceeded)
//	INT i
//	REPEAT config.iRegisteredStats i
//		IF config.target[i] = stat
//			
//			IF config.invalidationReason[i] != MSSIR_VALID
//				eESCM = ESCM_INVALIDATED
//				RETURN TRUE
//			ENDIF
//			
//			IF NOT g_DLCMissionStatTrackingPrototypes[stat].less_than_threshold
//			
//				CPRINTLN(DEBUG_MISSION_STATS, "screen check: ", GET_DLC_MISSION_STAT_DEBUG_NAME(stat), " value[ ",config.StatCurrentValue[i]," ] >= target [ ", iTarget, " ]")
//				
//				IF config.StatCurrentValue[i] >= iTarget
//					eESCM = ESCM_CHECKED
//					visibleSucceeded++
//				ELSE
//					eESCM = ESCM_UNCHECKED
//				ENDIF
//				
//				RETURN TRUE
//			ELSE
//				CPRINTLN(DEBUG_MISSION_STATS, "screen check: ", GET_DLC_MISSION_STAT_DEBUG_NAME(stat), " value[ ",config.StatCurrentValue[i]," ] < target [ ", iTarget, " ]")
//				
//				IF config.StatCurrentValue[i] < iTarget
//					eESCM = ESCM_CHECKED
//					visibleSucceeded++
//				ELSE
//					eESCM = ESCM_UNCHECKED
//				ENDIF
//				
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	
//	eESCM = ESCM_INVALIDATED
//	RETURN TRUE
//ENDFUNC



/// PURPOSE:
///    
/// PARAMS:
///    e - the stat to increment
///    number - the value to increment it by
///    overrideToValue - instead of incrementing by "number" override it to this value
//PROC INFORM_DLC_MISSION_STATS_OF_INCREMENT(DLC_ASSASSINATION_STATCONFIG &config, DLC_ASSASSINATION_STATS stat, INT number = 1, BOOL overrideToValue = FALSE)
//	#IF IS_DEBUG_BUILD
//		IF NOT IS_REPEAT_PLAY_ACTIVE()
//			IF NOT g_savedGlobals.sFlow.isGameflowActive
//				EXIT
//			ENDIF
//		ENDIF
//		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
//			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
//		ENDIF
//	#ENDIF
//	INT i = 0
//	
//	REPEAT config.iRegisteredStats i
//		IF config.target[i] = stat
//			IF overrideToValue
//				config.StatCurrentValue[i] = number
//				CPRINTLN(DEBUG_MISSION_STATS, "Setting mission stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(stat)," to ", number)
//			ELSE
//				config.StatCurrentValue[i] += number
//				CPRINTLN(DEBUG_MISSION_STATS, "Incrementing mission stat ",GET_DLC_MISSION_STAT_DEBUG_NAME(stat)," by ", number, " new value ",config.StatCurrentValue[i])
//			ENDIF
//			
//			EXIT
//		ENDIF
//	ENDREPEAT
//	
////	IF config.g_MissionStatTrackingPrototypes[config.target[i]].type = MISSION_STAT_TYPE_UNIQUE_BOOL
////		IF config.StatCurrentValue[i] > 1
////			config.StatCurrentValue[i] = 1
////		ENDIF
////		IF config.StatCurrentValue[i] < 0
////			config.StatCurrentValue[i] = 0
////		ENDIF
////	ENDIF
//	
//	
//	CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_OF_INCREMENT Specified stat not logged: ", GET_DLC_MISSION_STAT_NAME(stat), " by ", number)
//	DEBUG_PRINTCALLSTACK()
//	
//	SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_INCREMENT Specified stat not logged. If reporting this issue please complete the mission (s pass is fine) and note if the mission success dialog appears or not and what stats appear in it if it does. Thanks!")
//ENDPROC


//PROC UPDATE_DLC_ASSASSINATION_STATS(DLC_ASSASSINATION_STATCONFIG &config)		//UPDATE_STATS()
//	
//	FLOAT fTimeTaken = GET_TIMER_IN_SECONDS(config.missionTimer)
//	fTimeTaken *= 1000.0
//	
//	INT i = 0
//	CPRINTLN(DEBUG_MISSION_STATS, "Gathering stats from ", config.iRegisteredStats)
//	REPEAT config.iRegisteredStats i
//		INT j = -1, k = 0
//		
//		REPEAT MAX_TRACKED_MISSION_STATS k
//			IF config.target[i] = INT_TO_ENUM(DLC_ASSASSINATION_STATS, ENUM_TO_INT(g_MissionStatTrackingArray[k].target))
//				CPRINTLN(DEBUG_MISSION_STATS, "Found matching tracked stat array at ", k, ".")
//				
//				j = k
//			ENDIF
//		ENDREPEAT
//		
//		IF g_MissionStatTrackingArray[j].invalidationReason != MSSIR_VALID
//			CPRINTLN(DEBUG_MISSION_STATS, "MSSIR_VALID - ", GET_STRING_FROM_MISSION_STAT_TYPES(config.type[i]))
//			config.invalidationReason[i] = g_MissionStatTrackingArray[j].invalidationReason
//		ENDIF
//		
//		SWITCH config.type[i]
//			CASE MISSION_STAT_TYPE_TOTALTIME
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " has total time ", FLOOR(fTimeTaken), "ms")
//				INFORM_DLC_MISSION_STATS_OF_INCREMENT(config, config.target[i], FLOOR(fTimeTaken), TRUE)
//			BREAK
//			CASE MISSION_STAT_TYPE_UNIQUE_BOOL
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " has unique bool - let the mission script set it!")
//			BREAK
//			CASE MISSION_STAT_TYPE_FRACTION
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " has fraction - let the mission script set it!")
//			BREAK
//			CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]),
//						" has damage threshold for a single entity, inspect tracking array [", j,
//						"] - iTrackingDelta:", g_MissionStatTrackingArray[j].iTrackingDelta,
//						", ivalue:", g_MissionStatTrackingArray[j].ivalue)
//				
////				BOOL bFoundDamagedEntity
////				bFoundDamagedEntity = FALSE
//				
//				INFORM_DLC_MISSION_STATS_OF_INCREMENT(config, config.target[i], g_MissionStatTrackingArray[j].iTrackingDelta, TRUE)
////				bFoundDamagedEntity = TRUE
//			BREAK
//			CASE MISSION_STAT_TYPE_HEADSHOTS
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " has headshot tracked!")
//				
//				BOOL bFoundHeadshotEntity
//				bFoundHeadshotEntity = FALSE
//				
//				INT mHS
//				mHS = 0
//			
//				WHILE mHS < ENUM_TO_INT(MAX_MISSION_STATS)
//				AND NOT bFoundHeadshotEntity
//				
//					IF g_MissionStatTrackingPrototypes[mHS].type = MISSION_STAT_TYPE_HEADSHOTS
//						CPRINTLN(DEBUG_MISSION_STATS, "		Tracked prototype ", mHS, " is headshot [", g_MissionStatTrackingArray[j].ivalue, "]")
//						
//						INFORM_DLC_MISSION_STATS_OF_INCREMENT(config, config.target[i], g_MissionStatTrackingArray[j].ivalue, TRUE)
//						bFoundHeadshotEntity = TRUE
//					ELSE
//						CPRINTLN(DEBUG_MISSION_STATS, "		Tracked prototype ", mHS, " is something else (", GET_STRING_FROM_MISSION_STAT_TYPES(g_MissionStatTrackingPrototypes[mHS].type), "[", g_MissionStatTrackingArray[j].ivalue, "])!!!")
//					ENDIF
//					
//					mHS++
//				ENDWHILE
//				
//				IF NOT bFoundHeadshotEntity
//					SCRIPT_ASSERT("Stat has headshot tracked???")
//				ELSE
//					
//				ENDIF
//			BREAK
//			CASE MISSION_STAT_TYPE_WINDOWED_TIMER
//				TEXT_LABEL_63 strTW
//				strTW  = "Stat \""
//				#IF IS_DEBUG_BUILD
//				strTW += GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i])
//				#ENDIF
//				strTW  = "Stat \""
//				#IF NOT IS_DEBUG_BUILD
//				strTW += "DP_SOMETHING_"
//				strTW += ENUM_TO_INT(config.target[i])
//				#ENDIF
//				strTW += "\" has a timed window stat"
//				
//				
//				BOOL bFoundTimeWindow
//				bFoundTimeWindow = FALSE
//				
//				CPRINTLN(DEBUG_MISSION_STATS, "	Gathering tracked from ", g_iMissionStatsBeingTracked)
//				INT mTW
//				mTW = 0
//				
//				WHILE mTW < ENUM_TO_INT(MAX_MISSION_STATS)
//				AND NOT bFoundTimeWindow
//					
//					IF g_MissionStatTrackingPrototypes[mTW].type = MISSION_STAT_TYPE_WINDOWED_TIMER
//						CPRINTLN(DEBUG_MISSION_STATS, "	Tracked stat ", mTW, " is time window [", g_MissionStatTrackingArray[j].ivalue, "]")
//						
//						INFORM_DLC_MISSION_STATS_OF_INCREMENT(config, config.target[i], g_MissionStatTrackingArray[j].ivalue, TRUE)
//						bFoundTimeWindow = TRUE
//					ELSE
//						CPRINTLN(DEBUG_MISSION_STATS, "	Tracked stat ", mTW, " is something else (", ENUM_TO_INT(g_MissionStatTrackingPrototypes[mTW].type), ")!!!")
//					ENDIF
//					
//					mTW++
//				ENDWHILE
//				
//				IF NOT bFoundTimeWindow
//					CPRINTLN(DEBUG_MISSION_STATS, strTW)
//					SCRIPT_ASSERT(strTW)
//				ELSE
//					
//				ENDIF
//				
//			BREAK
//			
//			
//			CASE MISSION_STAT_TYPE_ACCURACY
//				CPRINTLN(DEBUG_MISSION_STATS, "Stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " is for accuracy???")
//				
//				INT totalShots, totalHits
//				totalShots = 0
//				totalHits = 0
//				
//				totalShots = (RETRIEVE_INT_STAT(SP0_SHOTS) - g_iMissionStatsSP0Shots)
//				totalHits = (RETRIEVE_INT_STAT(SP0_HITS) - g_iMissionStatsSP0Hits)
//				CPRINTLN(DEBUG_MISSION_STATS, "	Sp 0 hits/shots : ", totalHits, "/", totalShots)
//				
//				
//				totalShots += (RETRIEVE_INT_STAT(SP1_SHOTS) -g_iMissionStatsSP1Shots)
//				totalHits += (RETRIEVE_INT_STAT(SP1_HITS) - g_iMissionStatsSP1Hits)
//				CPRINTLN(DEBUG_MISSION_STATS, "	+Sp 1 hits/shots : ", totalHits, "/", totalShots)
//				
//				totalShots += (RETRIEVE_INT_STAT(SP2_SHOTS) -g_iMissionStatsSP2Shots)
//				totalHits += (RETRIEVE_INT_STAT(SP2_HITS) - g_iMissionStatsSP2Hits)
//				CPRINTLN(DEBUG_MISSION_STATS, "	+Sp 2 hits/shots : ", totalHits, "/", totalShots)
//				
//				//SCRIPT_ASSERT("Accuracy metrics end")
//				
//				//now use totalShots and totalHits to work out the mission accuracy
//			//	WAIT(0)
//				FLOAT fPercentage
//				fPercentage = 100.0
//				IF NOT (totalShots = 0)
//				
//					CPRINTLN(DEBUG_MISSION_STATS, "	totalHits/totalShots : ", totalHits, "/", totalShots)
//					
//					FLOAT ftotal, fhits
//					ftotal = TO_FLOAT(totalShots)
//					fhits = TO_FLOAT(totalHits)
//					
//					fPercentage = TO_FLOAT(FLOOR((fhits/ftotal)*100.0))
//					
//					IF fPercentage > 100.0
//						fPercentage = 100.0
//					ENDIF
//					
//					CPRINTLN(DEBUG_MISSION_STATS, "	fPercentage: ", fPercentage, "%")
//
//				ENDIF
//				
//			//	overallAccuracyMetric = fPercentage
//			//	overallBulletsMetric = totalShots
//				
//				CPRINTLN(DEBUG_MISSION_STATS, "	Tracked stat ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " has accuracy [", ROUND(fPercentage), "]")
//				INFORM_DLC_MISSION_STATS_OF_INCREMENT(config, config.target[i], ROUND(fPercentage), TRUE)
//				
//			BREAK
//			
//			DEFAULT
//				TEXT_LABEL_63 str
//				str  = "Stat \""
//				#IF IS_DEBUG_BUILD
//				str += GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i])
//				#ENDIF
//				str  = "Stat \""
//				#IF NOT IS_DEBUG_BUILD
//				str += "DP_SOMETHING_"
//				str += ENUM_TO_INT(config.target[i])
//				#ENDIF
//				str += "\" has an unknown type ["
//				str += GET_STRING_FROM_MISSION_STAT_TYPES(config.type[i])
//				str += "]??"
//				
//				CPRINTLN(DEBUG_MISSION_STATS, str)
//				SCRIPT_ASSERT(str)
//			BREAK
//		ENDSWITCH
//	ENDREPEAT
//	
//ENDPROC


//PROC UPLOAD_DATA_FOR_TRACKED_DLC_STAT(MISSION_STAT_SYSTEM_INVALIDATION_REASON invalidationReason, INT ivalue, DLC_ASSASSINATION_STATS target)
//	IF invalidationReason = MSSIR_VALID
//		CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_DLC_STAT - ivalue:", ivalue, " target:", GET_DLC_MISSION_STAT_DEBUG_NAME(target))
//		
////		IF(g_DLCMissionStatTrackingPrototypes[g_DLCMissionStatTrackingArray[index].target].less_than_threshold)
////			CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_DLC_STAT - lessthan check ",oldvalue," > ",g_DLCMissionStatTrackingArray[index].ivalue)
////			//should we log the stat? is it a record, or has it never been set?
////			IF oldvalue > g_DLCMissionStatTrackingArray[index].ivalue OR oldvalue < 0
////				CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_DLC_STAT - lessthan record, setting")
//				SET_DLC_MISSION_STAT_STORED_VALUE(ivalue, target)
////			ENDIF
////		ELSE
////			CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_DLC_STAT - greaterthan check ",g_DLCMissionStatTrackingArray[index].ivalue," > ",oldvalue)
////			IF g_DLCMissionStatTrackingArray[index].ivalue > oldvalue OR oldvalue < 0 
////				CPRINTLN(DEBUG_MISSION_STATS, "UPLOAD_DATA_FOR_TRACKED_DLC_STAT - greaterthan record, setting")
////				SET_DLC_MISSION_STAT_STORED_VALUE(g_DLCMissionStatTrackingArray[index].ivalue, g_DLCMissionStatTrackingArray[index].target)
////			ENDIF
////		ENDIF
//	ENDIF
//ENDPROC


//PROC MISSION_STAT_BLOCKING_END_OF_DLC_ASSASSINATION_SCREEN(DLC_ASSASSINATION_STATCONFIG &config)
//
//	//lodge the values into the stat system 
//	//BEGIN_REPLAY_STATS(47,47)
//	//ADD_REPLAY_STAT_VALUE(INT ValueOfStat)
//	//END_REPLAY_STATS()
//	
//	REQUEST_ADDITIONAL_TEXT("MISHSTA", MINIGAME_TEXT_SLOT)
//	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
//		CPRINTLN(DEBUG_MISSION_STATS, "Waiting additional text to load.")
//		WAIT(0)
//	ENDWHILE
//	
//	PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
//	WHILE NOT IS_MISSION_COMPLETE_READY_FOR_UI()
//		CPRINTLN(DEBUG_MISSION_STATS, "Waiting mission complete audio to be ready.")
//		WAIT(0)
//	ENDWHILE
//	
//	UPDATE_DLC_ASSASSINATION_STATS(config)
//	
//	END_SCREEN_DATASET esd
//	esd.bShowSkipperPrompt = TRUE
//	SET_ENDSCREEN_DATASET_HEADER(esd, "MISHPA", GET_DLC_ASSASSINATION_TITLE_STRING(config.dlcAssassinationIndex))
//	
//	INT visibleAttempted = config.iRegisteredStats
//	INT visiblesucceeded = 0
//	
//	INT i = 0
//	MISSION_STAT_SYSTEM_INVALIDATION_REASON eLastInvalidationReasonShown = MSSIR_VALID
//	
//	REPEAT config.iRegisteredStats i
//		INT iSuccessThreshold = g_DLCMissionStatTrackingPrototypes[config.target[i]].success_threshold
//		
//		END_SCREEN_CHECK_MARK_STATUS eEndScreenCheckMarkStatus = ESCM_CHECKED
//		
//		END_SCREEN_ELEMENT_FORMATTING eFormatting = ESEF_NAME_ONLY
//		IF GET_FORMATTING_TYPE_FOR_DLC_STAT(config.type[i], eFormatting)
//			
//			#IF IS_DEBUG_BUILD
//			INT iDebugTarget = -1
//			GET_TARGET_FOR_DLC_STAT(config.target[i], iDebugTarget)
//			BOOL bMatchDebugTarget = TRUE
//		
//			IF g_DLCMissionStatTrackingPrototypes[config.target[i]].type != config.type[i]
//				CASSERTLN(DEBUG_MISSION_STATS, "type mismatch ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " - ", GET_STRING_FROM_MISSION_STAT_TYPES(g_DLCMissionStatTrackingPrototypes[config.target[i]].type), " != ", GET_STRING_FROM_MISSION_STAT_TYPES(config.type[i]), "??")
//				bMatchDebugTarget = FALSE
//			ENDIF
//			IF iSuccessThreshold != iDebugTarget
//				CASSERTLN(DEBUG_MISSION_STATS, "target mismatch ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " - ", iSuccessThreshold, " != ", iDebugTarget, "??")
//				bMatchDebugTarget = FALSE
//			ENDIF
//			
//			IF bMatchDebugTarget
//				CERRORLN(DEBUG_MISSION_STATS, "type and target ", GET_DLC_MISSION_STAT_DEBUG_NAME(config.target[i]), " match.")
//			ENDIF
//			#ENDIF
//			
//			IF config.invalidationReason[i] = MSSIR_VALID
//				IF GET_END_SCREEN_CHECK_MARK_FOR_DLC_STAT(config, config.target[i], iSuccessThreshold, eEndScreenCheckMarkStatus, visiblesucceeded)
//					ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd, 
//						  eFormatting,
//						  GET_DLC_MISSION_STAT_NAME(config.target[i]),
//						  "",
//						  config.StatCurrentValue[i],
//						  iSuccessThreshold,
//						  eEndScreenCheckMarkStatus, 
//						  FALSE)
//				ENDIF
//			ELSE
//				STRING invalidationReason = "MTPHPERRET" 
//				SWITCH config.invalidationReason[i]
//					CASE MSSIR_CHEAT_ACTIVE	invalidationReason = "MTPHPERCHE" BREAK 
//					CASE MSSIR_SKIP			invalidationReason = "MTPHPERSKI" BREAK 
//					CASE MSSIR_TAXI_USED	invalidationReason = "MTPHPERTAX" BREAK 
//					CASE MSSIR_NOT_SET		invalidationReason = "MTPHPERNOREC" BREAK
//				ENDSWITCH
//				IF eLastInvalidationReasonShown = config.invalidationReason[i]
//					invalidationReason = ""
//				ENDIF
//				eLastInvalidationReasonShown = config.invalidationReason[i]
//
//				ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT(esd,
//						ESEF_RAW_STRING,
//						GET_DLC_MISSION_STAT_NAME(config.target[i]),
//						invalidationReason,
//						config.StatCurrentValue[i],
//						0,
//						ESCM_INVALIDATED)
//				
//			ENDIF
//		ENDIF
//		
//		UPLOAD_DATA_FOR_TRACKED_DLC_STAT(config.invalidationReason[i], config.StatCurrentValue[i], config.target[i])
//		
//		MISSION_STAT_RESET_STAT_REGISTER(i)
//		g_iMissionStatsBeingTracked--
//	ENDREPEAT
//	
//	config.fCompletionPercent = GET_MISSION_STAT_PERCENTAGE(visiblesucceeded, visibleattempted)
//	
//	STRING resultString = "MTPHPER"
//	END_SCREEN_MEDAL_STATUS ems = ESMS_NO_MEDAL
//	SWITCH GET_STAT_MEDAL_VALUE(config.fCompletionPercent)
//		CASE 3	//HUD_COLOUR_GOLD
//			resultString = "MTPHPER_G"
//			ems = ESMS_GOLD
//		BREAK
//		CASE 2	//HUD_COLOUR_SILVER
//			resultString = "MTPHPER_S"
//			ems = ESMS_SILVER
//		BREAK
//		CASE 1	// HUD_COLOUR_BRONZE
//			resultString = "MTPHPER_B"
//			ems = ESMS_BRONZE
//		BREAK
//	ENDSWITCH
//
//	//SET_ENDSCREEN_COMPLETION_LINE_STATE(esd,TRUE, "MTPHPER", ROUND(config.fCompletionPercent), 100,ESC_PERCENTAGE_COMPLETION,ESMS_NO_MEDAL)
//	SET_ENDSCREEN_COMPLETION_LINE_STATE(esd, TRUE, resultString, ROUND(config.fCompletionPercent),0, ESC_PERCENTAGE_COMPLETION, ems)
//	
//	
//	ENDSCREEN_PREPARE(esd, TRUE)	// blocks here until loaded
//	RENDER_ENDSCREEN(esd, TRUE)	 // blocks here until done
//	
//	ENDSCREEN_SHUTDOWN(esd)
//	
//	
//	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
//	g_bMissionOverStatTrigger = FALSE
//	
//	
//	
//ENDPROC


//#ENDIF

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Old stat system///////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////














PROC RESET_MISSION_REGISTERS_FROM_START()
        INT i = 0
        REPEAT g_iMissionStatsBeingTracked i
            MISSION_STAT_RESET_STAT_REGISTER(i)
        ENDREPEAT
ENDPROC


//yes public includes private in this case, is quite derp


//this was moved out to auto gen also to fix header cycle
FUNC INT GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS( ENUM_MISSION_STATS m )
	
	INT i = 0
	INT crewsum = 0
	SWITCH m
		CASE FH2A_CREW_TAKE
		CASE FH2B_CREW_TAKE
			REPEAT MAX_CREW_SIZE i
				IF g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].eCrewStatus[i] = CMST_FINE
					crewsum += g_savedGlobals.sHeistData.sEndScreenData[g_iMissionStatSystemHeistIndex].iCrewMemberTake[i]
				ENDIF
			ENDREPEAT
			CPRINTLN(DEBUG_MISSION_STATS, "CREWTAKE for 1631323: Found a total of ", crewsum)
			RETURN crewsum
	ENDSWITCH

    i = 0
    REPEAT g_iMissionStatsBeingTracked i
        IF g_MissionStatTrackingArray[i].target  = m
			CPRINTLN(DEBUG_MISSION_STATS, "GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS N: Returning value ",g_MissionStatTrackingArray[i].ivalue," from register ", i, " with enum index ",m)
 			RETURN g_MissionStatTrackingArray[i].ivalue
        ENDIF
    ENDREPEAT
	
	SCRIPT_ASSERT("GET_VALUE_OF_STAT_FROM_TRANSITORY_TRACKING_REGISTERS N: target not in the registers. Pass this bug to Default Levels.")
	RETURN 0
ENDFUNC

/*
ENUM MISSION_STAT_SYSTEM_TIME_WARP_SOURCE
	MSSTWS_TAXI
ENDENUM
*/


/// PURPOSE:
///    Prevent the autosave happening before the primed stats have been written to the profile
/// RETURNS:
///    
FUNC BOOL IS_MISSION_STAT_UPLOAD_PENDING()
	#if USE_CLF_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) < 1
			CPRINTLN(DEBUG_MISSION_STATS, "IS_MISSION_STAT_UPLOAD_PENDING: stat watcher not running, returning false")
			RETURN FALSE
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) < 1
			CPRINTLN(DEBUG_MISSION_STATS, "IS_MISSION_STAT_UPLOAD_PENDING: stat watcher not running, returning false")
			RETURN FALSE
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
			CPRINTLN(DEBUG_MISSION_STATS, "IS_MISSION_STAT_UPLOAD_PENDING: stat watcher not running, returning false")
			RETURN FALSE
		ENDIF
	#endif
	#endif
	
	IF NOT g_bMissionStatSystemPrimed
		CPRINTLN(DEBUG_MISSION_STATS, "IS_MISSION_STAT_UPLOAD_PENDING: no prime, returning false")
		RETURN FALSE
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "IS_MISSION_STAT_UPLOAD_PENDING: g_bMissionStatSystemUploadPending state ", g_bMissionStatSystemUploadPending)
	ENDIF
	RETURN g_bMissionStatSystemUploadPending
ENDFUNC




PROC RESET_STAT_SYSTEM_DEADPOOL()
	INT i = 0
	REPEAT STAT_DEADPOOL_DEPTH i
		g_StatSystemDeadpool[i].entar = NULL
		g_StatSystemDeadpool[i].statar = UNSET_MISSION_STAT_ENUM
	ENDREPEAT
	g_StatSystemDeadpoolEntries = 0
ENDPROC 

PROC RESET_STAT_SYSTEM_WEPPOOL()
	INT i = 0
	REPEAT STAT_WEPPOOL_DEPTH i
		g_StatSystemWeaponpool[i].statar = UNSET_MISSION_STAT_ENUM
	ENDREPEAT
	g_StatSystemWeponpoolEntries = 0
ENDPROC


/// PURPOSE:
///    Log a weapon that will then auto increment the given stat for each ped the player kills with it. 4 weapons may be logged at once.
/// PARAMS:
///    wep - the weapon
///    stat - the stat
PROC ADD_WEPPOOL_TRIGGER(WEAPON_TYPE wep,ENUM_MISSION_STATS stat)
	IF STAT_WEPPOOL_DEPTH = g_StatSystemWeponpoolEntries
			SCRIPT_ASSERT("ADD_WEPPOOL_TRIGGER: Wep pool has no space left")
		EXIT
	ENDIF
	CPRINTLN(DEBUG_MISSION_STATS, "ADD_WEPPOOL_TRIGGER: Registering wep ",wep," and stat ",stat)
	g_StatSystemWeaponpool[g_StatSystemWeponpoolEntries].weptar = wep
	g_StatSystemWeaponpool[g_StatSystemWeponpoolEntries].statar = stat
	++g_StatSystemWeponpoolEntries
	
ENDPROC



/// PURPOSE:
///    Log a ped that on death will trigger an increment of the given stat. 
/// PARAMS:
///    entity - 
///    stat - 
PROC ADD_DEADPOOL_TRIGGER(ENTITY_INDEX entity,ENUM_MISSION_STATS stat)

	
	//look for null entry
	INT i = 0
	REPEAT STAT_DEADPOOL_DEPTH i
		IF g_StatSystemDeadpool[i].entar = null
			g_StatSystemDeadpool[i].entar = entity
			g_StatSystemDeadpool[i].statar = stat
			IF i = g_StatSystemDeadpoolEntries
				++g_StatSystemDeadpoolEntries
			ENDIF
			EXIT
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_MISSION_STATS, "ADD_DEADPOOL_TRIGGER: dead pool has no space left")
ENDPROC


/// PURPOSE
///    invalidate time based stats due to time warp, this should only be used for things like the taxi time skips
///    or anything else that is not part of the mission that teleports the player or changes the time
///    it should only be used on things that the player can trigger.
PROC MISSION_STAT_SYSTEM_ALERT_TIME_WARP()//MISSION_STAT_SYSTEM_TIME_WARP_SOURCE source
	CPRINTLN(DEBUG_MISSION_STATS, "MISSION_STAT_SYSTEM_ALERT_TIME_WARP: On mission time warp stat invalidation check")
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		SWITCH g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type 
			CASE MISSION_STAT_TYPE_TOTALTIME
				g_MissionStatTrackingArray[i].invalidationReason = MSSIR_TAXI_USED
			BREAK
			CASE MISSION_STAT_TYPE_WINDOWED_TIMER
				IF g_bMissionStatTimeWindowGate
					g_MissionStatTrackingArray[i].invalidationReason = MSSIR_TAXI_USED
				ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC



PROC TRIGGER_MISSION_STATS_UI(BOOL IgnoreFadeMode = FALSE, BOOL SuppressFadeIn = FALSE)

	g_bEndScreenSuppressFadeIn = SuppressFadeIn
	IF g_bMissionOverStatTrigger
		CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: already triggered")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: stats UI triggered")
	
	IF g_bSuppressNextStatTrigger
		CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: suppressed by g_bSuppressNextStatTrigger")
		g_bSuppressNextStatTrigger = FALSE
		EXIT
	
	ENDIF
	#if USE_CLF_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) > 0
			PRINTSTRING("TRIGGER_MISSION_STATS_UI end of mission overlay triggered! ")
			IF (g_bMissionOverStatTrigger)
				PRINTSTRING("g_bMissionOverStatTrigger state is true already.")
			ELSE
				PRINTSTRING("g_bMissionOverStatTrigger state is false, setting to true.")
			ENDIF
			PRINTNL()
			g_MissionStatUIIgnoreFade = IgnoreFadeMode
			g_bMissionOverStatTrigger = TRUE
			g_bMissionStatSystemUploadPending =TRUE
			#IF IS_DEBUG_BUILD
				IF NOT g_bMissionStatSystemPrimed
					CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: Stat watcher wasn't primed, seeing if standalone splash was primed")
				ENDIF
			#ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI mission stats triggered without the stat wacher running. Cancelling request.")
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) > 0
			PRINTSTRING("TRIGGER_MISSION_STATS_UI end of mission overlay triggered! ")
			IF (g_bMissionOverStatTrigger)
				PRINTSTRING("g_bMissionOverStatTrigger state is true already.")
			ELSE
				PRINTSTRING("g_bMissionOverStatTrigger state is false, setting to true.")
			ENDIF
			PRINTNL()
			g_MissionStatUIIgnoreFade = IgnoreFadeMode
			g_bMissionOverStatTrigger = TRUE
			g_bMissionStatSystemUploadPending =TRUE
			#IF IS_DEBUG_BUILD
				IF NOT g_bMissionStatSystemPrimed
					CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: Stat watcher wasn't primed, seeing if standalone splash was primed")
				ENDIF
			#ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI mission stats triggered without the stat wacher running. Cancelling request.")
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 0
			PRINTSTRING("TRIGGER_MISSION_STATS_UI end of mission overlay triggered! ")
			IF (g_bMissionOverStatTrigger)
				PRINTSTRING("g_bMissionOverStatTrigger state is true already.")
			ELSE
				PRINTSTRING("g_bMissionOverStatTrigger state is false, setting to true.")
			ENDIF
			PRINTNL()
			g_MissionStatUIIgnoreFade = IgnoreFadeMode
			g_bMissionOverStatTrigger = TRUE
			g_bMissionStatSystemUploadPending =TRUE
			#IF IS_DEBUG_BUILD
				IF NOT g_bMissionStatSystemPrimed
					CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI: Stat watcher wasn't primed, seeing if standalone splash was primed")
				ENDIF
			#ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "TRIGGER_MISSION_STATS_UI mission stats triggered without the stat wacher running. Cancelling request.")
		ENDIF
	#endif
	#endif
	//SCRIPT_ASSERT("Stat watcher fired")
ENDPROC



PROC SET_TIME_TARGET_FOR_STAT_PROTOTYPE(ENUM_MISSION_STATS s, INT minutes,INT seconds)
	//work out the ms 
	g_MissionStatTrackingPrototypes[s].success_threshold = (minutes*60000) + (seconds*1000)
ENDPROC

PROC DO_MISSION_STAT_INITIAL_CONFIGURATION()
	INT initrep = 0
	MissionStatInfo msi 
	
#IF USE_NRM_DLC
	REPEAT MAX_MISSION_STATS_NRM initrep
#ENDIF
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_STATS_CLF initrep
#ENDIF
#IF NOT USE_SP_DLC
	REPEAT MAX_MISSION_STATS initrep
#ENDIF
		//.statname =  INT_TO_ENUM(STATSENUM, 0)
		ENUM_MISSION_STATS ems = INT_TO_ENUM(ENUM_MISSION_STATS,initrep)
		
		msi.statname = INT_TO_ENUM(STATSENUM, 0)
		INTERNAL_GENERATED_MISSION_STAT_CONFIGURE_TYPES_AND_VALUES_FOR_INDEX(ems, msi)
		g_MissionStatTrackingPrototypes[initrep].type = msi.type
		//g_MissionStatTrackingPrototypes[initrep].currentvalue = msi.currentvalue
		g_MissionStatTrackingPrototypes[initrep].success_threshold = msi.success_threshold
		g_MissionStatTrackingPrototypes[initrep].less_than_threshold = msi.less_than_threshold
		g_MissionStatTrackingPrototypes[initrep].statname = msi.statname
		g_MissionStatTrackingPrototypes[initrep].MinRange = msi.MinRange
		g_MissionStatTrackingPrototypes[initrep].MaxRange = msi.MaxRange
		g_MissionStatTrackingPrototypes[initrep].bHidden = msi.bHidden
		g_MissionStatTrackingPrototypes[initrep].lb_differentiator = msi.lb_differentiator
		g_MissionStatTrackingPrototypes[initrep].lb_weight_PPC = msi.lb_weight_PPC
		g_MissionStatTrackingPrototypes[initrep].lb_min_legal = msi.lb_min_legal
		g_MissionStatTrackingPrototypes[initrep].lb_max_legal = msi.lb_max_legal
		g_MissionStatTrackingPrototypes[initrep].lb_precedence = msi.lb_precedence
		
		g_MissionStatTrackingPrototypes[initrep].currentvalue = GET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingPrototypes[initrep].statname)
		CPRINTLN(DEBUG_MISSION_STATS, "DO_MISSION_STAT_INITIAL_CONFIGURATION: got initial value of ", g_MissionStatTrackingPrototypes[initrep].currentvalue , " for index ", initrep )
#IF USE_NRM_DLC
	ENDREPEAT
#ENDIF
#IF USE_CLF_DLC
	ENDREPEAT
#ENDIF
#IF NOT USE_SP_DLC
	ENDREPEAT
#ENDIF
	
	//now register stat associations with the flow.
	INTERNAL_GENERATED_ASSOCIATE_STATS_WITH_FLOW()
ENDPROC

FUNC FLOAT GET_RC_MS_PERCENTAGE(g_eRC_MissionIDs m)
	RETURN g_savedGlobals.sRandomChars.savedRC[m].fStatCompletion
ENDFUNC

PROC SET_RC_MS_PERCENTAGE(g_eRC_MissionIDs m, FLOAT fperc, BOOL bSkip = FALSE)
	IF bSkip
		g_savedGlobals.sRandomChars.savedRC[m].fStatCompletion = 50
	ELSE
		g_savedGlobals.sRandomChars.savedRC[m].fStatCompletion = fperc
	ENDIF
	
	// hey andy i added this boolean here so I can tell the achievement controller to check the medals
	// i'm not doing this by looping through the values in the repeat play menu - aaron
	g_bCheckSolidGoldBabyAchievement = TRUE
ENDPROC

FUNC FLOAT GET_SM_MS_PERCENTAGE(SP_MISSIONS m)
	RETURN g_savedGlobals.sFlow.missionSavedData[m].fStatCompletion
ENDFUNC

PROC SET_SM_MS_PERCENTAGE(SP_MISSIONS m, FLOAT fperc, BOOL bSkip = FALSE)
	
	
	#if USE_CLF_DLC
		IF bSkip
			g_savedGlobalsClifford.sFlow.missionSavedData[m].fStatCompletion = 50
		ELSE
			g_savedGlobalsClifford.sFlow.missionSavedData[m].fStatCompletion = fperc
		ENDIF
	#endif
	
	#if USE_NRM_DLC
		IF bSkip
			g_savedGlobalsnorman.sFlow.missionSavedData[m].fStatCompletion = 50
		ELSE
			g_savedGlobalsnorman.sFlow.missionSavedData[m].fStatCompletion = fperc
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF bSkip
			g_savedGlobals.sFlow.missionSavedData[m].fStatCompletion = 50
		ELSE
			g_savedGlobals.sFlow.missionSavedData[m].fStatCompletion = fperc
		ENDIF
	#endif
	#endif
	
	// hey andy i added this boolean here so I can tell the achievement controller to check the medals
	// i'm not doing this by looping through the values in the repeat play menu - aaron
	g_bCheckSolidGoldBabyAchievement = TRUE
ENDPROC

PROC LODGE_PERCENTAGE_FOR_CURRENT_MISSION(FLOAT fperc, BOOL bSkip = FALSE)
	FLOAT fLastPerc 
	
	IF g_LeaderboardLastMissionRCType
		//RC mission
		
		g_eRC_MissionIDs m = INT_TO_ENUM(g_eRC_MissionIDs,g_LeaderboardLastMissionID)
		fLastPerc = GET_RC_MS_PERCENTAGE(m)
		
		CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Lodging Percentage For RC Mission - Old %:", fLastPerc, " Current %:", fperc)
		IF (fLastPerc < 100.0)
			CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Previous Percentage is less than 100%")
			IF fperc >= 100.0
				++g_savedGlobals.sFlowCustom.iMissionGolds
			ENDIF
		ENDIF
		
		IF (fperc >= fLastPerc)
			SET_RC_MS_PERCENTAGE(m,fperc,bSkip)
		ENDIF
		
	ELIF g_LeaderboardLastMissionID = -1
		SCRIPT_ASSERT("LODGE_PERCENTAGE_FOR_CURRENT_MISSION - g_LeaderboardLastMissionID is null!!!")
		
	ELSE
		//regular mission
		CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Lodging Percentage For SP Mission ", g_LeaderboardLastMissionID, ".")
		
		SP_MISSIONS mi = INT_TO_ENUM(SP_MISSIONS,g_LeaderboardLastMissionID)
		fLastPerc = GET_SM_MS_PERCENTAGE(mi)
		CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Lodging Percentage For SP Mission - Old %:", fLastPerc, " Current %:", fperc)
		
		IF fLastPerc < 100.0
			CPRINTLN(DEBUG_ACHIEVEMENT, "ACH10 - Previous Percentage is less than 100%")
			IF fperc >= 100.0
				#if USE_CLF_DLC
					++g_savedGlobalsClifford.sFlowCustom.iMissionGolds
				#endif
				#if USE_NRM_DLC
					++g_savedGlobalsnorman.sFlowCustom.iMissionGolds
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					++g_savedGlobals.sFlowCustom.iMissionGolds
				#endif
				#endif
			ENDIF
		ENDIF
		
		IF (fperc >= fLastPerc)
			SET_SM_MS_PERCENTAGE(mi,fperc,bSkip)
		ENDIF
	ENDIF
ENDPROC



PROC CHECK_REPLAY_MISSION_STAT_STATE()

	//check for lodged values needing uploaded after replay
	IF HAVE_REPLAY_STATS_BEEN_STORED()
		IF GET_REPLAY_STAT_MISSION_ID() = 33
			IF GET_REPLAY_STAT_MISSION_TYPE() = 33
				INT statcount = GET_REPLAY_STAT_COUNT()
				CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Detected unprocessed replay stats, n : ", statcount, ".")

				INT OverridePerc = -1
				INT endpoint = statcount-4
				IF ((statcount-4)%3) = 0 //  4 stats signifying mission, percent, skip status and rc type
					statcount = (statcount-4)/3
					INT i = 0
					REPEAT statcount i
						INT value = GET_REPLAY_STAT_AT_INDEX((i*3)+1)
						ENUM_MISSION_STATS s = INT_TO_ENUM(ENUM_MISSION_STATS, GET_REPLAY_STAT_AT_INDEX((i*3)+2))
						
						CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Setting mission stat stored values: ", value, " : ", s, ".")

						SWITCH s
							CASE ASS4_MIRROR_PERCENTAGE		    		    		    
							CASE ASS5_MIRROR_PERCENT	
							CASE ASS3_MIRROR_PERCENTAGE	
							CASE ASS1_MIRROR_PERCENT
							CASE ASS2_MIRROR_PERCENT
								OverridePerc = value
								BREAK
						ENDSWITCH
						SET_MISSION_STAT_STORED_VALUE(value, s)
					ENDREPEAT	
				
					INT mishid = GET_REPLAY_STAT_AT_INDEX(endpoint)
					INT perc = GET_REPLAY_STAT_AT_INDEX(endpoint+1)
					BOOL skipStatus = FALSE
					IF GET_REPLAY_STAT_AT_INDEX(endpoint+2) > 0
						skipStatus = TRUE
					ENDIF
					BOOL rcType = FALSE
					IF GET_REPLAY_STAT_AT_INDEX(endpoint+3) > 0
						rcType = TRUE
					ENDIF

					g_LeaderboardLastMissionRCType = rcType
					g_LeaderboardLastMissionID = mishid
					IF OverridePerc = -1
						LODGE_PERCENTAGE_FOR_CURRENT_MISSION( TO_FLOAT(perc), skipStatus)
					ELSE
						LODGE_PERCENTAGE_FOR_CURRENT_MISSION( TO_FLOAT(OverridePerc), skipStatus)
					ENDIF
					
				ENDIF

				CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Making autosave request after updating mission replay stats.")
				MAKE_AUTOSAVE_REQUEST()
			ENDIF
#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Replay stats mission ID incorrect.")
#ENDIF
		ENDIF
		
		IF GET_REPLAY_STAT_MISSION_ID() = 13
			IF GET_REPLAY_STAT_MISSION_TYPE() = 37
				INT statcount = GET_REPLAY_STAT_COUNT()
				
				IF statcount = 3 // Rampage data found.
					INT iRampage = GET_REPLAY_STAT_AT_INDEX(0)
					INT iMedal = GET_REPLAY_STAT_AT_INDEX(1)
					INT iHighScore = GET_REPLAY_STAT_AT_INDEX(2)
					
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Found stored rampage data for rampage ", iRampage, ".")
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Medal rank ", iMedal,".")
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> High score ", iHighScore,".")
					g_savedGlobals.sRampageData.playerData[iRampage].iMedalIndex = iMedal
					g_savedGlobals.sRampageData.playerData[iRampage].iHighScore = iHighScore
					
					CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> Making autosave request after updating rampage replay stats.")
					MAKE_AUTOSAVE_REQUEST()
				ENDIF
			ENDIF
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<REPSTATS> no replay stats found")
	#ENDIF
	ENDIF
	
ENDPROC



PROC RESET_MISSION_STATS_ENTITY_WATCH()
	g_iEntityWatchListLoggedCount = 0
	
	
	INT i = 0
	
	REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES i
		g_EntityWatchList[i].index = NULL
	ENDREPEAT
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_CHECKPOINT()
	//TODO , bank any stats that have been succeeded that are not invalidated by replay

ENDPROC



PROC INFORM_MISSION_STATS_OF_FINANCE_MODEL(MODEL_NAMES name, INT value)
	IF g_iFinanceLookupLogged = MAX_FINANCE_LOOKUP_ENTRIES
		SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_FINANCE_MODEL maximum amount of entries logged")
		EXIT
	ENDIF
	
	g_FinanaceLookup[g_iFinanceLookupLogged].target = name
	g_FinanaceLookup[g_iFinanceLookupLogged].value = value
	
	
	
	++g_iFinanceLookupLogged
	
ENDPROC

PROC INFORM_MISSION_STATS_OF_FINANCE_WATCH_RESET()
	g_iFinanceLookupLogged = 0
ENDPROC
/*
PROC PRIME_MISSION_STATS_LEADBOARD_ENUMS(LEADERBOARDS_ENUM bestrun, LEADERBOARDS_ENUM indiv)
	//g_bMissionScoreLeadboardPrimed = TRUE

	g_eTargetMissionScoreLeaderboardBestRun = bestrun
	g_eTargetMissionScoreLeaderboardIndivRec = indiv

ENDPROC
*/

PROC RESET_MISSION_STATS_SYSTEM()

	
	IF g_bMissionStatSystemBlocker	
		CPRINTLN(DEBUG_MISSION_STATS, "RESET_MISSION_STATS_SYSTEM: blocked by g_bMissionStatSystemBlocker")
		EXIT
	ENDIF
	CPRINTLN(DEBUG_MISSION_STATS, "RESET_MISSION_STATS_SYSTEM: CALLED by script : ", GET_THIS_SCRIPT_NAME())
	//g_eTargetMissionScoreLeaderboardBestRun = LEADERBOARD_MINI_GAMES_TENNIS //Using tennis to signify
	//g_eTargetMissionScoreLeaderboardIndivRec = LEADERBOARD_MINI_GAMES_TENNIS //an invalid value for now
	//g_bMissionScoreLeadboardPrimed = FALSE
	//SCRIPT_ASSERT("None blocked stat sys reset")
	
	//g_LeaderboardLastMissionRCType = FALSE
	//g_LeaderboardLastMissionID = -1
	
	
	g_bMissionStatTimeWindowClosedForGood = FALSE
	g_iMissionStatsBeingTracked = 0
	//SCRIPT_ASSERT("Tracked stats reset")
	g_MissionStatSingleSpeedWatchEntity = NULL
	IF g_bMissionStatTimeWindowGate
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> time window closed by RESET_MISSION_STATS_SYSTEM")
	ENDIF
	g_bMissionStatTimeWindowGate = FALSE
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	RESET_MISSION_STATS_ENTITY_WATCH()
	g_bTrackingInnocentsLogged = FALSE
	g_bMissionStatTimeDeltaReset = TRUE
		
	RESET_MISSION_STATS_ACCURACY_STATE()
	INFORM_MISSION_STATS_OF_FINANCE_WATCH_RESET()
	
	RESET_STAT_SYSTEM_DEADPOOL()
	RESET_STAT_SYSTEM_WEPPOOL() 
	
	g_bMissionNoStatsNeedsSplashAndSting = FALSE
	g_MissionStatUIIgnoreFade = FALSE
	g_iMissionStatsStartTime = -1
	
ENDPROC

PROC RESET_MISSION_STATS_INVALIDATIONS()
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
	
		g_MissionStatTrackingArray[i].invalidationReason = MSSIR_VALID
	
	ENDREPEAT

ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_SHITSKIP()
	CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_SHITSKIP fired with ", g_iMissionStatsBeingTracked, " being tracked") 
	g_bMissionStatSystemResponseToShitskipNeeded = TRUE
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		g_MissionStatTrackingArray[i].invalidationReason = MSSIR_SKIP
	ENDREPEAT
	IF g_bMissionStatTimeWindowGate
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> time window closed by INFORM_MISSION_STATS_SYSTEM_OF_SHITSKIP")
	ENDIF
	g_bMissionStatTimeWindowGate = FALSE
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_RESTART()
	CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_RESTART:<STAT WATCHER> primed restart")
	g_bMissionStatSystemResponseToReplayNeeded = TRUE
	
	
	/*
	IF softReset
		CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_RESTART:<STAT WATCHER> Soft reset")
		g_bMissionStatSystemResponseToReplayNeeded = TRUE
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_RESTART:<STAT WATCHER> Hard reset ")
		//only reset the values in the stats registers to their defaults

			INT i = 0
			REPEAT g_iMissionStatsBeingTracked i
				MISSION_STAT_RESET_STAT_REGISTER(i)//,g_bMissionStatSystemResponseToShitskipNeeded)
			ENDREPEAT

		
	ENDIF*/
	IF g_bMissionStatTimeWindowGate
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> time window closed by INFORM_MISSION_STATS_SYSTEM_OF_RESTART")
	ENDIF
	g_bMissionStatTimeWindowGate = FALSE
ENDPROC

PROC CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART()
	
	IF NOT g_bMissionStatSystemResponseToReplayNeeded
		CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: failed response bool unset")
		EXIT
	ENDIF
	//invalidate stats that are unable to go cross replay
	IF g_iMissionStatsBeingTracked = 0
		CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: no stats being tracked, nothing to invalidate")
		EXIT
	ENDIF
	
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		SWITCH g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type
			CASE MISSION_STAT_TYPE_TOTALTIME
			CASE MISSION_STAT_TYPE_ACCURACY
				g_MissionStatTrackingArray[i].invalidationReason = MSSIR_CHECKPOINT
				CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: Invalidated time/accuracy tracking array element ", i, " \"", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "\"")
			BREAK
			CASE MISSION_STAT_TYPE_WINDOWED_TIMER
				IF g_bMissionStatTimeWindowGate
					g_MissionStatTrackingArray[i].invalidationReason = MSSIR_CHECKPOINT
					CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: Invalidated windowed timer element ", i, " \"", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "\"")
#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: Unopened windowed timer element ", i, " \"", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "\"")
#ENDIF
				ENDIF
			BREAK	
			CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
				IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].less_than_threshold
					IF g_MissionStatTrackingArray[i].ivalue != 0
						g_MissionStatTrackingArray[i].invalidationReason = MSSIR_CHECKPOINT
						CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: Invalidated damage less than element ", i, " \"", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), "\"")
#IF IS_DEBUG_BUILD
					ELSE
						CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: damage less than element non zero ",i)
#ENDIF
					ENDIF
#IF IS_DEBUG_BUILD
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: damage less than element not set ",i)
#ENDIF
				ENDIF
			BREAK
			
			
#IF IS_DEBUG_BUILD
			DEFAULT
				CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: type is ", GET_STRING_FROM_MISSION_STAT_TYPES(g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type), " - ",  i)
			BREAK
#ENDIF
		ENDSWITCH
		
		//g_MissionStatTrackingArray[i].invalidated=  TRUE
		
	ENDREPEAT
	
	CPRINTLN(DEBUG_MISSION_STATS, "CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART: complete ",i)
	g_bMissionStatSystemResponseToReplayNeeded = FALSE
	
ENDPROC

PROC INVALIDATE_STATS_IF_CHEATS_ACTIVE()

	//EXIT // TODO, add check for any cheats active
	/*
	INT g_iBitsetCheatsCurrentlyDisabled       // Bitset which tells the cheat controller which cheats are disabled this frame.  
	INT g_iBitsetCheatsCurrentlyActive         // Bitset which keeps track of which cheats are currently active.
	INT g_iBitsetCheatsUsedThisSession         // Bitset which keeps track of which cheats were used this session.
	*/

	IF g_iBitsetCheatsCurrentlyActive = 0
	AND !g_bHasAnyCheatBeenUsed
		EXIT
	ENDIF
	
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		g_MissionStatTrackingArray[i].invalidationReason = MSSIR_CHEAT_ACTIVE
	ENDREPEAT
	
ENDPROC
PROC TERMINATE_STAT_WATCHERCLF()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) > 0
	
		IF IS_MISSION_STAT_UPLOAD_PENDING() // it might be about to upload and terminate anyway
			IF g_bStatsInWatchingLoop AND (!g_bMissionOverStatTrigger) // its not been triggered and its sat in watching loop, kill it
				g_bStatsInWatchingLoop = FALSE
				TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watchCLF")
			ENDIF
		ELSE
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watchCLF")
		ENDIF
	ENDIF
ENDPROC
PROC TERMINATE_STAT_WATCHERNRM()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) > 0
	
		IF IS_MISSION_STAT_UPLOAD_PENDING() // it might be about to upload and terminate anyway
			IF g_bStatsInWatchingLoop AND (!g_bMissionOverStatTrigger) // its not been triggered and its sat in watching loop, kill it
				g_bStatsInWatchingLoop = FALSE
				TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watchNRM")
			ENDIF
		ELSE
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watchNRM")
		ENDIF
	ENDIF
ENDPROC
PROC TERMINATE_STAT_WATCHER()
	#if USE_CLF_DLC
		TERMINATE_STAT_WATCHERCLF()
		exit
	#endif
	#if USE_NRM_DLC
		TERMINATE_STAT_WATCHERNRM()
		exit
	#endif
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 0
	
		IF IS_MISSION_STAT_UPLOAD_PENDING() // it might be about to upload and terminate anyway
			IF g_bStatsInWatchingLoop AND (!g_bMissionOverStatTrigger) // its not been triggered and its sat in watching loop, kill it
				g_bStatsInWatchingLoop = FALSE
				TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watcher")
			ENDIF
		ELSE
			TERMINATE_ALL_SCRIPTS_WITH_THIS_NAME("mission_stat_watcher")
		ENDIF
	ENDIF
ENDPROC

PROC FLOW_PRIME_STATS_FOR_MISSION(SP_MISSIONS mi)
	CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Stats system priming for mission ", GET_SP_MISSION_DISPLAY_STRING_FROM_ID(mi), ".")
	
	BOOL bC2SpecialCase = FALSE
#if USE_CLF_DLC
	SP_MISSIONS m = mi
#endif

#if USE_NRM_DLC
	SP_MISSIONS m = mi
#endif

#if not USE_SP_DLC
	IF mi = SP_MISSION_FINALE_C1 
		g_bSuppressNextStatTrigger = TRUE
	ENDIF	
	
	SP_MISSIONS m = mi
	IF m = SP_MISSION_FINALE_CREDITS
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Priming cancelled early for credits script.")
		EXIT
	ENDIF		
	IF m = SP_MISSION_FINALE_C2
		CPRINTLN(DEBUG_MISSION_STATS, "Starting finale SP_MISSION_FINALE_C2, doing special case.")
		m = SP_MISSION_FINALE_C1 //Finale C2 uses the stats of C1		
		IF g_bMissionStatSystemPrimed
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: FINALE C2 triggered while system primed, just as planned.")
			EXIT
		ENDIF		
		bC2SpecialCase = TRUE
	ENDIF	
	IF (g_bMagDemoActive)
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Cannot prime stats system in mag demo mode!")
		RESET_MISSION_STATS_SYSTEM()
		EXIT
	ENDIF
#ENDIF

	
	g_sMissionStatsName = GET_SP_MISSION_NAME_LABEL(m)
	g_bMissionStatSystemHeistMode = FALSE
	g_MissionStatSystemSuppressVisual = FALSE
	IF IS_BIT_SET(g_sMissionStaticData[m].settingsBitset, MF_INDEX_NO_PASS)
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: This mission has no visible screen.")
		g_MissionStatSystemSuppressVisual = TRUE
	ENDIF
	
	
SWITCH m

#IF USE_CLF_DLC
  	CASE SP_MISSION_CLF_TRAIN
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is on the ignore list, bailing/terminating.")
		g_MissionStatSystemSuppressVisual = TRUE
		g_bMissionStatSystemResetFlag = TRUE
		RESET_MISSION_STATS_SYSTEM()
		TERMINATE_STAT_WATCHER()
  		EXIT
		BREAK
		
	CASE SP_MISSION_CLF_FIN
	CASE SP_MISSION_CLF_SPA_1
	CASE SP_MISSION_CLF_SPA_2
	CASE SP_MISSION_CLF_SPA_FIN
	CASE SP_MISSION_CLF_IAA_TRA
	CASE SP_MISSION_CLF_IAA_LIE
	CASE SP_MISSION_CLF_IAA_JET
	CASE SP_MISSION_CLF_IAA_HEL
	CASE SP_MISSION_CLF_IAA_DRO
	CASE SP_MISSION_CLF_IAA_RTS
	CASE SP_MISSION_CLF_KOR_PRO
	CASE SP_MISSION_CLF_KOR_RES
	CASE SP_MISSION_CLF_KOR_SUB
	CASE SP_MISSION_CLF_KOR_SAT
	CASE SP_MISSION_CLF_KOR_5
	CASE SP_MISSION_CLF_RUS_PLA
	CASE SP_MISSION_CLF_RUS_CAR
	CASE SP_MISSION_CLF_RUS_VAS
	CASE SP_MISSION_CLF_RUS_SAT
	CASE SP_MISSION_CLF_RUS_JET
	CASE SP_MISSION_CLF_RUS_CLK
	CASE SP_MISSION_CLF_ARA_1
	CASE SP_MISSION_CLF_ARA_DEF
	CASE SP_MISSION_CLF_ARA_FAKE
	CASE SP_MISSION_CLF_ARA_TNK
	CASE SP_MISSION_CLF_CAS_SET
	CASE SP_MISSION_CLF_CAS_PR1
	CASE SP_MISSION_CLF_CAS_PR2
	CASE SP_MISSION_CLF_CAS_PR3
	CASE SP_MISSION_CLF_CAS_HEI
	CASE SP_MISSION_CLF_MIL_DAM
	CASE SP_MISSION_CLF_MIL_PLA
	CASE SP_MISSION_CLF_MIL_RKT
	CASE SP_MISSION_CLF_JET_1
	CASE SP_MISSION_CLF_JET_2
	CASE SP_MISSION_CLF_JET_3
	CASE SP_MISSION_CLF_JET_REP
	CASE SP_MISSION_CLF_ASS_POL  
	CASE SP_MISSION_CLF_ASS_RET  
	CASE SP_MISSION_CLF_ASS_CAB 
	CASE SP_MISSION_CLF_ASS_GEN  
	CASE SP_MISSION_CLF_ASS_SUB  
	CASE SP_MISSION_CLF_ASS_HEL  
	CASE SP_MISSION_CLF_ASS_VIN  
	CASE SP_MISSION_CLF_ASS_HNT 
	CASE SP_MISSION_CLF_ASS_SKY  
	CASE SP_MISSION_CLF_RC_ALEX_GRND
	CASE SP_MISSION_CLF_RC_ALEX_AIR
	CASE SP_MISSION_CLF_RC_ALEX_UNDW
	CASE SP_MISSION_CLF_RC_ALEX_RWRD
	CASE SP_MISSION_CLF_RC_MEL_MONT
	CASE SP_MISSION_CLF_RC_MEL_AIRP
	CASE SP_MISSION_CLF_RC_MEL_WING
	CASE SP_MISSION_CLF_RC_MEL_DIVE
	CASE SP_MISSION_CLF_RC_AGN_BODY
	CASE SP_MISSION_CLF_RC_AGN_RCPT
	CASE SP_MISSION_CLF_RC_AGN_ESCP
	CASE SP_MISSION_CLF_RC_CLA_HACK
	CASE SP_MISSION_CLF_RC_CLA_DRUG
	CASE SP_MISSION_CLF_RC_CLA_FIN
	
  		//CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is on the ignore list, bailing/terminating.")
//		g_MissionStatSystemSuppressVisual = TRUE
//		g_bMissionStatSystemResetFlag = TRUE
//		RESET_MISSION_STATS_SYSTEM()
//		TERMINATE_STAT_WATCHER()
//  		EXIT
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission with no stats but end screen visual being primed.")
			g_bMissionNoStatsNeedsSplashAndSting = TRUE
			g_bMissionStatSystemPrimed = TRUE
			g_bMissionStatSystemMissionStarted = TRUE
	BREAK

	//	CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is a DLC assassination.")
//	BREAK
#ENDIF

#IF USE_NRM_DLC     	
	CASE SP_MISSION_NRM_SUR_START	
	CASE SP_MISSION_NRM_SUR_AMANDA	
	CASE SP_MISSION_NRM_SUR_TRACEY	
	CASE SP_MISSION_NRM_SUR_MICHAEL	
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is on the ignore list, bailing/terminating.")
		g_MissionStatSystemSuppressVisual = TRUE
		g_bMissionStatSystemResetFlag = TRUE
		RESET_MISSION_STATS_SYSTEM()
		TERMINATE_STAT_WATCHER()
  		EXIT
	CASE SP_MISSION_NRM_SUR_HOME			
	CASE SP_MISSION_NRM_SUR_JIMMY	
	CASE SP_MISSION_NRM_SUR_PARTY	
	CASE SP_MISSION_NRM_SUR_CURE	
	CASE SP_MISSION_NRM_RESCUE_ENG
	CASE SP_MISSION_NRM_RESCUE_MED
	CASE SP_MISSION_NRM_RESCUE_GUN
	CASE SP_MISSION_NRM_SUP_FUEL
	CASE SP_MISSION_NRM_SUP_AMMO
	CASE SP_MISSION_NRM_SUP_MEDS
	CASE SP_MISSION_NRM_SUP_FOOD
	CASE SP_MISSION_NRM_RADIO_A
	CASE SP_MISSION_NRM_RADIO_B
	CASE SP_MISSION_NRM_RADIO_C
  		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission with no stats but end screen visual being primed.")
		g_bMissionNoStatsNeedsSplashAndSting = TRUE
		g_bMissionStatSystemPrimed = TRUE
		g_bMissionStatSystemMissionStarted = TRUE
	BREAK
#ENDIF

#IF NOT USE_SP_DLC
		CASE SP_MISSION_PROLOGUE
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is the Proglogue which is followed by the intro title. Priming with supressed end screen.")
			g_bMissionStatSystemPrimed = TRUE
			g_MissionStatSystemSuppressVisual = TRUE
		BREAK


		CASE SP_MISSION_FINALE_A
		CASE SP_MISSION_FINALE_B
		CASE SP_MISSION_FINALE_C1
		CASE SP_MISSION_FINALE_C2
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is a finale which is followed by credits. Priming with supressed end screen.")
			g_bMissionStatSystemPrimed = TRUE
			g_MissionStatSystemSuppressVisual = TRUE
		BREAK
	
	
		CASE SP_MISSION_ASSASSIN_1
		CASE SP_MISSION_ASSASSIN_2
		CASE SP_MISSION_ASSASSIN_3
		CASE SP_MISSION_ASSASSIN_4
		CASE SP_MISSION_ASSASSIN_5
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is an assassination. Supressing visual as the scripts display their own pass screen.")
			g_bMissionStatSystemPrimed = TRUE
			g_MissionStatSystemSuppressVisual = TRUE
		BREAK
		

		CASE SP_MISSION_ME_AMANDA
		CASE SP_MISSION_ME_JIMMY
		CASE SP_MISSION_ME_TRACEY
	 		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission with no stats but end screen visual being primed.")
			g_bMissionNoStatsNeedsSplashAndSting = TRUE
			g_bMissionStatSystemPrimed = TRUE
			g_bMissionStatSystemMissionStarted = TRUE
		BREAK
		
			
		CASE SP_MISSION_FBI_4_INTRO
	    CASE SP_MISSION_FINALE_INTRO
	    CASE SP_MISSION_FINALE_CREDITS
	    CASE SP_MISSION_SHRINK_1
	    CASE SP_MISSION_SHRINK_2
	    CASE SP_MISSION_SHRINK_3
	    CASE SP_MISSION_SHRINK_4
	    CASE SP_MISSION_SHRINK_5
	    CASE SP_HEIST_FINALE_2_INTRO
	  		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is on the ignore list, bailing/terminating.")
			g_MissionStatSystemSuppressVisual = TRUE
			g_bMissionStatSystemResetFlag = TRUE
			RESET_MISSION_STATS_SYSTEM()
			TERMINATE_STAT_WATCHER()
	  		EXIT
		BREAK

        CASE SP_HEIST_JEWELRY_2     
        CASE SP_HEIST_DOCKS_2A
        CASE SP_HEIST_DOCKS_2B
        CASE SP_HEIST_RURAL_2
        CASE SP_HEIST_AGENCY_3A
        CASE SP_HEIST_AGENCY_3B
        CASE SP_HEIST_FINALE_2A
        CASE SP_HEIST_FINALE_2B
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Mission is a heist, setting flag and index.")
			g_iMissionStatSystemHeistIndex = GET_HEIST_ID_FROM_FINALE_MISSION_ID(m)
			g_bMissionStatSystemHeistMode = TRUE
			sEndScreen.bSformeUploadDone = FALSE
		BREAK
#ENDIF

	ENDSWITCH
	
	
	BOOL lastType = g_LeaderboardLastMissionRCType
	g_LeaderboardLastMissionRCType = FALSE
	
	
	INT last = g_LeaderboardLastMissionID
	g_LeaderboardLastMissionID = ENUM_TO_INT(m)
	
	
	IF NOT g_bMissionNoStatsNeedsSplashAndSting
		IF (g_LeaderboardLastMissionID != last) 
			OR (g_iMissionStatsBeingTracked = 0)
			OR (lastType != g_LeaderboardLastMissionRCType)
			
			g_bHasAnyCheatBeenUsed = FALSE
			RESET_MISSION_STATS_SYSTEM()
			
//			#IF USE_CLF_DLC
//				INTERNAL_FLOW_PRIME_STATS_FOR_DLC_MISSION(m)
//			#ENDIF
//			#IF USE_NRM_DLC
//				INTERNAL_FLOW_PRIME_STATS_FOR_DLC_MISSION(m)
//			#ENDIF
			#IF NOT USE_SP_DLC
				INTERNAL_FLOW_PRIME_STATS_FOR_MISSION(m)
			#ENDIF	
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Stat launch priming registers l = ",last,"c = ",g_LeaderboardLastMissionID, " :  rc typelast ", lastType, "  rc type current ", g_LeaderboardLastMissionRCType , " : tracked ", g_iMissionStatsBeingTracked)
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_MISSION: Stat launch with no register priming to maintain invalidation status")
			g_bMissionStatSystemPrimed = TRUE
		ENDIF
	ENDIF
	
	IF bC2SpecialCase
		//invalidate mission time due to checkpoint
		g_bMissionStatSystemResponseToReplayNeeded = TRUE //counts as a replay
	ENDIF
	
	g_iMissionStatsStartTime = GET_GAME_TIMER()
	
	g_bMissionStatSystemResetFlag = FALSE
	
	CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART()
	g_bMissionStatSystemBlocker = FALSE
ENDPROC


#if not USE_SP_DLC
PROC FLOW_PRIME_STATS_FOR_RC_MISSION(g_eRC_MissionIDs m)

	IF g_bMissionStatSystemResponseToShitskipNeeded = TRUE
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: Post shitskip!")
		INFORM_MISSION_STATS_SYSTEM_OF_SHITSKIP()
		g_bMissionStatSystemResponseToShitskipNeeded = FALSE
		IF g_bMissionStatSystemPrimed
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: No re prime after shitskip!")
			EXIT
		ENDIF
	ENDIF


	IF (g_bMagDemoActive)
		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: Cannot prime stats system in mag demo mode!")
		g_bMissionStatSystemResetFlag = TRUe
		RESET_MISSION_STATS_SYSTEM()
		EXIT
	ENDIF

	CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: Stats system priming for RC mission")
	g_sMissionStatsName = GET_RC_MISSION_NAME_LABEL(m)
	
	g_bMissionStatSystemHeistMode = FALSE
	g_MissionStatSystemSuppressVisual = FALSE

	
	SWITCH(m)
		
		// Any that need to trigger the splash and sting
		CASE RC_ABIGAIL_2
		CASE RC_EPSILON_1
			g_bMissionNoStatsNeedsSplashAndSting = TRUE
			g_bMissionStatSystemPrimed = TRUE
			g_bMissionStatSystemMissionStarted = TRUE
		BREAK
		
		CASE RC_ABIGAIL_1
		CASE RC_BARRY_3
	  	CASE RC_BARRY_4
	  	CASE RC_EPSILON_2
	  	CASE RC_EPSILON_3
	  	CASE RC_EPSILON_5
	  	CASE RC_EPSILON_7
	  	CASE RC_JOSH_1
		CASE RC_MAUDE_1
		CASE RC_MRS_PHILIPS_1
		CASE RC_MRS_PHILIPS_2
	  	CASE RC_NIGEL_1
	  	CASE RC_PAPARAZZO_3
	  	CASE RC_OMEGA_1
	  	CASE RC_OMEGA_2
		CASE RC_RAMPAGE_1
		CASE RC_RAMPAGE_2
		CASE RC_RAMPAGE_3
		CASE RC_RAMPAGE_4
		CASE RC_RAMPAGE_5
			g_bMissionStatSystemResetFlag = TRUE
			g_MissionStatSystemSuppressVisual = TRUE
	  		CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: mission is on the ignore list, bailing/terminating.")
			RESET_MISSION_STATS_SYSTEM()
			TERMINATE_STAT_WATCHER()
	  		EXIT
		BREAK
	ENDSWITCH
	
	
	BOOL lastType = g_LeaderboardLastMissionRCType
	g_LeaderboardLastMissionRCType = TRUE
	INT last = g_LeaderboardLastMissionID
	g_LeaderboardLastMissionID = ENUM_TO_INT(m)
	
	CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION: Launching for RC id ", m )
	IF NOT g_bMissionNoStatsNeedsSplashAndSting
	
	
		IF (g_LeaderboardLastMissionID != last) 
			OR (g_iMissionStatsBeingTracked = 0)
			OR (lastType != g_LeaderboardLastMissionRCType)
					
				g_bHasAnyCheatBeenUsed = FALSE	
				RESET_MISSION_STATS_SYSTEM()
				CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION:Stat launch priming registers l = ",last,"  c = ",g_LeaderboardLastMissionID, " :  rc typelast ", lastType, "  rc type current ", g_LeaderboardLastMissionRCType , " : tracked ", g_iMissionStatsBeingTracked)
				INTERNAL_FLOW_PRIME_STATS_FOR_RC_MISSION(m)
	

		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, "FLOW_PRIME_STATS_FOR_RC_MISSION:Stat launch with no register priming to maintain invalidation status")		
			g_bMissionStatSystemPrimed = TRUE
		ENDIF
		
		
	ENDIF
		
	g_iMissionStatsStartTime = GET_GAME_TIMER()
	
	CARRY_OUT_STATS_SYSTEM_RESPONSE_TO_RESTART()
	

	
	g_bMissionStatSystemBlocker = FALSE
ENDPROC
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//Stat system information

//Ingame cutscen e watch
PROC INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
	//If this changes, make sure to alter the call in cutscene_public.sch.
	g_bMissionStatSystemSequenceStatus = TRUE
ENDPROC
PROC INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
	//If this changes, make sure to alter the call in cutscene_public.sch.
	g_bMissionStatSystemSequenceStatus = FALSE
ENDPROC

//Duplicate version due to email confusion doh
PROC INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_START()
	g_bMissionStatSystemSequenceStatus = TRUE
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_END()
	g_bMissionStatSystemSequenceStatus = FALSE
ENDPROC

//Action cam watch
PROC INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_START()
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
	
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
			
		ENDIF
	#ENDIF
	g_bMissionStatSystemActionCamWatchStatus = TRUE
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_ACTION_CAM_END()
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	g_bMissionStatSystemActionCamWatchStatus = FALSE
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(ENUM_MISSION_STATS windowstat = UNSET_MISSION_STAT_ENUM,BOOL bZero = FALSE)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	g_eCurrentTimeWindowTarget = windowstat
	IF NOT g_bMissionStatTimeWindowGate
		g_bMissionStatTimeWindowGate = TRUE
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> Time window opened")
	ENDIF
	
	IF bZero
		INT i
		REPEAT g_iMissionStatsBeingTracked i
			IF g_MissionStatTrackingArray[i].target = windowstat
				g_MissionStatTrackingArray[i].ivalue = 0
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(BOOL bFinalClose = FALSE,
														ENUM_MISSION_STATS SpecificClose = UNSET_MISSION_STAT_ENUM)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF									
	IF g_bMissionStatTimeWindowGate
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> time window \"", SAFE_GET_MISSION_STAT_NAME(SpecificClose), "\" closed by INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED, bFinalClose: ", bFinalClose)
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> time window \"", SAFE_GET_MISSION_STAT_NAME(SpecificClose), "\" tried to be closed INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED, but it isn't open???, bFinalClose: ", bFinalClose)
	ENDIF
	
	g_bMissionStatTimeWindowGate = FALSE
	IF bFinalClose
		g_bMissionStatTimeWindowClosedForGood = TRUE
	ENDIF
	
	INT i = 0
	IF SpecificClose = UNSET_MISSION_STAT_ENUM
		//close all of them
		REPEAT g_iMissionStatsBeingTracked i
			IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type = MISSION_STAT_TYPE_WINDOWED_TIMER
				g_MissionStatTrackingArray[i].windowDeltaTracking = FALSE
				
				#IF USE_TU_CHANGES
				CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> stop tracking unset stat [", i, "] ", g_MissionStatTrackingArray[i].ivalue)
				EXIT
				#ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		//search the registers and close the specific one
		//close all of them
		REPEAT g_iMissionStatsBeingTracked i
			#IF USE_TU_CHANGES
			IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) > 0
			#ENDIF

				IF g_MissionStatTrackingArray[i].target = SpecificClose
					g_MissionStatTrackingArray[i].windowDeltaTracking = FALSE
					CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> stop tracking stat ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " [", i, "] ", g_MissionStatTrackingArray[i].ivalue)
					EXIT
				ENDIF
			#IF USE_TU_CHANGES
			ENDIF
			#ENDIF
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF SpecificClose = UNSET_MISSION_STAT_ENUM
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " = UNSET_MISSION_STAT_ENUM, repeat for ", g_iMissionStatsBeingTracked, ".")
		REPEAT g_iMissionStatsBeingTracked i
			IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type = MISSION_STAT_TYPE_WINDOWED_TIMER
				CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> g_MissionStatTrackingPrototypes[", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), ":", i, "].type = ", GET_STRING_FROM_MISSION_STAT_TYPES(g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type), " - same!!!")
			ELSE
				CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> g_MissionStatTrackingPrototypes[", SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target), ":", i, "].type = ", GET_STRING_FROM_MISSION_STAT_TYPES(g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type), " - different???")
			ENDIF
		ENDREPEAT
		CWARNINGLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() / was unsuccessfully logged.")
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " != UNSET_MISSION_STAT_ENUM, repeat for ", g_iMissionStatsBeingTracked, ".")
		REPEAT g_iMissionStatsBeingTracked i
			IF ENUM_TO_INT(g_MissionStatTrackingArray[i].target) > 0
				IF g_MissionStatTrackingArray[i].target = SpecificClose
					CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> g_MissionStatTrackingArray[", i, "].target = ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " - same!!!")
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> g_MissionStatTrackingArray[", i, "].target = ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " - different???")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION_STATS, "<MISSION STAT> g_MissionStatTrackingArray[", i, "].target = ", SAFE_GET_MISSION_STAT_NAME(SpecificClose), " - null$$$")
			ENDIF
		ENDREPEAT
		CWARNINGLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(", SAFE_GET_MISSION_STAT_NAME(SpecificClose), ") / was unsuccessfully logged.")
	ENDIF
	#ENDIF
ENDPROC

PROC INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(ENTITY_INDEX in, ENUM_MISSION_STATS specific = UNSET_MISSION_STAT_ENUM)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive) AND (in != NULL)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
			EXIT
		ENDIF
	#ENDIF
	g_MissionStatSingleSpeedWatchEntity = in
	g_MissionStatSpeedWatchEntitySpecific = specific
ENDPROC


PROC INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(ENTITY_INDEX in, ENUM_MISSION_STATS specificTarget = UNSET_MISSION_STAT_ENUM)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_bLoadedClifford
			AND NOT g_bLoadedNorman
				IF NOT g_savedGlobals.sFlow.isGameflowActive
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while gameflow not active.")
					EXIT
				ENDIF
			ELIF g_bLoadedClifford
				IF NOT g_savedGlobalsClifford.sFlow.isGameflowActive
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while CLF Trevor gameflow not active.")
					EXIT
				ENDIF
			ELIF g_bLoadedNorman
				IF NOT g_savedGlobalsnorman.sFlow.isGameflowActive
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while norman gameflow not active.")
					EXIT
				ENDIF
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive) AND (in != NULL)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed.")
			EXIT
		ENDIF
	#ENDIF
	
	//g_iMissionStatsBeingTracked
	//g_MissionStatTrackingArray[MAX_TRACKED_MISSION_STATS]
	g_MissionStatSingleDamageWatchEntity = in
	INT i
	REPEAT g_iMissionStatsBeingTracked i
		IF specificTarget = UNSET_MISSION_STAT_ENUM
		OR g_MissionStatTrackingArray[i].target = specificTarget
			IF g_MissionStatTrackingArray[i].TrackingEntity != in 
				g_MissionStatTrackingArray[i].TrackingEntity = in
				g_MissionStatTrackingArray[i].bTrackingEntityChanged = TRUE
				g_MissionStatTrackingArray[i].iTrackingDelta = 0

#IF IS_DEBUG_BUILD
				IF in != NULL
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform for damaged entity ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(in)), " in slot ", i)
				ELSE
					CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform for damaged entity NULL in slot ", i)
				ENDIF
#ENDIF	//	IS_DEBUG_BUILD
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


PROC INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ENTITY_INDEX in, BOOL ignoreDuplicates = FALSE)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
			EXIT
		ENDIF
	#ENDIF
	INT di = 0
	#IF IS_DEBUG_BUILD
		IF in = NULL
			SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: Entity passed in is null. ")
			EXIT
		ENDIF
		IF NOT DOES_ENTITY_EXIST(in)
			SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: Entity passed in does not exist. ")
			EXIT
		ENDIF
		
		REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES di
			IF g_EntityWatchList[di].index != NULL
				IF g_EntityWatchList[di].index = in
					IF ignoreDuplicates
						CPRINTLN(DEBUG_MISSION_STATS, "Mission passed a duplicate into INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY")
					ELSE
						SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: Entity is already in the list. ")
					ENDIF
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	#ENDIF

	IF ignoreDuplicates//do check for duplicates in release mode if this is the case
		REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES di
			IF g_EntityWatchList[di].index != NULL
				IF g_EntityWatchList[di].index = in
					EXIT
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF



	IF MAX_ENTITY_WATCH_LIST_ENTRIES = g_iEntityWatchListLoggedCount
		SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: No space left to log entity")
		EXIT
	ENDIF
	
	INT foundindex = -1
	INT i = 0
	
	WHILE (foundindex = -1) AND (i != MAX_ENTITY_WATCH_LIST_ENTRIES)
		IF g_EntityWatchList[i].index = NULL
			foundindex = i
		ENDIF
		++i
	ENDWHILE
	
	
	IF foundindex = -1
		SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: No free index found")
		EXIT
	ENDIF
	
	
	g_EntityWatchList[foundindex].index = in
	g_EntityWatchList[foundindex].type = MISSION_STAT_TYPE_HEADSHOTS
	
	CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY: add ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(in)), " [", NATIVE_TO_INT(in), "] to slot ", foundindex, ".")
	
	++g_iEntityWatchListLoggedCount
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    in - 
PROC REMOVE_HEADSHOT_WATCH_ENTITY_FROM_MISSION_STATS(ENTITY_INDEX in)
	
	INT di = 0
	REPEAT MAX_ENTITY_WATCH_LIST_ENTRIES di
		IF g_EntityWatchList[di].index = in
				
			g_EntityWatchList[di].index = NULL
			g_EntityWatchList[di].type = MISSION_STAT_TYPE_UNSET
			
			CPRINTLN(DEBUG_MISSION_STATS, "REMOVE_HEADSHOT_WATCH_ENTITY_FROM_MISSION_STATS: remove ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(in)), " [", NATIVE_TO_INT(in), "] from slot ", di, ".")
			
			--g_iEntityWatchListLoggedCount
			
			EXIT
		ENDIF
	ENDREPEAT
	
	SCRIPT_ASSERT("REMOVE_HEADSHOT_WATCH_ENTITY_FROM_MISSION_STATS - entity not in watchlist")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    e - the stat to increment
///    number - the value to increment it by
///    overrideToValue - instead of incrementing by "number" override it to this value
PROC INFORM_MISSION_STATS_OF_INCREMENT(ENUM_MISSION_STATS e, INT number = 1, BOOL overrideToValue = FALSE)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	INT i = 0
	
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingArray[i].target = e
			IF overrideToValue
				g_MissionStatTrackingArray[i].ivalue = number
				//CPRINTLN(DEBUG_MISSION_STATS, "Setting mission stat ",e," to ", number)
			ELSE
				g_MissionStatTrackingArray[i].ivalue += number
				//CPRINTLN(DEBUG_MISSION_STATS, "Incrementing mission stat ",e," by ", number, " new value ",g_MissionStatTrackingArray[i].ivalue)
			ENDIF
			
			EXIT
		ENDIF
	ENDREPEAT
	
#IF USE_TU_CHANGES
	IF g_MissionStatTrackingArray[i].target != UNSET_MISSION_STAT_ENUM
#ENDIF

		IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type = MISSION_STAT_TYPE_UNIQUE_BOOL
			IF g_MissionStatTrackingArray[i].ivalue > 1
				g_MissionStatTrackingArray[i].ivalue = 1
			ENDIF
			IF g_MissionStatTrackingArray[i].ivalue < 0
				g_MissionStatTrackingArray[i].ivalue = 0
			ENDIF
		ENDIF
		
#IF USE_TU_CHANGES
	ENDIF
#ENDIF
	
	
	#IF IS_DEBUG_BUILD
	SAVE_STRING_TO_DEBUG_FILE("look at ")
	SAVE_INT_TO_DEBUG_FILE(g_iMissionStatsBeingTracked)
	SAVE_STRING_TO_DEBUG_FILE(" stats for \"")
	SAVE_STRING_TO_DEBUG_FILE(SAFE_GET_MISSION_STAT_NAME(e))
	SAVE_STRING_TO_DEBUG_FILE("\"")
	SAVE_NEWLINE_TO_DEBUG_FILE()
	REPEAT g_iMissionStatsBeingTracked i
		SAVE_STRING_TO_DEBUG_FILE("	g_MissionStatTrackingArray[")
		SAVE_INT_TO_DEBUG_FILE(i)
		SAVE_STRING_TO_DEBUG_FILE("].target = ")
		SAVE_STRING_TO_DEBUG_FILE(SAFE_GET_MISSION_STAT_NAME(g_MissionStatTrackingArray[i].target))
		SAVE_NEWLINE_TO_DEBUG_FILE()
	ENDREPEAT
	SAVE_NEWLINE_TO_DEBUG_FILE()
	#ENDIF
	
	
	CPRINTLN(DEBUG_MISSION_STATS, "Failed incrementing mission stat ", SAFE_GET_MISSION_STAT_NAME(e) ," by ", number, " new value ",g_MissionStatTrackingArray[i].ivalue)
	SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_INCREMENT Specified stat not logged. If reporting this issue please complete the mission (s pass is fine) and note if the mission success dialog appears or not and what stats appear in it if it does. Thanks!")
ENDPROC


PROC INFORM_MISSION_STATS_OF_TIME_TOTAL_CHANGE(ENUM_MISSION_STATS e, INT ms)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_OF_TIME_TOTAL_CHANGE: Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingArray[i].target = e
				g_MissionStatTrackingArray[i].ivalue = ms
				CPRINTLN(DEBUG_MISSION_STATS, "INFORM_MISSION_STATS_OF_TIME_TOTAL_CHANGE: Setting mission stat ",e," to ", ms)
			EXIT
		ENDIF
	ENDREPEAT
	SCRIPT_ASSERT("INFORM_MISSION_STATS_OF_INCREMENT Specified stat not logged. If reporting this issue please complete the mission (s pass is fine) and note if the mission success dialog appears or not and what stats appear in it if it does. Thanks!")
ENDPROC

PROC INFORM_MISSION_STATS_OF_DISTANCE(ENUM_MISSION_STATS e, FLOAT distance)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingArray[i].target = e
				g_MissionStatTrackingArray[i].fvalue = distance
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

PROC INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(INT dollars)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		IF g_MissionStatTrackingPrototypes[g_MissionStatTrackingArray[i].target].type = MISSION_STAT_TYPE_FINANCE_DIRECT
			g_MissionStatTrackingArray[i].ivalue += dollars
		ENDIF
	ENDREPEAT
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//Stat system configuration


	
/// PURPOSE:
///    Tell the stat system a mission detected boolean stat has been successfully completed
/// PARAMS:
///    e - An enum mission stat that is set to boolean type in the mission stat system
PROC INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(ENUM_MISSION_STATS e)
	#IF IS_DEBUG_BUILD
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT g_savedGlobals.sFlow.isGameflowActive
				EXIT
			ENDIF
		ENDIF
		IF !g_bMissionStatSystemPrimed AND (!g_bMagDemoActive)
			CPRINTLN(DEBUG_MISSION_STATS, "Mission stats system inform call fired while stats system not primed. Pass this bug to Default Levels.")
		ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
		IF NOT g_MissionStatTrackingPrototypes[e].bHidden
			CPRINTLN(DEBUG_MISSION_STATS, GET_THIS_SCRIPT_NAME(), ": attempting register stat : ", SAFE_GET_MISSION_STAT_NAME(e), " \"", GET_STRING_FROM_TEXT_FILE(SAFE_GET_MISSION_STAT_NAME(e)),"\" - bool just happened")
		ELSE
			CPRINTLN(DEBUG_MISSION_STATS, GET_THIS_SCRIPT_NAME(), ": attempting register stat : STAT_MISS_HIDDEN_", ENUM_TO_INT(e), " - bool just happened")
		ENDIF
		
	#ENDIF
	
	g_bMissionStatSystemResetFlag = FALSE //incase this was not suppressed previously
	
	//check target is a bool stat
	IF NOT (g_MissionStatTrackingPrototypes[e].type = MISSION_STAT_TYPE_UNIQUE_BOOL)
		SCRIPT_ASSERT("INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED being called on a mission stat that is not a bool stat")
		EXIT
	ENDIF
	
	//check for stat being tracked
	BOOL set = FALSE
	INT i = 0
	REPEAT g_iMissionStatsBeingTracked i
		//if it is then set it
		IF g_MissionStatTrackingArray[i].target = e
			set = TRUE
			g_MissionStatTrackingArray[i].ivalue = 1
			g_MissionStatTrackingArray[i].fvalue = 0.0
			
			
			IF g_MissionStatTrackingArray[i].invalidationReason = MSSIR_SKIP
				//g_MissionStatTrackingArray[i].invalidationReason = MSSIR_VALID
				
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION_STATS, GET_THIS_SCRIPT_NAME(), ":Invalidating shitskipped stat: ", e)
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT set
		SCRIPT_ASSERT("INFORM_STAT_SYSTEM_OF_BOOL_STAT_SUCCESS called but no stat of type is in list!")
	ENDIF
ENDPROC


/// PURPOSE: Updated the last completed mission stat used to name saved games.
///    
PROC SET_LAST_COMPLETED_MISSION_STAT(STRING sMissionNameLabel, INT iPlayerInvolvedBitset)
	//Set character independent stats.
	STAT_SET_GXT_LABEL(SP_LAST_MISSION_NAME, sMissionNameLabel)
	
	//Set character specific stats. 
	IF IS_BIT_SET(iPlayerInvolvedBitset, ENUM_TO_INT(CHAR_MICHAEL))
		STAT_SET_GXT_LABEL(SP0_LAST_MISSION_NAME, sMissionNameLabel)
	ENDIF
	IF IS_BIT_SET(iPlayerInvolvedBitset, ENUM_TO_INT(CHAR_FRANKLIN))
		STAT_SET_GXT_LABEL(SP1_LAST_MISSION_NAME, sMissionNameLabel)
	ENDIF
	IF IS_BIT_SET(iPlayerInvolvedBitset, ENUM_TO_INT(CHAR_TREVOR))
		STAT_SET_GXT_LABEL(SP2_LAST_MISSION_NAME, sMissionNameLabel)
	ENDIF
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    e -  The mission stat enum to lookup
/// RETURNS:
///    The mission stat type
FUNC ENUM_MISSION_STAT_TYPES GET_MISSION_STAT_TYPE(ENUM_MISSION_STATS e)
	RETURN g_MissionStatTrackingPrototypes[e].type
ENDFUNC

FUNC BOOL Is_Mission_DLC_Mission(SP_MISSIONS eMissionIndex, INT iDLCStage)
	#IF USE_CLF_DLC
	eMissionIndex = eMissionIndex
		RETURN TRUE
	#ENDIF
	#IF USE_NRM_DLC
	eMissionIndex = eMissionIndex
		RETURN TRUE
	#ENDIF
	
	IF iDLCStage = 0
		RETURN FALSE
	ENDIF
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
		IF eMissionIndex != SP_MISSION_ASSASSIN_1
		AND eMissionIndex != SP_MISSION_ASSASSIN_2
		AND eMissionIndex != SP_MISSION_ASSASSIN_3
		AND eMissionIndex != SP_MISSION_ASSASSIN_4
		AND eMissionIndex != SP_MISSION_ASSASSIN_5
			RETURN FALSE
		ENDIF
		
	#endif
	#endif
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Used for display, handles unset values gracefully
/// PARAMS:
///    e -  The mission stat enum to lookup
/// RETURNS:
///    The mission stat
FUNC INT GET_MISSION_STAT_FRIENDLY_VALUE(
	ENUM_MISSION_STATS paramStat)

	INT i = GET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingPrototypes[paramStat].statname)//g_MissionStatTrackingPrototypes[e].currentvalue
	IF i > -2
		RETURN i
	ENDIF

	RETURN 0
ENDFUNC


PROC SET_SCALEFORM_DATA_FOR_TIME_STAT(INT paramValue, INT paramIndex, INT paramPassed, STRING paramNameLabel,
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	INT iMinutes = FLOOR(TO_FLOAT(paramValue)/60000.0)
	INT iSeconds = FLOOR(TO_FLOAT(paramValue)/1000.0) - (iMinutes*60)
	
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
		
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMinutes)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSeconds)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SET_SCALEFORM_DATA_FOR_BOOL_STAT(INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
		
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF

	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SET_SCALEFORM_DATA_FOR_PERCENTAGE_STAT(INT paramValue, INT paramMaxValue, INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	FLOAT fSuccessPercentage = (TO_FLOAT(paramValue)/TO_FLOAT(paramMaxValue))*100.0
	CPRINTLN(DEBUG_MISSION_STATS, "SET_SCALEFORM_DATA_FOR_PERCENTAGE_STAT: ",paramValue," / ",paramMaxValue," = ", fSuccessPercentage)
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
		
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CEIL(fSuccessPercentage))//1503127
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


PROC SET_SCALEFORM_DATA_FOR_FRACTION_STAT(INT paramValue, INT paramMaxValue, INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
	
	INT val = paramValue
	IF val > paramMaxValue
		val = paramMaxValue
	ENDIF
	CPRINTLN(DEBUG_MISSION_STATS, "SET_SCALEFORM_DATA_FOR_FRACTION_STAT: ",paramValue," -> ",val)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(val)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramMaxValue)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SCALEFORM_DATA_FOR_INT_STAT(INT paramValue, INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
		
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramValue)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


PROC SET_SCALEFORM_DATA_FOR_MONETARY_STAT(INT paramValue, INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
		
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(6)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramValue)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SET_SCALEFORM_DATA_FOR_INVALID_STAT(INT paramValue, INT paramIndex, INT paramPassed, STRING paramNameLabel, 
	ENUM_MISSION_STATS paramStat, SCALEFORM_INDEX paramMovie = NULL)
	IF paramMovie != NULL
		BEGIN_SCALEFORM_MOVIE_METHOD(paramMovie,"SET_DATA_SLOT")
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	ENDIF
	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramIndex)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramPassed)
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)			//Invalid stat type
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(paramValue)
	
	// show stat descriptions in pause menu
	IF paramMovie = NULL
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_RP_STATD")	
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramNameLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MISSION_STAT_DESCRIPTION(paramStat))
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(paramNameLabel)
	ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
	
endproc


FUNC BOOL GET_MISSION_STAT_PASS_STATUS_FOR_VALUE(ENUM_MISSION_STATS paramStat, INT value)
	#IF IS_DEBUG_BUILD
	IF NOT g_MissionStatTrackingPrototypes[paramStat].bHidden
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> GET_MISSION_STAT_PASS_STATUS_FOR_VALUE : stat ", SAFE_GET_MISSION_STAT_NAME(paramStat), " \"", GET_STRING_FROM_TEXT_FILE(GET_MISSION_STAT_NAME(paramStat)),"\" : value ", value)
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> GET_MISSION_STAT_PASS_STATUS_FOR_VALUE : stat ", ENUM_TO_INT(paramStat), ": value ", value)
	ENDIF
	#ENDIF
	
	IF value < 0
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> value smaller than zero")
		RETURN FALSE
	ENDIF
	
	//MissionStatInfo eStatInfo = 
	SWITCH g_MissionStatTrackingPrototypes[paramStat].type
		 //stat types that can never pass with zero or less than as the value
		CASE MISSION_STAT_TYPE_TOTALTIME
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
			IF value < 1
				CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> unset stat value. fail")
				RETURN FALSE
			ENDIF
			BREAK
	ENDSWITCH
	
	IF value = HIGHEST_INT
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> Stat shitskip value detected, invalidated")
		RETURN FALSE
	ENDIF

	//#1624877 - allow it to be any value greater than zero if Franklin owns the mod shop
	IF (paramStat = FINPC1_MOD_GAUNTLET OR paramStat = FINPC2_MOD_GAUNTLET OR paramStat = FINPC3_MOD_GAUNTLET)
	AND g_savedGlobals.sPropertyData.propertyOwnershipData[PROPERTY_CAR_MOD_SHOP].charOwner = GET_CURRENT_PLAYER_PED_ENUM()
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> Change stat for ", paramStat, " as Franklin owns a garage.")
		
		g_MissionStatTrackingPrototypes[paramStat].success_threshold = 0 
	ENDIF
	
		
	IF g_MissionStatTrackingPrototypes[paramStat].less_than_threshold
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> GET_MISSION_STAT_PASS_STATUS_FOR_VALUE lessthan : stat ",value," < ", g_MissionStatTrackingPrototypes[paramStat].success_threshold)
		IF value < g_MissionStatTrackingPrototypes[paramStat].success_threshold
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION_STATS, "<STATSET> GET_MISSION_STAT_PASS_STATUS_FOR_VALUE more or equal : stat ",value," >= ", g_MissionStatTrackingPrototypes[paramStat].success_threshold)
		IF value >= g_MissionStatTrackingPrototypes[paramStat].success_threshold
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC INT GET_MISSION_STAT_PASS_STATUS(ENUM_MISSION_STATS paramStat)

	//Generic calculation to see if this stat has met its passed criteria.	
	IF GET_MISSION_STAT_PASS_STATUS_FOR_VALUE(paramStat, GET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingPrototypes[paramStat].statname))
		RETURN 1
	ENDIF
	RETURN 0

ENDFUNC


FUNC BOOL SET_SCALEFORM_DATA_FOR_MISSION_STAT(ENUM_MISSION_STATS paramStat, INT paramIndex, SCALEFORM_INDEX paramMovie = NULL)

	IF paramMovie != NULL
		IF NOT HAS_SCALEFORM_MOVIE_LOADED(paramMovie)
			SCRIPT_ASSERT("SET_SCALEFORM_DATA_FOR_MISSION_STAT: Command was called before the required scaleform was loaded. Aborting.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	MissionStatInfo sStatInfo
	INT iPassed
	
	IF g_MissionStatTrackingPrototypes[paramStat].bHidden
		SCRIPT_ASSERT("Called on a stat with hidden setting, this will probably result in undefined stats appearing in the list. Pass this bug to Default Levels..")
		RETURN TRUE//hidden stats don't have ingame strings, skip them. But don't count as failed
	ENDIF
	
	sStatInfo = g_MissionStatTrackingPrototypes[paramStat]
	iPassed = GET_MISSION_STAT_PASS_STATUS(paramStat)
	SWITCH paramStat
		CASE ASS1_MIRROR_TIME				
		CASE ASS1_MIRROR_CASH				
		CASE ASS1_MIRROR_PERCENT				
		CASE ASS1_MIRROR_MEDAL	
		CASE ASS2_MIRROR_TIME						
		CASE ASS2_MIRROR_PERCENT				
		CASE ASS2_MIRROR_MEDAL				
		CASE ASS2_MIRROR_CASH	
		CASE ASS3_MIRROR_TIME				
		CASE ASS3_MIRROR_CASH				
		CASE ASS3_MIRROR_PERCENTAGE			
		CASE ASS3_MIRROR_MEDAL	
		CASE ASS4_MIRROR_TIME				
		CASE ASS4_MIRROR_CASH				
		CASE ASS4_MIRROR_PERCENTAGE		    
		CASE ASS4_MIRROR_MEDAL	
		CASE ASS5_MIRROR_TIME 			    
		CASE ASS5_MIRROR_CASH			    
		CASE ASS5_MIRROR_PERCENT			    
		CASE ASS5_MIRROR_MEDAL	
			iPassed = -1
			BREAK
	ENDSWITCH
	
	INT paramValue
	STRING paramNameLabel
	
	paramValue = GET_MISSION_STAT_FRIENDLY_VALUE(paramStat)
	paramNameLabel = GET_MISSION_STAT_NAME(paramStat)

	CDEBUG1LN(debug_mission_stats,"Stat val ",paramValue," ind ",paramIndex," pas ",iPassed," nameL ",paramNameLabel," stat ",ENUM_TO_INT(paramStat))
	//B* 1762462, 1927403: Mark invalidated stats as not passed, gray-out
	IF paramValue = -1
		SET_SCALEFORM_DATA_FOR_INVALID_STAT(	paramValue, paramIndex,0,paramNameLabel,paramStat,paramMovie)
		iPassed = 0
	ELSE
	//Operate based on stat type.
	SWITCH sStatInfo.type
		CASE MISSION_STAT_TYPE_TOTALTIME
			SET_SCALEFORM_DATA_FOR_TIME_STAT(	paramValue, 
												paramIndex, 
												iPassed, 
												"MTTIME",
												paramStat,
												paramMovie)
		BREAK
		CASE MISSION_STAT_TYPE_ACTION_CAM_USE
			SET_SCALEFORM_DATA_FOR_TIME_STAT(	paramValue, 
												paramIndex, 
												iPassed,
												paramNameLabel,
												paramStat,
												paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_UNIQUE_BOOL
			SET_SCALEFORM_DATA_FOR_BOOL_STAT(	paramIndex, 
												iPassed,
												paramNameLabel,
												paramStat,
												paramMovie)
		BREAK
		CASE MISSION_STAT_TYPE_SPECIAL_ABILITY_USE
		CASE MISSION_STAT_TYPE_WINDOWED_TIMER
			SET_SCALEFORM_DATA_FOR_TIME_STAT(	paramValue, 
												paramIndex, 
												iPassed,
												paramNameLabel,
												paramStat,
												paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_SPEED_THRESHOLD
			SET_SCALEFORM_DATA_FOR_PERCENTAGE_STAT(	paramValue, 
													sStatInfo.success_threshold, 
													paramIndex, 
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_SINGLE_ENTITY_DAMAGE_THRESHOLD
			SET_SCALEFORM_DATA_FOR_BOOL_STAT(	paramIndex, 
												iPassed,
												paramNameLabel,
												paramStat,
												paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_HEADSHOTS
			SET_SCALEFORM_DATA_FOR_FRACTION_STAT(	paramValue, 
													sStatInfo.success_threshold, 
													paramIndex, 
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_FRACTION
			SET_SCALEFORM_DATA_FOR_FRACTION_STAT(	paramValue, 
													sStatInfo.success_threshold, 
													paramIndex, 
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
		
		CASE MISSION_STAT_TYPE_ACCURACY
			SET_SCALEFORM_DATA_FOR_PERCENTAGE_STAT(	paramValue, 
													100,//sStatInfo.success_threshold, 
													paramIndex, 
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
		CASE MISSION_STAT_TYPE_INNOCENTS_KILLED
		CASE MISSION_STAT_TYPE_PURE_COUNT
		CASE MISSION_STAT_TYPE_PURE_COUNT_DISTANCE //TODO this needs it's own scaleform upload call
		CASE MISSION_STAT_TYPE_BULLETS_FIRED
			SET_SCALEFORM_DATA_FOR_INT_STAT(paramValue,
											paramIndex,
											iPassed,
											paramNameLabel,
											paramStat,
											paramMovie)
		BREAK
		CASE MISSION_STAT_TYPE_PURE_COUNT_PERCENTAGE
			SET_SCALEFORM_DATA_FOR_PERCENTAGE_STAT(	paramValue, 
													100, 
													paramIndex, 
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
			
		CASE MISSION_STAT_TYPE_FINANCE_DIRECT
		CASE MISSION_STAT_TYPE_FINANCE_TABLE
			SET_SCALEFORM_DATA_FOR_MONETARY_STAT(	paramValue,
													paramIndex,
													iPassed,
													paramNameLabel,
													paramStat,
													paramMovie)
		BREAK
		
		
		CASE MISSION_STAT_TYPE_UNSET
			CPRINTLN(DEBUG_MISSION_STATS, "Missing stat type ", sStatInfo.type ," for stat ", paramNameLabel, " with index ", paramStat)
			SCRIPT_ASSERT("SET_SCALEFORM_DATA_FOR_MISSION_STAT: Stat passed had an unset type. Bug Andrew K!")
		BREAK
		
	ENDSWITCH
	
	ENDIF
	
	IF iPassed = 0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
		
ENDFUNC




FUNC FLOAT SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS_ESD(END_SCREEN_DATASET &esd,
													INT visiblesucceeded,INT visibleattempted, BOOL bSkipDetected)
													//MISSION_STAT_SYSTEM_INVALIDATION_REASON reason = MSSIR_CHECKPOINT)
	//
	CPRINTLN(DEBUG_MISSION_STATS, "Setting percentage and medal using SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS_ESD")

	FLOAT fUploadPercentage = GET_MISSION_STAT_PERCENTAGE(visiblesucceeded,visibleattempted)
	INT medal = GET_STAT_MEDAL_VALUE(fUploadPercentage)
	

	
	IF bSkipDetected
		fUploadPercentage = 50
		medal = 1
	ENDIF
	
	
	IF(fUploadPercentage = 0)
		SET_ENDSCREEN_COMPLETION_LINE_STATE(esd, TRUE, "MTPHPER", 0,0,
				ESC_PERCENTAGE_COMPLETION, ESMS_NO_MEDAL)
		RETURN fUploadPercentage
	ENDIF
	
	
	END_SCREEN_MEDAL_STATUS ems = ESMS_NO_MEDAL

	SWITCH GET_MEDAL_HUD_COLOUR(medal)
		CASE HUD_COLOUR_GOLD
			ems = ESMS_GOLD
		BREAK
		CASE HUD_COLOUR_SILVER
			ems = ESMS_SILVER
		BREAK
		CASE HUD_COLOUR_BRONZE
			ems = ESMS_BRONZE
		BREAK
	ENDSWITCH

	//TEXT_LABEL showstring = "MTPHPER"
		
	SWITCH GET_MEDAL_HUD_COLOUR(medal)
		CASE HUD_COLOUR_GOLD
			SET_ENDSCREEN_COMPLETION_LINE_STATE(esd,
												TRUE,
												 "MTPHPER",//_G",
												 ROUND(fUploadPercentage),0,
												 ESC_PERCENTAGE_COMPLETION,
												 ems)
		BREAK
		CASE HUD_COLOUR_SILVER
			SET_ENDSCREEN_COMPLETION_LINE_STATE(esd,
												TRUE,
												 "MTPHPER",//_S",
												 ROUND(fUploadPercentage),0,
												 ESC_PERCENTAGE_COMPLETION,
												 ems)
		BREAK
		CASE HUD_COLOUR_BRONZE
			SET_ENDSCREEN_COMPLETION_LINE_STATE(esd,
												TRUE,
												 "MTPHPER",//_B",
												 ROUND(fUploadPercentage),0,
												 ESC_PERCENTAGE_COMPLETION,
												 ems)
		BREAK
	ENDSWITCH
	

										
	RETURN fUploadPercentage
ENDFUNC



FUNC FLOAT SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS(INT visiblesucceeded,INT visibleattempted, BOOL bSkipDetected,
													 INT iOverridePercent, INT iOverrideMedal)
													//BOOL purerun,
													//MISSION_STAT_SYSTEM_INVALIDATION_REASON reason = MSSIR_CHECKPOINT)
	CPRINTLN(DEBUG_MISSION_STATS, "Setting percentage and medal using SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS")

	FLOAT fUploadPercentage = GET_MISSION_STAT_PERCENTAGE(visiblesucceeded,visibleattempted)
	INT medal = GET_STAT_MEDAL_VALUE(fUploadPercentage)
	
	
	
		IF bSkipDetected
			fUploadPercentage = 50
			medal = 1
		ENDIF
		
		IF iOverrideMedal > -1
			medal = iOverrideMedal
			CPRINTLN(DEBUG_MISSION_STATS, "Medal overriden to : ",medal)
		ENDIF
		
		IF iOverridePercent > -1
			fUploadPercentage = TO_FLOAT(iOverridePercent)
			CPRINTLN(DEBUG_MISSION_STATS, "Percentage overriden to : ",fUploadPercentage)
		ENDIF
		
		CDEBUG1LN(debug_mission_stats,"Miss totals: visSuc ",visiblesucceeded,", visAtempt ",visibleAttempted,", p% ",fUploadPercentage,", medal ",medal,", skip",bSkipDetected)
		
		IF medal = 0
		
			BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fUploadPercentage)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MTPHPER")
			END_SCALEFORM_MOVIE_METHOD()
		
		ELSE
		// normal version is now SET_SCALEFORM_DATA_FOR_MISSION_STAT_TOTALS_ESD
			// pause menu version
			BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DESCRIPTION")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(GET_MEDAL_HUD_COLOUR(medal)))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fUploadPercentage)
			//IF purerun
				//SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MTPHPER")
				SWITCH GET_MEDAL_HUD_COLOUR(medal)
					CASE HUD_COLOUR_GOLD
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MTPHPER_G")
					BREAK
					CASE HUD_COLOUR_SILVER
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MTPHPER_S")
					BREAK
					CASE HUD_COLOUR_BRONZE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MTPHPER_B")
					BREAK
				ENDSWITCH
			//ELSE
				//SET_SCALEFORM_UPLOAD_STAT_INVALIDATION_REASON_STRING(reason)
			//ENDIF
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
	RETURN fUploadPercentage
ENDFUNC




#IF IS_DEBUG_BUILD

PROC DEBUG_SET_MISSION_STAT_TO_COMPLETED_VALUE(ENUM_MISSION_STATS paramStat)

	//Naive first pass implementation!
	g_MissionStatTrackingPrototypes[paramStat].currentvalue = g_MissionStatTrackingPrototypes[paramStat].success_threshold
	IF g_MissionStatTrackingPrototypes[paramStat].less_than_threshold
		g_MissionStatTrackingPrototypes[paramStat].currentvalue--	
	ENDIF
	
	//B* 1870062: Set overridden assassination pass stats to 100%
	SWITCH paramStat
		CASE ASS1_MIRROR_PERCENT
		CASE ASS2_MIRROR_PERCENT
		CASE ASS3_MIRROR_PERCENTAGE
		CASE ASS4_MIRROR_PERCENTAGE
		CASE ASS5_MIRROR_PERCENT
			g_MissionStatTrackingPrototypes[paramStat].currentvalue = 100
		BREAK
	ENDSWITCH
	
	SWITCH paramStat
		CASE ASS1_MIRROR_MEDAL 
		CASE ASS2_MIRROR_MEDAL
		CASE ASS3_MIRROR_MEDAL
		CASE ASS4_MIRROR_MEDAL
		CASE ASS5_MIRROR_MEDAL
			g_MissionStatTrackingPrototypes[paramStat].currentvalue = 3
		BREAK
	ENDSWITCH
	//stored value that was previously missing
	SET_MISSION_STAT_STORED_VALUE(g_MissionStatTrackingPrototypes[paramStat].currentvalue, paramStat)
	
ENDPROC

#ENDIF

