//
//	script_MISC.sch
//
//  common drawing procedures/functions used in the game.
//
//  see this wiki section for more info on the commands.
//  https://www.someoneaddlinkhere.com
//

USING "commands_HUD.sch"
USING "commands_Misc.sch"

//PURPOSE: sets an int to store the values of an RGBA
FUNC INT INT_COLOUR(INT red, INT green, INT blue, INT iAlpha)
	RETURN ((red*16777216) + (green*65536) + (blue*256) + (iAlpha))
ENDFUNC

//PURPOSE: gets the alpha value from the int
FUNC INT INT_ALPHA(INT colour)
	RETURN GET_BITS_IN_RANGE(colour, 0, 7)
ENDFUNC

//PURPOSE: gets the blue value from the int
FUNC INT INT_BLUE(INT colour)
	RETURN GET_BITS_IN_RANGE(colour, 8, 15)
ENDFUNC

//PURPOSE: gets the green value from the int
FUNC INT INT_GREEN(INT colour)
	RETURN GET_BITS_IN_RANGE(colour, 16, 23)
ENDFUNC

//PURPOSE: gets the red value from the int
FUNC INT INT_RED(INT colour)
	RETURN GET_BITS_IN_RANGE(colour, 24, 31)
ENDFUNC

//PURPOSE: gets the width that the text will take up when drawn
FUNC FLOAT GET_TEXT_WIDTH(STRING text, FLOAT scaleX, FLOAT scaleY)
	SET_TEXT_FONT(FONT_STANDARD)
	SET_TEXT_SCALE(scaleX, scaleY)
	RETURN GET_STRING_WIDTH(text)
ENDFUNC

//#NO_WIKI
//PURPOSE: sets up the text for the display commands
PROC setupDisplayText(FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT backshadow, INT backshadowColour, TEXT_FONTS eFont = FONT_STANDARD)
	SET_TEXT_FONT(eFont)
	SET_TEXT_SCALE(scaleX, scaleY)
	SET_TEXT_WRAP(wrapL, wrapR)
	SET_TEXT_COLOUR(INT_RED(colour), INT_GREEN(colour), INT_BLUE(colour), INT_ALPHA(colour))
	SET_TEXT_DROPSHADOW(backshadow, INT_RED(backshadowColour), INT_GREEN(backshadowColour), INT_BLUE(backshadowColour), INT_ALPHA(backshadowColour))
ENDPROC
//#END_NO_WIKI

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT(posX, posY, text)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_NUMBER(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT iNumber, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD, INT bStereo = 0)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_NUMBER(posX, posY, text, iNumber, bStereo)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_STRING(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, STRING withString, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_STRING(posX, posY, text, withString)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_2_STRINGS(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, STRING withString, STRING withString2, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_TWO_STRINGS(posX, posY, text, withString, withString2)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_2_NUMBERS(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT iNumber1, INT iNumber2, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_2_NUMBERS(posX, posY, text, iNumber1, iNumber2)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_3_NUMBERS(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT iNumber1, INT iNumber2, INT iNumber3, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_3_NUMBERS(posX, posY, text, iNumber1, iNumber2, iNumber3)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_PLAYER_NAME(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, STRING playerName, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	DISPLAY_TEXT_WITH_PLAYER_NAME(posX, posY, text, playerName, HUD_COLOUR_PURE_WHITE)
ENDPROC

//PURPOSE: draws the text label to the screen
PROC DISPLAY_TEXT_LABEL_WITH_STRING_AND_NUMBER(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, STRING withString, INT iNumber, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	//DISPLAY_TEXT_WITH_STRING_AND_NUMBER(posX, posY, text, withString, iNumber)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(text)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(withString)
		ADD_TEXT_COMPONENT_INTEGER(iNumber)
	END_TEXT_COMMAND_DISPLAY_TEXT(posX, posY)
	
ENDPROC


/// PURPOSE:
///    Prints a help message with 2 numbers in it (to replace the ~1~ tokens in your string table string.
/// NOTES:
///    This was previously in commands_hud.sch, alongside all of the other "PRINT_HELP_WITH_****" functions.
///    However, code wanted this function moved out of the native headers.
PROC PRINT_HELP_WITH_TWO_NUMBERS(STRING pTextLabel, INT NumberToDisplay1, INT NumberToDisplay2)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay1)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay2)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE)
ENDPROC

PROC PRINT_HELP_FOREVER_WITH_TWO_NUMBERS(STRING pTextLabel, INT NumberToDisplay1, INT NumberToDisplay2)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay1)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay2)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, TRUE)
ENDPROC

PROC PRINT_HELP_FOREVER_WITH_TWO_NUMBERS_NO_SOUND(STRING pTextLabel, INT NumberToDisplay1, INT NumberToDisplay2)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay1)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay2)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, FALSE)
ENDPROC

PROC PRINT_HELP_WITH_NUMBER_AND_STRING(STRING pTextLabel, INT NumberToDisplay, STRING SubStringTextLabel)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE)
ENDPROC

PROC PRINT_HELP_FOREVER_WITH_NUMBER_AND_STRING(STRING pTextLabel, INT NumberToDisplay, STRING SubStringTextLabel)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, TRUE, TRUE)
ENDPROC

PROC PRINT_HELP_WITH_4_STRINGS(STRING TextLabel, STRING SubStringTextLabel1, STRING SubStringTextLabel2, STRING SubStringTextLabel3, STRING SubStringTextLabel4,INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel1)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel2)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel3)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel4)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE,iOverrideTime)
ENDPROC

PROC PRINT_HELP_WITH_6_STRINGS(STRING TextLabel, STRING SubStringTextLabel1, STRING SubStringTextLabel2, STRING SubStringTextLabel3, STRING SubStringTextLabel4, STRING SubStringTextLabel5, STRING SubStringTextLabel6,INT iOverrideTime = -1)
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(TextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel1)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel2)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel3)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel4)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel5)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel6)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE,iOverrideTime)
ENDPROC


FUNC BOOL IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED(STRING pTextLabel, INT NumberToDisplay1, INT NumberToDisplay2)
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay1)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay2)
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC


FUNC BOOL IS_THIS_HELP_MESSAGE_WITH_NUMBER_AND_STRING_BEING_DISPLAYED(STRING pTextLabel, INT NumberToDisplay, STRING SubStringTextLabel)
	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(pTextLabel)
		ADD_TEXT_COMPONENT_INTEGER(NumberToDisplay)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubStringTextLabel)
	RETURN END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
ENDFUNC


#IF IS_DEBUG_BUILD
//PURPOSE: draws some literal text to the screen
PROC DEBUG_DISPLAY_LITERAL_TEXT(STRING text, FLOAT posX, FLOAT posY, FLOAT scaleX, FLOAT scaleY, FLOAT wrapL, FLOAT wrapR, INT colour, INT backshadow = 0, INT backshadowColour = 0, TEXT_FONTS eFont = FONT_STANDARD)
	setupDisplayText(scaleX, scaleY, wrapL, wrapR, colour, backshadow, backshadowColour, eFont)
	SET_TEXT_USE_UNDERSCORE(TRUE)
	DISPLAY_TEXT_WITH_LITERAL_STRING(posX, posY, "STRING", text)
	SET_TEXT_USE_UNDERSCORE(FALSE)
ENDPROC
#ENDIF	//	IS_DEBUG_BUILD

