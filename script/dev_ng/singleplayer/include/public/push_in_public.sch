//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   push_in_public.sch                                   						//
//      AUTHOR          :   Rob Bray                                                    				//
//      DESCRIPTION     :   Common functions for push in cams											//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_vehicle.sch" 
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_physics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_audio.sch"
USING "script_player.sch"
USING "script_misc.sch"

CONST_INT PUSH_IN_INTERP_TIME 1000
CONST_INT PUSH_IN_CUT_TIME 1000
CONST_INT PUSH_IN_POSTFX_TIME 700
CONST_INT PUSH_IN_SPEED_UP_TIME 700
CONST_FLOAT PUSH_IN_DISTANCE 1.5
CONST_FLOAT PUSH_IN_SPEED_UP_PROPORTION 0.2

ENUM PUSH_IN_STATE
	PUSH_IN_NOT_CREATED = 0,
	PUSH_IN_RUNNING,
	PUSH_IN_DONE
ENDENUM

STRUCT PUSH_IN_DATA
	PUSH_IN_STATE state
	// for both
	CAMERA_INDEX startCam
	CAMERA_INDEX endCam
	
	// for spline only
	CAMERA_INDEX splineCam
	CAMERA_INDEX midCam
	
	ENTITY_INDEX targetEntity
	enumCharacterList toCharacter
	INT iCreateTime
	BOOL bDonePostFX
	VECTOR vDirectionMod
	
	FLOAT fDistance
	FLOAT fSpeedUpProportion
	INT iInterpTime
	INT iCutTime
	INT iPostFXTime
	INT iSpeedUpTime
ENDSTRUCT

PROC DESTROY_PUSH_IN_CAMS(PUSH_IN_DATA &pushInData)
	IF DOES_CAM_EXIST(pushInData.startCam)
		DESTROY_CAM(pushInData.startCam)
	ENDIF
	IF DOES_CAM_EXIST(pushInData.endCam)
		DESTROY_CAM(pushInData.endCam)
	ENDIF
	IF DOES_CAM_EXIST(pushInData.splineCam)
		DESTROY_CAM(pushInData.splineCam)
	ENDIF
	IF DOES_CAM_EXIST(pushInData.midCam)
		DESTROY_CAM(pushInData.midCam)
	ENDIF
ENDPROC

PROC FILL_PUSH_IN_DATA(PUSH_IN_DATA &pushInData, ENTITY_INDEX targetEntity, enumCharacterList toCharacter, FLOAT fDistance = PUSH_IN_DISTANCE, INT iInterpTime = PUSH_IN_INTERP_TIME, INT iCutTime = PUSH_IN_CUT_TIME, INT iPostFXTime = PUSH_IN_POSTFX_TIME, INT iSpeedUpTime = 0, FLOAT fSpeedUpProportion = PUSH_IN_SPEED_UP_PROPORTION)
	pushInData.targetEntity = targetEntity
	pushInData.toCharacter = toCharacter
	pushInData.fDistance = fDistance
	pushInData.iInterpTime = iInterpTime
	pushInData.iCutTime = iCutTime
	pushInData.iPostFXTime = iPostFXTime
	pushInData.iSpeedUpTime = iSpeedUpTime
	pushInData.fSpeedUpProportion = fSpeedUpProportion
ENDPROC

PROC RESET_PUSH_IN(PUSH_IN_DATA &pushInData)
	DESTROY_PUSH_IN_CAMS(pushInData)
	pushInData.state = PUSH_IN_NOT_CREATED
	pushInData.vDirectionMod = <<0,0,0>>
ENDPROC

PROC SET_PUSH_IN_DIRECTION_MODIFIER(PUSH_IN_DATA &pushInData, VECTOR vDirectionMod)
	pushInData.vDirectionMod = vDirectionMod
ENDPROC

FUNC BOOL HANDLE_PUSH_IN(PUSH_IN_DATA &pushInData, BOOL bCamsAttached = FALSE, BOOL bDestroyCamsAtEnd = TRUE, BOOL bDoPostFX = TRUE, BOOL bOnlyAttachStartIfCamsAttached = FALSE, BOOL bDoColouredFlash = TRUE, CAM_SPLINE_SMOOTHING_FLAGS customSmoothingFlags = CAM_SPLINE_NO_SMOOTH )
	SWITCH pushInData.state
		CASE PUSH_IN_NOT_CREATED
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
			IF NOT IS_ENTITY_DEAD(pushInData.targetEntity)
				DESTROY_PUSH_IN_CAMS(pushInData)
				
				VECTOR vRenderingCamPos
				vRenderingCamPos = GET_FINAL_RENDERED_CAM_COORD()
				IF bCamsAttached
					vRenderingCamPos += GET_ENTITY_VELOCITY(pushInData.targetEntity) * GET_FRAME_TIME()
				ENDIF
				
				VECTOR vRenderingCamOffset 
				vRenderingCamOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(pushInData.targetEntity, vRenderingCamPos)
				VECTOR vRenderingCamRot 
				vRenderingCamRot = GET_FINAL_RENDERED_CAM_ROT()
				VECTOR vModifiedRenderingCamRot 
				vModifiedRenderingCamRot = vRenderingCamRot+ pushInData.vDirectionMod
				VECTOR vRenderingCamOffsetRot 
				vRenderingCamOffsetRot = vModifiedRenderingCamRot - GET_ENTITY_ROTATION(pushInData.targetEntity) 
				VECTOR vRenderingCamDirection
				vRenderingCamDirection = <<-SIN(vModifiedRenderingCamRot.z) * COS(vModifiedRenderingCamRot.x), COS(vModifiedRenderingCamRot.z) * COS(vModifiedRenderingCamRot.x), SIN(vModifiedRenderingCamRot.x)>> 
				VECTOR vRenderingCamAttachedDirection 
				vRenderingCamAttachedDirection = <<-SIN(vRenderingCamOffsetRot.z) * COS(vRenderingCamOffsetRot.x), COS(vRenderingCamOffsetRot.z) * COS(vRenderingCamOffsetRot.x), SIN(vRenderingCamOffsetRot.x)>> 
				FLOAT fRenderingCamFOV 
				fRenderingCamFOV = GET_FINAL_RENDERED_CAM_FOV()
				
				// spline
				IF pushInData.iSpeedupTime > 0
				OR customSmoothingFlags <> CAM_SPLINE_NO_SMOOTH
					pushInData.splineCam = CREATE_CAMERA(CAMTYPE_SPLINE_TIMED, FALSE)
				ENDIF
		
				// start
				pushInData.startCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				IF bCamsAttached
					ATTACH_CAM_TO_ENTITY(pushInData.startCam, pushInData.targetEntity, vRenderingCamOffset)
				ELSE
					SET_CAM_COORD(pushInData.startCam, vRenderingCamPos)
				ENDIF
				SET_CAM_ROT(pushInData.startCam, vRenderingCamRot)
				SET_CAM_FOV(pushInData.startCam,fRenderingCamFOV)
				
				// end
				pushInData.endCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				IF bCamsAttached
				AND NOT bOnlyAttachStartIfCamsAttached
					ATTACH_CAM_TO_ENTITY(pushInData.endCam, pushInData.targetEntity, vRenderingCamOffset + (vRenderingCamAttachedDirection * pushInData.fDistance))
				ELSE
					SET_CAM_COORD(pushInData.endCam, vRenderingCamPos + (vRenderingCamDirection * pushInData.fDistance))
				ENDIF
				SET_CAM_ROT(pushInData.endCam, vRenderingCamRot)
				SET_CAM_FOV(pushInData.endCam, fRenderingCamFOV)	
				
				// mid - spline
				IF pushInData.iSpeedupTime > 0
				AND customSmoothingFlags = CAM_SPLINE_NO_SMOOTH
					pushInData.midCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					IF bCamsAttached
					AND NOT bOnlyAttachStartIfCamsAttached
						ATTACH_CAM_TO_ENTITY(pushInData.midCam, pushInData.targetEntity, vRenderingCamOffset + (vRenderingCamAttachedDirection * pushInData.fDistance * pushInData.fSpeedUpProportion))
					ELSE
						SET_CAM_COORD(pushInData.midCam, vRenderingCamPos + (vRenderingCamDirection * pushInData.fDistance * pushInData.fSpeedUpProportion))
					ENDIF
					SET_CAM_ROT(pushInData.midCam, vRenderingCamRot)
					SET_CAM_FOV(pushInData.midCam, fRenderingCamFOV)	
				ENDIF
				
				IF pushInData.iSpeedupTime > 0
				OR customSmoothingFlags <> CAM_SPLINE_NO_SMOOTH
					// spline
					ADD_CAM_SPLINE_NODE_USING_CAMERA(pushInData.splineCam, pushInData.startCam, 0, CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS)
					IF customSmoothingFlags = CAM_SPLINE_NO_SMOOTH
						ADD_CAM_SPLINE_NODE_USING_CAMERA(pushInData.splineCam, pushInData.midCam, pushInData.iSpeedUpTime, CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS)
					ENDIF
					ADD_CAM_SPLINE_NODE_USING_CAMERA(pushInData.splineCam, pushInData.endCam, pushInData.iInterpTime - pushInData.iSpeedUpTime, CAM_SPLINE_NODE_SMOOTH_LENS_PARAMS)
					SET_CAM_SPLINE_SMOOTHING_STYLE(pushInData.splineCam, customSmoothingFlags)
					SET_CAM_ACTIVE(pushInData.splineCam, TRUE)
				ELSE
					// interp
					SET_CAM_ACTIVE_WITH_INTERP(pushInData.endCam, pushInData.startCam, pushInData.iInterpTime, GRAPH_TYPE_ACCEL, GRAPH_TYPE_ACCEL)
				ENDIF
				
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				pushInData.iCreateTime = GET_GAME_TIMER()
				pushInData.bDonePostFX = FALSE
				pushInData.state = PUSH_IN_RUNNING
			ELSE
				SCRIPT_ASSERT("attempting to create push-in cam with dead entity")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE PUSH_IN_RUNNING
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		
			IF bDoPostFX
				IF NOT pushInData.bDonePostFX
					IF GET_GAME_TIMER() >= pushInData.iCreateTime + pushInData.iPostFXTime			
						//PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						
						IF bDoColouredFlash
							SWITCH pushInData.toCharacter
								CASE CHAR_FRANKLIN
									ANIMPOSTFX_PLAY("CamPushInFranklin", 0, FALSE)		
								BREAK
								CASE CHAR_MICHAEL
									ANIMPOSTFX_PLAY("CamPushInMichael", 0, FALSE)	
								BREAK
								CASE CHAR_TREVOR
									ANIMPOSTFX_PLAY("CamPushInTrevor", 0, FALSE)	
								BREAK
							ENDSWITCH
						ELSE
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)	
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")

						pushInData.bDonePostFX = TRUE
					ENDIF
				ENDIF	
			ENDIF
		
			IF GET_GAME_TIMER() >= pushInData.iCreateTime + pushInData.iCutTime
				IF bDestroyCamsAtEnd
					DESTROY_PUSH_IN_CAMS(pushInData)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF

				RETURN TRUE
			ENDIF
		BREAK
	
		CASE PUSH_IN_DONE
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


