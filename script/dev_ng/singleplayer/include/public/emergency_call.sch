USING "commands_misc.sch"
USING "commands_script.sch"

USING "globals.sch"

// PURPOSE: Will set a global to suppress emergency call responses. Also stores the script doing the suppressing.
PROC SUPPRESS_EMERGENCY_CALLS()
	PRINTLN("SUPPRESS_EMERGENCY_CALLS called by ", GET_THIS_SCRIPT_NAME(), ".")
	g_bEmergencyCallsSuppressed = TRUE
	g_sEmergencyCallsSuppressedByThisScript = GET_THIS_SCRIPT_NAME()
ENDPROC

FUNC BOOL ARE_EMERGENCY_CALLS_SUPPRESSED()
	RETURN g_bEmergencyCallsSuppressed
ENDFUNC

PROC RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	IF g_bEmergencyCallsSuppressed
		PRINTLN("RELEASE_SUPPRESSED_EMERGENCY_CALLS called by ", GET_THIS_SCRIPT_NAME(), ".")
		g_bEmergencyCallsSuppressed = FALSE
		g_sEmergencyCallsSuppressedByThisScript = "NULL"
	ELSE
		PRINTLN("RELEASE_SUPPRESSED_EMERGENCY_CALLS called by ", GET_THIS_SCRIPT_NAME(), " but they are not suppressed.")
	ENDIF
ENDPROC

PROC FORCE_EMERGENCY_CALL_PRIORITY_HIGH(BOOL bForce = TRUE)
	IF bForce
		g_bEmergencyCallPriorityIsHigh = TRUE
		PRINTLN("FORCE_EMERGENCY_CALL_PRIORITY_HIGH called TRUE by ", GET_THIS_SCRIPT_NAME(), ".")
	ELSE
		g_bEmergencyCallPriorityIsHigh = FALSE
		PRINTLN("FORCE_EMERGENCY_CALL_PRIORITY_HIGH called FALSE by ", GET_THIS_SCRIPT_NAME(), ".")
	ENDIF
ENDPROC

// PURPOSE: Returns the name of the script that last called SUPPRESS_EMERGENCY_CALLS()
FUNC TEXT_LABEL_31 EMERGENCY_CALLS_SUPPRESSING_SCRIPT()
	RETURN g_sEmergencyCallsSuppressedByThisScript
ENDFUNC




