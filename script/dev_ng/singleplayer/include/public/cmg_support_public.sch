USING "cmg_structs_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	CLIFFORD MINIGAME FILE: cmg_support_public.sch
//	Sam Hackett
//
//	This mirrors the bob file: minigame_handler_support.sch
//
//	"CMG_TYPE" is our version of bob's "MINIGAME_TYPES" enum.
//	These are the support functions for that enum.
//
//	See also:
//		cmg_structs_public.sch
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// These functions must match BOB generic_minigame versions


#IF IS_DEBUG_BUILD
	FUNC STRING ENUM_NAME_CMG_TYPE(CMG_TYPE eMgType)
		SWITCH (eMgType)
			CASE CMG_TYPE_Blackjack	RETURN "CMG_TYPE_Blackjack"	BREAK
			CASE CMG_TYPE_Poker		RETURN "CMG_TYPE_Poker"		BREAK
			CASE CMG_TYPE_Roulette	RETURN "CMG_TYPE_Roulette"	BREAK
			CASE CMG_TYPE_Slot		RETURN "CMG_TYPE_Slot"		BREAK

			CASE CMG_TYPE_MAX		RETURN "CMG_TYPE_MAX"		BREAK
			CASE CMG_TYPE_INVALID	RETURN "CMG_TYPE_INVALID"	BREAK
		ENDSWITCH
		
		CPRINTLN(DEBUG_MINIGAME,	"ENUM_NAME_CMG_TYPE() Invalid minigame type: ", eMgType)
		SCRIPT_ASSERT(				"ENUM_NAME_CMG_TYPE() Invalid minigame type")
		RETURN "CMG_TYPE_unknown"
	ENDFUNC
#ENDIF

FUNC STRING CMG_GET_DISPLAY_NAME(CMG_TYPE eMgType)		// To deprecate to debug only? Is that possible, or does BOB system need it?
	SWITCH (eMgType)
		CASE CMG_TYPE_Blackjack	RETURN "Blackjack"		BREAK
		CASE CMG_TYPE_Poker		RETURN "Poker"			BREAK
		CASE CMG_TYPE_Roulette	RETURN "Roulette"		BREAK
		CASE CMG_TYPE_Slot		RETURN "Slot machine"	BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MINIGAME,	"CMG_GET_DISPLAY_NAME() Invalid minigame type: ", eMgType)
	SCRIPT_ASSERT(				"CMG_GET_DISPLAY_NAME() Invalid minigame type")
	RETURN "Casino"
ENDFUNC

FUNC STRING CMG_GET_PRINT_NAME(CMG_TYPE eMgType)
	SWITCH (eMgType)
		CASE CMG_TYPE_Blackjack	RETURN "MG_BJACK"		BREAK
		CASE CMG_TYPE_Poker		RETURN "MG_POKER"		BREAK
		CASE CMG_TYPE_Roulette	RETURN "MG_ROULETTE"	BREAK
		CASE CMG_TYPE_Slot		RETURN "MG_SLOT"		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MINIGAME,	"CMG_GET_PRINT_NAME() Invalid minigame type: ", ENUM_NAME_CMG_TYPE(eMgType))
	SCRIPT_ASSERT(				"CMG_GET_PRINT_NAME() Invalid minigame type")
	RETURN "MG_unknown"
ENDFUNC

FUNC STRING CMG_GET_TEXT_BLOCK(CMG_TYPE eMgType)
	SWITCH (eMgType)
		CASE CMG_TYPE_Blackjack		RETURN "MGBLK"			BREAK
		CASE CMG_TYPE_Poker			RETURN "MGPKR"			BREAK
		CASE CMG_TYPE_Roulette		RETURN "MGRLT"			BREAK
		CASE CMG_TYPE_Slot			RETURN "DLCSLOT"		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MINIGAME,	"CMG_GET_TEXT_BLOCK() Invalid eGameType: ", ENUM_NAME_CMG_TYPE(eMgType))
	SCRIPT_ASSERT(				"CMG_GET_TEXT_BLOCK() Invalid eMgType")
	RETURN "MG_unknown"
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

