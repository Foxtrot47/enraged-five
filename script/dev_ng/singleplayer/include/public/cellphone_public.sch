USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_clock.sch"
USING "commands_entity.sch"
USING "commands_graphics.sch"
USING "cellphone_private.sch"

//Wiki-ed.
//Allows other scripts to check if the phone is on-screen.  If you want to make sure the phone has finished moving up onscreen, set the BOOL parameter to TRUE

FUNC BOOL IS_PHONE_ONSCREEN (BOOL ShouldCheckForFullyMovedUp = FALSE)

    IF ShouldCheckForFullyMovedUp = TRUE 

        IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY))

            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //This bit is set by cellphone flashhand when phone has finished moving up.
                RETURN TRUE
            ELSE
                RETURN FALSE
            ENDIF
        ELSE
            RETURN FALSE
        ENDIF
    ELSE

        #if USE_CLF_DLC
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) > 0    //Phone may be moving offscreen after getting set to "away".
                                                                                                        //Flashhand terminates only after it is fully offscreen.
                RETURN TRUE
            ENDIF
        #endif
        #if USE_NRM_DLC 
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) > 0    //Phone may be moving offscreen after getting set to "away".
                RETURN TRUE
            ENDIF
        #endif

        #if not USE_SP_DLC
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhand")) > 0    //Phone may be moving offscreen after getting set to "away".
                RETURN TRUE
            ENDIF
        #endif

        IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY)) //Phone may be in TAKEOUT state.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF

    ENDIF

ENDFUNC

//Added to check if the script for cellphone_flashhand is running based on the active TU
FUNC BOOL IS_THE_SCRIPT_cellphone_flashhand_RUNNING()
    #if USE_CLF_DLC
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) > 0 
        RETURN TRUE
    ENDIF
    #endif
    #if USE_NRM_DLC
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) > 0 
        RETURN TRUE
    ENDIF
    #endif

    #if not USE_SP_DLC
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhand")) > 0 
        RETURN TRUE
    ENDIF
    #endif
    
    RETURN FALSE
ENDFUNC

//DANGER - Only the Race Hud should use this! See Steve T. if you think you might need something similar. See bug 1033043
//This specifically checks for the AWAY state but does not wait on flashhand terminating before returning FALSE. If the phone is moving offscreen upon
//AWAY or DISABLED then we need to check its progress.
//If the phone has moved below the visible threshold, then it's safe to draw the race hud as the phone will not overlap it.

//Standard IS_PHONE_ONSCREEN commands may return true for longer as the phone's drawing script remains around a little to avoid 
//the render target appearing blank or pink in certain situations.

FUNC BOOL IS_PHONE_ACTIVE_OR_OVERLAPPING_HUD_ITEMS()

    IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY)) //Phone may be in TAKEOUT state.

        RETURN TRUE

    ELSE //Phone may be in process of being put away, so no longer active but we need to check position on screen to see if it's still above the visible threshold.

      IF IS_THE_SCRIPT_cellphone_flashhand_RUNNING() 
        VECTOR TempPhonePosVec = <<0.0, -500.0, 0.0>> //Set Y component well below visible threshold initially.

        GET_MOBILE_PHONE_POSITION (TempPhonePosVec)

            IF g_Chosen_Ratio = DISPLAY_16_9

                IF TempPhonePosVec.Y > -119.0

                    RETURN TRUE

                ELSE           

                    RETURN FALSE

                ENDIF

            ELSE //Must be 4_3

                IF TempPhonePosVec.Y > -101.0

                    RETURN TRUE

                ELSE           

                    RETURN FALSE

                ENDIF

            ENDIF

        ELSE  //Phone not active, can't be overlapping either!

            RETURN FALSE

        //Seems pointless, but this fixes intellisense.
        ENDIF

    ENDIF

ENDFUNC

//This will return true if the phone is onscreen and in ringing state. If you want to make sure the phone has finished moving up onscreen before checking for the ringing state,
//set the passed BOOL parameter to TRUE

FUNC BOOL IS_PHONE_ONSCREEN_AND_RINGING (BOOL ShouldCheckForFullyMovedUp = FALSE)

    IF ShouldCheckForFullyMovedUp = TRUE 

        IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY))
            IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_HAS_CELLPHONE_FULLY_MOVED_UP) //This bit is set by cellphone flashhand when phone has finished moving up.
                IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED
                    RETURN TRUE
                ELSE
                    RETURN FALSE
                ENDIF
            ELSE
                RETURN FALSE
            ENDIF
        ELSE
            RETURN FALSE
        ENDIF
    ELSE

        #if USE_CLF_DLC
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) > 0    //Phone may be moving offscreen after getting set to "away".
                                                                                        //Flashhand terminates only after it is fully offscreen.
                IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 

                    RETURN TRUE

                ELSE

                    RETURN FALSE

                ENDIF

            ENDIF
        #endif
        #if USE_NRM_DLC
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) > 0    //Phone may be moving offscreen after getting set to "away".
                                                                                        //Flashhand terminates only after it is fully offscreen.
                IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 

                    RETURN TRUE

                ELSE

                    RETURN FALSE

                ENDIF

            ENDIF
        #endif

        #if not USE_SP_DLC
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhand")) > 0    //Phone may be moving offscreen after getting set to "away".
                                                                                        //Flashhand terminates only after it is fully offscreen.
                IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 

                    RETURN TRUE

                ELSE

                    RETURN FALSE

                ENDIF

            ENDIF
        #endif

        IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY)) //Phone may be in TAKEOUT state.

            IF g_InboundCallWaitAccRej = CALL_WAITING_TO_BE_ANSWERED 
                RETURN TRUE
            ELSE
                RETURN FALSE
            ENDIF
        ELSE
            RETURN FALSE
        ENDIF

    ENDIF

ENDFUNC

//If the cellphone was in use and the player receives an injury or starts ragdolling then, in SP only, the phone is flagged as forced away specifically.
//The global is reset the next time the cellphone is launched to screen.
FUNC BOOL HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()

    IF g_B_ForcedAway
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF  

ENDFUNC

//Wiki-ed.

//This simulates pressing up on the d-pad to launch the phone. Cellphone_controller checks for this bit set and will launch the phone if it's not disabled.
PROC LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()

    SET_BIT (BitSet_CellphoneDisplay, g_BS_LAUNCH_PHONE_TO_HOMESCREEN)

ENDPROC

FUNC BOOL CHECK_AIMING_FIRING_RAGDOLL_STATE_FOR_DPAD_TAKEOUT()

    //We may need to remove this check for multiplayer...

    IF IS_PLAYER_PLAYING (PLAYER_ID())

        /*Removing again... IS_AIM_CAM_ACTIVE seems to be returning TRUE when going into cover. See 2168736. Also look at 2032679 in cellphone_controller.
        IF IS_AIM_CAM_ACTIVE() //Fix for 2155944.

            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Can't take out phone via dpad as IS_AIM_CAM_ACTIVE is returning TRUE - 2155944")
                cdPrintnl ()
            #endif
            
            RETURN TRUE
    
        ENDIF
        */


        //IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), INT_TO_ENUM(PED_CONFIG_FLAGS, 78)) //Replace this int_to_enum with proper ENUM when Graeme put it in.
        IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsAimingGun)
        OR IS_PED_RAGDOLL(PLAYER_PED_ID())
        OR IS_PED_BEING_STUNNED (PLAYER_PED_ID())
        OR IS_PED_IN_PARACHUTE_FREE_FALL (PLAYER_PED_ID())
        OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING   
        //OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING  

        //Legacy
        //IF (IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID()) 
        //OR IS_PED_SHOOTING(PLAYER_PED_ID())) //Has a window of opportunity... not constant, so we need a replacement.
        //OR IS_PLAYER_FREE_AIMING(PLAYER_ID())

            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Can't take out phone via dpad. Player aiming, targetting, firing, freefalling, parachuting or ragdolling")
                cdPrintnl ()
            #endif
            
            RETURN TRUE
    
        ELSE
        
            RETURN FALSE
        
        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Can't take out phone via dpad. Player not playing according to aim and fire or ragdoll check.")
            cdPrintnl ()
        #endif

        RETURN TRUE 
    ENDIF

ENDFUNC

FUNC BOOL CHECK_AIMING_FIRING_STATE_FOR_CALL_LAUNCH()

    IF g_bInMultiplayer

        BOOL sniper_equipped = FALSE

        WEAPON_TYPE TempWeapon

        GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), TempWeapon)

        IF IS_PLAYER_PLAYING (PLAYER_ID())
            IF TempWeapon = WEAPONTYPE_SNIPERRIFLE
            OR TempWeapon = WEAPONTYPE_HEAVYSNIPER
            OR TempWeapon = WEAPONTYPE_REMOTESNIPER

                sniper_equipped = TRUE

            ENDIF
        ENDIF

        //Less checks for multiplayer, but we still don't want calls to come through when sniper scope is on.
        IF IS_AIM_CAM_ACTIVE()
        AND sniper_equipped = TRUE

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PUB - Can't launch phone for mp call. Player aiming in sniper mode.")
                cdPrintnl ()

            #endif

            RETURN TRUE

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        IF IS_PLAYER_PLAYING (PLAYER_ID())

            //IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), INT_TO_ENUM(PED_CONFIG_FLAGS, 78)) //Replace this int_to_enum with proper ENUM when Graeme put it in.
            IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsAimingGun)

            //PCF_IsAimingGun

            //Legacy
            //IF (IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID()) 
            //OR IS_PED_SHOOTING(PLAYER_PED_ID())) //Has a window of opportunity... not constant, so we need a replacement.
            //OR IS_PLAYER_FREE_AIMING(PLAYER_ID())

                #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PUB - Can't launch phone for call. Player aiming, targetting or firing.")
                    cdPrintnl ()

                #endif

                RETURN TRUE 
                //Quick Fix
                //RETURN FALSE

            ELSE
                RETURN FALSE
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Can't launch phone for call. Player not playing according to aim and fire check.")
                cdPrintnl ()
            #endif
            RETURN TRUE
        ENDIF
    ENDIF

ENDFUNC

//Wiki-ed.
//This will launch a cellphone application directly from the phone's away position. Primarily useful for cutscenes or to force an app in hand, it's best not to alter the default
//behaviour of the last two parameters. See Steve T if you have any questions - he uses these for specific cases but they are powerful in a phone context.

FUNC BOOL LAUNCH_CELLPHONE_APPLICATION (enumApplicationList AppEnum, BOOL LoadScaleformMovies = TRUE, BOOL OverrideOnscreenCheck = FALSE, BOOL UseContactStackSize = FALSE)

        
   IF g_bInMultiplayer
            
        //2139576
        //Make exceptions to normal behaviour for MP launching of Trackify and Snapmatic. Would need further exceptions for any app launched from an MP script rather than the phone itself.
		
        IF AppEnum <> AppTRACKIFY
        AND AppEnum <> AppCAMERA
		AND AppEnum <> AppDummyApp0
		AND AppEnum <> AppTEXTS

            #if IS_DEBUG_BUILD
                SCRIPT_ASSERT ("Weird MP LAUNCH_CELLPHONE_APPLICATION detected!")

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Weird MP LAUNCH_CELLPHONE_APPLICATION detected! AppEnum is ")
                cdPrintint (ENUM_TO_INT(AppENum))
                cdPrintnl()
            #endif

            RETURN FALSE
        
        ENDIF


        
        IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)

            IF AppEnum = AppTRACKIFY

                 #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Setting AppTrackify MP launch bits.")
                    cdPrintnl()
                #endif


                SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                SET_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY)
            
            ENDIF



            IF AppEnum = AppCAMERA

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Setting AppCamera MP launch bits.")
                    cdPrintnl()
                #endif


                SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                SET_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPCAMERA)
            
            ENDIF
			
			IF AppEnum = AppDummyApp0
				#if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Setting AppDummyApp0 MP launch bits.")
                    cdPrintnl()
                #endif


                SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                SET_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
			ENDIF
			
			IF AppEnum = AppTEXTS
				#if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Setting AppTEXTS MP launch bits.")
                    cdPrintnl()
                #endif
				
                SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
                SET_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTEXTS)
			ENDIF

        ENDIF

		IF g_AppList[AppEnum].AppScriptNameHash = 0
			Fill_All_Primary_App_Sheets() //Need to do this or else the AppScriptNameHash can be 0
		ENDIF
		
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) > 0

            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_CELLPHONE_CONTROLLER_LAUNCH)
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTRACKIFY)
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPCAMERA)
			CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPDUMMYAPP0)
			CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_LAUNCH_CELLPHONE_MP_APPTEXTS)

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - MP LAUNCH_CELLPHONE_APPLICATION... Success or script already running. Returned TRUE and cleared MP launch bits.")
                cdPrintnl()
            #endif

            RETURN TRUE

        ELSE

            RETURN FALSE
            
        ENDIF


   ELSE        
        
        
        
        
        
        
        Get_Cellphone_Owner()

        /* Allowing non-conversation phone use in water now...
        IF IS_PLAYER_PLAYING (PLAYER_ID())

            IF NOT g_bInMultiplayer //multiplayer allows phone use in water... 

                IF NOT (g_Cellphone.PhoneOwner = CHAR_FRANKLIN)     //i.e Michael or Trevor
                AND IS_ENTITY_IN_WATER (PLAYER_PED_ID())            //and they are in the water

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Can't launch_cellphone_application current player character does not have waterproof phone.")
                        cdPrintnl ()

                    #endif

                    RETURN FALSE

                ENDIF

            ENDIF

        ENDIF
        */

        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL 

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch Cellphone Application called but phone in ONGOING_CALL state... can't launch application.")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF OverrideOnscreenCheck = FALSE

            IF IS_PHONE_ONSCREEN() = TRUE   //could also check for PDS_RUNNINGAPP if I need to check for another app running.

                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PUB - The phone is already onscreen.. can't launch application")
                    cdPrintnl()
                #endif

                RETURN FALSE

            ENDIF

        ENDIF

        IF g_Phone_Blocked_While_Moving_Down = TRUE

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Phone currently moving down screen,  can't launch application")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF g_Cellphone.PhoneDS = PDS_RUNNINGAPP 

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Onscreen check overridden but an app is already running.. can't launch application")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF LoadScaleformMovies = TRUE

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch cellphone application received a request to load movies but these should have been loaded by flashhand")
                cdPrintnl()
            #endif

            /* Removed as part of ongoing scaleform streamlining work with Derek and Graeme - 11.05.2011

            IF g_B_Scaleform_Movies_Loaded = FALSE

                g_B_Scaleform_Movies_Loaded = TRUE

                //Usually done when phone is "taken out" in cellphone_controller.sc
                //SF_MovieIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_b") 

                Get_Cellphone_Owner()

                SF_MovieIndex = REQUEST_SCALEFORM_MOVIE (This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_filename_label[This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option])

                SF_MovieButtonsIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_buttons")

                WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
                OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)
                    WAIT(0)
                ENDWHILE

            ENDIF

            */

        ENDIF

        IF NOT IS_THREAD_ACTIVE (Cellphone_FlashResponseThread) //#1570364

            #if USE_CLF_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhandCLF")

                        Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhandCLF", DEFAULT_STACK_SIZE)

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhandCLF")

                    ENDIF

                ENDIF
            #endif
            #if USE_NRM_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhandNRM")

                        Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhandNRM", DEFAULT_STACK_SIZE)

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhandNRM")

                    ENDIF

                ENDIF
            #endif  

            #if not USE_SP_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhand")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhand")

                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 //1943564 - Required to prevent rare double launch of cellphone_flashhand.

                            #if USE_TU_CHANGES

                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", DEFAULT_STACK_SIZE)

                            #endif

                            #if not USE_TU_CHANGES

                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", CELLPHONE_STACK_SIZE)

                            #endif

                        ENDIF

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhand")

                    ENDIF

                ENDIF
            #endif

        ENDIF

        //Important! There is a danger of subsequently launched app scripts calling movie methods whilst movies might not have loaded via flashhand's calls.
        //This check is therefore required!

        WHILE NOT g_B_Scaleform_Movies_Loaded //Make sure that the movies requested by cellphone_flashhand have loaded before continuing.
                                              //This bool is set after Create_3dPhoneObject has been called from cellphone_flashhand.sc

            WAIT (0)

        ENDWHILE

        Fill_All_Primary_App_Sheets()   //From cellphone_private.sch - we must make sure that the application sheet is completely filled before running an app.

        #IF USE_TU_CHANGES

            //May impact LAUNCH_TRACKIFY_IMMEDIATELY...

            TitleUpdate_Patch_In_JobList_For_SP_Phone()

        #ENDIF

        //Hashfix required.

        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0 //This thread is not already running so we can safely launch it.

            g_TerminateApp = FALSE  //make sure that Check_Application_Exit() doesn't fire off accidentally.

            //Hide_phone_for_hotswap check?

            //IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) < (ENUM_TO_INT(PDS_TAKEOUT))

                //only create the phone if it is not already onscreen... might put check in the creation procedure itself.
                //Create_3dPhoneObject() //from cellphone_private.sch //Phone should be created by cellphone_flashhand only now. Avoids multiple instances of phone assert.

            //ENDIF

            //This is a liitle bit dangerous. There's a risk of RUNNINGAPP getting assigned whilst the phone is moving away that needs addressed. Might get rid of onscreen check.
                g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 7. Cellphone Public assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif

            //Would need to terminate all other running apps if we allow this Launcher to be done when the phone is onscreen....

            request_load_scriptstring (g_AppList[AppEnum].AppScriptName)

            IF UseContactStackSize 

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0 //Dev_NG only fix for 2133806  

                    Application_Thread = START_NEW_SCRIPT (g_AppList[AppEnum].AppScriptName, CONTACTS_APP_STACK_SIZE)

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Launch cellphone application launching chosen script with contacts stack size")
                        cdPrintnl()
                    #endif

                ENDIF

            ELSE

                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0 //Dev_NG only fix for 2133806  

                    Application_Thread = START_NEW_SCRIPT (g_AppList[AppEnum].AppScriptName, CELLPHONE_STACK_SIZE)

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Launch cellphone application launching chosen script with cellphone stack size.")
                        cdPrintnl()
                    #endif

                ENDIF

            ENDIF

            SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[AppEnum].AppScriptName)
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch Cellphone Application public function has requested a script that is already running.")
                cdPrintnl()
            #endif
            RETURN TRUE

        ENDIF


    ENDIF //End of g_bInMultiplayer check.

ENDFUNC











//DO NOT USE THIS without asking Steve T first. It should be used by the Cellphone's internal scripts only.
FUNC BOOL CC_INTERNAL_LAUNCH_CELLPHONE_APPLICATION (enumApplicationList AppEnum, BOOL LoadScaleformMovies = TRUE, BOOL OverrideOnscreenCheck = FALSE, BOOL UseContactStackSize = FALSE)

        
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - CC_INTERNAL_LAUNCH_Cellphone_Application called.")
            cdPrintnl()
        #endif


               
        Get_Cellphone_Owner()



        IF g_bInMultiplayer = FALSE
            IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR)

                IF CellphoneAppAutoLaunchType = ACT_APP_INVITE // Search for "CELLPHONE_FH - Activatable  INVITE  App caused direct launch of app" to see where this launch request is made.
                OR CellphoneAppAutoLaunchType = ACT_APP_BOSSINV

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Internal Launch Cellphone Application called and in Director Mode but exception made for ACT_APP_INVITE or BOSSINV")
                        cdPrintnl()
                    #endif

                    
                ELSE

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Internal Launch Cellphone Application called but currently in Director Mode... can't launch application.")
                        cdPrintnl()
                    #endif

                    RETURN FALSE

                ENDIF

            ENDIF
        ENDIF



        
        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL 

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch Cellphone Application called but phone in ONGOING_CALL state... can't launch application.")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF OverrideOnscreenCheck = FALSE
        
            IF IS_PHONE_ONSCREEN() = TRUE   //could also check for PDS_RUNNINGAPP if I need to check for another app running.

                #if IS_DEBUG_BUILD
                    cdPrintstring("CELLPHONE_PUB - The phone is already onscreen.. can't launch application")
                    cdPrintnl()
                #endif

                RETURN FALSE

            ENDIF

        ENDIF

        IF g_Phone_Blocked_While_Moving_Down = TRUE

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Phone currently moving down screen,  can't launch application")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF g_Cellphone.PhoneDS = PDS_RUNNINGAPP 

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Onscreen check overridden but an app is already running.. can't launch application")
                cdPrintnl()
            #endif

            RETURN FALSE

        ENDIF

        IF LoadScaleformMovies = TRUE

            #if IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch cellphone application received a request to load movies but these should have been loaded by flashhand")
                cdPrintnl()
            #endif

            /* Removed as part of ongoing scaleform streamlining work with Derek and Graeme - 11.05.2011
            
            IF g_B_Scaleform_Movies_Loaded = FALSE

                g_B_Scaleform_Movies_Loaded = TRUE
                            
                //Usually done when phone is "taken out" in cellphone_controller.sc
                //SF_MovieIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_b") 

                Get_Cellphone_Owner()

                SF_MovieIndex = REQUEST_SCALEFORM_MOVIE (This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_filename_label[This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option])
                
                SF_MovieButtonsIndex = REQUEST_SCALEFORM_MOVIE ("cellphone_buttons")
                       
                WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex)
                OR NOT HAS_SCALEFORM_MOVIE_LOADED (SF_MovieButtonsIndex)
                    WAIT(0)
                ENDWHILE

            ENDIF

            */

        ENDIF
        
        IF NOT IS_THREAD_ACTIVE (Cellphone_FlashResponseThread) //#1570364
            
            #if USE_CLF_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhandCLF")

                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandCLF")) = 0 

                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhandCLF", DEFAULT_STACK_SIZE)

                        ENDIF

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhandCLF")

                    ENDIF

                ENDIF
            #endif
            #if USE_NRM_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhandNRM")

                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhandNRM")) = 0  //1943564 - Required to prevent rare double launch of cellphone_flashhand.
                        
                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhandNRM", DEFAULT_STACK_SIZE)

                        ENDIF

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhandNRM")

                    ENDIF

                ENDIF
            #endif  

            #if not USE_SP_DLC
                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("cellphone_flashhand")) = 0    //Make sure flashhand can run only once if we do let this function
                                                                                                                //run with the phone already onscreen in the future.

                    IF g_Cellphone.PhoneDS < PDS_TAKEOUT // #1570364

                        //Launch flashhandler to fill app slots and draw phone surround.
                        request_load_scriptstring ("cellphone_flashhand")

                        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) = 0 //1943564 - Required to prevent rare double launch of cellphone_flashhand.
                        
                            #if USE_TU_CHANGES

                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", DEFAULT_STACK_SIZE)

                            #endif

                            #if not USE_TU_CHANGES

                            Cellphone_FlashResponseThread = START_NEW_SCRIPT ("cellphone_flashhand", CELLPHONE_STACK_SIZE)

                            #endif

                        ENDIF

                        SET_SCRIPT_AS_NO_LONGER_NEEDED ("cellphone_flashhand")

                    ENDIF

                ENDIF
            #endif

        ENDIF

        //Important! There is a danger of subsequently launched app scripts calling movie methods whilst movies might not have loaded via flashhand's calls.
        //This check is therefore required!

        WHILE NOT g_B_Scaleform_Movies_Loaded //Make sure that the movies requested by cellphone_flashhand have loaded before continuing.
                                              //This bool is set after Create_3dPhoneObject has been called from cellphone_flashhand.sc
     
            WAIT (0)
        
        ENDWHILE

        Fill_All_Primary_App_Sheets()   //From cellphone_private.sch - we must make sure that the application sheet is completely filled before running an app.
        
        #IF USE_TU_CHANGES

            //May impact LAUNCH_TRACKIFY_IMMEDIATELY...

            TitleUpdate_Patch_In_JobList_For_SP_Phone()
        
        #ENDIF
        
        //Hashfix required.
             
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0 //This thread is not already running so we can safely launch it.
                   
            g_TerminateApp = FALSE  //make sure that Check_Application_Exit() doesn't fire off accidentally.

            //Hide_phone_for_hotswap check?

            //IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) < (ENUM_TO_INT(PDS_TAKEOUT))
    
                //only create the phone if it is not already onscreen... might put check in the creation procedure itself.
                //Create_3dPhoneObject() //from cellphone_private.sch //Phone should be created by cellphone_flashhand only now. Avoids multiple instances of phone assert.
            
            //ENDIF

            //This is a liitle bit dangerous. There's a risk of RUNNINGAPP getting assigned whilst the phone is moving away that needs addressed. Might get rid of onscreen check.
                g_Cellphone.PhoneDS = PDS_RUNNINGAPP

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 7. Cellphone Public assigns PDS_RUNNINGAPP")
                    cdPrintnl()   
                #endif
                  
            //Would need to terminate all other running apps if we allow this Launcher to be done when the phone is onscreen....
        
            request_load_scriptstring (g_AppList[AppEnum].AppScriptName)

            IF UseContactStackSize 


                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0

                    Application_Thread = START_NEW_SCRIPT (g_AppList[AppEnum].AppScriptName, CONTACTS_APP_STACK_SIZE)

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Launch cellphone application launching chosen script with contacts stack size")
                        cdPrintnl()
                    #endif

                ENDIF

            ELSE


                IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_AppList[AppEnum].AppScriptNameHash) = 0

                    Application_Thread = START_NEW_SCRIPT (g_AppList[AppEnum].AppScriptName, CELLPHONE_STACK_SIZE)

                    #if IS_DEBUG_BUILD
                        cdPrintstring("CELLPHONE_PUB - Launch cellphone application launching chosen script with cellphone stack size.")
                        cdPrintnl()
                    #endif

                ENDIF

            ENDIF

            SET_SCRIPT_AS_NO_LONGER_NEEDED (g_AppList[AppEnum].AppScriptName)

            RETURN TRUE


        ELSE


            #IF IS_DEBUG_BUILD
                cdPrintstring("CELLPHONE_PUB - Launch Cellphone Application public function has requested a script that is already running.")
                cdPrintnl()
            #endif

            RETURN TRUE

        ENDIF

ENDFUNC


























//Wiki-ed.
//This should be used by App scripts to check exit routines from simple RUNNINGAPP states only. See Steve T about using this. 
//It's not as simple as it looks, particulary if you have specific audio other than keytones and apps with secondary screens.
//This should be called by all apps in RUNNINGAPP state and checked for a return of TRUE. If it returns TRUE, then the app should call its local cleanup function.
//e.g   IF CHECK_FOR_APPLICATION_EXIT() 
//           Local_Cleanup_and_Terminate()
//      ENDIF    

//If a script is not checking CHECK_APPLICATION_EXIT() it should have a failsafe that ensure the correct display state is selected and scaleform 
//resets the display to the homescreen to avoid any timing issues. Pressing "Select" then "Back" in very quick succession for example.

FUNC BOOL CHECK_FOR_APPLICATION_EXIT()

    //If the PHONE_GO_BACK_INPUT check is true in cellphone_flashhand.sc, that script will set g_TerminateApp to true IF the app is in PDS_RUNNINGAPP state only.
    //Any app looping CHECK_FOR_APPLICATION_EXIT and checking the return value whilst in RUNNINGAPP state does not need to explicitly check for "back" keypresses.
    //This will do it automatically.

    //This means that an app can have a secondary screen and define itself as being in COMPLEXAPP state. When a "back" option button is pressed, which must be checked
    //by the local script, the app can return to RUNNINGAPP state and its primary screen without fear of inadvertently exiting completely.

    IF g_TerminateApp = TRUE 
    OR g_Cellphone.PhoneDS < PDS_RUNNINGAPP //Added for 910427
        g_MainGui_RendertargetNeedsReset = TRUE //Tell cellphone_flashhand.sc that the rendertarget should be reinitialised as failsafe. The app may have altered it.

        //Trial Inclusion Run 24.09.10  See CRITICAL: Running App to Homescreen transition on "Back" press in cellphone_flashhand.sc
        //Trial failed. It's dangerous to let anything other than cellphone_flashhand.sc in the critical section do it.
        /*
        g_Cellphone.PhoneDS = PDS_MAXIMUM

        Update_Scaleform_VSE ()
        */

        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC

//Wiki-ed.
//If the phone is forced away ( player dies, hit by car etc.) or disabled, then it's important that any running app terminates correctly.
//This should be called by all apps and checked for a return of TRUE. If it returns TRUE, then the app should call its local cleanup function.
//e.g   IF CHECK_FOR_ABNORMAL_EXIT() 
//           Local_Cleanup_and_Terminate()
//      ENDIF  

FUNC BOOL CHECK_FOR_ABNORMAL_EXIT()

    IF g_Cellphone.PhoneDS = PDS_DISABLED 
    OR g_Cellphone.PhoneDS = PDS_AWAY
    OR g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY
    OR g_Cellphone_FH_Needs_To_Exit = TRUE//Additional check for 295259. Looked like appTextMessage had altered the app state just as flashhand was aborting.
        g_MainGui_RendertargetNeedsReset = TRUE //Tell cellphone_flashhand.sc that the rendertarget should be reinitialised as failsafe. The app may have altered it.
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC

//Wiki-ed
//This will let you know if the contacts list is currently being displayed on the cellphone.

FUNC BOOL IS_CELLPHONE_ON_HOME_SCREEN()

    IF g_Cellphone.PhoneDS = PDS_MAXIMUM //phone in home screen display state.
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC

//Wiki-ed
//This will let you know if the contacts list is currently being displayed on the cellphone.

FUNC BOOL IS_CONTACTS_LIST_ON_SCREEN()

    #if USE_CLF_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContactsCLF")) > 0 //phone in contacts list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #endif
     #if USE_NRM_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContactsNRM")) > 0 //phone in contacts list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #endif
    #if not USE_SP_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContacts")) > 0 //phone in contacts list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #endif

ENDFUNC

//Wiki-ed
//This will let you know if the text message list is currently being displayed on the cellphone.

FUNC BOOL IS_TEXT_MESSAGE_LIST_ON_SCREEN()

    #if USE_CLF_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessCLF")) > 0 //phone in text_message list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #endif
    #if USE_NRM_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessNRM")) > 0 //phone in text_message list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE            
        ENDIF
    #endif
    #if not USE_SP_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessage")) > 0 //phone in text_message list state as script is running.
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #endif

ENDFUNC

//Wiki-ed
//This will let you know if the cellphone camera application is currently running fullscreen

FUNC BOOL IS_CELLPHONE_CAMERA_IN_USE()

    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appCamera")) > 0 
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC

PROC REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (BOOL ShouldReposition) 

    IF ShouldReposition = TRUE
        SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_REPOSITION_SUBS_FOR_LANDSCAPE_PHONE)
        #if IS_DEBUG_BUILD 
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Landscape Cellphone Camera will allow LONG subtitles. Set by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Landscape Cellphone Camera set to allow long subtitles.")
            cdPrintnl()
        #endif
    ELSE
        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_REPOSITION_SUBS_FOR_LANDSCAPE_PHONE)
        #if IS_DEBUG_BUILD 
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Landscape Cellphone Camera now set to NORMAL. Set by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Landscape Cellphone Camera now set to NORMAL.")
            cdPrintnl()
        #endif
    ENDIF

ENDPROC

//Will return true if the Cellphone camera application is in self-portrait mode.

FUNC BOOL IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()

    IF IS_BIT_SET (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_IS_CAMERA_IN_SELFIE_MODE)
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC

//Wiki-ed
//This will force all SP phones into Normal profile if they have been in SLEEP mode. This should really only be used by the flow to ensure that the phone is not in sleep mode after entering a mission trigger.
//New 27.03.12 - Quiet Mode has been added. If phones were in quiet mode, this will persist if this function is called. Quiet Mode still accepts calls, but does not play sound FX.

PROC SET_CELLPHONE_PROFILE_TO_NORMAL()

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_MICHAEL].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_MICHAEL].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Michael Cellphone Profile set to Normal from SET_CELLPHONE_PROFILE_TO_NORMAL. ")
            cdPrintnl()
        #endif
    ENDIF

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_FRANKLIN].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_FRANKLIN].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Franklin Cellphone Profile set to Normal from SET_CELLPHONE_PROFILE_TO_NORMAL. ")
            cdPrintnl()
        #endif
    ENDIF

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_TREVOR].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_TREVOR].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Trevor Cellphone Profile set to Normal from SET_CELLPHONE_PROFILE_TO_NORMAL. ")
            cdPrintnl()
        #endif
    ENDIF

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)
    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

    //MP? Doesn't have sleep mode.

ENDPROC

PROC SET_CELLPHONE_PROFILE_TO_SLEEP() //Requested by Ben in 1521392

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_MICHAEL].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_MICHAEL].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Michael Cellphone Profile set to SLEEP from SET_CELLPHONE_PROFILE_TO_SLEEP. ")
            cdPrintnl()
        #endif
    ENDIF

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_FRANKLIN].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_FRANKLIN].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Franklin Cellphone Profile set to SLEEP from SET_CELLPHONE_PROFILE_TO_SLEEP. ")
            cdPrintnl()
        #endif
    ENDIF

    IF This_Cellphone_Owner_Settings_ListContents[CHAR_TREVOR].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)
        This_Cellphone_Owner_Settings_ListContents[CHAR_TREVOR].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB - Trevor Cellphone Profile set to SLEEP from SET_CELLPHONE_PROFILE_TO_SLEEP. ")
            cdPrintnl()
        #endif
    ENDIF

    SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_SLEEP_SIGNIFIER)
    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)
    //MP? Doesn't have sleep mode.

ENDPROC

FUNC BOOL IS_CURRENT_PLAYER_IN_SLEEP_MODE()

    Get_Cellphone_Owner()
    
    IF This_Cellphone_Owner_Settings_ListContents[g_Cellphone.PhoneOwner].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_SLEEP_MODE)
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF
    
ENDFUNC

FUNC INT GET_SINGLEPLAYER_CELLPHONE_OPERATING_SYSTEM()

    #if IS_DEBUG_BUILD

        cdPrintstring("CELLPHONE_PUB - Returning int for OS Type.")
        cdPrintnl()

    #endif

    IF g_bInMultiplayer

        g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER

    ELSE

        g_Cellphone.PhoneOwner = GET_CURRENT_PLAYER_PED_ENUM()  //This does not return CHAR_MULTIPLAYER at this point if in MP, rather an array_bursting NO_CHARACTER.

    ENDIF

    IF g_Cellphone.PhoneOwner > CHAR_MULTIPLAYER

        g_Cellphone.PhoneOwner = CHAR_MULTIPLAYER //In the mission creator, the player ped is in a funny state so was returning 153! The game cannot set g_bInMultiplayer here either.
                                                  //Force the return value to be the multiplayer phone at this time as a precaution #1357711

        #if IS_DEBUG_BUILD

            cdPrintstring("CELLPHONE_PUB - GET_CURRENT_PLAYER_PED_ENUM() is returning an abnormal number, for safety, setting owner to CHAR_MULTIPLAYER.")
            cdPrintnl()

        #endif

    ENDIF
    #if USE_CLF_DLC
        RETURN  g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].OSTypeForThisPlayer
    #endif
    #if USE_NRM_DLC
        RETURN  g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].OSTypeForThisPlayer
    #endif
    #if not USE_SP_DLC
        RETURN  g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].OSTypeForThisPlayer
    #endif

ENDFUNC

//Wiki-ed.
//Returns the text_label string of the currently highlighted app on the cellphone. If you want to check if a specific app is highlighted use IS_THIS_APP_CURRENTLY_HIGHLIGHTED.
//Example Usage:
//CurrentAppString = GET_CURRENTLY_HIGHLIGHTED_APP() 

/*  This has been heavily impacted by Derek's scaleform changes. Commented out until we get a solution to the enforced delay to get_current_selection.
FUNC TEXT_LABEL GET_CURRENTLY_HIGHLIGHTED_APP()

    INT HighlightedIndex = 0 
    INT AdjustedAppIndex = 0

    TEXT_LABEL ReturnLabel = "" 

    IF g_Cellphone.PhoneDS = PDS_MAXIMUM //phone in homescreen state

        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("cellphone_flashhand")) > 0  //ergo cellphone scaleform must be loaded

            //HighlightedIndex = CALL_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
            BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")

            HighlightedIndex = END_SCALEFORM_MOVIE_METHOD_INT()

            IF (HighlightedIndex > ENUM_TO_INT(MAX_APPLICATIONS))
            //OR (HighlightedIndex = ENUM_TO_INT(MAX_APPLICATIONS))
            OR (HighlightedIndex < 0)

                HighlightedIndex = ENUM_TO_INT(MAX_APPLICATIONS) //Avoid array overrun and scaleform method returning -1 value if someone is using this to check another screen by mistake.

            ENDIF

            AdjustedAppIndex = AppSlot[HighlightedIndex]

            //ReturnString = GET_STRING_FROM_TEXT_FILE (g_AppList[AdjustedAppIndex].AppName)

            ReturnLabel = (g_AppList[AdjustedAppIndex].AppName)

        ELSE

            ReturnLabel = ""  //This has to be like this. Turns out you can't return NULL here.

        ENDIF

    ELSE    

        #IF IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_app is checking this frame but cellphone is not currently on home screen!")
            cdPrintnl()

        #endif

        ReturnLabel = ""  //This has to be like this. Turns out you can't return NULL here.

    ENDIF

    RETURN ReturnLabel

ENDFUNC

*/

FUNC BOOL IS_THE_SCRIPT_appContacts_RUNNING()               
    #if USE_CLF_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContactsCLF")) > 0 //phone in contacts list state
            RETURN TRUE
        ENDIF
    #endif
    #if USE_NRM_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContactsNRM")) > 0 //phone in contacts list state
            RETURN TRUE
        ENDIF
    #endif
    #if not USE_SP_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appContacts")) > 0 //phone in contacts list state
            RETURN TRUE
        ENDIF
    #endif
    RETURN FALSE
ENDFUNC

//Wiki-ed

//Returns the enum of the currently highlighted contact on the contacts list. 

//This has been heavily impacted by Derek's scaleform changes which forces us to wait for the scaleform return value to be ready.
//The WAIT (0) is expensive and not for the best in a public function, so this should be used only where absolutely necessary.

//You should therefore wrap this check with IS_CONTACTS_LIST_ON_SCREEN(). A warning line will spam if you don't.

//Example usage:
/*
    IF IS_CONTACTS_LIST_ON_SCREEN()
        IF GET_CURRENTLY_HIGHLIGHTED_CONTACT() = CHAR_DETONATEPHONE
            DISPLAY_TEXT_WITH_LITERAL_STRING (0.35, 0.84, "STRING", "CHAR_DETONATEPHONE_HIGHLIGHTED")
        ENDIF
    ENDIF
*/

FUNC enumCharacterList GET_CURRENTLY_HIGHLIGHTED_CONTACT()

    SCALEFORM_RETURN_INDEX SF_HighlightedContactIndex
    INT i_HighlightedContactIndex 
    INT AdjustedIndex = 0
    enumCharacterList ReturnCharacter = NO_CHARACTER
    INT TimeoutValveStarted, TimeOutValveCurrent

    IF g_bInMultiplayer

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is not available in MP.")
            cdPrintnl()

        #endif

        ReturnCharacter = NO_CHARACTER

    ELSE

        IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) = TRUE

            IF IS_THE_SCRIPT_appContacts_RUNNING()      
                IF IS_THE_SCRIPT_cellphone_flashhand_RUNNING()

                        BOOL TimeOutValveFired = FALSE

                        BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "GET_CURRENT_SELECTION")
                        SF_HighlightedContactIndex =END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

                        TimeOutValveStarted = GET_GAME_TIMER()

                        //This is an expensive fix for PT 1431163. Will make a better function after lockdown where cellphone_flashhand passes the value back.
                        WHILE (NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (SF_HighlightedContactIndex))
                        AND TimeOutValveFired = FALSE
                        AND HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) = TRUE

                            WAIT (0)

                            TimeOutValveCurrent = GET_GAME_TIMER()

                            IF ((TimeOutValveCurrent - TimeOutValveStarted) > 1500)
                                TimeOutValveFired = TRUE
                            ENDIF

                            #if IS_DEBUG_BUILD
                                cdPrintstring("CELLPHONE_PUB - GET_CURRENTLY_HIGHLIGHTED_CONTACT is waiting on return value from scaleform.")
                                cdPrintnl()

                                cdPrintstring( "SF_HighlightedContactIndex is: ")
                                PRINTINT (NATIVE_TO_INT(SF_HighlightedContactIndex))
                                cdPrintnl()

                            #endif

                        ENDWHILE

                        //Assume the return value must have been returned okay in the first instance...
                        i_HighlightedContactIndex = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (SF_HighlightedContactIndex)

                        #if USE_CLF_DLC
                            IF (i_HighlightedContactIndex > ENUM_TO_INT(MAX_CLF_CHARACTERS))
                            OR (i_HighlightedContactIndex < 0)
                                i_HighlightedContactIndex = ENUM_TO_INT(MAX_CLF_CHARACTERS) //Avoid array overrun and scaleform method returning -1 value if someone is using this to check another screen by mistake.
                            ENDIF                           
                        #endif
                        #if USE_NRM_DLC
                            IF (i_HighlightedContactIndex > ENUM_TO_INT(MAX_NRM_CHARACTERS))
                            OR (i_HighlightedContactIndex < 0)
                                i_HighlightedContactIndex = ENUM_TO_INT(MAX_NRM_CHARACTERS) //Avoid array overrun and scaleform method returning -1 value if someone is using this to check another screen by mistake.
                            ENDIF
                        #endif

                        #if not USE_SP_DLC
                            IF (i_HighlightedContactIndex > ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()))
                            OR (i_HighlightedContactIndex < 0)
                                i_HighlightedContactIndex = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()) //Avoid array overrun and scaleform method returning -1 value if someone is using this to check another screen by mistake.
                            ENDIF                      
                        #endif

                        AdjustedIndex = ContactListSlot[i_HighlightedContactIndex] 

                        ReturnCharacter = (INT_TO_ENUM (enumCharacterList, AdjustedIndex))

                        //Now check the other two breakout clauses didn't fire. If so return NO_CHARACTER.

                        IF HAS_SCALEFORM_MOVIE_LOADED (SF_MovieIndex) = FALSE  //Broke out of multi-condition WHILE above...

                            #if IS_DEBUG_BUILD

                                cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is checking this frame but cellphone movie is no longer loaded whilst getting return value. Returning NO CHARACTER")
                                cdPrintnl()

                            #endif

                            ReturnCharacter = NO_CHARACTER  //This has to be like this.

                        ENDIF

                        IF TimeOutValveFired = TRUE //Broke out of multi-condition WHILE above...

                            #if IS_DEBUG_BUILD

                                cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is checking this frame but timeout safety valve was fired. Returning NO CHARACTER")
                                cdPrintnl()

                            #endif

                            ReturnCharacter = NO_CHARACTER  //This has to be like this.

                        ENDIF

                    ELSE

                        #if IS_DEBUG_BUILD

                            cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is checking this frame but cellphone_flashhand no longer running. Returning NO CHARACTER")
                            cdPrintnl()

                        #endif

                        ReturnCharacter = NO_CHARACTER  //This has to be like this. Turns out you can't return NULL here.

                ENDIF

            ELSE    

                #if IS_DEBUG_BUILD

                    cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is checking this frame but cellphone is not currently on contacts screen.")
                    cdPrintnl()

                #endif

                ReturnCharacter = NO_CHARACTER  //This has to be like this. Turns out you can't return NULL here.

            //Seems pointless but fixes intellisense.
            ENDIF

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PUB - Get_currently_highlighted_contact is checking this frame but cellphone movie is no longer loaded.")
                cdPrintnl()

            #endif

            ReturnCharacter = NO_CHARACTER  //This has to be like this. Turns out you can't return NULL here.

        ENDIF

     ENDIF

     RETURN ReturnCharacter

ENDFUNC

//Wiki-ed

//This will check if the name of the app you pass in is the currently highlighted option on the cellphone homescreen
//Pass in the application name text label by referring to american_cellphone.txt. Search for "app names". They start from CELL_0 onwards.
//Example Usage:
//IF (Is_This_App_Currently_Highlighted ("CELL_16"))
//  cdPrintstring ("AppSettingsHighlighted")
//  cdPrintnl()
//ENDIF

//
/*  This relies on GET_CURRENTLY_HIGHLIGHTED_APP which has been heavily impacted by Derek's scaleform changes. Commented out until we get a solution.

FUNC BOOL IS_THIS_APP_CURRENTLY_HIGHLIGHTED (STRING WhichAppTextLabel)

    //IF (ARE_STRINGS_EQUAL (Get_Currently_Highlighted_App(), WhichAppTextLabel)) //Direct comparison won't compile...

    TEXT_LABEL Temp_Label = Get_Currently_Highlighted_App()

    IF ARE_STRINGS_EQUAL (Temp_Label, WhichAppTextLabel)

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC
*/

//Wiki-ed

//This will check if the name of the contact you pass in is the currently highlighted option on the contacts list
//Pass in the character name text label from the character sheet or refer to american_cellphone.txt.  The label will be of the form CELL_1xx.
//Example Usage:
//IF (Is_This_Contact_Currently_Highlighted ("CELL_103"))
//  cdPrintstring ("Franklin Highlighted")
//  cdPrintnl()
//ENDIF

/*  This relies on GET_CURRENTLY_HIGHLIGHTED_CONTACT which has been heavily impacted by Derek's scaleform changes. Commented out until we get a solution.

FUNC BOOL IS_THIS_CONTACT_CURRENTLY_HIGHLIGHTED (STRING WhichContactLabel)

    //IF (ARE_STRINGS_EQUAL (Get_Currently_Highlighted_Contact(), WhichContactName)) //Direct comparison won't compile.

    TEXT_LABEL Temp_Label = Get_Currently_Highlighted_Contact()

    IF ARE_STRINGS_EQUAL (Temp_Label, WhichContactLabel)

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC
*/

//Wiki-ed.
//Check if the cellphone camera script has just taken a picture. Call this every frame in the segment of script where you want to check.
//The global bool, Cellphone_Pic_Just_Taken, resets to false after two seconds. ( temp measure )

FUNC BOOL HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()

    IF Cellphone_Pic_Just_Taken
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC


//Added to allow external scripts know if Snapmatic, the cellphone cam,  is running but is in the "delete / save " picture mode rather than in the viewfinder.
FUNC BOOL IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE()

    IF g_Cell_Pic_Stage = PIC_STAGE_HOLDING_LQ_COPY
    OR g_Cell_Pic_Stage > PIC_STAGE_HOLDING_LQ_COPY

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC





//This can be used to prevent the cellphone's camera application displaying "delete" and "continue" and attempting to save to the cloud gallery when "continue" is selected.
//When passed with TRUE, this proc will only display "continue" and when that is selected, no save will take place and the camera readies itself for another photo to be taken.
PROC BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE (BOOL b_PassedBypass)

    IF b_PassedBypass = TRUE

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE)

        #if IS_DEBUG_BUILD
            PRINTNL()       
            PRINTLN("\nCELLPHONE_PUB - Cellphone Camera Default Save Routine BYPASSED from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            PRINTNL() 
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_BYPASS_CAMERA_APP_SAVE_ROUTINE)

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - Cellphone Camera Default Save Routine REINSTATED from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            PRINTNL()

        #endif

    ENDIF

ENDPROC

//Special procedure for Docks_Setup1 mission. During a picture taking sequence it was requested that the player's actions are inhibited. 
PROC INHIBIT_CELLPHONE_CAMERA_FUNCTIONS_FOR_DOCKS_SETUP(BOOL b_ShouldInhibit = FALSE)

    IF b_ShouldInhibit = TRUE

        SET_BIT (BitSet_CellphoneDisplay, g_BS_DOCKS_SETUP_TUTORIAL_ONGOING)

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Docks Setup tutorial bit has been set. Cellphone Camera joypad button functionality inhibited.")
            cdPrintnl()
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DOCKS_SETUP_TUTORIAL_ONGOING)

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Docks Setup tutorial bit cleared. Cellphone Camera functionality returned to normal.")
            cdPrintnl()
        #endif

    ENDIF

ENDPROC

FUNC BOOL IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()

    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_DOCKS_SETUP_TUTORIAL_ONGOING)

        RETURN TRUE 

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

/*Moved to dialogue_public.sch
//Check this after an inbound cellphone call has been successfully launched.
FUNC BOOL CHECK_CELLPHONE_LAST_CALL_REJECTED()

    IF g_LastInboundCallRejected
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC
*/

//Wiki-ed.
//Check if the phone is in disabled mode or not.
FUNC BOOL IS_CELLPHONE_DISABLED()

    IF g_Cellphone.PhoneDS = PDS_DISABLED

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Check if the phone is in disabled mode or is being disabled this frame only. This cannot be combined with the above and is primarily a check for MP peculiarity.

//Self note: If an existing script checked for IS_CELLPHONE_DISABLED before deciding if they should re-enable or not when quitting, e.g the debug menu, and I_C_D included a check for this_frame_only
//then there's a chance that the phone would not be re-enabled if the DISABLE_THIS_FRAME_ONLY call has ceased while in the menu. Some things still need the phone fully disabled.
//Will change next game to everyone disabling_this_frame_only. Too many scripts are using the system "as was" to alter this late in development.

FUNC BOOL IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()

    IF g_Cellphone.PhoneDS = PDS_DISABLED
    OR g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Wiki-ed.
//Allows the phone to be hidden but remain active during any ongoing, answered phonecall. Should only really be used for hotswap transitions where swapping on the fly 
//between character phones is necessary.

PROC HIDE_PHONE_FOR_HOTSWAP (BOOL ShouldHidePhone)

    IF ShouldHidePhone

        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL //only allow phone to be hidden for hotswap during ongoing phonecall

            g_Phone_Active_but_Hidden = TRUE

            #if IS_DEBUG_BUILD

                PRINTLN("\nCELLPHONE_PUB - Hide phone from hotswap true via [", GET_THIS_SCRIPT_NAME(), "] through public function.")

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Phone display has been hidden for hotswap transition.")
                cdPrintnl()

            #endif

        ENDIF

    ELSE

        g_Phone_Active_but_Hidden = FALSE

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Hide phone from hotswap false via [", GET_THIS_SCRIPT_NAME(), "] through public function.")

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Phone should be displayed - hotswap hide no longer active")
            cdPrintnl()

        #endif

    ENDIF

ENDPROC

// This should be called if we want to put away the phone smoothly, the same animation that plays when we hang up the phone. This function can be used if we do not want to actually hang it up yet.
// HIDE_ACTIVE_PHONE does the same thing but without animation and instead it will instantly hide the phone from the screen. This also causes an issue because g_Cellphone.PhoneDS = PDS_AWAY 
// is called inside there which makes the animation play at the END of the call too if we keep the phone hidden.

PROC PUT_AWAY_ACTIVE_PHONE ()
	
	IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()

        g_Cellphone.PhoneDS = PDS_AWAY

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 362-b. CELLPHONE_PUB  PUT_AWAY_ACTIVE_PHONE assigns PDS_AWAY")
            cdPrintnl()   
        #endif

    ENDIF
	
ENDPROC

//DANGER!
//You really need a very good reason for doing this. It's in place for use by the full screen Internet that is launched from the phone. 
//The in-game internet is a special case - it pauses the game. If you think you need to use this function, then please email Steven Taylor beforehand.
//This will hide a cellphone which is currently onscreen and not allow any button presses to interact with it. 

PROC HIDE_ACTIVE_PHONE (BOOL ShouldHidePhone, BOOL UseCamPositions = FALSE)
	DEBUG_PRINTCALLSTACK()
    IF ShouldHidePhone

        IF IS_PHONE_ONSCREEN () //only allow phone to be hidden if it's onscreen

            g_Phone_Active_but_Hidden = TRUE

            IF UseCamPositions //if we are using this proc to hide the cellphone during camera functionality, we need to keep a hold of its current position.

                GET_MOBILE_PHONE_POSITION (g_TempCamPosVec)

            ENDIF

            g_3dPhonePosVec = g_This_Screen_3dPhoneStartVec[g_Chosen_Ratio] 

            //Update the 3d phone position...
            SET_MOBILE_PHONE_POSITION (g_3dPhonePosVec)

            #if IS_DEBUG_BUILD

                PRINTLN("\nCELLPHONE_PUB - Hide active phone set to true via [", GET_THIS_SCRIPT_NAME(), "] through public function.")

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Phone display has now been hidden.")
                cdPrintnl()

            #endif

        ENDIF

    ELSE

        IF g_Phone_Active_but_Hidden = TRUE //only let this happen if the phone has already been hidden.

            g_Phone_Active_but_Hidden = FALSE

            g_3dPhonePosVec = g_This_Screen_3dPhoneEndVec[g_Chosen_Ratio] 

            IF UseCamPositions

                 SET_MOBILE_PHONE_POSITION (g_TempCamPosVec) //restore previous cellphone camera position if need be.

            ELSE

                //Update the 3d phone position...
                SET_MOBILE_PHONE_POSITION (g_3dPhonePosVec)

            ENDIF

            #if IS_DEBUG_BUILD

                PRINTLN("\nCELLPHONE_PUB - Hide active phone now false via [", GET_THIS_SCRIPT_NAME(), "] through public function.")

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Phone should be displayed - no longer hidden.")
                cdPrintnl()

            #endif

        ENDIF

    ENDIF

ENDPROC

//Wiki-ed
//Will terminate any ongoing phonecall and force the cellphone down offscreen, out of the player's hand. Setting the bool parameter to TRUE will force the hud phone
//off screen instantly rather than smoothly moving down the screen.

PROC HANG_UP_AND_PUT_AWAY_PHONE (BOOL b_MovePhoneOffScreenInstantly = FALSE)
	
	IF IS_DRONE_SNAPMATIC_ACTIVE()
		PRINTLN("\nCELLPHONE_PUB - IGNORE HANG_UP_AND_PUT_AWAY_PHONE called by [", GET_THIS_SCRIPT_NAME(), "] through public function as drone snapmatic is active")
		EXIT
	ENDIF

    IF g_Phone_Active_but_Hidden 
		IF IS_LOCAL_PLAYER_USING_DRONE()
         	HIDE_ACTIVE_PHONE(TRUE, TRUE)
		ELSE
			 HIDE_ACTIVE_PHONE (FALSE) //make sure phone isn't hidden.
		ENDIF	
    ENDIF    

    IF g_cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
    OR g_cellphone.PhoneDS = PDS_ONGOING_CALL

        //Set bit for interrogating if a script hang up has occurred during a call state.
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAST_CALL_ABORTED_DUE_TO_SCRIPT_HANGUP)

        //SetUpPhoneConversation() in dialogue private will clear this bit when the next phonecall is accepted into the system.

    ENDIF

    //Perhaps class as an interruption? It can only be done by script, an end user hang up will result in the "interruption" flag triggering already.

    IF IS_MOBILE_PHONE_CALL_ONGOING()

        STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation as this is a call....

    ENDIF

    #if IS_DEBUG_BUILD

        PRINTLN("\nCELLPHONE_PUB - HANG_UP_AND_PUT_AWAY_PHONE called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif

    g_ConversationStatus = CONV_STATE_HANGUPAWAY //tell dialogue handler to clean up a call using HungupAwayCleanup specifically.

    IF b_MovePhoneOffScreenInstantly = TRUE 

        SET_BIT (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY)

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Move phone down instantly set by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_MOVE_PHONE_DOWN_INSTANTLY)  

    ENDIF   

    //Candidate for inclusion... post August milestone. Should get done by CONV_STATE_HANGUPAWAY but might be a wee bit safer below.
    //Put phone away... only if not disabled

    IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()

        g_Cellphone.PhoneDS = PDS_AWAY

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 362. CELLPHONE_PUB  HANG_UP_AND_PUT_AWAY_PHONE assigns PDS_AWAY")
            cdPrintnl()   
        #endif

    ENDIF

ENDPROC

//Wiki-ed.
//Puts phone in disabled mode - this means pressing up on the d-pad will NOT bring the phone on screen and setting this to true will hang up and put away the phone.
PROC DISABLE_CELLPHONE (BOOL IsPhoneDisabled)

    IF IsPhoneDisabled

        Check_For_Ongoing_Call_Interruption() //added for bug #413480

        IF g_cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        OR g_cellphone.PhoneDS = PDS_ONGOING_CALL

            //Set bit for interrogating if a script hang up has occurred during a call state.
            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAST_CALL_ABORTED_DUE_TO_SCRIPT_HANGUP)

            //SetUpPhoneConversation() in dialogue private will clear this bit when the next phonecall is accepted into the system.

        ENDIF

        g_Cellphone.PhoneDS = PDS_DISABLED

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 8. Cellphone Public assigns PDS_DISABLED")
            cdPrintnl()   
        #endif

        IF IS_PHONE_ONSCREEN()
            HANG_UP_AND_PUT_AWAY_PHONE() //tidy up call...
        ENDIF

        //Moved above for bug 69357.
        //g_Cellphone.PhoneDS = PDS_DISABLED

        #if IS_DEBUG_BUILD 

            g_Cellphone_Disable_WidgetToggle = TRUE //Update debug widget to let it know that the phone is disabled.

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Cellphone disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Mobile phone has been set to disabled via script DISABLE_CELLPHONE (TRUE). No calls can be passed to it!")
            cdPrintnl()

        #endif

    ELSE 

        IF g_Cellphone.PhoneDS = PDS_DISABLED  //Look at this! Debug menu is acting weirdly when running every frame test!

            IF NOT (g_Cellphone.PhoneDS = PDS_DISABLED_THIS_FRAME_ONLY)

                g_Cellphone.PhoneDS = PDS_AWAY

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 9. Cellphone Public assigns PDS_AWAY")
                    cdPrintnl()   
                #endif

            ENDIF

            #if IS_DEBUG_BUILD 

                g_Cellphone_Disable_WidgetToggle = FALSE //Update debug widget to let it know that the phone is enabled.

                //Make sure this always displays regardless of phone debug text widget toggle.
                PRINTLN("\nCELLPHONE_PUB - Cellphone re-enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Mobile phone enabled via script DISABLE_CELLPHONE (FALSE). Calls enabled!")
                cdPrintnl()

            #endif

        ENDIF

    ENDIF

ENDPROC

PROC DISABLE_CELLPHONE_THIS_FRAME_ONLY(BOOL b_MovePhoneOffScreenInstantly = FALSE)
	
	IF IS_DRONE_SNAPMATIC_ACTIVE()
		PRINTLN("\nCELLPHONE_PUB - IGNORE DISABLE_CELLPHONE_THIS_FRAME_ONLY called by [", GET_THIS_SCRIPT_NAME(), "] through public function as drone snapmatic is active")
		EXIT
	ENDIF
	
    IF NOT (g_Cellphone.PhoneDS = PDS_DISABLED) //Phone has already been fully disabled. Take no action.

        IF IS_PHONE_ONSCREEN()

            HANG_UP_AND_PUT_AWAY_PHONE(b_MovePhoneOffScreenInstantly) //tidy up call...

        ENDIF

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CELLPHONE_THIS_FRAME)

    ENDIF

    #if IS_DEBUG_BUILD
        IF g_DumpDisableEveryFrameCaller
        OR g_DisplayNewSideTaskSignifier = TRUE

            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - Attempt to disable cellphone THIS FRAME from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
			
			DEBUG_PRINTCALLSTACK()

            IF g_DisplayNewSideTaskSignifier = TRUE

                cdPrintstring("DISABLE_CELLPHONE_THIS_FRAME_ONLY() calling script has been detailed above due to attempted user takeout. Search for Attempt to disable.")
                cdPrintnl()

                g_DisplayNewSideTaskSignifier = FALSE

            ENDIF

        ENDIF 
    #endif  

ENDPROC

PROC BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL (BOOL ShouldBlockMovement = FALSE)

    IF ShouldBlockMovement

        SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL set to TRUE from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            //PRINTLN("\nCELLPHONE_PUB - Also setting fully vanished bit to TRUE to solve url:bugstar:1866104")
        #endif

    ELSE    

        CLEAR_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_DO_NOT_MOVE_PHONE_UP_ON_LAUNCH)

        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - BLOCK_HUD_PHONE_MOVEMENT_FOR_SWITCH_SCENE_CALL set to FALSE from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

ENDPROC

PROC DISABLE_CELLPHONE_DIALOGUE_LINE_SKIP (BOOL bLineSkipDisable)

    IF bLineSkipDisable

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CELLPHONE_LINESKIP)

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - Cellphone Dialogue line skip disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CELLPHONE_LINESKIP)

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - Cellphone Dialogue line skip enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

    ENDIF

ENDPROC

PROC DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()

    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_CAMERA_APP_THIS_FRAME)

    //Comment and compile with this line to see who is disabling at any given time...

    #if IS_DEBUG_BUILD
        IF g_DumpDisableEveryFrameCaller

            PRINTNL()
            PRINTLN("\nCELLPHONE_PUB - Cellphone Camera App disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

            DEBUG_PRINTCALLSTACK()

        ENDIF
    #endif

ENDPROC

PROC DISABLE_CELLPHONE_INTERNET_APP_THIS_FRAME_ONLY()

    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REQUESTED_DISABLE_INTERNET_APP_THIS_FRAME)                                        

ENDPROC

//Wiki-ed
//Allows script to alter the contact names displayed on the calling screen on the fly - only really useful for hot swap transitions so that names can be changed mid-phonecall.
//The second parameter, can be forgotten about - unless you are altering names mid-conference call and need to alter both names on the calling screen.
//Contact names are taken from the character list as normal. e.g CHAR_ARIANO

PROC CHANGE_CALLING_SCREEN_CONTACT_NAMES (enumCharacterList PrimaryChar, enumCharacterList SecondaryChar = NO_CHARACTER)

    //This needs updated to change scaleform method.

    g_TheContactInvolvedinCall = PrimaryChar
    g_AdditionalContactInvolvedinCall = SecondaryChar

ENDPROC

//You should only call this after checking the phone is fully moved up onscreen or if appContacts is running. 
//This is because, for safety reasons, the phone will reset any force on launch of its main graphics handling script.
//e.g Best usage:
/*
IF IS_CONTACTS_LIST_ON_SCREEN()
    FORCE_SELECTION_OF_THIS_CONTACT_ONLY (CHAR_LAMAR)
ENDIF
*/

PROC FORCE_SELECTION_OF_THIS_CONTACT_ONLY (enumCharacterList ForcedChar)

    g_ForcedContactSelection = ForcedChar

ENDPROC                                                                        

PROC ENABLE_AUTO_SCROLL_TO_CONTACT (enumCharacterList DesiredContact)

    g_AutoScrollContact = DesiredContact

    SET_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

    #if IS_DEBUG_BUILD 
        cdPrintnl()
        cdPrintstring ("CELLPHONE_PUB - Auto Scroll for contact bit has been set. Will auto scroll if appContacts runs.")
        cdPrintnl()
    #endif

ENDPROC

PROC DISABLE_AUTO_SCROLL_TO_CONTACT()

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_AUTO_SCROLL_CONTACT_ENABLED)

    #if IS_DEBUG_BUILD 
        cdPrintnl()
        cdPrintstring ("CELLPHONE_PUB - Auto Scroll for contact bit has been disabled.")
        cdPrintnl()
    #endif

ENDPROC

//DO NOT CALL! This is here for convenience of reference.

PROC Create_Contact_Feed_Entry_Immediately()

    TEXT_LABEL_63 s_TXD_Contact_decision
    
    #if USE_CLF_DLC
        s_TXD_Contact_decision = (GET_FILENAME_FOR_AUDIO_CONVERSATION (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[g_Temp_ContactHeadshotRef].picture))
    #endif
    #if USE_NRM_DLC
        s_TXD_Contact_decision = (GET_FILENAME_FOR_AUDIO_CONVERSATION (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[g_Temp_ContactHeadshotRef].picture))
    #endif
    #if not USE_SP_DLC
        s_TXD_Contact_decision = (GET_FILENAME_FOR_AUDIO_CONVERSATION (g_sCharacterSheetAll[g_Temp_ContactHeadshotRef].picture))
    #endif

    #if IS_DEBUG_BUILD  //Important, make sure this is always in the logs.
        PRINTNL()
        PRINTSTRING("Extract headshot txd was ")
        PRINTSTRING (s_TXD_Contact_Decision)
        PRINTNL()
    #endif

    IF g_UseContactSignifierNewType = FALSE

        BEGIN_TEXT_COMMAND_THEFEED_POST ("")

            TEXT_LABEL_63 s_TempContactHolder

            s_TempContactHolder = GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText)

            STRING s_ContactAddedString
            s_ContactAddedString = GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_253")

                #if IS_DEBUG_BUILD

                    cdPrintstring ("Strings for contact test are:  ")
                    cdPrintstring (s_TXD_Contact_decision) 
                    cdPrintstring (", ")
                    cdPrintstring (s_TempContactHolder)
                    cdPrintstring (", ")
                    cdPrintstring (s_ContactAddedString)
                    cdPrintnl()

                #endif

        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT (s_TXD_Contact_decision, s_TXD_Contact_decision, FALSE, TEXT_ICON_NEW_CONTACT, s_ContactAddedString, s_TempContactHolder)

    ELSE

        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_255")

            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (SignifierData[SIG_CONTACT1].SignifierPrimaryText)

        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(s_TXD_Contact_decision, s_TXD_Contact_decision, FALSE, TEXT_ICON_NEW_CONTACT, "")

    ENDIF

    #if IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("CELLPHONE_CONT - Created New Contact feed entry. ")
        cdPrintint (g_Current_Number_of_Signifiers)
        cdPrintnl ()

            SWITCH g_Cellphone.PhoneOwner

                CASE CHAR_MICHAEL  

                    PRINTLN("Owner this frame is Michael - ")

                BREAK

                CASE CHAR_FRANKLIN  

                    PRINTLN("Owner this frame is Franklin - ")

                BREAK

                CASE CHAR_TREVOR 

                    PRINTLN("Owner this frame is Trevor - ")

                BREAK

                DEFAULT

                    PRINTLN("Owner this frame is MP - ")

                BREAK

            ENDSWITCH

            PRINTSTRING("CELLPHONE_CONT Created new feed entry as this contact has been added for the current owner. New contact: ")
            PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(SignifierData[SIG_CONTACT1].SignifierPrimaryText))
            PRINTNL()

    #endif

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)

ENDPROC

//Wiki-ed.
//Adds a new contact to the phonebooks of each playable character. The new contact should be in the form CHAR_NAME from the character enumlist in charsheet_globals.sch
//Once added to a phonebook, the new contact will appear in the player's phone's contact list provided the character sheet has designated that the new character is allowed to
//be included in the contacts list. 
//The full phonebooks enum is as follows:
//    MICHAEL_BOOK,  //0
//    FRANKLIN_BOOK, //1
//    TREVOR_BOOK,   //2
//
//    MULTIPLAYER_BOOK, //3
//    ALL_OWNERS_BOOKS
//The third parameter takes in a bool which lets cellphone_controller.sc know if it should display a "new contact available" message. This defaults to true.

PROC ADD_CONTACT_TO_PHONEBOOK (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook, BOOL ShouldDisplayNewContactSignifier = TRUE)

    g_Temp_ContactHeadshotRef = CharacterToAdd
    //Check if the character passed is allowed to be added to the phonebook via reference to the character sheet.   
    IF g_CharacterSheetNonSaved[CharacterToAdd].phone <> NO_PHONE_CONTACT  //may do fancier check for last characters of string so names can be matched explicitly. Safer that way.

        Get_Cellphone_Owner()
        IF WhichPhoneBook = ALL_OWNERS_BOOKS        
            #if USE_CLF_DLC
                //Add contact as listed in all three characters books. Not done in 0 to 2 loop for ease of testing...
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[0] = LISTED
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[1] = LISTED
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[2] = LISTED
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[0] = KNOWN_CALLER
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[1] = KNOWN_CALLER
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[2] = KNOWN_CALLER
            #endif
            #if USE_NRM_DLC
                //Add contact as listed in all three characters books. Not done in 0 to 2 loop for ease of testing...
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[0] = LISTED
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[1] = LISTED
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[2] = LISTED
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[0] = KNOWN_CALLER
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[1] = KNOWN_CALLER
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[2] = KNOWN_CALLER
            #endif

            #if not USE_SP_DLC
                //Add contact as listed in all three characters books. Not done in 0 to 2 loop for ease of testing...
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 0, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 1, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 2, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 0, KNOWN_CALLER)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 1, KNOWN_CALLER)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 2, KNOWN_CALLER)
            #endif

        ELSE
            //For bug 900228, if the contact has already been added then we will make sure that we don't display the new contact signifier in the feed entry.
            #if USE_CLF_DLC
                IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT(WhichPhonebook)] = LISTED
                AND g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT(WhichPhonebook)] = KNOWN_CALLER
                    #if IS_DEBUG_BUILD
                        PRINTNL()           
                        PRINTSTRING("CELLPHONE_PUB - t. Will not create feed entry for new contact as this contact has already been added as a known caller.")
                        PRINTNL()
                    #endif

                    #if USE_TU_CHANGES
                        ShouldDisplayNewContactSignifier = FALSE
                    #endif 

                ENDIF
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = LISTED
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = KNOWN_CALLER
            #endif
             #if USE_NRM_DLC
                IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT(WhichPhonebook)] = LISTED
                AND g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT(WhichPhonebook)] = KNOWN_CALLER
                    #if IS_DEBUG_BUILD
                        PRINTNL()           
                        PRINTSTRING("CELLPHONE_PUB - t. Will not create feed entry for new contact as this contact has already been added as a known caller.")
                        PRINTNL()
                    #endif

                    #if USE_TU_CHANGES
                        ShouldDisplayNewContactSignifier = FALSE
                    #endif

                ENDIF
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = LISTED
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = KNOWN_CALLER
            #endif

            #if not USE_SP_DLC
                IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CharacterToAdd, ENUM_TO_INT(WhichPhonebook)) = LISTED 
                AND GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(CharacterToAdd, ENUM_TO_INT(WhichPhonebook)) = KNOWN_CALLER 
                    #if IS_DEBUG_BUILD
                        PRINTNL()           
                        PRINTSTRING("CELLPHONE_PUB - t. Will not create feed entry for new contact as this contact has already been added as a known caller.")
                        PRINTNL() 
                    #endif

                    #if USE_TU_CHANGES
                        ShouldDisplayNewContactSignifier = FALSE
                    #endif

                ENDIF
               	INT iPhoneBookID = ENUM_TO_INT(WhichPhonebook)
                GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd,  iPhoneBookID, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, iPhoneBookID, KNOWN_CALLER)
            #endif 
			
			
			#IF FEATURE_TUNER
				IF CharacterToAdd = CHAR_SESSANTA
				AND NOT MPGlobalsAmbience.bAddSessantaContactWithTicker
					ShouldDisplayNewContactSignifier = FALSE
				ENDIF
				
				IF CharacterToAdd = CHAR_MOODYMANN
				AND NOT MPGlobalsAmbience.bAddMoodymannContactWithTicker
					ShouldDisplayNewContactSignifier = FALSE
				ENDIF
				
				IF CharacterToAdd = CHAR_KDJ
				AND NOT MPGlobalsAmbience.bAddMoodymannContactWithTicker
					ShouldDisplayNewContactSignifier = FALSE
				ENDIF
			#ENDIF
        ENDIF

            IF ShouldDisplayNewContactSignifier     
                //For bug 880374, we should also hold a buffer string of the contact for the chosen recipient player character 

                IF NOT g_bInMultiplayer // don't include this check in MP
                    IF WhichPhoneBook <> ALL_OWNERS_BOOKS              
                        IF ENUM_TO_INT(g_Cellphone.PhoneOwner) <> ENUM_TO_INT(WhichPhoneBook)
                        #if USE_CLF_DLC
                            BufferedContactSignifierText[ENUM_TO_INT(WhichPhoneBook)] = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                        #endif
                         #if USE_NRM_DLC
                            BufferedContactSignifierText[ENUM_TO_INT(WhichPhoneBook)] = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                        #endif
                        #if not USE_SP_DLC
                            BufferedContactSignifierText[ENUM_TO_INT(WhichPhoneBook)] = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)
                        #endif 
                            BufferedContactAvailable[ENUM_TO_INT(WhichPhoneBook)] = TRUE
                            BufferedHeadshotRef[ENUM_TO_INT(WhichPhoneBook)] = CharacterToAdd
                            #if IS_DEBUG_BUILD
                                PRINTNL()           
                                PRINTSTRING("CELLPHONE_PUB - Will not create feed entry for new contact as phonebook owner is not under control. Adding this contact to buffer:")
                                PRINTNL()
                                #if USE_CLF_DLC
                                PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label))
                                #endif
                                #if USE_NRM_DLC
                                PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label))
                                #endif
                                #if not USE_SP_DLC
                                PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CharacterToAdd].label))
                                #endif 
                                PRINTNL()
                                PRINTSTRING("Phonebook to add to was ")
                                PRINTINT(ENUM_TO_INT(WhichPhoneBook))
                                PRINTSTRING(" but current owner is ")
                                PRINTINT(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                                PRINTNL()
                            #endif

                        ELSE                                   
                            IF CharacterToAdd = g_Cellphone.PhoneOwner   
                                #if IS_DEBUG_BUILD
                                    PRINTNL()           
                                    PRINTSTRING("CELLPHONE_PUB - Will not create feed entry for new contact as the following is the same character as the current player.")
                                    PRINTNL()
                                    #if USE_CLF_DLC
                                    PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label))
                                    #endif
                                    #if USE_NRM_DLC
                                    PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label))
                                    #endif
                                    #if not USE_SP_DLC
                                    PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CharacterToAdd].label))
                                    #endif
                                    PRINTNL()
                                    PRINTSTRING("Phonebook to add to was ")
                                    PRINTINT(ENUM_TO_INT(WhichPhoneBook))
                                    PRINTSTRING(" and current owner is ")
                                    PRINTINT(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                                    PRINTNL()
                                #endif

                            ELSE       
                                #if USE_CLF_DLC
                                SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                                #endif
                                #if USE_NRM_DLC
                                SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                                #endif
                                #if not USE_SP_DLC
                                SignifierData[SIG_CONTACT1].SignifierPrimaryText = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)
                                #endif 
                                
                                SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
                                Create_Contact_Feed_Entry_Immediately()
                            ENDIF
                        ENDIF    
                    ELSE
                        #if IS_DEBUG_BUILD
                            PRINTNL()           
                            PRINTSTRING("CELLPHONE_PUB - Contact added to ALL_OWNERS_BOOKS. Allowing feed entry creation.")
                            PRINTNL()
                        #endif
                        #if USE_CLF_DLC
                        SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                        #endif
                        #if USE_NRM_DLC
                        SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                        #endif
                        #if not USE_SP_DLC
                        SignifierData[SIG_CONTACT1].SignifierPrimaryText = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)
                        #endif
                        SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
                        Create_Contact_Feed_Entry_Immediately()
                    ENDIF
                ELSE
                    #if IS_DEBUG_BUILD
                        PRINTNL()           
                        PRINTSTRING("CELLPHONE_PUB - Multiplayer contact added. Allowing feed entry creation.")
                        PRINTNL()
                    #endif
                    #if USE_CLF_DLC
                    SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                    #endif
                    #if USE_NRM_DLC
                    SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
                    #endif 
                    #if not USE_SP_DLC
                    SignifierData[SIG_CONTACT1].SignifierPrimaryText = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)
                    #endif                     
                    SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
                    Create_Contact_Feed_Entry_Immediately()
                ENDIF
            ENDIF    //end of ShouldDisplayNewContactSignifier  signifier check.

        #if IS_DEBUG_BUILD 
            PRINTLN("\nCELLPHONE_PUB - Add contact to phonebook called via [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            cdPrintnl()             
            PRINTLN("CELLPHONE_PUB - Adding known contact to phonebook: ")
            #if USE_CLF_DLC
            PRINTLN(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label)
            #endif
            #if USE_NRM_DLC
            PRINTLN(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label)
            #endif
            #if not USE_SP_DLC
            PRINTLN(Get_String_From_TextLabel(g_sCharacterSheetAll[CharacterToAdd].label))
            #endif 
            cdPrintnl()
        #endif
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character should not be added as a phone contact. See Steve or Keith")
        #endif
    ENDIF
ENDPROC

#if USE_CLF_DLC
PROC ADD_UNKNOWN_CONTACT_TO_PHONEBOOK_CLF (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook, BOOL ShouldDisplayNewContactSignifier = FALSE)
    //Check if the character passed is allowed to be added to the phonebook via reference to the character sheet.   
    IF g_CharacterSheetNonSaved[CharacterToAdd].phone <> NO_PHONE_CONTACT  //may do fancier check for last characters of string so names can be matched explicitly. Safer that way.

        IF WhichPhoneBook = ALL_OWNERS_BOOKS
            //Add contact as listed in all three characters books.  Not done in 0 to 2 loop for ease of testing...
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[0] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[1] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[2] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[0] = UNKNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[1] = UNKNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[2] = UNKNOWN_CALLER
        ELSE
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = UNKNOWN_CALLER
        ENDIF

        IF ShouldDisplayNewContactSignifier
            SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
            SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
            //SignifierData[SIG_CONTACT1].SignifierCharacter = CharacterToAdd
            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)
        ENDIF
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character should not be added as a phone contact. See Steve or Keith")
        #endif
    ENDIF
ENDPROC
#endif
#if USE_NRM_DLC
PROC ADD_UNKNOWN_CONTACT_TO_PHONEBOOK_NRM (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook, BOOL ShouldDisplayNewContactSignifier = FALSE)
    //Check if the character passed is allowed to be added to the phonebook via reference to the character sheet.   
    IF g_CharacterSheetNonSaved[CharacterToAdd].phone <> NO_PHONE_CONTACT  //may do fancier check for last characters of string so names can be matched explicitly. Safer that way.

        IF WhichPhoneBook = ALL_OWNERS_BOOKS
            //Add contact as listed in all three characters books.  Not done in 0 to 2 loop for ease of testing...
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[0] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[1] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[2] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[0] = UNKNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[1] = UNKNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[2] = UNKNOWN_CALLER
        ELSE
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = UNKNOWN_CALLER
        ENDIF

        IF ShouldDisplayNewContactSignifier
            SignifierData[SIG_CONTACT1].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToAdd].label
            SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
            //SignifierData[SIG_CONTACT1].SignifierCharacter = CharacterToAdd
            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)
        ENDIF
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character should not be added as a phone contact. See Steve or Keith")
        #endif
    ENDIF
ENDPROC
#endif

PROC ADD_UNKNOWN_CONTACT_TO_PHONEBOOK (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook, BOOL ShouldDisplayNewContactSignifier = FALSE)
#if USE_CLF_DLC
    ADD_UNKNOWN_CONTACT_TO_PHONEBOOK_CLF(CharacterToAdd,WhichPhonebook,ShouldDisplayNewContactSignifier)
#endif
#if USE_NRM_DLC
    ADD_UNKNOWN_CONTACT_TO_PHONEBOOK_NRM(CharacterToAdd,WhichPhonebook,ShouldDisplayNewContactSignifier)
#endif

#if not USE_SP_DLC
    //Check if the character passed is allowed to be added to the phonebook via reference to the character sheet.   
    IF g_CharacterSheetNonSaved[CharacterToAdd].phone <> NO_PHONE_CONTACT  //may do fancier check for last characters of string so names can be matched explicitly. Safer that way.

        IF WhichPhoneBook = ALL_OWNERS_BOOKS
            //Add contact as listed in all three characters books.  Not done in 0 to 2 loop for ease of testing...
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 0, LISTED)
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 1, LISTED)
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, 2, LISTED)
			
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 0, UNKNOWN_CALLER)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 1, UNKNOWN_CALLER)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, 2, UNKNOWN_CALLER)
        ELSE
            GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToAdd, ENUM_TO_INT (WhichPhonebook), LISTED)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToAdd, ENUM_TO_INT (WhichPhonebook), UNKNOWN_CALLER)
        ENDIF

        IF ShouldDisplayNewContactSignifier
            SignifierData[SIG_CONTACT1].SignifierPrimaryText = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)
            SignifierData[SIG_CONTACT1].SignifierPhonebook = WhichPhonebook
            //SignifierData[SIG_CONTACT1].SignifierCharacter = CharacterToAdd
            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_CONTACT_SIGNIFIER)
        ENDIF
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character should not be added as a phone contact. See Steve or Keith")
        #endif
    ENDIF
#endif
ENDPROC

//Will maybe need to implement ADD_ADDITIONAL_CONTACT_TO_PHONEBOOK() post December deadline so as to facilitate adding two contacts at once.

 //Wiki-ed.
//Removes a contact from all playable characters phonebooks.

PROC REMOVE_CONTACT_FROM_ALL_PHONEBOOKS (enumCharacterList CharacterToRemove)

    //Check if the character passed is allowed to be added to the phonebook via reference to the character sheet.   
    IF g_CharacterSheetNonSaved[CharacterToRemove].phone <> NO_PHONE_CONTACT  //may do fancier check for last characters of string so names can be matched explicitly. Safer that way.
        
        #if USE_CLF_DLC
            //Revert contact to not listed in all three characters books.
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[0] = NOT_LISTED //Michael
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[1] = NOT_LISTED //Franklin
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[2] = NOT_LISTED //Trevor
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[0] = UNKNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[1] = UNKNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[2] = UNKNOWN_CALLER
            #if IS_DEBUG_BUILD
                PRINTLN("\nCELLPHONE_PUB - Remove Contact from all phonebooks function called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from all phonebooks: ")
                cdPrintstring(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].label)
                cdPrintnl()
            #endif      
        #endif
        
        #if USE_NRM_DLC
            //Revert contact to not listed in all three characters books.
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[0] = NOT_LISTED //Michael
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[1] = NOT_LISTED //Franklin
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[2] = NOT_LISTED //Trevor
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[0] = UNKNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[1] = UNKNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[2] = UNKNOWN_CALLER
            #if IS_DEBUG_BUILD
                PRINTLN("\nCELLPHONE_PUB - Remove Contact from all phonebooks function called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from all phonebooks: ")
                cdPrintstring(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].label)
                cdPrintnl()
            #endif      
        #endif
        
        #if not USE_SP_DLC
            //Revert contact to not listed in all three characters books.
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToRemove, 0, NOT_LISTED)
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToRemove, 1, NOT_LISTED)
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToRemove, 2, NOT_LISTED)
		   	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToRemove, 0, UNKNOWN_CALLER)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToRemove, 1, UNKNOWN_CALLER)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToRemove, 2, UNKNOWN_CALLER)
			
            #if IS_DEBUG_BUILD
                PRINTLN("\nCELLPHONE_PUB - Remove Contact from all phonebooks function called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from all phonebooks: ")
                cdPrintstring(g_sCharacterSheetAll[CharacterToRemove].label)
                cdPrintnl()
            #endif      
        #endif


    ELSE

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

    ENDIF

ENDPROC

PROC REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK (enumCharacterList CharacterToRemove, enumPhoneBookPresence WhichPhoneBook)

    IF g_CharacterSheetNonSaved[CharacterToRemove].phone <> NO_PHONE_CONTACT

        IF ENUM_TO_INT (WhichPhoneBook) > 3 //i.e not Michael, Franklin or Trevor or MP has been passed
            #if IS_DEBUG_BUILD
                SCRIPT_ASSERT ("You must pass in Michael, Franklin or Trevor book when removing contact from individual phonebook.")
            #endif    
        ELSE

        #if USE_CLF_DLC
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = NOT_LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = UNKNOWN_CALLER
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from individual phonebook: ")
                cdPrintstring(g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToRemove].label)
                cdPrintnl()
            #endif
        #endif
        #if USE_NRM_DLC
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].PhonebookState[ENUM_TO_INT (WhichPhonebook)] = NOT_LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].StatusAsCaller[ENUM_TO_INT (WhichPhonebook)] = UNKNOWN_CALLER
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from individual phonebook: ")
                cdPrintstring(g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToRemove].label)
                cdPrintnl()
            #endif
        #endif  

        #if not USE_SP_DLC
			INT iPhoneBookID = ENUM_TO_INT (WhichPhonebook)
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(CharacterToRemove, iPhoneBookID, NOT_LISTED)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CharacterToRemove, iPhoneBookID, UNKNOWN_CALLER)
			
            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Removed contact from individual phonebook: ")
				TEXT_LABEL tlLabel = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToRemove)
				STRING strLabel = Get_String_From_TextLabel(tlLabel)
                cdPrintstring(strLabel)
                cdPrintnl()
            #endif
        #endif

        ENDIF

    ELSE

        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve or Keith")
        #endif

    ENDIF

ENDPROC

//Wiki-ed.
//This will force the contact passed in to have his name appear first in the phonebook contact list, regardless of alphabetical order.
//It is possible to make several contacts "priority" names. They will appear in the order that they have been called.

PROC MAKE_CONTACT_ENTRY_PRIORITY (enumCharacterList CharToForce)

    IF g_CharacterSheetNonSaved[CharToForce].phone <> NO_PHONE_CONTACT
        #if USE_CLF_DLC
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharToForce].alphaInt = 1 //Set this character to be first in the contacts list by setting their alphabetical sort int to 1, the lowest value.
        #endif
        #if USE_NRM_DLC
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharToForce].alphaInt = 1 //Set this character to be first in the contacts list by setting their alphabetical sort int to 1, the lowest value.
        #endif
        #if not USE_SP_DLC
           GLOBAL_CHARACTER_SHEET_SET_ORIGINAL_ALPHA_INT(CharToForce, 1) //Set this character to be first in the contacts list by setting their alphabetical sort int to 1, the lowest value.
        #endif
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve or Keith")
        #endif
    ENDIF

ENDPROC

//Wiki-ed.
//This will make sure that the contact name passed in will appear in natural alphabetical order within the phonebook contact list. Call this to return any
//names that were made a priority for the duration of a particular mission back to normal.

PROC MAKE_CONTACT_ENTRY_ALPHA (enumCharacterList CharToForce)
    
    IF g_CharacterSheetNonSaved[CharToForce].phone <> NO_PHONE_CONTACT 
        #if USE_CLF_DLC
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharToForce].alphaInt = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharToForce].original_alphaInt //Set this character's alpha int back to its' original value.
        #endif
        #if USE_NRM_DLC
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharToForce].alphaInt = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharToForce].original_alphaInt //Set this character's alpha int back to its' original value.
        #endif
        #if not USE_SP_DLC
			GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(CharToForce, GLOBAL_CHARACTER_SHEET_GET_ORIGINAL_ALPHA_INT(CharToForce))
        #endif
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve or Keith")
        #endif
    ENDIF

ENDPROC

//Wiki-ed
//This procedure will remove all contacts from all three player characters' phonebooks. Should only be used by the flow!

PROC REMOVE_ALL_CONTACTS_FROM_ALL_PLAYER_CHARACTERS_PHONEBOOKS()

    INT cs_index    
        #if USE_CLF_DLC 
            REPEAT MAX_CLF_CHARACTERS cs_index
                IF NOT (g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = SPECIAL_NUMBERS)
                //Make sure special numbers like 911 are not removed. Only need to check first character. Special numbers are uniform across all player characters.
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = NOT_LISTED
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[1] = NOT_LISTED
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[2] = NOT_LISTED
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[0] = UNKNOWN_CALLER
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[1] = UNKNOWN_CALLER
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[2] = UNKNOWN_CALLER
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[0] = NO_MISSED_CALL
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[1] = NO_MISSED_CALL
                    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[2] = NO_MISSED_CALL
                ENDIF
            ENDREPEAT   
        #endif
        #if USE_NRM_DLC
            REPEAT MAX_NRM_CHARACTERS cs_index
                IF NOT (g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = SPECIAL_NUMBERS)
                //Make sure special numbers like 911 are not removed. Only need to check first character. Special numbers are uniform across all player characters.
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = NOT_LISTED
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[1] = NOT_LISTED
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[2] = NOT_LISTED
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[0] = UNKNOWN_CALLER
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[1] = UNKNOWN_CALLER
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[2] = UNKNOWN_CALLER
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[0] = NO_MISSED_CALL
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[1] = NO_MISSED_CALL
                    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].MissedCallStatus[2] = NO_MISSED_CALL
                ENDIF
            ENDREPEAT
        #endif

        #if not USE_SP_DLC 
			INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
            REPEAT iMaxCharacter cs_index
				enumCharacterList eCharacter = INT_TO_ENUM(enumCharacterList, cs_index)
                IF NOT (GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(eCharacter, 0) = SPECIAL_NUMBERS) 
                //Make sure special numbers like 911 are not removed. Only need to check first character. Special numbers are uniform across all player characters.
					GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(eCharacter, 0, NOT_LISTED)
					GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(eCharacter, 1, NOT_LISTED)
					GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(eCharacter, 2, NOT_LISTED)
					GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(eCharacter, 0, UNKNOWN_CALLER)
					GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(eCharacter, 1, UNKNOWN_CALLER)
					GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(eCharacter, 2, UNKNOWN_CALLER)
					GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(eCharacter, 0, NO_MISSED_CALL)		
					GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(eCharacter, 1, NO_MISSED_CALL)		
					GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(eCharacter, 2, NO_MISSED_CALL)					
                ENDIF
            ENDREPEAT   
        #endif

    #IF IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_PUB - Removing all contacts from all singleplayer phonebooks via public function!")
        cdPrintnl()

    #endif

ENDPROC

PROC REMOVE_ALL_CONTACTS_FROM_MULTIPLAYER_PHONEBOOKS()

    INT cs_index
    REPEAT MAX_CHARACTERS_MP cs_index
        IF NOT (GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, cs_index), 3) = SPECIAL_NUMBERS) 
        //Make sure special numbers like 911 are not removed. Only need to check first character.
			GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(INT_TO_ENUM(enumCharacterList, cs_index), 3, NOT_LISTED)
			GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(INT_TO_ENUM(enumCharacterList, cs_index), 3, UNKNOWN_CALLER)
			GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(INT_TO_ENUM(enumCharacterList, cs_index), 3, NO_MISSED_CALL)
        ENDIF        
    ENDREPEAT

    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_PUB - Removing all contacts from multiplayer phonebooks via public function!")
        cdPrintnl()

    #endif

ENDPROC

//Wiki-ed
//This procedure adds all characters, whether they are a phone contact or not, to all three player characters' phonebooks.
//This should be used in a debug capacity only.

//Remember to comment in ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKS() WAIT (0) in appContacts.

#if IS_DEBUG_BUILD
#if USE_CLF_DLC
PROC ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKSCLF()

    INT cs_index
    REPEAT MAX_CLF_CHARACTERS cs_index

        IF cs_index <> ENUM_TO_INT(CHAR_CLF_CHAT_CALL)
        OR cs_index <> ENUM_TO_INT(CHAR_CLF_AMMUNATION)
            //Add contact as listed in all three characters books.
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[1] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[2] = LISTED
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[0] = KNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[1] = KNOWN_CALLER
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[2] = KNOWN_CALLER

        ENDIF                   
    ENDREPEAT

    #if IS_DEBUG_BUILD
        cdPrintstring ("CELLPHONE_PUB - Adding all characters to all singleplayer phonebooks via public function!")
        cdPrintnl()
    #endif

ENDPROC
#endif
#if USE_NRM_DLC
PROC ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKSNRM()

    INT cs_index
    REPEAT MAX_NRM_CHARACTERS cs_index
        IF cs_index <> ENUM_TO_INT(CHAR_NRM_CHAT_CALL)
        OR cs_index <> ENUM_TO_INT(CHAR_NRM_AMMUNATION)
            //Add contact as listed in all three characters books.
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[0] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[1] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].PhonebookState[2] = LISTED
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[0] = KNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[1] = KNOWN_CALLER
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[cs_index].StatusAsCaller[2] = KNOWN_CALLER
        ENDIF                   
    ENDREPEAT

    #if IS_DEBUG_BUILD
        cdPrintstring ("CELLPHONE_PUB - Adding all characters to all singleplayer phonebooks via public function!")
        cdPrintnl()
    #endif

ENDPROC
#endif

PROC ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKS()
    #if USE_CLF_DLC
        ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKSCLF()
        exit
    #endif
    #if USE_NRM_DLC
        ADD_ALL_CHARS_TO_ALL_PLAYER_CHARACTERS_PHONEBOOKSNRM()
        exit
    #endif
    
    #if not USE_SP_DLC
        INT cs_index
		INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE())
        REPEAT iMaxCharacter cs_index
            IF cs_index <> ENUM_TO_INT(CHAR_CHAT_CALL)
            OR cs_index <> ENUM_TO_INT(CHAR_AMMUNATION)
                //Add contact as listed in all three characters books.
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(cs_index, 0, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(cs_index, 1, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(cs_index, 2, LISTED)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(cs_index, 0, KNOWN_CALLER)
				GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(cs_index, 1, KNOWN_CALLER)
                GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(cs_index, 2, KNOWN_CALLER)
            ENDIF                   
        ENDREPEAT

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Adding all characters to all singleplayer phonebooks via public function!")
            cdPrintnl()
        #endif
    #endif
ENDPROC

#endif //debug

#if USE_CLF_DLC
FUNC BOOL IS_CONTACT_IN_PHONEBOOK_CLF (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook)
    IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[ENUM_TO_INT(CharacterToAdd)].PhonebookState[ENUM_TO_INT(WhichPhonebook)] = LISTED
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns TRUE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns FALSE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN FALSE
    ENDIF
endfunc
#endif

#if USE_NRM_DLC
FUNC BOOL IS_CONTACT_IN_PHONEBOOK_NRM (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook)
    IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[ENUM_TO_INT(CharacterToAdd)].PhonebookState[ENUM_TO_INT(WhichPhonebook)] = LISTED
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns TRUE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns FALSE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN FALSE
    ENDIF
endfunc
#endif

FUNC BOOL IS_CONTACT_IN_PHONEBOOK (enumCharacterList CharacterToAdd, enumPhonebookPresence WhichPhonebook)
#if USE_CLF_DLC
    return IS_CONTACT_IN_PHONEBOOK_CLF(CharacterToAdd,WhichPhonebook)
#endif
#if USE_NRM_DLC
    return IS_CONTACT_IN_PHONEBOOK_NRM(CharacterToAdd,WhichPhonebook)
#endif

#if not USE_SP_DLC
    IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CharacterToAdd, ENUM_TO_INT(WhichPhonebook)) = LISTED 
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns TRUE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            IF g_DumpDisableEveryFrameCaller
                PRINTNL()
                PRINTLN("\nCELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
                cdPrintstring ("CELLPHONE_PUB - IS_CONTACT_IN_PHONEBOOK returns FALSE.")
                cdPrintnl()
            ENDIF
        #endif
        RETURN FALSE
    ENDIF
#endif
ENDFUNC

//Wiki-ed.
//Checks if the current player character is attempting to make a call to the character passed in and should be used for triggering phone calls from scripts.

//This will return true in the following circumstances:
// - waiting for the contact to answer.
// - whilst the contact's phone returns an engaged tone.

//Will return false if:
// - the player isn't attempting to call the contact
// - the contact is already in a phone conversation or relaying an answerphone messagee to the player, i.e the call is already connected.

//See also IS_CONTACT_IN_CONNECTED_CALL 

FUNC BOOL IS_CALLING_CONTACT (enumCharacterList CharacterToCheck)

    IF g_b_Is_This_An_Activity_Call
    OR g_b_Is_This_An_Activity_Cancellation_Call
    OR g_b_Is_This_A_Secondary_Function_Call

        IF CharacterToCheck = CHAR_CALL911  //Adding exception for tricky bug 2117600. This seems the safest fix.

            //This will spam if included without Dump widget clause...
            #if IS_DEBUG_BUILD

                IF g_DumpDisableEveryFrameCaller

                    cdPrintnl()
                    cdPrintstring("IS_CALLING_CONTACT reporting that a 911 exemption has taken place even though Activity or Secondary Function call was set to true. 2117600")
                    cdPrintnl()

                ENDIF

            #endif

        ELSE

            RETURN FALSE  //If either of the flags are set, story mode should completely ignore the calls. They should be picked up by the friend controller or secondary function check.

        ENDIF

    ENDIF

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        //OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL 
        //Removed as this can feasibly spam the phone. Update: 07.09.10... That would mean the script which is triggering the phonecall 
        //is at fault. Might be reinstated to see who's doing it but IS_CONTACT_IN_CONNECTED_CALL should now be used to check specific ongoing call status.
            IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            //Note! g_TheContactInvolvedinCall is used. That means the player has pro-actively selected the contact from the contacts list via appContacts.sc

                RETURN TRUE

            ELSE

                RETURN FALSE

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

//New. This would need to check for special bool or bit, so that IS_CALLING_CONTACT doesn't return true. So work needs done on the above function also.

FUNC BOOL IS_CALLING_FRIEND_FOR_ACTIVITY (enumCharacterList CharacterToCheck)

    IF g_b_Is_This_An_Activity_Call = FALSE  //If this isn't flagged as an activity call, it should be ignored.

        RETURN FALSE

    ENDIF

    IF g_b_Is_This_An_Activity_Cancellation_Call = TRUE//If the call is flagged as requesting a cancellation then it should be ignored also.

        RETURN FALSE

    ENDIF

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        //OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL 
        //Removed as this can feasibly spam the phone. Update: 07.09.10... That would mean the script which is triggering the phonecall 
        //is at fault. Might be reinstated to see who's doing it but IS_CONTACT_IN_CONNECTED_CALL should now be used to check specific ongoing call status.
            IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            //Note! g_TheContactInvolvedinCall is used. That means the player has pro-actively selected the contact from the contacts list via appContacts.sc

                RETURN TRUE

            ELSE

                RETURN FALSE

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL IS_CALLING_FRIEND_FOR_ACTIVITY_CANCELLATION (enumCharacterList CharacterToCheck)

    IF g_b_Is_This_An_Activity_Call = TRUE

        RETURN FALSE   //Ignore if this is a "hang out" call.

    ENDIF

    IF g_b_Is_This_An_Activity_Cancellation_Call = FALSE //If it's not a hang out call, then ignore if it's not an activity cancellation request call also.
                                                         //It must be a story call.

        RETURN FALSE

    ENDIF

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        //OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL 
        //Removed as this can feasibly spam the phone. Update: 07.09.10... That would mean the script which is triggering the phonecall 
        //is at fault. Might be reinstated to see who's doing it but IS_CONTACT_IN_CONNECTED_CALL should now be used to check specific ongoing call status.
            IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            //Note! g_TheContactInvolvedinCall is used. That means the player has pro-actively selected the contact from the contacts list via appContacts.sc

                RETURN TRUE

            ELSE

                RETURN FALSE

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL IS_CALLING_CONTACT_FOR_SECONDARY_FUNCTION (enumCharacterList CharacterToCheck)

    IF g_b_Is_This_A_Secondary_Function_Call = FALSE

        RETURN FALSE   //Ignore if this is not a secondary function call.

    ENDIF

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
        //OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL 
        //Removed as this can feasibly spam the phone. Update: 07.09.10... That would mean the script which is triggering the phonecall 
        //is at fault. Might be reinstated to see who's doing it but IS_CONTACT_IN_CONNECTED_CALL should now be used to check specific ongoing call status.
            IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            //Note! g_TheContactInvolvedinCall is used. That means the player has pro-actively selected the contact from the contacts list via appContacts.sc

                RETURN TRUE

            ELSE

                RETURN FALSE

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL ADD_SECONDARY_OPTION_FOR_MP_CHARACTER (INT SpecialMPCharacter_ArrayPos, enumCharacterList CharacterToAdd, STRING WhichSecondaryOptionLabel_1)

     IF NOT (ARE_STRINGS_EQUAL (g_SpecialMPCharacters[SpecialMPCharacter_ArrayPos].SecondaryFunctionLabel_1, "CELL_SFUN_NULL"))

        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB -  Cannot add a secondary character option as this Special MP Character array is already occupied at passed position ")
            cdPrintint(SpecialMPCharacter_ArrayPos)
            cdPrintnl()
            cdPrintstring("This is already occupied by ")
            cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_SpecialMPCharacters[SpecialMPCharacter_ArrayPos].Name_TextLabel))
            cdPrintnl()
        #endif

        RETURN FALSE

     ELSE

        g_SpecialMPCharacters[SpecialMPCharacter_ArrayPos].Name_TextLabel = GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToAdd)

        g_SpecialMPCharacters[SpecialMPCharacter_ArrayPos].SecondaryFunctionLabel_1 = WhichSecondaryOptionLabel_1

        #if IS_DEBUG_BUILD
            cdPrintstring("CELLPHONE_PUB -  Added a secondary character option into Special MP Character array at passed position ")
            cdPrintint(SpecialMPCharacter_ArrayPos)
            cdPrintnl()
            cdPrintstring("This is now occupied by ")
            cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(g_SpecialMPCharacters[SpecialMPCharacter_ArrayPos].Name_TextLabel))
            cdPrintnl()

        #endif

        RETURN TRUE

    ENDIF

ENDFUNC

FUNC BOOL REMOVE_SECONDARY_OPTION_FOR_MP_CHARACTER (enumCharacterList CharacterToRemove)

    INT tempIndex = 0
    BOOL b_FoundSecondaryOption = FALSE

    WHILE tempIndex < MAX_SPECIAL_MP_CHARACTERS

        IF ARE_STRINGS_EQUAL (GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToRemove), g_SpecialMPCharacters[tempIndex].Name_TextLabel)

            IF NOT (ARE_STRINGS_EQUAL (g_SpecialMPCharacters[tempIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")) //A secondary function must have been defined if the contents are no longer the predefined null.

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_PUB - Removing secondary option found at special MP character index ")
                    cdPrintint(tempIndex)
                    cdPrintstring(" who is ")
                    cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToRemove)))
                    cdPrintnl()
                #endif

                g_SpecialMPCharacters[tempIndex].SecondaryFunctionLabel_1 = "CELL_SFUN_NULL" 

                b_FoundSecondaryOption = TRUE

            ENDIF

        ENDIF

        tempIndex ++

    ENDWHILE

    IF b_FoundSecondaryOption 

        RETURN TRUE

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_PUB - Attempted to remove secondary option for ")
            cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToRemove)))
            cdPrintstring(" but none defined.")
            cdPrintnl()
        #endif

        RETURN FALSE

    ENDIF 

ENDFUNC

FUNC BOOL IS_SECONDARY_OPTION_AVAILABLE_FOR_THIS_MP_CHARACTER (enumCharacterList CharacterToCheck)

    INT tempIndex = 0
    BOOL b_FoundSecondaryOption = FALSE

    WHILE tempIndex < MAX_SPECIAL_MP_CHARACTERS

        IF ARE_STRINGS_EQUAL (GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToCheck), g_SpecialMPCharacters[tempIndex].Name_TextLabel)

            IF NOT (ARE_STRINGS_EQUAL (g_SpecialMPCharacters[tempIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")) //A secondary function must have been defined if the contents are no longer the predefined null.

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_PUB - Secondary option found for special MP character at index ")
                    cdPrintint(tempIndex)
                    cdPrintstring(" who is ")
                    cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToCheck)))
                    cdPrintnl()
                #endif

                b_FoundSecondaryOption = TRUE

            ENDIF

        ENDIF

        tempIndex ++

    ENDWHILE

    IF b_FoundSecondaryOption 

        RETURN TRUE

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_PUB - No secondary option found for ")
            cdPrintstring(GET_FILENAME_FOR_AUDIO_CONVERSATION(GLOBAL_CHARACTER_SHEET_GET_LABEL(CharacterToCheck)))
            cdPrintnl()
        #endif

        RETURN FALSE

    ENDIF 

ENDFUNC

//If you need to know which postion in the special MP character array is free for adding a new character into, use this.

FUNC INT GET_FREE_POSITION_FOR_MP_CHARACTER_SECONDARY_OPTION()

    INT tempIndex = 0
    INT FreePositionInt = -99
    BOOL b_FoundFreePosition = FALSE

    WHILE tempIndex < MAX_SPECIAL_MP_CHARACTERS

            IF (ARE_STRINGS_EQUAL (g_SpecialMPCharacters[tempIndex].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")) //A secondary function must have been defined if the contents are no longer the predefined null.

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("CELLPHONE_PUB - Free position found for special MP character at index ")
                    cdPrintint(tempIndex)
                    cdPrintnl()
                #endif

                FreePositionInt =  tempIndex

                b_FoundFreePosition = TRUE

            ENDIF

        tempIndex ++

    ENDWHILE

    IF b_FoundFreePosition

        RETURN FreePositionInt

    ELSE

        RETURN -99

    ENDIF

ENDFUNC

PROC SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE (STRING TextLabelForSecondaryOption)

    g_SpecialSPCharacters[0].SecondaryFunctionLabel_1 = TextLabelForSecondaryOption

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Michael - secondary contact option available.")
        cdPrintnl()
    #endif

ENDPROC

PROC SET_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE (STRING TextLabelForSecondaryOption)

    g_SpecialSPCharacters[1].SecondaryFunctionLabel_1 = TextLabelForSecondaryOption

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Franklin - secondary contact option available.")
        cdPrintnl()
    #endif

ENDPROC

PROC SET_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE (STRING TextLabelForSecondaryOption)

    g_SpecialSPCharacters[2].SecondaryFunctionLabel_1 = TextLabelForSecondaryOption

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Trevor - secondary contact option available.")
        cdPrintnl()
    #endif

ENDPROC

PROC SET_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE (STRING TextLabelForSecondaryOption)

    g_SpecialSPCharacters[3].SecondaryFunctionLabel_1 = TextLabelForSecondaryOption

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Lester - secondary contact option available.")
        cdPrintnl()
    #endif

ENDPROC

FUNC BOOL IS_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE()

    IF ARE_STRINGS_EQUAL(g_SpecialSPCharacters[0].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")
        RETURN FALSE
    ENDIF

    RETURN TRUE

ENDFUNC

FUNC BOOL IS_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE()

    IF ARE_STRINGS_EQUAL(g_SpecialSPCharacters[1].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")
        RETURN FALSE
    ENDIF

    RETURN TRUE

ENDFUNC

FUNC BOOL IS_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE()

    IF ARE_STRINGS_EQUAL(g_SpecialSPCharacters[2].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")
        RETURN FALSE
    ENDIF

    RETURN TRUE

ENDFUNC

FUNC BOOL IS_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE()

    IF ARE_STRINGS_EQUAL(g_SpecialSPCharacters[3].SecondaryFunctionLabel_1, "CELL_SFUN_NULL")
        RETURN FALSE
    ENDIF

    RETURN TRUE

ENDFUNC

PROC REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()

    g_SpecialSPCharacters[0].SecondaryFunctionLabel_1 = "CELL_SFUN_NULL"

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Michael - secondary contact option removed.")
        cdPrintnl()
    #endif

ENDPROC

PROC REMOVE_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION()

    g_SpecialSPCharacters[1].SecondaryFunctionLabel_1 = "CELL_SFUN_NULL"

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Franklin - secondary contact option removed.")
        cdPrintnl()
    #endif

ENDPROC

PROC REMOVE_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION()

    g_SpecialSPCharacters[2].SecondaryFunctionLabel_1 = "CELL_SFUN_NULL"

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Trevor - secondary contact option removed.")
        cdPrintnl()
    #endif

ENDPROC

PROC REMOVE_LESTER_SECONDARY_CONTACT_LIST_FUNCTION()

    g_SpecialSPCharacters[3].SecondaryFunctionLabel_1 = "CELL_SFUN_NULL"

    #if IS_DEBUG_BUILD
        cdPrintstring("CELLPHONE_PUB. Lester - secondary contact option removed.")
        cdPrintnl()
    #endif

ENDPROC

//Wiki-ed.
//This function will return true if the player's attempt to call to a contact has either:
// - got through and the contact is responding in real time.
// - got through but the contact is responding via answerphone.
//N.B it will also return true if the contact is calling the player and the phone is waiting to be answered.

//It will return false:
// - whilst the contact's phone is ringing and a full connection has not been made.
// - during an engaged tone.

FUNC BOOL IS_CONTACT_IN_CONNECTED_CALL (enumCharacterList CharacterToCheck)

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF g_Cellphone.PhoneDS = PDS_ONGOING_CALL

            IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            //Note! g_TheContactInvolvedinCall is used. This is stipulated when a phone call is incoming or an outgoing phonecall is fully connected.

                RETURN TRUE

            ELSE

                RETURN FALSE

            ENDIF

        ELSE

        RETURN FALSE

        ENDIF

    ELSE

        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

//As soon as the user selects a contact from the contacts list this will return true.
//This will return true before an ongoing call or attempting to call state and during a call / hangout menu interim.
//The function will only return false again when another contact has been selected, or a call from a different contact comes in, so hanging 
//up the phone will not necessarily clear the last contact selection, ( could be done if desired, though! )

FUNC BOOL HAS_CONTACT_BEEN_SELECTED_IN_CONTACTS_LIST (enumCharacterList CharacterToCheck)

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        IF ENUM_TO_INT(g_TheContactInvolvedinCall) = ENUM_TO_INT(CharacterToCheck)

            RETURN TRUE

        ELSE

            RETURN FALSE

        ENDIF

    ELSE

        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

PROC SET_CALL_PREVENTED_FOR_CHAR_DETONATEPHONE (BOOL b_PreventCall)

    IF b_PreventCall

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE)

        #if IS_DEBUG_BUILD 

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Call prevented for CHAR_DETONATEPHONE[", GET_THIS_SCRIPT_NAME(), "] through public function.")        
            PRINTSTRING("Will be re-enabled automatically on phone exit.")
            PRINTNL()

        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_PREVENT_CALL_FOR_CHAR_DETONATEPHONE)

        #if IS_DEBUG_BUILD 

            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Call enabled for CHAR_DETONATEPHONE[", GET_THIS_SCRIPT_NAME(), "] through public function.") 

        #endif

    ENDIF

ENDPROC

//Wiki-ed.
//Similar to the above "IS_CALLING_CONTACT" function, this will return true if the player is attempting to call ANY contact from within his phonebook.
//As this function will not be used to trigger phone calls, it can be used to check if the player is having a connected phone conversation with anyone.

//Handy for flow work if you want to check whether or not the phone, in a conversation sense, 
//is free before attempting to trigger a call. You could use this to work out how long the phone has been free etc.

FUNC BOOL IS_CALLING_ANY_CONTACT ()

    IF g_Cellphone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT
    OR g_Cellphone.PhoneDS = PDS_ONGOING_CALL //It's safe to check this here, as IS_CALLING_ANY_CONTACT will not be directly triggering a phonecall, so poor scripting
                                              //cannot spam the system.

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//This should only be used by the player list application. It is used to simulate an outgoing call to a human friend but does not go through the dialogue system.
//This will launch appContacts and keep the call screen and appContacts running up until the following happens:
//The phone aborts due to abnormal exit.
//The user who initiated the call selects hang up - this will trigger the state assignment 54 clause check for player list app active in Cellphone_Flashhand.
//A TERMINATE_APPCONTACTS_FOR_CHAT_CALL() is explicitly made by the app script.. This won't remove the screen itself, but does force appContacts to terminate, assign complex app and stop the dialing tone.

PROC LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL(STRING WhichChatCallerString)

    g_TheContactInvolvedinCall = CHAR_CHAT_CALL //Critical! This character is looked for specifically in appContacts to make sure the correct dialling tones
                                                //are played and no engaged signal is cued.

    g_ChatCallerString = WhichChatCallerString

    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT    //Setting this here will ensure the scaleform methods below take precedence above the creation of the contacts list itself.
                                                                //when appContacts is launched. See that script for more info.
        Put_Cellphone_To_Ear()

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 4527. CELLPHONE_PUBLIC assigns PDS_ATTEMPTING_TO_CALL_CONTACT during chat_call launch.")
            cdPrintnl()   
        #endif

    ENDIF                                                                                      

    #if USE_CLF_DLC
        //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
        request_load_scriptstring ("appContactsCLF")

        Application_Thread = START_NEW_SCRIPT ("appContactsCLF", CONTACTS_APP_STACK_SIZE)

        SET_SCRIPT_AS_NO_LONGER_NEEDED ("appContactsCLF")
    #endif
    #if USE_NRM_DLC
        //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
        request_load_scriptstring ("appContactsNRM")

        Application_Thread = START_NEW_SCRIPT ("appContactsNRM", CONTACTS_APP_STACK_SIZE)

        SET_SCRIPT_AS_NO_LONGER_NEEDED ("appContactsNRM")
    #endif

    #if not USE_CLF_DLC
    #if not USE_NRM_DLC
        //appContacts handles all outgoing pro-active non-script calls by the player. We need to launch this.
        request_load_scriptstring ("appContacts")

        Application_Thread = START_NEW_SCRIPT ("appContacts", CONTACTS_APP_STACK_SIZE)

        SET_SCRIPT_AS_NO_LONGER_NEEDED ("appContacts")
    #endif
    #endif

    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

            END_TEXT_COMMAND_SCALEFORM_STRING()

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_211") //Label to specify we are making an outgoing call, e.e CALLING, DIALLING

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_211") //This field is no longer displayed on the phone, so safe 
                                                            //to duplicate required parameter with previous text label.

    END_SCALEFORM_MOVIE_METHOD ()

    Update_Scaleform_VSE ()

    #if IS_DEBUG_BUILD

        cdPrintstring ("appTextMessage - Attempting to call human player for chat call: ")
        cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
        cdPrintstring (" which is charsheet entry ")
        cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
        cdPrintnl()

    #endif

ENDPROC

PROC LAUNCH_CALL_SCREEN_FOR_OUTGOING_CHAT_CALL_FROM_APPCONTACTS (STRING WhichChatCallerString)

    g_TheContactInvolvedinCall = CHAR_CHAT_CALL //Critical! This character is looked for specifically in appContacts to make sure the correct dialling tones
                                                //are played and no engaged signal is cued.

    g_ChatCallerString = WhichChatCallerString

    IF g_CellPhone.PhoneDS > PDS_AWAY //make sure that the following state assignment cannot interfere with AWAY or DISABLED assignment.

        g_CellPhone.PhoneDS = PDS_ATTEMPTING_TO_CALL_CONTACT    //Setting this here will ensure the scaleform methods below take precedence above the creation of the contacts list itself.
                                                                //when appContacts is launched. See that script for more info.
        Put_Cellphone_To_Ear()

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("STATE ASSIGNMENT 45278. CELLPHONE_PUBLIC assigns PDS_ATTEMPTING_TO_CALL_CONTACT during chat_call launch.")
            cdPrintnl()   
        #endif

    ENDIF                                                                                      

    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

            BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

                ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

            END_TEXT_COMMAND_SCALEFORM_STRING()

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_211") //Label to specify we are making an outgoing call, e.e CALLING, DIALLING

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_211") //This field is no longer displayed on the phone, so safe 
                                                            //to duplicate required parameter with previous text label.

    END_SCALEFORM_MOVIE_METHOD ()

    //Adding this on a trial basis to prevent quitting before PROCESS_VOICE_SESSION_STARTED_EVENT() has fired. 1602347
    SET_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Do not display hang up button until Voice session event is TRUE - this is done in ply_to_ply calls                                                                   
                                                                                     //when voice session is started via PROCESS_VOICE_SESSION_STARTED_EVENT
    Update_Scaleform_VSE () //This was always here...

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_INCOMING_OR_OUTGOING_CALL_HANGUP) //Re-enable immediately for safety.

    #if IS_DEBUG_BUILD

        cdPrintstring ("Cellphone Public - Attempting to call human player from within appContacts for chat call: ")
        cdPrintstring (g_sCharacterSheetAll[g_TheContactInvolvedinCall].label)
        cdPrintstring (" which is charsheet entry ")
        cdPrintint (ENUM_TO_INT(g_TheContactInvolvedInCall))
        cdPrintnl()

    #endif

ENDPROC

PROC TERMINATE_APPCONTACTS_FOR_CHAT_CALL()

    #if IS_DEBUG_BUILD

        PRINTLN("\nCELLPHONE_PUB - Terminate appcontacts for chat called by[", GET_THIS_SCRIPT_NAME(), "] through public function.")

        cdPrintnl()

        /* Removed - appMPPlayerList is currently redundant.
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appMPPlayerList")) = 0
            SCRIPT_ASSERT ("AppMPPlayerList is not running but chat call termination proc called! See Steve T.")
            PRINTLN("\nCELLPHONE_PUB - Warning! AppMPPlayerList is not running. This may cause a phone lock up if used incorrectly!")
            cdPrintnl()
        ENDIF
        */

    #endif

    //Deliberately outwith the thread clause immediately above for testing.
    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_TERMINATE_APPCONTACTS_FOR_CHAT_CALL)

ENDPROC

PROC SET_OUTGOING_CHAT_CALL_AS_ENGAGED (BOOL b_SetAsEngaged)

    IF b_SetAsEngaged

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)

        #if IS_DEBUG_BUILD 

            cdPrintstring ("CELLPHONE_PUB - Setting outgoing chat call bit as engaged.")
            cdPrintnl()

        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SET_CHAT_CALL_AS_ENGAGED)

        #if IS_DEBUG_BUILD 

            cdPrintstring ("CELLPHONE_PUB - Clearing outgoing call engaged bit")
            cdPrintnl()

        #endif

    ENDIF

ENDPROC

PROC SET_PLAYER_CHAT_CALL_AS_CONNECTED()

    BEGIN_SCALEFORM_MOVIE_METHOD (SF_MovieIndex, "SET_DATA_SLOT")

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)

        BEGIN_TEXT_COMMAND_SCALEFORM_STRING ("CELL_CONDFON") //~a~

            ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME (g_ChatCallerString)

        END_TEXT_COMMAND_SCALEFORM_STRING()

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_300") //Point to CHAR_DEFAULT headshot for just now...
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //"CONNECTED"

        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CELL_219") //This field is invisible, so feel free to duplicate with previous text label.

    END_SCALEFORM_MOVIE_METHOD ()

    PRINTLN("SET_PLAYER_CHAT_CALL_AS_CONNECTED- Called from ", GET_THIS_SCRIPT_NAME())

    voiceSession.bConnectedCallOnGoing = TRUE
    CLEAR_BIT(BitSet_CellphoneDisplay, g_BS_PROMPT_FLASHHAND_TO_PLAY_REMOTE_RING)   
    Update_Scaleform_VSE ()

    /* Moved to TitleUpdate_Post_Set_Player_Chat_Call_As_Connected() in cellphone_private
    IF g_Cellphone.PhoneDS > PDS_AWAY

        g_Cellphone.PhoneDS = PDS_ONGOING_CALL  //#1602347 - Makes sure that the hands free option can be used from the outgoing call end.

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintString("STATE ASSIGNMENT 36. Cellphone_Controller assigns PDS_ONGOING_CALL")
            cdPrintnl()

        #endif

        g_IsThisAnMPChatCall = TRUE //Required so that the hands free handler can work for the call initiator

        IF g_bInMultiplayer //Add hands free button if in MP.

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING (SF_MovieIndex, "SET_SOFT_KEYS", BADGER_OTHER, 1,
                                5, INVALID_SCALEFORM_PARAM, INVALID_SCALEFORM_PARAM, "CELL_201") //Hands Free / Hide Phone button - NEEDS NEW ICON! #1566736

            #if IS_DEBUG_BUILD

                cdPrintnl()
                cdPrintString("CELLPHONE_PUBLIC - Adding Hands Free option SOFT KEY - 2")
                cdPrintnl()

            #endif

            IF NOT IS_BIT_SET(BitSet_CellphoneDisplay_Third, g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

                PRINT_HELP("CELL_HF_HIDE")

                SET_BIT (BitSet_CellphoneDisplay_Third,  g_BSTHIRD_HUMAN_HIDE_FOR_HANDS_FREE_HELP_DONE)

            ENDIF

        ENDIF

    ENDIF
    */

ENDPROC

//Text Messaging

//Wiki-ed.
//Send a text message to the player's phone.
//
//Text messages for the three singleplayer characters phones are stored in one large common array. A filter system is in operation, that specifies if a particular message should
//appear in that player's text message list when the application is run. Messages such as replays can easily appear in all filter streams.

//Parameter notes:

//enumCharacterList WhichSender is the name of the character to be displayed as the sender of the message on the phone. Take from the character sheet as normal. 

//This parameter removed on trial basis! Text will be global for text messages rather than stored in a specific block.
//STRING WhichBlockOfTextToLoad is the common text block where all text messages are stored. This block is loaded when the phone displays the messages. 
//For just now, the common text block is TMSG. We may be using multiple text blocks in the future hence why the parameter is passed in.

//STRING WhichTextLabel is the FULL text label from the .txt file that corresponds to the message you want to send.

//enumTxtMsgLockedStatus WhichLockStatus defines what type of message is being sent. Passing in TXTMSG_LOCKED means the message will have no chance of being overwritten
//unless the text array becomes completely full of LOCKED messages This is highly unlikely to happen. Mission critical text messages should have TXTMSG_LOCKED status.
//Unimportant messages should be passed in with TXTMSG_UNLOCKED. Once the text array is full, any incoming message will search for the earliest UNLOCKED message to overwrite.

//enumTxtMsgMissionCritical WhichCriticalStatus - If a txtmsg is critical then when the text message application is brought up on screen, it will default to the single message display of
//this message rather than the normal list of chronological texts.

//enumTxtMsgIsReplyRequired WhichReplyStatus - if a text message is a question that allows a simple YES / NO reply e.g "Replay Mission?", then set this to REPLY_IS_REQUIRED 
//so the text message system knows to set up specific behaviour for these special messages.

//BOOL ShouldDisplayNewTxtMsgSignifier specifies that any new text message sent will display an onscreen indicator when it gets through to the phone.

//Example usage: SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_MICHAEL, "TMSG", "TXT_101", TXTMSG_LOCKED)

//This lets the text message system know if a textmsg, after having been found by a string comparison in the array, is currently listed as included in any filter.
//Used by DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER

FUNC BOOL IS_TEXTMSG_PRESENT_IN_ANY_FILTER(INT passedIndex)
    #if USE_CLF_DLC
        IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[0] = TRUE //Michael
        OR g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[1] = TRUE //Franklin
        OR g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[2] = TRUE //Trevor
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #ENDIF
    #if USE_NRM_DLC
        IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[0] = TRUE //Michael
        OR g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[1] = TRUE //Franklin
        OR g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[2] = TRUE //Trevor
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #ENDIF
    #if not USE_SP_DLC
        IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[0] = TRUE //Michael
        OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[1] = TRUE //Franklin
        OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[2] = TRUE //Trevor
            RETURN TRUE
        ELSE
            RETURN FALSE
        ENDIF
    #ENDIF

ENDFUNC

FUNC BOOL IS_TEXTMSG_PRESENT_IN_MULTIPLAYER_FILTER(INT passedIndex)

    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[passedIndex].PhonePresence[3] = TRUE //MULTIPLAYER

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Wiki-ed.
FUNC BOOL SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (enumCharacterList WhichSender, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,

                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ,   

                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,

                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //Make sure we only send to current player...

    enumTxtMsgSpecialComponents ForcedComponentType = NO_SPECIAL_COMPONENTS
    STRING ForcedBodyStringComponent = "NULL"
    INT ForcedNumberComponent = -99

    STRING ForcedSenderStringComponent = "NULL"

    INT ForcedNumberofAdditionalStrings = 0
    STRING ForcedSecondString = "NULL"
    STRING ForcedThirdString = "NULL"

    enumRecipientList SilentMessageRecipient = NO_SILENT_RECIPIENT

    IF ProcessTextMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            ForcedComponentType, ForcedBodyStringComponent, ForcedSenderStringComponent, ForcedNumberComponent,

                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus,
                            ShouldDisplayNewTxtMsgSignifier, WhichBarterStatus, WhichCanCallSenderStatus, ForcedNumberOfAdditionalStrings,
                            ForcedSecondString, ForcedThirdString, SilentMessageRecipient) = TRUE

        IF ShouldDisplayNewTxtMsgSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle

            #if USE_CLF_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[WhichSender].label
            #ENDIF
            #if USE_NRM_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[WhichSender].label
            #ENDIF
            #if not USE_SP_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText =  GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichSender)
            #ENDIF

            g_Temp_MP_PassedCharacterListRef = WhichSender

            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Text message successfully sent to current player. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL SEND_SILENT_TEXT_MESSAGE_TO_PLAYER_CHARACTER (enumCharacterList WhichSender, enumRecipientList WhichSilentRecipient, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,

                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ,   

                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,

                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER)

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //Make sure we only send to one player...

    enumTxtMsgSpecialComponents ForcedComponentType = NO_SPECIAL_COMPONENTS
    STRING ForcedBodyStringComponent = "NULL"
    INT ForcedNumberComponent = -99

    STRING ForcedSenderStringComponent = "NULL"

    INT ForcedNumberofAdditionalStrings = 0
    STRING ForcedSecondString = "NULL"
    STRING ForcedThirdString = "NULL"

    IF ProcessTextMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            ForcedComponentType, ForcedBodyStringComponent, ForcedSenderStringComponent, ForcedNumberComponent,

                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus,
                            ShouldDisplayNewTxtMsgSignifier, WhichBarterStatus, WhichCanCallSenderStatus, ForcedNumberOfAdditionalStrings,
                            ForcedSecondString, ForcedThirdString, WhichSilentRecipient) = TRUE

        IF ShouldDisplayNewTxtMsgSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle

            #if USE_CLF_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[WhichSender].label
            #ENDIF
            #if USE_NRM_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[WhichSender].label
            #ENDIF
            #if not USE_SP_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText =  GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichSender)
            #ENDIF

            g_Temp_MP_PassedCharacterListRef = WhichSender

            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Silent Text message successfully sent to specified player. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Wiki-ed.

#if USE_CLF_DLC
FUNC BOOL SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER_CLF (enumCharacterList WhichPlayerCharacter, enumCharacterList WhichSender, STRING WhichTextLabel,
                                               enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ, 
                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER)
    INT PC_Decision
    SWITCH WhichPlayerCharacter
        CASE CHAR_MICHAEL
            PC_Decision = SP_MICHAEL_BUFFER_INDEX
        BREAK

        CASE CHAR_FRANKLIN
            PC_Decision = SP_FRANKLIN_BUFFER_INDEX
        BREAK

            CASE CHAR_TREVOR
            PC_Decision = SP_TREVOR_BUFFER_INDEX
        BREAK

        DEFAULT
            #if IS_DEBUG_BUILD
                SCRIPT_ASSERT("BUFFERED TEXT MESSAGE - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintnl ()
            #endif  
            RETURN FALSE
        BREAK

    ENDSWITCH

    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLabel = WhichTextLabel
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgSender = WhichSender

    IF WhichLockStatus = TXTMSG_EMPTY
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you cannot define a text message lock status as empty. See Steve T")
            cdPrintnl ()
        #endif
        RETURN FALSE
    ELSE
        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLockStatus = WhichLockStatus
    ENDIF

    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCritical              = WhichCriticalStatus
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgAutoUnlockStatus      = WhichAutoUnlockStatus
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReplyStatus           = WhichReplyStatus
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCanCallSenderStatus   = WhichCanCallSenderStatus
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgBarterStatus          = WhichBarterStatus
    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReadStatus                = UNREAD_TXTMSG

         #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - BUFFERED TEXT Sending a message to character buffer ")
            cdPrintnl ()
            cdPrintint (PC_Decision)
            cdPrintnl()
        #endif

    RETURN TRUE                                        
ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER_NRM (enumCharacterList WhichPlayerCharacter, enumCharacterList WhichSender, STRING WhichTextLabel,
                                               enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ, 
                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER)
    INT PC_Decision
    SWITCH WhichPlayerCharacter
        CASE CHAR_NRM_MICHAEL
            PC_Decision = SP_MICHAEL_BUFFER_INDEX
        BREAK
        CASE CHAR_NRM_JIMMY
            PC_Decision = SP_FRANKLIN_BUFFER_INDEX
        BREAK
        CASE CHAR_NRM_TRACEY    
            PC_Decision = SP_TREVOR_BUFFER_INDEX
        BREAK

        DEFAULT
            #if IS_DEBUG_BUILD
                SCRIPT_ASSERT("BUFFERED TEXT MESSAGE - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintnl ()
            #endif  
            RETURN FALSE
        BREAK

    ENDSWITCH

    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLabel = WhichTextLabel
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgSender = WhichSender

    IF WhichLockStatus = TXTMSG_EMPTY
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you cannot define a text message lock status as empty. See Steve T")
            cdPrintnl ()
        #endif
        RETURN FALSE
    ELSE
        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLockStatus = WhichLockStatus
    ENDIF

    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCritical                = WhichCriticalStatus
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgAutoUnlockStatus        = WhichAutoUnlockStatus
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReplyStatus             = WhichReplyStatus
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCanCallSenderStatus     = WhichCanCallSenderStatus
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgBarterStatus            = WhichBarterStatus
    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReadStatus              = UNREAD_TXTMSG

         #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - BUFFERED TEXT Sending a message to character buffer ")
            cdPrintnl ()
            cdPrintint (PC_Decision)
            cdPrintnl()
        #endif

    RETURN TRUE                                        
ENDFUNC
#endif

FUNC BOOL SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER (enumCharacterList WhichPlayerCharacter, enumCharacterList WhichSender, STRING WhichTextLabel,
                                               enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ, 
                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender  WhichCanCallSenderStatus = CAN_CALL_SENDER)

#if USE_CLF_DLC
    return SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER_CLF(WhichPlayerCharacter, WhichSender,WhichTextLabel,WhichLockStatus,
                                                        WhichCriticalStatus,WhichAutoUnlockStatus,WhichReplyStatus,WhichBarterStatus,WhichCanCallSenderStatus)
#endif
#if USE_NRM_DLC
    return SEND_TEXT_MESSAGE_TO_CHARACTER_BUFFER_NRM(WhichPlayerCharacter, WhichSender,WhichTextLabel,WhichLockStatus,
                                                        WhichCriticalStatus,WhichAutoUnlockStatus,WhichReplyStatus,WhichBarterStatus,WhichCanCallSenderStatus)
#endif

#if not USE_SP_DLC
    INT PC_Decision
    SWITCH WhichPlayerCharacter
        CASE CHAR_MICHAEL
            PC_Decision = SP_MICHAEL_BUFFER_INDEX
        BREAK

        CASE CHAR_FRANKLIN
            PC_Decision = SP_FRANKLIN_BUFFER_INDEX
        BREAK

            CASE CHAR_TREVOR
            PC_Decision = SP_TREVOR_BUFFER_INDEX
        BREAK

        DEFAULT
            #if IS_DEBUG_BUILD
                SCRIPT_ASSERT("BUFFERED TEXT MESSAGE - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you must send to MICHAEL, FRANKLIN or TREVOR. See Steve T")
                cdPrintnl ()
            #endif  
            RETURN FALSE
        BREAK

    ENDSWITCH

    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLabel = WhichTextLabel
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgSender = WhichSender

    IF WhichLockStatus = TXTMSG_EMPTY
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - When sending a message to a character buffer, you cannot define a text message lock status as empty. See Steve T")
            cdPrintnl ()
        #endif
        RETURN FALSE
    ELSE
        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgLockStatus = WhichLockStatus
    ENDIF

    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCritical              = WhichCriticalStatus
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgAutoUnlockStatus      = WhichAutoUnlockStatus
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReplyStatus           = WhichReplyStatus
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgCanCallSenderStatus   = WhichCanCallSenderStatus
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgBarterStatus          = WhichBarterStatus
    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[PC_Decision].TxtMsgReadStatus            = UNREAD_TXTMSG

         #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PRIV - BUFFERED TEXT Sending a message to character buffer ")
            cdPrintnl ()
            cdPrintint (PC_Decision)
            cdPrintnl()
        #endif

    RETURN TRUE
#endif
ENDFUNC

//To force the text message to display the WhichSenderStringComponent rather than a character name from the Character list, set Whichsender to NO_CHARACTER

FUNC BOOL SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (enumCharacterList WhichSender, STRING WhichTextLabel, 
                                               enumTxtMsgLockedStatus WhichLockStatus, 

                                               STRING WhichBodyStringComponent, INT WhichNumberComponent,
                                               STRING WhichSenderStringComponent, 

                                               enumTxtMsgSpecialComponents WhichComponentType = NO_SPECIAL_COMPONENTS,

                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ,  

                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,
                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender WhichCanCallSenderStatus = CAN_CALL_SENDER, 
                                               INT WhichNumberofAdditionalStrings = 0,
                                               STRING WhichSecondStringComponent = NULL, STRING WhichThirdStringComponent = NULL)

    SWITCH WhichNumberOfAdditionalStrings

        CASE 0

            WhichSecondStringComponent = "NULL"
            WhichThirdStringComponent = "NULL"

        BREAK

        CASE 1

            WhichThirdStringComponent = "NULL"

        BREAK

        CASE 2

            //Do not clear. Accept second and third string components as is.

        BREAK

        DEFAULT

            #if IS_DEBUG_BUILD

                SCRIPT_ASSERT ("SEND_TEXT_MESSAGE... Weird additional string counter passed in. See Steve T.")

                cdPrintstring ("SEND_TEXT_MESSAGE... Weird additional string counter passed in. See Steve T.")
                cdPrintnl()

            #endif
        BREAK

    ENDSWITCH

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) //Make sure we only send to current player...

    enumRecipientList SilentMessageRecipient = NO_SILENT_RECIPIENT

    IF ProcessTextMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            WhichComponentType, WhichBodyStringComponent, WhichSenderStringComponent, WhichNumberComponent, 
                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus, 
                            ShouldDisplayNewTxtMsgSignifier, WhichBarterStatus, WhichCanCallSenderStatus, WhichNumberOfAdditionalStrings,
                            WhichSecondStringComponent, WhichThirdStringComponent, SilentMessageRecipient) = TRUE

        IF ShouldDisplayNewTxtMsgSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle
            #if USE_CLF_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[WhichSender].label
            #endif
            #if USE_NRM_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[WhichSender].label
            #endif
            #if not USE_SP_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichSender)
            #endif

            g_Temp_MP_PassedCharacterListRef = WhichSender
            g_Temp_MP_SenderString = WhichSenderStringComponent

            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Special component text message successfully sent to current player. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//This should be used to send a text message to the three single player characters. Not multiplayer!

FUNC BOOL SEND_TEXT_MESSAGE_TO_ALL_PLAYER_CHARACTERS (enumCharacterList WhichSender, STRING WhichTextLabel, enumTxtMsgLockedStatus WhichLockStatus, 
                                               enumTxtMsgMissionCritical WhichCriticalStatus = TXTMSG_NOT_CRITICAL,
                                               enumTxtMsgAutoUnlockAfterRead WhichAutoUnlockStatus = TXTMSG_AUTO_UNLOCK_AFTER_READ,  

                                               enumTxtMsgIsReplyRequired WhichReplyStatus = NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayNewTxtMsgSignifier = TRUE,

                                               enumTxtMsgIsBarterRequired WhichBarterStatus = NO_BARTER_REQUIRED,
                                               enumTxtMsgCanCallSender WhichCanCallSenderStatus = CAN_CALL_SENDER) 

   

    SET_BIT (BitSet_CellphoneDisplay, g_BS_SENDING_TXTMSG_TO_ALL_PLAYERS) 

    enumTxtMsgSpecialComponents ForcedComponentType = NO_SPECIAL_COMPONENTS
    STRING ForcedBodyStringComponent = "NULL"
    INT ForcedNumberComponent = -99

    STRING ForcedSenderStringComponent = "NULL"

    INT ForcedNumberofAdditionalStrings = 0
    STRING ForcedSecondString = "NULL"
    STRING ForcedThirdString = "NULL"

    enumRecipientList SilentMessageRecipient = NO_SILENT_RECIPIENT

    IF ProcessTextMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            ForcedComponentType, ForcedBodyStringComponent, ForcedSenderStringComponent, ForcedNumberComponent,

                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus,
                            ShouldDisplayNewTxtMsgSignifier, WhichBarterStatus, WhichCanCallSenderStatus,
                            ForcedNumberOfAdditionalStrings,
                            ForcedSecondString, ForcedThirdString, SilentMessageRecipient) = TRUE

        IF ShouldDisplayNewTxtMsgSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle
            
            #if USE_CLF_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[WhichSender].label
            #endif
            #if USE_NRM_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[WhichSender].label
            #endif
            #if not USE_SP_DLC
                SignifierData[SIG_TEXT].SignifierPrimaryText =  GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichSender)
				
				
            #endif

            g_Temp_MP_PassedCharacterListRef = WhichSender

            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Text message successfully sent to all player characters. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Only for use by MP only! Removes the signifier above the radar and will stop the phone defaulting to the textmessage app for the most recent text.
//Always do this BEFORE a purge if you want to make sure the signifier does not reappear.
PROC CLEAR_TEXT_MESSAGE_SIGNIFIER_FOR_MULTIPLAYER()

    IF g_bInMultiplayer
            #if USE_CLF_DLC
                g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LaunchToTextMessageScreen = FALSE 
            #endif
            #if USE_NRM_DLC
                g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LaunchToTextMessageScreen = FALSE 
            #endif
            #if not USE_SP_DLC
                g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LaunchToTextMessageScreen = FALSE 
            #endif

            #if IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring("CELLPHONE_PUBLIC - Clearing signifier via CLEAR_TEXT_MESSAGE_SIGNIFIER_FOR_MP")
                cdPrintnl()
            #endif

    ENDIF

ENDPROC

PROC CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER (enumCharacterList CharacterToClear)

    IF CharacterToClear <> CHAR_MICHAEL
    AND CharacterToClear <> CHAR_FRANKLIN
    AND CharacterToClear <> CHAR_TREVOR

        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_PUBLIC - CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER called on non player character!")
            cdPrintnl()
        #endif

    ELSE
        #if USE_CLF_DLC
            g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LaunchToTextMessageScreen = FALSE
            g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LastMessageSentMustBeRead = FALSE
        #endif
        #if USE_NRM_DLC
            g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LaunchToTextMessageScreen = FALSE
            g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LastMessageSentMustBeRead = FALSE
        #endif
        #if not USE_SP_DLC
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LaunchToTextMessageScreen = FALSE
            g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CharacterToClear].g_LastMessageSentMustBeRead = FALSE
        #endif
        
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring("CELLPHONE_PUBLIC - CLEAR_AUTO_LAUNCH_TO_TEXT_MESSAGE_APP_FOR_THIS_SP_CHARACTER called on player character ")
            cdPrintint(ENUM_TO_INT(CharacterToClear))
            cdPrintnl()
        #endif

    ENDIF

ENDPROC

//DANGER! This should only be used by MP when necessary. It clears out anything in the MP presence filter / portion of the overall text message array.

PROC PURGE_TEXT_MESSAGES_FOR_MULTIPLAYER()

    INT TxtMsgArrayIndex = MP_TEXT_PORTION_START //defined in cellphone_globals.sch

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES //As MP portion concludes at end of global text array, it's safe to do this.

            IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[CHAR_MULTIPLAYER] = TRUE

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[CHAR_MULTIPLAYER] = FALSE

            ENDIF

        TxtMsgArrayIndex ++

    ENDWHILE

    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LaunchToTextMessageScreen = FALSE

    #IF IS_DEBUG_BUILD

        cdPrintnl()
        cdPrintstring ("_______________________________________________________________________________________________________")
        cdPrintnl()
        cdPrintstring ("CELLPHONE_PUB - Purged mp text message list! g_TextMessage array set to TXTMSG_EMPTY and filters cleared.")
        cdPrintstring ("_______________________________________________________________________________________________________")
        cdPrintnl()

    #endif

ENDPROC

#if USE_CLF_DLC
FUNC BOOL HAS_THIS_TEXT_MESSAGE_BEEN_READ_CLF(STRING LabelToCheck)
    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = READ_TXTMSG
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Read Message found at text message array index  ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                    RETURN TRUE
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No text message label specified. Cannot locate read status of empty string.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE

    RETURN FALSE
ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL HAS_THIS_TEXT_MESSAGE_BEEN_READ_NRM(STRING LabelToCheck)
    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = READ_TXTMSG
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Read Message found at text message array index  ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                    RETURN TRUE
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No text message label specified. Cannot locate read status of empty string.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE

    RETURN FALSE
ENDFUNC
#endif

FUNC BOOL HAS_THIS_TEXT_MESSAGE_BEEN_READ (STRING LabelToCheck)
#if USE_CLF_DLC
    return HAS_THIS_TEXT_MESSAGE_BEEN_READ_CLF(LabelToCheck)
#endif
#if USE_NRM_DLC
    return HAS_THIS_TEXT_MESSAGE_BEEN_READ_NRM(LabelToCheck)
#endif

#if not USE_SP_DLC
    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = READ_TXTMSG
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Read Message found at text message array index  ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                    RETURN TRUE
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No text message label specified. Cannot locate read status of empty string.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE

    RETURN FALSE
#endif  
ENDFUNC

//Wiki-ed.
//This will set all text messages that were sent to the player's phone with that text label to TXTMSG_EMPTY. This means that they will no longer be included in
//the text message list displayed on the phone and the slot occupied in the text message array is available to write to when a fresh text message is set.
//Notes: For safety's sake, only messages with status of TXTMSG_UNLOCKED can be removed by this method

//This is a proc so that all messages with that label can be deleted at once.
#if USE_CLF_DLC
PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER_CLF(STRING LabelToDelete)
    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this message. It has LOCKED status.")
                        cdPrintnl()
                    #endif
                ELSE

                    Get_Cellphone_Owner()
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE
                    IF IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)
                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Remove message from current player filter only. Current player Int is ")
                            cdPrintint (ENUM_TO_INT(g_Cellphone.PhoneOwner))
                            cdPrintnl()
                        #endif
                    ELSE
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = NO_REPLY_REQUIRED

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Current player message no longer in any player filter. Assigned EMPTY status to array position: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF

                    //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
                    THEFEED_REMOVE_ITEM (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
ENDPROC
#endif

#if USE_NRM_DLC
PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER_NRM(STRING LabelToDelete)
    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this message. It has LOCKED status.")
                        cdPrintnl()
                    #endif
                ELSE

                    Get_Cellphone_Owner()
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE
                    IF IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)
                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Remove message from current player filter only. Current player Int is ")
                            cdPrintint (ENUM_TO_INT(g_Cellphone.PhoneOwner))
                            cdPrintnl()
                        #endif
                    ELSE
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = NO_REPLY_REQUIRED

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Current player message no longer in any player filter. Assigned EMPTY status to array position: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF

                    //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
                    THEFEED_REMOVE_ITEM (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
ENDPROC
#endif

PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER (STRING LabelToDelete)
#if USE_CLF_DLC
    DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER_CLF(LabelToDelete)
#endif
#if USE_NRM_DLC
    DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER_NRM(LabelToDelete)
#endif

#if not USE_SP_DLC
    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this message. It has LOCKED status.")
                        cdPrintnl()
                    #endif
                ELSE

                    Get_Cellphone_Owner()
                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE
                    IF IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)
                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Remove message from current player filter only. Current player Int is ")
                            cdPrintint (ENUM_TO_INT(g_Cellphone.PhoneOwner))
                            cdPrintnl()
                        #endif
                    ELSE
                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = NO_REPLY_REQUIRED

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Current player message no longer in any player filter. Assigned EMPTY status to array position: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF

                    //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
                    THEFEED_REMOVE_ITEM (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
#endif  
ENDPROC

//Whenever a text message has been sent, a feed ID is associated with it. By passing in the text label the text was originally sent with, the feed ID can be extracted in order
//to remove the feed entry independently of the text message itself. The latter remains in the phone unless other action is taken.
PROC REMOVE_TEXT_MESSAGE_FEED_ENTRY (STRING LabelOfFeedToRemove)

    INT TxtMsgArrayIndex = 0

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        #if USE_CLF_DLC
             IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelOfFeedToRemove)
                    //If still on screen, we should remove the Feed Entry associated with this text message label.
                    THEFEED_REMOVE_ITEM (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Call made to remove feed item associated with label at text message array index ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                 ENDIF
            ENDIF
        #endif
        #if USE_NRM_DLC
            IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelOfFeedToRemove)
                    //If still on screen, we should remove the Feed Entry associated with this text message label.
                    THEFEED_REMOVE_ITEM (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Call made to remove feed item associated with label at text message array index ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                 ENDIF
            ENDIF
        #endif
        #if not USE_SP_DLC
            IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelOfFeedToRemove)
                    //If still on screen, we should remove the Feed Entry associated with this text message label.
                    THEFEED_REMOVE_ITEM (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Call made to remove feed item associated with label at text message array index ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl()
                    #endif
                 ENDIF
            ENDIF
        #endif
        
        TxtMsgArrayIndex ++

    ENDWHILE

ENDPROC

//This will only remove feed entries that have been associated with a sent text message. Some feed messages may not have been sent by script.
//Consider using these if necessary:
//NATIVE PROC THEFEED_HIDE_THIS_FRAME()
//NATIVE PROC THEFEED_FLUSH_QUEUE()  - this will get rid of ALL feed items

PROC FLUSH_TEXT_MESSAGE_FEED_ENTRIES() 

    INT TxtMsgArrayIndex = 0

    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_PUB - Call made to flush all text message related feed items.")
        cdPrintnl()

    #endif

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        #if USE_CLF_DLC
            //Remove the Feed Entry associated with this text message label.
            THEFEED_REMOVE_ITEM (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
        #endif
        #if USE_NRM_DLC
            //Remove the Feed Entry associated with this text message label.
            THEFEED_REMOVE_ITEM (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
        #endif
        #if not USE_SP_DLC
            //Remove the Feed Entry associated with this text message label.
            THEFEED_REMOVE_ITEM (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgFeedEntryId)
        #endif
        
        TxtMsgArrayIndex ++

    ENDWHILE

ENDPROC
#if USE_CLF_DLC
PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS_CLF (STRING LabelToDelete)

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                    #IF IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this text message. It has LOCKED status.")
                        cdPrintnl ()
                    #endif
                ELSE
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE
                    
                    IF NOT IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)  //This should always return false due to the FALSE settings above.
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - All singleplayer characters message no longer in any player filter. Deleted message from list at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        
        TxtMsgArrayIndex ++
    ENDWHILE
ENDPROC
#endif

#if USE_NRM_DLC
PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS_NRM (STRING LabelToDelete)

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED
                    #IF IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this text message. It has LOCKED status.")
                        cdPrintnl ()
                    #endif
                ELSE
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE
                    
                    IF NOT IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)  //This should always return false due to the FALSE settings above.
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - All singleplayer characters message no longer in any player filter. Deleted message from list at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        
        TxtMsgArrayIndex ++
    ENDWHILE
ENDPROC
#endif

PROC DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS (STRING LabelToDelete)
#if USE_CLF_DLC
    DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS_CLF(LabelToDelete)
#endif
#if USE_NRM_DLC
    DELETE_TEXT_MESSAGE_BY_LABEL_FROM_ALL_PLAYER_CHARACTERS_NRM(LabelToDelete)
#endif

#if not USE_SP_DLC
    INT TxtMsgArrayIndex = 0

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES

        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)

            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToDelete)

                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_LOCKED

                    #IF IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Cannot delete this text message. It has LOCKED status.")
                        cdPrintnl ()

                    #endif

                ELSE

                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE

                    IF NOT IS_TEXTMSG_PRESENT_IN_ANY_FILTER (TxtMsgArrayIndex)  //This should always return false due to the FALSE settings above.

                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG

                        #IF IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - All singleplayer characters message no longer in any player filter. Deleted message from list at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl()
                        #endif
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot delete empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        
        TxtMsgArrayIndex ++
    ENDWHILE
#endif  
ENDPROC

FUNC BOOL CHECK_FOR_MESSAGE_PRESENCE_IN_CHARACTER_MESSAGE_LIST (STRING LabelToCheck, enumCharacterList CharacterToCheck )

    INT TxtMsgArrayIndex = 0
    BOOL CheckBool = FALSE

    IF ENUM_TO_INT(CharacterToCheck) > 3 //i.e not any SP or MP player character...

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - The character passed into CHECK_FOR_MESSAGE_PRESENCE is not a phone owner! See Steve T.")
            cdPrintnl ()
        #endif
        RETURN (FALSE)
    ENDIF

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        #if USE_CLF_DLC
            IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                    IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[ENUM_TO_INT(CharacterToCheck)]
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Specified message found in specified character message list at: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                        #endif
                        CheckBool = TRUE
                    ENDIF
                ENDIF
             ENDIF
        #endif
        #if USE_NRM_DLC
            IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                    IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[ENUM_TO_INT(CharacterToCheck)]
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Specified message found in specified character message list at: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                        #endif
                        CheckBool = TRUE
                    ENDIF
                ENDIF
             ENDIF
        #endif
        #if not USE_SP_DLC
            IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
                IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[ENUM_TO_INT(CharacterToCheck)]
                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - Specified message found in specified character message list at: ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                        #endif
                        CheckBool = TRUE
                    ENDIF
                ENDIF
             ENDIF
        #endif 
         TxtMsgArrayIndex ++
    ENDWHILE

    IF CheckBool = TRUE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message found in specified character message list.")
            cdPrintnl ()
        #endif
        RETURN (TRUE)
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message NOT found in specified character message list.")
            cdPrintnl ()
        #endif
        RETURN (FALSE)
    ENDIF

ENDFUNC
#if USE_CLF_DLC
FUNC BOOL CHECK_FOR_MESSAGE_PRESENCE_IN_ANY_PLAYER_CHARACTER_MESSAGE_LIST_CLF (STRING LabelToCheck)

    INT TxtMsgArrayIndex = 0
    BOOL CheckBool = FALSE

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES

        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0]
                OR g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1]
                OR g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2]  
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Specified message found in Michael, Trevor or Franklin message list at: ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl ()
                    #endif
                    CheckBool = TRUE
                ENDIF
            ENDIF
         ENDIF
         TxtMsgArrayIndex ++
    ENDWHILE
    IF CheckBool = TRUE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (TRUE)
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message NOT found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (FALSE)
    ENDIF

ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL CHECK_FOR_MESSAGE_PRESENCE_IN_ANY_PLAYER_CHARACTER_MESSAGE_LIST_NRM (STRING LabelToCheck)

    INT TxtMsgArrayIndex = 0
    BOOL CheckBool = FALSE

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES

        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0]
                OR g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1]
                OR g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2]  
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Specified message found in Michael, Trevor or Franklin message list at: ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl ()
                    #endif
                    CheckBool = TRUE
                ENDIF
            ENDIF
         ENDIF
         TxtMsgArrayIndex ++
    ENDWHILE
    IF CheckBool = TRUE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (TRUE)
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message NOT found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (FALSE)
    ENDIF

ENDFUNC
#endif

FUNC BOOL CHECK_FOR_MESSAGE_PRESENCE_IN_ANY_PLAYER_CHARACTER_MESSAGE_LIST (STRING LabelToCheck)
#if USE_CLF_DLC
    return CHECK_FOR_MESSAGE_PRESENCE_IN_ANY_PLAYER_CHARACTER_MESSAGE_LIST_CLF(LabelToCheck)
#endif
#if USE_NRM_DLC
    return CHECK_FOR_MESSAGE_PRESENCE_IN_ANY_PLAYER_CHARACTER_MESSAGE_LIST_NRM(LabelToCheck)
#endif

#if not USE_SP_DLC
    INT TxtMsgArrayIndex = 0
    BOOL CheckBool = FALSE

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES

        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToCheck)
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0]
                OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1]
                OR g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2]  
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Specified message found in Michael, Trevor or Franklin message list at: ")
                        cdPrintint (TxtMsgArrayIndex)
                        cdPrintnl ()
                    #endif
                    CheckBool = TRUE
                ENDIF
            ENDIF
         ENDIF
         TxtMsgArrayIndex ++
    ENDWHILE
    IF CheckBool = TRUE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (TRUE)
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Specified message NOT found in any character message list.")
            cdPrintnl ()
        #endif
        RETURN (FALSE)
    ENDIF
#endif
ENDFUNC

//DANGER! This should only be used by the flow when necessary.
//This will completely reset the master text message list and as a consequence, all player character filters.
PROC PURGE_TEXT_MESSAGES_FOR_ALL_PLAYER_CHARACTERS()

    INT TxtMsgArrayIndex = 0

    #if USE_CLF_DLC
        WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE
            TxtMsgArrayIndex ++    
        ENDWHILE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LaunchToTextMessageScreen = FALSE
    #endif
    #if USE_NRM_DLC
        WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE
            TxtMsgArrayIndex ++    
        ENDWHILE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LaunchToTextMessageScreen = FALSE
    #endif

    #if not USE_SP_DLC
        WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReadStatus = UNREAD_TXTMSG
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = FALSE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = FALSE
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = FALSE
            TxtMsgArrayIndex ++    
        ENDWHILE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LaunchToTextMessageScreen = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LaunchToTextMessageScreen = FALSE
    #endif

    #IF IS_DEBUG_BUILD
        cdPrintnl()
        cdPrintstring ("_______________________________________________________________________________________________________")
        cdPrintnl()
        cdPrintstring ("CELLPHONE_PUB - Purged text message list! g_TextMessage array set to TXTMSG_EMPTY and filters cleared.")
        cdPrintstring ("_______________________________________________________________________________________________________")
        cdPrintnl()
    #endif

ENDPROC

FUNC BOOL IS_TEXTMESSAGE_LIST_IN_SINGLE_MESSAGE_VIEW() 

    #if USE_CLF_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessCLF")) > 0

            IF g_Cellphone.PhoneDS = PDS_COMPLEXAPP

                RETURN TRUE

            ELSE

                RETURN FALSE // Using text message app but it must be in full list view, so return false.

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF
    #endif
     #if USE_NRM_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessNRM")) > 0

            IF g_Cellphone.PhoneDS = PDS_COMPLEXAPP

                RETURN TRUE

            ELSE

                RETURN FALSE // Using text message app but it must be in full list view, so return false.

            ENDIF

        ELSE

            RETURN FALSE

        ENDIF
    #endif

    #if not USE_CLF_DLC
    #if not USE_NRM_DLC
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTextMessage")) > 0
            IF g_Cellphone.PhoneDS = PDS_COMPLEXAPP
                RETURN TRUE
            ELSE                
                RETURN FALSE // Using text message app but it must be in full list view, so return false.
            ENDIF
        ELSE
            RETURN FALSE
        ENDIF
    #endif
    #endif

ENDFUNC

PROC AUTO_RETURN_TO_FULL_TEXT_MESSAGE_LIST_VIEW()

    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_AUTO_BACK_TEXTS_SINGLEMESSAGE_VIEW) 

    #if IS_DEBUG_BUILD

        PRINTLN("\nCELLPHONE_PUB - Auto_Return_to_Full_Text bit set by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

    #endif

ENDPROC

//Wiki-ed.
//DANGER! This should really only be used by the flow. Locked messages will include those that are mission critical, so if you are removing the lock in order to delete
//a message, then proceed with caution!

//Update: If the message was critical it gets rid of that status and removes any signifiers.
#if USE_CLF_DLC
PROC UNLOCK_TEXT_MESSAGE_BY_LABEL_CLF (STRING LabelToUnlock, BOOL b_EnMasseDeletion = FALSE)

    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToUnlock)
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called on EMPTY message index by [", GET_THIS_SCRIPT_NAME(), "] through public function. Overwriting with UNLOCK as a test.")
                        PRINTNL()
                    #endif  
                ENDIF

                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
                IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_CRITICAL
                    //Revert all three player characters... do this based on presence
                    IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = TRUE
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[0] = FALSE
                    ENDIF

                    IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = TRUE
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[1] = FALSE
                    ENDIF

                    IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = TRUE
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[2] = FALSE
                    ENDIF

                    // KGM 19/9/11: Added to allow Critical MP txtmsgs to be unlocked and cleaned up without being read
                    IF g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[3] = TRUE
                        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - MP Critical text message marked as not critical at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                            cdPrintstring ("CELLPHONE_PUB - MP Setting g_LastMessageSentMustBeRead to FALSE")
                            cdPrintnl()
                        #endif
                        //g_LastMessageSentMustBeRead[3] = FALSE
                    ENDIF

                    g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_NOT_CRITICAL

                    //If en masse deletion is required, set the flag in this message and this will ensure that when the message is deleted, either manually or from script,
                    //it will be removed from all character filters.
                    IF b_EnMasseDeletion = TRUE
                        g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgDeletionMode = DELETE_FROM_ALL_CHARACTERS
                    ENDIF
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Unlocked text message at array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintnl ()
                #endif
            ENDIF
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE

ENDPROC 
#endif
#if USE_NRM_DLC
PROC UNLOCK_TEXT_MESSAGE_BY_LABEL_NRM (STRING LabelToUnlock, BOOL b_EnMasseDeletion = FALSE)

    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToUnlock)
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called on EMPTY message index by [", GET_THIS_SCRIPT_NAME(), "] through public function. Overwriting with UNLOCK as a test.")
                        PRINTNL()
                    #endif  
                ENDIF

                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
                IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_CRITICAL
                    //Revert all three player characters... do this based on presence
                    IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = TRUE
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MICHAEL].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[0] = FALSE
                    ENDIF

                    IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = TRUE
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[1] = FALSE
                    ENDIF

                    IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = TRUE
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[2] = FALSE
                    ENDIF

                    // KGM 19/9/11: Added to allow Critical MP txtmsgs to be unlocked and cleaned up without being read
                    IF g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[3] = TRUE
                        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - MP Critical text message marked as not critical at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                            cdPrintstring ("CELLPHONE_PUB - MP Setting g_LastMessageSentMustBeRead to FALSE")
                            cdPrintnl()
                        #endif
                        //g_LastMessageSentMustBeRead[3] = FALSE
                    ENDIF

                    g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_NOT_CRITICAL

                    //If en masse deletion is required, set the flag in this message and this will ensure that when the message is deleted, either manually or from script,
                    //it will be removed from all character filters.
                    IF b_EnMasseDeletion = TRUE
                        g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgDeletionMode = DELETE_FROM_ALL_CHARACTERS
                    ENDIF
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Unlocked text message at array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintnl ()
                #endif
            ENDIF
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE

ENDPROC
#endif

PROC UNLOCK_TEXT_MESSAGE_BY_LABEL (STRING LabelToUnlock, BOOL b_EnMasseDeletion = FALSE)
#if USE_CLF_DLC
    unlock_text_message_by_label_CLF(LabelToUnlock,b_EnMasseDeletion)
#endif
#if USE_NRM_DLC
    unlock_text_message_by_label_NRM(LabelToUnlock,b_EnMasseDeletion)
#endif
#if not USE_SP_DLC
    #if IS_DEBUG_BUILD
        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToUnlock)
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_EMPTY
                    #if IS_DEBUG_BUILD
                        PRINTNL()
                        PRINTLN("\nCELLPHONE_PUB - UNLOCK_TEXT_MESSAGE_BY_LABEL called on EMPTY message index by [", GET_THIS_SCRIPT_NAME(), "] through public function. Overwriting with UNLOCK as a test.")
                        PRINTNL()
                    #endif  
                ENDIF

                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLockStatus = TXTMSG_UNLOCKED
                IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_CRITICAL
                    //Revert all three player characters... do this based on presence
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[0] = TRUE
                        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[0] = FALSE
                    ENDIF

                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[1] = TRUE
                        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[1] = FALSE
                    ENDIF

                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[2] = TRUE
                        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
                        //g_LastMessageSentMustBeRead[2] = FALSE
                    ENDIF

                    // KGM 19/9/11: Added to allow Critical MP txtmsgs to be unlocked and cleaned up without being read
                    IF g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].PhonePresence[3] = TRUE
                        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

                        #if IS_DEBUG_BUILD
                            cdPrintstring ("CELLPHONE_PUB - MP Critical text message marked as not critical at array position ")
                            cdPrintint (TxtMsgArrayIndex)
                            cdPrintnl ()
                            cdPrintstring ("CELLPHONE_PUB - MP Setting g_LastMessageSentMustBeRead to FALSE")
                            cdPrintnl()
                        #endif
                        //g_LastMessageSentMustBeRead[3] = FALSE
                    ENDIF

                    g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgCritical = TXTMSG_NOT_CRITICAL

                    //If en masse deletion is required, set the flag in this message and this will ensure that when the message is deleted, either manually or from script,
                    //it will be removed from all character filters.
                    IF b_EnMasseDeletion = TRUE
                        g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgDeletionMode = DELETE_FROM_ALL_CHARACTERS
                    ENDIF
                ENDIF

                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Unlocked text message at array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintnl ()
                #endif
            ENDIF
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
#endif
ENDPROC

//Tbis should ONLY be used by the flow... not for MULTIPLAYER. If you need an equivalent, please speak to me.

//Checks if any player character has a critical message waiting...
#if USE_CLF_DLC
FUNC BOOL IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_ANY_PLAYER_CLF()  

    IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead 
    OR g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB -  Critical text message pending in Michael's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF

        IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Franklin's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF

        IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Trevor's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical text message pending in any character's stream. Can accept new critical message.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_ANY_PLAYER_NRM()  

    IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead 
    OR g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead
    
        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MICHAEL].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB -  Critical text message pending in Michael's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF
        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Franklin's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF
        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Trevor's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical text message pending in any character's stream. Can accept new critical message.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif
FUNC BOOL IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_ANY_PLAYER()  
#if USE_CLF_DLC
    return IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_ANY_PLAYER_CLF()   
#endif
#if USE_NRM_DLC
    return IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_ANY_PLAYER_NRM()   
#endif

#if not USE_SP_DLC
    IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead 
    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB -  Critical text message pending in Michael's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF

        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Franklin's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF

        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in Trevor's list and has yet to be read. Won't accept another message for that character unless this is overridden.")
                cdPrintnl()
            #endif
        ENDIF
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical text message pending in any character's stream. Can accept new critical message.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF
#endif
ENDFUNC

//Specifies if the cellphone signifier movie is drawn to the screen, regardless of any signifiers present. 
//
//CAUTION!
//
//
//Any onscreen signifiers will be hidden when this command is called with TRUE. Please ensure you call this with FALSE in your mission cleanup.
//

PROC HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE (BOOL b_PassedBool)

    IF b_PassedBool = TRUE

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Signifier movie hidden by script [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            cdPrintstring("CELLPHONE_PUB - Signifier movie HIDDEN by script")
            cdPrintnl()

            g_Debug_Signifiers_Hidden_by_Script = TRUE

        #endif

        SET_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE)

    ELSE

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Signifier movie now set to draw by script [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            cdPrintstring("CELLPHONE_PUB - Signifier movie re-enabled by script")
            cdPrintnl()
            
            g_Debug_Signifiers_Hidden_by_Script = FALSE

        #endif

        CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_DISABLE_SIGNIFIERS_FOR_CUTSCENE)

    ENDIF

ENDPROC

//Checks if the player character currently under control has a critical message in unread state...
FUNC BOOL IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_CURRENT_PLAYER()  

    Get_Cellphone_Owner()
    
    //IF g_LastMessageSentMustBeRead[g_Cellphone.PhoneOwner]
    #if USE_CLF_DLC
        IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead
                #IF IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - Critical text message pending in current character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                    cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    cdPrintnl()
                #endif
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for current character. Can accept new critical message for his stream.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
    #endif
    #if USE_NRM_DLC
        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead
                #IF IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - Critical text message pending in current character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                    cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    cdPrintnl()
                #endif
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for current character. Can accept new critical message for his stream.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
    #endif
    #if not USE_SP_DLC
        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].g_LastMessageSentMustBeRead
                #IF IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring ("CELLPHONE_PUB - Critical text message pending in current character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                    cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                    cdPrintnl()
                #endif
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for current character. Can accept new critical message for his stream.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
    #endif  
ENDFUNC

//Checks if the player character currently under control has a critical message in unread state...
FUNC BOOL IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_PLAYER_ENUM(enumCharacterList PlayerEnum)  

    //Check enum is valid.
    IF  PlayerEnum != CHAR_MICHAEL
    AND PlayerEnum != CHAR_FRANKLIN
    AND PlayerEnum != CHAR_TREVOR
        SCRIPT_ASSERT("IS_CRITICAL_MESSAGE_ALREADY_PENDING_FOR_PLAYER_ENUM: Enum passed was not for a player. Could not return valid result.")
        RETURN FALSE
    ENDIF

    //IF g_LastMessageSentMustBeRead[g_Cellphone.PhoneOwner]
    #if USE_CLF_DLC
        IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[PlayerEnum].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in player character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                cdPrintnl()
            #ENDIF
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for player character. Can accept new critical message for his stream.")
                cdPrintnl()
            #ENDIF
            RETURN FALSE
        ENDIF
    #endif
    
    #if USE_NRM_DLC
        IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[PlayerEnum].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in player character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                cdPrintnl()
            #ENDIF
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for player character. Can accept new critical message for his stream.")
                cdPrintnl()
            #ENDIF
            RETURN FALSE
        ENDIF
    #endif
    
    #if not USE_SP_DLC
        IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[PlayerEnum].g_LastMessageSentMustBeRead
            #IF IS_DEBUG_BUILD
                cdPrintnl()
                cdPrintstring ("CELLPHONE_PUB - Critical text message pending in player character's stream and has yet to be read. Won't accept another message for that character unless this is overridden. Character Int is ")
                cdPrintint(ENUM_TO_INT(g_Cellphone.PhoneOwner))
                cdPrintnl()
            #ENDIF
            RETURN TRUE
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No critical text message pending for player character. Can accept new critical message for his stream.")
                cdPrintnl()
            #ENDIF
            RETURN FALSE
        ENDIF
    #endif
ENDFUNC

//Overrides any critical block for all player characters. Singleplayer only.
#if USE_CLF_DLC
FUNC BOOL OVERRIDE_CRITICAL_MESSAGE_BLOCK_FOR_ALL_PLAYERS_CLF()

    IF g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead
    OR g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - A critical message was already pending but blocks in all player streams have been overridden. Another message can be accepted")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical message was pending but blocks overridden anyway.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif

#if USE_NRM_DLC
FUNC BOOL OVERRIDE_CRITICAL_MESSAGE_BLOCK_FOR_ALL_PLAYERS_NRM()

    IF g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead
    OR g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - A critical message was already pending but blocks in all player streams have been overridden. Another message can be accepted")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical message was pending but blocks overridden anyway.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif

FUNC BOOL OVERRIDE_CRITICAL_MESSAGE_BLOCK_FOR_ALL_PLAYERS()

#if USE_CLF_DLC
    return OVERRIDE_CRITICAL_MESSAGE_BLOCK_FOR_ALL_PLAYERS_CLF()
#endif
#if USE_NRM_DLC
    return OVERRIDE_CRITICAL_MESSAGE_BLOCK_FOR_ALL_PLAYERS_NRM()
#endif

#if not USE_SP_DLC
    IF g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead
    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead
    OR g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - A critical message was already pending but blocks in all player streams have been overridden. Another message can be accepted")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE
        g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - No critical message was pending but blocks overridden anyway.")
            cdPrintnl()
        #endif
        RETURN FALSE
    ENDIF
#endif
ENDFUNC

//Wiki-ed.
//This function can be used to check if a text message that takes a reply, e.g a "replay mission" text, has received a specified reply.
//Returns one of the following:
//ENUM enumTxtMsgIsReplyRequired //specifies whether this text message will allow the player to reply with yes / no... a "replay mission" txt for example.

//    NO_REPLY_REQUIRED,
//    REPLY_IS_REQUIRED, 
//    REPLIED_YES,
//    REPLIED_NO

//ENDENUM
#if USE_CLF_DLC
FUNC enumTxtMsgIsReplyRequired GET_TEXT_MESSAGE_REPLY_STATUS_CLF (STRING LabelToQuery)

    INT TxtMsgArrayIndex = 0
    enumTxtMsgIsReplyRequired eReplyStatusToReturn = NO_REPLY_REQUIRED
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToQuery)
                eReplyStatusToReturn = g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus
                RETURN eReplyStatusToReturn
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot find reply match for empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
    
    RETURN eReplyStatusToReturn
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC enumTxtMsgIsReplyRequired GET_TEXT_MESSAGE_REPLY_STATUS_NRM (STRING LabelToQuery)

    INT TxtMsgArrayIndex = 0
    enumTxtMsgIsReplyRequired eReplyStatusToReturn = NO_REPLY_REQUIRED
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToQuery)
                eReplyStatusToReturn = g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus
                RETURN eReplyStatusToReturn
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot find reply match for empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
    
    RETURN eReplyStatusToReturn
ENDFUNC
#endif

FUNC enumTxtMsgIsReplyRequired GET_TEXT_MESSAGE_REPLY_STATUS (STRING LabelToQuery)
#if USE_CLF_DLC
    return GET_TEXT_MESSAGE_REPLY_STATUS_CLF(LabelToQuery)
#endif
#if USE_NRM_DLC
    return GET_TEXT_MESSAGE_REPLY_STATUS_NRM(LabelToQuery)
#endif

#if not USE_SP_DLC

    INT TxtMsgArrayIndex = 0
    enumTxtMsgIsReplyRequired eReplyStatusToReturn = NO_REPLY_REQUIRED
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToQuery)
                eReplyStatusToReturn = g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus
                RETURN eReplyStatusToReturn
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No txtmessage label specified. Cannot find reply match for empty string.")
                cdPrintnl ()
            #endif
        ENDIF
        TxtMsgArrayIndex ++
    ENDWHILE
    
    RETURN eReplyStatusToReturn
#endif  
ENDFUNC

//If you need to make sure that any text message with the passed label in the global array does not already contain a RESPONDED_YES or RESPONDED_NO tag
//but does require a reply if the same label is sent again, e.g a replay message, then call this. Call it just before you make your new request to SEND_TEXT_MESSAGE...
//This prevents a scenario where a fresh text message may auto-accept or reject because an earlier text with the same label has already been responded to by the player
//which would result in GET_TEXT_MESSAGE_REPLY_STATUS firing off the previous response.
#if USE_CLF_DLC
PROC SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED_CLF (STRING LabelToClear)

    INT TxtMsgArrayIndex = 0

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToClear)
                g_savedGlobalsClifford.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = REPLY_IS_REQUIRED
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Setting text message in array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintstring ("to require a reply via script.")
                    cdPrintnl()
                #endif
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Cannot find reply match for empty string.")
                cdPrintnl()
            #endif
        ENDIF

        TxtMsgArrayIndex ++
    ENDWHILE

ENDPROC
#endif
#if USE_NRM_DLC
PROC SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED_NRM (STRING LabelToClear)

    INT TxtMsgArrayIndex = 0
    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToClear)
                g_savedGlobalsnorman.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = REPLY_IS_REQUIRED
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Setting text message in array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintstring ("to require a reply via script.")
                    cdPrintnl()
                #endif
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Cannot find reply match for empty string.")
                cdPrintnl()
            #endif
        ENDIF

        TxtMsgArrayIndex ++
    ENDWHILE

ENDPROC
#endif
PROC SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED (STRING LabelToClear)

#if USE_CLF_DLC
    SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED_CLF(LabelToclear)
#endif

#if USE_NRM_DLC
    SET_ALL_MATCHING_TEXT_MESSAGES_TO_REPLY_REQUIRED_NRM(LabelToclear)
#endif

#if not USE_SP_DLC
    INT TxtMsgArrayIndex = 0

    WHILE TxtMsgArrayIndex < MAX_TEXT_MESSAGES
        IF NOT IS_STRING_NULL (g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel)
            IF ARE_STRINGS_EQUAL(g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgLabel, LabelToClear)
                g_SavedGlobals.sTextMessageSavedData.g_TextMessage[TxtMsgArrayIndex].TxtMsgReplyStatus = REPLY_IS_REQUIRED
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Setting text message in array position ")
                    cdPrintint (TxtMsgArrayIndex)
                    cdPrintstring ("to require a reply via script.")
                    cdPrintnl()
                #endif
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Cannot find reply match for empty string.")
                cdPrintnl()
            #endif
        ENDIF

        TxtMsgArrayIndex ++
    ENDWHILE
#endif
ENDPROC

//Picture Message handling.

//Important that you set this to false when you're finished and in cleanup. It dynamically alters the cellphone camera help text to include and facilitate a "send" option.

PROC ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP (BOOL b_Enable)

    IF b_Enable

        SET_BIT (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Camera and help text set to include SEND functionality")
            cdPrintnl()

        #endif

    ELSE

        CLEAR_BIT (Bitset_CellphoneDisplay, g_BS_DISPLAY_SEND_IN_CAMERA_HELP_TEXT)

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Camera and help text now removes SEND functionality")
            cdPrintnl()

        #endif

    ENDIF

ENDPROC

//If this is true, but HAS_CONTACT_RECEIVED is false, then you know the wrong contact has been selected by the user...

FUNC BOOL HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT() 

    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SENDPIC_CONTACT_SELECTED)

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//Checks that a specific contact has received a picture message.  If a character has been sent a picture message, HAS_PICMSG_STORED will be the enum state for this element of his character sheet data.

FUNC BOOL HAS_CONTACT_RECEIVED_PICTURE_MESSAGE (enumCharacterList CharacterToCheck) 

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT
        #if USE_CLF_DLC
            IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].picmsgStatus = HAS_PICMSG_STORED
                RETURN TRUE
            ELSE
                RETURN FALSE
            ENDIF
        #endif
        #if USE_NRM_DLC
            IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].picmsgStatus = HAS_PICMSG_STORED
                RETURN TRUE
            ELSE
                RETURN FALSE
            ENDIF
        #endif
        #if not USE_SP_DLC
            IF GLOBAL_CHARACTER_SHEET_GET_PICTURE_MESSAGE_STATUS(CharacterToCheck) = HAS_PICMSG_STORED  
                RETURN TRUE
            ELSE
                RETURN FALSE
            ENDIF
        #endif  
    ELSE

        /* Removed to make thing simpler for Hunting oddjob.
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("This character is not a phone contact. See Steve or Keith")
        #endif
        */

        RETURN FALSE

    ENDIF

ENDFUNC

//Get rids of the HAS_PICMSG_STORED element of a character's individual sheet. Do this before your "take pic and send" routines to make sure you are starting afresh.

FUNC BOOL CLEAR_CONTACT_PICTURE_MESSAGE (enumCharacterList CharacterToCheck)
    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT
        #if USE_CLF_DLC
            g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].picmsgStatus = NO_PICMSG_STORED
        #endif
        #if USE_NRM_DLC
            g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].picmsgStatus = NO_PICMSG_STORED
        #endif
        #if not USE_SP_DLC
			GLOBAL_CHARACTER_SHEET_SET_PICTURE_MESSAGE_STATUS(CharacterToCheck, NO_PICMSG_STORED)
        #endif
        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Picture message status for contact cleared to empty.")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve or Keith")
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC

//Missed Calls
#if USE_CLF_DLC
FUNC BOOL SET_CONTACT_AS_MISSED_CALLER_CLF (enumCharacterList CharacterToCheck)

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT

        g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
        IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].phonebookState[ENUM_TO_INT (g_Cellphone.PhoneOwner)] = NOT_LISTED 
            //Adds missed caller to current player.    
            INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)

            ADD_UNKNOWN_CONTACT_TO_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt), FALSE)

        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Missed call from contact already in phonebook...")
                cdPrintnl()
            #endif
        ENDIF
        #IF IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Missed call set.")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE     
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve, Ben or Keith")
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL SET_CONTACT_AS_MISSED_CALLER_NRM (enumCharacterList CharacterToCheck)

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT
        g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
        IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].phonebookState[ENUM_TO_INT (g_Cellphone.PhoneOwner)] = NOT_LISTED 
            //Adds missed caller to current player.    
            INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)

            ADD_UNKNOWN_CONTACT_TO_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt), FALSE)

        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Missed call from contact already in phonebook...")
                cdPrintnl()
            #endif
        ENDIF
        #IF IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Missed call set.")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE     
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve, Ben or Keith")
        #endif
        RETURN FALSE
    ENDIF

ENDFUNC
#endif

FUNC BOOL SET_CONTACT_AS_MISSED_CALLER (enumCharacterList CharacterToCheck)
#if USE_CLF_DLC
    return SET_CONTACT_AS_MISSED_CALLER_CLF(CharacterToCheck)
#endif
#if USE_NRM_DLC
    return SET_CONTACT_AS_MISSED_CALLER_NRM(CharacterToCheck)
#endif
#if not USE_SP_DLC
    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT
		GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(CharacterToCheck, ENUM_TO_INT(g_Cellphone.PhoneOwner), MISSED_CALL)
 
        IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CharacterToCheck, ENUM_TO_INT (g_Cellphone.PhoneOwner)) = NOT_LISTED 
            //Adds missed caller to current player.    
            INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)

            ADD_UNKNOWN_CONTACT_TO_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt), FALSE)

        ELSE
            #IF IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - Missed call from contact already in phonebook...")
                cdPrintnl()
            #endif
        ENDIF
        #IF IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Missed call set.")
            cdPrintnl()
        #endif
        RETURN TRUE
    ELSE     
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve, Ben or Keith")
        #endif
        RETURN FALSE
    ENDIF
#endif
ENDFUNC

FUNC BOOL CLEAR_MISSED_CALLER_STATUS_FOR_CONTACT (enumCharacterList CharacterToCheck)

    IF g_CharacterSheetNonSaved[CharacterToCheck].phone <> NO_PHONE_CONTACT
        #if USE_CLF_DLC
            IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Clearing missed call status...")
                    cdPrintnl()
                #endif
                g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = NO_MISSED_CALL
                //Remove this caller from the current player's phonebook if he is an unknown caller.
                IF g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CharacterToCheck].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER                            
                    INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)                
                    REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt)) 
                ENDIF
                RETURN TRUE
            ELSE
                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Missed call status was already clear for this contact.")
                    cdPrintnl()
                #endif
                RETURN FALSE
            ENDIF 
        #endif
        #if USE_NRM_DLC
            IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = MISSED_CALL
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Clearing missed call status...")
                    cdPrintnl()
                #endif
                g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].missedCallStatus[g_Cellphone.PhoneOwner] = NO_MISSED_CALL
                //Remove this caller from the current player's phonebook if he is an unknown caller.
                IF g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CharacterToCheck].StatusAsCaller[g_Cellphone.PhoneOwner] = UNKNOWN_CALLER                            
                    INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)                
                    REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt)) 
                ENDIF
                RETURN TRUE
            ELSE
                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Missed call status was already clear for this contact.")
                    cdPrintnl()
                #endif
                RETURN FALSE
            ENDIF 
        #endif

        #if not USE_SP_DLC
            IF GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(CharacterToCheck, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = MISSED_CALL
                #IF IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Clearing missed call status...")
                    cdPrintnl()
                #endif
				GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(CharacterToCheck, ENUM_TO_INT(g_Cellphone.PhoneOwner), NO_MISSED_CALL)
				
                //Remove this caller from the current player's phonebook if he is an unknown caller.
                IF  GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(CharacterToCheck, ENUM_TO_INT(g_Cellphone.PhoneOwner)) = UNKNOWN_CALLER                      
                    INT tempInt = ENUM_TO_INT (g_Cellphone.PhoneOwner)                
                    REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK (CharacterToCheck, INT_TO_ENUM (enumPhoneBookPresence, tempInt)) 
                ENDIF
                RETURN TRUE
            ELSE
                #if IS_DEBUG_BUILD
                    cdPrintstring ("CELLPHONE_PUB - Missed call status was already clear for this contact.")
                    cdPrintnl()
                #endif
                RETURN FALSE
            ENDIF 
        #endif
    ELSE
        #if IS_DEBUG_BUILD
            SCRIPT_ASSERT ("CELLPHONE_PUB - This character is not a phone contact. See Steve, Ben or Keith")
        #endif
        RETURN FALSE
    ENDIF
ENDFUNC

//_______________________________________________________________________________________________________________________________________________________________________
//
//                      Special Application section.
//
//_______________________________________________________________________________________________________________________________________________________________________

//Second application screen functionality.

//Currently only the sniper app is making use of the second screen. This may be expanded upon.
#if USE_CLF_DLC
PROC ENABLE_SECOND_SCREEN_SNIPER_APP_CLF (BOOL bEnable = FALSE)

    IF bEnable 
        g_savedGlobalsClifford.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE
        g_savedGlobalsClifford.sCellphoneSettingsData.b_IsSniperAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF

ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC ENABLE_SECOND_SCREEN_SNIPER_APP_NRM (BOOL bEnable = FALSE)

    IF bEnable 
        g_savedGlobalsnorman.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE
        g_savedGlobalsnorman.sCellphoneSettingsData.b_IsSniperAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF

ENDPROC
#ENDIF
PROC ENABLE_SECOND_SCREEN_SNIPER_APP (BOOL bEnable = FALSE)
#if USE_CLF_DLC
    ENABLE_SECOND_SCREEN_SNIPER_APP_CLF(bEnable)
#endif
#if USE_NRM_DLC
    ENABLE_SECOND_SCREEN_SNIPER_APP_NRM(bEnable)
#endif
#if not USE_SP_DLC
    IF bEnable 
        g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE
        g_savedGlobals.sCellphoneSettingsData.b_IsSniperAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Sniper App disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF
#endif

ENDPROC

FUNC BOOL HAS_SNIPER_APP_BEEN_SELECTED()

    IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_SNIPER_APP_SELECTED)

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

PROC CLEAR_SNIPER_APP_SELECTION()

    CLEAR_BIT (BitSet_CellphoneDisplay, g_BS_SNIPER_APP_SELECTED)

ENDPROC

//Trackify
#if USE_CLF_DLC
PROC ENABLE_SECOND_SCREEN_TRACKIFY_APP_CLF (BOOL bEnable = FALSE)
    IF bEnable 
        g_savedGlobalsClifford.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ELSE
        g_savedGlobalsClifford.sCellphoneSettingsData.b_IsTrackifyAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF
ENDPROC
#ENDIF

#if USE_NRM_DLC
PROC ENABLE_SECOND_SCREEN_TRACKIFY_APP_NRM (BOOL bEnable = FALSE)
    IF bEnable 
        g_savedGlobalsnorman.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ELSE
        g_savedGlobalsnorman.sCellphoneSettingsData.b_IsTrackifyAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF
ENDPROC
#ENDIF

PROC ENABLE_SECOND_SCREEN_TRACKIFY_APP (BOOL bEnable = FALSE)
#if USE_CLF_DLC
    ENABLE_SECOND_SCREEN_TRACKIFY_APP_CLF(bEnable)
#ENDIF
#if USE_NRM_DLC
    ENABLE_SECOND_SCREEN_TRACKIFY_APP_NRM(bEnable)
#ENDIF
#if not USE_SP_DLC
    IF bEnable 
        g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable = TRUE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ELSE
        g_savedGlobals.sCellphoneSettingsData.b_IsTrackifyAppAvailable = FALSE
        #if IS_DEBUG_BUILD
            PRINTLN("\nCELLPHONE_PUB - Trackify disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif
    ENDIF
#ENDIF
ENDPROC

//Multiplayer Trackify

PROC ENABLE_MULTIPLAYER_TRACKIFY_APP (BOOL bEnable = FALSE)

    IF bEnable 

        SET_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - MULTIPLAYER Trackify enabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

         //g_b_Secondary_Screen_Available = TRUE //enable seconds screen if not already present, which in turn adds trackify app.

    ELSE

        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_ENABLE_MP_TRACKIFY)

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - MULTIPLAYER Trackify disabled from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

         //g_b_Secondary_Screen_Available = FALSE //default to having secondary screen unavailable

    ENDIF

ENDPROC

FUNC BOOL LAUNCH_TRACKIFY_IMMEDIATELY(BOOL SkipLoadingScreen = FALSE)

    IF LAUNCH_CELLPHONE_APPLICATION (appTrackify, FALSE, TRUE)

        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Trackify launched from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        #endif

        IF SkipLoadingScreen = TRUE

            SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)

        ELSE

            CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_SKIP_TRACKIFY_LOADING_SCREEN)

        ENDIF

        RETURN TRUE

    ELSE

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Failed to launch Trackify via script function.")
            cdPrintnl()

        #endif

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL IS_CELLPHONE_TRACKIFY_IN_USE()

    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH ("appTrackify")) > 0 

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

PROC SET_TRACKIFY_TARGET_VECTOR (VECTOR YourVector, BOOL b_HideTargetArrow = FALSE, BOOL b_DisplayRelativeHeight = FALSE)

    g_v_TrackifyTarget = YourVector

    CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REMOVE_TRACKIFY_TARGET)

    IF b_HideTargetArrow

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify target arrow hidden from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify target arrow shown from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

    IF b_DisplayRelativeHeight

        SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify relative height set to display from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify relative height hidden from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

ENDPROC

PROC SET_NUMBER_OF_MULTIPLE_TRACKIFY_TARGETS(INT YourTargetAmount)

    IF YourTargetAmount > c_MaxNumberOfTrackifyTargets //currently at 4

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - WARNING! Number of trackify targets is greater than Max Number. Set from [", GET_THIS_SCRIPT_NAME(), "] through public function.")

            cdPrintstring("CELLPHONE_PUB - Faulty number of Trackify Targets passed was ")
            cdPrintint(YourTargetAmount)

        #endif

    ELSE

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Number of Multiple Trackify Targets set to  ")
            cdPrintint(YourTargetAmount)
            cdPrintnl()

        #endif

        g_i_NumberOfTrackifyTargets = YourTargetAmount

    ENDIF

ENDPROC

//Specifies the arrow type for this Trackify target, Useful for displaying relative height indicators between a target blip and another entity. You need to set this every time you want the arrow to change.
//Trackify doesn't work out the relative heights between things. You need to do that calculation in your script and then call the procedure below.
/*
	ENUM eTrackifyArrowType
		Arrow_Off,
		Arrow_Neutral, 
		Arrow_Up,
		Arrow_Down
	ENDENUM
*/
PROC SET_TRACKIFY_MULTIPLE_TARGET_ARROW_TYPE (INT WhichTarget, eTrackifyArrowType ArrowType)

	g_v_TrackifyMPTargetArrowType[WhichTarget] = ArrowType

ENDPROC






PROC SET_TRACKIFY_MULTIPLE_TARGET_VECTOR (INT WhichTarget, VECTOR YourVector, BOOL b_HideTargetArrow = TRUE, BOOL b_DisplayRelativeHeight = FALSE)

    g_v_TrackifyMultipleTarget[WhichTarget] = YourVector

    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_PUB - Specifying Multiple Target Vector instance ")
        cdPrintint(WhichTarget)
        cdPrintstring(" at ")
        cdPrintint(FLOOR(YourVector.X))
        cdPrintint(FLOOR(YourVector.Y))
        cdPrintint(FLOOR(YourVector.Z))
        cdPrintnl()

    #endif

    SWITCH WhichTarget

        CASE 0
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_0)
        BREAK

        CASE 1
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_1)
        BREAK

        CASE 2
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_2)
        BREAK

        CASE 3
            CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_3)
        BREAK

        DEFAULT

            #if IS_DEBUG_BUILD
                //Make sure this always displays regardless of phone debug text widget toggle.
                PRINTLN("\nCELLPHONE_PUB - Weird Target specified [", GET_THIS_SCRIPT_NAME(), "] through public function. This will likely cause an array overrun")
            #endif

        BREAK

    ENDSWITCH

    g_v_TrackifyMultipleTarget[WhichTarget] = YourVector

    IF b_HideTargetArrow

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify target arrow hidden from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_HIDE_TRACKIFY_TARGET_ARROW)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify target arrow shown from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

    IF b_DisplayRelativeHeight

        SET_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify relative height set to display from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Third, g_BSTHIRD_DISPLAY_TRACKIFY_RELATIVE_HEIGHT)

        #if IS_DEBUG_BUILD
            //Make sure this always displays regardless of phone debug text widget toggle.
            PRINTLN("\nCELLPHONE_PUB - Trackify relative height hidden from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

ENDPROC

PROC REMOVE_TRACKIFY_MULTIPLE_TARGET (INT WhichTarget)

    #if IS_DEBUG_BUILD

        cdPrintstring ("CELLPHONE_PUB - Specifying Multiple Target Vector for removal. Passed instance ")
        cdPrintint(WhichTarget)
        cdPrintnl()

    #endif

    SWITCH WhichTarget

        CASE 0
            SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_0)
        BREAK

        CASE 1
            SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_1)
        BREAK

        CASE 2
            SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_2)
        BREAK

        CASE 3
            SET_BIT (BitSet_CellphoneTU, g_BSTU_REMOVE_MP_TRACKIFY_TARGET_3)
        BREAK

        DEFAULT

            #if IS_DEBUG_BUILD
                //Make sure this always displays regardless of phone debug text widget toggle.
                PRINTLN("\nCELLPHONE_PUB - Weird Removal Target specified [", GET_THIS_SCRIPT_NAME(), "] through public function..")
            #endif

        BREAK

    ENDSWITCH

ENDPROC

PROC REMOVE_TRACKIFY_TARGET ()

    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_REMOVE_TRACKIFY_TARGET)

    #if IS_DEBUG_BUILD
    //Make sure this always displays regardless of phone debug text widget toggle.
    PRINTLN("\nCELLPHONE_PUB - Trackify target removed from [", GET_THIS_SCRIPT_NAME(), "] through public function.")
    #endif

ENDPROC

//This function will make a concluded call return the phone to the homescreen rather than put it away. You need to call this during or just before the phonecall takes
//place as the controlling bit is cleared after the phone returns to the homescreen or it is put away by other means.

PROC SET_FINISHED_CALL_RETURNS_TO_HOMESCREEN (BOOL ReturnToHomeScreen)

    IF ReturnToHomeScreen

        //Make sure this always displays regardless of phone debug text widget toggle.
        #if IS_DEBUG_BUILD

            PRINTLN("\nCELLPHONE_PUB - Concluded cellphone call set to homescreen return by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Set_Finished_Call_Returns_To_Homescreen bit set!")
            cdPrintnl()

        #endif

        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

    ELSE

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_FINISHED_CALL_RETURNS_TO_HOMESCREEN)

    ENDIF

ENDPROC

FUNC BOOL IS_CELLPHONE_ON_HOMESCREEN()

    IF g_Cellphone.PhoneDS = PDS_MAXIMUM

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//You need to make sure that the finale contacts are currently in the player's phonebook before calling this.
FUNC BOOL LAUNCH_FINALE_CHOICE_CONTACT_LIST()


    IF g_Cellphone.PhoneDS > PDS_AWAY

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - 2433277 fix 2. Tried to launch Finale Choice Contact List but phone was already in hand. Hanging up phone.")
            cdPrintnl()
        #endif

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) //Fix for 2433277

        HANG_UP_AND_PUT_AWAY_PHONE()

        RETURN FALSE

    ENDIF


    SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) //Needs to be done before app launch, get reset automatically after successful launch.

    IF (LAUNCH_CELLPHONE_APPLICATION (AppCONTACTS, FALSE, TRUE, TRUE)) //don't load movies, override phone already on screen check, use CONTACTS_APP_STACK_SIZE...

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - Launch Finale Choice Contact List function called and launched from script. 2433277 launch fix.")
            cdPrintnl()
        #endif

        RETURN TRUE

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintstring ("CELLPHONE_PUB - 2433277 fix. Tried to launch Finale Choice Contact List but Launch_Cellphone_Application has returned false. 2433277 fix.")
            cdPrintnl()
        #endif

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_LAUNCH_FINALE_CONTACTS) //Fix for 2433277

        RETURN FALSE

    ENDIF

ENDFUNC

//Sidetask prototypes for ambient department.
//Example usage:

//SET_SIDETASK_LIVE(CHAR_ARIANO, Sidetask_2)

FUNC BOOL SET_SIDETASK_LIVE (enumCharacterList WhichSideTaskGiver, enumSideTaskList WhichSideTaskIdentifier, BOOL ShouldDisplayNewSideTaskSignifier = TRUE,
                                                enumSideTaskLockedStatus WhichLockStatus = SIDETASK_UNLOCKED)

    IF IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()  //If the phone is flow sleeping or disabled, no new sidetask can be delivered, so we must return false. 

        #IF IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Mobile phone is flow sleeping! Can't set sidetask as live!")
            cdPrintnl()

        #endif

        RETURN FALSE

    ENDIF

    IF ProcessLiveSideTask (WhichSideTaskGiver, WhichSideTaskIdentifier, ShouldDisplayNewSideTaskSignifier, WhichLockStatus) = TRUE

        #IF IS_DEBUG_BUILD 

            cdPrintstring ("CELLPHONE_PUB - Sidetask successfully set to live.")
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

PROC SET_SIDETASK_DEAD (enumSideTaskList WhichSideTaskIdentifier)

    INT SideTaskArrayIndex = 0

    WHILE SideTaskArrayIndex < MAX_NUM_SIDETASKS

            IF g_LiveSideTask[SideTaskArrayIndex].SideTaskIdentifier = WhichSideTaskIdentifier

                IF g_LiveSideTask[SideTaskArrayIndex].SideTaskLockStatus = SIDETASK_LOCKED

                    #IF IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Cannot set this sidetask to dead. It has LOCKED status.")
                        cdPrintnl()

                    #endif

                ELSE

                    g_LiveSideTask[SideTaskArrayIndex].SideTaskLockStatus = SIDETASK_EMPTY

                    #IF IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Sidetask set to dead and removed from live array at position ")
                        cdPrintint (SideTaskArrayIndex)
                        cdPrintnl()

                    #endif

                ENDIF

            ENDIF

        SideTaskArrayIndex ++

    ENDWHILE

ENDPROC

//________________________________________________________________________________________________________________________________________________________________________________
//
//                                                                      Special MP and SP character secondary function integration.
//________________________________________________________________________________________________________________________________________________________________________________

 FUNC BOOL SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER (enumCharacterList WhichSender, STRING WhichTextLabel, enumMPemailLockedStatus WhichLockStatus, 
                                               enumMPemailMissionCritical WhichCriticalStatus = EMAIL_NOT_CRITICAL,

                                               enumMPemailAutoUnlockAfterRead WhichAutoUnlockStatus = EMAIL_AUTO_UNLOCK_AFTER_READ,   

                                               enumMPemailIsReplyRequired WhichReplyStatus = MPE_NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayEmailSignifier = TRUE,

                                               enumMPemailIsBarterRequired WhichBarterStatus = MPE_NO_BARTER_REQUIRED,
                                               enumMPemailCanCallSender  WhichCanCallSenderStatus = MPE_CANNOT_CALL_SENDER)

    enumTxtMsgSpecialComponents ForcedComponentType = NO_SPECIAL_COMPONENTS
    STRING ForcedBodyStringComponent = "NULL"
    INT ForcedNumberComponent = -99

    STRING ForcedSenderStringComponent = "NULL"

    INT ForcedNumberofAdditionalStrings = 0
    STRING ForcedSecondString = "NULL"
    STRING ForcedThirdString = "NULL"

    enumRecipientList SilentMessageRecipient = NO_SILENT_RECIPIENT

    IF ProcessEmailMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            ForcedComponentType, ForcedBodyStringComponent, ForcedSenderStringComponent, ForcedNumberComponent,

                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus,
                            ShouldDisplayEmailSignifier, WhichBarterStatus, WhichCanCallSenderStatus, ForcedNumberOfAdditionalStrings,
                            ForcedSecondString, ForcedThirdString, SilentMessageRecipient) = TRUE

        IF ShouldDisplayEmailSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle

            /* Need updated
            g_TextMessageIconStyle = WhichIconStyle



            g_Temp_MP_PassedCharacterListRef = WhichSender

            SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)
            */

            SET_BIT (BitSet_CellphoneTU, g_BSTU_DISPLAY_EMAIL_SIGNIFIER) 

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Email message successfully sent to current player. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//To force the text message to display the WhichSenderStringComponent rather than a character name from the Character list, set Whichsender to NO_CHARACTER

FUNC BOOL SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS (enumCharacterList WhichSender, STRING WhichTextLabel, 
                                               enumMPemailLockedStatus WhichLockStatus, 

                                               STRING WhichBodyStringComponent, INT WhichNumberComponent,
                                               STRING WhichSenderStringComponent, 

                                               enumTxtMsgSpecialComponents WhichComponentType = NO_SPECIAL_COMPONENTS,

                                               enumMPemailMissionCritical WhichCriticalStatus = EMAIL_NOT_CRITICAL,
                                               enumMPemailAutoUnlockAfterRead WhichAutoUnlockStatus = EMAIL_AUTO_UNLOCK_AFTER_READ,  

                                               enumMPemailIsReplyRequired WhichReplyStatus = MPE_NO_REPLY_REQUIRED, 
                                               eTextMessageIconStyle WhichIconStyle = ICON_STANDARD_TEXTMSG,
                                               BOOL ShouldDisplayEmailSignifier = TRUE,

                                               enumMPemailIsBarterRequired WhichBarterStatus = MPE_NO_BARTER_REQUIRED,
                                               enumMPemailCanCallSender WhichCanCallSenderStatus = MPE_CANNOT_CALL_SENDER, 

                                               INT WhichNumberofAdditionalStrings = 0,
                                               STRING WhichSecondStringComponent = NULL, STRING WhichThirdStringComponent = NULL)

    SWITCH WhichNumberOfAdditionalStrings

        CASE 0

            WhichSecondStringComponent = "NULL"
            WhichThirdStringComponent = "NULL"

        BREAK

        CASE 1

            WhichThirdStringComponent = "NULL"

        BREAK

        CASE 2

            //Do not clear. Accept second and third string components as is.

        BREAK

        DEFAULT

            #if IS_DEBUG_BUILD

                SCRIPT_ASSERT ("SEND_EMAIL_MESSAGE_SC... Weird additional string counter passed in. See Steve T.")

                cdPrintstring ("SEND_EMAIL_MESSAGE_SC... Weird additional string counter passed in. See Steve T.")
                cdPrintnl()

            #endif
        BREAK

    ENDSWITCH

    enumRecipientList SilentMessageRecipient = NO_SILENT_RECIPIENT

    IF ProcessEmailMessage (WhichSender, WhichTextLabel, WhichLockStatus, 

                            WhichComponentType, WhichBodyStringComponent, WhichSenderStringComponent, WhichNumberComponent, 
                            WhichCriticalStatus, WhichAutoUnlockStatus, WhichReplyStatus, 
                            ShouldDisplayEmailSignifier, WhichBarterStatus, WhichCanCallSenderStatus, WhichNumberOfAdditionalStrings,
                            WhichSecondStringComponent, WhichThirdStringComponent, SilentMessageRecipient) = TRUE

        IF ShouldDisplayEmailSignifier = TRUE

            g_TextMessageIconStyle = WhichIconStyle

            //g_Temp_MP_PassedCharacterListRef = WhichSender
            //g_Temp_MP_SenderString = WhichSenderStringComponent

            //SET_BIT (BitSet_CellphoneDisplay, g_BS_DISPLAY_NEW_TXTMSG_SIGNIFIER)
            //SET_BIT (BitSet_CellphoneDisplay, g_BS_CELLPHONE_TEXTFEED_NEEDS_UPDATED)

            SET_BIT (BitSet_CellphoneTU, g_BSTU_DISPLAY_EMAIL_SIGNIFIER) 

        ENDIF

        #if IS_DEBUG_BUILD 

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUB - Special component email message successfully sent to current player. Label was: ")
            cdPrintstring (WhichTextLabel)
            cdPrintnl()

        #endif

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

FUNC BOOL IS_EMAIL_PRESENT_IN_MULTIPLAYER_FILTER(INT passedIndex)

    IF g_EmailMessage[passedIndex].PhonePresence[3] = TRUE //MULTIPLAYER

        RETURN TRUE

    ELSE

        RETURN FALSE

    ENDIF

ENDFUNC

//If the player has a message matching the text label passed in his current inbox, then this will return true.
FUNC BOOL CHECK_FOR_MP_EMAIL_PRESENCE_IN_CHARACTER_INBOX (STRING LabelToCheck, enumCharacterList CharacterToCheck = CHAR_MULTIPLAYER )

    INT EmailMsgArrayIndex = 0
    BOOL CheckBool = FALSE

    IF ENUM_TO_INT(CharacterToCheck) > 3 //i.e not any SP or MP player character...

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - The character passed into CHECK_FOR_MP_EMAIL_PRESENCE is not a phone owner! See Steve T.")
            cdPrintnl ()

        #endif

        RETURN (FALSE)

    ENDIF

    WHILE EmailMsgArrayIndex < MAX_MP_EMAILS

        IF NOT IS_STRING_NULL (g_EmailMessage[EmailMsgArrayIndex].emailLabel)

            IF ARE_STRINGS_EQUAL(g_EmailMessage[EmailMsgArrayIndex].emailLabel, LabelToCheck)

                IF g_EmailMessage[EmailMsgArrayIndex].PhonePresence[ENUM_TO_INT(CharacterToCheck)]

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Specified email found in specified character inbox at: ")
                        cdPrintint (EmailMsgArrayIndex)
                        cdPrintnl ()

                    #endif

                    CheckBool = TRUE

                ENDIF

            ENDIF

         ENDIF

         EmailMsgArrayIndex ++

    ENDWHILE

    IF CheckBool = TRUE

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Specified email found in specified character inbox.")
            cdPrintnl ()

        #endif

        RETURN (TRUE)

    ELSE

        #if IS_DEBUG_BUILD

            cdPrintstring ("CELLPHONE_PUB - Specified email NOT found in specified character inbox.")
            cdPrintnl ()

        #endif

        RETURN (FALSE)

    ENDIF

ENDFUNC

//Returns one of the following 
//    MPE_NO_REPLY_REQUIRED,
//    MPE_REPLY_IS_REQUIRED, 
//    MPE_REPLIED_BARTER,
//    MPE_REPLIED_YES,
//    MPE_REPLIED_NO

FUNC enumMPemailIsReplyRequired GET_MP_EMAIL_REPLY_STATUS (STRING LabelToQuery)

    INT EmailMsgArrayIndex = 0

    enumMPemailIsReplyRequired eReplyStatusToReturn = MPE_NO_REPLY_REQUIRED

    WHILE EmailMsgArrayIndex < MAX_MP_EMAILS

        IF NOT IS_STRING_NULL (g_EmailMessage[EmailMsgArrayIndex].emailLabel)

            IF ARE_STRINGS_EQUAL(g_EmailMessage[EmailMsgArrayIndex].emailLabel, LabelToQuery)

                eReplyStatusToReturn = g_EmailMessage[EmailMsgArrayIndex].EmailReplyStatus

                RETURN eReplyStatusToReturn

            ENDIF

        ELSE

            #IF IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PUB - No email label specified. Cannot find reply match for empty string.")
                cdPrintnl ()

            #endif

        ENDIF

        EmailMsgArrayIndex ++

    ENDWHILE

    RETURN eReplyStatusToReturn

ENDFUNC

PROC SET_MP_EMAIL_REPLY_STATUS(STRING LabelToSet, enumMPemailIsReplyRequired WhichReplyStatus = MPE_NO_REPLY_REQUIRED)

    #if IS_DEBUG_BUILD

        PRINTLN("\nCELLPHONE_PUB - SET_MP_EMAIL_REPLY_STATUS called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        //DEBUG_PRINTCALLSTACK()

    #endif

    INT EmailArrayIndex = 0

    WHILE EmailArrayIndex < MAX_MP_EMAILS

        IF NOT IS_STRING_NULL (g_EmailMessage[EmailArrayIndex].EmailLabel)

            IF ARE_STRINGS_EQUAL (g_EmailMessage[EmailArrayIndex].EmailLabel, LabelToSet)

                IF g_EmailMessage[EmailArrayIndex].EmailLockStatus = EMAIL_LOCKED

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Warning! This email was locked before setting reply status: ")
                        cdPrintint (EmailArrayIndex)
                        cdPrintnl()

                    #endif

                ENDIF

                Get_Cellphone_Owner()

                g_EmailMessage[EmailArrayIndex].EmailReplyStatus = WhichReplyStatus

            ENDIF

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PUB - No email label specified. Cannot set reply status of empty string.")
                cdPrintnl()

            #endif

        ENDIF

        EmailArrayIndex ++

    ENDWHILE

ENDPROC

PROC DELETE_EMAIL_BY_LABEL_FROM_CURRENT_PLAYER (STRING LabelToDelete)

    #if IS_DEBUG_BUILD

        PRINTLN("\nCELLPHONE_PUB - DELETE_EMAIL_BY_LABEL_FROM_CURRENT_PLAYER called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")

        //DEBUG_PRINTCALLSTACK()

    #endif

    INT EmailArrayIndex = 0

    WHILE EmailArrayIndex < MAX_MP_EMAILS

        IF NOT IS_STRING_NULL (g_EmailMessage[EmailArrayIndex].EmailLabel)

            IF ARE_STRINGS_EQUAL (g_EmailMessage[EmailArrayIndex].EmailLabel, LabelToDelete)

                IF g_EmailMessage[EmailArrayIndex].EmailLockStatus = EMAIL_LOCKED

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Warning! This email was locked before deletion: ")
                        cdPrintint (EmailArrayIndex)
                        cdPrintnl()

                    #endif

                ENDIF

                    Get_Cellphone_Owner()

                    g_EmailMessage[EmailArrayIndex].PhonePresence[g_Cellphone.PhoneOwner] = FALSE

                    g_EmailMessage[EmailArrayIndex].EmailLockStatus = EMAIL_EMPTY
                    g_EmailMessage[EmailArrayIndex].EmailReadStatus = UNREAD_MPEMAIL
                    g_EmailMessage[EmailArrayIndex].EmailReplyStatus = MPE_NO_REPLY_REQUIRED

                    #if IS_DEBUG_BUILD

                        cdPrintstring ("CELLPHONE_PUB - Current player email message no longer in player filter. Assigned EMPTY status to array position: ")
                        cdPrintint (EmailArrayIndex)
                        cdPrintnl()

                    #endif

                    //If still on screen, we should remove the Feed Entry associated with this text message label now that it has been deleted. 
                    THEFEED_REMOVE_ITEM (g_EmailMessage[EmailArrayIndex].EmailFeedEntryId)

            ENDIF

        ELSE

            #if IS_DEBUG_BUILD

                cdPrintstring ("CELLPHONE_PUB - No email label specified. Cannot delete empty string.")
                cdPrintnl()

            #endif

        ENDIF

        EmailArrayIndex ++

    ENDWHILE

ENDPROC

FUNC BOOL HAS_THIS_MP_EMAIL_BEEN_READ (STRING LabelToCheck)

    INT EmailArrayIndex = 0
	
    WHILE EmailArrayIndex < MAX_MP_EMAILS
        IF NOT IS_STRING_NULL (g_EmailMessage[EmailArrayIndex].emailLabel)
            IF ARE_STRINGS_EQUAL(g_EmailMessage[EmailArrayIndex].emailLabel, LabelToCheck)
                IF g_EmailMessage[EmailArrayIndex].emailReadStatus =  READ_MPEMAIL
                    #if IS_DEBUG_BUILD
                        cdPrintstring ("CELLPHONE_PUB - Read Message found at MP email array index  ")
                        cdPrintint (EmailArrayIndex)
                        cdPrintnl()
                    #endif
                    RETURN TRUE
                ENDIF
            ENDIF
        ELSE
            #if IS_DEBUG_BUILD
                cdPrintstring ("CELLPHONE_PUB - No MP email specified. Cannot locate read status of empty string.")
                cdPrintnl()
            #endif
            RETURN FALSE
        ENDIF
       EmailArrayIndex ++
    ENDWHILE

    RETURN FALSE
 
ENDFUNC







PROC HIDE_PHONE_IN_FIRST_PERSON_EXPERIMENTAL()

    //May need to check that no phone or chat call is ongoing here 
    IF g_Cellphone.PhoneDS = PDS_MAXIMUM
    OR g_Cellphone.PhoneDS = PDS_RUNNINGAPP
    OR g_Cellphone.PhoneDS = PDS_COMPLEXAPP 

        //Might have to have a check in here to not put the phone away if it's on the last couple of lines? Polish.

        //Set appropriate bits...
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_VANISH_PHONE) //Let flashhand know transit routine should commence...
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_ALREADY_SIGNALLED_TO_VANISH_BY_DH) //we don't want to go through this routine more than once in one call...

        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_PHONE_FULLY_VANISHED)
        CLEAR_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CALL_IN_PROGRESS_REINSTATE_PHONE)

        #if IS_DEBUG_BUILD

            cdPrintnl()
            cdPrintstring ("CELLPHONE_PUBLIC - HIDE_PHONE_IN_FIRST_PERSON_EXPERIMENTAL()")
            cdPrintnl()

        #endif

    ENDIF

ENDPROC

//#1896233
//Returns the correct currently selected cellphone prop tint ID for the passed player character from the player's saved setting. 
//The return value can be passed into commands such as SET_PLAYER_PHONE_PALETTE_IDX to modify the cellphone prop's outer casing.
//Alex will be opening up functionality on other peds with 1895413

//Update! TASK_USE_MOBILE_PHONE will always use Michael's phone as the prop on any other character, so the return values have been altered to cope with this.
//The original phone prop character specific return values are cloned in PROP_SPECIFIC_GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER should we need it.

FUNC INT GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER(enumCharacterList PassedChar)

        //WARNING! The tints are not uniform ids. "Blue" is not tint ID 0 across all phone props. Hence the explicit cases.
        //UPDATE! Number after // in the CASE decisions reflects the correct values for that character's specific phone prop. TASK_USE_MOBILE_PHONE 
        //use's Michael's prop regardless of ped it seems.

        IF PassedChar = CHAR_MICHAEL //Franklin's tints seem jumbled...
            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF  
                    CASE 1  RETURN 0    BREAK //Light Blue Theme from Settings Menu                
                    CASE 2  RETURN 1    BREAK//Green
                    CASE 3  RETURN 2    BREAK//Red
                    CASE 4  RETURN 3    BREAK//Orange
                    CASE 5  RETURN 4    BREAK//Grey
                    CASE 6  RETURN 5    BREAK//Purple
                    CASE 7  RETURN 6    BREAK//Pink
                    
                    DEFAULT
                        #if IS_DEBUG_BUILD
                            cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Michael CASE. Returning -1")
                            cdPrintnl()
                        #endif
                        RETURN -1
                    BREAK
                    
            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF
        ENDIF

        IF PassedChar = CHAR_FRANKLIN //Remove this when tints are fixed...

            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF
            
                CASE 1  RETURN 0    BREAK //Light Blue Theme from Settings Menu                
                CASE 2  RETURN 1    BREAK//Green
                CASE 3  RETURN 2    BREAK//Red
                CASE 4  RETURN 3    BREAK//Orange
                CASE 5  RETURN 4    BREAK//Grey
                CASE 6  RETURN 5    BREAK//Purple
                CASE 7  RETURN 6    BREAK//Pink

                DEFAULT
                    #if IS_DEBUG_BUILD
                        cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Franklin CASE. Returning -1")
                        cdPrintnl()
                    #endif
                    RETURN -1
                BREAK
                
            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF

        ENDIF

        IF PassedChar = CHAR_TREVOR

            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF
            
                CASE 1  RETURN 0    BREAK //Light Blue Theme from Settings Menu                
                CASE 2  RETURN 1    BREAK//Green
                CASE 3  RETURN 2    BREAK//Red
                CASE 4  RETURN 3    BREAK//Orange
                CASE 5  RETURN 4    BREAK//Grey
                CASE 6  RETURN 5    BREAK//Purple
                CASE 7  RETURN 6    BREAK//Pink

                DEFAULT
                    #if IS_DEBUG_BUILD
                        cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Trevor CASE. Returning -1")
                        cdPrintnl()
                    #endif
                    RETURN -1
                BREAK
                
            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF
        ENDIF

        #if IS_DEBUG_BUILD
            cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Passed Character was not Michael, Franklin or Trevor. Returning -2")
            cdPrintnl()
        #endif
        
    RETURN -2
ENDFUNC

FUNC INT PROP_SPECIFIC_GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER(enumCharacterList PassedChar)

        //WARNING! The tints are not uniform ids. "Blue" is not tint ID 0 across all phone props. Hence the explicit cases.
        //UPDATE!

        IF PassedChar = CHAR_MICHAEL //Franklin's tints seem jumbled...
            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer
            #ENDIF

                CASE 1  RETURN 0    BREAK //Light Blue Theme from Settings Menu                
                CASE 2  RETURN 1    BREAK//Green
                CASE 3  RETURN 2    BREAK//Red
                CASE 4  RETURN 3    BREAK//Orange
                CASE 5  RETURN 4    BREAK//Grey
                CASE 6  RETURN 5    BREAK//Purple
                CASE 7  RETURN 6    BREAK//Pink

                DEFAULT
                    #if IS_DEBUG_BUILD
                        cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Michael CASE. Returning -1")
                        cdPrintnl()
                    #endif
                    RETURN -1
                BREAK

            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF
        ENDIF

        IF PassedChar = CHAR_FRANKLIN //Remove this when tints are fixed...

            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer
            #ENDIF
            
                CASE 1   RETURN 6     BREAK//Light Blue Theme from Settings Menu
                CASE 2   RETURN 0     BREAK//Green
                CASE 3   RETURN 1     BREAK//Red
                CASE 4   RETURN 2     BREAK//Orange
                CASE 5   RETURN 3     BREAK//Grey
                CASE 6   RETURN 4     BREAK//Purple
                CASE 7   RETURN 5     BREAK//Pink

                DEFAULT
                    #if IS_DEBUG_BUILD
                        cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Franklin CASE. Returning -1")
                        cdPrintnl()
                    #endif
                    RETURN -1
                BREAK
            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF

        ENDIF

        IF PassedChar = CHAR_TREVOR

            #if USE_CLF_DLC
                SWITCH g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF  
            #if USE_NRM_DLC
                SWITCH g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF  
            #if not USE_SP_DLC
                SWITCH g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer
            #ENDIF
            
                CASE 1    RETURN 5     BREAK//Light Blue Theme from Settings Menu
                CASE 2    RETURN 6     BREAK//Green
                CASE 3    RETURN 1     BREAK//Red
                CASE 4    RETURN 0     BREAK//Orange
                CASE 5    RETURN 2     BREAK//Grey
                CASE 6    RETURN 3     BREAK//Purple
                CASE 7    RETURN 4     BREAK//Pink
                
                DEFAULT
                    #if IS_DEBUG_BUILD
                        cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Weird Trevor CASE. Returning -1")
                        cdPrintnl()
                    #endif
                    RETURN -1
                BREAK
                
            #if USE_CLF_DLC
                ENDSWITCH
            #ENDIF
            #if USE_NRM_DLC
                ENDSWITCH
            #ENDIF
            #if not USE_SP_DLC
                ENDSWITCH
            #ENDIF

        ENDIF

        #if IS_DEBUG_BUILD
            cdPrintstring("GET_PHONE_SURROUND_TINT_FOR_SP_PLAYER_CHARACTER - Passed Character was not Michael, Franklin or Trevor. Returning -2")
            cdPrintnl()
        #endif

    RETURN -2
ENDFUNC





//2574720
//MP App Presidential Extraction. Need to display two sets of co-ordinates on a Cellphone App. One for a "president" player and the other for the bodyguard.
//Color

PROC SET_EXTRACTION_PRESIDENT_VECTOR (VECTOR YourVector, HUD_COLOURS PassedColourX = HUD_COLOUR_GREEN, HUD_COLOURS PassedColourY = HUD_COLOUR_YELLOW, HUD_COLOURS PassedColourZ = HUD_COLOUR_WHITE)

    g_v_ExtractionPresidentVec = YourVector

    g_hc_PresX = PassedColourX
    g_hc_PresY = PassedColourY
    g_hc_PresZ = PassedColourZ
    
    #if IS_DEBUG_BUILD
        cdPrintstring("Cellphone EXTRACTION_APP - g_v_ExtractionPresidentVec and colours set this frame")
        cdPrintnl()
    #endif

ENDPROC


PROC SET_EXTRACTION_BODYGUARD_VECTOR (VECTOR YourVector, HUD_COLOURS PassedColourX = HUD_COLOUR_WHITE, HUD_COLOURS PassedColourY = HUD_COLOUR_YELLOW, HUD_COLOURS PassedColourZ = HUD_COLOUR_GREEN)

    g_v_ExtractionBodyguardVec = YourVector

    g_hc_BodygX = PassedColourX
    g_hc_BodygY = PassedColourY
    g_hc_BodygZ = PassedColourZ

    #if IS_DEBUG_BUILD
        cdPrintstring("Cellphone EXTRACTION_APP -  g_v_ExtractionBodyguardVec and colours set this frame")
        cdPrintnl()
    #endif

ENDPROC


//THIS WILL BE DEPRECATED.2603763 requested a change in functionality for the Extraction app, that requires two columns in both President and Player view.
//PLEASE CONSIDER using SET_THIS_PLAYER_TO_PRESIDENT_VIEW instead.
//If this is set to true, then the Bodyguard co-ordinates will not display on the Presidential Extraction app. If the player is playing as the President then you'd want this to be set to true.
//By default this is  FALSE. Both co-ordinates will display.
PROC HIDE_EXTRACTION_BODYGUARD_COLUMN (BOOL PassedBool) 

    IF PassedBool = TRUE

        SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW )   


        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone EXTRACTION_APP -  g_v_ExtractionBodyguardVec has been set to HIDDEN - This proc will be deprecated soon!")
            cdPrintnl()
        #endif


    ELSE

        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW )   


        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone EXTRACTION_APP -  g_v_ExtractionBodyguardVec has been set to DISPLAY - This proc will be deprecated soon!")
            cdPrintnl()
        #endif

    ENDIF

ENDPROC





PROC SET_THIS_PLAYER_TO_PRESIDENT_VIEW (BOOL PassedBool) 

    IF PassedBool = TRUE

        SET_BIT (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW )   


        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone EXTRACTION_APP -  SET_THIS_PLAYER_TO_PRESIDENT_VIEW called with TRUE, setting respective bit.")
            cdPrintnl()
        #endif


    ELSE

        CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_SET_EXTRACTION_PRES_VIEW )   


        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone EXTRACTION_APP -  SET_THIS_PLAYER_TO_PRESIDENT_VIEW has been called with FALSE, clearing respective bit.")
            cdPrintnl()
        #endif

    ENDIF

ENDPROC












//2588287  - If the player has the phone open and we need to repopulate the context sensitive bottom right homescreen position with a different app,
//we can use this to repopulate the applist without closing and launching the phone. The bit is actioned upon and reset in cellphone_flashhand.sc
PROC REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST()
    
    IF NOT IS_BIT_SET (BitSet_CellphoneTU, g_BSTU_REQUEST_DYNAMIC_APPLIST_UPDATE)


        IF g_Cellphone.PhoneDS > PDS_TAKEOUT

            #if IS_DEBUG_BUILD
                cdPrintstring("Cellphone_Public REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST has been called.")
                cdPrintnl()
        
                PRINTLN("\nCELLPHONE_PUB - REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
            #endif

        
            SET_BIT (BitSet_CellphoneTU, g_BSTU_REQUEST_DYNAMIC_APPLIST_UPDATE)  //This will signal cellphone_flashhand to perform the more complex mechanics. That script is responsible for resetting the bit.

        ELSE

            #if IS_DEBUG_BUILD
                cdPrintstring("Cellphone_Public REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST called but phone was either already away or in takeout state.")
                cdPrintnl()
        
                PRINTLN("\nCELLPHONE_PUB - REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST called but phone is already away or in takeout state. [", GET_THIS_SCRIPT_NAME(), "] used public function.")
            #endif

        ENDIF
        

    ELSE

        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone_Public REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST has been called but action bit is already set.")
            cdPrintnl()
        
            PRINTLN("\nCELLPHONE_PUB - REQUEST_DYNAMIC_UPDATE_OF_CELLPHONE_APPLIST called whilst action bit is already set by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

    ENDIF

ENDPROC



PROC DISPLAY_GR_BEACON_SIGNIFIER_BESIDE_RADAR()

        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone_Public DISPLAY_GR_BEACON_SIGNIFIER_BESIDE_RADAR() has been called.")
            cdPrintnl()
        
            PRINTLN("\nCELLPHONE_PUB -  DISPLAY_GR_BEACON_SIGNIFIER_BESIDE_RADAR() called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

		SET_BIT (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

ENDPROC




PROC CLEAR_GR_BEACON_SIGNIFIER_FROM_RADAR()

        #if IS_DEBUG_BUILD
            cdPrintstring("Cellphone_Public CLEAR_GR_BEACON_SIGNIFIER_FROM_RADAR() has been called.")
            cdPrintnl()
        
            PRINTLN("\nCELLPHONE_PUB -  CLEAR_GR_BEACON_SIGNIFIER_FROM_RADAR() called by [", GET_THIS_SCRIPT_NAME(), "] through public function.")
        #endif

		CLEAR_BIT (BitSet_CellphoneTU, g_BSTU_BEACON_SIGNIFIER_DISPLAYED)
        SET_BIT (BitSet_CellphoneDisplay_Continued, g_BSC_CELLPHONE_SIGNIFIERS_NEEDS_UPDATED)

ENDPROC





