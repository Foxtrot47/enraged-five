//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   biotech_container_public.sch                                   			//
//      AUTHOR          :   Rob Bray                                                    				//
//      DESCRIPTION     :   Common functions to consistently create container for biotech heist		//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_vehicle.sch" 
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_physics.sch"
USING "commands_entity.sch"

CONST_FLOAT ROPE_LENGTH 6.0

CONST_FLOAT CHOPPER_ROPE_OFFSET_X 0.7
CONST_FLOAT CHOPPER_ROPE_OFFSET_Y 0.7
CONST_FLOAT CHOPPER_ROPE_OFFSET_Z -0.85
CONST_FLOAT CONTAINER_ROPE_OFFSET_X 1.2
CONST_FLOAT CONTAINER_ROPE_OFFSET_Y 1.7
CONST_FLOAT CONTAINER_ROPE_OFFSET_Z 1.4

// obtain the offset to attach the particular rope to the chopper
FUNC VECTOR GET_ROPE_OFFSET_FROM_CHOPPER(INT iRope)
	SWITCH iRope
		CASE 0
			RETURN <<CHOPPER_ROPE_OFFSET_X, CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 1
			RETURN <<-CHOPPER_ROPE_OFFSET_X, CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 2
			RETURN <<CHOPPER_ROPE_OFFSET_X, -CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 3
			RETURN <<-CHOPPER_ROPE_OFFSET_X, -CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Passed in a bad rope to GET_ROPE_OFFSET_FROM_CHOPPER. Use 0-3.")
	RETURN <<0,0,0>>
ENDFUNC

// obtain the offset to attach the rope to the container
FUNC VECTOR GET_ROPE_OFFSET_FROM_CONTAINER(INT iRope)
	SWITCH iRope
		CASE 0
			RETURN <<-CONTAINER_ROPE_OFFSET_X, -CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 1
			RETURN <<CONTAINER_ROPE_OFFSET_X, -CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 2
			RETURN <<-CONTAINER_ROPE_OFFSET_X, CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 3
			RETURN <<CONTAINER_ROPE_OFFSET_X, CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Passed in a bad rope to GET_ROPE_OFFSET_FROM_CONTAINER. Use 0-3.")
	RETURN <<0,0,0>>
ENDFUNC

// delete ropes 
PROC DELETE_BIOTECH_ROPES(ROPE_INDEX &ropes[], BOOL &bRopesCreated, BOOL &bRopesAttached)
	IF bRopesCreated
		INT i 
		REPEAT 4 i 
			DELETE_ROPE(ropes[i])
		ENDREPEAT
	ENDIF
	
	bRopesCreated = FALSE
	bRopesAttached = FALSE
ENDPROC

// create ropes
PROC CREATE_BIOTECH_ROPES(VEHICLE_INDEX &chopperVehicle, ROPE_INDEX &ropes[], BOOL &bRopesCreated, BOOL bTaut = FALSE)
	IF NOT bRopesCreated
		INT i 
		VECTOR vCreatePos 
		IF IS_VEHICLE_DRIVEABLE(chopperVehicle)
			REPEAT 4 i
				vCreatePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chopperVehicle, GET_ROPE_OFFSET_FROM_CHOPPER(i))
				PHYSICS_ROPE_TYPE ropeType
				IF bTaut
					ropeType = PHYSICS_ROPE_THIN
				ELSE
					ropeType = PHYSICS_ROPE_THIN
				ENDIF
				ropes[i] = ADD_ROPE(vCreatePos, <<0,0,0>>, ROPE_LENGTH, ropeType)
			ENDREPEAT
			bRopesCreated = TRUE
		ENDIF
	ENDIF
ENDPROC

// get a heading normalised between 0 and 360
FUNC FLOAT NORMALISE_HEADING(FLOAT fHeading)
	FLOAT fReturn = fHeading
	IF fReturn < 0
		fReturn += 360
	ELIF fReturn > 360
		fReturn -= 360
	ENDIF
	RETURN fReturn
ENDFUNC

// attach chopper to container with rope
PROC ATTACH_CHOPPER_TO_CONTAINER_WITH_ROPE(VEHICLE_INDEX &chopperVehicle, OBJECT_INDEX &containerObject, ROPE_INDEX &ropes[], BOOL &bRopesCreated, BOOL &bRopesAttached, BOOL bPin = FALSE, BOOL bTaut = FALSE, bool force_ropes_to_create = false)
	IF NOT bRopesCreated
		CREATE_BIOTECH_ROPES(chopperVehicle, ropes, bRopesCreated, bTaut)
	ENDIF
	
	IF bRopesCreated
		IF NOT bRopesAttached
			INT i 
			VECTOR vChopperAttachPos
			VECTOR vContainerAttachPos
			IF IS_VEHICLE_DRIVEABLE(chopperVehicle)
			AND DOES_ENTITY_EXIST(containerObject)
				IF DOES_ENTITY_HAVE_PHYSICS(containerObject)
				OR bPin
				or force_ropes_to_create
				
					DETACH_ENTITY(containerObject, false, false)
					REPEAT 4 i
						vChopperAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chopperVehicle, GET_ROPE_OFFSET_FROM_CHOPPER(i))
						vContainerAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(containerObject, GET_ROPE_OFFSET_FROM_CONTAINER(i))
						IF bPin
							//DETACH_ROPE_FROM_ENTITY(ropes[i], containerObject)
							PIN_ROPE_VERTEX(ropes[i], 0, vChopperAttachPos)
							PIN_ROPE_VERTEX(ropes[i], GET_ROPE_VERTEX_COUNT(ropes[i])-1, vContainerAttachPos)
						ELSE
							UNPIN_ROPE_VERTEX(ropes[i], 0)
							UNPIN_ROPE_VERTEX(ropes[i], GET_ROPE_VERTEX_COUNT(ropes[i])-1)
							ATTACH_ENTITIES_TO_ROPE(ropes[i], containerObject, chopperVehicle, vContainerAttachPos, vChopperAttachPos, ROPE_LENGTH, 0, 0)
						ENDIF
					ENDREPEAT
					
					IF NOT bPin
						SET_OBJECT_PHYSICS_PARAMS(containerObject, 1000, 1.0,  <<0.02, 0.02, 0.003>>, <<0.02, 0.4, 0.4>>) //old mass = 1600
						//APPLY_FORCE_TO_ENTITY(containerObject, APPLY_TYPE_EXTERNAL_IMPULSE, <<0,0,0.1>>, <<0,0,0>>, 0, FALSE, TRUE, TRUE)
						set_entity_dynamic(containerObject, true)
					ENDIF
					bRopesAttached = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// maintain rope pin attachment while container on ground
PROC MAINTAIN_ROPE_PIN_ATTACHMENT(VEHICLE_INDEX &chopperVehicle, ROPE_INDEX &ropes[])
	INT i 
	VECTOR vChopperAttachPos
	IF IS_VEHICLE_DRIVEABLE(chopperVehicle)
		REPEAT 4 i
			vChopperAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chopperVehicle, GET_ROPE_OFFSET_FROM_CHOPPER(i))
			PIN_ROPE_VERTEX(ropes[i], 0, vChopperAttachPos)
		ENDREPEAT
	ENDIF
ENDPROC

// maintain rope pin attachment to container while attached to a vehicle
PROC MAINTAIN_ROPE_PIN_ATTACHMENT_TO_VEHICLE(VEHICLE_INDEX &pinVehicle, ROPE_INDEX &ropes[], VECTOR vContainerOffsetFromVehicle)
	INT i 
	VECTOR vContainerAttachPos
	IF IS_VEHICLE_DRIVEABLE(pinVehicle)
		REPEAT 4 i
			vContainerAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pinVehicle, GET_ROPE_OFFSET_FROM_CONTAINER(i) + vContainerOffsetFromVehicle)
			PIN_ROPE_VERTEX(ropes[i], GET_ROPE_VERTEX_COUNT(ropes[i])-1, vContainerAttachPos)
		ENDREPEAT
	ENDIF
ENDPROC

// unpin rope ends
PROC UNPIN_ROPE_ENDS(ROPE_INDEX &ropes[], BOOL bUnpinFirst, BOOL bUnpinLast)
	INT i 
	REPEAT 4 i
		IF bUnpinFirst
			UNPIN_ROPE_VERTEX(ropes[i], 0)
		ENDIF
		
		IF bUnpinLast
			UNPIN_ROPE_VERTEX(ropes[i], GET_ROPE_VERTEX_COUNT(ropes[i])-1)
		ENDIF
	ENDREPEAT
ENDPROC
