//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	building_control_public.sch									//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Contains all the public funtions for the building and door	//
//							states.														//
//							This deals with both single and multi-player data.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "building_control_private.sch"

PROC REMOVE_IPL_WHEN_SCREEN_IS_FADED_OUT(STRING strIPLName)
	
	// Remove straight away if the screen is faded out
	IF IS_SCREEN_FADED_OUT()
		IF IS_IPL_ACTIVE(strIPLName)
			REMOVE_IPL(strIPLName)
		ENDIF
		
	// Otherwise add to a list and let the building controller pick it up
	ELSE
		INT iIPL
		INT iIPLHash = GET_HASH_KEY(strIPLName)
		INT iIPLRepeatHash
		BOOL bAddedToList = FALSE
		INT iFreeSlot = -1
		REPEAT MAX_IPLS_TO_TRACK_FOR_REMOVAL iIPL
		
			iIPLRepeatHash = GET_HASH_KEY(g_tlIPLsToRemoveOnFadeOut[iIPL])
			
			IF iIPLRepeatHash = 0
				iFreeSlot = iIPL
			ENDIF
			
			IF iIPLRepeatHash = iIPLHash
				// Already added to list so ignore.
				bAddedToList = TRUE
			ENDIF
		ENDREPEAT
		
		IF NOT bAddedToList
		AND iFreeSlot != -1
			g_tlIPLsToRemoveOnFadeOut[iFreeSlot] = strIPLName
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_IPL_FROM_REMOVAL_LIST(STRING strIPLName)
	
	INT iIPL
	INT iIPLHash = GET_HASH_KEY(strIPLName)
	REPEAT MAX_IPLS_TO_TRACK_FOR_REMOVAL iIPL
		IF GET_HASH_KEY(g_tlIPLsToRemoveOnFadeOut[iIPL]) = iIPLHash
			g_tlIPLsToRemoveOnFadeOut[iIPL] = ""
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Add an ipl to a global array which can then be removed when no load scene is active
PROC ADD_IPL_WHEN_NO_LOAD_SCENE_IS_ACTIVE(STRING strIPLName)
	
	// Add straight away if no load scene is active
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		IF NOT IS_IPL_ACTIVE(strIPLName)
			REQUEST_IPL(strIPLName)
		ENDIF
	
	// Otherwise add to a list and let the building controller pick it up
	ELSE
		INT iIPL
		INT iIPLHash = GET_HASH_KEY(strIPLName)
		INT iIPLRepeatHash
		BOOL bAddedToList = FALSE
		INT iFreeSlot = -1
		REPEAT MAX_IPLS_TO_TRACK_FOR_ADD iIPL
		
			iIPLRepeatHash = GET_HASH_KEY(g_tlIPLsToAddOnNoLoadScene[iIPL])
			
			IF iIPLRepeatHash = 0
				iFreeSlot = iIPL
			ENDIF
			
			IF iIPLRepeatHash = iIPLHash
				// Already added to list so ignore.
				bAddedToList = TRUE
			ENDIF
		ENDREPEAT
		
		IF NOT bAddedToList
		AND iFreeSlot != -1
			g_tlIPLsToAddOnNoLoadScene[iFreeSlot] = strIPLName
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Updates the state of the specified door enum
///    
/// PARAMS:
///    eDoor - The door enum set up in building_globals.sch
///    eNewState - The state that we want to change the door to
PROC SET_DOOR_STATE(DOOR_NAME_ENUM eName, DOOR_STATE_ENUM eNewState)
	IF eName <> DUMMY_DOORNAME
	
		// Grab the current saved state
		DOOR_STATE_ENUM eCurrentState
		IF g_bInMultiplayer
			#IF USE_SP_DLC				eCurrentState = MPGlobals_OLD.g_MPBuildingData.eDoorState[eName]	#ENDIF
			#IF NOT USE_SP_DLC
				#IF USE_TU_CHANGES		eCurrentState = g_MPBuildingData.eDoorState[eName]		#ENDIF
				#IF NOT USE_TU_CHANGES	eCurrentState = MPGlobals_OLD.g_MPBuildingData.eDoorState[eName]	#ENDIF
			#ENDIF
		ELSE
			#if USE_CLF_DLC
				eCurrentState = g_savedGlobalsClifford.sBuildingData.eDoorState[eName]
			#endif
			#if USE_NRM_DLC
				eCurrentState = g_savedGlobalsnorman.sBuildingData.eDoorState[eName]
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				eCurrentState = g_savedGlobals.sBuildingData.eDoorState[eName]
			#endif
			#endif
		ENDIF
		
		// Only perform any updates if the current state differs from the new state
		IF eCurrentState <> eNewState
		OR IS_BIT_SET(g_iUpdateDoorStateBitset[ENUM_TO_INT(eName)/32], (ENUM_TO_INT(eName)%32))
			// Set the saved state
			IF eNewState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
			OR eNewState = DOORSTATE_FORCE_UNLOCKED_THIS_FRAME
			OR eNewState = DOORSTATE_FORCE_OPEN_THIS_FRAME
			OR eNewState = DOORSTATE_FORCE_CLOSED_THIS_FRAME
			OR eNewState = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
				SET_BIT(g_iOverrideDoorStateBitset[ENUM_TO_INT(eName)/32], ENUM_TO_INT(eName)%32)
				g_eOverrideDoorState[eName] = eNewState
			ELSE
				IF g_bInMultiplayer
					#IF USE_SP_DLC				MPGlobals_OLD.g_MPBuildingData.eDoorState[eName] = eNewState	#ENDIF
					#IF NOT USE_SP_DLC
						#IF USE_TU_CHANGES		g_MPBuildingData.eDoorState[eName] = eNewState		#ENDIF
						#IF NOT USE_TU_CHANGES	MPGlobals_OLD.g_MPBuildingData.eDoorState[eName] = eNewState	#ENDIF
					#ENDIF
				ELSE
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sBuildingData.eDoorState[eName] = eNewState
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sBuildingData.eDoorState[eName] = eNewState
					#endif
					
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sBuildingData.eDoorState[eName] = eNewState
					#endif
					#endif
					
				ENDIF
			ENDIF
			
			SET_BIT(g_iUpdateDoorStateBitset[ENUM_TO_INT(eName)/32], (ENUM_TO_INT(eName)%32))
			PERFORM_DOOR_LOCK(eName)
			
			// If we failed to set it's state, try again the following frame.
			IF IS_BIT_SET(g_iUpdateDoorStateBitset[ENUM_TO_INT(eName)/32], (ENUM_TO_INT(eName)%32))
				QUEUE_DOOR_UPDATE_FOR_NEXT_FRAME(eName)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BUILDING_STATE_ENUM GET_BUILDING_STATE(BUILDING_NAME_ENUM eName)
	IF eName <> DUMMY_BUILDINGNAME
	
		IF g_bInMultiplayer
			#IF USE_TU_CHANGES			RETURN g_MPBuildingData.eBuildingState[eName]		#ENDIF
			#IF NOT USE_TU_CHANGES		RETURN MPGlobals_OLD.g_MPBuildingData.eBuildingState[eName]		#ENDIF
		ELSE
		
			#if USE_CLF_DLC
				RETURN g_savedGlobalsClifford.sBuildingData.eBuildingState[eName]
			#endif
			#if USE_NRM_DLC
				RETURN g_savedGlobalsnorman.sBuildingData.eBuildingState[eName]
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				RETURN g_savedGlobals.sBuildingData.eBuildingState[eName]
			#endif
			#endif
		ENDIF
	ENDIF
	
	RETURN BUILDINGSTATE_NORMAL
ENDFUNC

/// PURPOSE: Updates the state of the specified building enum
///    
/// PARAMS:
///    eBuilding - The building enum set up in building_globals.sch
///    eNewState - The state that we want to change the building to
PROC SET_BUILDING_STATE(BUILDING_NAME_ENUM eName, BUILDING_STATE_ENUM eNewState, BOOL bWaitForPlayerToLeaveArea = FALSE, BOOL bRefreshInterior = TRUE, BOOL bIgnorePlayerDeathCheck = FALSE)
	IF eName <> DUMMY_BUILDINGNAME
	
		CPRINTLN(DEBUG_BUILDING, "SET_BUILDING_STATE(", eName, ", ", eNewState, ", ", bWaitForPlayerToLeaveArea, ", ", bRefreshInterior, ") called by ", GET_THIS_SCRIPT_NAME(), ".")
	
		IF g_bInMultiplayer
			#IF USE_SP_DLC			MPGlobals_OLD.g_MPBuildingData.eBuildingState[eName] = eNewState	#ENDIF
			#IF NOT USE_SP_DLC
				#IF USE_TU_CHANGES		g_MPBuildingData.eBuildingState[eName] = eNewState		#ENDIF
				#IF NOT USE_TU_CHANGES	MPGlobals_OLD.g_MPBuildingData.eBuildingState[eName] = eNewState	#ENDIF
			#ENDIF
		ELSE
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sBuildingData.eBuildingState[eName] = eNewState
			#endif
			#if USE_NRM_DLC
				g_savedGlobalsnorman.sBuildingData.eBuildingState[eName] = eNewState
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				g_savedGlobals.sBuildingData.eBuildingState[eName] = eNewState
			#endif
			#endif
		ENDIF
		
		g_bBuildingStateUpdateDelay[eName] = bWaitForPlayerToLeaveArea
		
		// Set the global flag so the building_controller can pick
		// it up if we fail to perfrom the state change this frame.
		g_bUpdateBuildingState[eName] = TRUE
		
		PERFORM_BUILDING_SWAP(eName, bRefreshInterior, bIgnorePlayerDeathCheck)
		
		SET_BUILDING_STATE_ASSOCIATED_FEATURES( eName, eNewState )
		
	ENDIF
ENDPROC

/// PURPOSE: Force the buidling controller to re-apply any road node blocking areas.
PROC UPDATE_ALL_BUILDING_STATE_BLOCKS()
	INT i
	REPEAT NUMBER_OF_BUILDINGS i
		g_bBuildingRoadNodeBlockUpdate[i] = TRUE
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Returns whether the given interior is a road tunnel (shouldn't clear shop blips while inside)
///    POPULATE_ROAD_TUNNEL_INTERIOR_INDEX_LIST() must be called in order to use this.
FUNC BOOL IS_INTERIOR_A_ROAD_TUNNEL(INTERIOR_INSTANCE_INDEX intIdx)
	INT iIndex
	REPEAT gRoadTunnelCount iIndex
		IF g_intRoadTunnels[iIndex] = intIdx
		
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Manually populates the list of road tunnel interior indices for checking if an interior is a road tunnel
PROC POPULATE_ROAD_TUNNEL_INTERIOR_INDEX_LIST()
	g_intRoadTunnels[0] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1190.58, -685.387, 11.0753 >>,"sm20_tun4")
	g_intRoadTunnels[1] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1292.36, -730.629, 11.0934 >>,"sm20_tun3")
	g_intRoadTunnels[2] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1411.97, -759.518, 15.5455 >>,"sm20_tun2")
	g_intRoadTunnels[3] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1531.56, -762.391, 15.3451 >>,"sm20_tun1")
	g_intRoadTunnels[4] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -573.473, -580.793, 25.3082 >>,"kt1_04_roadtunnel_int")
	g_intRoadTunnels[5] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -675.628, -606.272, 25.3078 >>,"kt1_03_carpark_int")
	g_intRoadTunnels[6] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 274.441, -636.403, 29.0854 >>,"dt1_rd1_tun3")
	g_intRoadTunnels[7] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 142.198, -581.121, 31.2974 >>,"dt1_rd1_tun2")
	g_intRoadTunnels[8] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 204.925, -601.567, 29.3757 >>,"dt1_rd1_tun")
	g_intRoadTunnels[9] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2159.62, 5995.87, 51.2999 >>,"cs2_roadsb_tunnel_03")
	g_intRoadTunnels[10] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2250.3, 5915.56, 49.6273 >>,"cs2_roadsb_tunnel_02")
	g_intRoadTunnels[11] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2341.18, 5814.9, 46.7075 >>,"cs2_roadsb_tunnel_01")
	g_intRoadTunnels[12] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -183.656, 4664.52, 130.5 >>,"cs1_12_tunnel03_int")
	g_intRoadTunnels[13] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -273.86, 4752.12, 138.21 >>,"cs1_12_tunnel02_int")
	g_intRoadTunnels[14] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -410.302, 4860.98, 144.864 >>,"cs1_12_tunnel01_int")
	g_intRoadTunnels[15] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -2596.83, 3088.87, 15.4225 >>,"ch1_roadsdint_tun2")
	g_intRoadTunnels[16] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -2583.04, 3268.07, 13.3157 >>,"ch1_roadsdint_tun1")
	g_intRoadTunnels[17] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -179.51, -180.189, 43.6251 >>,"bt1_04_carpark")
ENDPROC

/// PURPOSE:
///    Returns whether the given interior is a tunnel (shouldn't clear shop blips while inside)
/// RETURNS:
///    
FUNC BOOL IS_INTERIOR_A_TUNNEL(INTERIOR_INSTANCE_INDEX intIdx)
	int i = 0
	FOR i = 0 to gTunnelCount -1
		IF g_intTunnels[i] = intIdx
	
			return TRUE
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC
	
/// PURPOSE:
///    Manually populates the list of tunnel interior indices for checking if an interior is an open-air tunnel
PROC POPULATE_TUNNEL_INTERIOR_INDEX_LIST()
	g_intTunnels[0] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1680.49, -929.44, -0.462531 >>,"vbca_tunnel1")
	g_intTunnels[1] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1562.04, -876.91, -0.471913 >>,"vbca_tunnel2")
	g_intTunnels[2] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1429.65, -823.211, -0.432763 >>,"vbca_tunnel3")
	g_intTunnels[3] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1316.9, -843.515, 1.43639 >>,"vbca_tunnel4")
	g_intTunnels[4] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1249.67, -896.27, 0.293292 >>,"vbca_tunnel5")
	g_intTunnels[5] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -38.9818, -570.534, 28.4812 >>,"v_31_tun_01")
	g_intTunnels[6] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 481.908, -577.602, 2.41908 >>,"v_31_newtunnel1")
	g_intTunnels[7] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1029.02, -260.955, 48.2681 >>,"v_31_newtun5")
	g_intTunnels[8] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1022.25, -205.648, 42.8956 >>,"v_31_newtun4b")
	g_intTunnels[9] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 823.852, -299.823, 4.54864 >>,"v_31_newtun3")
	g_intTunnels[0] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 615.395, -409.282, -1.57599 >>,"v_31_newtun2")
	g_intTunnels[11] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1190.58, -685.387, 11.0753 >>,"sm20_tun4")
	g_intTunnels[12] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1292.36, -730.629, 11.0934 >>,"sm20_tun3")
	g_intTunnels[13] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1411.97, -759.518, 15.5455 >>,"sm20_tun2")
	g_intTunnels[14] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -1531.56, -762.391, 15.3451 >>,"sm20_tun1")
	g_intTunnels[15] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 558.653, -1486.49, 21.4096 >>,"sc1_rd_inttunshort")
	g_intTunnels[16] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 569.673, -1920.17, 21.1009 >>,"sc1_rd_inttun3b_end")
	g_intTunnels[17] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 512.412, -1908.55, 21.2086 >>,"sc1_rd_inttun3b")
	g_intTunnels[18] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 434.343, -1945.27, 17.3936 >>,"sc1_rd_inttun3")
	g_intTunnels[19] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 513.914, -2009.82, 21.1486 >>,"sc1_rd_inttun2b_end")
	g_intTunnels[20] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 465.139, -2025.49, 19.4406 >>,"sc1_rd_inttun2b")
	g_intTunnels[21] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 406.938, -1978.14, 16.3512 >>,"sc1_rd_inttun2")
	g_intTunnels[22] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 319.321, -1896.97, 22.4086 >>,"sc1_rd_inttun1")
	g_intTunnels[23] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -573.473, -580.793, 25.3082 >>,"kt1_04_roadtunnel_int")
	g_intTunnels[24] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -675.628, -606.272, 25.3078 >>,"kt1_03_carpark_int")
	g_intTunnels[25] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1314.63, -904.059, 53.0877 >>,"id2_21_a_tun5")
	g_intTunnels[26] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1248.64, -773.6, 44.5493 >>,"id2_21_a_tun4")
	g_intTunnels[27] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1213.45, -597.799, 37.7533 >>,"id2_21_a_tun3")
	g_intTunnels[28] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 1083.25, -502.542, 34.6573 >>,"id2_21_a_tun2")
	g_intTunnels[29] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 926.428, -488.737, 33.8564 >>,"id2_21_a_tun1")
	g_intTunnels[30] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 732.663, -2486.45, 11.0686 >>,"id1_11_tunnel8_int")
	g_intTunnels[31] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 749.494, -2364.79, 16.2255 >>,"id1_11_tunnel7_int")
	g_intTunnels[32] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 758.957, -2260.08, 23.4637 >>,"id1_11_tunnel6_int")
	g_intTunnels[33] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 769.889, -2124.24, 21.8223 >>,"id1_11_tunnel5_int")
	g_intTunnels[34] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 789.138, -1963.58, 20.6408 >>,"id1_11_tunnel4_int")
	g_intTunnels[35] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 815.852, -1832.2, 22.9671 >>,"id1_11_tunnel3_int")
	g_intTunnels[36] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 829.752, -1718.51, 20.4594 >>,"id1_11_tunnel2_int")
	g_intTunnels[37] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 829.845, -1718.51, 20.1823 >>,"id1_11_tunnel1_int")
	g_intTunnels[38] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 274.441, -636.403, 29.0854 >>,"dt1_rd1_tun3")
	g_intTunnels[39] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 142.198, -581.121, 31.2974 >>,"dt1_rd1_tun2")
	g_intTunnels[40] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 204.925, -601.567, 29.3757 >>,"dt1_rd1_tun")
	g_intTunnels[41] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2571.05, 3907.95, 41.1896 >>,"cs4_rwayb_tunnelint")
	g_intTunnels[42] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -493.613, 4275.55, 89.1677 >>,"cs3_03railtunnel_int4")
	g_intTunnels[43] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -476.018, 4201.55, 87.9392 >>,"cs3_03railtunnel_int3")
	g_intTunnels[44] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -457.748, 4125.51, 86.1208 >>,"cs3_03railtunnel_int2")
	g_intTunnels[45] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -442.948, 4064.86, 84.1041 >>,"cs3_03railtunnel_int1")
	g_intTunnels[46] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2159.62, 5995.87, 51.2999 >>,"cs2_roadsb_tunnel_03")
	g_intTunnels[47] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2250.3, 5915.56, 49.6273 >>,"cs2_roadsb_tunnel_02")
	g_intTunnels[48] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 2341.18, 5814.9, 46.7075 >>,"cs2_roadsb_tunnel_01")
	g_intTunnels[49] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -537.422, 4613.09, 89.7512 >>,"cs1_14brailway6")
	g_intTunnels[50] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -540.452, 4719.74, 89.7576 >>,"cs1_14brailway5")
	g_intTunnels[51] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -543.783, 4821.95, 89.7357 >>,"cs1_14brailway4")
	g_intTunnels[52] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -546.036, 4923.02, 89.8919 >>,"cs1_14brailway3")
	g_intTunnels[53] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -546.265, 4999.07, 90.8104 >>,"cs1_14brailway2")
	g_intTunnels[54] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -539.261, 5077.03, 91.6235 >>,"cs1_14brailway1")
	g_intTunnels[55] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -183.656, 4664.52, 130.5 >>,"cs1_12_tunnel03_int")
	g_intTunnels[56] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -273.86, 4752.12, 138.21 >>,"cs1_12_tunnel02_int")
	g_intTunnels[57] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -410.302, 4860.98, 144.864 >>,"cs1_12_tunnel01_int")
	g_intTunnels[58] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -2596.83, 3088.87, 15.4225 >>,"ch1_roadsdint_tun2")
	g_intTunnels[59] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -2583.04, 3268.07, 13.3157 >>,"ch1_roadsdint_tun1")
	g_intTunnels[60] = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -179.51, -180.189, 43.6251 >>,"bt1_04_carpark")

ENDPROC

#IF FEATURE_FOW_EXPANSION
PROC CONFIGURE_FOW_FOR_SP()
	CPRINTLN(DEBUG_BUILDING, "[FOW] CONFIGURE_FOW_FOR_SP() called by script ", GET_THIS_SCRIPT_NAME())
	
	SET_MINIMAP_FOW_DO_NOT_UPDATE(FALSE)
	ENABLE_MINIMAP_FOW_CUSTOM_WORLD_EXTENTS(FALSE)
ENDPROC

PROC CONFIGURE_FOW_FOR_MP_MAIN()
	CPRINTLN(DEBUG_BUILDING, "[FOW] CONFIGURE_FOW_FOR_MP_MAIN() called by script ", GET_THIS_SCRIPT_NAME())
	
	ENABLE_MINIMAP_FOW_CUSTOM_WORLD_EXTENTS(TRUE)
	SET_MINIMAP_FOW_CUSTOM_WORLD_POS_AND_SIZE(FOW_ISLAND_MAP_OFFSET_X, FOW_ISLAND_MAP_OFFSET_Y, FOW_MAP_WIDTH, FOW_MAP_HEIGHT)
	
	SET_MINIMAP_ALLOW_FOW_IN_MP(FALSE)
	SET_MINIMAP_FOW_DO_NOT_UPDATE(TRUE)
ENDPROC

PROC CONFIGURE_FOW_FOR_MP_ISLAND()
	CPRINTLN(DEBUG_BUILDING, "[FOW] CONFIGURE_FOW_FOR_MP_ISLAND() called by script ", GET_THIS_SCRIPT_NAME())

	SET_MINIMAP_ALLOW_FOW_IN_MP(TRUE)
	SET_MINIMAP_FOW_DO_NOT_UPDATE(FALSE)
	
	ENABLE_MINIMAP_FOW_CUSTOM_WORLD_EXTENTS(TRUE)
	SET_MINIMAP_FOW_CUSTOM_WORLD_POS_AND_SIZE(FOW_ISLAND_MAP_OFFSET_X, FOW_ISLAND_MAP_OFFSET_Y, FOW_MAP_WIDTH, FOW_MAP_HEIGHT)
	
	// Initialise the mp save data if we haven't done so already
	INT minX, minY, maxX, maxY, fillValueForRestOfMap
	IF NOT GET_MINIMAP_FOW_MP_SAVE_DETAILS(minX, minY, maxX, maxY, fillValueForRestOfMap)
		
		
		// If we need to adjust save details in future we can compare our new data with the data returned from GET_MINIMAP_FOW_MP_SAVE_DETAILS.
		// The grid points that were previously saved will remain intact if they are still within the bounds.
		
		// Full map for now, we will reduce this to only occupy the Island Heist map.
		SET_MINIMAP_FOW_MP_SAVE_DETAILS(TRUE, FOW_MP_SAVE_MIN_X, FOW_MP_SAVE_MIN_Y, FOW_MP_SAVE_MAX_X, FOW_MP_SAVE_MAX_Y, FOW_MP_SAVE_FILL)
		SET_MINIMAP_REQUEST_CLEAR_FOW(TRUE)
		CPRINTLN(DEBUG_BUILDING, "[FOW] CONFIGURE_FOW_FOR_MP_ISLAND() resetting for first time use")
	ENDIF
ENDPROC
#ENDIF // FEATURE_FOW_EXPANSION

