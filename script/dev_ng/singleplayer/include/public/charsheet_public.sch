//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   charsheet_public.sc                                         //
//      AUTHOR          :   Steve Taylor                                                //
//      DESCRIPTION     :   Primarily initialises charsheet                             //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "script_misc.sch"

/// PURPOSE:
///    Checks if the given character is valid in the character sheet
/// RETURNS:
///    TRUE if the character is <= MAX_CHARACTERS_PLUS_DUMMY
FUNC BOOL GLOBAL_CHARACTER_SHEET_IS_A_VALID_CHARACTER(enumCharacterList eCharacter)
	IF eCharacter > MAX_CHARACTERS_PLUS_DUMMY
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the given character is valid in the character sheet and asserts if it isn't
/// RETURNS:
///    TRUE if the character is <= MAX_CHARACTERS_PLUS_DUMMY, asserts and returns FALSE if not
FUNC BOOL GLOBAL_CHARACTER_SHEET_VALIDATE_CHARACTER(enumCharacterList eCharacter)
	IF NOT GLOBAL_CHARACTER_SHEET_IS_A_VALID_CHARACTER(eCharacter)
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_VALIDATE_CHARACTER - invalid character: ", ENUM_TO_INT(eCharacter))
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

///--------------------------------
///    SETTERS
///--------------------------------

/// PURPOSE:
///    Sets the characters game model ensuring data is stored in the correct array
///    for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    eModel - the model to use
PROC GLOBAL_CHARACTER_SHEET_SET_GAME_MODEL(enumCharacterList eCharacter, MODEL_NAMES eModel)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_GAME_MODEL - setting character:",
	ENUM_TO_INT(eCharacter), " to ", ENUM_TO_INT(eModel))

	g_sCharacterSheetAll[eCharacter].game_model = eModel
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_GAME_MODEL - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].game_model = eModel
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///     Sets the characters alpha int (used for ordering) ensuring data is stored in the correct array
///    	for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    iAlphaInt - the alpha int order to use
PROC GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(enumCharacterList eCharacter, int iAlphaInt)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT - setting character:",
	ENUM_TO_INT(eCharacter), " to ", iAlphaInt)

	g_sCharacterSheetAll[eCharacter].alphaint = iAlphaInt
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].alphaint = iAlphaInt
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///     Sets the characters original alpha int (used for ordering) ensuring data is stored in the correct array
///    	for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    iOriginalAlphaInt - the original alpha int order to use
PROC GLOBAL_CHARACTER_SHEET_SET_ORIGINAL_ALPHA_INT(enumCharacterList eCharacter, int iOriginalAlphaInt)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_ORIGINAL_ALPHA_INT - setting character:",
	ENUM_TO_INT(eCharacter), " to ", iOriginalAlphaInt)

	g_sCharacterSheetAll[eCharacter].original_alphaint = iOriginalAlphaInt
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_ORIGINAL_ALPHA_INT - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].original_alphaint = iOriginalAlphaInt
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///     Sets the characters original alpha int (used for ordering) ensuring data is stored in the correct array
///    	for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    strLabel - the label string to use
PROC GLOBAL_CHARACTER_SHEET_SET_LABEL(enumCharacterList eCharacter, STRING strLabel)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_LABEL - setting character:",
	ENUM_TO_INT(eCharacter), " to ", strLabel)
	
	g_sCharacterSheetAll[eCharacter].label = strLabel
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_LABEL - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].label = strLabel
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters picture ensuring data is stored in the correct array
///    for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    strPicture - the texture name of the picture to use
PROC GLOBAL_CHARACTER_SHEET_SET_PICTURE(enumCharacterList eCharacter, STRING strPicture)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PICTURE - setting character:",
	ENUM_TO_INT(eCharacter), " to ", strPicture)
	
	g_sCharacterSheetAll[eCharacter].picture = strPicture
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PICTURE - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].picture = strPicture
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters friend ensuring data is stored in the correct array
///    for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    eFriend - the friend to set
PROC GLOBAL_CHARACTER_SHEET_SET_FRIEND(enumCharacterList eCharacter, enumFriend eFriend)	
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_FRIEND - setting character:",
	ENUM_TO_INT(eCharacter), " to ", ENUM_TO_INT(eFriend))
	
	g_sCharacterSheetAll[eCharacter].friend = eFriend
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_FRIEND - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].friend = eFriend
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters phonebook state ensuring data is stored in the correct array
///    for the gamemode (SP or MP)
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    iCharacterPhonebookID - the id of the phonebooks owner 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer (can't use enum due to legacy code)
///    ePhoneBookState - the new state
PROC GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(enumCharacterList eCharacter, INT iCharacterPhonebookID, enumIndividualPhoneBookState ePhoneBookState)
	IF iCharacterPhonebookID < 0 OR iCharacterPhonebookID > CHARACTER_SHEET_MAX_PLAYER_ID
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE invalid phonebook id")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF
	
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE - setting character:",
	ENUM_TO_INT(eCharacter), " phonebook ", iCharacterPhonebookID, " to ", ENUM_TO_INT(ePhoneBookState))
	
	g_sCharacterSheetAll[eCharacter].PhoneBookState[iCharacterPhonebookID] = ePhoneBookState
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].PhoneBookState[iCharacterPhonebookID] = ePhoneBookState
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters bank account ensuring data is stored in the correct array
///    for the gamemode (SP or MP)  
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    eBankAccount - the bankaccount name to set
PROC GLOBAL_CHARACTER_SHEET_SET_BANK_ACCOUNT(enumCharacterList eCharacter, enumBankAccountName eBankAccount)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_BANK_ACCOUNT - setting character:",
	ENUM_TO_INT(eCharacter), " to ", ENUM_TO_INT(eBankAccount))
	
	g_sCharacterSheetAll[eCharacter].bank_account = eBankAccount
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_BANK_ACCOUNT - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].bank_account = eBankAccount
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters picture message ensuring data is stored in the correct array
///    for the gamemode (SP or MP)  
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    ePicMsgStatus - the picture message status to set
PROC GLOBAL_CHARACTER_SHEET_SET_PICTURE_MESSAGE_STATUS(enumCharacterList eCharacter, enumPicMsgStatus ePicMsgStatus)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PICTURE_MESSAGE_STATUS - setting character:",
	ENUM_TO_INT(eCharacter), " to ", ENUM_TO_INT(ePicMsgStatus))
	
	g_sCharacterSheetAll[eCharacter].picmsgStatus = ePicMsgStatus
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_PICTURE_MESSAGE_STATUS - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].picmsgStatus = ePicMsgStatus
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters missed call status ensuring data is stored in the correct array
///    for the gamemode (SP or MP)  
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    iPhoneOwnerID - the id of the phone owner 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer (can't use enum due to legacy code)
///    eMissedCallStatus - the new missed call status to set
PROC GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS(enumCharacterList eCharacter, INT iPhoneOwnerID, enumMissedCallStatus eMissedCallStatus)
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS - setting character:",
	ENUM_TO_INT(eCharacter), " phone owner ", iPhoneOwnerID, " to ", ENUM_TO_INT(eMissedCallStatus))
	
	g_sCharacterSheetAll[eCharacter].missedCallStatus[iPhoneOwnerID] = eMissedCallStatus
	
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].missedCallStatus[iPhoneOwnerID] = eMissedCallStatus
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets the characters status as a caller ensuring data is stored in the correct array
///    for the gamemode (SP or MP)   
/// PARAMS:
///    eCharacter - the character to change, asserts if greater than MAX_CHARACTERS_PLUS_DUMMY
///    iPhoneOwnerID -  the id of the phone owner 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer (can't use enum due to legacy code)
///    eStatusAsCaller - the new status as caller to set
PROC GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(enumCharacterList eCharacter, INT iPhoneOwnerID, enumStatusAsCaller eStatusAsCaller)
	IF iPhoneOwnerID < 0 OR iPhoneOwnerID > CHARACTER_SHEET_MAX_PLAYER_ID
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER invalid phone owner id")
		DEBUG_PRINTCALLSTACK()
		EXIT
	ENDIF	
	
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER - setting character:",
	ENUM_TO_INT(eCharacter), " phone owner ", iPhoneOwnerID, " to ", ENUM_TO_INT(eStatusAsCaller))
	
	g_sCharacterSheetAll[eCharacter].statusAsCaller[iPhoneOwnerID] = eStatusAsCaller
		
	IF eCharacter < MAX_CHARACTERS_SP_SAVE
		PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_SET_MISSED_CALL_STATUS - also storing in sp array")
		g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter].statusAsCaller[iPhoneOwnerID] = eStatusAsCaller
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Copies all the saved contacts into the charsheet all array that acts as the main character sheet
///    that contains both singleplayer saved contacts and multiplayer unsaved contacts
PROC GLOBAL_CHARACTER_SHEET_COPY_SAVED_CONTACTS_TO_OVERFLOW_ARRAY()
	PRINTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_COPY_SAVED_CONTACTS_TO_OVERFLOW_ARRAY - copying array")
	INT i
	enumCharacterList eCharacter
	INT iMaxChar = ENUM_TO_INT(MAX_CHARACTERS_SP_SAVE)
	REPEAT iMaxChar i
		eCharacter = INT_TO_ENUM(enumCharacterList, i)
		COPY_SCRIPT_STRUCT(g_sCharacterSheetAll[eCharacter], g_SavedGlobals.sCharSheetData.g_CharacterSheet[eCharacter], SIZE_OF(structCharacterSheet))	
	ENDREPEAT
ENDPROC

///-----------------------------
///    GETTERS
///-----------------------------    

/// PURPOSE:
///    Gets the characters info
/// PARAMS:
///    sCharacterSheet - the struct to fill with the character info
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
PROC GLOBAL_CHARACTER_SHEET_GET(structCharacterSheet &sCharacterSheet, enumCharacterList eCharacter)
	COPY_SCRIPT_STRUCT(sCharacterSheet, g_sCharacterSheetAll[eCharacter], SIZE_OF(structCharacterSheet))
ENDPROC

/// PURPOSE:
///    Gets the characters gamemodel
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters gamemodel, asserts and returns DUMMY_MODEL_FOR_SCRIPT if eCharacter is invalid
FUNC MODEL_NAMES GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(enumCharacterList eCharacter)	
	RETURN g_sCharacterSheetAll[eCharacter].game_model
ENDFUNC

/// PURPOSE:
///    Gets the characters alpha int that i used for alphabetical ordering
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters alpha int that i used for alphabetical ordering, asserts and returns -1 if eCharacter is invalid
FUNC INT GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(enumCharacterList eCharacter)	
	RETURN g_sCharacterSheetAll[eCharacter].alphaint
ENDFUNC

/// PURPOSE:
///    Gets the characters original alpha int that i used for alphabetical ordering
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters original alpha int that i used for alphabetical ordering, asserts and returns -1 if eCharacter is invalid
FUNC INT GLOBAL_CHARACTER_SHEET_GET_ORIGINAL_ALPHA_INT(enumCharacterList eCharacter)
	RETURN g_sCharacterSheetAll[eCharacter].original_alphaint
ENDFUNC

/// PURPOSE:
///    Gets the characters label
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The charcters label, asserts and  returns "" if eCharacter is invalid
FUNC TEXT_LABEL GLOBAL_CHARACTER_SHEET_GET_LABEL(enumCharacterList eCharacter)	
	RETURN g_sCharacterSheetAll[eCharacter].label
ENDFUNC

/// PURPOSE:
///    Gets the characters picture
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters picture, asserts and returns "" if eCharacter is invalid
FUNC TEXT_LABEL GLOBAL_CHARACTER_SHEET_GET_PICTURE(enumCharacterList eCharacter)
	RETURN g_sCharacterSheetAll[eCharacter].picture
ENDFUNC

/// PURPOSE:
///    Gets the characters friend 
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters friend, asserts and returns NO_FRIEND if eCharacter is invalid
FUNC enumFriend GLOBAL_CHARACTER_SHEET_GET_FRIEND(enumCharacterList eCharacter)
	RETURN g_sCharacterSheetAll[eCharacter].friend
ENDFUNC

/// PURPOSE:
///    Gets the characters phonebook state
/// PARAMS:
///    iCharacterPhonebookID - 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters phonebook state, asserts and returns NOT_LISTED on invalid iCharacterPhonebookID or eCharacter
FUNC enumIndividualPhoneBookState GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(enumCharacterList eCharacter, INT iCharacterPhonebookID)
	IF iCharacterPhonebookID < 0 OR iCharacterPhonebookID > CHARACTER_SHEET_MAX_PLAYER_ID
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE invalid phonebook id")
		DEBUG_PRINTCALLSTACK()
		RETURN NOT_LISTED
	ENDIF	
	
	RETURN g_sCharacterSheetAll[eCharacter].PhoneBookState[iCharacterPhonebookID]
ENDFUNC

/// PURPOSE:
///    Gets the characters bank account
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters bank account, asserts and returns NO_ACCOUNT if eCharacter is invalid
FUNC enumBankAccountName GLOBAL_CHARACTER_SHEET_GET_BANK_ACCOUNT(enumCharacterList eCharacter)
	RETURN g_sCharacterSheetAll[eCharacter].bank_account
ENDFUNC

/// PURPOSE:
///    Gets the characters picture message status
/// PARAMS:
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters picture message status, asserts and returns NO_PICMSG_STORED if eCharacter is invalid
FUNC enumPicMsgStatus GLOBAL_CHARACTER_SHEET_GET_PICTURE_MESSAGE_STATUS(enumCharacterList eCharacter)
	RETURN g_sCharacterSheetAll[eCharacter].picmsgStatus
ENDFUNC

/// PURPOSE:
///    Gets the characters missed call status
/// PARAMS:
///    iPhoneOwnerID - 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters missed call status, asserts and returns CHAR_BLANK_ENTRY on invalid iPhoneOwnerID or eCharacter
FUNC enumMissedCallStatus GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS(enumCharacterList eCharacter, INT iPhoneOwnerID)
	IF iPhoneOwnerID < 0 OR iPhoneOwnerID > CHARACTER_SHEET_MAX_PLAYER_ID
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_GET_MISSED_CALL_STATUS invalid phone owner id")
		DEBUG_PRINTCALLSTACK()
		RETURN NO_MISSED_CALL
	ENDIF	
	
	RETURN g_sCharacterSheetAll[eCharacter].missedCallStatus[iPhoneOwnerID]
ENDFUNC

/// PURPOSE:
///    Gets the characters status as caller
/// PARAMS:
///    iPhoneOwnerID - 0 = michael, 1 = Franklin, 2 = Trevor, 3 = Multiplayer
///    eCharacter - the character to get, asserts if eCharacter greater than MAX_CHARACTERS_PLUS_DUMMY
/// RETURNS:
///    The characters status as caller, asserts and returns UNKNOWN_CALLER on invalid iPhoneOwnerID or eCharacter
FUNC enumStatusAsCaller GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER(enumCharacterList eCharacter, INT iPhoneOwnerID)	
	IF iPhoneOwnerID < 0 OR iPhoneOwnerID > CHARACTER_SHEET_MAX_PLAYER_ID
		ASSERTLN("[CHARACTER_SHEET] GLOBAL_CHARACTER_SHEET_GET_STATUS_AS_CALLER invalid phone owner id")
		DEBUG_PRINTCALLSTACK()
		RETURN UNKNOWN_CALLER
	ENDIF	
	
	RETURN g_sCharacterSheetAll[eCharacter].statusAsCaller[iPhoneOwnerID]
ENDFUNC

/// PURPOSE:
///    Gets the max character for the current gamemode
/// RETURNS:
///    MAX_CHARACTERS_MP for multiplayer and creator, MAX_CHARACTERS for singleplayer
FUNC enumCharacterList GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()
	// TODO: Add global override
	IF g_Private_Gamemode_Current = GAMEMODE_FM 
	OR g_Private_Gamemode_Current = GAMEMODE_CREATOR
		RETURN MAX_CHARACTERS_MP
	ENDIF
	
	RETURN MAX_CHARACTERS
ENDFUNC


//Open up for public use if necessary.
/// PURPOSE:
///    
/// PARAMS:
///    whichChar - 
///    whichModel - 
///    whichInt - 
///    whichLabel - This must be unique as it is used as an ID for the global saved vars.
///    whichPicLabel - 
///    whichRoleLabel - 
///    isPhoneContact - 
///    isEmailContact - 
///    whichAnsMessage - 
///    isFriend - 
///    isFamilyMember - 
///    MichaelBookState - 
///    FranklinBookState - 
///    TrevorBookState - 
///    whichNumberLabel - 
///    whichBankAccount - 
PROC Fill_Character_SheetCLF (enumCharacterList whichChar, MODEL_NAMES whichModel, INT whichInt, STRING whichLabel, STRING whichPicLabel, STRING whichRoleLabel, enumPhoneContact isPhoneContact,
     enumEmailContact isEmailContact, STRING whichAnsMessage,  enumFriend isFriend, enumFamilyMember isFamilyMember, enumIndividualPhoneBookState MichaelBookState,
     enumIndividualPhoneBookState FranklinBookState, enumIndividualPhoneBookState TrevorBookState, 
     STRING whichNumberLabel, enumBankAccountName whichBankAccount = NO_ACCOUNT)

    
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].game_model = whichModel    
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].alphaInt = whichInt
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].original_alphaInt = g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].alphaInt //make a copy of the alpha_int so it can be restored after any priority contact manipulation.
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].label = whichLabel
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].picture = whichPicLabel

    whichRoleLabel = whichRoleLabel //Unreferenced bypass in case this has to go back in.
    
    g_CharacterSheetNonSaved[whichChar].phone = isPhoneContact
    g_CharacterSheetNonSaved[whichChar].email = isEmailContact
    g_CharacterSheetNonSaved[whichChar].ansphone_labelroot = whichAnsMessage
    
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].friend = isFriend  
	
    isFamilyMember = isFamilyMember //Unreferenced bypass in case this has to go back in.

    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[0] = MichaelBookState
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[1] = FranklinBookState
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[2] = TrevorBookState

    g_CharacterSheetNonSaved[whichChar].phonebookNumberLabel = whichNumberLabel
    
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[whichChar].bank_account = whichBankAccount


ENDPROC
PROC Fill_Character_SheetNRM (enumCharacterList whichChar, MODEL_NAMES whichModel, INT whichInt, STRING whichLabel, STRING whichPicLabel, STRING whichRoleLabel, enumPhoneContact isPhoneContact,
     enumEmailContact isEmailContact, STRING whichAnsMessage,  enumFriend isFriend, enumFamilyMember isFamilyMember, enumIndividualPhoneBookState MichaelBookState,
     enumIndividualPhoneBookState FranklinBookState, enumIndividualPhoneBookState TrevorBookState, 
     STRING whichNumberLabel, enumBankAccountName whichBankAccount = NO_ACCOUNT)

    
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].game_model = whichModel
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].alphaInt = whichInt
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].original_alphaInt = g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].alphaInt //make a copy of the alpha_int so it can be restored after any priority contact manipulation.
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].label = whichLabel
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].picture = whichPicLabel

    whichRoleLabel = whichRoleLabel //Unreferenced bypass in case this has to go back in.

    g_CharacterSheetNonSaved[whichChar].phone = isPhoneContact
    g_CharacterSheetNonSaved[whichChar].email = isEmailContact
    g_CharacterSheetNonSaved[whichChar].ansphone_labelroot = whichAnsMessage
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].friend = isFriend
            
    isFamilyMember = isFamilyMember //Unreferenced bypass in case this has to go back in.

    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[0] = MichaelBookState
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[1] = FranklinBookState
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].PhoneBookState[2] = TrevorBookState
    g_CharacterSheetNonSaved[whichChar].phonebookNumberLabel = whichNumberLabel    
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[whichChar].bank_account = whichBankAccount

    

ENDPROC
PROC Fill_Character_Sheet (enumCharacterList whichChar, MODEL_NAMES whichModel, INT whichInt, STRING whichLabel, STRING whichPicLabel, STRING whichRoleLabel, enumPhoneContact isPhoneContact,
	 enumEmailContact isEmailContact, STRING whichAnsMessage,  enumFriend isFriend, enumFamilyMember isFamilyMember, enumIndividualPhoneBookState MichaelBookState,
     enumIndividualPhoneBookState FranklinBookState, enumIndividualPhoneBookState TrevorBookState, 
     STRING whichNumberLabel, enumBankAccountName whichBankAccount = NO_ACCOUNT)

    #IF USE_CLF_DLC 
        IF g_bLoadedClifford
            Fill_Character_SheetCLF(whichChar,whichModel,whichInt,whichLabel,whichPicLabel,whichRoleLabel,isPhoneContact,isEmailContact,whichAnsMessage,
                                    isFriend,isFamilyMember,MichaelBookState,FranklinBookState,TrevorBookState,whichNumberLabel, whichBankAccount)
            EXIT
        ENDIF       
    #ENDIF
    #IF USE_NRM_DLC 
        IF g_bLoadedNorman
            Fill_Character_SheetNRM(whichChar,whichModel,whichInt,whichLabel,whichPicLabel,whichRoleLabel,isPhoneContact,isEmailContact,whichAnsMessage,
                                    isFriend,isFamilyMember,MichaelBookState,FranklinBookState,TrevorBookState,whichNumberLabel, whichBankAccount)
            EXIT
        ENDIF       
    #ENDIF
    
	GLOBAL_CHARACTER_SHEET_SET_GAME_MODEL(whichChar, whichModel)
	GLOBAL_CHARACTER_SHEET_SET_ALPHA_INT(whichChar, whichInt)
	GLOBAL_CHARACTER_SHEET_SET_ORIGINAL_ALPHA_INT(whichChar, GLOBAL_CHARACTER_SHEET_GET_ALPHA_INT(whichChar))
	GLOBAL_CHARACTER_SHEET_SET_LABEL(whichChar, whichLabel)
	GLOBAL_CHARACTER_SHEET_SET_PICTURE(whichChar, whichPicLabel)

    whichRoleLabel = whichRoleLabel //Unreferenced bypass in case this has to go back in.

    g_CharacterSheetNonSaved[whichChar].phone = isPhoneContact
    g_CharacterSheetNonSaved[whichChar].email = isEmailContact

    g_CharacterSheetNonSaved[whichChar].ansphone_labelroot = whichAnsMessage

    GLOBAL_CHARACTER_SHEET_SET_FRIEND(whichChar, isFriend)
    
    
    isFamilyMember = isFamilyMember //Unreferenced bypass in case this has to go back in.
	
	GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(whichChar, 0, MichaelBookState)
	GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(whichChar, 1, FranklinBookState)
	GLOBAL_CHARACTER_SHEET_SET_PHONEBOOK_STATE(whichChar, 2, TrevorBookState)

    g_CharacterSheetNonSaved[whichChar].phonebookNumberLabel = whichNumberLabel
    
	GLOBAL_CHARACTER_SHEET_SET_BANK_ACCOUNT(whichChar, whichBankAccount)

ENDPROC


PROC Fill_Gameworld_Numbers (enumGameWorldNumbers whichGWentry, STRING whichDialledNumberLabel, STRING whichRootLabel, STRING whichVoiceID)

    g_GameworldNumber[whichGWentry].Gameworld_DialledNumberLabel = whichDialledNumberLabel 
    g_GameworldNumber[whichGWentry].Gameworld_AnsphoneLabelRoot = whichRootLabel
    g_GameworldNumber[whichGWentry].Gameworld_VoiceID = whichVoiceID

ENDPROC

PROC Fill_Special_MP_Characters (INT WhichArrayPosition, enumCharacterList WhichChar, STRING WhichSecondaryFunctionLabel_1, STRING WhichSecondaryFunctionLabel_2)
 
    g_SpecialMPCharacters[WhichArrayPosition].Name_TextLabel  = GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichChar)
    g_SpecialMPCharacters[WhichArrayPosition].SecondaryFunctionLabel_1 = WhichSecondaryFunctionLabel_1
    g_SpecialMPCharacters[WhichArrayPosition].SecondaryFunctionLabel_2 = WhichSecondaryFunctionLabel_2


ENDPROC

PROC Fill_Special_SP_Characters (INT WhichArrayPosition, enumCharacterList WhichChar, STRING WhichSecondaryFunctionLabel_1)
 
    g_SpecialSPCharacters[WhichArrayPosition].Name_TextLabel  = GLOBAL_CHARACTER_SHEET_GET_LABEL(WhichChar)
    g_SpecialSPCharacters[WhichArrayPosition].SecondaryFunctionLabel_1 = WhichSecondaryFunctionLabel_1

ENDPROC






//Fill in the cellphone setting list with the defined data for each field.
PROC Fill_Setting_List(INT PassedFillIndex, enumSettingList PassedSetting, INT PassedSettingOrderInt, STRING PassedSettingLabel, INT PassedSettingIcon, enumPhoneGuiPresence PassedGuiPresence)
 

    This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_orderInt = PassedSettingOrderInt
    
    //Make copy of orderInt in original orderInt in case the order is changed to make the setting a priority.
    This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_original_orderInt = PassedSettingOrderInt  
    This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_Primary_Label = PassedSettingLabel
    This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_Icon_Int = PassedSettingIcon
    This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].phoneGuiPresence = PassedGuiPresence    
    //Temporary check to remove unreferenced reporting. This var within the struct is likely to be used before project end.    
    IF This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_original_orderInt = 0
        This_Cellphone_Owner_Settings_ListContents[PassedFillIndex].g_SettingList[PassedSetting].Setting_original_orderInt = 0

    ENDIF

ENDPROC



PROC Fill_All_Phone_SettingsCLF()
 
    //CHAR_MICHAEL 0
    //CHAR_FRANKLIN 1
    //CHAR_TREVOR 2
    //CHAR_MULTIPLAYER 3
   
    INT fill_index = 0
    WHILE fill_index < 4

        Fill_Setting_List (fill_index, SETTING_PROFILE, 160, "CELL_700", 25, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_RINGTONE, 180, "CELL_710", 18,  AVAILABLE_IN_GUI) //180
        Fill_Setting_List (fill_index, SETTING_THEME, 200, "CELL_720", 23, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_VIBRATE, 220, "CELL_730", 20, AVAILABLE_IN_GUI)  //220
        Fill_Setting_List (fill_index, SETTING_WALLPAPER, 20, "CELL_740", 23, AVAILABLE_IN_GUI)  //Referred to as "background", hence 20 as the alpha sort 3rd parameter.

        #if USE_TU_CHANGES
            Fill_Setting_List (fill_index, SETTING_MISC_A, 150, "CELL_705", 18, AVAILABLE_IN_GUI) //Invite Sound toggle added at request of LB bug 1775637
        #endif
        //Make sure these match order of PhoneProfileEnum in Cellphone Globals.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[0] = "CELL_800"     //Normal
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[0] = 140
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[0] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[1] = "CELL_802"     //Quiet
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[1] = 170
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[2] = "CELL_801"     //Sleep
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[2] = 190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[2] = 26
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)




        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[0] = "CELL_810"    //Ringtone 1
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[0] = "PHONE_GENERIC_RING_01"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[0] = 160
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[0] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[1] = "CELL_811"    //Ringtone 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[1] = "PHONE_GENERIC_RING_02"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[1] = 163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[1] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[2] = "CELL_812"    //Ringtone 3
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[2] = "PHONE_GENERIC_RING_03"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[2] = 167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[2] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[3] = "CELL_813"    //Special Case Ringtone 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[3] = "Silent Ringtone Dummy"  //Do not change filename! Other scripts compare this string.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[3] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[3] = 51




       
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[0] = "CELL_820"    //Theme 0 - dummy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[0] = FALSE      //not included in settings list!
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[0] = 100
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[0] = 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[1] = "CELL_820"    //Theme 1 - light blue
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[1] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[1] = 1 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[2] = "CELL_821"    //Theme 2 - green
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[2] = 75
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[2] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[2] = 2 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[3] = "CELL_822"    //Theme 3 - red
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[3] = 187
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[3] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[3] = 3 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[4] = "CELL_823"    //Theme 4 - orange
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[4] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[4] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[4] = 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[5] = "CELL_824"    //Theme 5 - grey
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[5] = 77
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[5] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[5] = 5 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[6] = "CELL_825"    //Theme 6 - purple
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[6] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[6] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[6] = 6 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[7] = "CELL_826"    //Theme 7 - pink
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[7] = 164
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[7] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[7] = 7
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[8] = "CELL_846"    //Theme 8 - Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[8] = FALSE //redacted #1167512
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[8] = 196
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[8] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[8] = 2 //Points to green in SF theme list.
        
        //Wallpapers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_840"    //Default
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[0] = 300
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[0] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[1] = "CELL_841"    //Badger
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[1] = 3021
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[1] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[2] = "CELL_842"    //Whiz
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[2] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[2] = 3233
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[2] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[3] = "CELL_843"    //Tinkle
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[3] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[3] = 3205
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[3] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[4] = "CELL_844"    //Swingers (Pink)
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[4] = 3197
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[4] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[5] = "CELL_845"    //Pisswasser
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[5] = 3163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[5] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[6] = "CELL_846"    //Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[6] = 3194
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[6] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[7] = "CELL_847"    //Rep. Space Rs
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[7] = 3192
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[7] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[8] = "CELL_848"    //Poppy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[8] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[8] = 3167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[8] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[9] = "CELL_849"    //Panic
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[9] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[9] = 3161
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[9] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[10] = "CELL_850"    //Benders
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[10] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[10] = 3023
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[10] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[11] = "CELL_851"    //Corkers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[11] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[11] = 3038
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[11] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[12] = "CELL_852"    //Devils
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[12] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[12] = 3045
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[12] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[13] = "CELL_853"    //Feud
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[13] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[13] = 3065
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[13] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[14] = "CELL_854"    //Jardineros
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[14] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[14] = 3102
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[14] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[15] = "CELL_855"    //Shrimps
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[15] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[15] = 3190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[15] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[16] = "CELL_856"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[16] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[16] = 3195
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[16] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[17] = "CELL_857"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[17] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[17] = 3198
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[17] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option = 1 //Make default backup as failsafe
       
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[0] = "CELL_831"     //Vibrate OFF      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[0] = 153
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[0] = 21
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Vibrate ON      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[1] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[1] = 20
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Currently_Selected_Option = 1//ON //requested by LB 847970

        #if USE_TU_CHANGES
            //Invite Sound toggle added at request of LB bug 1775637
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[0] = "CELL_831"//"CELL_831"     //Invite Sound OFF      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[0] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[0] = 153
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[0] = 51
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Invite sound ON      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[1] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[1] = 157
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[1] = 18
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Currently_Selected_Option = 1//ON
        #endif
        fill_index ++
    ENDWHILE


    //Important: Theme Integration.
    //This section specifies the starting theme and OS name of each player character and sets a default for multiplayer before savegame restoration.
    
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ScaleformOS_Movie_Name = "cellphone_iFruit" //requested by 993529  
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer = 1
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].OSTypeForThisPlayer= OS_BITTERSWEET 
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].VibrateForThisPlayer = 1 //requested by LB 847970
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ScaleformOS_Movie_Name = "cellphone_badger" //requested by 993529
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer = 2
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].RingtoneForThisPlayer = "PHONE_GENERIC_RING_02"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].OSTypeForThisPlayer = OS_IFRUIT
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ProviderForThisPlayer = PROVIDER_TINKLE
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ScaleformOS_Movie_Name = "cellphone_facade"//"cellphone_ifruit"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer = 5 //setting to grey for test this can change bk to red //3
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].RingtoneForThisPlayer = "PHONE_GENERIC_RING_03"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].OSTypeForThisPlayer = OS_FACADE
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ProviderForThisPlayer = PROVIDER_WHIZ
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].WallpaperForThisPlayer = WALLPAPER_DEFAULT    
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ScaleformOS_Movie_Name = "cellphone_ifruit"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ThemeForThisPlayer = 1
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].OSTypeForThisPlayer = OS_POLICE
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer = 1  //requested by LB 847970    
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobalsClifford.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("CHARSHEET PUBLIC - Initialised cellphone character setting pre savegame restoration. Set MP vibrate stat to 1")
        PRINTNL()

    #endif

ENDPROC
PROC Fill_All_Phone_SettingsNRM()
 
    //CHAR_MICHAEL 0
    //CHAR_NRM_JIMMY  = CHAR_FRANKLIN 1
    //CHAR_NRM_TRACEY = CHAR_TREVOR 2
    //CHAR_NRM_MULT   = CHAR_MULTIPLAYER 3
   
    INT fill_index = 0
    WHILE fill_index < 4

        Fill_Setting_List (fill_index, SETTING_PROFILE, 160, "CELL_700", 25, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_RINGTONE, 180, "CELL_710", 18,  AVAILABLE_IN_GUI) //180
        Fill_Setting_List (fill_index, SETTING_THEME, 200, "CELL_720", 23, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_VIBRATE, 220, "CELL_730", 20, AVAILABLE_IN_GUI)  //220
        Fill_Setting_List (fill_index, SETTING_WALLPAPER, 20, "CELL_740", 23, AVAILABLE_IN_GUI)  //Referred to as "background", hence 20 as the alpha sort 3rd parameter.

        #if USE_TU_CHANGES
            Fill_Setting_List (fill_index, SETTING_MISC_A, 150, "CELL_705", 18, AVAILABLE_IN_GUI) //Invite Sound toggle added at request of LB bug 1775637
        #endif
        //Make sure these match order of PhoneProfileEnum in Cellphone Globals.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[0] = "CELL_800"     //Normal
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[0] = 140
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[0] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[1] = "CELL_802"     //Quiet
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[1] = 170
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[2] = "CELL_801"     //Sleep
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[2] = 190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[2] = 26
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)






        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[0] = "CELL_810"    //Ringtone 1
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[0] = "PHONE_GENERIC_RING_01"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[0] = 160
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[0] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[1] = "CELL_811"    //Ringtone 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[1] = "PHONE_GENERIC_RING_02"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[1] = 163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[1] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[2] = "CELL_812"    //Ringtone 3
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[2] = "PHONE_GENERIC_RING_03"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[2] = 167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[2] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[3] = "CELL_813"    //Special Case Ringtone 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[3] = "Silent Ringtone Dummy"  //Do not change filename! Other scripts compare this string.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[3] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[3] = 51




       
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[0] = "CELL_820"    //Theme 0 - dummy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[0] = FALSE      //not included in settings list!
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[0] = 100
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[0] = 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[1] = "CELL_820"    //Theme 1 - light blue
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[1] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[1] = 1 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[2] = "CELL_821"    //Theme 2 - green
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[2] = 75
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[2] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[2] = 2 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[3] = "CELL_822"    //Theme 3 - red
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[3] = 187
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[3] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[3] = 3 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[4] = "CELL_823"    //Theme 4 - orange
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[4] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[4] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[4] = 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[5] = "CELL_824"    //Theme 5 - grey
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[5] = 77
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[5] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[5] = 5 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[6] = "CELL_825"    //Theme 6 - purple
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[6] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[6] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[6] = 6 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[7] = "CELL_826"    //Theme 7 - pink
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[7] = 164
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[7] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[7] = 7
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[8] = "CELL_846"    //Theme 8 - Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[8] = FALSE //redacted #1167512
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[8] = 196
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[8] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[8] = 2 //Points to green in SF theme list.
        
        //Wallpapers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_840"    //Default
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[0] = 300
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[0] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[1] = "CELL_841"    //Badger
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[1] = 3021
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[1] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[2] = "CELL_842"    //Whiz
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[2] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[2] = 3233
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[2] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[3] = "CELL_843"    //Tinkle
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[3] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[3] = 3205
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[3] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[4] = "CELL_844"    //Swingers (Pink)
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[4] = 3197
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[4] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[5] = "CELL_845"    //Pisswasser
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[5] = 3163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[5] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[6] = "CELL_846"    //Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[6] = 3194
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[6] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[7] = "CELL_847"    //Rep. Space Rs
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[7] = 3192
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[7] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[8] = "CELL_848"    //Poppy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[8] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[8] = 3167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[8] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[9] = "CELL_849"    //Panic
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[9] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[9] = 3161
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[9] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[10] = "CELL_850"    //Benders
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[10] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[10] = 3023
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[10] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[11] = "CELL_851"    //Corkers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[11] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[11] = 3038
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[11] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[12] = "CELL_852"    //Devils
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[12] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[12] = 3045
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[12] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[13] = "CELL_853"    //Feud
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[13] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[13] = 3065
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[13] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[14] = "CELL_854"    //Jardineros
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[14] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[14] = 3102
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[14] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[15] = "CELL_855"    //Shrimps
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[15] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[15] = 3190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[15] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[16] = "CELL_856"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[16] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[16] = 3195
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[16] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[17] = "CELL_857"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[17] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[17] = 3198
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[17] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option = 1 //Make default backup as failsafe
       
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[0] = "CELL_831"     //Vibrate OFF      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[0] = 153
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[0] = 21
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Vibrate ON      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[1] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[1] = 20
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Currently_Selected_Option = 1//ON //requested by LB 847970

        #if USE_TU_CHANGES
            //Invite Sound toggle added at request of LB bug 1775637
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[0] = "CELL_831"     //Invite Sound OFF      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[0] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[0] = 153
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[0] = 51
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Invite sound ON      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[1] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[1] = 157
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[1] = 18
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Currently_Selected_Option = 1//ON
        #endif
        fill_index ++
    ENDWHILE


    //Important: Theme Integration.
    //This section specifies the starting theme and OS name of each player character and sets a default for multiplayer before savegame restoration.
    
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ScaleformOS_Movie_Name = "cellphone_iFruit" //requested by 993529  
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer = 1
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].OSTypeForThisPlayer= OS_BITTERSWEET 
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].VibrateForThisPlayer = 1 //requested by LB 847970
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].ScaleformOS_Movie_Name = "cellphone_badger" //requested by 993529
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].ThemeForThisPlayer = 3 //red 
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].RingtoneForThisPlayer = "PHONE_GENERIC_RING_02"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].OSTypeForThisPlayer = OS_IFRUIT
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].ProviderForThisPlayer = PROVIDER_TINKLE
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].WallpaperForThisPlayer = 7 //"CELL_847"    //Rep. Space Rs
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_JIMMY].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].ScaleformOS_Movie_Name = "cellphone_facade"//"cellphone_ifruit"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].ThemeForThisPlayer = 7 //pink for tracey
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].RingtoneForThisPlayer = "PHONE_GENERIC_RING_03"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].OSTypeForThisPlayer = OS_FACADE
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].ProviderForThisPlayer = PROVIDER_WHIZ
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].WallpaperForThisPlayer = WALLPAPER_DEFAULT    
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_TRACEY].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].ScaleformOS_Movie_Name = "cellphone_ifruit"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].ThemeForThisPlayer = 1
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].OSTypeForThisPlayer = OS_POLICE
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].VibrateForThisPlayer = 1  //requested by LB 847970    
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobalsnorman.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_NRM_MULT].g_LastMessageSentMustBeRead = FALSE

    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("CHARSHEET PUBLIC - Initialised cellphone character setting pre savegame restoration. Set MP vibrate stat to 1")
        PRINTNL()

    #endif

ENDPROC




PROC Fill_All_Phone_Settings()
 
    //CHAR_MICHAEL 0
    //CHAR_FRANKLIN 1
    //CHAR_TREVOR 2
    //CHAR_MULTIPLAYER 3
   
    INT fill_index = 0
    WHILE fill_index < 4

        Fill_Setting_List (fill_index, SETTING_PROFILE, 160, "CELL_700", 25, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_RINGTONE, 180, "CELL_710", 18,  AVAILABLE_IN_GUI) //180
        Fill_Setting_List (fill_index, SETTING_THEME, 200, "CELL_720", 23, AVAILABLE_IN_GUI)
        Fill_Setting_List (fill_index, SETTING_VIBRATE, 220, "CELL_730", 20, AVAILABLE_IN_GUI)  //220
        Fill_Setting_List (fill_index, SETTING_WALLPAPER, 20, "CELL_740", 23, AVAILABLE_IN_GUI)  //Referred to as "background", hence 20 as the alpha sort 3rd parameter.

        #if USE_TU_CHANGES
            Fill_Setting_List (fill_index, SETTING_MISC_A, 150, "CELL_705", 18, AVAILABLE_IN_GUI) //Invite Sound toggle added at request of LB bug 1775637
        #endif
        //Make sure these match order of PhoneProfileEnum in Cellphone Globals.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[0] = "CELL_800"     //Normal
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[0] = 140
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[0] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[1] = "CELL_802"     //Quiet
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[1] = 170
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_labels[2] = "CELL_801"     //Sleep
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Order[2] = 190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Secondary_Icon_Int[2] = 26
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_PROFILE)].Setting_Currently_Selected_Option = ENUM_TO_INT(PROFILE_NORMAL_MODE)



        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[0] = "CELL_810"    //Ringtone 1
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[0] = "PHONE_GENERIC_RING_01"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[0] = 160
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[0] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[1] = "CELL_811"    //Ringtone 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[1] = "PHONE_GENERIC_RING_02"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[1] = 163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[1] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[2] = "CELL_812"    //Ringtone 3
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[2] = "PHONE_GENERIC_RING_03"
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[2] = 167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[2] = 18

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_labels[3] = "CELL_813"    //Special Case Ringtone 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Option_filename_label[3] = "Silent Ringtone Dummy"  //Do not change filename! Other scripts compare this string.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Order[3] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_RINGTONE)].Setting_Secondary_Icon_Int[3] = 51
       



        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[0] = "CELL_820"    //Theme 0 - dummy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[0] = FALSE      //not included in settings list!
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[0] = 100
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[0] = 2
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[1] = "CELL_820"    //Theme 1 - light blue
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[1] = 25
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[1] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[1] = 1 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[2] = "CELL_821"    //Theme 2 - green
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[2] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[2] = 75
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[2] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[2] = 2 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[3] = "CELL_822"    //Theme 3 - red
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[3] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[3] = 187
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[3] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[3] = 3 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[4] = "CELL_823"    //Theme 4 - orange
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[4] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[4] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[4] = 4 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[5] = "CELL_824"    //Theme 5 - grey
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[5] = 77
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[5] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[5] = 5 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[6] = "CELL_825"    //Theme 6 - purple
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[6] = 168
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[6] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[6] = 6 
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[7] = "CELL_826"    //Theme 7 - pink
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[7] = 164
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[7] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[7] = 7
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_labels[8] = "CELL_846"    //Theme 8 - Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Option_available[8] = FALSE //redacted #1167512
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Order[8] = 196
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Secondary_Icon_Int[8] = 23
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Linked_SF_Reference[8] = 2 //Points to green in SF theme list.
        
        //Wallpapers
        IF fill_index = 3 //In MP, Default will now use Crew Emblem in position zero.
                                                                                      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_CREWEMB"    //Default
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[0] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[0] = 300
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[0] = 23

        ELSE

            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[0] = "CELL_840"    //Default
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[0] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[0] = 300
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[0] = 23

        ENDIF


        //The next three were removed. Setting_Secondary_Option_available is set to FALSE for each.
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[1] = "CELL_841"    //Badger
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[1] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[1] = 3021
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[1] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[2] = "CELL_842"    //Whiz
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[2] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[2] = 3233
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[2] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[3] = "CELL_843"    //Tinkle
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[3] = FALSE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[3] = 3205
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[3] = 23



        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[4] = "CELL_844"    //Swingers (Pink)
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[4] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[4] = 3197
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[4] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[5] = "CELL_845"    //Pisswasser
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[5] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[5] = 3163
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[5] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[6] = "CELL_846"    //Sprunk
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[6] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[6] = 3194
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[6] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[7] = "CELL_847"    //Rep. Space Rs
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[7] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[7] = 3192
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[7] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[8] = "CELL_848"    //Poppy
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[8] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[8] = 3167
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[8] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[9] = "CELL_849"    //Panic
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[9] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[9] = 3161
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[9] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[10] = "CELL_850"    //Benders
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[10] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[10] = 3023
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[10] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[11] = "CELL_851"    //Corkers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[11] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[11] = 3038
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[11] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[12] = "CELL_852"    //Devils
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[12] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[12] = 3045
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[12] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[13] = "CELL_853"    //Feud
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[13] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[13] = 3065
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[13] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[14] = "CELL_854"    //Jardineros
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[14] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[14] = 3102
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[14] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[15] = "CELL_855"    //Shrimps
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[15] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[15] = 3190
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[15] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[16] = "CELL_856"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[16] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[16] = 3195
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[16] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_labels[17] = "CELL_857"    //Squeezers
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Option_available[17] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Order[17] = 3198
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_WALLPAPER)].Setting_Secondary_Icon_Int[17] = 23

        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_THEME)].Setting_Currently_Selected_Option = 1 //Make default backup as failsafe
       
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[0] = "CELL_831"     //Vibrate OFF      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[0] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[0] = 153
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[0] = 21
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Vibrate ON      
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Option_available[1] = TRUE
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Order[1] = 157
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Secondary_Icon_Int[1] = 20
        This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_VIBRATE)].Setting_Currently_Selected_Option = 1//ON //requested by LB 847970

        #if USE_TU_CHANGES
            //Invite Sound toggle added at request of LB bug 1775637
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[0] = "CELL_831"     //Invite Sound OFF      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[0] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[0] = 153
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[0] = 51
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_labels[1] = "CELL_830"     //Invite sound ON      
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Option_available[1] = TRUE
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Order[1] = 157
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Secondary_Icon_Int[1] = 18
            This_Cellphone_Owner_Settings_ListContents[fill_index].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Currently_Selected_Option = 1//ON
        #endif
        fill_index ++
    ENDWHILE


    //Important: Theme Integration.
    //This section specifies the starting theme and OS name of each player character and sets a default for multiplayer before savegame restoration.
    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ScaleformOS_Movie_Name = "cellphone_iFruit" //requested by 993529  
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ThemeForThisPlayer = 1
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].OSTypeForThisPlayer= OS_BITTERSWEET 
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].VibrateForThisPlayer = 1 //requested by LB 847970
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MICHAEL].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ScaleformOS_Movie_Name = "cellphone_badger" //requested by 993529
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ThemeForThisPlayer = 2
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].RingtoneForThisPlayer = "PHONE_GENERIC_RING_02"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].OSTypeForThisPlayer = OS_IFRUIT
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].ProviderForThisPlayer = PROVIDER_TINKLE
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_FRANKLIN].g_LastMessageSentMustBeRead = FALSE

    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ScaleformOS_Movie_Name = "cellphone_facade"//"cellphone_ifruit"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ThemeForThisPlayer = 3
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].RingtoneForThisPlayer = "PHONE_GENERIC_RING_03"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].OSTypeForThisPlayer = OS_FACADE
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].VibrateForThisPlayer = 1  //requested by LB 847970
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].ProviderForThisPlayer = PROVIDER_WHIZ
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].WallpaperForThisPlayer = WALLPAPER_DEFAULT    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_TREVOR].g_LastMessageSentMustBeRead = FALSE


    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ScaleformOS_Movie_Name = "cellphone_ifruit"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ThemeForThisPlayer = 1
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].RingtoneForThisPlayer = "PHONE_GENERIC_RING_01"
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].OSTypeForThisPlayer = OS_POLICE
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].VibrateForThisPlayer = 1  //requested by LB 847970    
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].ProviderForThisPlayer = PROVIDER_BADGER
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].WallpaperForThisPlayer = WALLPAPER_DEFAULT
    g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[CHAR_MULTIPLAYER].g_LastMessageSentMustBeRead = FALSE

    #if IS_DEBUG_BUILD

        PRINTNL()
        PRINTSTRING ("CHARSHEET PUBLIC - Initialised cellphone character setting pre savegame restoration. Set MP vibrate stat to 1")
        PRINTNL()

    #endif

ENDPROC

#IF USE_CLF_DLC
PROC Initialise_CharSheet_Global_Variables_On_StartupCLF()      

    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING("Initialising CharSheet from charsheet_public.sch prior to reload of save data agt...")
        PRINTNL()
    #endif

    //Player characters...
    Fill_Character_SheetCLF (CHAR_MICHAEL, PLAYER_ZERO, 1360, "CELL_101", "CELL_301", "CELL_401", PC_MICHAEL, EMAIL_MICHAEL, "NO_ANSMSG", 
        FR_MICHAEL,FM_TREVOR_0_MICHAEL,NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1001", BANK_ACCOUNT_MICHAEL)   
        
    Fill_Character_SheetCLF (CHAR_TREVOR, PLAYER_TWO, 2070, "CELL_102", "CELL_302", "CELL_402", PC_TREVOR, EMAIL_TREVOR, "NO_ANSMSG",
        FR_TREVOR,FM_TREVOR_0_TREVOR, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1002", BANK_ACCOUNT_TREVOR)  
        
    Fill_Character_SheetCLF (CHAR_FRANKLIN, PLAYER_ONE, 670, "CELL_103", "CELL_303", "CELL_403", PC_FRANKLIN, EMAIL_FRANKLIN, "NO_ANSMSG",
        FR_FRANKLIN,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1003", BANK_ACCOUNT_FRANKLIN)    
   
    //Virtual multiplayer character
    Fill_Character_SheetCLF(CHAR_MULTIPLAYER, A_M_Y_BeachVesp_01, 240, "CELL_197", "CELL_195", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //No phone number required.
    
    
    // story characters
       
    Fill_Character_SheetCLF (CHAR_CLF_RON, IG_NERVOUSRON, 1880, "CELL_129", "CELL_329", "CELL_429", PC_CLF_RON,   NO_EMAIL_CONTACT, "RON_APH1",
        NO_FRIEND, FM_TREVOR_0_RON, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1029")    
    
    Fill_Character_SheetCLF (CHAR_CLF_PAPERMAN, IG_PAPER, 1970, "CELL_134", "CELL_300", "CELL_300", PC_STEVE,   NO_EMAIL_CONTACT, "STE_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1034")
        
    Fill_Character_SheetCLF (CHAR_CLF_14, IG_AGENT14, 150, "CELL_A_101", "CELL_300", "CELL_300", PC_CLF_14,NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_SIMON_FENCE, U_M_M_SPYACTOR, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_SIMON_FENCE,  NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_KAREN_DANIELS, IG_MICHELLE, 150, "CELL_105", "CELL_300", "CELL_300", PC_CLF_KAREN_DANIELS, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_WADE, IG_WADE, 2300, "CELL_135", "CELL_334", "CELL_434", PC_CLF_WADE,   NO_EMAIL_CONTACT, "WAD_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1035")   
    
    Fill_Character_SheetCLF (CHAR_CLF_MILES_GOLDSTEIN, A_M_Y_HIPSTER_02, 150, "CELL_A_102", "CELL_300", "CELL_300", PC_CLF_MILES_GOLDSTEIN, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_AFIF, A_M_M_BUSINESS_01, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_AFIF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_ADIL, A_M_Y_BUSINESS_01, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_ADIL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_BASIL, A_M_Y_BUSINESS_02, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_BASIL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_DR_MINTON, S_M_M_DOCTOR_01, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_DR_MINTON,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_JUNG_HO_SONG, G_M_Y_KOREAN_01, 150, "CELL_135", "CELL_300", "CELL_300", PC_CLF_JUNG_HO_SONG,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_CASINO_OWNER, A_M_M_PROLHOST_01, 377, "CELL_135", "CELL_300", "CELL_300", PC_CLF_CASINO_OWNER,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
    
    Fill_Character_SheetCLF (CHAR_CLF_LILLY, A_F_Y_Business_01, 1249, "CELL_135", "CELL_300", "CELL_300", PC_CLF_LILLY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")   
    
    Fill_Character_SheetCLF (CHAR_CLF_CLAIRE, A_F_Y_Business_02, 388, "CELL_135", "CELL_300", "CELL_300", PC_CLF_CLAIRE,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_AGNES, A_F_M_TOURIST_01, 388, "CELL_135", "CELL_300", "CELL_300", PC_CLF_AGNES,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_MELODY, A_F_Y_HIPSTER_01, 388, "CELL_135", "CELL_300", "CELL_300", PC_CLF_MELODY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_ALEX, A_M_Y_GENSTREET_01, 388, "CELL_135", "CELL_300", "CELL_300", PC_CLF_ALEX,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_SheetCLF (CHAR_CLF_PAYPHONE, A_M_Y_BeachVesp_01, 150, "CELL_A_113", "CELL_300", "CELL_300", PC_CLF_PAYPHONE,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    //services
    Fill_Character_SheetCLF (CHAR_CLF_LS_CUSTOMS, A_M_Y_BeachVesp_01, 1270, "CELL_E_209", "CELL_E_309", "CELL_400", PC_CLF_LS_CUSTOMS,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //
    
    Fill_Character_SheetCLF (CHAR_CLF_AMMUNATION, A_M_Y_BeachVesp_01, 113, "CELL_E_220", "CELL_E_320", "CELL_400", PC_CLF_AMMUNATION,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_01")

    //March MCW  - maybe put special number in phone state enum? e.g Not_LISTED, LISTED, SPECIAL_NUMBERS
    Fill_Character_SheetCLF (CHAR_CLF_CALL911, A_M_Y_BeachVesp_01, 560, "CELL_131", "CELL_331", "CELL_431", PC_CLF_CALL911,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_9011") //Should have low alpha int to appear at top if included in contacts list.
             
    Fill_Character_SheetCLF (CHAR_CLF_TAXI, A_M_Y_BeachVesp_01, 480, "CELL_163", "CELL_394", "CELL_446", PC_CLF_TAXI,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1062")        
    //BLOCKED contact - used in assassin missions
    Fill_Character_SheetCLF(CHAR_CLF_BLOCKED, A_M_Y_BeachVesp_01, 240, "CELL_195", "CELL_300", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
    //Dummy used specifically for human player to human player chat call.
    Fill_Character_SheetCLF (CHAR_CLF_CHAT_CALL, A_M_Y_BeachVesp_01, 90, "CELL_E_219", "CELL_E_319", "CELL_431", PC_CLF_CHAT_CALL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //This should never be added 
    
    Fill_Character_SheetCLF (CHAR_CLF_BLANK_ENTRY, A_M_Y_BeachVesp_01, 320, "CELL_196", "CELL_300", "CELL_196", PC_CLF_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")

    Fill_Character_SheetCLF (MAX_CLF_CHARACTERS, A_M_Y_BeachVesp_01, 100, "CELL_181", "CELL_300", "CELL_196", PC_CLF_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
     
     
    //set already known 
    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_LS_CUSTOMS].StatusAsCaller[0] = KNOWN_CALLER
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_LS_CUSTOMS].StatusAsCaller[1] = KNOWN_CALLER
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_LS_CUSTOMS].StatusAsCaller[2] = KNOWN_CALLER
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_LS_CUSTOMS].StatusAsCaller[3] = KNOWN_CALLER
    
    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_CALL911].StatusAsCaller[0] = KNOWN_CALLER
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_CALL911].StatusAsCaller[1] = KNOWN_CALLER
    g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[CHAR_CLF_CALL911].StatusAsCaller[2] = KNOWN_CALLER
     
     
    Fill_All_Phone_SettingsCLF()    

ENDPROC
#endif
#if USE_NRM_DLC
PROC Initialise_CharSheet_Global_Variables_On_StartupNRM()      

    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING("Initialising CharSheet from charsheet_public.sch prior to reload of save data NRM...")
        PRINTNL()
    #endif

    //Player characters...
    Fill_Character_SheetNRM (CHAR_MICHAEL, PLAYER_ZERO, 1360, "CELL_101", "CELL_301", "CELL_401", PC_MICHAEL, EMAIL_MICHAEL, "NO_ANSMSG", 
        FR_MICHAEL,FM_TREVOR_0_MICHAEL,NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1001", BANK_ACCOUNT_MICHAEL)    
    
    Fill_Character_SheetNRM (CHAR_NRM_JIMMY, IG_JIMMYDISANTO, 1030, "CELL_124", "CELL_324", "CELL_424", PC_NRM_JIMMY, EMAIL_FRANKLIN, "NO_ANSMSG",
        FR_JIMMY, FM_MICHAEL_SON, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1024", BANK_ACCOUNT_FRANKLIN)   
    
    Fill_Character_SheetNRM (CHAR_NRM_TRACEY, IG_TRACYDISANTO, 2050, "CELL_125", "CELL_325", "CELL_425", PC_NRM_TRACEY, EMAIL_TREVOR, "NO_ANSMSG",
        FR_TREVOR,FM_MICHAEL_DAUGHTER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1025", BANK_ACCOUNT_TREVOR)   
     
    //story characters
    Fill_Character_SheetNRM (CHAR_NRM_AMANDA, IG_AMANDATOWNLEY, 150, "CELL_126", "CELL_326", "CELL_426", PC_NRM_AMANDA,   NO_EMAIL_CONTACT, "AMA_APH1",
        FR_AMANDA, FM_MICHAEL_WIFE, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1026")
    
    //Virtual multiplayer character
    Fill_Character_SheetNRM(CHAR_NRM_MULT, A_M_Y_BeachVesp_01, 240, "CELL_197", "CELL_195", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //No phone number required.
         
    Fill_Character_SheetNRM (CHAR_NRM_AMMUNATION, A_M_Y_BeachVesp_01, 113, "CELL_E_220", "CELL_E_320", "CELL_400", PC_NRM_AMMUNATION,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_01")

    //March MCW  - maybe put special number in phone state enum? e.g Not_LISTED, LISTED, SPECIAL_NUMBERS
    Fill_Character_SheetNRM (CHAR_NRM_CALL911, A_M_Y_BeachVesp_01, 560, "CELL_131", "CELL_331", "CELL_431", PC_NRM_CALL911,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_9011") //Should have low alpha int to appear at top if included in contacts list.
        
    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CHAR_NRM_CALL911].StatusAsCaller[0] = KNOWN_CALLER
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CHAR_NRM_CALL911].StatusAsCaller[1] = KNOWN_CALLER
    g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[CHAR_NRM_CALL911].StatusAsCaller[2] = KNOWN_CALLER
         
    Fill_Character_SheetNRM (CHAR_NRM_TAXI, A_M_Y_BeachVesp_01, 480, "CELL_163", "CELL_394", "CELL_446", PC_NRM_TAXI,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1062")        
    //BLOCKED contact - used in assassin missions
    Fill_Character_SheetNRM(CHAR_NRM_BLOCKED, A_M_Y_BeachVesp_01, 240, "CELL_195", "CELL_300", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
    //Dummy used specifically for human player to human player chat call.
    Fill_Character_SheetNRM (CHAR_NRM_CHAT_CALL, A_M_Y_BeachVesp_01, 90, "CELL_E_219", "CELL_E_319", "CELL_431", PC_NRM_CHAT_CALL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //This should never be added 
    
    Fill_Character_SheetNRM (CHAR_NRM_BLANK_ENTRY, A_M_Y_BeachVesp_01, 320, "CELL_196", "CELL_300", "CELL_196", PC_NRM_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")

    Fill_Character_SheetNRM (MAX_NRM_CHARACTERS, A_M_Y_BeachVesp_01, 100, "CELL_181", "CELL_300", "CELL_196", PC_NRM_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")     
        
    Fill_All_Phone_SettingsNRM()    

ENDPROC
#ENDIF


PROC Initialise_CharSheet_Global_Variables_On_Startup()      
    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING("Initialising CharSheet from charsheet_public.sch prior to reload of save data...")
        PRINTNL()
    #endif

    //Player characters...
    Fill_Character_Sheet (CHAR_MICHAEL, PLAYER_ZERO, 1360, "CELL_101", "CELL_301", "CELL_401", PC_MICHAEL, EMAIL_MICHAEL, "NO_ANSMSG", 
        FR_MICHAEL,FM_TREVOR_0_MICHAEL,NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1001", BANK_ACCOUNT_MICHAEL)
    
    Fill_Character_Sheet (CHAR_TREVOR, PLAYER_TWO, 2070, "CELL_102", "CELL_302", "CELL_402", PC_TREVOR, EMAIL_TREVOR, "NO_ANSMSG",
        FR_TREVOR,FM_TREVOR_0_TREVOR, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1002", BANK_ACCOUNT_TREVOR)
    
    Fill_Character_Sheet (CHAR_FRANKLIN, PLAYER_ONE, 670, "CELL_103", "CELL_303", "CELL_403", PC_FRANKLIN, EMAIL_FRANKLIN, "NO_ANSMSG",
        FR_FRANKLIN,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1003", BANK_ACCOUNT_FRANKLIN)
    
    //Virtual multiplayer character
    Fill_Character_Sheet(CHAR_MULTIPLAYER, A_M_Y_BeachVesp_01, 240, "CELL_197", "CELL_195", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //No phone number required.

    //Conference Calls.
    //Steve calls with either Michael or Trevor on the line at the same time in the FBI Officer's strand.
    Fill_Character_Sheet (CHAR_STEVE_MIKE_CONF, DUMMY_MODEL_FOR_SCRIPT, 340, "CELL_189","CELL_389", "CELL_489", PC_MIKE_TREV_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //No dedicated number.

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_MIKE_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_MIKE_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_MIKE_CONF, 2, KNOWN_CALLER)

    Fill_Character_Sheet (CHAR_STEVE_TREV_CONF, DUMMY_MODEL_FOR_SCRIPT, 350, "CELL_190","CELL_390", "CELL_489", PC_MIKE_TREV_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //No dedicated number.

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_TREV_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_TREV_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_STEVE_TREV_CONF, 2, KNOWN_CALLER)

    //Michael could call Franklin and Trevor
    Fill_Character_Sheet (CHAR_FRANK_TREV_CONF, DUMMY_MODEL_FOR_SCRIPT, 370, "CELL_191", "CELL_391", "CELL_489", PC_FRANK_TREV_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //Shouldn't have a dedicated number 

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_FRANK_TREV_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_FRANK_TREV_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_FRANK_TREV_CONF, 2, KNOWN_CALLER)

    //Lester and Franklin
    Fill_Character_Sheet (CHAR_LEST_FRANK_CONF, DUMMY_MODEL_FOR_SCRIPT, 373, "CELL_E_277", "CELL_E_382", "CELL_489", PC_LEST_FRANK_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //Shouldn't have a dedicated number 

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 2, KNOWN_CALLER)

    //Lester and Michael
    Fill_Character_Sheet (CHAR_LEST_MIKE_CONF, DUMMY_MODEL_FOR_SCRIPT, 375, "CELL_E_278", "CELL_E_383", "CELL_489", PC_LEST_MIKE_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //Shouldn't have a dedicated number 

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LEST_MIKE_CONF, 2, KNOWN_CALLER)
    
    //Trevor could call Michael and Franklin
    Fill_Character_Sheet (CHAR_MIKE_FRANK_CONF, DUMMY_MODEL_FOR_SCRIPT, 380, "CELL_192", "CELL_392", "CELL_489", PC_MIKE_FRANK_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //No dedicated number

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_FRANK_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_FRANK_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_FRANK_CONF, 2, KNOWN_CALLER)

    
    //Franklin could call Michael and Trevor
    Fill_Character_Sheet (CHAR_MIKE_TREV_CONF, DUMMY_MODEL_FOR_SCRIPT, 390, "CELL_193","CELL_393", "CELL_489", PC_MIKE_TREV_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //No dedicated number.

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_TREV_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_TREV_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_MIKE_TREV_CONF, 2, KNOWN_CALLER)




    //Special ID for when arbitrary subsets of the player characters may be involved in a call.
    Fill_Character_Sheet (CHAR_ALL_PLAYERS_CONF, DUMMY_MODEL_FOR_SCRIPT, 360, "CELL_199", "CELL_396", "CELL_489", PC_ALL_PLAYERS, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //Shouldn't have a dedicated number
        
    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".	
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_ALL_PLAYERS_CONF, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_ALL_PLAYERS_CONF, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_ALL_PLAYERS_CONF, 2, KNOWN_CALLER)
    





    //FBI dude.
    Fill_Character_Sheet (CHAR_LESTER, IG_LESTERCREST, 1240, "CELL_111", "CELL_311", "CELL_411", PC_LESTER,   NO_EMAIL_CONTACT, "LES_APH2",
        NO_FRIEND,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1011")
    
    Fill_Character_Sheet (CHAR_LESTER_DEATHWISH, IG_LESTERCREST, 1240, "CELL_E_211", "CELL_E_311", "CELL_411", PC_LESTER, NO_EMAIL_CONTACT, "LES_APH1",
        NO_FRIEND,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1048")

              
              
              
    //Dial_a_Sub!
    Fill_Character_Sheet (CHAR_DIAL_A_SUB, DUMMY_MODEL_FOR_SCRIPT, 1950, "CELL_E_212", "CELL_E_312", "CELL_411", PC_DIAL_A_SUB, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1054")
        
    //Changing third parameter for alphabetical sorting. "Los Santos Talent" will now be referred to as "Acting Up" on the phone.
    Fill_Character_Sheet (CHAR_DIRECTOR, DUMMY_MODEL_FOR_SCRIPT, 120, "CELL_E_284", "CELL_E_323", "CELL_E_385", PC_DIRECTOR, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELLC_DIRECTOR")

    //Chop
    Fill_Character_Sheet (CHAR_CHOP, A_C_CHOP, 361, "CELL_E_225", "CELL_386", "CELL_411", PC_CHOP, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_32")

    Fill_Character_Sheet (CHAR_ISAAC, DUMMY_MODEL_FOR_SCRIPT, 980, "CELL_E_283", "CELL_E_322", "CELL_E_385", PC_NOIR, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1006")

    // Story characters
    Fill_Character_Sheet (CHAR_JIMMY, IG_JIMMYDISANTO, 1030, "CELL_124", "CELL_324", "CELL_424", PC_JIMMY,   NO_EMAIL_CONTACT, "JIM_APH1",
        FR_JIMMY, FM_MICHAEL_SON, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1024")
    
    Fill_Character_Sheet (CHAR_TRACEY, IG_TRACYDISANTO, 2050, "CELL_125", "CELL_325", "CELL_425", PC_TRACEY,   NO_EMAIL_CONTACT, "TRA_APH1",
        NO_FRIEND, FM_MICHAEL_DAUGHTER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1025")
    
    //Needs contact pic, using default via CELL_300
    Fill_Character_Sheet (CHAR_ABIGAIL, IG_ABIGAIL, 110, "CELL_E_240", "CELL_E_377", "CELL_400", PC_ABIGAIL,   NO_EMAIL_CONTACT, "ABI_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_27")


    Fill_Character_Sheet (CHAR_AMANDA, IG_AMANDATOWNLEY, 150, "CELL_126", "CELL_326", "CELL_426", PC_AMANDA,   NO_EMAIL_CONTACT, "AMA_APH1",
        FR_AMANDA, FM_MICHAEL_WIFE, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1026")
    //Answer message added 19/06/13 for MP
    Fill_Character_Sheet (CHAR_SIMEON, IG_SIEMONYETARIAN, 1930, "CELL_127", "CELL_327", "CELL_427", PC_SIMEON,   NO_EMAIL_CONTACT, "ANS_Sbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1027")
    
    Fill_Character_Sheet (CHAR_LAMAR, IG_LAMARDAVIS, 1210, "CELL_128", "CELL_328", "CELL_428", PC_LAMAR,   NO_EMAIL_CONTACT, "LAM_APH1",
        FR_LAMAR,FM_FRANKLIN_LAMAR, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1028")
        
    Fill_Character_Sheet (CHAR_RON, IG_NERVOUSRON, 1880, "CELL_129", "CELL_329", "CELL_429", PC_RON,   NO_EMAIL_CONTACT, "RON_APH1",
        NO_FRIEND, FM_TREVOR_0_RON, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1029")
        
    Fill_Character_Sheet (CHAR_CHENG, IG_TAOCHENG, 340, "CELL_133", "CELL_332", "CELL_432", PC_CHENG,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1033")

	//Was CHAR_SAEEDA but has been renamed for bug 2853732 to use as a combined phonecall contact for the Stunt pack.
    Fill_Character_Sheet (CHAR_CEOASSIST, IG_TAOCHENG, 320, "CELL_CEOA", "CELL_AGBOSSPIC", "CELL_432", PC_SAEEDA,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_12")


        
    Fill_Character_Sheet (CHAR_STEVE, IG_STEVEHAINS, 1970, "CELL_134", "CELL_333", "CELL_433", PC_STEVE,   NO_EMAIL_CONTACT, "STE_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1034")
        
    Fill_Character_Sheet (CHAR_WADE, IG_WADE, 2300, "CELL_135", "CELL_334", "CELL_434", PC_WADE,   NO_EMAIL_CONTACT, "WAD_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1035")
        
    Fill_Character_Sheet (CHAR_TENNIS_COACH, IG_TENNISCOACH, 2020, "CELL_136", "CELL_335", "CELL_435", PC_TENNIS_COACH,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1036")
        
    Fill_Character_Sheet (CHAR_SOLOMON, IG_SOLOMON, 1950, "CELL_137", "CELL_336", "CELL_436", PC_SOLOMON,   NO_EMAIL_CONTACT, "SOL_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1037")
        
    Fill_Character_Sheet (CHAR_LAZLOW, IG_LAZLOW, 1220, "CELL_138", "CELL_337", "CELL_437", PC_LAZLOW,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND,NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1038")
        
    Fill_Character_Sheet (CHAR_ESTATE_AGENT, A_M_Y_BUSINESS_01, 560, "CELL_139", "CELL_338", "CELL_438", PC_ESTATE_AGENT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1039")
        
    Fill_Character_Sheet (CHAR_DEVIN, IG_DEVIN, 450, "CELL_142", "CELL_342", "CELL_442", PC_DEVIN,   NO_EMAIL_CONTACT, "DEV_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1042")
        
    Fill_Character_Sheet (CHAR_DAVE, IG_DAVENORTON, 420, "CELL_143", "CELL_343", "CELL_443", PC_DAVE,   NO_EMAIL_CONTACT, "DAV_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1043")
    //CDM 19/6/13 added answer message for MP    
    Fill_Character_Sheet (CHAR_MARTIN, A_M_Y_BUSINESS_01, 1330, "CELL_144", "CELL_344", "CELL_444", PC_MARTIN,   NO_EMAIL_CONTACT, "ANS_MMbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1044")
        
    Fill_Character_Sheet (CHAR_FLOYD, IG_FLOYD, 650, "CELL_145", "CELL_345", "CELL_445", PC_FLOYD,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1074")
        
    Fill_Character_Sheet (CHAR_GAYMILITARY, A_M_Y_BUSINESS_01, 1380, "CELL_146", "CELL_346", "CELL_401", PC_GAYMILITARY,   NO_EMAIL_CONTACT, "MIL_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1045")
        
    Fill_Character_Sheet (CHAR_OSCAR, G_M_Y_MexGoon_02, 1590, "CELL_164", "CELL_363", "CELL_400", PC_OSCAR,   NO_EMAIL_CONTACT, "OSC_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1063")

    Fill_Character_Sheet (CHAR_CHENGSR, IG_CHENGSR, 355, "CELL_200", "CELL_395", "CELL_494", PC_CHENGSR,   NO_EMAIL_CONTACT, "CHE_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1066")
        
    Fill_Character_Sheet (CHAR_DR_FRIEDLANDER, IG_DRFRIEDLANDER, 492, "CELL_121", "CELL_397", "CELL_496", PC_DR_FRIEDLANDER,   NO_EMAIL_CONTACT, "SHR_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1067")
        
    Fill_Character_Sheet (CHAR_STRETCH, IG_STRETCH, 1977, "CELL_122", "CELL_322", "CELL_422", PC_STRETCH,   NO_EMAIL_CONTACT, "STR_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1068")
        
    Fill_Character_Sheet (CHAR_ORTEGA, IG_ORTEGA, 1580, "CELL_123", "CELL_323", "CELL_423", PC_ORTEGA,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1069")
        
    Fill_Character_Sheet (CHAR_ONEIL, A_M_M_FARMER_01, 1570, "CELL_E_208", "CELL_381", "CELL_400", PC_ONEIL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1059")
        
    Fill_Character_Sheet (CHAR_PATRICIA, IG_PATRICIA, 1620 , "CELL_E_210", "CELL_382", "CELL_400", PC_PATRICIA,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1071")
        
    Fill_Character_Sheet (CHAR_TANISHA, IG_TANISHA, 2010 , "CELL_E_218", "CELL_384", "CELL_400", PC_TANISHA,   NO_EMAIL_CONTACT, "TAN_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1073")
        
    Fill_Character_Sheet (CHAR_DENISE, IG_DENISE, 440 , "CELL_E_226", "CELL_387", "CELL_400", PC_DENISE,   NO_EMAIL_CONTACT, "DEN_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1074")
         
    Fill_Character_Sheet (CHAR_MOLLY, IG_MOLLY, 1360 , "CELL_E_227", "CELL_388", "CELL_400", PC_MOLLY,   NO_EMAIL_CONTACT, "MOL_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1076")
                
    Fill_Character_Sheet (CHAR_RICKIE, IG_LIFEINVAD_01, 1860 , "CELL_E_217", "CELL_383", "CELL_400", PC_RICKIE,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1072")
        
    Fill_Character_Sheet (CHAR_CHEF, IG_CHEF, 330 , "CELL_E_224", "CELL_385", "CELL_400", PC_CHEF,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_28")
        
    //Random Characters.
    Fill_Character_Sheet (CHAR_BARRY, IG_BARRY, 200, "CELL_147", "CELL_347", "CELL_400", PC_BARRY,   NO_EMAIL_CONTACT, "BAR_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1046")
        
    Fill_Character_Sheet (CHAR_BEVERLY, IG_BEVERLY, 220, "CELL_148", "CELL_348", "CELL_400", PC_BEVERLY,   NO_EMAIL_CONTACT, "BEV_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1047")

    // 2063915 - set correct blimp contact pic depending on if using CGtoNG
    IF IS_LAST_GEN_PLAYER()
        Fill_Character_Sheet (CHAR_BLIMP, IG_BEVERLY, 250, "CELL_E_279", "CELL_E_379", "CELL_400", PC_BLIMP,   NO_EMAIL_CONTACT, "NO_ANSMSG",
            NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_40")
    ELSE
        Fill_Character_Sheet (CHAR_BLIMP, IG_BEVERLY, 250, "CELL_E_279", "CELL_E_386", "CELL_400", PC_BLIMP,   NO_EMAIL_CONTACT, "NO_ANSMSG",
            NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_40")
    ENDIF


        
    Fill_Character_Sheet (CHAR_CRIS, S_M_M_HIGHSEC_01, 380, "CELL_166", "CELL_365", "CELL_448", PC_CRIS,   NO_EMAIL_CONTACT, "CRI_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1075")
    //CDM 19/6/13 added answer message for MP   
    Fill_Character_Sheet (CHAR_DOM, IG_DOM, 450, "CELL_150", "CELL_350", "CELL_400", PC_DOM,   NO_EMAIL_CONTACT, "ANS_Dbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1049")
     
     // TODO whichNumberLabel doesn't have a new number setup yet.
    Fill_Character_Sheet (CHAR_HAO, IG_HAO, 800, "CELL_E_246", "CELL_E_346", "CELL_400", PC_HAO,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1049")
        
    //Known as "Cletus"
    Fill_Character_Sheet (CHAR_HUNTER, IG_CLETUS, 370, "CELL_167", "CELL_366", "CELL_449", PC_HUNTER,   NO_EMAIL_CONTACT, "CLE_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1076")
    
    Fill_Character_Sheet (CHAR_JIMMY_BOSTON, IG_JIMMYBOSTON, 1040, "CELL_151", "CELL_357", "CELL_400", PC_JIMMY_BOSTON,   NO_EMAIL_CONTACT, "JIB_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1050")
        
    Fill_Character_Sheet (CHAR_JOE, IG_JOEMINUTEMAN, 1060, "CELL_152", "CELL_352", "CELL_400", PC_JOE,   NO_EMAIL_CONTACT, "JOE_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1051")
        
    Fill_Character_Sheet (CHAR_JOSEF, IG_JOSEF, 1070, "CELL_153", "CELL_353", "CELL_400", PC_JOSEF,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1052")
        
    Fill_Character_Sheet (CHAR_JOSH, IG_JOSH, 1080, "CELL_154", "CELL_354", "CELL_400", PC_JOSH,   NO_EMAIL_CONTACT, "JOS_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1053")
                
    Fill_Character_Sheet (CHAR_MANUEL, IG_MANUEL, 1310, "CELL_156", "CELL_356", "CELL_400", PC_MANUEL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1055")
        
    Fill_Character_Sheet (CHAR_MARNIE, IG_MARNIE, 1320, "CELL_157", "CELL_E_310", "CELL_400", PC_MARNIE,   NO_EMAIL_CONTACT, "MAR_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1056")
    
    Fill_Character_Sheet (CHAR_MARY_ANN, IG_MARYANN, 1340, "CELL_158", "CELL_358", "CELL_400", PC_MARY_ANN,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1057")

    // TODO whichNumberLabel doesn't have a new number setup yet.
    Fill_Character_Sheet (CHAR_MAUDE, IG_MAUDE, 1340, "CELL_E_244", "CELL_E_344", "CELL_400", PC_MAUDE,   NO_EMAIL_CONTACT, "MAU_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1057")
        
    Fill_Character_Sheet (CHAR_MONKEY_MAN, DUMMY_MODEL_FOR_SCRIPT, 1360, "CELL_E_282", "CELL_300", "CELL_E_384", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1005")
        
    Fill_Character_Sheet (CHAR_MRS_THORNHILL, IG_MRS_THORNHILL, 1390, "CELL_161", "CELL_361", "CELL_400", PC_MRS_THORNHILL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1060")
        
    Fill_Character_Sheet (CHAR_NIGEL, IG_NIGEL, 1430, "CELL_162", "CELL_362", "CELL_400", PC_NIGEL,   NO_EMAIL_CONTACT, "NIG_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1061")
        
    Fill_Character_Sheet (CHAR_SASQUATCH, IG_ORLEANS, 1910, "CELL_168", "CELL_367", "CELL_450", PC_SASQUATCH,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1077")
        
    Fill_Character_Sheet (CHAR_ASHLEY, IG_ASHLEY, 178, "CELL_E_202", "CELL_368", "CELL_400", PC_ASHLEY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1070")

    Fill_Character_Sheet (CHAR_ANDREAS, IG_ANDREAS, 180, "CELL_E_205", "CELL_369", "CELL_400", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")


    Fill_Character_Sheet (CHAR_DREYFUSS, IG_DREYFUSS, 490, "CELL_E_206", "CELL_398", "CELL_400", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1078")

    Fill_Character_Sheet (CHAR_OMEGA, IG_OMEGA, 1540, "CELL_E_207", "CELL_399", "CELL_400", PC_OMEGA,   NO_EMAIL_CONTACT, "OME_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1079")



    Fill_Character_Sheet (CHAR_DOMESTIC_GIRL, A_M_Y_BeachVesp_01, 480, "CELL_140", "CELL_340", "CELL_401", PC_DOMESTIC_GIRL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1040")

    //Now known as Ursula
    Fill_Character_Sheet (CHAR_HITCHER_GIRL, A_F_Y_HIKER_01, 2170, "CELL_141", "CELL_341", "CELL_401", PC_HITCHER_GIRL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1041")
        
    Fill_Character_Sheet (CHAR_SOCIAL_CLUB, DUMMY_MODEL_FOR_SCRIPT, 1960, "CELL_179", "CELL_379", "CELL_400", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1064")
        
    Fill_Character_Sheet (CHAR_LS_TOURIST_BOARD, DUMMY_MODEL_FOR_SCRIPT, 1290, "CELL_E_245", "CELL_378", "CELL_400", PC_LS_TOURIST,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1075")
        
    Fill_Character_Sheet (CHAR_MECHANIC, S_M_Y_XMECH_02, 1360, "CELL_180", "CELL_380", "CELL_400", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1065")
		
    //These might need unique phone number tokens.
    Fill_Character_Sheet (CHAR_STRIPPER_JULIET, S_F_Y_STRIPPER_01, 1090, "CELL_112", "CELL_312", "CELL_400", PC_STRIPPER1,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

    Fill_Character_Sheet (CHAR_STRIPPER_NIKKI, S_F_Y_STRIPPER_02, 1460, "CELL_113", "CELL_313", "CELL_413", PC_STRIPPER2,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1081")
    
    Fill_Character_Sheet (CHAR_STRIPPER_CHASTITY, S_F_Y_STRIPPER_01, 340, "CELL_114", "CELL_314", "CELL_414", PC_STRIPPER3,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1082")
                                                         
    Fill_Character_Sheet (CHAR_STRIPPER_CHEETAH, S_F_Y_STRIPPER_02, 350, "CELL_115", "CELL_315", "CELL_415", PC_STRIPPER4,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1083")
    
    Fill_Character_Sheet (CHAR_STRIPPER_SAPPHIRE,S_F_Y_STRIPPER_01, 1910, "CELL_116", "CELL_316", "CELL_416", PC_STRIPPER5,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1084")
    
    Fill_Character_Sheet (CHAR_STRIPPER_INFERNUS, S_F_Y_STRIPPER_02, 940, "CELL_117", "CELL_317", "CELL_417", PC_STRIPPER6,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1085")
    
    Fill_Character_Sheet (CHAR_STRIPPER_FUFU, S_F_Y_STRIPPER_01, 680, "CELL_118", "CELL_318", "CELL_418", PC_STRIPPER7,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1086")
    
    Fill_Character_Sheet (CHAR_STRIPPER_PEACH, S_F_Y_STRIPPER_02, 1630, "CELL_119", "CELL_319", "CELL_419", PC_STRIPPER8,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1087")

    Fill_Character_Sheet (CHAR_BROKEN_DOWN_GIRL, A_F_Y_Fitness_02, 280, "CELL_120", "CELL_320", "CELL_420", PC_BROKEN_DOWN_GIRL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1088")

    Fill_Character_Sheet (CHAR_ANTONIA, A_F_Y_Hipster_01, 190, "CELL_E_280", "CELL_E_321", "CELL_400", PC_BURIAL_ANTONIA,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_41")


    Fill_Character_Sheet (CHAR_TAXI_LIZ, A_F_Y_EASTSA_03, 1260, "CELL_E_201", "CELL_321", "CELL_421", PC_TAXI_LIZ,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1089")
        
        
    // Property Management  
    Fill_Character_Sheet (CHAR_PROPERTY_TAXI_LOT, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_248", "CELL_E_350", "CELL_400", PC_PROPERTY_TAXI_LOT,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
  
    Fill_Character_Sheet (CHAR_PROPERTY_CINEMA_VINEWOOD, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_249", "CELL_E_351", "CELL_400", PC_PROPERTY_CINEMA_VINEWOOD,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
    
    Fill_Character_Sheet (CHAR_PROPERTY_CINEMA_DOWNTOWN, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_250", "CELL_E_352", "CELL_400", PC_PROPERTY_CINEMA_DOWNTOWN,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
        
    Fill_Character_Sheet (CHAR_PROPERTY_CINEMA_MORNINGWOOD, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_251", "CELL_E_353", "CELL_400", PC_PROPERTY_CINEMA_MORNINGWOOD,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
  
    Fill_Character_Sheet (CHAR_PROPERTY_CAR_SCRAP_YARD, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_252", "CELL_E_354", "CELL_400", PC_PROPERTY_CAR_SCRAP_YARD,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
        
    Fill_Character_Sheet (CHAR_PROPERTY_WEED_SHOP, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_254", "CELL_E_356", "CELL_400", PC_PROPERTY_WEED_SHOP,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
  
    Fill_Character_Sheet (CHAR_PROPERTY_BAR_TEQUILALA, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_255", "CELL_E_357", "CELL_400", PC_PROPERTY_BAR_TEQUILALA,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
    
    Fill_Character_Sheet (CHAR_PROPERTY_BAR_PITCHERS, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_256", "CELL_E_358", "CELL_400", PC_PROPERTY_BAR_PITCHERS,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")   
        
    Fill_Character_Sheet (CHAR_PROPERTY_BAR_HEN_HOUSE, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_260", "CELL_E_362", "CELL_400", PC_PROPERTY_BAR_HEN_HOUSE,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

    Fill_Character_Sheet (CHAR_PROPERTY_BAR_HOOKIES, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_262", "CELL_E_364", "CELL_400", PC_PROPERTY_BAR_HOOKIES,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
        
    Fill_Character_Sheet (CHAR_PROPERTY_GOLF_CLUB, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_263", "CELL_E_366", "CELL_400", PC_PROPERTY_GOLF_CLUB,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

    Fill_Character_Sheet (CHAR_PROPERTY_CAR_MOD_SHOP, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_264", "CELL_E_367", "CELL_400", PC_PROPERTY_CAR_MOD_SHOP,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")
        
    Fill_Character_Sheet (CHAR_PROPERTY_TOWING_IMPOUND, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_265", "CELL_E_368", "CELL_400", PC_PROPERTY_TOWING_IMPOUND,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

    Fill_Character_Sheet (CHAR_PROPERTY_ARMS_TRAFFICKING, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_266", "CELL_E_369", "CELL_400", PC_PROPERTY_ARMS_TRAFFICKING,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

    Fill_Character_Sheet (CHAR_PROPERTY_SONAR_COLLECTIONS, DUMMY_MODEL_FOR_SCRIPT, 1, "CELL_E_267", "CELL_E_370", "CELL_400", PC_PROPERTY_SONAR_COLLECTIONS,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1080")

        
    // Towing ped, Tonya
    Fill_Character_Sheet (CHAR_TOW_TONYA, IG_TONYA, 2040, "CELL_E_223", "CELL_370", "CELL_470", PC_TOW_TONYA,   NO_EMAIL_CONTACT, "TON_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1090")


    //Special Number - see bug 1020339
    Fill_Character_Sheet (CHAR_LS_CUSTOMS, DUMMY_MODEL_FOR_SCRIPT, 1270, "CELL_E_209", "CELL_E_309", "CELL_400", PC_LS_CUSTOMS,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1091") //

    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LS_CUSTOMS, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LS_CUSTOMS, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LS_CUSTOMS, 2, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_LS_CUSTOMS, 3, KNOWN_CALLER)


        
    Fill_Character_Sheet (CHAR_AMMUNATION, DUMMY_MODEL_FOR_SCRIPT, 113, "CELL_E_220", "CELL_E_320", "CELL_400", PC_AMMUNATION,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_01")




    //MP Contacts
    // ...bosses
    // Lost: Al Carter (which is also the name of the Lost Mechanic so slight discrepency there)
    Fill_Character_Sheet (CHAR_MP_BIKER_BOSS, A_M_Y_BeachVesp_01, 150, "CELL_174", "CELL_MP_320", "CELL_400", PC_MP_BIKERBOSS,   NO_EMAIL_CONTACT, "ANS_Lbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_02")
    
    Fill_Character_Sheet (CHAR_MP_FAM_BOSS, A_M_Y_BeachVesp_01, 610, "CELL_176", "CELL_MP_322", "CELL_400", PC_MP_FAMBOSS,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_04")
    
    // Vagos: Edgar Claros
    Fill_Character_Sheet (CHAR_MP_MEX_BOSS, A_M_Y_BeachVesp_01, 520, "CELL_177", "CELL_MP_323", "CELL_400", PC_MP_MEXBOSS,   NO_EMAIL_CONTACT, "ANS_Vbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_05")
    
    Fill_Character_Sheet (CHAR_MP_PROF_BOSS, A_M_Y_BeachVesp_01, 1670, "CELL_178", "CELL_MP_324", "CELL_400", PC_MP_PROFBOSS,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_06")
    
    // ...lieutenants
	
	//Was CHAR_MP_MEX_LT.  Now reusing for the female Personal Assistant, CHAR_MP_P_ASSISTF, for executive pack. See 2769151.
    Fill_Character_Sheet (CHAR_MP_P_ASSISTF, A_M_Y_BeachVesp_01, 180, "CELL_P_ASSISTF", "CELL_P_AF_PIC", "CELL_400", PC_MP_MEXLT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_08")
    
	//Was CHAR_MP_BIKER_MECHANIC, now used as male Personal Assistant contact for 2686174
    Fill_Character_Sheet (CHAR_MP_P_ASSIST, A_M_Y_BeachVesp_01, 190, "CELL_P_ASSIST", "CELL_P_A_PIC", "CELL_400", PC_MP_BIKERMECHANIC,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_09")

     
    // ...docks contacts
    Fill_Character_Sheet (CHAR_MP_AGENT_14, INT_TO_ENUM(MODEL_NAMES, HASH("IG_AGENT14")), 150, "CELL_165", "CELL_MP_A14PIC", "CELL_400", PC_MP_AGENT_14,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_11")

    Fill_Character_Sheet (CHAR_MP_STRETCH, A_M_Y_BeachVesp_01, 1970, "CELL_172", "CELL_MP_331", "CELL_400", PC_MP_STRETCH,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_13")
        
    // MP Freemode
    // ...general contact (used in intro)
    Fill_Character_Sheet (CHAR_MP_FM_CONTACT, A_M_Y_BeachVesp_01, 1220, "CELL_E_215", "CELL_MP_342", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_14")
        
    // ...brucie (used as a special ability contact)
    Fill_Character_Sheet (CHAR_MP_BRUCIE, A_M_Y_BeachVesp_01, 280, "CELL_E_216", "CELL_MP_343", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "ANS_Bbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_25")
        
    // ...merryweather (used as a special ability contact)  
    Fill_Character_Sheet (CHAR_MP_MERRYWEATHER, A_M_Y_BeachVesp_01, 1349, "CELL_E_221", "CELL_MP_344", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "ANS_MWbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_26")
        
    // ...gerald (used as a Contact Mission contact)  
    Fill_Character_Sheet (CHAR_MP_GERALD, A_M_Y_BeachVesp_01, 823, "CELL_E_228", "CELL_MP_349", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "ANS_GBusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1004")
     
     // ...mechanic (used as a special ability contact) //Los Santos Customs at present.
    Fill_Character_Sheet (CHAR_MP_MECHANIC, A_M_Y_BeachVesp_01, 1343, "CELL_E_MP0", "CELL_MP_345", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "ANS_MECbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_29")    
        
     // ...Julio Fabrizio
    Fill_Character_Sheet (CHAR_MP_JULIO, A_M_Y_BeachVesp_01, 1060, "CELL_E_242", "CELL_MP_346", "CELL_400", PC_MP_FM_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_30")    

    // ...Strip Club PR  //Listed as promotion@V-Unicorn.
    Fill_Character_Sheet (CHAR_MP_STRIPCLUB_PR, A_M_Y_BeachVesp_01, 1670, "CELL_E_243", "CELL_MP_347", "CELL_400", PC_MP_STRIPCLUB_PR,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_31")
    
    // ...snitches
    // KGM 28/3/12: Generic MP Snitch - Corey Parker
    Fill_Character_Sheet (CHAR_MP_SNITCH, A_M_Y_BeachVesp_01, 380, "CELL_169", "CELL_MP_332", "CELL_400", PC_MP_SNITCH,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_16")


    
   

    // ...other mission flow contacts
    //This CHAR_ENUM was originally CHAR_MP_FIB_CONTACT but have renamed for 2568241
    Fill_Character_Sheet (CHAR_YACHT_CAPTAIN, A_M_Y_BeachVesp_01, 310, "CELL_YACHT", "CELL_YACHTPIC", "CELL_400", PC_MP_FIB_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_20")
    
    Fill_Character_Sheet (CHAR_MP_ARMY_CONTACT, A_M_Y_BeachVesp_01, 170, "CELL_185", "CELL_MP_337", "CELL_400", PC_MP_ARMY_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_21")



    //Pegasus vehicle delivery service - see bug #1300310
    Fill_Character_Sheet (CHAR_PEGASUS_DELIVERY, DUMMY_MODEL_FOR_SCRIPT, 1640, "CELL_E_247", "CELL_E_347", "CELL_400", PC_PEGASUS_DELIVERY,   NO_EMAIL_CONTACT, "ANS_PGbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_33")



    Fill_Character_Sheet (CHAR_LIFEINVADER, DUMMY_MODEL_FOR_SCRIPT, 1240, "CELL_E_276", "CELL_E_376", "CELL_400", PC_LIFEINVADER,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_39")




    //This CHAR_ENUM was originally unused CHAR_MP_ROBERTO but have renamed for 2519183
    Fill_Character_Sheet (CHAR_BENNYS_OMW, A_M_Y_BeachVesp_01, 240, "CELL_BENNY", "CELL_BENNYPIC", "CELL_400", PC_MP_ROBERTO,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_23")


    //Reusing this character as the Agency Boss for TODO 2511934. CELL_AGBOSS is the name of the character that appears on the cellphone.
    Fill_Character_Sheet (CHAR_MP_RAY_LAVOY, A_M_Y_BeachVesp_01, 250, "CELL_AGBOSS", "CELL_AGBOSSPIC", "CELL_400", PC_MP_RAY_LAVOY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_24")
    


    //MP version of Detonation for use in Cellphone - "Detonate"
    Fill_Character_Sheet (CHAR_MP_DETONATEPHONE, A_M_Y_BeachVesp_01, 450, "CELL_E_222", "CELL_330", "CELL_430", PC_DETONATEPHONE,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
        
    //MP insurance company
    Fill_Character_Sheet (CHAR_MP_MORS_MUTUAL, A_M_Y_BeachVesp_01, 1370, "CELL_E_275", "CELL_MP_348", "CELL_400", PC_MP_MORS_MUTUAL,   NO_EMAIL_CONTACT, "ANS_INSbusy",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_38") 


    //MP vehicle site transaction message sources
    Fill_Character_Sheet (CHAR_CARSITE, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_271", "CELL_MP_352", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_34")
    Fill_Character_Sheet (CHAR_PLANESITE, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_272", "CELL_MP_355", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_35")
    Fill_Character_Sheet (CHAR_MILSITE, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_273", "CELL_MP_353", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_36")
    Fill_Character_Sheet (CHAR_BOATSITE, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_274", "CELL_MP_351", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_37")
    Fill_Character_Sheet (CHAR_BIKESITE_PAMC, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_269", "CELL_MP_354", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_42")
    Fill_Character_Sheet (CHAR_CARSITE_SSASA, DUMMY_MODEL_FOR_SCRIPT, 1810, "CELL_E_270", "CELL_MP_350", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_43")

    
    //MP Biker Clubhouse Contacts.  -  Added these as brand new entries, rather than a reuse, for TODO 2945259. Have added some new mobile numbers for these, starting from CELL_MN16_x
	//Current display names for contacts are - BIKER_CH1 is "Malc", BIIKER_CH2 is "LJT". Need to update alphabetical order if these change.
	Fill_Character_Sheet (CHAR_BIKER_CH1, DUMMY_MODEL_FOR_SCRIPT, 1320, "CELL_CH_BIK1", "CELL_BIK1_PIC", "CELL_400", PC_BIKER_CH1, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_1")
    
	Fill_Character_Sheet (CHAR_BIKER_CH2, DUMMY_MODEL_FOR_SCRIPT, 1260, "CELL_CH_BIK2", "CELL_BIK2_PIC", "CELL_400", PC_BIKER_CH2, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_2")
    
	Fill_Character_Sheet (CHAR_MAZE_MPEMAIL, DUMMY_MODEL_FOR_SCRIPT, 1310, "CELL_MAZENAME", "CELL_MAZEPIC", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_2")
	
	Fill_Character_Sheet (CHAR_MAPSENDER, DUMMY_MODEL_FOR_SCRIPT, 1390, "CELL_TREASNAME", "CELL_MAPPIC", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_2")



	//Nightclub DLC Contacts - See 4601562. 
	Fill_Character_Sheet (CHAR_NCLUBT, DUMMY_MODEL_FOR_SCRIPT, 2050, "CELL_NCLUBT_N", "CELL_NCLUBT_PIC", "CELL_400", PC_NCLUBT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_1")

	Fill_Character_Sheet (CHAR_NCLUBL, DUMMY_MODEL_FOR_SCRIPT, 1210, "CELL_NCLUBL_N", "CELL_NCLUBL_PIC", "CELL_400", PC_NCLUBL, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_1")

	Fill_Character_Sheet (CHAR_NCLUBE, DUMMY_MODEL_FOR_SCRIPT, 560, "CELL_NCLUBE_N", "CELL_NCLUBE_PIC", "CELL_400", PC_NCLUBE, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_1")


	//Business Battles. Hacker Truck. Paige. See 4826130	
	Fill_Character_Sheet (CHAR_BBPAIGE, DUMMY_MODEL_FOR_SCRIPT, 1610, "CELL_BBPAIGE_N", "CELL_BBPAIGE_P", "CELL_400", PC_BBPAIGE, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_1")


	//Arena Wars. See 5345719	
	Fill_Character_Sheet (CHAR_ARENA, DUMMY_MODEL_FOR_SCRIPT, 1710, "CELL_ARENA_N", "CELL_ARENA_PIC", "CELL_400", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_3")
	

	//Arena Wars. Bryony. See 4826130	
	Fill_Character_Sheet (CHAR_BRYONY, DUMMY_MODEL_FOR_SCRIPT, 280, "CELL_BRYONY_N", "CELL_BRYONY_P", "CELL_400", PC_BRYONY, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_4")
		
	#IF FEATURE_CASINO
	Fill_Character_Sheet (CHAR_CASINO_MANAGER, DUMMY_MODEL_FOR_SCRIPT, 1390, "CELL_CAS_MAN_N", "CELL_CAS_MAN_P", "CELL_400", PC_CASINO_MANAGER, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_5")
	Fill_Character_Sheet (CHAR_CASINO_TOMCONNORS, DUMMY_MODEL_FOR_SCRIPT, 2090, "CELL_CAS_TOM_N", "CELL_CAS_TOM_P", "CELL_400", PC_CHAR_BLANK_ENTRY, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_5")
	#ENDIF
	
	Fill_Character_Sheet (CHAR_COMIC_STORE, DUMMY_MODEL_FOR_SCRIPT, 1260, "CELL_COMIC_N", "CELL_COMIC_P", "CELL_400", PC_HARDCORE_COMIC_STORE, NO_EMAIL_CONTACT, "NO_ANSMG",
      NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN17")
	
	Fill_Character_Sheet (CHAR_CASINO_MPEMAIL, DUMMY_MODEL_FOR_SCRIPT, 1400, "CELL_CASINONAME", "CELL_CASINOPIC", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN17_1")
		
	Fill_Character_Sheet (CHAR_CASINO_TAO_TRANSLATOR, DUMMY_MODEL_FOR_SCRIPT, 2060, "CELL_CAS_TTR_N", "CELL_CAS_TTR_P", "CELL_400", PC_CHAR_BLANK_ENTRY, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_5")
		
	#IF FEATURE_CASINO_HEIST
		Fill_Character_Sheet (CHAR_ARCADE_WENDY, DUMMY_MODEL_FOR_SCRIPT, 2350, "CELL_WENDY_N", "CELL_WENDY_P", "CELL_400", PC_ARCADE_WENDY, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_6")	
		
		Fill_Character_Sheet (CHAR_ARCADE_CELEB, DUMMY_MODEL_FOR_SCRIPT, 2580, "CELL_CELEB_N", "CELL_CELEB_P", "CELL_400", PC_ARCADE_TW, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_7")	
		
		Fill_Character_Sheet (CHAR_ARCADE_MN, DUMMY_MODEL_FOR_SCRIPT, 1480, "CELL_MN_N", "", "CELL_400", PC_ARCADE_MN, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_8")	
	#ENDIF
	
	#IF FEATURE_CASINO_NIGHTCLUB
		Fill_Character_Sheet(CHAR_KEINEMUSIK, DUMMY_MODEL_FOR_SCRIPT, 1165, "CELL_KM_N", "CELL_KM_P", "CELL_400", PC_KEINEMUSIK, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN19")
		
		Fill_Character_Sheet(CHAR_MOODYMANN, DUMMY_MODEL_FOR_SCRIPT, 1481, "CELL_MM_N", "CELL_MM_P", "CELL_400", PC_MOODYMANN, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN22")	
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
		Fill_Character_Sheet(CHAR_MIGUEL_MADRAZO, DUMMY_MODEL_FOR_SCRIPT, 1365, "CELL_MIGEL_N", "CELL_MIGEL_P", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN18")
		
		Fill_Character_Sheet(CHAR_PAVEL, DUMMY_MODEL_FOR_SCRIPT, 1613, "CELL_PAVEL_N", "CELL_PAVEL_P", "CELL_400", PC_ISLAND_MIGUEL, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN20")
	
		Fill_Character_Sheet(CHAR_STILL_SLIPPIN_RADIO, DUMMY_MODEL_FOR_SCRIPT, 1812, "CELL_SLIP_N", "CELL_SLIP_P", "CELL_400", PC_STILL_SLIPPIN_RADIO, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN21")
	
	#ENDIF
	
	#IF FEATURE_TUNER
		Fill_Character_Sheet(CHAR_SESSANTA, DUMMY_MODEL_FOR_SCRIPT, 1919, "CELL_SES_N", "CELL_SES_P", "CELL_400", PC_SESSANTA, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN23")
		
		Fill_Character_Sheet(CHAR_KDJ, DUMMY_MODEL_FOR_SCRIPT, 1925, "CELL_KDJ_N", "CELL_KDJ_P", "CELL_400", PC_KDJ, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN22")	
		
		Fill_Character_Sheet (CHAR_LS_CAR_MEET_MPEMAIL, DUMMY_MODEL_FOR_SCRIPT, 1400, "CELL_LSCMNAME", "CELL_LSCMPIC", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN29")
	#ENDIF
	
	#IF FEATURE_FIXER
		Fill_Character_Sheet(CHAR_FIXER_FRANKLIN, DUMMY_MODEL_FOR_SCRIPT, 650, "CELL_FRANKLIN_N", "CELL_FRANKLIN_P", "CELL_400", PC_FIXER_FRANKLIN, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN23")
		
		Fill_Character_Sheet(CHAR_FIXER_IMANI, DUMMY_MODEL_FOR_SCRIPT, 940, "CELL_IMANI_N", "CELL_IMANI_P", "CELL_400", PC_FIXER_IMANI, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN24")
		
		Fill_Character_Sheet(CHAR_FIXER_PRODUCER, DUMMY_MODEL_FOR_SCRIPT, 479, "CELL_DRE_N", "CELL_DRE_P", "CELL_400", PC_FIXER_DRE, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN25")
		
		//Franklin and Imani conference
   		 Fill_Character_Sheet (CHAR_FIXER_FRANKLIN_IMANI_CONF, DUMMY_MODEL_FOR_SCRIPT, 2120, "CELL_FRA_IMA_N", "CELL_FRA_IMA_P", "CELL_489", PC_FIXER_FRAKLIN_IMANI_CONF, NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //No dedicated number
		
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	
		Fill_Character_Sheet(CHAR_JUNK_ENERGY, DUMMY_MODEL_FOR_SCRIPT, 785, "CELL_JUNK_EN_N", "CELL_JUNK_EN_P", "CELL_400", PC_JUNK_ENERGY, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet(CHAR_MARCEL, DUMMY_MODEL_FOR_SCRIPT, 1315, "CELL_MARCEL_N", "CELL_MARCEL_P", "CELL_400", PC_MARCEL, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet(CHAR_YOHAN, DUMMY_MODEL_FOR_SCRIPT, 2370, "CELL_YOHAN_N", "CELL_YOHAN_P", "CELL_400", PC_YOHAN, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet(CHAR_ULP, DUMMY_MODEL_FOR_SCRIPT, 160, "CELL_ULP_N", "CELL_ULP_P", "CELL_400", PC_ULP, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet(CHAR_SINDY, DUMMY_MODEL_FOR_SCRIPT, 1920, "CELL_SINDY_N", "CELL_SINDY_P", "CELL_400", PC_SINDY, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet(CHAR_LUPE, DUMMY_MODEL_FOR_SCRIPT, 1280, "CELL_LUPE_N", "CELL_LUPE_P", "CELL_400", PC_LUPE, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN26")
		
		Fill_Character_Sheet (CHAR_LUXURY_AUTOS_MPEMAIL, DUMMY_MODEL_FOR_SCRIPT, 1216, "CELL_LXAUNAME", "CELL_LXAU_P", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN27")
		
		Fill_Character_Sheet (CHAR_PREMIUM_DELUXE_MPEMAIL, DUMMY_MODEL_FOR_SCRIPT, 1618, "CELL_PMDXNAME", "CELL_PMDX_P", "CELL_400", NO_PHONE_CONTACT, NO_EMAIL_CONTACT, "NO_ANSMG",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN28")
	#ENDIF
	
	#IF FEATURE_DLC_2_2022	
		Fill_Character_Sheet(CHAR_VAGOS, A_M_Y_BeachVesp_01, 2470, "CELL_VAGOS_N", "CELL_VAGOS_P", "CELL_400", PC_VAGOS, NO_EMAIL_CONTACT, "ANS_Vbusy",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_05")
		
		Fill_Character_Sheet(CHAR_DAX, A_M_Y_BeachVesp_01, 425, "CELL_DAX_N", "CELL_DAX_P", "CELL_400", PC_DAX, NO_EMAIL_CONTACT, "ANS_Vbusy",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_05")
		
		Fill_Character_Sheet(CHAR_COOK, A_M_Y_BeachVesp_01, 325, "CELL_COOK_N", "CELL_COOK_P", "CELL_400", PC_COOK, NO_EMAIL_CONTACT, "ANS_Vbusy",
		NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_05")
	#ENDIF	
	
	#IF FEATURE_COPS_N_CROOKS
		Fill_Character_Sheet (CHAR_POLICE_DISPATCH, DUMMY_MODEL_FOR_SCRIPT, 2060, "CELL_POL_DIS_N", "CELL_POL_DIS_P", "CELL_400", PC_CHAR_BLANK_ENTRY, NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN16_5")
	#ENDIF
	
    //Special one-time only contacts


    //For the prologue mission - "Detonation"
    Fill_Character_Sheet (CHAR_DETONATEBOMB, DUMMY_MODEL_FOR_SCRIPT, 430, "CELL_130", "CELL_330", "CELL_430", PC_DETONATEBOMB,   NO_EMAIL_CONTACT, "NO_ANSMG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
 
    //For Lester / lifeblurb mission - "Detonate"  - listed as Jay Norris, hence the alphabetical int parameter.
    Fill_Character_Sheet (CHAR_DETONATEPHONE, DUMMY_MODEL_FOR_SCRIPT, 1020, "CELL_132", "CELL_330", "CELL_430", PC_DETONATEPHONE,   NO_EMAIL_CONTACT, "DET_APH1",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
 


    //March MCW  - maybe put special number in phone state enum? e.g Not_LISTED, LISTED, SPECIAL_NUMBERS
    Fill_Character_Sheet (CHAR_CALL911, DUMMY_MODEL_FOR_SCRIPT, 560, "CELL_131", "CELL_331", "CELL_431", PC_CALL911,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_9011") //Should have low alpha int to appear at top if included in contacts list.
        
    //Will be known to all players, but not added as a contact. As a special number, it should never become "unknown".
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_CALL911, 0, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_CALL911, 1, KNOWN_CALLER)
	GLOBAL_CHARACTER_SHEET_SET_STATUS_AS_CALLER(CHAR_CALL911, 2, KNOWN_CALLER)

   
   
      
    Fill_Character_Sheet (CHAR_TAXI, DUMMY_MODEL_FOR_SCRIPT, 480, "CELL_163", "CELL_394", "CELL_446", PC_TAXI,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, SPECIAL_NUMBERS, SPECIAL_NUMBERS, SPECIAL_NUMBERS, "CELL_1062")
        
    //BLOCKED contact - used in assassin missions
    Fill_Character_Sheet(CHAR_BLOCKED, DUMMY_MODEL_FOR_SCRIPT, 240, "CELL_195", "CELL_300", "CELL_195", NO_PHONE_CONTACT,   NO_EMAIL_CONTACT, "NO_ANSMSG", 
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")


    //Dummy used specifically for human player to human player chat call.
    Fill_Character_Sheet (CHAR_CHAT_CALL, DUMMY_MODEL_FOR_SCRIPT, 90, "CELL_E_219", "CELL_E_319", "CELL_431", PC_CHAT_CALL,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091") //This should never be added 



                                                                                                                                             

    //Test characters for checking alphabetical order / savegame resolutions and other stuff. Do not remove!
    Fill_Character_Sheet (CHAR_CASTRO, DUMMY_MODEL_FOR_SCRIPT, 320, "CELL_109", "CELL_309", "CELL_409", PC_CASTRO,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
    
    Fill_Character_Sheet (CHAR_ARTHUR, DUMMY_MODEL_FOR_SCRIPT, 120, "CELL_ARTU", "CELL_300", "CELL_410", PC_ARTHUR,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")

    Fill_Character_Sheet (CHAR_BLANK_ENTRY, DUMMY_MODEL_FOR_SCRIPT, 320, "CELL_196", "CELL_300", "CELL_196", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")


    Fill_Character_Sheet (MAX_CHARACTERS, DUMMY_MODEL_FOR_SCRIPT, 100, "CELL_181", "CELL_300", "CELL_196", PC_CHAR_BLANK_ENTRY,   NO_EMAIL_CONTACT, "NO_ANSMSG",
        NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_1091")
     





    Fill_All_Phone_Settings()
    





    //__________________________________________________________________________________________________________________________________________________________________

    //Fill Gameworld Numbers and Cheat Codes:

    //Submit list for adding a new cheat:
    //This file and if necessary, charsheet_global_definitions.sch.
    //assets_ng/gametext/american/american_cellphone.txt
    //appcontacts.sc


    Fill_Gameworld_Numbers(GW_TEST0, "CELLGW_1", "RON_APH2", "NervousRon") //1459352
    Fill_Gameworld_Numbers(GW_CHEAT1, "CELLC_MONOC", "CELLPHONE_CHEAT", "CHEAT_MONOC") //Monochrome Cellphone only Test cheat. 1-999-EMPEROR. Uses BOOL g_IsMonochromeCheatActive = FALSE

    
    
    Fill_Gameworld_Numbers(GW_CHEAT2, "CELLC_SUPJUM", "CELLPHONE_CHEAT", "CHEAT_SUPJUM")    //1-999-HOPTOIT mapped to CHEAT_TYPE_SUPER_JUMP                     
    
    Fill_Gameworld_Numbers(GW_CHEAT3, "CELLC_HEALA", "CELLPHONE_CHEAT", "CHEAT_HEALA")      //1-999-TURTLE mapped to CHEAT_TYPE_GIVE_HEALTH_ARMOR               

    Fill_Gameworld_Numbers(GW_CHEAT4, "CELLC_SLIDEYC", "CELLPHONE_CHEAT", "CHEAT_SLIDEYC")  //1-999-SNOWDAY mapped to CHEAT_TYPE_SLIDEY_CARS  
    
    Fill_Gameworld_Numbers(GW_CHEAT5, "CELLC_FASTR", "CELLPHONE_CHEAT", "CHEAT_FASTR")      //1-999-CATCHME mapped to CHEAT_TYPE_FAST_RUN  
                                                           
    Fill_Gameworld_Numbers(GW_CHEAT6, "CELLC_WANTDN", "CELLPHONE_CHEAT", "CHEAT_WANTDN")    //1-999-LAWYERUP mapped to CHEAT_TYPE_WANTED_LEVEL_DOWN    

    Fill_Gameworld_Numbers(GW_CHEAT7, "CELLC_WANTUP", "CELLPHONE_CHEAT", "CHEAT_WANTUP")    //1-999-FUGITIVE mapped to CHEAT_TYPE_WANTED_LEVEL_UP 

    Fill_Gameworld_Numbers(GW_CHEAT8, "CELLC_WEATHER", "CELLPHONE_CHEAT", "CHEAT_WEATHER")  //1-999-MAKEITRAIN mapped to CHEAT_TYPE_ADVANCE_WEATHER
    
    Fill_Gameworld_Numbers(GW_CHEAT9, "CELLC_FASTS", "CELLPHONE_CHEAT", "CHEAT_FASTS")      //1-999-GOTGILLS mapped to CHEAT_TYPE_FASTSWIM
     
    Fill_Gameworld_Numbers(GW_CHEAT10, "CELLC_POWER", "CELLPHONE_CHEAT", "CHEAT_POWER")     //1-999-POWERUP mapped to CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE
    
    Fill_Gameworld_Numbers(GW_CHEAT11, "CELLC_CHUTE", "CELLPHONE_CHEAT", "CHEAT_CHUTE")     //1-999-SKYDIVE mapped to CHEAT_TYPE_GIVE_PARACHUTE

    Fill_Gameworld_Numbers(GW_CHEAT12, "CELLC_BANG", "CELLPHONE_CHEAT", "CHEAT_BANG")       //1-999-HIGHEX mapped to CHEAT_TYPE_BANG_BANG

    Fill_Gameworld_Numbers(GW_CHEAT13, "CELLC_FLAMB", "CELLPHONE_CHEAT", "CHEAT_FLAMB")     //1-999-INCENDIARY mapped to CHEAT_TYPE_FLAMING_BULLETS

    Fill_Gameworld_Numbers(GW_CHEAT14, "CELLC_EXPMEL", "CELLPHONE_CHEAT", "CHEAT_EXPMEL")   //1-999-HOTHANDS mapped to CHEAT_TYPE_EXPLOSIVE_MELEE.

    Fill_Gameworld_Numbers(GW_CHEAT15, "CELLC_ZEROG", "CELLPHONE_CHEAT", "CHEAT_ZEROG")     //1-999-FLOATER mapped to CHEAT_TYPE_0_GRAVITY

    Fill_Gameworld_Numbers(GW_CHEAT16, "CELLC_INVINC", "CELLPHONE_CHEAT", "CHEAT_INVINC")   //1-999-PAINKILLER mapped to CHEAT_TYPE_INVINCIBILITY



    Fill_Gameworld_Numbers(GW_CHEAT17, "CELLC_SLOWMO", "CELLPHONE_CHEAT", "CHEAT_SLOWMO")   //1-999-SLOWMO mapped to CHEAT_TYPE_SLOWMO

    
    Fill_Gameworld_Numbers(GW_CHEAT18, "CELLC_SKYFA", "CELLPHONE_CHEAT", "CHEAT_SKYFA")     //1-999-SKYFALL mapped to CHEAT_TYPE_SKYFALL


    Fill_Gameworld_Numbers(GW_CHEAT19, "CELLC_DRUNK", "CELLPHONE_CHEAT", "CHEAT_DRUNK")     //1-999-LIQUOR mapped to CHEAT_TYPE_DRUNK

    
    Fill_Gameworld_Numbers(GW_CHEAT20, "CELLC_DEADEYE", "CELLPHONE_CHEAT", "CHEAT_DEADEYE") //1-999-DEADEYE mapped to CHEAT_TYPE_AIM_SLOWMO




    //Vehicle Spawn Cheats CHEAT_ModelName
    Fill_Gameworld_Numbers(GW_CHEAT21, "CELLC_BUZZARD", "CELLPHONE_CHEAT", "CHEAT_BUZZARD") //1-999-BUZZOFF mapped to CHEAT_TYPE_SPAWN_VEHICLE
    
    Fill_Gameworld_Numbers(GW_CHEAT22, "CELLC_COMET2", "CELLPHONE_CHEAT", "CHEAT_COMET2")  //1-999-COMET mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT23, "CELLC_BMX", "CELLPHONE_CHEAT", "CHEAT_BMX")         //1-999-BANDIT mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT24, "CELLC_CADDY", "CELLPHONE_CHEAT", "CHEAT_CADDY")     //1-999-HOLEIN1 mapped to CHEAT_TYPE_SPAWN_VEHICLE


    Fill_Gameworld_Numbers(GW_CHEAT25, "CELLC_DUSTER", "CELLPHONE_CHEAT", "CHEAT_DUSTER")   //1-999-FLYSPRAY mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT26, "CELLC_PCJ", "CELLPHONE_CHEAT", "CHEAT_PCJ")         //1-999-ROCKET mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT27, "CELLC_RAPIDGT", "CELLPHONE_CHEAT", "CHEAT_RAPIDGT") //1-999-RAPIDGT mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT28, "CELLC_STRETCH", "CELLPHONE_CHEAT", "CHEAT_STRETCH") //1-999-VINEWOOD mapped to CHEAT_TYPE_SPAWN_VEHICLE

    
    Fill_Gameworld_Numbers(GW_CHEAT29, "CELLC_STUNT", "CELLPHONE_CHEAT", "CHEAT_STUNT")     //1-999-BARNSTORMER mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT30, "CELLC_TRASH", "CELLPHONE_CHEAT", "CHEAT_TRASH")     //1-999-TRASHED mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT31, "CELLC_SANCHEZ", "CELLPHONE_CHEAT", "CHEAT_SANCHEZ") //1-999-OFFROAD mapped to CHEAT_TYPE_SPAWN_VEHICLE

                                                                                                                                       

    //Current Gen to Next Gen Bonus Vehicle Spawn Cheats CHEAT_type ( names may be temporary placeholders. )

    Fill_Gameworld_Numbers(GW_CHEAT32, "CELLC_SEAPLANE", "CELLPHONE_CHEAT", "CHEAT_SEAPLANE")   //1-999-EXTINCT mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT33, "CELLC_DUELC", "CELLPHONE_CHEAT", "CHEAT_DUELC")         //1-999-DEATHCAR mapped to CHEAT_TYPE_SPAWN_VEHICLE

    Fill_Gameworld_Numbers(GW_CHEAT34, "CELLC_BUBBLE", "CELLPHONE_CHEAT", "CHEAT_BUBBLE")       //1-999-BUBBLES mapped to CHEAT_TYPE_SPAWN_VEHICLE

  

    //2066607
    Fill_Gameworld_Numbers(GW_CHEAT35, "CELLC_WEAPONS", "CELLPHONE_CHEAT", "CHEAT_WEAPONS")     //1-999-TOOLUP mapped to CHEAT_TYPE_GIVE_WEAPONS

    //2172238
    Fill_Gameworld_Numbers(GW_CHEAT36, "CELLC_DIRECTOR", "CELLPHONE_CHEAT", "CHEAT_DIRECTOR")   //1-999-LSTALENT mapped to force the player into director mode.




    //Subsequent cheats must be filled out like this:
    //Fill_Gameworld_Numbers(GW_CHEATn, "TextLabelThatContainsPhoneNumber", "CELLPHONE_CHEAT", "AppContactsReference")

    //GW_CHEATn is from the enum ENUM enumGameWorldNumbers in charsheet_global_definitions.sch, add more if required. Do NOT reuse an enum.
    //Textlabel should be entered in {------Cellphone Cheats - associated wih gameworld numbers--------} section of americancellphone.txt and noted correctly like the test example.
    //The TextLabel must contain the phone number that should be dialled to trigger the cheat. An alphanumeric keymap is shown in americancellphone for reference.
    //The AppContactsReference is used in the voice string comparison section of AppContacts.sc to decide which cheat is which. Search for CASE CC_PLAY_ANSWERPHONE. 
    //AppContacts must be updated with the correct trigger mechanism from Stephen Robertson who maintains the cheat controller. Any new cheats must go through him first.



    //__________________________________________________________________________________________________________________________________________________________________





    
    //Fill Special MP Characters  - All test names! Keith, you'd need to replace these with the actual CHARS who are to be special contacts with secondary functions 
    //available underneath "call" when they are selected in the contacts menu.

    //This must be done after the character sheet data has been established so we can extract the right text label for the character name.
    //The CELL_SFUN_NULL is a special use text label. If the secondary function labels are set to this then I know that they are not to be displayed in game.
    //Steve T. 04.12.12

    Fill_Special_MP_Characters(0, CHAR_DR_FRIEDLANDER, "CELL_SFUN_NULL", "CELL_SFUN_NULL")
    Fill_Special_MP_Characters(1, CHAR_AMANDA, "CELL_SFUN_NULL", "CELL_SFUN_NULL")
    Fill_Special_MP_Characters(2, CHAR_FRANKLIN, "CELL_SFUN_NULL", "CELL_SFUN_NULL")
    Fill_Special_MP_Characters(3, CHAR_JIMMY, "CELL_SFUN_NULL", "CELL_SFUN_NULL")


    Fill_Special_SP_Characters(0, CHAR_MICHAEL, "CELL_SFUN_NULL")
    Fill_Special_SP_Characters(1, CHAR_FRANKLIN, "CELL_SFUN_NULL")
    Fill_Special_SP_Characters(2, CHAR_TREVOR, "CELL_SFUN_NULL")
    Fill_Special_SP_Characters(3, CHAR_LESTER, "CELL_SFUN_NULL")
    
    //Was CHAR_MP_MEX_LT.  Now reusing for the female Personal Assistant for executive pack. See 2769151.  Already filled above. Commenting this out.
    //#IF USE_TU_CHANGES
        //Fill_Character_Sheet (CHAR_MP_MEX_LT, A_M_Y_BeachVesp_01, 1230 , "CELL_E_204", "CELL_MP_326", "CELL_400", PC_MP_MEXLT,   NO_EMAIL_CONTACT, "NO_ANSMG",
        //NO_FRIEND, NO_FAMILY_MEMBER, NOT_LISTED, NOT_LISTED, NOT_LISTED, "CELL_MN_08")
    
    //#ENDIF




ENDPROC


