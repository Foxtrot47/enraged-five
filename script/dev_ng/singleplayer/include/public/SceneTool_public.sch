//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//-- SceneTool_public - Functions and datatypes,	for defining data to be used when playing a
//													cutscene, (that can be setup using an editor in rag)
//   sam.hackett@rockstarleeds.com
//---------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------
//
//	This tool allows you to write a cutscene that utilises data in the following formats:
//	
//		- Pans		- these contains two camera positions, a duration, shake value, etc
//		- Cuts		- these contain one camera position, shake value, etc
//		- Markers	- these are just a vector position
//		- Placers	- these are a vector position + a float heading
//
//	If you build a cutscene using these data types, there are functions that allow you to easily
//	create a rag editor for setting up your scene, and exporting the data.
//
//
//	Your scene playback header should include this file
//	Your scene editing header should include SceneTool_debug.sch
//
//---------------------------------------------------------------------------------------------------



FUNC BOOL SceneTool_ExecutePan(structSceneTool_Pan& pan, CAMERA_INDEX& hCam0, CAMERA_INDEX& hCam1)

	// Create new cams
	IF DOES_CAM_EXIST(hCam0)
		DESTROY_CAM(hCam0)
	ENDIF
	IF DOES_CAM_EXIST(hCam1)
		DESTROY_CAM(hCam1)
	ENDIF
	hCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	hCam1 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	IF DOES_CAM_EXIST(hCam0) AND DOES_CAM_EXIST(hCam1)

		SET_CAM_COORD(hCam0,	pan.mStart.vPos)
		SET_CAM_ROT(hCam0,		pan.mStart.vRot)
		SET_CAM_FOV(hCam0,		pan.fFov)
		
		SET_CAM_COORD(hCam1,	pan.mEnd.vPos)
		SET_CAM_ROT(hCam1,		pan.mEnd.vRot)
		SET_CAM_FOV(hCam1,		pan.fFov)

		SHAKE_CAM(hCam0,		"HAND_SHAKE", pan.fShake)
		SHAKE_CAM(hCam1,		"HAND_SHAKE", pan.fShake)
			
		IF pan.fDuration > 0.1
			SET_CAM_ACTIVE_WITH_INTERP(hCam1, hCam0, ROUND(pan.fDuration * 1000.0))
		ELSE
			SET_CAM_ACTIVE(hCam0, TRUE)
		ENDIF
		
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		RETURN TRUE
		
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SceneTool_ExecuteCut(structSceneTool_Cut& cut, CAMERA_INDEX& hCam0)

	// Create new cams
	IF DOES_CAM_EXIST(hCam0)
		DESTROY_CAM(hCam0)
	ENDIF
	hCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	IF DOES_CAM_EXIST(hCam0)

		SET_CAM_COORD(hCam0,	cut.mCam.vPos)
		SET_CAM_ROT(hCam0,		cut.mCam.vRot)
		SET_CAM_FOV(hCam0,		cut.fFov)
			
		SHAKE_CAM(hCam0,		"HAND_SHAKE", cut.fShake)
		
		SET_CAM_ACTIVE(hCam0, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		RETURN TRUE
		
	ENDIF

	RETURN FALSE
ENDFUNC

