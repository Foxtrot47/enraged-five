//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author: Ben Rollinson				Date: 12/12/14				│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							Director Mode Public Header							│
//│																				│
//│		Public interface for the new next-gen only Director Mode system. 		│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals.sch"
USING "rage_builtins.sch"
USING "globals_sp_dlc_feature_flags.sch"

USING "script_heist.sch"
USING "director_mode_private.sch"
USING "cheat_controller_public.sch"
using "screens_header.sch"
//USING "random_events_public.sch"

#IF FEATURE_SP_DLC_DIRECTOR_MODE

#IF IS_DEBUG_BUILD

	FUNC STRING GET_DIRECTOR_UNLOCK_STORY_CHAR_STRING(DirectorUnlockStory eCharacter)
		RETURN PRIVATE_Get_Director_Unlock_Story_Char_String(eCharacter)
	ENDFUNC
	

	FUNC STRING GET_DIRECTOR_UNLOCK_HEIST_CHAR_STRING(DirectorUnlockHeist eCharacter)
		RETURN PRIVATE_Get_Director_Unlock_Heist_Char_String(eCharacter)
	ENDFUNC
	
	
	FUNC STRING GET_DIRECTOR_UNLOCK_SPECIAL_CHAR_STRING(DirectorUnlockSpecial eCharacter)
		RETURN PRIVATE_Get_Director_Unlock_Special_Char_String(eCharacter)
	ENDFUNC
			
			
	FUNC STRING GET_DIRECTOR_UNLOCK_ANIMAL_CHAR_STRING(DirectorUnlockAnimal eCharacter)
		RETURN PRIVATE_Get_Director_Unlock_Animal_Char_String(eCharacter)
	ENDFUNC

#ENDIF

FUNC BOOL IS_DIRECTOR_MODE_LAUNCHING()
	IF g_bLaunchDirectorMode
		RETURN TRUE
	ENDIF
	IF (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR))
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode")) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIRECTOR_MODE_RUNNING(BOOL bOrLaunching = FALSE)
	IF bOrLaunching
		IF IS_DIRECTOR_MODE_LAUNCHING()
			RETURN TRUE
		ENDIF
	ENDIF
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_AMOUNT_OF_BITS_SET_IN_BITSET( INT iBitSet )
	INT iBit 		= 0
	INT iBitCount	= 0
	REPEAT 32 iBit
		IF IS_BIT_SET( iBitSet, iBit )
			iBitCount ++
		ENDIF
	ENDREPEAT
	RETURN iBitCount
ENDFUNC

FUNC BOOL IS_DIRECTOR_STORY_CHARCTER_UNLOCKED( DirectorUnlockStory eStoryChar )
	#IF IS_DEBUG_BUILD
		IF ENUM_TO_INT( MAX_DU_STORY ) > 31
			CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_STORY_CHARCTER_UNLOCKED: Too many Story Charas for bitset. Need to create a new bitset or remove story charas. Bug RowanJ.")
		ENDIF
	#ENDIF
	
	IF eStoryChar != DU_STORY_INVALID
	AND eStoryChar != MAX_DU_STORY
		RETURN IS_BIT_SET( g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock, ENUM_TO_INT( eStoryChar ) )
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_STORY_CHARCTER_UNLOCKED: Invalid story chara index passed. DU_STORY_INVALID and MAX_DU_STORY can not be used.")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_AMOUNT_OF_UNLOCKED_STORY_CHARAS()
	INT iIndex = 0
	INT iCount = 0
	REPEAT MAX_DU_STORY iIndex
	DirectorUnlockStory eStoryChar = INT_TO_ENUM( DirectorUnlockStory, iIndex )
		IF eStoryChar != DU_STORY_INVALID
		AND eStoryChar != MAX_DU_STORY
			IF IS_DIRECTOR_STORY_CHARCTER_UNLOCKED( eStoryChar )
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC BOOL IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED( DirectorUnlockHeist eHeistChar )
	#IF IS_DEBUG_BUILD
		IF ENUM_TO_INT( MAX_DU_HEIST ) > 31
			CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED: Too many Heist Charas for bitset. Need to create a new bitset or remove charas. Bug RowanJ.")
		ENDIF
	#ENDIF
	
	IF eHeistChar != DU_HEIST_INVALID
	AND eHeistChar != MAX_DU_HEIST
		RETURN IS_BIT_SET( g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock, ENUM_TO_INT( eHeistChar ) )
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED: Invalid story chara index passed. DU_HEIST_INVALID and MAX_DU_HEIST can not be used.")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_AMOUNT_OF_UNLOCKED_HEIST_CHARAS()
	INT iIndex = 0
	INT iCount = 0
	REPEAT MAX_DU_HEIST iIndex
	DirectorUnlockHeist eHeistChar = INT_TO_ENUM( DirectorUnlockHeist, iIndex )
		IF eHeistChar != DU_HEIST_INVALID
		AND eHeistChar != MAX_DU_HEIST
			IF IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED( eHeistChar )
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC BOOL IS_VALID_DM_SPECIAL_CHARA_UNLOCK( DirectorUnlockSpecial eSpecialChar )
	IF eSpecialChar = DU_SPECIAL_INVALID
	OR eSpecialChar = MAX_DU_SPECIAL
	OR eSpecialChar = DU_SPECIAL_MIME	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED( DirectorUnlockSpecial eSpecialChar )
	#IF IS_DEBUG_BUILD
		IF ENUM_TO_INT( MAX_DU_SPECIAL ) > 31
			CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED: Too many Special Charas for bitset. Need to create a new bitset or remove charas. Bug RowanJ.")
		ENDIF
	#ENDIF
	
	IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
		RETURN IS_BIT_SET( g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock, ENUM_TO_INT( eSpecialChar ) )
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED: Invalid story chara index passed. DU_SPECIAL_INVALID, MAX_DU_SPECIAL and DU_SPECIAL_MIME can not be used.")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC SPECIAL_PEDS GET_SPECIAL_PEDS_FROM_DIRECTOR_UNLOCK_SPECIAL( DirectorUnlockSpecial eSpecialChar, BOOL bSilenceAsserts = FALSE )

	IF NOT IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
		IF NOT bSilenceAsserts
			ASSERTLN( " - director_mode_public - GET_SPECIAL_PEDS_FROM_DIRECTOR_UNLOCK_SPECIAL - Invalid special unlock ped passed in. RETURNING RETURN SP_NONE. " )
		ENDIF
		RETURN SP_NONE
	ELSE
		SWITCH eSpecialChar
			CASE 	DU_SPECIAL_ANDYMOON					RETURN SP_ANDYMOON
			CASE 	DU_SPECIAL_BAYGOR					RETURN SP_BAYGOR 		
			CASE 	DU_SPECIAL_BILLBINDER				RETURN SP_BILLBINDER	
			CASE 	DU_SPECIAL_CLINTON					RETURN SP_CLINTON		
			CASE 	DU_SPECIAL_GRIFF					RETURN SP_GRIFF 		
			CASE 	DU_SPECIAL_JANE						RETURN SP_JANE				
			CASE 	DU_SPECIAL_JEROME					RETURN SP_JEROME		
			CASE 	DU_SPECIAL_JESSE					RETURN SP_JESSE		
			CASE 	DU_SPECIAL_MANI						RETURN SP_MANI			
			CASE 	DU_SPECIAL_PAMELADRAKE				RETURN SP_PAMELADRAKE	
			CASE 	DU_SPECIAL_SUPERHERO				RETURN SP_SUPERHERO	  		
			CASE 	DU_SPECIAL_ZOMBIE					RETURN SP_ZOMBIE	
			DEFAULT 
				IF NOT bSilenceAsserts
					ASSERTLN( " RETURNING SP_NONE" )	
				ENDIF
				RETURN SP_NONE					
		ENDSWITCH								    
	ENDIF										   		 
												  		 
ENDFUNC											  

FUNC BOOL IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DirectorUnlockSpecial eSpecialChar )
	IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
		RETURN IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sSpecialPedData.ePedsKilled, GET_SPECIAL_PEDS_FROM_DIRECTOR_UNLOCK_SPECIAL( eSpecialChar ) )
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED: Invalid story chara index passed. DU_SPECIAL_INVALID, MAX_DU_SPECIAL and DU_SPECIAL_MIME can not be used.")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_AMOUNT_OF_UNLOCKED_SPECIAL_CHARAS()
	INT iIndex = 0
	INT iCount = 0
	REPEAT MAX_DU_SPECIAL iIndex
		DirectorUnlockSpecial eSpecialChar = INT_TO_ENUM( DirectorUnlockSpecial, iIndex )
		IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
			IF IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED( eSpecialChar )
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC BOOL IS_DIRECTOR_LOCATION_UNLOCKED(DirectorTravelLocation eTravelLocation)
	#IF IS_DEBUG_BUILD
		IF ENUM_TO_INT(MAX_DTL) > 31
			CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_LOCATION_UNLOCKED: Too many travel locations for bitset. Need to create a new bitset or remove locations. Bug BenR.")
		ENDIF
	#ENDIF
	
	IF eTravelLocation != DTL_INVALID 
	AND eTravelLocation != MAX_DTL
		RETURN IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, ENUM_TO_INT(eTravelLocation))
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_LOCATION_UNLOCKED: Invalid location index passed. DTL_INVALID and MAX_DTL can not be used.")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_AMOUNT_OF_UNLOCKED_LOCATIONS()
	INT iIndex = 0
	INT iCount = 0
	REPEAT MAX_DTL iIndex
		DirectorTravelLocation eTravelLoc = INT_TO_ENUM( DirectorTravelLocation, iIndex )
		IF eTravelLoc != DTL_INVALID
		AND eTravelLoc != DTL_USER_1
		AND eTravelLoc != DTL_USER_2
		#IF FEATURE_SP_DLC_DM_WAYPOINT_WARP
		AND eTravelLoc != DTL_WAYPOINT
		#ENDIF
		AND eTravelLoc != MAX_DTL
			IF IS_DIRECTOR_LOCATION_UNLOCKED( eTravelLoc )
				iCount ++
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC



#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_LOCATION_BITSET_INFORMATION()
	INT index = 0
	CDEBUG3LN( DEBUG_DIRECTOR, "" )
	CDEBUG3LN( DEBUG_DIRECTOR, "***************** DEBUG LOCATION SPEW *****************" )
	
	CDEBUG3LN( DEBUG_DIRECTOR, " BITSET TOTAL = ", g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed )
	FOR index = 0 TO ENUM_TO_INT( MAX_DTL ) - 1
		BOOL 	bIsBitSet 		= IS_BIT_SET( g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, index )
		CDEBUG3LN( DEBUG_DIRECTOR, " - Bit Index [", index, "] = ", bIsBitSet, "   - ( ", PRIVATE_Get_Director_Travel_Location_String( INT_TO_ENUM( DirectorTravelLocation, index ) ), " )." )
	ENDFOR
	
	CDEBUG3LN( DEBUG_DIRECTOR, "*******************************************************" )
	CDEBUG3LN( DEBUG_DIRECTOR, "" )
ENDPROC

PROC DEBUG_PRINT_ACHIEVEMENT_PROGRESS_INFORMATION_FOR_DM()
	CDEBUG3LN( DEBUG_DIRECTOR, "")
	CDEBUG3LN( DEBUG_DIRECTOR, "***************** ACHIEVEMENT PROGRESS SPEW *****************")
	
	IF IS_XBOX_PLATFORM()
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR3 - 'Humans Of Los Santos' - Achievement progress          : ", GET_ACHIEVEMENT_PROGRESS( ENUM_TO_INT( ACHR3 ) ) )
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR6 - 'Ensemble Piece'       - Achievement progress          : ", GET_ACHIEVEMENT_PROGRESS( ENUM_TO_INT( ACHR6 ) ) )
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR8 - 'Location Scout'       - Achievement progress          : ", GET_ACHIEVEMENT_PROGRESS( ENUM_TO_INT( ACHR8 ) ) )
	ENDIF
	
	CDEBUG3LN( DEBUG_DIRECTOR, "")
	
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR3 - 'Humans Of Los Santos' - Actual Special Chars Unlocked : ", GET_AMOUNT_OF_UNLOCKED_SPECIAL_CHARAS() )
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR3 - 'Ensemble Piece'       - Actual Story Charac Unlocked  : ", GET_AMOUNT_OF_UNLOCKED_STORY_CHARAS() )
	CDEBUG3LN( DEBUG_DIRECTOR, " - ACHR8 - 'Location Scout'       - Actual Locations Unlocked     : ", GET_AMOUNT_OF_UNLOCKED_LOCATIONS() )
	
	CDEBUG3LN( DEBUG_DIRECTOR, "*************************************************************")
	CDEBUG3LN( DEBUG_DIRECTOR, "")
ENDPROC

PROC DEBUG_PRINT_SPECIAL_CHARACTER_DEAD_BITSET()
	
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	CDEBUG1LN( DEBUG_DIRECTOR, " *********************** SPEC PED DEAD BITSET SPEW *********************** ")
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	
	CDEBUG1LN( DEBUG_DIRECTOR, " ****** Bit set  = ", g_savedGlobals.sSpecialPedData.ePedsKilled )
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	
	CDEBUG1LN( DEBUG_DIRECTOR, " ### Actual bitset information ### ")
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	
	
	INT iIndex
	REPEAT 31 iIndex
		BOOL bBitSet = IS_BIT_SET( ENUM_TO_INT( g_savedGlobals.sSpecialPedData.ePedsKilled ), iIndex )
		CDEBUG1LN( DEBUG_DIRECTOR, " - iIndex[", iIndex, "] = ", bBitSet )
	ENDREPEAT
	
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	CDEBUG1LN( DEBUG_DIRECTOR, " ### ENUM bitset information ### ")
	CDEBUG1LN( DEBUG_DIRECTOR, "")
		
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_ANDYMOON =     ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_ANDYMOON ), 	" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_ANDYMOON ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_BAYGOR =       ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_BAYGOR ),   	" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_BAYGOR ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_BILLBINDER =   ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_BILLBINDER ), 	" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_BILLBINDER ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_CLINTON =      ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_CLINTON ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_CLINTON ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_GRIFF =        ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_GRIFF ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_GRIFF ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_IMPOTENTRAGE = ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_IMPOTENTRAGE ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_JANE =         ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_JANE ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_JANE ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_JEROME =       ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_JEROME ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_JEROME ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_JESSE =        ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_JESSE ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_JESSE ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_MANI =         ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_MANI ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_MANI ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_MIME =         ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_MIME ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_PAMELADRAKE =  ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_PAMELADRAKE ), 	" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_PAMELADRAKE ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_SUPERHERO =    ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_SUPERHERO ), 	" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_SUPERHERO ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_ZOMBIE =       ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_ZOMBIE ), 		" - IS_DIRECTOR_SPECIAL_CHARCTER_DEAD = ", IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( DU_SPECIAL_ZOMBIE ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_DANCER =       ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_DANCER ) )
	CDEBUG1LN( DEBUG_DIRECTOR, " - SP_TONYA =        ", IS_BITMASK_ENUM_AS_ENUM_SET( g_savedGlobals.sSpecialPedData.ePedsKilled, SP_TONYA ) )
	
	CDEBUG1LN( DEBUG_DIRECTOR, "")
	CDEBUG1LN( DEBUG_DIRECTOR, " ************************************************************************* ")
	CDEBUG1LN( DEBUG_DIRECTOR, "")
ENDPROC
#ENDIF


PROC SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DirectorUnlockStory eCharacter, BOOL bHidePopups = FALSE)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		//IF NOT IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock, iCharacter)
		IF NOT IS_DIRECTOR_STORY_CHARCTER_UNLOCKED( eCharacter )
			CPRINTLN(DEBUG_DIRECTOR, "Setting Director Mode story character ", GET_DIRECTOR_UNLOCK_STORY_CHAR_STRING(eCharacter), " unlocked.")
			SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock, iCharacter)
			IF NOT bHidePopups
				#IF NOT USE_SP_DLC
				
					//Display a feed message.
					PRIVATE_Display_Director_Character_Unlock_Feed_Message(PRIVATE_Get_Director_Story_Name_Label(eCharacter))
					
					//Display help text if it hasn't displayed before. Attempt to display 
					//for 20 seconds then give up and wait till next time.
					IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_DIRECTOR_MODE_STORY)
						ADD_HELP_TO_FLOW_QUEUE("DI_HLP_STRY", FHP_HIGH, 0, 20000, DEFAULT_HELP_TEXT_TIME, 7, CID_BLANK, CID_DIRECTOR_HELP_STORY_SEEN)
					ENDIF

				#ENDIF
			ENDIF
			
		ENDIF
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "SET_DIRECTOR_STORY_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
ENDPROC

PROC SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DirectorUnlockHeist eCharacter, BOOL bHidePopups = TRUE)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		//IF NOT IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock, iCharacter)
		IF NOT IS_DIRECTOR_HEIST_CHARCTER_UNLOCKED( eCharacter )
			CPRINTLN(DEBUG_DIRECTOR, "Setting Director Mode heist character ", GET_DIRECTOR_UNLOCK_HEIST_CHAR_STRING(eCharacter), " unlocked.")
			SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock, iCharacter)
			
			IF NOT bHidePopups
				#IF NOT USE_SP_DLC
				
					//Display a feed message.
					PRIVATE_Display_Director_Character_Unlock_Feed_Message(PRIVATE_Get_Director_Heist_Name_Label(eCharacter))

				#ENDIF
			ENDIF
		ENDIF
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
ENDPROC

PROC SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DirectorUnlockSpecial eCharacter, BOOL bHidePopups = FALSE)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		//IF NOT IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock, iCharacter)
		IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eCharacter )
			IF NOT IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED( eCharacter )
				CPRINTLN(DEBUG_DIRECTOR, "Setting Director Mode special character ", GET_DIRECTOR_UNLOCK_SPECIAL_CHAR_STRING(eCharacter), " unlocked.")
				SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock, iCharacter)
							
				IF NOT bHidePopups
					#IF NOT USE_SP_DLC
					
						//Display a feed message.
						PRIVATE_Display_Director_Character_Unlock_Feed_Message(PRIVATE_Get_Director_Special_Name_Label(eCharacter))
						
						//Display help text if it hasn't displayed before. Attempt to display 
						//for 20 seconds then give up and wait till next time.
						IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_DIRECTOR_MODE_SPECIAL)
							ADD_HELP_TO_FLOW_QUEUE("DI_HLP_SPCL", FHP_HIGH, 0, 20000, DEFAULT_HELP_TEXT_TIME, 7, CID_BLANK, CID_DIRECTOR_HELP_SPECIAL_SEEN)
						ENDIF

					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
ENDPROC

PROC SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DirectorUnlockAnimal eCharacter, BOOL bHidePopups = FALSE)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		IF NOT IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock, iCharacter)
			CPRINTLN(DEBUG_DIRECTOR, "Setting Director Mode animal character ", GET_DIRECTOR_UNLOCK_ANIMAL_CHAR_STRING(eCharacter), " unlocked.")
			SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock, iCharacter)
			
			IF NOT bHidePopups
				#IF NOT USE_SP_DLC
				
					//Display a feed message.
					PRIVATE_Display_Director_Character_Unlock_Feed_Message(PRIVATE_Get_Director_Animal_Name_Label(eCharacter))
					
					//Display help text if it hasn't displayed before. Attempt to display 
					//for 20 seconds then give up and wait till next time.
					IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_DIRECTOR_MODE_ANIMALS)
						ADD_HELP_TO_FLOW_QUEUE("DI_HLP_ANML", FHP_HIGH, 0, 20000, DEFAULT_HELP_TEXT_TIME, 7, CID_BLANK, CID_DIRECTOR_HELP_ANIMALS_SEEN)
					ENDIF

				#ENDIF
			ENDIF
		ENDIF
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
ENDPROC

PROC UNLOCK_DIRECTOR_TRAVEL_LOCATION(DirectorTravelLocation eTravelLocation, BOOL bHidePopups = FALSE)
	IF NOT IS_DIRECTOR_LOCATION_UNLOCKED(eTravelLocation)
	
		CPRINTLN( DEBUG_DIRECTOR, " - Unlocking travel location ", PRIVATE_Get_Director_Travel_Location_String(eTravelLocation), " - BIT: ", ENUM_TO_INT( eTravelLocation ), "." )
		CPRINTLN( DEBUG_DIRECTOR, " - g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed =  ", g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, " / ", iMaxLocationsBitValue, "." )
		SET_BIT(g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, ENUM_TO_INT(eTravelLocation))
		//Display a feed message for the unlock.
		IF NOT bHidePopups
			//PRIVATE_Display_Director_Location_Unlock_Feed_Message(PRIVATE_Get_Director_Travel_Location_Name_Label(eTravelLocation))
		ENDIF
	ENDIF
ENDPROC

PROC UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(SP_MISSIONS eStoryMission, BOOL bHidePopups = FALSE)

	#IF USE_SP_DLC
		UNUSED_PARAMETER(eStoryMission)
		UNUSED_PARAMETER(bHidePopups)
	#ENDIF

	#IF NOT USE_SP_DLC
		SWITCH eStoryMission
			CASE SP_MISSION_PROLOGUE		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_BRAD, TRUE)					BREAK
			CASE SP_MISSION_ARMENIAN_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FRANKLIN, TRUE)				BREAK
			CASE SP_MISSION_ARMENIAN_2		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_SIMEON, bHidePopups)			BREAK
			CASE SP_MISSION_ARMENIAN_3		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MICHAEL, bHidePopups)		BREAK
			CASE SP_MISSION_FAMILY_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_JIMMY, bHidePopups)			BREAK
			CASE SP_MISSION_FAMILY_2		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TRACEY, bHidePopups)			BREAK
			CASE SP_MISSION_FAMILY_3		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_AMANDA_T, bHidePopups)		BREAK
//			CASE SP_MISSION_LESTER_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LESTER, bHidePopups)			BREAK
			CASE SP_MISSION_LAMAR			
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LAMAR, bHidePopups)
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_STRETCH, bHidePopups)
			BREAK
			CASE SP_MISSION_TREVOR_1		
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TREVOR, bHidePopups)
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_WADE, bHidePopups)
			BREAK
			CASE SP_MISSION_TREVOR_2		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_RON, bHidePopups)			BREAK
			CASE SP_MISSION_CHINESE_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TAO_C, bHidePopups)			BREAK
			CASE SP_MISSION_TREVOR_3		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FLOYD, bHidePopups)			BREAK
			CASE SP_MISSION_FAMILY_4		
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_LAZLOW, bHidePopups)	
											SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_FABIEN, bHidePopups)
			BREAK
			CASE SP_MISSION_FBI_1			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DAVE_N, bHidePopups)			BREAK
			CASE SP_MISSION_FBI_2			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_STEVE_HAINS, bHidePopups)	BREAK
			CASE SP_MISSION_FBI_3			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DEVIN, bHidePopups)			BREAK
			CASE SP_MISSION_SOLOMON_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_SOLOMON, bHidePopups)		BREAK
			CASE SP_MISSION_MARTIN_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_PATRICIA, bHidePopups)		BREAK
			CASE SP_MISSION_FRANKLIN_2		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_TANISHA, bHidePopups)		BREAK
			
			CASE SP_MISSION_SHRINK_1
			CASE SP_MISSION_SHRINK_2
			CASE SP_MISSION_SHRINK_3
			CASE SP_MISSION_SHRINK_4
			CASE SP_MISSION_SHRINK_5		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DR_F, bHidePopups)			BREAK
			//CASE SP_MISSION_FINALE_CREDITS	SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_DR_F, TRUE)					BREAK
		ENDSWITCH
	#ENDIF
	
ENDPROC


PROC UNLOCK_DIRECTOR_STORY_CHAR_FOR_RC_MISSION(g_eRC_MissionIDs eRcMission, BOOL bHidePopups = FALSE)
	SWITCH eRcMission
		CASE RC_PAPARAZZO_1		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_BEVERLY, bHidePopups)		BREAK
		CASE RC_EPSILON_8		SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_CHRIS_F, bHidePopups)		BREAK
		CASE RC_NIGEL_1			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MRS_T, bHidePopups)			BREAK
		CASE RC_MAUDE_1			SET_DIRECTOR_STORY_CHARACTER_UNLOCKED(DU_STORY_MAUDE, bHidePopups)			BREAK
	ENDSWITCH
ENDPROC


PROC UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(INT iHeist, BOOL bHidePopups = FALSE)

	INT iCrewIndex
	REPEAT CM_MAX_CREW_MEMBERS iCrewIndex
		CrewMember eCrewMember = INT_TO_ENUM(CrewMember, iCrewIndex)
		IF IS_HEIST_CREW_MEMBER_UNLOCKED(eCrewMember)
		OR IS_HEIST_CREW_MEMBER_DEAD(eCrewMember)
				IF NOT (eCrewMember = CM_GUNMAN_G_CHEF_UNLOCK AND iHeist = HEIST_AGENCY)
					SWITCH eCrewMember
						CASE CM_GUNMAN_G_GUSTAV				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_GUSTAV)		BREAK	
						CASE CM_GUNMAN_B_NORM				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_NORM)		BREAK		
						CASE CM_HACKER_G_PAIGE				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_PAIGE)		BREAK		
						CASE CM_HACKER_M_CHRIS				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_CHRISTIAN)	BREAK		
						CASE CM_DRIVER_G_EDDIE				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_EDDIE)		BREAK
						CASE CM_DRIVER_B_KARIM				SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_KARIM)		BREAK	
						CASE CM_GUNMAN_G_PACKIE_UNLOCK		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_PACKIE)		BREAK		
						CASE CM_HACKER_B_RICKIE_UNLOCK		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_RICKIE)		BREAK	
						CASE CM_DRIVER_G_TALINA_UNLOCK		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_TALINA)		BREAK
						
						//Unlock in flow ahead of planning board missions, but should only be 
						//awarded in Director Mode after planning board missions.
						CASE CM_GUNMAN_G_CHEF_UNLOCK	
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_1)
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_CHEF)
							ENDIF
						BREAK
						
						CASE CM_GUNMAN_B_DARYL				
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_1)
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_DARYL)
							ENDIF
						BREAK
						
						CASE CM_GUNMAN_M_HUGH				
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_HUGH)		
							ENDIF		
						BREAK
						
						CASE CM_GUNMAN_G_KARL				
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2_INTRO)
								SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_KARL)		
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
	ENDREPEAT
	
	IF NOT IS_HEIST_CREW_MEMBER_UNLOCKED( CM_GUNMAN_G_PACKIE_UNLOCK )
	AND IS_BIT_SET( g_savedGlobals.sRandomEventData.iREVariationComplete[ RE_GETAWAYDRIVER ], 0 )
		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_PACKIE )
	ENDIF
	
	IF NOT IS_HEIST_CREW_MEMBER_UNLOCKED( CM_DRIVER_G_TALINA_UNLOCK )
	AND IS_BIT_SET( g_savedGlobals.sRandomEventData.iREVariationComplete[ RE_CRASHRESCUE ], 0 )
		SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED( DU_HEIST_TALINA )
	ENDIF
	
	//#2481999 If the player still hasn't picked up one of Rickies calls 
	//and they have passed the Finale Heist planning board, the player 
	//has missed their last opportunity to unlock him for Director Mode. 
	//Unlock him automatically to ensure they get him.
	IF NOT IS_HEIST_CREW_MEMBER_UNLOCKED( CM_HACKER_B_RICKIE_UNLOCK )
		IF iHeist = HEIST_FINALE
			SET_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DU_HEIST_RICKIE)
		ENDIF
	ENDIF
		
	IF NOT bHidePopups
		#IF NOT USE_SP_DLC
			//Display help text if it hasn't displayed before. Attempt to display 
			//for 20 seconds then give up and wait till next time.
			IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_DIRECTOR_MODE_HEISTS)
				ADD_HELP_TO_FLOW_QUEUE("DI_HLP_HST", FHP_HIGH, 0, 20000, DEFAULT_HELP_TEXT_TIME, 7, CID_BLANK, CID_DIRECTOR_HELP_HEISTS_SEEN)
			ENDIF
			
			//Display a feed message.
			PRIVATE_Display_Director_Character_Unlock_Feed_Message("DI_FEED_HST")
		#ENDIF
	ENDIF

ENDPROC


PROC UNLOCK_DIRECTOR_SPECIAL_CHAR_FROM_SCRIPT_HASH(INT iScriptHash, BOOL bHidePopups = FALSE)
	DirectorUnlockSpecial eUnlock = PRIVATE_Get_Director_Unlock_From_Special_Ped_Script_Hash(iScriptHash)
	IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eUnlock )
		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(eUnlock, bHidePopups)
	ENDIF
ENDPROC


PROC UNLOCK_DIRECTOR_ANIMAL_CHAR_FROM_ANIMAL_PICKUP(AnimalPickup eAnimalPickup, BOOL bHidePopups = FALSE)
	DirectorUnlockAnimal eUnlock = PRIVATE_Get_Director_Animal_Unlock_From_Animal_Pickup(eAnimalPickup)
	IF eUnlock != DU_ANIMAL_INVALID
		SET_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(eUnlock, bHidePopups)
	ENDIF							
ENDPROC


FUNC VECTOR GET_DIRECTOR_TRAVEL_POSITION(DirectorTravelLocation eTravelLocation)
	
	SWITCH eTravelLocation
		CASE DTL_USER_1				RETURN <<0,0,0>>								BREAK
		CASE DTL_USER_2				RETURN <<0,0,0>>								BREAK
		CASE DTL_LS_INTERNATIONAL	RETURN <<-1018.2023, -2697.0581, 12.9816>>		BREAK
		CASE DTL_DOCKS				RETURN <<592.3612, -3032.3877, 5.0693>>			BREAK
		CASE DTL_LS_DOWNTOWN		RETURN <<202.1544, -1019.9752, 28.3850>>		BREAK
		CASE DTL_DEL_PERRO_PIER		RETURN <<-1624.0209, -968.4926, 12.0176>>		BREAK
		CASE DTL_VINEWOOD_HILLS		RETURN <<-1347.5137, 726.7493, 185.1062>>		BREAK
		CASE DTL_WIND_FARM			RETURN <<2206.5620, 2489.0945, 86.7392>>		BREAK
		CASE DTL_SANDY_SHORES		RETURN <<2039.8926, 3775.5852, 31.1919>>		BREAK
		CASE DTL_STAB_CITY			RETURN <<74.1428, 3552.7986, 46.8948>>			BREAK
		CASE DTL_FORT_ZANCUDO		RETURN <<-1570.4391, 2778.4441, 16.1308>>		BREAK
		CASE DTL_CANYON				RETURN <<-885.9214, 4428.6768, 20.0067>>		BREAK
		CASE DTL_CABLE_CAR			RETURN <<463.9337, 5618.8994, 785.1277>>		BREAK
		CASE DTL_PALETO				RETURN <<-321.5562, 6071.3320, 30.3203>>		BREAK
		CASE DTL_LIGHTHOUSE			RETURN <<3352.7612, 5149.7046, 19.1508>>		BREAK
		CASE DTL_VINEYARD			RETURN <<-1873.3,2089.5,141>>					BREAK
		CASE DTL_STUDIO				RETURN <<-1050,-472,36.6>>						BREAK
		CASE DTL_GOLFCLUB			RETURN <<-1376,47,53.7>>						BREAK
		CASE DTL_ALTRUIST			RETURN <<-1059,4906,211.3>>						BREAK
		CASE DTL_POWERSTATION		RETURN <<2684.1,1613.3,24.6>>					BREAK
		CASE DTL_SAWMILL			RETURN <<-580.9,5246.4,70.5>>					BREAK
		CASE DTL_QUARRY				RETURN <<2952.4,2788.3,41.5>>					BREAK
		CASE DTL_DAM				RETURN <<1661.7,-9.5,173.8>>					BREAK
		CASE DTL_STORMDRAIN			RETURN <<1069.1,-254.1,57.8>>					BREAK
		CASE DTL_OBSERVATORY		RETURN <<-412.6,1169.5,325.9>>					BREAK
		CASE DTL_RADIOARRAY			RETURN <<2072.9,2931.4,47.5>>					BREAK
		CASE DTL_LSU				RETURN <<-1618.7,178.6,60.3>>					BREAK
		CASE DTL_COVE				RETURN <<3068.3,2212.3,2.9>>					BREAK
		CASE DTL_MTGORDO			RETURN <<2877.6,5910.6,369.6>>					BREAK
		
		
	ENDSWITCH

	CASSERTLN(DEBUG_DIRECTOR, "GET_DIRECTOR_TRAVEL_POSITION: Location ", PRIVATE_Get_Director_Travel_Location_String(eTravelLocation), " has no position defined.")
	RETURN <<0,0,0>>
ENDFUNC

PROC GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN(VECTOR &vPos, FLOAT &fHead,MODEL_NAMES mModel)
	IF IS_THIS_MODEL_A_BOAT(mModel) 
	OR IS_THIS_MODEL_A_JETSKI(mModel) 
	OR mModel = SUBMERSIBLE
	OR mModel = SUBMERSIBLE2
		VECTOR vBoatPos[30]
		FLOAT fBoatHead[30]
		vBoatPos[0] = <<1337.1980, 4279.3647, 29.1928>> 	fBoatHead[0] = 178.5
		vBoatPos[1] = <<710.3667, 4109.7666, 27.0076>>		fBoatHead[1] = 170.6
		vBoatPos[2] = <<614.7804, 3624.1785, 28.4500>>		fBoatHead[2] = 346.2192
		vBoatPos[3] = <<1734.7354, 3984.5637, 29.1863>>		fBoatHead[3] = 20.6065
		vBoatPos[4] = <<3371.7280, 5181.5552, -2.0261>>		fBoatHead[4] = 270.0560
		vBoatPos[5] = <<3465.3110, 5872.0649, -0.8379>>		fBoatHead[5] = 27.9325
		vBoatPos[6] = <<2992.0388, 6388.0903, -1.8533>>		fBoatHead[6] = 63.5079
		vBoatPos[7] = <<1539.0465, 6657.6167, -0.5041>>		fBoatHead[7] = 133.5472
		vBoatPos[8] = <<13.7854, 7070.5996, -1.6533>>		fBoatHead[8] = 147.9066
		vBoatPos[9] = <<-281.9527, 6612.5762, -1.5175>>		fBoatHead[9] = 115.5444
		vBoatPos[10] = <<-493.8891, 6479.9980, -3.7036>>	fBoatHead[10] = 31.4430
		vBoatPos[11] = <<-1617.1379, 5255.4751, -2.9299>>	fBoatHead[11] = 25.8541
		vBoatPos[12] = <<-2820.0291, 2374.5281, -0.9006>>	fBoatHead[12] = 151.3831
		vBoatPos[13] = <<-3185.0657, 1729.9214, -0.8405>>	fBoatHead[13] = 187.6676
		vBoatPos[14] = <<-3425.0286, 947.7670, -1.8002>>	fBoatHead[14] = 84.6526
		vBoatPos[15] = <<-2354.9851, -371.4839, -1.1723>>	fBoatHead[15] = 232.9323
		vBoatPos[16] = <<-1559.8241, -1259.7800, -0.4354>>	fBoatHead[16] = 209.0601
		vBoatPos[17] = <<-790.8649, -1499.9752, -3.0017>>	fBoatHead[17] = 117.2064
		vBoatPos[18] = <<-461.9034, -2226.6384, -2.3394>>	fBoatHead[18] = 270.3839
		vBoatPos[19] = <<604.4341, -2112.4780, -3.2858>>	fBoatHead[19] = 182.6449
		vBoatPos[20] = <<-1023.5532, -949.4196, -1.8741>>	fBoatHead[20] = 296.6459
		vBoatPos[21] = <<2308.9727, -2150.3108, -1.7398>>	fBoatHead[21] = 183.3645
		vBoatPos[22] = <<2506.7468, -1304.1779, -0.4532>>	fBoatHead[22] = 357.4107
		vBoatPos[23] = <<2852.6619, -667.7905, -2.8556>>	fBoatHead[23] = 359.0744
		vBoatPos[24] = <<2965.9697, 668.2945, -3.5758>>		fBoatHead[24] = 359.1271
		vBoatPos[25] = <<3772.8408, 3825.9951, -1.2615>>	fBoatHead[25] = 343.9890
		vBoatPos[26] = <<-193.4529, 785.6457, 193.7793>>	fBoatHead[26] = 56.3848
		vBoatPos[27] = <<1684.2738, 35.1368, 153.0065>>		fBoatHead[27] = 272.0354
		vBoatPos[28] = <<-1526.5443, 1498.4705, 107.9476>>	fBoatHead[28] = 344.7227
		vBoatPos[29] = <<-584.7012, 4408.2534, 13.4584>>	fBoatHead[29] = 100.2413
		
		INT i, iBestSpot = 0
		FLOAT fBestDistSq = 1000000000
		FOR i = 0 to COUNT_OF(vBoatPos) - 1
			IF VDIST2(vPos, vBoatPos[i]) < fBestDistSq
				fBestDistSq = VDIST2(vPos, vBoatPos[i])
				iBestSpot = i
			ENDIF
		ENDFOR
		
		vPos = vBoatPos[iBestSpot]
		fHead = fBoatHead[iBestSpot]
		
		CPRINTLN(debug_director, "GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN: Best boat location at ",vPos)
	ELIF IS_THIS_MODEL_A_HELI(mModel)
		VECTOR vHeliPos[8]
		FLOAT fHeliHead[8]
	
		vHeliPos[0] = <<-74.1696, -819.5928, 325.1753>>		fHeliHead[0] = 109.6636
		vHeliPos[1] = <<-145.4422, -593.6234, 210.7752>>		fHeliHead[1] = 10.7413
		vHeliPos[2] = <<-725.2798, -1442.9688, 4.0005>>		fHeliHead[2] = 240.5840
		vHeliPos[3] = <<300.8448, -1453.5085, 45.5095>>		fHeliHead[3] = 250.2448
		vHeliPos[4] = <<581.7233, 11.0254, 102.2337>>		fHeliHead[4] = 81.0753
		vHeliPos[5] = <<1770.6411, 3240.1450, 41.1264>>		fHeliHead[5] = 86.8879
		vHeliPos[6] = <<2129.9731, 4807.3804, 40.1959>>		fHeliHead[6] = 125.9246
		vHeliPos[7] = <<-280.3902, 6042.7524, 30.5280>>		fHeliHead[7] = 256.7738
		
		INT i, iBestSpot = 0
		FLOAT fBestDistSq = 1000000000
		FOR i = 0 to COUNT_OF(vHeliPos) - 1
			IF VDIST2(vPos, vHeliPos[i]) < fBestDistSq
				fBestDistSq = VDIST2(vPos, vHeliPos[i])
				iBestSpot = i
			ENDIF
		ENDFOR
		
		vPos = vHeliPos[iBestSpot]
		fHead = fHeliHead[iBestSpot]
		CPRINTLN(debug_director, "GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN: Best heli location at ",vPos)
	ELIF IS_THIS_MODEL_A_PLANE(mModel)
		VECTOR vPlanPos[2]
		FLOAT fPlanHead[2]
		
		vPlanPos[0] = <<2125.4165, 4801.7993, 40.0111>>		fPlanHead[0] = 115.5818
		vPlanPos[1] = <<1708.1094, 3255.7339, 40.0340>>		fPlanHead[1] = 105.6965
		
		INT i, iBestSpot = 0
		FLOAT fBestDistSq = 1000000000
		FOR i = 0 to COUNT_OF(vPlanPos) - 1
			IF VDIST2(vPos, vPlanPos[i]) < fBestDistSq
				fBestDistSq = VDIST2(vPos, vPlanPos[i])
				iBestSpot = i
			ENDIF
		ENDFOR
		
		vPos = vPlanPos[iBestSpot]
		fHead = fPlanHead[iBestSpot]
		CPRINTLN(debug_director, "GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN: Best heli location at ",vPos)
	ELSE
		//Usual failsafe, just drop somewhere near the player
		vPos = GET_ENTITY_COORDS(player_ped_id())+<<5,5,0>>
		FLOAT fGroundZ
		GET_GROUND_Z_FOR_3D_COORD(vPos, fGroundZ)
		vPos.z = fGroundZ
		CPRINTLN(debug_director,"GET_DIRECTOR_FAILSAFE_POSITION_FOR_VEHICLE_SPAWN: Getting failsafe vehicle position at ",vpos)
	ENDIF
ENDPROC

// Check one location per frame to see if it has been revealed in the fog of war.
// If we find a revealed location flag a saved bit to say it is unlocked.
//PROC MAINTAIN_DIRECTOR_LOCATION_REVEALED_CHECKS(DirectorTravelLocation &eCurrentLocation)
//	IF NOT IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
//	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_DIRECTOR )
//		IF NOT IS_DIRECTOR_LOCATION_UNLOCKED(eCurrentLocation)
//			IF #IF FEATURE_SP_DLC_DM_WAYPOINT_WARP eCurrentLocation = DTL_WAYPOINT
//			OR #ENDIF eCurrentLocation = DTL_USER_1
//			OR eCurrentLocation = DTL_USER_2
//				CPRINTLN(DEBUG_DIRECTOR, "Travel location ", PRIVATE_Get_Director_Travel_Location_String(eCurrentLocation), " has been unlocked.")
//				UNLOCK_DIRECTOR_TRAVEL_LOCATION(eCurrentLocation)
//			ELSE
//				IF VDIST2( GET_PLAYER_COORDS( PLAYER_ID() ), GET_DIRECTOR_TRAVEL_POSITION( eCurrentLocation ) ) < ( cf_REVEAL_LOC_DIST * cf_REVEAL_LOC_DIST )
//				//IF GET_MINIMAP_FOW_COORDINATE_IS_REVEALED(GET_DIRECTOR_TRAVEL_POSITION(eCurrentLocation))
//					CPRINTLN(DEBUG_DIRECTOR, "Travel location ", PRIVATE_Get_Director_Travel_Location_String(eCurrentLocation), " has been unlocked.")
//					UNLOCK_DIRECTOR_TRAVEL_LOCATION(eCurrentLocation)
//				ENDIF
//			ENDIF
//		ENDIF
//
//		//Step to the next location index for next frame.
//		INT iNextLocation = ENUM_TO_INT(eCurrentLocation) + 1
//		IF iNextLocation >= ENUM_TO_INT(MAX_DTL)
//			iNextLocation = 0
//		ENDIF
//		eCurrentLocation = INT_TO_ENUM(DirectorTravelLocation, iNextLocation)
//	ENDIF
//ENDPROC


PROC UNLOCK_ALL_DIRECTOR_PROGRESS_FOR_CURRENT_PLAYTHROUGH()
	INT index

	CDEBUG1LN(DEBUG_DIRECTOR, "Checking for story mission characters to unlock.")
	REPEAT SP_MISSION_MAX index
		SP_MISSIONS eMission = INT_TO_ENUM(SP_MISSIONS, index)
		IF GET_MISSION_COMPLETE_STATE(eMission)
			SWITCH eMission
				//Fix for 2300669: Shrink sessions can be auto-completed by the flow. This looks to
				//Director Mode like they played and passed the session. Set a saved bit to record when the 
				//player actually attended a session.
				CASE SP_MISSION_SHRINK_1
				CASE SP_MISSION_SHRINK_2
				CASE SP_MISSION_SHRINK_3
				CASE SP_MISSION_SHRINK_4
				CASE SP_MISSION_SHRINK_5
					IF IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_SHRINK_SESSION_ATTENDED)
					OR GET_MISSION_COMPLETE_STATE( SP_MISSION_SHRINK_5 )
						UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(eMission, TRUE)
					ENDIF
				BREAK
				
				CASE SP_MISSION_FINALE_CREDITS
					IF GET_MISSION_FLOW_FLAG_STATE( FLOWFLAG_TREVOR_KILLED )
						SET_DIRECTOR_STORY_CHARACTER_UNLOCKED( DU_STORY_MRS_T, TRUE )
						SET_DIRECTOR_STORY_CHARACTER_UNLOCKED( DU_STORY_MAUDE, TRUE )
					ELIF GET_MISSION_FLOW_FLAG_STATE( FLOWFLAG_MICHAEL_KILLED )
						SET_DIRECTOR_STORY_CHARACTER_UNLOCKED( DU_STORY_CHRIS_F, TRUE )
						SET_DIRECTOR_STORY_CHARACTER_UNLOCKED( DU_STORY_DR_F, TRUE )
					ENDIF
				BREAK
				
				//No extra checks need for all other missions.
				DEFAULT
					UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(eMission, TRUE)
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_DIRECTOR, "Checking for RC mission characters to unlock.")
	REPEAT MAX_RC_MISSIONS index
		g_eRC_MissionIDs eRcMission = INT_TO_ENUM(g_eRC_MissionIDs, index)
		IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[eRcMission].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
			UNLOCK_DIRECTOR_STORY_CHAR_FOR_RC_MISSION(eRcMission, TRUE)
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_DIRECTOR, "Checking for special characters to unlock.")
	REPEAT MAX_DU_SPECIAL index
		DirectorUnlockSpecial eSpecialChar = INT_TO_ENUM( DirectorUnlockSpecial, index )
		IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
			IF IS_DIRECTOR_SPECIAL_CHARCTER_DEAD( eSpecialChar )
				IF NOT IS_DIRECTOR_SPECIAL_CHARCTER_UNLOCKED( eSpecialChar )
					SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED( eSpecialChar, TRUE )
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	#IF NOT USE_SP_DLC
		CDEBUG1LN(DEBUG_DIRECTOR, "Checking for heist characters to unlock.")
		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_1)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_JEWEL, TRUE)
		ENDIF
		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_1)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_RURAL_BANK, TRUE)
		ENDIF
		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_AGENCY, TRUE)
		ENDIF
		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2_INTRO)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_FINALE, TRUE)
		ENDIF
	#ENDIF
	
	CDEBUG1LN(DEBUG_DIRECTOR, "Checking for animal characters to unlock.")
	REPEAT AP_COUNT index
		AnimalPickup eAnimal = INT_TO_ENUM(AnimalPickup, index)
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iPeyoteAnimalSeen, index)
			UNLOCK_DIRECTOR_ANIMAL_CHAR_FROM_ANIMAL_PICKUP(eAnimal, TRUE)
		ENDIF
	ENDREPEAT

	CDEBUG1LN(DEBUG_DIRECTOR, "Checking for travel locations to unlock.")	//Unlock the user ones, and waypoint
	REPEAT MAX_DTL	index
		DirectorTravelLocation eTravelLocation = INT_TO_ENUM(DirectorTravelLocation, index)
		IF NOT IS_DIRECTOR_LOCATION_UNLOCKED(eTravelLocation)
			IF eTravelLocation = DTL_USER_1 
			OR eTravelLocation = DTL_USER_2
			#IF FEATURE_SP_DLC_DM_WAYPOINT_WARP OR eTravelLocation = DTL_WAYPOINT #ENDIF
			//OR GET_MINIMAP_FOW_COORDINATE_IS_REVEALED(GET_DIRECTOR_TRAVEL_POSITION(eTravelLocation))
				UNLOCK_DIRECTOR_TRAVEL_LOCATION(eTravelLocation, TRUE)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BOOL IS_DIRECTOR_STORY_CHARACTER_UNLOCKED(DirectorUnlockStory eCharacter)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		RETURN IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterStoryUnlock, iCharacter)
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_STORY_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_DIRECTOR_HEIST_CHARACTER_UNLOCKED(DirectorUnlockHeist eCharacter)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		RETURN IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterHeistUnlock, iCharacter)
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_HEIST_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(DirectorUnlockSpecial eCharacter)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		RETURN IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterSpecialUnlock, iCharacter)
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(DirectorUnlockAnimal eCharacter)
	INT iCharacter = ENUM_TO_INT(eCharacter)
	IF iCharacter >= 0 AND iCharacter <= 31
		RETURN IS_BIT_SET(g_savedGlobals.sDirectorModeData.iBitsetCharacterAnimalUnlock, iCharacter)
	ELSE
		CASSERTLN(DEBUG_DIRECTOR, "IS_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED: Character ", iCharacter, " was out of range for a bitset. Bug BenR!")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CALCULATE_MAX_LOCATIONS_BIT_VALUE()
	iMaxLocationsBitValue = ( FLOOR( POW( 2.0, TO_FLOAT( ENUM_TO_INT( MAX_DTL ) ) ) ) - 1 )
	CPRINTLN( DEBUG_DIRECTOR, " - CALCULATE_MAX_LOCATIONS_BIT_VALUE - iMaxLocationsBitValue = ", iMaxLocationsBitValue, " from  ( 2 ^ ", ENUM_TO_INT( MAX_DTL ), " ) - 1. " )
ENDPROC

FUNC BOOL ARE_ALL_DM_LOCATIONS_UNLOCKED()

	#IF IS_DEBUG_BUILD
	IF iMaxLocationsBitValue = -1
		ASSERTLN( "- director_mode_public - ARE_ALL_DM_LOCATIONS_UNLOCKED - iMaxLocationsBitValue is unset - CALCULATE_MAX_LOCATIONS_BIT_VALUE needs to be called in the init of DM. Bug RBJ." )
	ENDIF
	#ENDIF

	IF g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed = iMaxLocationsBitValue
		CDEBUG1LN( DEBUG_DIRECTOR, " - ARE_ALL_DM_LOCATIONS_UNLOCKED returning TRUE." )
		RETURN TRUE
	ELSE
		// Remove
//		CDEBUG3LN( DEBUG_DIRECTOR, " - ARE_ALL_DM_LOCATIONS_UNLOCKED returning FALSE - g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed = ", g_savedGlobals.sDirectorModeData.iBitsetTravelLocationRevealed, " - iMaxLocationsBitValue = ", iMaxLocationsBitValue, "." )
//		#IF IS_DEBUG_BUILD 		DEBUG_PRINT_LOCATION_BITSET_INFORMATION()	#ENDIF
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED()
	INT iChara
	FOR iChara = 0 TO COUNT_OF( DirectorUnlockSpecial ) - 2	
		IF NOT IS_VALID_DM_SPECIAL_CHARA_UNLOCK( INT_TO_ENUM( DirectorUnlockSpecial, iChara ) )
			CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED - Skipping unlock check on: ", iChara )
		ELSE
			IF NOT IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED( INT_TO_ENUM( DirectorUnlockSpecial, iChara ) )
				CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED - ", iChara, " is NOT unlocked... Returning FALSE." )
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_SPECIAL_CHARAS_UNLOCKED - Returning TRUE" )
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_DM_STORY_CHARAS_UNLOCKED()
	INT iChara
	FOR iChara = 0 TO COUNT_OF( DirectorUnlockStory ) - 2
		IF INT_TO_ENUM( DirectorUnlockStory, iChara ) = MAX_DU_STORY
		OR INT_TO_ENUM( DirectorUnlockStory, iChara ) = DU_STORY_INVALID
		OR INT_TO_ENUM( DirectorUnlockStory, iChara ) = DU_STORY_LESTER	//B* 2222926: Removed the Lester unlock notification for Director mode
			CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_STORY_CHARAS_UNLOCKED - Skipping unlock check on: ", iChara )
		ELSE
			IF NOT IS_DIRECTOR_STORY_CHARACTER_UNLOCKED( INT_TO_ENUM( DirectorUnlockStory, iChara ) )
				CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_STORY_CHARAS_UNLOCKED - ", iChara, " is NOT unlocked... Returning FALSE." )
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_STORY_CHARAS_UNLOCKED - Returning TRUE" )
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_DM_ANIMALS_UNLOCKED( BOOL bExcludeSasquatchCheck = FALSE )
	INT iChara
	FOR iChara = 0 TO COUNT_OF( DirectorUnlockAnimal ) - 2
		IF bExcludeSasquatchCheck
		AND INT_TO_ENUM( DirectorUnlockAnimal, iChara ) = DU_ANIMAL_SASQUATCH
			CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_ANIMALS_UNLOCKED - Skipping unlock check on: ", iChara )
		ELSE
			IF INT_TO_ENUM( DirectorUnlockAnimal, iChara ) = MAX_DU_ANIMAL
			OR INT_TO_ENUM( DirectorUnlockAnimal, iChara ) = DU_ANIMAL_INVALID
				CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_ANIMALS_UNLOCKED - Skipping unlock check on: ", iChara )
			ELSE
				IF NOT IS_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED( INT_TO_ENUM( DirectorUnlockAnimal, iChara ) )
					CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_ANIMALS_UNLOCKED - ", iChara, " is NOT unlocked... Returning FALSE." )
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	CPRINTLN( DEBUG_DIRECTOR, " - ARE_ALL_DM_ANIMALS_UNLOCKED - Returning TRUE" )
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD

	PROC UNLOCK_ALL_DIRECTOR_PROGRESS_FOR_DEBUG()
		INT index

		//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for story mission characters to unlock.")
		REPEAT SP_MISSION_MAX index
			SP_MISSIONS eMission = INT_TO_ENUM(SP_MISSIONS, index)
			UNLOCK_DIRECTOR_STORY_CHAR_FOR_STORY_MISSION(eMission, TRUE)
		ENDREPEAT

		//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for RC mission characters to unlock.")
		REPEAT MAX_RC_MISSIONS index
			g_eRC_MissionIDs eRcMission = INT_TO_ENUM(g_eRC_MissionIDs, index)
			UNLOCK_DIRECTOR_STORY_CHAR_FOR_RC_MISSION(eRcMission, TRUE)
		ENDREPEAT

		//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for special characters to unlock.")
		REPEAT MAX_DU_SPECIAL index
			DirectorUnlockSpecial eSpecChara = INT_TO_ENUM( DirectorUnlockSpecial, index )
			IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecChara )
				SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED( eSpecChara )
			ENDIF
		ENDREPEAT

		#IF NOT USE_SP_DLC
			//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for heist characters to unlock.")
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_JEWEL, TRUE)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_RURAL_BANK, TRUE)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_AGENCY, TRUE)
			UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_FINALE, TRUE)
		#ENDIF
		
		//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for animal characters to unlock.")
		REPEAT AP_COUNT index
			AnimalPickup eAnimal = INT_TO_ENUM(AnimalPickup, index)
			UNLOCK_DIRECTOR_ANIMAL_CHAR_FROM_ANIMAL_PICKUP(eAnimal, TRUE)
		ENDREPEAT

		//CDEBUG1LN(DEBUG_DIRECTOR, "Checking for travel locations to unlock.")
		//		REPEAT MAX_DTL	index
//			DirectorTravelLocation eTravelLocation = INT_TO_ENUM(DirectorTravelLocation, index)
//			IF NOT IS_DIRECTOR_LOCATION_UNLOCKED(eTravelLocation)
//				UNLOCK_DIRECTOR_TRAVEL_LOCATION(eTravelLocation, TRUE)
//			ENDIF
//		ENDREPEAT
//		
	ENDPROC

	PROC UNLOCK_ALL_DIRECTOR_LOCATIONS_FOR_DEBUG()
		INT index = 0
		REPEAT MAX_DTL	index
			DirectorTravelLocation eTravelLocation = INT_TO_ENUM(DirectorTravelLocation, index)
			IF NOT IS_DIRECTOR_LOCATION_UNLOCKED(eTravelLocation)
				UNLOCK_DIRECTOR_TRAVEL_LOCATION(eTravelLocation, TRUE)
			ENDIF
		ENDREPEAT
	ENDPROC

	PROC DEBUG_PRINT_DIRECTOR_CHAR_UNLOCKS()
		INT index
		
		CPRINTLN(DEBUG_DIRECTOR, "--- DIRECTOR MODE CHAR UNLOCK STATE ---")
		CPRINTLN(DEBUG_DIRECTOR, "--- Story Characters ---")
		REPEAT MAX_DU_STORY index
			DirectorUnlockStory eCharacter = INT_TO_ENUM(DirectorUnlockStory, index)
			CPRINTLN(DEBUG_DIRECTOR, PRIVATE_Get_Director_Unlock_Story_Char_String(eCharacter), " : ", PICK_STRING(IS_DIRECTOR_STORY_CHARACTER_UNLOCKED(eCharacter), "UNLOCKED", "LOCKED"))
		ENDREPEAT
		
		CPRINTLN(DEBUG_DIRECTOR, "--- Heist Characters ---")
		REPEAT MAX_DU_HEIST index
			DirectorUnlockHeist eCharacter = INT_TO_ENUM(DirectorUnlockHeist, index)
			CPRINTLN(DEBUG_DIRECTOR, PRIVATE_Get_Director_Unlock_Heist_Char_String(eCharacter), " : ", PICK_STRING(IS_DIRECTOR_HEIST_CHARACTER_UNLOCKED(eCharacter), "UNLOCKED", "LOCKED"))
		ENDREPEAT
		
		CPRINTLN(DEBUG_DIRECTOR, "--- Special Characters ---")
		REPEAT MAX_DU_SPECIAL index
			DirectorUnlockSpecial eCharacter = INT_TO_ENUM(DirectorUnlockSpecial, index)
			CPRINTLN(DEBUG_DIRECTOR, PRIVATE_Get_Director_Unlock_Special_Char_String(eCharacter), " : ", PICK_STRING(IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(eCharacter), "UNLOCKED", "LOCKED"))
		ENDREPEAT
		
		CPRINTLN(DEBUG_DIRECTOR, "--- Animal Characters ---")
		REPEAT MAX_DU_ANIMAL index
			DirectorUnlockAnimal eCharacter = INT_TO_ENUM(DirectorUnlockAnimal, index)
			CPRINTLN(DEBUG_DIRECTOR, PRIVATE_Get_Director_Unlock_Animal_Char_String(eCharacter), " : ", PICK_STRING(IS_DIRECTOR_ANIMAL_CHARACTER_UNLOCKED(eCharacter), "UNLOCKED", "LOCKED"))
		ENDREPEAT
		CPRINTLN(DEBUG_DIRECTOR, "---------------------------------------")
		
	ENDPROC

#ENDIF


FUNC BOOL IS_WEAPON_VALID_FOR_DIRECTOR_MODE(WEAPON_TYPE eWeaponType)
	SWITCH eWeaponType
		CASE WEAPONTYPE_UNARMED
		CASE WEAPONTYPE_ANIMAL
		CASE WEAPONTYPE_COUGAR
		CASE WEAPONTYPE_GRENADELAUNCHER_SMOKE
		CASE WEAPONTYPE_STINGER
		CASE WEAPONTYPE_BZGAS
		CASE WEAPONTYPE_BALL
		CASE WEAPONTYPE_FLARE
		CASE WEAPONTYPE_FIREEXTINGUISHER
		CASE WEAPONTYPE_DIGISCANNER
		CASE WEAPONTYPE_AIRSTRIKE_ROCKET
		CASE WEAPONTYPE_WATER_CANNON
		CASE WEAPONTYPE_ELECTRIC_FENCE
		CASE WEAPONTYPE_VEHICLE_WEAPON_TANK
		CASE WEAPONTYPE_VEHICLE_SPACE_ROCKET
		CASE WEAPONTYPE_VEHICLE_PLAYER_LASER
		CASE WEAPONTYPE_VEHICLE_PLAYER_BULLET
		CASE WEAPONTYPE_VEHICLE_ROTORS
		CASE WEAPONTYPE_VEHICLE_PLAYER_BUZZARD
		CASE WEAPONTYPE_PASSENGER_ROCKET
		CASE WEAPONTYPE_VEHICLE_ROCKET
		CASE WEAPONTYPE_VEHICLE_WEAPON_PLANE_ROCKET
		CASE WEAPONTYPE_OBJECT
		CASE WEAPONTYPE_BRIEFCASE
		CASE WEAPONTYPE_BRIEFCASE_02
		CASE GADGETTYPE_PARACHUTE
		CASE GADGETTYPE_JETPACK
		CASE WEAPONTYPE_AMMO_RPG
		CASE WEAPONTYPE_AMMO_TANK
		CASE WEAPONTYPE_AMMO_SPACE_ROCKET
		CASE WEAPONTYPE_AMMO_PLAYER_LASER
		CASE WEAPONTYPE_AMMO_ENEMY_LASER
		CASE WEAPONTYPE_AMMO_GRENADE_LAUNCHER
		CASE WEAPONTYPE_AMMO_GRENADE_LAUNCHER_SMOKE
		CASE WEAPONTYPE_RAMMEDBYVEHICLE
		CASE WEAPONTYPE_RUNOVERBYVEHICLE
		CASE WEAPONTYPE_EXPLOSION
		CASE WEAPONTYPE_FALL
		CASE WEAPONTYPE_DLC_PROGRAMMABLEAR
		CASE WEAPONTYPE_DLC_RUBBERGUN
		CASE WEAPONTYPE_DLC_LOUDHAILER
		CASE WEAPONTYPE_DLC_HARPOON
		CASE WEAPONTYPE_DLC_FLAREGUN
		CASE WEAPONTYPE_DLC_GARBAGEBAG
		CASE WEAPONTYPE_DLC_HANDCUFFS
		CASE WEAPONTYPE_DLC_SNOWBALL
		CASE WEAPONTYPE_DLC_VEHICLE_TORPEDO
		CASE WEAPONTYPE_DLC_VEHICLE_SPYCARGUN
		CASE WEAPONTYPE_DLC_VEHICLE_SPYCARROCKET
		
		//Removing the new Gunrunning MK2 weapons from director mode as per bug 3623830 - Steve T.
		CASE  WEAPONTYPE_DLC_ASSAULTRIFLE_MK2 
        CASE WEAPONTYPE_DLC_CARBINERIFLE_MK2 
        CASE WEAPONTYPE_DLC_COMBATMG_MK2
        CASE WEAPONTYPE_DLC_HEAVYSNIPER_MK2 
        CASE WEAPONTYPE_DLC_PISTOL_MK2 
        CASE WEAPONTYPE_DLC_SMG_MK2 

		//Removing the new Christmas2017 MK2 weapons from director mode as per bug 4259343 - Scott R. 
		CASE WEAPONTYPE_DLC_SNSPISTOL_MK2
        CASE WEAPONTYPE_DLC_REVOLVER_MK2
        CASE WEAPONTYPE_DLC_SPECIALCARBINE_MK2
        CASE WEAPONTYPE_DLC_BULLPUPRIFLE_MK2
        CASE WEAPONTYPE_DLC_PUMPSHOTGUN_MK2
        CASE WEAPONTYPE_DLC_MARKSMANRIFLE_MK2
		
			RETURN FALSE
		DEFAULT
			RETURN TRUE
	ENDSWITCH
ENDFUNC

//
//ENUM FEATURE_BLOCK_REASON
//	DMBR_NONE = 0,
//	
//	DMBR_ON_MISSION,
//	DMBR_IN_CUTSCENE,
//	DMBR_IN_SHOP,
//	DMBR_WANTED,
//	DMBR_ONLINE,
//	DMBR_UNSAFE_ACTION,
//	DMBR_UNSAFE_LOCATION,
//	DMBR_IN_VEHICLE,	
//	DMBR_WEARING_PARA,	
//	DMBR_FALLING,		
//	DMBR_DEAD			
//ENDENUM

//
//FUNC STRING GET_DIRECTOR_MODE_BLOCK_MESSAGE_TEXT(FEATURE_BLOCK_REASON eBlockReason)
//
//	SWITCH eBlockReason
//		CASE DMBR_ON_MISSION		RETURN "DI_BLK_MISS" 	BREAK
//		CASE DMBR_IN_SHOP			RETURN "DI_BLK_SHOP"	BREAK
//		CASE DMBR_IN_CUTSCENE		RETURN "DI_BLK_CUTS"	BREAK
//		CASE DMBR_WANTED			RETURN "DI_BLK_WANT"	BREAK
//		CASE DMBR_ONLINE			RETURN "DI_BLK_ONLI"	BREAK
//		CASE DMBR_UNSAFE_ACTION		RETURN "DI_BLK_ACT"		BREAK
//		CASE DMBR_UNSAFE_LOCATION	RETURN "DI_BLK_LOC"		BREAK
//		CASE DMBR_IN_VEHICLE		RETURN "DI_BLK_VEH"		BREAK
//		CASE DMBR_WEARING_PARA		RETURN "DI_BLK_PARA"	BREAK
//		CASE DMBR_FALLING			RETURN "DI_BLK_FALL"	BREAK
//		CASE DMBR_DEAD				RETURN "DI_BLK_DEAD"	BREAK
//		DEFAULT
//			CASSERTLN(DEBUG_DIRECTOR, "DO_DIRECTOR_MODE_CHECKS: Director mode is blocked for a reason that is not being handled. Bug BenR.")
//			RETURN "ERROR"
//		BREAK
//	ENDSWITCH
//	RETURN "ERROR"
//ENDFUNC
//

//PROC CACHE_DIRECTOR_BLOCKED_INTERIORS()
//	CPRINTLN(debug_director,"Director Mode: Caching blocked interior instances")
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_58_SOL_OFFICE, g_intSolomonOffice)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_GARAGEM_SP, g_intPlayerGarage)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_DOWNTOWN, g_intCinema1)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_MORNINGWOOD, g_intCinema2)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CINEMA_VINEWOOD, g_intCinema3)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_CHOPSHOP, g_intChopShop)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_EPSILONISM, g_intEpsilon2)
//	GET_INTERIOR_INSTANCE_INDEX_FOR_INTERIOR(INTERIOR_V_FOUNDRY, g_intFoundry)
//	
//	g_intChickenFactory1 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -76.6618, 6222.1914, 32.2412 >>, "V_factory1")
//	g_intChickenFactory2 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -98.2637, 6210.0225, 31.9240 >>, "V_factory2")
//	g_intChickenFactory3 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -115.8956, 6179.7485, 32.4102 >>, "V_factory3")
//	g_intChickenFactory4 = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -149.8199, 6144.9775, 31.3353 >>, "V_factory4")
//	g_intShootRange = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 16.3605, -1100.2587, 28.7970 >>, "v_gun")
//	g_intStripClub = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 125.1948, -1284.1304, 28.2847 >>, "v_strip3")
//	g_intMine = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-545.5717, 1987.1454, 126.0262>>, "cs6_08_mine_int")
//	g_intOmega = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<2330.5984, 2571.9353, 45.6802>>, "ch3_01_trlr_int")
//	
//	//Shop robbery interiors
//	int i
//	FOR i = 0 to ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS) -1
//		g_intShopRob[i] = GET_INTERIOR_AT_COORDS(GET_SHOP_ROBBERY_POSITION_BY_SHOP_INDEX(INT_TO_ENUM(SHOP_ROBBERIES_SHOP_INDEX, i)))
//	ENDFOR
//	
//	g_bDirectorInteriorsCached = TRUE	
//ENDPROC
//
//
///// PURPOSE: Processes a list of blocked locations, returning whether the player is in any of them
//FUNC FEATURE_BLOCK_REASON GET_BLOCKED_LOCATION_REASON()
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<207.433578,-1019.795410,-100.472763>>, <<189.933777,-1019.623474,-95.568832>>, 17.187500)
//	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)	
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player in vehicle in garage.")
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
//	
//	//Epsilon 2 door check
//	IF g_RandomChars[RC_EPSILON_2].rcIsAwaitingTrigger	//Should be IS_RC_MISSION_AVAILABLE(RC_EPSILON_2) but can't include randomchar_public
//		IF VDIST2(vPlayerPos, <<241.9889, 360.4732, 105.6166>>) < 2
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player too close to Epsilon2 starting area.")
//			RETURN DMBR_ON_MISSION
//		ENDIF
//	ENDIF
//	
//	//Cablecar interior checks
//	IF VDIST2(vPlayerPos, <<-740.93457, 5599.42627, 40.71515>>)<11
//		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<-746.129883,5599.225586,40.475605>>, <<-737.631958,5599.363770,44.169304>>, 3.375000)
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Cablecar 1")
//			RETURN DMBR_UNSAFE_LOCATION
//		ENDIF
//	ENDIF
//		IF VDIST2(vPlayerPos, <<-740.93457, 5590.42627, 40.71515>>)<11
//		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<-746.129883,5590.667480,40.439201>>, <<-737.658508,5590.591797,44.523270>>, 3.375000)
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Cablecar 2")
//			RETURN DMBR_UNSAFE_LOCATION
//		ENDIF
//	ENDIF
//	IF VDIST2(vPlayerPos, <<446.32654, 5566.35010, 780.21515>>)<11
//		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<451.166077,5566.451172,780.170288>>, <<442.521051,5566.374023,783.981934>>, 3.375000)	
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Cablecar 3")
//			RETURN DMBR_UNSAFE_LOCATION
//		ENDIF
//	ENDIF
//	IF VDIST2(vPlayerPos, <<446.32654, 5577.35010, 780.21515>>)<11
//		IF IS_POINT_IN_ANGLED_AREA( vPlayerPos, <<451.166077,5577.866699,780.189880>>, <<442.576501,5577.579102,783.908630>>, 3.375000)
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Cablecar 4")
//			RETURN DMBR_UNSAFE_LOCATION
//		ENDIF
//	ENDIF
//	
//	//---------Various small positions - cheap checks (vdist2) preferred---------
//	//Strip club doors
//	IF VDIST2(vPlayerPos,<<128.83,-1297.98,29.3>>) < 2
//	OR 	VDIST2(vPlayerPos,<<95.07,-1284.98,29.3>>) < 2
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is next to a stripclub entrance")
//		RETURN DMBR_UNSAFE_LOCATION
//	ENDIF
//	//Foundry side door
//	IF VDIST2(vPlayerPos,<<1081.95056, -1976.76184, 30.47218>>) < 6
//#IF NOT USE_SP_DLC
//	AND IS_MISSION_AVAILABLE(SP_MISSION_FINALE_C1)
//#ENDIF
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is next to the foundry door")
//		RETURN DMBR_UNSAFE_LOCATION
//	ENDIF
//	
//	//Cache interior indices for blocked areas
//	IF NOT g_bDirectorInteriorsCached
//		CACHE_DIRECTOR_BLOCKED_INTERIORS()
//	ENDIF
//	
//	INTERIOR_INSTANCE_INDEX intPlayer = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
//	IF intPlayer = NULL
//		RETURN DMBR_NONE
//		//Do nothing as the player is outside
//	ELIF intPlayer = g_intCinema1 OR intPlayer = g_intCinema2 OR intPlayer = g_intCinema3
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in a Cinema.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intSolomonOffice
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Solomon's office.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intPlayerGarage
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Garage.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intChickenFactory1
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Chicken Factory 1.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intChickenFactory2
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Chicken Factory 2.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intChickenFactory3
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Chicken Factory 3.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intChickenFactory4
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Chicken Factory 4.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intShootRange
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Shooting Range.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intStripClub
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Strip Club.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intChopShop
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Chop Shop.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intEpsilon2
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Epsilon2 room.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intMine
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in the Murder Mistery mine area.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intOmega
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in Omega's Garage.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ELIF intPlayer = g_intFoundry
//		CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is inside the foundry.")
//		RETURN DMBR_UNSAFE_LOCATION
//	ENDIF
//	
//	//Check shop robbery interiors
//	INT i
//	FOR i = 0 to ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS)-1
//		IF intPlayer = g_intShopRob[i]
//			CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in robbable shop ",i, " / ",ENUM_TO_INT(NUM_SHOP_ROBBERIES_SHOPS))
//			RETURN DMBR_UNSAFE_LOCATION
//		ENDIF
//	ENDFOR
//	
//	RETURN DMBR_NONE
//ENDFUNC
//
//
///// PURPOSE: Returns whether Director Mode is currently blocked and why.
//FUNC FEATURE_BLOCK_REASON GET_DIRECTOR_MODE_BLOCKED_REASON(BOOL bSilenceDebug = FALSE)
//	
//	#IF IS_FINAL_BUILD
//		UNUSED_PARAMETER(bSilenceDebug)
//	#ENDIF
//
//	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player dead / arrested.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_DEAD
//	ENDIF
//
//	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
//	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: On mission.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	IF IS_RESULT_SCREEN_DISPLAYING()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Results screen on.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	IF IS_COLLECTED_SCREEN_DISPLAYING()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Collected screen on.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_ACTION
//	ENDIF
//	
//	IF g_bPlayerLockedInToTrigger
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is locked in to triggering a mission.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	IF g_bCurrentlyBuyingProperty
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is currently buying property.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_ACTION
//	ENDIF
//	
//	// Covers the gap between being on mission + the pass screen displaying.
//	IF g_bLoadedClifford
//		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchCLF")) > 0 
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Mission stat watcher is running.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_ON_MISSION
//		ENDIF
//	ELIF g_bLoadedNorman
//		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watchNRM")) > 0 
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Mission stat watcher is running.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_ON_MISSION
//		ENDIF
//	ELSE
//		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) > 0 
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Mission stat watcher is running.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_ON_MISSION
//		ENDIF
//	ENDIF
//	
//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_choice")) > 0 
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The Finale Choice script is running.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("creator")) > 0 
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Creator is running.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("respawn_controller")) > 0 
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Respawn controller is running (death/arrest).")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_DEAD
//	ENDIF
//	
//	IF IS_REPEAT_PLAY_ACTIVE()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Repeat play in progress.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	//Falling/skydiving
//	IF IS_PED_FALLING(PLAYER_PED_ID()) OR IS_PED_RAGDOLL(PLAYER_PED_ID())
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player is falling.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_FALLING
//	ENDIF
//	
//	IF IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_GETTING_LAP_DANCE)
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player getting a lapdance.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	//In any shop interior
//	IF IS_PLAYER_IN_ANY_SHOP()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_REPEAT, "Director Mode blocked: Player is inside a shop.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	//Browsing specific shops
//	IF IS_PLAYER_IN_BARBER_CHAIR_OR_DOING_BARBER_INTRO()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_REPEAT, "Director Mode blocked: Player gettting haircut (or in barber intro).")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	IF IS_PLAYER_BROWSING_MOD_SHOP()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player is inside a mod shop.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	IF IS_PLAYER_BROWSING_TATTOO_SHOP()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player browsing tattoo shop.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player browsing in a shop.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	IF IS_CUSTOM_MENU_ON_SCREEN()
//	AND NOT g_bDirectorPIMenuUp
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player using a custom menu.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_ACTION
//	ENDIF
//	
//	IF IS_ANY_SHOP_INTRO_RUNNING()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Shop intro is running.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_SHOP
//	ENDIF
//	
//	IF IS_PLAYER_SWITCH_IN_PROGRESS()
//	OR Is_Player_Timetable_Scene_In_Progress()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player switch is running.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_CUTSCENE
//	ENDIF
//	
//	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
//		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//			//Location checks
//			FEATURE_BLOCK_REASON br = GET_BLOCKED_LOCATION_REASON()
//			IF br <> DMBR_NONE
//				RETURN br
//			ENDIF
//			
//			IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
//			OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_INVALID
//				#IF IS_DEBUG_BUILD
//					IF NOT bSilenceDebug	
//						CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is parachuting.")
//					ENDIF
//				#ENDIF
//				RETURN DMBR_WEARING_PARA
//			ENDIF
//			
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
//				#IF IS_DEBUG_BUILD
//					IF NOT bSilenceDebug	
//						CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in a vehicle.")
//					ENDIF
//				#ENDIF
//				RETURN DMBR_IN_VEHICLE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Vending machine check
//	IF g_bCurrentlyUsingVendingMachine
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Player using a vending machine.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_ACTION
//	ENDIF
//	
//	IF g_bScriptsSetSafeForCutscene
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: Scripts are set safe for cutscene.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_CUTSCENE
//	ENDIF
//	
//	IF IS_PLAYER_PLAYING(PLAYER_ID())
//		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player has a wanted rating.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_WANTED
//		ENDIF
//		
//		IF IS_PLAYER_CLIMBING(PLAYER_ID())
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is climbing.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_UNSAFE_ACTION
//		ENDIF
//		
//		IF IS_PED_GETTING_UP(PLAYER_PED_ID())
//			#IF IS_DEBUG_BUILD
//				IF NOT bSilenceDebug	
//					CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is getting up.")
//				ENDIF
//			#ENDIF
//			RETURN DMBR_UNSAFE_ACTION
//		ENDIF
//	ENDIF
//	
//	IF NETWORK_IS_GAME_IN_PROGRESS()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in an online session.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ONLINE
//	ENDIF
//	
//	IF IS_PLAYER_USING_ATM()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is using an ATM.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_ACTION
//	ENDIF
//	
//	//On Rampage mission
//	IF g_bIsOnRampage
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is locked into a rampage mission.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_ON_MISSION
//	ENDIF
//	
//	//Locked in a Gameplay Hint cam
//	IF IS_GAMEPLAY_HINT_ACTIVE()
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is locked into a Hint Cam.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_IN_CUTSCENE
//	ENDIF
//	
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<439.4357, -997.4747, 28.9584>>,<<428.3288, -997.0398, 24.8372>>,8)
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in impound garage")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_LOCATION
//	ENDIF
//	
//	IF IS_ENTITY_IN_WATER(player_ped_id())
//		#IF IS_DEBUG_BUILD
//			IF NOT bSilenceDebug	
//				CDEBUG1LN(DEBUG_DIRECTOR, "Director Mode blocked: The player is in water.")
//			ENDIF
//		#ENDIF
//		RETURN DMBR_UNSAFE_LOCATION
//	ENDIF
//
//	RETURN DMBR_NONE
//ENDFUNC


//#IF IS_DEBUG_BUILD
//	FUNC STRING DEBUG_GET_DIRECTOR_MODE_BLOCK_REASON_STRING(FEATURE_BLOCK_REASON eBlockReason)
//		SWITCH eBlockReason
//			CASE DMBR_NONE				RETURN "NONE"				BREAK
//			CASE DMBR_ON_MISSION		RETURN "ON_MISSION"			BREAK
//			CASE DMBR_IN_CUTSCENE		RETURN "IN_CUTSCENE"		BREAK
//			CASE DMBR_IN_SHOP			RETURN "IN_SHOP"			BREAK
//			CASE DMBR_WANTED			RETURN "WANTED"				BREAK
//			CASE DMBR_ONLINE			RETURN "ONLINE"				BREAK
//			CASE DMBR_UNSAFE_ACTION		RETURN "UNSAFE_ACTION"		BREAK
//			CASE DMBR_UNSAFE_LOCATION	RETURN "UNSAFE_LOCATION"	BREAK
//			CASE DMBR_IN_VEHICLE		RETURN "IN_VEHICLE"			BREAK
//			CASE DMBR_WEARING_PARA		RETURN "WEARING_PARA"		BREAK
//			CASE DMBR_FALLING			RETURN "FALLING"			BREAK
//			CASE DMBR_DEAD				RETURN "DEAD"				BREAK
//		ENDSWITCH
//
//		CASSERTLN(DEBUG_DIRECTOR, "DEBUG_GET_DIRECTOR_MODE_BLOCK_REASON_STRING: Block reason has no debug string defined.")
//		RETURN "INVALID!"
//	ENDFUNC
//#ENDIF


PROC UNLOCK_AUTO_DOORS(BOOL bDoorDataCheck)	
	IF bDoorDataCheck = TRUE
		CPRINTLN(DEBUG_DIRECTOR, "Door Data isn't loaded, can't edit doors yet")
		EXIT
	ENDIF
	
	INT i
	INT limit = ENUM_TO_INT(AUTODOOR_MAX)-1
	FOR i = 0 TO limit
		IF i != ENUM_TO_INT(AUTODOOR_HAYES_GARAGE)
			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[i].doorID, DOORSTATE_UNLOCKED)
		ENDIF
	ENDFOR
ENDPROC


PROC STORE_AUTO_DOORS_STATES_AND_SET_ALL_UNLOCKED(DOOR_STATE_ENUM &doorArray[], BOOL &bDoorDataCheck)	
	IF g_sAutoDoorData[0].doorID = 0
	AND bDoorDataCheck = FALSE
		CPRINTLN(DEBUG_DIRECTOR, "Door Data isn't loaded, not caching it")
		bDoorDataCheck = TRUE
		EXIT
	ENDIF
	
	INT i
	INT limit = ENUM_TO_INT(AUTODOOR_MAX)-1
	FOR i = 0 TO limit
		doorArray[i] = DOOR_SYSTEM_GET_DOOR_STATE(g_sAutoDoorData[i].doorID)
	ENDFOR
	
	bDoorDataCheck = FALSE
ENDPROC


PROC RESTORE_AUTO_DOORS_STATES(DOOR_STATE_ENUM &doorArray[], BOOL bDoorDataCheck)
	IF bDoorDataCheck = TRUE
		CPRINTLN(DEBUG_DIRECTOR, "Door Data wasn't loaded, no values to restore")
		EXIT
	ENDIF
	
	INT i
	INT limit = ENUM_TO_INT(AUTODOOR_MAX)-1
	FOR i = 0 TO limit
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[i].doorID, doorArray[i])
	ENDFOR
ENDPROC

STRUCT DIR_OUTFIT_DATA
	//clothes
	INT 	iComponentDrawableID[NUM_PED_COMPONENTS]
	INT 	iComponentTextureID[NUM_PED_COMPONENTS]	
	INT 	iPropDrawableID[9]
	INT 	iPropTextureID[9]	
	
	//Head blend
	INT 	iHeadMum 		
	INT 	iHeadMumVar 	
	INT 	iHeadDad 		
	INT 	iHeadDadVar 	
	FLOAT 	fDominance	
	FLOAT	fSkinToneBlend
	
	//Micromorphs
	MICRO_MORPH_TYPE 	MicroMorph[ciCHARACTER_CREATOR_MAX_MICRO_MORPH]
	FLOAT 				fMorphBlend[ciCHARACTER_CREATOR_MAX_MICRO_MORPH]
	
	//hair
	PED_COMP_NAME_ENUM 	whichHair
	INT					iColour1
	INT 				iColour2
	
	//Overlay	
	HEAD_OVERLAY_SLOT 	overlaySlot[ciCHARACTER_CREATOR_MAX_OVERLAYS]
	INT 				iOverlayValue[ciCHARACTER_CREATOR_MAX_OVERLAYS]
	FLOAT 				fOverlayBlend[ciCHARACTER_CREATOR_MAX_OVERLAYS]	
	INT 				iColour[ciCHARACTER_CREATOR_MAX_OVERLAYS]
	INT 				iColourSaved2[ciCHARACTER_CREATOR_MAX_OVERLAYS]
	RAMP_TYPE 			rtType[ciCHARACTER_CREATOR_MAX_OVERLAYS]
	
	//eyes
	FLOAT 				fEyes
	
	//Tattoos
	TATTOO_NAME_ENUM	eTatEnum[MAX_TATTOOS_IN_SHOP]
	INT					iTatCollection[MAX_TATTOOS_IN_SHOP]
	INT					iTatPreset[MAX_TATTOOS_IN_SHOP]	
	
	//crew
	CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM eShirtType = CREW_TSHIRT_OFF
	
ENDSTRUCT
STRUCT DIR_ONLINE_CHAR
	DIR_OUTFIT_DATA outfitdata
	MODEL_NAMES		model		= DUMMY_MODEL_FOR_SCRIPT
	BOOL			bSetup		= False
ENDSTRUCT

ENUM DIRECTOR_CHAR_OUTFITS
	 DCO_NONE = -1	
	 
	,DCO_AMANDA_ARM3				= 0//DEFAULT CHAR START
	,DCO_AMANDA_FAM5
	,DCO_AMANDA_FAM6
	,DCO_AMANDA_FACEMASK
	,DCO_AMANDA_MIKE4
	,DCO_AMANDA_TENNIS  
	,DCO_AMANDA_MIKE_EVENT
	,DCO_AMANDA_CAR_SCENE
	,DCO_AMANDA_ROUND_TABLE
	,DCO_AMANDA_SHOUT_AT_MAID
	,DCO_AMANDA_YOGA									//10
	,DCO_AMANDA_SUNBATHING
	,DCO_AMANDA_PASSED_OUT
	
	,DCO_BEVERLY_1 //DEFAULT CHAR START
	
	,DCO_BRAD_PROLOGUE //DEFAULT CHAR START
	,DCO_BRAD_NO_MASK
	
	,DCO_CHRIS_FORMAGE_1 //DEFAULT CHAR START
	
	,DCO_DAVE_FIB1 //DEFAULT CHAR START
	,DCO_DAVE_FIB3
	,DCO_DAVE_FIB5_DIVE
	,DCO_DAVE_FIB5_GAS_MASK
	
	,DCO_DEVIN_1//DEFAULT CHAR START 					//20
	,DCO_DEVIN_CS2
 	,DCO_DEVIN_CS3
 	,DCO_DEVIN_FIN

	,DCO_DENISE_PELVIC_FLOOR//DEFAULT CHAR START
	,DCO_DENISE_FACEMASK
	
	,DCO_DR_FRIEDLANDER_1//DEFAULT CHAR START
	,DCO_DR_FRIEDLANDER_2
	
	,DCO_FABEIN_FAM5//DEFAULT CHAR START
	,DCO_FABEIN_FAM6
	
	,DCO_FLOYD_DOCK_SETUP//DEFAULT CHAR START
	,DCO_FLOYD_DOCK_PREP
	,DCO_FLOYD_CRYING1
	,DCO_FLOYD_CRYING2
	
	,DCO_JIMMY_ARM1//DEFAULT CHAR START   				//30
	,DCO_JIMMY_ARM3
	,DCO_JIMMY_FAM2
	,DCO_JIMMY_FAM5
	,DCO_JIMMY_FAM6
	,DCO_JIMMY_MIKE_EVENT
	,DCO_JIMMY_DROPOFF_SCENES
	,DCO_JIMMY_GAMING
	,DCO_JIMMY_WATCHING_TV
	,DCO_JIMMY_STUDIO
	
	,DCO_LAMAR_ARM1//DEFAULT CHAR START   				//40
	,DCO_LAMAR_ARM2
	,DCO_LAMAR_LAMAR1
	,DCO_LAMAR_FRANK2
	,DCO_LAMAR_TAUNT1
	,DCO_LAMAR_TAUNT2
	,DCO_LAMAR_TAUNT3
	
	,DCO_LAZLOW_1//DEFAULT CHAR START
	,DCO_LAZLOW_PREM
	
	,DCO_LESTER_LESTER1//DEFAULT CHAR START
	,DCO_LESTER_PALETO_SETUP
	
	,DCO_MAUDE_1//DEFAULT CHAR START   					//50
	
	,DCO_MRS_THORNHILL_1//DEFAULT CHAR START
	
//	,DCO_RON_1//DEFAULT CHAR START	
	,DCO_RON_BORING//DEFAULT CHAR START
	
	,DCO_PATRICIA_1//DEFAULT CHAR START
	
	,DCO_RICKIE_1//DEFAULT CHAR START
	
	,DCO_SIMEON_ARM1//DEFAULT CHAR START
	,DCO_SIMEON_ARM2
	,DCO_SIMEON_ARM3
	
	,DCO_SOLOMON_1//DEFAULT CHAR START
	
	,DCO_STEVE_HAINES_FIB2//DEFAULT CHAR START   		//60
	,DCO_STEVE_HAINES_FIB4
	,DCO_STEVE_HAINES_FIB5_DIVE
	,DCO_STEVE_HAINES_FIB5_GAS_MASK
	
	,DCO_STRETCH_1//DEFAULT CHAR START
	
	,DCO_TANISHA_1//DEFAULT CHAR START
	
	,DCO_TAO_CHENG_CHINESE1//DEFAULT CHAR START
	,DCO_TAO_CHENG_CHINESE2
	
	,DCO_TRACEY_ARM3//DEFAULT CHAR START
	,DCO_TRACEY_FAM2
	,DCO_TRACEY_FAM6									//70
	,DCO_TRACEY_MIKE4
	,DCO_TRACEY_MIKE_EVENT
	,DCO_TRACEY_DROPOFF_SCENE_A
	,DCO_TRACEY_DROPOFF_SCENE_B
	,DCO_TRACEY_TV
	,DCO_TRACEY_ROUND_TABLE
	,DCO_TRACEY_CAR
	,DCO_TRACEY_SUNBATHING
	,DCO_TRACEY_WORKOUT
	,DCO_TRACEY_DRUGS									//80
	,DCO_TRACEY_DRUNK
	
	,DCO_WADE_TREV1//DEFAULT CHAR START
	,DCO_WADE_TREV3
	,DCO_WADE_DOCK_SETUP
	
	,DCO_MOLLY_1//DEFAULT CHAR START
	
	//SPECIAL Ped Variations
	,DCO_ANDY_MOON_1
	,DCO_BAYGOR_1
	,DCO_BILL_BINDER_1
	,DCO_CLINTON_1
	,DCO_GRIFF_1
	,DCO_IMPOTENT_RAGE_1
	,DCO_JANE_1
	,DCO_JANE_2
	,DCO_JANE_3
	,DCO_JEROME_1
	,DCO_JESCO_1
	,DCO_JEESE_1
	,DCO_MANI_1
	,DCO_MIME_1
	,DCO_PAMELA_DRAKE_1
	,DCO_ZOMBIE_1
		
	,DCO_MULTIPLAYER_1
	,DCO_MULTIPLAYER_2
	
	,MAX_DIRECTOR_CHAR_OUTFITS
ENDENUM

FUNC BOOL DOES_OUTFIT_BELONG_TO_DIRECTOR_CHAR(DIRECTOR_CHAR_OUTFITS eOutfit , MODEL_NAMES eCharModel) 

	SWITCH eCharModel
		
		CASE IG_AMANDATOWNLEY
			SWITCH eOutfit
				CASE DCO_AMANDA_FAM5        
				CASE DCO_AMANDA_ARM3
				CASE DCO_AMANDA_FAM6
				CASE DCO_AMANDA_MIKE4
				CASE DCO_AMANDA_MIKE_EVENT
				CASE DCO_AMANDA_CAR_SCENE
				CASE DCO_AMANDA_ROUND_TABLE
				CASE DCO_AMANDA_TENNIS
				CASE DCO_AMANDA_FACEMASK
				CASE DCO_AMANDA_SHOUT_AT_MAID
				CASE DCO_AMANDA_YOGA
				CASE DCO_AMANDA_SUNBATHING
				CASE DCO_AMANDA_PASSED_OUT
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_BEVERLY	
			SWITCH eOutfit
				case DCO_BEVERLY_1
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_BRAD	
			SWITCH eOutfit
				case DCO_BRAD_PROLOGUE
				CASE DCO_BRAD_NO_MASK
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_CHRISFORMAGE	
			SWITCH eOutfit
				case DCO_CHRIS_FORMAGE_1
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_DAVENORTON	
			SWITCH eOutfit
				CASE DCO_DAVE_FIB1
				CASE DCO_DAVE_FIB3
				CASE DCO_DAVE_FIB5_DIVE
				CASE DCO_DAVE_FIB5_GAS_MASK
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_DEVIN	
			SWITCH eOutfit
				case DCO_DEVIN_1
				CASE DCO_DEVIN_CS2
				CASE DCO_DEVIN_CS3
				CASE DCO_DEVIN_FIN
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_DENISE	
			SWITCH eOutfit
				case DCO_DENISE_FACEMASK
				case DCO_DENISE_PELVIC_FLOOR
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_DRFRIEDLANDER	
			SWITCH eOutfit
				case DCO_DR_FRIEDLANDER_1
				CASE DCO_DR_FRIEDLANDER_2
					RETURN TRUE
				break
			ENDSWITCH
		BREAK
		CASE IG_FABIEN	
			SWITCH eOutfit
				case DCO_FABEIN_FAM5
				case DCO_FABEIN_FAM6
					RETURN TRUE
				break	
			ENDSWITCH
		BREAK
		CASE IG_FLOYD	
			SWITCH eOutfit
				case DCO_FLOYD_CRYING1
				case DCO_FLOYD_CRYING2
				case DCO_FLOYD_DOCK_PREP
				case DCO_FLOYD_DOCK_SETUP
					RETURN TRUE
				break	
			ENDSWITCH
		BREAK
		CASE IG_JIMMYDISANTO	
			SWITCH eOutfit
				case DCO_JIMMY_ARM1
				case DCO_JIMMY_ARM3
				case DCO_JIMMY_FAM2
				case DCO_JIMMY_FAM5
				case DCO_JIMMY_FAM6
				case DCO_JIMMY_MIKE_EVENT
				case DCO_JIMMY_DROPOFF_SCENES
				case DCO_JIMMY_GAMING
				case DCO_JIMMY_WATCHING_TV
				case DCO_JIMMY_STUDIO				
					RETURN TRUE
				break	
			ENDSWITCH
		BREAK
		CASE IG_LAMARDAVIS	
			SWITCH eOutfit
				CASE DCO_LAMAR_ARM1
				CASE DCO_LAMAR_ARM2
				CASE DCO_LAMAR_LAMAR1
				CASE DCO_LAMAR_FRANK2
				CASE DCO_LAMAR_TAUNT1
				CASE DCO_LAMAR_TAUNT2
				CASE DCO_LAMAR_TAUNT3	
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_LAZLOW	 
			SWITCH eOutfit
				CASE DCO_LAZLOW_1
				CASE DCO_LAZLOW_PREM
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_LESTERCREST	
			SWITCH eOutfit
				CASE DCO_LESTER_LESTER1
				CASE DCO_LESTER_PALETO_SETUP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_MAUDE	
		
			SWITCH eOutfit
				CASE DCO_MAUDE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_MRS_THORNHILL
			SWITCH eOutfit
				CASE DCO_MRS_THORNHILL_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_NERVOUSRON	
			SWITCH eOutfit
//				CASE DCO_RON_1
				CASE DCO_RON_BORING
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_PATRICIA	
			SWITCH eOutfit
				CASE DCO_PATRICIA_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_LIFEINVAD_01	
			SWITCH eOutfit
				CASE DCO_RICKIE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_SIEMONYETARIAN	 
			SWITCH eOutfit
				CASE DCO_SIMEON_ARM1
				CASE DCO_SIMEON_ARM2
				CASE DCO_SIMEON_ARM3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_SOLOMON	
			SWITCH eOutfit
				CASE DCO_SOLOMON_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_STEVEHAINS
			SWITCH eOutfit
				CASE DCO_STEVE_HAINES_FIB2
				CASE DCO_STEVE_HAINES_FIB4
				CASE DCO_STEVE_HAINES_FIB5_DIVE
				CASE DCO_STEVE_HAINES_FIB5_GAS_MASK
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_STRETCH
			SWITCH eOutfit
				CASE DCO_STRETCH_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_TANISHA	
			SWITCH eOutfit
				CASE DCO_TANISHA_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_TAOCHENG	
			SWITCH eOutfit
				CASE DCO_TAO_CHENG_CHINESE1
				CASE DCO_TAO_CHENG_CHINESE2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_TRACYDISANTO
			SWITCH eOutfit
				CASE DCO_TRACEY_ARM3
				CASE DCO_TRACEY_CAR
				CASE DCO_TRACEY_DROPOFF_SCENE_A
				CASE DCO_TRACEY_DROPOFF_SCENE_B
				CASE DCO_TRACEY_DRUGS
				CASE DCO_TRACEY_DRUNK
				CASE DCO_TRACEY_FAM2
				CASE DCO_TRACEY_FAM6
				CASE DCO_TRACEY_MIKE_EVENT
				CASE DCO_TRACEY_MIKE4
				CASE DCO_TRACEY_ROUND_TABLE
				CASE DCO_TRACEY_SUNBATHING
				CASE DCO_TRACEY_TV
				CASE DCO_TRACEY_WORKOUT				
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IG_WADE	
			SWITCH eOutfit
				CASE DCO_WADE_DOCK_SETUP		
				CASE DCO_WADE_TREV1
				CASE DCO_WADE_TREV3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK		
		CASE U_M_Y_Hippie_01
			SWITCH eOutfit
				CASE DCO_ANDY_MOON_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE U_M_Y_BAYGOR
			SWITCH eOutfit
				CASE DCO_BAYGOR_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_O_FinGuru_01 
			SWITCH eOutfit
				CASE DCO_BILL_BINDER_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_Y_MILITARYBUM 
			SWITCH eOutfit
				CASE DCO_CLINTON_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_M_GRIFF_01 
			SWITCH eOutfit
				CASE DCO_GRIFF_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_Y_IMPORAGE 
			SWITCH eOutfit
				CASE DCO_IMPOTENT_RAGE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_F_Y_COMJane 
			SWITCH eOutfit
				CASE DCO_JANE_1
				CASE DCO_JANE_2
				CASE DCO_JANE_3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE S_M_M_StrPreach_01 
			SWITCH eOutfit
				CASE DCO_JEROME_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_O_TAPHILLBILLY 
			SWITCH eOutfit
				CASE DCO_JESCO_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE u_m_m_jesus_01 
			SWITCH eOutfit
				CASE DCO_JEESE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_Y_MANI 
			SWITCH eOutfit
				CASE DCO_MANI_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE S_M_Y_MIME 
			SWITCH eOutfit
				CASE DCO_MIME_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_F_O_MOVIESTAR 
			SWITCH eOutfit
				CASE DCO_PAMELA_DRAKE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE U_M_Y_ZOMBIE_01 
			SWITCH eOutfit
				CASE DCO_ZOMBIE_1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE MP_M_FREEMODE_01
		CASE MP_F_FREEMODE_01
			SWITCH eOutfit
				CASE DCO_MULTIPLAYER_1
				CASE DCO_MULTIPLAYER_2
					RETURN TRUE
				BREAK
			ENDSWITCH			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CHAR_OUTFIT_MAX(MODEL_NAMES eDirChar)	
	int i
	int iOutfitAmount = -1 //set amount to -1 //  0 will repersent default outfit
	REPEAT Enum_to_int(MAX_DIRECTOR_CHAR_OUTFITS) i
		if DOES_OUTFIT_BELONG_TO_DIRECTOR_CHAR(INT_TO_ENUM(DIRECTOR_CHAR_OUTFITS,i),eDirChar)
			iOutfitAmount++
		endif
	ENDREPEAT
	RETURN iOutfitAmount
ENDFUNC
FUNC DIRECTOR_CHAR_OUTFITS GET_CHAR_OUTFIT_DEFAULT(MODEL_NAMES eDirChar)
	
	SWITCH eDirChar		
		CASE IG_AMANDATOWNLEY
			RETURN DCO_AMANDA_ARM3
		BREAK
		CASE IG_BEVERLY
			RETURN DCO_BEVERLY_1
		BREAK
		CASE IG_BRAD	
			RETURN DCO_BRAD_PROLOGUE
		BREAK
		CASE IG_CHRISFORMAGE
			RETURN DCO_CHRIS_FORMAGE_1
		BREAK
		CASE IG_DAVENORTON	
			RETURN DCO_DAVE_FIB1
		BREAK
		CASE IG_DEVIN			
			RETURN DCO_DEVIN_1
		BREAK
		CASE IG_DENISE	
			RETURN DCO_DENISE_PELVIC_FLOOR
		BREAK
		CASE IG_DRFRIEDLANDER	
			RETURN DCO_DR_FRIEDLANDER_1
		BREAK
		CASE IG_FABIEN	
			RETURN DCO_FABEIN_FAM5
		BREAK
		CASE IG_FLOYD	
			RETURN DCO_FLOYD_DOCK_SETUP
		BREAK
		CASE IG_JIMMYDISANTO	
			RETURN DCO_JIMMY_ARM1
		BREAK
		CASE IG_LAMARDAVIS	
			RETURN DCO_LAMAR_ARM1
		BREAK
		CASE IG_LAZLOW	 
			RETURN DCO_LAZLOW_1
		BREAK
		CASE IG_LESTERCREST	
			RETURN DCO_LESTER_LESTER1
		BREAK
		CASE IG_MAUDE	
			RETURN DCO_MAUDE_1
		BREAK
		CASE IG_MRS_THORNHILL
			RETURN DCO_MRS_THORNHILL_1
		BREAK
		CASE IG_NERVOUSRON	
			RETURN DCO_RON_BORING
		BREAK
		CASE IG_PATRICIA	
			RETURN DCO_PATRICIA_1
		BREAK
		CASE IG_LIFEINVAD_01 //RICKIE MODEL COULD BE WRONG
			RETURN DCO_RICKIE_1
		BREAK
		CASE IG_SIEMONYETARIAN	 
			RETURN DCO_SIMEON_ARM1
		BREAK
		CASE IG_SOLOMON	
			RETURN DCO_SOLOMON_1
		BREAK
		CASE IG_STEVEHAINS
			RETURN DCO_STEVE_HAINES_FIB2
		BREAK
		CASE IG_STRETCH
			RETURN DCO_STRETCH_1
		BREAK
		CASE IG_TANISHA	
			RETURN DCO_TANISHA_1
		BREAK
		CASE IG_TAOCHENG	
			RETURN DCO_TAO_CHENG_CHINESE1 
		BREAK
		CASE IG_TRACYDISANTO
			RETURN DCO_TRACEY_ARM3 
		BREAK
		CASE IG_WADE	
			RETURN DCO_WADE_TREV1					
		BREAK			
	
		CASE U_M_Y_Hippie_01
			RETURN DCO_ANDY_MOON_1
		BREAK
		
		CASE U_M_Y_BAYGOR
			RETURN DCO_BAYGOR_1
		BREAK
		CASE U_M_O_FinGuru_01
			RETURN DCO_BILL_BINDER_1
		BREAK
		CASE U_M_Y_MILITARYBUM
			RETURN DCO_CLINTON_1
		BREAK
		CASE U_M_M_GRIFF_01
			RETURN DCO_GRIFF_1
		BREAK
		CASE U_M_Y_IMPORAGE
			RETURN DCO_IMPOTENT_RAGE_1
		BREAK
		CASE U_F_Y_COMJane
			RETURN DCO_JANE_1
		BREAK
		CASE S_M_M_StrPreach_01
			RETURN DCO_JEROME_1
		BREAK
		CASE U_M_O_TAPHILLBILLY
			RETURN DCO_JESCO_1
		BREAK
		CASE u_m_m_jesus_01
			RETURN DCO_JEESE_1
		BREAK
		CASE U_M_Y_MANI
			RETURN DCO_MANI_1
		BREAK
		CASE S_M_Y_MIME
			RETURN DCO_MIME_1
		BREAK
		CASE U_F_O_MOVIESTAR
			RETURN DCO_PAMELA_DRAKE_1
		BREAK
		CASE U_M_Y_ZOMBIE_01
			RETURN DCO_ZOMBIE_1
		BREAK
		CASE MP_M_FREEMODE_01
		CASE MP_F_FREEMODE_01
			RETURN DCO_MULTIPLAYER_1			
		BREAK
		
	ENDSWITCH
	RETURN DCO_NONE
ENDFUNC

FUNC BOOL GET_DIRECTOR_CHAR_OUTFIT_DATA(DIR_OUTFIT_DATA &sOutfitsData,DIRECTOR_CHAR_OUTFITS eCharOutfit,MODEL_NAMES ePedModel = DUMMY_MODEL_FOR_SCRIPT)
	INT i
	REPEAT NUM_PED_COMPONENTS i
		sOutfitsData.iComponentDrawableID[i] = -1
	ENDREPEAT	
	REPEAT COUNT_OF(sOutfitsData.iPropDrawableID) i
		sOutfitsData.iPropDrawableID[i] = -1
	ENDREPEAT
	
	sOutfitsData.iHeadMum 		= -1
	sOutfitsData.iHeadMumVar 	= -1
	sOutfitsData.iHeadDad 		= -1
	sOutfitsData.iHeadDadVar 	= -1
	sOutfitsData.fDominance 	= -1
	sOutfitsData.fSkinToneBlend = -1	
	
	REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH i
		sOutfitsData.MicroMorph[i] 		= MMT_NOSE_WIDTH
		sOutfitsData.fMorphBlend[i] 	= -1
	ENDREPEAT
	
	sOutfitsData.whichHair = DUMMY_PED_COMP
	sOutfitsData.iColour1	= -1
	sOutfitsData.iColour2	= -1
	
	REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS i
		sOutfitsData.overlaySlot[i] 	= HOS_BLEMISHES 
		sOutfitsData.iOverlayValue[i]	= -1
		sOutfitsData.fOverlayBlend[i]	= -1
		sOutfitsData.iColour[i]			= -1
		sOutfitsData.iColourSaved2[i]	= -1
		sOutfitsData.rtType[i]			= RT_NONE
	ENDREPEAT
	
	sOutfitsData.fEyes = -1
	
	REPEAT MAX_TATTOOS_IN_SHOP i
		sOutfitsData.eTatEnum[i]		= INVALID_TATTOO
		sOutfitsData.iTatCollection[i]	= -1
		sOutfitsData.iTatPreset[i]		= -1
	ENDREPEAT
	
	sOutfitsData.eShirtType = CREW_TSHIRT_OFF
	
	SWITCH eCharOutfit
		
		CASE DCO_AMANDA_ARM3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_AMANDA_FAM5 
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_AMANDA_FAM6
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			
		BREAK
		CASE DCO_AMANDA_MIKE4
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK		
		CASE DCO_AMANDA_CAR_SCENE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_AMANDA_ROUND_TABLE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_AMANDA_MIKE_EVENT
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0			
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_AMANDA_TENNIS
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_AMANDA_SHOUT_AT_MAID
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_AMANDA_FACEMASK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	2
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	7	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 1							sOutfitsData.iPropTextureID[1] = 0
		BREAK	
		CASE DCO_AMANDA_YOGA
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK		
		CASE DCO_AMANDA_SUNBATHING
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	6	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_AMANDA_PASSED_OUT
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_BEVERLY_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
		BREAK
		CASE DCO_BRAD_PROLOGUE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_BRAD_NO_MASK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_CHRIS_FORMAGE_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
		BREAK
		CASE DCO_DAVE_FIB1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0

		BREAK
		CASE DCO_DAVE_FIB3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   2
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	3
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_DAVE_FIB5_DIVE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_DAVE_FIB5_GAS_MASK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
			sOutfitsData.iPropDrawableID[1] = 2							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_DEVIN_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_DEVIN_CS2							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1		sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0
		BREAK							
		CASE DCO_DEVIN_CS3							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	2		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0
		BREAK							
		CASE DCO_DEVIN_FIN							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	4		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	3		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1		sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0
		BREAK		 				 	
		
		CASE DCO_DENISE_PELVIC_FLOOR
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_DENISE_FACEMASK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_DR_FRIEDLANDER_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_DR_FRIEDLANDER_2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_FABEIN_FAM5
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_FABEIN_FAM6
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_FLOYD_DOCK_SETUP
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_FLOYD_DOCK_PREP
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_FLOYD_CRYING1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_FLOYD_CRYING2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_JIMMY_ARM1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_ARM3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_FAM2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_FAM5
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	2
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	2
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_FAM6
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	3	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_MIKE_EVENT
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	6	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_DROPOFF_SCENES
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_GAMING
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_WATCHING_TV
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_JIMMY_STUDIO
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	2
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_ARM1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_ARM2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_LAMAR1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   2
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_FRANK2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_TAUNT1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_TAUNT2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAMAR_TAUNT3	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   2
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAZLOW_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_LAZLOW_PREM							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	29		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	25		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	13		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	18		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2]	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0
		BREAK		 				 	
		
		CASE DCO_LESTER_LESTER1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_LESTER_PALETO_SETUP
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] = 0			sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_MAUDE_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_MRS_THORNHILL_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
//		CASE DCO_RON_1
//			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
//			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
//			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
//			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
//			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
//			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
//			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
//		BREAK
		CASE DCO_RON_BORING
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
			sOutfitsData.iPropDrawableID[0] = 0			sOutfitsData.iPropTextureID[0] = 0
			sOutfitsData.iPropDrawableID[1] = 0			sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_PATRICIA_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_RICKIE_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
		BREAK
		CASE DCO_SIMEON_ARM1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
		BREAK
		CASE DCO_SIMEON_ARM2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
		BREAK
		CASE DCO_SIMEON_ARM3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
		BREAK
		CASE DCO_SOLOMON_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_STEVE_HAINES_FIB2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_STEVE_HAINES_FIB4
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_STEVE_HAINES_FIB5_DIVE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_STEVE_HAINES_FIB5_GAS_MASK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] = 2							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_STRETCH_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[0] = 0							sOutfitsData.iPropTextureID[0] = 0
		BREAK
		CASE DCO_TANISHA_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TAO_CHENG_CHINESE1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0			
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		
		BREAK
		CASE DCO_TAO_CHENG_CHINESE2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_TRACEY_ARM3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_FAM2
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK
		CASE DCO_TRACEY_FAM6		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	3		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	4		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	3		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[0] 	=	0						sOutfitsData.iPropTextureID[0] 	=	0
		BREAK		 				 	

		CASE DCO_TRACEY_MIKE4
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	1
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_MIKE_EVENT
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_DROPOFF_SCENE_A
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_DROPOFF_SCENE_B
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_TV
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_ROUND_TABLE
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_CAR
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_SUNBATHING	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	7	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] = 1							sOutfitsData.iPropTextureID[0] = 0
		BREAK
		CASE DCO_TRACEY_WORKOUT
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_DRUGS
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_TRACEY_DRUNK
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	5	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	6	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	2	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK
		CASE DCO_WADE_TREV1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
			sOutfitsData.iPropDrawableID[0] = 0							sOutfitsData.iPropTextureID[0] = 0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
			
		BREAK
		CASE DCO_WADE_TREV3
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	4	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	2	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   1
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	3	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_WADE_DOCK_SETUP
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	1	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	1	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=   0
		BREAK
		CASE DCO_MOLLY_1
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	= 	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	=   0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =   0
			sOutfitsData.iPropDrawableID[1] = 0							sOutfitsData.iPropTextureID[1] = 0
		BREAK
		CASE DCO_ANDY_MOON_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK		 				 	
		CASE DCO_BAYGOR_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
			sOutfitsData.iPropDrawableID[1] 	=	0					sOutfitsData.iPropTextureID[1] 	=	0
		BREAK		 				 	
		CASE DCO_BILL_BINDER_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK		 				 	
		CASE DCO_CLINTON_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
		BREAK		 				 	
		CASE DCO_GRIFF_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	=	0
			sOutfitsData.iPropDrawableID[0] 					=	0	sOutfitsData.iPropTextureID[0] 						=	0
			sOutfitsData.iPropDrawableID[1] 					=	0	sOutfitsData.iPropTextureID[1] 						=	0
		BREAK		 				 	
		CASE DCO_IMPOTENT_RAGE_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2]=	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2] =	0
		BREAK		 				 	
		CASE DCO_JANE_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	2
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	1		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] 	=	0						sOutfitsData.iPropTextureID[1] 	=	0
		BREAK		 				 	
		CASE DCO_JANE_2							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	1
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] 	=	0						sOutfitsData.iPropTextureID[1] 	=	0
		BREAK							
		CASE DCO_JANE_3							
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	2
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	2		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] 	=	0						sOutfitsData.iPropTextureID[1] 	=	0
		BREAK							
	 				 	
		CASE DCO_JEROME_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iPropDrawableID[1] 					=	0		sOutfitsData.iPropTextureID[1] 						=	0
		BREAK		 				 	
	 				 	
		CASE DCO_JESCO_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0			
		BREAK		 				 	
		 	
		CASE DCO_JEESE_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
		BREAK		 				 	
		CASE DCO_MANI_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0	sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	=	0
		BREAK		 				 	
		CASE DCO_MIME_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0	sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
		BREAK		 				 	
		CASE DCO_PAMELA_DRAKE_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAIR] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAIR]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL] =	0		sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]  =	0
			sOutfitsData.iPropDrawableID[1] 	=	0						sOutfitsData.iPropTextureID[1] 	=	0
		BREAK							
			
		CASE DCO_ZOMBIE_1		 				 	
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	=	0
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	=	0		sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	=	0
		BREAK							
		
		CASE DCO_MULTIPLAYER_1
		CASE DCO_MULTIPLAYER_2
			INT iLastActiveCharacterSlot
			if  eCharOutfit= DCO_MULTIPLAYER_1
				iLastActiveCharacterSlot = 0
			elif eCharOutfit = DCO_MULTIPLAYER_2
				iLastActiveCharacterSlot = 1
			endif
			
//			GET_MENU_PED_INT_STAT(INT_TO_ENUM(STATSENUM, ENUM_TO_INT(MPPLY_LAST_MP_CHAR)), iLastActiveCharacterSlot)
			
			sOutfitsData.iComponentDrawableID[PED_COMP_HEAD] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HEAD,     iLastActiveCharacterSlot)	
			sOutfitsData.iComponentTextureID[PED_COMP_HEAD]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HEAD, 	 iLastActiveCharacterSlot)	
			sOutfitsData.iComponentDrawableID[PED_COMP_BERD] 	 = GET_STORED_MP_PLAYER_COMPONENT_CHARACTER_MENU_BERD_DRAWABLE(iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_BERD]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_BERD,     iLastActiveCharacterSlot)	
			sOutfitsData.iComponentDrawableID[PED_COMP_TORSO] 	 = GET_STORED_MP_PLAYER_COMPONENT_CHARACTER_MENU_TORSO_DRAWABLE(iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_TORSO]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_TORSO,    iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_LEG] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_LEG,      iLastActiveCharacterSlot) 
			sOutfitsData.iComponentTextureID[PED_COMP_LEG]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_LEG,      iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_HAND] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HAND,     iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_HAND]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HAND,     iLastActiveCharacterSlot)	
			sOutfitsData.iComponentDrawableID[PED_COMP_FEET] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_FEET,     iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_FEET]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_FEET,     iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_TEETH] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_TEETH,    iLastActiveCharacterSlot) 
			sOutfitsData.iComponentTextureID[PED_COMP_TEETH]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_TEETH,    iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL]  = GET_STORED_MP_PLAYER_COMPONENT_CHARACTER_MENU_SPECIAL_DRAWABLE(iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL]   = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_SPECIAL,  iLastActiveCharacterSlot) 
			sOutfitsData.iComponentDrawableID[PED_COMP_SPECIAL2] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_SPECIAL2, iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_SPECIAL2]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_SPECIAL2, iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_DECL] 	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_DRAWVAR_DECL,     iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_DECL]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_DECL,     iLastActiveCharacterSlot)
			sOutfitsData.iComponentDrawableID[PED_COMP_JBIB] 	 = GET_STORED_MP_PLAYER_COMPONENT_CHARACTER_MENU_JBIB_DRAWABLE(iLastActiveCharacterSlot)
			sOutfitsData.iComponentTextureID[PED_COMP_JBIB]  	 = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_TEXTVAR_JBIB,     iLastActiveCharacterSlot)			
						
			sOutfitsData.iPropDrawableID[0] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_0,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[0]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_0,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[1] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_1,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[1]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_1,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[2] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_2,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[2]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_2,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[3] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_3,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[3]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_3,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[4] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_4,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[4]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_4,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[5] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_5,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[5]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_5,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[6] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_6,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[6]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_6,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[7] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_7,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[7]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_7,  iLastActiveCharacterSlot)
			sOutfitsData.iPropDrawableID[8] = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPINDX_8,  iLastActiveCharacterSlot)
			sOutfitsData.iPropTextureID[8]  = GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CLTH_PROPTEXT_8,  iLastActiveCharacterSlot)
			//Set 0'd props to -1 if anchor doesn't contain any props by default
			If sOutfitsData.iPropDrawableID[3] = 0
				sOutfitsData.iPropDrawableID[3] = -1
			endif
			If sOutfitsData.iPropDrawableID[4] = 0
				sOutfitsData.iPropDrawableID[4] = -1
			endif
			If sOutfitsData.iPropDrawableID[5] = 0
				sOutfitsData.iPropDrawableID[5] = -1
			endif
			If sOutfitsData.iPropDrawableID[7] = 0
				sOutfitsData.iPropDrawableID[7] = -1
			endif
			If sOutfitsData.iPropDrawableID[8] = 0
				sOutfitsData.iPropDrawableID[8] = -1
			endif				
				
			sOutfitsData.iHeadMum 		= GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_M1_HEAD_TYPE, iLastActiveCharacterSlot)
			sOutfitsData.iHeadMumVar 	= GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_M1_HEAD_VAR, iLastActiveCharacterSlot)
			sOutfitsData.iHeadDad 		= GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_D1_HEAD_TYPE, iLastActiveCharacterSlot)				
			sOutfitsData.iHeadDadVar 	= GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_D1_HEAD_VAR, iLastActiveCharacterSlot)
			sOutfitsData.fDominance 	= GET_MP_FLOAT_MENUPED_CHARSLOT(MP_STAT_HEADBLEND_GEOM_BLEND, iLastActiveCharacterSlot)
			sOutfitsData.fSkinToneBlend = GET_MP_FLOAT_MENUPED_CHARSLOT(MP_STAT_HEADBLEND_TEX_BLEND, iLastActiveCharacterSlot)
			
			INT iStartIndex	
			INT iTotalNumHeads			
			
			IF sOutfitsData.iHeadMumVar = 1
				iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_FEMALE)
			Else
				iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE)
			endif				
			sOutfitsData.iHeadMum += iStartIndex	
				
			IF sOutfitsData.iHeadDadVar = 1
				iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_UNIQUE_MALE)
			Else
				iStartIndex = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE)
			endif				
			sOutfitsData.iHeadDad += iStartIndex				
			
			iTotalNumHeads = GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_FEMALE) + GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_MALE) + GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_FEMALE) + GET_PED_HEAD_BLEND_NUM_HEADS(HBHT_UNIQUE_MALE)				
			
			// We need to cap our heads if we excee the number of valid heads
			IF sOutfitsData.iHeadMum >= iTotalNumHeads				
				sOutfitsData.iHeadMum = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_FEMALE)					
			ENDIF
			
			IF sOutfitsData.iHeadDad >= iTotalNumHeads
				sOutfitsData.iHeadDad = GET_PED_HEAD_BLEND_FIRST_INDEX(HBHT_MALE)					
			ENDIF
			//hair
			PED_COMP_NAME_ENUM eWhichHair
			eWhichHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT,iLastActiveCharacterSlot))
			IF (eWhichHair != DUMMY_PED_COMP)
				PED_COMP_NAME_ENUM eGRHairItem
				eGRHairItem = DUMMY_PED_COMP
				IF (ePedModel = MP_M_FREEMODE_01)
					eGRHairItem = GET_MALE_HAIR(eWhichHair)
				ELIF (ePedModel = MP_F_FREEMODE_01)
					eGRHairItem = GET_FEMALE_HAIR(eWhichHair)
				ENDIF
				
				IF (eGRHairItem != DUMMY_PED_COMP)
				AND (eWhichHair != eGRHairItem)
					CPRINTLN(DEBUG_MISSION,"[GET_DIRECTOR_CHAR_OUTFIT_DATA] gr_hair: replacing hair enum ", eWhichHair, " with gunrunning hair enum ", eGRHairItem)
					eWhichHair = eGRHairItem
				ENDIF
			ENDIF
			
			sOutfitsData.WhichHair = eWhichHair
			sOutfitsData.iColour1 = GET_MP_INT_STAT_MENUPED_CHARSLOT(MP_STAT_HAIR_TINT, iLastActiveCharacterSlot)
			sOutfitsData.iColour2 = GET_MP_INT_STAT_MENUPED_CHARSLOT(MP_STAT_SEC_HAIR_TINT, iLastActiveCharacterSlot)						
			
			//micro morphs						
			MP_FLOAT_STATS microMorphStat	
			int iMorph
			REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH iMorph
				sOutfitsData.microMorph[iMorph] = INT_TO_ENUM(MICRO_MORPH_TYPE, iMorph)		
				microMorphStat = GET_CHARACTER_FEATURE_BLEND_STAT(sOutfitsData.microMorph[iMorph])
				sOutfitsData.fMorphBlend[iMorph] = GET_MP_FLOAT_MENUPED_CHARSLOT(microMorphStat, iLastActiveCharacterSlot)		
			ENDREPEAT		
			
			// Overlay			
			STATS_PACKED overlayStat
			MP_FLOAT_STATS overlayStatBlend
			MP_INT_STATS overlayColourVal
			MP_INT_STATS overlaySecColourVal						
			INT iColourSaved			
			INT iOverlay
			REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlay								
				sOutfitsData.overlaySlot[iOverlay] 	= GET_CHARACTER_CREATOR_OVERLAY_FOR_INDEX(iOverlay)
				overlayStat 						= GET_CHARACTER_OVERLAY_STAT(sOutfitsData.overlaySlot[iOverlay])
				overlayStatBlend 					= GET_CHARACTER_OVERLAY_BLEND_STAT(sOutfitsData.overlaySlot[iOverlay])
				
				IF overlayStat != INT_TO_ENUM(STATS_PACKED, -1)
				AND overlayStatBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1)							
					sOutfitsData.iOverlayValue[iOverlay] 	= GET_PACKED_CHARACTER_MENU_STAT_INT(overlayStat, iLastActiveCharacterSlot)
					sOutfitsData.fOverlayBlend[iOverlay] 	= GET_MP_FLOAT_MENUPED_CHARSLOT(overlayStatBlend, iLastActiveCharacterSlot)					
					overlayColourVal 						= GET_CHARACTER_OVERLAY_COLOUR_STAT(sOutfitsData.overlaySlot[iOverlay])
					overlaySecColourVal 					= GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(sOutfitsData.overlaySlot[iOverlay])					
					IF overlayColourVal != INT_TO_ENUM(MP_INT_STATS, -1)		
						iColourSaved 							= GET_MP_INT_STAT_MENUPED_CHARSLOT(overlayColourVal, iLastActiveCharacterSlot)
						sOutfitsData.iColourSaved2[iOverlay] 	= GET_MP_INT_STAT_MENUPED_CHARSLOT(overlaySecColourVal, iLastActiveCharacterSlot)										
						UNPACK_CHARACTER_OVERLAY_TINT_VALUES(iColourSaved, sOutfitsData.iColour[iOverlay], sOutfitsData.rtType[iOverlay])
					ENDIF
				ENDIF
			ENDREPEAT	

			//eyes
			sOutfitsData.fEyes = GET_MP_FLOAT_MENUPED_CHARSLOT(MP_STAT_FEATURE_20, iLastActiveCharacterSlot)		

			//Tattoos
			TATTOO_DATA_STRUCT sTattooData
			TATTOO_FACTION_ENUM eFaction
			
			if ePedModel = MP_M_FREEMODE_01
				eFaction = TATTOO_MP_FM
			else
				eFaction = TATTOO_MP_FM_F
			endif
			INT iMaxTattoos			
			INT iTat
			
			iMaxTattoos = 0
			
			REPEAT MAX_NUMBER_OF_TATTOOS iTat 
				if iMaxTattoos < MAX_TATTOOS_IN_SHOP
					IF IS_MP_TATTOO_CURRENT_PEDMENU_CHARSLOT(INT_TO_ENUM(TATTOO_NAME_ENUM, iTat), iLastActiveCharacterSlot)						
						IF GET_TATTOO_DATA_USING_MODEL(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, iTat), eFaction, ePedModel)
							CDEBUG3LN(debug_director,"Normal tat iCollection: ",sTattooData.iCollection," Preset: ",sTattooData.iPreset	," iTat: ",iTat)
							sOutfitsData.eTatEnum[iMaxTattoos] 			= 	INT_TO_ENUM(TATTOO_NAME_ENUM, iTat)
							sOutfitsData.iTatCollection[iMaxTattoos] 	= 	sTattooData.iCollection
							sOutfitsData.iTatPreset[iMaxTattoos] 		= 	sTattooData.iPreset		
							iMaxTattoos++
						ENDIF						
					endif
				endif
			ENDREPEAT
			
			INT iDLCIndex
			INT iDLCCount 
			sTattooShopItemValues sDLCTattooData
			TATTOO_NAME_ENUM eDLCTattoo
			
			iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
			
			REPEAT iDLCCount iDLCIndex
				if iMaxTattoos < MAX_TATTOOS_IN_SHOP
					IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
						IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
							eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
							IF IS_MP_TATTOO_CURRENT_PEDMENU_CHARSLOT(eDLCTattoo, iLastActiveCharacterSlot)
								CDEBUG3LN(debug_director,"DLC tat Label: ", sDLCTattooData.Label, " iCollection: ",sDLCTattooData.Collection," Preset: ",sDLCTattooData.Preset	," iMaxTattoos: ",iMaxTattoos, "Locked: FALSE Charslot: TRUE")
								sOutfitsData.eTatEnum[iMaxTattoos] 			= 	eDLCTattoo
								sOutfitsData.iTatCollection[iMaxTattoos] 	= 	sDLCTattooData.Collection
								sOutfitsData.iTatPreset[iMaxTattoos] 		= 	sDLCTattooData.Preset	
								iMaxTattoos++
							ELSE
								CDEBUG3LN(debug_director,"DLC tat Label: ", sDLCTattooData.Label, " iCollection: ",sDLCTattooData.Collection," Preset: ",sDLCTattooData.Preset	," iMaxTattoos: ",iMaxTattoos, "Locked: FALSE Charslot: FALSE")
							ENDIF
						ELSE
							CDEBUG3LN(debug_director,"DLC tat Label: ", sDLCTattooData.Label, " iCollection: ",sDLCTattooData.Collection," Preset: ",sDLCTattooData.Preset	," iMaxTattoos: ",iMaxTattoos, "Locked: TRUE")
						ENDIF
					ENDIF
				ENDIF	
			ENDREPEAT
			
			//crew info			
			sOutfitsData.eShirtType = INT_TO_ENUM(CHAR_CREATOR_CREW_TSHIRT_OPTION_ENUM, GET_PACKED_CHARACTER_MENU_STAT_INT(PACKED_MP_CREW_T_TYPE, iLastActiveCharacterSlot))
			CPRINTLN(DEBUG_MISSION,"GET_DIRECTOR_CHAR_OUTFIT - sOutfitsData.eShirtType [", enum_to_INT(sOutfitsData.eShirtType),"]")
		BREAK		
	ENDSWITCH
	
	bool bfoundOutfit = false
	REPEAT NUM_PED_COMPONENTS i
		if sOutfitsData.iComponentDrawableID[i] != -1
			CPRINTLN(DEBUG_MISSION,"GET_DIRECTOR_CHAR_OUTFIT - found outfit [", enum_to_INT(eCharOutfit),"] compID: ", sOutfitsData.iComponentDrawableID[i]," textureID:",sOutfitsData.iComponentTextureID[i])
			bfoundOutfit = true
		endif
	ENDREPEAT		
	REPEAT COUNT_OF(sOutfitsData.iPropDrawableID) i
		if sOutfitsData.iPropDrawableID[i] != -1
		and sOutfitsData.iPropDrawableID[i] != 255
			CPRINTLN(DEBUG_MISSION,"GET_DIRECTOR_CHAR_OUTFIT - found outfit [", enum_to_INT(eCharOutfit),"] iPropDrawableID: ", sOutfitsData.iPropDrawableID[i]," textureID:",sOutfitsData.iPropTextureID[i])
			bfoundOutfit = true
		endif
	ENDREPEAT
		
	IF bfoundOutfit		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("GET_DIRECTOR_CHAR_OUTFIT - Enum missing tell Craig V. - ", enum_to_INT(eCharOutfit))
		SCRIPT_ASSERT("GET_DIRECTOR_CHAR_OUTFIT - Enum missing tell Craig V.")
	#ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SET_CHAR_OUTFIT(ped_index pedID,DIRECTOR_CHAR_OUTFITS eDirOutfit,DIR_OUTFIT_DATA &sOutfitsData,bool bGetNewData = TRUE)
	
	BOOL bfoundOutfit = true
	
	//Only using passed in "sOutfitsData" when applying cached Online character data
	If bGetNewData		
		if not GET_DIRECTOR_CHAR_OUTFIT_DATA(sOutfitsData,eDirOutfit,GET_ENTITY_MODEL(pedID))		
			bfoundOutfit = false
		endif	
	endif
	
	if bfoundOutfit
		CLEAR_ALL_PED_PROPS(pedID)	
		INT i			
		REPEAT NUM_PED_COMPONENTS i
			IF sOutfitsData.iComponentDrawableID[i] != -1			
				SET_PED_COMPONENT_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, i), sOutfitsData.iComponentDrawableID[i], sOutfitsData.iComponentTextureID[i])
			ENDIF
		ENDREPEAT			
		REPEAT COUNT_OF(sOutfitsData.iPropDrawableID) i
			IF sOutfitsData.iPropDrawableID[i] != -1
			AND sOutfitsData.iPropDrawableID[i] != 255
				SET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sOutfitsData.iPropDrawableID[i], sOutfitsData.iPropTextureID[i], TRUE)
			ENDIF
		ENDREPEAT
		
		if eDirOutfit = DCO_MULTIPLAYER_1
		or eDirOutfit = DCO_MULTIPLAYER_2
			
			IF (sOutfitsData.iComponentDrawableID[COMP_TYPE_BERD] > 0)	
			AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(pedID), ENUM_TO_INT(PED_COMP_BERD), sOutfitsData.iComponentDrawableID[COMP_TYPE_BERD], sOutfitsData.iComponentTextureID[COMP_TYPE_BERD]), DLC_RESTRICTION_TAG_SHRINK_HEAD, ENUM_TO_INT(SHOP_PED_COMPONENT))
				CPRINTLN(DEBUG_PED_COMP, "[SET_CHAR_OUTFIT] Shrinking head as restriction tag has been hit ")
				
				scrPedHeadBlendData playerBlendData
				GET_PED_HEAD_BLEND_DATA(pedID, playerBlendData)
				
				IF (GET_ENTITY_MODEL(pedID) = MP_M_FREEMODE_01)
					CPRINTLN(DEBUG_PED_COMP, "[SET_CHAR_OUTFIT] Force male head to small - Berd item tag is set: DLC_RESTRICTION_TAG_SHRINK_HEAD")
					SET_PED_HEAD_BLEND_DATA(pedID, 0,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0,  playerBlendData.m_texBlend, 0.0)
				ELIF (GET_ENTITY_MODEL(pedID) = MP_F_FREEMODE_01)
					CPRINTLN(DEBUG_PED_COMP, "[SET_CHAR_OUTFIT] Force female head to small - Berd item tag is set: DLC_RESTRICTION_TAG_SHRINK_HEAD")
					SET_PED_HEAD_BLEND_DATA(pedID, 21,0,0, playerBlendData.m_tex0, playerBlendData.m_tex1, playerBlendData.m_tex2, 0.0, playerBlendData.m_texBlend, 0.0)
				ENDIF				
			
				MICRO_MORPH_TYPE microMorph
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH microMorph
					SET_PED_MICRO_MORPH(pedID, microMorph, 0.0)
				ENDREPEAT
			ELSE
				//blend
				SET_PED_HEAD_BLEND_DATA(pedId, sOutfitsData.iHeadMum, sOutfitsData.iHeadDad, 0, sOutfitsData.iHeadMum, sOutfitsData.iHeadDad, 0, sOutfitsData.fDominance, sOutfitsData.fSkinToneBlend, 0, TRUE)
				
				//morphs
				int iMorph
				REPEAT ciCHARACTER_CREATOR_MAX_MICRO_MORPH iMorph
					SET_PED_MICRO_MORPH(pedId, sOutfitsData.microMorph[iMorph], sOutfitsData.fMorphBlend[iMorph])
				ENDREPEAT
			ENDIF
			
			//hair
			CPRINTLN(DEBUG_PED_COMP, "[SET_CHAR_OUTFIT] starting hair stuff for online char ", sOutfitsData.WhichHair)
			g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(pedId), COMP_TYPE_HAIR, sOutfitsData.WhichHair)
			SET_PED_COMPONENT_VARIATION(pedId, PED_COMP_HAIR, g_sTempCompData[1].iDrawable, g_sTempCompData[1].iTexture)						
			SET_PED_HAIR_TINT(pedId, sOutfitsData.iColour1, sOutfitsData.iColour2)
			
			SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_BERD, sOutfitsData.iComponentDrawableID[PED_COMP_BERD], sOutfitsData.iComponentTextureID[PED_COMP_BERD])
		
			//overlay
			int iOverlay
			REPEAT ciCHARACTER_CREATOR_MAX_OVERLAYS iOverlay		
				if sOutfitsData.iOverlayValue[iOverlay] != -1
				and sOutfitsData.fOverlayBlend[iOverlay] != -1
					SET_PED_HEAD_OVERLAY(pedId, sOutfitsData.overlaySlot[iOverlay], sOutfitsData.iOverlayValue[iOverlay], sOutfitsData.fOverlayBlend[iOverlay])							
					if sOutfitsData.iColourSaved2[iOverlay] != -1
						SET_PED_HEAD_OVERLAY_TINT(pedId, sOutfitsData.overlaySlot[iOverlay], sOutfitsData.rtType[iOverlay], sOutfitsData.iColour[iOverlay], sOutfitsData.iColourSaved2[iOverlay])						
					ENDIF
				ENDIF
			ENDREPEAT
			
			//eyes	
			SET_HEAD_BLEND_EYE_COLOR(pedId, ROUND(sOutfitsData.fEyes))
			
			// tattoos
			CPRINTLN(DEBUG_PED_COMP, "[SET_CHAR_OUTFIT] starting tattoo stuff for online char ")
			TATTOO_DATA_STRUCT sTattooData
			TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(pedId)
			BOOL bBlockChestTattoos = SHOULD_CHEST_TATTOOS_BE_BLOCKED_FOR_OUTFIT(pedId, TRUE)
			BOOL bBlockBackTattoos 	= SHOULD_BACK_TATTOOS_BE_BLOCKED_FOR_OUTFIT_DM(pedId)
			
			INT iTat
			REPEAT MAX_TATTOOS_IN_SHOP iTat
				if sOutfitsData.iTatCollection[iTat] != -1
				and sOutfitsData.iTatPreset[iTat] != -1
				and sOutfitsData.eTatEnum[iTat] != INVALID_TATTOO
					if GET_TATTOO_DATA(sTattooData, sOutfitsData.eTatEnum[iTat], eFaction, pedId)
						IF IS_TATTOO_SAFE_TO_APPLY_DM(sTattooData.sLabel, sTattooData.iCollection, sTattooData.iUpgradeGroup, bBlockChestTattoos, true, false, bBlockBackTattoos)
							ADD_PED_DECORATION_FROM_HASHES(pedId, sOutfitsData.iTatCollection[iTat], sOutfitsData.iTatPreset[iTat])		
							ADD_SECONDARY_PED_DECORATION_FROM_HASHES(pedId, sOutfitsData.iTatCollection[iTat], sOutfitsData.iTatPreset[iTat])
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			//crew
			GIVE_PLAYER_CREW_TSHIRT(pedId,sOutfitsData.eShirtType,FALSE)
			
		endif
		
		RETURN TRUE
	else
		RETURN FALSE
		CPRINTLN(DEBUG_MISSION,"[SET_CHAR_OUTFIT] GET_DIRECTOR_CHAR_OUTFIT_DATA Return False Outfit enum: ",enum_to_INT(eDirOutfit))
	endif
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FAT_PED(MODEL_NAMES model)
	SWITCH model
		CASE S_M_Y_FIREMAN_01 
		CASE S_M_M_SNOWCOP_01 
		CASE S_M_Y_SWAT_01 
		CASE A_M_M_HILLBILLY_01 
		CASE G_M_Y_MEXGOON_02 
		CASE G_M_Y_MEXGOON_03 
		CASE G_M_Y_MEXGOON_01 
		CASE S_M_Y_CONSTRUCT_01 
		CASE S_M_Y_CONSTRUCT_02 
		CASE S_M_M_DOCKWORK_01 
		CASE S_M_Y_FACTORY_01 
		CASE S_M_Y_GARBAGE 
		CASE S_M_M_GARDENER_01 
		CASE G_M_Y_SALVAGOON_02 
		CASE S_M_M_MIGRANT_01 
		CASE S_M_Y_CHEF_01 
		CASE S_F_M_MAID_01 
		CASE S_M_Y_VALET_01 
		CASE S_M_M_MARIACHI_01 
		CASE A_M_Y_MUSCLBEAC_02 
		CASE IG_TRACYDISANTO
		CASE s_m_y_blackops_02
		CASE s_m_y_marine_03
		CASE a_m_y_motox_01
		CASE IG_ORLEANS
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SUPER_FAT_PED(MODEL_NAMES model)
	SWITCH model
//		CASE A_F_M_BODYBUILD_01
		CASE a_f_m_eastsa_02 
		CASE A_F_Y_SOUCENT_01
		CASE A_F_Y_TOURIST_02
		CASE A_F_M_SALTON_01
		CASE A_F_M_TRAMP_01 
		CASE G_M_M_MEXBOSS_01
		CASE A_M_Y_MUSCLBEAC_01 
		CASE IG_MAUDE
		CASE A_M_M_Farmer_01
		CASE G_M_M_MEXBOSS_02
		CASE A_F_M_FatBla_01
		CASE A_F_M_FatWhite_01
		CASE A_M_M_FatLatin_01
		CASE A_M_M_Genfat_01
		CASE A_M_M_Genfat_02
		CASE A_F_M_FATCULT_01
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC DISABLE_UNSAFE_DIRECTOR_MODE_CHEATS(MODEL_NAMES newModelToLoad)

	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_FAST_SWIM, TRUE)
	IF newModelToLoad = S_M_M_MOVALIEN_01
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, TRUE)
	ENDIF
	DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_PARACHUTE, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_INVINCIBILITY, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_SLOWMO, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_AIM_SLOWMO, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_SKYFALL, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_DRUNK, TRUE)
	
	//DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_SLIDEY_CARS, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_BANG_BANG, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_0_GRAVITY, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_FLAMING_BULLETS, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_EXPLOSIVE_MELEE, TRUE)	
	//DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
ENDPROC

PROC ENABLE_DIRECTOR_MODE_CHEATS()
	DISABLE_CHEAT(CHEAT_TYPE_FAST_RUN, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_FAST_SWIM, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_WEAPONS, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_ADVANCE_WEATHER, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_PARACHUTE, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_INVINCIBILITY, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_SLOWMO, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_SKYFALL, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_AIM_SLOWMO, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_DRUNK, FALSE)
	
	//DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_SLIDEY_CARS, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_BANG_BANG, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_0_GRAVITY, FALSE)
	//DISABLE_CHEAT(CHEAT_TYPE_FLAMING_BULLETS, TRUE)
	//DISABLE_CHEAT(CHEAT_TYPE_EXPLOSIVE_MELEE, TRUE)	
	//DISABLE_CHEAT(CHEAT_TYPE_SPAWN_VEHICLE, TRUE)
ENDPROC

PROC DISABLE_DIRECTOR_MODE_SETTINGS_CHEATS(BOOL bDisable = TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_SUPER_JUMP, bDisable)
	DISABLE_CHEAT(CHEAT_TYPE_SLIDEY_CARS, bDisable)
	DISABLE_CHEAT(CHEAT_TYPE_BANG_BANG, bDisable)
	DISABLE_CHEAT(CHEAT_TYPE_FLAMING_BULLETS, bDisable)
	DISABLE_CHEAT(CHEAT_TYPE_EXPLOSIVE_MELEE, bDisable)	
	DISABLE_CHEAT(CHEAT_TYPE_0_GRAVITY, bDisable)
ENDPROC

PROC DEACTIVATE_DIRECTOR_CHEATS()	
	INT i
	FOR i = 0 TO CHEAT_TYPE_MAX
		IF IS_CHEAT_ACTIVE(INT_TO_ENUM(CHEATS_BITSET_ENUM,i))
			TRIGGER_CHEAT(INT_TO_ENUM(CHEATS_BITSET_ENUM,i))
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL IS_DIRECTOR_PED_MALE(MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT)

	IF model = DUMMY_MODEL_FOR_SCRIPT
		model = GET_ENTITY_MODEL(PLAYER_PED_ID())
	ENDIF
	
	cprintln(debug_director,"IS_DIRECTOR_PED_MALE() model = ",GET_MODEL_NAME_FOR_DEBUG(model)," drawable head = ",GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD))
	
	//Had to change this as a model check alone wasn't sufficient here.
	IF (model = HC_DRIVER AND GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 2) //Talina
	OR (model = HC_HACKER AND GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_HEAD) = 1) //PAIGE
		RETURN FALSE
	ENDIF
	
	SWITCH model
		CASE 	A_F_Y_RUNNER_01
		CASE	A_F_M_BevHills_02
		CASE	A_F_M_Business_02  
		CASE	A_F_Y_Hipster_04   
		CASE	A_F_M_BevHills_01
		CASE	S_F_Y_AirHostess_01
		CASE	A_F_Y_Hipster_02
		CASE	S_F_Y_Shop_MID
		CASE	U_F_Y_SpyActress
		CASE	S_F_M_FemBarber
		CASE	A_F_Y_Business_03
		CASE	A_F_Y_Business_01 
		CASE	S_F_M_Shop_HIGH
		CASE	A_F_Y_Business_02 
		CASE	S_F_Y_Hooker_01
		CASE	A_F_Y_Business_04 
		CASE	A_F_Y_Hipster_01  
		CASE	A_F_Y_Hipster_03    
		CASE	A_F_Y_Vinewood_01
		CASE	A_F_Y_Vinewood_04
		CASE	A_F_Y_BevHills_01 
		CASE	A_F_Y_BevHills_03 
		CASE	A_F_Y_BevHills_02 
		CASE	A_F_Y_BevHills_04 
		CASE	A_F_Y_Genhot_01
		CASE	A_F_Y_SouCent_01 
		CASE	A_F_M_FatWhite_01 
		CASE	A_F_M_EastSA_02
		CASE	A_F_Y_EastSA_01
		CASE	A_F_Y_EastSA_02
		CASE	A_F_Y_Indian_01
		CASE	A_F_M_FatBla_01
		CASE	A_F_Y_SouCent_02
		CASE	A_F_M_SkidRow_01
		CASE	A_F_M_TrampBeac_01
		CASE	A_F_M_Tramp_01
		CASE	A_F_M_Salton_01
		CASE	A_F_Y_Tourist_02
		CASE	A_F_Y_Beach_01
		CASE	A_F_M_Beach_01
		CASE	A_F_M_BodyBuild_01
		CASE	A_F_Y_Yoga_01
		CASE	A_F_Y_Skater_01
		CASE	A_F_Y_Tennis_01
		CASE	A_F_Y_Golfer_01
		CASE	G_F_Y_Vagos_01
		CASE	A_F_Y_EPSILON_01 
		CASE	A_F_Y_EastSA_03
		CASE	G_F_Y_Lost_01
		CASE	A_F_M_FATCULT_01
		CASE	U_F_M_Corpse_01 
		CASE	S_F_Y_Migrant_01
		CASE	S_F_Y_Factory_01
		CASE	S_F_M_Maid_01
		CASE	S_F_Y_Cop_01
		CASE	S_F_Y_RANGER_01
		CASE	S_F_Y_BAYWATCH_01
		CASE	S_F_Y_SHERIFF_01
		CASE	IG_AMANDATOWNLEY 
		CASE	IG_MAUDE 
		CASE	IG_MRS_THORNHILL 
		CASE	IG_PATRICIA 
		CASE	IG_TANISHA 
		CASE	IG_TRACYDISANTO 
		CASE	U_F_Y_COMJane 
		CASE	U_F_O_MOVIESTAR 
		CASE 	IG_ORLEANS
		CASE 	MP_F_FREEMODE_01
			RETURN FALSE
		BREAK
 	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_MODEL_AN_ANIMAL(model_names animalModel)
	
	switch animalModel
		case A_C_BOAR FALLTHRU
		CASE A_C_CAT_01 FALLTHRU
		CASE A_C_COW FALLTHRU
		CASE A_C_COYOTE FALLTHRU
		CASE A_C_DEER FALLTHRU
		CASE A_C_HUSKY FALLTHRU
		CASE A_C_MTLION FALLTHRU
		CASE A_C_PIG FALLTHRU
		CASE A_C_POODLE FALLTHRU
		CASE A_C_PUG FALLTHRU
		CASE A_C_RABBIT_01 FALLTHRU
		CASE A_C_RETRIEVER FALLTHRU
		CASE A_C_ROTTWEILER FALLTHRU
		CASE A_C_SHEPHERD FALLTHRU
		CASE A_C_WESTY FALLTHRU
		CASE A_C_CHICKENHAWK FALLTHRU
		CASE A_C_CORMORANT FALLTHRU
		CASE A_C_CROW FALLTHRU
		CASE A_C_HEN FALLTHRU
		CASE A_C_PIGEON FALLTHRU
		CASE A_C_SEAGULL FALLTHRU
		CASE IG_ORLEANS
			RETURN TRUE
		BREAK
	endswitch
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIRECTOR_PED_AN_ONLINE_CHARA( MODEL_NAMES mnCurrentPedModel )
	IF mnCurrentPedModel = MP_M_FREEMODE_01
	OR mnCurrentPedModel = MP_F_FREEMODE_01
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_NAME_FROM_STORY_UNLOCK_CHARA( DirectorUnlockStory duStory )

	SWITCH duStory
		CASE DU_STORY_MICHAEL		RETURN 	PLAYER_ONE 			
		CASE DU_STORY_FRANKLIN		RETURN  PLAYER_TWO 			
		CASE DU_STORY_TREVOR		RETURN  PLAYER_ZERO 		
		CASE DU_STORY_AMANDA_T		RETURN  IG_AMANDATOWNLEY 	
		CASE DU_STORY_BEVERLY		RETURN  IG_BEVERLY 			
		CASE DU_STORY_BRAD			RETURN  IG_BRAD 			
		CASE DU_STORY_CHRIS_F		RETURN  IG_CHRISFORMAGE 	
		CASE DU_STORY_DAVE_N		RETURN  IG_DAVENORTON 		
		CASE DU_STORY_DEVIN			RETURN  IG_DEVIN 			
		CASE DU_STORY_DR_F			RETURN  IG_DRFRIEDLANDER 	
		CASE DU_STORY_FABIEN		RETURN  IG_FABIEN 			
		CASE DU_STORY_FLOYD			RETURN  IG_FLOYD 			
		CASE DU_STORY_JIMMY			RETURN  IG_JIMMYDISANTO 	
		CASE DU_STORY_LAMAR			RETURN  IG_LAMARDAVIS 		
		CASE DU_STORY_LAZLOW		RETURN  IG_LAZLOW 			
		CASE DU_STORY_LESTER		RETURN  IG_LESTERCREST 		
		CASE DU_STORY_MAUDE			RETURN  IG_MAUDE 			
		CASE DU_STORY_MRS_T			RETURN  IG_MRS_THORNHILL 	
		CASE DU_STORY_RON			RETURN  IG_NERVOUSRON 		   
		CASE DU_STORY_PATRICIA		RETURN  IG_PATRICIA 		
		CASE DU_STORY_SIMEON		RETURN  IG_SIEMONYETARIAN 	
		CASE DU_STORY_SOLOMON		RETURN  IG_SOLOMON 			
		CASE DU_STORY_STEVE_HAINS	RETURN  IG_STEVEHAINS 		     
		CASE DU_STORY_STRETCH		RETURN  IG_STRETCH 			
		CASE DU_STORY_TANISHA		RETURN  IG_TANISHA 			
		CASE DU_STORY_TAO_C			RETURN  IG_TAOCHENG 		
		CASE DU_STORY_TRACEY		RETURN  IG_TRACYDISANTO 	
		CASE DU_STORY_WADE			RETURN  IG_WADE 			
		DEFAULT			
			ASSERTLN( "Invalid DirectorUnlockStory enum input to GET_MODEL_NAME_FROM_STORY_UNLOCK_CHARA- Add model to switch. RBJ" )	
			RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_MODEL_NAME_FROM_SPECIAL_UNLOCK_CHARA( DirectorUnlockSpecial duSpecial )
	
	SWITCH duSpecial
		CASE DU_SPECIAL_ANDYMOON		RETURN U_M_Y_Hippie_01
		CASE DU_SPECIAL_BAYGOR			RETURN U_M_Y_BAYGOR 
		CASE DU_SPECIAL_BILLBINDER		RETURN U_M_O_FinGuru_01 
		CASE DU_SPECIAL_CLINTON			RETURN U_M_Y_MILITARYBUM 
		CASE DU_SPECIAL_GRIFF			RETURN U_M_M_GRIFF_01 
		CASE DU_SPECIAL_JANE			RETURN U_F_Y_COMJane 
		CASE DU_SPECIAL_JEROME			RETURN S_M_M_StrPreach_01 
		CASE DU_SPECIAL_JESSE			RETURN U_M_M_Jesus_01 
		CASE DU_SPECIAL_MANI			RETURN U_M_Y_MANI 
		CASE DU_SPECIAL_MIME			RETURN S_M_Y_MIME 
		CASE DU_SPECIAL_PAMELADRAKE		RETURN U_F_O_MOVIESTAR 
		CASE DU_SPECIAL_SUPERHERO		RETURN U_M_Y_IMPORAGE 
		CASE DU_SPECIAL_ZOMBIE			RETURN U_M_Y_ZOMBIE_01 
		DEFAULT			
			ASSERTLN( "Invalid DirectorUnlockSpecial enum input to GET_MODEL_NAME_FROM_SPECIAL_UNLOCK_CHARA- Add model to switch. RBJ" )	
			RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL IS_DIRECTOR_PED_A_STORY_CHARACTER( MODEL_NAMES mnCurrPedModel )
	INT iIndex
	REPEAT MAX_DU_STORY iIndex
		IF mnCurrPedModel = GET_MODEL_NAME_FROM_STORY_UNLOCK_CHARA( INT_TO_ENUM( DirectorUnlockStory, iIndex ) )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DIRECTOR_PED_A_SPECIAL_CHARACTER( MODEL_NAMES mnCurrPedModel )
	INT iIndex
	REPEAT MAX_DU_SPECIAL iIndex
		IF mnCurrPedModel = GET_MODEL_NAME_FROM_SPECIAL_UNLOCK_CHARA( INT_TO_ENUM( DirectorUnlockSpecial, iIndex ) )
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC REVISE_WEAPON_ANIMS_FOR_PED_MODEL()

	//some default weapon anims don't work with fat and super fat peds.

	MODEL_NAMES model = GET_ENTITY_MODEL(player_ped_id())
	BOOL isModelMale = IS_DIRECTOR_PED_MALE(model)
		
	IF model = hc_gunman
		IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_SPECIAL2) = 1 //ballisic armor
			cprintln(DEBUG_DIRECTOR,"Body armor set super fat")
			SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("BALLISTIC"))
		ELSE
			IF GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 7 // body armor
			OR GET_PED_DRAWABLE_VARIATION(player_ped_id(),PED_COMP_TORSO) = 2 //fireman
				cprintln(DEBUG_DIRECTOR,"Body armor set fat")
				SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("BALLISTIC"))
			ENDIF
		ENDIF
	ELIF model = IG_JIMMYDISANTO
		cprintln(DEBUG_DIRECTOR,"Jimmy animation set to female")
		SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("FEMALE"))
	ELSE	
		IF isModelMale
		//male
			IF IS_FAT_PED(model) OR IS_SUPER_FAT_PED(model)
				SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("BALLISTIC"))
				cprintln(DEBUG_DIRECTOR,"superfat/fat male set ballistic")
			ENDIF
		ELSE
		//female
			IF IS_FAT_PED(model)
				SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("FAT"))
				cprintln(DEBUG_DIRECTOR,"fat female set fat")
			ELIF IS_SUPER_FAT_PED(model)
				SET_WEAPON_ANIMATION_OVERRIDE(player_ped_id(), HASH("SUPERFAT"))
				cprintln(DEBUG_DIRECTOR,"superfat female set superfat")
			ENDIF
		ENDIF
	ENDIF		
ENDPROC


/// PURPOSE:
///    Handles disaplying a warning screen to the player telling them that Director Mode could not
///    be launched for a given blocking reason. Call once and it will hand the calling script
///    thread until the screen has been cleared.
PROC DO_DIRECTOR_MODE_WARNING_SCREEN_WAIT(FEATURE_BLOCK_REASON eBlockReason)
	CPRINTLN(DEBUG_DIRECTOR, "Starting to display blocked reason warning screen...")
    SET_GAME_PAUSED(TRUE)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
    FLOAT fTime = 0
    WHILE NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
    OR fTime < 1.0  //Frame time is in seconds
        STRING strFBLabel = GET_FEATURE_BLOCK_MESSAGE_TEXT(eBlockReason,DEBUG_DIRECTOR)
        SET_WARNING_MESSAGE("FBR_GENERIC", FE_WARNING_CONTINUE, DEFAULT, DEFAULT, DEFAULT, "FBR_DIR_MODE", strFBLabel)
        
		WAIT(0)
        fTime += GET_FRAME_TIME()
    ENDWHILE
    SET_GAME_PAUSED(FALSE)
	CPRINTLN(DEBUG_DIRECTOR, "...Warning screen ended.")
ENDPROC


/// PURPOSE:
///    Handles disaplying a warning screen to the player asking if they want to enter Director Mode. 
///    Launches Director Mode if the player accepts. Call once and it will hand the calling script
///    thread until a decision is made.
PROC DO_DIRECTOR_MODE_ACCEPT_SCREEN_WAIT()
	CPRINTLN(DEBUG_DIRECTOR, "Starting to display enter Director Mode accept screen...")
	SET_GAME_PAUSED(TRUE)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	
	FLOAT fTime = 0
	WHILE (NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	OR fTime < 1.0
		SET_WARNING_MESSAGE_WITH_HEADER("VEUI_HDR_ALERT", "VE_DIR_MODE_SURE", FE_WARNING_OKCANCEL)	
		WAIT(0)
		fTime += GET_FRAME_TIME()
	ENDWHILE
	
	SET_GAME_PAUSED(FALSE)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		CPRINTLN(DEBUG_DIRECTOR, "...The player accepted...")
		DO_SCREEN_FADE_OUT(0)  
		g_bLaunchDirectorMode = TRUE
	ENDIF
	
	CPRINTLN(DEBUG_DIRECTOR, "...Accept screen ended.")
ENDPROC


#ENDIF
