//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   player_ped_public.sc                                        //
//      AUTHOR          :   Kenneth Ross                                                //
//      DESCRIPTION     :   Handles all the player character related procs.             //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "script_clock.sch"
USING "script_DEBUG.sch"
USING "script_network.sch"
USING "commands_clock.sch"
USING "commands_streaming.sch"
USING "commands_cutscene.sch"
USING "player_ped_data.sch"
USING "tattoo_private.sch"
USING "automatic_door_public.sch"
USING "vehicle_public.sch"
USING "flow_help_public.sch"
USING "player_scene_post_mission.sch"
USING "social_public.sch"
USING "candidate_public.sch"
USING "net_player_headshots.sch"
USING "mission_repeat_public.sch"

#if USE_CLF_DLC
	USING "static_mission_data_helpCLF.sch"
#endif
#if USE_NRM_DLC
	USING "static_mission_data_helpNRM.sch"
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    WARDROBE DATA																															   ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC VECTOR GET_PLAYER_PED_WARDROBE_COORDS(PLAYER_WARDROBE_ENUM eWardrobe)

	SWITCH eWardrobe
		CASE PW_MICHAEL_MANSION			RETURN <<-811.8961, 175.2218, 76.7308>> 		BREAK
		CASE PW_MICHAEL_COUNTRYSIDE		RETURN << 1969.4883, 3815.0303, 33.4337 >>		BREAK
		CASE PW_TREVOR_COUNTRYSIDE		RETURN << 1969.4883, 3815.0303, 33.4337 >>		BREAK
		CASE PW_TREVOR_CITY				RETURN << -1150.4913, -1513.3470, 10.6394 >>	BREAK
		CASE PW_TREVOR_STRIPCLUB		RETURN <<105.3011, -1303.3383, 27.7688>>		BREAK
		CASE PW_FRANKLIN_AUNTS			RETURN << -17.9973, -1438.9110, 31.1065 >>		BREAK
		CASE PW_FRANKLIN_HILLS			RETURN << 9.0157, 528.7267, 170.6221 >>			BREAK
		//B*1550550 this is now set for all freemode its only used for the low end to check if the player is facing the wardrobe.
		CASE PW_FREEMODE				RETURN <<259.791, -1004.398, -100.004>>			BREAK  
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PLAYER PED DATA                                                                                                                             ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
	/// PURPOSE: Gets the STRING value of the specified ped - debug use.
	FUNC STRING GET_PLAYER_PED_STRING(enumCharacterList ePed)
		SWITCH ePed
			CASE CHAR_MICHAEL			RETURN "CHAR_MICHAEL"			BREAK
			CASE CHAR_TREVOR			RETURN "CHAR_TREVOR"			BREAK
			CASE CHAR_FRANKLIN			RETURN "CHAR_FRANKLIN"			BREAK
			CASE CHAR_LESTER			RETURN "CHAR_LESTER"			BREAK
			CASE CHAR_ALL_PLAYERS_CONF	RETURN "CHAR_ALL_PLAYERS_CONF"	BREAK
			CASE CHAR_FRANK_TREV_CONF	RETURN "CHAR_FRANK_TREV_CONF"	BREAK
			CASE CHAR_MIKE_FRANK_CONF	RETURN "CHAR_MIKE_FRANK_CONF"	BREAK
			CASE CHAR_MIKE_TREV_CONF	RETURN "CHAR_MIKE_TREV_CONF"	BREAK
			CASE CHAR_STEVE_MIKE_CONF	RETURN "CHAR_STEVE_MIKE_CONF"	BREAK
			CASE CHAR_STEVE_TREV_CONF	RETURN "CHAR_STEVE_TREV_CONF"	BREAK
			CASE CHAR_JIMMY				RETURN "CHAR_JIMMY"				BREAK
			CASE CHAR_TRACEY			RETURN "CHAR_TRACEY"			BREAK
			CASE CHAR_AMANDA			RETURN "CHAR_AMANDA"			BREAK
			CASE CHAR_SIMEON			RETURN "CHAR_SIMEON"			BREAK
			CASE CHAR_LAMAR				RETURN "CHAR_LAMAR"				BREAK
			CASE CHAR_RON				RETURN "CHAR_RON"				BREAK
			CASE CHAR_CHENG				RETURN "CHAR_CHENG"				BREAK
			CASE CHAR_STEVE				RETURN "CHAR_STEVE"				BREAK
			CASE CHAR_WADE				RETURN "CHAR_WADE"				BREAK
			CASE CHAR_TENNIS_COACH 		RETURN "CHAR_TENNIS_COACH"		BREAK
			CASE CHAR_SOLOMON 			RETURN "CHAR_SOLOMON"			BREAK
			CASE CHAR_LAZLOW 			RETURN "CHAR_LAZLOW"			BREAK
			CASE CHAR_ESTATE_AGENT		RETURN "CHAR_ESTATE_AGENT"		BREAK
			CASE CHAR_DEVIN				RETURN "CHAR_DEVIN"				BREAK
			CASE CHAR_DAVE				RETURN "CHAR_DAVE"				BREAK
			CASE CHAR_MARTIN			RETURN "CHAR_MARTIN"			BREAK
			CASE CHAR_FLOYD				RETURN "CHAR_FLOYD"				BREAK
			CASE CHAR_BARRY				RETURN "CHAR_BARRY"				BREAK	
			CASE CHAR_BEVERLY			RETURN "CHAR_BEVERLY"			BREAK
			CASE CHAR_CRIS				RETURN "CHAR_CRIS"				BREAK
			CASE CHAR_DOM				RETURN "CHAR_DOM"				BREAK
			CASE CHAR_HAO				RETURN "CHAR_HAO"				BREAK
			CASE CHAR_HUNTER			RETURN "CHAR_HUNTER"			BREAK
			CASE CHAR_JIMMY_BOSTON		RETURN "CHAR_JIMMY_BOSTON"		BREAK
			CASE CHAR_JOE				RETURN "CHAR_JOE"				BREAK
			CASE CHAR_JOSEF				RETURN "CHAR_JOSEF"				BREAK
			CASE CHAR_JOSH				RETURN "CHAR_JOSH"				BREAK
			CASE CHAR_MANUEL			RETURN "CHAR_MANUEL"			BREAK
			CASE CHAR_MARNIE			RETURN "CHAR_MARNIE"			BREAK
			CASE CHAR_MARY_ANN			RETURN "CHAR_MARY_ANN"			BREAK
			CASE CHAR_MAUDE				RETURN "CHAR_MAUDE"				BREAK
			CASE CHAR_MRS_THORNHILL		RETURN "CHAR_MRS_THORNHILL"		BREAK
			CASE CHAR_NIGEL				RETURN "CHAR_NIGEL"				BREAK
			CASE CHAR_SASQUATCH			RETURN "CHAR_SASQUATCH"			BREAK
			CASE CHAR_ONEIL				RETURN "CHAR_ONEIL"				BREAK
			CASE CHAR_STRIPPER_JULIET	RETURN "CHAR_STRIPPER_JULIET"	BREAK
			CASE CHAR_STRIPPER_NIKKI	RETURN "CHAR_STRIPPER_NIKKI"	BREAK
			CASE CHAR_STRIPPER_CHASTITY	RETURN "CHAR_STRIPPER_CHASTITY"	BREAK
			CASE CHAR_STRIPPER_CHEETAH	RETURN "CHAR_STRIPPER_CHEETAH"	BREAK
			CASE CHAR_STRIPPER_SAPPHIRE	RETURN "CHAR_STRIPPER_SAPPHIRE"	BREAK
			CASE CHAR_STRIPPER_INFERNUS	RETURN "CHAR_STRIPPER_INFERNUS"	BREAK
			CASE CHAR_STRIPPER_FUFU		RETURN "CHAR_STRIPPER_FUFU"		BREAK
			CASE CHAR_STRIPPER_PEACH	RETURN "CHAR_STRIPPER_PEACH"	BREAK
			CASE CHAR_DETONATEBOMB		RETURN "CHAR_DETONATEBOMB"		BREAK
			CASE CHAR_CALL911			RETURN "CHAR_CALL911"			BREAK
			CASE CHAR_CASTRO			RETURN "CHAR_CASTRO"			BREAK
			CASE CHAR_ARTHUR			RETURN "CHAR_ARTHUR"			BREAK
			CASE CHAR_DOMESTIC_GIRL		RETURN "CHAR_DOMESTIC_GIRL"		BREAK
			CASE CHAR_HITCHER_GIRL		RETURN "CHAR_HITCHER_GIRL"		BREAK
			CASE CHAR_SOCIAL_CLUB		RETURN "CHAR_SOCIAL_CLUB"		BREAK
			CASE CHAR_MECHANIC			RETURN "CHAR_MECHANIC"			BREAK
			CASE CHAR_MULTIPLAYER		RETURN "CHAR_MULTIPLAYER"		BREAK
			CASE NO_CHARACTER			RETURN "NO_CHARACTER"			BREAK
		ENDSWITCH
		
		RETURN "PED_STRING_MISSING"
	ENDFUNC
#ENDIF

/// PURPOSE: Returns TRUE if the specified ped enum is one of the playable characters.
FUNC BOOL IS_PLAYER_PED_PLAYABLE(enumCharacterList ePed)
    RETURN (ENUM_TO_INT(ePed) < NUM_OF_PLAYABLE_PEDS)
ENDFUNC

/// PURPOSE: Returns TRUE if the specified ped enum is one of the playable characters.
FUNC BOOL IS_PLAYER_MODEL_PLAYABLE(MODEL_NAMES eModel)
	IF eModel = PLAYER_ZERO
	OR eModel = PLAYER_ONE
	OR eModel = PLAYER_TWO
		RETURN TRUE
	ENDIF
    RETURN FALSE
ENDFUNC


/// PURPOSE: Returns the model enum for the specified player character
FUNC MODEL_NAMES GET_PLAYER_PED_MODELCLF(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)	
		RETURN g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[ePed].game_model	
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_PLAYER_PED_MODELCLF - Ped is not a player character.")
                PRINTSTRING("\nGET_PLAYER_PED_MODELCLF - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC
/// PURPOSE: Returns the model enum for the specified player character
FUNC MODEL_NAMES GET_PLAYER_PED_MODELNRM(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[ePed].game_model
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_PLAYER_PED_MODELNRM - Ped is not a player character.")
                PRINTSTRING("\GET_PLAYER_PED_MODELNRM - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC
/// PURPOSE: Returns the model enum for the specified player character
FUNC MODEL_NAMES GET_PLAYER_PED_MODEL(enumCharacterList ePed)
    #if USE_CLF_DLC
		if g_bLoadedClifford
			RETURN GET_PLAYER_PED_MODELCLF(eped)
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			RETURN GET_PLAYER_PED_MODELNRM(eped)
		endif
	#endif
	
	IF IS_PLAYER_PED_PLAYABLE(ePed)			
        RETURN GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(ePed)		
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_PLAYER_PED_MODEL - Ped is not a player character.")
                PRINTSTRING("\nGET_PLAYER_PED_MODEL - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: Returns the player ped enum based on the specified peds model
FUNC enumCharacterList GET_PLAYER_PED_ENUMCLF(PED_INDEX ped)
    IF DOES_ENTITY_EXIST(ped)
        INT i
        MODEL_NAMES ePedModel = GET_ENTITY_MODEL(ped)
        FOR i = 0 TO NUM_OF_PLAYABLE_PEDS-1
            IF GET_PLAYER_PED_MODELCLF(INT_TO_ENUM(enumCharacterList, i)) = ePedModel
                RETURN (INT_TO_ENUM(enumCharacterList, i))
            ENDIF
        ENDFOR
    ENDIF
    
    RETURN NO_CHARACTER
ENDFUNC
FUNC enumCharacterList GET_PLAYER_PED_ENUMNRM(PED_INDEX ped)
    IF DOES_ENTITY_EXIST(ped)
        INT i
        MODEL_NAMES ePedModel = GET_ENTITY_MODEL(ped)
        FOR i = 0 TO NUM_OF_PLAYABLE_PEDS-1
            IF GET_PLAYER_PED_MODELNRM(INT_TO_ENUM(enumCharacterList, i)) = ePedModel
                RETURN (INT_TO_ENUM(enumCharacterList, i))
            ENDIF
        ENDFOR
    ENDIF
    
    RETURN NO_CHARACTER
ENDFUNC
FUNC enumCharacterList GET_PLAYER_PED_ENUM(PED_INDEX ped)
	#if USE_CLF_DLC
		if g_bLoadedClifford
			return GET_PLAYER_PED_ENUMCLF(ped) 
		endif 
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			return GET_PLAYER_PED_ENUMNRM(ped) 
		endif 
	#endif


    IF DOES_ENTITY_EXIST(ped)
        INT i
        MODEL_NAMES ePedModel = GET_ENTITY_MODEL(ped)
        FOR i = 0 TO NUM_OF_PLAYABLE_PEDS-1
            IF GET_PLAYER_PED_MODEL(INT_TO_ENUM(enumCharacterList, i)) = ePedModel
                RETURN (INT_TO_ENUM(enumCharacterList, i))
            ENDIF
        ENDFOR
    ENDIF
    
    RETURN NO_CHARACTER
ENDFUNC

/// PURPOSE:
///    Returns the info needed to store this current variation.
///    If this item shouldn't be stored, it returns the info for blank version.
///    e.g. Parachute should be ignored, so we return the info for "no special component"
/// PARAMS:
///    mPed - the ped we are storing variations for
///    ePedComponent - component we are storing
///    iDrawable - where to store the drawable info
///    iTexture - where to store the texture info
///    iPalette - where to store the palette info
PROC GET_VARIATION_TO_STORE(PED_INDEX mPed, PED_COMPONENT ePedComponent, INT &iDrawable, INT &iTexture, INT &iPalette, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_DEFAULT, enumCharacterList ePedOverride = NO_CHARACTER)

	enumCharacterList eChar = GET_PLAYER_PED_ENUM(mPed)
	
	// get current item info- will overwrite if this item needs to be ignored
	IF mPed != NULL // skip over when null, used for cutscene variation setup (Kenneth)
		iDrawable = GET_PED_DRAWABLE_VARIATION(mPed, ePedComponent)
		iTexture = GET_PED_TEXTURE_VARIATION(mPed, ePedComponent)
		iPalette = GET_PED_PALETTE_VARIATION(mPed, ePedComponent)
	ELSE
		eChar = ePedOverride
	ENDIF
	
	IF eCompBlocks = CBF_NONE
		EXIT
	ENDIF
			
	SWITCH eChar
		CASE CHAR_MICHAEL
			IF ePedComponent = PED_COMP_SPECIAL
				// Parachute- return No Special
				IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
				OR (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_PARACHUTES) != CBF_NONE
					IF iDrawable = 15
						CPRINTLN(DEBUG_PED_COMP, "Parachute equipped: storing No Special instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
				// Scuba tank - return No Special
				IF (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_SCUBA) != CBF_NONE
					IF iDrawable = 3
					OR iDrawable = 22
						CPRINTLN(DEBUG_PED_COMP, "Scuba tank: storing No Special instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
			ELIF ePedComponent = PED_COMP_SPECIAL2
				// Parachute- return No Special
				IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
				OR (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_PARACHUTES) != CBF_NONE
					IF iDrawable = 5
						CPRINTLN(DEBUG_PED_COMP, "Parachute equipped: storing No Special2 instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
				
				// Masks - return No Special 2
				IF (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_MASKS) != CBF_NONE
					IF iDrawable = 8
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No Special2 instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			IF ePedComponent = PED_COMP_SPECIAL
				// Parachute- return No Special
				IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
				OR (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_PARACHUTES) != CBF_NONE
					IF iDrawable = 1
					OR iDrawable = 10
						CPRINTLN(DEBUG_PED_COMP, "Parachute equipped: storing No Special instead.")
						iDrawable = 14 
						iTexture = 0
					ENDIF
				ENDIF
				
				// Scuba tank - return No Special
				IF (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_SCUBA) != CBF_NONE
					IF iDrawable = 19
						CPRINTLN(DEBUG_PED_COMP, "Scuba tank: storing No Special instead.")
						iDrawable = 14
						iTexture = 0
					ENDIF
				ENDIF
			ELIF ePedComponent = PED_COMP_SPECIAL2
				// Masks - return No Special 2
				IF (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_MASKS) != CBF_NONE
					IF iDrawable = 5
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No Special2 instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF ePedComponent = PED_COMP_SPECIAL
				// Parachute- return No Special
				IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
				OR (eCompBlocks & CBF_ALL) != CBF_NONE
				OR (eCompBlocks & CBF_PARACHUTES) != CBF_NONE
					IF iDrawable = 3
						CPRINTLN(DEBUG_PED_COMP, "Parachute equipped: storing No Special instead.")
						iDrawable = 14 
						iTexture = 0
					ENDIF
				ENDIF
				
			ELIF ePedComponent = PED_COMP_SPECIAL2
				// Masks - return No Special 2
				IF iDrawable >= 5 AND iDrawable <= 7
					IF (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_MASKS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No Special instead.")
						iDrawable = 0 
						iTexture = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Returns the info needed to store this current prop variation.
///    If this item shouldn't be stored, it returns the info for blank version.
///    e.g. Parachute should be ignored, so we return the info for "no special component"
/// PARAMS:
///    mPed - the ped we are storing variations for
///    ePropPos - prop type we are storing
///    iIndex - where to store the index info
///    iTexture - where to store the texture info
PROC GET_PROP_VARIATION_TO_STORE(PED_INDEX mPed, PED_PROP_POSITION ePropPos, INT &iIndex, INT &iTexture, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_DEFAULT, enumCharacterList ePedOverride = NO_CHARACTER)

	enumCharacterList eChar = GET_PLAYER_PED_ENUM(mPed)
	
	// get current item info- will overwrite if this item needs to be ignored
	IF mPed != NULL // skip over when null, used for cutscene variation setup (Kenneth)
		iIndex = GET_PED_PROP_INDEX(mPed, ePropPos)
		iTexture = GET_PED_PROP_TEXTURE_INDEX(mPed, ePropPos)
	ELSE
		eChar = ePedOverride
	ENDIF
	
	IF eCompBlocks = CBF_NONE
		EXIT
	ENDIF
	
	// We dont store helmets so if we have one on, revert to the stored head prop.
	IF ePropPos = ANCHOR_HEAD
		IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
		OR (eCompBlocks & CBF_ALL) != CBF_NONE
		OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
			IF mPed != NULL // skip over when null, used for cutscene variation setup (Kenneth)
				IF IS_PED_WEARING_HELMET(mPed)
				AND GET_PED_HELMET_STORED_HAT_PROP_INDEX(mPed) != -1
					CPRINTLN(DEBUG_PED_COMP, "Code have a stored hat so using this instead.")
					iIndex = GET_PED_HELMET_STORED_HAT_PROP_INDEX(mPed)
					iTexture = GET_PED_HELMET_STORED_HAT_TEX_INDEX(mPed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH eChar
		CASE CHAR_MICHAEL
			IF ePropPos = ANCHOR_HEAD
				// Cycling Helmet- return No Head Prop
				IF iIndex = 7
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Cycling Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				
				// Motorbike Helmet- return No Head Prop
				ELIF iIndex = 11
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Motorbike Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Helicopter Headset- return No Head Prop
				ELIF iIndex = 21
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HEADSETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Helicopter Headset equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Moto-X helmet- return No Head Prop
				ELIF iIndex = 23
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Moto-X helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Flight cap
				ELIF iIndex = 27
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Flight cap equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Fighter jet helmet
				ELIF iIndex = 28
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Fighter jet helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Masks - return No Head Prop
				ELIF iIndex >= 14 AND iIndex <= 20
					IF (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_MASKS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				ENDIF
			ELIF ePropPos = ANCHOR_EYES
				// Scuba mask- return No Eye Prop
				IF iIndex = 1
					IF (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_SCUBA) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Scuba Mask equipped: storing No eye prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			IF ePropPos = ANCHOR_HEAD
				// Cycling Helmet- return No Head Prop
				IF iIndex = 2
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Cycling Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				
				// Helicopter headset- return No Head Prop
				ELIF iIndex = 4
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HEADSETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Helicopter Headset: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				
				// Motorbike Helmet- return No Head Prop
				ELIF iIndex = 6
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Motorbike Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				
				// Moto X Helmet- return No Head Prop
				ELIF iIndex = 17
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Moto X Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Flight cap
				ELIF iIndex = 20
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Flight cap equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Fighter jet helmet
				ELIF iIndex = 21
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Fighter jet helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Masks - return No Head Prop
				ELIF iIndex >= 8 AND iIndex <= 14
					IF (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_MASKS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF ePropPos = ANCHOR_HEAD
			
				// Motorbike Helmet - return No Head Prop
				IF iIndex = 9
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Motorbike Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
			
				// Motorbike Helmet (Army Style)- return No Head Prop
				ELIF iIndex = 11
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Motorbike Helmet equipped (Army Style): storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Helicopter Headset- return No Head Prop
				ELIF iIndex = 12
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Helicopter Headset equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Flight cap
				ELIF iIndex = 21
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Flight cap equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Moto X Helmet- return No Head Prop
				ELIF iIndex = 23
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Moto X Helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Fighter jet helmet
				ELIF iIndex = 27
					IF (eCompBlocks & CBF_DEFAULT) != CBF_NONE
					OR (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_HELMETS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Fighter jet helmet equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
					
				// Masks - return No Head Prop
				ELIF (iIndex >= 14 AND iIndex <= 20) OR iIndex = 2
					IF (eCompBlocks & CBF_ALL) != CBF_NONE
					OR (eCompBlocks & CBF_MASKS) != CBF_NONE
						CPRINTLN(DEBUG_PED_COMP, "Mask equipped: storing No head prop instead.")
						iIndex = -1 
						iTexture = -1
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Fills the struct with the peds component variations and props
PROC GET_PED_VARIATIONS(PED_INDEX pedID, PED_VARIATION_STRUCT &sVariations, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_DEFAULT, INT iSlot = -1)
	IF NOT IS_PED_INJURED(pedID)
		
		enumCharacterList eChar = GET_PLAYER_PED_ENUM(pedID)
		CPRINTLN(DEBUG_PED_COMP, "GET_PED_VARIATIONS for ", eChar)
		INT i
		REPEAT NUM_PED_COMPONENTS i
			GET_VARIATION_TO_STORE(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i], sVariations.iTextureVariation[i], sVariations.iPaletteVariation[i], eCompBlocks)
			//CPRINTLN(DEBUG_PED_COMP, GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "...component variation [",sVariations.iDrawableVariation[i], ",", sVariations.iTextureVariation[i], "]")
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
			GET_PROP_VARIATION_TO_STORE(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i], sVariations.iPropTexture[i], eCompBlocks)
			//CPRINTLN(DEBUG_PED_COMP,"...prop [", sVariations.iPropIndex[i], ",", sVariations.iPropTexture[i], "]")
		ENDREPEAT
		
		IF IS_PLAYER_PED_PLAYABLE(eChar)
			#if USE_CLF_DLC
				sVariations.eStoredHairstyle = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle
				sVariations.eItemThatForcedHairChange = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange
		 		sVariations.eTypeThatForcedHairChange = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>GET_PED_VARIATIONS storing mask info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "<clf>eStoredHairstyle = ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
			
				sVariations.eStoredBeard = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard
				sVariations.eItemThatForcedBeardChange = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange
		 		sVariations.eTypeThatForcedBeardChange = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>GET_PED_VARIATIONS storing Beard info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "<clf>eStoredBeard = ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange)
			#endif
			#if USE_NRM_DLC
				sVariations.eStoredHairstyle = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle
				sVariations.eItemThatForcedHairChange = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange
		 		sVariations.eTypeThatForcedHairChange = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>GET_PED_VARIATIONS storing mask info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "<clf>eStoredHairstyle = ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
			
				sVariations.eStoredBeard = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard
				sVariations.eItemThatForcedBeardChange = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange
		 		sVariations.eTypeThatForcedBeardChange = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>GET_PED_VARIATIONS storing Beard info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "<clf>eStoredBeard = ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange)
			#endif
			#if not USE_CLF_DLC		
			#if not USE_NRM_DLC
				sVariations.eStoredHairstyle = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle
				sVariations.eItemThatForcedHairChange = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange
		 		sVariations.eTypeThatForcedHairChange = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, "GET_PED_VARIATIONS storing mask info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "eStoredHairstyle = ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
			
				sVariations.eStoredBeard = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard
				sVariations.eItemThatForcedBeardChange = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange
		 		sVariations.eTypeThatForcedBeardChange = g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, "GET_PED_VARIATIONS storing Beard info for ", eChar)
				CPRINTLN(DEBUG_PED_COMP, "eStoredBeard = ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange)
			#endif
			#endif
		
		ELIF NETWORK_IS_GAME_IN_PROGRESS() 
		#IF USE_TU_CHANGES
		AND GET_ENTITY_MODEL(pedID) = GET_ENTITY_MODEL(PLAYER_PED_ID())
		#ENDIF
		#IF NOT USE_TU_CHANGES
		AND pedID = PLAYER_PED_ID()
		#ENDIF
			
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, iSlot)
				sVariations.eStoredHairstyle = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, iSlot))
			ELSE
				sVariations.eStoredHairstyle = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, iSlot))
			ENDIF
			
			
			sVariations.eItemThatForcedHairChange = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, iSlot))
			sVariations.eTypeThatForcedHairChange = INT_TO_ENUM(PED_COMP_TYPE_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, iSlot))
			CPRINTLN(DEBUG_PED_COMP, "GET_PED_VARIATIONS storing mask info for ", eChar)
//			CPRINTLN(DEBUG_PED_COMP, "eStoredHairstyle = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eTypeThatForcedHairChange))
		ENDIF
		
		#IF NOT USE_SP_DLC
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND pedID = PLAYER_PED_ID()
				CPRINTLN(DEBUG_PED_COMP, "GET_PED_VARIATIONS restore hair  stuff for ", "Called by...", GET_THIS_SCRIPT_NAME())
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, iSlot)
					sVariations.eStoredHairstyle = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, iSlot))
				ELSE
					sVariations.eStoredHairstyle = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, iSlot))
				ENDIF
			ENDIF
		#ENDIF
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the peds component variations and props
PROC SET_PED_VARIATIONS(PED_INDEX pedID, PED_VARIATION_STRUCT &sVariations, BOOL bBypassMPBerdCheck = FALSE, BOOL bBypassMPHeadPropCheck = FALSE, BOOL bValidateVariations = TRUE, BOOL bIgnoreDeadCheck = FALSE)

	CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS called...")

	IF NOT IS_PED_INJURED(pedID)
	OR bIgnoreDeadCheck
	
		enumCharacterList eChar = GET_PLAYER_PED_ENUM(pedID)
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID)
		PED_COMP_NAME_ENUM eReturnItem
		CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS for ", eChar, " called by...", GET_THIS_SCRIPT_NAME())
		
		INT i
		REPEAT NUM_PED_COMPONENTS i
		
			// Within the new character creator we do not want BERD's applied
			IF i != ENUM_TO_INT(PED_COMP_BERD)
			OR (i = ENUM_TO_INT(PED_COMP_BERD) AND NOT bBypassMPBerdCheck)
			
				IF bValidateVariations
					IF (sVariations.iDrawableVariation[i] != 0)
					AND (sVariations.iDrawableVariation[i] >= GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, i)))
						CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS, invalid drawable for component ", i, ". Stored=", sVariations.iDrawableVariation[i], ", Max=", GET_NUMBER_OF_PED_DRAWABLE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, i)))
						sVariations.iDrawableVariation[i] = 0
					ENDIF
					IF (sVariations.iTextureVariation[i] != 0)
					AND (sVariations.iTextureVariation[i] >= GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i]))
						CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS, invalid texture for component ", i, ". Stored=", sVariations.iTextureVariation[i], ", Max=", GET_NUMBER_OF_PED_TEXTURE_VARIATIONS(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i]))
						sVariations.iTextureVariation[i] = 0
					ENDIF
				ENDIF
				
				SET_PED_COMPONENT_VARIATION(pedID, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i], sVariations.iTextureVariation[i], sVariations.iPaletteVariation[i])
						
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS, skipping component: ", i)
			#ENDIF
			
			ENDIF
			
			//CPRINTLN(DEBUG_PED_COMP, GET_COMP_VARIATION_TYPE_STRING(INT_TO_ENUM(PED_COMPONENT, i)), "...component variation [",sVariations.iDrawableVariation[i], ",", sVariations.iTextureVariation[i], "]")
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
		
			IF bValidateVariations
				IF sVariations.iPropIndex[i] != -1
				AND sVariations.iPropIndex[i] != 255
					IF (sVariations.iPropIndex[i] >= GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(pedID, INT_TO_ENUM(PED_PROP_POSITION, i)))
						CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS, invalid drawable for prop ", i, ". Stored=", sVariations.iPropIndex[i], ", Max=", GET_NUMBER_OF_PED_PROP_DRAWABLE_VARIATIONS(pedID, INT_TO_ENUM(PED_PROP_POSITION, i)))
						sVariations.iPropIndex[i] = -1
					ELIF (sVariations.iPropTexture[i] >= GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i]))
						CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS, invalid texture for prop ", i, ". Stored=", sVariations.iPropTexture[i], ", Max=", GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i]))
						sVariations.iPropTexture[i] = 0
					ENDIF
				ENDIF
			ENDIF
			
			IF sVariations.iPropIndex[i] != -1
			AND sVariations.iPropIndex[i] != 255
				//PRINTSTRING("...prop [")PRINTINT(sVariations.iPropIndex[i])PRINTSTRING(",")PRINTINT(sVariations.iPropTexture[i])PRINTSTRING("]")PRINTNL()
				SET_PED_PROP_INDEX(pedID, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i], sVariations.iPropTexture[i])
			ELSE
				CLEAR_PED_PROP(pedID, INT_TO_ENUM(PED_PROP_POSITION, i))
			ENDIF
		ENDREPEAT
		
		IF IS_PLAYER_PED_PLAYABLE(eChar)
		
			#if USE_CLF_DLC
				CPRINTLN(DEBUG_PED_COMP, "<clf>SET_PED_VARIATIONS restore hair  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = sVariations.eStoredHairstyle 
				g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange = sVariations.eItemThatForcedHairChange 
			 	g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange = sVariations.eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>.eStoredHairstyle = ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
				IF IS_SAFE_TO_RESTORE_SAVED_HAIR(pedID, ePedModel, eReturnItem)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_HAIR, eReturnItem, FALSE)
				ENDIF
			
				CPRINTLN(DEBUG_PED_COMP, "<clf>SET_PED_VARIATIONS restore Beard  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = sVariations.eStoredBeard
				g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange = sVariations.eItemThatForcedBeardChange 
			 	g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange = sVariations.eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>.eStoredBeard = ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange))
			
				UNUSED_PARAMETER(bBypassMPBerdCheck)
				UNUSED_PARAMETER(bBypassMPHeadPropCheck)
			#endif
			#if USE_NRM_DLC
				CPRINTLN(DEBUG_PED_COMP, "<clf>SET_PED_VARIATIONS restore hair  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = sVariations.eStoredHairstyle 
				g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange = sVariations.eItemThatForcedHairChange 
			 	g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange = sVariations.eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>.eStoredHairstyle = ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
				IF IS_SAFE_TO_RESTORE_SAVED_HAIR(pedID, ePedModel, eReturnItem)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_HAIR, eReturnItem, FALSE)
				ENDIF
			
				CPRINTLN(DEBUG_PED_COMP, "<clf>SET_PED_VARIATIONS restore Beard  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = sVariations.eStoredBeard
				g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange = sVariations.eItemThatForcedBeardChange 
			 	g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange = sVariations.eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, "<clf>.eStoredBeard = ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange))
			
				UNUSED_PARAMETER(bBypassMPBerdCheck)
				UNUSED_PARAMETER(bBypassMPHeadPropCheck)
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS restore hair  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = sVariations.eStoredHairstyle 
				g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange = sVariations.eItemThatForcedHairChange 
			 	g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange = sVariations.eTypeThatForcedHairChange
				CPRINTLN(DEBUG_PED_COMP, ".eStoredHairstyle = ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle, " eItemThatForcedHairChange= ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedHairChange, " eTypeThatForcedHairChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedHairChange))
				IF IS_SAFE_TO_RESTORE_SAVED_HAIR(pedID, ePedModel, eReturnItem)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_HAIR, eReturnItem, FALSE)
				ENDIF
			
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS restore Beard  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = sVariations.eStoredBeard
				g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange = sVariations.eItemThatForcedBeardChange 
			 	g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange = sVariations.eTypeThatForcedBeardChange
				CPRINTLN(DEBUG_PED_COMP, ".eStoredBeard = ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard, " eItemThatForcedBeardChange= ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eItemThatForcedBeardChange, " eTypeThatForcedBeardChange = ", GET_PED_COMP_TYPE_STRING(g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eTypeThatForcedBeardChange))
			#endif
			#endif
			
			IF IS_SAFE_TO_RESTORE_SAVED_BEARD(pedID, ePedModel, eReturnItem)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_BERD, eReturnItem, FALSE)
			ENDIF
		
		#IF NOT USE_SP_DLC
		ELIF NETWORK_IS_GAME_IN_PROGRESS()
		#IF USE_TU_CHANGES
		AND GET_ENTITY_MODEL(pedID) = GET_ENTITY_MODEL(PLAYER_PED_ID())
		#ENDIF
		#IF NOT USE_TU_CHANGES
		AND pedID = PLAYER_PED_ID()
		#ENDIF
			BOOL bBlockTorsoDecals = SHOULD_TORSO_DECALS_BE_BLOCKED_FOR_OUTFIT(pedID)
			
//			#IF USE_TU_CHANGES
//			CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS restore berd  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
//			// additional berd checks are required for masks that shrink heads use ped comp system
//			PED_COMP_NAME_ENUM eBerdItem 
//			eBerdItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sVariations.iDrawableVariation[PED_COMP_BERD], sVariations.iTextureVariation[PED_COMP_BERD], COMP_TYPE_BERD)
//			SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_BERD, eBerdItem, FALSE)
//			#ENDIF
			
			CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS restore hair  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, ENUM_TO_INT(sVariations.eItemThatForcedHairChange), g_iPedComponentSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, ENUM_TO_INT(sVariations.eTypeThatForcedHairChange), g_iPedComponentSlot)			
			// equip the hair item via ped comp system so we get the correct hair overlay
			#IF USE_TU_CHANGES
				PED_COMP_NAME_ENUM eHairItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sVariations.iDrawableVariation[PED_COMP_HAIR], sVariations.iTextureVariation[PED_COMP_HAIR], COMP_TYPE_HAIR)
				IF (eHairItem != DUMMY_PED_COMP)
					PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
					IF (ePedModel = MP_M_FREEMODE_01)
						eGRHairItem = GET_MALE_HAIR(eHairItem)
					ELIF (ePedModel = MP_F_FREEMODE_01)
						eGRHairItem = GET_FEMALE_HAIR(eHairItem)
					ENDIF
					
					IF (eGRHairItem != DUMMY_PED_COMP)
					AND (eHairItem != eGRHairItem)
						CPRINTLN(DEBUG_PED_COMP,"SET_PED_VARIATIONS gr_hair: replacing hair enum ", eHairItem, " with gunrunning hair enum ", eGRHairItem)
						eHairItem = eGRHairItem
					ENDIF
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_HAIR, eHairItem, FALSE,
						DEFAULT,				//INT iPaletteOverride = -1,
						DEFAULT,				//BOOL bUseGlobalOutfitData = FALSE,
						DEFAULT,				//BOOL bPreviewing = FALSE,
						DEFAULT,				//BOOL bReEquipAccessories = FALSE,
						DEFAULT,				//INT iHairTint1 = -1,
						DEFAULT,				//INT iHairTint2 = -1,
						g_iPedComponentSlot,	//INT iSlot = -1,
						bBlockTorsoDecals)		//BOOL bBlockTorsoDecals)
			#ENDIF
			
			
			CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS restore beard stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_beardDO, ENUM_TO_INT(sVariations.eStoredbeardstyle), g_iPedComponentSlot)
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_beardDO_SA, ENUM_TO_INT(sVariations.eStoredbeardstyle), g_iPedComponentSlot)
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_beard_ITEM, ENUM_TO_INT(sVariations.eItemThatForcedbeardChange), g_iPedComponentSlot)
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_beard_TYPE, ENUM_TO_INT(sVariations.eTypeThatForcedbeardChange), g_iPedComponentSlot)
			//SET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_beard_SA_STAT, TRUE)
//			CPRINTLN(DEBUG_PED_COMP, ".eStoredbeardstyle = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eStoredbeardstyle, " eItemThatForcedbeardChange= ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eItemThatForcedbeardChange, " eTypeThatForcedbeardChange = ", GET_PED_COMP_TYPE_STRING(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.eTypeThatForcedbeardChange))
			
			// equip the beard item via ped comp system so we get the correct beard controlled head size/overlays
			IF NOT bBypassMPBerdCheck
			#IF USE_TU_CHANGES
				PED_COMP_NAME_ENUM eBeardItem = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sVariations.iDrawableVariation[PED_COMP_BERD], sVariations.iTextureVariation[PED_COMP_BERD], COMP_TYPE_BERD)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_BERD, eBeardItem, FALSE,
						DEFAULT,				//INT iPaletteOverride = -1,
						DEFAULT,				//BOOL bUseGlobalOutfitData = FALSE,
						DEFAULT,				//BOOL bPreviewing = FALSE,
						DEFAULT,				//BOOL bReEquipAccessories = FALSE,
						DEFAULT,				//INT iHairTint1 = -1,
						DEFAULT,				//INT iHairTint2 = -1,
						g_iPedComponentSlot,	//INT iSlot = -1,
						bBlockTorsoDecals)		//BOOL bBlockTorsoDecals)
			#ENDIF
			ENDIF
			
			// equip the Helmet item via ped comp system so we get the correct Helmet controlled visor
			IF NOT bBypassMPHeadPropCheck
			#IF USE_TU_CHANGES
				PED_COMP_NAME_ENUM eHelmetItem = GET_PROP_ITEM_FROM_VARIATIONS(pedID, sVariations.iPropIndex[ANCHOR_HEAD], sVariations.iPropTexture[ANCHOR_HEAD], ANCHOR_HEAD)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_PROPS, eHelmetItem, FALSE,
						DEFAULT,				//INT iPaletteOverride = -1,
						DEFAULT,				//BOOL bUseGlobalOutfitData = FALSE,
						DEFAULT,				//BOOL bPreviewing = FALSE,
						DEFAULT,				//BOOL bReEquipAccessories = FALSE,
						DEFAULT,				//INT iHairTint1 = -1,
						DEFAULT,				//INT iHairTint2 = -1,
						g_iPedComponentSlot,	//INT iSlot = -1,
						bBlockTorsoDecals)		//BOOL bBlockTorsoDecals)
			#ENDIF
			ENDIF
		#ENDIF
		ENDIF
		
		
		
		// always have to apply to for player ped. We always have to record players hair, other wise we are recording wrong data KW

		#IF NOT USE_SP_DLC
		#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND pedID = PLAYER_PED_ID()
				CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS 2 restore hair  stuff for ", eChar, "  Called by...", GET_THIS_SCRIPT_NAME())
				PED_COMP_NAME_ENUM eStoredHairstyle = sVariations.eStoredHairstyle
				IF ePedModel = MP_M_FREEMODE_01
					eStoredHairstyle = GET_MALE_HAIR(eStoredHairstyle)
				ELSE
					eStoredHairstyle = GET_FEMALE_HAIR(eStoredHairstyle)
				ENDIF
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, ENUM_TO_INT(eStoredHairstyle), g_iPedComponentSlot)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, ENUM_TO_INT(eStoredHairstyle), g_iPedComponentSlot)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, TRUE)
			ENDIF
		#ENDIF
		#ENDIF
		
		// Pogo glasses need to be tied to the pogo mask.
		IF ePedModel = MP_M_FREEMODE_01
		OR ePedModel = MP_F_FREEMODE_01
			INT iDLCBerdHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(pedID), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(pedID, PED_COMP_BERD))
			INT iDLCEyesHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(pedID), ENUM_TO_INT(ANCHOR_EYES), GET_PED_PROP_INDEX(pedID, ANCHOR_EYES), GET_PED_PROP_TEXTURE_INDEX(pedID, ANCHOR_EYES))
			
			IF iDLCBerdHash = HASH("DLC_MP_ARENA_M_BERD_13_0")
			OR iDLCBerdHash = HASH("DLC_MP_ARENA_F_BERD_13_0")
				// Force glasses on
				IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCEyesHash, DLC_RESTRICTION_TAG_ARENA_DRAW_0, ENUM_TO_INT(SHOP_PED_PROP))
					scrShopPedProp propItem
					INIT_SHOP_PED_PROP(propItem)
					IF iDLCBerdHash = HASH("DLC_MP_ARENA_M_BERD_13_0")
						GET_SHOP_PED_QUERY_PROP(HASH("DLC_MP_ARENA_M_PEYES_0_0"), propItem)
					ELSE
						GET_SHOP_PED_QUERY_PROP(HASH("DLC_MP_ARENA_F_PEYES_0_0"), propItem)
					ENDIF
					SET_PED_PROP_INDEX(pedID, ANCHOR_EYES, propItem.m_propIndex, propItem.m_textureIndex)
					CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS - Forcing pogo glasses on")
				ENDIF
			ELSE
				// Force glasses off
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCEyesHash, DLC_RESTRICTION_TAG_ARENA_DRAW_0, ENUM_TO_INT(SHOP_PED_PROP))
					CLEAR_PED_PROP(pedID, ANCHOR_EYES)
					CPRINTLN(DEBUG_PED_COMP, "SET_PED_VARIATIONS - Forcing pogo glasses off")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC


/// PURPOSE: Returns the vehicle model enum for the specified player character
FUNC MODEL_NAMES GET_PLAYER_VEH_MODEL(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        PED_VEH_DATA_STRUCT sData
        GET_PLAYER_VEH_DATA(ePed, sData, eTypePreference)
        RETURN sData.model
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_PLAYER_VEH_MODEL - Ped is not a player character.")
                PRINTSTRING("\nGET_PLAYER_VEH_MODEL - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: Makes a request for the appropriate player ped model
PROC REQUEST_PLAYER_PED_MODELCLF(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_PLAYER_PED_MODELCLF(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_PLAYER_PED_MODEL CLF - Ped is not a player character.")
                PRINTSTRING("\nREQUEST_PLAYER_PED_MODEL CLF - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC
PROC REQUEST_PLAYER_PED_MODELNRM(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_PLAYER_PED_MODELNRM(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_PLAYER_PED_MODEL NRM - Ped is not a player character.")
                PRINTSTRING("\nREQUEST_PLAYER_PED_MODEL NRM - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC
PROC REQUEST_PLAYER_PED_MODEL(enumCharacterList ePed)
#if USE_CLF_DLC
	if g_bLoadedClifford
		REQUEST_PLAYER_PED_MODELCLF(ePed)
		EXIT
	endif
#endif
#if USE_NRM_DLC
	if g_bLoadedNorman
		REQUEST_PLAYER_PED_MODELNRM(ePed)
		EXIT
	endif
#endif

    IF IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_PLAYER_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_PLAYER_PED_MODEL - Ped is not a player character.")
                PRINTSTRING("\nREQUEST_PLAYER_PED_MODEL - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Checks to see if the model for the specified ped has been loaded
FUNC BOOL HAS_PLAYER_PED_MODEL_LOADEDCLF(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_PLAYER_PED_MODELCLF(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")
                PRINTSTRING("\nHAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC
FUNC BOOL HAS_PLAYER_PED_MODEL_LOADEDNRM(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_PLAYER_PED_MODELNRM(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")
                PRINTSTRING("\nHAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC
FUNC BOOL HAS_PLAYER_PED_MODEL_LOADED(enumCharacterList ePed)
#if USE_CLF_DLC
	if g_bLoadedClifford
		return HAS_PLAYER_PED_MODEL_LOADEDCLF(ePed)		
	endif
#endif
#if USE_NRM_DLC
	if g_bLoadedNorman
		return HAS_PLAYER_PED_MODEL_LOADEDNRM(ePed)		
	endif
#endif   
	
	IF IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")
                PRINTSTRING("\nHAS_PLAYER_PED_MODEL_LOADED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

/// PURPOSE: Marks the specified player peds model as no longer needed
PROC SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDEDCLF(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODELCLF(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED CLF- Ped is not a player character.")
                PRINTSTRING("\nSET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED CLF- Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC
PROC SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDEDNRM(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODELNRM(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED NRM - Ped is not a player character.")
                PRINTSTRING("\nSET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED NRM - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC
PROC SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED(enumCharacterList ePed)
#if USE_CLF_DLC
	if g_bLoadedClifford
		SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDEDCLF(ePed)		
		EXIT
	endif
#endif
#if USE_NRM_DLC
	if g_bLoadedNorman
		SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDEDNRM(ePed)	
		EXIT
	endif
#endif     

	IF IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED - Ped is not a player character.")
                PRINTSTRING("\nSET_PLAYER_PED_MODEL_AS_NO_LONGER_NEEDED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Makes a request for the appropriate player vehicle model
PROC REQUEST_PLAYER_VEH_MODEL(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_PLAYER_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_PLAYER_VEH_MODEL - Ped is not a player character.")
                PRINTSTRING("\nREQUEST_PLAYER_VEH_MODEL - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Checks to see if the model for the specified ped vehicle has been loaded
FUNC BOOL HAS_PLAYER_VEH_MODEL_LOADED(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_PLAYER_VEH_MODEL_LOADED - Ped is not a player character.")
                PRINTSTRING("\nHAS_PLAYER_VEH_MODEL_LOADED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

/// PURPOSE: Marks the specified player vehicle model as no longer needed
PROC SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED - Ped is not a player character.")
                PRINTSTRING("\nSET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED - Ped is not a player character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    STORY PED DATA                                                                                                                              ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns the model enum for the specified story character
FUNC MODEL_NAMES GET_NPC_PED_MODEL(enumCharacterList ePed)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
        	RETURN g_savedGlobalsClifford.sCharSheetData.g_CharacterSheet[ePed].game_model
		#endif
		#if USE_NRM_DLC
        	RETURN g_savedGlobalsnorman.sCharSheetData.g_CharacterSheet[ePed].game_model
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
        	RETURN GLOBAL_CHARACTER_SHEET_GET_GAME_MODEL(ePed)
		#endif
		#endif
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_NPC_PED_MODEL - Ped is not a story character.")
                PRINTSTRING("\nGET_NPC_PED_MODEL - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: Returns the vehicle model enum for the specified story character
FUNC MODEL_NAMES GET_NPC_VEH_MODEL(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        PED_VEH_DATA_STRUCT sData
        GET_NPC_VEH_DATA(ePed, sData, eTypePreference)
        RETURN sData.model
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("GET_NPC_VEH_MODEL - Ped is not a story character.")
                PRINTSTRING("\nGET_NPC_VEH_MODEL - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    
    RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE: Makes a request for the appropriate story ped model
PROC REQUEST_NPC_PED_MODEL(enumCharacterList ePed)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_NPC_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_NPC_PED_MODEL - Ped is not a story character.")
                PRINTSTRING("\nREQUEST_NPC_PED_MODEL - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Checks to see if the model for the specified ped has been loaded
FUNC BOOL HAS_NPC_PED_MODEL_LOADED(enumCharacterList ePed)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_NPC_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_NPC_PED_MODEL_LOADED - Ped is not a story character.")
                PRINTSTRING("\nHAS_NPC_PED_MODEL_LOADED - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

/// PURPOSE: Marks the specified story peds model as no longer needed
PROC SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(enumCharacterList ePed)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(ePed))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED - Ped is not a story character.")
                PRINTSTRING("\nSET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Returns the story ped enum based on the specified peds model
FUNC enumCharacterList GET_NPC_PED_ENUM(PED_INDEX ped)
    IF DOES_ENTITY_EXIST(ped)
        INT i
        MODEL_NAMES ePedModel = GET_ENTITY_MODEL(ped)
		INT iMaxCharacter = ENUM_TO_INT(GLOBAL_CHARACTER_SHEET_GET_MAX_CHARACTERS_FOR_GAMEMODE()) - 1
        FOR i = NUM_OF_PLAYABLE_PEDS TO iMaxCharacter
            IF GET_NPC_PED_MODEL(INT_TO_ENUM(enumCharacterList, i)) = ePedModel
                RETURN (INT_TO_ENUM(enumCharacterList, i))
            ENDIF
        ENDFOR
    ENDIF
    
    RETURN NO_CHARACTER
ENDFUNC

/// PURPOSE: Makes a request for the appropriate story peds vehicle model
PROC REQUEST_NPC_VEH_MODEL(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        REQUEST_MODEL(GET_NPC_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("REQUEST_NPC_VEH_MODEL - Ped is not a story character.")
                PRINTSTRING("\nREQUEST_NPC_VEH_MODEL - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Checks to see if the model for the specified story peds vehicle has been loaded
FUNC BOOL HAS_NPC_VEH_MODEL_LOADED(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        RETURN HAS_MODEL_LOADED(GET_NPC_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("HAS_NPC_VEH_MODEL_LOADED - Ped is not a story character.")
                PRINTSTRING("\nHAS_NPC_VEH_MODEL_LOADED - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

/// PURPOSE: Marks the specified story peds vehicle model as no longer needed
PROC SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_VEH_MODEL(ePed, eTypePreference))
    ELSE
        IF ePed <> NO_CHARACTER
            #IF IS_DEBUG_BUILD
                SCRIPT_ASSERT("SET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED - Ped is not a story character.")
                PRINTSTRING("\nSET_NPC_VEH_MODEL_AS_NO_LONGER_NEEDED - Ped is not a story character.")PRINTNL()
            #ENDIF
        ENDIF
    ENDIF
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PED CHECKS                                                                                                                                  ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns true if the player is using the clothes shop or wardrobe to change clothes.
FUNC BOOL IS_PLAYER_CHANGING_CLOTHES()
	RETURN g_bPlayerIsChangingClothes
ENDFUNC

/// PURPOSE: Updates the global which states if the player is currently changing their clothes
PROC SET_PLAYER_IS_CHANGING_CLOTHES(BOOL bChangingClothes)

	PRINTLN("SET_PLAYER_IS_CHANGING_CLOTHES(", bChangingClothes, ") called by ", GET_THIS_SCRIPT_NAME())
	
	IF NOT bChangingClothes AND g_bPlayerIsChangingClothes
		PRINTLN("...telling mission triggerer to refresh any lead-in scenes at trigger locations.")
		g_bCleanupTriggerScene = TRUE 
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			Start_Export_Player_Headshot_Timer()
		ENDIF
	ENDIF
	
	g_bPlayerIsChangingClothes = bChangingClothes
ENDPROC

PROC SET_PLAYER_HAS_JUST_CHANGED_CLOTHES()
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed] = GET_CURRENT_TIMEOFDAY()
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed])
			#ENDIF
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed] = GET_CURRENT_TIMEOFDAY()
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed])
			#ENDIF
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed] = GET_CURRENT_TIMEOFDAY()
			TEXT_LABEL_63 tLastChangedOutfits
			#IF IS_DEBUG_BUILD
			tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(g_savedGlobals.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed])
			#ENDIF
		#endif
		#endif
		
		PRINTLN("g_sLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePed), "] set to ", tLastChangedOutfits)
	ELSE
		PRINTLN("g_sLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePed), "] invalid.")
	ENDIF
ENDPROC
FUNC TIMEOFDAY GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(enumCharacterList ePed)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
			RETURN g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed]
		#endif	
		#if USE_NRM_DLC
			RETURN g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed]
		#endif	
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			RETURN g_savedGlobals.sPlayerData.sInfo.sLastTimeWeChangedClothes[ePed]
		#endif	
		#endif
	ENDIF
	RETURN INVALID_TIMEOFDAY
ENDFUNC

CONST_INT iCONST_RealtimeHoursToWaitToChangeClothes		10
FUNC BOOL HAVE_REALTIME_HOURS_PASSED_SINCE_PED_LAST_CHANGED_CLOTHES(enumCharacterList ePed, INT iRealtimeHoursToWaitToChangeClothes = iCONST_RealtimeHoursToWaitToChangeClothes)
	
	IF iRealtimeHoursToWaitToChangeClothes > 0
	
		TIMEOFDAY sLastChangedOutfits = GET_TIME_PLAYER_PED_LAST_CHANGED_CLOTHES(ePed)
		IF Is_TIMEOFDAY_Valid(sLastChangedOutfits)
			
			#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 tLastChangedOutfits = TIMEOFDAY_TO_TEXT_LABEL(sLastChangedOutfits)
			
			INT iSeconds, iMinutes, iHours, iDays, iMonths, iYears
			GET_DIFFERENCE_BETWEEN_NOW_AND_TIMEOFDAY(sLastChangedOutfits, iSeconds, iMinutes, iHours, iDays, iMonths, iYears)
			#ENDIF
			
			INT iGameHoursToWaitToChangeClothes = iRealtimeHoursToWaitToChangeClothes * 30
			IF NOT HasNumOfHoursPassedSincePedTimeStruct(sLastChangedOutfits, iGameHoursToWaitToChangeClothes)
		
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_PED_COMP,"dont change outfits - not enough time has passed since [",
						GET_PLAYER_PED_STRING(ePed),
						"] clothes changed [", tLastChangedOutfits, ", ",
						iMinutes, "m ", iHours, "h ",
						iDays, "d ", iMonths+(iYears*12), "m / ",
						iGameHoursToWaitToChangeClothes, "gamehrs]")
				#ENDIF
				
				RETURN FALSE
			ELSE
			
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_PED_COMP, "allow change outfits - g_iLastTimeWeChangedClothes[",
						GET_PLAYER_PED_STRING(ePed), "] enough time has passed [",
						tLastChangedOutfits, ", ",
						iMinutes, "m ", iHours, "h ",
						iDays, "d ", iMonths+(iYears*12), "m / ",
						iGameHoursToWaitToChangeClothes, "gamehrs]")
				#ENDIF
				
			ENDIF
		ELSE
			CPRINTLN(DEBUG_PED_COMP, "allow change outfits - g_iLastTimeWeChangedClothes[", GET_PLAYER_PED_STRING(ePed), "] is null")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_PED_COMP, "allow change outfits - bypass hour check")
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PLAYER_HAS_JUST_CHANGED_HAIRDO()
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed] = GET_GAME_TIMER()		
			PRINTLN("<CLF>g_iLastTimeWeChangedHairdo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed])
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed] = GET_GAME_TIMER()		
			PRINTLN("<NRM>g_iLastTimeWeChangedHairdo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed])
		#endif
		#if not USE_CLF_DLC
		#if not use_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed] = GET_GAME_TIMER()		
			PRINTLN("g_iLastTimeWeChangedHairdo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobals.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed])
		#endif		
		#endif
	ELSE
		PRINTLN("g_iLastTimeWeChangedHairdo[", GET_PLAYER_PED_STRING(ePed), "] invalid.")
	ENDIF
ENDPROC
FUNC INT GET_TIME_PLAYER_PED_LAST_CHANGED_HAIRDO(enumCharacterList ePed)
	IF IS_PLAYER_PED_PLAYABLE(ePed)		
		#if USE_CLF_DLC
			RETURN g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed]
		#endif	
		#if USE_NRM_DLC
			RETURN g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed]
		#endif	
		#if not USE_CLF_DLC
		#if not use_NRM_DLC
			RETURN g_savedGlobals.sPlayerData.sInfo.iLastTimeWeChangedHairdo[ePed]
		#endif
		#endif
	ENDIF
	RETURN -1
ENDFUNC

PROC SET_PLAYER_HAS_JUST_GOT_TATTOO()
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed] = GET_GAME_TIMER()		
			PRINTLN("<CLF>g_iLastTimeWeGotTattoo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed])		
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed] = GET_GAME_TIMER()		
			PRINTLN("<NRM>g_iLastTimeWeGotTattoo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed])		
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed] = GET_GAME_TIMER()		
			PRINTLN("g_iLastTimeWeGotTattoo[", GET_PLAYER_PED_STRING(ePed), "] set to ", g_savedGlobals.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed])
		#endif		
		#endif
	ELSE
		PRINTLN("g_iLastTimeWeGotTattoo[", GET_PLAYER_PED_STRING(ePed), "] invalid.")
	ENDIF
ENDPROC
FUNC INT GET_TIME_PLAYER_PED_LAST_GOT_TATTOO(enumCharacterList ePed)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		
		#if USE_CLF_DLC
			RETURN g_savedGlobalsClifford.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed]
		#endif
		#if USE_NRM_DLC
			RETURN g_savedGlobalsnorman.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed]
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			RETURN g_savedGlobals.sPlayerData.sInfo.iLastTimeWeGotTattoo[ePed]
		#endif
		#endif
	
	ENDIF
	RETURN -1
ENDFUNC

/// PURPOSE: Returns TRUE if the default ped info has been set for each player character
FUNC BOOL HAS_DEFAULT_INFO_BEEN_SET()
	#if USE_CLF_DLC
	 	RETURN g_savedGlobalsClifford.sPlayerData.sInfo.bDefaultInfoSet
	#endif
	#if USE_NRM_DLC
	 	RETURN g_savedGlobalsnorman.sPlayerData.sInfo.bDefaultInfoSet
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sPlayerData.sInfo.bDefaultInfoSet
	#endif   
	#endif
ENDFUNC

/// PURPOSE: Sets the global flag that states if a player character is currently available
PROC SET_PLAYER_PED_AVAILABLE(enumCharacterList ePed, BOOL bAvailable)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		#if USE_CLF_DLC
		 	g_savedGlobalsClifford.sPlayerData.sInfo.bPedAvailable[ePed] = bAvailable
		#endif
		#if USE_NRM_DLC
		 	g_savedGlobalsnorman.sPlayerData.sInfo.bPedAvailable[ePed] = bAvailable
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_SavedGlobals.sPlayerData.sInfo.bPedAvailable[ePed] = bAvailable
		#endif
		#endif
    ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if the specified player is available at this point in the game
FUNC BOOL IS_PLAYER_PED_AVAILABLE(enumCharacterList ePed)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#if USE_CLF_DLC
		 	IF g_savedGlobalsClifford.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE() // this is also needed for repeat play
	        	RETURN g_savedGlobalsClifford.sPlayerData.sInfo.bPedAvailable[ePed]
			ELSE
				RETURN TRUE
			ENDIF
		#endif
		#if USE_NRM_DLC
		 	IF g_savedGlobalsnorman.sFlow.isGameflowActive
			OR IS_REPEAT_PLAY_ACTIVE() // this is also needed for repeat play
	        	RETURN g_savedGlobalsnorman.sPlayerData.sInfo.bPedAvailable[ePed]
			ELSE
				RETURN TRUE
			ENDIF
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF g_savedGlobals.sFlow.isGameflowActive
			OR g_bMagDemoActive
			OR IS_REPEAT_PLAY_ACTIVE() // this is also needed for repeat play
	        	RETURN g_SavedGlobals.sPlayerData.sInfo.bPedAvailable[ePed]
			ELSE
				RETURN TRUE
			ENDIF
		#endif	
		#endif
    ELSE
        RETURN FALSE
    ENDIF
ENDFUNC

/// PURPOSE: Sets the current ped global enum based on the player model
///    NOTE: If this proc gets called too many times per frame then perhaps we should consider another method...
PROC UPDATE_CURRENT_PLAYER_PED_ENUMCLF()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())	
        IF GET_PLAYER_PED_MODELCLF(g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed) <> GET_ENTITY_MODEL(PLAYER_PED_ID())
            enumCharacterList ePed = GET_PLAYER_PED_ENUMCLF(PLAYER_PED_ID())
            IF IS_PLAYER_PED_PLAYABLE(ePed)
			
				// Keep track of the previous player ped
				IF g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed <> ePed
				AND IS_PLAYER_PED_PLAYABLE(g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed)
					g_savedGlobalsClifford.sPlayerData.sInfo.ePreviousPed = g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed
				ENDIF
				
                g_savedGlobalsClifford.sPlayerData.sInfo.eLastKnownPed = ePed
                g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed = ePed
                EXIT
            ENDIF
        ELSE
            g_savedGlobalsClifford.sPlayerData.sInfo.eLastKnownPed = g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed
            EXIT
        ENDIF
    ENDIF
    
    // Ped model not player ped
    g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed = NO_CHARACTER

ENDPROC
PROC UPDATE_CURRENT_PLAYER_PED_ENUMNRM()


	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())	
        IF GET_PLAYER_PED_MODELNRM(g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed) <> GET_ENTITY_MODEL(PLAYER_PED_ID())
            enumCharacterList ePed = GET_PLAYER_PED_ENUMNRM(PLAYER_PED_ID())
            IF IS_PLAYER_PED_PLAYABLE(ePed)
			
				// Keep track of the previous player ped
				IF g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed <> ePed
				AND IS_PLAYER_PED_PLAYABLE(g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed)
					g_savedGlobalsnorman.sPlayerData.sInfo.ePreviousPed = g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed
				ENDIF
				
                g_savedGlobalsnorman.sPlayerData.sInfo.eLastKnownPed = ePed
                g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed = ePed
                EXIT
            ENDIF
        ELSE
            g_savedGlobalsnorman.sPlayerData.sInfo.eLastKnownPed = g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed
            EXIT
        ENDIF
    ENDIF
    
    // Ped model not player ped
    g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed = NO_CHARACTER

ENDPROC
PROC UPDATE_CURRENT_PLAYER_PED_ENUM()

#if USE_CLF_DLC
	if g_bLoadedClifford
		UPDATE_CURRENT_PLAYER_PED_ENUMCLF()
		EXIT
	endif
#endif
#if USE_NRM_DLC
	if g_bLoadedNorman
		UPDATE_CURRENT_PLAYER_PED_ENUMNRM()
		EXIT
	endif
#endif

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())	
	    IF GET_PLAYER_PED_MODEL(g_savedGlobals.sPlayerData.sInfo.eCurrentPed) <> GET_ENTITY_MODEL(PLAYER_PED_ID())
	        enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	        IF IS_PLAYER_PED_PLAYABLE(ePed)
			#IF FEATURE_SP_DLC_DIRECTOR_MODE
			AND (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR) OR g_bDirectorForceSaveData)
			#ENDIF
			
				// Keep track of the previous player ped
				IF g_savedGlobals.sPlayerData.sInfo.eCurrentPed <> ePed
				AND IS_PLAYER_PED_PLAYABLE(g_savedGlobals.sPlayerData.sInfo.eCurrentPed)
					g_savedGlobals.sPlayerData.sInfo.ePreviousPed = g_savedGlobals.sPlayerData.sInfo.eCurrentPed
				ENDIF
				
	            g_savedGlobals.sPlayerData.sInfo.eLastKnownPed = ePed
	            g_savedGlobals.sPlayerData.sInfo.eCurrentPed = ePed
	            EXIT
	        ENDIF
	    ELSE
			IF g_savedGlobals.sPlayerData.sInfo.eCurrentPed <> NO_CHARACTER
	        	g_savedGlobals.sPlayerData.sInfo.eLastKnownPed = g_savedGlobals.sPlayerData.sInfo.eCurrentPed
			ENDIF
			EXIT
	    ENDIF
	ENDIF

	// Ped model not player ped
	g_savedGlobals.sPlayerData.sInfo.eCurrentPed = NO_CHARACTER

    	
ENDPROC

/// PURPOSE: Sets the front end blip name for the current player
PROC UPDATE_PLAYER_PED_BLIP_NAME()

	UPDATE_CURRENT_PLAYER_PED_ENUM()
	
	BLIP_INDEX playerBlip = GET_MAIN_PLAYER_BLIP_ID()
	IF DOES_BLIP_EXIST(playerBlip)
		#if USE_CLF_DLC
			IF g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed = CHAR_MICHAEL
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_MICHAEL")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_MICHAEL")
			ELIF g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed = CHAR_FRANKLIN
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_FRANKLIN")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_FRANKLIN")
			ELIF g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed = CHAR_TREVOR
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_TREV")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_TREV")
			ELSE
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_PLAYER")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_PLAYER")
			ENDIF
		#endif
		#if USE_NRM_DLC
			IF g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed = CHAR_MICHAEL
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_MICHAEL")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_MICHAEL")
			ELIF g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed = CHAR_NRM_JIMMY
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_FRANKLIN")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_FRANKLIN")
			ELIF g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed = CHAR_NRM_TRACEY
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_TREV")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_TREV")
			ELSE
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_PLAYER")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_PLAYER")
			ENDIF
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			enumCharacterList eCurrentChar = g_savedGlobals.sPlayerData.sInfo.eCurrentPed
			
			//B* 2218640: For Director mode, consider the last player ped
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				eCurrentChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
			ENDIF
			
			IF  eCurrentChar = CHAR_MICHAEL
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_MICHAEL")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_MICHAEL")
			ELIF eCurrentChar = CHAR_FRANKLIN
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_FRANKLIN")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_FRANKLIN")
			ELIF eCurrentChar = CHAR_TREVOR
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_TREV")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_TREV")
			ELSE
				PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - setting blip name to BLIP_PLAYER")
				SET_BLIP_NAME_FROM_TEXT_FILE(playerBlip, "BLIP_PLAYER")
			ENDIF
		#endif	
		#endif
	ELSE
		PRINTLN("UPDATE_PLAYER_PED_BLIP_NAME - failed to set blip name")
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("UPDATE_PLAYER_PED_BLIP_NAME: GET_MAIN_PLAYER_BLIP_ID() returned invalud blip ID. Tell Kenneth R.")
		#ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TREVOR_BIKE_GANG_STATUS(enumCharacterList ePed)
	
	//RELATIONSHIP_TYPE OldRel = GET_RELATIONSHIP_BETWEEN_GROUPS(RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
	IF ePed = CHAR_TREVOR
		IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE()
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
		ELSE
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)					
		ENDIF
	ELSE
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
	ENDIF

ENDPROC


/// PURPOSE: Configures any ambient relationships that differ between playable characters.
PROC UPDATE_PLAYER_CHARACTER_RELATIONSHIPS(enumCharacterList ePlayerChar)

	SWITCH ePlayerChar
		CASE CHAR_MICHAEL
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_LOST, RELGROUPHASH_PLAYER)
			
			//Jimmy, Tracey, Amanda, Maid, Gardener
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_M, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_M)
			
			//Denise, Lamar Stretch
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_F, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_F)
			
			//Ron, Patricia, Floyd
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_T, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_T)
		BREAK
		
		CASE CHAR_FRANKLIN
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_LOST, RELGROUPHASH_PLAYER)
			
			//Jimmy, Tracey, Amanda, Maid, Gardener
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_M, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_M)
			
			//Denise, Lamar Stretch
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_F, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_F)
			
			//Ron, Patricia, Floyd
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_T, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_T)
		BREAK
		
		CASE CHAR_TREVOR
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_AMBIENT_GANG_FAMILY, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, RELGROUPHASH_AMBIENT_GANG_LOST)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_GANG_LOST, RELGROUPHASH_PLAYER)
			
			//Jimmy, Tracey, Amanda, Maid, Gardener
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_M, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_M)
			
			//Denise, Lamar Stretch
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_F, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_F)
			
			//Ron, Patricia, Floyd
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_FAMILY_T, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_FAMILY_T)
		BREAK
		
		DEFAULT
			SCRIPT_ASSERT("SETUP_CHARACTER_SPECIFIC_PLAYER_RELATIONSHIPS: Player char enum was not a playable character!")
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE: Returns the name of the player characters timecycle modifier
FUNC STRING GET_PLAYER_PED_TIMECYCLE_MODIFIER(enumCharacterList ePed)
	ePed = ePed
	
	RETURN ""			//#1570413
//  ##  //  ##  //  ##  //
//	IF ePed = CHAR_MICHAEL
//		RETURN "micheal"
//	ELIF ePed = CHAR_FRANKLIN
//		RETURN "micheal" // Asked to use Michaels for now
//		//RETURN "franklin"
//	ELIF ePed = CHAR_TREVOR
//		RETURN "micheal" // Asked to use Michaels for now
//		//RETURN "trevor"
//	ENDIF
//	
//	RETURN ""
ENDFUNC


PROC UPDATE_PLAYER_PED_TIMECYCLE_MODIFIER()
	IF g_bUseCharacterFilters
		UPDATE_CURRENT_PLAYER_PED_ENUM()
		#if USE_CLF_DLC
		SET_NEXT_PLAYER_TCMODIFIER(GET_PLAYER_PED_TIMECYCLE_MODIFIER(g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed))		
		#endif
		#if USE_NRM_DLC
		SET_NEXT_PLAYER_TCMODIFIER(GET_PLAYER_PED_TIMECYCLE_MODIFIER(g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed))		
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
		SET_NEXT_PLAYER_TCMODIFIER(GET_PLAYER_PED_TIMECYCLE_MODIFIER(g_savedGlobals.sPlayerData.sInfo.eCurrentPed))		
		#endif
		#endif
	ELSE
		SET_NEXT_PLAYER_TCMODIFIER("")
	ENDIF
ENDPROC


/// PURPOSE: Returns TRUE if the specified ped enum matches the current player ped enum
FUNC BOOL IS_PED_THE_CURRENT_PLAYER_PED(enumCharacterList ePed)
   
	#if USE_CLF_DLC
		if g_bLoadedClifford
			UPDATE_CURRENT_PLAYER_PED_ENUMCLF()
			RETURN (ePed = g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed)		
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			UPDATE_CURRENT_PLAYER_PED_ENUMNRM()
			RETURN (ePed = g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed)		
		endif
	#endif
	UPDATE_CURRENT_PLAYER_PED_ENUM()
	RETURN (ePed = g_savedGlobals.sPlayerData.sInfo.eCurrentPed)
	
ENDFUNC

/// PURPOSE: Gets the ENUM value of the current ped in control
FUNC enumCharacterList GET_CURRENT_PLAYER_PED_ENUMCLF()
    UPDATE_CURRENT_PLAYER_PED_ENUMCLF()
	RETURN g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed
	
ENDFUNC
FUNC enumCharacterList GET_CURRENT_PLAYER_PED_ENUMNRM()
    UPDATE_CURRENT_PLAYER_PED_ENUMNRM()	
	RETURN g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed	
ENDFUNC
FUNC enumCharacterList GET_CURRENT_PLAYER_PED_ENUM()
    	
	#if USE_CLF_DLC
		if g_bLoadedClifford
			return GET_CURRENT_PLAYER_PED_ENUMCLF()
		endif
	#endif
	#if  USE_NRM_DLC
		if g_bLoadedNorman
			return GET_CURRENT_PLAYER_PED_ENUMNRM()
		endif
	#endif
	
	UPDATE_CURRENT_PLAYER_PED_ENUM()
	RETURN g_savedGlobals.sPlayerData.sInfo.eCurrentPed
	
ENDFUNC

/// PURPOSE: Gets the ENUM value of the last know player ped in control
FUNC enumCharacterList GET_LAST_KNOWN_PLAYER_PED_ENUM()
	#if USE_CLF_DLC
		RETURN g_savedGlobalsClifford.sPlayerData.sInfo.eLastKnownPed	
	#endif
	#if USE_NRM_DLC
		RETURN g_savedGlobalsnorman.sPlayerData.sInfo.eLastKnownPed	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sPlayerData.sInfo.eLastKnownPed
	#endif  
	#endif
ENDFUNC

/// PURPOSE: Gets the ENUM value of the previous player ped in control
FUNC enumCharacterList GET_PREVIOUS_PLAYER_PED_ENUM()
    
	#if USE_CLF_DLC
		UPDATE_CURRENT_PLAYER_PED_ENUMCLF()
		RETURN g_savedGlobalsClifford.sPlayerData.sInfo.ePreviousPed
	#endif
	#if USE_NRM_DLC
		UPDATE_CURRENT_PLAYER_PED_ENUMNRM()
		RETURN g_savedGlobalsnorman.sPlayerData.sInfo.ePreviousPed
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		UPDATE_CURRENT_PLAYER_PED_ENUM()
		RETURN g_savedGlobals.sPlayerData.sInfo.ePreviousPed
	#endif  	
	#endif
ENDFUNC

/// PURPOSE: Gets the INT value of the current ped in control
FUNC INT GET_CURRENT_PLAYER_PED_INT()
    UPDATE_CURRENT_PLAYER_PED_ENUM()
	#if USE_CLF_DLC
		RETURN ENUM_TO_INT(g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed)
	#endif
	#if USE_NRM_DLC
		RETURN ENUM_TO_INT(g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed)
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN ENUM_TO_INT(g_savedGlobals.sPlayerData.sInfo.eCurrentPed)
	#endif  
	#endif
    
ENDFUNC

/// PURPOSE: Get a constant int value that represents the bitset BIT for the current ped in control.
FUNC INT GET_CURRENT_PLAYER_PED_BIT()
	UPDATE_CURRENT_PLAYER_PED_ENUM()
	
	#if USE_CLF_DLC
		SWITCH g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed
		CASE CHAR_MICHAEL
			RETURN BIT_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			RETURN BIT_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
			RETURN BIT_TREVOR
		BREAK
	ENDSWITCH	
	#endif
	#if USE_NRM_DLC
		SWITCH g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed
		CASE CHAR_MICHAEL
			RETURN BIT_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			RETURN BIT_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
			RETURN BIT_TREVOR
		BREAK
	ENDSWITCH	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		SWITCH g_savedGlobals.sPlayerData.sInfo.eCurrentPed
		CASE CHAR_MICHAEL
			RETURN BIT_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			RETURN BIT_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
			RETURN BIT_TREVOR
		BREAK
	ENDSWITCH
	#endif 
	#endif
		
	SCRIPT_ASSERT("GET_CURRENT_PLAYER_PED_BIT: Invalid current player ped ENUM. Could not return a valid bit")
	RETURN BIT_NOBODY
ENDFUNC

FUNC INT GET_PLAYER_PED_BIT(enumCharacterList ePlayerEnum)
	SWITCH ePlayerEnum
		CASE CHAR_MICHAEL
			RETURN BIT_MICHAEL
		BREAK
		CASE CHAR_FRANKLIN
			RETURN BIT_FRANKLIN
		BREAK
		CASE CHAR_TREVOR
			RETURN BIT_TREVOR
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("GET_PLAYER_PED_BIT: Enum passed was not for a valid player charcter. Could not return a valid result.")
	RETURN BIT_NOBODY
ENDFUNC

#IF IS_DEBUG_BUILD

	/// PURPOSE: Gets the STRING value of the current ped in control
	FUNC STRING GET_CURRENT_PLAYER_PED_STRING()
		UPDATE_CURRENT_PLAYER_PED_ENUM()
		#if USE_CLF_DLC
			RETURN GET_PLAYER_PED_STRING(g_savedGlobalsClifford.sPlayerData.sInfo.eCurrentPed)
		#endif
		#if USE_NRM_DLC
			RETURN GET_PLAYER_PED_STRING(g_savedGlobalsnorman.sPlayerData.sInfo.eCurrentPed)
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			RETURN GET_PLAYER_PED_STRING(g_savedGlobals.sPlayerData.sInfo.eCurrentPed)
		#endif 		
		#endif
	ENDFUNC
	
	PROC PRINT_PED_VARIATIONS(PED_INDEX ped)
		IF NOT IS_PED_INJURED(ped)
			PRINTLN("VARIATIONS for ", GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM(ped)))
				PRINTLN("...PED_COMP_HEAD     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_HEAD), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_HEAD), "]")
				PRINTLN("...PED_COMP_BERD     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_BERD), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_BERD), "]")
				PRINTLN("...PED_COMP_HAIR     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_HAIR), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_HAIR), "]")
				PRINTLN("...PED_COMP_TORSO    [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_TORSO), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_TORSO), "]")
				PRINTLN("...PED_COMP_LEG      [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_LEG), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_LEG), "]")
				PRINTLN("...PED_COMP_HAND     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_HAND), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_HAND), "]")
				PRINTLN("...PED_COMP_FEET     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_FEET), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_FEET), "]")
				PRINTLN("...PED_COMP_TEETH    [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_TEETH), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_TEETH), "]")
				PRINTLN("...PED_COMP_SPECIAL  [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_SPECIAL), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_SPECIAL), "]")
				PRINTLN("...PED_COMP_SPECIAL2 [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_SPECIAL2), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_SPECIAL2), "]")
				PRINTLN("...PED_COMP_DECL     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_DECL), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_DECL), "]")
				PRINTLN("...PED_COMP_JBIB     [", GET_PED_DRAWABLE_VARIATION(ped, PED_COMP_JBIB), ",", GET_PED_TEXTURE_VARIATION(ped, PED_COMP_JBIB), "]")
				PRINTNL()
				PRINTLN("...ANCHOR_HEAD       [", GET_PED_PROP_INDEX(ped, ANCHOR_HEAD), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_HEAD), "]")
				PRINTLN("...ANCHOR_EYES       [", GET_PED_PROP_INDEX(ped, ANCHOR_EYES), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_EYES), "]")
				PRINTLN("...ANCHOR_EARS       [", GET_PED_PROP_INDEX(ped, ANCHOR_EARS), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_EARS), "]")
				PRINTLN("...ANCHOR_MOUTH      [", GET_PED_PROP_INDEX(ped, ANCHOR_MOUTH), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_MOUTH), "]")
				PRINTLN("...ANCHOR_LEFT_HAND  [", GET_PED_PROP_INDEX(ped, ANCHOR_LEFT_HAND), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_LEFT_HAND), "]")
				PRINTLN("...ANCHOR_RIGHT_HAND [", GET_PED_PROP_INDEX(ped, ANCHOR_RIGHT_HAND), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_RIGHT_HAND), "]")
				PRINTLN("...ANCHOR_LEFT_WRIST [", GET_PED_PROP_INDEX(ped, ANCHOR_LEFT_WRIST), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_LEFT_WRIST), "]")
				PRINTLN("...ANCHOR_RIGHT_WRIST[", GET_PED_PROP_INDEX(ped, ANCHOR_RIGHT_WRIST), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_RIGHT_WRIST), "]")
				PRINTLN("...ANCHOR_HIP        [", GET_PED_PROP_INDEX(ped, ANCHOR_HIP), ",", GET_PED_PROP_TEXTURE_INDEX(ped, ANCHOR_HIP), "]")
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC
	
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PED REQUEST FUNCTIONS                                                                                                                       ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns TRUE if a request to switch the player character is currently in progress
FUNC BOOL IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN (g_sPlayerPedRequest.eState = PR_STATE_PROCESSING)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Cancel a request that has just been made and is still in the init stage
PROC CANCEL_PLAYER_PED_SWITCH_REQUEST()
	IF (g_sPlayerPedRequest.eState = PR_STATE_PROCESSING)
	AND (g_sPlayerPedRequest.iStage = 0)
		g_sPlayerPedRequest.eState = PR_STATE_WAITING
		#IF IS_DEBUG_BUILD
			PRINTLN("CANCEL_PLAYER_PED_SWITCH_REQUEST(), ", GET_THIS_SCRIPT_NAME())
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Make a request to change the current player character
FUNC BOOL MAKE_PLAYER_PED_SWITCH_REQUEST(enumCharacterList ePed, BOOL MakePedInvincible = TRUE )	//, PED_REQUEST_TYPE_ENUM eType)
        
    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
    AND IS_PLAYER_PED_PLAYABLE(ePed)
    
        IF NOT IS_PED_THE_CURRENT_PLAYER_PED(ePed)
        OR NETWORK_IS_GAME_IN_PROGRESS()
    
			g_sPlayerPedRequest.ePed    = ePed
		//	g_sPlayerPedRequest.eType   = eType
			g_sPlayerPedRequest.iStage  = 0
			g_sPlayerPedRequest.eState  = PR_STATE_PROCESSING
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF MakePedInvincible 
					SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				ENDIF
				CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5.0)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("MAKE_PLAYER_PED_SWITCH_REQUEST(", GET_PLAYER_PED_STRING(ePed), "), ", GET_THIS_SCRIPT_NAME())
			#ENDIF
            
            RETURN TRUE
        ENDIF
        
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

/// PURPOSE: Make a request to change the current player character when MP fails
///    NOTE: This shoul donly be called when in MP or when kicked out of MP.
FUNC BOOL MAKE_PLAYER_PED_SWITCH_REQUEST_FROM_MP_KICK(enumCharacterList ePed)	//, PED_REQUEST_TYPE_ENUM eType)

    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
    AND IS_PLAYER_PED_PLAYABLE(ePed)
	
		g_sPlayerPedRequest.ePed    = ePed
	//	g_sPlayerPedRequest.eType   = eType
		g_sPlayerPedRequest.iStage  = 0
		g_sPlayerPedRequest.eState  = PR_STATE_PROCESSING
		
		#IF IS_DEBUG_BUILD
			PRINTLN("MAKE_PLAYER_PED_SWITCH_REQUEST_FROM_MP_KICK(", GET_PLAYER_PED_STRING(ePed), "), ", GET_THIS_SCRIPT_NAME())
		#ENDIF
        
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

FUNC BOOL WILL_PLAYER_PED_SWITCH_HAPPEN(enumCharacterList ePed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
    AND NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN TRUE
	ENDIF
	RETURN FALSE
	
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    SAVED PED DATA                                                                                                                              ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF IS_DEBUG_BUILD
USING "player_ped_debug.sch"		//for SaveNewGameToSwitchLog()
#ENDIF

/// PURPOSE: Used to store the players ped ID so the player controller can query ped state during cutscenes
PROC STORE_TEMP_PLAYER_PED_ID(PED_INDEX pedID)

	IF IS_PED_INJURED(pedID)
		EXIT
	ENDIF
	
	PRINTLN("STORE_TEMP_PLAYER_PED_ID()")
	PRINTLN("...pedID = ", NATIVE_TO_INT(pedID))
	#IF IS_DEBUG_BUILD
		PRINTLN("...model = ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(pedID)))
	#ENDIF
	
	INT i
	REPEAT NUM_PLAYER_PED_IDS i
		// Dont store if it's already in the list
		IF DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[i])
		AND g_piCreatedPlayerPedIDs[i] = pedID
			PRINTLN("...slot = ", i)
			EXIT
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_PLAYER_PED_IDS i
		IF NOT DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[i])
			g_piCreatedPlayerPedIDs[i] = pedID
			PRINTLN("...slot = ", i)
			EXIT
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("More than 9 player peds exist! Tell Kenneth R.")
	#ENDIF
	PRINTLN("More than 9 player peds exist, unable to store temp ped ID")
ENDPROC

FUNC INT GET_NUMBER_OF_SCRIPT_CREATED_PLAYER_VEHICLES(enumCharacterList ePed, MODEL_NAMES eModelCheck = DUMMY_MODEL_FOR_SCRIPT)
	
	INT iCount = 0
	
	INT iVeh
	REPEAT NUM_PLAYER_VEHICLE_IDS iVeh
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[iVeh])
		AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[iVeh])
			IF g_eCreatedPlayerVehiclePed[iVeh] = ePed
				IF eModelCheck = DUMMY_MODEL_FOR_SCRIPT
				OR eModelCheck = GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[iVeh])
					iCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

/// PURPOSE: Used to store the players vehicle ID so the vehicle gen controller can query vehicle state
PROC STORE_TEMP_PLAYER_VEHICLE_ID(VEHICLE_INDEX vehID, enumCharacterList ePed)
	INT i
	REPEAT NUM_PLAYER_VEHICLE_IDS i
		IF NOT DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			g_viCreatedPlayerVehicleIDs[i] = vehID
			g_eCreatedPlayerVehiclePed[i] = ePed
			g_eCreatedPlayerVehicleModel[i] = GET_ENTITY_MODEL(vehID)
			
			IF IS_THIS_MODEL_A_CAR(g_eCreatedPlayerVehicleModel[i])
				g_iCreatedPlayerVehicleCleanupTimer[ePed][SAVED_VEHICLE_SLOT_CAR] = -1
			ELSE
				g_iCreatedPlayerVehicleCleanupTimer[ePed][SAVED_VEHICLE_SLOT_BIKE] = -1
			ENDIF
			
			PRINTLN("Storing temp player vehicleID ", NATIVE_TO_INT(vehID), " in slot ", i)
			i = NUM_PLAYER_VEHICLE_IDS // Bail out of repeat
		ENDIF
		IF i = NUM_PLAYER_VEHICLE_IDS-1
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("More than 9 player vehicles exist! Tell Kenneth R.")
			#ENDIF
			PRINTLN("More than 9 player vehicles exist, unable to store temp vehicle ID")
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Used to delete all vehicles that have been created through the player ped procs.
///    NOTE: Use ePed = NO_CHARACTER to delete all characters vehicles
PROC DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(enumCharacterList ePed = NO_CHARACTER, VEHICLE_CREATE_TYPE_ENUM eVehicleType = VEHICLE_TYPE_DEFAULT)
	INT i
	REPEAT NUM_PLAYER_VEHICLE_IDS i
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[i])
			IF ePed = NO_CHARACTER
			OR g_eCreatedPlayerVehiclePed[i] = ePed
				IF eVehicleType = VEHICLE_TYPE_DEFAULT
				OR GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[i]) = GET_PLAYER_VEH_MODEL(ePed, eVehicleType)
					#IF USE_TU_CHANGES
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_viCreatedPlayerVehicleIDs[i])
					#ENDIF
					
						PRINTLN("Deleting player vehicleID ", NATIVE_TO_INT(g_viCreatedPlayerVehicleIDs[i]), " in slot ", i)
						SET_ENTITY_AS_MISSION_ENTITY(g_viCreatedPlayerVehicleIDs[i], FALSE, TRUE)
						DELETE_VEHICLE(g_viCreatedPlayerVehicleIDs[i])
						g_eCreatedPlayerVehiclePed[i] = NO_CHARACTER
					
					#IF USE_TU_CHANGES
					ELSE
						//SCRIPT_ASSERT("ignorable - ")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Returns TRUE if we have made a copy of the ped index for player character
FUNC BOOL IS_PED_IN_TEMP_PLAYER_PED_ID_LIST(PED_INDEX pedID)
	IF DOES_ENTITY_EXIST(pedID)
	AND NOT IS_PED_INJURED(pedID)
		INT iPed
		REPEAT NUM_PLAYER_PED_IDS iPed
			IF DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[iPed])
			AND NOT IS_PED_INJURED(g_piCreatedPlayerPedIDs[iPed])
				IF g_piCreatedPlayerPedIDs[iPed] = pedID
				AND GET_ENTITY_MODEL(g_piCreatedPlayerPedIDs[iPed]) = GET_ENTITY_MODEL(pedID)
				
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: Used to store the npc vehicle ID so the vehicle gen controller can query vehicle state
PROC STORE_TEMP_NPC_VEHICLE_ID(VEHICLE_INDEX vehID)
	INT i
	REPEAT NUM_NPC_VEHICLE_IDS i
		IF NOT DOES_ENTITY_EXIST(g_viCreatedNPCVehicleIDs[i])
			g_viCreatedNPCVehicleIDs[i] = vehID
			PRINTLN("Storing temp npc vehicleID ", NATIVE_TO_INT(vehID), " in slot ", i)
			i = NUM_NPC_VEHICLE_IDS // Bail out of repeat
		ENDIF
		IF i = NUM_NPC_VEHICLE_IDS-1
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("More than 3 npc vehicles exist! Tell Kenneth R.")
			#ENDIF
			PRINTLN("More than 3 npc vehicles exist, unable to store temp vehicle ID")
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Returns TRUE if the specified vehicle is one of the player character vehicles
///    NOTE: Set ePed = NO_CHARACTER to check all player characters vehicles
FUNC BOOL IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(VEHICLE_INDEX vehID, enumCharacterList ePed = NO_CHARACTER, VEHICLE_CREATE_TYPE_ENUM eVehicleType = VEHICLE_TYPE_DEFAULT)
	IF NOT DOES_ENTITY_EXIST(vehID)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(vehID)
		RETURN FALSE
	ENDIF

	INT iVeh
	REPEAT NUM_PLAYER_VEHICLE_IDS iVeh
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[iVeh])
			IF g_viCreatedPlayerVehicleIDs[iVeh] = vehID
				IF ePed = NO_CHARACTER
				OR ePed = g_eCreatedPlayerVehiclePed[iVeh]
					IF eVehicleType = VEHICLE_TYPE_DEFAULT
					OR GET_ENTITY_MODEL(vehID) = GET_PLAYER_VEH_MODEL(ePed, eVehicleType)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the vehicle data in the setup struct matches the player vehicle data
///    NOTE: This compares vehicle model and number plate.
FUNC BOOL DOES_VEH_SETUP_STRUCT_MATCH_PLAYER_VEHICLE(enumCharacterList ePed, VEHICLE_SETUP_STRUCT &sVehDataToCompare)
	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN FALSE
	ENDIF
	
	#if USE_CLF_DLC
		// Check stored CAR data
		IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF	
		// Check stored BIKE data
		IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF
	#endif
	#if USE_NRM_DLC
		// Check stored CAR data
		IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF	
		// Check stored BIKE data
		IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		// Check stored CAR data
		IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF	
		// Check stored BIKE data
		IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model != DUMMY_MODEL_FOR_SCRIPT
			IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehDataToCompare.eModel
			AND GET_HASH_KEY(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
				RETURN TRUE
			ENDIF
		ENDIF
	#endif 
	#endif
	
	
	// Check the default data
	// Grab the vehicle data for the specified ped
	PED_VEH_DATA_STRUCT sVehData
	GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_CAR)
	IF sVehData.model != DUMMY_MODEL_FOR_SCRIPT
		IF sVehData.model = sVehDataToCompare.eModel
		AND GET_HASH_KEY(sVehData.tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
			RETURN TRUE
		ENDIF
	ENDIF
	
	GET_PLAYER_VEH_DATA(ePed, sVehData, VEHICLE_TYPE_BIKE)
	IF sVehData.model != DUMMY_MODEL_FOR_SCRIPT
		IF sVehData.model = sVehDataToCompare.eModel
		AND GET_HASH_KEY(sVehData.tlNumberPlate) = GET_HASH_KEY(sVehDataToCompare.tlPlateText)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the specified player vehicle type is in the area.
///    NOTE: This compares vehicle model and number plate.
///    		 Set fRadius to -1 to check entire map.
FUNC BOOL IS_PLAYER_VEHICLE_IN_AREA(enumCharacterList ePed, VEHICLE_CREATE_TYPE_ENUM eVehicleType, VECTOR vCoord, FLOAT fRadius)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN FALSE
	ENDIF
	
	INT iVeh
	PED_VEH_DATA_STRUCT sTempVehData
	MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT
	
	GET_PLAYER_VEH_DATA(ePed, sTempVehData, eVehicleType)
	eModel = sTempVehData.model
	
	IF eModel != DUMMY_MODEL_FOR_SCRIPT
		// Repeat through g_viCreatedPlayerVehicleIDs[], checking if driveable and has same number plate and then within range.
		REPEAT NUM_PLAYER_VEHICLE_IDS iVeh
			IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[iVeh])
			AND IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[iVeh])
				IF GET_ENTITY_MODEL(g_viCreatedPlayerVehicleIDs[iVeh]) = eModel
				AND g_eCreatedPlayerVehiclePed[iVeh] = ePed
					IF fRadius = -1
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(g_viCreatedPlayerVehicleIDs[iVeh], FALSE), vCoord) <= fRadius
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns TRUE if the specified player is in one of their player created vehicles
FUNC BOOL IS_PLAYER_PED_IN_PERSONAL_VEHICLE(PED_INDEX pedID)

	IF IS_PED_INJURED(pedID)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedID)
		RETURN FALSE
	ENDIF
	
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN FALSE
	ENDIF
	
	// Repeat through g_viCreatedPlayerVehicleIDs[], checking if driveable and has same number plate
	INT iVeh
	REPEAT NUM_PLAYER_VEHICLE_IDS iVeh
		IF DOES_ENTITY_EXIST(g_viCreatedPlayerVehicleIDs[iVeh])
			IF IS_VEHICLE_DRIVEABLE(g_viCreatedPlayerVehicleIDs[iVeh])
				IF IS_PED_IN_VEHICLE(pedID, g_viCreatedPlayerVehicleIDs[iVeh])
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
PROC Initialise_PlayerSceneData_Global_Variables_On_StartupCLF()
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> Initialise_PlayerSceneData_Global_Variables_On_Startup()")
	
		INT iPeds
		REPEAT NUM_OF_PLAYABLE_PEDS iPeds
			g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[iPeds]				= PR_SCENE_INVALID			
			INT iQueue
			REPEAT g_iCOUNT_OF_LAST_SCENE_QUEUE iQueue
				g_savedGlobalsClifford.sPlayerSceneData.g_eLastSceneQueue[iPeds][iQueue]	= PR_SCENE_INVALID
			ENDREPEAT			
		ENDREPEAT		
		g_savedGlobalsClifford.sPlayerSceneData.g_iSeenOneOffSceneBit = 0
	
	
	#IF IS_DEBUG_BUILD
	SaveNewGameToSwitchLog()
	#ENDIF
	
ENDPROC
PROC Initialise_PlayerSceneData_Global_Variables_On_StartupNRM()
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> Initialise_PlayerSceneData_Global_Variables_On_Startup()")
	
		INT iPeds
		REPEAT NUM_OF_PLAYABLE_PEDS iPeds
			g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[iPeds]				= PR_SCENE_INVALID			
			INT iQueue
			REPEAT g_iCOUNT_OF_LAST_SCENE_QUEUE iQueue
				g_savedGlobalsnorman.sPlayerSceneData.g_eLastSceneQueue[iPeds][iQueue]	= PR_SCENE_INVALID
			ENDREPEAT			
		ENDREPEAT		
		g_savedGlobalsnorman.sPlayerSceneData.g_iSeenOneOffSceneBit = 0
	
	
	#IF IS_DEBUG_BUILD
	SaveNewGameToSwitchLog()
	#ENDIF
	
ENDPROC
PROC Initialise_PlayerSceneData_Global_Variables_On_Startup()
	#if USE_CLF_DLC
		if g_bLoadedClifford
			Initialise_PlayerSceneData_Global_Variables_On_StartupCLF()
			EXIT
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			Initialise_PlayerSceneData_Global_Variables_On_StartupNRM()
			EXIT
		endif
	#endif
	PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> Initialise_PlayerSceneData_Global_Variables_On_Startup()")
	
		INT iPeds
		REPEAT NUM_OF_PLAYABLE_PEDS iPeds
			g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[iPeds]				= PR_SCENE_INVALID			
			INT iQueue
			REPEAT g_iCOUNT_OF_LAST_SCENE_QUEUE iQueue
				g_SavedGlobals.sPlayerSceneData.g_eLastSceneQueue[iPeds][iQueue]	= PR_SCENE_INVALID
			ENDREPEAT			
		ENDREPEAT		
		g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit = 0
	
	
	#IF IS_DEBUG_BUILD
	SaveNewGameToSwitchLog()
	#ENDIF
	
ENDPROC




///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPON COMPONENTS UNLOCKED
///    
FUNC INT GET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
	
	INT iStatData
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC
PROC SET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, INT iStatData)
	
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_UNLOCK_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_COMP_UNLOCKED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET(ePed, eWeapon, eComponent, iStatData)
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_COMP_UNLOCKED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// All dlc and special weapons/components are unlocked by default
		IF IS_WEAPON_UNLOCKED_BY_DEFAULT(eWeapon)
			RETURN TRUE
		ENDIF
	
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_UNLOCKED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_ALL_PLAYER_PED_WEAPON_COMPS_UNLOCKED_FOR_WEAPON(enumCharacterList ePed, WEAPON_TYPE eWeapon)
	INT iWeaponComp = 0
	WEAPONCOMPONENT_TYPE eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eWeapon, iWeaponComp)
	WHILE eWeaponComp != WEAPONCOMPONENT_INVALID
		SET_PLAYER_PED_WEAPON_COMP_UNLOCKED(ePed, eWeapon, eWeaponComp, TRUE)
		iWeaponComp++
		eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eWeapon, iWeaponComp)
	ENDWHILE
ENDPROC

///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPON COMPONENTS VIEWED
///    
FUNC INT GET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
	
	INT iStatData
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_VIEW_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_VIEW_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_VIEW_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC
PROC SET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, INT iStatData)
	
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_VIEW_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_VIEW_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_VIEW_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_COMP_VIEWED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET(ePed, eWeapon, eComponent, iStatData)
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_COMP_VIEWED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_VIEWED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPON COMPONENTS PURCHASED
///    
FUNC INT GET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
	
	INT iStatData
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_ADDON_PURCH_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_ADDON_PURCH_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_ADDON_PURCH_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC

PROC SET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, INT iStatData)
	
	INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
	INT iBitset = GET_WEAPON_ADDON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_ADDON_PURCH_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_ADDON_PURCH_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_ADDON_PURCH_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_COMP_PURCHASED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET(ePed, eWeapon, eComponent, iStatData)
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_COMP_PURCHASED(enumCharacterList ePed, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eComponent)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_COMP_PURCHASED_BITSET(ePed, eWeapon, eComponent)
		INT iIndex = GET_WEAPON_ADDON_INDEX_FROM_WEAPONS(eComponent, eWeapon)
		INT iBit = GET_WEAPON_ADDON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC



///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPONS UNLOCKED
///    
FUNC INT GET_PLAYER_PED_WEAPON_UNLOCKED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon)	
	
	INT iStatData
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_UNLOCK_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_UNLOCK_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_UNLOCK_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_UNLOCK_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_UNLOCK_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_UNLOCK_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_UNLOCK_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_UNLOCK_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_UNLOCK_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_UNLOCKED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC
PROC SET_PLAYER_PED_WEAPON_UNLOCKED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, INT iStatData)
	
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_UNLOCK_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_UNLOCK_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_UNLOCK_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_UNLOCK_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_UNLOCK_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_UNLOCK_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_UNLOCK_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_UNLOCK_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_UNLOCK_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_UNLOCKED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_UNLOCKED(enumCharacterList ePed, WEAPON_TYPE eWeapon, BOOL bState = TRUE, BOOL bUnlockAllComponents = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_UNLOCKED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_UNLOCKED_BITSET(ePed, eWeapon, iStatData)
		
		IF bUnlockAllComponents
			SET_ALL_PLAYER_PED_WEAPON_COMPS_UNLOCKED_FOR_WEAPON(ePed, eWeapon)
		ENDIF
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_UNLOCKED(enumCharacterList ePed, WEAPON_TYPE eWeapon)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// All dlc and special weapons/components are unlocked by default
		IF IS_WEAPON_UNLOCKED_BY_DEFAULT(eWeapon)
			RETURN TRUE
		ENDIF
	
		INT iStatData = GET_PLAYER_PED_WEAPON_UNLOCKED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DLC_WEAPON_UNLOCKED_FOR_SHOPS(WEAPON_TYPE eWeapon)

	// DLC weapons now unlock in flow for NG game.
	SWITCH eWeapon
		CASE WEAPONTYPE_DLC_ASSAULTSMG
			RETURN g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_1].completed
		BREAK
	
		CASE WEAPONTYPE_DLC_HATCHET
			IF IS_LAST_GEN_PLAYER()
				RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_TREVOR_1].completed)
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE WEAPONTYPE_DLC_RAILGUN
			IF IS_LAST_GEN_PLAYER()
				RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_EXILE_1].completed)
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE WEAPONTYPE_DLC_BOTTLE
		CASE WEAPONTYPE_HAMMER
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_2].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_SNSPISTOL
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_2].completed OR g_savedGlobals.sFlow.missionSavedData[SP_MISSION_LAMAR].completed OR g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FAMILY_3].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_HEIST_JEWELRY_2].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_DAGGER
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_TREVOR_1].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_HEAVYPISTOL
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FBI_1].completed)
		BREAK

		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_HEIST_DOCKS_2A].completed OR g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FBI_4].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_MUSKET
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_SOLOMON_1].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_GUSENBERG
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FINALE_CREDITS].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_PISTOL50
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FBI_5].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_2A].completed OR g_savedGlobals.sFlow.missionSavedData[SP_HEIST_FINALE_2B].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_MARKSMANRIFLE
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_HEIST_RURAL_2].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_HEAVYSHOTGUN
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FBI_5].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_FIREWORK
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FINALE_CREDITS].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_PROXMINE
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_TREVOR_3].completed)
		BREAK
		
		CASE WEAPONTYPE_DLC_HOMINGLAUNCHER
			RETURN (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_EXILE_1].completed)
		BREAK
	ENDSWITCH
	
	//Not a DLC weapon, return TRUE by default
	RETURN TRUE
ENDFUNC

///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPONS VIEWED
///    
FUNC INT GET_PLAYER_PED_WEAPON_VIEWED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon)	
	
	INT iStatData
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_VIEWED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC
PROC SET_PLAYER_PED_WEAPON_VIEWED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, INT iStatData)
	
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_VIEWED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_VIEWED(enumCharacterList ePed, WEAPON_TYPE eWeapon, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_VIEWED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_VIEWED_BITSET(ePed, eWeapon, iStatData)
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_VIEWED(enumCharacterList ePed, WEAPON_TYPE eWeapon)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_VIEWED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC

///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - WEAPONS PURCHASED
///    
FUNC INT GET_PLAYER_PED_WEAPON_PURCHASED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon)	
	
	INT iStatData
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("GET_PLAYER_PED_WEAPON_PURCHASED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC
PROC SET_PLAYER_PED_WEAPON_PURCHASED_BITSET(enumCharacterList ePed, WEAPON_TYPE eWeapon, INT iStatData)
	
	INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
	INT iBitset = GET_WEAPON_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("SET_PLAYER_PED_WEAPON_PURCHASED_BITSET - ran out of stats. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_WEAPON_PURCHASED(enumCharacterList ePed, WEAPON_TYPE eWeapon, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_PURCHASED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_WEAPON_PURCHASED_BITSET(ePed, eWeapon, iStatData)
    ENDIF
ENDPROC
FUNC BOOL IS_PLAYER_PED_WEAPON_PURCHASED(enumCharacterList ePed, WEAPON_TYPE eWeapon)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_WEAPON_PURCHASED_BITSET(ePed, eWeapon)
		INT iIndex = GET_WEAPON_INDEX_FROM_WEAPON_TYPE(eWeapon)
		INT iBit = GET_WEAPON_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC



///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - PLAYER KIT PURCHASED
///    
FUNC INT GET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET(enumCharacterList ePed, PLAYERKIT ePlyrKit)
	
	INT iStatData
	INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
	INT iBitset = GET_KIT_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_7, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_7, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_7, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 8
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_8, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_8, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_8, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_9, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_9, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_9, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 10
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_10, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_10, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_10, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 11
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_11, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_11, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_11, iStatData) BREAK
			ENDSWITCH
		BREAK
		
		CASE 12
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_PURCH_12, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_PURCH_12, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_PURCH_12, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_PED_COMP, "GET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET - ran out of stats [iBitset: ", iBitset, "]. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC

PROC SET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET(enumCharacterList ePed, PLAYERKIT ePlyrKit, INT iStatData)
	
	INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
	INT iBitset = GET_KIT_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_7, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_7, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_7, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 8
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_8, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_8, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_8, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_9, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_9, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_9, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 10
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_10, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_10, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_10, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 11
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_11, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_11, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_11, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 12
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_PURCH_12, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_PURCH_12, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_PURCH_12, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_PED_COMP, "SET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET - ran out of stats [iBitset: ", iBitset, "]. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_PLAYERKIT_PURCHASED(enumCharacterList ePed, PLAYERKIT ePlyrKit, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET(ePed, ePlyrKit)
		INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
		INT iBit = GET_KIT_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET(ePed, ePlyrKit, iStatData)
    ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_PED_PLAYERKIT_PURCHASED(enumCharacterList ePed, PLAYERKIT ePlyrKit)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_PLAYERKIT_PURCHASED_BITSET(ePed, ePlyrKit)
		INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
		INT iBit = GET_KIT_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC


///////////////////////////////////////////////////////////////////////////////////////////////////
///    
///    SHOP ITEM STATS - PLAYER KIT VIEWED
///    
FUNC INT GET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET(enumCharacterList ePed, PLAYERKIT ePlyrKit)
	
	INT iStatData
	INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
	INT iBitset = GET_KIT_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_7, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_7, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_7, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 8
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_8, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_8, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_8, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_9, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_9, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_9, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 10
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_10, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_10, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_10, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 11
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_11, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_11, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_11, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 12
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_GET_INT(SP0_WEAP_TINT_VIEW_12, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_GET_INT(SP1_WEAP_TINT_VIEW_12, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_GET_INT(SP2_WEAP_TINT_VIEW_12, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_PED_COMP, "GET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET - ran out of stats [iBitset: ", iBitset, "]. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
	RETURN (iStatData)
ENDFUNC

PROC SET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET(enumCharacterList ePed, PLAYERKIT ePlyrKit, INT iStatData)
	
	INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
	INT iBitset = GET_KIT_BITSET(iIndex)
	
	SWITCH iBitset
		CASE 0
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_0, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_0, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_0, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_1, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_1, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_1, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_2, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_2, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_2, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_3, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_3, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_3, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_4, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_4, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_4, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_5, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_5, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_5, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_6, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_6, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_6, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_7, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_7, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_7, iStatData) BREAK
			ENDSWITCH
		BREAK
		#IF USE_TU_CHANGES
		CASE 8
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_8, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_8, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_8, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 9
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_9, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_9, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_9, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 10
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_10, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_10, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_10, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 11
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_11, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_11, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_11, iStatData) BREAK
			ENDSWITCH
		BREAK
		CASE 12
			SWITCH ePed 
				CASE CHAR_MICHAEL	STAT_SET_INT(SP0_WEAP_TINT_VIEW_12, iStatData) BREAK
				CASE CHAR_FRANKLIN	STAT_SET_INT(SP1_WEAP_TINT_VIEW_12, iStatData) BREAK
				CASE CHAR_TREVOR	STAT_SET_INT(SP2_WEAP_TINT_VIEW_12, iStatData) BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF IS_DEBUG_BUILD
				CASSERTLN(DEBUG_PED_COMP, "SET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET - ran out of stats [iBitset: ", iBitset, "]. Tell Kenneth R.")
			#ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SET_PLAYER_PED_PLAYERKIT_VIEWED(enumCharacterList ePed, PLAYERKIT ePlyrKit, BOOL bState = TRUE)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET(ePed, ePlyrKit)
		INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
		INT iBit = GET_KIT_INDEX_BITSET(iIndex)
		IF bState
        	SET_BIT(iStatData, iBit)
		ELSE
			CLEAR_BIT(iStatData, iBit)
		ENDIF
		SET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET(ePed, ePlyrKit, iStatData)
    ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_PED_PLAYERKIT_VIEWED(enumCharacterList ePed, PLAYERKIT ePlyrKit)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
		INT iStatData = GET_PLAYER_PED_PLAYERKIT_VIEWED_BITSET(ePed, ePlyrKit)
		INT iIndex = GET_KIT_INDEX_FROM_PLAYERKIT_TYPE(ePlyrKit)
		INT iBit = GET_KIT_INDEX_BITSET(iIndex)
		RETURN (IS_BIT_SET(iStatData, iBit))
    ENDIF
	RETURN FALSE
ENDFUNC




/// PURPOSE: Returns TRUE if the tattoo has been viewed by the current player character
FUNC BOOL HAS_PLAYER_PED_TATTOO_BEEN_VIEWED(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			#if USE_CLF_DLC
				RETURN IS_BIT_SET(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
			#endif
			#if USE_NRM_DLC
				RETURN IS_BIT_SET(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				RETURN IS_BIT_SET(g_savedGlobals.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
			#endif
			#endif
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Set a tattoo to be viewed for the specified character
PROC SET_PLAYER_PED_TATTOO_AS_VIEWED(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo, BOOL bViewed)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			IF bViewed
				#if USE_CLF_DLC
					SET_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC
					SET_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC 
					SET_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif				
				#endif
			ELSE
				#if USE_CLF_DLC
					CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC
					CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					CLEAR_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iViewedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif	
				#endif
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if the tattoo has been unlocked for the specified character
FUNC BOOL IS_PLAYER_PED_TATTOO_UNLOCKED(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// DLC items always unlocked.
		IF ePed = CHAR_MICHAEL
			IF eTattoo >= TATTOO_SP_MICHAEL_DLC
				RETURN TRUE
			ENDIF
		ELIF ePed = CHAR_FRANKLIN
			IF eTattoo >= TATTOO_SP_FRANKLIN_DLC
				RETURN TRUE
			ENDIF
		ELIF ePed = CHAR_TREVOR
			IF eTattoo >= TATTOO_SP_TREVOR_DLC
				RETURN TRUE
			ENDIF
		ENDIF
	
	
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			#if USE_CLF_DLC
				RETURN (IS_BIT_SET(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif
			#if USE_NRM_DLC
				RETURN (IS_BIT_SET(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				RETURN (IS_BIT_SET(g_savedGlobals.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif			
			#endif
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Set a tattoo to be locked or unlocked for the specified character
PROC SET_PLAYER_PED_TATTOO_UNLOCKED(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo, BOOL bUnlocked)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			IF bUnlocked
				#if USE_CLF_DLC
					SET_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC
					SET_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					SET_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif				
				#endif
			ELSE
				#if USE_CLF_DLC
					CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC
					CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					CLEAR_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iUnlockedTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif		
				#endif
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Returns TRUE if the tattoo is currently on the specified character
FUNC BOOL IS_PLAYER_PED_TATTOO_CURRENT(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			#if USE_CLF_DLC
				RETURN (IS_BIT_SET(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif
			#if USE_NRM_DLC
				RETURN (IS_BIT_SET(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				RETURN (IS_BIT_SET(g_savedGlobals.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex))
			#endif	
			#endif
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Set a tattoo to be current for the specified character
PROC SET_PED_TATTOO_CURRENT(enumCharacterList ePed, TATTOO_NAME_ENUM eTattoo, BOOL bCurrent)
	IF IS_PLAYER_PED_PLAYABLE(ePed)
		TATTOO_DATA_STRUCT sTattooData
		IF GET_TATTOO_DATA(sTattooData, eTattoo, GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), NULL)
			IF bCurrent
				#if USE_CLF_DLC
					SET_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC
					SET_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					SET_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif				
				#endif
			ELSE
				#if USE_CLF_DLC
					CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if USE_NRM_DLC 
					CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					CLEAR_BIT(g_savedGlobals.sPlayerData.sTattoos[ePed].iCurrentTattoos[sTattooData.iBitset], sTattooData.iBitIndex)
				#endif
				#endif
			ENDIF
		ENDIF
	ENDIF
ENDPROC
/// PURPOSE: Sets the player peds decorations.
PROC RESTORE_PLAYER_PED_TATTOOSCLF(PED_INDEX ped)
	
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		CLEAR_PED_DECORATIONS(ped)
		
		INT iBitset, iBit
		TATTOO_DATA_STRUCT sTattooData
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitset
			REPEAT 32 iBit				
				IF IS_BIT_SET(g_savedGlobalsClifford.sPlayerData.sTattoos[ePed].iCurrentTattoos[iBitset], iBit)
					IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitset, iBit), GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), ped)
						#IF IS_DEBUG_BUILD
							PRINTLN("Adding tattoo '", sTattooData.sLabel, "' to ", GET_PLAYER_PED_STRING(ePed))
						#ENDIF
						
						ADD_PED_DECORATION_FROM_HASHES(ped, sTattooData.iCollection, sTattooData.iPreset)
					ENDIF
				ENDIF	
			ENDREPEAT
		ENDREPEAT
    ENDIF
ENDPROC
/// PURPOSE: Sets the player peds decorations.
PROC RESTORE_PLAYER_PED_TATTOOSNRM(PED_INDEX ped)
	
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		CLEAR_PED_DECORATIONS(ped)
		
		INT iBitset, iBit
		TATTOO_DATA_STRUCT sTattooData
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitset
			REPEAT 32 iBit				
				IF IS_BIT_SET(g_savedGlobalsnorman.sPlayerData.sTattoos[ePed].iCurrentTattoos[iBitset], iBit)
					IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitset, iBit), GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), ped)
						#IF IS_DEBUG_BUILD
							PRINTLN("Adding tattoo '", sTattooData.sLabel, "' to ", GET_PLAYER_PED_STRING(ePed))
						#ENDIF
						
						ADD_PED_DECORATION_FROM_HASHES(ped, sTattooData.iCollection, sTattooData.iPreset)
					ENDIF
				ENDIF				
			ENDREPEAT
		ENDREPEAT
    ENDIF
ENDPROC
/// PURPOSE: Sets the player peds decorations.
PROC RESTORE_PLAYER_PED_TATTOOS(PED_INDEX ped)
	#if USE_CLF_DLC
		RESTORE_PLAYER_PED_TATTOOSCLF(ped)
		exit
	#endif
	#if USE_NRM_DLC
		RESTORE_PLAYER_PED_TATTOOSNRM(ped)
		exit
	#endif
	
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		CLEAR_PED_DECORATIONS(ped)
		
		INT iBitset, iBit
		TATTOO_DATA_STRUCT sTattooData
		REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitset
			REPEAT 32 iBit
				IF IS_BIT_SET(g_savedGlobals.sPlayerData.sTattoos[ePed].iCurrentTattoos[iBitset], iBit)
					IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitset, iBit), GET_TATTOO_FACTION_FOR_PLAYER_PED(ePed), ped)
						#IF IS_DEBUG_BUILD
							PRINTLN("Adding tattoo '", sTattooData.sLabel, "' to ", GET_PLAYER_PED_STRING(ePed))
						#ENDIF
						
						ADD_PED_DECORATION_FROM_HASHES(ped, sTattooData.iCollection, sTattooData.iPreset)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
    ENDIF
ENDPROC


PROC SET_STORED_FBI4_MASK(PED_INDEX pedID)
	IF NOT IS_PED_INJURED(pedID)
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
		IF ePed = CHAR_MICHAEL
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed], FALSE)
				CPRINTLN(DEBUG_PED_COMP, "default mask being applied")
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P0_MASK_HOCKEY_WHITE, FALSE)
			ENDIF
		ELIF ePed = CHAR_FRANKLIN
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed], FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P1_MASK_SKULL_GREY, FALSE)
			ENDIF
		ELIF ePed = CHAR_TREVOR
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed], FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P2_MASK_MONKEY, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_STORED_FBI4_BOILER_SUIT(PED_INDEX pedID)
	IF NOT IS_PED_INJURED(pedID)
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
		IF ePed = CHAR_MICHAEL
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed], FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2, FALSE)
			ENDIF
		ELIF ePed = CHAR_FRANKLIN
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed], FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1, FALSE)
			ENDIF
		ELIF ePed = CHAR_TREVOR
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
			OR NOT SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed], FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRELOAD_STORED_FBI4_OUTFIT(PED_INDEX pedID)
	IF NOT IS_PED_INJURED(pedID)
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
		IF ePed = CHAR_MICHAEL
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
				PRELOAD_OUTFIT(pedID, OUTFIT_P0_PREP_BOILER_SUIT_2)
			ELSE
				PRELOAD_OUTFIT(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed])
			ENDIF
			
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
				PRELOAD_PED_COMP(pedID, COMP_TYPE_PROPS, PROPS_P0_MASK_HOCKEY_WHITE)
			ELSE
				PRELOAD_PED_COMP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed])
			ENDIF
			
		ELIF ePed = CHAR_FRANKLIN
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
				PRELOAD_OUTFIT(pedID, OUTFIT_P1_PREP_BOILER_SUIT_1)
			ELSE
				PRELOAD_OUTFIT(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed])
			ENDIF
			
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
				PRELOAD_PED_COMP(pedID, COMP_TYPE_PROPS, PROPS_P1_MASK_SKULL_GREY)
			ELSE
				PRELOAD_PED_COMP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed])
			ENDIF
			
		ELIF ePed = CHAR_TREVOR
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed] = DUMMY_PED_COMP
				PRELOAD_OUTFIT(pedID, OUTFIT_P2_PREP_BOILER_SUIT_3)
			ELSE
				PRELOAD_OUTFIT(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4BoilerSuit[ePed])
			ENDIF
			
			IF g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed] = DUMMY_PED_COMP
				PRELOAD_PED_COMP(pedID, COMP_TYPE_PROPS, PROPS_P2_MASK_MONKEY)
			ELSE
				PRELOAD_PED_COMP(pedID, g_savedGlobals.sPlayerData.sInfo.eFBI4MaskType[ePed], g_savedGlobals.sPlayerData.sInfo.eFBI4MaskName[ePed])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_STORED_FBI4_OUTFIT(PED_INDEX pedID)
	SET_STORED_FBI4_BOILER_SUIT(pedID)
	SET_STORED_FBI4_MASK(pedID)
ENDPROC

/// PURPOSE: Sets the ped component and prop variations
PROC SET_PLAYER_PED_VARIATION_STATS(PED_INDEX ped, INT iSlot = 0)

	IF IS_PED_INJURED(ped)
		PRINTLN("SET_PLAYER_PED_VARIATION_STATS - Ped is dead")
		EXIT
	ENDIF
	
	IF ENUM_TO_INT(GET_ENTITY_MODEL(ped)) != GET_PACKED_STAT_INT(PACKED_SP_PLAYER_CHAR, iSlot)
		PRINTLN("SET_PLAYER_PED_VARIATION_STATS - Ped doesn't match last player model")
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTLN("SET_PLAYER_PED_VARIATION_STATS - Setting player ped varitions from stats")
	#ENDIF
	
	PED_VARIATION_STRUCT sTempVariations
	
	// Store the peds variations in the packed stats for use with the front end menu.
	sTempVariations.iTextureVariation[0] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HEAD, iSlot)
	sTempVariations.iTextureVariation[1] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_BERD, iSlot)
	sTempVariations.iTextureVariation[2] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAIR, iSlot)
	sTempVariations.iTextureVariation[3] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TORSO, iSlot)
	sTempVariations.iTextureVariation[4] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_LEG, iSlot)
	sTempVariations.iTextureVariation[5] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAND, iSlot)
	sTempVariations.iTextureVariation[6] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_FEET, iSlot)
	sTempVariations.iTextureVariation[7] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TEETH, iSlot)
	sTempVariations.iTextureVariation[8] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL, iSlot)
	sTempVariations.iTextureVariation[9] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL2, iSlot)
	sTempVariations.iTextureVariation[10] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_DECL, iSlot)
	sTempVariations.iTextureVariation[11] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_JBIB, iSlot)
	
	sTempVariations.iDrawableVariation[0] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HEAD, iSlot)
	sTempVariations.iDrawableVariation[1] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_BERD, iSlot)
	sTempVariations.iDrawableVariation[2] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAIR, iSlot)
	sTempVariations.iDrawableVariation[3] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TORSO, iSlot)
	sTempVariations.iDrawableVariation[4] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_LEG, iSlot)
	sTempVariations.iDrawableVariation[5] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAND, iSlot)
	sTempVariations.iDrawableVariation[6] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_FEET, iSlot)
	sTempVariations.iDrawableVariation[7] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TEETH, iSlot)
	sTempVariations.iDrawableVariation[8] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL, iSlot)
	sTempVariations.iDrawableVariation[9] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL2, iSlot)
	sTempVariations.iDrawableVariation[10] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_DECL, iSlot)
	sTempVariations.iDrawableVariation[11] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_JBIB, iSlot)
	
	sTempVariations.iPaletteVariation[0] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HEAD, iSlot)
	sTempVariations.iPaletteVariation[1] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_BERD, iSlot)
	sTempVariations.iPaletteVariation[2] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HAIR, iSlot)
	sTempVariations.iPaletteVariation[3] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_TORSO, iSlot)
	sTempVariations.iPaletteVariation[4] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_LEG, iSlot)
	sTempVariations.iPaletteVariation[5] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HAND, iSlot)
	sTempVariations.iPaletteVariation[6] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_FEET, iSlot)
	sTempVariations.iPaletteVariation[7] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_TEETH, iSlot)
	sTempVariations.iPaletteVariation[8] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL, iSlot)
	sTempVariations.iPaletteVariation[9] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL2, iSlot)
	sTempVariations.iPaletteVariation[10] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_DECL, iSlot)
	sTempVariations.iPaletteVariation[11] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_JBIB, iSlot)
	
	sTempVariations.iPropIndex[0] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_0, iSlot)
	sTempVariations.iPropIndex[1] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_1, iSlot)
	sTempVariations.iPropIndex[2] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_2, iSlot)
	sTempVariations.iPropIndex[3] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_3, iSlot)
	sTempVariations.iPropIndex[4] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_4, iSlot)
	sTempVariations.iPropIndex[5] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_5, iSlot)
	sTempVariations.iPropIndex[6] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_6, iSlot)
	sTempVariations.iPropIndex[7] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_7, iSlot)
	sTempVariations.iPropIndex[8] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_8, iSlot)
	
	sTempVariations.iPropTexture[0] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_0, iSlot)
	sTempVariations.iPropTexture[1] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_1, iSlot)
	sTempVariations.iPropTexture[2] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_2, iSlot)
	sTempVariations.iPropTexture[3] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_3, iSlot)
	sTempVariations.iPropTexture[4] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_4, iSlot)
	sTempVariations.iPropTexture[5] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_5, iSlot)
	sTempVariations.iPropTexture[6] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_6, iSlot)
	sTempVariations.iPropTexture[7] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_7, iSlot)
	sTempVariations.iPropTexture[8] = GET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_8, iSlot)
	
	SET_PED_VARIATIONS(ped, sTempVariations)
ENDPROC

PROC UPDATE_PLAYER_PED_VARIATION_STATS()
	IF IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
	OR NOT HAS_IMPORTANT_STATS_LOADED()
		EXIT
	ENDIF
	
	
	PED_VARIATION_STRUCT sTempVariations
	GET_PED_VARIATIONS(PLAYER_PED_ID(), sTempVariations)
	
	// Store the peds variations in the packed stats for use with the front end menu.
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HEAD, sTempVariations.iTextureVariation[0])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_BERD, sTempVariations.iTextureVariation[1])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAIR, sTempVariations.iTextureVariation[2])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TORSO, sTempVariations.iTextureVariation[3])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_LEG, sTempVariations.iTextureVariation[4])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAND, sTempVariations.iTextureVariation[5])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_FEET, sTempVariations.iTextureVariation[6])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_TEETH, sTempVariations.iTextureVariation[7])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL, sTempVariations.iTextureVariation[8])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_SPECIAL2, sTempVariations.iTextureVariation[9])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_DECL, sTempVariations.iTextureVariation[10])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_JBIB, sTempVariations.iTextureVariation[11])
	
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HEAD, sTempVariations.iDrawableVariation[0])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_BERD, sTempVariations.iDrawableVariation[1])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAIR, sTempVariations.iDrawableVariation[2])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TORSO, sTempVariations.iDrawableVariation[3])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_LEG, sTempVariations.iDrawableVariation[4])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAND, sTempVariations.iDrawableVariation[5])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_FEET, sTempVariations.iDrawableVariation[6])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_TEETH, sTempVariations.iDrawableVariation[7])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL, sTempVariations.iDrawableVariation[8])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_SPECIAL2, sTempVariations.iDrawableVariation[9])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_DECL, sTempVariations.iDrawableVariation[10])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_JBIB, sTempVariations.iDrawableVariation[11])
	
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HEAD, sTempVariations.iPaletteVariation[0])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_BERD, sTempVariations.iPaletteVariation[1])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HAIR, sTempVariations.iPaletteVariation[2])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_TORSO, sTempVariations.iPaletteVariation[3])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_LEG, sTempVariations.iPaletteVariation[4])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HAND, sTempVariations.iPaletteVariation[5])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_FEET, sTempVariations.iPaletteVariation[6])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_TEETH, sTempVariations.iPaletteVariation[7])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL, sTempVariations.iPaletteVariation[8])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_SPECIAL2, sTempVariations.iPaletteVariation[9])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_DECL, sTempVariations.iPaletteVariation[10])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_JBIB, sTempVariations.iPaletteVariation[11])
	
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_0, sTempVariations.iPropIndex[0])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_1, sTempVariations.iPropIndex[1])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_2, sTempVariations.iPropIndex[2])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_3, sTempVariations.iPropIndex[3])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_4, sTempVariations.iPropIndex[4])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_5, sTempVariations.iPropIndex[5])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_6, sTempVariations.iPropIndex[6])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_7, sTempVariations.iPropIndex[7])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPINDX_8, sTempVariations.iPropIndex[8])
	
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_0, sTempVariations.iPropTexture[0])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_1, sTempVariations.iPropTexture[1])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_2, sTempVariations.iPropTexture[2])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_3, sTempVariations.iPropTexture[3])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_4, sTempVariations.iPropTexture[4])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_5, sTempVariations.iPropTexture[5])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_6, sTempVariations.iPropTexture[6])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_7, sTempVariations.iPropTexture[7])
	SET_PACKED_STAT_INT(PACKED_SP_CLTH_PROPTEXT_8, sTempVariations.iPropTexture[8])
	
	SET_PACKED_STAT_INT(PACKED_SP_PLAYER_CHAR, ENUM_TO_INT(GET_CURRENT_PLAYER_PED_ENUM()))
	
	#IF USE_TU_CHANGES
		STAT_SET_BOOL(CLO_STORED_INITIAL, TRUE)
	#ENDIF
	
	#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerData.sInfo.bPlayerVariationsStoredToStats = TRUE
	#endif
	#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerData.sInfo.bPlayerVariationsStoredToStats = TRUE
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		g_savedGlobals.sPlayerData.sInfo.bPlayerVariationsStoredToStats = TRUE
	#endif
	#endif
ENDPROC


/// PURPOSE: Grabs the ped component and prop variations
///    NOTE: Param bStoreOnMission should be TRUE if mission clothes are to be saved as normal clothes
PROC STORE_PLAYER_PED_VARIATIONS(PED_INDEX ped, BOOL bStoreOnMission = FALSE)
    IF NOT IS_PED_INJURED(ped)
	
		// Grab the ped enum
	    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
	    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
			#IF IS_DEBUG_BUILD
				PRINTLN("STORE_PLAYER_PED_VARIATIONS - Storing variation for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
			#ENDIF
			
			//If trevor has pants down. default his variation
			IF ePed = CHAR_TREVOR
				PED_COMP_NAME_ENUM eCurrentComp
				eCurrentComp = GET_PED_COMP_ITEM_CURRENT_SP(ped, COMP_TYPE_LEGS)
				IF eCurrentComp = LEGS_P2_TOILET
					SETUP_DEFAULT_PLAYER_VARIATIONS(ePed)		
					SET_PED_VARIATIONS(ped, g_savedGlobals.sPlayerData.sInfo.sVariations[ePed])		   
				endif					
			endif
			
			
			PED_VARIATION_STRUCT sTempVariations
			GET_PED_VARIATIONS(ped, sTempVariations)
			
			// Update the last stored ped clothes.
			g_sLastStoredPlayerPedClothes[ePed] = sTempVariations
			
			IF ped = PLAYER_PED_ID()
				UPDATE_PLAYER_PED_VARIATION_STATS()
			ENDIF
			
			// Never allow the clothes shop to store clothes during missions that require you to get an outfit
			IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != GET_HASH_KEY("clothes_shop_sp")
			OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("lester1")) = 0 AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("michael4")) = 0)
			
				// Only store ped clothes in the saved globals when not on mission or flagged as safe to store on mission
				IF bStoreOnMission
				OR (NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_SPMC)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_PREP)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_CHAR)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS))
				
					#IF IS_DEBUG_BUILD
						IF bStoreOnMission
							DEBUG_PRINTCALLSTACK()
							PRINTLN("STORE_PLAYER_PED_VARIATIONS - Clothes getting stored on mission for ", GET_PLAYER_PED_STRING(ePed))
							PRINTLN("...MISSION TYPE = ", g_OnMissionState)
						ENDIF
					#ENDIF
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed] = sTempVariations					
						// Track count so we know when to reset when DLC items are removed
						g_savedGlobalsClifford.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)					
						// Track the ped component enums
						INT iComp
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed] = sTempVariations					
						// Track count so we know when to reset when DLC items are removed
						g_savedGlobalsnorman.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)					
						// Track the ped component enums
						INT iComp
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT
					#endif
					
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sPlayerData.sInfo.sVariations[ePed] = sTempVariations					
						// Track count so we know when to reset when DLC items are removed
						g_savedGlobals.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)					
						// Track the ped component enums
						INT iComp
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT
						REPEAT NUM_PED_COMPONENTS iComp
							g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[iComp][ePed] = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(ped, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComp)))
						ENDREPEAT					
						// Set the suit removed flag once set
						IF ePed = CHAR_MICHAEL
							IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_SET_HAGGARD_SUIT]
								g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_REM_HAGGARD_SUIT] = TRUE
							ENDIF
						ENDIF	
					#endif	
					#endif
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Sets the stored hairdo for the player ped
PROC STORE_PLAYER_PED_HAIRDO(PED_INDEX ped, BOOL bStoreOnMission = FALSE)
	IF NOT IS_PED_INJURED(ped)
	
		// Grab the ped enum
	    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
	    IF IS_PLAYER_PED_PLAYABLE(ePed)
		
			PED_VARIATION_STRUCT sTempVariations
			GET_PED_VARIATIONS(ped, sTempVariations)
			
			// Store the peds variations in the packed stats for use with the front end menu.
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HEAD, sTempVariations.iTextureVariation[0])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_BERD, sTempVariations.iTextureVariation[1])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_TEXTVAR_HAIR, sTempVariations.iTextureVariation[2])
			
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HEAD, sTempVariations.iDrawableVariation[0])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_BERD, sTempVariations.iDrawableVariation[1])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_DRAWVAR_HAIR, sTempVariations.iDrawableVariation[2])
			
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HEAD, sTempVariations.iPaletteVariation[0])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_BERD, sTempVariations.iPaletteVariation[1])
			SET_PACKED_STAT_INT(PACKED_SP_CLTH_PALVAR_HAIR, sTempVariations.iPaletteVariation[2])
			
			// Update the last stored ped clothes.
			g_sLastStoredPlayerPedClothes[ePed].iTextureVariation[0] = sTempVariations.iTextureVariation[0]
			g_sLastStoredPlayerPedClothes[ePed].iTextureVariation[1] = sTempVariations.iTextureVariation[1]
			g_sLastStoredPlayerPedClothes[ePed].iTextureVariation[2] = sTempVariations.iTextureVariation[2]
			
			g_sLastStoredPlayerPedClothes[ePed].iDrawableVariation[0] = sTempVariations.iDrawableVariation[0]
			g_sLastStoredPlayerPedClothes[ePed].iDrawableVariation[1] = sTempVariations.iDrawableVariation[1]
			g_sLastStoredPlayerPedClothes[ePed].iDrawableVariation[2] = sTempVariations.iDrawableVariation[2]
			
			g_sLastStoredPlayerPedClothes[ePed].iPaletteVariation[0] = sTempVariations.iPaletteVariation[0]
			g_sLastStoredPlayerPedClothes[ePed].iPaletteVariation[1] = sTempVariations.iPaletteVariation[1]
			g_sLastStoredPlayerPedClothes[ePed].iPaletteVariation[2] = sTempVariations.iPaletteVariation[2]
						
			// Only store ped clothes in the saved globals when not on mission or flagged as safe to store on mission
			IF (NOT IS_CURRENTLY_ON_MISSION_TO_TYPE() OR bStoreOnMission)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("STORE_PLAYER_PED_HAIRDO - Storing variations for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
				#ENDIF
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[0] = sTempVariations.iTextureVariation[0]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[1] = sTempVariations.iTextureVariation[1]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[2] = sTempVariations.iTextureVariation[2]
					
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[0] = sTempVariations.iDrawableVariation[0]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[1] = sTempVariations.iDrawableVariation[1]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[2] = sTempVariations.iDrawableVariation[2]
					
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[0] = sTempVariations.iPaletteVariation[0]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[1] = sTempVariations.iPaletteVariation[1]
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[2] = sTempVariations.iPaletteVariation[2]	
				#endif
				#if USE_NRM_DLC
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[0] = sTempVariations.iTextureVariation[0]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[1] = sTempVariations.iTextureVariation[1]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[2] = sTempVariations.iTextureVariation[2]
					
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[0] = sTempVariations.iDrawableVariation[0]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[1] = sTempVariations.iDrawableVariation[1]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[2] = sTempVariations.iDrawableVariation[2]
					
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[0] = sTempVariations.iPaletteVariation[0]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[1] = sTempVariations.iPaletteVariation[1]
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[2] = sTempVariations.iPaletteVariation[2]	
				#endif
				
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[0] = sTempVariations.iTextureVariation[0]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[1] = sTempVariations.iTextureVariation[1]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[2] = sTempVariations.iTextureVariation[2]
					
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[0] = sTempVariations.iDrawableVariation[0]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[1] = sTempVariations.iDrawableVariation[1]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[2] = sTempVariations.iDrawableVariation[2]
					
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[0] = sTempVariations.iPaletteVariation[0]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[1] = sTempVariations.iPaletteVariation[1]
					g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iPaletteVariation[2] = sTempVariations.iPaletteVariation[2]	
				#endif	
				#endif
				
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE: Sets the stored hairdo for the player ped
PROC RESTORE_PLAYER_PED_HAIRDO(PED_INDEX ped)
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_HAIRDO - Restoring hairdo for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
		#ENDIF
		#if USE_CLF_DLC
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HAIR], g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HAIR])
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HEAD], g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HEAD])
    	#endif
		#if USE_NRM_DLC
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HAIR], g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HAIR])
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HEAD], g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HEAD])
    	#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HAIR], g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HAIR])
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HEAD, g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_HEAD], g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_HEAD])
    	#endif	
		#endif
	ENDIF
ENDPROC



/// PURPOSE: Sets the ped component and prop variations
PROC RESTORE_PLAYER_PED_VARIATIONSCLF(PED_INDEX ped)
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_VARIATIONS - Restoring variation for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
		#ENDIF
				
		// Reset to default if the DLC item count has changed
		// - Perhaps we can use DLC_CHECK_SAVEGAME_PACK_CONFIGURATION instead...
		IF NOT g_bDLCClothesCheckedOnRestore[ePed]
			IF g_savedGlobalsClifford.sPlayerData.sInfo.iTrackedPedComps[ePed] != 0
				IF GET_TOTAL_PED_COMP_VARIATIONS(ped) != g_savedGlobalsClifford.sPlayerData.sInfo.iTrackedPedComps[ePed]
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_PED_COMP, "OUTFIT CHECK: Setting default outfit as dlc content checks have changed for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
					#ENDIF
					SETUP_DEFAULT_PLAYER_VARIATIONS_CLF(ePed)
					g_savedGlobalsClifford.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)
				ENDIF
			ENDIF
		ENDIF		
		SET_PED_VARIATIONS(ped, g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed])		
		
		g_bDLCClothesCheckedOnRestore[ePed] = TRUE
		
    ENDIF
ENDPROC
/// PURPOSE: Sets the ped component and prop variations
PROC RESTORE_PLAYER_PED_VARIATIONSNRM(PED_INDEX ped)
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_VARIATIONS - Restoring variation for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
		#ENDIF
				
		// Reset to default if the DLC item count has changed
		// - Perhaps we can use DLC_CHECK_SAVEGAME_PACK_CONFIGURATION instead...
		IF NOT g_bDLCClothesCheckedOnRestore[ePed]
			IF g_savedGlobalsnorman.sPlayerData.sInfo.iTrackedPedComps[ePed] != 0
				IF GET_TOTAL_PED_COMP_VARIATIONS(ped) != g_savedGlobalsnorman.sPlayerData.sInfo.iTrackedPedComps[ePed]
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_PED_COMP, "OUTFIT CHECK: Setting default outfit as dlc content checks have changed for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
					#ENDIF
					SETUP_DEFAULT_PLAYER_VARIATIONS_NRM(ePed)
					g_savedGlobalsnorman.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)
				ENDIF
			ENDIF
		ENDIF		
		SET_PED_VARIATIONS(ped, g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed])		
				
		g_bDLCClothesCheckedOnRestore[ePed] = TRUE
		
    ENDIF
ENDPROC
/// PURPOSE: Sets the ped component and prop variations
PROC RESTORE_PLAYER_PED_VARIATIONS(PED_INDEX ped)
	#if USE_CLF_DLC
		RESTORE_PLAYER_PED_VARIATIONSCLF(ped)
		exit	
	#endif
	#if USE_NRM_DLC
		RESTORE_PLAYER_PED_VARIATIONSNRM(ped)
		exit
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_VARIATIONS - Restoring variation for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
		#ENDIF
		
		// Reset to default if the DLC item count has changed
		// - Perhaps we can use DLC_CHECK_SAVEGAME_PACK_CONFIGURATION instead...
		IF NOT g_bDLCClothesCheckedOnRestore[ePed]
			IF g_savedGlobals.sPlayerData.sInfo.iTrackedPedComps[ePed] != 0
				IF GET_TOTAL_PED_COMP_VARIATIONS(ped) != g_savedGlobals.sPlayerData.sInfo.iTrackedPedComps[ePed]
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_PED_COMP, "OUTFIT CHECK: Setting default outfit as dlc content checks have changed for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
					#ENDIF
					SETUP_DEFAULT_PLAYER_VARIATIONS(ePed)
					g_savedGlobals.sPlayerData.sInfo.iTrackedPedComps[ePed] = GET_TOTAL_PED_COMP_VARIATIONS(ped)
				ENDIF
			ENDIF
		ENDIF		
		SET_PED_VARIATIONS(ped, g_savedGlobals.sPlayerData.sInfo.sVariations[ePed])
		
		// Reset to default if the items shouldn't be unlocked yet
		IF NOT g_bDLCClothesCheckedOnRestore[ePed]	
			PED_COMP_NAME_ENUM eCurrentComp
			BOOL bSetDefaultVariations = FALSE
			
			IF ePed = CHAR_MICHAEL
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
					eCurrentComp = GET_PED_COMP_ITEM_CURRENT_SP(ped, COMP_TYPE_OUTFIT)
					IF eCurrentComp = OUTFIT_P0_MOVIE_TUXEDO
						bSetDefaultVariations = TRUE
						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_PED_COMP, "OUTFIT CHECK: Setting default outfit as Movie Tuxedo not available for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
						#ENDIF
						
					ENDIF
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO, FALSE, FALSE)
				ENDIF
				
				IF NOT LOCAL_GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
					eCurrentComp = GET_PED_COMP_ITEM_CURRENT_SP(ped, COMP_TYPE_TORSO)
					IF eCurrentComp = TORSO_P0_GILET_0
					OR eCurrentComp = TORSO_P0_GILET_1
					OR eCurrentComp = TORSO_P0_GILET_2
					OR eCurrentComp = TORSO_P0_GILET_3
					OR eCurrentComp = TORSO_P0_GILET_4
					OR eCurrentComp = TORSO_P0_GILET_5
						bSetDefaultVariations = TRUE
						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_PED_COMP,"OUTFIT CHECK: Setting default outfit as Programmer torso not available for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
						#ENDIF
					ENDIF
					
					eCurrentComp = GET_PED_COMP_ITEM_CURRENT_SP(ped, COMP_TYPE_LEGS)
					IF eCurrentComp = LEGS_P0_CARGO_SHORTS_0
					OR eCurrentComp = LEGS_P0_CARGO_SHORTS_1
					OR eCurrentComp = LEGS_P0_CARGO_SHORTS_2
					OR eCurrentComp = LEGS_P0_CARGO_SHORTS_3
					OR eCurrentComp = LEGS_P0_CARGO_SHORTS_4
						bSetDefaultVariations = TRUE
						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_PED_COMP, "OUTFIT CHECK: Setting default outfit as Programmer legs not available for ", GET_PLAYER_PED_STRING(ePed), " - ", GET_THIS_SCRIPT_NAME())
						#ENDIF
					ENDIF
					
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_0, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_1, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_2, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_3, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_4, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_TORSO, TORSO_P0_GILET_5, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_0, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_1, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_2, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_3, TRUE, FALSE)
					SET_PED_COMP_ITEM_DETAILS_SP(GET_PLAYER_PED_MODEL(CHAR_MICHAEL), COMP_TYPE_LEGS, LEGS_P0_CARGO_SHORTS_4, TRUE, FALSE)
				ENDIF
			ELIF ePed = CHAR_TREVOR
				eCurrentComp = GET_PED_COMP_ITEM_CURRENT_SP(ped, COMP_TYPE_LEGS)
				IF eCurrentComp = LEGS_P2_TOILET
					bSetDefaultVariations = true
				endif
			ENDIF
			
			IF bSetDefaultVariations
				SETUP_DEFAULT_PLAYER_VARIATIONS(ePed)				
				SET_PED_VARIATIONS(ped, g_savedGlobals.sPlayerData.sInfo.sVariations[ePed])		    
			ENDIF
		ENDIF
		
		g_bDLCClothesCheckedOnRestore[ePed] = TRUE
		
    ENDIF
	#ENDIF
	#ENDIF
ENDPROC

/// PURPOSE: Cycles through the list of weapons and checks to see if the player has any ammo or mods
PROC STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PED_INDEX ped)
    // Grab the ped enum
    IF NOT IS_PED_INJURED(ped)
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT - Storing weapons, pedID = ", NATIVE_TO_INT(ped))
		#ENDIF
		#if USE_CLF_DLC
			GET_PED_WEAPONS(ped, g_savedGlobalsClifford.sPlayerData.sInfo.sMissionSnapshotWeapons)
			
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
			ENDFOR
		#endif
		#if USE_NRM_DLC
			GET_PED_WEAPONS(ped, g_savedGlobalsnorman.sPlayerData.sInfo.sMissionSnapshotWeapons)
		
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
			ENDFOR
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			GET_PED_WEAPONS(ped, g_savedGlobals.sPlayerData.sInfo.sMissionSnapshotWeapons)
			
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
			ENDFOR
    	#endif	
		#endif
		
	ENDIF
ENDPROC

/// PURPOSE: Cycles through the list of weapons and checks to see if the player has any ammo or mods
PROC RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(PED_INDEX ped, BOOL bRemoveCurrentWeapons = TRUE)
    // Grab the ped enum
    IF NOT IS_PED_INJURED(ped)
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT - Restoring weapons, pedID = ", NATIVE_TO_INT(ped), " Called by: ", GET_THIS_SCRIPT_NAME())
		#ENDIF
		#if USE_CLF_DLC
			SET_PED_WEAPONS(ped, g_savedGlobalsClifford.sPlayerData.sInfo.sMissionSnapshotWeapons, bRemoveCurrentWeapons)
			
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			ENDFOR
    	#endif
		#if USE_NRM_DLC
			SET_PED_WEAPONS(ped, g_savedGlobalsnorman.sPlayerData.sInfo.sMissionSnapshotWeapons, bRemoveCurrentWeapons)
			
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			ENDFOR
    	#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			SET_PED_WEAPONS(ped, g_savedGlobals.sPlayerData.sInfo.sMissionSnapshotWeapons, bRemoveCurrentWeapons)
			
			INT slotIndex
			FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
				HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			ENDFOR
    	#endif	
		#endif
		
	ENDIF
ENDPROC

/// PURPOSE: Cycles through the list of weapons and checks to see if the player has any ammo or mods
PROC RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(enumCharacterList ePed)
    // Grab the ped enum
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR - Restoring weapons, ped enum = ", ENUM_TO_INT(ePed))
		#ENDIF
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)] = g_savedGlobalsClifford.sPlayerData.sInfo.sMissionSnapshotWeapons
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)] = g_savedGlobalsnorman.sPlayerData.sInfo.sMissionSnapshotWeapons
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)] = g_savedGlobals.sPlayerData.sInfo.sMissionSnapshotWeapons
    	#endif	
		#endif
		
	ENDIF
ENDPROC


/// PURPOSE: Cycles through the list of weapons and checks to see if the player has any ammo or mods
PROC STORE_PLAYER_PED_WEAPONS(PED_INDEX ped, BOOL storeEquippedWeapon = FALSE)
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		// To fix the infinite ammo issues we are now only going to store the players ammo.
		// Any issues, see Kenneth.
		IF ped = PLAYER_PED_ID()
	
			#IF IS_DEBUG_BUILD
				PRINTLN("STORE_PLAYER_PED_WEAPONS - Storing weapons for ", GET_PLAYER_PED_STRING(ePed), " pedID = ", NATIVE_TO_INT(ped), " Called by: ", GET_THIS_SCRIPT_NAME())
				
				INT iEntityInstanceID
				STRING strEntityScript = GET_ENTITY_SCRIPT(ped, iEntityInstanceID)
				IF NOT IS_STRING_NULL(strEntityScript)
					PRINTLN("...pedID belongs to script '", strEntityScript, "'")
				ENDIF
				
				IF NOT IS_PED_IN_TEMP_PLAYER_PED_ID_LIST(ped)
					PRINTLN("STORE_PLAYER_PED_WEAPONS - Ped not set up correctly so weapons are likely to be removed")
					//SCRIPT_ASSERT("STORE_PLAYER_PED_WEAPONS - Ped not set up correctly. Tell Kenneth R.")
				ENDIF
			#ENDIF
			
			WEAPON_TYPE equippedWeapon
			INT slotIndex
			
			#IF USE_CLF_DLC
				GET_PED_WEAPONS(ped, g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)])
				
				FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
					g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
					IF storeEquippedWeapon
						equippedWeapon = HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED()
						IF g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = equippedWeapon
							g_savedGlobalsClifford.sPlayerData.sInfo.equippedWeaponSlot = slotIndex
						ENDIF
					ENDIF
				ENDFOR
			#ENDIF
			#IF USE_NRM_DLC
				GET_PED_WEAPONS(ped, g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)])
							
				FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
					g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
					IF storeEquippedWeapon
						equippedWeapon = HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED()
						IF g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = equippedWeapon
							g_savedGlobalsnorman.sPlayerData.sInfo.equippedWeaponSlot = slotIndex
						ENDIF
					ENDIF
				ENDFOR
			#ENDIF
			
			#IF NOT USE_SP_DLC
				GET_PED_WEAPONS(ped, g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)])		
				
				FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
					g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = HUD_GET_WEAPON_WHEEL_TOP_SLOT(slotIndex)
					IF storeEquippedWeapon
						equippedWeapon = HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED()
						IF g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] = equippedWeapon
							g_savedGlobals.sPlayerData.sInfo.equippedWeaponSlot = slotIndex
						ENDIF
					ENDIF
				ENDFOR
			#ENDIF
		
			INT iTint
			GET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iTint)
			IF (ePed = CHAR_MICHAEL) 	STAT_SET_INT(SP0_PARACHUTE_CURRENT_TINT, iTint)
			ELIF (ePed = CHAR_FRANKLIN) STAT_SET_INT(SP1_PARACHUTE_CURRENT_TINT, iTint)
			ELIF (ePed = CHAR_TREVOR) 	STAT_SET_INT(SP2_PARACHUTE_CURRENT_TINT, iTint)
			ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("STORE_PLAYER_PED_WEAPONS - NOT storing weapons for ", GET_PLAYER_PED_STRING(ePed), " pedID = ", NATIVE_TO_INT(ped), " as it is not the PLAYER!")
			#ENDIF				
		ENDIF
    ENDIF   
ENDPROC


/// PURPOSE: Removes all the peds weapons and then sets the new ammo and mods for each
PROC RESTORE_PLAYER_PED_WEAPONSCLF(PED_INDEX ped, BOOL bRemoveCurrentWeapons = TRUE, BOOL setEquippedWeapon = FALSE)

	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		PRINTLN("RESTORE_PLAYER_PED_WEAPONS - Restoring weapons for ", GET_PLAYER_PED_STRING(ePed), " Called by: ", GET_THIS_SCRIPT_NAME())
		
		SET_PED_WEAPONS(ped, g_savedGlobalsClifford.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)], bRemoveCurrentWeapons)
		
		INT iTint
		IF (ePed = CHAR_MICHAEL) 	STAT_GET_INT(SP0_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_FRANKLIN) STAT_GET_INT(SP1_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_TREVOR) 	STAT_GET_INT(SP2_PARACHUTE_CURRENT_TINT, iTint)
		ENDIF
		SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iTint)

		INT slotIndex
		FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
			HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			IF setEquippedWeapon
				IF slotIndex = g_savedGlobalsClifford.sPlayerData.sInfo.equippedWeaponSlot
				OR g_savedGlobalsClifford.sPlayerData.sInfo.equippedWeaponSlot = -1 AND slotIndex = ENUM_TO_INT(WEAPON_WHEEL_SLOT_UNARMED_MELEE)//default melee
					SET_CURRENT_PED_WEAPON(ped, g_savedGlobalsClifford.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)],TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC


/// PURPOSE: Removes all the peds weapons and then sets the new ammo and mods for each
PROC RESTORE_PLAYER_PED_WEAPONSNRM(PED_INDEX ped, BOOL bRemoveCurrentWeapons = TRUE, BOOL setEquippedWeapon = FALSE)

	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		PRINTLN("RESTORE_PLAYER_PED_WEAPONS - Restoring weapons for ", GET_PLAYER_PED_STRING(ePed), " Called by: ", GET_THIS_SCRIPT_NAME())
	
		SET_PED_WEAPONS(ped, g_savedGlobalsnorman.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)], bRemoveCurrentWeapons)
		
		INT iTint
		IF (ePed = CHAR_MICHAEL) 	STAT_GET_INT(SP0_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_FRANKLIN) STAT_GET_INT(SP1_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_TREVOR) 	STAT_GET_INT(SP2_PARACHUTE_CURRENT_TINT, iTint)
		ENDIF
		SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iTint)

		INT slotIndex
		FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
			HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			IF setEquippedWeapon
				IF slotIndex = g_savedGlobalsnorman.sPlayerData.sInfo.equippedWeaponSlot
				OR g_savedGlobalsnorman.sPlayerData.sInfo.equippedWeaponSlot = -1 AND slotIndex = ENUM_TO_INT(WEAPON_WHEEL_SLOT_UNARMED_MELEE)//default melee
					SET_CURRENT_PED_WEAPON(ped, g_savedGlobalsnorman.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)],TRUE)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC


/// PURPOSE: Removes all the peds weapons and then sets the new ammo and mods for each
PROC RESTORE_PLAYER_PED_WEAPONS(PED_INDEX ped, BOOL bRemoveCurrentWeapons = TRUE, BOOL setEquippedWeapon = FALSE)
	#IF USE_CLF_DLC
		RESTORE_PLAYER_PED_WEAPONSCLF(ped, bRemoveCurrentWeapons, setEquippedWeapon)
		EXIT
	#ENDIF
	#IF USE_NRM_DLC
		RESTORE_PLAYER_PED_WEAPONSNRM(ped, bRemoveCurrentWeapons, setEquippedWeapon)	
		EXIT
	#ENDIF
	
	// Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)	
		PRINTLN("RESTORE_PLAYER_PED_WEAPONS - Restoring weapons for ", GET_PLAYER_PED_STRING(ePed), " Called by: ", GET_THIS_SCRIPT_NAME())

		SET_PED_WEAPONS(ped, g_savedGlobals.sPlayerData.sInfo.sWeapons[ENUM_TO_INT(ePed)], bRemoveCurrentWeapons)
		
		INT slotIndex
		FOR slotIndex = WEAPON_WHEEL_SLOT_PISTOL TO (ENUM_TO_INT(MAX_WHEEL_SLOTS)-1)
			HUD_SET_WEAPON_WHEEL_TOP_SLOT(g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
			IF setEquippedWeapon
				IF slotIndex = g_savedGlobals.sPlayerData.sInfo.equippedWeaponSlot
				OR g_savedGlobals.sPlayerData.sInfo.equippedWeaponSlot = -1 AND slotIndex = ENUM_TO_INT(WEAPON_WHEEL_SLOT_UNARMED_MELEE)//default melee
					IF g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] != WEAPONTYPE_INVALID
					AND g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)] != WEAPONTYPE_MOLOTOV
						if HAS_PED_GOT_WEAPON(ped, g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)])
							SET_CURRENT_PED_WEAPON(ped, g_savedGlobals.sPlayerData.sInfo.eWeaponToShow[slotIndex][ENUM_TO_INT(ePed)],TRUE)
						endif
					ENDIF
				ENDIF
			ENDIF
		ENDFOR

		INT iTint
		IF (ePed = CHAR_MICHAEL) 	STAT_GET_INT(SP0_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_FRANKLIN) STAT_GET_INT(SP1_PARACHUTE_CURRENT_TINT, iTint)
		ELIF (ePed = CHAR_TREVOR) 	STAT_GET_INT(SP2_PARACHUTE_CURRENT_TINT, iTint)
		ENDIF
		SET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iTint)
	ENDIF
ENDPROC

/// PURPOSE: Stores the player peds health.
PROC STORE_PLAYER_PED_HEALTH(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	   // Store the health as a percentage
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] = (((TO_FLOAT(GET_ENTITY_HEALTH(ped))-100.0) / (TO_FLOAT(GET_PED_MAX_HEALTH(ped))-100.0)) * 100.0) // Health under 100 is considered dead so remove margin
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] = (((TO_FLOAT(GET_ENTITY_HEALTH(ped))-100.0) / (TO_FLOAT(GET_PED_MAX_HEALTH(ped))-100.0)) * 100.0) // Health under 100 is considered dead so remove margin
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] = (((TO_FLOAT(GET_ENTITY_HEALTH(ped))-100.0) / (TO_FLOAT(GET_PED_MAX_HEALTH(ped))-100.0)) * 100.0) // Health under 100 is considered dead so remove margin
		#endif	    
		#endif
    ENDIF
ENDPROC

/// PURPOSE: Sets the player peds health.
PROC RESTORE_PLAYER_PED_HEALTHCLF(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		// Fix for #1335266 - Multiply players health with tunable.
		IF ped = PLAYER_PED_ID()
		AND GET_PED_MAX_HEALTH(ped) = 200
			SET_PED_MAX_HEALTH(ped, ROUND(GET_PED_MAX_HEALTH(ped)*g_sMPTunables.fMaxHealthMultiplier))
			PRINTLN("RESTORE_PLAYER_PED_HEALTH - Increasing players max health for tunable")
		ENDIF
				
		// Make sure the ped will have some health
        IF g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] <= 0
            g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] = 100
			
		// give ped at least a minimum of 10% health
		ELIF g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] <= 10
		 	g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] = 10
        ENDIF			
	
	    // Health is stored as a percentage so set it as a percentage of the max health => (perc / 100) * (max-100) + 100
		SET_ENTITY_HEALTH(ped, ROUND(((g_savedGlobalsClifford.sPlayerData.sInfo.fHealthPerc[ePed] / 100.0) * (TO_FLOAT(GET_PED_MAX_HEALTH(ped)) - 100.0)) + 100.0))
    	
	ENDIF
ENDPROC

/// PURPOSE: Sets the player peds health.
PROC RESTORE_PLAYER_PED_HEALTHNRM(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		// Fix for #1335266 - Multiply players health with tunable.
		IF ped = PLAYER_PED_ID()
		AND GET_PED_MAX_HEALTH(ped) = 200
			SET_PED_MAX_HEALTH(ped, ROUND(GET_PED_MAX_HEALTH(ped)*g_sMPTunables.fMaxHealthMultiplier))
			PRINTLN("RESTORE_PLAYER_PED_HEALTH - Increasing players max health for tunable")
		ENDIF		
		
		// Make sure the ped will have some health
        IF g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] <= 0
            g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] = 100
			
		// give ped at least a minimum of 10% health
		ELIF g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] <= 10
		 	g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] = 10
        ENDIF			
	
	    // Health is stored as a percentage so set it as a percentage of the max health => (perc / 100) * (max-100) + 100
		SET_ENTITY_HEALTH(ped, ROUND(((g_savedGlobalsnorman.sPlayerData.sInfo.fHealthPerc[ePed] / 100.0) * (TO_FLOAT(GET_PED_MAX_HEALTH(ped)) - 100.0)) + 100.0))
	
	ENDIF
ENDPROC
/// PURPOSE: Sets the player peds health.
PROC RESTORE_PLAYER_PED_HEALTH(PED_INDEX ped)
    #if USE_CLF_DLC
		RESTORE_PLAYER_PED_HEALTHCLF(ped)
		exit
	#endif
	#if USE_NRM_DLC
		RESTORE_PLAYER_PED_HEALTHNRM(ped)
		exit
	#endif
	
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		// Fix for #1335266 - Multiply players health with tunable.
		IF ped = PLAYER_PED_ID()
		AND GET_PED_MAX_HEALTH(ped) = 200
			SET_PED_MAX_HEALTH(ped, ROUND(GET_PED_MAX_HEALTH(ped)*g_sMPTunables.fMaxHealthMultiplier))
			PRINTLN("RESTORE_PLAYER_PED_HEALTH - Increasing players max health for tunable")
		ENDIF
		
		// Make sure the ped will have some health
        IF g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] <= 0
            g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] = 100
			
		// give ped at least a minimum of 10% health
		ELIF g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] <= 10
		 	g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] = 10
        ENDIF
		
	    // Health is stored as a percentage so set it as a percentage of the max health => (perc / 100) * (max-100) + 100
		SET_ENTITY_HEALTH(ped, ROUND(((g_savedGlobals.sPlayerData.sInfo.fHealthPerc[ePed] / 100.0) * (TO_FLOAT(GET_PED_MAX_HEALTH(ped)) - 100.0)) + 100.0))
    			
	ENDIF
ENDPROC

/// PURPOSE: Stores the player peds armour.
PROC STORE_PLAYER_PED_ARMOUR(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		#if USE_CLF_DLC
 			g_savedGlobalsClifford.sPlayerData.sInfo.iArmour[ePed] = GET_PED_ARMOUR(ped)		
		#endif
		#if USE_NRM_DLC
 			g_savedGlobalsnorman.sPlayerData.sInfo.iArmour[ePed] = GET_PED_ARMOUR(ped)		
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
 			g_savedGlobals.sPlayerData.sInfo.iArmour[ePed] = GET_PED_ARMOUR(ped)
		#endif
		#endif
    ENDIF
ENDPROC
/// PURPOSE: Sets the player peds armour.
PROC RESTORE_PLAYER_PED_ARMOURCLF(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)	
        ADD_ARMOUR_TO_PED(ped, g_savedGlobalsClifford.sPlayerData.sInfo.iArmour[ePed]-GET_PED_ARMOUR(ped))		
    ENDIF
ENDPROC
/// PURPOSE: Sets the player peds armour.
PROC RESTORE_PLAYER_PED_ARMOURNRM(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)		
        ADD_ARMOUR_TO_PED(ped, g_savedGlobalsnorman.sPlayerData.sInfo.iArmour[ePed]-GET_PED_ARMOUR(ped))		
    ENDIF
ENDPROC
/// PURPOSE: Sets the player peds armour.
PROC RESTORE_PLAYER_PED_ARMOUR(PED_INDEX ped)
    #if USE_CLF_DLC
    	RESTORE_PLAYER_PED_ARMOURCLF(ped)
		exit
	#endif
	#if USE_NRM_DLC
   	 	RESTORE_PLAYER_PED_ARMOURNRM(ped)
		exit
	#endif
	
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)	
      	 ADD_ARMOUR_TO_PED(ped, g_savedGlobals.sPlayerData.sInfo.iArmour[ePed]-GET_PED_ARMOUR(ped))		
    ENDIF
ENDPROC

/// PURPOSE: Stores the player peds coordinates and heading.
PROC STORE_PLAYER_PED_POSITION(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		
		#IF IS_DEBUG_BUILD
		PRINTSTRING("<")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING("> STORE_PLAYER_PED_POSITION(")
		PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
		PRINTSTRING(", ")
		PRINTVECTOR(GET_ENTITY_COORDS(ped))
		PRINTSTRING(", ")
		PRINTFLOAT(GET_ENTITY_HEADING(ped))
		PRINTSTRING("):")
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			SWITCH_TYPE eSwitchType = GET_PLAYER_SWITCH_TYPE()
			PRINTSTRING(Get_String_From_Switch_Type(eSwitchType))
			
			IF (eSwitchType <> SWITCH_TYPE_SHORT)
				SWITCH_STATE eSwitchState = GET_PLAYER_SWITCH_STATE()
				
				PRINTSTRING(", ")
				PRINTSTRING(Get_String_From_Switch_State(eSwitchState))
				
			ENDIF
			
		ELSE
			PRINTSTRING("no switch")
		ENDIF
		
		PRINTNL()
		#ENDIF
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		AND (GET_PLAYER_SWITCH_TYPE() <> SWITCH_TYPE_SHORT)
			//
			
			IF GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
				EXIT
			ENDIF
		ENDIF
		
		#if USE_CLF_DLC
			g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed] = GET_ENTITY_COORDS(ped)
			g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed] = GET_ENTITY_HEADING(ped)
			g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = GET_ROOM_KEY_FROM_ENTITY(ped)
			
			//clamp last known coords for world limits #1578363
			IF (g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].x >= 8000.0)
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].x = 7500.0
			ELIF (g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].x <= -8000.0)
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].x = -7500.0
			ENDIF
			
			IF (g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].y >= 8000.0)
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].y = 7500.0
			ELIF (g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].y <= -8000.0)
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].y = -7500.0
			ENDIF
			
			IF (g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].z >= 2500.0)
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].z = 2000.0
			ENDIF		
		#endif
		#if USE_NRM_DLC
			g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed] = GET_ENTITY_COORDS(ped)
			g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed] = GET_ENTITY_HEADING(ped)
			g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = GET_ROOM_KEY_FROM_ENTITY(ped)
			
			//clamp last known coords for world limits #1578363
			IF (g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].x >= 8000.0)
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].x = 7500.0
			ELIF (g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].x <= -8000.0)
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].x = -7500.0
			ENDIF
			
			IF (g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].y >= 8000.0)
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].y = 7500.0
			ELIF (g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].y <= -8000.0)
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].y = -7500.0
			ENDIF
			
			IF (g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].z >= 2500.0)
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].z = 2000.0
			ENDIF		
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = GET_ENTITY_COORDS(ped)
			g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = GET_ENTITY_HEADING(ped)
			g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = GET_ROOM_KEY_FROM_ENTITY(ped)
			
			//clamp last known coords for world limits #1578363
			IF (g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].x >= 8000.0)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].x = 7500.0
			ELIF (g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].x <= -8000.0)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].x = -7500.0
			ENDIF
			
			IF (g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].y >= 8000.0)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].y = 7500.0
			ELIF (g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].y <= -8000.0)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].y = -7500.0
			ENDIF
			
			IF (g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].z >= 2500.0)
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].z = 2000.0
			ENDIF		
		#endif		
		#endif 
		

		
		
    ENDIF
ENDPROC

/// PURPOSE: Sets the player peds coordinates and heading.
PROC RESTORE_PLAYER_PED_POSITION(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
	
		#if USE_CLF_DLC
			#IF IS_DEBUG_BUILD
				PRINTSTRING("<")PRINTSTRING(GET_THIS_SCRIPT_NAME())	PRINTSTRING("> RESTORE_PLAYER_PED_POSITION(")	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
				PRINTSTRING(", ")PRINTVECTOR(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING(", ")
				PRINTFLOAT(g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING(", ")
				
				SWITCH g_sPlayerPedRequest.eState
					CASE PR_STATE_WAITING
						PRINTSTRING("PR_STATE_WAITING")
					BREAK
					CASE PR_STATE_PROCESSING
						PRINTSTRING("PR_STATE_PROCESSING")
					BREAK
					CASE PR_STATE_COMPLETE
						PRINTSTRING("PR_STATE_COMPLETE")
					BREAK
				ENDSWITCH
				
				PRINTNL()
			#ENDIF
				
			IF (g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed] = 0.0)
			AND ARE_VECTORS_EQUAL(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
				EXIT
			ENDIF
			
	        SET_ENTITY_COORDS_NO_OFFSET(ped, g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed])
	        SET_ENTITY_HEADING(ped, g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed])			
		#endif
		#if USE_NRM_DLC
			#IF IS_DEBUG_BUILD
				PRINTSTRING("<")PRINTSTRING(GET_THIS_SCRIPT_NAME())	PRINTSTRING("> RESTORE_PLAYER_PED_POSITION(")	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
				PRINTSTRING(", ")PRINTVECTOR(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING(", ")
				PRINTFLOAT(g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING(", ")
				
				SWITCH g_sPlayerPedRequest.eState
					CASE PR_STATE_WAITING
						PRINTSTRING("PR_STATE_WAITING")
					BREAK
					CASE PR_STATE_PROCESSING
						PRINTSTRING("PR_STATE_PROCESSING")
					BREAK
					CASE PR_STATE_COMPLETE
						PRINTSTRING("PR_STATE_COMPLETE")
					BREAK
				ENDSWITCH
				
				PRINTNL()
			#ENDIF
				
			IF (g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed] = 0.0)
			AND ARE_VECTORS_EQUAL(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
				EXIT
			ENDIF
			
	        SET_ENTITY_COORDS_NO_OFFSET(ped, g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed])
	        SET_ENTITY_HEADING(ped, g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed])			
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			#IF IS_DEBUG_BUILD
				PRINTSTRING("<")PRINTSTRING(GET_THIS_SCRIPT_NAME())	PRINTSTRING("> RESTORE_PLAYER_PED_POSITION(")	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
				PRINTSTRING(", ")PRINTVECTOR(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING(", ")
				PRINTFLOAT(g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING(", ")
				
				SWITCH g_sPlayerPedRequest.eState
					CASE PR_STATE_WAITING
						PRINTSTRING("PR_STATE_WAITING")
					BREAK
					CASE PR_STATE_PROCESSING
						PRINTSTRING("PR_STATE_PROCESSING")
					BREAK
					CASE PR_STATE_COMPLETE
						PRINTSTRING("PR_STATE_COMPLETE")
					BREAK
				ENDSWITCH
				
				PRINTNL()
			#ENDIF
			
			IF (g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = 0.0)
			AND ARE_VECTORS_EQUAL(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed], <<0,0,0>>)
				EXIT
			ENDIF
			
	        SET_ENTITY_COORDS_NO_OFFSET(ped, g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed])
	        SET_ENTITY_HEADING(ped, g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed])		
		#endif
		#endif
		
		
    ENDIF
ENDPROC

/// PURPOSE: Stores the player peds velocity
PROC STORE_PLAYER_PED_VELOCITY(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		#if USE_CLF_DLC
      	  g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed] = GET_ENTITY_VELOCITY(ped)
		#endif
		#if USE_NRM_DLC
      	  g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed] = GET_ENTITY_VELOCITY(ped)
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
      	  g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed] = GET_ENTITY_VELOCITY(ped)
		#endif
		#endif
    ENDIF
ENDPROC

/// PURPOSE: Sets the player peds velocity
PROC RESTORE_PLAYER_PED_VELOCITY(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		#if USE_CLF_DLC
    	    SET_ENTITY_VELOCITY(ped, g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#endif
		#if USE_NRM_DLC
    	    SET_ENTITY_VELOCITY(ped, g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
     	   SET_ENTITY_VELOCITY(ped, g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#endif
		#endif
    ENDIF
ENDPROC

FUNC VECTOR MODIFY_VEHICLE_NODE_FOR_LANES(VECTOR vInCoord, FLOAT fInHead, INT iNumLanes)
	FLOAT fModDist
	
	IF iNumLanes <= 1
		fModDist = 0
	ELSE
		fModDist = 2.8
	ENDIF
	
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInCoord, fInHead, <<fModDist, 0, 0>>)
ENDFUNC


FUNC BOOL GetVehicleOffsetFromCoord(VECTOR vPrevCoord, FLOAT vPrevHead,
		FLOAT fDistance, NODE_FLAGS eNodeFlag,
		VECTOR &vVehicleCoord_a, FLOAT &fVehicleHead_a)
	
	IF ARE_VECTORS_ALMOST_EQUAL(vPrevCoord, <<0,0,0>>, 1.5)
		vVehicleCoord_a = <<0,0,0>>
		fVehicleHead_a = 0.0
		
		RETURN FALSE
	ENDIF
	
	INT iTotalLanes
	
	VECTOR vVehicleCoord
	FLOAT fVehicleHead
	
	FLOAT fRandomOffsetX = GET_RANDOM_FLOAT_IN_RANGE(-fDistance/2,fDistance/2) * 0.5
	
	VECTOR vOffsetFromPed=GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPrevCoord, vPrevHead, <<fRandomOffsetX,fDistance,0>>)
	
	/*
	IF GET_CLOSEST_VEHICLE_NODE_WITH_HEADING(vOffsetFromPed, vVehicleCoord, fVehicleHead, eNodeFlag)
		vVehicleCoord_a = vVehicleCoord
		fVehicleHead_a = fVehicleHead
		
		RETURN TRUE
	ENDIF
	*/
	
	IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vOffsetFromPed, 1, vVehicleCoord, fVehicleHead, iTotalLanes, eNodeFlag)
		vVehicleCoord_a = vVehicleCoord
		fVehicleHead_a = fVehicleHead
		
		vVehicleCoord_a = MODIFY_VEHICLE_NODE_FOR_LANES(vVehicleCoord_a, fVehicleHead_a, iTotalLanes)
		
		RETURN TRUE
	ENDIF
	
	vVehicleCoord_a = vPrevCoord
	fVehicleHead_a = vPrevHead
	
	RETURN FALSE
ENDFUNC

FUNC BOOL STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(PED_INDEX ped, 
		VECTOR &vChosenPlaneCoord,
		FLOAT &fChosenPlaneHead)
	
	CONST_INT iPLANE_LOCATIONS	20
	VECTOR vPlaneCoords[iPLANE_LOCATIONS]
	FLOAT fPlaneHead[iPLANE_LOCATIONS]
	
	INT iCount = 0
	vPlaneCoords[iCount] = <<-988.0314, -937.3164, 247.4575>>	fPlaneHead[iCount] =21.7565		iCount++
	vPlaneCoords[iCount] = <<-1190.6515, -1442.3416, 274.3851>>	fPlaneHead[iCount] =4.3914		iCount++
	vPlaneCoords[iCount] = <<-1552.3132, 384.6868, 103.5778>>	fPlaneHead[iCount] =313.7197	iCount++
	vPlaneCoords[iCount] = <<-1807.5070, 2422.1606, 238.0954>>	fPlaneHead[iCount] =266.1874	iCount++
	vPlaneCoords[iCount] = <<-676.9284, 4432.5078, 360.7240>>	fPlaneHead[iCount] =272.4361	iCount++
	vPlaneCoords[iCount] = <<2049.1240, 6062.8164, 294.5194>>	fPlaneHead[iCount] =225.8041	iCount++
	vPlaneCoords[iCount] = <<2043.3855, 3475.9351, 42.2975>>	fPlaneHead[iCount] =116.8867	iCount++
	vPlaneCoords[iCount] = <<2634.4377, 2338.4937, 254.4784>>	fPlaneHead[iCount] =142.5168	iCount++
	vPlaneCoords[iCount] = <<1590.3884, 1001.1693, 287.6988>>	fPlaneHead[iCount] =134.4989	iCount++
	vPlaneCoords[iCount] = <<2056.5725, -632.6298, 94.2403>>	fPlaneHead[iCount] =124.6985	iCount++
	vPlaneCoords[iCount] = <<1558.5808, -2094.0803, 302.2927>>	fPlaneHead[iCount] =97.0006		iCount++
	vPlaneCoords[iCount] = <<1135.8334, -3115.1716, 4.8009>>	fPlaneHead[iCount] =42.7435		iCount++
	vPlaneCoords[iCount] = <<-428.0257, -2891.2900, 181.5473>>	fPlaneHead[iCount] =2.7579		iCount++
	vPlaneCoords[iCount] = <<-86.4255, -1548.1797, 272.6201>>	fPlaneHead[iCount] =335.0247	iCount++
	vPlaneCoords[iCount] = <<282.3189, 210.1434, 212.4410>>		fPlaneHead[iCount] =349.6890	iCount++
	vPlaneCoords[iCount] = <<824.9931, 2404.9763, 239.2513>>	fPlaneHead[iCount] =355.0965	iCount++
	vPlaneCoords[iCount] = <<1190.4857, 4000.3528, 235.3204>>	fPlaneHead[iCount] =328.8051	iCount++
	vPlaneCoords[iCount] = <<-3013.5188, 1294.3378, 31.2512>>	fPlaneHead[iCount] =244.3188	iCount++
	vPlaneCoords[iCount] = <<-205.1947, 1965.1676, 181.5578>>	fPlaneHead[iCount] =295.9389	iCount++
	vPlaneCoords[iCount] = <<-457.0207, 6013.2930, 35.6048>>	fPlaneHead[iCount] =313.8458	iCount++
	
	VECTOR vPedCoord = GET_ENTITY_COORDS(ped)
	
	FLOAT fShortDist = 9999999.99
	INT iClosestCount = -1
	
	iCount = 0
	FOR iCount = 0 TO iPLANE_LOCATIONS-1
		FLOAT fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vPedCoord, vPlaneCoords[iCount])
		IF fCurrentDist < fShortDist
			fShortDist = fCurrentDist
			iClosestCount = iCount
		ENDIF
	ENDFOR
	
	IF iClosestCount >= 0 AND iClosestCount < iPLANE_LOCATIONS
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 str = "store default plane coords (in air:"
		str += iClosestCount
		str += ")"
		PRINTLN(str)
		#ENDIF
		
		vChosenPlaneCoord = vPlaneCoords[iClosestCount]
		fChosenPlaneHead = fPlaneHead[iClosestCount]
		RETURN TRUE
	ELSE
		SCRIPT_ASSERT("store default plane coords (in air:invalid)")
		
		vChosenPlaneCoord = <<0,0,0>>
		fChosenPlaneHead = 0
		RETURN FALSE
	ENDIF
ENDFUNC

#IF USE_TU_CHANGES 
FUNC BOOL SAFE_TO_STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(VEHICLE_INDEX vehPlane)
	
	PRINTLN("SAFE_TO_STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE - ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehPlane), " for bug #1768229")
	
	
	IF (GET_ENTITY_MODEL(vehPlane) = BLIMP)
		PRINTLN("	\"GET_ENTITY_MODEL(vehPlane) = BLIMP\"")
		
		RETURN FALSE
	ENDIF
		
	IF NOT IS_ENTITY_IN_AIR(vehPlane)
		PRINTLN("	\"NOT IS_ENTITY_IN_AIR(vehPlane)\"")
		
		RETURN FALSE
	ENDIF
	
	FLOAT FFF
	IF GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(vehPlane), FFF)
		PRINTLN("	\"GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(vehPlane), ", FFF, ")...\"")
	ELSE
		PRINTLN("	\"NOT GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(vehPlane), ", FFF, ")\"")
		RETURN FALSE
	ENDIF
	
	PRINTLN("	\"safe to store default!!!\"")
	RETURN TRUE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Checks whether the ped is currently in scuba gear and stores for transition.
/// PARAMS:
///    ped - pedIndex
///    ePed - enumCharList
PROC STORE_DEFAULT_PLAYER_SCUBA_SWITCH_STATE(PED_INDEX ped, enumCharacterList ePed)

	BOOL bPedInScuba = IS_USING_PED_SCUBA_GEAR_VARIATION(ped)
	BOOL bPedInWater = IS_ENTITY_IN_WATER(ped)

	CDEBUG3LN(DEBUG_SWITCH, " - player_ped_public - STORE_DEFAULT_PLAYER_SCUBA_STATE - Checking scuba state now for ePed (enumCharList): ", ePed, "...")
	CDEBUG3LN(DEBUG_SWITCH, " - player_ped_public - STORE_DEFAULT_PLAYER_SCUBA_STATE - bPedInScuba: ", PICK_STRING(bPedInScuba, "TRUE", "FALSE"), " - bPedInWater: ", PICK_STRING(bPedInWater, "TRUE", "FALSE"))

	IF bPedInScuba
	AND bPedInWater
		CDEBUG1LN(DEBUG_SWITCH, " - player_ped_public - STORE_DEFAULT_PLAYER_SCUBA_STATE - Setting g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba to TRUE for (enumCharacterList):", ePed)
		g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba = TRUE
	ELSE
		CDEBUG3LN(DEBUG_SWITCH, " - player_ped_public - STORE_DEFAULT_PLAYER_SCUBA_STATE - Ped (enumCharacterList):", ePed, " was detected as NOT being in scuba gear.")
		g_sDefaultPlayerSwitchState[ePed].bPlayerInScuba = FALSE	//	Not neccessary but failsafe incase left on somewhere.
	ENDIF

ENDPROC

/// PURPOSE: 
PROC STORE_DEFAULT_PLAYER_VEHICLE_SWITCH_STATE(PED_INDEX ped, enumCharacterList ePed)
	CONST_FLOAT fDistanceA	 50.0
	CONST_FLOAT fDistanceB	150.0
	CONST_FLOAT fDistanceC	500.0
	
	
	CONST_FLOAT MIN_SPEED		10.0
	CONST_FLOAT MAX_SPEED		20.0
	
	CONST_FLOAT MIN_PLANE_SPEED	40.0
	
	FLOAT fPlayerSpeed = GET_ENTITY_SPEED(ped)
	
	
	g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced = FALSE
	
	IF IS_PED_IN_FLYING_VEHICLE(ped)
		VEHICLE_INDEX vehPlane = GET_VEHICLE_PED_IS_IN(ped)
		
#IF NOT USE_TU_CHANGES 
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SWITCH, "SAFE_TO_STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE - ", GET_MODEL_NAME_FOR_DEBUG(vehPlane), " for bug #1768229 (NOT USE_TU_CHANGES")
			#ENDIF
		IF IS_ENTITY_IN_AIR(vehPlane) 
#ENDIF
		
#IF USE_TU_CHANGES 
		IF SAFE_TO_STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(vehPlane) 
#ENDIF
		
			IF STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(ped, g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a, g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a)
			AND STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(ped, g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b, g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b)
			AND STORE_DEFAULT_PLAYER_PLANE_SWITCH_STATE(ped, g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c, g_sDefaultPlayerSwitchState[ePed].fVehicleHead_c)
				
				IF IS_PED_IN_ANY_PLANE(ped)
					IF fPlayerSpeed < MIN_PLANE_SPEED
						fPlayerSpeed = MIN_PLANE_SPEED
					ENDIF
				ELSE
					IF fPlayerSpeed > MAX_SPEED
						fPlayerSpeed = MAX_SPEED
					ELIF fPlayerSpeed < MIN_SPEED
						fPlayerSpeed = MIN_SPEED
					ENDIF
				ENDIF
				
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = fPlayerSpeed
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = fPlayerSpeed
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = fPlayerSpeed
				
				IF NOT ARE_VECTORS_ALMOST_EQUAL(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a, <<0,0,0>>, 1.5)
					#if USE_CLF_DLC
						g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed]		= g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a
						g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed]			= g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a
					#endif
					#if USE_NRM_DLC
						g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed]		= g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a
						g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed]			= g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a
					#endif
					#if not USE_CLF_DLC
					#if not USE_NRM_DLC
						g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]			= g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a
						g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]			= g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a
					#endif
					#endif
				ENDIF
				
				EXIT
			ENDIF
			
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
			
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
			
			SCRIPT_ASSERT("store default flying vehicle coords (in air-not found?)")
			
			EXIT
			
#IF NOT USE_TU_CHANGES 
		ELSE
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
			
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
			
			#IF IS_DEBUG_BUILD
				IF (GET_ENTITY_MODEL(vehPlane) = BLIMP)
					CPRINTLN(DEBUG_SWITCH, "store default flying vehicle coords (blimp)")
				ELSE	
					CPRINTLN(DEBUG_SWITCH, "store default flying vehicle coords (on land)")
				ENDIF
			#ENDIF
			
			EXIT
		ENDIF
#ENDIF

#IF USE_TU_CHANGES 
		ELSE
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
			
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
			g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
			
			#IF IS_DEBUG_BUILD
				IF (GET_ENTITY_MODEL(vehPlane) = BLIMP)
					CPRINTLN(DEBUG_SWITCH, "store default flying vehicle coords (blimp)")
				ELSE	
					CPRINTLN(DEBUG_SWITCH, "store default flying vehicle coords (on land)")
				ENDIF
			#ENDIF
			
			EXIT
		ENDIF
#ENDIF
		
	ENDIF
	
	IF IS_PED_IN_ANY_BOAT(ped) OR IS_PED_IN_ANY_SUB(ped)
		// //
		g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
		g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
		g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
		
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SWITCH, "	STORE_DEFAULT_PLAYER_BOAT_SWITCH_STATE(vVehicleCoord_a:", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a, ")	//", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a)
		CPRINTLN(DEBUG_SWITCH, "	STORE_DEFAULT_PLAYER_BOAT_SWITCH_STATE(vVehicleCoord_b:", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b, ")	//", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b)
		CPRINTLN(DEBUG_SWITCH, "	STORE_DEFAULT_PLAYER_BOAT_SWITCH_STATE(vVehicleCoord_c:", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c, ")	//", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c)
		CPRINTLN(DEBUG_SWITCH, "")
		#ENDIF
		
		EXIT
	ENDIF
	
	
	IF fPlayerSpeed > MAX_SPEED
		fPlayerSpeed = MAX_SPEED
	ELIF fPlayerSpeed < MIN_SPEED
		fPlayerSpeed = MIN_SPEED
	ENDIF
	
	// //
	IF GetVehicleOffsetFromCoord(GET_ENTITY_COORDS(ped), GET_ENTITY_HEADING(ped),
			fDistanceA, 
			NF_IGNORE_SLIPLANES|NF_IGNORE_SWITCHED_OFF_DEADENDS,
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a,
			g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a)
		
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = fPlayerSpeed
	ELSE
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
	ENDIF
	
	IF GetVehicleOffsetFromCoord(g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a, g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a,
			fDistanceB-fDistanceA, 
			NF_IGNORE_SLIPLANES|NF_IGNORE_SWITCHED_OFF_DEADENDS,
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b,
			g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b)
			
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = fPlayerSpeed
	ELSE
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
	ENDIF
	
	IF GetVehicleOffsetFromCoord(g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b, g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b,
			fDistanceC-fDistanceB, 
			NF_IGNORE_SLIPLANES|NF_IGNORE_SWITCHED_OFF_DEADENDS,
			g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c,
			g_sDefaultPlayerSwitchState[ePed].fVehicleHead_c)

		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = fPlayerSpeed
	ELSE
		g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
	ENDIF
	
	g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced = FALSE
	IF IS_PED_IN_ANY_VEHICLE(ped)
		VEHICLE_INDEX vehSubmerged = GET_VEHICLE_PED_IS_IN(ped)
		
		#IF IS_DEBUG_BUILD
		SAVE_STRING_TO_DEBUG_FILE("submerged: ")
		SAVE_FLOAT_TO_DEBUG_FILE(GET_ENTITY_SUBMERGED_LEVEL(vehSubmerged))
		SAVE_STRING_TO_DEBUG_FILE(", on_all_wheels: ")
		SAVE_BOOL_TO_DEBUG_FILE(IS_VEHICLE_ON_ALL_WHEELS(vehSubmerged))
		SAVE_NEWLINE_TO_DEBUG_FILE()
		#ENDIF
		
		IF GET_ENTITY_SUBMERGED_LEVEL(vehSubmerged) > 0.0
		AND IS_VEHICLE_ON_ALL_WHEELS(vehSubmerged)
			CPRINTLN(DEBUG_SWITCH, "g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced [players vehicle is sitting in water]")
			g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced = TRUE
		ENDIF
	ENDIF
	
	INT iPlayerHashKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
	IF (iPlayerHashKey = HASH("") AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<361.761,-593.973,27.415>>, <<301.502,-570.014,73.120>>, 26.5))
	OR (iPlayerHashKey = HASH("") AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<331.066,-601.842,56.725>>, <<285.118,-584.950,41.253>>, 26.5))
	OR (iPlayerHashKey = HASH("v_40_Room1"))	// entrance
	OR (iPlayerHashKey = HASH("v_40_Room2"))	// corridor between two broken walls
	OR (iPlayerHashKey = HASH("v_40_Room3"))	// ward (near broken wall)
	OR (iPlayerHashKey = HASH("v_40_Room4"))	// ward area (past final broken wall)
	OR (iPlayerHashKey = HASH("V_40_Room005"))	// entrance from shop to first broken wall
		CPRINTLN(DEBUG_SWITCH, "g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced [player is in locked Nigel2 hospital:", iPlayerHashKey, "]")
		g_sDefaultPlayerSwitchState[ePed].bVehicleCoordForced = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SWITCH, "	STORE_DEFAULT_PLAYER_VEHICLE_SWITCH_STATE(", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a, ", ", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b, ", ", g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c, ")	//", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a, ", ", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b, ", ", g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c)
	#ENDIF
	
ENDPROC


FUNC BOOL GetPedOffsetFromCoord(VECTOR vPrevCoord, FLOAT vPrevHead,
		FLOAT fDistance, //NODE_FLAGS eNodeFlag,
		VECTOR &vWalkCoord_a, FLOAT &fWalkHead_a)
	
	
	GET_SAFE_COORD_FLAGS eSafeCoordFlag = GSC_FLAG_NOT_ISOLATED|GSC_FLAG_NOT_WATER|GSC_FLAG_NOT_INTERIOR
	INT iTotalLanes
	NODE_FLAGS eNodeFlag = NF_NONE
	
	VECTOR vSafeWalktoCoord, vRoadCoord
	FLOAT fSafeWalktoGroundZ, fRoadHead
	
	CONST_INT iNUM_OF_ANGLES_TO_CHECK	3
	INT iAngle
	
	REPEAT iNUM_OF_ANGLES_TO_CHECK iAngle
			
		FLOAT fDebugJumpAngle = (TO_FLOAT(iAngle) / TO_FLOAT(iNUM_OF_ANGLES_TO_CHECK)) * 360.0
		VECTOR vOffsetAngle = <<SIN(fDebugJumpAngle), COS(fDebugJumpAngle), 0.0>> 
		
		VECTOR vOffsetFromPed = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPrevCoord, vPrevHead, vOffsetAngle * fDistance)
		IF NOT GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vOffsetFromPed, 1, vRoadCoord, fRoadHead, iTotalLanes, eNodeFlag)
			fRoadHead = GET_RANDOM_FLOAT_IN_RANGE(-180,180)
		ENDIF
		
		IF GET_SAFE_COORD_FOR_PED(vOffsetFromPed, TRUE, vSafeWalktoCoord, eSafeCoordFlag | GSC_FLAG_ONLY_NETWORK_SPAWN | GSC_FLAG_ONLY_PAVEMENT)
			vWalkCoord_a = vSafeWalktoCoord
			fWalkHead_a = fRoadHead
			
			VECTOR vSafeWalktoTarget = <<0,0,0>>
			VECTOR vSafeWalktoOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSafeWalktoCoord, fWalkHead_a, <<0,5,0>>)
			IF GET_SAFE_COORD_FOR_PED(vSafeWalktoOffset, TRUE, vSafeWalktoTarget, eSafeCoordFlag)
				VECTOR vSafeCoordDiff = vSafeWalktoTarget - vWalkCoord_a
				fWalkHead_a = GET_HEADING_FROM_VECTOR_2D(vSafeCoordDiff.x, vSafeCoordDiff.y)
				
				IF GET_GROUND_Z_FOR_3D_COORD(vWalkCoord_a, fSafeWalktoGroundZ)
					
					PRINTSTRING("NET AND PAV ")
					PRINTINT(iAngle)
					PRINTSTRING(": ")
					PRINTVECTOR(vOffsetAngle)
					PRINTSTRING(" * ")
					PRINTFLOAT(fDistance)
					PRINTNL()
					
					vWalkCoord_a.z = fSafeWalktoGroundZ
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF GET_SAFE_COORD_FOR_PED(vOffsetFromPed, TRUE, vSafeWalktoCoord, eSafeCoordFlag | GSC_FLAG_ONLY_PAVEMENT)
			vWalkCoord_a = vSafeWalktoCoord
			fWalkHead_a = fRoadHead
			
			VECTOR vSafeWalktoTarget = <<0,0,0>>
			VECTOR vSafeWalktoOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSafeWalktoCoord, fWalkHead_a, <<0,5,0>>)
			IF GET_SAFE_COORD_FOR_PED(vSafeWalktoOffset, TRUE, vSafeWalktoTarget, eSafeCoordFlag)
				VECTOR vSafeCoordDiff = vSafeWalktoTarget - vWalkCoord_a
				fWalkHead_a = GET_HEADING_FROM_VECTOR_2D(vSafeCoordDiff.x, vSafeCoordDiff.y)
				
				IF GET_GROUND_Z_FOR_3D_COORD(vWalkCoord_a, fSafeWalktoGroundZ)
					
					PRINTSTRING("JUST PAV ")
					PRINTINT(iAngle)
					PRINTSTRING(": ")
					PRINTVECTOR(vOffsetAngle)
					PRINTSTRING(" * ")
					PRINTFLOAT(fDistance)
					PRINTNL()
					
					vWalkCoord_a.z = fSafeWalktoGroundZ
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		IF GET_SAFE_COORD_FOR_PED(vOffsetFromPed, TRUE, vSafeWalktoCoord, eSafeCoordFlag)
			vWalkCoord_a = vSafeWalktoCoord
			fWalkHead_a = fRoadHead
			
			VECTOR vSafeWalktoTarget = <<0,0,0>>
			VECTOR vSafeWalktoOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSafeWalktoCoord, fWalkHead_a, <<0,5,0>>)
			IF GET_SAFE_COORD_FOR_PED(vSafeWalktoOffset, TRUE, vSafeWalktoTarget, eSafeCoordFlag)
				VECTOR vSafeCoordDiff = vSafeWalktoTarget - vWalkCoord_a
				fWalkHead_a = GET_HEADING_FROM_VECTOR_2D(vSafeCoordDiff.x, vSafeCoordDiff.y)
				
				IF GET_GROUND_Z_FOR_3D_COORD(vWalkCoord_a, fSafeWalktoGroundZ)
					
					PRINTSTRING("default flags ")
					PRINTINT(iAngle)
					PRINTSTRING(": ")
					PRINTVECTOR(vOffsetAngle)
					PRINTSTRING(" * ")
					PRINTFLOAT(fDistance)
					PRINTNL()
					
					vWalkCoord_a.z = fSafeWalktoGroundZ
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		PED_INDEX closePed = NULL
		IF GET_CLOSEST_PED(vOffsetFromPed, 1.0, TRUE, FALSE, closePed)
			IF NOT IS_PED_INJURED(closePed)
			AND NOT IS_PED_IN_ANY_VEHICLE(closePed)
			AND NOT IS_PED_RAGDOLL(closePed)
				vWalkCoord_a = GET_ENTITY_COORDS(closePed)
				fWalkHead_a = GET_ENTITY_HEADING(closePed)
				
				VECTOR vSafeWalktoTarget = <<0,0,0>>
				VECTOR vSafeWalktoOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSafeWalktoCoord, fRoadHead, <<0,5,0>>)
				IF GET_SAFE_COORD_FOR_PED(vSafeWalktoOffset, TRUE, vSafeWalktoTarget, eSafeCoordFlag)
					VECTOR vSafeCoordDiff = vSafeWalktoTarget - vWalkCoord_a
					fWalkHead_a = GET_HEADING_FROM_VECTOR_2D(vSafeCoordDiff.x, vSafeCoordDiff.y)
					
					IF GET_GROUND_Z_FOR_3D_COORD(vWalkCoord_a, fSafeWalktoGroundZ)
						
						PRINTSTRING("from ped[ ")
						PRINTSTRING(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(closePed)))
						PRINTSTRING("] ")
						PRINTINT(iAngle)
						PRINTSTRING(": ")
						PRINTVECTOR(vOffsetAngle)
						PRINTSTRING(" * ")
						PRINTFLOAT(fDistance)
						PRINTNL()
						
						vWalkCoord_a.z = fSafeWalktoGroundZ
						RETURN TRUE
					ENDIF
				ENDIF

			ENDIF
		ENDIF
		
		PRINTSTRING("failed at ")
		PRINTINT(iAngle)
		PRINTSTRING(": ")
		PRINTVECTOR(vOffsetAngle)
		PRINTSTRING(" * ")
		PRINTFLOAT(fDistance)
		PRINTNL()
		
	ENDREPEAT
	
	PRINTSTRING("same as prev... ")
	PRINTNL()
						
	
	vWalkCoord_a = vPrevCoord
	fWalkHead_a = vPrevHead
	RETURN FALSE
ENDFUNC


/// PURPOSE: 
PROC STORE_DEFAULT_PLAYER_WALKING_SWITCH_STATE(PED_INDEX ped, enumCharacterList ePed)
	
	GetPedOffsetFromCoord(GET_ENTITY_COORDS(ped), GET_ENTITY_HEADING(ped),
			10.0, //NODE_FLAGS eNodeFlag,
			g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a,
			g_sDefaultPlayerSwitchState[ePed].fWalkHead_a)
	
	GetPedOffsetFromCoord(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a, g_sDefaultPlayerSwitchState[ePed].fWalkHead_a,
			50.0, //NODE_FLAGS eNodeFlag,
			g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b,
			g_sDefaultPlayerSwitchState[ePed].fWalkHead_b)
	
	GetPedOffsetFromCoord(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b, g_sDefaultPlayerSwitchState[ePed].fWalkHead_b,
			100.0, //NODE_FLAGS eNodeFlag,
			g_sDefaultPlayerSwitchState[ePed].vWalkCoord_c,
			g_sDefaultPlayerSwitchState[ePed].fWalkHead_c)
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("	STORE_DEFAULT_PLAYER_WALKING_SWITCH_STATE(")
	PRINTVECTOR(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a)
	PRINTSTRING(", ")
	PRINTVECTOR(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b)
	PRINTSTRING(", ")
	PRINTVECTOR(g_sDefaultPlayerSwitchState[ePed].vWalkCoord_c)
	PRINTNL()
	#ENDIF
	
ENDPROC

/// PURPOSE: 
PROC STORE_DEFAULT_PLAYER_SWITCH_STATE(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		
		#IF IS_DEBUG_BUILD
		PRINTSTRING("STORE_DEFAULT_PLAYER_SWITCH_STATE(")
		PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
		PRINTSTRING(", ")
		PRINTVECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		PRINTSTRING(", ")
		PRINTFLOAT(GET_ENTITY_HEADING(PLAYER_PED_ID()))
		PRINTSTRING(")")
		PRINTNL()
		#ENDIF
		
		STORE_DEFAULT_PLAYER_SCUBA_SWITCH_STATE(ped, ePed)
		
		STORE_DEFAULT_PLAYER_VEHICLE_SWITCH_STATE(ped, ePed)
//		STORE_DEFAULT_PLAYER_WALKING_SWITCH_STATE(ped, ePed)
		
		#IF IS_DEBUG_BUILD
		PRINTNL()
		#ENDIF
		
    ENDIF
ENDPROC

/// PURPOSE: Stores the player peds wanted level
PROC STORE_PLAYER_PED_WANTED(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		
		#IF IS_DEBUG_BUILD
		PRINTSTRING("<")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING("> STORE_PLAYER_PED_WANTED(")
		PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
		PRINTSTRING(", ")
		PRINTINT(GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
		PRINTSTRING("):")
//		PRINTSTRING("g_sPlayerPedRequest.eType = ")
//		SWITCH g_sPlayerPedRequest.eType
//			CASE PR_TYPE_MISSION
//				PRINTSTRING("PR_TYPE_MISSION")
//			BREAK
//			CASE PR_TYPE_AMBIENT
//				PRINTSTRING("PR_TYPE_AMBIENT")
//			BREAK
//		ENDSWITCH
		
		PRINTSTRING(", ")
		SWITCH g_sPlayerPedRequest.eState
			CASE PR_STATE_WAITING
				PRINTSTRING("PR_STATE_WAITING")
			BREAK
			CASE PR_STATE_PROCESSING
				PRINTSTRING("PR_STATE_PROCESSING")
			BREAK
			CASE PR_STATE_COMPLETE
				PRINTSTRING("PR_STATE_COMPLETE")
			BREAK
		ENDSWITCH
		
		PRINTNL()
		#ENDIF
		
//		IF g_sPlayerPedRequest.eType = PR_TYPE_MISSION
//			IF g_sPlayerPedRequest.eState = PR_STATE_PROCESSING
//				EXIT
//			ENDIF
//		ENDIF
		
		IF (ped = PLAYER_PED_ID())
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownWantedLevel[ePed] = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			#endif
			#if USE_NRM_DLC
				g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownWantedLevel[ePed] = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				g_savedGlobals.sPlayerData.sInfo.iLastKnownWantedLevel[ePed] = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			#endif
			#endif
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTSTRING("<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> STORE_PLAYER_PED_WANTED(")
			PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
			PRINTSTRING("):")
			PRINTSTRING(" is not the player")
			PRINTNL()
			#ENDIF
			
		ENDIF
    ENDIF
ENDPROC

/// PURPOSE: Sets the player peds wanted level
PROC RESTORE_PLAYER_PED_WANTED(PED_INDEX ped)

    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		
		#IF IS_DEBUG_BUILD
		PRINTSTRING("<")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING("> RESTORE_PLAYER_PED_WANTED(")
		PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
		PRINTSTRING("):")

		PRINTSTRING(", ")
		SWITCH g_sPlayerPedRequest.eState
			CASE PR_STATE_WAITING
				PRINTSTRING("PR_STATE_WAITING")
			BREAK
			CASE PR_STATE_PROCESSING
				PRINTSTRING("PR_STATE_PROCESSING")
			BREAK
			CASE PR_STATE_COMPLETE
				PRINTSTRING("PR_STATE_COMPLETE")
			BREAK
		ENDSWITCH
		
		PRINTNL()
		#ENDIF
		
		
        IF (ped = PLAYER_PED_ID())
			#if USE_CLF_DLC
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownWantedLevel[ePed])
			#endif
			#if USE_NRM_DLC
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownWantedLevel[ePed])
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), g_savedGlobals.sPlayerData.sInfo.iLastKnownWantedLevel[ePed])
			#endif
			#endif
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTSTRING("<")
			PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING("> RESTORE_PLAYER_PED_WANTED(")
			PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
			PRINTSTRING("):")
			PRINTSTRING(" is not the player")
			PRINTNL()
			#ENDIF
		ENDIF
		
	ELSE
	
		#IF IS_DEBUG_BUILD
		PRINTSTRING("<")
		PRINTSTRING(GET_THIS_SCRIPT_NAME())
		PRINTSTRING("> RESTORE_PLAYER_PED_WANTED(")
		PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
		PRINTSTRING("):")
		
		BOOL bFailReason = FALSE
		IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
			PRINTSTRING("unplayable")
			bFailReason = TRUE
		ENDIF
		IF IS_PED_INJURED(ped)
			IF bFailReason
				PRINTSTRING(", ")
			ENDIF
			PRINTSTRING("injured")
			bFailReason = TRUE
		ENDIF
		IF NOT bFailReason
			PRINTSTRING("failed")
		ENDIF
		
		PRINTNL()
		#ENDIF
		
    ENDIF
ENDPROC

/// PURPOSE: Stores the current game time to mark last usage of player ped.
PROC STORE_PLAYER_PED_LAST_GAME_TIME(PED_INDEX ped)
    
    // Grab the ped enum
    enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
    IF IS_PLAYER_PED_PLAYABLE(ePed)
    AND NOT IS_PED_INJURED(ped)
		#if USE_CLF_DLC
     	   g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()
		#endif
		#if USE_NRM_DLC
     	   g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
        	g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()
		#endif
		#endif
    ENDIF
ENDPROC

/// PURPOSE: Calls all the seperate store procs
PROC STORE_PLAYER_PED_INFO(PED_INDEX ped, BOOL storeEquippedWeapon = FALSE)

    #IF IS_DEBUG_BUILD
		PRINTSTRING("\n STORE_PLAYER_PED_INFO - Storing info for ")PRINTSTRING(GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM(ped)))PRINTNL()
	#ENDIF
    
    STORE_PLAYER_PED_LAST_GAME_TIME(ped)
    //STORE_PLAYER_PED_VARIATIONS(ped)	// Variations now have to be stored manually.
    STORE_PLAYER_PED_WEAPONS(ped, storeEquippedWeapon)
    STORE_PLAYER_PED_HEALTH(ped)
    STORE_PLAYER_PED_ARMOUR(ped)
    STORE_PLAYER_PED_POSITION(ped)
	STORE_PLAYER_PED_VELOCITY(ped)
	STORE_PLAYER_PED_WANTED(ped)
ENDPROC

/// PURPOSE: Calls all the seperate restore procs
PROC RESTORE_PLAYER_PED_INFO(PED_INDEX ped)
    
    #IF IS_DEBUG_BUILD
		PRINTSTRING("\n RESTORE_PLAYER_PED_INFO - Restoring info for ")PRINTSTRING(GET_PLAYER_PED_STRING(GET_PLAYER_PED_ENUM(ped)))PRINTNL()
	#ENDIF
    
    RESTORE_PLAYER_PED_VARIATIONS(ped)
    RESTORE_PLAYER_PED_WEAPONS(ped)
    RESTORE_PLAYER_PED_HEALTH(ped)
    RESTORE_PLAYER_PED_ARMOUR(ped)
    RESTORE_PLAYER_PED_POSITION(ped)
	RESTORE_PLAYER_PED_VELOCITY(ped)
	RESTORE_PLAYER_PED_TATTOOS(ped)
ENDPROC



PROC SAFE_AMBIENT_STORE_PLAYER_PED_INFO(PED_INDEX ped)
	
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(ped)
	#if USE_CLF_DLC
 	  	VECTOR vLastKnownCoords = g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fLastKnownHead = g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed]
		INT iLastKnownRoomKey = g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[ePed]
		VECTOR vLastKnownVelocity = g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed]
	#endif
	#if USE_NRM_DLC
 	  	VECTOR vLastKnownCoords = g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fLastKnownHead = g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed]
		INT iLastKnownRoomKey = g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[ePed]
		VECTOR vLastKnownVelocity = g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed]
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
    	VECTOR vLastKnownCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fLastKnownHead = g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
		INT iLastKnownRoomKey = g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[ePed]
		VECTOR vLastKnownVelocity = g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed]
	#endif
	#endif
	STORE_PLAYER_PED_INFO(ped)
	
	IF NOT ARE_VECTORS_EQUAL(vLastKnownCoords, <<0,0,0>>)
	
	#if USE_CLF_DLC
		FLOAT fLastKnownCoordZAbsf = ABSF(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed].z - vLastKnownCoords.z)
		FLOAT fLastKnownCoordDist2 = VDIST2(vLastKnownCoords, g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed])
		FLOAT fLastKnownVelocityMag = VMAG(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#IF IS_DEBUG_BUILD
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(":AR_STORE_PLAYER_PED_INFO(")PRINTSTRING(GET_PLAYER_PED_STRING(ePed))PRINTSTRING(")")PRINTNL()		
			PRINTSTRING("	vLastKnownCoords[")PRINTVECTOR(vLastKnownCoords)PRINTSTRING("] <> g_vLastKnownCoords[")PRINTVECTOR(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING("], absf: ")PRINTFLOAT(fLastKnownCoordZAbsf)PRINTSTRING("], vdist: ")PRINTFLOAT(SQRT(fLastKnownCoordDist2))PRINTNL()
			PRINTSTRING("	fLastKnownHead[")PRINTFLOAT(fLastKnownHead)PRINTSTRING("] <> g_fLastKnownHead[")PRINTFLOAT(g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	iLastKnownRoomKey[")PRINTINT(iLastKnownRoomKey)PRINTSTRING("] <> g_iLastKnownRoomKey[")PRINTINT(g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	vLastKnownVelocity[")PRINTVECTOR(vLastKnownVelocity)PRINTSTRING(": ")PRINTFLOAT(VMAG(vLastKnownVelocity))PRINTSTRING("] <> g_vLastKnownVelocity[")PRINTVECTOR(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed])PRINTSTRING(": ")PRINTFLOAT(VMAG(g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed]))PRINTSTRING("]")PRINTNL()		
			PRINTNL()
		#ENDIF
	#endif
	#if USE_NRM_DLC
		FLOAT fLastKnownCoordZAbsf = ABSF(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed].z - vLastKnownCoords.z)
		FLOAT fLastKnownCoordDist2 = VDIST2(vLastKnownCoords, g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed])
		FLOAT fLastKnownVelocityMag = VMAG(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#IF IS_DEBUG_BUILD
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(":AR_STORE_PLAYER_PED_INFO(")PRINTSTRING(GET_PLAYER_PED_STRING(ePed))PRINTSTRING(")")PRINTNL()		
			PRINTSTRING("	vLastKnownCoords[")PRINTVECTOR(vLastKnownCoords)PRINTSTRING("] <> g_vLastKnownCoords[")PRINTVECTOR(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING("], absf: ")PRINTFLOAT(fLastKnownCoordZAbsf)PRINTSTRING("], vdist: ")PRINTFLOAT(SQRT(fLastKnownCoordDist2))PRINTNL()
			PRINTSTRING("	fLastKnownHead[")PRINTFLOAT(fLastKnownHead)PRINTSTRING("] <> g_fLastKnownHead[")PRINTFLOAT(g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	iLastKnownRoomKey[")PRINTINT(iLastKnownRoomKey)PRINTSTRING("] <> g_iLastKnownRoomKey[")PRINTINT(g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	vLastKnownVelocity[")PRINTVECTOR(vLastKnownVelocity)PRINTSTRING(": ")PRINTFLOAT(VMAG(vLastKnownVelocity))PRINTSTRING("] <> g_vLastKnownVelocity[")PRINTVECTOR(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed])PRINTSTRING(": ")PRINTFLOAT(VMAG(g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed]))PRINTSTRING("]")PRINTNL()		
			PRINTNL()
		#ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		FLOAT fLastKnownCoordZAbsf = ABSF(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed].z - vLastKnownCoords.z)
		FLOAT fLastKnownCoordDist2 = VDIST2(vLastKnownCoords, g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed])
		FLOAT fLastKnownVelocityMag = VMAG(g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed])
		#IF IS_DEBUG_BUILD
			PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING(":AR_STORE_PLAYER_PED_INFO(")PRINTSTRING(GET_PLAYER_PED_STRING(ePed))PRINTSTRING(")")PRINTNL()		
			PRINTSTRING("	vLastKnownCoords[")PRINTVECTOR(vLastKnownCoords)PRINTSTRING("] <> g_vLastKnownCoords[")PRINTVECTOR(g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed])PRINTSTRING("], absf: ")PRINTFLOAT(fLastKnownCoordZAbsf)PRINTSTRING("], vdist: ")PRINTFLOAT(SQRT(fLastKnownCoordDist2))PRINTNL()
			PRINTSTRING("	fLastKnownHead[")PRINTFLOAT(fLastKnownHead)PRINTSTRING("] <> g_fLastKnownHead[")PRINTFLOAT(g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	iLastKnownRoomKey[")PRINTINT(iLastKnownRoomKey)PRINTSTRING("] <> g_iLastKnownRoomKey[")PRINTINT(g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[ePed])PRINTSTRING("]")PRINTNL()
			PRINTSTRING("	vLastKnownVelocity[")PRINTVECTOR(vLastKnownVelocity)PRINTSTRING(": ")PRINTFLOAT(VMAG(vLastKnownVelocity))PRINTSTRING("] <> g_vLastKnownVelocity[")PRINTVECTOR(g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed])PRINTSTRING(": ")PRINTFLOAT(VMAG(g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed]))PRINTSTRING("]")PRINTNL()		
			PRINTNL()
		#ENDIF
	#endif
	#endif
		
		BOOL bRestoreStoredPedInfo = FALSE
		IF fLastKnownVelocityMag > 25.0
			
			CONST_FLOAT fCONST_RESTORE_PED_INFO_DIST	25.0		//100.0
			
			IF (fLastKnownCoordZAbsf > fCONST_RESTORE_PED_INFO_DIST)
				bRestoreStoredPedInfo = TRUE
			ENDIF
			IF (fLastKnownCoordDist2 > (fCONST_RESTORE_PED_INFO_DIST*fCONST_RESTORE_PED_INFO_DIST))
				bRestoreStoredPedInfo = TRUE
			ENDIF
		ENDIF
		
		#IF USE_TU_CHANGES
		IF NOT IS_PED_INJURED(ped)
		#ENDIF
		
			IF IS_PED_IN_ANY_VEHICLE(ped)
				bRestoreStoredPedInfo = TRUE
				
				PRINTLN("player ped ", GET_PLAYER_PED_STRING(ePed), " is in vehicle")
			ENDIF
			
			IF NOT HAS_COLLISION_LOADED_AROUND_ENTITY(ped)
				bRestoreStoredPedInfo = TRUE
				
				PRINTLN("no collision around player ped ", GET_PLAYER_PED_STRING(ePed), " being stored")
			ENDIF
		
		#IF USE_TU_CHANGES
		ENDIF
		#ENDIF
		
		IF bRestoreStoredPedInfo
			
			PRINTSTRING("	bRestoreStoredPedInfo")PRINTNL()
			#if USE_CLF_DLC
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed] = vLastKnownCoords
				g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed] = fLastKnownHead
				g_savedGlobalsClifford.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = iLastKnownRoomKey			
				g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownVelocity[ePed] = vLastKnownVelocity
			#endif
			#if USE_NRM_DLC
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed] = vLastKnownCoords
				g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed] = fLastKnownHead
				g_savedGlobalsnorman.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = iLastKnownRoomKey			
				g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownVelocity[ePed] = vLastKnownVelocity
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = vLastKnownCoords
				g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = fLastKnownHead
				g_savedGlobals.sPlayerData.sInfo.iLastKnownRoomKey[ePed] = iLastKnownRoomKey			
				g_savedGlobals.sPlayerData.sInfo.vLastKnownVelocity[ePed] = vLastKnownVelocity
			#endif
			#endif
		ELSE
			
			PRINTSTRING("	don't bRestoreStoredPedInfo")PRINTNL()
			
		ENDIF
	ENDIF
ENDPROC
#if USE_CLF_DLC
PROC ResetLastKnownPedInfoCLF(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)

	IF (eMissionID = SP_MISSION_MAX)
		SCRIPT_ASSERT("ResetLastKnownPedInfo: SP_MISSION_MAX is not valid mission IDs to pass.")
		EXIT
	ENDIF
	
	enumCharacterList ePed
	REPEAT NUM_OF_PLAYABLE_PEDS ePed
//		PED_REQUEST_SCENE_ENUM eOldScene = g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed]
		
			VECTOR vLastKnownCoords = <<0,0,0>>
			FLOAT fLastKnownHead = 0
			IF NOT GetLastKnownPedInfoPostMission(g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
				g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
				CLEAR_TIMEOFDAY(sInfo.sLastTimeActive[ePed])
				
				sInfo.vLastKnownCoords[ePed]		= <<0,0,0>>
				sInfo.fLastKnownHead[ePed]			= 0.0
				sInfo.iLastKnownRoomKey[ePed]		= 0
				sInfo.vLastKnownVelocity[ePed]		= <<0,0,0>>
				sInfo.iLastKnownWantedLevel[ePed]	= 0
				
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_c = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
				
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_c = 0
				
			ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF (eMissionID = SP_MISSION_NONE)
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", "SP_MISSION_NONE", ").")
	ELSE
		#if USE_CLF_DLC
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", GET_SCRIPT_FROM_HASH(g_sMissionStaticData[eMissionID].scriptHash), ").")		
		#endif
	ENDIF
	#ENDIF
	
ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC ResetLastKnownPedInfoNRM(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)

	IF (eMissionID = SP_MISSION_MAX_NRM)
		SCRIPT_ASSERT("ResetLastKnownPedInfo: SP_MISSION_MAX is not valid mission IDs to pass.")
		EXIT
	ENDIF
	
	enumCharacterList ePed
	REPEAT NUM_OF_PLAYABLE_PEDS ePed
//		PED_REQUEST_SCENE_ENUM eOldScene = g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed]		
			VECTOR vLastKnownCoords = <<0,0,0>>
			FLOAT fLastKnownHead = 0
			IF NOT GetLastKnownPedInfoPostMission(g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
				g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
				CLEAR_TIMEOFDAY(sInfo.sLastTimeActive[ePed])
				
				sInfo.vLastKnownCoords[ePed]		= <<0,0,0>>
				sInfo.fLastKnownHead[ePed]			= 0.0
				sInfo.iLastKnownRoomKey[ePed]		= 0
				sInfo.vLastKnownVelocity[ePed]		= <<0,0,0>>
				sInfo.iLastKnownWantedLevel[ePed]	= 0
				
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_c = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
				
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_c = 0
				
			ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF (eMissionID = SP_MISSION_NONE)
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", "SP_MISSION_NONE", ").")
	ELSE	
		#if USE_NRM_DLC
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", GET_SCRIPT_FROM_HASH_NRM(g_sMissionStaticData[eMissionID].scriptHash), ").")	
		#endif
	ENDIF
	#ENDIF
	
ENDPROC
#ENDIF
PROC ResetLastKnownPedInfo(PED_INFO_STRUCT &sInfo, SP_MISSIONS eMissionID)
	#if USE_CLF_DLC
		if g_bLoadedClifford
			ResetLastKnownPedInfoCLF(sInfo,eMissionID)
			EXIT
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			ResetLastKnownPedInfoNRM(sInfo,eMissionID)
			EXIT
		endif
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF (eMissionID = SP_MISSION_MAX)
		SCRIPT_ASSERT("ResetLastKnownPedInfo: SP_MISSION_MAX is not valid mission IDs to pass.")
		EXIT
	ENDIF
	
	enumCharacterList ePed
	REPEAT NUM_OF_PLAYABLE_PEDS ePed
		PED_REQUEST_SCENE_ENUM eOldScene = g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed]
		
		IF (((eOldScene = PR_SCENE_Fa_PHONECALL_ARM3) OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM1) OR (eOldScene = PR_SCENE_Fa_PHONECALL_FAM3))
		OR ((eOldScene = PR_SCENE_Fa_STRIPCLUB_ARM3)
//		OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM1) OR (eOldScene = PR_SCENE_Fa_STRIPCLUB_FAM3)
		OR (eOldScene = PR_SCENE_Ma_FAMILY6) OR (eOldScene = PR_SCENE_Ma_FINALEC) OR (eOldScene = PR_SCENE_Fa_FINALEC)
		))
		AND NOT (IS_BIT_SET(g_savedGlobals.sFlow.controls.bitsetIDs[FLOWBITSET_MINIGAME_ACTIVE], ENUM_TO_INT(MINIGAME_STRIPCLUB)))
			#IF IS_DEBUG_BUILD
			PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(",
					"dont reset, eOldScene - STRIPCLUB",
					").")
			#ENDIF
			
		ELSE
			VECTOR vLastKnownCoords = <<0,0,0>>
			FLOAT fLastKnownHead = 0
			IF NOT GetLastKnownPedInfoPostMission(g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
				g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_INVALID
				CLEAR_TIMEOFDAY(sInfo.sLastTimeActive[ePed])
				
				sInfo.vLastKnownCoords[ePed]		= <<0,0,0>>
				sInfo.fLastKnownHead[ePed]			= 0.0
				sInfo.iLastKnownRoomKey[ePed]		= 0
				sInfo.vLastKnownVelocity[ePed]		= <<0,0,0>>
				sInfo.iLastKnownWantedLevel[ePed]	= 0
				
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_a = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_b = 0
				g_sDefaultPlayerSwitchState[ePed].vVehicleCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fVehicleHead_c = 0
				g_sDefaultPlayerSwitchState[ePed].fVehicleSpeed_c = 0
				
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_a = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_a = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_b = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_b = 0
				g_sDefaultPlayerSwitchState[ePed].vWalkCoord_c = <<0,0,0>>
				g_sDefaultPlayerSwitchState[ePed].fWalkHead_c = 0
				
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	IF (eMissionID = SP_MISSION_NONE)
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", "SP_MISSION_NONE", ").")
	ELSE
		PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> ResetLastKnownPedInfo(", g_sMissionStaticData[eMissionID].scriptName, ").")	
	ENDIF
	#ENDIF
	
	#endif	// dlc check
	#endif
ENDPROC

/// PURPOSE: Sets the details for the players special vehicle
PROC UPDATE_PLAYER_PED_SAVED_VEHICLE(enumCharacterList ePed, VEHICLE_INDEX &vehID, INT iVehicleSlot, BOOL bUpdateCarApp)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
	AND DOES_ENTITY_EXIST(vehID)
	AND IS_VEHICLE_DRIVEABLE(vehID)
	
		#if USE_CLF_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF
		#endif
		#if USE_NRM_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF
		#endif
		#endif
		
		// Slot specific updates/checks.
		IF iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Updating sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR] for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			// Nothing to do.
			
		ELIF iVehicleSlot = SAVED_VEHICLE_SLOT_BIKE
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Updating sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE] for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			// Nothing to do.
			
		ELIF iVehicleSlot = SAVED_VEHICLE_SLOT_GARAGE
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Updating sPlayerVehicle[SAVED_VEHICLE_SLOT_GARAGE] for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			// Nothing to do.
			
		ELIF iVehicleSlot = SAVED_VEHICLE_SLOT_MODDED
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Updating sPlayerVehicle[SAVED_VEHICLE_SLOT_MODDED] for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			// Track last modded car so we can send to impound if it gets lost.
			TRACK_VEHICLE_FOR_IMPOUND(vehID, ePed)
		ENDIF
		
		IF GET_NUM_MOD_KITS(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
		VEHICLE_MODEL_FAIL_ENUM eFailReason
		
#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)	
			PRINTSTRING("update player: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update player: there is no trailer")PRINTNL()			
		ENDIF		
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[9] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 10)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[10] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 11)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[11] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 12)
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = TRUE
			ELSE
				g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
		ENDIF		
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreR, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreG, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreB)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))		
		g_savedGlobalsClifford.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed] = NUMBER_OF_CLF_SAVEHOUSE
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonR, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonG, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
		AND IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehID, FALSE, eFailReason)
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModVariation)
		ENDIF
		UNUSED_PARAMETER(bUpdatecarApp)
#endif
#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)	
			PRINTSTRING("update player: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update player: there is no trailer")PRINTNL()			
		ENDIF		
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)		
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = TRUE
			ELSE
				g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
		ENDIF		
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreR, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreG, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreB)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))		
		g_savedGlobalsnorman.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed] = NUMBER_OF_NRM_SAVEHOUSE
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonR, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonG, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
		AND IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehID, FALSE, eFailReason)
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModVariation)
		ENDIF
		UNUSED_PARAMETER(bUpdatecarApp)
#endif
	
		
		
		
#if not USE_CLF_DLC
#if not USE_NRM_DLC
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)	
			PRINTSTRING("update player: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update player: there is no trailer")PRINTNL()			
		ENDIF		
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[9] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 10)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[10] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 11)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bExtraOn[11] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 12)
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = TRUE
			ELSE
				g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bConvertible = FALSE
		ENDIF		
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreB)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))		
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		
		g_savedGlobals.sPlayerData.sInfo.eLastVehicleSentToCloudSavehouse[ePed] = NUMBER_OF_SAVEHOUSE_LOCATIONS
		
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
		AND IS_VEHICLE_SAFE_FOR_MOD_SHOP(vehID, FALSE, eFailReason)
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModVariation)
			
			// Overwrite the CarApp data
			IF bUpdateCarApp
				// Game data
				g_savedGlobals.sSocialData.sCarAppData[ePed].bSendDataToCloud = TRUE
				
				// Vehicle data
				g_savedGlobals.sSocialData.sCarAppData[ePed].eModel 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].model
				g_savedGlobals.sSocialData.sCarAppData[ePed].iWindowTint 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWindowTintColour
				g_savedGlobals.sSocialData.sCarAppData[ePed].iTyreSmokeR 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreR
				g_savedGlobals.sSocialData.sCarAppData[ePed].iTyreSmokeG 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreG
				g_savedGlobals.sSocialData.sCarAppData[ePed].iTyreSmokeB 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iTyreB
				g_savedGlobals.sSocialData.sCarAppData[ePed].iWheelType 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iWheelType
				g_savedGlobals.sSocialData.sCarAppData[ePed].bBulletProofTyres 			= (NOT g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].bTyresCanBurst)
				g_savedGlobals.sSocialData.sCarAppData[ePed].tlPlateText 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].tlNumberPlate
				g_savedGlobals.sSocialData.sCarAppData[ePed].iPlateBack 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iPlateBack
				g_savedGlobals.sSocialData.sCarAppData[ePed].iEngine 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_ENGINE]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iBrakes 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_BRAKES]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iWheels 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_WHEELS]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iExhaust 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_EXHAUST]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iSuspension 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_SUSPENSION]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iArmour 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_ARMOUR]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHorn 						= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_HORN]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iLights 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_TOGGLE_XENON_LIGHTS]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iTyreSmoke 				= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_TOGGLE_TYRE_SMOKE]
				g_savedGlobals.sSocialData.sCarAppData[ePed].iTurbo 					= g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iModIndex[MOD_TOGGLE_TURBO]
				
				
				
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountEngine 			= GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountBrakes 			= GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountExhaust 			= GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountWheels 			= GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountHorn 				= GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountArmour 			= GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModCountSuspension 		= GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
				g_savedGlobals.sSocialData.sCarAppData[ePed].iModColoursThatCanBeSet 	= GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[0] 				= GET_VEHICLE_DEFAULT_HORN(vehID)
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[1] 				= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 0)
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[2] 				= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 1)
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[3] 				= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 2)
				g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[4] 				= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 3)
				g_savedGlobals.sSocialData.sCarAppData[ePed].eModKitType 				= GET_VEHICLE_MOD_KIT_TYPE(vehID)
				g_savedGlobals.sSocialData.sCarAppData[ePed].fModPriceModifier			= GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
				
				#IF USE_TU_CHANGES
					g_savedGlobals.sSocialData.sCarAppData[ePed].iHornHash[0] 				= GET_VEHICLE_DEFAULT_HORN_IGNORE_MODS(vehID)
				#ENDIF
				
				INT iColGroup
				MOD_COLOR_TYPE colorType
				INT baseCol, specCol
				GET_VEHICLE_MOD_COLOR_1(vehID, colorType, baseCol, specCol)
				IF colorType = MCT_METALLIC
					iColGroup = 0
				ELIF colorType = MCT_CLASSIC
					iColGroup = 1
				ELIF colorType = MCT_MATTE
					iColGroup = 2
				ELIF colorType = MCT_METALS
					iColGroup = 3
				ELIF colorType = MCT_CHROME
					iColGroup = 4
				ELSE
					iColGroup = -1
				ENDIF
				GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColourExtra1, iColGroup, TRUE, g_savedGlobals.sSocialData.sCarAppData[ePed].iColourID1)
				
				GET_VEHICLE_MOD_COLOR_2(vehID, colorType, baseCol)
				IF colorType = MCT_METALLIC
					iColGroup = 0
				ELIF colorType = MCT_CLASSIC
					iColGroup = 1
				ELIF colorType = MCT_MATTE
					iColGroup = 2
				ELIF colorType = MCT_METALS
					iColGroup = 3
				ELIF colorType = MCT_CHROME
					iColGroup = 4
				ELSE
					iColGroup = -1
				ENDIF
				GET_CAR_APP_COLOUR_INDEX_FROM_VEHICLE_COLOUR(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[iVehicleSlot][ePed].iColour2, -1, iColGroup, FALSE, g_savedGlobals.sSocialData.sCarAppData[ePed].iColourID2)
				
				PRINTLN("UPDATE_PLAYER_PED_SAVED_VEHICLE() - Car App globals updated with new vehicle data")
				PRINTLN("iColourID1 = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iColourID1)
				PRINTLN("iColourID2 = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iColourID2)
				PRINTLN("iWindowTint = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iWindowTint)
				PRINTLN("iTyreSmoke = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iTyreSmoke)
				PRINTLN("iEngine = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iEngine)
				PRINTLN("iExhaust = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iExhaust)
				PRINTLN("iBrakes = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iBrakes)
				PRINTLN("iWheels = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iWheels)
				PRINTLN("iSuspension = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iSuspension)
				PRINTLN("iHorn = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iHorn)
				PRINTLN("iLights = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iLights)
				PRINTLN("iTurbo = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iTurbo)
				PRINTLN("iArmour = ", g_savedGlobals.sSocialData.sCarAppData[ePed].iArmour)
			ENDIF
		ENDIF
#endif
#endif
    ENDIF
ENDPROC

/// PURPOSE: Sets the details for the npc peds stored vehicle
PROC UPDATE_NPC_PED_SAVED_VEHICLE(enumCharacterList ePed, VEHICLE_INDEX &vehID, INT iVehicleSlot)
    IF DOES_ENTITY_EXIST(vehID)
	AND IS_VEHICLE_DRIVEABLE(vehID)
		
		INT iPed
		
		IF ePed = CHAR_AMANDA
			iPed = 0
		ELIF ePed = CHAR_TRACEY
			iPed = 1
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, ped ID out of range, ePed=", ePed)
				SCRIPT_ASSERT("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, ped ID out of range")
			#ENDIF
			EXIT
		ENDIF
		
		#if USE_CLF_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF		
		#endif
		#if USE_NRM_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF		
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF iVehicleSlot > COUNT_OF(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle)
				#IF IS_DEBUG_BUILD
					PRINTLN("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range, iVehicleSlot=", iVehicleSlot)
					SCRIPT_ASSERT("UPDATE_NPC_PED_SAVED_VEHICLE() - Unable to update vehicle, slot ID out of range")
				#ENDIF
				EXIT
			ENDIF		
		#endif
		#endif
		
		// Slot specific updates/checks.
		IF iVehicleSlot = SAVED_VEHICLE_SLOT_CAR
			#IF IS_DEBUG_BUILD
				PRINTLN("UPDATE_NPC_PED_SAVED_VEHICLE() - Updating sNPCVehicle[SAVED_VEHICLE_SLOT_CAR] for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			// Nothing to do.
		ENDIF
		
		IF GET_NUM_MOD_KITS(vehID) != 0
			SET_VEHICLE_MOD_KIT(vehID, 0)
		ENDIF
		
#if USE_CLF_DLC
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)			
			PRINTSTRING("update npc: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update npc: there is no trailer")PRINTNL()			
		ENDIF
		
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[9] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 10)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[10] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 11)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[11] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 12)
		
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = TRUE
			ELSE
				g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
		ENDIF
		
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour1, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra1, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreR, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreG, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreB)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonR, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonG, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModIndex, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModVariation)
		ENDIF
					
#endif
#if USE_NRM_DLC
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)			
			PRINTSTRING("update npc: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update npc: there is no trailer")PRINTNL()			
		ENDIF
		
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
		
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = TRUE
			ELSE
				g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
		ENDIF
		
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour1, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra1, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreR, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreG, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreB)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonR, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonG, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModIndex, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModVariation)
		ENDIF
					
#endif	
#if not USE_CLF_DLC
#if not USE_NRM_DLC
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].model = GET_ENTITY_MODEL(vehID)
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehID, vehTrailer)
			g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].modelTrailer	= GET_ENTITY_MODEL(vehTrailer)			
			PRINTSTRING("update npc: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("update npc: there is no trailer")PRINTNL()			
		ENDIF
		
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fDirtLevel = GET_VEHICLE_DIRT_LEVEL(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].fHealth = GET_ENTITY_HEALTH(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[0] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 1)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[1] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 2)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[2] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 3)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[3] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 4)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[4] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 5)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[5] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 6)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[6] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 7)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[7] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 8)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[8] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 9)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[9] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 10)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[10] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 11)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bExtraOn[11] = IS_VEHICLE_EXTRA_TURNED_ON(vehID, 12)
		
		IF IS_VEHICLE_A_CONVERTIBLE(vehID)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehID)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = TRUE
			ELSE
				g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
			ENDIF
		ELSE
			g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bConvertible = FALSE
		ENDIF
		
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iRadioIndex = GET_PLAYER_RADIO_STATION_INDEX()
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehID)
		GET_VEHICLE_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour1, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColour2)
		GET_VEHICLE_EXTRA_COLOURS(vehID, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iColourExtra2)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehID, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iTyreB)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iLivery = GET_VEHICLE_LIVERY(vehID)
		g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehID))
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehID, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_FRONT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_BACK)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_LEFT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehID, NEON_RIGHT)
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
				
		IF GET_VEHICLE_MOD_KIT(vehID) >= 0
			GET_VEHICLE_MOD_DATA(vehID, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[iVehicleSlot][iPed].iModVariation)
		ENDIF
#endif
#endif
		
		
		
    ENDIF
ENDPROC


/// PURPOSE:		Checks if a coord is contained within the area defined by a Vector ID.
///    
/// PARAM NOTES:	vPosition - The coord being checked for containment within the Vector ID.
///    				eVectorID - An enum referencing the Vector ID having its area queried.
/// RETURN:			A BOOL that will be TRUE if the coord is contained within the Vector ID's area.
FUNC BOOL IS_COORD_IN_VECTOR_ID_AREA_for_autosave(VECTOR vPosition, VectorID eVectorID)

	IF eVectorID != VID_BLANK
		IF VDIST2(vPosition, g_sVectorIDData[eVectorID].position) <= (g_sVectorIDData[eVectorID].radius*g_sVectorIDData[eVectorID].radius)
			RETURN TRUE
		ELIF g_sVectorIDData[eVectorID].extensionID != VID_BLANK
			RETURN IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vPosition, g_sVectorIDData[eVectorID].extensionID)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC
#if USE_CLF_DLC
FUNC BOOL CHECK_PLAYER_MISSION_BLIPS_on_autosaveCLF()
	
	CONST_FLOAT	fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE			50.0
	CONST_FLOAT	fCONST_MIN_COORD_DIST_FROM_SAFEHOUSE			30.0
	
	PED_INDEX pedID = PLAYER_PED_ID()
	IF IS_PED_INJURED(pedID)
		SCRIPT_ASSERT("IS_PED_INJURED(pedID)	//CHECK_PLAYER_MISSION_BLIPS_on_autosave()")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iValidMissions = 0
	STATIC_BLIP_NAME_ENUM eClosestMissionBlip = STATIC_BLIP_NAME_DUMMY_FINAL
	PRINTLN("CHECK_PLAYER_MISSION_BLIPS_on_autosave ", GET_CURRENT_PLAYER_PED_STRING())
	#ENDIF
	
//	IF g_savedGlobalsClifford.sFlow.controls.flagIDs[FLOWFLAG_BLOCK_SAVEHOUSE_FOR_AUTOSAVE]		//Get_Mission_Flow_Flag_State[]
//		PRINTLN("	FLOWFLAG - block safehouse for autosave")
//		RETURN TRUE
//	ENDIF
	
 //	* SAFEHOUSE BLIPS #939375 * //
	VECTOR vSavehouseCoord = <<0,0,0>>
	FLOAT fReturnHeading = 0
	
	IF OVERRIDE_SAVE_HOUSE(FALSE, <<0,0,0>>, 0, TRUE, vSavehouseCoord, fReturnHeading)
		
		//Check for mission triggers to avoid on loading.
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
			IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
				IF g_TriggerableMissionsTU[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE
					IF g_TriggerableMissionsTU[iTriggerIndex].eMissionID != SP_MISSION_NONE
						STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_TriggerableMissionsTU[iTriggerIndex].eMissionID].blip
						IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
							
							VECTOR vMissionCoord
							IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
								ELSE
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
								ENDIF
							ELSE
								vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
							ENDIF
						
							FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
							
							#IF IS_DEBUG_BUILD
							PRINTSTRING("	eMissionBlip ")
							PRINTINT(ENUM_TO_INT(eMissionBlip))
							PRINTSTRING(" and iSavehouse at ")
							PRINTVECTOR(vSavehouseCoord)
							PRINTNL()
							
							PRINTSTRING("	vMissionCoord: ")
							PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
							PRINTSTRING("	[MAX:")
							PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
							PRINTSTRING("]")
							
							PRINTNL()
							#ENDIF
							
							IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
								RETURN TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
							iValidMissions++
							
							IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
								eClosestMissionBlip = eMissionBlip
							ELSE
								FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
								
								IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
									eClosestMissionBlip = eMissionBlip
								ENDIF
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for running missions with blips to avoid on loading.
		IF g_eRunningMission != SP_MISSION_NONE
			STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_eRunningMission].blip
			IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
				
				VECTOR vMissionCoord
				IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
					ELSE
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
					ENDIF
				ELSE
					vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
				ENDIF
			
				FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
				
				#IF IS_DEBUG_BUILD
				PRINTSTRING("	g_eRunningMission ")
				PRINTINT(ENUM_TO_INT(eMissionBlip))
				PRINTSTRING(" and iSavehouse at ")
				PRINTVECTOR(vSavehouseCoord)
				PRINTNL()
				
				PRINTSTRING("	vMissionCoord: ")
				PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
				PRINTSTRING("	[MAX:")
				PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
				PRINTSTRING("]")
				
				PRINTNL()
				#ENDIF
				
				IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				iValidMissions++
				
				IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
					eClosestMissionBlip = eMissionBlip
				ELSE
					FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
					
					IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
						eClosestMissionBlip = eMissionBlip
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		// #1447979	//
		INT iQueueGameTimer = GET_GAME_TIMER()
		CONST_INT iCONST_QueueGameTimerMs 180000	//3min
	
		INT index
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index				
			VectorID eVectorID = g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedCallsDelay = g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedCallsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE CALLS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index				
			VectorID eVectorID = g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedTextsDelay = g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedTextsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE TEXTS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT			
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index				
			VectorID eVectorID = g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedEmailsDelay = g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedEmailsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE EMAILS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("	iValidMissions:", iValidMissions, ", eClosestMissionBlip", ENUM_TO_INT(eClosestMissionBlip))
	#ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL CHECK_PLAYER_MISSION_BLIPS_on_autosaveNRM()
	
	CONST_FLOAT	fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE			50.0
	CONST_FLOAT	fCONST_MIN_COORD_DIST_FROM_SAFEHOUSE			30.0
	
	PED_INDEX pedID = PLAYER_PED_ID()
	IF IS_PED_INJURED(pedID)
		SCRIPT_ASSERT("IS_PED_INJURED(pedID)	//CHECK_PLAYER_MISSION_BLIPS_on_autosave()")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iValidMissions = 0
	STATIC_BLIP_NAME_ENUM eClosestMissionBlip = STATIC_BLIP_NAME_DUMMY_FINAL
	PRINTLN("CHECK_PLAYER_MISSION_BLIPS_on_autosave ", GET_CURRENT_PLAYER_PED_STRING())
	#ENDIF
	
//	IF g_savedGlobalsnorman.sFlow.controls.flagIDs[FLOWFLAG_BLOCK_SAVEHOUSE_FOR_AUTOSAVE]		//Get_Mission_Flow_Flag_State[]
//		PRINTLN("	FLOWFLAG - block safehouse for autosave")
//		RETURN TRUE
//	ENDIF
	
	
 //	* SAFEHOUSE BLIPS #939375 * //
	VECTOR vSavehouseCoord = <<0,0,0>>
	FLOAT fReturnHeading = 0
	
	IF OVERRIDE_SAVE_HOUSE(FALSE, <<0,0,0>>, 0, TRUE, vSavehouseCoord, fReturnHeading)
		
		//Check for mission triggers to avoid on loading.
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
			IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
				IF g_TriggerableMissionsTU[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE
					IF g_TriggerableMissionsTU[iTriggerIndex].eMissionID != SP_MISSION_NONE
						STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_TriggerableMissionsTU[iTriggerIndex].eMissionID].blip
						IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
							
							VECTOR vMissionCoord
							IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
								ELSE
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
								ENDIF
							ELSE
								vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
							ENDIF
						
							FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
							
							#IF IS_DEBUG_BUILD
							PRINTSTRING("	eMissionBlip ")
							PRINTINT(ENUM_TO_INT(eMissionBlip))
							PRINTSTRING(" and iSavehouse at ")
							PRINTVECTOR(vSavehouseCoord)
							PRINTNL()
							
							PRINTSTRING("	vMissionCoord: ")
							PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
							PRINTSTRING("	[MAX:")
							PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
							PRINTSTRING("]")
							
							PRINTNL()
							#ENDIF
							
							IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
								RETURN TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
							iValidMissions++
							
							IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
								eClosestMissionBlip = eMissionBlip
							ELSE
								FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
								
								IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
									eClosestMissionBlip = eMissionBlip
								ENDIF
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for running missions with blips to avoid on loading.
		IF g_eRunningMission != SP_MISSION_NONE
			STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_eRunningMission].blip
			IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
				
				VECTOR vMissionCoord
				IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
					ELSE
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
					ENDIF
				ELSE
					vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
				ENDIF
			
				FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
				
				#IF IS_DEBUG_BUILD
				PRINTSTRING("	g_eRunningMission ")
				PRINTINT(ENUM_TO_INT(eMissionBlip))
				PRINTSTRING(" and iSavehouse at ")
				PRINTVECTOR(vSavehouseCoord)
				PRINTNL()
				
				PRINTSTRING("	vMissionCoord: ")
				PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
				PRINTSTRING("	[MAX:")
				PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
				PRINTSTRING("]")
				
				PRINTNL()
				#ENDIF
				
				IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				iValidMissions++
				
				IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
					eClosestMissionBlip = eMissionBlip
				ELSE
					FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
					
					IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
						eClosestMissionBlip = eMissionBlip
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		// #1447979	//
		INT iQueueGameTimer = GET_GAME_TIMER()
		CONST_INT iCONST_QueueGameTimerMs 180000	//3min
	
		INT index		
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index				
			VectorID eVectorID = g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedCallsDelay = g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedCallsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE CALLS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index				
			VectorID eVectorID = g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedTextsDelay = g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedTextsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE TEXTS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT			
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index				
			VectorID eVectorID = g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				INT iQueuedEmailsDelay = g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime - iQueueGameTimer
				IF (iQueuedEmailsDelay < iCONST_QueueGameTimerMs)
					IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
						PRINTLN("	QUEUE EMAILS - blocked for comms ", index, ".")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("	iValidMissions:", iValidMissions, ", eClosestMissionBlip", ENUM_TO_INT(eClosestMissionBlip))
	#ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
FUNC BOOL CHECK_PLAYER_MISSION_BLIPS_on_autosave()

#if USE_CLF_DLC
	return CHECK_PLAYER_MISSION_BLIPS_on_autosaveCLF()
#endif

#if USE_NRM_DLC
	return CHECK_PLAYER_MISSION_BLIPS_on_autosaveNRM()
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	CONST_FLOAT	fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE			50.0
	CONST_FLOAT	fCONST_MIN_COORD_DIST_FROM_SAFEHOUSE			30.0
	
	PED_INDEX pedID = PLAYER_PED_ID()
	IF IS_PED_INJURED(pedID)
		SCRIPT_ASSERT("IS_PED_INJURED(pedID)	//CHECK_PLAYER_MISSION_BLIPS_on_autosave()")
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iValidMissions = 0
	STATIC_BLIP_NAME_ENUM eClosestMissionBlip = STATIC_BLIP_NAME_DUMMY_FINAL
	PRINTLN("CHECK_PLAYER_MISSION_BLIPS_on_autosave ", GET_CURRENT_PLAYER_PED_STRING())
	#ENDIF

	IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_BLOCK_SAVEHOUSE_FOR_AUTOSAVE]		//Get_Mission_Flow_Flag_State[]
		PRINTLN("	FLOWFLAG - block safehouse for autosave")
		RETURN TRUE
	ENDIF

	
 //	* SAFEHOUSE BLIPS #939375 * //
	VECTOR vSavehouseCoord = <<0,0,0>>
	FLOAT fReturnHeading = 0
	
	IF OVERRIDE_SAVE_HOUSE(FALSE, <<0,0,0>>, 0, TRUE, vSavehouseCoord, fReturnHeading)
		
		//Check for mission triggers to avoid on loading.
		INT iTriggerIndex
		REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
			IF g_TriggerableMissions[iTriggerIndex].bUsed
				IF g_TriggerableMissions[iTriggerIndex].eType = MISSION_TRIGGER_TYPE_SCENE
					IF g_TriggerableMissions[iTriggerIndex].eMissionID != SP_MISSION_NONE
						STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_TriggerableMissions[iTriggerIndex].eMissionID].blip
						IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
							
							VECTOR vMissionCoord
							IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
								IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
								ELSE
									vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
								ENDIF
							ELSE
								vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
							ENDIF
						
							FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
							
							#IF IS_DEBUG_BUILD
							PRINTSTRING("	eMissionBlip ")
							PRINTINT(ENUM_TO_INT(eMissionBlip))
							PRINTSTRING(" and iSavehouse at ")
							PRINTVECTOR(vSavehouseCoord)
							PRINTNL()
							
							PRINTSTRING("	vMissionCoord: ")
							PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
							PRINTSTRING("	[MAX:")
							PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
							PRINTSTRING("]")
							
							PRINTNL()
							#ENDIF
							
							IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
								RETURN TRUE
							ENDIF
							
							#IF IS_DEBUG_BUILD
							iValidMissions++
							
							IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
								eClosestMissionBlip = eMissionBlip
							ELSE
								FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
								
								IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
									eClosestMissionBlip = eMissionBlip
								ENDIF
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Check for running missions with blips to avoid on loading.
		IF g_eRunningMission != SP_MISSION_NONE
			STATIC_BLIP_NAME_ENUM eMissionBlip = g_sMissionStaticData[g_eRunningMission].blip
			IF eMissionBlip != STATIC_BLIP_NAME_DUMMY_FINAL
				
				VECTOR vMissionCoord
				IF IS_BIT_SET(g_GameBlips[eMissionBlip].iSetting, STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)
					IF IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[GET_CURRENT_PLAYER_PED_INT()]
					ELSE
						vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
					ENDIF
				ELSE
					vMissionCoord = g_GameBlips[eMissionBlip].vCoords[0]
				ENDIF
			
				FLOAT fSafehouseToBlipDist2 = VDIST2(vSavehouseCoord, vMissionCoord)
				
				#IF IS_DEBUG_BUILD
				PRINTSTRING("	g_eRunningMission ")
				PRINTINT(ENUM_TO_INT(eMissionBlip))
				PRINTSTRING(" and iSavehouse at ")
				PRINTVECTOR(vSavehouseCoord)
				PRINTNL()
				
				PRINTSTRING("	vMissionCoord: ")
				PRINTFLOAT(SQRT(fSafehouseToBlipDist2))
				PRINTSTRING("	[MAX:")
				PRINTFLOAT(fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE)
				PRINTSTRING("]")
				
				PRINTNL()
				#ENDIF
				
				IF (fSafehouseToBlipDist2 < (fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE*fCONST_MIN_MISSION_DIST_FROM_SAFEHOUSE))
					RETURN TRUE
				ENDIF
				
				#IF IS_DEBUG_BUILD
				iValidMissions++
				
				IF (eClosestMissionBlip >= STATIC_BLIP_NAME_DUMMY_FINAL)
					eClosestMissionBlip = eMissionBlip
				ELSE
					FLOAT fSafehouseToClosestBlipDist = VDIST(vSavehouseCoord, g_GameBlips[eClosestMissionBlip].vCoords[0])
					
					IF (SQRT(fSafehouseToBlipDist2) < fSafehouseToClosestBlipDist)
						eClosestMissionBlip = eMissionBlip
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		// #1447979	//
		INT iQueueGameTimer = GET_GAME_TIMER()
		CONST_INT iCONST_QueueGameTimerMs 180000	//3min
	
		INT index		
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index				
			VectorID eVectorID = g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					INT iQueuedCallsDelay = g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedCallsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
							PRINTLN("	QUEUE CALLS - blocked for comms ", index, ".")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT			
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index				
			VectorID eVectorID = g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					INT iQueuedTextsDelay = g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedTextsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
							PRINTLN("	QUEUE TEXTS - blocked for comms ", index, ".")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT			
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index				
			VectorID eVectorID = g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eRestrictedAreaID
			IF eVectorID <> VID_BLANK
				IF ((eVectorID <> VID_BLANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_HARBOR) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RACE_TRACK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_WINDMILLS) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_NORTH_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_MAZE_BANK) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CRANE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RIVER_CLIFF) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_RUNAWAY_TRAIN) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_GOLF_COURSE) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_1_5K) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_CANAL) AND (eVectorID <> VID_AMBIENT_BASEJUMP_ROCK_CLIFF))
					INT iQueuedEmailsDelay = g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iQueueTime - iQueueGameTimer
					IF (iQueuedEmailsDelay < iCONST_QueueGameTimerMs)
						IF IS_COORD_IN_VECTOR_ID_AREA_for_autosave(vSavehouseCoord, eVectorID)
							PRINTLN("	QUEUE EMAILS - blocked for comms ", index, ".")
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		VECTOR vCentreProperty = <<0,0,0>>
		FLOAT fPropertyDistChec = -1
		
		FLOW_BITSET_IDS eFlowBitsetToQuery 
		INT iOffsetBitIndex 

		//	For Michael's Family Home
		//Is this bit in our first or second bitset? 
		IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) <= 31 
			eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
			iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) 
		ELSE 
			eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
			iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_M_HOME) - 31 
		ENDIF
		
		IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
			// cutscene script is due to relaunch or is active
			vCentreProperty = << -808.79742, 169.31934, 70.95580 >>
			fPropertyDistChec = 85.0
			
			FLOAT fPropertyDist2 = VDIST2(vSavehouseCoord, vCentreProperty)
			IF (fPropertyDist2 < (fPropertyDistChec*fPropertyDistChec))
			
				#IF IS_DEBUG_BUILD
				PRINTSTRING("safehouse load blocked too close to Michael safehouse tutorial scene [")
				PRINTFLOAT(SQRT(fPropertyDist2))
				PRINTSTRING("m]")
				PRINTNL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		//	For Franklin's hills safehouse -      
		//Is this bit in our first or second bitset? 
		IF ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) <= 31 
			eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_1 
			iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) 
		ELSE 
			eFlowBitsetToQuery = FLOWBITSET_RESTART_FLOW_LAUNCHED_SCRIPTS_2 
			iOffsetBitIndex = ENUM_TO_INT(LAUNCH_BIT_SH_INTRO_F_HILLS) - 31 
		ENDIF
		IF IS_BIT_SET (g_savedGlobals.sFlow.controls.bitsetIDs[eFlowBitsetToQuery], iOffsetBitIndex) 
			// cutscene script is due to relaunch or is active 
			vCentreProperty = << -2.62564, 528.32562, 178.39198 >>
			fPropertyDistChec = 65.0
			
			FLOAT fPropertyDist2 = VDIST2(vSavehouseCoord, vCentreProperty)
			IF (fPropertyDist2 < (fPropertyDistChec*fPropertyDistChec))
			
				#IF IS_DEBUG_BUILD
				PRINTSTRING("safehouse load blocked too close to Franklin safehouse tutorial scene [")
				PRINTFLOAT(SQRT(fPropertyDist2))
				PRINTSTRING("m]")
				PRINTNL()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		// // // //	//
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTLN("	iValidMissions:", iValidMissions, ", eClosestMissionBlip", ENUM_TO_INT(eClosestMissionBlip))
	#ENDIF
	
	RETURN FALSE
#endif
#endif
ENDFUNC
#if USE_CLF_DLC
FUNC BOOL SHOULD_PROCESS_DESCENT_SWITCH_ON_LOADCLF()

	#IF IS_DEBUG_BUILD
	PRINTSTRING("SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD(")
	
	PRINTSTRING("wasFadedOut:")
	PRINTBOOL(g_savedGlobalsClifford.sFlowCustom.wasFadedOut)
	
	PRINTSTRING(", wasFadedOut_switch:")
	PRINTBOOL(g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch)
	
	PRINTSTRING(", bIgnoreScreenFade:")
	PRINTBOOL(g_sAutosaveData.bIgnoreScreenFade)
	
	PRINTSTRING(", g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos:")
	PRINTVECTOR(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos)
	
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	
	IF g_savedGlobalsClifford.sFlowCustom.wasFadedOut_switch
		IF (g_savedGlobalsClifford.sFlow.missionSavedData[SP_MISSION_CLF_TRAIN].completed)	// #1104329, before start
			IF ARE_VECTORS_EQUAL(g_savedGlobalsClifford.sRepeatPlayData.mPlayerStruct.vPos, <<0.0,0.0,0.0>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
#ENDIF
#if USE_NRM_DLC
FUNC BOOL SHOULD_PROCESS_DESCENT_SWITCH_ON_LOADNRM()

	#IF IS_DEBUG_BUILD
	PRINTSTRING("SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD(")
	
	PRINTSTRING("wasFadedOut:")
	PRINTBOOL(g_savedGlobalsnorman.sFlowCustom.wasFadedOut)
	
	PRINTSTRING(", wasFadedOut_switch:")
	PRINTBOOL(g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch)
	
	PRINTSTRING(", bIgnoreScreenFade:")
	PRINTBOOL(g_sAutosaveData.bIgnoreScreenFade)
	
	PRINTSTRING(", g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos:")
	PRINTVECTOR(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos)
	
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	
	IF g_savedGlobalsnorman.sFlowCustom.wasFadedOut_switch
		IF (g_savedGlobalsnorman.sFlow.missionSavedData[SP_MISSION_NRM_SUR_START].completed)	// #1104329, before Arm1
			IF ARE_VECTORS_EQUAL(g_savedGlobalsnorman.sRepeatPlayData.mPlayerStruct.vPos, <<0.0,0.0,0.0>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC
#ENDIF
FUNC BOOL SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD()
	#if USE_CLF_DLC
		return SHOULD_PROCESS_DESCENT_SWITCH_ON_LOADCLF()		
	#endif
	#if USE_NRM_DLC
		return SHOULD_PROCESS_DESCENT_SWITCH_ON_LOADNRM()			
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	#IF IS_DEBUG_BUILD
	PRINTSTRING("SHOULD_PROCESS_DESCENT_SWITCH_ON_LOAD(")
	
	PRINTSTRING("wasFadedOut:")
	PRINTBOOL(g_savedGlobals.sFlowCustom.wasFadedOut)
	
	PRINTSTRING(", wasFadedOut_switch:")
	PRINTBOOL(g_savedGlobals.sFlowCustom.wasFadedOut_switch)
	
	PRINTSTRING(", bIgnoreScreenFade:")
	PRINTBOOL(g_sAutosaveData.bIgnoreScreenFade)
	
	PRINTSTRING(", g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos:")
	PRINTVECTOR(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos)
	
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	
	IF g_savedGlobals.sFlowCustom.wasFadedOut_switch
		IF (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_1].completed)	// #1104329, before Arm1
			IF ARE_VECTORS_EQUAL(g_savedGlobals.sRepeatPlayData.mPlayerStruct.vPos, <<0.0,0.0,0.0>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
	#ENDIF
	#ENDIF
ENDFUNC

PROC REGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(enumCharacterList char, PED_INDEX &ped)
	CPRINTLN(DEBUG_FLOW, "REGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS char= ", char)
	SWITCH char
		CASE CHAR_MICHAEL
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)
		BREAK
		CASE CHAR_FRANKLIN			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_3].completed) 
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			ENDIF		
			#endif
			#endif
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT]
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_FRAN_HILLS_GARAGE, ped)
			ENDIF		
			#endif
			#endif	
		BREAK
		CASE CHAR_TREVOR
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_FAMILY_4].completed) 
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			ENDIF		
			#endif
			#endif	
			
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)
		BREAK
	ENDSWITCH
ENDPROC


PROC UNREGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS(enumCharacterList char, PED_INDEX &ped)
	CPRINTLN(DEBUG_FLOW, "UNREGISTER_PLAYER_PED_WITH_AUTOMATIC_DOORS char= ", char)
	SWITCH char
		CASE CHAR_MICHAEL
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)
		BREAK
		CASE CHAR_FRANKLIN
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT]
				UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_FRAN_HILLS_GARAGE, ped)
			ENDIF		
			#endif
			#endif				
		BREAK
		CASE CHAR_TREVOR
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_IN, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_GATE_OUT, ped)
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DTOWN_VINEWOOD_GARAGE, ped)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Returns TRUE if the player is allowed to use the wardrobe and shops to change clothes.
FUNC BOOL CAN_PLAYER_CHANGE_CLOTHES_ON_MISSION()
	RETURN (g_bPlayerCanChangeClothesOnMission)
ENDFUNC

/// PURPOSE: Allows scripts to block the player from using the wardrobe and shops to change clothes.
PROC SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(BOOL bCanChange)
	
	g_bPlayerCanChangeClothesOnMission = bCanChange
	
	#IF IS_DEBUG_BUILD
		IF bCanChange
			PRINTLN("SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE) called by script '", GET_THIS_SCRIPT_NAME(),"'")
		ELSE
			PRINTLN("SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE) called by script '", GET_THIS_SCRIPT_NAME(),"'")
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE:
///    set the global saved bool bChangedClothesOnMission for ePed to TRUE - that player char has had their outfit changed on mission
/// PARAMS:
///    ePed - char ID of the player who's changed his changed (CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR)
PROC SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(enumCharacterList ePed)
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		EXIT
	ENDIF
	#if USE_CLF_DLC
	g_savedGlobalsClifford.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = TRUE
	#endif
	#if USE_NRM_DLC
	g_savedGlobalsnorman.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = TRUE
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	g_savedGlobals.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = TRUE
	#endif
	#endif
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(", ")
	PRINTBOOL(TRUE)
	PRINTSTRING(") called by script ")
	PRINTSTRING(GET_THIS_SCRIPT_NAME())
	PRINTNL()
	#ENDIF
ENDPROC

/// PURPOSE:
///    set the global saved bool bChangedClothesOnMission for ePed to hasChange - whether the player has changed outfit or not
/// PARAMS:
///    ePed - char ID of the player who's changed his changed (CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR)
///    hasChange - the value to set a player chars saved bChangedClothesOnMission bool
PROC RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(enumCharacterList ePed, BOOL hasChange)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		EXIT
	ENDIF
	#if USE_CLF_DLC
	g_savedGlobalsClifford.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = hasChange
	#endif
	#if USE_NRM_DLC
	g_savedGlobalsnorman.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = hasChange
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	g_savedGlobals.sPlayerData.sInfo.bChangedClothesOnMission[ePed] = hasChange
	#endif
	#endif
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(", ")
	PRINTBOOL(hasChange)
	PRINTSTRING(") called by script ")
	PRINTSTRING(GET_THIS_SCRIPT_NAME())
	PRINTNL()
	#ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    ePed - char ID of the player who's changed his changed (CHAR_MICHAEL, CHAR_FRANKLIN, CHAR_TREVOR)
/// RETURNS:
///    TRUE if the player changed his outfit on a mission, FALSE otherwise
FUNC BOOL GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(enumCharacterList ePed)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sPlayerData.sInfo.bChangedClothesOnMission[ePed]			
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sPlayerData.sInfo.bChangedClothesOnMission[ePed]			
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	RETURN g_savedGlobals.sPlayerData.sInfo.bChangedClothesOnMission[ePed]			
	#endif
	#endif
	
ENDFUNC


FUNC BOOL StoreInGlobalSelector(PED_INDEX pedID, enumCharacterList ePed, BOOL bInLeadIn = FALSE)
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("StoreInGlobalSelector(")
	PRINTINT(NATIVE_TO_INT(pedID))
	PRINTSTRING(", ")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	
	SELECTOR_SLOTS_ENUM eSelectorPed = NUMBER_OF_SELECTOR_PEDS
	IF ePed = CHAR_MICHAEL
		eSelectorPed = SELECTOR_PED_MICHAEL
	ELIF ePed = CHAR_TREVOR
		eSelectorPed = SELECTOR_PED_TREVOR
	ELIF ePed = CHAR_FRANKLIN
		eSelectorPed = SELECTOR_PED_FRANKLIN
	ELIF ePed = NO_CHARACTER
		eSelectorPed = SELECTOR_PED_MULTIPLAYER
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed])
		
		INT iQueueSpot = -1, iQueue
		REPEAT COUNT_OF(g_ambientSelectorPedDeleteQueue) iQueue
			IF (iQueueSpot < 0)
				IF NOT DOES_ENTITY_EXIST(g_ambientSelectorPedDeleteQueue[iQueue])
					iQueueSpot = iQueue
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF (iQueueSpot < 0)
		OR (iQueueSpot > COUNT_OF(g_ambientSelectorPedDeleteQueue))
			
			SCRIPT_ASSERT("invalid iQueueSpot???")
			
			iQueueSpot = 0
		ENDIF
		
		g_ambientSelectorPedDeleteQueue[iQueueSpot] = g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed]
		
		IF bInLeadIn
			IF NOT IS_PED_INJURED(g_ambientSelectorPedDeleteQueue[iQueueSpot])
				IF DOES_ENTITY_EXIST(pedID)
					IF NOT IS_ENTITY_DEAD(pedID)
						CPRINTLN(DEBUG_TRIGGER, "Telling old ", GET_PLAYER_PED_STRING(ePed), " ped to flee coords of lead-in created ", GET_PLAYER_PED_STRING(ePed), ".")
						CLEAR_PED_TASKS(g_ambientSelectorPedDeleteQueue[iQueueSpot])
						TASK_SMART_FLEE_COORD(g_ambientSelectorPedDeleteQueue[iQueueSpot], GET_ENTITY_COORDS(pedID), 1000.0, -1, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	g_sPlayerPedRequest.sSelectorPeds.pedID[eSelectorPed]		= pedID
	
	#if USE_CLF_DLC
	g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()	
	g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed]		= GET_ENTITY_COORDS(pedID)
	g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed]			= GET_ENTITY_HEADING(pedID)
	#endif
	#if USE_NRM_DLC
	g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()	
	g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed]		= GET_ENTITY_COORDS(pedID)
	g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed]			= GET_ENTITY_HEADING(pedID)
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]		= GET_CURRENT_TIMEOFDAY()	
	g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]		= GET_ENTITY_COORDS(pedID)
	g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]		= GET_ENTITY_HEADING(pedID)
	#endif
	#endif
	
	
	IF bInLeadIn
		SET_BIT(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(eSelectorPed))
		CPRINTLN(DEBUG_TRIGGER, "Flagged selector pedID for ", GET_PLAYER_PED_STRING(ePed), " as created for lead-in.")
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL StoreAsGlobalFriend(PED_INDEX pedID, enumCharacterList ePed)
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("StoreAsGlobalFriend(")
	PRINTINT(NATIVE_TO_INT(pedID))
	PRINTSTRING(", ")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(")")
	PRINTNL()
	#ENDIF
	
	
	enumFriend eFriend = NO_FRIEND
	IF ePed = CHAR_LAMAR
		eFriend = FR_LAMAR
		
	ELIF ePed = CHAR_JIMMY
		eFriend = FR_JIMMY
		
	ELIF ePed = CHAR_AMANDA
		eFriend = FR_AMANDA
		
	ELSE
		eFriend = NO_FRIEND
		RETURN FALSE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("g_pGlobalFriends[ENUM_TO_INT(")
	PRINTSTRING(GET_PLAYER_PED_STRING(ePed))
	PRINTSTRING(":")
	PRINTINT(ENUM_TO_INT(eFriend))
	PRINTSTRING(") - NUM_OF_PLAYABLE_PEDS")
	PRINTSTRING(":")
	PRINTINT(NUM_OF_PLAYABLE_PEDS)
	PRINTSTRING("] = ")
	PRINTINT(NATIVE_TO_INT(pedID))
	PRINTNL()
	#ENDIF
	
	g_pGlobalFriends[ENUM_TO_INT(eFriend) - NUM_OF_PLAYABLE_PEDS] = pedID
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE(PED_INDEX PedIndex, VEHICLE_INDEX VehicleIndex, BOOL ConsiderEnteringAsInVehicle = FALSE)
	IF IS_PED_IN_VEHICLE(PedIndex, VehicleIndex, ConsiderEnteringAsInVehicle)
		
		MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(VehicleIndex)
		
		IF IS_THIS_MODEL_A_BOAT(eVehicleModel)
		OR (eVehicleModel = SUBMERSIBLE OR eVehicleModel = SUBMERSIBLE2)
			IF IS_ENTITY_IN_WATER(VehicleIndex)
				PRINTSTRING("IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE - vehicle is a boat in the water")PRINTNL()
				
				RETURN FALSE
			ENDIF
			
			PRINTSTRING("IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE - vehicle is a boat NOT in the water")PRINTNL()
			RETURN TRUE
		ENDIF
		
		IF IS_THIS_MODEL_A_PLANE(eVehicleModel) OR IS_THIS_MODEL_A_HELI(eVehicleModel)
		OR (eVehicleModel = BLIMP)
			IF IS_ENTITY_IN_AIR(VehicleIndex)
				PRINTSTRING("IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE - vehicle is a plane in the air")PRINTNL()
				
				RETURN FALSE
			ENDIF
			
			PRINTSTRING("IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE - vehicle is a plane NOT in the air")PRINTNL()
			RETURN TRUE
		ENDIF
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the vehicle model enum for the specified story character
PROC STORE_VEH_DATA_FROM_VEH(PED_INDEX pedIndex, VEHICLE_INDEX vehicleIndex, PED_VEH_DATA_STRUCT &sVehData,
		VECTOR &vVehStoredCoord, FLOAT &fVehStoredHead, enumTimetableVehState &eVehState
		#IF USE_TU_CHANGES	, VEHICLE_GEN_NAME_ENUM &eVehGen 	#ENDIF, BOOL awkwardAngle = FALSE	)
	
	sVehData.bIsPlayerVehicle = FALSE
	
	#IF USE_TU_CHANGES
	enumCharacterList ePed = NO_CHARACTER
	#ENDIF
		
	IF NOT IS_ENTITY_DEAD(pedIndex)
	AND NOT IS_ENTITY_DEAD(vehicleIndex)
		
		#IF NOT USE_TU_CHANGES
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedIndex)
		#ENDIF
		#IF USE_TU_CHANGES
		ePed = GET_PLAYER_PED_ENUM(pedIndex)
		#ENDIF
		
		#IF USE_TU_CHANGES
		IF ENUM_TO_INT(ePed) > NUM_OF_PLAYABLE_PEDS
			IF NETWORK_IS_GAME_IN_PROGRESS()
				PRINTLN("STORE_VEH_DATA_FROM_VEH: ePed > NUM_OF_PLAYABLE_PEDS!!")
			ELSE
				SCRIPT_ASSERT("STORE_VEH_DATA_FROM_VEH: ePed > NUM_OF_PLAYABLE_PEDS!!")
			ENDIF
			
			EXIT
		ENDIF
		#ENDIF
		
		//1586393
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF DECOR_EXIST_ON(vehicleIndex,"GetawayVehicleValid")
				IF DECOR_GET_BOOL(vehicleIndex, "GetawayVehicleValid")
					IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED]
					OR g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_AGENCY_PREP_2_DONE]
					OR g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_HEIST_FINALE_PREPE_DONE]
						eVehState = PTVS_0_noVehicle
						g_vPlayerVeh[ePed] = NULL					
						EXIT
					ENDIF
				ENDIF
			ENDIF
		#endif
		#endif
		
		IF IS_PED_IN_VEHICLE(pedIndex, vehicleIndex) OR awkwardAngle
//				vehicleIndex = GET_VEHICLE_PED_IS_IN(pedIndex)
			eVehState = PTVS_2_playerInVehicle
			g_vPlayerVeh[ePed] = vehicleIndex
		ELIF IS_PED_IN_ANY_VEHICLE(pedIndex)
			vehicleIndex = GET_VEHICLE_PED_IS_IN(pedIndex)
			eVehState = PTVS_2_playerInVehicle
			g_vPlayerVeh[ePed] = vehicleIndex
		ELSE
			
			#IF USE_TU_CHANGES
			// work around for GET_VEHICLE_GEN_VEHICLE_INDEX() //
			VEHICLE_GEN_NAME_ENUM eVehicleGen
			REPEAT NUMBER_OF_VEHICLES_TO_GEN eVehicleGen
				IF vehicleIndex = g_sVehicleGenNSData.vehicleID[eVehicleGen]
					
					PRINTSTRING("url:bugstar:1648196 - switch trying to store a vehgen vehicle [")
					PRINTINT(ENUM_TO_INT(eVehicleGen))
					PRINTSTRING("] as players last known veh")
					PRINTNL()
					
					eVehState = PTVS_0_noVehicle
					g_vPlayerVeh[ePed] = NULL					
					EXIT
				ENDIF
			ENDREPEAT
			// // // // // // // // // // // // // // // // // //
			#ENDIF
			#IF NOT USE_TU_CHANGES
			#IF IS_DEBUG_BUILD
				PRINTSTRING("url:bugstar:1648196 - switch trying to store a vehgen vehicle not not using TU changes so who knows if it worked!!")
				PRINTNL()
			#ENDIF
			#ENDIF
			
			IF NOT IS_ENTITY_DEAD(g_vPlayerVeh[ePed])
				
				IF NOT IS_VEHICLE_IN_PLAYERS_GARAGE(g_vPlayerVeh[ePed], ePed, FALSE)
				OR IS_PED_IN_VEHICLE(pedIndex, g_vPlayerVeh[ePed])
					VECTOR vPedCoord, vVehCoord
					vPedCoord = GET_ENTITY_COORDS(pedIndex)
					vVehCoord = GET_ENTITY_COORDS(g_vPlayerVeh[ePed])
					
					IF IS_PED_IN_VEHICLE_AND_SAFE_TO_REMOVE(pedIndex, g_vPlayerVeh[ePed])
						IF VDIST2(vPedCoord, vVehCoord) < (150*150)
							vVehStoredCoord = vVehCoord
							fVehStoredHead = GET_ENTITY_HEADING(g_vPlayerVeh[ePed])
							eVehState = PTVS_1_playerWithVehicle
							g_vPlayerVeh[ePed] = vehicleIndex
							
							IF VDIST2(vPedCoord, vVehCoord) < (1.5*1.5)
								PRINTLN("STORE_VEH_DATA_FROM_VEH: PTVS_1_playerWithVehicle TOO CLOSE ", GET_PLAYER_PED_STRING(ePed), " ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(g_vPlayerVeh[ePed]), ", ", VDIST(vPedCoord, vVehCoord), ", vPedCoord:", vPedCoord)
							ELSE
								PRINTLN("STORE_VEH_DATA_FROM_VEH: PTVS_1_playerWithVehicle FAR ENOUGH ", GET_PLAYER_PED_STRING(ePed), " ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(g_vPlayerVeh[ePed]), ", ", VDIST(vPedCoord, vVehCoord), ", vPedCoord:", vPedCoord)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					eVehState = PTVS_0_noVehicle
					g_vPlayerVeh[ePed] = NULL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT iExtra
	IF DOES_ENTITY_EXIST(vehicleIndex)
		sVehData.model					= GET_ENTITY_MODEL(vehicleIndex)
		
		//sVehData.modelTrailer	= DUMMY_MODEL_FOR_SCRIPT
		VEHICLE_INDEX vehTrailer
		IF GET_VEHICLE_TRAILER_VEHICLE(vehicleIndex, vehTrailer)
			sVehData.modelTrailer	= GET_ENTITY_MODEL(vehTrailer)
			
			
			PRINTSTRING("store veh: there is a trailer")PRINTNL()
		ELSE
			PRINTSTRING("store veh: there is no trailer")PRINTNL()
			
		ENDIF
		
		sVehData.fDirtLevel				= GET_VEHICLE_DIRT_LEVEL(vehicleIndex)
		sVehData.fHealth				= GET_ENTITY_HEALTH(vehicleIndex)
		
		// Colours
		sVehData.iColourCombo			= GET_VEHICLE_COLOUR_COMBINATION(vehicleIndex)
		
		IF sVehData.iColourCombo > -1
			sVehData.bColourCombo		= TRUE
			sVehData.iColour1			= -1
			sVehData.iColour2			= -1
		ELSE
			sVehData.bColourCombo		= FALSE
			GET_VEHICLE_COLOURS(vehicleIndex, sVehData.iColour1, sVehData.iColour2)
		ENDIF
		
		// Can't get vehicle extra colour for boats (url:bugstar:354001)
		IF NOT IS_THIS_MODEL_A_BOAT(sVehData.model)
			sVehData.bColourExtra		= TRUE
			GET_VEHICLE_EXTRA_COLOURS(vehicleIndex, sVehData.iColourExtra1, sVehData.iColourExtra2)
		ELSE
			sVehData.bColourExtra		= FALSE
			sVehData.iColourExtra1		= -1
			sVehData.iColourExtra2		= -1
		ENDIF
		
		// Plates
		sVehData.tlNumberPlate = GET_VEHICLE_NUMBER_PLATE_TEXT(vehicleIndex)
		sVehData.iPlateBack = GET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehicleIndex)
		
		// Tyres
		sVehData.bTyresCanBurst = GET_VEHICLE_TYRES_CAN_BURST(vehicleIndex)
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehicleIndex, sVehData.iTyreR, sVehData.iTyreG, sVehData.iTyreB)
		
		// Windows
		sVehData.iWindowTintColour = GET_VEHICLE_WINDOW_TINT(vehicleIndex)
		
		// Neon lights
		GET_VEHICLE_NEON_COLOUR(vehicleIndex, sVehData.iNeonR, sVehData.iNeonG, sVehData.iNeonB)
		IF GET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_FRONT)
			SET_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ELSE
			CLEAR_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_BACK)
			SET_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ELSE
			CLEAR_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_LEFT)
			SET_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ELSE
			CLEAR_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
		ENDIF
		IF GET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_RIGHT)
			SET_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ELSE
			CLEAR_BIT(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
		ENDIF
		
		// Livery
		sVehData.iLivery = GET_VEHICLE_LIVERY(vehicleIndex)
		
		// Wheel type
		sVehData.iWheelType = ENUM_TO_INT(GET_VEHICLE_WHEEL_TYPE(vehicleIndex))
		
		// Extras
		REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
			sVehData.bExtraOn[iExtra]	= IS_VEHICLE_EXTRA_TURNED_ON(vehicleIndex, iExtra+1)
		ENDREPEAT
		
		IF IS_VEHICLE_A_CONVERTIBLE(vehicleIndex)
			CONVERTIBLE_ROOF_STATE eRoofState = GET_CONVERTIBLE_ROOF_STATE(vehicleIndex)
			IF eRoofState = CRS_RAISED
			OR eRoofState = CRS_ROOF_STUCK_RAISED
				sVehData.bConvertible = TRUE
				
				PRINTSTRING("store veh: is a convertible-raised")PRINTNL()
				
			ELSE
				sVehData.bConvertible = FALSE
				
				PRINTSTRING("store veh: is a convertible-lowered")PRINTNL()
				
			ENDIF
		ELSE
			sVehData.bConvertible = FALSE
			
			PRINTSTRING("store veh: not a convertible")PRINTNL()
			
		ENDIF
		
		// Radio
		IF (pedIndex = PLAYER_PED_ID())
			sVehData.iRadioIndex		= GET_PLAYER_RADIO_STATION_INDEX()
		ELSE
			//
		ENDIF
		
		// Mods
		GET_VEHICLE_MOD_DATA(vehicleIndex, sVehData.iModIndex, sVehData.iModVariation)
		
		// Env Eff
		sVehData.fEnvEff = GET_VEHICLE_ENVEFF_SCALE(vehicleIndex)
		
		sVehData.bIsPlayerVehicle = IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(vehicleIndex)
		
		#IF USE_TU_CHANGES
		eVehGen = VEHGEN_NONE
		SWITCH ePed
			CASE CHAR_MICHAEL
				IF g_savedGlobals.sVehicleGenData.sDynamicData[0].eModel = GET_ENTITY_MODEL(vehicleIndex)
					eVehGen = VEHGEN_WEB_HANGAR_MICHAEL
				ENDIF
			BREAK
			CASE CHAR_FRANKLIN
				IF g_savedGlobals.sVehicleGenData.sDynamicData[1].eModel = GET_ENTITY_MODEL(vehicleIndex)
					eVehGen = VEHGEN_WEB_HANGAR_FRANKLIN
				ENDIF
			BREAK
			CASE CHAR_TREVOR
				IF g_savedGlobals.sVehicleGenData.sDynamicData[2].eModel  = GET_ENTITY_MODEL(vehicleIndex)
					eVehGen = VEHGEN_WEB_HANGAR_TREVOR
				ENDIF
			BREAK
		ENDSWITCH
		
		#ENDIF
	ELSE
		
		eVehState = PTVS_0_noVehicle
		vVehStoredCoord = <<0,0,0>>
		fVehStoredHead = 0
		
		sVehData.model					= DUMMY_MODEL_FOR_SCRIPT
		sVehData.modelTrailer			= DUMMY_MODEL_FOR_SCRIPT
		sVehData.fDirtLevel				= 0
		sVehData.fHealth				= 0
		sVehData.iColourCombo			= 0
		sVehData.iColour1				= 0
		sVehData.iColour2				= 0
		sVehData.iColourExtra1			= 0
		sVehData.iColourExtra2			= 0
		sVehData.bColourCombo			= FALSE
		sVehData.bColourExtra			= FALSE
		
		REPEAT COUNT_OF(sVehData.bExtraOn) iExtra
			sVehData.bExtraOn[iExtra]	= FALSE
		ENDREPEAT
		sVehData.bConvertible			= FALSE
		
		sVehData.iRadioIndex			= 0
		
		eVehState = PTVS_0_noVehicle
		
		#IF USE_TU_CHANGES
		eVehGen = VEHGEN_NONE
		#ENDIF
		
		sVehData.fEnvEff = 0.0
	ENDIF
	
ENDPROC


/// PURPOSE: Returns the vehicle model enum for the specified story character
PROC STORE_VEH_DATA_FROM_PEDCLF(PED_INDEX pedIndex, PED_VEH_DATA_STRUCT &sVehData,
		VECTOR &vVehStoredCoord, FLOAT &fVehStoredHead, enumTimetableVehState &eVehState
		#IF USE_TU_CHANGES	, VEHICLE_GEN_NAME_ENUM &eVehGen	#ENDIF	)
	
//	eVehState = PTVS_0_noVehicle
//	vVehStoredCoord = <<0,0,0>>
//	fVehStoredHead = 0
	VEHICLE_INDEX vehicleIndex = NULL
	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_ENTITY_DEAD(pedIndex)
			IF IS_PED_IN_ANY_VEHICLE(pedIndex)
				vehicleIndex = GET_VEHICLE_PED_IS_IN(pedIndex)
				eVehState = PTVS_2_playerInVehicle
			ELSE
				enumCharacterList ePed = GET_PLAYER_PED_ENUMCLF(pedIndex)
				IF NOT IS_ENTITY_DEAD(g_vPlayerVeh[ePed])
					IF IS_VEHICLE_SEAT_FREE(g_vPlayerVeh[ePed], VS_DRIVER)	//#1519778
						VECTOR vPedCoord, vVehCoord
						vPedCoord = GET_ENTITY_COORDS(pedIndex)
						vVehCoord = GET_ENTITY_COORDS(g_vPlayerVeh[ePed])
						
						IF VDIST2(vPedCoord, vVehCoord) < (150*150)
//							vVehStoredCoord = vVehCoord
//							fVehStoredHead = GET_ENTITY_HEADING(g_vPlayerVeh[ePed])
//							eVehState = PTVS_1_playerWithVehicle
//							vehicleIndex = g_vPlayerVeh[ePed]
//							
//							IF VDIST2(vPedCoord, vVehCoord) < (1.5*1.5)
//								PRINTLN("STORE_VEH_DATA_FROM_PEDCLF: PTVS_1_playerWithVehicle TOO CLOSE ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
//							ELSE
//								PRINTLN("STORE_VEH_DATA_FROM_PEDCLF: PTVS_1_playerWithVehicle FAR ENOUGH ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	STORE_VEH_DATA_FROM_VEH(pedIndex, vehicleIndex, sVehData,
			vVehStoredCoord, fVehStoredHead, eVehState	#IF USE_TU_CHANGES	, eVehGen	#ENDIF	)
	
ENDPROC
PROC STORE_VEH_DATA_FROM_PEDNRM(PED_INDEX pedIndex, PED_VEH_DATA_STRUCT &sVehData,
		VECTOR &vVehStoredCoord, FLOAT &fVehStoredHead, enumTimetableVehState &eVehState
		#IF USE_TU_CHANGES	, VEHICLE_GEN_NAME_ENUM &eVehGen	#ENDIF	)
	
//	eVehState = PTVS_0_noVehicle
//	vVehStoredCoord = <<0,0,0>>
//	fVehStoredHead = 0
	VEHICLE_INDEX vehicleIndex = NULL
	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_ENTITY_DEAD(pedIndex)
			IF IS_PED_IN_ANY_VEHICLE(pedIndex)
				vehicleIndex = GET_VEHICLE_PED_IS_IN(pedIndex)
				eVehState = PTVS_2_playerInVehicle
			ELSE
				enumCharacterList ePed = GET_PLAYER_PED_ENUMNRM(pedIndex)
				IF NOT IS_ENTITY_DEAD(g_vPlayerVeh[ePed])
					IF IS_VEHICLE_SEAT_FREE(g_vPlayerVeh[ePed], VS_DRIVER)	//#1519778
						VECTOR vPedCoord, vVehCoord
						vPedCoord = GET_ENTITY_COORDS(pedIndex)
						vVehCoord = GET_ENTITY_COORDS(g_vPlayerVeh[ePed])
						
						IF VDIST2(vPedCoord, vVehCoord) < (150*150)
//							vVehStoredCoord = vVehCoord
//							fVehStoredHead = GET_ENTITY_HEADING(g_vPlayerVeh[ePed])
//							eVehState = PTVS_1_playerWithVehicle
//							vehicleIndex = g_vPlayerVeh[ePed]
//							
//							IF VDIST2(vPedCoord, vVehCoord) < (1.5*1.5)
//								PRINTLN("STORE_VEH_DATA_FROM_PEDNRM: PTVS_1_playerWithVehicle TOO CLOSE ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
//							ELSE
//								PRINTLN("STORE_VEH_DATA_FROM_PEDNRM: PTVS_1_playerWithVehicle FAR ENOUGH ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	STORE_VEH_DATA_FROM_VEH(pedIndex, vehicleIndex, sVehData,
			vVehStoredCoord, fVehStoredHead, eVehState	#IF USE_TU_CHANGES	, eVehGen	#ENDIF	)
	
ENDPROC

PROC STORE_VEH_DATA_FROM_PED(PED_INDEX pedIndex, PED_VEH_DATA_STRUCT &sVehData,
		VECTOR &vVehStoredCoord, FLOAT &fVehStoredHead, enumTimetableVehState &eVehState
		#IF USE_TU_CHANGES	, VEHICLE_GEN_NAME_ENUM &eVehGen	#ENDIF	)
	
	BOOL awkwardAngle = FALSE
	
	#if USE_CLF_DLC
		if g_bLoadedClifford	
			STORE_VEH_DATA_FROM_PEDCLF(pedIndex,sVehData,vVehStoredCoord,fVehStoredHead,eVehState	#IF USE_TU_CHANGES	, eVehGen	#ENDIF	)
			exit
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			STORE_VEH_DATA_FROM_PEDNRM(pedIndex,sVehData,vVehStoredCoord,fVehStoredHead,eVehState	#IF USE_TU_CHANGES	, eVehGen	#ENDIF	)
			exit
		endif
	#endif
	
//	eVehState = PTVS_0_noVehicle
//	vVehStoredCoord = <<0,0,0>>
//	fVehStoredHead = 0
	VEHICLE_INDEX vehicleIndex = NULL
	IF DOES_ENTITY_EXIST(pedIndex)
		IF NOT IS_ENTITY_DEAD(pedIndex)
			IF IS_PED_IN_ANY_VEHICLE(pedIndex)
				vehicleIndex = GET_VEHICLE_PED_IS_IN(pedIndex)
				eVehState = PTVS_2_playerInVehicle
			ELSE
				enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedIndex)
				IF NOT IS_ENTITY_DEAD(g_vPlayerVeh[ePed])
					IF IS_VEHICLE_SEAT_FREE(g_vPlayerVeh[ePed], VS_DRIVER)	//#1519778
						VECTOR vPedCoord, vVehCoord
						vPedCoord = GET_ENTITY_COORDS(pedIndex)
						vVehCoord = GET_ENTITY_COORDS(g_vPlayerVeh[ePed])
						
						//B*-2337678
						IF GET_ENTITY_MODEL(g_vPlayerVeh[ePed]) = luxor2
							IF VDIST(vPedCoord, vVehCoord) < 10.0
								PRINTLN("STORE_VEH_DATA_FROM_PED: PTVS_1_playerWithVehicle  ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
								eVehState = PTVS_2_playerInVehicle
								vehicleIndex = g_vPlayerVeh[ePed]
								awkwardAngle = TRUE
							ELSE
								PRINTLN("STORE_VEH_DATA_FROM_PED: PTVS_1_playerWithVehicle TOO FAR ", GET_PLAYER_PED_STRING(ePed), " ", VDIST(vPedCoord, vVehCoord))
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	STORE_VEH_DATA_FROM_VEH(pedIndex, vehicleIndex, sVehData,
			vVehStoredCoord, fVehStoredHead, eVehState	#IF USE_TU_CHANGES	, eVehGen	#ENDIF , awkwardAngle	)
			
	CPRINTLN(DEBUG_SWITCH,"eVehState = ",eVehState)
	
ENDPROC


/// PURPOSE: 
PROC SET_VEH_DATA_FROM_STRUCT(VEHICLE_INDEX &vehicleIndex, PED_VEH_DATA_STRUCT sVehData)

	// Colours
    IF sVehData.bColourCombo
        SET_VEHICLE_COLOUR_COMBINATION(vehicleIndex, sVehData.iColourCombo)
    ELSE
        SET_VEHICLE_COLOURS(vehicleIndex, sVehData.iColour1, sVehData.iColour2)
    ENDIF
    IF sVehData.bColourExtra
        SET_VEHICLE_EXTRA_COLOURS(vehicleIndex, sVehData.iColourExtra1, sVehData.iColourExtra2)
    ENDIF
    
    // Dirt
    SET_VEHICLE_DIRT_LEVEL(vehicleIndex, sVehData.fDirtLevel)
	
    SET_ENTITY_HEALTH(vehicleIndex, sVehData.fHealth)
    
    // Extras
	INT iExtra
	REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
		SET_VEHICLE_EXTRA(vehicleIndex, iExtra+1, (NOT sVehData.bExtraOn[iExtra]))
	ENDREPEAT
	
	IF sVehData.bConvertible
		IF IS_VEHICLE_A_CONVERTIBLE(vehicleIndex)
			RAISE_CONVERTIBLE_ROOF(vehicleIndex, TRUE)		//SET_CONVERTIBLE_ROOF(vehicleIndex, sVehData.bConvertible)
		ENDIF
	ENDIF
	
	// Number plate
	// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
	INT iPlateBack
	TEXT_LABEL_15 tlPlateText
	IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
	AND sVehData.bIsPlayerVehicle
		
		#IF NOT USE_TU_CHANGES
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehicleIndex, sVehData.tlNumberPlate)
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehicleIndex, sVehData.iPlateBack)
	ELSE
		#ENDIF
		
		#IF USE_TU_CHANGES
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehicleIndex, tlPlateText)
		SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehicleIndex, iPlateBack)
	ELIF NOT IS_STRING_NULL_OR_EMPTY(sVehData.tlNumberPlate)
		#ENDIF
		
		SET_VEHICLE_NUMBER_PLATE_TEXT(vehicleIndex, sVehData.tlNumberPlate)
		IF sVehData.iPlateBack >= 0
		AND sVehData.iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
			SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(vehicleIndex, sVehData.iPlateBack)
		ENDIF
	ENDIF
				
	// Tyres
	SET_VEHICLE_TYRE_SMOKE_COLOR(vehicleIndex, sVehData.iTyreR, sVehData.iTyreG, sVehData.iTyreB)
	SET_VEHICLE_TYRES_CAN_BURST(vehicleIndex, sVehData.bTyresCanBurst)
	
	// Window tint
	SET_VEHICLE_WINDOW_TINT(vehicleIndex, sVehData.iWindowTintColour)
	
	// Neon lights
	SET_VEHICLE_NEON_COLOUR(vehicleIndex, sVehData.iNeonR, sVehData.iNeonG, sVehData.iNeonB)
	SET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_FRONT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
	SET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_BACK, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
	SET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_LEFT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
	SET_VEHICLE_NEON_ENABLED(vehicleIndex, NEON_RIGHT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
	
	// Livery
	IF GET_VEHICLE_LIVERY_COUNT(vehicleIndex) > 1
	AND sVehData.iLivery >= 0
		SET_VEHICLE_LIVERY(vehicleIndex, sVehData.iLivery)
	ENDIF
	
	// Wheels
	IF sVehData.iWheelType > -1
		IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehicleIndex))
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehicleIndex))
				IF (INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType) = MWT_BIKE)
					SET_VEHICLE_WHEEL_TYPE(vehicleIndex, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
				ENDIF
			ELSE
				SET_VEHICLE_WHEEL_TYPE(vehicleIndex, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
			ENDIF
		ENDIF
	ENDIF
	
	// Mods
	SET_VEHICLE_MOD_DATA(vehicleIndex, sVehData.iModIndex, sVehData.iModVariation)
	
	// Env Eff
	SET_VEHICLE_ENVEFF_SCALE(vehicleIndex, sVehData.fEnvEff)
				
ENDPROC



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PLAYER PED CREATION FUNCTIONS                                                                                                               ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

ENUM enumCreateState
	eCS_0_ok_to_create = 0,
	
	eCS_1_PLAYER_NOT_PLAYING,
	eCS_2_PLAYER_LAST_SCENE_DEAD,
	eCS_3_SWITCH_IN_PROGRESS,
	eCS_4_INVALID_LAST_TIME_ACTIVE,
	eCS_5_TOO_MANY_HOURS_PASSED,
	eCS_6_INVALID_LAST_KNOWN_COORDS,
	eCS_7_TOO_FAR_TO_CREATE,
	eCS_8_TOO_CLOSE_TO_CREATE,
	eCS_9_SPHERE_VISIBLE,
	eCS_10_POST_MISSION_SWITCH,
	eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY,
	eCS_12_NEAR_MISSION_TRIGGER,
	eCS_13_LEADIN_TRIGGER,
	eCS_14_DIRECTOR_MODE,
	
	// // // // // // // // // // // // // // // // // // // // // // // // 
	
	eDS_0_ok_to_delete,				//15
	
	eDS_1_PLAYER_NOT_PLAYING,		//16
	eDS_2_PLAYER_LAST_SCENE_DEAD,	//17
	eDS_3_CURRENT_SELECTOR_PED,		//18
	eDS_4_NEW_SELECTOR_PED,			//19
	eDS_5_SWITCH_IN_PROGRESS,		//20
	eDS_6_TOO_CLOSE_TO_DELETE,		//21
	eDS_7_ON_SCREEN,				//22
//	eDS_8_SWITCH_TYPE_MISSION,		//*23
	eDS_9_SWITCH_STATE_PROCESSING,	//24
	eDS_10_LEADIN,					//25
	eDS_11_DIRECTOR_MODE,			//26
	
	// // // // // // // // // // // // // // // // // // // // // // // // 
	
	eCS_MAX
ENDENUM

CONST_FLOAT fCONST_FRIEND_TOO_FAR_TO_CREATE			150.0
CONST_FLOAT fCONST_FRIEND_TOO_CLOSE_TO_CREATE		15.0

CONST_FLOAT fCONST_FRIEND_TOO_CLOSE_TO_DELETE		200.0
CONST_FLOAT fCONST_FRIEND_TOO_FAR_TO_SEE			40.0

FUNC BOOL SafeToCreatePlayerAmbientPedAtLastKnownLocationCLF(enumCharacterList ePed,enumCreateState &eCreateState)
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		eCreateState = eCS_1_PLAYER_NOT_PLAYING
		RETURN FALSE
	ENDIF
	
	IF (g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
		eCreateState = eCS_2_PLAYER_LAST_SCENE_DEAD
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		IF GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
		
		IF GET_PLAYER_SWITCH_STATE() != SWITCH_STATE_JUMPCUT_DESCENT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
	ENDIF
	
	TIMEOFDAY sLastTimeActive = g_savedGlobalsClifford.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		eCreateState = eCS_4_INVALID_LAST_TIME_ACTIVE
		RETURN FALSE
	ENDIF
	IF HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, g_iCONST_GAME_HOURS_FOR_DEFAULT_SWITCH)
		eCreateState = eCS_5_TOO_MANY_HOURS_PASSED
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		eCreateState = eCS_14_DIRECTOR_MODE
		RETURN FALSE
	ENDIF
	
	VECTOR vFriendCharLastKnown			= g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed]
	IF ARE_VECTORS_EQUAL(vFriendCharLastKnown, <<0,0,0>>)
	AND (g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed] = 0)
		eCreateState = eCS_6_INVALID_LAST_KNOWN_COORDS
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCharCurrent			= GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerDistFromCharLastKnown2	= VDIST2(vFriendCharLastKnown, vPlayerCharCurrent)
	
	IF (fPlayerDistFromCharLastKnown2 > (fCONST_FRIEND_TOO_FAR_TO_CREATE*fCONST_FRIEND_TOO_FAR_TO_CREATE))
		eCreateState = eCS_7_TOO_FAR_TO_CREATE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_TOO_CLOSE_TO_CREATE*fCONST_FRIEND_TOO_CLOSE_TO_CREATE))
			eCreateState = eCS_8_TOO_CLOSE_TO_CREATE
			RETURN FALSE
		ENDIF
	
		CONST_FLOAT fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE		100.0
		CONST_FLOAT fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE		30.0
		
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE*fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE))
			IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE*fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE))
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 3.0)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 3.0, HUD_COLOUR_BLUELIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 0.5)
				AND WOULD_ENTITY_BE_OCCLUDED(GET_PLAYER_PED_MODEL(ePed), vFriendCharLastKnown, FALSE)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 1.0, HUD_COLOUR_REDLIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	
	
	IF GetLastKnownPedInfoPostMission(g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
		eCreateState = eCS_10_POST_MISSION_SWITCH
		RETURN FALSE
	ENDIF		

	
	IF g_bSupressAmbientPlayersForFriendActivity
		eCreateState = eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY
		RETURN FALSE
	ENDIF
	
	STATIC_BLIP_NAME_ENUM eBlip
	REPEAT g_iTotalStaticBlips eBlip
		IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_ACTIVE)
			
			CONST_FLOAT	fCONST_MIN_DIST_FROM_MISSION_BLIP	10.0
			VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
			IF VDIST2(vFriendCharLastKnown, vBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
				eCreateState = eCS_12_NEAR_MISSION_TRIGGER
				RETURN FALSE
			ENDIF
			
			//for multi-char missions
			IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eBlip].bMultiCoordAndSprite
				INT iMultiBlip, iMULTI_BLIP_COUNT = 3
				REPEAT iMULTI_BLIP_COUNT iMultiBlip
					VECTOR vMultiBlipCoord = g_GameBlips[eBlip].vCoords[iMultiBlip]
					IF NOT IS_VECTOR_ZERO(vMultiBlipCoord)
						IF VDIST2(vFriendCharLastKnown, vMultiBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
							eCreateState = eCS_12_NEAR_MISSION_TRIGGER
							RETURN FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	INT iTriggerIndex
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
			SP_MISSIONS eMissionID = g_TriggerableMissionsTU[iTriggerIndex].eMissionID
			FLOAT fFriendRejectDistance = g_TriggerableMissionsTU[iTriggerIndex].sScene.fFriendRejectDistance
			fFriendRejectDistance *= 1.5		//#1721545
			INT iFriendsToAcceptBitset = g_TriggerableMissionsTU[iTriggerIndex].sScene.iFriendsToAcceptBitset
			
			eBlip = g_sMissionStaticData[eMissionID].blip
			
			VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
			FLOAT fDist2FamilyToTrigger = VDIST2(vFriendCharLastKnown, vBlipCoord)
			
			IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(ePed))
				IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
					eCreateState = eCS_13_LEADIN_TRIGGER
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	//
	eCreateState = eCS_0_ok_to_create
	RETURN TRUE
ENDFUNC
FUNC BOOL SafeToCreatePlayerAmbientPedAtLastKnownLocationNRM(enumCharacterList ePed,enumCreateState &eCreateState)
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		eCreateState = eCS_1_PLAYER_NOT_PLAYING
		RETURN FALSE
	ENDIF
	
	IF (g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
		eCreateState = eCS_2_PLAYER_LAST_SCENE_DEAD
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		IF GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
		
		IF GET_PLAYER_SWITCH_STATE() != SWITCH_STATE_JUMPCUT_DESCENT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
	ENDIF
	
	TIMEOFDAY sLastTimeActive = g_savedGlobalsnorman.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		eCreateState = eCS_4_INVALID_LAST_TIME_ACTIVE
		RETURN FALSE
	ENDIF
	IF HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, g_iCONST_GAME_HOURS_FOR_DEFAULT_SWITCH)
		eCreateState = eCS_5_TOO_MANY_HOURS_PASSED
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		eCreateState = eCS_14_DIRECTOR_MODE
		RETURN FALSE
	ENDIF
	
	VECTOR vFriendCharLastKnown			= g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed]
	IF ARE_VECTORS_EQUAL(vFriendCharLastKnown, <<0,0,0>>)
	AND (g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = 0)
		eCreateState = eCS_6_INVALID_LAST_KNOWN_COORDS
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCharCurrent			= GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerDistFromCharLastKnown2	= VDIST2(vFriendCharLastKnown, vPlayerCharCurrent)
	
	IF (fPlayerDistFromCharLastKnown2 > (fCONST_FRIEND_TOO_FAR_TO_CREATE*fCONST_FRIEND_TOO_FAR_TO_CREATE))
		eCreateState = eCS_7_TOO_FAR_TO_CREATE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_TOO_CLOSE_TO_CREATE*fCONST_FRIEND_TOO_CLOSE_TO_CREATE))
			eCreateState = eCS_8_TOO_CLOSE_TO_CREATE
			RETURN FALSE
		ENDIF
	
		CONST_FLOAT fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE		100.0
		CONST_FLOAT fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE		30.0
		
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE*fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE))
			IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE*fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE))
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 3.0)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 3.0, HUD_COLOUR_BLUELIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 0.5)
				AND WOULD_ENTITY_BE_OCCLUDED(GET_PLAYER_PED_MODELNRM(ePed), vFriendCharLastKnown, FALSE)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 1.0, HUD_COLOUR_REDLIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	
	
	IF GetLastKnownPedInfoPostMission(g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
		eCreateState = eCS_10_POST_MISSION_SWITCH
		RETURN FALSE
	ENDIF		
	
	IF g_bSupressAmbientPlayersForFriendActivity
		eCreateState = eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY
		RETURN FALSE
	ENDIF
	
	STATIC_BLIP_NAME_ENUM eBlip
	REPEAT g_iTotalStaticBlips eBlip
		IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_ACTIVE)
			
			CONST_FLOAT	fCONST_MIN_DIST_FROM_MISSION_BLIP	10.0
			VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
			IF VDIST2(vFriendCharLastKnown, vBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
				eCreateState = eCS_12_NEAR_MISSION_TRIGGER
				RETURN FALSE
			ENDIF
			
			//for multi-char missions
			IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eBlip].bMultiCoordAndSprite
				INT iMultiBlip, iMULTI_BLIP_COUNT = 3
				REPEAT iMULTI_BLIP_COUNT iMultiBlip
					VECTOR vMultiBlipCoord = g_GameBlips[eBlip].vCoords[iMultiBlip]
					IF NOT IS_VECTOR_ZERO(vMultiBlipCoord)
						IF VDIST2(vFriendCharLastKnown, vMultiBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
							eCreateState = eCS_12_NEAR_MISSION_TRIGGER
							RETURN FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	INT iTriggerIndex
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
			SP_MISSIONS eMissionID = g_TriggerableMissionsTU[iTriggerIndex].eMissionID
			FLOAT fFriendRejectDistance = g_TriggerableMissionsTU[iTriggerIndex].sScene.fFriendRejectDistance
			fFriendRejectDistance *= 1.5		//#1721545
			INT iFriendsToAcceptBitset = g_TriggerableMissionsTU[iTriggerIndex].sScene.iFriendsToAcceptBitset
			
			eBlip = g_sMissionStaticData[eMissionID].blip
			
			VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
			FLOAT fDist2FamilyToTrigger = VDIST2(vFriendCharLastKnown, vBlipCoord)
			
			IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(ePed))
				IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
					eCreateState = eCS_13_LEADIN_TRIGGER
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	//
	eCreateState = eCS_0_ok_to_create
	RETURN TRUE
ENDFUNC
FUNC BOOL SafeToCreatePlayerAmbientPedAtLastKnownLocation(enumCharacterList ePed,enumCreateState &eCreateState)

#if USE_CLF_DLC
	if g_bLoadedClifford
		return SafeToCreatePlayerAmbientPedAtLastKnownLocationCLF(ePed,eCreateState)
	endif
#endif	
#if USE_NRM_DLC
	if g_bLoadedNorman
		return SafeToCreatePlayerAmbientPedAtLastKnownLocationNRM(ePed,eCreateState)
	endif
#endif
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		eCreateState = eCS_1_PLAYER_NOT_PLAYING
		RETURN FALSE
	ENDIF
	
	IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
		eCreateState = eCS_2_PLAYER_LAST_SCENE_DEAD
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		IF GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_SHORT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
		
		IF GET_PLAYER_SWITCH_STATE() != SWITCH_STATE_JUMPCUT_DESCENT
			eCreateState = eCS_3_SWITCH_IN_PROGRESS
			RETURN FALSE
		ENDIF
	ENDIF
	
	TIMEOFDAY sLastTimeActive = g_savedGlobals.sPlayerData.sInfo.sLastTimeActive[ePed]
	IF NOT Is_TIMEOFDAY_Valid(sLastTimeActive)
		eCreateState = eCS_4_INVALID_LAST_TIME_ACTIVE
		RETURN FALSE
	ENDIF
	IF HasNumOfHoursPassedSincePedTimeStruct(sLastTimeActive, g_iCONST_GAME_HOURS_FOR_DEFAULT_SWITCH)
		eCreateState = eCS_5_TOO_MANY_HOURS_PASSED
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		eCreateState = eCS_14_DIRECTOR_MODE
		RETURN FALSE
	ENDIF
	
	VECTOR vFriendCharLastKnown			= g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
	IF ARE_VECTORS_EQUAL(vFriendCharLastKnown, <<0,0,0>>)
	AND (g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = 0)
		eCreateState = eCS_6_INVALID_LAST_KNOWN_COORDS
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCharCurrent			= GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerDistFromCharLastKnown2	= VDIST2(vFriendCharLastKnown, vPlayerCharCurrent)
	
	IF (fPlayerDistFromCharLastKnown2 > (fCONST_FRIEND_TOO_FAR_TO_CREATE*fCONST_FRIEND_TOO_FAR_TO_CREATE))
		eCreateState = eCS_7_TOO_FAR_TO_CREATE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_TOO_CLOSE_TO_CREATE*fCONST_FRIEND_TOO_CLOSE_TO_CREATE))
			eCreateState = eCS_8_TOO_CLOSE_TO_CREATE
			RETURN FALSE
		ENDIF
	
		CONST_FLOAT fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE		100.0
		CONST_FLOAT fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE		30.0
		
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE*fCONST_FRIEND_IGNORE_VISIBILITY_TO_CREATE))
			IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE*fCONST_FRIENDSPHERE_VISIBILITY_TO_CREATE))
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 3.0)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 3.0, HUD_COLOUR_BLUELIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ELSE
				IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 0.5)
				AND WOULD_ENTITY_BE_OCCLUDED(GET_PLAYER_PED_MODEL(ePed), vFriendCharLastKnown, FALSE)
					eCreateState = eCS_9_SPHERE_VISIBLE
					
					#IF IS_DEBUG_BUILD
					DrawDebugSceneSphere(vFriendCharLastKnown, 1.0, HUD_COLOUR_REDLIGHT, 0.1)
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vLastKnownCoords
	FLOAT fLastKnownHead
	
	#if USE_CLF_DLC
		IF GetLastKnownPedInfoPostMission(g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
			eCreateState = eCS_10_POST_MISSION_SWITCH
			RETURN FALSE
		ENDIF		
	#endif
	#if USE_NRM_DLC
		IF GetLastKnownPedInfoPostMission(g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
			eCreateState = eCS_10_POST_MISSION_SWITCH
			RETURN FALSE
		ENDIF		
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		IF GetLastKnownPedInfoPostMission(g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed], vLastKnownCoords, fLastKnownHead)
			eCreateState = eCS_10_POST_MISSION_SWITCH
			RETURN FALSE
		ENDIF		
	#endif
	#endif
	
	IF g_bSupressAmbientPlayersForFriendActivity
		eCreateState = eCS_11_SUPRESSED_FOR_FRIEND_ACTIVITY
		RETURN FALSE
	ENDIF
	
	STATIC_BLIP_NAME_ENUM eBlip
	REPEAT g_iTotalStaticBlips eBlip
		IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_ACTIVE)
			
			CONST_FLOAT	fCONST_MIN_DIST_FROM_MISSION_BLIP	10.0
			VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
			IF VDIST2(vFriendCharLastKnown, vBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
				eCreateState = eCS_12_NEAR_MISSION_TRIGGER
				RETURN FALSE
			ENDIF
			
			//for multi-char missions
			IF IS_BIT_SET(g_GameBlips[eBlip].iSetting,STATIC_BLIP_SETTING_MULTI_COORD_AND_SPRITE)//g_GameBlips[eBlip].bMultiCoordAndSprite
				INT iMultiBlip, iMULTI_BLIP_COUNT = 3
				REPEAT iMULTI_BLIP_COUNT iMultiBlip
					VECTOR vMultiBlipCoord = g_GameBlips[eBlip].vCoords[iMultiBlip]
					IF NOT IS_VECTOR_ZERO(vMultiBlipCoord)
						IF VDIST2(vFriendCharLastKnown, vMultiBlipCoord) < (fCONST_MIN_DIST_FROM_MISSION_BLIP*fCONST_MIN_DIST_FROM_MISSION_BLIP)
							eCreateState = eCS_12_NEAR_MISSION_TRIGGER
							RETURN FALSE
						ENDIF
					ENDIF
				ENDREPEAT
				
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	INT iTriggerIndex
	
	#if USE_CLF_DLC	
		REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
			IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
				SP_MISSIONS eMissionID = g_TriggerableMissionsTU[iTriggerIndex].eMissionID
				FLOAT fFriendRejectDistance = g_TriggerableMissionsTU[iTriggerIndex].sScene.fFriendRejectDistance
				fFriendRejectDistance *= 1.5		//#1721545
				INT iFriendsToAcceptBitset = g_TriggerableMissionsTU[iTriggerIndex].sScene.iFriendsToAcceptBitset
				
				eBlip = g_sMissionStaticData[eMissionID].blip
				
				VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
				FLOAT fDist2FamilyToTrigger = VDIST2(vFriendCharLastKnown, vBlipCoord)
				
				IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(ePed))
					IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
						eCreateState = eCS_13_LEADIN_TRIGGER
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
			IF g_TriggerableMissionsTU[iTriggerIndex].bUsed
				SP_MISSIONS eMissionID = g_TriggerableMissionsTU[iTriggerIndex].eMissionID
				FLOAT fFriendRejectDistance = g_TriggerableMissionsTU[iTriggerIndex].sScene.fFriendRejectDistance
				fFriendRejectDistance *= 1.5		//#1721545
				INT iFriendsToAcceptBitset = g_TriggerableMissionsTU[iTriggerIndex].sScene.iFriendsToAcceptBitset
				
				eBlip = g_sMissionStaticData[eMissionID].blip
				
				VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
				FLOAT fDist2FamilyToTrigger = VDIST2(vFriendCharLastKnown, vBlipCoord)
				
				IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(ePed))
					IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
						eCreateState = eCS_13_LEADIN_TRIGGER
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	#endif
	#if not USE_SP_DLC
		REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
			IF g_TriggerableMissions[iTriggerIndex].bUsed
				SP_MISSIONS eMissionID = g_TriggerableMissions[iTriggerIndex].eMissionID
				FLOAT fFriendRejectDistance = g_TriggerableMissions[iTriggerIndex].sScene.fFriendRejectDistance
				fFriendRejectDistance *= 1.5		//#1721545
				INT iFriendsToAcceptBitset = g_TriggerableMissions[iTriggerIndex].sScene.iFriendsToAcceptBitset
				
				eBlip = g_sMissionStaticData[eMissionID].blip
				
				VECTOR vBlipCoord = g_GameBlips[eBlip].vCoords[0]
				FLOAT fDist2FamilyToTrigger = VDIST2(vFriendCharLastKnown, vBlipCoord)
				
				IF NOT IS_BIT_SET(iFriendsToAcceptBitset, ENUM_TO_INT(ePed))
					IF (fDist2FamilyToTrigger < (fFriendRejectDistance*fFriendRejectDistance))
						eCreateState = eCS_13_LEADIN_TRIGGER
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT	
	#endif
	
	
	//
	eCreateState = eCS_0_ok_to_create
	RETURN TRUE
ENDFUNC
FUNC BOOL SafeToDeletePlayerAmbientPed(PED_INDEX pedID, enumCharacterList ePed, SELECTOR_SLOTS_ENUM eSelectorChar,
		enumCreateState &eDeleteState)
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		eDeleteState = eDS_1_PLAYER_NOT_PLAYING
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
		IF (g_savedGlobalsClifford.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
			eDeleteState = eDS_2_PLAYER_LAST_SCENE_DEAD
			RETURN FALSE
		ENDIF	
	#endif
	#if USE_NRM_DLC
		IF (g_savedGlobalsnorman.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
			eDeleteState = eDS_2_PLAYER_LAST_SCENE_DEAD
			RETURN FALSE
		ENDIF	
	#endif
	#if not USE_SP_DLC
		IF (g_SavedGlobals.sPlayerSceneData.g_ePlayerLastScene[ePed] = PR_SCENE_DEAD)
			eDeleteState = eDS_2_PLAYER_LAST_SCENE_DEAD
			RETURN FALSE
		ENDIF	
	#endif
	
//	IF g_sPlayerPedRequest.sSelectorPeds.eCurrentSelectorPed = eSelectorChar
//		eDeleteState = eDS_3_CURRENT_SELECTOR_PED
//		RETURN FALSE
//	ENDIF
	IF g_sPlayerPedRequest.sSelectorPeds.eNewSelectorPed = eSelectorChar
		eDeleteState = eDS_4_NEW_SELECTOR_PED
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		eDeleteState = eDS_5_SWITCH_IN_PROGRESS
		RETURN FALSE
	ENDIF
	
	IF g_sPlayerPedRequest.eState = PR_STATE_PROCESSING
		eDeleteState = eDS_9_SWITCH_STATE_PROCESSING
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_DIRECTOR)
		eDeleteState = eDS_11_DIRECTOR_MODE
		RETURN TRUE
	ENDIF
	
	VECTOR vFriendCharLastKnown			= GET_ENTITY_COORDS(pedID, FALSE)
	VECTOR vPlayerCharCurrent			= GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	FLOAT fPlayerDistFromCharLastKnown2	= VDIST2(vFriendCharLastKnown, vPlayerCharCurrent)
	
	IF HAS_COLLISION_LOADED_AROUND_ENTITY(pedID)
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_TOO_CLOSE_TO_DELETE*fCONST_FRIEND_TOO_CLOSE_TO_DELETE))
			eDeleteState = eDS_6_TOO_CLOSE_TO_DELETE
			RETURN FALSE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		//[MF] Commenting the following out as a fix for B* 1681207
//		SAVE_STRING_TO_DEBUG_FILE("no collision around ped at dist [")
//		SAVE_FLOAT_TO_DEBUG_FILE(SQRT(fPlayerDistFromCharLastKnown2))
//		SAVE_STRING_TO_DEBUG_FILE("]")
//		SAVE_NEWLINE_TO_DEBUG_FILE()
		#ENDIF
		
		
		PRINTSTRING("no collision around ped at dist [")
		PRINTFLOAT(SQRT(fPlayerDistFromCharLastKnown2))
		PRINTSTRING("]")
		PRINTNL()
	ENDIF
	IF IS_SPHERE_VISIBLE(vFriendCharLastKnown, 5.0)
		IF (fPlayerDistFromCharLastKnown2 < (fCONST_FRIEND_TOO_FAR_TO_SEE*fCONST_FRIEND_TOO_FAR_TO_SEE))
			eDeleteState = eDS_7_ON_SCREEN
			RETURN FALSE
		ENDIF
	ENDIF
//	IF g_sPlayerPedRequest.eType = PR_TYPE_MISSION
//		eDeleteState = eDS_8_SWITCH_TYPE_MISSION
//		RETURN FALSE
//	ENDIF
	
	// // #1356105 // // // // // // // // // // // // // // //
	SP_MISSIONS eTriggerMission = g_eMissionTriggerProcessing
	IF (eTriggerMission != SP_MISSION_NONE)
		
		SELECTOR_SLOTS_ENUM eSelectorPed = NUMBER_OF_SELECTOR_PEDS
		IF ePed = CHAR_MICHAEL
			eSelectorPed = SELECTOR_PED_MICHAEL
		ELIF ePed = CHAR_TREVOR
			eSelectorPed = SELECTOR_PED_TREVOR
		ELIF ePed = CHAR_FRANKLIN
			eSelectorPed = SELECTOR_PED_FRANKLIN
		ELIF ePed = NO_CHARACTER
			eSelectorPed = SELECTOR_PED_MULTIPLAYER
			RETURN FALSE
		ENDIF
		
		IF IS_BIT_SET(g_sPlayerPedRequest.iBitsetPedInLeadIn, ENUM_TO_INT(eSelectorPed))
			eDeleteState = eDS_10_LEADIN
			RETURN FALSE
		ENDIF
	ENDIF
	
	// // // // // // // // // // // // // // // // // // // //
	
	
	//
	eDeleteState = eDS_0_ok_to_delete
	RETURN TRUE
ENDFUNC

/// PURPOSE: Set any special outfits when the player is created at this point in the flow.
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTSCLF(PED_INDEX pedID, BOOL bIgnoreSPUnderwearCheck)

	enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission.")
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_UNDERWEAR_SP(pedID)
	AND NOT bIgnoreSPUnderwearCheck
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - ", GET_PLAYER_PED_STRING(ePed), " is in underwear.")
		DEBUG_PRINTCALLSTACK()
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
		RETURN TRUE
	ENDIF
	
	BOOL bScubaRemoved = FALSE
	
	SWITCH ePed
		CASE CHAR_MICHAEL
						
			//Bed?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_BED)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has bed outfit on")				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			//Ludendorff?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Ludendorff outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER_NO_MASK)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Scuba Water (no mask) outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			bScubaRemoved = FALSE
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P0_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_SCUBA_ACCS_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//Security?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SECURITY)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Michael has Security outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			bScubaRemoved = FALSE
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P1_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Franklin has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P1_SCUBA_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Franklin has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P1_DUMMY, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//White Tuxedo?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Franklin has White Tuxedo outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_TOILET)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has toilet outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_LADIES)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has ladies outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_TORSO, TORSO_P2_DRESS)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has dress torso on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			bScubaRemoved = FALSE
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P2_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Trevor has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P2_SCUBA_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Trevor has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P2_DUMMY, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//Security?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SECURITY)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSCLF() - Checking Trevor has Security outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTSNRM(PED_INDEX pedID, BOOL bIgnoreSPUnderwearCheck)

	enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission.")
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_UNDERWEAR_SP(pedID)
	AND NOT bIgnoreSPUnderwearCheck
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - ", GET_PLAYER_PED_STRING(ePed), " is in underwear.")
		DEBUG_PRINTCALLSTACK()
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
		RETURN TRUE
	ENDIF
	
	BOOL bScubaRemoved = FALSE
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			
			//Bed?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_BED)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has bed outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			//Ludendorff?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Ludendorff outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER_NO_MASK)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Scuba Water (no mask) outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			bScubaRemoved = FALSE
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P0_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_SCUBA_ACCS_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//Security?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SECURITY)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTSNRM() - Checking Michael has Security outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			
		BREAK
		
		CASE CHAR_TREVOR
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTS(PED_INDEX pedID, BOOL bIgnoreSPUnderwearCheck = FALSE)

#if USE_CLF_DLC
	return SET_FLOW_OUTFIT_REQUIREMENTSCLF(pedID, bIgnoreSPUnderwearCheck)
#endif	
#if USE_NRM_DLC
	return SET_FLOW_OUTFIT_REQUIREMENTSNRM(pedID, bIgnoreSPUnderwearCheck)	
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
	enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission.")
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
		STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
		RETURN TRUE
	ENDIF
	
	IF NOT bIgnoreSPUnderwearCheck
		IF IS_PED_IN_UNDERWEAR_SP(pedID)
			PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - ", GET_PLAYER_PED_STRING(ePed), " is in underwear.")
			
			RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
			SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
			STORE_PLAYER_PED_VARIATIONS(pedID, TRUE)
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - ", GET_PLAYER_PED_STRING(ePed), " ignores the underwear check.")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
	BOOL bScubaRemoved = FALSE
	
	SWITCH ePed
		CASE CHAR_MICHAEL
			
			// Default outfit?
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
			AND NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_REM_HAGGARD_SUIT]
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Setting haggard suit on Michael")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_DEFAULT, FALSE)
				g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_SET_HAGGARD_SUIT] = TRUE
				RETURN TRUE
			ENDIF
			
			// Topless?
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HIDE_BARE_CHEST]
				g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HIDE_BARE_CHEST] = FALSE
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has a top on")
				
				IF NOT IS_PED_INJURED(pedID)
					IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_TORSO, TORSO_P0_BARE_CHEST)
						SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_TORSO, TORSO_P0_YOGA_0, FALSE)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Bed?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_BED)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has bed outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			//Ludendorff?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_LUDENDORFF)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Ludendorff outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P0_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER_NO_MASK)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Scuba Water (no mask) outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P0_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_SCUBA_ACCS_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Michael has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P0_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//Security?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_SECURITY)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Michael has Security outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P1_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P1_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Franklin has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P1_SCUBA_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Franklin has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P1_DUMMY, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//White Tuxedo?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_WHITE_TUXEDO)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Franklin has White Tuxedo outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P1_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_TOILET)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has toilet outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_LADIES)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has ladies outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			
			IF NOT bIgnoreSPUnderwearCheck
				IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_TORSO, TORSO_P2_DRESS)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has dress torso on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - ", GET_PLAYER_PED_STRING(ePed), " ignores Trevors has dress torso check.")
				DEBUG_PRINTCALLSTACK()
			ENDIF

			
			//Boiler suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_1)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_1)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Boiler Suit 1 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_2)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_2)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Boiler Suit 2 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3)
				IF NOT IS_PED_COMP_ITEM_AVAILABLE_SP(GET_ENTITY_MODEL(pedID), COMP_TYPE_OUTFIT, OUTFIT_P2_PREP_BOILER_SUIT_3)
					PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Boiler Suit 3 outfit on")
					
					SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
					RETURN TRUE
				ENDIF
			ENDIF
			
			//Scuba suit?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_LAND)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Scuba Land outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_WATER)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Scuba Water outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_P2_SCUBA_MASK)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Trevor has Scuba Water mask on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P2_SCUBA_1)
				PRINTLN("SET_FLOW_prop_REQUIREMENTS() - Checking Trevor has Scuba Water tank on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_SPECIAL, SPECIAL_P2_DUMMY, FALSE)
				bScubaRemoved = TRUE
			ENDIF
			IF bScubaRemoved
				RETURN TRUE
			ENDIF
			
			//Security?
			IF IS_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_SECURITY)
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS() - Checking Trevor has Security outfit on")
				
				SET_PED_COMP_ITEM_CURRENT_SP(pedID, COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
#endif
#endif
ENDFUNC


/// PURPOSE: Set any special outfits when the player is created at this point in the flow.
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLECLF(STRING sHandle, enuMCharacterList ePed)
	
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission")
		
		PED_COMP_OUTFIT_DATA_STRUCT sOutfitData
		PED_COMP_ITEM_DATA_STRUCT sItemData
		sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODELCLF(ePed), OUTFIT_DEFAULT)
		
		INT iItemType, iProp
		REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
		
			IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
				sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODELCLF(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
				IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
					// Dont set any props
					REPEAT NUM_PLAYER_PED_PROPS iProp
						SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODEL(ePed))
					ENDREPEAT
				ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
				AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sItemData.iDrawable, sItemData.iTexture, GET_PLAYER_PED_MODELCLF(ePed))
				ENDIF
			ENDIF
		ENDREPEAT
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		RETURN TRUE
	ENDIF

	
	SWITCH ePed
		CASE CHAR_MICHAEL
			
		BREAK
		
		CASE CHAR_TREVOR
			PED_COMP_OUTFIT_DATA_STRUCT sOutfitData, sDefaultOutfitData
			PED_COMP_ITEM_DATA_STRUCT sItemData, sDefaultItemData
			
			// If Trevor is not wearing a dress, make sure he doesnt appear in just his underwear.
			sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), COMP_TYPE_TORSO, TORSO_P2_DRESS)
			IF g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_TORSO] = sItemData.iDrawable
				// do nothing.
			ELSE
				// put some clothes on
				sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_P2_UNDERWEAR)
				sDefaultOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_P2_DEFAULT)
				
				INT iItemType, iProp
				REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
					IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
						IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
							// Dont set any props
							REPEAT NUM_PLAYER_PED_PROPS iProp
								SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODEL(ePed))
							ENDREPEAT
						ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
						AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
							
							#IF IS_DEBUG_BUILD
							IF iItemType >= NUM_PED_COMPONENTS
								PRINTLN("iItemType >= NUM_PED_COMPONENTS???")
								SCRIPT_ASSERT("iItemType >= NUM_PED_COMPONENTS???")
								
								RETURN FALSE
							ENDIF
							#ENDIF
							
							IF (iItemType = ENUM_TO_INT(COMP_TYPE_TORSO)
							OR iItemType = ENUM_TO_INT(COMP_TYPE_LEGS))
								sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
								
								IF sItemData.iDrawable = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[iItemType]
								AND sItemData.iTexture = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[iItemType]
									
									PRINTLN("//cutscene Underwear torso or legs? [ePed: ", ePed, ", iItemType: ", iItemType, ", iDrawable: ", sItemData.iDrawable, ", iTexture: ", sItemData.iTexture, "]")
	//								SCRIPT_ASSERT("//cutscene Underwear torso or legs?")
									
									sDefaultItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sDefaultOutfitData.eItems[iItemType])
									
									g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[iItemType] = sDefaultItemData.iDrawable
									g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[iItemType] = sDefaultItemData.iTexture
									
									SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sDefaultItemData.iDrawable, sDefaultItemData.iTexture, GET_PLAYER_PED_MODELCLF(ePed))
									
									IF iItemType = ENUM_TO_INT(COMP_TYPE_LEGS)
										
										PRINTLN("//cutscene Underwear feet? [ePed: ", ePed, "]")
		//								SCRIPT_ASSERT("//cutscene Underwear feet?")
										
										sDefaultItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODELCLF(ePed), COMP_TYPE_FEET, sDefaultOutfitData.eItems[COMP_TYPE_FEET])
										
										g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_FEET] = sDefaultItemData.iDrawable
										g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_FEET] = sDefaultItemData.iTexture
										
										SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_FEET), sDefaultItemData.iDrawable, sDefaultItemData.iTexture, GET_PLAYER_PED_MODELCLF(ePed))
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLENRM(STRING sHandle, enuMCharacterList ePed)
	
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission")
		
		PED_COMP_OUTFIT_DATA_STRUCT sOutfitData
		PED_COMP_ITEM_DATA_STRUCT sItemData
		sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_DEFAULT)
		
		INT iItemType, iProp
		REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
		
			IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
				sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
				IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
					// Dont set any props
					REPEAT NUM_PLAYER_PED_PROPS iProp
						SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODELNRM(ePed))
					ENDREPEAT
				ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
				AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sItemData.iDrawable, sItemData.iTexture, GET_PLAYER_PED_MODELNRM(ePed))
				ENDIF
			ENDIF
		ENDREPEAT
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		RETURN TRUE
	ENDIF

	
	SWITCH ePed
		CASE CHAR_MICHAEL
			
		BREAK
		
		CASE CHAR_NRM_JIMMY
			
		BREAK
		CASE CHAR_NRM_TRACEY
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE(STRING sHandle, enuMCharacterList ePed)
#if USE_CLF_DLC
	return SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLECLF(sHandle,ePed)		
#endif
#if USE_NRM_DLC
	return SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLENRM(sHandle,ePed)	
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed)
		PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE() - ", GET_PLAYER_PED_STRING(ePed), " has changed outfits on mission")
		
		PED_COMP_OUTFIT_DATA_STRUCT sOutfitData
		PED_COMP_ITEM_DATA_STRUCT sItemData
		sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_DEFAULT)
		
		INT iItemType, iProp
		REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
		
			IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
				sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
				IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
					// Dont set any props
					REPEAT NUM_PLAYER_PED_PROPS iProp
						SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODEL(ePed))
					ENDREPEAT
				ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
				AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sItemData.iDrawable, sItemData.iTexture, GET_PLAYER_PED_MODEL(ePed))
				ENDIF
			ENDIF
		ENDREPEAT
		
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(ePed, FALSE)
		RETURN TRUE
	ENDIF

	
	SWITCH ePed
		CASE CHAR_MICHAEL
			IF g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_HAS_HAGGARD_SUIT]
			AND NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_REM_HAGGARD_SUIT]
				PRINTLN("SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE() - Setting haggard suit on Michael")
				PED_COMP_OUTFIT_DATA_STRUCT sOutfitData
				PED_COMP_ITEM_DATA_STRUCT sItemData
				sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_DEFAULT)
				
				INT iItemType, iProp
				REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
				
					IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
						sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
						IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
							// Dont set any props
							REPEAT NUM_PLAYER_PED_PROPS iProp
								SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODEL(ePed))
							ENDREPEAT
						ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
						AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
							SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sItemData.iDrawable, sItemData.iTexture, GET_PLAYER_PED_MODEL(ePed))
						ENDIF
					ENDIF
				ENDREPEAT
				g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MIC_SET_HAGGARD_SUIT] = TRUE
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			PED_COMP_OUTFIT_DATA_STRUCT sOutfitData, sDefaultOutfitData
			PED_COMP_ITEM_DATA_STRUCT sItemData, sDefaultItemData
			
			// If Trevor is not wearing a dress, make sure he doesnt appear in just his underwear.
			sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), COMP_TYPE_TORSO, TORSO_P2_DRESS)
			IF g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_TORSO] = sItemData.iDrawable
				// do nothing.
			ELSE
				// put some clothes on
				sOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_P2_UNDERWEAR)
				sDefaultOutfitData = GET_PED_COMPONENT_DATA_FOR_OUTFIT(GET_PLAYER_PED_MODEL(ePed), OUTFIT_P2_DEFAULT)
				
				INT iItemType, iProp
				REPEAT NUMBER_OF_PED_COMP_TYPES iItemType
					IF sOutfitData.eItems[iItemType] != DUMMY_PED_COMP
						IF iItemType = ENUM_TO_INT(COMP_TYPE_PROPS)
							// Dont set any props
							REPEAT NUM_PLAYER_PED_PROPS iProp
								SET_CUTSCENE_PED_PROP_VARIATION(sHandle, INT_TO_ENUM(PED_PROP_POSITION, iProp), -1, 0, GET_PLAYER_PED_MODEL(ePed))
							ENDREPEAT
						ELIF iItemType != ENUM_TO_INT(COMP_TYPE_OUTFIT)
						AND iItemType != ENUM_TO_INT(COMP_TYPE_PROPGROUP)
							
							#IF IS_DEBUG_BUILD
							IF iItemType >= NUM_PED_COMPONENTS
								PRINTLN("iItemType >= NUM_PED_COMPONENTS???")
								SCRIPT_ASSERT("iItemType >= NUM_PED_COMPONENTS???")
								
								RETURN FALSE
							ENDIF
							#ENDIF
							
							IF (iItemType = ENUM_TO_INT(COMP_TYPE_TORSO)
							OR iItemType = ENUM_TO_INT(COMP_TYPE_LEGS))
								sItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sOutfitData.eItems[iItemType])
								
								IF sItemData.iDrawable = g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[iItemType]
								AND sItemData.iTexture = g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[iItemType]
									
									PRINTLN("//cutscene Underwear torso or legs? [ePed: ", ePed, ", iItemType: ", iItemType, ", iDrawable: ", sItemData.iDrawable, ", iTexture: ", sItemData.iTexture, "]")
	//								SCRIPT_ASSERT("//cutscene Underwear torso or legs?")
									
									sDefaultItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType), sDefaultOutfitData.eItems[iItemType])
									
									g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[iItemType] = sDefaultItemData.iDrawable
									g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[iItemType] = sDefaultItemData.iTexture
									
									SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(INT_TO_ENUM(PED_COMP_TYPE_ENUM, iItemType)), sDefaultItemData.iDrawable, sDefaultItemData.iTexture, GET_PLAYER_PED_MODEL(ePed))
									
									IF iItemType = ENUM_TO_INT(COMP_TYPE_LEGS)
										
										PRINTLN("//cutscene Underwear feet? [ePed: ", ePed, "]")
		//								SCRIPT_ASSERT("//cutscene Underwear feet?")
										
										sDefaultItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(GET_PLAYER_PED_MODEL(ePed), COMP_TYPE_FEET, sDefaultOutfitData.eItems[COMP_TYPE_FEET])
										
										g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iDrawableVariation[PED_COMP_FEET] = sDefaultItemData.iDrawable
										g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iTextureVariation[PED_COMP_FEET] = sDefaultItemData.iTexture
										
										SET_CUTSCENE_PED_COMPONENT_VARIATION(sHandle, GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_FEET), sDefaultItemData.iDrawable, sDefaultItemData.iTexture, GET_PLAYER_PED_MODEL(ePed))
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDREPEAT
			ENDIF
			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	#endif
	#endif
ENDFUNC



/// PURPOSE: Creates the specified player character on foot at their last saved location.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should either be CHAR_MICHAEL, CHAR_FRANKLIN, or CHAR_TREVOR
FUNC BOOL CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATIONCLF(PED_INDEX &ReturnPed, enumCharacterList ePed, BOOL bCleanupModel = TRUE)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODELCLF(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed], g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed], FALSE, FALSE)
            SET_ENTITY_COORDS_NO_OFFSET(Returnped, g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed])
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
            
			StoreInGlobalSelector(ReturnPed, ePed)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
			
            RESTORE_PLAYER_PED_INFO(ReturnPed)
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
			IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATIONNRM(PED_INDEX &ReturnPed, enumCharacterList ePed, BOOL bCleanupModel = TRUE)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODELNRM(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed], g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed], FALSE, FALSE)
            SET_ENTITY_COORDS_NO_OFFSET(Returnped, g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed])
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
            
			StoreInGlobalSelector(ReturnPed, ePed)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
			
            RESTORE_PLAYER_PED_INFO(ReturnPed)
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
			IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(PED_INDEX &ReturnPed, enumCharacterList ePed, BOOL bCleanupModel = TRUE)
	#if USE_CLF_DLC
		if g_bLoadedClifford
			return CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATIONCLF(ReturnPed,ePed,bCleanupModel)
		endif
	#endif
	#if USE_NRM_DLC
		if g_bLoadedNorman
			return CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATIONNRM(ReturnPed,ePed,bCleanupModel)
		endif	
	#endif
	
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODEL(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed], g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed], FALSE, FALSE)
            SET_ENTITY_COORDS_NO_OFFSET(Returnped, g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed])
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
            
			StoreInGlobalSelector(ReturnPed, ePed)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
			
            RESTORE_PLAYER_PED_INFO(ReturnPed)
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
			IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

/// PURPOSE: Creates the specified player character on foot.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the main characters.
FUNC BOOL CREATE_PLAYER_PED_ON_FOOTCLF(PED_INDEX &ReturnPed, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE, BOOL bInLeadIn = FALSE)
    
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODELCLF(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			StoreInGlobalSelector(ReturnPed, ePed, bInLeadIn)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
            
            RESTORE_PLAYER_PED_VARIATIONSCLF(ReturnPed)
            RESTORE_PLAYER_PED_WEAPONSCLF(ReturnPed)
            RESTORE_PLAYER_PED_ARMOURCLF(ReturnPed)
			RESTORE_PLAYER_PED_TATTOOSCLF(ReturnPed)
			
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
            IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_ON_FOOTNRM(PED_INDEX &ReturnPed, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE, BOOL bInLeadIn = FALSE)
    
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODELNRM(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			StoreInGlobalSelector(ReturnPed, ePed, bInLeadIn)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
            
            RESTORE_PLAYER_PED_VARIATIONSNRM(ReturnPed)
            RESTORE_PLAYER_PED_WEAPONSNRM(ReturnPed)
            RESTORE_PLAYER_PED_ARMOURNRM(ReturnPed)
			RESTORE_PLAYER_PED_TATTOOSNRM(ReturnPed)
			
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
            IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_ON_FOOT(PED_INDEX &ReturnPed, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE, BOOL bInLeadIn = FALSE, BOOL bIgnoreUnderwearcheck = FALSE)

 #if USE_CLF_DLC
	 if g_bLoadedClifford	
	 	return CREATE_PLAYER_PED_ON_FOOTCLF(ReturnPed,ePed,vCoords,fHeading,bCleanupModel,bInLeadIn)
	 endif 
 #endif
 
 #if USE_NRM_DLC
 	if g_bLoadedNorman
	 	return CREATE_PLAYER_PED_ON_FOOTNRM(ReturnPed,ePed,vCoords,fHeading,bCleanupModel,bInLeadIn)
	 endif
 #endif
	
	
	
    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODEL(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
			#IF IS_DEBUG_BUILD
				PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_ON_FOOT - ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			StoreInGlobalSelector(ReturnPed, ePed, bInLeadIn)
			
			SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
            
            RESTORE_PLAYER_PED_VARIATIONS(ReturnPed)
            RESTORE_PLAYER_PED_WEAPONS(ReturnPed)
            RESTORE_PLAYER_PED_ARMOUR(ReturnPed)
			RESTORE_PLAYER_PED_TATTOOS(ReturnPed)
			
			SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed, bIgnoreUnderwearcheck)
			STORE_TEMP_PLAYER_PED_ID(ReturnPed)
			
            IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_ON_FOOT - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

/// PURPOSE: Creates the specified player character in a vehicle that has already been created.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the main characters.
FUNC BOOL CREATE_PLAYER_PED_INSIDE_VEHICLE(PED_INDEX &ReturnPed, enumCharacterList ePed, VEHICLE_INDEX vehicle, VEHICLE_SEAT seat = VS_DRIVER, BOOL bCleanupModel = TRUE, BOOL bInLeadIn = FALSE, BOOL bIgnoreUnderwearcheck = FALSE)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
        // Load the required model
        MODEL_NAMES model = GET_PLAYER_PED_MODEL(ePed)
        
		REQUEST_MODEL(model)
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            
            IF DOES_ENTITY_EXIST(vehicle)
			AND IS_VEHICLE_DRIVEABLE(vehicle)
                ReturnPed = CREATE_PED_INSIDE_VEHICLE(vehicle, PEDTYPE_MISSION, model, seat)
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ".sc: CREATE_PLAYER_PED_INSIDE_VEHICLE - ", GET_PLAYER_PED_STRING(ePed))
					
					//try and catch 557329
					IF ARE_STRINGS_EQUAL(GET_THIS_SCRIPT_NAME(), "debug")
						DEBUG_PRINTCALLSTACK()
					ENDIF
					
				#ENDIF
                
				StoreInGlobalSelector(ReturnPed, ePed, bInLeadIn)
				
				SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(ReturnPed, FALSE)
                
                RESTORE_PLAYER_PED_VARIATIONS(ReturnPed)
                RESTORE_PLAYER_PED_WEAPONS(ReturnPed)
                RESTORE_PLAYER_PED_ARMOUR(ReturnPed)
				RESTORE_PLAYER_PED_TATTOOS(ReturnPed)
				
				SET_FLOW_OUTFIT_REQUIREMENTS(ReturnPed, bIgnoreUnderwearcheck)
				STORE_TEMP_PLAYER_PED_ID(ReturnPed)
				
                IF bCleanupModel
                    SET_MODEL_AS_NO_LONGER_NEEDED(model)
                ENDIF
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_PLAYER_PED_INSIDE_VEHICLE - Ped is not a player character.")
            PRINTSTRING("\nCREATE_PLAYER_PED_INSIDE_VEHICLE - Ped is not a player character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

/// PURPOSE: Repositions the players last vehicle (if they have any)
PROC REPOSITION_PLAYERS_VEHICLE(VECTOR vCoords, FLOAT fHeading)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VEHICLE_INDEX lastVehID = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(lastVehID)
			IF IS_VEHICLE_DRIVEABLE(lastVehID)
				SET_ENTITY_COORDS(lastVehID, vCoords)
				SET_ENTITY_HEADING(lastVehID, fHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(lastVehID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Checks to see if the players last vehicle matches the specified flags
FUNC BOOL IS_PLAYERS_VEHICLE_OF_TYPE(PLAYER_VEHICLE_FLAGS flags)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		VEHICLE_INDEX lastVehID = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(lastVehID)
			IF IS_VEHICLE_DRIVEABLE(lastVehID)
			
				MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(lastVehID)
				enumCharacterList eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_CAR)) != 0
					IF NOT IS_THIS_MODEL_A_CAR(eVehicleModel)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_BIKE)) != 0
					IF NOT IS_THIS_MODEL_A_BIKE(eVehicleModel)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_HELI)) != 0
					IF NOT IS_THIS_MODEL_A_HELI(eVehicleModel)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_BOAT)) != 0
					IF NOT IS_THIS_MODEL_A_BOAT(eVehicleModel)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_2_DOOR)) != 0
					IF GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "door_dside_r") <> -1
					OR GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "door_pside_r") <> -1
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_4_DOOR)) != 0
					IF GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "door_dside_r") = -1
					OR GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "door_pside_r") = -1
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_2_SEATER)) != 0
					IF IS_THIS_MODEL_A_BIKE(eVehicleModel)
						IF GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "seat_r") = -1
							RETURN FALSE
						ENDIF
					ELSE
						IF GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "seat_dside_r") <> -1
						OR GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "seat_pside_r") <> -1
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_4_SEATER)) != 0
					IF GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "seat_dside_r") = -1
					OR GET_ENTITY_BONE_INDEX_BY_NAME(lastVehID, "seat_pside_r") = -1
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_DEFAULT_PLAYER_VEHICLE)) != 0
					IF NOT IS_PLAYER_PED_PLAYABLE(eCurrentPed)
						RETURN FALSE
					ENDIF
					IF GET_PLAYER_VEH_MODEL(eCurrentPed) <> eVehicleModel
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF (ENUM_TO_INT(flags) & ENUM_TO_INT(PV_SAFE_FOR_REPOSITION)) != 0
					IF NOT IS_THIS_MODEL_A_CAR(eVehicleModel)
					AND NOT IS_THIS_MODEL_A_BIKE(eVehicleModel)
						RETURN FALSE
					ENDIF
					
					IF IS_BIG_VEHICLE(lastVehID)
						RETURN FALSE
					ENDIF
					
					INT iInstanceID
					STRING sScriptName = GET_ENTITY_SCRIPT(lastVehID, iInstanceID)
					IF NOT IS_STRING_NULL_OR_EMPTY(sScriptName)
						IF GET_HASH_KEY(sScriptName) = GET_HASH_KEY("taxiService")
							RETURN FALSE
						ENDIF
					ENDIF
					
					// Do not reposition vehicles in the garage.
					IF IS_VEHICLE_IN_PLAYERS_GARAGE(lastVehID, eCurrentPed, FALSE)
						RETURN FALSE
					ENDIF
				ENDIF
				
				// All flags match
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Unable to query last vehicle
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns true if the extra is turned on
FUNC BOOL IS_MR_RASPBERRY_JAM_TURNED_ON(VEHICLE_INDEX ReturnVeh)
	RETURN IS_VEHICLE_EXTRA_TURNED_ON(ReturnVeh, 5)
ENDFUNC

/// PURPOSE: Sets the vehicle extra for Trevors bodhi2
PROC TURN_ON_MR_RASPBERRY_JAM(VEHICLE_INDEX &ReturnVeh)
	// Attach Mr Raspberry Jam to Trevors truck
	#if USE_CLF_DLC
		IF NOT IS_MR_RASPBERRY_JAM_TURNED_ON(ReturnVeh)
			SET_VEHICLE_EXTRA(ReturnVeh, 5, true)
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF NOT IS_MR_RASPBERRY_JAM_TURNED_ON(ReturnVeh)
			SET_VEHICLE_EXTRA(ReturnVeh, 5, true)
		ENDIF
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF NOT IS_MR_RASPBERRY_JAM_TURNED_ON(ReturnVeh)
			SET_VEHICLE_EXTRA(ReturnVeh, 5, (NOT g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_PLAYER_VEH_T_UNLOCK_RASP_JAM]))
		ENDIF
	#endif
	#endif
ENDPROC

/// PURPOSE: Creates the specified player characters vehicle.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the main characters.
///    		 The eTypePreference will use the player vehicle that closely matches the type specified
#if USE_CLF_DLC
FUNC BOOL CREATE_PLAYER_VEHICLECLF(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// Grab the vehicle data for the specified ped
		INT iPlateBack
		TEXT_LABEL_15 tlPlateText
    	PED_VEH_DATA_STRUCT sVehData
    	GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF sVehData.model = DUMMY_MODEL_FOR_SCRIPT
			// Bail out so we don't get stuck.
			// Scripts should be checking if the vehicle is driveable anyway.
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Clear the save data for Michaels car when he gets it back from Jimmy.
		IF ePed = CHAR_MICHAEL
		AND NOT g_savedGlobalsClifford.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
			g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = DUMMY_MODEL_FOR_SCRIPT
			// we set the mods down below...
		ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate)
					IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack >= 0
					AND g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreR, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreG, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModIndex, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
		
		// Create the stored BIKE vehicle if it has the same model.
		ELIF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored bike data")
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra1, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate)
					IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack >= 0
					AND g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreR, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreG, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModIndex, g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsClifford.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
			
		// Just use default vehicle data.
		ELSE
		
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
	            
				BOOL bPlayerVehicle = TRUE
	            
	            PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using default data")
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Keep track of initial plate text in case we need to use it
				TEXT_LABEL_15 sInitialPlateText
				sInitialPlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, sVehData.iColour1, sVehData.iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, sVehData.iColourExtra1, sVehData.iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT sVehData.bExtraOn[iExtra]))
				ENDREPEAT
				IF sVehData.bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, sVehData.bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sVehData.tlNumberPlate)
					IF sVehData.iPlateBack >= 0
					AND sVehData.iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, sVehData.iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, sVehData.iTyreR, sVehData.iTyreG, sVehData.iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, sVehData.bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, sVehData.iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND sVehData.iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, sVehData.iLivery)
				ENDIF
				
				// Wheels
				IF sVehData.iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, sVehData.iModIndex, sVehData.iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				//////////////////////
				///    	Flow checks
				
				// Use random number plate for bagger bike if it is not unlocked yet
				IF ePed = CHAR_FRANKLIN
					IF GET_ENTITY_MODEL(ReturnVeh) = BAGGER
						SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sInitialPlateText)
						bPlayerVehicle = FALSE
					ENDIF
				
				// Attach Mr Raspberry Jam to Trevors truck
				ELIF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
					
				// Create Michaels car with the Jimmy mods.
				ELIF ePed = CHAR_MICHAEL
				AND NOT g_savedGlobalsClifford.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
				AND GET_ENTITY_MODEL(ReturnVeh) = TAILGATER
				
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting Jimmy mods")
					
					SET_VEHICLE_MOD(ReturnVeh, MOD_GRILL, 1) // Chrome Grille
					SET_VEHICLE_MOD(ReturnVeh, MOD_HORN, 7) // Musical Horn 5
					SET_VEHICLE_MOD(ReturnVeh, MOD_ENGINE, 2) // EMS Upgrade, Level 3
					SET_VEHICLE_MOD(ReturnVeh, MOD_BUMPER_R, 3) // EMS Upgrade, Level 3painted Bumper & Diffuser
					SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 5) // Double Vented Hood
					SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0) // Lip Spoiler
					SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 3) // Custom Skirts 4
					SET_VEHICLE_MOD(ReturnVeh, MOD_GEARBOX, 1) // Sports Transmission
					SET_VEHICLE_MOD(ReturnVeh, MOD_EXHAUST, 3) // Dual Exit Exhaust
					SET_VEHICLE_MOD(ReturnVeh, MOD_BRAKES, 2) // Race Brakes
					TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_XENON_LIGHTS, TRUE) // Xenon Lights
					SET_VEHICLE_WHEEL_TYPE(ReturnVeh, MWT_LOWRIDER)
					SET_VEHICLE_MOD(ReturnVeh, MOD_WHEELS, 11) // Dollar Wheels
					SET_VEHICLE_WINDOW_TINT(ReturnVeh, 2) // Dark Smoke
					
					g_savedGlobalsClifford.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar = TRUE
					
					// Need to save this!
					UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, ReturnVeh, SAVED_VEHICLE_SLOT_CAR, TRUE)
				ENDIF
	        
	            IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
				
				
				#IF IS_DEBUG_BUILD
					// CarApp debug
					IF g_bDebugLosSantosCustomsPlayedStatusOverride
					
						// Do not use if the vehicle has not been unlocked by the player
						IF sVehData.model != BAGGER
						
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting debug app mods")
							
							SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, "APPDEBUG")
							
							IF GET_NUM_MOD_KITS(ReturnVeh) > 0
								SET_VEHICLE_MOD_KIT(ReturnVeh, 0)
								
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TURBO, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_SUBWOOFER, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TYRE_SMOKE, TRUE)
								
								SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, 255, 255, 255)
								SET_VEHICLE_WINDOW_TINT(ReturnVeh, 0)
								
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_ARMOUR) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_ARMOUR, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SPOILER) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SKIRT) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_BONNET) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_GRILL) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF		
				#ENDIF
				
				IF bPlayerVehicle
					STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				ENDIF
				
	            RETURN TRUE
			ENDIF
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            PRINTSTRING("\n")
            PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(": CREATE_PLAYER_VEHICLE - Ped is not a player character.")
			PRINTNL()
            SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Ped is not a player character.")
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL CREATE_PLAYER_VEHICLENRM(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)

    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// Grab the vehicle data for the specified ped
		INT iPlateBack
		TEXT_LABEL_15 tlPlateText
    	PED_VEH_DATA_STRUCT sVehData
    	GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF sVehData.model = DUMMY_MODEL_FOR_SCRIPT
			// Bail out so we don't get stuck.
			// Scripts should be checking if the vehicle is driveable anyway.
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Clear the save data for Michaels car when he gets it back from Jimmy.
		IF ePed = CHAR_MICHAEL
		AND NOT g_savedGlobalsnorman.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
			g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = DUMMY_MODEL_FOR_SCRIPT
			// we set the mods down below...
		ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate)
					IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack >= 0
					AND g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreR, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreG, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModIndex, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
		
		// Create the stored BIKE vehicle if it has the same model.
		ELIF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored bike data")
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra1, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate)
					IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack >= 0
					AND g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreR, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreG, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModIndex, g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsnorman.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
			
		// Just use default vehicle data.
		ELSE
		
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
	            
				BOOL bPlayerVehicle = TRUE
	            
	            PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using default data")
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Keep track of initial plate text in case we need to use it
				TEXT_LABEL_15 sInitialPlateText
				sInitialPlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, sVehData.iColour1, sVehData.iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, sVehData.iColourExtra1, sVehData.iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT sVehData.bExtraOn[iExtra]))
				ENDREPEAT
				IF sVehData.bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, sVehData.bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sVehData.tlNumberPlate)
					IF sVehData.iPlateBack >= 0
					AND sVehData.iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, sVehData.iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, sVehData.iTyreR, sVehData.iTyreG, sVehData.iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, sVehData.bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, sVehData.iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND sVehData.iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, sVehData.iLivery)
				ENDIF
				
				// Wheels
				IF sVehData.iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, sVehData.iModIndex, sVehData.iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				//////////////////////
				///    	Flow checks
				
				// Use random number plate for bagger bike if it is not unlocked yet
				IF ePed = CHAR_FRANKLIN
					IF GET_ENTITY_MODEL(ReturnVeh) = BAGGER
						SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sInitialPlateText)
						bPlayerVehicle = FALSE
					ENDIF
				
				// Attach Mr Raspberry Jam to Trevors truck
				ELIF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
					
				// Create Michaels car with the Jimmy mods.
				ELIF ePed = CHAR_MICHAEL
				AND NOT g_savedGlobalsnorman.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
				AND GET_ENTITY_MODEL(ReturnVeh) = TAILGATER
				
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting Jimmy mods")
					
					SET_VEHICLE_MOD(ReturnVeh, MOD_GRILL, 1) // Chrome Grille
					SET_VEHICLE_MOD(ReturnVeh, MOD_HORN, 7) // Musical Horn 5
					SET_VEHICLE_MOD(ReturnVeh, MOD_ENGINE, 2) // EMS Upgrade, Level 3
					SET_VEHICLE_MOD(ReturnVeh, MOD_BUMPER_R, 3) // EMS Upgrade, Level 3painted Bumper & Diffuser
					SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 5) // Double Vented Hood
					SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0) // Lip Spoiler
					SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 3) // Custom Skirts 4
					SET_VEHICLE_MOD(ReturnVeh, MOD_GEARBOX, 1) // Sports Transmission
					SET_VEHICLE_MOD(ReturnVeh, MOD_EXHAUST, 3) // Dual Exit Exhaust
					SET_VEHICLE_MOD(ReturnVeh, MOD_BRAKES, 2) // Race Brakes
					TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_XENON_LIGHTS, TRUE) // Xenon Lights
					SET_VEHICLE_WHEEL_TYPE(ReturnVeh, MWT_LOWRIDER)
					SET_VEHICLE_MOD(ReturnVeh, MOD_WHEELS, 11) // Dollar Wheels
					SET_VEHICLE_WINDOW_TINT(ReturnVeh, 2) // Dark Smoke
					
					g_savedGlobalsnorman.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar = TRUE
					
					// Need to save this!
					UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, ReturnVeh, SAVED_VEHICLE_SLOT_CAR, TRUE)
				ENDIF
	        
	            IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
				
				
				#IF IS_DEBUG_BUILD
					// CarApp debug
					IF g_bDebugLosSantosCustomsPlayedStatusOverride
					
						// Do not use if the vehicle has not been unlocked by the player
						IF sVehData.model != BAGGER
						
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting debug app mods")
							
							SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, "APPDEBUG")
							
							IF GET_NUM_MOD_KITS(ReturnVeh) > 0
								SET_VEHICLE_MOD_KIT(ReturnVeh, 0)
								
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TURBO, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_SUBWOOFER, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TYRE_SMOKE, TRUE)
								
								SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, 255, 255, 255)
								SET_VEHICLE_WINDOW_TINT(ReturnVeh, 0)
								
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_ARMOUR) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_ARMOUR, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SPOILER) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SKIRT) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_BONNET) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_GRILL) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF		
				#ENDIF
				
				IF bPlayerVehicle
					STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				ENDIF
				
	            RETURN TRUE
			ENDIF
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            PRINTSTRING("\n")
            PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(": CREATE_PLAYER_VEHICLE - Ped is not a player character.")
			PRINTNL()
            SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Ped is not a player character.")
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
#endif
FUNC BOOL CREATE_PLAYER_VEHICLE(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
#if USE_CLF_DLC
	return 	CREATE_PLAYER_VEHICLECLF(ReturnVeh,ePed,vCoords,fHeading,bCleanupModel,eTypePreference)	
#endif
#if USE_NRM_DLC
	return 	CREATE_PLAYER_VEHICLENRM(ReturnVeh,ePed,vCoords,fHeading,bCleanupModel,eTypePreference)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC
    IF IS_PLAYER_PED_PLAYABLE(ePed)
	
		// Grab the vehicle data for the specified ped
		INT iPlateBack
		TEXT_LABEL_15 tlPlateText
    	PED_VEH_DATA_STRUCT sVehData
    	GET_PLAYER_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF sVehData.model = DUMMY_MODEL_FOR_SCRIPT
			// Bail out so we don't get stuck.
			// Scripts should be checking if the vehicle is driveable anyway.
			RETURN TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Clear the save data for Michaels car when he gets it back from Jimmy.
		IF ePed = CHAR_MICHAEL
		AND NOT g_savedGlobals.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
		AND (g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED])
			g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = DUMMY_MODEL_FOR_SCRIPT
			// we set the mods down below...
		ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
				
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].tlNumberPlate)
					IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack >= 0
					AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWindowTintColour)
				
				// Neon lights
				SET_VEHICLE_NEON_COLOUR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iNeonB)
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_FRONT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_BACK, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_LEFT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_RIGHT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_CAR][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
		
		// Create the stored BIKE vehicle if it has the same model.
		ELIF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model = sVehData.model
		
			REQUEST_MODEL(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using stored bike data")
				ReturnVeh = CREATE_VEHICLE(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].tlNumberPlate)
					IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack >= 0
					AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWindowTintColour)
				
				// Neon lights
				SET_VEHICLE_NEON_COLOUR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iNeonB)
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_FRONT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_BACK, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_LEFT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_RIGHT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				// Attach Mr Raspberry Jam to Trevors truck
				IF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobals.sPlayerData.sInfo.sPlayerVehicle[SAVED_VEHICLE_SLOT_BIKE][ePed].model)
	            ENDIF
				
				STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				
				RETURN TRUE
			ENDIF
			
		// Just use default vehicle data.
		ELSE
		
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
	            
				BOOL bPlayerVehicle = TRUE
	            
	            PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Using default data")
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Keep track of initial plate text in case we need to use it
				TEXT_LABEL_15 sInitialPlateText
				sInitialPlateText = GET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, sVehData.iColour1, sVehData.iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, sVehData.iColourExtra1, sVehData.iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT sVehData.bExtraOn[iExtra]))
				ENDREPEAT
				IF sVehData.bConvertible
					SET_CONVERTIBLE_ROOF(ReturnVeh, sVehData.bConvertible)
				ENDIF
				
				// Number plate
				// Bug #437236 - Set all license plates for all profiles in SP and MP to have the same text when reading from the cloud data.
				IF GET_CAR_APP_NUMBER_PLATE(tlPlateText, iPlateBack)
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, tlPlateText)
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, iPlateBack)
				ELSE
					SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sVehData.tlNumberPlate)
					IF sVehData.iPlateBack >= 0
					AND sVehData.iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
						SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, sVehData.iPlateBack)
					ENDIF
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, sVehData.iTyreR, sVehData.iTyreG, sVehData.iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, sVehData.bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, sVehData.iWindowTintColour)
				
				// Neon lights
				SET_VEHICLE_NEON_COLOUR(ReturnVeh, sVehData.iNeonR, sVehData.iNeonG, sVehData.iNeonB)
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_FRONT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_BACK, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_LEFT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_RIGHT, IS_BIT_SET(sVehData.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND sVehData.iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, sVehData.iLivery)
				ENDIF
				
				// Wheels
				IF sVehData.iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, sVehData.iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods
				SET_VEHICLE_MOD_DATA(ReturnVeh, sVehData.iModIndex, sVehData.iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				//////////////////////
				///    	Flow checks
				
				// Use random number plate for bagger bike if it is not unlocked yet
				IF ePed = CHAR_FRANKLIN
					IF GET_ENTITY_MODEL(ReturnVeh) = BAGGER
					AND NOT (g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE])
						SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, sInitialPlateText)
						bPlayerVehicle = FALSE
					ENDIF
				
				// Attach Mr Raspberry Jam to Trevors truck
				ELIF ePed = CHAR_TREVOR
					IF GET_ENTITY_MODEL(ReturnVeh) = BODHI2
						TURN_ON_MR_RASPBERRY_JAM(ReturnVeh)
					ENDIF
					
				// Create Michaels car with the Jimmy mods.
				ELIF ePed = CHAR_MICHAEL
				AND NOT g_savedGlobals.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar
				AND (g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED])
				AND GET_ENTITY_MODEL(ReturnVeh) = TAILGATER
				
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting Jimmy mods")
					
					SET_VEHICLE_MOD(ReturnVeh, MOD_GRILL, 1) // Chrome Grille
					SET_VEHICLE_MOD(ReturnVeh, MOD_HORN, 7) // Musical Horn 5
					SET_VEHICLE_MOD(ReturnVeh, MOD_ENGINE, 2) // EMS Upgrade, Level 3
					SET_VEHICLE_MOD(ReturnVeh, MOD_BUMPER_R, 3) // EMS Upgrade, Level 3painted Bumper & Diffuser
					SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 5) // Double Vented Hood
					SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0) // Lip Spoiler
					SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 3) // Custom Skirts 4
					SET_VEHICLE_MOD(ReturnVeh, MOD_GEARBOX, 1) // Sports Transmission
					SET_VEHICLE_MOD(ReturnVeh, MOD_EXHAUST, 3) // Dual Exit Exhaust
					SET_VEHICLE_MOD(ReturnVeh, MOD_BRAKES, 2) // Race Brakes
					TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_XENON_LIGHTS, TRUE) // Xenon Lights
					SET_VEHICLE_WHEEL_TYPE(ReturnVeh, MWT_LOWRIDER)
					SET_VEHICLE_MOD(ReturnVeh, MOD_WHEELS, 11) // Dollar Wheels
					SET_VEHICLE_WINDOW_TINT(ReturnVeh, 2) // Dark Smoke
					
					g_savedGlobals.sPlayerData.sInfo.bJimmyModsSetOnMichaelsCar = TRUE
					
					// Need to save this!
					UPDATE_PLAYER_PED_SAVED_VEHICLE(ePed, ReturnVeh, SAVED_VEHICLE_SLOT_CAR, TRUE)
				ENDIF
	        
	            IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
				
				
				#IF IS_DEBUG_BUILD
					// CarApp debug
					IF g_bDebugLosSantosCustomsPlayedStatusOverride
					
						// Do not use if the vehicle has not been unlocked by the player
						IF sVehData.model != BAGGER
						OR g_savedGlobals.sFlow.controls.flagIDs[FLOWFLAG_PLAYER_VEH_F_UNLOCK_BIKE]
						
							PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_PLAYER_VEHICLE() Setting debug app mods")
							
							SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, "APPDEBUG")
							
							IF GET_NUM_MOD_KITS(ReturnVeh) > 0
								SET_VEHICLE_MOD_KIT(ReturnVeh, 0)
								
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TURBO, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_SUBWOOFER, TRUE)
								TOGGLE_VEHICLE_MOD(ReturnVeh, MOD_TOGGLE_TYRE_SMOKE, TRUE)
								
								SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, 255, 255, 255)
								SET_VEHICLE_WINDOW_TINT(ReturnVeh, 0)
								
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_ARMOUR) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_ARMOUR, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SPOILER) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SPOILER, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_SKIRT) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_SKIRT, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_BONNET) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
								IF GET_NUM_VEHICLE_MODS(ReturnVeh, MOD_GRILL) >= 1
									SET_VEHICLE_MOD(ReturnVeh, MOD_BONNET, 0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF		
				#ENDIF
				
				IF bPlayerVehicle
					STORE_TEMP_PLAYER_VEHICLE_ID(ReturnVeh, ePed)
				ENDIF
				
	            RETURN TRUE
			ENDIF
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            PRINTSTRING("\n")
            PRINTSTRING(GET_THIS_SCRIPT_NAME())
			PRINTSTRING(": CREATE_PLAYER_VEHICLE - Ped is not a player character.")
			PRINTNL()
            SCRIPT_ASSERT("CREATE_PLAYER_VEHICLE - Ped is not a player character.")
        #ENDIF
    ENDIF

    RETURN FALSE
#endif
#endif
ENDFUNC

FUNC BOOL CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATIONCLF(PED_INDEX &ReturnPed,
		enumCharacterList ePed,
		PED_VEH_DATA_STRUCT sSavedVehicle,
		BOOL bCleanupModel = TRUE)
	
	enumTimetableVehState eSavedLastVehState = g_ePlayerLastVehState[ePed]
		
	IF sSavedVehicle.model <> DUMMY_MODEL_FOR_SCRIPT
	AND (eSavedLastVehState <> PTVS_0_noVehicle)
		REQUEST_MODEL(sSavedVehicle.model)
		REQUEST_PLAYER_PED_MODELCLF(ePed)
		
		
		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(sSavedVehicle.modelTrailer)
			IF NOT HAS_MODEL_LOADED(sSavedVehicle.modelTrailer)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(sSavedVehicle.model)
		OR NOT HAS_PLAYER_PED_MODEL_LOADED(ePed)
			RETURN FALSE
		ENDIF
		
		VECTOR vCoords = g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fHeading = g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed]
		
		IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			vCoords		= g_vPlayerLastVehCoord[ePed]
			fHeading	= g_fPlayerLastVehHead[ePed]
		ENDIF
		
		VECTOR vOccupiedBounds = <<2,2,2>>
		IF IS_AREA_OCCUPIED(vCoords-vOccupiedBounds, vCoords+vOccupiedBounds, FALSE, TRUE, TRUE, FALSE, FALSE)
			
			PRINTLN("IS_AREA_OCCUPIED???")
			
			VECTOR vVehicleCoord
			FLOAT fVehicleHead
			IF GetVehicleOffsetFromCoord(vCoords, fHeading, 0.0, NF_NONE, vVehicleCoord, fVehicleHead)
				IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
					g_vPlayerLastVehCoord[ePed] = vVehicleCoord
					g_fPlayerLastVehHead[ePed] = fVehicleHead
				ELSE
					g_savedGlobalsClifford.sPlayerData.sInfo.vLastKnownCoords[ePed] = vVehicleCoord
					g_savedGlobalsClifford.sPlayerData.sInfo.fLastKnownHead[ePed] = fVehicleHead
				ENDIF
			
				RETURN FALSE
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vehicleID = CREATE_VEHICLE(sSavedVehicle.model, vCoords, fHeading)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehicleID, FALSE)
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehicleID, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehicleID, TRUE)
		SET_VEH_DATA_FROM_STRUCT(vehicleID, sSavedVehicle)
		
		// Storing as personal player vehicle, to fix B*1978148
		IF sSavedVehicle.bIsPlayerVehicle
			STORE_TEMP_PLAYER_VEHICLE_ID(vehicleID, ePed)
		ENDIF

		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			VEHICLE_INDEX trailerID = CREATE_VEHICLE(sSavedVehicle.modelTrailer, vCoords+<<0,0,5>>, fHeading)
			ATTACH_VEHICLE_TO_TRAILER(vehicleID, trailerID)
		ENDIF
		
        IF bCleanupModel
            SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.model)
			
			IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.modelTrailer)
			ENDIF
        ENDIF
		
		IF (eSavedLastVehState = PTVS_2_playerInVehicle)
			IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ReturnPed, ePed, vehicleID, VS_DRIVER, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELIF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			IF CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELSE
			SCRIPT_ASSERT("CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATION - eSavedLastVehState???")
			RETURN FALSE
		ENDIF
		
		RETURN FALSE
	ELSE
		RETURN CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
	ENDIF
	
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATIONNRM(PED_INDEX &ReturnPed,
		enumCharacterList ePed,
		PED_VEH_DATA_STRUCT sSavedVehicle,
		BOOL bCleanupModel = TRUE)
	
	enumTimetableVehState eSavedLastVehState = g_ePlayerLastVehState[ePed]
		
	IF sSavedVehicle.model <> DUMMY_MODEL_FOR_SCRIPT
	AND (eSavedLastVehState <> PTVS_0_noVehicle)
		REQUEST_MODEL(sSavedVehicle.model)
		REQUEST_PLAYER_PED_MODELNRM(ePed)
		
		
		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(sSavedVehicle.modelTrailer)
			IF NOT HAS_MODEL_LOADED(sSavedVehicle.modelTrailer)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(sSavedVehicle.model)
		OR NOT HAS_PLAYER_PED_MODEL_LOADED(ePed)
			RETURN FALSE
		ENDIF
		
		VECTOR vCoords = g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fHeading = g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed]
		
		IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			vCoords		= g_vPlayerLastVehCoord[ePed]
			fHeading	= g_fPlayerLastVehHead[ePed]
		ENDIF
		
		VECTOR vOccupiedBounds = <<2,2,2>>
		IF IS_AREA_OCCUPIED(vCoords-vOccupiedBounds, vCoords+vOccupiedBounds, FALSE, TRUE, TRUE, FALSE, FALSE)
			
			PRINTLN("IS_AREA_OCCUPIED???")
			
			VECTOR vVehicleCoord
			FLOAT fVehicleHead
			IF GetVehicleOffsetFromCoord(vCoords, fHeading, 0.0, NF_NONE, vVehicleCoord, fVehicleHead)
				IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
					g_vPlayerLastVehCoord[ePed] = vVehicleCoord
					g_fPlayerLastVehHead[ePed] = fVehicleHead
				ELSE
					g_savedGlobalsnorman.sPlayerData.sInfo.vLastKnownCoords[ePed] = vVehicleCoord
					g_savedGlobalsnorman.sPlayerData.sInfo.fLastKnownHead[ePed] = fVehicleHead
				ENDIF
			
				RETURN FALSE
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vehicleID = CREATE_VEHICLE(sSavedVehicle.model, vCoords, fHeading)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehicleID, FALSE)
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehicleID, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehicleID, TRUE)
		SET_VEH_DATA_FROM_STRUCT(vehicleID, sSavedVehicle)
		
		// Storing as personal player vehicle, to fix B*1978148
		IF sSavedVehicle.bIsPlayerVehicle
			STORE_TEMP_PLAYER_VEHICLE_ID(vehicleID, ePed)
		ENDIF

		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			VEHICLE_INDEX trailerID = CREATE_VEHICLE(sSavedVehicle.modelTrailer, vCoords+<<0,0,5>>, fHeading)
			ATTACH_VEHICLE_TO_TRAILER(vehicleID, trailerID)
		ENDIF
		
        IF bCleanupModel
            SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.model)
			
			IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.modelTrailer)
			ENDIF
        ENDIF
		
		IF (eSavedLastVehState = PTVS_2_playerInVehicle)
			IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ReturnPed, ePed, vehicleID, VS_DRIVER, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELIF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			IF CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELSE
			SCRIPT_ASSERT("CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATION - eSavedLastVehState???")
			RETURN FALSE
		ENDIF
		
		RETURN FALSE
	ELSE
		RETURN CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
	ENDIF
	
ENDFUNC
FUNC BOOL CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATION(PED_INDEX &ReturnPed,
		enumCharacterList ePed,
		PED_VEH_DATA_STRUCT sSavedVehicle,
		BOOL bCleanupModel = TRUE)

#if USE_CLF_DLC
	if g_bLoadedClifford
	 	return CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATIONCLF(ReturnPed,ePed,sSavedVehicle,bCleanupModel)
	endif
#endif
#if USE_NRM_DLC
	if g_bLoadedNorman
	 	return CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATIONNRM(ReturnPed,ePed,sSavedVehicle,bCleanupModel)
	endif	
#endif		

	enumTimetableVehState eSavedLastVehState = g_ePlayerLastVehState[ePed]
		
	IF sSavedVehicle.model <> DUMMY_MODEL_FOR_SCRIPT
	AND (eSavedLastVehState <> PTVS_0_noVehicle)
		REQUEST_MODEL(sSavedVehicle.model)
		REQUEST_PLAYER_PED_MODEL(ePed)
		
		
		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(sSavedVehicle.modelTrailer)
			IF NOT HAS_MODEL_LOADED(sSavedVehicle.modelTrailer)
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF NOT HAS_MODEL_LOADED(sSavedVehicle.model)
		OR NOT HAS_PLAYER_PED_MODEL_LOADED(ePed)
			RETURN FALSE
		ENDIF
		
		VECTOR vCoords = g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed]
		FLOAT fHeading = g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed]
		
		IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			vCoords		= g_vPlayerLastVehCoord[ePed]
			fHeading	= g_fPlayerLastVehHead[ePed]
		ENDIF
		
		VECTOR vOccupiedBounds = <<2,2,2>>
		IF IS_AREA_OCCUPIED(vCoords-vOccupiedBounds, vCoords+vOccupiedBounds, FALSE, TRUE, TRUE, FALSE, FALSE)
			
			PRINTLN("IS_AREA_OCCUPIED???")
			
			VECTOR vVehicleCoord
			FLOAT fVehicleHead
			IF GetVehicleOffsetFromCoord(vCoords, fHeading, 0.0, NF_NONE, vVehicleCoord, fVehicleHead)
				IF (eSavedLastVehState = PTVS_1_playerWithVehicle)
					g_vPlayerLastVehCoord[ePed] = vVehicleCoord
					g_fPlayerLastVehHead[ePed] = fVehicleHead
				ELSE
					g_savedGlobals.sPlayerData.sInfo.vLastKnownCoords[ePed] = vVehicleCoord
					g_savedGlobals.sPlayerData.sInfo.fLastKnownHead[ePed] = fVehicleHead
				ENDIF
			
				RETURN FALSE
			ENDIF
		ENDIF
		
		VEHICLE_INDEX vehicleID = CREATE_VEHICLE(sSavedVehicle.model, vCoords, fHeading)
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehicleID, FALSE)
		SET_VEHICLE_CAN_SAVE_IN_GARAGE(vehicleID, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehicleID, TRUE)
		SET_VEH_DATA_FROM_STRUCT(vehicleID, sSavedVehicle)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehicleID, TRUE)
		
		// Storing as personal player vehicle, to fix B*1978148
		IF sSavedVehicle.bIsPlayerVehicle
			STORE_TEMP_PLAYER_VEHICLE_ID(vehicleID, ePed)
		ENDIF

		IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
			VEHICLE_INDEX trailerID = CREATE_VEHICLE(sSavedVehicle.modelTrailer, vCoords+<<0,0,5>>, fHeading)
			ATTACH_VEHICLE_TO_TRAILER(vehicleID, trailerID)
		ENDIF
		
        IF bCleanupModel
            SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.model)
			
			IF sSavedVehicle.modelTrailer <> DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(sSavedVehicle.modelTrailer)
			ENDIF
        ENDIF
		
		IF (eSavedLastVehState = PTVS_2_playerInVehicle)
			IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ReturnPed, ePed, vehicleID, VS_DRIVER, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELIF (eSavedLastVehState = PTVS_1_playerWithVehicle)
			IF CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleID)
				RETURN TRUE
			ENDIF
		ELSE
			SCRIPT_ASSERT("CREATE_PLAYER_PED_IN_LAST_KNOWN_VEHICLE_AT_LOCATION - eSavedLastVehState???")
			RETURN FALSE
		ENDIF
		
		RETURN FALSE
	ELSE
		RETURN CREATE_PLAYER_PED_ON_FOOT_AT_LAST_KNOWN_LOCATION(ReturnPed, ePed, bCleanupModel)
	ENDIF
	
ENDFUNC
/// PURPOSE: Returns TRUE if the player is considered to be in their default vehicle.
FUNC BOOL IS_PLAYER_USING_DEFAULT_VEHICLE(VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
  		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		// Grab the players last vehicle (will be the current vehicle if they are in one)
		VEHICLE_INDEX tempVeh = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(tempVeh)
		AND IS_VEHICLE_DRIVEABLE(tempVeh)
		
			// If this was a player created vehicle
			IF IS_VEHICLE_IN_TEMP_PLAYER_VEHICLE_ID_LIST(tempVeh)
			
				// Type check
				IF eTypePreference = VEHICLE_TYPE_CAR
					IF NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(tempVeh))
						RETURN FALSE
					ENDIF
				ELIF eTypePreference = VEHICLE_TYPE_BIKE
					IF NOT IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(tempVeh))
						RETURN FALSE
					ENDIF
				ENDIF
				
				// Player is in the vehicle
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh)
					RETURN TRUE
				ENDIF
				
				// Player is within 50m of the vehicle
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(tempVeh)) < 50.0
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    CHOP PED CREATION FUNCTIONS                                                                                                                ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Returns the model enum for Chop
FUNC MODEL_NAMES GET_CHOP_MODEL()
	RETURN (A_C_CHOP)
ENDFUNC

/// PURPOSE: Creates Chop the dog at the specified coords.
///    NOTE: This function should be called each frame until TRUE is returned.
FUNC BOOL CREATE_CHOP(PED_INDEX &ReturnPed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE)
    
    // Load the required model
    MODEL_NAMES model = GET_CHOP_MODEL()
    
	REQUEST_MODEL(model)
    IF HAS_MODEL_LOADED(model)
        IF DOES_ENTITY_EXIST(ReturnPed)
            DELETE_PED(ReturnPed)
        ENDIF
        ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
		#IF IS_DEBUG_BUILD
			PRINTLN("CREATE_CHOP_PED - Chop created at ", GET_STRING_FROM_VECTOR(vCoords), " by \"", GET_THIS_SCRIPT_NAME(), "\".")
		#ENDIF
		
		// Set Chops variations
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_HAIR, 0, 0)
		IF g_savedGlobals.sSocialData.bDogAppUsed // B*1536385 Set Chop's collar if the player has used the app
			SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_TORSO, 0, g_savedGlobals.sSocialData.sDogAppData.iCollar)
		ELSE
			SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_TORSO, 0, 4) // B*1168557 Set to Ballas collar by default
		ENDIF
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_LEG, 0, 0) // Coat
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_JBIB, 0, 0)
		CLEAR_ALL_PED_PROPS(ReturnPed)
		
        IF bCleanupModel
            SET_MODEL_AS_NO_LONGER_NEEDED(model)
        ENDIF
        
        RETURN TRUE
    ENDIF
	
    RETURN FALSE
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    STORY PED CREATION FUNCTIONS                                                                                                                ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Creates the specified non-player character on foot.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the story characters (CHAR_LESTER, CHAR_TRACEY, CHAR_LAMAR etc).
FUNC BOOL CREATE_NPC_PED_ON_FOOT(PED_INDEX &ReturnPed, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE)

    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        
		// Request and load the required model
        MODEL_NAMES model = GET_NPC_PED_MODEL(ePed)

		REQUEST_MODEL(model)

		IF HAS_MODEL_LOADED(model)
            
			// Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            ReturnPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
            
            SET_PED_DEFAULT_COMPONENT_VARIATION(ReturnPed)
			
			//#1103185
			IF (model = IG_LAMARDAVIS)
				IF (GET_PED_DRAWABLE_VARIATION(ReturnPed, PED_COMP_TORSO) = 0)
					SET_PED_COMPONENT_VARIATION(ReturnPed, PED_COMP_HAND, 2, 0)
				ENDIF
			ENDIF
			
			StoreAsGlobalFriend(ReturnPed, ePed)
            
            IF bCleanupModel
                SET_MODEL_AS_NO_LONGER_NEEDED(model)
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_NPC_PED_ON_FOOT - Ped is not story character.")
            PRINTSTRING("\nCREATE_NPC_PED_ON_FOOT - Ped is not story character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

/// PURPOSE: Creates the specified non-player character in a vehicle that has already been created.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the story characters (CHAR_LESTER, CHAR_TRACEY, CHAR_LAMAR etc).
FUNC BOOL CREATE_NPC_PED_INSIDE_VEHICLE(PED_INDEX &ReturnPed, enumCharacterList ePed, VEHICLE_INDEX vehicle, VEHICLE_SEAT seat = VS_DRIVER, BOOL bCleanupModel = TRUE)

    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		
		// Load the required model
        MODEL_NAMES model = GET_NPC_PED_MODEL(ePed)
        
		// Request model
		REQUEST_MODEL(model)
		
        IF HAS_MODEL_LOADED(model)
            // Ally: As the selector script is will be active in both sp and mp, we can't have it creating 
            // networked objects when it is not a net script. 
            IF DOES_ENTITY_EXIST(ReturnPed)
                DELETE_PED(ReturnPed)
            ENDIF
            
            IF DOES_ENTITY_EXIST(vehicle)
			AND IS_VEHICLE_DRIVEABLE(vehicle)
                ReturnPed = CREATE_PED_INSIDE_VEHICLE(vehicle, PEDTYPE_MISSION, model, seat, FALSE, FALSE)
                
                SET_PED_DEFAULT_COMPONENT_VARIATION(ReturnPed)
				StoreAsGlobalFriend(ReturnPed, ePed)
				
				IF bCleanupModel
                    SET_MODEL_AS_NO_LONGER_NEEDED(model)
                ENDIF
            ENDIF
            
            RETURN TRUE
        ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_NPC_PED_INSIDE_VEHICLE - Ped is not a story character.")
            PRINTSTRING("\nCREATE_NPC_PED_INSIDE_VEHICLE - Ped is not a story character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC

/// PURPOSE: Creates the specified non-player characters vehicle.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the story characters (CHAR_LESTER, CHAR_TRACEY, CHAR_LAMAR etc).
///    		 The eTypePreference will use the player vehicle that closely matches the type specified
/// PURPOSE: Creates the specified non-player characters vehicle.
///    NOTE: This function should be called each frame until TRUE is returned.
///          The ePed param should be one of the story characters (CHAR_LESTER, CHAR_TRACEY, CHAR_LAMAR etc).
///    		 The eTypePreference will use the player vehicle that closely matches the type specified
#if USE_CLF_DLC
FUNC BOOL CREATE_NPC_VEHICLECLF(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)

    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        // Grab the vehicle dta for the specified ped
        PED_VEH_DATA_STRUCT sVehData
        GET_NPC_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF (ePed = CHAR_AMANDA AND g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][0].model = sVehData.model)
		OR (ePed = CHAR_TRACEY AND g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][1].model = sVehData.model)
		
			INT iPed
			IF ePed = CHAR_AMANDA
				iPed = 0
			ELIF ePed = CHAR_TRACEY
				iPed = 1
			ENDIF
			
			REQUEST_MODEL(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	           	PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour1, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra1, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
					IF IS_VEHICLE_A_CONVERTIBLE(ReturnVeh)
						IF g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
							RAISE_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ELSE
							LOWER_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				// Number plate
				SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].tlNumberPlate)
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack >= 0
				AND g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack)
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreR, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreG, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods					
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModIndex, g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				IF bCleanupModel
					SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsClifford.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
				ENDIF
	           
				
				STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				
				RETURN TRUE
			ENDIF
		ELSE
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
			
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using default car data for ped ", ePed)
	            
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Colours
	            SET_VEH_DATA_FROM_STRUCT(ReturnVeh, sVehData)
				
				IF ePed = CHAR_AMANDA
				OR ePed = CHAR_TRACEY
					STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
	            
	            RETURN TRUE
	        ENDIF
		ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Ped is not story character.")
            PRINTSTRING("\nCREATE_NPC_VEHICLE - Ped is not story character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
#ENDIF
#if USE_NRM_DLC
FUNC BOOL CREATE_NPC_VEHICLENRM(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)

    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        // Grab the vehicle dta for the specified ped
        PED_VEH_DATA_STRUCT sVehData
        GET_NPC_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF (ePed = CHAR_NRM_AMANDA AND g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][0].model = sVehData.model)
//		OR (ePed = CHAR_TRACEY AND g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][1].model = sVehData.model)
		
			INT iPed
			IF ePed = CHAR_NRM_AMANDA
				iPed = 0
//			ELIF ePed = CHAR_TRACEY
//				iPed = 1
			ENDIF
			
			REQUEST_MODEL(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	           	PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour1, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra1, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
					IF IS_VEHICLE_A_CONVERTIBLE(ReturnVeh)
						IF g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
							RAISE_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ELSE
							LOWER_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				// Number plate
				SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].tlNumberPlate)
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack >= 0
				AND g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack)
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreR, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreG, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWindowTintColour)
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods	
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModIndex, g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
				IF bCleanupModel
               	 SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobalsnorman.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
				endif
	           				
				STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				
				RETURN TRUE
			ENDIF
		ELSE
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
			
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using default car data for ped ", ePed)
	            
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Colours
	            SET_VEH_DATA_FROM_STRUCT(ReturnVeh, sVehData)
				
				IF ePed = CHAR_NRM_AMANDA
					STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
	            
	            RETURN TRUE
	        ENDIF
		ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Ped is not story character.")
            PRINTSTRING("\nCREATE_NPC_VEHICLE - Ped is not story character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL CREATE_NPC_VEHICLE(VEHICLE_INDEX &ReturnVeh, enumCharacterList ePed, VECTOR vCoords, FLOAT fHeading, BOOL bCleanupModel = TRUE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT)
	#if USE_CLF_DLC
		return CREATE_NPC_VEHICLECLF(ReturnVeh,ePed,vCoords,fHeading,bCleanupModel,eTypePreference)
	#endif
	#if USE_NRM_DLC
		return CREATE_NPC_VEHICLENRM(ReturnVeh,ePed,vCoords,fHeading,bCleanupModel,eTypePreference)
	#endif
	
	#if not USE_SP_DLC

    IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
        // Grab the vehicle dta for the specified ped
        PED_VEH_DATA_STRUCT sVehData
        GET_NPC_VEH_DATA(ePed, sVehData, eTypePreference)
		
		IF DOES_ENTITY_EXIST(ReturnVeh)
			IF GET_ENTITY_MODEL(ReturnVeh) != sVehData.model
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Vehicle ID already exists with different model")
				#ENDIF
			ENDIF
			RETURN TRUE
        ENDIF
		
		// Create the stored CAR vehicle if it has the same model.
		IF (ePed = CHAR_AMANDA AND g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][0].model = sVehData.model)
		OR (ePed = CHAR_TRACEY AND g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][1].model = sVehData.model)
		
			INT iPed
			IF ePed = CHAR_AMANDA
				iPed = 0
			ELIF ePed = CHAR_TRACEY
				iPed = 1
			ENDIF
			
			REQUEST_MODEL(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	        IF HAS_MODEL_LOADED(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
	           	PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using stored car data for ped ", ePed)
				ReturnVeh = CREATE_VEHICLE(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Bug 492963 - Increase vehicle health by 250 (each part)
				//SET_ENTITY_MAX_HEALTH(ReturnVeh, 1250)
				SET_ENTITY_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_ENGINE_HEALTH(ReturnVeh, 1250)
				SET_VEHICLE_PETROL_TANK_HEALTH(ReturnVeh, 1250)
				sVehData.fHealth = 1250
				
				// Colours
				SET_VEHICLE_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour1, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColour2)
				SET_VEHICLE_EXTRA_COLOURS(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra1, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iColourExtra2)
				
				// Dirt
	            SET_VEHICLE_DIRT_LEVEL(ReturnVeh, sVehData.fDirtLevel)
				
				// Extras
				INT iExtra
				REPEAT NUMBER_OF_VEHICLE_EXTRAS iExtra
					SET_VEHICLE_EXTRA(ReturnVeh, iExtra+1, (NOT g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bExtraOn[iExtra]))
				ENDREPEAT
				IF g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
					IF IS_VEHICLE_A_CONVERTIBLE(ReturnVeh)
						IF g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bConvertible
							RAISE_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ELSE
							LOWER_CONVERTIBLE_ROOF(ReturnVeh, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				// Number plate
				SET_VEHICLE_NUMBER_PLATE_TEXT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].tlNumberPlate)
				IF g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack >= 0
				AND g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack < GET_NUMBER_OF_VEHICLE_NUMBER_PLATES()
					SET_VEHICLE_NUMBER_PLATE_TEXT_INDEX(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iPlateBack)
				ENDIF
				
				// Tyres
				SET_VEHICLE_TYRE_SMOKE_COLOR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreR, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreG, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iTyreB)
				SET_VEHICLE_TYRES_CAN_BURST(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].bTyresCanBurst)
				
				// Window tint
				SET_VEHICLE_WINDOW_TINT(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWindowTintColour)
				
				// Neon lights
				SET_VEHICLE_NEON_COLOUR(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iNeonR, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iNeonG, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iNeonB)
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_FRONT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_BACK, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_BACK))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_LEFT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT))
				SET_VEHICLE_NEON_ENABLED(ReturnVeh, NEON_RIGHT, IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT))
				
				// Livery
				IF GET_VEHICLE_LIVERY_COUNT(ReturnVeh) > 1
				AND g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery >= 0
					SET_VEHICLE_LIVERY(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iLivery)
				ENDIF
				
				// Wheels
				IF g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType > -1
					IF NOT IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(ReturnVeh))
						IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(ReturnVeh))
							IF (INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType) = MWT_BIKE)
								SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
							ENDIF
						ELSE
							SET_VEHICLE_WHEEL_TYPE(ReturnVeh, INT_TO_ENUM(MOD_WHEEL_TYPE, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iWheelType))
						ENDIF
					ENDIF
				ENDIF
				
				// Mods								
				SET_VEHICLE_MOD_DATA(ReturnVeh, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModIndex, g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].iModVariation)
				
				// Env Eff
				SET_VEHICLE_ENVEFF_SCALE(ReturnVeh, sVehData.fEnvEff)
				
               	IF bCleanupModel
					SET_MODEL_AS_NO_LONGER_NEEDED(g_savedGlobals.sPlayerData.sInfo.sNPCVehicle[SAVED_VEHICLE_SLOT_CAR][iPed].model)
				endif				
	           				
				STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				
				RETURN TRUE
			ENDIF
		ELSE
			REQUEST_MODEL(sVehData.model)
	        IF HAS_MODEL_LOADED(sVehData.model)
			
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": CREATE_NPC_VEHICLE() Using default car data for ped ", ePed)
	            
				ReturnVeh = CREATE_VEHICLE(sVehData.model, vCoords, fHeading, FALSE, FALSE)
	            SET_VEHICLE_ON_GROUND_PROPERLY(ReturnVeh)
				SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(ReturnVeh, FALSE)
				SET_VEHICLE_CAN_SAVE_IN_GARAGE(ReturnVeh, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(ReturnVeh, TRUE)
				
				// Colours
	            SET_VEH_DATA_FROM_STRUCT(ReturnVeh, sVehData)
				
				IF ePed = CHAR_AMANDA
				OR ePed = CHAR_TRACEY
					STORE_TEMP_NPC_VEHICLE_ID(ReturnVeh)
				ENDIF
				
				IF bCleanupModel
	                SET_MODEL_AS_NO_LONGER_NEEDED(sVehData.model)
	            ENDIF
	            
	            RETURN TRUE
	        ENDIF
		ENDIF
    ELSE
        #IF IS_DEBUG_BUILD
            SCRIPT_ASSERT("CREATE_NPC_VEHICLE - Ped is not story character.")
            PRINTSTRING("\nCREATE_NPC_VEHICLE - Ped is not story character.")PRINTNL()
        #ENDIF
    ENDIF

    RETURN FALSE
	
	#ENDIF
	
ENDFUNC
CONST_FLOAT NO_HEADING  -1.0

PROC DO_PLAYER_MAP_WARP_WITH_LOAD(VECTOR paramCoords, FLOAT paramHeading = NO_HEADING, BOOL paramFadeScreen = TRUE, INT paramTimeout = 12000, FLOAT paramLoadRadius = 125.0, BOOL bKeepOriginalZ = FALSE)
	IF paramFadeScreen
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ENDIF
		ENDIF
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
					
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		//Move streaming focus to warp location.
		SET_FOCUS_POS_AND_VEL(paramCoords, <<0,0,0>>)
	
		//Load streaming volume at new warp position.
		INT iBreakOutTimer = GET_GAME_TIMER() + paramTimeout
		NEW_LOAD_SCENE_START_SPHERE(paramCoords, paramLoadRadius)

		WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND GET_GAME_TIMER() < iBreakOutTimer 
			WAIT(0)
		ENDWHILE
		
		//Streaming volume loaded, move player.
		IF NOT (IS_ENTITY_DEAD(PLAYER_PED_ID()))
			IF NOT bKeepOriginalZ
				paramCoords.Z += 1.0
				GET_GROUND_Z_FOR_3D_COORD(paramCoords, paramCoords.Z)
			ENDIF
			SET_ENTITY_COORDS(PLAYER_PED_ID(), paramCoords)
			IF paramHeading != NO_HEADING
				SET_ENTITY_HEADING(PLAYER_PED_ID(), paramHeading)
			ENDIF
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF

		CLEAR_FOCUS()
		NEW_LOAD_SCENE_STOP()
		
		//Reset area.
		CLEAR_AREA(paramCoords, 5000.0, TRUE, FALSE)
		INSTANTLY_FILL_PED_POPULATION()
		INSTANTLY_FILL_VEHICLE_POPULATION()
		
		// reset camera behind player
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	ENDIF	
	
	IF paramFadeScreen
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
ENDPROC

//Based on the DO_PLAYER_MAP_WARP_WITH_LOAD function, made to use the code native to perform a warp with fade and load
PROC DO_PLAYER_TELEPORT(VECTOR vCoords, 
FLOAT fHeading = NO_HEADING, 
BOOL bFadeScreen = TRUE
, BOOL bSnapToGround = TRUE
, INT iTimeout = 12000)
	CDEBUG1LN(DEBUG_SYSTEM,"Performing player teleport to ",vCoords," from script ",GET_THIS_SCRIPT_NAME())
	START_PLAYER_TELEPORT(PLAYER_ID(),vCoords, fHeading, DEFAULT, bSnapToGround, bFadeScreen)
	INT iWaitTime = GET_GAME_TIMER() + iTimeout
	
	WHILE NOT (UPDATE_PLAYER_TELEPORT(PLAYER_ID()) OR iWaitTime < GET_GAME_TIMER())
		CDEBUG3LN(DEBUG_SYSTEM,"Waiting for player teleport to finish")
		WAIT(0)
	ENDWHILE
	CDEBUG1LN(DEBUG_SYSTEM,"Done performing teleport to coords ",vCoords)
ENDPROC


PROC DO_PLAYER_WARP_WITH_LOAD_AND_PAUSE(VECTOR paramPosition, FLOAT paramHeading = NO_HEADING, INT paramFrameTimeout = 350, FLOAT paramLoadRadius = 60.0, BOOL paramFadeInOutScreen = FALSE)
	CPRINTLN(DEBUG_SYSTEM, "DO_PLAYER_WARP_WITH_LOAD_AND_PAUSE: Warping player to ",  paramPosition, ".")
	
	IF paramFadeInOutScreen
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
	
	// Move the player and the camera
	CLEAR_AREA(paramPosition, 5.0, TRUE)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), paramPosition)
			IF paramHeading != NO_HEADING
				SET_ENTITY_HEADING(PLAYER_PED_ID(), paramHeading)
			ENDIF
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			RESET_PED_AUDIO_FLAGS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	//Wait a frame for the camera to catch up before pausing
	WAIT(0) 
	SET_GAME_PAUSED(TRUE)
	
	//Load the streaming volume.
	NEW_LOAD_SCENE_START_SPHERE(paramPosition, paramLoadRadius)
	
	INT iNewLoadSceneTimeout = 0 
	
	CPRINTLN(DEBUG_SYSTEM, "Starting new load scene.")
	WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
	AND (iNewLoadSceneTimeout < paramFrameTimeout)
		iNewLoadSceneTimeout++
		WAIT(0)
	ENDWHILE		
	CPRINTLN(DEBUG_SYSTEM, "Finished new load scene. Load took ", iNewLoadSceneTimeout, " frames.")
	
	//Clean up the new load scene.
	NEW_LOAD_SCENE_STOP()
	
	//Clear the area around the player.
	CLEAR_AREA(paramPosition, 5.0, TRUE)
	
	//Reset the camera behind the player.
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	
	//Unpause when all is done.
	SET_GAME_PAUSED(FALSE)
	
	IF paramFadeInOutScreen
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
ENDPROC


FUNC BOOL STORE_Clock_For_Multiplayer_Switch()
	
	g_sMultiplayerTransitionTOD = GET_CURRENT_TIMEOFDAY()
	
	RETURN FALSE
ENDFUNC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                                                    ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
FUNC BOOL Is_Player_Timetable_Scene_In_ProgressCLF()
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_timetableCLF")) > 0
		// player timetable scene is in progress!
		RETURN TRUE
	ENDIF
	
	// player timetable scene is NOT in progress...
	RETURN FALSE
ENDFUNC

FUNC BOOL Is_Player_Timetable_Scene_In_ProgressNRM()
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_timetableNRM")) > 0
		// player timetable scene is in progress!
		RETURN TRUE
	ENDIF
	
	// player timetable scene is NOT in progress...
	RETURN FALSE
ENDFUNC

FUNC BOOL Is_Player_Timetable_Scene_In_Progress()

	#IF USE_CLF_DLC
		IF g_bLoadedClifford
			RETURN Is_Player_Timetable_Scene_In_ProgressCLF()
		ENDIF
	#ENDIF
	#IF USE_NRM_DLC
		IF g_bLoadedNorman
			RETURN Is_Player_Timetable_Scene_In_ProgressNRM()
		ENDIF
	#ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("player_timetable_scene")) > 0
		// player timetable scene is in progress!
		RETURN TRUE
	ENDIF
	
	// player timetable scene is NOT in progress...
	RETURN FALSE
ENDFUNC

FUNC PED_REQUEST_SCENE_ENUM Get_Player_Timetable_Scene_In_Progress()
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		// player switch is in progress!
		RETURN g_eSelectedPlayerCharScene
	ENDIF
	IF Is_Player_Timetable_Scene_In_Progress()
		// player timetable scene is in progress!
		RETURN g_eSelectedPlayerCharScene
	ENDIF
	
	// player timetable scene is NOT in progress...
	RETURN PR_SCENE_INVALID
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///    PED CUTSCENE VARIATIONS                                                                                                                     ///
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Determine whether the player_controller script should set player ped data when a cutscene is running.
///    			Data includes player ped variations and weapons, as well as vehicle mods, colours, and plate text.
///    NOTE: Please ensure that this gets set back to TRUE when you are done.
PROC SET_PLAYER_PED_DATA_IN_CUTSCENES(BOOL bClothes = TRUE, BOOL bWeapons = TRUE)
	
	PRINTLN("SET_PLAYER_PED_DATA_IN_CUTSCENES(", bClothes, ", ", bWeapons, ") called by ", GET_THIS_SCRIPT_NAME())
	
	g_bSetPlayerClothesInCutscenes = bClothes
	g_bSetPlayerWeaponsInCutscenes = bWeapons
ENDPROC

PROC GET_PED_VARIATIONS_FOR_CUTSCENE(PED_INDEX pedID, PED_VARIATION_STRUCT &sVariations, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_ALL)
	INT iComponent
	PED_COMP_NAME_ENUM eItemRemoval
	PED_VARIATION_STRUCT sFullVariations
	PED_COMP_ITEM_DATA_STRUCT sItemDetails
	MODEL_NAMES eModel = GET_ENTITY_MODEL(pedID)
	
	GET_PED_VARIATIONS(pedID, sFullVariations, CBF_NONE)
	GET_PED_VARIATIONS(pedID, sVariations, eCompBlocks)
	
	BOOL bHairForced = FALSE
	BOOL bBeardForced = FALSE
	
	#IF IS_DEBUG_BUILD
		enumCharacterList ePed = GET_PLAYER_PED_ENUM(pedID)
	#ENDIF
	
	
	// If we remove an item, and it forced a hair/beard change, re-instate the hair/bear
	REPEAT NUM_PED_COMPONENTS iComponent
		IF sVariations.iDrawableVariation[iComponent] != sFullVariations.iDrawableVariation[iComponent]
		OR sVariations.iTextureVariation[iComponent] != sFullVariations.iTextureVariation[iComponent]
			PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Component has been changed on slot ", iComponent)
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sFullVariations.iDrawableVariation[iComponent], sFullVariations.iTextureVariation[iComponent], GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent))), eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sFullVariations.iDrawableVariation[iComponent], sFullVariations.iTextureVariation[iComponent], GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent))), eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_PLAYER_PED_PROPS iComponent
		IF sVariations.iPropIndex[iComponent] != sFullVariations.iPropIndex[iComponent]
		OR sVariations.iPropTexture[iComponent] != sFullVariations.iPropTexture[iComponent]
			PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Prop has been changed on slot ", iComponent)
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, COMP_TYPE_PROPS, GET_PROP_ITEM_FROM_VARIATIONS(pedID, sFullVariations.iPropIndex[iComponent], sFullVariations.iPropTexture[iComponent], INT_TO_ENUM(PED_PROP_POSITION, iComponent)), eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, COMP_TYPE_PROPS, GET_PROP_ITEM_FROM_VARIATIONS(pedID, sFullVariations.iPropIndex[iComponent], sFullVariations.iPropTexture[iComponent], INT_TO_ENUM(PED_PROP_POSITION, iComponent)), eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Set any forced hair or beard requisites
	IF bBeardForced OR bHairForced
		PED_COMP_NAME_ENUM eBeard = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sVariations.iDrawableVariation[PED_COMP_BERD], sVariations.iTextureVariation[PED_COMP_BERD], COMP_TYPE_BERD)
		PED_COMP_NAME_ENUM eHair = GET_PED_COMP_ITEM_FROM_VARIATIONS(pedID, sVariations.iDrawableVariation[PED_COMP_HAIR], sVariations.iTextureVariation[PED_COMP_HAIR], COMP_TYPE_HAIR)
		PED_COMP_NAME_ENUM eRequisite = GET_HEAD_REQUISITE_SP(eModel, eHair, eBeard)
		
		IF eRequisite != DUMMY_PED_COMP
			
			#IF IS_DEBUG_BUILD
				PRINTLN("GET_PED_VARIATIONS_FOR_CUTSCENE() - Forcing requisite head for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HEAD, eRequisite)
			sVariations.iDrawableVariation[PED_COMP_HEAD] = sItemDetails.iDrawable
			sVariations.iTextureVariation[PED_COMP_HEAD] = sItemDetails.iTexture
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: This should be used to pass in the ped index minus items that cause issues - parachutes, helmets, headsets etc.
PROC SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(STRING sSceneHandle, PED_INDEX pedID, MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_ALL)
	
	PED_VARIATION_STRUCT sVariations
	GET_PED_VARIATIONS_FOR_CUTSCENE(pedID, sVariations, eCompBlocks)
	
	// Now add to the cutscene
	INT iComponent
	REPEAT NUM_PED_COMPONENTS iComponent
		SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle, INT_TO_ENUM(PED_COMPONENT, iComponent), sVariations.iDrawableVariation[iComponent], sVariations.iTextureVariation[iComponent], eModel)
	ENDREPEAT
	REPEAT NUM_PLAYER_PED_PROPS iComponent
		IF sVariations.iPropIndex[iComponent] = -1
		OR sVariations.iPropIndex[iComponent] = 255
			SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), -1, 0, eModel)
		ELSE
			SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), sVariations.iPropIndex[iComponent], sVariations.iPropTexture[iComponent], eModel)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE: Sets the stored varaitions to be used in the cutscene for the given player ped/scene handle
///    NOTE: This is the clothes that the ped had before the start of a mission
PROC SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS(enumCharacterList ePed, STRING sSceneHandle, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_ALL)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
		EXIT
	ENDIF
	
	#if USE_CLF_DLC
	PED_VARIATION_STRUCT sFullVariations = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed]
	PED_VARIATION_STRUCT sVariations = g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed]
	#endif
	#if USE_NRM_DLC
	PED_VARIATION_STRUCT sFullVariations = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed]
	PED_VARIATION_STRUCT sVariations = g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed]
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	PED_VARIATION_STRUCT sFullVariations = g_savedGlobals.sPlayerData.sInfo.sVariations[ePed]
	PED_VARIATION_STRUCT sVariations = g_savedGlobals.sPlayerData.sInfo.sVariations[ePed]
	#endif
	#endif	
	
	INT i
	REPEAT NUM_PED_COMPONENTS i
		GET_VARIATION_TO_STORE(NULL, INT_TO_ENUM(PED_COMPONENT, i), sVariations.iDrawableVariation[i], sVariations.iTextureVariation[i], sVariations.iPaletteVariation[i], eCompBlocks, ePed)
	ENDREPEAT
	
	REPEAT NUM_PLAYER_PED_PROPS i
		GET_PROP_VARIATION_TO_STORE(NULL, INT_TO_ENUM(PED_PROP_POSITION, i), sVariations.iPropIndex[i], sVariations.iPropTexture[i], eCompBlocks, ePed)
	ENDREPEAT
	
	INT iComponent
	PED_COMP_NAME_ENUM eItemRemoval
	PED_COMP_ITEM_DATA_STRUCT sItemDetails
	MODEL_NAMES eModel = GET_PLAYER_PED_MODEL(ePed)
	
	
	BOOL bHairForced = FALSE
	BOOL bBeardForced = FALSE
	
	// If we remove an item, and it forced a hair/beard change, re-instate the hair/bear
	REPEAT NUM_PED_COMPONENTS iComponent
		IF sVariations.iDrawableVariation[iComponent] != sFullVariations.iDrawableVariation[iComponent]
		OR sVariations.iTextureVariation[iComponent] != sFullVariations.iTextureVariation[iComponent]
			PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Component has been changed on slot ", iComponent)
			#if USE_CLF_DLC
				IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
					#ENDIF
					
					sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
					sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
					sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
					bHairForced = TRUE
				ENDIF
				
				IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
					#ENDIF
					
					sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
					sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
					sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
					bBeardForced = TRUE
				ENDIF
			#endif
			#if USE_NRM_DLC
				IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
					#ENDIF
					
					sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
					sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
					sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
					bHairForced = TRUE
				ENDIF
				
				IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
					#ENDIF
					
					sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
					sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
					sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
					bBeardForced = TRUE
				ENDIF		
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, GET_COMP_TYPE_FROM_PED_COMPONENT(INT_TO_ENUM(PED_COMPONENT, iComponent)), g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF		
			#endif
			#endif				
		ENDIF
	ENDREPEAT
	
	REPEAT NUM_PLAYER_PED_PROPS iComponent
		IF sVariations.iPropIndex[iComponent] != sFullVariations.iPropIndex[iComponent]
		OR sVariations.iPropTexture[iComponent] != sFullVariations.iPropTexture[iComponent]
			PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop has been changed on slot ", iComponent)
			#if USE_CLF_DLC
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF	
			#endif
			#if USE_NRM_DLC
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF		
			#endif
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC	
			IF DOES_ITEM_FORCE_HAIR_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobals.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored hair for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HAIR, sVariations.eStoredHairstyle)
				sVariations.iDrawableVariation[PED_COMP_HAIR] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_HAIR] = sItemDetails.iTexture
				bHairForced = TRUE
			ENDIF
			
			IF DOES_ITEM_FORCE_BEARD_CHANGE(eModel, COMP_TYPE_PROPS, g_savedGlobals.sPlayerData.sInfo.eTrackedPedProps[iComponent][ePed], eItemRemoval)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting stored beard for ", GET_PLAYER_PED_STRING(ePed))
				#ENDIF
				
				sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_BERD, sVariations.eStoredBeard)
				sVariations.iDrawableVariation[PED_COMP_BERD] = sItemDetails.iDrawable
				sVariations.iTextureVariation[PED_COMP_BERD] = sItemDetails.iTexture
				bBeardForced = TRUE
			ENDIF		
			#endif
			#endif	
			
			
			
		ENDIF
	ENDREPEAT
	
	// Set any forced hair or beard requisites
	IF bBeardForced OR bHairForced
	#if USE_CLF_DLC
		PED_COMP_NAME_ENUM eBeard = g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_BERD][ePed]
		PED_COMP_NAME_ENUM eHair = g_savedGlobalsClifford.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_HAIR][ePed]	
	#endif
	#if USE_NRM_DLC
		PED_COMP_NAME_ENUM eBeard = g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_BERD][ePed]
		PED_COMP_NAME_ENUM eHair = g_savedGlobalsnorman.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_HAIR][ePed]	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		PED_COMP_NAME_ENUM eBeard = g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_BERD][ePed]
		PED_COMP_NAME_ENUM eHair = g_savedGlobals.sPlayerData.sInfo.eTrackedPedComp[PED_COMP_HAIR][ePed]	
	#endif
	#endif	
	
		PED_COMP_NAME_ENUM eRequisite = GET_HEAD_REQUISITE_SP(eModel, eHair, eBeard)
		
		IF eRequisite != DUMMY_PED_COMP
			
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Forcing requisite head for ", GET_PLAYER_PED_STRING(ePed))
			#ENDIF
			
			sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_SP(eModel, COMP_TYPE_HEAD, eRequisite)
			sVariations.iDrawableVariation[PED_COMP_HEAD] = sItemDetails.iDrawable
			sVariations.iTextureVariation[PED_COMP_HEAD] = sItemDetails.iTexture
		ENDIF
	ENDIF
	
	// Now add to the cutscene
	PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variations for ", GET_PLAYER_PED_STRING(ePed), " using stored variations")
	REPEAT NUM_PED_COMPONENTS iComponent
		SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle, INT_TO_ENUM(PED_COMPONENT, iComponent), sVariations.iDrawableVariation[iComponent], sVariations.iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(ePed))
	ENDREPEAT
	REPEAT NUM_PLAYER_PED_PROPS iComponent
		IF g_sLastStoredPlayerPedClothes[ePed].iPropIndex[iComponent] = -1
		OR g_sLastStoredPlayerPedClothes[ePed].iPropIndex[iComponent] = 255
			SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), -1, 0, GET_PLAYER_PED_MODEL(ePed))
		ELSE
			SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), sVariations.iPropIndex[iComponent], sVariations.iPropTexture[iComponent], GET_PLAYER_PED_MODEL(ePed))
		ENDIF
	ENDREPEAT
	
	SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE(sSceneHandle, ePed)
ENDPROC

/// PURPOSE: Sets the current stored varaitions to be used in the cutscene for the given player ped/scene handle
PROC SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS(enumCharacterList ePed, STRING sSceneHandle, COMPONENT_BLOCK_FLAGS eCompBlocks = CBF_ALL)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePed)
	OR IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	INT iComponent
	PED_VARIATION_STRUCT sVariations
	
	// If this is the player, use current variations
	IF ePed = GET_CURRENT_PLAYER_PED_ENUM()
	
		IF g_iFlowBitsetBlockPlayerCutsceneVariations[ePed] = 0
			PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variations for ", GET_PLAYER_PED_STRING(ePed), " using player ped index.")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle, PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()))
		ELSE
			PRINTLN("SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variations for ", GET_PLAYER_PED_STRING(ePed), " using player ped index with some blocked components.")
			GET_PED_VARIATIONS_FOR_CUTSCENE(PLAYER_PED_ID(), sVariations, eCompBlocks)
			REPEAT NUM_PED_COMPONENTS iComponent
				IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], iComponent)
					PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Component ", iComponent, " passed to cutscene from current player ped data.")
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle, INT_TO_ENUM(PED_COMPONENT, iComponent), sVariations.iDrawableVariation[iComponent], sVariations.iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(ePed))
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Component ", iComponent, " blocked.")
				#ENDIF
				ENDIF
			ENDREPEAT
			REPEAT NUM_PLAYER_PED_PROPS iComponent
				IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], NUM_PED_COMPONENTS + iComponent)
					IF sVariations.iPropIndex[iComponent] = -1
					OR sVariations.iPropIndex[iComponent] = 255
						PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " passed to cutscene from current player ped data. (-1)")
						SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), -1, 0, GET_PLAYER_PED_MODEL(ePed))
					ELSE
						PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " passed to cutscene from current player ped data.")
						SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), sVariations.iPropIndex[iComponent], sVariations.iPropTexture[iComponent], GET_PLAYER_PED_MODEL(ePed))
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " blocked.")
				#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF

	// Otherwise use stored ped or stored variations
	ELSE
		INT iPed = 0
		BOOL bPedUsed = FALSE
		WHILE iPed < NUM_PLAYER_PED_IDS AND bPedUsed = FALSE
			IF DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[iPed])
				IF NOT IS_PED_INJURED(g_piCreatedPlayerPedIDs[iPed])
				AND GET_ENTITY_MODEL(g_piCreatedPlayerPedIDs[iPed]) = GET_PLAYER_PED_MODEL(ePed)
					IF g_iFlowBitsetBlockPlayerCutsceneVariations[ePed] = 0
						PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variations for ", GET_PLAYER_PED_STRING(ePed), " using stored ped index.")
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED_SAFE(sSceneHandle, g_piCreatedPlayerPedIDs[iPed], GET_ENTITY_MODEL(g_piCreatedPlayerPedIDs[iPed]))
					ELSE
						PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variations for ", GET_PLAYER_PED_STRING(ePed), " using stored ped index with some components blocked.")
						GET_PED_VARIATIONS_FOR_CUTSCENE(g_piCreatedPlayerPedIDs[iPed], sVariations, eCompBlocks)
						REPEAT NUM_PED_COMPONENTS iComponent
							IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], iComponent)
								PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Component ", iComponent, " passed to cutscene from stored player data.")
								SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle, INT_TO_ENUM(PED_COMPONENT, iComponent), sVariations.iDrawableVariation[iComponent], sVariations.iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(ePed))
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Component ", iComponent, " blocked.")
							#ENDIF
							ENDIF
						ENDREPEAT
						REPEAT NUM_PLAYER_PED_PROPS iComponent
							IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], NUM_PED_COMPONENTS + iComponent)
								IF sVariations.iPropIndex[iComponent] = -1
								OR sVariations.iPropIndex[iComponent] = 255
									PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " passed to cutscene from stored player data. (-1)")
									SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), -1, 0, GET_PLAYER_PED_MODEL(ePed))
								ELSE
									PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " passed to cutscene from stored player data.")
									SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), sVariations.iPropIndex[iComponent], sVariations.iPropTexture[iComponent], GET_PLAYER_PED_MODEL(ePed))
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Prop ", iComponent, " blocked.")
							#ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					bPedUsed = TRUE
				ENDIF
			ENDIF
			iPed++
		ENDWHILE
		
		IF NOT bPedUsed
			PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Setting cutscene variation for ", GET_PLAYER_PED_STRING(ePed), " using stored variations.")
			REPEAT NUM_PED_COMPONENTS iComponent
				IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], iComponent)
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandle, INT_TO_ENUM(PED_COMPONENT, iComponent), g_sLastStoredPlayerPedClothes[ePed].iDrawableVariation[iComponent], g_sLastStoredPlayerPedClothes[ePed].iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(ePed))
				ELSE
					PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Cutscene variation blocked for component ", iComponent, ".")
				ENDIF
			ENDREPEAT
			
			REPEAT NUM_PLAYER_PED_PROPS iComponent
				IF g_sLastStoredPlayerPedClothes[ePed].iPropIndex[iComponent] = -1
				OR g_sLastStoredPlayerPedClothes[ePed].iPropIndex[iComponent] = 255
					// Don't bother setting this..
					SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), -1, 0, GET_PLAYER_PED_MODEL(ePed))
				ELSE
					IF NOT IS_BIT_SET(g_iFlowBitsetBlockPlayerCutsceneVariations[ePed], NUM_PED_COMPONENTS + iComponent)
						SET_CUTSCENE_PED_PROP_VARIATION(sSceneHandle, INT_TO_ENUM(PED_PROP_POSITION, iComponent), g_sLastStoredPlayerPedClothes[ePed].iPropIndex[iComponent], g_sLastStoredPlayerPedClothes[ePed].iPropTexture[iComponent], GET_PLAYER_PED_MODEL(ePed))
					ELSE
						PRINTLN("SET_CURRENT_PLAYER_PED_CUTSCENE_VARIATIONS() - Cutscene prop blocked for prop position ", iComponent, ".")
					ENDIF
				ENDIF
			ENDREPEAT
			
			SET_FLOW_OUTFIT_REQUIREMENTS_FOR_CUTSCENE_HANDLE(sSceneHandle, ePed)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if player is in control of Trevor and has activated Trevor's special ability.
/// RETURNS:
///    TREU if player has activated Trevor's special ability, FALSE if otherwise.
FUNC BOOL IS_PLAYER_USING_TREVORS_SPECIAL_ABILITY()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_ForceCombatTaunt, TRUE)
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Sets up the player ped for an on mission lead in. This only needs to be called once, use CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN() to reset.
///    Call UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN() each frame while doing the lead in.
PROC BLOCK_PLAYER_FOR_LEAD_IN(BOOL bForceUnarmed)

	//Set player proofs.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanBeArrested, FALSE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		IF bForceUnarmed
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
		ENDIF
		REMOVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	//Clear wanted level and disable it from returning during the lock in
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF

	SET_MAX_WANTED_LEVEL(0)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	
ENDPROC

/// PURPOSE:
///    Clears any flags set when you started blocking.
PROC CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_CanBeArrested, TRUE)
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	INT i
		
	REPEAT NUM_PLAYER_PED_IDS i	
		IF DOES_ENTITY_EXIST(g_piCreatedPlayerPedIDs[i])
			IF NOT IS_ENTITY_DEAD(g_piCreatedPlayerPedIDs[i])
				SET_ENTITY_PROOFS(g_piCreatedPlayerPedIDs[i], FALSE, FALSE, FALSE, FALSE, FALSE)
				SET_ENTITY_INVINCIBLE(g_piCreatedPlayerPedIDs[i], FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Only reset wanted multipliers if a mission hasn't started.
	//If a mission has started then the wanted settings will have been
	//reset in the start procedures already. -BenR
	IF g_OnMissionState != MISSION_TYPE_STORY
	AND g_OnMissionState != MISSION_TYPE_STORY_FRIENDS
	AND g_OnMissionState != MISSION_TYPE_STORY_PREP
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Blocks various controls and functions of the player that need to be blocked on a per frame basis.
///    Must be called every frame. Running is disabled by default extra flag for speacial case B*1402721
PROC UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(BOOL bDisableWeaponSwap, BOOL bEnableRun = FALSE)
	
	IF NOT bEnableRun
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_SPRINT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_COVER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_ENTER)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DETONATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DUCK)
	IF bDisableWeaponSwap
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF bEnableRun
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_RUN)
		ELSE
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		ENDIF
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, true)
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Blocks all vehicle controls of the player.
///    Must be called every frame.
PROC DISABLE_PLAYER_VEHICLE_CONTROLS_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

/// PURPOSE:
///    Blocks vehicle controls of the player that are used for driving a vehicle.
///    Must be called every frame.
PROC DISABLE_PLAYER_VEHICLE_DRIVING_CONTROLS_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)	
ENDPROC

/// PURPOSE:
///    Blocks vehicle controls of the player that are not used for driving.
///    Must be called every frame.
PROC DISABLE_PLAYER_VEHICLE_MISC_CONTROLS_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

/// PURPOSE:
///    Removes headset from all SP characters
///    Called when player passes a mission, or rejects a replay (or accepts prep mission replay)
PROC REMOVE_ALL_HEADSETS()
	INT iPed
	PED_INDEX mPed
	PED_COMP_NAME_ENUM eHeadSet
	enumCharacterList ePed
	
	FOR iPed = 0 TO NUM_PLAYER_PED_IDS-1
	
		mPed = g_piCreatedPlayerPedIDs[iPed] 
		
		IF DOES_ENTITY_EXIST(mPed)
		AND NOT IS_PED_INJURED(mPed)
			
			ePed = GET_PLAYER_PED_ENUM(mPed)
			eHeadSet = DUMMY_PED_COMP
			
			SWITCH ePed
				CASE CHAR_MICHAEL
					eHeadSet = PROPS_P0_HEADSET
				BREAK
				CASE CHAR_FRANKLIN
					eHeadSet = PROPS_P1_HEADSET
				BREAK
				CASE CHAR_TREVOR
					eHeadSet = PROPS_P2_HEADSET
				BREAK
			ENDSWITCH
			
			IF eHeadSet != DUMMY_PED_COMP
				IF IS_PED_COMP_ITEM_CURRENT_SP(mPed, COMP_TYPE_PROPS, eHeadSet)
					// head set equipped, remove it
					REMOVE_PED_COMP_ITEM_SP(mPed, COMP_TYPE_PROPS, eHeadSet)
				ENDIF
				
				#if USE_CLF_DLC
				// update saved variations, so headset is fully removed
					IF g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = ENUM_TO_INT(eHeadSet)
						g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = -1
					ENDIF
				#endif
				#if USE_NRM_DLC
				// update saved variations, so headset is fully removed
					IF g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = ENUM_TO_INT(eHeadSet)
						g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = -1
					ENDIF
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC	
				// update saved variations, so headset is fully removed
					IF g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = ENUM_TO_INT(eHeadSet)
						g_savedGlobals.sPlayerData.sInfo.sVariations[ePed].iPropIndex[ANCHOR_EARS] = -1
					ENDIF
				#endif
				#endif				
				
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


PROC SETUP_PLAYER_PED_DEFAULTS_FOR_MP(PED_INDEX ped)

    SWITCH GET_ENTITY_MODEL(ped)
	// MICHAEL
		CASE PLAYER_ZERO
		BREAK
	// FRANKLIN
		CASE PLAYER_ONE
		BREAK
	// TREVOR
		CASE PLAYER_TWO
			ADD_PED_DECORATION_FROM_HASHES( ped, HASH("singleplayer_overlays"), HASH("tp_001") )
		BREAK
	ENDSWITCH

ENDPROC


/// Patch the players weapons if the DLC weapon data changes.
PROC PATCH_DLC_WEAPON_LAYOUT(PED_WEAPONS_STRUCT &sTempWeapons, scrShopWeaponData &weaponData)
	INT iWeaponCount = GET_NUM_DLC_WEAPONS()
	INT iWeapon
	INT iCharIndex
	INT iWeaponSlot
	INT iWeaponSlotToAdd
	BOOL bSearchingForSlot
	
	
	REPEAT 3 iCharIndex
	
		PRINTLN("PATCHING DLC WEAPON LAYOUT called by ",GET_THIS_SCRIPT_NAME()," - charID=", iCharIndex)
		
		sTempWeapons = g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex]
		iWeaponSlotToAdd = 0
		
		REPEAT iWeaponCount iWeapon
			IF iWeaponSlotToAdd < iWeaponCount
			AND GET_DLC_WEAPON_DATA(iWeapon, weaponData)
			AND NOT IS_CONTENT_ITEM_LOCKED(weaponData.m_lockHash)
			AND NOT IS_DLC_WEAPON_LOCKED_BY_SCRIPT(INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash))
				// Find out if we already had this weapon in the array
				bSearchingForSlot = TRUE
				iWeaponSlot = 0
				WHILE bSearchingForSlot AND iWeaponSlot < COUNT_OF(sTempWeapons.sDLCWeaponInfo)
					IF sTempWeapons.sDLCWeaponInfo[iWeaponSlot].eWeaponType = INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash)
						// Player already had this DLC weapon so bung it into the new slot
						g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd] = sTempWeapons.sDLCWeaponInfo[iWeaponSlot]
						PRINTLN("...slot ", iWeaponSlotToAdd, " = ", weaponData.Label, " with ", g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iAmmoCount, " ammo")
						iWeaponSlotToAdd++
						bSearchingForSlot = FALSE
					ENDIF
					iWeaponSlot++
				ENDWHILE
				
				// If we didn't find this weapon, give it to the player with 2 clips.
				IF bSearchingForSlot
				AND (IS_XBOX360_VERSION() OR IS_PS3_VERSION())	// Only do this on current gen.		
					g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iAmmoCount = weaponData.DefaultClipSize*2
					g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].eWeaponType = INT_TO_ENUM(WEAPON_TYPE, weaponData.m_nameHash)
					g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iModsAsBitfield = 0
					g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iTint = 0
					g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iCamo = 0
					PRINTLN("...slot ", iWeaponSlotToAdd, " = ", weaponData.Label, " with ", g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeaponSlotToAdd].iAmmoCount, " ammo *new*")
					iWeaponSlotToAdd++
				ENDIF
			ENDIF
		ENDREPEAT
		
		// No clear out the remaining slots
		FOR iWeapon = iWeaponSlotToAdd TO NUMBER_OF_DLC_WEAPONS-1
			g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].eWeaponType = WEAPONTYPE_INVALID
			g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iModsAsBitfield = 0
			g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iTint = 0
			g_savedGlobals.sPlayerData.sInfo.sWeapons[iCharIndex].sDLCWeaponInfo[iWeapon].iCamo = 0
			PRINTLN("...slot ", iWeapon, " = empty")
		ENDFOR
		
		//Push the new weapon data out to the player character.
		IF GET_CURRENT_PLAYER_PED_INT() = iCharIndex
			RESTORE_PLAYER_PED_WEAPONS(PLAYER_PED_ID())
		ENDIF
	ENDREPEAT
ENDPROC

