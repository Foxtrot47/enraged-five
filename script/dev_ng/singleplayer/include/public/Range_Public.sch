// Range_Public.sch
USING "weapon_enums.sch"
USING "globals.sch"

/// PURPOSE: The weapon categories.
ENUM RANGE_WEAPON_CATEGORY
	WEAPCAT_PISTOL 		= 0,
	WEAPCAT_SMG,
	WEAPCAT_AR,
	WEAPCAT_SHOTGUN,
	WEAPCAT_LIGHT_MG,	
	WEAPCAT_MINIGUN,
	WEAPCAT_RAILGUN,
	NUM_WEAPON_CATS
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_RANGE_WEAPON_CATEGORY(RANGE_WEAPON_CATEGORY eCat)
	SWITCH eCat
		CASE WEAPCAT_PISTOL   RETURN "WEAPCAT_PISTOL" 
		CASE WEAPCAT_SMG      RETURN "WEAPCAT_SMG"
		CASE WEAPCAT_AR       RETURN "WEAPCAT_AR"
		CASE WEAPCAT_SHOTGUN  RETURN "WEAPCAT_SHOTGUN"
		CASE WEAPCAT_LIGHT_MG RETURN "WEAPCAT_LIGHT_MG"	
		CASE WEAPCAT_MINIGUN  RETURN "WEAPCAT_MINIGUN"
		CASE WEAPCAT_RAILGUN  RETURN "WEAPCAT_RAILGUN"
		CASE NUM_WEAPON_CATS  RETURN "NUM_WEAPON_CATS"
	ENDSWITCH
	RETURN "unknown RANGE_WEAPON_CATEGORY"
ENDFUNC
#ENDIF



ENUM GRID_INDEX
	INVALID_LOC = -1,
	gridA1 = 0,
	gridA2,
	gridA3,
	gridA4,
	gridA5,
	gridB1,
	gridB2,
	gridB3,
	gridB4,
	gridB5,
	gridC1,
	gridC2,
	gridC3,
	gridC4,
	gridC5,
	gridD1,
	gridD2,
	gridD3,
	gridD4,
	gridD5,
	MAX_GRID_LOCS
ENDENUM

/// PURPOSE:
///    Returns the challenge name for the currently highlighted challenge.
FUNC TEXT_LABEL_15 GET_RANGE_MP_WEAPON_SPRITE_NAME(RANGE_WEAPON_CATEGORY eCategory, WEAPON_TYPE eWeapon, TEXT_LABEL_15 & szDirName)
	TEXT_LABEL_15 szName
	
	SWITCH (eCategory)
		CASE WEAPCAT_PISTOL
			szDirName = "SRange_Weap"
			szName = "Weap_HG_"
		BREAK
		
		CASE WEAPCAT_SMG
			szDirName = "SRange_Weap2"
			szName = "Weap_SMG_"
		BREAK
		
		CASE WEAPCAT_AR
			szDirName = "SRange_Weap"
			szName = "Weap_AR_"
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			szDirName = "SRange_Weap2"
			szName = "Weap_SG_"
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			szDirName = "SRange_Weap2"
			szName = "Weap_LMG_"
		BREAK
		
		CASE WEAPCAT_MINIGUN
			szDirName = "SRange_Weap"
			szName = "Weap_HVY_1"
			RETURN szName
		BREAK
		
		//NG Current get owners only
		CASE WEAPCAT_RAILGUN
			szDirName = "SRange_Weap"
			szName = "Weap_RG_1"
			RETURN szName
		BREAK
		
	ENDSWITCH
	
	SWITCH eWeapon
		CASE WEAPONTYPE_PISTOL
		CASE WEAPONTYPE_MICROSMG
		CASE WEAPONTYPE_ASSAULTRIFLE
		CASE WEAPONTYPE_PUMPSHOTGUN
		CASE WEAPONTYPE_MG
		CASE WEAPONTYPE_MINIGUN
			szName += 1
		BREAK
		CASE WEAPONTYPE_COMBATPISTOL
		CASE WEAPONTYPE_SMG
		CASE WEAPONTYPE_CARBINERIFLE
		CASE WEAPONTYPE_SAWNOFFSHOTGUN
		CASE WEAPONTYPE_COMBATMG
			szName += 2
		BREAK
		CASE WEAPONTYPE_APPISTOL
		CASE WEAPONTYPE_DLC_ASSAULTSMG
		CASE WEAPONTYPE_ADVANCEDRIFLE
		CASE WEAPONTYPE_ASSAULTSHOTGUN
		CASE WEAPONTYPE_DLC_ASSAULTMG
			szName += 3
		BREAK
		CASE WEAPONTYPE_DLC_PISTOL50
		CASE WEAPONTYPE_DLC_HEAVYRIFLE
		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN
		CASE WEAPONTYPE_DLC_GUSENBERG
			szName += 4
		BREAK
		CASE WEAPONTYPE_DLC_HEAVYPISTOL
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE
			szName += 5
		BREAK
		CASE WEAPONTYPE_DLC_SNSPISTOL
		CASE WEAPONTYPE_DLC_SPECIALCARBINE
			szName += 6
		BREAK
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL
			szName += 7
		BREAK
	ENDSWITCH
	
	RETURN szName
ENDFUNC

/// PURPOSE:
///    Returns the challenge name for the currently highlighted challenge.
FUNC TEXT_LABEL_15 GET_WEAPON_SPRITE_NAME(RANGE_WEAPON_CATEGORY eCategory, RANGE_WEAPON_TYPE eWeapon, TEXT_LABEL_15 & szDirName)
	TEXT_LABEL_15 szName
	
	SWITCH (eCategory)
		CASE WEAPCAT_PISTOL
			szDirName = "SRange_Weap"
			szName = "Weap_HG_"
		BREAK
		
		CASE WEAPCAT_SMG
			szDirName = "SRange_Weap2"
			szName = "Weap_SMG_"
		BREAK
		
		CASE WEAPCAT_AR
			szDirName = "SRange_Weap"
			szName = "Weap_AR_"
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			szDirName = "SRange_Weap2"
			szName = "Weap_SG_"
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			szDirName = "SRange_Weap2"
			szName = "Weap_LMG_"
		BREAK
		
		CASE WEAPCAT_MINIGUN
			szDirName = "SRange_Weap"
			szName = "Weap_HVY_1"
			RETURN szName
		BREAK
		
		//NG Current get owners only
		CASE WEAPCAT_RAILGUN
			szDirName = "SRange_Weap"
			szName = "Weap_RG_1"
			RETURN szName
		BREAK
		
	ENDSWITCH
	
	szName += ENUM_TO_INT(eWeapon) + 1
	RETURN szName
ENDFUNC

/// PURPOSE:
///    Returns the challenge name for the currently highlighted challenge.
FUNC TEXT_LABEL_15 GET_CHALLENGE_SPRITE_NAME(RANGE_WEAPON_CATEGORY eCategory, INT iChallenge, TEXT_LABEL_15 & szSpriteFolder)
	TEXT_LABEL_15 szName = "Chal_Blank"
	
	// If we haven't selected a challenge, leave us as blank.
	IF (iChallenge <> -1)
		SWITCH (eCategory)
			CASE WEAPCAT_PISTOL
				szSpriteFolder = "SRange_Chal"
				szName = "Chal_HG"
			BREAK
			
			CASE WEAPCAT_SMG
				szSpriteFolder = "SRange_Chal2"
				szName = "Chal_SMG"
			BREAK
			
			CASE WEAPCAT_SHOTGUN
				szSpriteFolder = "SRange_Chal2"
				szName = "Chal_SG"
			BREAK
			
			CASE WEAPCAT_AR
				szSpriteFolder = "SRange_Chal"
				szName = "Chal_AR"
			BREAK
			
			CASE WEAPCAT_LIGHT_MG
				szSpriteFolder = "SRange_Chal2"
				szName = "Chal_LMG"
			BREAK
			
			CASE WEAPCAT_MINIGUN
				szSpriteFolder = "SRange_Chal"
				szName = "Chal_HVY"
			BREAK
			
			//NG Current gen owners only
			CASE WEAPCAT_RAILGUN
				szSpriteFolder = "SRange_Chal2"
				szName = "Chal_RG"
			BREAK
			
		ENDSWITCH
		
		szName += iChallenge + 1

	ENDIF
	
	RETURN szName
ENDFUNC

/// PURPOSE:
///    Retreive the category name from the enum.
FUNC STRING GET_CATEGORY_STRING_INFO(RANGE_WEAPON_CATEGORY eWeaponCategory, BOOL bNeedTHEVersion = FALSE)
	SWITCH (eWeaponCategory)
		CASE WEAPCAT_PISTOL
			IF bNeedTHEVersion
				RETURN "WTT_PIST"
			ELSE
				RETURN "SHR_CAT_HG"
			ENDIF
		BREAK
		
		CASE WEAPCAT_SMG
			IF bNeedTHEVersion
				RETURN "WTT_SMG_MCR"
			ELSE
				RETURN "SHR_CAT_SMG"
			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF bNeedTHEVersion
				RETURN "WTT_RIFLE_ASL"
			ELSE
				RETURN "SHR_CAT_AR"
			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF bNeedTHEVersion
				RETURN "WTT_SG_PMP"
			ELSE
				RETURN "SHR_CAT_SG"
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF bNeedTHEVersion
				RETURN "WTT_MG"
			ELSE
				RETURN "SHR_CAT_LMG"
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			IF bNeedTHEVersion
				RETURN "WTT_MINIGUN"
			ELSE
				RETURN "SHR_CAT_HVY"
			ENDIF
		BREAK
		
		//NG Current get owners only
		CASE WEAPCAT_RAILGUN
			IF bNeedTHEVersion
				RETURN "WTT_RAILGUN"
			ELSE
				RETURN "SHR_CAT_RAILGUN"
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Given the category and the range weapon type, get the game weapon type.
/// RETURNS:
///    The WEAPON_TYPE corresponding.
FUNC WEAPON_TYPE GET_WEAPONTYPE_FROM_RANGE_INFO(RANGE_WEAPON_CATEGORY eWeapCat, RANGE_WEAPON_TYPE eRangeWeapon)
	SWITCH (eWeapCat)
		CASE WEAPCAT_PISTOL
			IF (eRangeWeapon = RANGE_PISTOL)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_PISTOL")
				RETURN WEAPONTYPE_PISTOL
			ELIF (eRangeWeapon = RANGE_DLC_PISTOL50)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_DLC_PISTOL50")
				RETURN WEAPONTYPE_DLC_PISTOL50
			ELIF (eRangeWeapon = RANGE_COMBATPISTOL)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_COMBATPISTOL")
				RETURN WEAPONTYPE_COMBATPISTOL
			ELIF (eRangeWeapon = RANGE_APPISTOL)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_APPISTOL")
				RETURN WEAPONTYPE_APPISTOL
			ELIF (eRangeWeapon = RANGE_DLC_SNSPISTOL)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_APPISTOL")
				RETURN WEAPONTYPE_DLC_SNSPISTOL
			ENDIF
		BREAK
		
		CASE WEAPCAT_SMG
			IF (eRangeWeapon = RANGE_MICROSMG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_MICROSMG")
				RETURN WEAPONTYPE_MICROSMG
			ELIF (eRangeWeapon = RANGE_SMG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_SMG")
				RETURN WEAPONTYPE_SMG
			ELIF (eRangeWeapon = RANGE_DLC_ASSAULTSMG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_DLC_ASSAULTSMG")
				RETURN WEAPONTYPE_DLC_ASSAULTSMG
			ENDIF
		BREAK
		
		CASE WEAPCAT_AR
			IF (eRangeWeapon = RANGE_ASSAULTRIFLE)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_ASSAULTRIFLE")
				RETURN WEAPONTYPE_ASSAULTRIFLE
			ELIF (eRangeWeapon = RANGE_CARBINERIFLE)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_CARBINERIFLE")
				RETURN WEAPONTYPE_CARBINERIFLE
			ELIF (eRangeWeapon = RANGE_DLC_SPECIALCARBINE)
				RETURN WEAPONTYPE_DLC_SPECIALCARBINE
			ELIF (eRangeWeapon = RANGE_DLC_HEAVYRIFLE)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_DLC_HEAVYRIFLE")
				RETURN WEAPONTYPE_DLC_HEAVYRIFLE
			ELIF (eRangeWeapon = RANGE_ADVANCEDRIFLE)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_ADVANCEDRIFLE")
				RETURN WEAPONTYPE_ADVANCEDRIFLE
			ENDIF
		BREAK
		
		CASE WEAPCAT_SHOTGUN
			IF (eRangeWeapon = RANGE_PUMPSHOTGUN)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_PUMPSHOTGUN")
				RETURN WEAPONTYPE_PUMPSHOTGUN
			ELIF (eRangeWeapon = RANGE_SAWNOFFSHOTGUN)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_SAWNOFFSHOTGUN")
				RETURN WEAPONTYPE_SAWNOFFSHOTGUN
			ELIF (eRangeWeapon = RANGE_DLC_BULLPUPSHOTGUN)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_DLC_BULLPUPSHOTGUN")
				RETURN WEAPONTYPE_DLC_BULLPUPSHOTGUN
			ELIF (eRangeWeapon = RANGE_ASSAULTSHOTGUN)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_ASSAULTSHOTGUN")
				RETURN WEAPONTYPE_ASSAULTSHOTGUN
			ENDIF
		BREAK
		
		CASE WEAPCAT_LIGHT_MG
			IF (eRangeWeapon = RANGE_MG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_MG")
				RETURN WEAPONTYPE_MG
			ELIF (eRangeWeapon = RANGE_COMBATMG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_COMBATMG")
				RETURN WEAPONTYPE_COMBATMG
			ELIF (eRangeWeapon = RANGE_DLC_ASSAULTSMG)
//				PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_DLC_ASSAULTMG")
				RETURN WEAPONTYPE_DLC_ASSAULTMG
			ENDIF
		BREAK
		
		CASE WEAPCAT_MINIGUN
			IF (eRangeWeapon = RANGE_MINIGUN)	
	//			PRINTLN("GET_WEAPONTYPE_FROM_RANGE_INFO returning WEAPONTYPE_MINIGUN")
				RETURN WEAPONTYPE_MINIGUN	
			ENDIF
		BREAK
		
		CASE WEAPCAT_RAILGUN
			//NG Current gen owners only
			IF (eRangeWeapon = RANGE_RAILGUN)
				RETURN WEAPONTYPE_DLC_RAILGUN
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN WEAPONTYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Returns the highest medal achieved in eRoundType
/// PARAMS:
///    eRoundType - 
/// RETURNS:
///    The highest medal earned in a given round
FUNC RANGE_ROUND_MEDAL GET_RANGE_CHALLENGE_ROUND_MEDAL(RANGE_ROUND_TYPE eRoundType, enumCharacterList ePlayerChar)
	RETURN (g_savedGlobals.sRangeData[ePlayerChar].sRounds[eRoundType].eTopMedal)
ENDFUNC

/// PURPOSE:
///    Returns the highest medal awarded in a weapon unlock category
/// PARAMS:
///    eCat - 
/// RETURNS:
///    Highest medal earned in a shooting range weapon category
FUNC RANGE_ROUND_MEDAL GET_HIGHEST_MEDAL_IN_RANGE_CHALLENGE_CATEGORY(RANGE_WEAPON_CATEGORY eWeapCat, enumCharacterList ePlayerChar)
	RANGE_ROUND_TYPE eRound1, eRound2, eRound3, eRound4
	RANGE_ROUND_MEDAL eReturnMedal
	
	// Determine the challenges we're asking about
	IF eWeapCat = WEAPCAT_PISTOL
		eRound1 = RT_PISTOL_CHAL_1
		eRound2 = RT_PISTOL_CHAL_2
		eRound3 = RT_PISTOL_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_SMG
		eRound1 = RT_SMG_CHAL_1
		eRound2 = RT_SMG_CHAL_2
		eRound3 = RT_SMG_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_AR
		eRound1 = RT_AR_CHAL_1
		eRound2 = RT_AR_CHAL_2
		eRound3 = RT_AR_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_SHOTGUN
		eRound1 = RT_SHOTGUN_CHAL_1
		eRound2 = RT_SHOTGUN_CHAL_2
		eRound3 = RT_SHOTGUN_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_LIGHT_MG
		eRound1 = RT_LMG_CHAL_1
		eRound2 = RT_LMG_CHAL_2
		eRound3 = RT_LMG_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_MINIGUN
		eRound1 = RT_MINIGUN_CHAL_1
		eRound2 = RT_MINIGUN_CHAL_2
		eRound3 = RT_MINIGUN_CHAL_3
		eRound4 = RT_INVALID
	//NG Current gen owners only
	ELIF eWeapCat = WEAPCAT_RAILGUN
		eRound1 = RT_RAILGUN_CHAL_1
		eRound2 = RT_RAILGUN_CHAL_2
		eRound3 = RT_RAILGUN_CHAL_3
		eRound4 = RT_RAILGUN_CHAL_4
	ELSE
		PRINTLN("Invalid RANGE_UNLOCK_CATEGORIES eWeapCat sent in")
		RETURN RRM_NONE
	ENDIF
	
	// Set our returnMedal to the medal in round 1
	eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound1, ePlayerChar)
	// if the round 2 medal is greater, return that one
	IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound2, ePlayerChar) > eReturnMedal
		eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound2, ePlayerChar)
	ENDIF
	// if the round 3 medal is greater, return that one
	IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound3, ePlayerChar) > eReturnMedal
		eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound3, ePlayerChar)
	ENDIF
	IF eRound4 <> RT_INVALID
		// if the round 4 medal is greater, return that one
		IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound4, ePlayerChar) > eReturnMedal
			eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound4, ePlayerChar)
		ENDIF
	ENDIF
	// Return that medal!
	RETURN eReturnMedal
ENDFUNC

/// PURPOSE:
///    Returns the highest medal awarded in a weapon unlock category
/// PARAMS:
///    eCat - 
/// RETURNS:
///    Highest medal earned in a shooting range weapon category
FUNC RANGE_ROUND_MEDAL GET_LOWEST_MEDAL_IN_RANGE_CHALLENGE_CATEGORY(RANGE_WEAPON_CATEGORY eWeapCat, enumCharacterList ePlayerChar)
	RANGE_ROUND_TYPE eRound1, eRound2, eRound3, eRound4
	RANGE_ROUND_MEDAL eReturnMedal
	
	// Determine the challenges we're asking about
	IF eWeapCat = WEAPCAT_PISTOL
		eRound1 = RT_PISTOL_CHAL_1
		eRound2 = RT_PISTOL_CHAL_2
		eRound3 = RT_PISTOL_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_SMG
		eRound1 = RT_SMG_CHAL_1
		eRound2 = RT_SMG_CHAL_2
		eRound3 = RT_SMG_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_AR
		eRound1 = RT_AR_CHAL_1
		eRound2 = RT_AR_CHAL_2
		eRound3 = RT_AR_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_SHOTGUN
		eRound1 = RT_SHOTGUN_CHAL_1
		eRound2 = RT_SHOTGUN_CHAL_2
		eRound3 = RT_SHOTGUN_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_LIGHT_MG
		eRound1 = RT_LMG_CHAL_1
		eRound2 = RT_LMG_CHAL_2
		eRound3 = RT_LMG_CHAL_3
		eRound4 = RT_INVALID
	ELIF eWeapCat = WEAPCAT_MINIGUN
		eRound1 = RT_MINIGUN_CHAL_1
		eRound2 = RT_MINIGUN_CHAL_2
		eRound3 = RT_MINIGUN_CHAL_3
		eRound4 = RT_INVALID
	//NG Current gen owners only
	ELIF eWeapCat = WEAPCAT_RAILGUN
		eRound1 = RT_RAILGUN_CHAL_1
		eRound2 = RT_RAILGUN_CHAL_2
		eRound3 = RT_RAILGUN_CHAL_3
		eRound4 = RT_RAILGUN_CHAL_4
	ELSE
		PRINTLN("Invalid RANGE_UNLOCK_CATEGORIES eWeapCat sent in")
		RETURN RRM_NONE
	ENDIF
	
	// Set our returnMedal to the medal in round 1
	eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound1, ePlayerChar)
	// if the round 2 medal is greater, return that one
	IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound2, ePlayerChar) < eReturnMedal
		eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound2, ePlayerChar)
	ENDIF
	// if the round 3 medal is greater, return that one
	IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound3, ePlayerChar) < eReturnMedal
		eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound3, ePlayerChar)
	ENDIF
	IF eRound4 != RT_INVALID
		// if the round 4 medal is greater, return that one
		IF GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound4, ePlayerChar) < eReturnMedal
			eReturnMedal = GET_RANGE_CHALLENGE_ROUND_MEDAL(eRound4, ePlayerChar)
		ENDIF
	ENDIF
	// Return that medal!
	RETURN eReturnMedal
ENDFUNC

