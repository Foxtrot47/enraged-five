//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	scaleform_helper.sch										//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	A quick way to load/unload scaleform movies.				//
//							Prevents scripts from having to setup additional control 	//
//							enums and flags.											//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

/// PURPOSE: Helper function used to make sure a scaleform movie is ready to use
///    		 Ideally this function should be called each frame, and have
///    		 the calling script perform the movie specifics when TRUE is returned.
///    NOTE: Currently, this header doesn't check to see if the specific movie name
///    		 has been requested from another script - an assert will be thrown if it has
/// 
/// PARAMS:
///    sData - Fill this data struct with the movie name and required load settings
/// RETURNS: TRUE when the specified movie has loaded
FUNC BOOL SETUP_SCALEFORM_MOVIE(SCALEFORM_DATA_STRUCT &sData)
	SWITCH sData.stage
		CASE SF_REQUEST
			PRINTSTRING("\n*****************")
			PRINTSTRING("\n[ScaleformHelper] SETUP_SCALEFORM_MOVIE - Setup movie '")PRINTSTRING(sData.filename)PRINTSTRING("'")
	
			// We only need to request the movie once
			IF NOT HAS_SCALEFORM_MOVIE_LOADED(sData.movieID)
				PRINTSTRING("\n[ScaleformHelper] SETUP_SCALEFORM_MOVIE - Requesting movie...")
				
				sData.movieID = REQUEST_SCALEFORM_MOVIE(sData.filename)
				sData.stage = SF_WAIT
				
				// If we requested the movie to block load then skip to loaded stage
				IF sData.blockLoad
					IF HAS_SCALEFORM_MOVIE_LOADED(sData.movieID)
						sData.loadTimer = GET_GAME_TIMER()
						sData.stage = SF_LOADED
					ENDIF
				ENDIF
			ELSE
				PRINTSTRING("\n[ScaleformHelper] SETUP_SCALEFORM_MOVIE - Movie already loaded")
				
				// Movie has already been loaded
				sData.loadTimer = GET_GAME_TIMER()
				sData.stage = SF_LOADED
			ENDIF
			
			PRINTSTRING("\n***************************************************")PRINTNL()
		BREAK
		
		CASE SF_WAIT
			// Wait for the movie to load
			IF HAS_SCALEFORM_MOVIE_LOADED(sData.movieID)
				PRINTSTRING("\n*****************")
				PRINTSTRING("\n[ScaleformHelper] SETUP_SCALEFORM_MOVIE - Setup movie '")PRINTSTRING(sData.filename)PRINTSTRING("'")
				PRINTSTRING("\n[ScaleformHelper] SETUP_SCALEFORM_MOVIE - Movie loaded")
				PRINTSTRING("\n***************************************************")PRINTNL()
				
				sData.loadTimer = GET_GAME_TIMER()
				sData.stage = SF_LOADED
			ENDIF
		BREAK
		
		CASE SF_LOADED
			// Waiting for script to call CLEANUP_SCALEFORM_MOVIE()...
			
			// Detect if the movie has been cleaned up...
			IF NOT HAS_SCALEFORM_MOVIE_LOADED(sData.movieID)
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("[ScaleformHelper] SETUP_SCALEFORM_MOVIE - movie was cleanud up!")
				#ENDIF
				sData.stage = SF_REQUEST
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN (sData.stage = SF_LOADED)
ENDFUNC

/// PURPOSE: Helper function used to unload a scaleform movie
/// 
/// PARAMS:
///    sData - This should be the same data struct used to load the scaleform movie using LOAD_SCALEFORM_MOVIE_HELPER()
PROC CLEANUP_SCALEFORM_MOVIE(SCALEFORM_DATA_STRUCT &sData)
	IF sData.stage <> SF_REQUEST
		// Cleanup the movie and return to the default stage
		IF HAS_SCALEFORM_MOVIE_LOADED(sData.movieID)
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sData.movieID)
		ENDIF
		
		sData.movieID = NULL
		sData.stage = SF_REQUEST
		PRINTSTRING("\n[ScaleformHelper] CLEANUP_SCALEFORM_MOVIE - Cleaned up movie")
	ELSE
		PRINTSTRING("\n[ScaleformHelper] CLEANUP_SCALEFORM_MOVIE - Failed cleanup! in request stage")
	ENDIF
ENDPROC
