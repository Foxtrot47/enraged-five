//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 14/02/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 		Vector ID Public Header								│
//│																				│
//│			The public interface for managing and doing area checks with 		│
//│			VectorIDs.															│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛  

USING "rage_builtins.sch"
USING "globals.sch"

USING "../../scripts/Ambient/BaseJumping/bj.sch"
USING "../../scripts/Ambient/BaseJumping/bj_args.sch"
USING "../../scripts/Ambient/BaseJumping/bj_data.sch"
USING "../../scripts/Minigames/Assassinations/Assassination_MissionData.sch"

#IF NOT USE_SP_DLC		USING "vector_ID_data_GTA5.sch"			#ENDIF
#IF USE_CLF_DLC			USING "vector_ID_data_CLF.sch"			#ENDIF
#IF USE_NRM_DLC			USING "vector_ID_data_NRM.sch"			#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ Vector ID Checking ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL IS_PED_AT_VECTOR_ID(PED_INDEX pedID, VectorID eVectorID, FLOAT fRadiusOverride = 0.0)
	IF NOT IS_ENTITY_DEAD(pedID)
		VectorIDData sVecID = g_sVectorIDData[eVectorID]
		
		FLOAT fRadius = sVecID.radius
		IF fRadiusOverride > 0.0
			fRadius = fRadiusOverride
		ENDIF

		IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedID), sVecID.position) < fRadius)
			RETURN TRUE
		ELIF sVecID.extensionID <> VID_BLANK
			//Recursively check the extension vector ID.
			RETURN IS_PED_AT_VECTOR_ID(pedID, sVecID.extensionID, fRadiusOverride)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


DEBUGONLY PROC DEBUG_DRAW_VECTOR_ID_AREA(VectorID eVectorID, STRING strDebugName, FLOAT fRadiusOverride = 0.0)
	VectorIDData sVecID = g_sVectorIDData[eVectorID]
	
	FLOAT fRadius = sVecID.radius
	IF fRadiusOverride > 0.0
		fRadius = fRadiusOverride
	ENDIF

	DRAW_DEBUG_TEXT(strDebugName, <<sVecID.position.X, sVecID.position.Y, sVecID.position.Z>>, 255, 0, 0, 255)
	DRAW_DEBUG_SPHERE(sVecID.position, fRadius, 255, 0, 255, 40) 
	
	//Recursively draw any daisy-chained restricted areas.
	IF sVecID.extensionID != VID_BLANK
		DEBUG_DRAW_VECTOR_ID_AREA(sVecID.extensionID, strDebugName, fRadiusOverride)
	ENDIF
ENDPROC
