USING "globals.sch"

/// PURPOSE:
///    Returns the medal earned for a specific lesson. 
/// PARAMS:
///    eThisLesson
/// RETURNS:
///    Medal earned in a pilot school lesson
FUNC PILOT_SCHOOL_MEDAL GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(enumCharacterList thisChar, PILOT_SCHOOL_CLASSES_ENUM eThisLesson)
	IF thisChar < CHAR_MICHAEL OR thisChar > CHAR_TREVOR
		PRINTLN("INVALID PLAYER CHARACTER!!! RETURNING NO MEDAL")
		RETURN PS_NONE
	ENDIF
	
	RETURN g_savedGlobals.sFlightSchoolData[thisChar].PlayerData[eThisLesson].eMedal
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_GOLD_MEDAL_FOR_PILOT_SCHOOL_LESSON(enumCharacterList thisChar, PILOT_SCHOOL_CLASSES_ENUM eThisLesson)
	IF thisChar < CHAR_MICHAEL OR thisChar > CHAR_TREVOR
		PRINTLN("INVALID PLAYER CHARACTER!!! RETURNING NO FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_savedGlobals.sFlightSchoolData[thisChar].PlayerData[eThisLesson].eMedal = PS_GOLD
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_SILVER_MEDAL_FOR_PILOT_SCHOOL_LESSON(enumCharacterList thisChar, PILOT_SCHOOL_CLASSES_ENUM eThisLesson)
	IF thisChar < CHAR_MICHAEL OR thisChar > CHAR_TREVOR
		PRINTLN("INVALID PLAYER CHARACTER!!! RETURNING NO FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_savedGlobals.sFlightSchoolData[thisChar].PlayerData[eThisLesson].eMedal = PS_SILVER
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_BRONZE_MEDAL_FOR_PILOT_SCHOOL_LESSON(enumCharacterList thisChar, PILOT_SCHOOL_CLASSES_ENUM eThisLesson)
	IF thisChar < CHAR_MICHAEL OR thisChar > CHAR_TREVOR
		PRINTLN("INVALID PLAYER CHARACTER!!! RETURNING NO FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_savedGlobals.sFlightSchoolData[thisChar].PlayerData[eThisLesson].eMedal = PS_BRONZE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_ANY_MEDAL_FOR_PILOT_SCHOOL_LESSON(enumCharacterList thisChar, PILOT_SCHOOL_CLASSES_ENUM eThisLesson)
	IF thisChar < CHAR_MICHAEL OR thisChar > CHAR_TREVOR
		PRINTLN("INVALID PLAYER CHARACTER!!! RETURNING NO FALSE")
		RETURN FALSE
	ENDIF
	
	IF g_savedGlobals.sFlightSchoolData[thisChar].PlayerData[eThisLesson].eMedal = PS_NONE
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

