
/////////
///    Functions and datatypes concerning the display of the end screen described at
///        https://devstar.rockstargames.com/wiki/index.php/MISSION_BOX
///    
///    
///    
///    
///    
///    Ak
//////////

  
USING "rage_builtins.sch"
USING "globals.sch"
USING "menu_public.sch"
USING "shared_hud_displays.sch"
//USING "net_mission_details_hud.sch" // can't use this because cyclic headers

USING "end_screen_old.sch"	//	For old versions of functions used by single player scripts in Final Submission. They use END_SCREEN_DATASET instead of END_SCREEN_DATASET_TITLE_UPDATE.


///////////
///    
///    Duplicate functions that need to live here due to cyclic header bull
///   
///    
///    
ENUM CREW_RANK_ORDER_COPY
	CREW_RANK_NONE_COPY  = -1,

	CREW_RANK_LEADER_COPY  = 0,
	CREW_RANK_MEMBER1_COPY,
	CREW_RANK_MEMBER2_COPY,
	CREW_RANK_MEMBER3_COPY,
	CREW_RANK_MEMBER4_COPY,
	CREW_RANK_MEMBER5_COPY
ENDENUM
PROC DRAW_CREW_TAG_GAMER_COPY(GAMER_HANDLE& aPlayer, FLOAT ScreenX, FLOAT ScreenY, FLOAT ScaleMultiplyer = 1.0)

	BOOL IsRockstar
	BOOL IsPrivate
	INT iRankPosition
	
	GAMER_HANDLE bPlayers = aPlayer
	bPlayers = bPlayers
	
	TEXT_LABEL_31 tl31_CrewIcon = ""
	TEXT_LABEL_31 tl31_CrewIconBackground = ""
	TEXT_LABEL_31 tl31_CrewText = "EFG"
	TEXT_LABEL_31 tl31_CrewRank = ""

	/*
	IF IS_PLAYER_IN_ACTIVE_CLAN(aPlayer)
		IsPrivate = (IS_PLAYER_CLAN_PRIVATE(aPlayer))
		IF IS_PLAYER_CLAN_LEADER(aPlayer)
			iRankPosition = 0
		ELSE
			iRankPosition = -1
		ENDIF
		IsRockstar = IS_PLAYER_IN_A_ROCKSTAR_CREW(aPlayer)
		tl31_CrewText  = GET_PLAYER_CLAN_TAG(aPlayer)
	ELSE
		EXIT
	ENDIF
	*/
	
	TEXT_STYLE TSCrewStyle
	TEXT_STYLE TSCrewIconStyle
	TEXT_STYLE TSCrewIconStyleBackground
	TEXT_STYLE TSCrewRankCharacter
	TEXT_PLACEMENT CrewPlacement
	TEXT_PLACEMENT CrewIconPlacement
	TEXT_PLACEMENT CrewRankCharacter

//	#IF IS_DEBUG_BUILD
//	IF CrewTagWidget = FALSE
//		START_WIDGET_GROUP("DRAW_CREW_TAG")
//			ADD_WIDGET_BOOL("bUseWidgetLogic", bUseWidgetLogic)
//			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewPlacement, "WidgetCrewPlacement")
//			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewIconPlacement, "WidgetCrewIconPlacement")
//			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewRankCharacterPlacement, "WidgetCrewRankCharacterPlacement")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewStyle, "WidgetCrewStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewIconStyle, "WidgetCrewIconStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewIconBackgroundStyle, "WidgetCrewIconBackgroundStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewRankCharacter, "WidgetCrewRankCharacter")
//			ADD_WIDGET_BOOL("Is a Rockstar", WidgetIsRockstar)
//			ADD_WIDGET_BOOL("Is Private", WidgetIsPrivate)
//			ADD_WIDGET_INT_SLIDER("Rank Level", iRankOrder, 0, 6, 1)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalFactorX", PositionalFactorX, -100, 100, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleFactor", ScaleFactor, -10, 10, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalRankFactorX", PositionalRankFactorX, -100, 100, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalRankFactorY", PositionalRankFactorY, -100, 100, 0.001)
//
//
//		STOP_WIDGET_GROUP()
//	
//		CrewTagWidget = TRUE
//	ENDIF
//	#ENDIF
	
	SET_TEXT_PLACEMENT(CrewPlacement, ScreenX, ScreenY)
	SET_TEXT_PLACEMENT(CrewIconPlacement, ScreenX, ScreenY)
	SET_TEXT_PLACEMENT(CrewRankCharacter, ScreenX, ScreenY)
	
	
//	#IF IS_DEBUG_BUILD
//	ScaleMultiplyer += ScaleFactor
//	#ENDIF
	
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewStyle)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewIconStyle)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewIconStyleBackground)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewRankCharacter)

	TSCrewStyle.YScale += 0.24*ScaleMultiplyer
	TSCrewStyle.WrapEndX += 0
	TSCrewStyle.WrapStartX += 0
	TSCrewStyle.r += 0
	TSCrewStyle.g += 0
	TSCrewStyle.b += 0
	TSCrewStyle.a += 0
	
	TSCrewIconStyle.YScale += 0.24*ScaleMultiplyer
	TSCrewIconStyle.WrapEndX += 0
	TSCrewIconStyle.WrapStartX += 0
	TSCrewIconStyle.r += 0
	TSCrewIconStyle.g += 0
	TSCrewIconStyle.b += 0
	TSCrewIconStyle.a += 0
	
	TSCrewIconStyleBackground.YScale += 0.24*ScaleMultiplyer
	TSCrewIconStyleBackground.WrapEndX +=0
	TSCrewIconStyleBackground.WrapStartX +=0
	TSCrewIconStyleBackground.r += 0
	TSCrewIconStyleBackground.g += 0
	TSCrewIconStyleBackground.b += 0
	TSCrewIconStyleBackground.a += 0
	
	TSCrewRankCharacter.YScale += 0.24*ScaleMultiplyer
	TSCrewRankCharacter.WrapEndX += 0
	TSCrewRankCharacter.WrapStartX += 0
	TSCrewRankCharacter.r += 0
	TSCrewRankCharacter.g += 0
	TSCrewRankCharacter.b += 0
	TSCrewRankCharacter.a += 0
	
	

	SET_TEXT_BLACK(TSCrewStyle)
	TSCrewStyle.aFont = FONT_ROCKSTAR_TAG
	TSCrewIconStyle.aFont = FONT_ROCKSTAR_TAG
	TSCrewIconStyleBackground.aFont = FONT_ROCKSTAR_TAG
	TSCrewRankCharacter.aFont = FONT_ROCKSTAR_TAG
	
//	#IF IS_DEBUG_BUILD
//	TSCrewStyle.YScale += WidgetCrewStyle.YScale
//	TSCrewStyle.WrapEndX += WidgetCrewStyle.WrapEndX
//	TSCrewStyle.WrapStartX += WidgetCrewStyle.WrapStartX
//	TSCrewStyle.r += WidgetCrewStyle.r
//	TSCrewStyle.g += WidgetCrewStyle.g
//	TSCrewStyle.b += WidgetCrewStyle.b
//	TSCrewStyle.a += WidgetCrewStyle.a
//	
//	TSCrewIconStyle.YScale += WidgetCrewIconStyle.YScale
//	TSCrewIconStyle.WrapEndX += WidgetCrewIconStyle.WrapEndX
//	TSCrewIconStyle.WrapStartX += WidgetCrewIconStyle.WrapStartX
//	TSCrewIconStyle.r += WidgetCrewIconStyle.r
//	TSCrewIconStyle.g += WidgetCrewIconStyle.g
//	TSCrewIconStyle.b += WidgetCrewIconStyle.b
//	TSCrewIconStyle.a += WidgetCrewIconStyle.a
//	
//	TSCrewIconStyleBackground.YScale += WidgetCrewIconBackgroundStyle.YScale
//	TSCrewIconStyleBackground.WrapEndX += WidgetCrewIconBackgroundStyle.WrapEndX
//	TSCrewIconStyleBackground.WrapStartX += WidgetCrewIconBackgroundStyle.WrapStartX
//	TSCrewIconStyleBackground.r += WidgetCrewIconBackgroundStyle.r
//	TSCrewIconStyleBackground.g += WidgetCrewIconBackgroundStyle.g
//	TSCrewIconStyleBackground.b += WidgetCrewIconBackgroundStyle.b
//	TSCrewIconStyleBackground.a += WidgetCrewIconBackgroundStyle.a
//	
//	TSCrewRankCharacter.YScale += WidgetCrewRankCharacter.YScale
//	TSCrewRankCharacter.WrapEndX += WidgetCrewRankCharacter.WrapEndX
//	TSCrewRankCharacter.WrapStartX += WidgetCrewRankCharacter.WrapStartX
//	TSCrewRankCharacter.r += WidgetCrewRankCharacter.r
//	TSCrewRankCharacter.g += WidgetCrewRankCharacter.g
//	TSCrewRankCharacter.b += WidgetCrewRankCharacter.b
//	TSCrewRankCharacter.a += WidgetCrewRankCharacter.a
//	
//	#ENDIF
	
	
	
//	CrewPlacement.x += 0.017*PositionalFactorX
	CrewPlacement.x += ((0.005*ScaleMultiplyer)+0.005)
	CrewPlacement.y += 0.0
	CrewIconPlacement.x += 0
	CrewIconPlacement.y +=  0
	CrewRankCharacter.x += 0
	CrewRankCharacter.y += 0//0.500
	
//	#IF IS_DEBUG_BUILD
//		CrewPlacement.x += WidgetCrewPlacement.x
//		CrewPlacement.y += WidgetCrewPlacement.y
//		CrewIconPlacement.x += WidgetCrewIconPlacement.x
//		CrewIconPlacement.y += WidgetCrewIconPlacement.y
//		CrewRankCharacter.x += WidgetCrewRankCharacterPlacement.x
//		CrewRankCharacter.y += WidgetCrewRankCharacterPlacement.y	
//	#ENDIF
	
	

//	#IF IS_DEBUG_BUILD
//	IF bUseWidgetLogic
//		IF WidgetIsRockstar
//			tl31_CrewText += "@" 
//		ELSE
//			tl31_CrewText += "" 
//		ENDIF
//		
//		CREW_RANK_ORDER CrewIndex = INT_TO_ENUM(CREW_RANK_ORDER, iRankOrder)
//		tl31_CrewText = "RSND"
//		
//		SWITCH CrewIndex
//		
//			CASE CREW_RANK_NONE
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//		
//			CASE CREW_RANK_LEADER
//				tl31_CrewRank += "%"
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_MEMBER1
//	//			tl31_CrewRank += "&"
//				
//				
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_MEMBER2
//	//			tl31_CrewRank += "'"
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_MEMBER3
//	//			tl31_CrewRank += "("
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_MEMBER4
//	//			tl31_CrewRank += ")"
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_MEMBER5
//	//			tl31_CrewRank += "+"
//				
//				IF WidgetIsPrivate = FALSE
//					tl31_CrewIcon += "#" 
//					tl31_CrewIconBackground += "$"
//				ELSE
//					tl31_CrewIcon += "!"
//					tl31_CrewIconBackground +=   "\"" 
//				ENDIF
//			BREAK
//			
//			
//		
//		ENDSWITCH
//	ENDIF
//
//	#ENDIF
		
		

		
//	#IF IS_DEBUG_BUILD
//	IF bUseWidgetLogic = FALSE	
//	#ENDIF
		
	IF IsRockstar
		tl31_CrewText += "@" 
	ELSE
		tl31_CrewText += "" 
	ENDIF
	
	CREW_RANK_ORDER_COPY CrewIndex = INT_TO_ENUM(CREW_RANK_ORDER_COPY, iRankPosition)
	
	SWITCH CrewIndex
	
		CASE CREW_RANK_NONE_COPY
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK

		CASE CREW_RANK_LEADER_COPY
			tl31_CrewRank += "%"
			
			IF IsPrivate 
				tl31_CrewIcon += "." 
				tl31_CrewIconBackground += "/"
			ELSE
				tl31_CrewIcon += ","
				tl31_CrewIconBackground +=   "-" 
			ENDIF
		BREAK
		CASE CREW_RANK_MEMBER1_COPY
//			tl31_CrewRank += "&"
			
			
			
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK
		CASE CREW_RANK_MEMBER2_COPY
//			tl31_CrewRank += "'"
			
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK
		CASE CREW_RANK_MEMBER3_COPY
//			tl31_CrewRank += "("
			
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK
		CASE CREW_RANK_MEMBER4_COPY
//			tl31_CrewRank += ")"
			
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK
		CASE CREW_RANK_MEMBER5_COPY
//			tl31_CrewRank += "+"
			
			IF IsPrivate 
				tl31_CrewIcon += "#" 
				tl31_CrewIconBackground += "$"
			ELSE
				tl31_CrewIcon += "!"
				tl31_CrewIconBackground +=   "\"" 
			ENDIF
		BREAK
	
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	ENDIF
//	#ENDIF
		


	SET_TEXT_STYLE(TSCrewIconStyleBackground)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewIconPlacement, TSCrewIconStyleBackground, tl31_CrewIconBackground, "", HUD_COLOUR_WHITE)
	
		
		
	SET_TEXT_STYLE(TSCrewIconStyle)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewIconPlacement, TSCrewIconStyle, tl31_CrewIcon, "",HUD_COLOUR_BLACK )

	
	SET_TEXT_STYLE(TSCrewStyle)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewPlacement, TSCrewStyle, tl31_CrewText, "", HUD_COLOUR_BLACK)

	SET_TEXT_STYLE(TSCrewRankCharacter)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewRankCharacter, TSCrewRankCharacter, tl31_CrewRank, "", HUD_COLOUR_BLACK)


ENDPROC




/////////
///    



/*
CONST_FLOAT ESC_LINE_Y_ADVANCE 25.0		//0.0347 = 25 pixel advance
CONST_FLOAT ESC_LINE_Y_GAP 2.0			//0.00278 = 2 pixel advance
//CONST_INT ESC_ALPHA 77
CONST_FLOAT ESC_LINE_Y_ADVANCE_HEADER 33.0	//0.04722222192 = 34 pixel advance
CONST_FLOAT ESC_HALF_WIDTH 0.1125
CONST_FLOAT ESC_INDENT 0.006
CONST_INT 	ESC_BLEND_TIME 1000
CONST_FLOAT ESC_Y_POSITION 0.2625

CONST_FLOAT ESC_X_PIXEL 0.00078125
CONST_FLOAT ESC_Y_PIXEL 0.00138888888
CONST_FLOAT ESC_SOCIAL_BULLSHIT_TAB_Y 0.138888888

//CONST_INT ESC_MAX_ELEMENTS 11

CONST_INT MEDAL_PIX_SIDE 16

FUNC FLOAT PIXEL_X(FLOAT PIXELSIZE)
	RETURN PIXELSIZE * ESC_X_PIXEL
ENDFUNC
FUNC FLOAT PIXEL_Y(FLOAT PIXELSIZE)
	RETURN PIXELSIZE * ESC_Y_PIXEL
ENDFUNC
FUNC FLOAT Get_PT_scale(FLOAT PTSIZE)
	RETURN PTSIZE * 0.025
ENDFUNC
*/

PROC END_SCREEN_WORK_OUT_WIDTHS_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)

	//SET_TEXT_WRAP(fontLeft,fontRight)
	
	FLOAT width = 0.0
	
	/*
	INT iTitleSubelements
	END_SCREEN_TITLE_SUB_ELEMENT_TYPE titleSubElementType[MAX_END_SCREEN_TITLE_SUB_ELEMENTS]
	TEXT_LABEL_63 titleSubstrings[MAX_END_SCREEN_TITLE_SUBSTRINGS]
	INT iTitleSubInts[MAX_END_SCREEN_TITLE_SUBINTEGERS]
	*/
	
	
	SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
	SET_TEXT_SCALE(1.0, Get_PT_scale(16))
	
	IF esd.iTitleSubelements = 0
		IF  esd.titleIsUserName
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")	
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.title)
			width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
			
			PRINTLN("END_SCREEN_WORK_OUT_WIDTHS_TITLE_UPDATE: User name title(",esd.title,") width ",width)
		ELSE
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(esd.title)
			width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
			
			PRINTLN("END_SCREEN_WORK_OUT_WIDTHS_TITLE_UPDATE: Regular no substring title width(",esd.title,") width ",width)
		ENDIF
	ELSE
		BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(esd.title)	
		INT iIntCount = 0
		INT iStrCount = 0
		INT i = 0
		REPEAT esd.iTitleSubelements i
			SWITCH esd.titleSubElementType[i]
				CASE ESTSET_INT
					ADD_TEXT_COMPONENT_INTEGER(esd.iTitleSubInts[iIntCount])
					++iIntCount
					BREAK
				CASE ESTSET_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(esd.titleSubstrings[iStrCount] )
					++iStrCount
					BREAK
				CASE ESTSET_LITERAL_STRING
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.titleSubstrings[iStrCount])
					++iStrCount
					BREAK
			ENDSWITCH
		ENDREPEAT
		width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
		
		PRINTLN("END_SCREEN_WORK_OUT_WIDTHS_TITLE_UPDATE: Substr width (",esd.title,") with substr ",iStrCount," and subints ",iIntCount," width ",width)
	ENDIF
	
	
	IF width > (ESC_HALF_WIDTH*2 - ESC_INDENT*2)
		esd.fTrueHalfWidth = (width/2.0) + ESC_INDENT*2
	ENDIF
	
	esd.fTrueHalfWidth *= GET_ASPECT_RATIO_MODIFIER()
	
	//check the width of all the elements
	//check the width of the completion line
	
	
ENDPROC


PROC PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)

ENDPROC

PROC PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE(STRING tl, FLOAT x, FLOAT y,BOOL c = FALSE, BOOL isPlayerName = FALSE)

	SET_TEXT_CENTRE(c)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()

	IF isPlayerName
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")	
	ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	ELSE
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(tl)
	ENDIF
	//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
	//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
	END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
ENDPROC


PROC PRIVATE_RENDER_ENDSCREEN_VALUE_TITLE_UPDATE(INT valueA, INT valueB, 
									FLOAT x, FLOAT y, 
									STRING base,
									END_SCREEN_ELEMENT_FORMATTING format)
												
	HUD_COLOURS hucu = HUD_COLOUR_WHITE
	
	//set the string value if needed
	SET_TEXT_CENTRE(FALSE)
	SET_TEXT_FONT(FONT_STANDARD)
	PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
	
	//add any needed sub values
	
	
	//do backing effects 
	FLOAT width = 0.0
	FLOAT xw = 8*	ESC_X_PIXEL
	FLOAT yh = 16*	ESC_Y_PIXEL
	INT r = 93
	INT g = 182
	INT b = 229
	
	INT a
	GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE_OFFICIAL,r,g,b,a)

	IF format = ESEF_DOLLAR_VALUE_REDBACK
		r = 194
		g = 80
		b = 80
	ENDIF
	
	SWITCH format
		//CASE ESEF_DOLLAR_VALUE
		CASE ESEF_DOLLAR_VALUE_REDBACK  
		CASE ESEF_DOLLAR_VALUE_BLUEBACK
			SET_TEXT_SCALE(1.0, Get_PT_scale(18))//CUSTOM_MENU_TEXT_SCALE_Y*1.2857)
			SET_TEXT_FONT(FONT_CONDENSED)
			IF valueA < 0
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ESMINDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(-1*valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
				width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(FALSE)
			ELSE
				BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ESDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
				width = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(FALSE)
			ENDIF
		
			width -= (width % ESC_X_PIXEL) //trim the sub pixel amount
			
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Left",
			x-(width), //+ (ESC_X_PIXEL*4),
			y+yh*0.6 + ESC_Y_PIXEL*2,
			xw,yh,
			0.0,
			r,g,b,255)
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Centre",
			x-width*0.5 - (ESC_X_PIXEL*2),
			y+yh*0.6 + ESC_Y_PIXEL*2,
			width - xw*0.5,yh,
			0.0,
			r,g,b,255)
			
			DRAW_SPRITE("CommonMenu", "BettingBox_Right",
			x- (ESC_X_PIXEL*4),
			y+yh*0.6+ ESC_Y_PIXEL*2,
			xw,yh,
			0.0,
			r,g,b,255)		
			
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		BREAK	
	ENDSWITCH
	
	SET_COLOUR_OF_NEXT_TEXT_COMPONENT(hucu)
	//start the string
	SWITCH format 
		CASE ESEF_RAW_PERCENT
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("PERCENTAGE")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			
			BREAK
		CASE ESEF_RAW_INTEGER
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_NUM")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			
			BREAK
		CASE ESEF_FRACTION
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_TWO_NUM")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		
		CASE ESEF_DOLLAR_VALUE_REDBACK
		CASE ESEF_DOLLAR_VALUE_BLUEBACK
			SET_TEXT_SCALE(1.0, Get_PT_scale(18))//CUSTOM_MENU_TEXT_SCALE_Y*1.2857)
		FALLTHRU
		CASE ESEF_DOLLAR_VALUE
			
			IF valueA < 0
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ESMINDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(-1*valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
			ELSE
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("ESDOLLA")
				ADD_TEXT_COMPONENT_FORMATTED_INTEGER(valueA, INTEGER_FORMAT_COMMA_SEPARATORS)
			ENDIF
			
			BREAK
		CASE ESEF_RAW_STRING
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			BREAK
		CASE ESEF_RAW_USERNAME
		CASE ESEF_RAW_USERNAME_WITH_RANK
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(base)
			BREAK
		CASE ESEF_TIME_H_M_S
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_HOURS | TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS)
			BREAK
		CASE ESEF_TIME_M_S
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS )
			BREAK
		
		CASE ESEF_TIME_M_S_MS
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS | TIME_FORMAT_MILLISECONDS | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			BREAK			
		CASE ESEF_TIME_M_S_MS_WITH_PERIOD //1506472
			SET_TEXT_FONT(FONT_STYLE_FIXED_WIDTH_NUMBERS)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_TIME(ValueA, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS | TIME_FORMAT_MILLISECONDS | TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			BREAK
		CASE ESEF_DISTANCE_VALUE_METERS
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("AHD_DIST")
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			BREAK
		CASE ESEF_SINGLE_NUMBER_SUB
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		CASE ESEF_DOUBLE_NUMBER_SUB_LEFT
		CASE ESEF_DOUBLE_NUMBER_SUB
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueA)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
		CASE ESEF_LEFT_AND_RIGHT_SUBSTRINGS
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(base)
			ADD_TEXT_COMPONENT_INTEGER(valueB)
			
			BREAK
			
		
			
		
		//CASE ESEF_LEFT_SIDE_INT_SUBSTRING //this only applys a substring to name
	ENDSWITCH
	
	IF format != ESEF_LEFT_SIDE_INT_SUBSTRING
		IF format = ESEF_DOLLAR_VALUE_REDBACK
		OR format = ESEF_DOLLAR_VALUE_BLUEBACK
			END_TEXT_COMMAND_DISPLAY_TEXT(x- ESC_X_PIXEL*4, y)
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		ELSE
			END_TEXT_COMMAND_DISPLAY_TEXT(x, y)
		ENDIF
	ENDIF

ENDPROC

PROC PRIVATE_RENDER_ENSCREEN_ELEMENT_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd, INT iNth, FLOAT yat, FLOAT fontLeft,FLOAT fontRight,INT EndScreenAlpha)
	
	//END_SCREEN_ELEMENT_FORMATTING 
	//TEXT_LABEL ElementName[iNth]
	//TEXT_LABEL ElementText[iNth]
	//INT ElementValA[iNth]
	//INT ElementValB[iNth]
	//END_SCREEN_CHECK_MARK_STATUS ElementCheck[iNth]
	//BOOL ElementInvalidation[iNth]
		//IF esd.ElementInvalidation[iNth]
		//	SET_TEXT_COLOUR(10,10,10,255)
		//ELSE
		INT txt_R,txt_G,txt_B,txt_A
		GET_HUD_COLOUR(HUD_COLOUR_WHITE,txt_R,txt_G,txt_B,txt_A)
		SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
		//ENDIF
		
		
		SET_TEXT_WRAP(fontLeft,fontRight)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		
		//render the name
		SET_TEXT_CENTRE(FALSE)
		
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		
		//SET_TEXT_SCALE(1.0, Get_PT_scale(2))//CUSTOM_MENU_TEXT_SCALE_Y
		//SET_TEXT_FONT(FONT_ROCKSTAR_TAG)
		IF esd.ElementIsPlayerName[iNth]
		
			//SET_TEXT_SCALE(1.0, Get_PT_scale(20))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_FONT(FONT_STANDARD)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.ElementName[iNth])
		ELSE
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_FONT(FONT_STANDARD)
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(esd.ElementName[iNth])
			IF esd.ElementFormat[iNth] = ESEF_LEFT_AND_RIGHT_SUBSTRINGS
			OR esd.ElementFormat[iNth] = ESEF_LEFT_SIDE_INT_SUBSTRING
				ADD_TEXT_COMPONENT_INTEGER(esd.ElementValA[iNth])
			ENDIF
		ENDIF
		//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
		//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
		END_TEXT_COMMAND_DISPLAY_TEXT(fontleft, yat)

		FLOAT widthPlus = 0.0//BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT
		IF esd.ElementIsPlayerName[iNth]
	
			//SET_TEXT_SCALE(1.0, Get_PT_scale(20))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_FONT(FONT_STANDARD)
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.ElementName[iNth])
		ELSE
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			SET_TEXT_FONT(FONT_STANDARD)
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(esd.ElementName[iNth])
			IF esd.ElementFormat[iNth] = ESEF_LEFT_AND_RIGHT_SUBSTRINGS
			OR esd.ElementFormat[iNth] = ESEF_LEFT_SIDE_INT_SUBSTRING
				ADD_TEXT_COMPONENT_INTEGER(esd.ElementValA[iNth])
			ENDIF
		ENDIF
		//SET_COLOUR_OF_NEXT_TEXT_COMPONENT(HUD_COLOUR_BLACK)
		//ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl)
		widthPlus += END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
		
		
				//GET_PLAYER_RANK()
		IF esd.ElementFormat[iNth] = ESEF_RAW_USERNAME_WITH_RANK
			IF esd.ElementRankLookup[iNth] != NULL			
				GAMER_HANDLE aGamer = GET_GAMER_HANDLE_PLAYER(esd.ElementRankLookup[iNth])						
				DRAW_CREW_TAG_GAMER_COPY(aGamer,
										fontLeft+widthPlus,//..+ESC_X_PIXEL*14,
										yat,//-ESC_Y_PIXEL*14,
										1.0)
			ENDIF
		ENDIF
		
		
		
		
		
		
		
		
		
		
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		SET_TEXT_FONT(FONT_STANDARD)
		
		
		

		FLOAT TRUEright = fontRight
		FLOAT ficonw
		FLOAT ficonh 
		//FLOAT ficonwh 
		//FLOAT ficonhh 
		
		
		
		SWITCH esd.ElementCheck[iNth]
			CASE ESCM_NO_MARK
				BREAK
			CASE ESCM_UNCHECKED
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_EMPTY,
					FALSE,TRUE,
					ficonw,ficonh)
				
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
					
				//render the unchecked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_EMPTY,FALSE), 
				(fontRight)- ESC_INDENT, yat +PIXEL_Y(ESC_LINE_Y_GAP)+ (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, 255)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
			CASE ESCM_CHECKED
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_TICK,
					FALSE,TRUE,
					ficonw,ficonh)
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
				//render the checked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_TICK,FALSE), 
				(fontRight)- ESC_INDENT , yat +PIXEL_Y(ESC_LINE_Y_GAP)+ (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, 255)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
			CASE ESCM_INVALIDATED
			
				GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_CROSS,
					FALSE,TRUE,
					ficonw,ficonh)
					//ficonwh = ficonw*0.5
					//ficonhh = ficonh*0.5
				//render the checked box
				DRAW_SPRITE("CommonMenu", GET_MENU_ICON_TEXTURE(MENU_ICON_BOX_CROSS,FALSE), 
				(fontRight)- ESC_INDENT , yat +PIXEL_Y(ESC_LINE_Y_GAP) + (0.25*ficonh), 
				ficonw, ficonh, 
				0, 
				255, 255, 255, 255)

				TRUEright -=  ficonw*0.38 + ESC_INDENT//width of check mark
				BREAK
				
		ENDSWITCH
		
		
		IF esd.ElementFormat[iNth] = ESEF_NAME_ONLY
			EXIT
		ENDIF
		//render the value
		
		
		IF esd.ElementFormat[iNth] = ESEF_DOUBLE_NUMBER_SUB_LEFT
			SET_TEXT_JUSTIFICATION(FONT_LEFT)
		ELSE
			SET_TEXT_JUSTIFICATION(FONT_RIGHT)
		ENDIF
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
		
		IF esd.ElementFormat[iNth] = ESEF_DOLLAR_VALUE_BLUEBACK
		OR esd.ElementFormat[iNth] = ESEF_DOLLAR_VALUE_REDBACK
			SET_TEXT_WRAP(fontLeft,TRUEright - ESC_X_PIXEL*3)
		ELSE
			SET_TEXT_WRAP(fontLeft,TRUEright + ESC_X_PIXEL*2)
		ENDIF
		
		

		
		
	
	
		PRIVATE_RENDER_ENDSCREEN_VALUE_TITLE_UPDATE(
		esd.ElementValA[iNth], esd.ElementValB[iNth], 
		fontRight, yat, 
		esd.ElementText[iNth],
		esd.ElementFormat[iNth])
		//esd.ElementInvalidation[iNth])

		//PRINTLN("PRIVATE_RENDER_ENSCREEN_ELEMENT_TITLE_UPDATE: <eleminvalidation> invalidation for ", iNth, " is ", esd.ElementInvalidation[iNth])
		
	
ENDPROC


PROC FORCE_ENDSCREEN_ANIM_OUT_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)
	esd.bShowSkipperPrompt = FALSE
	esd.bHoldOnEnd = FALSE
	esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
ENDPROC


/// PURPOSE:
//assumes that the scaleform is already loaded
//call at your own risk    
/// PARAMS:
///    esd - 
///    
///    
PROC RAW_ENDSCREEN_DRAW_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd, FLOAT skipperMult = 1.0)
	
	
	//set Alpha
	Int EndScreenAlpha 	= round(esd.fFadeOutMult * 255)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(esd.splash)	
		IF esd.fTitleUpMult > 0.10
			IF !esd.bStartedFailformAnim
				BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "TRANSITION_UP")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.15)
				END_SCALEFORM_MOVIE_METHOD()
				esd.bStartedFailformAnim = TRUE
			ENDIF
		ENDIF
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		//DRAW_SCALEFORM_MOVIE(esd.splash,0.5,0.775-0.275*esd.fTitleUpMult,1.0,1.0,255,255,255,255)//75-25
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(esd.splash,255,255,255,255)
	ENDIF
	//draw the background box
	PRINTLN("End screen blend in ", esd.fBlendInProgress, " sum ",esd.fBlendInTargetYSum, " tmult =  ", esd.fTitleUpMult," M alpha: ",EndScreenAlpha)
	FLOAT currentH = (esd.fBlendInTargetYSum*esd.fBlendInProgress)*(1.0 - esd.fScrollOutMult)
	FLOAT socialH = 0.0
	
	
	IF esd.bVoteModeEnabled
		socialH = ((ESC_SOCIAL_BULLSHIT_TAB_Y + PIXEL_Y(ESC_LINE_Y_GAP*2))*esd.fVoteBarProg)*(1.0 - esd.fScrollOutMult)
		currentH +=  socialH
	ENDIF
	
	IF esd.fScrollOutMult != 0.0
		FLOAT min =  PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)//+ PIXEL_Y(ESC_LINE_Y_GAP*2) // title box 
		IF currentH < min
		 	currentH = min
		ENDIF
	ELSE
		FLOAT min =  PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) - PIXEL_Y(ESC_LINE_Y_GAP)// Completion box 
		IF esd.fTitleUpMult >= 0.975
			IF currentH < min
			 	currentH = min
			ENDIF
		ENDIF
	ENDIF
	
	FLOAT yat 	= ESC_Y_POSITION
	
	IF esd.bCenterMessageMode
		yat = 0.5
	ENDIF
	
	FLOAT w = esd.fTrueHalfWidth *2
	
	FLOAT fontLeft 	= (0.499 - esd.fTrueHalfWidth) + ESC_INDENT // request both moved slightly to the left
	FLOAT fontRight = (0.499 + esd.fTrueHalfWidth) - ESC_INDENT // request both moved slightly to the left
	
	
	//show the "press button to continue prompt if needed
	IF esd.bShowSkipperPrompt OR esd.bVoteModeEnabled
		IF (esd.iEndScreenDisplayFinish-(7500*skipperMult)) < esd.IGameTimerReplacement	//normal mode
		OR (esd.bVoteModeEnabled AND esd.fBlendInProgress > 0.95 AND (esd.iEndScreenDisplayFinish-(10000)) < esd.IGameTimerReplacement) //vote mode
			IF esd.bVoteModeEnabled
				//timing
				
				IF esd.iVoteTimer < 0// init
					esd.iVoteTimer *= -1		
					esd.iVoteTimer = esd.IGameTimerReplacement + esd.iVoteTimer
				ENDIf
				IF esd.iVoteTimer > 0
					IF (esd.iVoteTimer - esd.IGameTimerReplacement) > 0
						//DRAW_GENERIC_TIMER(esd.iVoteTimer - esd.IGameTimerReplacement ,"TIMER_TIME")
						BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "OVERRIDE_RESPAWN_TEXT")
					        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
							ADD_TEXT_COMPONENT_SUBSTRING_TIME(esd.iVoteTimer - esd.IGameTimerReplacement, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS )
							END_TEXT_COMMAND_SCALEFORM_STRING()
				   		END_SCALEFORM_MOVIE_METHOD()
					
					ELSE
						esd.iVoteTimer = 0
						esd.eVoteResult = ESSRV_TIMEOUT
						esd.bVoteModeEnabled = FALSE
						esd.bHoldOnEnd = FALSE
						esd.bShowSkipperPrompt = FALSE
						esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
						esd.iVoteTimer = 0
					ENDIF
				ENDIF
				
			
			
				//scroll out effect
				IF esd.fVoteBarProg < 1.0
					esd.fVoteBarProg += 0.0 +@ (1.0/0.166) 
					IF esd.fVoteBarProg > 1.0
						esd.fVoteBarProg = 1.0
					ENDIF
				ENDIF
			ENDIF
			
		
			//SET_TEXT_WRAP(fontLeft,fontRight)
			//SET_TEXT_JUSTIFICATION(FONT_RIGHT)
			//SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y
			//PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("MTPHPERCONT", fontLeft, yat+currentH)//
			
			IF IS_SCREEN_FADED_OUT()
				HIDE_LOADING_ON_FADE_THIS_FRAME()
			ENDIF
			IF esd.button != NULL AND ( esd.fScrollOutMult < 0.10)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(esd.button,255,255,255,EndScreenAlpha)
			ENDIF
			
			
			IF esd.bVoteModeEnabled
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_ALTERNATE)
			
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME) 
                HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_THUMBS_UP	
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_THUMBS_DOWN
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF
				
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					esd.bVoteModeEnabled = FALSE
					esd.bHoldOnEnd = FALSE
					esd.bShowSkipperPrompt = FALSE
					esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
					esd.eVoteResult = ESSRV_ABSTAIN
					esd.iVoteTimer = 0
					PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
				ENDIF

			ELSE
				IF esd.bShowSkipperPrompt
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME) 
                    HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_LIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_HEAVY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_MELEE_ATTACK_ALTERNATE)
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SKIP_CUTSCENE)
						PLAY_SOUND_FRONTEND(-1,"CONTINUE","HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
						esd.bShowSkipperPrompt = FALSE
						esd.bHoldOnEnd = FALSE
						esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement+500
						PLAY_SOUND_FRONTEND (-1,  "continue", "HUD_FRONTEND_DEFAULT_SOUNDSET", FALSE)
					ENDIF
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
	

	
	PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
	INT r,g,b,a
	GET_HUD_COLOUR(HUD_COLOUR_PAUSE_BG,r,g,b,a)
//	a = round(255 * 0.73) //73% black
	DRAW_RECT(0.5,
			  yat+(currentH*0.5),
			  w,
			  currentH,
			  r,g,b,round(esd.fFadeOutMult*a))
	
	
//==================================================== WHITE LINE ===========================================================	

	IF currentH >= PIXEL_Y(ESC_LINE_Y_GAP)
		//draw whiteline top
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		DRAW_RECT(0.5,
				  yat-(PIXEL_Y(ESC_LINE_Y_GAP-0.5)-ESC_Y_PIXEL),
				  w,
				  PIXEL_Y(ESC_LINE_Y_GAP),
				  255,255,255,EndScreenAlpha)
	ELSE	
		EXIT
	ENDIF
	yat += PIXEL_Y(ESC_LINE_Y_GAP)
	
//==================================================== TITLE BOX ===========================================================	

	//draw the title string
	INT txt_R,txt_G,txt_B,txt_A
	GET_HUD_COLOUR(HUD_COLOUR_WHITE,txt_R,txt_G,txt_B,txt_A)
	SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
	SET_TEXT_WRAP(fontLeft,fontRight)
	SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
	SET_TEXT_SCALE(1.0, Get_PT_scale(16)) //17.36
	
							//	IS THE USERNAME BEING USED
							
	IF esd.titleIsUserName
		PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE(esd.title,0.5,yat+ESC_Y_PIXEL,TRUE,TRUE)//+PIXEL_Y(ESC_LINE_Y_GAP)
	ELSE
							//	  DISPLAY MISSION TITLE
							
		IF esd.iTitleSubelements = 0
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE(esd.title,0.5,yat+ESC_Y_PIXEL,TRUE)//+PIXEL_Y(ESC_LINE_Y_GAP)
		ELSE
		
							//	   TITLE WITH ELEMENTS
			SET_TEXT_CENTRE(TRUE)
			SET_TEXT_FONT(FONT_STANDARD)
			PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT(esd.title)
			INT iIntCount = 0
			INT iStrCount = 0
			INT i = 0
			REPEAT esd.iTitleSubelements i
				SWITCH esd.titleSubElementType[i]
					CASE ESTSET_INT
						ADD_TEXT_COMPONENT_INTEGER(esd.iTitleSubInts[iIntCount])
						++iIntCount
						BREAK
					CASE ESTSET_STRING
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(esd.titleSubstrings[iStrCount] )
						++iStrCount
						BREAK
					CASE ESTSET_LITERAL_STRING
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(esd.titleSubstrings[iStrCount])
						++iStrCount
						BREAK
				ENDSWITCH
			ENDREPEAT
			END_TEXT_COMMAND_DISPLAY_TEXT(0.5,yat+ESC_Y_PIXEL)//yat+PIXEL_Y(1.85))
		ENDIF
	ENDIF
	
//===========================================================================================================================
//==================================================== WHITE LINE ===========================================================	
	yat += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)-(PIXEL_Y(ESC_LINE_Y_GAP*2))
	IF currentH >= PIXEL_Y(ESC_LINE_Y_GAP)
		//draw whiteline top
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		DRAW_RECT(0.5,
				  yat+(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
				  w,
				  PIXEL_Y(ESC_LINE_Y_GAP),
				  255,255,255,EndScreenAlpha)
	ELSE	
		EXIT
	ENDIF
//===========================================================================================================================	
//=================================================== ELEMENTS BOX ==========================================================
//	yat += PIXEL_Y(ESC_LINE_Y_GAP*0.5)
	
	IF esd.iElements > 0 
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*0.25) //6.25 pxls
	ENDIF
	
	
	INT i = 0
	REPEAT esd.iElements i
	
		IF currentH >= (yat-ESC_Y_POSITION)
			//draw each of the content lines
			 PRIVATE_RENDER_ENSCREEN_ELEMENT_TITLE_UPDATE(esd, i, yat+PIXEL_Y(ESC_LINE_Y_GAP), fontLeft, fontRight,EndScreenAlpha)
		ELSE
			EXIT
		ENDIF
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE)
	ENDREPEAT
	
	
	IF esd.iElements  > 0
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE*0.25)//6.25 pxls
//===========================================================================================================================			
//==================================================== WHITE LINE ===========================================================	
		IF currentH >= (yat-ESC_Y_POSITION)
			yat += PIXEL_Y(ESC_LINE_Y_GAP)//y at the bottom of the white line
			//draw whiteline before end
			PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
			DRAW_RECT(0.5,
					  yat-PIXEL_Y(0.5),
					  w,
					  PIXEL_Y(ESC_LINE_Y_GAP),
					  255,255,255,EndScreenAlpha)
		ELSE	
			EXIT
		ENDIF
			
		
	ENDIF
//===========================================================================================================================
//================================================= COMPLETION BOX ==========================================================
	IF esd.bShowCompletion
	
		SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
	
		//TODO Draw completion line here
		SET_TEXT_WRAP(fontLeft,fontRight)
		SET_TEXT_JUSTIFICATION(FONT_LEFT)
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
		PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE(esd.CompletionResultString,fontLeft,yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
		
		//and percentage if needed

		SET_TEXT_WRAP(fontLeft,fontRight)
		SET_TEXT_JUSTIFICATION(FONT_RIGHT)
		SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
		SET_TEXT_CENTRE(FALSE)
		SET_TEXT_FONT(FONT_STANDARD)
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		
		FLOAT medalRight = fontRight
		
		TEXT_LABEL medalsprite = "Common_Medal"
		
		FLOAT ficonw
		FLOAT ficonh
		GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_BOX_EMPTY,
			FALSE,TRUE,
			ficonw,ficonh)
		
		//esd.CompletionMedalState = ESMS_GOLD
		SWITCH esd.CompletionMedalState
			//ESMS_NO_MEDAL,
			CASE ESMS_BRONZE
				medalRight -= ficonw*0.38 + ESC_INDENT//ESC_X_PIXEL*(MEDAL_PIX_SIDE/1.5)
				//medalsprite = "medal_bronze"
				BREAK
			CASE ESMS_SILVER
				medalRight -= ficonw*0.38 + ESC_INDENT//ESC_X_PIXEL*(MEDAL_PIX_SIDE/1.5)
				BREAK
			CASE ESMS_GOLD
				medalRight -= ficonw*0.38 + ESC_INDENT//ESC_X_PIXEL*(MEDAL_PIX_SIDE/1.5)
				//medalsprite = "medal_gold"
				BREAK
		
		ENDSWITCH
		
		
		
		
		SET_TEXT_WRAP(fontLeft,medalRight)
		
//	INT iCompletionValueA
//	INT iCompletionValueB
//	END_SCREEN_COMPLETION_TYPE CompletionType
		
		SWITCH esd.CompletionType
			
			
			CASE ESC_PERCENTAGE_COMPLETION
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("PERCENTAGE")
				ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
				END_TEXT_COMMAND_DISPLAY_TEXT(fontLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
				BREAK
				
			CASE ESC_FRACTION_COMPLETION
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("FO_TWO_NUM")
				ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
				ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueB)
				END_TEXT_COMMAND_DISPLAY_TEXT(fontLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
				BREAK
			
			CASE ESC_XP_COMPLETION
				BEGIN_TEXT_COMMAND_DISPLAY_TEXT("MTPHPER_XPNO")
				ADD_TEXT_COMPONENT_INTEGER(esd.iCompletionValueA)
				END_TEXT_COMMAND_DISPLAY_TEXT(fontLeft, yat+PIXEL_Y(ESC_LINE_Y_GAP*2))//4pxls after white line
				BREAK
		ENDSWITCH
		
		
		IF esd.CompletionMedalState != ESMS_NO_MEDAL
			INT mr = 255
			INT mg = 255
			INT mb = 255
			INT ma = EndScreenAlpha
			SWITCH esd.CompletionMedalState
				CASE ESMS_BRONZE
					GET_HUD_COLOUR(HUD_COLOUR_BRONZE,mr,mg,mb,ma)
					BREAK
				CASE ESMS_GOLD
					GET_HUD_COLOUR(HUD_COLOUR_GOLD,mr,mg,mb,ma)
					BREAK
				CASE ESMS_SILVER
					GET_HUD_COLOUR(HUD_COLOUR_SILVER,mr,mg,mb,ma)
					BREAK
			ENDSWITCH
		
		
			FLOAT fmedalw = ESC_X_PIXEL*MEDAL_PIX_SIDE*2//twice the size for new texture
			FLOAT fmedalh = ESC_Y_PIXEL*MEDAL_PIX_SIDE*2//twice the size for new texture
			//FLOAT fmedalwh = ESC_X_PIXEL*(MEDAL_PIX_SIDE/3)
			FLOAT fmedalhh = ESC_Y_PIXEL*(MEDAL_PIX_SIDE/3)
			DRAW_SPRITE("CommonMenu", medalsprite, 
			(fontRight)- ESC_INDENT,//(fontRight) - fmedalwh*1.1, 
			yat + (PIXEL_Y(9)) + fmedalhh, //fmedalhh+
			fmedalw, fmedalh, 
			0, 
			mr, mg, mb, EndScreenAlpha)		
		ENDIF
		
//===========================================================================================================================
//==================================================== WHITE LINE ===========================================================	
		yat += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER)-(PIXEL_Y(ESC_LINE_Y_GAP))
		IF currentH >= (yat-ESC_Y_POSITION)
			//draw whiteline before end
			PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
			DRAW_RECT(0.5,
					  yat,//-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
					  w,
					  PIXEL_Y(ESC_LINE_Y_GAP),
					  255,255,255,EndScreenAlpha)

		ELSE
			EXIT 
		ENDIF
	ENDIF
	

//===========================================================================================================================
//===================================================== VOTE BOX ============================================================
	
	IF esd.bVoteModeEnabled AND socialH > 0.0
		FLOAT remainder = socialH
		
		
		remainder -= socialH*0.03
		yat += socialH*0.03
		//[ES_HELP_SOC3:MISHSTA]
		//Like / Dislike
		
		//draw the title string
		IF esd.fVoteBarProg > 0.08
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC3",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
			//draw thumbs 
			FLOAT fThumbw = ESC_X_PIXEL * MEDAL_PIX_SIDE * 3
			FLOAT fThumbh = ESC_Y_PIXEL * MEDAL_PIX_SIDE * 3
			FLOAT fThumbhh = ESC_Y_PIXEL* (MEDAL_PIX_SIDE/3)
			
			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ES_HELP_SOC3")
			FLOAT wx = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
			
			
			//left thumb
			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb",	
			0.5 - (wx*0.5) - (fThumbw*0.3),
			yat + (PIXEL_Y(9)) + fThumbhh,
			fThumbw, fThumbh, 0,255, 255, 255, EndScreenAlpha)
			
			//right thumb
			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb", 	
			0.5+ (wx*0.5) + (fThumbw*0.3),	
			yat + (PIXEL_Y(9)) + fThumbhh*2,
			fThumbw, fThumbh, 180,255, 255, 255, EndScreenAlpha)
			
		ENDIF
		
		
		remainder -= socialH*0.23
		yat += socialH*0.22
		IF esd.fVoteBarProg > 0.3
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(12))//CUSTOM_MENU_TEXT_SCALE_Y)
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOCA",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
		ENDIF
		
		remainder -= socialH*0.15
		yat += socialH*0.15
		IF esd.fVoteBarProg > 0.4
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(12))//CUSTOM_MENU_TEXT_SCALE_Y)
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOCB",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
		ENDIF
		
		remainder -= socialH*0.15
		yat += socialH*0.15
		IF esd.fVoteBarProg > 0.5
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(12))//CUSTOM_MENU_TEXT_SCALE_Y)
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOCC",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
		ENDIF
		
		remainder -= socialH*0.15
		yat += socialH*0.15
		IF esd.fVoteBarProg > 0.6
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(12))//CUSTOM_MENU_TEXT_SCALE_Y)
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOCD",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
		ENDIF
		

		
		IF remainder > 0
			yat += remainder
		ENDIF
		
		
		
		
		
		
		
		
		/*
		remainder -= socialH*0.08
		yat += socialH*0.08
		//[ES_HELP_SOC1:MISHSTA]
		//Let the ~y~Social Club Community~s~ know if you
		
		//draw the title string
		IF esd.fVoteBarProg > 0.08
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC1",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
		ENDIF
		
		
		remainder -= socialH*0.2
		yat += socialH*0.2
		//[ES_HELP_SOC2:MISHSTA]
		//enjoyed this race. Vote here:
		
		//draw the title string
		IF esd.fVoteBarProg > 0.28
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(14))//CUSTOM_MENU_TEXT_SCALE_Y)
			if 	 esd.eVoteGameType = ESMGT_RACE
				PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC2",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
			elif esd.eVoteGameType = ESMGT_DEATHMATCH
				PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC2b",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
			elif esd.eVoteGameType = ESMGT_MISSION
				PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC2c",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
			endif
		ENDIF
		
		remainder -= socialH*0.3
		yat += socialH*0.3
		//[ES_HELP_SOC3:MISHSTA]
		//Like / Dislike
		
		//draw the title string
		IF esd.fVoteBarProg > 0.58
			SET_TEXT_COLOUR(txt_R,txt_G,txt_B,EndScreenAlpha)
			SET_TEXT_WRAP(fontLeft,fontRight)
			SET_TEXT_JUSTIFICATION(FONT_CENTRE) 
			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
			PRIVATE_RENDER_ENDSCREEN_LABEL_TITLE_UPDATE("ES_HELP_SOC3",0.5,yat+PIXEL_Y(ESC_LINE_Y_GAP*2),TRUE)
			//draw thumbs 
			FLOAT fThumbw = ESC_X_PIXEL * MEDAL_PIX_SIDE * 3
			FLOAT fThumbh = ESC_Y_PIXEL * MEDAL_PIX_SIDE * 3
			FLOAT fThumbhh = ESC_Y_PIXEL* (MEDAL_PIX_SIDE/3)
			
			SET_TEXT_SCALE(1.0, Get_PT_scale(17))
			BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("ES_HELP_SOC3")
			FLOAT wx = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
			
			
			//left thumb
			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb",	
			0.5 - (wx*0.5) - (fThumbw*0.6),
			yat + (PIXEL_Y(9)) + fThumbhh,
			fThumbw, fThumbh, 0,255, 255, 255, EndScreenAlpha)
			
			//right thumb
			DRAW_SPRITE("MPLeaderboard", "Leaderboard_thumb", 	
			0.5+ (wx*0.5) + (fThumbw*0.6),	
			yat + (PIXEL_Y(9)) + fThumbhh,
			fThumbw, fThumbh, 180,255, 255, 255, EndScreenAlpha)
			
		ENDIF
		
		IF remainder > 0
			yat += remainder
		ENDIF
		*/
		
	ENDIF
//===========================================================================================================================
//==================================================== WHITE LINE ===========================================================
	IF currentH >= (yat-ESC_Y_POSITION)
		//draw whiteline before end
		PRIVATE_ENDSCREEN_DRAW_ORDER_TITLE_UPDATE()
		DRAW_RECT(0.5,
				  yat, //-(PIXEL_Y(ESC_LINE_Y_GAP*0.5)),
				  w,
				  PIXEL_Y(ESC_LINE_Y_GAP),
				  255,255,255,EndScreenAlpha)
	ELSE
		EXIT 
	ENDIF
//===========================================================================================================================	

ENDPROC
Proc INIT_ENDSCREEN_VALUES_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)

	esd.fFadeOutMult 			= 1.0
	esd.iFadeOutSplashTimer 	= 0	
	esd.fVoteBarProg 			= 0.0	
	esd.iEndScreenDisplayFinish = 0
	esd.fTitleUpMult 			= 0.0	
	esd.fScrollOutMult 			= 0.0
	esd.fBlendInProgress 		= 0.0
	esd.fBlendInTargetYSum 		= 0.0 //the precalculated target height
	esd.bTransitionOutCalled	= FALSE
	esd.bDoneFlash 				= FALSE
	esd.IGameTimerReplacement 	= 0 
	esd.bNoLoadingScreenEnabled = FALSE
	 
	esd.fTrueHalfWidth = ESC_HALF_WIDTH
	 
	esd.bStartedFailformAnim = FALSE
ENDPROC
PROC RESET_ENDSCREEN_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)
	//SCALEFORM_INDEX splash
	INIT_ENDSCREEN_VALUES_TITLE_UPDATE(esd)
	
	esd.iVoteTimer 				= 0
	esd.iTitleSubelements 		= 0
	esd.iElements 				= 0
	esd.bVoteModeEnabled 		= FALSE	
	esd.eVoteResult 			= ESSRV_UNSET
		
ENDPROC

/// PURPOSE:
///    CALL THIS BEFORE PREPARE IF YOU USE IT
PROC ENDSCREEN_CONFIGURE_VOTE_PANEL_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,INT iVoteDurationSeconds,END_SCREEN_MP_GAME_TYPE gameType = ESMGT_RACE)
	
	esd.bVoteModeEnabled 	= TRUE
	esd.fVoteBarProg 		= 0.0
	esd.eVoteResult 		= ESSRV_UNSET
	esd.iVoteTimer 			= -iVoteDurationSeconds*1000
	esd.bHoldOnEnd 			= TRUE
	esd.eVoteGameType 		= gameType
ENDPROC

FUNC END_SCREEN_SOCIAL_RETURN_VALUE GET_ENDSCREEN_SOCIAL_RETURN_VALUE_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)
	RETURN esd.eVoteResult
ENDFUNC

//populate
//prepare
//render
//shutdown


/// PURPOSE:
///    render the data in the heist endscreen structure
/// PARAMS:
///    esd - data handle
///    bBlock - if true will block and re-render until done
///    	otherwise will return state until done
///   
/// RETURNS:
///    True when render complete
FUNC BOOL RENDER_ENDSCREEN_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd, BOOL bBlockUntilDone = FALSE,FLOAT skipperMultiplier = 1.0)
	
	// PT Fix 1558760
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PED_DEAD_OR_DYING(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			RETURN TRUE // bail out
		ENDIF
		
		IF IS_PED_BEING_ARRESTED(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			RETURN TRUE // bail out
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
				CPRINTLN(DEBUG_MISSION_STATS, "RENDER_ENDSCREEN_TITLE_UPDATE: Clearing player special ability as end screen is displaying.")
				SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
			ENDIF
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
	ENDIF	
	
	IF NOT esd.bNoLoadingScreenEnabled
		IF IS_SCREEN_FADED_OUT()
		OR IS_SCREEN_FADING_OUT()
			SET_NO_LOADING_SCREEN(TRUE)
			esd.bNoLoadingScreenEnabled = TRUE
		ENDIF
	ENDIF
	
	//DRAW_GENERIC_TIMER
	//DRAW_TIMER_HUD
	
	
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)

	
	//Stop the comms controller queue timers from ticking while the end screen is drawing.
	g_bPauseCommsQueuesThisFrame = TRUE
	
	IF NOT esd.bDoneFlash
		SWITCH GET_PLAYER_PED_ENUM(GET_PLAYER_PED(GET_PLAYER_INDEX()))
			CASE CHAR_FRANKLIN
				ANIMPOSTFX_PLAY("SuccessFranklin",1000,FALSE)
				BREAK
			CASE CHAR_TREVOR
				ANIMPOSTFX_PLAY("SuccessTrevor",1000,FALSE)
				BREAK
			DEFAULT//CASE CHAR_MICHAEL or MP
				ANIMPOSTFX_PLAY("SuccessMichael",1000,FALSE)
				BREAK
		ENDSWITCH
		esd.bDoneFlash = TRUE
	ENDIF
	
	
	IF esd.iEndScreenDisplayFinish = 0
		esd.iEndScreenDisplayFinish = esd.IGameTimerReplacement + FLOOR(15000*skipperMultiplier)
		PRINTLN("RENDER_ENDSCREEN: first call on dataset, setting timer")
	ENDIF
	
	IF esd.fBlendInTargetYSum = 0.0 //recalcuate the height
		esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) // Title box should = 33pxls
		esd.fBlendInTargetYSum += esd.iElements* PIXEL_Y(ESC_LINE_Y_ADVANCE)
		IF esd.iElements > 0
		
			esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE*0.5)
		
		ENDIF
		IF esd.bShowCompletion
			esd.fBlendInTargetYSum += PIXEL_Y(ESC_LINE_Y_ADVANCE_HEADER) - PIXEL_Y(ESC_LINE_Y_GAP)//completion
		ENDIF 
	ENDIF
	
	BOOL rendering = TRUE
	
	WHILE rendering
		//Set the 'end screen on' global boolean to true
		SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
	
		//render the data
		esd.IGameTimerReplacement += round(0 +@ 1000)
		
		RAW_ENDSCREEN_DRAW_TITLE_UPDATE(esd,skipperMultiplier)
		
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish + 666 - 15000*skipperMultiplier //text move up time
			IF esd.fTitleUpMult < 1.0
				esd.fTitleUpMult += 0.0 +@ (1.0/0.15)
				IF esd.fTitleUpMult < 0.0
					esd.fTitleUpMult = 0.0
				ENDIF
			ENDIF
		ENDIF
		IF esd.fTitleUpMult > 1.0
			esd.fTitleUpMult = 1.0
		ENDIF

		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish -  333
			IF NOT esd.bHoldOnEnd
				esd.fFadeOutMult -= 0.0 +@ (1.0/0.810) 
				IF esd.fFadeOutMult <= 0.0
					esd.fFadeOutMult = 0.0
				ENDIF		
			endif
		ENDIF
		IF esd.fFadeOutMult > 1.0
			esd.fFadeOutMult = 1.0
		ENDIF
		IF esd.fFadeOutMult <= 0.20
		and not esd.bTransitionOutCalled
		AND (esd.splash != NULL)
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "TRANSITION_OUT")	
			END_SCALEFORM_MOVIE_METHOD()
			esd.iFadeOutSplashTimer = esd.IGameTimerReplacement	
			esd.bTransitionOutCalled = true
		endif
		
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish - 333  
			IF NOT esd.bHoldOnEnd
				IF esd.fScrollOutMult < 1.0
					esd.fScrollOutMult += 0.0 +@ (1.0/0.140) 
					IF esd.fScrollOutMult < 0.0
						esd.fScrollOutMult = 0.0
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF esd.fScrollOutMult > 1.0
			esd.fScrollOutMult = 1.0
		ENDIF

		//update the blend
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish + 1165 - 15000*skipperMultiplier // grace period
		IF esd.fBlendInProgress < 1.0
			esd.fBlendInProgress += 0.0 +@ (1.0/0.140)
			IF esd.fBlendInProgress < 0.0
				esd.fBlendInProgress = 0.0
			ENDIF
		ENDIF
		ENDIF
		IF esd.fBlendInProgress > 1.0
			esd.fBlendInProgress = 1.0
		ENDIF
	  
		//check to see if done
		IF esd.IGameTimerReplacement > esd.iEndScreenDisplayFinish
		
			IF esd.bHoldOnEnd
				//display x to continue button if need be
				//wait for button press
					//if so then rendering = false
				IF NOT esd.bVoteModeEnabled					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_SKIP_CUTSCENE)
					// Has to change from INPUT_FRONTEND_ACCEPT because the player has control at this time, and could remap a player control button to this (B*1901789)
//							rendering  = FALSE							
						esd.bHoldOnEnd = FALSE
					ENDIF
				ENDIF
			ELSE
				IF esd.IGameTimerReplacement - esd.iFadeOutSplashTimer > 1000
				and esd.bTransitionOutCalled
					rendering = FALSE			
				endif
			ENDIF		
		endif
		
		IF bBlockUntilDone
			WAIT(0)
		ELSE
			//Set the 'end screen on' global boolean to false
			IF !rendering
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
			ENDIF
			RETURN !rendering
		ENDIF
		
		
	ENDWHILE
	PRINTLN("RENDER_ENDSCREEN: process complete")
	//made it out! the screen process is complete.
	
	//Set the 'end screen on' global boolean to false
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
	RETURN TRUE
ENDFUNC





//Population calls 
PROC SET_ENDSCREEN_DATASET_HEADER_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
								  STRING splash,
								  STRING title,
								  BOOL bTitleIsUserName = FALSE)
	//
	
	esd.passed_splash_message = splash
	esd.title = title

	
	esd.titleIsUserName = bTitleIsUserName
	esd.eSplashType = ESST_REGULAR
	
ENDPROC

PROC SET_ENDSCREEN_DATASET_SPLASH_WITH_SUBINTS_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
								  STRING splash,
								  INT ValueA,
								  INT ValueB = 0,
								  BOOL UseBoth = FALSE)
	//
	esd.passed_splash_message = splash
	esd.iSplashSubA = ValueA
	esd.iSplashSubB = ValueB
	IF UseBoth
		esd.eSplashType = ESST_TWO_SUB
	ELSE
		esd.eSplashType = ESST_ONE_SUB
	ENDIF
ENDPROC



PROC SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_LITERAL_STRING_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,STRING str)
	//
	esd.iTitleSubelements = 1
	esd.titleSubElementType[0] = ESTSET_LITERAL_STRING
	esd.titleSubstrings[0] = str
	
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SINGLE_SUB_INT_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
								  INT value)
	//
	esd.iTitleSubelements = 1
	esd.titleSubElementType[0] = ESTSET_INT
	esd.iTitleSubInts[0] = value
	
ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SUB_INT_STRING_INT_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
								  INT valueA,STRING str,INT valueB)
	//
	esd.iTitleSubelements = 3
	esd.titleSubElementType[0] = ESTSET_INT
	esd.titleSubElementType[1] = ESTSET_STRING
	esd.titleSubElementType[2] = ESTSET_INT
	
	esd.iTitleSubInts[0] = valueA
	esd.iTitleSubInts[1] = valueB

	esd.titleSubstrings[0] = str

ENDPROC

PROC SET_ENDSCREEN_DATASET_HEADER_SUB_INT_STRING_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
								  INT value,STRING str)
	//
	esd.iTitleSubelements = 2
	esd.titleSubElementType[0] = ESTSET_INT
	esd.titleSubElementType[1] = ESTSET_STRING
	
	esd.iTitleSubInts[0] = value

	esd.titleSubstrings[0] = str
	
ENDPROC



PROC ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
										   END_SCREEN_ELEMENT_FORMATTING type, 
										   STRING name,
										   STRING text,
										   INT valueA,
										   INT valueB,
										   END_SCREEN_CHECK_MARK_STATUS checkState,
										   BOOL bNameIsPlayerName = FALSE)
										   
	//
	IF esd.iElements = END_SCREEN_MAX_ELEMENTS
		EXIT	
	ENDIF
	INT i = esd.iElements
	esd.ElementFormat[i] = type
	esd.ElementName[i] = name
	esd.ElementText[i] = text
	esd.ElementValA[i] = valueA
	esd.ElementValB[i] = valueB
	esd.ElementCheck[i] = checkState
	//esd.ElementInvalidation[i] = bInvalidated
	//PRINTLN("ADD_ENDSCREEN_DATASET_CONTENT_ELEMENT: <eleminvalidation> invalidation for ", i, " is ", bInvalidated)
	esd.ElementIsPlayerName[i] = bNameIsPlayerName
	esd.ElementRankLookup[i] = NULL
	++esd.iElements
ENDPROC


PROC SET_LAST_ENDSCREEN_ELEMENT_PLAYER_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd, PLAYER_INDEX pi)
	IF esd.iElements = 0
		EXIT
	ENDIF
	
	esd.ElementRankLookup[esd.iElements - 1] = pi
	

ENDPROC


PROC SET_ENDSCREEN_COMPLETION_LINE_STATE_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd,
										 BOOL bShow,
										 STRING resultString,
										 INT valueA,
										 INT valueB,
										 END_SCREEN_COMPLETION_TYPE type = ESC_PERCENTAGE_COMPLETION,
										 END_SCREEN_MEDAL_STATUS medal = ESMS_NO_MEDAL)
	//
	esd.bShowCompletion = bShow
	esd.CompletionResultString = resultString

	
	esd.iCompletionValueA = valueA
	esd.iCompletionValueB = valueB
	esd.CompletionType = type
	 
	esd.CompletionMedalState = medal
ENDPROC



/// PURPOSE:
///    Prep the scaleform part of the end screen, call this after you have finished calling populate
///    functions on it.
///    If not set to block returns if it has completely prepped the scaleform
/// RETURNS:
///    
FUNC BOOL ENDSCREEN_PREPARE_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd, BOOL bBlockUntilLoaded = TRUE, BOOL bCenterMessageMode = FALSE)
	
	//esd.bShowCompletion = FALSE
// 	ENDSCREEN_CONFIGURE_VOTE_PANEL(esd,30)
	esd.bCenterMessageMode = bCenterMessageMode
	INIT_ENDSCREEN_VALUES_TITLE_UPDATE(esd) // prep values

	END_SCREEN_WORK_OUT_WIDTHS_TITLE_UPDATE(esd)
	
	
	IF esd.splash = NULL
		//do the request
		REQUEST_STREAMED_TEXTURE_DICT("CommonMenu")
		REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
		//IF bCenterMessageMode
			//esd.splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_CENTERED")
		//ELSE
			esd.splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
		//ENDIF
		esd.bStartedFailformAnim = FALSE
		
	ENDIF
	
	// Speirs added to fix PT 1575220
//	IF esd.bShowSkipperPrompt AND esd.button = NULL 
//	IF esd.bShowSkipperPrompt AND esd.button = NULL
		esd.button = REQUEST_SCALEFORM_MOVIE_INSTANCE("INSTRUCTIONAL_BUTTONS")
		PRINTLN("<ENDSCREEN_PREPARE> Requested instructional button scaleform. ID: ", NATIVE_TO_INT(esd.button), ".")
//	ENDIF

	IF bBlockUntilLoaded
		WHILE  (NOT HAS_SCALEFORM_MOVIE_LOADED(esd.splash))
			OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu"))
			OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard"))
			PRINTLN("<ENDSCREEN_PREPARE> Waiting for scaleform movie and textures to load.")
			WAIT(0)
		ENDWHILE
		
		
		
		
		IF esd.bShowSkipperPrompt OR esd.bVoteModeEnabled
			WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(esd.button)
				PRINTLN("<ENDSCREEN_PREPARE> Waiting for button movie.")
				WAIT(0)
			ENDWHILE
		ENDIF
		
	ELSE
	
		IF (NOT HAS_SCALEFORM_MOVIE_LOADED(esd.splash))
			OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("CommonMenu"))
			OR (NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard"))
			
			PRINTLN("ENDSCREEN_PREPARE: Movie or textures not loaded in none blocking mode, returning false")
			RETURN FALSE
		ENDIF
		
		IF esd.bShowSkipperPrompt
			IF  NOT HAS_SCALEFORM_MOVIE_LOADED(esd.button)
				PRINTLN("<ENDSCREEN_PREPARE> Waiting for button movie.")
				RETURN FALSE
			ENDIF
		ENDIF
		
	
	ENDIF

	IF esd.bCenterMessageMode
	
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "SHOW_CENTERED_MP_MESSAGE")
	ELSE

		BEGIN_SCALEFORM_MOVIE_METHOD(esd.splash, "SHOW_MISSION_PASSED_MESSAGE")
	ENDIF
	
	
	
	IF esd.eSplashType = ESST_REGULAR
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(esd.passed_splash_message)
	ELSE
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(esd.passed_splash_message)
			ADD_TEXT_COMPONENT_INTEGER(esd.iSplashSubA)
			IF esd.eSplashType = ESST_TWO_SUB
				ADD_TEXT_COMPONENT_INTEGER(esd.iSplashSubB)
			ENDIF
		END_TEXT_COMMAND_SCALEFORM_STRING()
	ENDIF
	
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")//esd.title) //this will need to be hidden
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)//alpha
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)//anim
	END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()

	IF esd.bShowSkipperPrompt
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "CLEAR_ALL")
		END_SCALEFORM_MOVIE_METHOD()
			
#IF NOT USE_TU_CHANGES
		//Removing this fixes #1813267.
		BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_CLEAR_SPACE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		END_SCALEFORM_MOVIE_METHOD()
#ENDIF
		
		
		IF esd.bVoteModeEnabled
			
			//up
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_TU")
			END_SCALEFORM_MOVIE_METHOD()
			//down 
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_TD")
			END_SCALEFORM_MOVIE_METHOD()
			
			//cancel
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP_AB")
			END_SCALEFORM_MOVIE_METHOD()
			
			
			
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(ICON_SPINNER)))
			
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(esd.iVoteTimer, TIME_FORMAT_MINUTES | TIME_FORMAT_SECONDS )
				END_TEXT_COMMAND_SCALEFORM_STRING()
		
			END_SCALEFORM_MOVIE_METHOD()
			
			

		ELSE
			
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ES_HELP")
			END_SCALEFORM_MOVIE_METHOD()
			

		ENDIF
		
		
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "DRAW_INSTRUCTIONAL_BUTTONS")
			END_SCALEFORM_MOVIE_METHOD()
		
		
	ENDIF
	
	g_bResultScreenPrepared = TRUE
	PRINTLN("ENDSCREEN_PREPARE: Complete")
	RETURN TRUE
ENDFUNC



/// PURPOSE:
///    call to release your end screen scaleform elements
/// PARAMS:
///    esd - 
PROC ENDSCREEN_SHUTDOWN_TITLE_UPDATE(END_SCREEN_DATASET_TITLE_UPDATE &esd)
	PRINTLN("ENDSCREEN_SHUTDOWN: Marking dataset scaleform as no longer needed")
	IF esd.splash != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(esd.splash)
		esd.splash = NULL
	ENDIF
	
	IF esd.bShowSkipperPrompt
		//2285274 - Other scripts may keep this scaleform in memory for other uses.
		//By default turn off mouse input in case they don't need it.
		IF IS_PC_VERSION()
			BEGIN_SCALEFORM_MOVIE_METHOD(esd.button, "TOGGLE_MOUSE_BUTTONS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	
		PRINTLN("ENDSCREEN_SHUTDOWN: Set instructional button scaleform as no longer needed.")
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(esd.button)
		esd.button = NULL
	ENDIF
	
	IF esd.bNoLoadingScreenEnabled
		SET_NO_LOADING_SCREEN(FALSE)
		esd.bNoLoadingScreenEnabled = FALSE
	ENDIF

	//1321416
	IF NOT g_bEndScreenSuppressFadeIn
		IF NOT IS_PLAYER_DEAD(GET_PLAYER_INDEX())//1454085
		IF NOT g_flowUnsaved.bUpdatingGameflow
			IF IS_SCREEN_FADED_OUT() AND (NOT IS_REPEAT_PLAY_ACTIVE())//1416830
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		ENDIF
	ENDIF
	
	//1925793: Mark stat screen as not displaying, just to be safe
	SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)

ENDPROC




























































