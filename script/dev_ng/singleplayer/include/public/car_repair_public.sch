USING "commands_misc.sch"
USING "commands_script.sch"
USING "player_Ped_public.sch"

USING "globals.sch"

FUNC FLOAT RunRightStick(FLOAT TheTargetAngle, FLOAT Difficulty, BOOL &InTarget)

	FLOAT GREEN = 0.0
	
	INT StickLength = 124
	
	INT RightX, RightY

	RightX = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT_AXIS_X) - 128
	RightY = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
	
	FLOAT TheRightTargetX = StickLength * COS(TheTargetAngle)
	FLOAT TheRightTargetY = StickLength * SIN(TheTargetAngle)
	
	FLOAT RightCrossHairCentreX = 0.7
	FLOAT RightCrossHairCentreY = 0.5
	FLOAT RightCrossHairSize = 0.2
		
	FLOAT Rightmultiplyer = (RightCrossHairSize/2)/StickLength //Step difference of the 2 graphs. 

	FLOAT RightStickHorizontalPos = (RightX*Rightmultiplyer)+RightCrossHairCentreX
	FLOAT RightStickVerticlePos = (RightY*Rightmultiplyer)+RightCrossHairCentreY
	
	FLOAT RightDrawnTargetX = (TheRightTargetX*Rightmultiplyer)+RightCrossHairCentreX
	FLOAT RightDrawnTargetY = (TheRightTargetY*Rightmultiplyer)+RightCrossHairCentreY
	
	FLOAT RightDrawnSweetSpotX = ((Difficulty*Rightmultiplyer)+RightCrossHairSize)/4
	FLOAT RightDrawnSweetSpotY = ((Difficulty*Rightmultiplyer)+RightCrossHairSize)/4
	
	BOOL isValid
	IF RightStickHorizontalPos < (RightDrawnTargetX+RightDrawnSweetSpotX) AND RightStickHorizontalPos > (RightDrawnTargetX-RightDrawnSweetSpotX)
		IF RightStickVerticlePos < (RightDrawnTargetY+RightDrawnSweetSpotY) AND RightStickVerticlePos > (RightDrawnTargetY-RightDrawnSweetSpotY)
			isValid = TRUE
		ENDIF
	ENDIF

	IF isValid = TRUE
			
		FLOAT Closeness = VDIST(<<RightDrawnTargetX, RightDrawnTargetY, 0>>, <<RightStickHorizontalPos, RightStickVerticlePos, 0>>)
		
		FLOAT b = 0.07 //Where the closeness value should be before it starts showing green/shaking. 0.0 is bang on target 0.1 is at the origin.
		FLOAT m = -0.0003 //gradient
		
		GREEN = (Closeness-b)/m
		
		IF Closeness < 0.05
			InTarget = TRUE
			Green = 180
		ENDIF
		
		IF Green > 180
			Green = 180
		ENDIF
		
	ENDIF

	RETURN GREEN

ENDFUNC

FUNC FLOAT RunLeftStick(FLOAT TheTargetAngle, FLOAT Difficulty, BOOL &InTarget)

	FLOAT GREEN = 0.0
	
	INT StickLength = 124
	
	INT LeftX, LeftY
	LeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_X) - 128
	LeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL,INPUT_FRONTEND_AXIS_Y) - 128
	
	FLOAT TheLeftTargetX = StickLength * COS(TheTargetAngle)
	FLOAT TheLeftTargetY = StickLength * SIN(TheTargetAngle)
	
	FLOAT LeftCrossHairCentreX = 0.3
	FLOAT LeftCrossHairCentreY = 0.5
	FLOAT LeftCrossHairSize = 0.2
		
	FLOAT Leftmultiplyer = (LeftCrossHairSize/2)/StickLength //Step difference of the 2 graphs. 

	FLOAT LeftStickHorizontalPos = (LeftX*Leftmultiplyer)+LeftCrossHairCentreX
	FLOAT LeftStickVerticlePos = (LeftY*Leftmultiplyer)+LeftCrossHairCentreY
	
	FLOAT LeftDrawnTargetX = (TheLeftTargetX*Leftmultiplyer)+LeftCrossHairCentreX
	FLOAT LeftDrawnTargetY = (TheLeftTargetY*Leftmultiplyer)+LeftCrossHairCentreY
	
	FLOAT LeftDrawnSweetSpotX = ((Difficulty*Leftmultiplyer)+LeftCrossHairSize)/4
	FLOAT LeftDrawnSweetSpotY = ((Difficulty*Leftmultiplyer)+LeftCrossHairSize)/4
	
	BOOL isValid
	IF LeftStickHorizontalPos < (LeftDrawnTargetX+LeftDrawnSweetSpotX) AND LeftStickHorizontalPos > (LeftDrawnTargetX-LeftDrawnSweetSpotX)
		IF LeftStickVerticlePos < (LeftDrawnTargetY+LeftDrawnSweetSpotY) AND LeftStickVerticlePos > (LeftDrawnTargetY-LeftDrawnSweetSpotY)
			isValid = TRUE
		ENDIF
	ENDIF

	IF isValid = TRUE
			
		FLOAT Closeness = VDIST(<<LeftDrawnTargetX, LeftDrawnTargetY, 0>>, <<LeftStickHorizontalPos, LeftStickVerticlePos, 0>>)
		
		FLOAT b = 0.07 //Where the closeness value should be before it starts showing green/shaking. 0.0 is bang on target 0.1 is at the origin.
		FLOAT m = -0.0003 //gradient
		
		GREEN = (Closeness-b)/m
		
		IF Closeness < 0.05
			InTarget = TRUE
			Green = 180
		ENDIF
		
		IF Green > 180
			Green = 180
		ENDIF
		
	ENDIF

	RETURN GREEN

ENDFUNC

FUNC FLOAT GetCombinedColour(FLOAT& GREEN, FLOAT& RED, FLOAT LeftGreen, FLOAT RightGreen)

	
	FLOAT CombinedColour = LeftGreen+RightGreen
	
	FLOAT Fraction = (0.00787401) //so the graph reads x = 0 y = 0, x = 200 y = 200, x = 400 y = 0; 1/127 Got from the graph y = 200SIN(1/127*x)
	FLOAT degree = Fraction*57.29577 //Convert to degrees without pi. 180/pi = 57.29577
	FLOAT trig  = SIN(degree*CombinedColour)
	
	RED = 200*trig

	IF CombinedColour < 200
		GREEN = CombinedColour
	ELSE
		GREEN = 200
	ENDIF

RETURN CombinedColour

ENDFUNC


PROC INCREMENT_CAR_REPAIRS_BY_PLAYER()
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		CASE CHAR_MICHAEL
			g_savedGlobals.sAmbient.iVehiclesRepairedByMichael++
		BREAK
		CASE CHAR_FRANKLIN
			g_savedGlobals.sAmbient.iVehiclesRepairedByFranklin++
		BREAK
		CASE CHAR_TREVOR
			g_savedGlobals.sAmbient.iVehiclesRepairedByTrevor++
		BREAK
	ENDSWITCH
ENDPROC




