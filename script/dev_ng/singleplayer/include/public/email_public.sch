USING "globals.sch"
USING "player_ped_public.sch"
USING "email_generated_functions.sch"
USING "finance_modifiers_public.sch"
#IF IS_DEBUG_BUILD
USING "flow_mission_data_public.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   email_public.sc
//      AUTHOR          :   Ak
//      DESCRIPTION     :   Public email system setting and information retrieval functions
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

FUNC TEXT_LABEL_63 RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)
	
    CPRINTLN(DEBUG_EMAIL,"RETRIEVE_EMAIL_STRING_TAG_BY_INDICE : ",indice)
    IF(indice > -1)
        TEXT_LABEL_63 s 
        s = "EMSTR_"
        s += indice
        CPRINTLN(DEBUG_EMAIL,"---",s)
        RETURN s//TEXT_LABEL_LAUNDERER(s)
    ENDIF
    CPRINTLN(DEBUG_EMAIL,"Failed!")
    TEXT_LABEL_63 fail = "FAIL"
    RETURN fail
ENDFUNC

FUNC BOOL PROPERTY_EMAIL_CALLBACK(SCALEFORM_INDEX sf,INT threadRegistrationID, INT logIndex, EMAIL_MESSAGE_ENUMS mailEnum)
    BOOL bUpdated = FALSE

    //new example of how to make extended email overrides
    //as before return false if you're not going to override it 
    INT placeholder = threadRegistrationID 
    placeholder = logIndex
    placeholder = placeholder

    INT iSlot = 0 
    
    CPRINTLN(DEBUG_EMAIL,"APP EMAIL VIEW - SCRIPT: Clearing slots")
    BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT_EMPTY")
    END_SCALEFORM_MOVIE_METHOD()
                
    TEXT_LABEL_63 title = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[mailEnum].iTitleTag)
    TEXT_LABEL_63 body
    SWITCH mailEnum
        CASE PROPMAN_TO_MIKE
            body = "PROPR_INCEMAIL1"
        BREAK
        CASE PROPMAN_TO_FRANK
            body = "PROPR_INCEMAIL3"
        BREAK
        CASE PROPMAN_TO_TREV
            body = "PROPR_INCEMAIL2"
        BREAK
    ENDSWITCH   
    
    //do the first mail
    
    CPRINTLN(DEBUG_EMAIL,"APP EMAIL VIEW - SCRIPT: Main part of email")
    BEGIN_SCALEFORM_MOVIE_METHOD(sf, "SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot)                                 
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[mailEnum].eTo)) 
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[mailEnum].eFrom))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(title)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(body) //override this to whatever you like
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_SIGNOFF_TAG(g_AllEmails[mailEnum].eFrom))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
    END_SCALEFORM_MOVIE_METHOD()
    ++iSlot
    //pass as many damn strings to the email as you like (can't vouch for memory) in the following form
    
    
    INT i
    REPEAT COUNT_OF(PROPERTY_ENUM) i 
        PROPERTY_ENUM thisProperty = INT_TO_ENUM(PROPERTY_ENUM, i)
    
        BOOL bAdd = FALSE
        SWITCH mailEnum
            CASE PROPMAN_TO_MIKE
                IF g_savedGlobals.sPropertyData.propertyOwnershipData[thisProperty].charOwner = CHAR_MICHAEL
                    CPRINTLN(DEBUG_EMAIL,"adding mike")
                    bAdd = TRUE
                ENDIF
            BREAK
            CASE PROPMAN_TO_FRANK
                IF g_savedGlobals.sPropertyData.propertyOwnershipData[thisProperty].charOwner = CHAR_FRANKLIN
                    CPRINTLN(DEBUG_EMAIL,"adding frank")
                    bAdd = TRUE
                ENDIF
            BREAK
            CASE PROPMAN_TO_TREV
                IF g_savedGlobals.sPropertyData.propertyOwnershipData[thisProperty].charOwner = CHAR_TREVOR
                    CPRINTLN(DEBUG_EMAIL,"adding trev") 
                    bAdd = TRUE
                ENDIF
            BREAK
        ENDSWITCH
        
        IF bAdd
            STRING sPropertyString

            SWITCH thisProperty
                CASE PROPERTY_TOWING_IMPOUND
                    sPropertyString = "ACCNA_TOWING"
                BREAK
                CASE PROPERTY_TAXI_LOT  
                    sPropertyString = "ACCNA_TAXI_LOT"
                BREAK   
                CASE PROPERTY_ARMS_TRAFFICKING  
                    sPropertyString = "ACCNA_ARMS"
                BREAK
                CASE PROPERTY_SONAR_COLLECTIONS  
                    sPropertyString = "ACCNA_SONAR"
                BREAK
                CASE PROPERTY_CAR_MOD_SHOP  
                    sPropertyString = "ACCNA_CARMOD"
                BREAK
                CASE PROPERTY_CINEMA_VINEWOOD
                    sPropertyString = "ACCNA_VCINEMA"
                BREAK
                CASE PROPERTY_CINEMA_DOWNTOWN
                    sPropertyString = "ACCNA_DCINEMA"
                BREAK
                CASE PROPERTY_CINEMA_MORNINGWOOD
                    sPropertyString = "ACCNA_MCINEMA"
                BREAK
                CASE PROPERTY_GOLF_CLUB
                    sPropertyString = "ACCNA_GOLF"
                BREAK
                CASE PROPERTY_CAR_SCRAP_YARD
                    sPropertyString = "ACCNA_CSCRAP"
                BREAK
                CASE PROPERTY_WEED_SHOP
                    sPropertyString = "ACCNA_SMOKE"
                BREAK
                CASE PROPERTY_BAR_TEQUILALA
                    sPropertyString = "ACCNA_TEQUILA"
                BREAK
                CASE PROPERTY_BAR_PITCHERS
                    sPropertyString = "ACCNA_PITCHERS"
                BREAK
                CASE PROPERTY_BAR_HEN_HOUSE
                    sPropertyString = "ACCNA_HEN"
                BREAK
                CASE PROPERTY_BAR_HOOKIES
                    sPropertyString = "ACCNA_HOOKIES"
                BREAK
                CASE PROPERTY_STRIP_CLUB
                    sPropertyString = "ACCNA_STRP"
                BREAK
            ENDSWITCH
            
            CPRINTLN(DEBUG_EMAIL,"APP EMAIL VIEW - SCRIP: extra string slot ", iSlot)
            BEGIN_SCALEFORM_MOVIE_METHOD(sf, "SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9) 
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSlot) 
                                      BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PROPR_INCEMAIL4")
                                      ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sPropertyString) 
                                      ADD_TEXT_COMPONENT_INTEGER(g_savedGlobals.sPropertyData.propertyOwnershipData[thisProperty].iLastIncome)
                                      END_TEXT_COMMAND_SCALEFORM_STRING()

            END_SCALEFORM_MOVIE_METHOD()
            ++iSlot
            
            bUpdated = TRUE
        ENDIF
    ENDREPEAT   
    

    RETURN bUpdated

    
    /*


    

    
    IF iOutputCount > 0
        bUpdated = TRUE 
        STRING sEmail
        SWITCH iOutputCount
            CASE 1
                sEmail = "PROPR_INCEMAIL1"
            BREAK
            CASE 2
                sEmail = "PROPR_INCEMAIL2"
            BREAK
            CASE 3
                sEmail = "PROPR_INCEMAIL3"
            BREAK
            CASE 4
                sEmail = "PROPR_INCEMAIL4"
            BREAK
            CASE 5
                sEmail = "PROPR_INCEMAIL5"
            BREAK
        ENDSWITCH
        BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sEmail)             
        
        REPEAT iOutputCount i
            PRINTSTRING("add output") printnl()
            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sOutputString[i])
            ADD_TEXT_COMPONENT_INTEGER(iOutputInt[i])
        ENDREPEAT
    
        END_TEXT_COMMAND_SCALEFORM_STRING() 
    ENDIF


    RETURN bUpdated // return false if you didn't update the content
    */
ENDFUNC


/*
BLANK = 1
SELECT = 2
PAGES = 3
BACK = 4
CALL = 5
HANGUP = 6
DAY = 7
WEEK = 8
KEYPAD = 9
OPEN = 10    
REPLY = 11   
DELETE = 12
YES = 13
NO = 14
SORT = 15
WEBSITE = 16
POLICE = 17
AMBULANCE = 18
FIRE = 19
PAGES2 = 20 (for second homemenu view)
*/
//SET_SOFT_KEYS_COLOUR(1, 77, 151, 4);
//SET_SOFT_KEYS_COLOUR(2, 197, 147, 0);
//SET_SOFT_KEYS_COLOUR(3, 166, 4, 0)
PROC CONFIGURE_EMAIL_SOFT_KEY_BACK(BOOL show)
        INT r,g,b,a
    
        IF IS_XBOX360_VERSION()
		OR IS_XBOX_PLATFORM()
            GET_HUD_COLOUR (HUD_COLOUR_RED, r, g, b, a )
        ELSE
            GET_HUD_COLOUR (HUD_COLOUR_RED, r, g, b, a )
        ENDIF

        //Swapped over PC build to use 360 colouring in Cellphone_flashhand.sc 28.11.13  - this carries across all apps automatically unless the change it themselves.
        
        /* Andrew - I've commented this out. Don't think you need it and it can screw up the Japanese build. ST

            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS_COLOUR")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_NEG)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(r)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(b)
            END_SCALEFORM_MOVIE_METHOD()
        */

     
    IF show
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_NEG)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
        END_SCALEFORM_MOVIE_METHOD()
    ELSE
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_NEG)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
        END_SCALEFORM_MOVIE_METHOD()
    ENDIF
ENDPROC

PROC CONFIGURE_EMAIL_SOFT_KEY_LINK(BOOL show)

        INT r,g,b,a
    
        IF IS_XBOX360_VERSION()
		OR IS_XBOX_PLATFORM()
            GET_HUD_COLOUR (HUD_COLOUR_BLUE, r, g, b, a )
        ELSE
            GET_HUD_COLOUR (HUD_COLOUR_PINKLIGHT, r, g, b, a )  //Andrew  - changed this to pinklight. ST
        ENDIF

        //Andrew - this one should be okay. Swapped over PC build to use 360 colouring in Cellphone_flashhand.sc 28.11.13. Removed set_soft_keys_colour block.
        /*
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS_COLOUR")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_OTHER)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(r)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(b)
            END_SCALEFORM_MOVIE_METHOD()
        */

    IF show
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_OTHER)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(16)
        END_SCALEFORM_MOVIE_METHOD()
    ELSE
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_OTHER)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(16)
        END_SCALEFORM_MOVIE_METHOD()
    ENDIF
ENDPROC


PROC CONFIGURE_EMAIL_SOFT_KEY_SELECT(BOOL show)


        INT r,g,b,a
    
        IF IS_XBOX360_VERSION()
		OR IS_XBOX_PLATFORM()
            GET_HUD_COLOUR (HUD_COLOUR_GREEN, r, g, b, a )
        ELSE
            GET_HUD_COLOUR (HUD_COLOUR_BLUE, r, g, b, a ) //should be blue
        ENDIF

        //Swapped over PC build to use 360 colouring in Cellphone_flashhand.sc 28.11.13  - this carries across all apps automatically unless the change it themselves.

        /* Andrew - I've commented this out. Don't think you need it and it can screw up the Japanese build. ST
            BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS_COLOUR")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_POS)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(r)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(g)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(b)
            END_SCALEFORM_MOVIE_METHOD()
        */


    IF show
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_POS)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
        END_SCALEFORM_MOVIE_METHOD()
    ELSE
        BEGIN_SCALEFORM_MOVIE_METHOD(SF_MovieIndex,"SET_SOFT_KEYS")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BADGER_POS)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
        END_SCALEFORM_MOVIE_METHOD()
    ENDIF
ENDPROC






CONST_INT EMAIL_OVERRIDE_SUBSTRING_MAX 10 // 5 - CC

PROC UPDATE_EMAILS_READ_FOR_CURRENT_PLAYER(INT by)

    CPRINTLN(DEBUG_EMAIL,"Old unread email totals are: ")
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP0 ",g_iUnreadEmailsSP0)
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP1 ",g_iUnreadEmailsSP1)
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP2 ",g_iUnreadEmailsSP2)
    CPRINTLN(DEBUG_EMAIL,"Total unread mails to change by ",by)
    SWITCH GET_CURRENT_PLAYER_PED_ENUM()
        CASE CHAR_MICHAEL
                g_iUnreadEmailsSP0 += by
                IF g_iUnreadEmailsSP0 < 0
                     g_iUnreadEmailsSP0 = 0
                ENDIF
            BREAK
        CASE CHAR_TREVOR
                g_iUnreadEmailsSP2 += by
                IF g_iUnreadEmailsSP2 < 0
                     g_iUnreadEmailsSP2 = 0
                ENDIF
            BREAK
        CASE CHAR_FRANKLIN
                g_iUnreadEmailsSP1 += by
                IF g_iUnreadEmailsSP1 < 0
                     g_iUnreadEmailsSP1 = 0
                ENDIF
            BREAK
        DEFAULT 
            CPRINTLN(DEBUG_EMAIL,"UPDATE_EMAILS_READ_FOR_CURRENT_PLAYER: invalid enum ID was : ", GET_CURRENT_PLAYER_PED_ENUM())
            SCRIPT_ASSERT("UPDATE_EMAILS_READ_FOR_CURRENT_PLAYER called on unrecognised ped!")
            BREAK
    ENDSWITCH
    CPRINTLN(DEBUG_EMAIL,"New unread email totals are: ")
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP0 ",g_iUnreadEmailsSP0)
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP1 ",g_iUnreadEmailsSP1)
    CPRINTLN(DEBUG_EMAIL,"g_iUnreadEmailsSP2 ",g_iUnreadEmailsSP2)
ENDPROC


PROC SET_EMAIL_BRANDING_ARGS_FROM_INDEX( INT index, EMAILER_ENUMS from)
    
    
    EMAIL_MESSAGE_ENUMS e = INT_TO_ENUM(EMAIL_MESSAGE_ENUMS, index)
    
    SWITCH e
    /*
        CASE 
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("bla")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
        BREAK
    */
    CASE EMAIL_HELIPAD_MIKE
    CASE EMAIL_HANGAR_MIKE
    CASE EMAIL_HELIPAD_FRANK
    CASE EMAIL_HELIPAD_TREV
    CASE EMAIL_HANGAR_FRANK
    CASE EMAIL_HANGAR_TREV
    CASE EMAIL_PLANE_MIKE
    CASE EMAIL_PLANE_FRANK
    CASE EMAIL_PLANE_TREVOR
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Elitas_Travel")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        EXIT
    CASE EMAIL_BOAT_MIKE
    CASE EMAIL_BOAT_FRANK
    CASE EMAIL_BOAT_TREVOR
    CASE EMAIL_MARINA_FRANK
    CASE EMAIL_MARINA_TREV
    CASE EMAIL_MARINA_MIKE
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Dock_Tease")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        EXIT
    CASE EMAIL_MILITARY_MIKE
    CASE EMAIL_MILITARY_FRANK
    CASE EMAIL_MILITARY_TREVOR
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Warstock")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        EXIT
        CASE EMAIL_CAR_FRANKLIN
        CASE EMAIL_CAR_MIKE
        CASE EMAIL_CAR_TREVOR
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Legendary_Motorsport")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        EXIT
    
    ENDSWITCH
    /*
    EmailAds_Appropriate_Associates.png
    EmailAds_Bawsaq.png
    EmailAds_Beseecher.png
    EmailAds_Bleeter.png
    EmailAds_CashForDeadDreams.png
    EmailAds_CCCKings.png
    EmailAds_Classic_Vinewood.png
    EmailAds_Elitas_Travel.png (updated)
    EmailAds_Fame_Or_Shame.png
    EmailAds_Fruit.png
    EmailAds_Himplants.png
    EmailAds_Jock_Cranley.png
    EmailAds_Lifeinvader.png
    EmailAds_LS_Customs.png (updated)
    EmailAds_LS_Freegans.png
    EmailAds_Manopause_Adventures.png
    EmailAds_Prop14.png
    EmailAds_Toilet_Cleaner.png
    
    //EmailAds_HushSmush
    //EmailAds_I_Will_Survive_It_All
    //EmailAds_LS_Customs
    //EmailAds_My_Divine_Within
    //EmailAds_Sue_Murry
    
    
    */
    SWITCH from
        CASE EMAILER_MILITARY_SITE
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_Warstock")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
            EXIT

        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Appropriate_Associates")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Bawsaq")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Beseecher")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Bleeter")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_C
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_CashForDeadDreams")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_CCCKings")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Classic_Vinewood")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Fame_Or_Shame")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Fruit")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Himplants")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Jock_Cranley")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE EMAILER_
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Lifeinvader")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        CASE EMAILER_LSC
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Customs")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
            EXIT
			
        CASE EMAILER_TOURIST_BOARD	// - CC
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("EmailAds_LS_Tourist_Info")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
            EXIT
        
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_LS_Freegans")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Manopause_Adventures")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Prop14")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        //CASE 
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EmailAds_Toilet_Cleaner")
        //  SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
        //  EXIT
        
    ENDSWITCH
    
    
    
    
    
    
    
    
    
    /*
    CULTINITIAL = 0,
    CULT1 = 1,
    CULT2 = 2,
    CULT3 = 3,
    CULT4 = 4,
    CULT5 = 5,
    CULTFINALE = 6,
    CULTDONATE500 = 7,
    CULTDONATE5000 = 8,
    CULTBUYROBES = 9,
    EMAIL_ORDER_PLACED = 10,
    EMAIL_ORDER_DELIVERED = 11,
    EMAIL_ORDER_REMINDER = 17,
    EMAIL_CAR_FRANKLIN = 32,
    EMAIL_CAR_MIKE = 33,
    EMAIL_CAR_TREVOR = 34,
    EMAIL_MILITARY_MIKE = 35,
    EMAIL_MILITARY_FRANK = 36,
    EMAIL_MILITARY_TREVOR = 37,
    EMAIL_BOAT_MIKE = 38,
    EMAIL_BOAT_FRANK = 39,
    EMAIL_BOAT_TREVOR = 40,
    EMAIL_PLANE_MIKE = 41,
    EMAIL_PLANE_FRANK = 42,
    EMAIL_PLANE_TREVOR = 43,
    EMAIL_HELIPAD_MIKE = 56,
    EMAIL_MARINA_MIKE = 57,
    EMAIL_HANGAR_MIKE = 58,
    EMAIL_HELIPAD_FRANK = 59,
    EMAIL_HELIPAD_TREV = 60,
    EMAIL_MARINA_FRANK = 61,
    EMAIL_MARINA_TREV = 62,
    EMAIL_HANGAR_FRANK = 63,
    EMAIL_HANGAR_TREV = 64,
    PROPMAN_TO_MIKE = 73,
    PROPMAN_TO_FRANK = 74,
    PROPMAN_TO_TREV = 75,
    EMAIL_BAIL_BONDS_INFO1 = 76,
    EMAIL_BAIL_BONDS_INFO2 = 77,
    EMAIL_BAIL_BONDS_INFO3 = 78,
    EMAIL_BAIL_BONDS_INFO4 = 79,
    EMAIL_BAIL_BONDS_FINAL = 80,
    EMAIL_BAIL_BONS_REMINDER_1 = 81,
    EMAIL_BAIL_BONS_REMINDER_2 = 82,
    EMAIL_BAIL_BONS_REMINDER_3 = 83,
    EMAIL_BAIL_BONS_REMINDER_4 = 84
    */

    
    //param 8 = String - Advert or branding TXD/Texure (both share same name, so only one string required) (256x64)
    //param 9 = Boolean - Advert/branding at top of email (true) or advert at bottom of email (false) 
    
    

    
    
ENDPROC


/*
FUNC TEXT_LABEL_63 RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)

    PRINTSTRING("RETRIEVE_EMAIL_STRING_TAG_BY_INDICE : ")
    PRINTINT(indice)
    PRINTNL()
    IF(indice > -1)
        TEXT_LABEL_63 s 
        s = "EMSTR_"
        s += indice
        
        
        PRINTSTRING(s)
        PRINTNL()
        
        RETURN s//TEXT_LABEL_LAUNDERER(s)
    ENDIF
    PRINTSTRING("Failed!")
    PRINTNL()
    TEXT_LABEL_63 fail = "FAIL"
    RETURN fail
ENDFUNC

FUNC STRING RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(INT indice)
    STRING s = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE_REAL(indice)
        PRINTSTRING("Return check : ")
        PRINTSTRING(s)
        PRINTNL()
    RETURN s
ENDFUNC
*/


FUNC BOOL CAN_A_THREAD_MAIL_FIRE_A_RESPONSE(EMAILER_ENUMS mailer,INT inbox_index)
    IF NOT (ENUM_TO_INT(mailer) < TOTAL_INBOXES)
        CPRINTLN(debug_email,"Email thread participant doesn't have an inbox, skipping snapshot\n")
        RETURN FALSE
    ENDIF

    INT maillogindex = g_Inboxes[mailer].EmailsLogIndex[inbox_index]
    INT mailthreadindex = g_Inboxes[mailer].ThreadIndex[inbox_index]
    CDEBUG1LN(debug_email,"CAN_A_THREAD_MAIL_FIRE_A_RESPONSE: Found log/thread indices",maillogindex,",",mailthreadindex)
	CDEBUG1LN(debug_email,"Active/ended:",g_AllEmailThreads[mailthreadindex].bActive,"/",g_AllEmailThreads[mailthreadindex].bEnded)
    IF NOT (g_AllEmailThreads[mailthreadindex].bActive AND NOT g_AllEmailThreads[mailthreadindex].bEnded)
        //thread is ended or inactive
        RETURN FALSE
    ENDIF
    CDEBUG1LN(debug_email,"CAN_A_THREAD_MAIL_FIRE_A_RESPONSE: Thread still active")
	
    IF NOT (maillogindex = (g_AllEmailThreads[mailthreadindex].iMailsInLog-1))//not the last mail
        RETURN FALSE
    ENDIF
    CDEBUG1LN(debug_email,"CAN_A_THREAD_MAIL_FIRE_A_RESPONSE: index not already last in thread")
	
    INT findmail =  g_AllEmailThreads[mailthreadindex].ThreadEmailLog[g_AllEmailThreads[mailthreadindex].iMailsInLog-1]
	CDEBUG1LN(debug_email,"CAN_A_THREAD_MAIL_FIRE_A_RESPONSE: Found mail ",findmail," with no. of responses: ",g_AllEmails[findmail].iResponses)
    IF g_AllEmails[findmail].iResponses > 0
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC


PROC FILL_SCALEFORM_RESPONSES_FOR_EMAIL(SCALEFORM_INDEX sf, enumCharacterList character,INT selectedUIIndex)
    
        //TODO move this data into the charsheet later
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        EXIT
    ENDIF
    
    //g_bEmailSystemPaused = TRUE//pause the email system until we're out of the selected mail
/*
    CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sf,"SET_DATA_SLOT",
                                                            8,//param 1 = View state enum
                                                            TO_FLOAT(0),//param 2 = Slot number
                                                            1,// param 3 = Icon type 0 = unread, 1 = read, 2 = needs reply
                                                            2,//param 4 = Boolean - Has attachment icon
                                                            -1,//skipped
                                                            "BSCO_BRT_S",//param 5 = String - Senders email address
                                                            "BSCO_MAI_S"//param 6 = String - Subject 
                                                            ) 
    
*/      
    //find the mail in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
    INT baselinemail = g_Inboxes[index].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    


    IF NOT g_Inboxes[index].IsDynamic[invval]
    
        INT threadindex = g_Inboxes[index].ThreadIndex[invval]
        INT emaillogindex = g_Inboxes[index].EmailsLogIndex[invval]
        INT firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]

        
        
        //now use the responses to fill out the list
        IF (g_AllEmails[firstmail].iResponses = 0)
            SCRIPT_ASSERT("FILL_SCALEFORM_RESPONSES_FOR_EMAIL: Attempting to populate scaleform response list for an email with no responses!")
            EXIT
        ENDIF
        
        CPRINTLN(DEBUG_EMAIL,"Email has responses: ",g_AllEmails[firstmail].iResponses)
        INT i = 0
        
        //g_Inboxes[index].iTotalMails // use MAX_INBOX_LOGGED_MAILS_THREADS if greater

/*
        BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT_EMPTY")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(21)
        END_SCALEFORM_MOVIE_METHOD()
  */      
        
        
        INT iresp = g_AllEmails[firstmail].iResponses 
        
        IF iresp > 1
            iresp = 1
        ENDIF
            
        REPEAT iresp i
         CPRINTLN(DEBUG_EMAIL,"populating responses!\n")
         //RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].Responses[i].iResponseNameTag)
         INT maaail = g_AllEmails[firstmail].Responses[i].iResponseMail
         
         TEXT_LABEL_63 send = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[maaail].iContentTag)
         CPRINTLN(DEBUG_EMAIL,"<EMAIL PUBLIC> RESPONsE STRING: ",send)
         //TEXT_LABEL subject = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].Responses[i].iResponseNameTag)
         /*
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sf,"SET_DATA_SLOT",
                                                                21,//param 1 = View state enum
                                                                TO_FLOAT(i),//param 2 = Slot number
                                                                INVALID_SCALEFORM_PARAM,
                                                                INVALID_SCALEFORM_PARAM,
                                                                INVALID_SCALEFORM_PARAM ,//skipped
                                                                send//param 5 = String - Senders email address
                                                               // subject//param 6 = String - Subject 
                                                                ) */
            /*                                                   
            BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")                                                    
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(21)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(send)
            END_SCALEFORM_MOVIE_METHOD()
            */
           // TEXT_LABEL_63 title = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[maaail].iTitleTag)
           TEXT_LABEL_63 body = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[maaail].iContentTag)
		   CPRINTLN(DEBUG_EMAIL,"<EMAIL PUBLIC> BODY STRING: ",body)
                
            BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")/*GET_EMAILER_ADDRESS_TAG(g_AllEmails[maaail].eTo)*/
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")//GET_EMAILER_ADDRESS_TAG(g_AllEmails[maaail].eFrom))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("EM_RESPONSE_NEW")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(body)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_SIGNOFF_TAG(g_AllEmails[maaail].eFrom))
            SET_EMAIL_BRANDING_ARGS_FROM_INDEX(maaail,g_AllEmails[maaail].eFrom)
            END_SCALEFORM_MOVIE_METHOD()
        ENDREPEAT
        
    ELSE
        SCRIPT_ASSERT("Dynamic email replies unimplemented.")
    ENDIF
    
ENDPROC


FUNC INT EMAILS_IN_INBOX(enumCharacterList character)
    
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH
    
    RETURN g_Inboxes[index].iTotalMails
ENDFUNC


PROC BLOCKING_PREP_TEXTURE_FOR_SCALEFORM_MOVIE_EMAIL(enumCharacterList character,INT selectedUIindex)

    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        EXIT
    ENDIF
    //find the mail in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
/*
    INT baselinemail = g_Inboxes[index].iTotalMails-1
    WHILE baselinemail > MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    

    //find the inverted value of selectedUIindex
    INT invval = baselinemail - selectedUIindex
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)

    WHILE invval >= MAX_INBOX_LOGGED_MAILS_THREADS
        invval -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
*/
    
    //count nth back from the latest
    INT baselinemail = g_Inboxes[index].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    
    
    
    
    CPRINTLN(DEBUG_EMAIL,"Startat: ",invval)    
    
    INT threadindex = g_Inboxes[index].ThreadIndex[invval]
    INT emaillogindex = g_Inboxes[index].EmailsLogIndex[invval]

    INT firstmail = -1
    BOOL bIsDynamic = g_Inboxes[index].IsDynamic[invval]
    
    IF NOT bIsDynamic
        firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
    
    ELSE

        INT tind = -1
        INT j = 0
        REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
            IF g_DynamicEmailThreadBuffers[j].registrationID = threadindex
                tind = j
            ENDIF
        ENDREPEAT
        IF tind = -1
             CPRINTLN(DEBUG_EMAIL,"BLOCKING_PREP_TEXTURE_FOR_SCALEFORM_MOVIE_EMAIL: Dynamic thread ID not found in buffer\n")
            EXIT
        ENDIF
        //emaillogindex
        firstmail = ENUM_TO_INT(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].sourceEmail)

    ENDIF
        
            
        

    IF NOT GET_DOES_EMAIL_HAVE_ATTACHMENT_TEXTURE(firstmail)
        CPRINTLN(DEBUG_EMAIL,"Email has no attachement texture")
        EXIT
    ENDIF
        
    
    IF(g_iEmailWithLoadedTextureIndex != -1)
        CPRINTLN(DEBUG_EMAIL,"Releasing old attachement texture")
        SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(g_iEmailWithLoadedTextureIndex))
    ENDIF
    g_iEmailWithLoadedTextureIndex = firstmail
    
    
    CPRINTLN(DEBUG_EMAIL,"Attempting to load texture dictionary (",GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(firstmail),") for email")
    
    
    REQUEST_STREAMED_TEXTURE_DICT(GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(firstmail))

    
    WHILE NOT HAS_STREAMED_TEXTURE_DICT_LOADED(GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(firstmail))
        CPRINTLN(DEBUG_EMAIL,"Waiting for email (",firstmail ,") texture dictionary(",GET_EMAIL_ATTACHMENT_TEXTURE_DICTIONARY_NAME(firstmail),") to finish loading")
        WAIT(100)
    ENDWHILE
    


ENDPROC


FUNC INT GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX(INT inboxIndex, INT selectedUIindex, INT &threadindex, INT &emaillogindex, BOOL &bRespondedTo)

    //find the mail in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
    INT baselinemail = g_Inboxes[inboxIndex].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    
    
    
    CPRINTLN(DEBUG_EMAIL,"GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX : Startat: ",invval)
    
    
    threadindex = g_Inboxes[inboxIndex].ThreadIndex[invval]
    emaillogindex = g_Inboxes[inboxIndex].EmailsLogIndex[invval]

    bRespondedTo = g_Inboxes[inboxIndex].HasFiredResponse[invval]

    CPRINTLN(DEBUG_EMAIL,"GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX : Thread and email value looked up : ",threadindex," ",emaillogindex)
    
    RETURN invval

ENDFUNC


FUNC BOOL DOES_INBOX_MAIL_HAVE_URL(enumCharacterList character, INT selectedUIindex)
    CPRINTLN(DEBUG_EMAIL,"DOES_INBOX_MAIL_HAVE_URL : uiIndex " ,selectedUIindex)
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        CPRINTLN(DEBUG_EMAIL,"DOES_INBOX_MAIL_HAVE_URL : no, invalid char index")
        RETURN FALSE
    ENDIF
    
    INT threadindex, emaillogindex 
    BOOL bResponded
    INT invval = GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX(index, selectedUIindex, threadindex, emaillogindex,bResponded)
    bResponded = bResponded
    
    INT firstmail = -1
    BOOL bIsDynamic = g_Inboxes[index].IsDynamic[invval]
    
    IF NOT bIsDynamic
        firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
        
    ELSE
        INT tind = -1
        INT j = 0
        REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
            IF g_DynamicEmailThreadBuffers[j].registrationID = threadindex
                tind = j
            ENDIF
        ENDREPEAT
        firstmail = ENUM_TO_INT(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].sourceEmail)
    ENDIF
    
    
    
    
    RETURN GET_DOES_EMAIL_HAVE_URL(firstmail)
    
ENDFUNC


FUNC STRING GET_EMAIL_IN_INBOX_URL(enumCharacterList character, INT selectedUIindex)

    CPRINTLN(DEBUG_EMAIL,"GET_EMAIL_IN_INBOX_URL : uiIndex " ,selectedUIindex)
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        CPRINTLN(DEBUG_EMAIL,"GET_EMAIL_IN_INBOX_URL : no, invalid char index")
        RETURN ""
    ENDIF
    BOOL bResponded
    INT threadindex, emaillogindex 
    INT invval = GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX(index, selectedUIindex, threadindex, emaillogindex,bResponded)
    bResponded = bResponded
    
    INT firstmail = -1
    BOOL bIsDynamic = g_Inboxes[index].IsDynamic[invval]
    
    IF NOT bIsDynamic
    
        firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
        
    ELSE
        INT tind = -1
        INT j = 0
        REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
            IF g_DynamicEmailThreadBuffers[j].registrationID = threadindex
                tind = j
            ENDIF
        ENDREPEAT
        firstmail = ENUM_TO_INT(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].sourceEmail)
    ENDIF
    
    RETURN GET_EMAIL_URL_STRING(firstmail)
ENDFUNC

FUNC EMAILER_ENUMS GET_CORRECT_EMAIL_TO_FIELD(enumCharacterList CurrentChar, EMAILER_ENUMS emailTo)
	IF emailTo = EMAILER_MICHAEL_DE_SANTO
		IF CurrentChar = CHAR_FRANKLIN
			RETURN EMAILER_FRANKLIN
		ELIF CurrentChar = CHAR_TREVOR
			RETURN EMAILER_TREVOR_PHILIPS
		ENDIF
	ENDIF
	RETURN emailTO
ENDFUNC

PROC FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED(SCALEFORM_INDEX sf, enumCharacterList character,INT selectedUIindex)

    BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT_EMPTY")
    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
    END_SCALEFORM_MOVIE_METHOD()
    
    
    CPRINTLN(DEBUG_EMAIL,"Attempting to fill out inbox mail: ",selectedUIindex)
    
    //TODO move this data into the charsheet later
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        EXIT
    ENDIF
    
    
    
    
    
    
    //g_bEmailSystemPaused = TRUE//pause because we're in the email
    /*
        To create an individual Email

        Emails are threaded and displayed in a scrolling html text field

        SET_DATA_SLOT

            * param 1 = View state enum
            * param 2 = Slot number
            * param 3 = String - To address
            * param 4 = String - From address
            * param 5 = String - Subject title
            * param 6 = String - Message body
            * param 7 = String - Sign off 

        SET_DATA_SLOT(9, 0, "TO: player_1@gtav.com", "FROM: frnkln69@eyefind.info", "Be there at 5.30 sharp", “Cheers
        Franklin”)
        SET_DATA_SLOT(9, 1, "TO: frnkln69@eyefind.info", "FROM: player_1@gtav.com", "Ok, See you tomorrow, what time?", “player_1”)
        DISPLAY_VIEW(9)
    */
    
    
    
/*
    //find the mail in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
    INT baselinemail = g_Inboxes[index].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    
    
    
    PRINTSTRING("Startat: ")
    PRINTINT(invval)
    PRINTNL()
    
    
    INT threadindex = g_Inboxes[index].ThreadIndex[invval]
    INT emaillogindex = g_Inboxes[index].EmailsLogIndex[invval]

    PRINTSTRING("Thread and email value looked up : ")
    PRINTINT(threadindex)
    PRINTSTRING(" ")
    PRINTINT(emaillogindex)
    PRINTNL()
    */
    BOOL bResponded
    INT threadindex, emaillogindex 
    INT invval = GET_THREAD_AND_EMAIL_INDICES_FROM_INBOX(index, selectedUIindex, threadindex, emaillogindex,bResponded)
    
    
    BOOL bIsDynamic = g_Inboxes[index].IsDynamic[invval]
    
    //its been viewed! hooray
    IF NOT g_Inboxes[index].HasBeenViewed[invval]
        UPDATE_EMAILS_READ_FOR_CURRENT_PLAYER(-1)
    ENDIF
    g_Inboxes[index].HasBeenViewed[invval] = TRUE
    
    ///REMOVE THIS
    // g_Inboxes[index].bDelete[invval] = TRUE
    //REMOVE THIS
    

    IF NOT bIsDynamic
        INT firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
        
        //check if the email has picture content
        
        CPRINTLN(DEBUG_EMAIL,"Uploading contents for g_AllEmails index: ",firstmail)

        //GET_EMAILER_ADDRESS_TAG//GET_EMAILER_NAME_TAG//GET_EMAILER_SIGNOFF_TAG

        //find the thread
        
        TEXT_LABEL_63 title = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].iTitleTag)
        TEXT_LABEL_63 body = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].iContentTag)
        
        /*
        LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sf,"SET_DATA_SLOT",
                                                            9,//* param 1 = View state enum
                                                            TO_FLOAT(0),//* param 2 = Slot number
                                                                     INVALID_SCALEFORM_PARAM ,
                                                                     INVALID_SCALEFORM_PARAM ,
                                                                     INVALID_SCALEFORM_PARAM ,
                                                                    GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eTo),//* param 3 = String - To address
                                                                    GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eFrom),//* param 4 = String - From address
                                                                    title,//* param 5 = String - Subject title
                                                                    body,//* param 6 = String - Message body
                                                                    GET_EMAILER_SIGNOFF_TAG(g_AllEmails[firstmail].eFrom)//* param 7 = String - Sign off 
                                                            ) */
        BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eTo))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eFrom))
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(title)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(body)
        SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_SIGNOFF_TAG(g_AllEmails[firstmail].eFrom))
        SET_EMAIL_BRANDING_ARGS_FROM_INDEX(firstmail,g_AllEmails[firstmail].eFrom)
        END_SCALEFORM_MOVIE_METHOD()

        //hide select if no responses
        IF g_AllEmails[firstmail].iResponses = 0
            CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
        ELSE
			//If responded of the thread has alread ended
            IF bResponded OR NOT CAN_A_THREAD_MAIL_FIRE_A_RESPONSE(INT_TO_ENUM(EMAILER_ENUMS,index),invval)
                CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
            ELSE
                CONFIGURE_EMAIL_SOFT_KEY_SELECT(TRUE)
            ENDIF
        ENDIF
        
        
        
        // read in all the previous thread mails
        CPRINTLN(DEBUG_EMAIL,"Mails in log: ",g_AllEmailThreads[threadindex].iMailsInLog)
        
        IF g_AllEmailThreads[threadindex].iMailsInLog > 1
            //start at emaillogindex and count down to zero
            //g_AllEmailThreads[threadindex].iMailsInLog //the added mails
            //
            
            INT times = emaillogindex
            INT countback = emaillogindex-1
            INT i = 0
            REPEAT times i  
                CPRINTLN(DEBUG_EMAIL,"Previous response : ",i)
                
                firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[countback]
                
                title = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].iTitleTag)
                body = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[firstmail].iContentTag)
                
                //Set the correct emailee address based on playing character
				EMAILER_ENUMS emailTo = GET_CORRECT_EMAIL_TO_FIELD(character, g_AllEmails[firstmail].eTo)
                
                
                

                
                /*
                LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING(sf,"SET_DATA_SLOT",
                                                                    9,//* param 1 = View state enum
                                                                    TO_FLOAT(i+1),//* param 2 = Slot number
                                                                            INVALID_SCALEFORM_PARAM ,
                                                                            INVALID_SCALEFORM_PARAM ,
                                                                            INVALID_SCALEFORM_PARAM ,
                                                                            GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eTo),//* param 3 = String - To address
                                                                            GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eFrom),//* param 4 = String - From address
                                                                            title,//* param 5 = String - Subject title
                                                                            body,//* param 6 = String - Message body
                                                                            GET_EMAILER_SIGNOFF_TAG(g_AllEmails[firstmail].eFrom)//* param 7 = String - Sign off 
                                                                    ) */
                BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i+1)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(emailTo))
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[firstmail].eFrom))
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(title)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(body)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_SIGNOFF_TAG(g_AllEmails[firstmail].eFrom))
                SET_EMAIL_BRANDING_ARGS_FROM_INDEX(firstmail,g_AllEmails[firstmail].eFrom)
                END_SCALEFORM_MOVIE_METHOD()
                
                CPRINTLN(DEBUG_EMAIL,"Countback : ",countback)
                
                CPRINTLN(DEBUG_EMAIL,"Mail : ",firstmail)
                
                countback--
            ENDREPEAT
            
        ENDIF
    
    ELSE//dynamic email

        INT tind = -1
        INT j = 0
        REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
            IF g_DynamicEmailThreadBuffers[j].registrationID = threadindex
                tind = j
            ENDIF
        ENDREPEAT
        IF tind = -1
            CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: Dynamic thread ID not found in buffer\n")
            EXIT
        ELSE

            //emaillogindex
            INT gmail = ENUM_TO_INT(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].sourceEmail)
            EMAIL_MESSAGE_ENUMS gmailEnum = g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].sourceEmail
            
    
            
            TEXT_LABEL_63 title = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[gmail].iTitleTag)
            TEXT_LABEL_63 body = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[gmail].iContentTag)
            IF g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].bOverrideContent
                body = g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].content
                CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: override content\n")
            ELSE
                CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: Not override content\n")
            ENDIF
            
            IF g_AllEmails[gmail].iResponses = 0
                CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
            ELSE
                IF bResponded
                    CONFIGURE_EMAIL_SOFT_KEY_SELECT(FALSE)
                ELSE
                    CONFIGURE_EMAIL_SOFT_KEY_SELECT(TRUE)
                ENDIF
            ENDIF
            
                
                BOOL bAlreadyDoneContent = FALSE
                bAlreadyDoneContent = PROPERTY_EMAIL_CALLBACK(sf, g_DynamicEmailThreadBuffers[tind].registrationID, emaillogindex , gmailEnum)
    
    
                IF !bAlreadyDoneContent
                BEGIN_SCALEFORM_MOVIE_METHOD(sf, "SET_DATA_SLOT")
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(9)
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                                                                        
                                                                        
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[gmail].eTo))    
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[gmail].eFrom))
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(title)
                    

                    
                    //IF !bAlreadyDoneContent
                        IF g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].iOverrideAdditional > 0
                            CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: substrng : " , body)
                            BEGIN_TEXT_COMMAND_SCALEFORM_STRING(body)
                            INT rep = 0
                            REPEAT g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].iOverrideAdditional rep
                                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_DynamicEmailThreadBuffers[tind].emails[emaillogindex].additional[rep])
                            ENDREPEAT               
                            END_TEXT_COMMAND_SCALEFORM_STRING()
                        ELSE
                            //no substrings
                            CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: no substrings\n")
                            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(body)
                        ENDIF
                    //ENDIF
                    SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_SIGNOFF_TAG(g_AllEmails[gmail].eFrom))
					
       				SET_EMAIL_BRANDING_ARGS_FROM_INDEX(gmail,g_AllEmails[gmail].eFrom) // ADDING THE ABILITY TO HAVE AN EMAIL AD BANNER IN A DYNAMIC EMAIL - CC
                    
                END_SCALEFORM_MOVIE_METHOD()
                CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_EMAIL_IN_INBOX_FOR_PED: override upload complete\n")
                ENDIF
            //ENDIF

        ENDIF

    ENDIF

ENDPROC


FUNC STRING GET_TXD_STRING_FROM_EMAILER(EMAILER_ENUMS fromchar, BOOL &foundTxtDict)
    foundTxtDict = TRUE
	
    SWITCH fromchar
        CASE EMAILER_MICHAEL_DE_SANTO
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_MICHAEL].picture)
        CASE EMAILER_FRANKLIN
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_FRANKLIN].picture)
        CASE EMAILER_TREVOR_PHILIPS
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_TREVOR].picture)
        CASE EMAILER_LESTER
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_LESTER].picture)
        CASE MARNIE_CULT
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_MARNIE].picture)
        CASE EMAILER_MAUDE
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_MAUDE].picture)
        CASE EMAILER_JIMMY_DE_SANTO
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_JIMMY].picture)
            
        CASE EMAILER_AMMUNATION
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_AMMUNATION].picture)
        CASE EMAILER_TOURIST_BOARD
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_LS_TOURIST_BOARD].picture)
        CASE EMAILER_LSC
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_LS_CUSTOMS].picture)
            
        CASE EMAILER_EVENTS_RACE
            RETURN "CHAR_CARSITE2"
            
        CASE EMAILER_DOCK_TEASE
            RETURN "CHAR_BOATSITE"
        
        CASE EMAILER_BANK_MAZE
            RETURN "CHAR_BANK_MAZE"
        CASE EMAILER_BANK_FLEECA
            RETURN "CHAR_BANK_FLEECA"
        CASE EMAILER_BANK_BOL
            RETURN "CHAR_BANK_BOL"
        CASE EMAILER_PROPERTY_MANAGEMENT
            RETURN "CHAR_MINOTAUR"
            
        CASE EMAILER_TRACEY_DE_SANTO
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_TRACEY].picture)
        CASE EMAILER_DAVE_NORTON
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_DAVE].picture)
        CASE EMAILER_AMANDA_DE_SANTA
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_AMANDA].picture)
        CASE EMAILER_NERVOUS_RON
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_RON].picture)
        CASE EMAILER_TANISHA_MARKS
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_TANISHA].picture)
        CASE EMAILER_DENISE
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_DENISE].picture)
        CASE EMAILER_LAMAR_DAVIES
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_LAMAR].picture)
        CASE EMAILER_PATRICA_MADRAZZO
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PATRICIA].picture)
        CASE EMAILER_SAEEDAKADAM
			//Steve T. Saeeda's enum entry is only used here so I'm reusing her for something else, so have a hardcoded reference to her picture label instead:
			RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION("CELL_E_381")
        CASE EMAILER_NIGEL
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_NIGEL].picture)

        CASE EMAILER_CULT_STORE
            RETURN "CHAR_EPSILON"
            
        CASE EMAILER_MILITARY_SITE
            RETURN "CHAR_MILSITE"
        CASE EMAILER_CAR_SITE
            RETURN "CHAR_CARSITE"
        CASE EMAILER_BOAT_SITE
            RETURN "CHAR_BOATSITE"
        CASE EMAILER_PLANE_SITE
            RETURN "CHAR_PLANESITE"
        CASE EMAILER_DR_FRIEDLANDER
            RETURN "CHAR_DR_FRIEDLANDER"
        CASE EMAILER_AUTO_SITE
            RETURN "CHAR_CARSITE2"
        CASE EMAILER_BIKE_SITE
            RETURN "CHAR_BIKESITE"
        CASE EMAILER_PRO_HOOKIES 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_BAR_HOOKIES].picture)
        CASE EMAILER_PRO_TOWING
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_TOWING_IMPOUND].picture)
        CASE EMAILER_PRO_TAXI 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_TAXI_LOT].picture)          
        CASE EMAILER_PRO_ARMS 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_ARMS_TRAFFICKING].picture)      
        CASE EMAILER_PRO_SONAR 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_SONAR_COLLECTIONS].picture)     
        CASE EMAILER_PRO_CAR_MOD 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_CAR_MOD_SHOP].picture)      
        CASE EMAILER_PRO_CIN_VINEWOOD 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_CINEMA_VINEWOOD].picture)       
        CASE EMAILER_PRO_CIN_DOWNTOWN 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_CINEMA_DOWNTOWN].picture)       
        CASE EMAILER_PRO_CIN_MORNINGWOOD 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_CINEMA_MORNINGWOOD].picture)        
        CASE EMAILER_PRO_GOLF_CLUB 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_GOLF_CLUB].picture)     
        CASE EMAILER_PRO_CAR_SCRAP 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_CAR_SCRAP_YARD].picture)        
        CASE EMAILER_PRO_WEED 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_WEED_SHOP].picture)     
        CASE EMAILER_PRO_TEQUILALA 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_BAR_TEQUILALA].picture)     
        CASE EMAILER_PRO_PITCHERS 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_BAR_PITCHERS].picture)      
        CASE EMAILER_PRO_HEN_HOUSE 
            RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sCharacterSheetAll[CHAR_PROPERTY_BAR_HEN_HOUSE].picture)              
    ENDSWITCH
    
    foundTxtDict = FALSE
    RETURN "ERROR!"
ENDFUNC


PROC EMAIL_FEED_ID_BUFFER_PURGE()
	INT i = 0
	REPEAT MAX_EMAIL_FEED_BUFFER_ENTRIES i
	    IF g_iEmailFeedBuffer[i] != -1
	        CPRINTLN(DEBUG_EMAIL,"EMAIL_FEED_ID_BUFFER_PURGE: removing index ",i, " with feed id ",  g_iEmailFeedBuffer[i])
	        THEFEED_REMOVE_ITEM(g_iEmailFeedBuffer[i])
	        g_iEmailFeedBuffer[i] = -1
	    ENDIF
	ENDREPEAT
	g_iEmailfeedBufferCaret = 0
ENDPROC
 
 
PROC EMAIL_FEED_ID_BUFFER_PUSH(INT pedantry)
    CPRINTLN(DEBUG_EMAIL,"EMAIL_FEED_ID_BUFFER_PUSH: logged feed ID : ", pedantry)
    g_iEmailFeedBuffer[g_iEmailFeedBufferCaret] = pedantry

       
    //Added by Steve T for d-pad up auto selection work.
       g_Last_App_ActivatableType = ACT_APP_EMAIL
       g_Last_App_ActivatableFeedID = pedantry


    ++g_iEmailFeedBufferCaret
    IF g_iEmailFeedBufferCaret = MAX_EMAIL_FEED_BUFFER_ENTRIES
        g_iEmailFeedBufferCaret = 0
    ENDIF
ENDPROC
 
 
PROC FEEDIFY_EMAIL_MESSAGE(	enumCharacterList forChar, 
							EMAILER_ENUMS fromchar,
							EMAIL_MESSAGE_ENUMS mail, 
							STRING fstr, 
							STRING ss1 = NULL, 
							STRING ss2 = NULL, 
							STRING ss3 = NULL, 
							STRING ss4 = NULL, 
							STRING ss5 = NULL, 
							STRING ss6 = NULL, 		// - CC
							STRING ss7 = NULL, 
							STRING ss8 = NULL, 
							STRING ss9 = NULL, 
							STRING ss10 = NULL) 	// - CC
							
	CPRINTLN(DEBUG_EMAIL, "<EMAIL_FEED> Creating feed message for email.")
    IF IS_CUTSCENE_PLAYING()
        CPRINTLN(DEBUG_EMAIL, "<EMAIL_FEED> Failed. A cutscene was playing.")
        EXIT
    ENDIF
	
    enumCharacterList currPlayer= GET_CURRENT_PLAYER_PED_ENUM()
    
    BOOL bFoundTextureDictionary = FALSE
    TEXT_LABEL_63 txdict = GET_TXD_STRING_FROM_EMAILER(fromchar, bFoundTextureDictionary)
    
    
    IF currplayer = forChar
		CPRINTLN(DEBUG_EMAIL, "<EMAIL_FEED> Player is the correct charater to receive feed.")
        SWITCH mail
            CASE PROPMAN_TO_MIKE
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Creating default feed post to Michael.")
                BEGIN_TEXT_COMMAND_THEFEED_POST("PROPR_INCEMAIL1")
            BREAK
            CASE PROPMAN_TO_FRANK
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Creating default feed post to Franklin.")
                BEGIN_TEXT_COMMAND_THEFEED_POST("PROPR_INCEMAIL3")
            BREAK
            CASE PROPMAN_TO_TREV
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> SCreating default feed post to Trevor.")
                BEGIN_TEXT_COMMAND_THEFEED_POST("PROPR_INCEMAIL2")
            BREAK
            DEFAULT
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Starting feed post.")
                BEGIN_TEXT_COMMAND_THEFEED_POST(fstr)
                IF !IS_STRING_NULL_OR_EMPTY(ss1)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 1.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss1)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss2)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 2.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss2)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss3)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 3.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss3)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss4)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 4.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss4)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss5)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 5.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss5)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss6)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 6.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss6)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss7)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 7.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss7)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss8)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 8.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss8)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss9)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 9.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss9)
                ENDIF
                IF !IS_STRING_NULL_OR_EMPTY(ss10)
					CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Adding substring 10.")
                    ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(ss10)
                ENDIF
            BREAK
        ENDSWITCH
 
        IF bFoundTextureDictionary
			CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Ending feed post with custom texture dictionary.")
            EMAIL_FEED_ID_BUFFER_PUSH(END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(txdict, txdict, FALSE, TEXT_ICON_EMAIL, GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_EMAILER_NAME_TAG(fromchar))))
        ELSE
			CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Ending feed post with default texture dictionary.")
            EMAIL_FEED_ID_BUFFER_PUSH(END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_DEFAULT", "CHAR_DEFAULT", FALSE, TEXT_ICON_EMAIL,  GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_EMAILER_NAME_TAG(fromchar))))
        ENDIF

         SWITCH g_Cellphone.PhoneOwner 
            CASE CHAR_MICHAEL 
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Playing Michael feed sound.")
                g_Owner_Soundset = "Phone_SoundSet_Michael"
                ++g_iUnreadEmailsSP0
                IF g_iUnreadEmailsSP0 > MAX_INBOX_LOGGED_MAILS_THREADS
                    g_iUnreadEmailsSP0 = MAX_INBOX_LOGGED_MAILS_THREADS
                ENDIF
            BREAK
            CASE CHAR_TREVOR
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Playing Trevor feed sound.")
                g_Owner_Soundset = "Phone_SoundSet_Trevor"
                ++g_iUnreadEmailsSP2
                IF g_iUnreadEmailsSP2 > MAX_INBOX_LOGGED_MAILS_THREADS
                    g_iUnreadEmailsSP2 = MAX_INBOX_LOGGED_MAILS_THREADS
                ENDIF
            BREAK
            CASE CHAR_FRANKLIN
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Playing Franklin feed sound.")
                g_Owner_Soundset = "Phone_SoundSet_Franklin"             
                ++g_iUnreadEmailsSP1
                IF g_iUnreadEmailsSP1 > MAX_INBOX_LOGGED_MAILS_THREADS
                    g_iUnreadEmailsSP1 = MAX_INBOX_LOGGED_MAILS_THREADS
                ENDIF
            BREAK
            DEFAULT
				CDEBUG1LN(DEBUG_EMAIL, "<EMAIL_FEED> Playing default feed sound.")
                g_Owner_Soundset = "Phone_SoundSet_Default"          
            BREAK
        ENDSWITCH 
        PLAY_SOUND_FRONTEND (-1, "Notification", g_Owner_Soundset)

    ELSE
		CPRINTLN(DEBUG_EMAIL, "<EMAIL_FEED> Failed. Player was the wrong character.")
    ENDIF
ENDPROC
 
 
/// PURPOSE:
///    log the current thread state as recived to the inbox of the given mailer
/// PARAMS:
/////    mailer - the reciver or CC'd 
///    thread_index - 
PROC LOG_EMAIL_THREAD_SNAPSHOT_TO_INBOX(EMAILER_ENUMS mailer,INT thread_index, BOOL dynamic = FALSE, BOOL delayFeedification = FALSE, BOOL bNoFeedMessage = FALSE)

    IF NOT (ENUM_TO_INT(mailer) < TOTAL_INBOXES)
		CPRINTLN(DEBUG_EMAIL, "Email thread participant doesn't have an inbox, skipping snapshot.")
        EXIT
    ENDIF

    INT indextostoreto = -1
    EMAILER_ENUMS frommailer
    EMAIL_MESSAGE_ENUMS mailenum
    TEXT_LABEL_63 emcotag = "UNSET"
    IF NOT dynamic
		CPRINTLN(DEBUG_EMAIL, "Logging email thread to an inbox - Static version.")
        INT iMailLogIndex = g_AllEmailThreads[thread_index].iMailsInLog-1
        IF iMailLogIndex < 0
            CPRINTLN(DEBUG_EMAIL, "Failed. Thread has no emails in log.")
            EXIT
        ENDIF
        INT emailindex = g_AllEmailThreads[thread_index].ThreadEmailLog[iMailLogIndex]
        mailenum = INT_TO_ENUM(EMAIL_MESSAGE_ENUMS,emailindex)
        emcotag = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[emailindex].iContentTag)
		CPRINTLN(DEBUG_EMAIL, "Static - emailindex ", emailindex ," : mailenum ", mailenum," : contag ", emcotag, ".")

        IF g_AllEmails[emailindex].eFrom = mailer AND (NOT (g_AllEmails[emailindex].eTo = mailer))
			CPRINTLN(DEBUG_EMAIL, "Email snapshot filtered.")
            EXIT // this is an email sent by this emailer and not to themselves, don't put it in this inbox
        ENDIF
        frommailer = g_AllEmails[emailindex].eFrom 
        
        //remember to rollover when max index is hit
        indextostoreto = g_Inboxes[mailer].iTotalMails
        
        //
        BOOL rollover = FALSE
        WHILE indextostoreto >= MAX_INBOX_LOGGED_MAILS_THREADS
            indextostoreto -= MAX_INBOX_LOGGED_MAILS_THREADS
            rollover = TRUE
        ENDWHILE
        
        IF rollover
            IF NOT g_Inboxes[mailer].HasBeenViewed[indextostoreto]
                //If this is a reused index and has not been viewed, then decrement emails count
                SWITCH mailer
				
                    CASE EMAILER_MICHAEL_DE_SANTO
                        g_iUnreadEmailsSP0--
                        IF g_iUnreadEmailsSP0 < 0
                            g_iUnreadEmailsSP0 = 0
                        ENDIF
                    BREAK
					
                    CASE EMAILER_FRANKLIN
                        g_iUnreadEmailsSP1--
                        IF g_iUnreadEmailsSP1 < 0
                            g_iUnreadEmailsSP1 = 0
                        ENDIF
                    BREAK
					
                    CASE EMAILER_TREVOR_PHILIPS
                        g_iUnreadEmailsSP2--
                        IF g_iUnreadEmailsSP2 < 0
                            g_iUnreadEmailsSP2 = 0
                        ENDIF
                    BREAK
					
                ENDSWITCH
            ENDIF
        ENDIF
        
        g_Inboxes[mailer].ThreadIndex[indextostoreto] = thread_index
        
        g_Inboxes[mailer].EmailsLogIndex[indextostoreto] = (iMailLogIndex)
        g_Inboxes[mailer].HasFiredResponse[indextostoreto] = FALSE
        g_Inboxes[mailer].IsDynamic[indextostoreto] = FALSE
        
        g_Inboxes[mailer].HasBeenViewed[indextostoreto] = FALSE
        
        ++g_Inboxes[mailer].iTotalMails
        

    ELSE//Dynamic snapshot
    
        CPRINTLN(DEBUG_EMAIL, "Logging email thread to an inbox - Dynamic version.")
        
		//remember to rollover when max index is hit
        indextostoreto = g_Inboxes[mailer].iTotalMails
        BOOL rollover = FALSE
        WHILE indextostoreto >= MAX_INBOX_LOGGED_MAILS_THREADS
            indextostoreto -= MAX_INBOX_LOGGED_MAILS_THREADS
            rollover = TRUE
        ENDWHILE
        
        IF rollover
            IF NOT g_Inboxes[mailer].HasBeenViewed[indextostoreto]
                //If this is a reused index and has not been viewed, then decrement emails count
                SWITCH mailer
                    
					CASE EMAILER_MICHAEL_DE_SANTO
                        g_iUnreadEmailsSP0--
                        IF g_iUnreadEmailsSP0 < 0
                            g_iUnreadEmailsSP0 = 0

                        ENDIF
                    BREAK
					
                    CASE EMAILER_FRANKLIN
                        g_iUnreadEmailsSP1--
                        IF g_iUnreadEmailsSP1 < 0
                            g_iUnreadEmailsSP1 = 0

                        ENDIF
                    BREAK
					
                    CASE EMAILER_TREVOR_PHILIPS
                        g_iUnreadEmailsSP2--
                        IF g_iUnreadEmailsSP2 < 0
                            g_iUnreadEmailsSP2 = 0

                        ENDIF
                    BREAK
					
                ENDSWITCH
            ENDIF
        ENDIF
        
        //check that this thread is still in at least one of the dynamic buffers
        INT dynamicFoundAt = -1
        INT i = 0
        REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i 
            IF g_DynamicEmailThreadBuffers[i].registrationID = thread_index
            AND g_DynamicEmailThreadBuffers[i].iProgress > 0
                dynamicFoundAt = i
            ENDIF 
        ENDREPEAT
        
        //thread not valid anymore, cannot snapshot
        IF dynamicFoundAt = -1
            CPRINTLN(DEBUG_EMAIL, "Failed to find dynamic thread buffer. Dynamic snapshot failed.")
            EXIT 
        ENDIF
		
		CPRINTLN(DEBUG_EMAIL, "Attempting snapshot of dynamic thread ", dynamicFoundAt, " at inbox index ", ENUM_TO_INT(mailer), " with progress ", ENUM_TO_INT(g_DynamicEmailThreadBuffers[dynamicFoundAt].iProgress), ".")

        //thread index now applies to g_DynamicEmailBuffers reg index
        g_Inboxes[mailer].ThreadIndex[indextostoreto] = g_DynamicEmailThreadBuffers[dynamicFoundAt].registrationID
        
        g_Inboxes[mailer].EmailsLogIndex[indextostoreto] = g_DynamicEmailThreadBuffers[dynamicFoundAt].iProgress-1
        g_Inboxes[mailer].HasFiredResponse[indextostoreto] = FALSE
        g_Inboxes[mailer].IsDynamic[indextostoreto] = TRUE
        
        g_Inboxes[mailer].HasBeenViewed[indextostoreto] = FALSE
        
        //also remember to set dynamic bool     
        ++g_Inboxes[mailer].iTotalMails
        
        INT s = g_Inboxes[mailer].EmailsLogIndex[indextostoreto]
        
        EMAIL_MESSAGE_ENUMS fromail = g_DynamicEmailThreadBuffers[dynamicFoundAt].emails[s].sourceEmail
        mailenum = fromail
        frommailer = g_AllEmails[fromail].eFrom
        
        IF g_DynamicEmailThreadBuffers[dynamicFoundAt].emails[g_DynamicEmailThreadBuffers[dynamicFoundAt].iProgress-1].bOverrideContent
            emcotag = g_DynamicEmailThreadBuffers[dynamicFoundAt].emails[g_DynamicEmailThreadBuffers[dynamicFoundAt].iProgress-1].content
			CDEBUG1LN(DEBUG_EMAIL, "Dynamic email content is overridden to: ", emcotag)
        ELSE//otherwise its not a content override, use the raw mail content
            //EMAIL_MESSAGE_ENUMS fromail = g_DynamicEmailThreadBuffers[dynamicFoundAt].emails[s].sourceEmail
            emcotag = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[fromail].iContentTag)
			CDEBUG1LN(DEBUG_EMAIL, "Dynamic email content is not overridden : ", emcotag)
        ENDIF
    ENDIF
    
    #IF IS_DEBUG_BUILD
        //Don't add any messages to the feed during a gameflow launch. -BenR
        IF NOT g_flowUnsaved.bUpdatingGameflow
    #ENDIF
    
    IF NOT bNoFeedMessage
		CPRINTLN(DEBUG_EMAIL, "About to attempt feedification, viewed at index ", g_Inboxes[mailer].HasBeenViewed[indextostoreto]," at ",indextostoreto," with delay setting ", delayFeedification, ".")
		
        IF (NOT g_Inboxes[mailer].HasBeenViewed[indextostoreto]) 
        AND (NOT delayFeedification)
            SWITCH mailer
                CASE EMAILER_MICHAEL_DE_SANTO
                    FEEDIFY_EMAIL_MESSAGE(CHAR_MICHAEL, frommailer, mailenum, emcotag)//used to be frommailer GET_EMAILER_NAME_TAG(frommailer)
                BREAK
                
				CASE EMAILER_FRANKLIN
					IF mailenum = WILDLIFE_INITIAL	// CC
						// SPECIAL CASE TO STOP ENTIRE INITIAL WILDLIFE PHOTOGRAPHY DYNAMIC EMAIL APPEARING IN FEED AND BRIEF MENU
	                    FEEDIFY_EMAIL_MESSAGE(CHAR_FRANKLIN, frommailer, mailenum, "PW_FEED_EM_1")
					ELSE
	                    FEEDIFY_EMAIL_MESSAGE(CHAR_FRANKLIN, frommailer, mailenum, emcotag) //now is content tag
					ENDIF
                BREAK
                
				CASE EMAILER_TREVOR_PHILIPS
                	FEEDIFY_EMAIL_MESSAGE(CHAR_TREVOR, frommailer, mailenum, emcotag) 
                BREAK
            ENDSWITCH
        ENDIF
    ENDIF
    
#IF IS_DEBUG_BUILD
    ENDIF
#ENDIF

ENDPROC


FUNC BOOL ADD_MAIL_TO_THREAD_LOG(INT thread_index, INT email_index)
	CPRINTLN(DEBUG_EMAIL, "Adding email to thread log. Thread index: " , thread_index, " Email index:", email_index ," Log index:", g_AllEmailThreads[thread_index].iMailsInLog, ".")

    IF (g_AllEmailThreads[thread_index].iMailsInLog >= MAX_THREAD_LOG_LENGTH)
        CPRINTLN(DEBUG_EMAIL, "Failed: No space left in thread email log!")
        SCRIPT_ASSERT("No space left in thread email log!")
        RETURN FALSE
    ENDIF
    
    g_AllEmailThreads[thread_index].ThreadEmailLog[g_AllEmailThreads[thread_index].iMailsInLog] = email_index
    ++g_AllEmailThreads[thread_index].iMailsInLog
    
    RETURN TRUE
ENDFUNC


// Progress thread - automatically called after a thread delay finishes, or manually to override a block.
PROC PROGRESS_EMAIL_THREAD(INT thread_index, BOOL bNoFeedMessage = FALSE)
	CPRINTLN(DEBUG_EMAIL, "Attempting to progress an email thread ", thread_index, ".")
	
    //Is the thread over?
    IF (NOT g_AllEmailThreads[thread_index].bActive) AND (NOT g_AllEmailThreads[thread_index].bEnded)
        //Thread over or not started.
		CPRINTLN(DEBUG_EMAIL, "Failed: Thread is over or not started.")
        EXIT
    ENDIF
    
    //No more mails in the stack.
    IF (g_AllEmailThreads[thread_index].iCurrentlyAt = g_AllEmailThreads[thread_index].iTotalThreadMails)
		CPRINTLN(DEBUG_EMAIL, "Thread is out of mails on the stack, cannot update.")
        
        INT iMailLogIndex = g_AllEmailThreads[thread_index].iMailsInLog-1
        IF iMailLogIndex < 0
			CPRINTLN(DEBUG_EMAIL, "Failed: Thread has no emails in log, something has gone badly wrong.")
            EXIT
        ENDIF
		
        //Check for current response pending.
        IF g_AllEmails[g_AllEmailThreads[thread_index].ThreadEmailLog[iMailLogIndex]].iResponses = 0
			CPRINTLN(DEBUG_EMAIL, "Failed: Thread has no responses pending either, ending it.")
            g_AllEmailThreads[thread_index].bEnded = TRUE
        ENDIF
        EXIT
    ENDIF
	
    //Thread has ended.
    IF g_AllEmailThreads[thread_index].bEnded
		CPRINTLN(DEBUG_EMAIL, "Failed: Thread has already ended.")
        EXIT
    ENDIF
    
    INT nextemail  = g_AllEmailThreads[thread_index].ThreadMails[g_AllEmailThreads[thread_index].iCurrentlyAt]
    g_AllEmailThreads[thread_index].iCurrentlyAt++
	
    //Add the next mail to the log and progress it.
   	CPRINTLN(DEBUG_EMAIL, "Progressing with ", thread_index," and ", nextemail, ".")
    ADD_MAIL_TO_THREAD_LOG(thread_index,nextemail)
	
    //Apply email effects/rules to the thread.
    g_AllEmailThreads[thread_index].bBLockedUntilResponse = g_AllEmails[nextemail].bEmailBlocksThread
    g_AllEmailThreads[thread_index].iDelay = g_AllEmails[nextemail].iDelay
       
    INT totalparticipants = g_AllEmailThreads[thread_index].iParticipants

    LOG_EMAIL_THREAD_SNAPSHOT_TO_INBOX(g_AllEmails[nextemail].eTo, thread_index, FALSE, FALSE, bNoFeedMessage)
	
	INT i
    REPEAT totalparticipants i
        IF g_AllEmailThreads[thread_index].Participants[i] != g_AllEmails[nextemail].eFrom
        AND g_AllEmailThreads[thread_index].Participants[i] != g_AllEmails[nextemail].eTo
		
			CDEBUG1LN(DEBUG_EMAIL, "Logging participant ", i," with thread ", thread_index)
            LOG_EMAIL_THREAD_SNAPSHOT_TO_INBOX(	g_AllEmailThreads[thread_index].Participants[i],
                                               	thread_index,
											   	FALSE,
											   	FALSE,
											   	bNoFeedMessage)
												
        ENDIF
    ENDREPEAT
        
    //If no more mails in the stack.
    IF g_AllEmailThreads[thread_index].iCurrentlyAt = g_AllEmailThreads[thread_index].iTotalThreadMails
        //If no responses in current mail.
        IF g_AllEmails[g_AllEmailThreads[thread_index].ThreadEmailLog[g_AllEmailThreads[thread_index].iMailsInLog-1]].iResponses = 0
            //End the thread
			CPRINTLN(DEBUG_EMAIL, "No more emails in the stack and no responses in the current mail. Ending email thread.")
            g_AllEmailThreads[thread_index].bEnded = TRUE
            EXIT
        ENDIF
    ENDIF
	
	CPRINTLN(DEBUG_EMAIL, "Thread progression finished.")
ENDPROC


// Reset thread.
PROC RESET_EMAIL_THREAD_INT(INT thread_index)
    CDEBUG1LN(DEBUG_EMAIL, "Resetting email thread ", thread_index, ".")
    
    g_AllEmailThreads[thread_index].bActive = FALSE
    g_AllEmailThreads[thread_index].iCurrentlyAt = 0
    g_AllEmailThreads[thread_index].iMailsInLog = 0
    g_AllEmailThreads[thread_index].bBLockedUntilResponse = FALSE
    g_AllEmailThreads[thread_index].iDelay = 0
    g_AllEmailThreads[thread_index].bEnded = FALSE
ENDPROC


// Start thread.
PROC BEGIN_EMAIL_THREAD_INT(INT thread_index, BOOL bNoFeedMessage = FALSE)
    //Note: be careful when changing the reset conditions as possibility for recursion dwells here
    g_bEmailSystemUpdated= TRUE
    IF (NOT g_AllEmailThreads[thread_index].bActive) AND (NOT g_AllEmailThreads[thread_index].bEnded)
        CPRINTLN(DEBUG_EMAIL, "Starting email thread ", thread_index, ".")
        //activate it
        g_AllEmailThreads[thread_index].bActive = TRUE
        //prime the pump with the first email
        PROGRESS_EMAIL_THREAD(thread_index,bNoFeedMessage)
    ELSE
        //thread is already active or ended, reset it then begin it
		CPRINTLN(DEBUG_EMAIL, "Attempting to begin email thread that is active or already over, attempting reset.")
        RESET_EMAIL_THREAD_INT(thread_index)
        BEGIN_EMAIL_THREAD_INT(thread_index,bNoFeedMessage)
    ENDIF
ENDPROC

//Stop or end a thread regardless of progress
PROC END_EMAIL_THREAD(INT thread_index,BOOL bNoFeedMessage = FALSE)
	bNoFeedMessage = bNoFeedMessage
	g_bEmailSystemUpdated = TRUE
	IF (g_AllEmailThreads[thread_index].bActive) AND (NOT g_AllEmailThreads[thread_index].bEnded)
		CPRINTLN(debug_email,"Ending email thread ",thread_index," at index ",g_AllEmailThreads[thread_index].iCurrentlyAt,"/",g_AllEmailThreads[thread_index].iTotalThreadMails-1)
		//End it
		g_AllEmailThreads[thread_index].bEnded = TRUE
		g_AllEmailThreads[thread_index].bActive= FALSE
	ELSE
		CPRINTLN(debug_email,"Attempted to stop email thread that was not started or active, no changes made")
	ENDIF
ENDPROC


// Fire thread response.
PROC FIRE_EMAIL_THREAD_RESPONSE(INT thread_index,INT response_index)
	CPRINTLN(DEBUG_EMAIL, "Firing email thread response for thread ", thread_index, ".")
	
    IF (NOT g_AllEmailThreads[thread_index].bActive) AND (NOT g_AllEmailThreads[thread_index].bEnded)
        //thread is ended or inactive
        EXIT
    ENDIF

    //is the response valid?
    
    
    INT iMailLogIndex = g_AllEmailThreads[thread_index].iMailsInLog-1
    IF iMailLogIndex < 0
		CPRINTLN(DEBUG_EMAIL, "Thread has no emails in log. Cannot begin a thread with a response.")
        SCRIPT_ASSERT("FIRE_EMAIL_THREAD_RESPONSE: Thread has no emails in log. Cannot begin a thread with a response.")
        EXIT
    ENDIF
    
    INT findmail =  g_AllEmailThreads[thread_index].ThreadEmailLog[iMailLogIndex]
    
    IF NOT(response_index < g_AllEmails[findmail].iResponses)
        //response index is not smaller than total responses! invalid fail
		CPRINTLN(DEBUG_EMAIL, "Response index is not smaller than total responses in the thread. Failed to add response.")
        SCRIPT_ASSERT("FIRE_EMAIL_THREAD_RESPONSE: Response index is not smaller than total responses in the thread. Failed to add response.")
        EXIT
    ENDIF
    
    //find the response email's index
    INT responsemail = g_AllEmails[findmail].Responses[response_index].iResponseMail
	CPRINTLN(debug_email,"Find:",findmail,"Response/Index:",responsemail,"/",response_index)
    CPRINTLN(debug_email,"Thread ",thread_index," at:",g_AllEmailThreads[thread_index].iCurrentlyAt,"/",g_AllEmailThreads[thread_index].iTotalThreadMails-1)
    //is the response email the same as the next one in the thread? If so don't bother logging this
    BOOL bForbidLog = FALSE
    
	
    IF g_AllEmailThreads[thread_index].iCurrentlyAt < g_AllEmailThreads[thread_index].iTotalThreadMails-1
        INT nextmail = g_AllEmailThreads[thread_index].ThreadMails[g_AllEmailThreads[thread_index].iCurrentlyAt]
		CPRINTLN(debug_email,"Next:",nextmail)
        IF responsemail = nextmail
            bForbidLog = TRUE
        ENDIF
    ENDIF
    
    IF NOT bForbidLog
        IF responsemail > -1
            //fire the email response into the thread log
            ADD_MAIL_TO_THREAD_LOG(thread_index, responsemail)
            //send the email/thread to the right inboxes
            
            INT totalparticipants = g_AllEmailThreads[thread_index].iParticipants
            //log the email and thread in the inbox of all participants apart from the sender //perhaps have an outbox for sender
            INT i = 0
            INT senderindex = ENUM_TO_INT(g_AllEmails[responsemail].eFrom)
            REPEAT totalparticipants i
                IF NOT (i = senderindex)
                    LOG_EMAIL_THREAD_SNAPSHOT_TO_INBOX(g_AllEmailThreads[thread_index].Participants[i],thread_index)
                ELSE
                    //todo, place email in outbox
                ENDIF
            ENDREPEAT
        ELSE
			CPRINTLN(DEBUG_EMAIL, "Response doesn't log an email.")
        ENDIF
		
#IF IS_DEBUG_BUILD
    ELSE
    	CPRINTLN(DEBUG_EMAIL, "Logging of this response forbidden as it is the same email as the next in the thread")
#ENDIF

    ENDIF
    
    
    IF g_AllEmails[findmail].Responses[response_index].iResponseFiresThread > -1
        BEGIN_EMAIL_THREAD_INT(g_AllEmails[findmail].Responses[response_index].iResponseFiresThread)
        
		CPRINTLN(DEBUG_EMAIL, 	"Response kicking off thread with index: ", g_AllEmails[findmail].Responses[response_index].iResponseFiresThread, 
								" Email: ", findmail, " Response: ", response_index, ".")
#IF IS_DEBUG_BUILD
    ELSE
		CPRINTLN(DEBUG_EMAIL, 	"Response fires no thread. Thread: ", g_AllEmails[findmail].Responses[response_index].iResponseFiresThread, 
								" Email: ", findmail, " Response: ", response_index, ".")
#ENDIF
    ENDIF
    
 
    //Apply the response's special effects.
    IF(g_AllEmails[findmail].Responses[response_index].bResponseEndsThread)
        //The response ended the thread, kill it.
         g_AllEmailThreads[thread_index].bEnded = TRUE
    ELSE
        //If the response did not end the thread then apply the email's special effects to the thread.
        IF responsemail > -1
            g_AllEmailThreads[thread_index].bBLockedUntilResponse = g_AllEmails[responsemail].bEmailBlocksThread
            g_AllEmailThreads[thread_index].iDelay = g_AllEmails[responsemail].iDelay
        ELSE
            g_AllEmailThreads[thread_index].bBLockedUntilResponse = FALSE //Otherwise there was no email to add, unblock thread and let counter finish up.
        ENDIF
    ENDIF
    
    IF g_AllEmailThreads[thread_index].iDelay < 30000
        g_AllEmailThreads[thread_index].iDelay = 30000
    ENDIF
ENDPROC




PROC RESET_EMAIL_THREAD(EMAIL_THREAD_ENUMS id)

    RESET_EMAIL_THREAD_INT(ENUM_TO_INT(id))

ENDPROC


PROC BEGIN_EMAIL_THREAD(EMAIL_THREAD_ENUMS id,bool bNoFeedMessage = false)

    BEGIN_EMAIL_THREAD_INT(ENUM_TO_INT(id),bNoFeedMessage)

ENDPROC


/// PURPOSE:
///    Jump the thread to the next instance of the specified email relative to it's current stage
/// PARAMS:
///    t - thread target
///    e - email to find
///    fromstart - if true then check from start rather than current point
///    index - instance of that email to find, 0 being the first
/// RETURNS:
///    True if the jump succeeded
///    False if the mail was not found
FUNC BOOL JUMP_EMAIL_THREAD_TO_STAGE(EMAIL_THREAD_ENUMS t, EMAIL_MESSAGE_ENUMS e,BOOL fromstart = FALSE, INT index = 0) 
	//Does the email exist past the thread's current point?
    INT ifound = -1
    
    INT icountdown = index
    
    INT i = 0
    INT startAt =0
    IF NOT fromstart
        startAt = g_AllEmailThreads[t].iCurrentlyAt
    ENDIF
     
    FOR i = startAt TO g_AllEmailThreads[t].iTotalThreadMails STEP 1
        //icountdown
        IF ifound = -1
            IF g_AllEmailThreads[t].ThreadMails[i] = ENUM_TO_INT(e)//found a match
                //g_AllEmailThreads[t]
                
                IF icountdown = 0
                    ifound = i
                ELSE
                    --icountdown
                ENDIF
            ENDIF
        ENDIF
    ENDFOR
    
    //Check for currently at
    IF ifound = -1
        RETURN FALSE
    ENDIF

    //If so then loop and progress the thread until it's current email is that point
    icountdown = index
    
    BOOL done = FALSE
    
    IF fromstart
        RESET_EMAIL_THREAD(t)
        BEGIN_EMAIL_THREAD(t)
    ENDIF
    
    WHILE NOT done
        //TODO, this will need changing is response emails are re-enabled
        IF g_AllEmailThreads[t].ThreadMails[g_AllEmailThreads[t].iCurrentlyAt] = ENUM_TO_INT(e)
            
            IF icountdown = 0
                done = TRUE
            ELSE
                --icountdown
                PROGRESS_EMAIL_THREAD(ENUM_TO_INT(t))               
            ENDIF
        
        ELSE
            PROGRESS_EMAIL_THREAD(ENUM_TO_INT(t))
        ENDIF
    ENDWHILE
    
    RETURN TRUE
ENDFUNC

FUNC INT GET_EMAIL_INDEX_FROM_MAILER_INBOX(EMAILER_ENUMS mailer,INT inbox_index)
    INT maillogindex = g_Inboxes[mailer].EmailsLogIndex[inbox_index]
    INT mailthreadindex = g_Inboxes[mailer].ThreadIndex[inbox_index]
    RETURN g_AllEmailThreads[mailthreadindex].ThreadEmailLog[maillogindex]
ENDFUNC



//query for reply
FUNC BOOL DOES_EMAIL_HAVE_REPLIES(INT email_index)
    IF g_AllEmails[email_index].iResponses > 0
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF
ENDFUNC



FUNC BOOL SCALEFORM_CHECK_FOR_RESPONSE_ALLOWED(enumCharacterList character,INT selectedUIindex)
        INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   //this will get changed later   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        RETURN FALSE
    ENDIF
    //find the thread in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
    INT baselinemail = g_Inboxes[index].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    
    
    
    IF NOT g_Inboxes[index].IsDynamic[invval]
    
        INT threadindex = g_Inboxes[index].ThreadIndex[invval]
        INT emaillogindex = g_Inboxes[index].EmailsLogIndex[invval]
        
        
        INT firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
        
		
        
        //does the email have responses?
        IF (g_AllEmails[firstmail].iResponses = 0)
            RETURN FALSE
        ENDIF
        
        //has it already fired a response?
        IF g_Inboxes[index].HasFiredResponse[invval] = TRUE
            RETURN FALSE
        ENDIF
		
		//Is it allowed to fire a response now?
		IF NOT CAN_A_THREAD_MAIL_FIRE_A_RESPONSE(INT_TO_ENUM(EMAILER_ENUMS,index),invval)
			RETURN FALSE
		ENDIF
        
        RETURN TRUE
    ELSE
        CPRINTLN(DEBUG_EMAIL,"SCALEFORM_CHECK_FOR_RESPONSE_ALLOWED: TODO: check dynamic email for response")
        RETURN FALSE
    
    ENDIF
ENDFUNC

PROC SCALEFORM_FIRE_RESPONSE(enumCharacterList character,INT selectedUIindex,INT selectedResponseIndex)
    CPRINTLN(DEBUG_EMAIL,"\nSCALEFORM_FIRE_RESPONSE: Scaleform triggering response : ",selectedUIindex," ",selectedResponseIndex)
    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index < -1
     AND index > TOTAL_INBOXES
        CPRINTLN(DEBUG_EMAIL,"SCALEFORM_FIRE_RESPONSE: outside of index range")
        EXIT
    ENDIF
    //find the thread in the inbox selected
    //because I flipped the indices for ordering reasons
    //find the baseline zero mail
    INT baselinemail = g_Inboxes[index].iTotalMails-1-selectedUIindex
    WHILE baselinemail >= MAX_INBOX_LOGGED_MAILS_THREADS
        baselinemail -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    CPRINTLN(DEBUG_EMAIL,"SCALEFORM_FIRE_RESPONSE: Baseline ",baselinemail," selected by scaleform ", selectedUIindex)
    INT invval = baselinemail
    
    
    
    INT threadindex = g_Inboxes[index].ThreadIndex[invval]
    INT emaillogindex = g_Inboxes[index].EmailsLogIndex[invval]
    
    
    
    IF emaillogindex < 0 OR threadindex < 0 
    OR (NOT (emaillogindex < MAX_THREAD_LOG_LENGTH))
    OR (NOT (threadindex < TOTAL_EMAIL_THREADS_IN_GAME))
        
        CPRINTLN(DEBUG_EMAIL,"SCALEFORM_FIRE_RESPONSE: Email or thread log index was invalid, caused by index ", index, " and invval of ",  invval)
        CPRINTLN(DEBUG_EMAIL,"Dumping entry info, tmails ", g_Inboxes[index].iTotalMails)        
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].EmailsLogIndex[",  invval,"] : ",g_Inboxes[index].EmailsLogIndex[invval])
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].ThreadIndex[",  invval,"] : ",g_Inboxes[index].ThreadIndex[invval])
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].HasFiredResponse[",  invval,"] : ",g_Inboxes[index].HasFiredResponse[invval])
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].PickedResponseIndex[",  invval,"] : ",g_Inboxes[index].PickedResponseIndex[invval])
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].HasBeenViewed[",  invval,"] : ",g_Inboxes[index].HasBeenViewed[invval])
        CPRINTLN(DEBUG_EMAIL," g_Inboxes[", index, " ].IsDynamic[",  invval,"] : ",g_Inboxes[index].IsDynamic[invval])
        
        SCRIPT_ASSERT("Resultant email log index is invalid, please pass log of this issue to Default Levels.")
        EXIT
    
    ENDIF
    

    INT firstmail = g_AllEmailThreads[threadindex].ThreadEmailLog[emaillogindex]
    //now use the responses to fill out the list
    IF (g_AllEmails[firstmail].iResponses = 0)
        SCRIPT_ASSERT("SCALEFORM_FIRE_RESPONSE: Attempting to populate scaleform response list for an email with no responses!")
        EXIT
    ENDIF
    
    g_Inboxes[index].HasFiredResponse[invval] = TRUE
    g_Inboxes[index].PickedResponseIndex[invval] = selectedResponseIndex
	
    //finally fire the right response
    CPRINTLN(DEBUG_EMAIL,"SCALEFORM_FIRE_RESPONSE: Attempting to fire response from thread ",threadindex," with selected response ", selectedResponseIndex)
    FIRE_EMAIL_THREAD_RESPONSE(threadindex,selectedResponseIndex)

ENDPROC


FUNC BOOL IS_STATIC_EMAIL_THREAD_ACTIVE(EMAIL_THREAD_ENUMS t)
    RETURN g_AllEmailThreads[t].bActive
ENDFUNC


FUNC BOOL IS_STATIC_EMAIL_THREAD_ENDED(EMAIL_THREAD_ENUMS t)
    RETURN g_AllEmailThreads[t].bEnded
ENDFUNC


FUNC BOOL IS_STATIC_EMAIL_THREAD_IN_PROGRESS(EMAIL_THREAD_ENUMS t)
    IF (IS_STATIC_EMAIL_THREAD_ACTIVE(t)) 
    AND (!IS_STATIC_EMAIL_THREAD_ENDED(t))
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC



/////////////////////////////////////////////////////////////////////////////
///    
///    Dynamic email calls
//

//private-ish calls

FUNC INT GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(DYNAMIC_EMAIL_THREAD_NAMES target)
    
    //look through the buffers for target
    INT i = 0
    INT ipotentialID = -1
    INT ipotentialIndex = -1
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
        IF g_DynamicEmailThreadBuffers[i].belongsTo = target
            //IF g_DynamicEmailThreadBuffers[i].bBufferCritical
                IF g_DynamicEmailThreadBuffers[i].registrationID > ipotentialIndex
                    ipotentialIndex = g_DynamicEmailThreadBuffers[i].registrationID 
                    ipotentialID = i
                ENDIF
            //ENDIF
        ENDIF
    ENDREPEAT
    
    
    
    IF ipotentialID != -1 AND ipotentialIndex != -1
        CPRINTLN(DEBUG_EMAIL,"GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX: index found ",ipotentialIndex," ID: ",ipotentialID)
        RETURN ipotentialID
    ENDIF
    
    //
    
    CPRINTLN(DEBUG_EMAIL,"GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX: Index not found")
    
    RETURN -1
ENDFUNC


FUNC INT ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD(DYNAMIC_EMAIL_THREAD_NAMES target, BOOL bNoCrit = FALSE)
    //is this already in the buffer?
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(target)
    
    IF index > -1
        IF (g_DynamicEmailThreadBuffers[index].iProgress < DYNAMIC_EMAIL_THREAD_MAX_LENGTH)
            CPRINTLN(DEBUG_EMAIL,"ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD: Found valid index ", index)
            RETURN index
        ENDIF
        
    ENDIF
    
    //attempt to assign a buffer
    //check for situation where all buffers are logged as critical
    //or full
        //assert
    INT i = 0
    BOOL noneCriticalFound = FALSE
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
        IF g_DynamicEmailThreadBuffers[i].bBufferCritical = FALSE
            noneCriticalFound = TRUE
        ENDIF
    ENDREPEAT
    
    IF NOT noneCriticalFound 
        CPRINTLN(DEBUG_EMAIL,"ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD: Cannot assign thread, all buffers are critical")
        SCRIPT_ASSERT("ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD: Buffers are all set to critical!")
        RETURN -1
    ENDIF
    
    //otherwise pick the oldest none critical buffer
    BOOL first = TRUE
    i = 0
    
    INT itarget = -1
    INT ivalue = -1
    
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
        IF g_DynamicEmailThreadBuffers[i].bBufferCritical = FALSE
                IF first
                    itarget = i
                    ivalue = g_DynamicEmailThreadBuffers[i].registrationID
                    first = FALSE
                ELSE
                    IF ivalue > g_DynamicEmailThreadBuffers[i].registrationID
                        itarget = i
                        ivalue = g_DynamicEmailThreadBuffers[i].registrationID
                    ENDIF
                ENDIF
        ENDIF
    ENDREPEAT


    IF g_DynamicEmailThreadBuffers[itarget].iProgress > 0
        CPRINTLN(DEBUG_EMAIL,"Targeted dynamic email index (",itarget,") has progress (",g_DynamicEmailThreadBuffers[itarget].iProgress,"), purging unread before claiming")
        //need to check for unread emails in this thread so can decrement the read counter
        i = 0
        REPEAT g_DynamicEmailThreadBuffers[itarget].iProgress i
        
            //itarget
            //g_DynamicEmailThreadBuffers[itarget].emails[i]
            INT ibox = 0
            REPEAT TOTAL_INBOXES ibox
                INT imaimax = g_Inboxes[ibox].iTotalMails 
                IF imaimax > MAX_INBOX_LOGGED_MAILS_THREADS
                    imaimax = MAX_INBOX_LOGGED_MAILS_THREADS
                ENDIF
                
                
                INT imai = 0
                REPEAT imaimax imai
                    IF g_Inboxes[ibox].IsDynamic[imai]
                        IF NOT g_Inboxes[ibox].HasBeenViewed[imai]
                            IF g_Inboxes[ibox].ThreadIndex[imai] = g_DynamicEmailThreadBuffers[itarget].registrationID
                                IF g_Inboxes[ibox].EmailsLogIndex[imai] = i
                                    SWITCH ibox
                                        CASE 0
                                            --g_iUnreadEmailsSP0
                                            //PRINTLN("Reducing dynamic g_iUnreadEmailsSP0")
                                            BREAK
                                        CASE 1
                                            --g_iUnreadEmailsSP1
                                            //PRINTLN("Reducing dynamic g_iUnreadEmailsSP1")
                                            BREAK
                                        CASE 2
                                            --g_iUnreadEmailsSP2
                                            //PRINTLN("Reducing dynamic g_iUnreadEmailsSP2")
                                            BREAK
                                    ENDSWITCH 
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDREPEAT
            ENDREPEAT
        ENDREPEAT
    ENDIF

    
    g_DynamicEmailThreadBuffers[itarget].belongsTo = target
    g_DynamicEmailThreadBuffers[itarget].iParticipants = 0
    IF !bNoCrit
        g_DynamicEmailThreadBuffers[itarget].bBufferCritical = TRUE
    ENDIF
    
    ++g_savedGlobals.sEmailData.g_PendingEmailIDGenerator
    IF g_savedGlobals.sEmailData.g_PendingEmailIDGenerator =0
        g_savedGlobals.sEmailData.g_PendingEmailIDGenerator = 1
    ENDIF
    
    g_DynamicEmailThreadBuffers[itarget].registrationID = g_savedGlobals.sEmailData.g_PendingEmailIDGenerator
    g_DynamicEmailThreadBuffers[itarget].iProgress = 0

    CPRINTLN(DEBUG_EMAIL,"ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD: Assigned ", itarget)
    RETURN itarget
ENDFUNC





//dynamic email pending system calls

/// PURPOSE:
///    
/// PARAMS:
///    target - the dynamic thread to prime
///    emailID - the email to prime
///    inGameHoursBeforeTrigger - the number of in game hours before this email should be triggered
/// RETURNS:
///    Value != 0 if successfully primed, this is the PENDING_EMAIL_ID used in other 
///    pending email calls
FUNC INT PRIME_EMAIL_FOR_FIRING_INTO_DYNAMIC_THREAD_IN_HOURS(DYNAMIC_EMAIL_THREAD_NAMES target, 
                                                             EMAIL_MESSAGE_ENUMS emailID, 
                                                             int inGameHoursBeforeTrigger)

    IF inGameHoursBeforeTrigger < 1
        RETURN 0
    ENDIF
    
    IF g_iDynamicEmailsPending = MAX_DYNAMIC_EMAILS_PENDING
        RETURN 0
    ENDIF
    INT iThreadID = 0
    INT i = 0
    
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
    
        IF g_DynamicEmailThreadBuffers[i].belongsTo = target
            IF g_DynamicEmailThreadBuffers[i].registrationID > 0
                iThreadID = g_DynamicEmailThreadBuffers[i].registrationID           
            ENDIF
        ENDIF
    ENDREPEAT
    
    IF iThreadID = 0
        RETURN 0//thread not active
    ENDIF
    
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i
        IF g_DynamicEmailsPending[i].RegIDOfTarget = 0
            
            g_DynamicEmailsPending[i].RegIDOfTarget = iThreadID
            g_DynamicEmailsPending[i].eTargetThread = target
            g_DynamicEmailsPending[i].eEmailID = emailID
            g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger = inGameHoursBeforeTrigger
            ++g_savedGlobals.sEmailData.g_PendingEmailIDGenerator
            IF g_savedGlobals.sEmailData.g_PendingEmailIDGenerator =0
                g_savedGlobals.sEmailData.g_PendingEmailIDGenerator = 1
            ENDIF
            
            
            g_DynamicEmailsPending[i].iOverrideAdditional = 0
            
            g_DynamicEmailsPending[i].iPendingIndex = ( g_savedGlobals.sEmailData.g_PendingEmailIDGenerator )
            
            g_DynamicEmailsPending[i].bNotSent = TRUE 
            
            CPRINTLN(DEBUG_EMAIL,"PRIME_EMAIL_FOR_FIRING_INTO_DYNAMIC_THREAD_IN_HOURS: primed index ", i," with thread id ", iThreadID)
            ++g_iDynamicEmailsPending
   
            
            RETURN g_DynamicEmailsPending[i].iPendingIndex
        ENDIF
    ENDREPEAT
    
    //fail no space
    RETURN 0
ENDFUNC

/// PURPOSE:
///    
/// PARAMS:
///    target - The thread to check for
///    emailID - The email to check for 
/// RETURNS:
///    TRUE if the countdown is still happening on the email
///    note if this returns false the email will have been sent UNLESS the thread buffer was released before it was sent
FUNC BOOL IS_DYNAMIC_EMAIL_PENDING(DYNAMIC_EMAIL_THREAD_NAMES target, EMAIL_MESSAGE_ENUMS emailID,INT PENDING_EMAIL_ID = -1)

    IF g_iDynamicEmailsPending < 1
        RETURN FALSE
    ENDIF
    
    INT i = 0
    
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i
        IF g_DynamicEmailsPending[i].RegIDOfTarget != 0
            IF g_DynamicEmailsPending[i].eTargetThread = target
            AND g_DynamicEmailsPending[i].eEmailID = emailID
            AND g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger > 0
            
                IF PENDING_EMAIL_ID = -1
                    RETURN TRUE
                ELSE
                    
                ENDIF
                
            ENDIF
        ENDIF
    
    ENDREPEAT

    RETURN FALSE
ENDFUNC

FUNC BOOL OVERRIDE_CONTENT_FOR_PENDING_DYNAMIC_EMAIL(INT PENDING_EMAIL_ID,DYNAMIC_EMAIL_THREAD_NAMES thread,EMAIL_MESSAGE_ENUMS emailID, STRING override)
    CPRINTLN(DEBUG_EMAIL,"OVERRIDE_CONTENT_FOR_PENDING_DYNAMIC_EMAIL fired for thread: ", thread, " and email message :", emailID, "with string '", override, "'")
    //start from the end of the list
    INT iCd = MAX_DYNAMIC_EMAILS_PENDING - 1
    INT i = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i
        IF g_DynamicEmailsPending[iCd].RegIDOfTarget != 0
            IF g_DynamicEmailsPending[iCd].eTargetThread = thread
            AND g_DynamicEmailsPending[iCd].eEmailID = emailID
                    
                    
                    IF PENDING_EMAIL_ID = g_DynamicEmailsPending[iCd].iPendingIndex
                        g_DynamicEmailsPending[iCd].bOverrideContent = TRUE
                        g_DynamicEmailsPending[iCd].content = override
                    ENDIF
                    
                RETURN TRUE
            ENDIF
        ENDIF
        --iCd
    ENDREPEAT
    RETURN FALSE
ENDFUNC

FUNC BOOL ADD_CONTENT_FOR_FOR_PENDING_DYNAMIC_EMAIL_SUBSTRING(INT PENDING_EMAIL_ID,DYNAMIC_EMAIL_THREAD_NAMES thread,EMAIL_MESSAGE_ENUMS emailID, STRING override)
    INT iCd = MAX_DYNAMIC_EMAILS_PENDING - 1
    INT i = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i
        IF g_DynamicEmailsPending[iCd].RegIDOfTarget != 0
            IF g_DynamicEmailsPending[iCd].eTargetThread = thread
            AND g_DynamicEmailsPending[iCd].eEmailID = emailID
            
                IF g_DynamicEmailsPending[iCd].iPendingIndex = PENDING_EMAIL_ID
                    
                     IF g_DynamicEmailsPending[iCd].iOverrideAdditional < 4
                        g_DynamicEmailsPending[iCd].additional[g_DynamicEmailsPending[iCd].iOverrideAdditional] = override
                        ++g_DynamicEmailsPending[iCd].iOverrideAdditional
                        RETURN TRUE
                     ENDIF
                    
                    
                    
                ENDIF
                    
                    
                
            ENDIF
        ENDIF
        --iCd
    ENDREPEAT
    RETURN FALSE
ENDFUNC




FUNC BOOL FIRE_EMAIL_INTO_DYNAMIC_THREAD(DYNAMIC_EMAIL_THREAD_NAMES target, 
                                         EMAIL_MESSAGE_ENUMS emailID,
                                         BOOL bAboutToOverrideContent = TRUE)
    //g_DynamicEmailBuffers
    //is this thread currently active in any buffer?
        //if not then try to make it so
        //if impossible to make it so return false and assert
        
    CPRINTLN(DEBUG_EMAIL,"FIRE_EMAIL_INTO_DYNAMIC_THREAD: about to override content - ", bAboutToOverrideContent)
    
    INT index = ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD(target, TRUE)
    
    IF (index = -1)
        CPRINTLN(DEBUG_EMAIL,"FIRE_EMAIL_INTO_DYNAMIC_THREAD: No thread assigned")
        RETURN FALSE
    ENDIF
    
    IF g_DynamicEmailThreadBuffers[index].iProgress = DYNAMIC_EMAIL_THREAD_MAX_LENGTH
        SCRIPT_ASSERT("FIRE_EMAIL_INTO_DYNAMIC_THREAD dynamic buffer is full, this should have been caught by ASSIGN_BUFFER_TO_DYNAMIC_EMAIL_THREAD, dumping this email")
        RETURN FALSE
    ENDIF

    //now the thread is active attempt to add the given email to it
    g_DynamicEmailThreadBuffers[index].belongsTo = target 
    
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress].sourceEmail = emailID
    
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress].bOverrideContent = FALSE
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress].iOverrideAdditional = 0
    

    //g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress].bHasContentCallback= g_bDyanmicEmailContentOverridePending
    //IF g_bDyanmicEmailContentOverridePending 
    
        
    //  g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress].contentCallback = g_nextDynamicCallbackHandle
    
    //  g_bDyanmicEmailContentOverridePending = FALSE
    //ENDIF
    
    
    g_DynamicEmailThreadBuffers[index].iProgress++
    
    CPRINTLN(DEBUG_EMAIL,"Dynamic thread updated progress : ",index," : ",g_DynamicEmailThreadBuffers[index].iProgress-1, " participants ",g_DynamicEmailThreadBuffers[index].iParticipants,"/",MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS)

    
    //add the from and to of the email to the paticipant ID if they're not in it and participants is not greater than max
    
    INT i = 0
    INT foundIndex = -1
    REPEAT g_DynamicEmailThreadBuffers[index].iParticipants i
        IF foundIndex = -1
            IF g_DynamicEmailThreadBuffers[index].Participants[i] = g_AllEmails[emailID].eTo
                foundIndex = i
            ENDIF
        ENDIF
    ENDREPEAT 
    IF foundindex = -1
		 IF g_DynamicEmailThreadBuffers[index].iParticipants = MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS
	        SCRIPT_ASSERT("Dynamic email thread already has max participants and tried to add 1 more!")
	        RETURN FALSE
	    ELSE
	        //add new and increment
	        g_DynamicEmailThreadBuffers[index].Participants[g_DynamicEmailThreadBuffers[index].iParticipants] = g_AllEmails[emailID].eTo
	        g_DynamicEmailThreadBuffers[index].iParticipants++
		ENDIF
    ENDIF 
        
    i = 0
    foundIndex = -1
    REPEAT g_DynamicEmailThreadBuffers[index].iParticipants i
        IF foundIndex = -1
            IF g_DynamicEmailThreadBuffers[index].Participants[i] = g_AllEmails[emailID].eFrom
                foundIndex = i
            ENDIF
        ENDIF
    ENDREPEAT 
    IF foundindex = -1
		IF g_DynamicEmailThreadBuffers[index].iParticipants = MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS
        	SCRIPT_ASSERT("Dynamic email thread already has max participants and tried to add 1 more!")
        	RETURN FALSE
    	ELSE
        //add new and increment
	        g_DynamicEmailThreadBuffers[index].Participants[g_DynamicEmailThreadBuffers[index].iParticipants] = g_AllEmails[emailID].eFrom
	        g_DynamicEmailThreadBuffers[index].iParticipants++
	    ENDIF
    ENDIF
    
    
    //now update the mailers and participant inboxes

    i = 0
    REPEAT g_DynamicEmailThreadBuffers[index].iParticipants i
        
        INT pint = ENUM_TO_INT(g_DynamicEmailThreadBuffers[index].Participants[i])
        
        IF pint < TOTAL_INBOXES // has an inbox
        
            //g_Inboxes[pint] - inbox to update
                    
            
            LOG_EMAIL_THREAD_SNAPSHOT_TO_INBOX(g_DynamicEmailThreadBuffers[index].Participants[i],
                                               g_DynamicEmailThreadBuffers[index].registrationID,
                                               TRUE,
                                               bAboutToOverrideContent)
        
        ENDIF
        
    
    ENDREPEAT
    
    
    
    RETURN TRUE
ENDFUNC

PROC PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD(DYNAMIC_EMAIL_THREAD_NAMES thread)
	CDEBUG2LN(DEBUG_EMAIL, 	"Pushing a feed notification from dynamic thread ", ENUM_TO_INT(thread), ".")
    
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    
    IF (index = -1)
		CDEBUG2LN(DEBUG_EMAIL, "Feedification failed due to null thread index.")
		SCRIPT_ASSERT("PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD: Feedification failed due to null thread index.")
        EXIT
    ENDIF
    
    IF g_DynamicEmailThreadBuffers[index].iProgress = 0
		CDEBUG2LN(DEBUG_EMAIL, "Feedification failed due to no thread progress.")
		SCRIPT_ASSERT("PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD: Feedification failed due to no thread progress.")
        EXIT
    ENDIF

    EMAIL_MESSAGE_ENUMS em = g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].sourceEmail
    INT i = g_DynamicEmailThreadBuffers[index].iProgress-1
    
    TEXT_LABEL body
    IF NOT g_DynamicEmailThreadBuffers[index].emails[i].bOverrideContent
        //PRINTLN("PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD: Dynamic email content not overidden, cannot feedify")
        //EXIT
        INT gmail = ENUM_TO_INT(g_DynamicEmailThreadBuffers[index].emails[i].sourceEmail)
        body = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[gmail].iContentTag)
    ELSE
        body = g_DynamicEmailThreadBuffers[index].emails[i].content
    ENDIF
	
	IF thread = DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY	// - CC
		STRING str_FeedBody
		SWITCH em
			CASE WILDLIFE_INITIAL
				str_FeedBody = "PW_FEED_EM_1"
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Email Public: PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD() - CHECK 1 - Updated Initial Feed Email Body")
			BREAK
			
			CASE WILDLIFE_FINAL
				str_FeedBody = "PW_FEED_EM_3"
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Email Public: PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD() - CHECK 2 - Updated Final Feed Email Body")
			BREAK
			
			DEFAULT
				str_FeedBody = "PW_FEED_EM_2"
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Email Public: PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD() - CHECK 3 - Updated Update Feed Email Body")
			BREAK
		ENDSWITCH
		
		FEEDIFY_EMAIL_MESSAGE(CHAR_FRANKLIN, g_AllEmails[em].eFrom, em, str_FeedBody)
		
	ELSE
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Email Public: PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD() - CHECK 4 - Normal Feed Email Body")
		
	    SWITCH g_AllEmails[em].eTo
	        CASE EMAILER_MICHAEL_DE_SANTO
	            FEEDIFY_EMAIL_MESSAGE(CHAR_MICHAEL, g_AllEmails[em].eFrom, em, body,
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[0],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[1],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[2],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[3],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[4],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[5], // - CC
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[6],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[7],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[8],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[9])	// - CC
	        BREAK
				
	        CASE EMAILER_FRANKLIN
	            FEEDIFY_EMAIL_MESSAGE(CHAR_FRANKLIN, g_AllEmails[em].eFrom, em, body,
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[0],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[1],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[2],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[3],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[4],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[5], // - CC
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[6],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[7],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[8],
		            g_DynamicEmailThreadBuffers[index].emails[i].additional[9])	// - CC
	        BREAK
				
	        CASE EMAILER_TREVOR_PHILIPS
	            FEEDIFY_EMAIL_MESSAGE(CHAR_TREVOR, g_AllEmails[em].eFrom, em, body,
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[0],
	           		g_DynamicEmailThreadBuffers[index].emails[i].additional[1],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[2],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[3],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[4],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[5], // - CC
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[6],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[7],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[8],
	            	g_DynamicEmailThreadBuffers[index].emails[i].additional[9])	// - CC
	        BREAK
	    ENDSWITCH
	ENDIF
ENDPROC


//override content for thread
PROC OVERRIDE_CONTENT_FOR_DYNAMIC_THREAD(DYNAMIC_EMAIL_THREAD_NAMES thread, STRING override, BOOL noSubstrings = TRUE)
	CDEBUG1LN(DEBUG_EMAIL, 	"Overriding thread ", ENUM_TO_INT(thread), 
							" with dynamic content: ", override, 
							". No substrings? ", PICK_STRING(noSubstrings, "TRUE", "FALSE"), ".")
							
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    
    IF (index = -1)
		CDEBUG1LN(DEBUG_EMAIL, "Override failed due to null thread index.")
		SCRIPT_ASSERT("OVERRIDE_CONTENT_FOR_DYNAMIC_THREAD: Override failed due to null thread index.")
        EXIT
    ENDIF
    
    IF g_DynamicEmailThreadBuffers[index].iProgress = 0
		CDEBUG1LN(DEBUG_EMAIL, "Override failed due to no thread progress.")
		SCRIPT_ASSERT("OVERRIDE_CONTENT_FOR_DYNAMIC_THREAD: Override failed due to no thread progress.")
        EXIT
    ENDIF
	
	CDEBUG1LN(DEBUG_EMAIL, 	"Overriding thread at buffer index ", index, 
							". Buffer progress is ", g_DynamicEmailThreadBuffers[index].iProgress-1, ".")
	
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].bOverrideContent = TRUE
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].content = override
	
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].iOverrideAdditional = 0
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[0] = ""
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[1] = ""
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[2] = ""
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[3] = ""
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[4] = ""
    
    IF noSubstrings
        PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD(thread)
    ENDIF

ENDPROC


//override content substring
PROC ADD_CONTENT_FOR_DYNAMIC_THREAD_SUBSTRING(DYNAMIC_EMAIL_THREAD_NAMES thread, STRING override)
	CDEBUG1LN(DEBUG_EMAIL, "Adding content for dynamic thread ", ENUM_TO_INT(thread),"'s substring. Override string: ", override, ".")
	
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    
    IF (index = -1)
		CDEBUG1LN(DEBUG_EMAIL, "Adding content failed due to null thread index.")
		SCRIPT_ASSERT("ADD_CONTENT_FOR_DYNAMIC_THREAD_SUBSTRING: Adding content failed due to null thread index.")
        EXIT
    ENDIF
    
    IF g_DynamicEmailThreadBuffers[index].iProgress = 0
		CDEBUG1LN(DEBUG_EMAIL, "Adding content failed due to no thread progress.")
		SCRIPT_ASSERT("ADD_CONTENT_FOR_DYNAMIC_THREAD_SUBSTRING: Adding content failed due to no thread progress.")
        EXIT
    ENDIF

    IF g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].iOverrideAdditional = EMAIL_OVERRIDE_SUBSTRING_MAX
		CDEBUG1LN(DEBUG_EMAIL, "Adding content failed due to too many content override requests for this buffer.")
		SCRIPT_ASSERT("ADD_CONTENT_FOR_DYNAMIC_THREAD_SUBSTRING: Adding content failed due to too many content override requests for this buffer.")
        EXIT
    ENDIF
    
	CDEBUG1LN(DEBUG_EMAIL, 	"Adding content at buffer index ", index, 
							". Buffer progress is ", g_DynamicEmailThreadBuffers[index].iProgress-1, ".")
    
    int at = g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].iOverrideAdditional
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].iOverrideAdditional++
    g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].additional[at] = override
ENDPROC


PROC RELEASE_DYNAMIC_THREAD_HOLD_ON_BUFFER(DYNAMIC_EMAIL_THREAD_NAMES thread)
	CDEBUG1LN(DEBUG_EMAIL, "Attempting to release dynamic thread", ENUM_TO_INT(thread), "'s hold on its buffer.") 

    IF thread = DYNAMIC_THREAD_BAIL_BONDS // 1510703
        //If not before final bailbond complete
        IF NOT IS_BIT_SET(g_savedGlobals.sBailBondData.iLauncherBitFlags, (ENUM_TO_INT(MAX_BAILBOND_IDS) -1))
            CDEBUG1LN(DEBUG_EMAIL, 	"Preventing dynamic buffer release for DYNAMIC_THREAD_BAIL_BONDS due to the chain not being finished yet.")
            EXIT
        ENDIF
    ENDIF
    
    //Is the thread currently in a buffer?
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    IF index = -1
		CDEBUG1LN(DEBUG_EMAIL, "Failed to release buffer as the buffer index is null.") 
        EXIT
    ENDIF
    
    g_DynamicEmailThreadBuffers[index].bBufferCritical = FALSE // Buffer priority is lowered but its not purged until a new thread.
	
    CDEBUG1LN(DEBUG_EMAIL, "Buffer released sucessfully.")
ENDPROC

PROC MAKE_INBOX_CONTIGUOUS( enumCharacterList character)

        INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
       EXIT
    ENDIF
    
        
    IF g_Inboxes[index].iTotalMails < 1
        CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: no mails to show")
        EXIT
    ENDIF
    
    INT startat = g_Inboxes[index].iTotalMails-1
        
    
    //
    WHILE startat >= MAX_INBOX_LOGGED_MAILS_THREADS
        startat -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    
    INT reach = g_Inboxes[index].iTotalMails
   
    IF reach > MAX_INBOX_LOGGED_MAILS_THREADS
        reach = MAX_INBOX_LOGGED_MAILS_THREADS
    ENDIF
    
    IF startat < 0 OR startat > MAX_INBOX_LOGGED_MAILS_THREADS-1
        SCRIPT_ASSERT("Invalid startat value for inbox")
        EXIT
    ENDIF
    
    CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: total mails ",g_Inboxes[index].iTotalMails)
    INT endat = startat - (reach-1)
    CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: endat pre ",endat)
    IF endat < 0
        endat = MAX_INBOX_LOGGED_MAILS_THREADS + endat
    ENDIF
    EMAIL_INBOX newInbox //make a new contiguous list in here
    CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: startat ",startat)
    CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: reach ",reach)
    CPRINTLN(DEBUG_EMAIL,"MAKE_INBOX_CONTIGUOUS: endat ",endat)
    
    INT i
    REPEAT reach i
        
        BOOL bValid = true
        
        IF g_Inboxes[index].bDelete[endat]
            bValid = FALSE
        ENDIF
        IF g_Inboxes[index].IsDynamic[endat]
            INT tid = g_Inboxes[index].ThreadIndex[endat]
            //use tid to find dynamic thread index
            INT tind = -1
            INT j = 0
            REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
                IF g_DynamicEmailThreadBuffers[j].registrationID = tid
                    tind = j
                ENDIF
            ENDREPEAT
            IF tind = -1
                bValid = FALSE
            ENDIF
        ENDIF

        IF bValid //copy to new
            INT ito = newInbox.iTotalMails
            newInbox.EmailsLogIndex[ito] = g_Inboxes[index].EmailsLogIndex[endat]
            newInbox.ThreadIndex[ito] = g_Inboxes[index].ThreadIndex[endat]
            newInbox.HasFiredResponse[ito] = g_Inboxes[index].HasFiredResponse[endat]
            newInbox.PickedResponseIndex[ito] = g_Inboxes[index].PickedResponseIndex[endat]
            newInbox.HasBeenViewed[ito] = g_Inboxes[index].HasBeenViewed[endat]
            newInbox.IsDynamic[ito] = g_Inboxes[index].IsDynamic[endat]
        
            ++newInbox.iTotalMails
        ENDIF
        ++endat 
        IF endat = MAX_INBOX_LOGGED_MAILS_THREADS
            endat = 0
        ENDIF
    ENDREPEAT

    g_Inboxes[index].iTotalMails = newInbox.iTotalMails
    REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
            g_Inboxes[index].bDelete[i] = FALSE
            g_Inboxes[index].EmailsLogIndex[i] = newInbox.EmailsLogIndex[i]
            g_Inboxes[index].ThreadIndex[i] = newInbox.ThreadIndex[i]
            g_Inboxes[index].HasFiredResponse[i] = newInbox.HasFiredResponse[i]
            g_Inboxes[index].PickedResponseIndex[i] = newInbox.PickedResponseIndex[i]
            g_Inboxes[index].HasBeenViewed[i] = newInbox.HasBeenViewed[i]
            g_Inboxes[index].IsDynamic[i] = newInbox.IsDynamic[i]
    ENDREPEAT

ENDPROC


////////////Old methods that use the new dynamic thread stuff

FUNC INT FILL_SCALEFORM_INBOX_FOR_PED(SCALEFORM_INDEX sf, enumCharacterList character)
    MAKE_INBOX_CONTIGUOUS(character)//avoids the dynamic email gap glitch
//TODO move this data into the charsheet later
    

    INT index = -1
    SWITCH character
        CASE CHAR_MICHAEL
            index = ENUM_TO_INT(EMAILER_MICHAEL_DE_SANTO)
         BREAK
        CASE CHAR_FRANKLIN
            index = ENUM_TO_INT(EMAILER_FRANKLIN)   
         BREAK
        CASE CHAR_TREVOR
            index = ENUM_TO_INT(EMAILER_TREVOR_PHILIPS)
         BREAK
    ENDSWITCH

    IF index = -1
        RETURN 0
    ENDIF
    
	
    BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT_EMPTY")
    SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
    END_SCALEFORM_MOVIE_METHOD()
    
    CPRINTLN(DEBUG_EMAIL,"Total mails in inbox: ",g_Inboxes[index].iTotalMails)
      
    IF g_Inboxes[index].iTotalMails < 1
        //PRINTLN("")
        RETURN 0 
    ENDIF
    
    INT startat = g_Inboxes[index].iTotalMails-1
        
    
    WHILE startat >= MAX_INBOX_LOGGED_MAILS_THREADS
        startat -= MAX_INBOX_LOGGED_MAILS_THREADS
    ENDWHILE
    
    INT reach = g_Inboxes[index].iTotalMails
    
    IF reach > MAX_INBOX_LOGGED_MAILS_THREADS
        reach = MAX_INBOX_LOGGED_MAILS_THREADS
    ENDIF
    
    IF startat < 0 OR startat > MAX_INBOX_LOGGED_MAILS_THREADS-1
        SCRIPT_ASSERT("Invalid startat value for inbox")
        RETURN 0
    ENDIF
    
    INT i = 0
    REPEAT reach i
        CPRINTLN(DEBUG_EMAIL,"Startat: ",startat)
        
        IF NOT g_Inboxes[index].IsDynamic[startat]

            
            INT threadindex = g_Inboxes[index].ThreadIndex[startat]
            INT logindex = g_Inboxes[index].EmailsLogIndex[startat]
            
            CPRINTLN(DEBUG_EMAIL,"Thread and email value looked up : ",threadindex," ",logindex)
            
            
            INT emailindex = g_AllEmailThreads[threadindex].ThreadEmailLog[logindex]
            
            CPRINTLN(DEBUG_EMAIL,"Writing an entry to inbox for g_AllEmails index: ",emailindex)
            
            INT iconint = 0 // deault to unread
            IF g_Inboxes[index].HasBeenViewed[startat]
                iconint = 1 //has been read
            ENDIF
            
            IF iconint = 1 //if open, see if needs to upgrade to no respo
                IF g_AllEmails[emailindex].iResponses > 0
                    IF g_Inboxes[index].HasFiredResponse[startat] = FALSE
                        iconint = 2 //expects a response!!
                    ENDIF
                ENDIF
            ENDIF

            TEXT_LABEL subject = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[emailindex].iTitleTag)

            BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iconint)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[emailindex].eFrom))
            SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(subject)
            END_SCALEFORM_MOVIE_METHOD()
        ELSE
            //its a dynamic thread/email, finding the email is a little different
            //SCRIPT_ASSERT("Dynamic email needs to be displayed here")
            INT tid = g_Inboxes[index].ThreadIndex[startat]
            
            //use tid to find dynamic thread index
            INT tind = -1
            
            INT j = 0
            
            REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS j
                IF g_DynamicEmailThreadBuffers[j].registrationID = tid
                    tind = j
                ENDIF
            ENDREPEAT
            
            
            IF tind = -1
                CPRINTLN(DEBUG_EMAIL,"FILL_SCALEFORM_INBOX_FOR_PED: Dynamic thread ID not found in buffer\n")
                SCRIPT_ASSERT("FILL_SCALEFORM_INBOX_FOR_PED: Dynamic thread ID not found in buffer")
                RETURN 1
            ELSE

                INT eind = g_Inboxes[index].EmailsLogIndex[startat]
                //BOOL viewed = g_Inboxes[index].HasBeenViewed[startat]
                INT iconint = 0 // deault to unread
                IF g_Inboxes[index].HasBeenViewed[startat]
                    iconint = 1 //has been read
                    
                ENDIF

                EMAIL_MESSAGE_ENUMS smail = g_DynamicEmailThreadBuffers[tind].emails[eind].sourceEmail
                IF iconint = 1
                    IF g_AllEmails[smail].iResponses > 0
                        IF g_Inboxes[index].HasFiredResponse[startat] = FALSE
                            iconint = 2 //expects a response!!
                        ENDIF
                    ENDIF
                ENDIF
                
                TEXT_LABEL_63 subject = RETRIEVE_EMAIL_STRING_TAG_BY_INDICE(g_AllEmails[smail].iTitleTag)

                BEGIN_SCALEFORM_MOVIE_METHOD(sf,"SET_DATA_SLOT")
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(8)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iconint)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_EMAILER_ADDRESS_TAG(g_AllEmails[smail].eFrom))
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(subject)
                END_SCALEFORM_MOVIE_METHOD()
            
                CPRINTLN(DEBUG_EMAIL,"DYNAMIC: thread ID   ",tid," buffer id ",tind," email ind ",eind," email global enumind ",ENUM_TO_INT(smail))
            ENDIF
        ENDIF

        startat--
        IF startat < 0
            startat = MAX_INBOX_LOGGED_MAILS_THREADS-1
        ENDIF
        
    ENDREPEAT
    RETURN reach
ENDFUNC





////////////////////////////////////////////
///    
///    Global saving calls
///    
///    
///    
///    
///    
///////////////////////


PROC SAVE_EMAIL_SYSTEM_STATE()
    //SCRIPT_ASSERT("SAVE_EMAIL_SYSTEM_STATE")

    //fill out g_savedGlobals.sEmailData

    //EmailSavedInbox InboxSnapshots[TOTAL_INBOXES]     
            
    //g_savedGlobals.sEmailData.InboxSnapshots[]        
    
    INT i = 0
    
    REPEAT TOTAL_INBOXES i

        #if USE_CLF_DLC
			g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxTotalMailsIn = g_Inboxes[i].iTotalMails	        
	        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE <clf>: Saved inbox with ",g_Inboxes[i].iTotalMails ," total mails")	        
	        INT j = 0	        
	        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j] 		= g_Inboxes[i].EmailsLogIndex[j]
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxThreadIndex[j] 			= g_Inboxes[i].ThreadIndex[j]
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j] 	= g_Inboxes[i].HasFiredResponse[j]
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j] = g_Inboxes[i].PickedResponseIndex[j]
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j] 		= g_Inboxes[i].HasBeenViewed[j]
	            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxIsDynamic[j] 			= g_Inboxes[i].IsDynamic[j]
	        ENDREPEAT
		#endif
		 #if USE_NRM_DLC
			g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxTotalMailsIn = g_Inboxes[i].iTotalMails	        
	        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE <clf>: Saved inbox with ",g_Inboxes[i].iTotalMails ," total mails")	        
	        INT j = 0	        
	        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j] 		= g_Inboxes[i].EmailsLogIndex[j]
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxThreadIndex[j] 			= g_Inboxes[i].ThreadIndex[j]
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j] 	= g_Inboxes[i].HasFiredResponse[j]
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j] = g_Inboxes[i].PickedResponseIndex[j]
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j] 		= g_Inboxes[i].HasBeenViewed[j]
	            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxIsDynamic[j] 			= g_Inboxes[i].IsDynamic[j]
	        ENDREPEAT
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sEmailData.InboxSnapshots[i].InboxTotalMailsIn = g_Inboxes[i].iTotalMails
	        
	        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Saved inbox with ",g_Inboxes[i].iTotalMails ," total mails")
	        
	        INT j = 0
	        
	        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j] = g_Inboxes[i].EmailsLogIndex[j]
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxThreadIndex[j] = g_Inboxes[i].ThreadIndex[j]
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j] = g_Inboxes[i].HasFiredResponse[j]
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j] = g_Inboxes[i].PickedResponseIndex[j]
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j] = g_Inboxes[i].HasBeenViewed[j]
	            g_savedGlobals.sEmailData.InboxSnapshots[i].InboxIsDynamic[j] = g_Inboxes[i].IsDynamic[j]
	        ENDREPEAT
		#endif
        #endif
        
    ENDREPEAT
    
    
    //active dynamic thread states
    //EmailSavedDynamicThread DynamicThreadBufferSnapshots[DYNAMIC_EMAIL_THREAD_BUFFERS]
                
            
                
    i = 0
   
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
        //g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i]
        #if USE_CLF_DLC
			g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo = g_DynamicEmailThreadBuffers[i].belongsTo
	        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID = g_DynamicEmailThreadBuffers[i].registrationID        
	        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].isCritical = g_DynamicEmailThreadBuffers[i].bBufferCritical   
	        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants = g_DynamicEmailThreadBuffers[i].iParticipants
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
	            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j] = g_DynamicEmailThreadBuffers[i].Participants[j]
	        ENDREPEAT
	        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].iProgress = g_DynamicEmailThreadBuffers[i].iProgress
	        j = 0
	        
	        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
	            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail = g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail
	            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent = g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent  
	            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content = g_DynamicEmailThreadBuffers[i].emails[j].content    
	            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional = g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional    
	            
	            INT k = 0
	            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
	                g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k] = g_DynamicEmailThreadBuffers[i].emails[j].additional[k]    
	            ENDREPEAT            
	        ENDREPEAT
		#endif
		 #if USE_NRM_DLC
			g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo = g_DynamicEmailThreadBuffers[i].belongsTo
	        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID = g_DynamicEmailThreadBuffers[i].registrationID        
	        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].isCritical = g_DynamicEmailThreadBuffers[i].bBufferCritical   
	        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants = g_DynamicEmailThreadBuffers[i].iParticipants
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
	            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j] = g_DynamicEmailThreadBuffers[i].Participants[j]
	        ENDREPEAT
	        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].iProgress = g_DynamicEmailThreadBuffers[i].iProgress
	        j = 0
	        
	        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
	            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail = g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail
	            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent = g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent  
	            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content = g_DynamicEmailThreadBuffers[i].emails[j].content    
	            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional = g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional    
	            
	            INT k = 0
	            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
	                g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k] = g_DynamicEmailThreadBuffers[i].emails[j].additional[k]    
	            ENDREPEAT            
	        ENDREPEAT
		#endif
        
        #if not USE_CLF_DLC
		#if not USE_NRM_DLC
	        g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo = g_DynamicEmailThreadBuffers[i].belongsTo
	        g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID = g_DynamicEmailThreadBuffers[i].registrationID        
	        g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].isCritical = g_DynamicEmailThreadBuffers[i].bBufferCritical   
	        g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants = g_DynamicEmailThreadBuffers[i].iParticipants
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
	            g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j] = g_DynamicEmailThreadBuffers[i].Participants[j]
	        ENDREPEAT
	        g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].iProgress = g_DynamicEmailThreadBuffers[i].iProgress
	        j = 0
	        
	        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
	            g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail = g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail
	            g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent = g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent  
	            g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content = g_DynamicEmailThreadBuffers[i].emails[j].content    
	            g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional = g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional    
	            
	            INT k = 0
	            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
	                g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k] = g_DynamicEmailThreadBuffers[i].emails[j].additional[k]    
	            ENDREPEAT            
	        ENDREPEAT
		#endif
		#endif
    ENDREPEAT
                
                
    //static thread progress states TOTAL_EMAIL_THREADS_IN_GAME
    //EmailSavedStaticThread StaticThreadSnapshots[TOTAL_EMAIL_THREADS_IN_GAME]



    i = 0
    
    REPEAT TOTAL_EMAIL_THREADS_IN_GAME i   
	
		#if USE_CLF_DLC
	        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].active = g_AllEmailThreads[i].bActive
	        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ended = g_AllEmailThreads[i].bEnded
	        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].blockedForResponse = g_AllEmailThreads[i].bBLockedUntilResponse
	        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt = g_AllEmailThreads[i].iCurrentlyAt
	     
		  	INT j       
	        CPRINTLN(DEBUG_EMAIL,"DebugMailLogDump <clf>: on save ", i, " - mails in log ",g_AllEmailThreads[i].iMailsInLog)
	        REPEAT MAX_THREAD_LOG_LENGTH j
	            IF j < g_AllEmailThreads[i].iMailsInLog
	                g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = g_AllEmailThreads[i].ThreadEmailLog[j]+1
	            ELSE
	                g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = -1
	            ENDIF
				
	            PRINTINT(g_AllEmailThreads[i].ThreadEmailLog[j])
	            PRINTSTRING(":")
	            
	        ENDREPEAT
	        PRINTNL()
		#endif
		#if USE_NRM_DLC
	        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].active = g_AllEmailThreads[i].bActive
	        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ended = g_AllEmailThreads[i].bEnded
	        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].blockedForResponse = g_AllEmailThreads[i].bBLockedUntilResponse
	        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt = g_AllEmailThreads[i].iCurrentlyAt
	     
		  	INT j       
	        CPRINTLN(DEBUG_EMAIL,"DebugMailLogDump <clf>: on save ", i, " - mails in log ",g_AllEmailThreads[i].iMailsInLog)
	        REPEAT MAX_THREAD_LOG_LENGTH j
	            IF j < g_AllEmailThreads[i].iMailsInLog
	                g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = g_AllEmailThreads[i].ThreadEmailLog[j]+1
	            ELSE
	                g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = -1
	            ENDIF
	            
	            PRINTINT(g_AllEmailThreads[i].ThreadEmailLog[j])
	            PRINTSTRING(":")
	            
	        ENDREPEAT
	        PRINTNL()
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
	        g_savedGlobals.sEmailData.StaticThreadSnapshots[i].active = g_AllEmailThreads[i].bActive
	        g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ended = g_AllEmailThreads[i].bEnded
	        g_savedGlobals.sEmailData.StaticThreadSnapshots[i].blockedForResponse = g_AllEmailThreads[i].bBLockedUntilResponse
	        g_savedGlobals.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt = g_AllEmailThreads[i].iCurrentlyAt
	     
		  	INT j       
	        CPRINTLN(DEBUG_EMAIL,"DebugMailLogDump: on save ", i, " - mails in log ",g_AllEmailThreads[i].iMailsInLog)
	        REPEAT MAX_THREAD_LOG_LENGTH j
	            IF j < g_AllEmailThreads[i].iMailsInLog
	                g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = g_AllEmailThreads[i].ThreadEmailLog[j]+1
	            ELSE
	                g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = -1
	            ENDIF
	            
	            PRINTINT(g_AllEmailThreads[i].ThreadEmailLog[j])
	            PRINTSTRING(":")
	            
	        ENDREPEAT
	        PRINTNL()
		#endif
		#endif
		
    ENDREPEAT


    //DYNAMIC_THREAD_EMAIL_PENDING DynamicPendingMails[DYNAMIC_THREAD_EMAIL_PENDING]
    //DYNAMIC_THREAD_EMAIL_PENDING g_DynamicEmailsPending[MAX_DYNAMIC_EMAILS_PENDING]
    i = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i 
	
		#if USE_CLF_DLC
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].eTargetThread = g_DynamicEmailsPending[i].eTargetThread
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].eEmailID = g_DynamicEmailsPending[i].eEmailID
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger = g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].bOverrideContent = g_DynamicEmailsPending[i].bOverrideContent
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].content = g_DynamicEmailsPending[i].content
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].iOverrideAdditional = g_DynamicEmailsPending[i].iOverrideAdditional
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
	            g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].additional[j] = g_DynamicEmailsPending[i].additional[j]
	        ENDREPEAT
	        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].RegIDOfTarget = g_DynamicEmailsPending[i].RegIDOfTarget
		#endif
		#if USE_NRM_DLC
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].eTargetThread = g_DynamicEmailsPending[i].eTargetThread
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].eEmailID = g_DynamicEmailsPending[i].eEmailID
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger = g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].bOverrideContent = g_DynamicEmailsPending[i].bOverrideContent
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].content = g_DynamicEmailsPending[i].content
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].iOverrideAdditional = g_DynamicEmailsPending[i].iOverrideAdditional
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
	            g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].additional[j] = g_DynamicEmailsPending[i].additional[j]
	        ENDREPEAT
	        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].RegIDOfTarget = g_DynamicEmailsPending[i].RegIDOfTarget
		#endif
		
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			g_savedGlobals.sEmailData.DynamicPendingMails[i].eTargetThread = g_DynamicEmailsPending[i].eTargetThread
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].eEmailID = g_DynamicEmailsPending[i].eEmailID
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger = g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].bOverrideContent = g_DynamicEmailsPending[i].bOverrideContent
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].content = g_DynamicEmailsPending[i].content
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].iOverrideAdditional = g_DynamicEmailsPending[i].iOverrideAdditional
	        
	        INT j = 0
	        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
	            g_savedGlobals.sEmailData.DynamicPendingMails[i].additional[j] = g_DynamicEmailsPending[i].additional[j]
	        ENDREPEAT
	        g_savedGlobals.sEmailData.DynamicPendingMails[i].RegIDOfTarget = g_DynamicEmailsPending[i].RegIDOfTarget
		#endif
		#endif
		
    ENDREPEAT
    

    CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: set to save ",g_iDynamicEmailsPending, " pending dynamic mails.")
    
    
    #IF IS_DEBUG_BUILD
        INT dbg = 0
        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Dumping inbox state")
        REPEAT TOTAL_INBOXES dbg 
            CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Inbox ",g_Inboxes[dbg].iTotalMails," inbox state on save:")
            INT dbj = 0
            REPEAT MAX_INBOX_LOGGED_MAILS_THREADS dbj
                CPRINTLN(DEBUG_EMAIL,"MAILSTATE:",dbj,":",
                g_Inboxes[dbg].EmailsLogIndex[dbj],":",
                g_Inboxes[dbg].ThreadIndex[dbj],":",
                g_Inboxes[dbg].HasFiredResponse[dbj],":",
                g_Inboxes[dbg].PickedResponseIndex[dbj],":",
                g_Inboxes[dbg].HasBeenViewed[dbj],":",g_Inboxes[dbg].IsDynamic[dbj])
            ENDREPEAT
        ENDREPEAT
    #ENDIF
    
    
ENDPROC



PROC SAVE_EMAIL_SYSTEM_STATE_CLF()
    //SCRIPT_ASSERT("SAVE_EMAIL_SYSTEM_STATE")

    //fill out g_savedGlobals.sEmailData

    //EmailSavedInbox InboxSnapshots[TOTAL_INBOXES]     
            
    //g_savedGlobals.sEmailData.InboxSnapshots[]        
    
    INT i = 0
    
    REPEAT TOTAL_INBOXES i

        
        g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxTotalMailsIn = g_Inboxes[i].iTotalMails
        
        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Saved inbox with ",g_Inboxes[i].iTotalMails ," total mails")
        
        INT j = 0        
        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j] 		= g_Inboxes[i].EmailsLogIndex[j]
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxThreadIndex[j] 			= g_Inboxes[i].ThreadIndex[j]
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j] 	= g_Inboxes[i].HasFiredResponse[j]
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j] = g_Inboxes[i].PickedResponseIndex[j]
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j] 		= g_Inboxes[i].HasBeenViewed[j]
            g_savedGlobalsClifford.sEmailData.InboxSnapshots[i].InboxIsDynamic[j] 			= g_Inboxes[i].IsDynamic[j]
        ENDREPEAT        
    ENDREPEAT
         
    i = 0   
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
       
        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo 		= g_DynamicEmailThreadBuffers[i].belongsTo
        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID 		= g_DynamicEmailThreadBuffers[i].registrationID        
        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].isCritical 		= g_DynamicEmailThreadBuffers[i].bBufferCritical   
        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants 	= g_DynamicEmailThreadBuffers[i].iParticipants
        
        INT j = 0        
        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j] 	= g_DynamicEmailThreadBuffers[i].Participants[j]
        ENDREPEAT       
        g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].iProgress 				= g_DynamicEmailThreadBuffers[i].iProgress
        
        j = 0        
        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail 		= g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail                
            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent 	= g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent            
            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content 			= g_DynamicEmailThreadBuffers[i].emails[j].content                
            g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional = g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional  
            
            INT k = 0
            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
                g_savedGlobalsClifford.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k] = g_DynamicEmailThreadBuffers[i].emails[j].additional[k]    
            ENDREPEAT            
        ENDREPEAT    
    ENDREPEAT
            
    i = 0    
    REPEAT TOTAL_EMAIL_THREADS_IN_GAME i    
        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].active 			= g_AllEmailThreads[i].bActive
        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ended 				= g_AllEmailThreads[i].bEnded
        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].blockedForResponse = g_AllEmailThreads[i].bBLockedUntilResponse
        g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt 		= g_AllEmailThreads[i].iCurrentlyAt
       
        INT j      
        CPRINTLN(DEBUG_EMAIL,"DebugMailLogDump: on save ", i, " - mails in log ",g_AllEmailThreads[i].iMailsInLog)
        REPEAT MAX_THREAD_LOG_LENGTH j
            IF j < g_AllEmailThreads[i].iMailsInLog
                g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = g_AllEmailThreads[i].ThreadEmailLog[j]+1
            ELSE
                g_savedGlobalsClifford.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = -1
            ENDIF            
            PRINTINT(g_AllEmailThreads[i].ThreadEmailLog[j])
            PRINTSTRING(":")            
        ENDREPEAT
        PRINTNL()
    ENDREPEAT

    i = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i 
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].eTargetThread 				= g_DynamicEmailsPending[i].eTargetThread
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].eEmailID 					= g_DynamicEmailsPending[i].eEmailID
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger 	= g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].bOverrideContent 			= g_DynamicEmailsPending[i].bOverrideContent
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].content 						= g_DynamicEmailsPending[i].content
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].iOverrideAdditional 			= g_DynamicEmailsPending[i].iOverrideAdditional
		
        INT j = 0
        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
            g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].additional[j] 		= g_DynamicEmailsPending[i].additional[j]
        ENDREPEAT
        g_savedGlobalsClifford.sEmailData.DynamicPendingMails[i].RegIDOfTarget 			= g_DynamicEmailsPending[i].RegIDOfTarget
    ENDREPEAT
    
    CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: set to save ",g_iDynamicEmailsPending, " pending dynamic mails.")    
    #IF IS_DEBUG_BUILD
        INT dbg = 0
        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Dumping inbox state")
        REPEAT TOTAL_INBOXES dbg 
            CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Inbox ",g_Inboxes[dbg].iTotalMails," inbox state on save:")
            INT dbj = 0
            REPEAT MAX_INBOX_LOGGED_MAILS_THREADS dbj
                CPRINTLN(DEBUG_EMAIL,"MAILSTATE:",dbj,":",
                g_Inboxes[dbg].EmailsLogIndex[dbj],":",
                g_Inboxes[dbg].ThreadIndex[dbj],":",
                g_Inboxes[dbg].HasFiredResponse[dbj],":",
                g_Inboxes[dbg].PickedResponseIndex[dbj],":",
                g_Inboxes[dbg].HasBeenViewed[dbj],":",g_Inboxes[dbg].IsDynamic[dbj])
            ENDREPEAT
        ENDREPEAT
    #ENDIF
    
ENDPROC


PROC SAVE_EMAIL_SYSTEM_STATE_NRM()
    //SCRIPT_ASSERT("SAVE_EMAIL_SYSTEM_STATE")

    //fill out g_savedGlobals.sEmailData

    //EmailSavedInbox InboxSnapshots[TOTAL_INBOXES]     
            
    //g_savedGlobals.sEmailData.InboxSnapshots[]        
    
    INT i = 0
    
    REPEAT TOTAL_INBOXES i

        
        g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxTotalMailsIn = g_Inboxes[i].iTotalMails
        
        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Saved inbox with ",g_Inboxes[i].iTotalMails ," total mails")
        
        INT j = 0        
        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j] 		= g_Inboxes[i].EmailsLogIndex[j]
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxThreadIndex[j] 			= g_Inboxes[i].ThreadIndex[j]
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j] 	= g_Inboxes[i].HasFiredResponse[j]
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j] = g_Inboxes[i].PickedResponseIndex[j]
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j] 		= g_Inboxes[i].HasBeenViewed[j]
            g_savedGlobalsnorman.sEmailData.InboxSnapshots[i].InboxIsDynamic[j] 			= g_Inboxes[i].IsDynamic[j]
        ENDREPEAT        
    ENDREPEAT
         
    i = 0   
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
       
        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo 		= g_DynamicEmailThreadBuffers[i].belongsTo
        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID 		= g_DynamicEmailThreadBuffers[i].registrationID        
        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].isCritical 		= g_DynamicEmailThreadBuffers[i].bBufferCritical   
        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants 	= g_DynamicEmailThreadBuffers[i].iParticipants
        
        INT j = 0        
        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j] 	= g_DynamicEmailThreadBuffers[i].Participants[j]
        ENDREPEAT       
        g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].iProgress 				= g_DynamicEmailThreadBuffers[i].iProgress
        
        j = 0        
        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail 		= g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail                
            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent 	= g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent            
            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content 			= g_DynamicEmailThreadBuffers[i].emails[j].content                
            g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional = g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional  
            
            INT k = 0
            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
                g_savedGlobalsnorman.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k] = g_DynamicEmailThreadBuffers[i].emails[j].additional[k]    
            ENDREPEAT            
        ENDREPEAT    
    ENDREPEAT
            
    i = 0    
    REPEAT TOTAL_EMAIL_THREADS_IN_GAME i    
        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].active 			= g_AllEmailThreads[i].bActive
        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ended 				= g_AllEmailThreads[i].bEnded
        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].blockedForResponse = g_AllEmailThreads[i].bBLockedUntilResponse
        g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt 		= g_AllEmailThreads[i].iCurrentlyAt
       
        INT j      
        CPRINTLN(DEBUG_EMAIL,"DebugMailLogDump: on save ", i, " - mails in log ",g_AllEmailThreads[i].iMailsInLog)
        REPEAT MAX_THREAD_LOG_LENGTH j
            IF j < g_AllEmailThreads[i].iMailsInLog
                g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = g_AllEmailThreads[i].ThreadEmailLog[j]+1
            ELSE
                g_savedGlobalsnorman.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] = -1
            ENDIF            
            PRINTINT(g_AllEmailThreads[i].ThreadEmailLog[j])
            PRINTSTRING(":")            
        ENDREPEAT
        PRINTNL()
    ENDREPEAT

    i = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i 
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].eTargetThread 				= g_DynamicEmailsPending[i].eTargetThread
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].eEmailID 					= g_DynamicEmailsPending[i].eEmailID
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger 	= g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].bOverrideContent 			= g_DynamicEmailsPending[i].bOverrideContent
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].content 						= g_DynamicEmailsPending[i].content
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].iOverrideAdditional 			= g_DynamicEmailsPending[i].iOverrideAdditional
		
        INT j = 0
        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
            g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].additional[j] 		= g_DynamicEmailsPending[i].additional[j]
        ENDREPEAT
        g_savedGlobalsnorman.sEmailData.DynamicPendingMails[i].RegIDOfTarget 			= g_DynamicEmailsPending[i].RegIDOfTarget
    ENDREPEAT
    
    CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: set to save ",g_iDynamicEmailsPending, " pending dynamic mails.")    
    #IF IS_DEBUG_BUILD
        INT dbg = 0
        CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Dumping inbox state")
        REPEAT TOTAL_INBOXES dbg 
            CPRINTLN(DEBUG_EMAIL,"SAVE_EMAIL_SYSTEM_STATE: Inbox ",g_Inboxes[dbg].iTotalMails," inbox state on save:")
            INT dbj = 0
            REPEAT MAX_INBOX_LOGGED_MAILS_THREADS dbj
                CPRINTLN(DEBUG_EMAIL,"MAILSTATE:",dbj,":",
                g_Inboxes[dbg].EmailsLogIndex[dbj],":",
                g_Inboxes[dbg].ThreadIndex[dbj],":",
                g_Inboxes[dbg].HasFiredResponse[dbj],":",
                g_Inboxes[dbg].PickedResponseIndex[dbj],":",
                g_Inboxes[dbg].HasBeenViewed[dbj],":",g_Inboxes[dbg].IsDynamic[dbj])
            ENDREPEAT
        ENDREPEAT
    #ENDIF
    
    
ENDPROC



/// PURPOSE:
///    Resets the email system and content to inital values
PROC INITIALISE_EMAIL_SYSTEM()

    INT i = 0
    REPEAT MAX_EMAIL_FEED_BUFFER_ENTRIES i
        IF g_iEmailFeedBuffer[i] != -1
            g_iEmailFeedBuffer[i] = -1
        ENDIF
    ENDREPEAT


    g_iUnreadEmailsSP0 = 0
    g_iUnreadEmailsSP1 = 0
    g_iUnreadEmailsSP2 = 0
    REPEAT TOTAL_EMAIL_THREADS_IN_GAME i
        g_AllEmailThreads[i].iCurrentlyAt = 0
        g_AllEmailThreads[i].iMailsInLog = 0
        g_AllEmailThreads[i].bActive = FALSE
    ENDREPEAT
    REPEAT TOTAL_EMAILS_IN_GAME i
        //g_AllEmails[i].iResponses = 0
        g_AllEmails[i].bEmailBlocksThread = FALSE
    ENDREPEAT
    REPEAT TOTAL_INBOXES i
        g_Inboxes[i].iTotalMails = 0
    ENDREPEAT
    
    //////////////////////////
    INITIALISE_EMAIL_SYSTEM_CONTENT()
ENDPROC




PROC LOAD_EMAIL_SYSTEM_STATE()
	CPRINTLN(DEBUG_EMAIL, "Restoring email system state from saved globals.")
    
    INT i, j, k
    REPEAT TOTAL_INBOXES i
        g_Inboxes[i].iTotalMails = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxTotalMailsIn
		CDEBUG1LN(DEBUG_EMAIL, "Loading inbox ", i, " with ", g_Inboxes[i].iTotalMails ," total mails.")
		
        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS j 
			CDEBUG2LN(DEBUG_EMAIL, "Restoring logged mail thread ", j, ".")
			
            g_Inboxes[i].EmailsLogIndex[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxEmailLogIndices[j]
            g_Inboxes[i].ThreadIndex[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxThreadIndex[j]
            g_Inboxes[i].HasFiredResponse[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxHasFiredResponses[j]
            g_Inboxes[i].PickedResponseIndex[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxPickedResponseIndices[j]
            g_Inboxes[i].HasBeenViewed[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxHasBeenViewed[j]
            g_Inboxes[i].IsDynamic[j] = g_savedGlobals.sEmailData.InboxSnapshots[i].InboxIsDynamic[j]
			
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Log index: ", g_Inboxes[i].EmailsLogIndex[j])
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Thread index: ", g_Inboxes[i].ThreadIndex[j])
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Has fired response: ", PICK_STRING(g_Inboxes[i].HasFiredResponse[j], "TRUE", "FALSE"))
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Picked response index: ", g_Inboxes[i].PickedResponseIndex[j])
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Has been viewed: ", PICK_STRING(g_Inboxes[i].HasBeenViewed[j], "TRUE", "FALSE"))
			CDEBUG3LN(DEBUG_EMAIL, "<LOGGED-THREAD> Is Dynamic: ", PICK_STRING(g_Inboxes[i].IsDynamic[j], "TRUE", "FALSE"))
        ENDREPEAT
    ENDREPEAT
	
    REPEAT DYNAMIC_EMAIL_THREAD_BUFFERS i
		CDEBUG1LN(DEBUG_EMAIL, "Loading dynamic email thread ", i, ".")

        g_DynamicEmailThreadBuffers[i].belongsTo = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].BelongsTo
        g_DynamicEmailThreadBuffers[i].bBufferCritical = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].isCritical
        g_DynamicEmailThreadBuffers[i].iParticipants = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].iParticipants
        g_DynamicEmailThreadBuffers[i].registrationID = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].AssignedID
		g_DynamicEmailThreadBuffers[i].iProgress = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].iProgress
        
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Belongs to: ", ENUM_TO_INT(g_DynamicEmailThreadBuffers[i].belongsTo))
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Buffer critical: ", PICK_STRING(g_DynamicEmailThreadBuffers[i].bBufferCritical, "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Registration ID: ", g_DynamicEmailThreadBuffers[i].registrationID)
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Progress: ", g_DynamicEmailThreadBuffers[i].iProgress)
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Participants: ", g_DynamicEmailThreadBuffers[i].iParticipants)
		
        REPEAT MAX_DYNAMIC_EMAIL_THREAD_PARTICIPANTS j
            g_DynamicEmailThreadBuffers[i].Participants[j]= g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].participantIDs[j]
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-THREAD> Participant ", j, ": ", ENUM_TO_INT(g_DynamicEmailThreadBuffers[i].Participants[j]), ".")
        ENDREPEAT
        
        REPEAT DYNAMIC_EMAIL_THREAD_MAX_LENGTH j
			CDEBUG2LN(DEBUG_EMAIL, "Loading thread buffer ", j, ".")
		
            g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].sourceEmail  
            g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].bOverrideContent
            g_DynamicEmailThreadBuffers[i].emails[j].content  = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].content
            g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].iOverrideAdditional
            
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-BUFFER> Source email: ", ENUM_TO_INT(g_DynamicEmailThreadBuffers[i].emails[j].sourceEmail))
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-BUFFER> Override content: ", PICK_STRING(g_DynamicEmailThreadBuffers[i].emails[j].bOverrideContent, "TRUE", "FALSE"))
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-BUFFER> Content: ", g_DynamicEmailThreadBuffers[i].emails[j].content)
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-BUFFER> Override additional: ", g_DynamicEmailThreadBuffers[i].emails[j].iOverrideAdditional)
			
            REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS k // ADDED BY CC
                g_DynamicEmailThreadBuffers[i].emails[j].additional[k]  = g_savedGlobals.sEmailData.DynamicThreadBufferSnapshots[i].DynamicEmailData[j].additional[k]
           		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-BUFFER> Additional ", k, ": ", g_DynamicEmailThreadBuffers[i].emails[j].additional[k])
			ENDREPEAT
        ENDREPEAT
    ENDREPEAT
    
    REPEAT TOTAL_EMAIL_THREADS_IN_GAME i
		CDEBUG1LN(DEBUG_EMAIL, "Loading global email thread data ", i, ".")

        g_AllEmailThreads[i].bActive = g_savedGlobals.sEmailData.StaticThreadSnapshots[i].active
        g_AllEmailThreads[i].bEnded = g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ended
        g_AllEmailThreads[i].bBLockedUntilResponse = g_savedGlobals.sEmailData.StaticThreadSnapshots[i].blockedForResponse
        g_AllEmailThreads[i].iCurrentlyAt = g_savedGlobals.sEmailData.StaticThreadSnapshots[i].iCurrentlyAt
		
		CDEBUG3LN(DEBUG_EMAIL, "<GLOBAL-THREAD> Active: ", PICK_STRING(g_AllEmailThreads[i].bActive, "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_EMAIL, "<GLOBAL-THREAD> Ended: ", PICK_STRING(g_AllEmailThreads[i].bEnded, "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_EMAIL, "<GLOBAL-THREAD> Blocked until response: ", PICK_STRING(g_AllEmailThreads[i].bBLockedUntilResponse, "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_EMAIL, "<GLOBAL-THREAD> Currently at: ", g_AllEmailThreads[i].iCurrentlyAt)

        REPEAT MAX_THREAD_LOG_LENGTH j
            IF g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j] > 0
				CDEBUG3LN(DEBUG_EMAIL, "<GLOBAL-THREAD> Found data in log index ", j, ": ", g_AllEmailThreads[i].ThreadEmailLog[j])
                g_AllEmailThreads[i].ThreadEmailLog[j] = g_savedGlobals.sEmailData.StaticThreadSnapshots[i].ThreadEmailLog[j]-1
                ++g_AllEmailThreads[i].iMailsInLog
            ENDIF
        ENDREPEAT
    ENDREPEAT
    
    g_iDynamicEmailsPending = 0
    REPEAT MAX_DYNAMIC_EMAILS_PENDING i
		CDEBUG1LN(DEBUG_EMAIL, "Loading dynamic email pending ", i, ".")
	
        g_DynamicEmailsPending[i].eTargetThread = g_savedGlobals.sEmailData.DynamicPendingMails[i].eTargetThread
        g_DynamicEmailsPending[i].eEmailID = g_savedGlobals.sEmailData.DynamicPendingMails[i].eEmailID
        g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger = g_savedGlobals.sEmailData.DynamicPendingMails[i].iInGameHoursBeforeTrigger
        g_DynamicEmailsPending[i].bOverrideContent = g_savedGlobals.sEmailData.DynamicPendingMails[i].bOverrideContent
        g_DynamicEmailsPending[i].content = g_savedGlobals.sEmailData.DynamicPendingMails[i].content
        g_DynamicEmailsPending[i].iOverrideAdditional = g_savedGlobals.sEmailData.DynamicPendingMails[i].iOverrideAdditional
		
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Target thread: ", ENUM_TO_INT(g_DynamicEmailsPending[i].eTargetThread))
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Email ID: ", ENUM_TO_INT(g_DynamicEmailsPending[i].eEmailID))
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> In game hours before trigger: ", g_DynamicEmailsPending[i].iInGameHoursBeforeTrigger)
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Override content: ", PICK_STRING(g_DynamicEmailsPending[i].bOverrideContent, "TRUE", "FALSE"))
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Content: ", g_DynamicEmailsPending[i].content)
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Override additional: ", g_DynamicEmailsPending[i].iOverrideAdditional)
        
        REPEAT MAX_DYNAMIC_EMAIL_SUBSTRINGS j // ADDED BY CC
            g_DynamicEmailsPending[i].additional[j] = g_savedGlobals.sEmailData.DynamicPendingMails[i].additional[j]
			CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Additional ", j, ": ", g_DynamicEmailsPending[i].additional[j])
        ENDREPEAT
		
        g_DynamicEmailsPending[i].RegIDOfTarget = g_savedGlobals.sEmailData.DynamicPendingMails[i].RegIDOfTarget
        IF g_DynamicEmailsPending[i].RegIDOfTarget != 0
            ++g_iDynamicEmailsPending
        ENDIF
		
		CDEBUG3LN(DEBUG_EMAIL, "<DYNAMIC-PENDING> Registration ID of target: ", g_DynamicEmailsPending[i].RegIDOfTarget)
		
    ENDREPEAT
	CDEBUG1LN(DEBUG_EMAIL, "Loaded  ", g_iDynamicEmailsPending, " pending dynamic mails.")

ENDPROC



#IF IS_DEBUG_BUILD

PROC DEBUG_SET_STATIC_EMAIL_THREAD_TO_READ(EMAIL_THREAD_ENUMS paramThread)
    INT iInboxIndex
    INT iThreadIndex
    REPEAT TOTAL_INBOXES iInboxIndex
        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS iThreadIndex
            IF NOT g_Inboxes[iInboxIndex].IsDynamic[iThreadIndex]
                IF g_Inboxes[iInboxIndex].ThreadIndex[iThreadIndex] = ENUM_TO_INT(paramThread)
                    g_Inboxes[iInboxIndex].HasBeenViewed[iThreadIndex] = TRUE
					CPRINTLN(DEBUG_EMAIL, "Script ", GET_THIS_SCRIPT_NAME(), " set email thread ", ENUM_TO_INT(paramThread), " as having been read.")
                ENDIF
            ENDIF
        ENDREPEAT
    ENDREPEAT
ENDPROC

#ENDIF


FUNC BOOL HAS_CURRENT_MAIL_IN_STATIC_THREAD_BEEN_READ_BY_ANY_RECIEVER(EMAIL_THREAD_ENUMS paramThread)
    INT iInboxIndex
    INT iThreadIndex
    REPEAT TOTAL_INBOXES iInboxIndex
        REPEAT MAX_INBOX_LOGGED_MAILS_THREADS iThreadIndex
            IF NOT g_Inboxes[iInboxIndex].IsDynamic[iThreadIndex]
                IF g_Inboxes[iInboxIndex].ThreadIndex[iThreadIndex] = ENUM_TO_INT(paramThread)
                    IF g_Inboxes[iInboxIndex].HasBeenViewed[iThreadIndex] = TRUE
                        RETURN TRUE
                    ENDIF
                ENDIF
            ENDIF
        ENDREPEAT
    ENDREPEAT
    RETURN FALSE
ENDFUNC



FUNC BOOL HAS_DYNAMIC_EMAIL_BEEN_READ_BY_PRIMARY_TARGET(DYNAMIC_EMAIL_THREAD_NAMES thread, 
                                      EMAIL_MESSAGE_ENUMS message,
                                      INT iNth = 0)

    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    IF index = -1
        RETURN FALSE
    ENDIF

    INT toTarg = ENUM_TO_INT(g_AllEmails[message].eTo)
    IF toTarg >= TOTAL_INBOXES
        RETURN FALSE
    ENDIF
    
    INT iProgressSeek = -1
    INT i = 0
    INT target = iNth
    REPEAT g_DynamicEmailThreadBuffers[index].iProgress i
        IF g_DynamicEmailThreadBuffers[index].emails[i].sourceEmail = message
            IF target = 0
                iProgressSeek = i            
            ELSE
                --target
            ENDIF
            
        ENDIF
    ENDREPEAT
    
    IF iProgressSeek = -1
        RETURN FALSE
    ENDIF
    
    //that is dynamic
    i = 0
    REPEAT MAX_INBOX_LOGGED_MAILS_THREADS i
        IF  g_Inboxes[toTarg].IsDynamic[i]
        AND g_Inboxes[toTarg].ThreadIndex[i] = g_DynamicEmailThreadBuffers[index].registrationID
        AND g_Inboxes[toTarg].EmailsLogIndex[i] = iProgressSeek
            //found the bugger, hooray
            RETURN g_Inboxes[toTarg].HasBeenViewed[i]   
        ENDIF
    ENDREPEAT

    RETURN FALSE

ENDFUNC


//check for response picked from thread
FUNC BOOL HAS_LAST_EMAIL_IN_DYNAMIC_THREAD_BEEN_READ(DYNAMIC_EMAIL_THREAD_NAMES thread)
    //is the selected thread in the buffer?
        //if not then false
    INT index = GET_DYNAMIC_EMAIL_THREAD_BUFFER_INDEX(thread)
    IF index = -1
        RETURN FALSE
    ENDIF
        
    //has the last email in the selected thread been responded to?
    IF g_DynamicEmailThreadBuffers[index].iProgress = 0
        RETURN FALSE
    ENDIF
        //find the target
        //target email
        EMAIL_MESSAGE_ENUMS emailID = g_DynamicEmailThreadBuffers[index].emails[g_DynamicEmailThreadBuffers[index].iProgress-1].sourceEmail
        //target emailer
        
        RETURN HAS_DYNAMIC_EMAIL_BEEN_READ_BY_PRIMARY_TARGET(thread, emailID)
        
ENDFUNC
