// Functions only used when setting up an RC mission
USING "rage_builtins.sch"
USING "globals.sch"
USING "RC_Helper_Functions.sch"

STRUCT TIME_DATA
	INT				iYear
	MONTH_OF_YEAR	month
	INT				iDay
	INT				iHour
	INT				iMinute
ENDSTRUCT

/// PURPOSE:
///    Works out what time it will be x hours and y minutes from now
/// PARAMS:
///    addHours		- Number of hours to add
///    addMinutes	- Number of minutes to add
///    time			- Struct to store the future time
PROC GET_FUTURE_TIME(INT addHours, INT addMinutes, TIME_DATA& time)
	INT iDummy
	DAY_OF_WEEK dummyDayOfWeek
	SIMULATE_ADD_TO_CLOCK_TIME(addHours, addMinutes, 0, time.iYear, time.month, time.iDay, time.iHour, time.iMinute, iDummy, dummyDayOfWeek)
ENDPROC

/// PURPOSE:
///    Used to check if the current clock is later than the time passed in.
/// PARAMS:
///    time			- Struct containing time to test clock against
FUNC BOOL IS_CLOCK_PAST_TIME(TIME_DATA& time)
	
	IF GET_CLOCK_YEAR() > time.iYear
		RETURN TRUE
	ELIF GET_CLOCK_YEAR() = time.iYear
		
		IF ENUM_TO_INT(GET_CLOCK_MONTH()) > ENUM_TO_INT(time.month)
			RETURN TRUE
		ELIF ENUM_TO_INT(GET_CLOCK_MONTH()) = ENUM_TO_INT(time.month)
			
			IF GET_CLOCK_DAY_OF_MONTH() > time.iDay
				RETURN TRUE
			ELIF GET_CLOCK_DAY_OF_MONTH() = time.iDay
				
				IF GET_CLOCK_HOURS() > time.iHour
					RETURN TRUE
				ELIF GET_CLOCK_HOURS() = time.iHour
					
					IF GET_CLOCK_MINUTES() >= time.iMinute
						RETURN TRUE
					ENDIF
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
