USING "commands_graphics.sch"
USING "script_maths.sch"

CONST_INT	CONST_iMaxDataSlots	8

STRUCT structMGTitleColour
	FLOAT	fTitleRed = 255.0
	FLOAT 	fTitleBlue = 255.0
	FLOAT	fTitleGreen = 255.0
ENDSTRUCT

ENUM mgStatMedals
	mgMedalEmpty = 0,
	mgMedalBronze,
	mgMedalSilver,
	mgMedalGold,
	mgMedalRampage
ENDENUM

STRUCT structMGTotals
	mgStatMedals	eMGMedal = mgMedalEmpty
	FLOAT			fMGMedalTotal = -1.0
	STRING			sMGMedalTotal
	STRING			sMGMedalMedalLabel
ENDSTRUCT

ENUM mgStatType
	mgStatInvalid = 0,	
	mgStatNumber,
	mgStatString,
	mgStatPercentage,
	mgStatTime,
	mgStatFraction,
	mgStatDollar,
	mgStatNoBox,
	mgStatStringWithPlayerName
ENDENUM

STRUCT structMGDataSlot
	INT 			iMGSlotNum
	INT 			iMGPassed
	mgStatType		eMGStatType
	FLOAT			fMGObj_1 
	FLOAT			fMGObj_2 
	STRING			sMGStat_1
	STRING			sMGStat_2
ENDSTRUCT

STRUCT structMGStatsTracker
	STRING 					sMGTitle
	STRING 					sMGDesc
	structMGTitleColour		eMGTitleColour	
	structMGTotals			mgTotals	
	structMGDataSlot		mgDataSlot[CONST_iMaxDataSlots]
	INT						iMGSlots = -1	
	FLOAT					fDisplayTime = 10.0
ENDSTRUCT

ENUM mgStatsBitFlags
	mgStatsScriptRequested = BIT0
ENDENUM

ENUM eMGStatsCheckboxType
	CHECKBOX_TYPE_HIDDEN = -1,  	//= don't display checkbox
   	CHECKBOX_TYPE_UNCHECKED, 	//= failure
    CHECKBOX_TYPE_CHECKED		//= success
ENDENUM

