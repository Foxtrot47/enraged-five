USING "respawn_location_private.sch"
USING "cellphone_public.sch"
USING "flow_mission_trigger_public.sch"
USING "RC_helper_functions.sch"
USING "friends_private.sch"

CONST_INT 		MAX_INVALIDMODS					10
CONST_FLOAT 	MAX_STREAM_IN_DIST				100.0	// Distance at which the scene loads and is created.
CONST_INT		MAX_RESTRICTED_AREAS			32
CONST_INT		MAX_RESTRICTED_ZONES			13
CONST_INT		MAX_HELP_BITFIELDS				2

ENUM GETAWAY_ID
	FIB4_GETAWAY,
	AGENCY_GETAWAY,
	FIN_GETAWAY
ENDENUM

ENUM MISSION_STATE
	MS_PLACE_FIRST_ITEM = 0,
	MS_DROP_OFF_AGENCY_CAR,
	MS_MONITOR_GETAWAY_AREA,
	MS_WAIT_ON_MISSION,
	MS_DISPLAYING_SPLASH,
	MS_DONE
ENDENUM

ENUM COP_MONITOR
	CM_MONITER,
	CM_INIT_WANTED,
	CM_LOSING_WANTED	
ENDENUM

ENUM HELP_TEXT_STATES
	HTS_WAITING_HELP,
	HTS_DISPLAY,
	HTS_WAIT_DISPLAY,
	HTS_DISPLAYING,
	HTS_FINISHED
ENDENUM

ENUM PHONE_CALL_STATE
	PCS_NULL,
	PCS_CONV_STARTED,
	PCS_UPDATE_LINE,
	PCS_WAITING_LAST_LINE,
	PCS_WAITING_PHONE_AWAY,
	PCS_DO_EXITED_VEHICLE_LINE
ENDENUM

ENUM RESTRICTION_TYPE
	RT_PUBLIC_AREA= 0,
	RT_RESIDENTAL_AREA,
	RT_LAW_ENFORCEMENT,
	RT_INACCESSIBLE,
	RT_NO_RESTRICTION,
	NUM_AREA_TYPES
ENDENUM

ENUM RESTRICTED_AREA_MONITOR_STATES
	RAMS_SLEEPING,
	RAMS_CHECK_IF_AREAS_IN_RANGE
ENDENUM

ENUM GETAWAY_FLOW_FLAGS
	GFF_IS_CAR_VALID = 0,							//0
	GFF_IS_HIDING_POS_VALID,						//1
	GFF_IS_PLAYER_NEAR_GETAWAY_AREA,				//2
	GFF_IN_FIB_VEHICLE_BEFORE_COMPLETE,				//3
	GFF_SHOULD_LOAD_TEXT,							//4
	GFF_TEXT_IS_LOADED,								//5
	GFF_CAR_INFO_GRABBED,							//6
	GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED,			//7
	GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE,			//8
	GFF_DO_EXIT_VEHICLE_CALL,						//9
	GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION,	//10
	GFF_ALT_CONTACT_SET,							//11
	GFF_DONE_LAST_PREP_REMINDER,					//12
	GFF_PLAYER_ENTERED_MOD_SHOP,					//13
	GFF_VEHICLE_PLACED,								//14
	GFF_SCRIPT_CLEARED_HELP,						//15
	GFF_PLAYER_IS_TREV_AGENCY,						//16
	GFF_CHECK_FIRST_HELP_BIT,						//17
	GFF_CREATED_BLIP_SHOWED_HELP_FIN,				//18
	GFF_GIVEN_BIGSCORE_SPECIFIC_CAR_HELP,			//19
	GFF_TIME_OUT_HIDING_PLACE
ENDENUM

ENUM RESULTS_SCREEN_STATES
	RSS_INIT,
	RSS_LOAD,
	RSS_UPDATE,
	RSS_CLEANUP
ENDENUM

//Need to be kept in order of priority
ENUM HELP_TEXT_ENUMS 
	HTE_ON_MISSION = 0,					///0
	HTE_ON_FRIEND_ACT,					///1
	HTE_ON_TAXI_MISSION,				///2
	HTE_WANTED_LOSE,					///3
	HTE_DROP_AT_CAR_PARK,				///4
	HTE_LEFT_CAR,						///5
	HTE_HEALTH_VEH,
	HTE_LOC_SUITABLEFT,					///6
	HTE_LOC_SUITABLEM,					///7
	HTE_LOC_FIRST_BIGSCORE,
	HTE_PICK_CAR_FRANK_TREV,			///8
	HTE_PICK_CAR_MIKE,					///9
	HTE_PICK_NEW_LOC_FRANK_TREV,		///10
	HTE_PICK_NEW_LOC_MIKE,				///11
	HTE_UNSUITABLE_VEH,					///12
	HTE_UNSUITABLE_VEH_SEATS,			///13
	HTE_CARGOBOBBED_VEH,				///14
	HTE_VEH_CAN_BE_USED_FRANK_TREV,		///15
	HTE_VEH_CAN_BE_USED_MIKE,			///16
	HTE_LOC_PLAN_BOARD,
	HTE_LOC_INACC,						///17
	HTE_LOC_PUBLIC_AREA,				///18
	HTE_LOC_LAW,						///19
	HTE_LOC_RESIDENTIAL,				///20
	HTE_VEH_STOP,						///21
	HTE_VEH_OWNED_MIKE,					///22
	HTE_VEH_OWNED_FRANK,				///23
	HTE_VEH_OWNED_TREV,					///24
	HTE_LOC_SEC_ROUTE,					///25
	HTE_LOC_FIB_LOC,					///26
	HTE_LOC_LEST_HOUSE,					///27
	HTE_LOC_AGENCY_BUILDING,			///28
	HTE_LOC_SAFEHOUSE_M,				///29
	HTE_LOC_SAFEHOUSE_F,				///30
	HTE_LOC_SAFEHOUSE_T,				///31

	///overflow
	HTE_LOC_PUBLIC,   					///32
	HTE_LOC_WATER,						///33
	HTE_LOC_OBSTRUCTED,					///34
	HTE_LOC_OBSTRUCTED_VEH,				///35
	HTE_VEH_UPSIDEDOWN,					///36
	HTE_VEH_NEAR_ROAD,					///37
	HTE_VEH_ON_ROAD,					///38
	HTE_LOC_STEEP,						///39
	HTE_LOC_UNEVEN,						///40
	HTE_PLACEMENT_REMINDER,				///41
	HTE_PLACEMENT_REMINDER_BIG,			///42

	NUM_HELP
ENDENUM

STRUCT PLAYER_PHONE_CALL_STRUCT
	INT iPlayerVoice
	STRING sPlayerVoice
	
	INT iOtherVoice
	STRING sOtherVoice
	
	STRING sConv
	STRING sResponse
	
	STRING sZoneName
	STRING sZoneConvLable
	
	enumCharacterList eContact
ENDSTRUCT

STRUCT RESTRICTED_AREA
	STRING 				sZone
	VECTOR 				vPos1
	VECTOR 				vPos2
	FLOAT 				fWidth
	
ENDSTRUCT

STRUCT RESTRICTED_AREA_LIST
	RESTRICTED_AREA		mRestrictedArea[MAX_RESTRICTED_AREAS]
	INT					iNumAreas	
	INT					iAreasToCheckBit = 0 
	INT					iTimeDelay = 0
	RESTRICTION_TYPE	eType
ENDSTRUCT

MISSION_STATE eMissionState = MS_PLACE_FIRST_ITEM
COP_MONITOR eCopMonitor = CM_MONITER
HELP_TEXT_STATES eHelpTextState = HTS_WAITING_HELP
PHONE_CALL_STATE ePhoneCallState = PCS_NULL
RESULTS_SCREEN_STATES eResultScreenState = RSS_INIT
GETAWAY_ID eGetawayID

enumCharacterList eHighlighted = NO_CHARACTER

PLAYER_PHONE_CALL_STRUCT mPlayerPhoneCallSettings


//help and flow
STRING sHelpLables[NUM_HELP]
INT iHelpToDisplay = -1
INT iHelpTextBit[MAX_HELP_BITFIELDS]
FLOAT fHelpDisplayedFor = 0.0

INT iGetawayFlowFlag = 0 

//Hiding vars
VECTOR vPotentialHidingPos = <<0,0,0>>

//Vehicle
VEHICLE_INDEX viVeh
VECTOR vCarPosition = <<0,0,0>>
FLOAT fCarHeading = 0.0
VEHICLE_SETUP_STRUCT mDroppedOffCarStruct
MODEL_NAMES mnInvalidModels[MAX_INVALIDMODS]

//restricted
RESTRICTED_AREA_MONITOR_STATES eRestrictedAreaMonitor = RAMS_CHECK_IF_AREAS_IN_RANGE
RESTRICTED_AREA_LIST mRestrictedAreaList[RT_NO_RESTRICTION]
STRING sRestrictedZoneList[MAX_RESTRICTED_ZONES]
STRING sCurrentZone = ""
INT iAreaBeingChecked = 0
INT iAreaTypeBeingChecked = 0 
INT iRestrictedAreaCheckDelay = -1

//result screen
SCALEFORM_INDEX splash
INT iTimeSplashEnd

INT iReminderTimer = -1
INT iReminderCount = 0

INT iHighlightWindow = -1

STRING sTextblockConvs

structPedsForConversation s_conversation_peds		//conversation struct

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID iGetawayWid
	BOOL bDisplayRestricedZone[4]
	BOOL bZoneDebug = FALSE
	BOOL bAddNewArea = FALSE
	BOOL bPrintNewArea = FALSE
	VECTOR vNewAreaPos1, vNewAreaPos2
	FLOAT fNewAreaWidth
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(iGetawayWid)
			DELETE_WIDGET_GROUP(iGetawayWid)
		ENDIF
	ENDPROC 

	/// PURPOSE:
	///    Sets up getaway vehicle widget
	PROC SET_UP_RAG_WIDGETS()
		CLEANUP_OBJECT_WIDGETS()
	
		iGetawayWid = START_WIDGET_GROUP("Getaway Vehicle")
			
			ADD_WIDGET_BOOL("Toggle Zone debug print out", bZoneDebug)

			START_WIDGET_GROUP("Public Restricted Zones")
				ADD_WIDGET_BOOL("Display current public restricted zones", bDisplayRestricedZone[0])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Law enforcement Restricted Zones")
				ADD_WIDGET_BOOL("Display current Law enforcement restricted zones", bDisplayRestricedZone[1])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Residential Restricted Zones")
				ADD_WIDGET_BOOL("Display current Residential restricted zones", bDisplayRestricedZone[2])
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Inaccessible Restricted Zones")
				ADD_WIDGET_BOOL("Display current Inaccessible restricted zones", bDisplayRestricedZone[3])
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Create new area")
				ADD_WIDGET_BOOL("Activate new area creation", bAddNewArea)
				ADD_WIDGET_BOOL("Print new area out to temp debug file", bPrintNewArea)
				
				ADD_WIDGET_FLOAT_SLIDER("New Area Width", fNewAreaWidth, -99999, 99999, 0.1)
				//mod z height
				ADD_WIDGET_VECTOR_SLIDER("Area pos 1 - LMB", vNewAreaPos1, -99999, 99999, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Area pos 2 - RMB", vNewAreaPos2, -99999, 99999, 0.1)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("Getaway flow flags")
				ADD_BIT_FIELD_WIDGET("State flow flags", iGetawayFlowFlag)
				ADD_BIT_FIELD_WIDGET("Help text flags", iHelpTextBit[0])
				ADD_BIT_FIELD_WIDGET("Help text flags", iHelpTextBit[1])
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	ENDPROC

	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)

		VECTOR vBottom[2]
		VECTOR vTop[2]
		
		vBottom[0] = vec1
		vBottom[1] = vec2
		
		vTop[0] = vec1
		vTop[1] = vec2
		
		IF vec1.z > vec2.z
			vBottom[0].z = vec2.z
			vBottom[1].z = vec2.z
			vTop[0].z = vec1.z
			vTop[1].z = vec1.z
		ELSE
			vBottom[0].z = vec1.z
			vBottom[1].z = vec1.z
			vTop[0].z = vec2.z
			vTop[1].z = vec2.z
		ENDIF

		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
		VECTOR w = side * (width / 2.0)

		// Bottom points
		VECTOR c1 = vBottom[0] - w  // base left
		VECTOR c2 = vBottom[0] + w  // base right              
		VECTOR c3 = vBottom[1] + w  // top rt
		VECTOR c4 = vBottom[1] - w  // top lt

		// Top points
		VECTOR d1 = vTop[0] - w  // base left
		VECTOR d2 = vTop[0] + w  // base right              
		VECTOR d3 = vTop[1] + w  // top rt
		VECTOR d4 = vTop[1] - w  // top lt

		// Draw bottom lines
		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
		// Draw top lines
		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
		// Draw uprights
		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
		
	ENDPROC
	
	/// PURPOSE:
	///    
	/// PARAMS:
	///    t - 
	PROC DRAW_RESTRICTED_ZONE(RESTRICTION_TYPE t)
		INT z
		FOR z=0 TO (mRestrictedAreaList[t].iNumAreas-1)
			IF IS_BIT_SET(mRestrictedAreaList[t].iAreasToCheckBit, z)
				DRAW_DEBUG_LOCATE_SPECIAL(mRestrictedAreaList[t].mRestrictedArea[z].vPos1, mRestrictedAreaList[t].mRestrictedArea[z].vPos2, mRestrictedAreaList[t].mRestrictedArea[z].fWidth)
			ENDIF
		ENDFOR
	ENDPROC
	
	/// PURPOSE:
	///    If we are adding a new restricted area monitor pressing the correct
	///    mouse buttons
	///    Or if we want to print out the values for the new area
	PROC MANAGE_NEW_AREA_CREATION()
		IF bAddNewArea
			IF IS_MOUSE_BUTTON_PRESSED(MB_RIGHT_BTN)
				IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
					vNewAreaPos2 = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
			ELSE
				IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
					vNewAreaPos1 = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
			ENDIF

			DRAW_DEBUG_LOCATE_SPECIAL(vNewAreaPos1, vNewAreaPos2, fNewAreaWidth)

			IF bPrintNewArea
				//FILL_RESTRICTED_AREA("MTCHIL", <<0,0,0>>, <<0,0,0>>, 0)
				SAVE_STRING_TO_DEBUG_FILE("FILL_RESTRICTED_AREA(\"")
				SAVE_STRING_TO_DEBUG_FILE(GET_NAME_OF_ZONE(vNewAreaPos1))
				SAVE_STRING_TO_DEBUG_FILE("\", ")
				SAVE_VECTOR_TO_DEBUG_FILE(vNewAreaPos1)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(vNewAreaPos2)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_FLOAT_TO_DEBUG_FILE(fNewAreaWidth)
				SAVE_STRING_TO_DEBUG_FILE(")")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				bPrintNewArea = FALSE
			ENDIF
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Update the rag widget drawing the restricted areas if the tick boxes are ticked
	PROC UPDATE_WIDGET()
		INT i 
		FOR i=0 TO 2
			IF bDisplayRestricedZone[0]
				DRAW_RESTRICTED_ZONE(RT_PUBLIC_AREA)
			ENDIF
			IF bDisplayRestricedZone[1]
				DRAW_RESTRICTED_ZONE(RT_LAW_ENFORCEMENT)
			ENDIF
			IF bDisplayRestricedZone[2]
				DRAW_RESTRICTED_ZONE(RT_RESIDENTAL_AREA)
			ENDIF
			IF bDisplayRestricedZone[3]
				DRAW_RESTRICTED_ZONE(RT_INACCESSIBLE)
			ENDIF
		ENDFOR
		MANAGE_NEW_AREA_CREATION()
	ENDPROC
	
	/// PURPOSE:
	///    doesnt nothing
	PROC UPDATE_DEBUG_PASS()
		

	ENDPROC

	BOOL bShowDebugText = TRUE
//	INT iOnMissionTimerCheck = -1
	INT iGenericPrintTimer = -1
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

#ENDIF

///******************************************************************************************************
///    					Flow STUFF			******				Flow STUFF
///******************************************************************************************************


/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup(e_g_Restore_Launched_ScriptBits removeRelaunch, BOOL bGoingToMP = FALSE)
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (FALSE)
		CLEANUP_OBJECT_WIDGETS()
	#ENDIF
	IF IS_VEHICLE_OK(viVeh)
		SAFE_RELEASE_VEHICLE(viVeh)
	ENDIF

	IF NOT bGoingToMP
		REMOVE_SCRIPT_FROM_RELAUNCH_LIST(removeRelaunch)//LAUNCH_BIT_FBI4_PREP3) 
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

/// PURPOSE:
///    Function updates the global INT used to save the state the  script is in
///    Requests an auto save as well
/// PARAMS:
///    state - The mission state to switch to
///    bJustUpdate - Should we just update the save global without requesting an auto save
PROC UPDATE_SAVED_STATE(MISSION_STATE state, BOOL bJustUpdate = FALSE)
	g_savedGlobals.sAmbient.iGetawayState = ENUM_TO_INT(state)
	
	eMissionState = state
	IF bJustUpdate
		EXIT
	ENDIF

	MAKE_AUTOSAVE_REQUEST()
ENDPROC




FUNC STRING SUB_HELP_USABLE(HELP_TEXT_ENUMS eLable)
	STRING sLable

	IF eGetawayID = FIB4_GETAWAY
		IF eLable = HTE_VEH_CAN_BE_USED_FRANK_TREV
			sLable = "PRC_USEFT"//This vehicle can be used as a getaway vehicle. Hide it in a discreet location. Select the "Mark Getaway Location" option when phoning Franklin or Trevor.

		ELIF  eLable = HTE_VEH_CAN_BE_USED_MIKE
			sLable = "PRC_USEM"//This vehicle can be used as a getaway vehicle. Hide it in a discreet location. Select the "Mark Getaway Location" option when phoning Michael.
			
		ELIF  eLable = 	HTE_PICK_CAR_FRANK_TREV
			sLable = "PRC_PICKCARTF"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Location" option when phoning Franklin or Trevor.

		ELIF  eLable = 	HTE_PICK_CAR_MIKE
			sLable = "PRC_PICKCARM"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Location" option when phoning Michael.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_MIKE
			sLable = "PRC_PICKNEWM"//Pick a new location for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_FRANK_TREV
			sLable = "PRC_PICKNEWTF"//Pick a new location for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Franklin or Trevor.

		ELIF  eLable = 	HTE_LOC_SUITABLEFT
			sLable = "PRC_LOCSUITFT"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Franklin or Trevor.

		ELIF  eLable = 	HTE_LOC_SUITABLEM
			sLable = "PRC_LOCSUITM"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael.
	
		ELIF  eLable = 	HTE_LOC_INACC
			sLable = "PRC_INACC"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael.
		ENDIF

	ELIF eGetawayID = AGENCY_GETAWAY
		IF eLable = HTE_VEH_CAN_BE_USED_FRANK_TREV
			sLable = "PRC_USEFL"//This vehicle can be used as a getaway vehicle. Hide it in a discreet location. Select the "Mark Getaway Location" option when phoning Franklin or Lester.

		ELIF  eLable = HTE_VEH_CAN_BE_USED_MIKE
			sLable = "PRC_USEML"//This vehicle can be used as a getaway vehicle. Hide it in a discreet location. Select the "Mark Getaway Location" option when phoning Michael or Lester.

		ELIF  eLable = 	HTE_PICK_CAR_FRANK_TREV
			sLable = "PRC_PICKCRFL"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Location" option when phoning Franklin or Lester.

		ELIF  eLable = 	HTE_PICK_CAR_MIKE
			sLable = "PRC_PICKCRML"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Location" option when phoning Michael or Lester.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_MIKE
			sLable = "PRC_PICKNEWML"//Pick a new location for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael or Lester.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_FRANK_TREV
			sLable = "PRC_PICKNEWFL"//Pick a new location for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Franklin or Lester.

		ELIF  eLable = 	HTE_LOC_SUITABLEFT
			sLable = "PRC_LOCSUITFL"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Franklin or Lester.

		ELIF  eLable = 	HTE_LOC_SUITABLEM
			sLable = "PRC_LOCSUITML"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael or Lester.
			
		ELIF  eLable = 	HTE_LOC_INACC
			sLable = "PRC_INACCF"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael or Lester.

		ENDIF

	ELIF eGetawayID = FIN_GETAWAY
		IF eLable = HTE_VEH_CAN_BE_USED_FRANK_TREV
			sLable = "PRC_USEL"//This vehicle can be used as a getaway vehicle. Select the "Mark Getaway Vehicle" option when phoning Lester.

		ELIF  eLable = HTE_VEH_CAN_BE_USED_MIKE
			sLable = "PRC_USEL"//This vehicle can be used as a getaway vehicle. Select the "Mark Getaway Vehicle" option when phoning Lester.

			
		ELIF  eLable = 	HTE_PICK_CAR_FRANK_TREV
			sLable = "PRC_PICKCARL"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Vehicle" option when phoning Lester.

		ELIF  eLable = 	HTE_PICK_CAR_MIKE
			sLable = "PRC_PICKCARL"//The getaway vehicle has been destroyed, find a new one. Select the "Mark Getaway Vehicle" option when phoning Lester.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_MIKE
			sLable = "PRC_PICKNEWL"//Getaway vehicle has been moved. Select the "Mark Getaway Vehicle" when phoning Lester, in a suitable vehicle to mark a new one.

		ELIF  eLable = 	HTE_PICK_NEW_LOC_FRANK_TREV
			sLable = "PRC_PICKNEWL"//Getaway vehicle has been moved. Select the "Mark Getaway Vehicle" when phoning Lester, in a suitable vehicle to mark a new one.

		ELIF  eLable = 	HTE_LOC_SUITABLEFT
			sLable = "PRC_LOCSUITFT"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Franklin or Trevor.

		ELIF  eLable = 	HTE_LOC_SUITABLEM
			sLable = "PRC_LOCSUITM"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael.

		ELIF  eLable = 	HTE_LOC_INACC
			sLable = "PRC_INACCF"//This location can be used for the getaway vehicle. Select the "Mark Getaway Location" option when phoning Michael or Lester.
		ENDIF
	ENDIF
	
	RETURN sLable
ENDFUNC



///******************************************************************************************************
///    					HELP TEXT BITS			******				HELP TEXT BITS
///******************************************************************************************************

/// PURPOSE:
///    assigns text lable to the correct slot in sHelpLables[NUM_HELP]
PROC POPULATE_HELP()
	sHelpLables[HTE_ON_MISSION] = "AM_H_FBIC1A"
	sHelpLables[HTE_ON_FRIEND_ACT] = "AM_H_FBIC1B"
	sHelpLables[HTE_ON_TAXI_MISSION] = "AM_H_FBIC1C"
	sHelpLables[HTE_WANTED_LOSE] = "PRC_WANT"
	sHelpLables[HTE_DROP_AT_CAR_PARK] = "PRC_DROPOFF"
	sHelpLables[HTE_LEFT_CAR] = "PRC_INVALVEH"
	sHelpLables[HTE_HEALTH_VEH] = "PRC_HEALTH"
	sHelpLables[HTE_LOC_SUITABLEFT] = SUB_HELP_USABLE(HTE_LOC_SUITABLEFT)
	sHelpLables[HTE_LOC_SUITABLEM] = SUB_HELP_USABLE(HTE_LOC_SUITABLEM)
	sHelpLables[HTE_LOC_FIRST_BIGSCORE] = "PRC_USEFIRST"
	sHelpLables[HTE_PICK_CAR_FRANK_TREV] = SUB_HELP_USABLE(HTE_PICK_CAR_FRANK_TREV)
	sHelpLables[HTE_PICK_CAR_MIKE] = SUB_HELP_USABLE(HTE_PICK_CAR_MIKE)
	sHelpLables[HTE_PICK_NEW_LOC_MIKE] = SUB_HELP_USABLE(HTE_PICK_NEW_LOC_MIKE)
	sHelpLables[HTE_PICK_NEW_LOC_FRANK_TREV] = SUB_HELP_USABLE(HTE_PICK_NEW_LOC_FRANK_TREV)
	sHelpLables[HTE_UNSUITABLE_VEH] = "PRC_UNUSE"
	sHelpLables[HTE_UNSUITABLE_VEH_SEATS] = "PRC_SEATS"
	sHelpLables[HTE_CARGOBOBBED_VEH] = "PRC_CBOBVAL"
	sHelpLables[HTE_VEH_CAN_BE_USED_FRANK_TREV] = SUB_HELP_USABLE(HTE_VEH_CAN_BE_USED_FRANK_TREV)
	sHelpLables[HTE_VEH_CAN_BE_USED_MIKE] = SUB_HELP_USABLE(HTE_VEH_CAN_BE_USED_MIKE)
	sHelpLables[HTE_LOC_INACC] = SUB_HELP_USABLE(HTE_LOC_INACC)
	sHelpLables[HTE_LOC_PUBLIC_AREA] = "PRC_PUBAREA"
	sHelpLables[HTE_LOC_LAW] = "PRC_LAWAREA"
	sHelpLables[HTE_LOC_RESIDENTIAL] = "PRC_RESAREA"
	sHelpLables[HTE_VEH_STOP] = "PRC_STOP"
	sHelpLables[HTE_VEH_OWNED_MIKE] = "PRC_OWNEDM"
	sHelpLables[HTE_VEH_OWNED_FRANK] = "PRC_OWNEDF"
	sHelpLables[HTE_VEH_OWNED_TREV] = "PRC_OWNEDT"
	sHelpLables[HTE_LOC_SEC_ROUTE] = "PRC_SECROUTE"
	sHelpLables[HTE_LOC_FIB_LOC] = "PRC_CLOSELOT"
	sHelpLables[HTE_LOC_LEST_HOUSE] = "PRC_CLOSELES"
	sHelpLables[HTE_LOC_AGENCY_BUILDING] = "PRC_CLSAGNT"
	sHelpLables[HTE_LOC_SAFEHOUSE_M] = "PRC_CLOSESAFE_M"
	sHelpLables[HTE_LOC_SAFEHOUSE_F] = "PRC_CLOSESAFE_F"
	sHelpLables[HTE_LOC_SAFEHOUSE_T] = "PRC_CLOSESAFE_T"
	sHelpLables[HTE_LOC_PUBLIC] = "PRC_PEDS"
	sHelpLables[HTE_LOC_WATER] = "PRC_WATER"
	sHelpLables[HTE_LOC_OBSTRUCTED] = "PRC_OBST"
	sHelpLables[HTE_LOC_OBSTRUCTED_VEH] = "PRC_OBSTVEH"
	sHelpLables[HTE_VEH_UPSIDEDOWN] = "PRC_UPDWN"
	sHelpLables[HTE_VEH_NEAR_ROAD] = "PRC_NEARROAD"
	sHelpLables[HTE_VEH_ON_ROAD] = "PRC_ONROAD"
	sHelpLables[HTE_LOC_PLAN_BOARD] = "PRC_PLAN"

	sHelpLables[HTE_LOC_STEEP] = "PRC_TOOSTEEP"
	sHelpLables[HTE_LOC_UNEVEN] = "PRC_UNEVEN"
	sHelpLables[HTE_PLACEMENT_REMINDER] = "PRC_REMIND"
	sHelpLables[HTE_PLACEMENT_REMINDER_BIG] = "PRC_REMINDA"
ENDPROC


/// PURPOSE:
///    Used for the overflow help text bit
///    Pass in an enum using ENUM_TO_INT if its over 31 in value
///    we take it away from 32 to give the value of the overflow help bitfield 
/// PARAMS:
///    i - the value of the enum we want to convert
/// RETURNS:
///    the converted value of i or just returns i if no 
///    converstion needed
FUNC INT CONVERT_FOR_OVER_FLOW_BIT(INT i)
	IF i > 31
		RETURN i - 32
	ENDIF

	RETURN i
ENDFUNC

/// PURPOSE:
///    Gets the index of the help text bitfield we need from the enum value passed in
///    enum value is ENUM_TO_INT conversion
/// PARAMS:
///    i - the enum value
/// RETURNS:
///    the index of the help bitfield  where the enum is meant 
///    to be stored
FUNC INT GET_HELP_BIT_ID(INT i)
	INT iReturnVal
	//if i is less then 32 we want it the first help bitfield 
	IF i < 32
		iReturnVal = 0
	ELSE
		//if i is greater then 32 we want it the overflow help bitfield 
		iReturnVal = 1
	ENDIF
	
	RETURN iReturnVal
ENDFUNC

/// PURPOSE:
///    Gets the string stored in the help lable array based off the 
///    enum value (ENUM_TO_INT)
/// PARAMS:
///    i - the enum value (ENUM_TO_INT)
/// RETURNS:
///    The string stored at sHelpLables[i]
FUNC STRING GET_HELP_LABLE_FROM_BIT(INT i)
	RETURN sHelpLables[i]
ENDFUNC

/// PURPOSE:
///    Sets a help text bit to display some help text
///    Sets the bShouldHelpUpdate flag to true
/// PARAMS:
///    HelpToShow - The help text enum to show
PROC SET_HELP_TEXT_BIT(HELP_TEXT_ENUMS HelpToShow)
	INT iHelp = CONVERT_FOR_OVER_FLOW_BIT(ENUM_TO_INT(HelpToShow))
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION,"Setting help bit == ", GET_STRING_FROM_TEXT_FILE(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HelpToShow))) ) #ENDIF
	SET_BIT(iHelpTextBit[GET_HELP_BIT_ID(ENUM_TO_INT(HelpToShow))], iHelp)
ENDPROC

/// PURPOSE:
///    Clears a help text bit 
/// PARAMS:
///    HelpToCLEAR - the help text blip to clear
PROC CLEAR_HELP_TEXT_BIT(HELP_TEXT_ENUMS HelpToCLEAR)
	INT iHelp = CONVERT_FOR_OVER_FLOW_BIT(ENUM_TO_INT(HelpToCLEAR))
	
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION,"Clearing help bit == ", GET_STRING_FROM_TEXT_FILE(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HelpToCLEAR)) )) #ENDIF

	CLEAR_BIT(iHelpTextBit[GET_HELP_BIT_ID(ENUM_TO_INT(HelpToCLEAR))], iHelp)
ENDPROC

/// PURPOSE:
///    Clears two help bits and if either one of them is being displayed we clear the 
///    help text 
/// PARAMS:
///    HelpToCLEAR - The first help bit to clear
///    HelpToCLEAR2 - the second help bit to clear
PROC CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT(HELP_TEXT_ENUMS HelpToCLEAR, HELP_TEXT_ENUMS HelpToCLEAR2 = NUM_HELP)
	CLEAR_HELP_TEXT_BIT(HelpToCLEAR)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HelpToCLEAR)))
		CLEAR_HELP()
		CPRINTLN(DEBUG_MISSION,"CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT() cleared help 1")
		SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
	ENDIF

	IF HelpToCLEAR2 != NUM_HELP
		CLEAR_HELP_TEXT_BIT(HelpToCLEAR2)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HelpToCLEAR2)))
			CLEAR_HELP()
			CPRINTLN(DEBUG_MISSION,"CLEAR_DISPLAYING_HELP_AND_CLEAR_BIT() cleared help 2")
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Iterates through all help bits and clears them
/// PARAMS:
///    Exclusion - The bit to exclude from clear up
PROC CLEAR_ALL_HELP_BITS(HELP_TEXT_ENUMS Exclusion, BOOL bClearHelp = FALSE)
	INT i

//	IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpLables[Exclusion])
//		#IF IS_DEBUG_BUILD SK_PRINT_INT("Clearing help -> not being displayed", ENUM_TO_INT(Exclusion) ) #ENDIF
//		CLEAR_HELP()
//	ENDIF

	FOR i=0 TO (ENUM_TO_INT(NUM_HELP)-1)
		IF Exclusion != INT_TO_ENUM(HELP_TEXT_ENUMS, i)
		AND Exclusion != NUM_HELP
			CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, i))
		ENDIF
	ENDFOR

	#IF IS_DEBUG_BUILD 
		IF Exclusion != NUM_HELP
			CPRINTLN(DEBUG_MISSION,"Excluding this bit == ", GET_STRING_FROM_TEXT_FILE(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(Exclusion)) ))
		ENDIF
	#ENDIF
	IF bClearHelp 
		IF Exclusion != NUM_HELP
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(Exclusion)))
			AND NOT g_bFlowHelpDisplaying
			AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
				CPRINTLN(DEBUG_MISSION,"CLEAR_ALL_HELP_BITS() cleared help 1")
				CLEAR_HELP()
			ENDIF
		ELSE
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT g_bFlowHelpDisplaying
			AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
				CPRINTLN(DEBUG_MISSION,"CLEAR_ALL_HELP_BITS() cleared help 2")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///******************************************************************************************************
///    					COP STUFF			******				COP STUFF
///******************************************************************************************************
PROC MONITER_PLAYER_WANTED()
	IF eMissionState = MS_PLACE_FIRST_ITEM
	OR eMissionState = MS_DROP_OFF_AGENCY_CAR
		SWITCH eCopMonitor
			CASE CM_MONITER
				
				IF IS_THIS_PRINT_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_WANTED_LOSE)))
					SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
					CLEAR_HELP()
					CPRINTLN(DEBUG_MISSION,"MONITER_PLAYER_WANTED() cleared help 1")
				ENDIF
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					#IF IS_DEBUG_BUILD SK_PRINT("eCopMonitor = CM_INIT_WANTED") #ENDIF
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_LOC_SUITABLEFT)))
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_LOC_SUITABLEM)))
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_PICK_CAR_FRANK_TREV)))
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_PICK_CAR_MIKE)))
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_PICK_NEW_LOC_FRANK_TREV)))
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_PICK_NEW_LOC_MIKE)))
					AND NOT IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
					AND NOT g_bFlowHelpDisplaying
						CPRINTLN(DEBUG_MISSION, "1 Clearing wanted level help text - ", GET_THIS_SCRIPT_NAME())
						SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
						CLEAR_HELP()
						CPRINTLN(DEBUG_MISSION,"MONITER_PLAYER_WANTED() cleared help 2")
					ENDIF
					eCopMonitor = CM_INIT_WANTED
				ENDIF
			BREAK

			CASE CM_INIT_WANTED
				IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
					IF eGetawayID = FIN_GETAWAY
						IF eMissionState = MS_DROP_OFF_AGENCY_CAR
							IF IS_VEHICLE_OK(viVeh)
								SET_HELP_TEXT_BIT(HTE_WANTED_LOSE)
								CLEAR_ALL_HELP_BITS(HTE_WANTED_LOSE, TRUE)
								#IF IS_DEBUG_BUILD SK_PRINT("Fin Getaway car valid lose wanted level") #ENDIF
							ENDIF
						ENDIF

						eCopMonitor = CM_LOSING_WANTED
					ELSE
						IF IS_VEHICLE_OK(viVeh)
							SET_HELP_TEXT_BIT(HTE_WANTED_LOSE)
							CLEAR_ALL_HELP_BITS(HTE_WANTED_LOSE, TRUE)
							#IF IS_DEBUG_BUILD SK_PRINT("Car valid Lose wanted level") #ENDIF
						ENDIF
						eCopMonitor = CM_LOSING_WANTED
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT("Car not valid Getaway lose wanted level") #ENDIF
					eCopMonitor = CM_LOSING_WANTED
				ENDIF
				
			BREAK
			
			CASE CM_LOSING_WANTED
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_WANTED_LOSE)))
						CPRINTLN(DEBUG_MISSION, "2 Clearing wanted level help text - ", GET_THIS_SCRIPT_NAME())
						SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
						CLEAR_HELP()
					ENDIF
					#IF IS_DEBUG_BUILD SK_PRINT("MONITER_PLAYER_WANTED - eCopMonitor = CM_MONITER") #ENDIF
					eCopMonitor = CM_MONITER
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

///******************************************************************************************************
///    					HELP TEXT MONITOR			******				HELP TEXT MONITOR
///******************************************************************************************************

/// PURPOSE:
///    -***** Dont think this is used *****-
///    Uses the flow help queue to garentee help is displayed 
/// PARAMS:
///    HelpText - the help lable to display
///    priority - the priority of the help if high it will try and display right away
///    delay - the delay before the help is displayed
///    expire - time before the help will stop trying to be displayed 
///    display - time to display the help for
/// RETURNS:
///    TRUE when the help has been displayed
FUNC BOOL DO_MISSION_FLOW_HELP(STRING HelpText, FlowHelpPriority priority = FHP_HIGH, INT delay = 0, INT expire = FLOW_HELP_NEVER_EXPIRES, INT display = DEFAULT_HELP_TEXT_TIME)

	#IF IS_DEBUG_BUILD SK_PRINT("DO_MISSION_FLOW_HELP - START") #ENDIF
	SWITCH GET_FLOW_HELP_MESSAGE_STATUS(HelpText)
		CASE FHS_EXPIRED
			#IF IS_DEBUG_BUILD SK_PRINT("ADD_HELP_TO_FLOW_QUEUE ") #ENDIF
			ADD_HELP_TO_FLOW_QUEUE(HelpText, priority, delay, expire, display)
		BREAK
		CASE FHS_DISPLAYED
			#IF IS_DEBUG_BUILD SK_PRINT("DO_MISSION_FLOW_HELP - END") #ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOW_FLAG_IDS GET_FLOW_FLAG()
	IF eGetawayID = FIB4_GETAWAY
		RETURN FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED
	ELIF eGetawayID = AGENCY_GETAWAY
		RETURN FLOWFLAG_HEIST_AGENCY_PREP_2_DONE
	ELIF eGetawayID = FIN_GETAWAY
		RETURN FLOWFLAG_HEIST_FINALE_PREPE_DONE
	ENDIF
	
	RETURN FLOWFLAG_NONE
ENDFUNC

FUNC INT GET_NUM_PREPS_COMPLETE_CHECK(SP_MISSIONS startMis, SP_MISSIONS endMis)
	INT iCount = 0
	INT iPrepMission

	FOR iPrepMission = ENUM_TO_INT(startMis) TO ENUM_TO_INT(endMis)
		CPRINTLN(DEBUG_MISSION, "PREP MISSION = = ", iPrepMission)
		IF GET_MISSION_COMPLETE_STATE(INT_TO_ENUM(SP_MISSIONS, iPrepMission))
			iCount++
		ENDIF
	ENDFOR
	
	IF GET_MISSION_FLOW_FLAG_STATE(GET_FLOW_FLAG())
		iCount++
	ENDIF

	CPRINTLN(DEBUG_MISSION, "iCount = = ", iCount)
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Runs through all the FIB 4 prep missions using the SP_ enum 
///    Checks their flow flags if they are complete add to the internal function count
///    Special case for FIB 4 prep 3 checks the flow flag and not its completion state
/// RETURNS:
///    TRUE if the internal count is greater or equal to 5 
FUNC BOOL ARE_ALL_OTHER_PREPS_COMPLETE_FIB()

	IF GET_NUM_PREPS_COMPLETE_CHECK(SP_MISSION_FBI_4_PREP_1, SP_MISSION_FBI_4_PREP_5) >= 4
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Runs through all the Agency prep missions using the SP_ enum 
///    Checks their flow flags if they are complete add to the internal function count
///    Special case for Agency prep 2 checks the flow flag and not its completion state
/// RETURNS:
///    TRUE if the internal count is greater or equal to 2 
FUNC BOOL ARE_ALL_OTHER_PREPS_COMPLETE_AGENCY()
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_PREP_1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Runs through all the Finale prep missions using the SP_ enum 
///    Checks their flow flags if they are complete add to the internal function count
///    Special case for Finale prep E checks the flow flag and not its completion state
/// RETURNS:
///    TRUE if the internal count is greater or equal to 5 
FUNC BOOL ARE_ALL_OTHER_PREPS_COMPLETE_FIN()
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_B)
	AND GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_PREP_D)
	AND GET_MISSION_FLOW_FLAG_STATE(GET_FLOW_FLAG())
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    
/// RETURNS:
///    TRUE if preps done
FUNC BOOL ARE_ALL_OTHER_PREPS_COMPLETE()
	IF eGetawayID = FIB4_GETAWAY
		RETURN ARE_ALL_OTHER_PREPS_COMPLETE_FIB()
	ELIF eGetawayID = AGENCY_GETAWAY
		RETURN ARE_ALL_OTHER_PREPS_COMPLETE_AGENCY()
	ELIF eGetawayID = FIN_GETAWAY
		RETURN ARE_ALL_OTHER_PREPS_COMPLETE_FIN()
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Monitor the reminder timer 
///    if the player hasn't seen any getaway vehicle help text for 8 minutes and the
///    vehicle hasnt been placed the reminder help is displayed but it has the lowest priority so if there is another 
///    help message set at the same time this isn;t displayed 
///    Reminder is only displayed once
PROC MONITOR_GETAWAY_REMINDER()
	IF NOT GET_MISSION_FLOW_FLAG_STATE(GET_FLOW_FLAG())
	AND NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR)
	AND NOT DOES_PLAYER_HAVE_END_OF_MISSION_CALL_QUEUED(GET_CURRENT_PLAYER_PED_ENUM())
	AND NOT Is_Player_Timetable_Scene_In_Progress()
	AND NOT IS_CELLPHONE_CONVERSATION_PLAYING()
		IF iReminderCount < 2
		AND	(GET_GAME_TIMER() - iReminderTimer) > 480000 //8 mins
			IF eGetawayID != FIN_GETAWAY
				SET_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER)
			ELSE
				SET_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER_BIG)
			ENDIF
			iReminderCount++
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Set reminder help bit reminder count is = ", iReminderCount) #ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Displays the getaway reminder if the player comes off mission and the getaway vehicle hasnt been placed
///    and the other preps are complete
PROC MONITOR_COMING_OFF_MISSION()
	IF NOT GET_MISSION_FLOW_FLAG_STATE(GET_FLOW_FLAG())
		IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_DONE_LAST_PREP_REMINDER))
			IF ARE_ALL_OTHER_PREPS_COMPLETE()
				IF eGetawayID != FIN_GETAWAY
					SET_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER)
				ELSE
					SET_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER_BIG)
				ENDIF
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_DONE_LAST_PREP_REMINDER))
				#IF IS_DEBUG_BUILD SK_PRINT("Set reminder help bit as last prep has been completed") #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a help bit in a help bitfield has been set then kicks off the displaying help state machine
/// PARAMS:
///    bitField - The bitfield to check the bit against 
///    i - the bit to check in the bitfield
PROC CHECK_HELP_BITS(INT bitField, INT &i)
	INT iHelpIncrement = CONVERT_FOR_OVER_FLOW_BIT(i)

	IF IS_BIT_SET(bitField, iHelpIncrement)
		//Check to see that the player hasnt entered a garbage or tow truck before the completing the preps
		//if they havent do the normal help
		IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IN_FIB_VEHICLE_BEFORE_COMPLETE))
			//the bit was set 
			iHelpToDisplay = i
			eHelpTextState = HTS_DISPLAY
			fHelpDisplayedFor = 0.0
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MISSION, "Help text bit set setting state to display and breaking ")
				CPRINTLN(DEBUG_MISSION, "Help bit set text is ==  ", GET_STRING_FROM_TEXT_FILE(GET_HELP_LABLE_FROM_BIT(iHelpToDisplay))) 
				CPRINTLN(DEBUG_MISSION, "Help bit set index is ==  ", iHelpToDisplay) 
			#ENDIF
			
			//Clear the reminder bit as there is about to be some getaway related help displayed 
			CLEAR_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER)
			CLEAR_HELP_TEXT_BIT(HTE_PLACEMENT_REMINDER_BIG)
			//reset the timer as well
			iReminderTimer = GET_GAME_TIMER()
			
			//break so that i will = the lable we want to display
			i = ENUM_TO_INT(NUM_HELP)
			
		ELSE //if they have entered one of the prep vehicles and teh preps arent done done display the text as the mission will trigger

			IF ENUM_TO_INT(HTE_UNSUITABLE_VEH) = iHelpIncrement
				#IF IS_DEBUG_BUILD SK_PRINT(" Unsuitable help string to display when in a Tow or Garbage truck before completeing the prep ") #ENDIF
				CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, iHelpIncrement) )
				i = ENUM_TO_INT(NUM_HELP)
			ELSE
				//hmm this should probably just break out and clear the help bits but its been 
				//working for over 4 months 
				iHelpToDisplay = i
				eHelpTextState = HTS_DISPLAY
				fHelpDisplayedFor = 0.0
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MISSION, "Help text bit set we are in a tow truck or garbage truck but the help isnt unsuitable vehicle ")
					CPRINTLN(DEBUG_MISSION, "Help bit set text is ==  ", GET_STRING_FROM_TEXT_FILE(sHelpLables[iHelpToDisplay]))
					CPRINTLN(DEBUG_MISSION, "Help bit set index is ==  ", iHelpToDisplay) 
				 #ENDIF

				//break so that i will = the lable we want to display
				i = ENUM_TO_INT(NUM_HELP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DO_HELP_DISPLAYING_STATES(HELP_TEXT_ENUMS eHelpToShow, BOOL bOffMission = FALSE)
	SWITCH eHelpTextState
		CASE HTS_DISPLAY
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT DOES_PLAYER_HAVE_END_OF_MISSION_CALL_QUEUED(GET_CURRENT_PLAYER_PED_ENUM())
			AND NOT Is_Player_Timetable_Scene_In_Progress()
				CPRINTLN(DEBUG_MISSION, "There is not any help being displayed = HTS_DISPLAY")
				PRINT_HELP(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)), 15000)

				eHelpTextState = HTS_WAIT_DISPLAY
			ELSE
				CPRINTLN(DEBUG_MISSION, "Some help is being displayed  dont display help = HTS_DISPLAY")
			ENDIF
		BREAK

		CASE HTS_WAIT_DISPLAY
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
				CPRINTLN(DEBUG_MISSION, "help is being displayed change state = HTS_DISPLAYING")
				fHelpDisplayedFor = 0.0
				eHelpTextState = HTS_DISPLAYING
			ELSE
				fHelpDisplayedFor += GET_FRAME_TIME()
				IF fHelpDisplayedFor >= 20.0
					CPRINTLN(DEBUG_MISSION, "help Hasn't been displayed for 20s breaking out to try again = ", GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
					eHelpTextState = HTS_FINISHED
				ELIF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
					CPRINTLN(DEBUG_MISSION, "help is not being displayed change state = HTS_FINISHED")
					CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, eHelpToShow) )
					CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
					eHelpTextState = HTS_FINISHED
				ENDIF
				CPRINTLN(DEBUG_MISSION, "help is not being displayed stay in this state = HTS_WAIT_DISPLAY = ", GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
			ENDIF
		BREAK

		CASE HTS_DISPLAYING
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
				CPRINTLN(DEBUG_MISSION, "help is not being displayed")
				CPRINTLN(DEBUG_MISSION, "fHelpDisplayedFor = = ", fHelpDisplayedFor)
				IF fHelpDisplayedFor >= 7.5
				OR IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
					CPRINTLN(DEBUG_MISSION, "help is not being displayed change state = HTS_FINISHED")
					CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, eHelpToShow) )
					CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
					eHelpTextState = HTS_FINISHED
				ELSE
					IF bOffMission
						CPRINTLN(DEBUG_MISSION, "help is not being displayed change state = HTS_WAITING_HELP")
						eHelpTextState = HTS_WAITING_HELP
					ELSE
						CPRINTLN(DEBUG_MISSION, "help is not being displayed change state = HTS_DISPLAY")
						eHelpTextState = HTS_DISPLAY
						fHelpDisplayedFor = 0.0
					ENDIF
				ENDIF
			ELSE
				fHelpDisplayedFor += GET_FRAME_TIME()
				CPRINTLN(DEBUG_MISSION, "Help is being displayed stay in this state = HTS_DISPLAYING")
			ENDIF
		BREAK

		CASE HTS_FINISHED
			CPRINTLN(DEBUG_MISSION, "help has been displayed reset help manger no more help required for now = HTS_WAITING_HELP")
			fHelpDisplayedFor = 0.0
			eHelpTextState = HTS_WAITING_HELP
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Monitors for help bits being set.
///    Then displays they via a state machine
///    the normal checks only run when the mission state is off mission 
///    when text is loaded and there isnt anything else happening that would get in the way
///    If we have had to go in to sleep mode then the monitor displayes the 
///    find a more discreet location for the vehicle help then resets to wait to come back off mission
PROC MONITER_HELP_TEXT()
	IF (g_OnMissionState = MISSION_TYPE_OFF_MISSION
	AND eMissionState != MS_WAIT_ON_MISSION
	AND IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
	AND NOT IS_MISSION_LEADIN_ACTIVE()
	AND g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST)
	AND NOT IS_RESULT_SCREEN_DISPLAYING()
	AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR) //if we arent on the prep e "mission"
		MONITOR_GETAWAY_REMINDER()
		MONITER_PLAYER_WANTED()
		SWITCH 	eHelpTextState
			CASE HTS_WAITING_HELP
				INT iHelpIncrement
				IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_FIRST_HELP_BIT))
					iHelpIncrement = -1
					WHILE iHelpIncrement < (ENUM_TO_INT(HTE_LOC_WATER)-1)
						iHelpIncrement++
						IF iHelpIncrement != ENUM_TO_INT(NUM_HELP)
							CHECK_HELP_BITS(iHelpTextBit[0], iHelpIncrement)
						ENDIF
					ENDWHILE
					CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_FIRST_HELP_BIT))
				ELSE
					iHelpIncrement = 31
					WHILE iHelpIncrement < (ENUM_TO_INT(NUM_HELP)-1)
						iHelpIncrement++
						IF iHelpIncrement != ENUM_TO_INT(NUM_HELP)
							CHECK_HELP_BITS(iHelpTextBit[1], iHelpIncrement)
						ENDIF
					ENDWHILE
					SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_FIRST_HELP_BIT))
				ENDIF
			BREAK

			CASE HTS_DISPLAY
			FALLTHRU
			CASE HTS_WAIT_DISPLAY
			FALLTHRU
			CASE HTS_DISPLAYING
			FALLTHRU
			CASE HTS_FINISHED
				DO_HELP_DISPLAYING_STATES(INT_TO_ENUM(HELP_TEXT_ENUMS, iHelpToDisplay), TRUE)
			BREAK
//				//if there isnt a help message already on screen display the help message
//				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//					PRINT_HELP(GET_HELP_LABLE_FROM_BIT(iHelpToDisplay), DEFAULT_HELP_TEXT_TIME)
//					eHelpTextState = HTS_WAIT_DISPLAY
//				ENDIF
//			BREAK
//			
//			CASE HTS_WAIT_DISPLAY
//				//when the message is being displayed move state to wait for it to finish
//				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(iHelpToDisplay))
//					eHelpTextState = HTS_DISPLAYING
//				ENDIF
//			BREAK
//
//			CASE HTS_DISPLAYING
//				//the help message has finished displaying for whatever reason
//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(iHelpToDisplay))
//					eHelpTextState = HTS_FINISHED
//				ENDIF
//			BREAK
//
//			CASE HTS_FINISHED
//				//message has fininshed displaying clear the help bit and reset the state machine
//				CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, iHelpToDisplay) )
//				iHelpToDisplay = -1
//				eHelpTextState = HTS_WAITING_HELP
//			BREAK
		ENDSWITCH
	ELSE
		IF g_OnMissionState != MISSION_TYPE_OFF_MISSION
		OR g_OnMissionState != MISSION_TYPE_STORY
		OR g_OnMissionState != MISSION_TYPE_STORY_PREP
		OR g_OnMissionState != MISSION_TYPE_RANDOM_CHAR
		OR g_OnMissionState != MISSION_TYPE_SWITCH
		OR IS_MISSION_LEADIN_ACTIVE()
		OR g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
		OR g_bTaxiProceduralRunning
	//		CPRINTLN(DEBUG_MISSION, "Try and display On mission help - This will loop")
			HELP_TEXT_ENUMS eHelpToShow = HTE_ON_MISSION
			IF IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
				eHelpToShow = HTE_ON_FRIEND_ACT
			ELIF g_bTaxiProceduralRunning
				eHelpToShow = HTE_ON_TAXI_MISSION
			ENDIF
			IF IS_BIT_SET(iHelpTextBit[GET_HELP_BIT_ID(ENUM_TO_INT(eHelpToShow))], ENUM_TO_INT(eHelpToShow))
				DO_HELP_DISPLAYING_STATES(eHelpToShow, FALSE)
//				CPRINTLN(DEBUG_MISSION, "On mission help bit set")
//				SWITCH 	eHelpTextState
//					CASE HTS_DISPLAY
//						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//							CPRINTLN(DEBUG_MISSION, "There is not any help being displayed while off mission display On Mission help = HTS_DISPLAY")
//							PRINT_HELP(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)), DEFAULT_HELP_TEXT_TIME)
//							eHelpTextState = HTS_WAIT_DISPLAY
//						ELSE
//							CPRINTLN(DEBUG_MISSION, "Some help is being displayed while off mission dont display On Mission help = HTS_DISPLAY")
//						ENDIF
//					BREAK
//
//					CASE HTS_WAIT_DISPLAY
//						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
//							CPRINTLN(DEBUG_MISSION, "On Mission help is being displayed change state = HTS_DISPLAYING")
//							eHelpTextState = HTS_DISPLAYING
//						ELSE
//							CPRINTLN(DEBUG_MISSION, "On Mission help is not being displayed stay in this state = HTS_WAIT_DISPLAY")
//						ENDIF
//					BREAK
//
//					CASE HTS_DISPLAYING
//						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(eHelpToShow)))
//							CPRINTLN(DEBUG_MISSION, "On Mission help is not being displayed change state = HTS_FINISHED")
//							eHelpTextState = HTS_FINISHED
//						ELSE
//							CPRINTLN(DEBUG_MISSION, "On Mission help is being displayed stay in this state = HTS_DISPLAYING")
//						ENDIF
//					BREAK
//
//					CASE HTS_FINISHED
//						CPRINTLN(DEBUG_MISSION, "On Mission help has been displayed reset help manger no more help required for now = HTS_FINISHED")
//						CLEAR_HELP_TEXT_BIT(INT_TO_ENUM(HELP_TEXT_ENUMS, eHelpToShow) )
//						eHelpTextState = HTS_WAITING_HELP
//					BREAK
//				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///******************************************************************************************************
///    					RESTRICTED AREA STUFF			******				RESTRICTED AREA STUFF
///******************************************************************************************************

/// PURPOSE:
///    Fill out a new restricted area struct
/// PARAMS:
///    zone - The name of the zone this area is in
///    pos1 - one of the positions that defines the area
///    pos2 - another postion that defines the area
///    width - the width of the area
/// RETURNS:
///    A new restricted area
FUNC RESTRICTED_AREA FILL_RESTRICTED_AREA(STRING zone, VECTOR pos1, VECTOR pos2, FLOAT width)
	RESTRICTED_AREA temp
	
	temp.sZone = zone
	temp.vPos1 = pos1
	temp.vPos2 = pos2
	temp.fWidth = width
	RETURN temp
ENDFUNC

/// PURPOSE:
///    populates the restricted area data
PROC POPULATE_RESTRICTED_AREAS()
	sRestrictedZoneList[0] = "ARMYB"
	sRestrictedZoneList[1] = "AIRP"
	sRestrictedZoneList[2] = "STAD"
	sRestrictedZoneList[3] = "TERMINA"
	sRestrictedZoneList[4] = "MOVIE"
	sRestrictedZoneList[5] = "JAIL"
	sRestrictedZoneList[6] = "OCEANA"
	sRestrictedZoneList[7] = "GOLF"
	sRestrictedZoneList[8] = "HORS"
	sRestrictedZoneList[9] = "MTCHIL"
	sRestrictedZoneList[10] = "MTGORDO"
	sRestrictedZoneList[11] = "SANCHIA"
	sRestrictedZoneList[12] = "TATAMO"


	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[0] = FILL_RESTRICTED_AREA("DELBE", <<-1615.25745, -952.59436, 20.01716>>, <<-2160.70581, -423.00000, -1.28679>>, 327.8) //Beach 1 
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[1] = FILL_RESTRICTED_AREA("DELBE", <<-1521.71631, -914.56757, 20.17247>>, <<-1855.71484, -1325.78406, -44.79295>>, 254.80) //peir
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[2] = FILL_RESTRICTED_AREA("BEACH", <<-1162.36987, -1815.00842, 15.33822>>, <<-1553.20715, -1098.68494, 0.46467>>, 253.91)//beach 2
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[3] = FILL_RESTRICTED_AREA("BEACH", <<-1464.21851, -1136.78284, 0.32167>>, <<-2241.56641, -358.88486, 20.32481>>, 282.21)//beach 2
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[4] = FILL_RESTRICTED_AREA("PBOX", <<156.41087, -1042.64124, 22.31273>>, <<238.63815, -821.12170, 35.10069>>, 176.96) //that square next to the FIB building where a barry mission is
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[5] = FILL_RESTRICTED_AREA("CHIL", <<883.11212, 534.72833, 115.72503>>, <<559.74249, 644.60199, 150.59714>>, 301.08) //Lester 1B stadium Opera
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[6] = FILL_RESTRICTED_AREA("EAST_V", <<941.45306, -329.42560, 60.77003>>, <<727.76514, -200.95193, 75.59085>>, 88.89) //Broker park
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[7] = FILL_RESTRICTED_AREA("EAST_V", <<689.11768, -273.14056, 60.21559>>, <<834.73260, -352.16550, 50.92442>>, 81.23) //Broker park
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[8] = FILL_RESTRICTED_AREA("MIRR", <<1048.49854, -357.03320, 60.92149>>, <<1401.58081, -783.89752, 75.7477>>, 325.93) //Mirror park
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[9] = FILL_RESTRICTED_AREA("MIRR", <<892.28357, -461.57523, 70.86029>>, <<1161.30066, -829.82990, 45.90131>>, 184.12) //Mirror park
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[10] = FILL_RESTRICTED_AREA("VCANA", <<-1161.36060, -1143.71631, -5.71593>>, <<-864.97137, -981.12573, 21.09691>>, 328.65) //Canals
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[11] = FILL_RESTRICTED_AREA("BAYTRE", <<251.40108, 1068.34717, 280.66629>>, <<189.60123, 1272.35217, 143.80351>>, 160.32) //Concert place
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[13] = FILL_RESTRICTED_AREA("OBSERV", <<-450.98346, 1048.40845, 252.94498>>, <<-389.56403, 1244.50391, 370.24692>>, 199.93) //Observatory
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[14] = FILL_RESTRICTED_AREA("AIRP", <<0,0,0>>, <<0,0,0>>, 0)//Airport
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[15] = FILL_RESTRICTED_AREA("TERMINA", <<0,0,0>>, <<0,0,0>>, 0) //Terminal
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[16] = FILL_RESTRICTED_AREA("STAD", <<0,0,0>>, <<0,0,0>>, 0) //Fame or shame stadium
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[17] = FILL_RESTRICTED_AREA("MOVIE", <<0,0,0>>, <<0,0,0>>, 0)//Movie lot
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[18] = FILL_RESTRICTED_AREA("GOLF", <<0,0,0>>, <<0,0,0>>, 0)//golf place
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[19] = FILL_RESTRICTED_AREA("HORS", <<0,0,0>>, <<0,0,0>>, 0) //Horse racetrack
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[20] = FILL_RESTRICTED_AREA("PBOX", <<-28.39781, -1071.24487, 50.21438>>, <<-49.83520, -1131.27661, 20.02555>>, 55.12) //Simeon's car dealership
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[21] = FILL_RESTRICTED_AREA("LOSSF", <<843.19617, 25.93548, 65.16061>>, <<1138.68652, 363.05872, 105.41279>>, 61.11) //Los santos freeway - by horse track
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[22] = FILL_RESTRICTED_AREA("ROCKF", <<-992.56384, -199.76730, 30.74956>>, <<-687.99042, -43.23445, 80.93306>>, 80.53) //rockford hils
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[23] = FILL_RESTRICTED_AREA("ROCKF", <<-251.8548, -446.2141, 29.5887>>, <<-362.6850, -434.7425, 90.9310>>, 50.0000) 
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[24] = FILL_RESTRICTED_AREA("ALTA", <<180.2637, -404.9771, 40.1713>>, <<289.9432, -445.1485, 124.3793>>, 100.0000) 
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[25] = FILL_RESTRICTED_AREA("ALTA", <<343.8481, -323.1273, 80.7749>>, <<427.0800, -361.0469, 45.3411>>, 85.0000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[26] = FILL_RESTRICTED_AREA("RANCHO", <<414.5057, -2092.0999, 19.8533>>, <<350.8093, -2158.3950, 12.3916>>, 55.0000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[27] = FILL_RESTRICTED_AREA("RANCHO", <<283.7474, -2103.9177, 12.9242>>, <<391.8871, -1983.2037, 33.0042>>, 100.0000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[28] = FILL_RESTRICTED_AREA("PBOX", <<85.0380, -670.3274, 42.8642>>, <<227.8234, -722.2458, 274.0000>>, 175.0000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[29] = FILL_RESTRICTED_AREA("PBOX", <<-107.2516, -906.3600, 28.2051>>, <<-49.9007, -752.9250, 330.0000>>, 125.0000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[30] = FILL_RESTRICTED_AREA("TEXTI", <<456.8563, -683.8335, 32.2903>>, <<457.5504, -819.4669, 25.9553>>, 14.4000)
	mRestrictedAreaList[RT_PUBLIC_AREA].mRestrictedArea[31] = FILL_RESTRICTED_AREA("ROCKF", <<-699.7205, -227.3646, 67.8180>>, <<-645.1068, -332.5107, 30.9132>>, 127.1000)
	
	mRestrictedAreaList[RT_PUBLIC_AREA].eType = RT_PUBLIC_AREA
	mRestrictedAreaList[RT_PUBLIC_AREA].iNumAreas = 32
	
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[0] = FILL_RESTRICTED_AREA("PBOX", <<-25.50944, -932.38464, 20.41711>>, <<119.94056, -523.43976, 33.07988>>, 363.40) //FIB
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[1] = FILL_RESTRICTED_AREA("DOWNT", <<-25.50944, -932.38464, 20.41711>>, <<119.94056, -523.43976, 33.07988>>, 363.40) //FIB
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[2] = FILL_RESTRICTED_AREA("COSI", <<1426.93445, 1225.11511, 90.76305>>, <<1429.98230, 1006.83069, 120.66425>>, 259.89) //Martins ranch
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[3] = FILL_RESTRICTED_AREA("COSI", <<3503.56030, 3546.40259, 20.18748>>, <<3513.95459, 3875.79517, 72.948060>>, 393.78) //drug fac
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[4] = FILL_RESTRICTED_AREA("SKID", <<403.54044, -864.46936, 20.33799>>, <<396.34406, -1127.32471, 35.49262>>, 325.93) //police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[5] = FILL_RESTRICTED_AREA("STRAW", <<543.86035, -1548.04297, 10.24609>>, <<361.65848, -1236.38416, 60.50910>>, 372.57) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[6] = FILL_RESTRICTED_AREA("RANCHO", <<543.86035, -1548.04297, 20.24609>>, <<361.65848, -1236.38416, 70.50910>>, 400) //Too close to hospital and police precinct - big score prep A station
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[7] = FILL_RESTRICTED_AREA("RANCHO", <<384.81558, -1933.41052, 16.59966>>, <<390.61108, -1932.07544, 30.60899>>, 70.52) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[8] = FILL_RESTRICTED_AREA("RANCHO", <<330.72287, -1889.69373, 20.92559>>, <<423.02939, -1792.35583, 35.78436>>, 102.07) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[9] = FILL_RESTRICTED_AREA("RANCHO", <<245.16766, -1949.12012, 18.20034>>, <<292.74945, -1896.08887, 35.93622>>, 22.10) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[10] = FILL_RESTRICTED_AREA("RANCHO", <<126.69929, -1836.23828, 33.35957>>, <<210.85199, -1903.31995, 18.27101>>, 47.98) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[11] = FILL_RESTRICTED_AREA("DAVIS", <<324.78290, -1925.65112, 30.02256>>, <<216.05585, -2048.78345, 10.22765>>, 41.97) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[12] = FILL_RESTRICTED_AREA("DAVIS", <<-120.61917, -1751.84375, 35.11157>>, <<152.19200, -1981.76868, 10.24828>>, 105.61) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[13] = FILL_RESTRICTED_AREA("PELUFF", <<1178.12878, 24.67330, 91.10038>>, <<938.56110, 176.20265, 69.35879>>, 575.71) //Too close to hospital and police precinct
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[14] = FILL_RESTRICTED_AREA("SANDY", <<1733.21399, 3622.21484, 40.87701>>, <<1938.26807, 3745.34009, 31.32682>>, 239.19) //Police Sandy
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[5] = FILL_RESTRICTED_AREA("JAIL", <<0,0,0>>, <<0,0,0>>, 0)//Prison
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[6] = FILL_RESTRICTED_AREA("ARMYB", <<0,0,0>>, <<0,0,0>>, 0)//Army base
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[17] = FILL_RESTRICTED_AREA("DAVIS", <<253.32857, -1567.82080, 28.22939>>, <<392.00284, -1684.73340, 70.04888>>, 288.30)//Too close to hospital and police precinct - big score prep A station
//	mRestrictedAreaList[RT_LAW_ENFORCEMENT].mRestrictedArea[18] = FILL_RESTRICTED_AREA("RANCHO", <<253.32857, -1567.82080, 28.22939>>, <<392.00284, -1684.73340, 70.04888>>, 288.30)//Too close to hospital and police precinct - big score prep A station

	mRestrictedAreaList[RT_LAW_ENFORCEMENT].eType = RT_LAW_ENFORCEMENT
	mRestrictedAreaList[RT_LAW_ENFORCEMENT].iNumAreas = 7
	
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[0] = FILL_RESTRICTED_AREA("PALETO", <<-422.86185, 6068.39893, 20.34662>>, <<-282.74524, 6206.32422, 50.46586>>, 196.61) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[1] = FILL_RESTRICTED_AREA("ROCKF", <<-755.25061, 147.42661, 75.41048>>, <<-1079.03320, 169.38055, 50.46801>>, 179.12) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[2] = FILL_RESTRICTED_AREA("ROCKF", <<-752.36743, 90.76733, 65.51710>>, <<-938.79803, -15.91457, 35.48347>>, 205.78) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[3] = FILL_RESTRICTED_AREA("ROCKF", <<-890.49988, 431.14490, 90.29848>>, <<-875.35760, 232.32660, 60.20724>>, 263.82) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[4] = FILL_RESTRICTED_AREA("ROCKF", <<-752.36743, 90.76733, 65.51710>>, <<-938.79803, -15.91457, 35.48347>>, 205.78) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[5] = FILL_RESTRICTED_AREA("ROCKF", <<-1198.18262, 638.63666, 115.10664>>, <<-444.50900, 750.93768, 198.29713>>, 473.88) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[6] = FILL_RESTRICTED_AREA("ROCKF", <<-844.40753, 400.94131, 80.43300>>, <<-109.53111, 420.50140, 120.20880>>, 256.03) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[7] = FILL_RESTRICTED_AREA("ROCKF", <<-518.62134, 648.82654, 130.93524>>, <<-64.07090, 582.45044, 215.30841>>, 162.72) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[8] = FILL_RESTRICTED_AREA("RICHM", <<-934.98016, 349.61014, 85.77298>>, <<-1432.45093, 269.98038, 50.73030>>, 185.03) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[9] = FILL_RESTRICTED_AREA("RICHM", <<-1633.81335, -69.53224, 65.10236>>, <<-1446.82288, 69.71544, 48.23926>>, 214.92) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[10] = FILL_RESTRICTED_AREA("RICHM", <<-1732.25806, 444.13525, 130.12581>>, <<-2065.69043, 412.21207, 98.09863>>, 175.17)
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[11] = FILL_RESTRICTED_AREA("RICHM", <<-1618.03625, 50.95197, 70.95364>>, <<-1396.51428, 221.10040, 50.84464>>, 161.61) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[12] = FILL_RESTRICTED_AREA("RICHM", <<-1801.28503, 106.87860, 72.12892>>, <<-1541.74011, 263.37378, 50.44112>>, 155.88) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[13] = FILL_RESTRICTED_AREA("PELUFF", <<-2208.56274, 146.41006, 150.93246>>, <<-2350.12231, 486.60657, 200.59520>>, 299.18) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[14] = FILL_RESTRICTED_AREA("PELUFF", <<-1852.32629, 134.41718, 70.06226>>, <<-1994.38721, 299.62833, 100.96516>>, 182.04) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[15] = FILL_RESTRICTED_AREA("CHIL", <<-1982.66968, 505.96481, 100.93644>>, <<-1918.84265, 713.63818, 150.73953>>, 168.9) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[16] = FILL_RESTRICTED_AREA("CHIL", <<-1455.75061, 887.33508, 191.97572>>, <<-1663.20728, 767.36841, 160.81076>>, 239.65) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[17] = FILL_RESTRICTED_AREA("CHIL", <<-1570.22180, 508.20563, 140.38838>>, <<-808.95325, 526.43335, 90.18556>>, 238.43) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[18] = FILL_RESTRICTED_AREA("CHIL", <<242.62039, 583.59045, 159.40428>>, <<268.04242, 827.44940, 201.69531>>, 105.09) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[19] = FILL_RESTRICTED_AREA("CHIL", <<-21.10285, 706.86481, 150.72626>>, <<-210.43822, 1056.27551, 280.31825>>, 290.15) //Concert place
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[20] = FILL_RESTRICTED_AREA("RGLEN", <<-1837.53833, 774.29840, 120.56287>>, <<-1765.84192, 831.00439, 160.35838>>, 68.09) //
	mRestrictedAreaList[RT_RESIDENTAL_AREA].mRestrictedArea[21] = FILL_RESTRICTED_AREA("DIVINE", <<-372.08490, 372.71826, 100.60433>>, <<390.21979, 532.51672, 180.53801>>, 305.88) //

	mRestrictedAreaList[RT_RESIDENTAL_AREA].eType = RT_RESIDENTAL_AREA
	mRestrictedAreaList[RT_RESIDENTAL_AREA].iNumAreas = 22

	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[0] = FILL_RESTRICTED_AREA("MTCHIL", <<0,0,0>>, <<0,0,0>>, 0) //Mount Chiliad	
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[1] = FILL_RESTRICTED_AREA("MTGORDO", <<0,0,0>>, <<0,0,0>>, 0) //Mount Gordo	
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[2] = FILL_RESTRICTED_AREA("SANCHIA", <<0,0,0>>, <<0,0,0>>, 0) //Mountain Range	
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[3] = FILL_RESTRICTED_AREA("TATAMO", <<0,0,0>>, <<0,0,0>>, 0) //Mountain Range	
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[4] = FILL_RESTRICTED_AREA("ELYSIAN", <<531.2397, -3019.2666, 50.0000>>, <<530.1656, -3393.6226, -22.4165>>, 210.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[5] = FILL_RESTRICTED_AREA("ELYSIAN", <<569.1023, -2913.0181, 15.8910>>, <<420.8226, -2912.7749, -15.0372>>, 25.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[6] = FILL_RESTRICTED_AREA("ELYSIAN", <<495.1012, -2833.1753, 5.1640>>, <<460.1983, -2813.5283, 0.4269>>, 12.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[7] = FILL_RESTRICTED_AREA("ELYSIAN", <<675.2973, -2747.4504, 4.9520>>, <<689.2358, -2747.3955, 10.9001>>, 4.3000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[8] = FILL_RESTRICTED_AREA("CYPRE", <<533.4370, -2693.2793, 17.4952>>, <<588.6345, -2693.4624, 5.3007>>, 15.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[9] = FILL_RESTRICTED_AREA("CYPRE", <<534.9656, -2699.1365, 4.9004>>, <<560.1779, -2662.1921, 9.0007>>, 15.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[10] = FILL_RESTRICTED_AREA("CYPRE", <<583.9030, -2689.2068, 16.9771>>, <<549.8469, -2665.3176, 3.9007>>, 17.3000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[11] = FILL_RESTRICTED_AREA("CYPRE", <<683.4280, -2635.1343, 9.3367>>, <<694.4725, -2679.6597, 4.7815>>, 10.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[12] = FILL_RESTRICTED_AREA("CYPRE", <<695.2171, -2694.7688, 5.9815>>, <<695.5035, -2679.1677, 4.8365>>, 10.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[13] = FILL_RESTRICTED_AREA("CYPRE", <<731.7991, -2659.5796, 4.7713>>, <<732.5307, -2678.4001, 10.5065>>, 25.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[14] = FILL_RESTRICTED_AREA("ELYSIAN", <<86.1885, -2430.6963, -0.1888>>, <<119.1515, -2453.1211, 2.8614>>, 13.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[15] = FILL_RESTRICTED_AREA("ELYSIAN", <<260.3166, -2426.7773, 21.2819>>, <<313.7871, -2433.6558, 6.5609>>, 20.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[16] = FILL_RESTRICTED_AREA("ELYSIAN", <<260.3166, -2426.7773, 21.2819>>, <<313.7871, -2433.6558, 6.5609>>, 20.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[17] = FILL_RESTRICTED_AREA("ELYSIAN", <<283.1514, -2456.7773, 19.4609>>, <<290.2325, -2403.6113, 4.2465>>, 20.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[18] = FILL_RESTRICTED_AREA("ELYSIAN", <<266.0341, -2446.7241, 19.4623>>, <<308.3783, -2414.5442, 4.5423>>, 20.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[19] = FILL_RESTRICTED_AREA("ELYSIAN", <<303.5269, -2451.4456, 19.4091>>, <<270.9798, -2409.4517, 4.4609>>, 20.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[20] = FILL_RESTRICTED_AREA("RANCHO", <<515.0260, -1653.5404, 37.2615>>, <<582.5710, -1577.8248, 26.3365>>, 100.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[21] = FILL_RESTRICTED_AREA("BURTON", <<-150.7403, -419.0541, 28.6163>>, <<-52.8669, -453.5552, 39.4051>>, 100.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[22] = FILL_RESTRICTED_AREA("SanAnd", <<50.2035, -470.7132, 36.9003>>, <<102.0394, -322.0089, 115.0000>>, 130.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[23] = FILL_RESTRICTED_AREA("ALTA", <<499.1769, -241.2495, 47.3462>>, <<393.5317, -205.7358, 79.3132>>, 120.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[24] = FILL_RESTRICTED_AREA("DTVINE", <<422.3247, 62.1180, 113.2905>>, <<478.4940, 43.3220, 83.4541>>, 80.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[25] = FILL_RESTRICTED_AREA("DTVINE", <<385.4908, 56.1423, 159.5800>>, <<332.4580, -87.6805, 63.3657>>, 80.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[26] = FILL_RESTRICTED_AREA("DTVINE", <<213.6192, 90.2228, 98.9357>>, <<203.3067, 61.8088, 86.9197>>, 60.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[27] = FILL_RESTRICTED_AREA("DTVINE", <<192.8130, -14.9451, 85.3158>>, <<149.2689, 0.6803, 67.0343>>, 40.0000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[28] = FILL_RESTRICTED_AREA("WVINE", <<16.1659, 61.8685, 70.8467>>, <<-17.1751, 74.3771, 76.8800>>, 4.5000)
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[29] = FILL_RESTRICTED_AREA("WVINE", <<-86.5425, 84.8701, 80.2147>>, <<-50.4826, 67.7335, 70.2970>>, 20.0000)	
	mRestrictedAreaList[RT_INACCESSIBLE].mRestrictedArea[30] = FILL_RESTRICTED_AREA("ELGORL", <<3449.8391, 5173.9814, 0.0662>>, <<3412.3091, 5166.8896, 14.8342>>, 33.7000)	

	mRestrictedAreaList[RT_INACCESSIBLE].eType = RT_INACCESSIBLE
	mRestrictedAreaList[RT_INACCESSIBLE].iNumAreas = 31
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		sCurrentZone = GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds restricted zones to the restricted zone check.
///    The players zone is checked every 200ms and each zone type has a 200ms delay on iterating through its members
///    If the restricted zone type has an areas that is in the current player zone the areas is added to the 
///    restricted zone type check bit 
///    Check all the areas then sleep the monitor 
PROC MONITOR_RESTRICTED_AREAS()
	SWITCH eRestrictedAreaMonitor
		CASE RAMS_SLEEPING
			//sleep the checking for areas to add to the check list
			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
				IF (GET_GAME_TIMER() - iRestrictedAreaCheckDelay) > 500
					//set current zone to zone player is in now 
					sCurrentZone = GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					#IF IS_DEBUG_BUILD 
						IF bZoneDebug
							CPRINTLN(DEBUG_MISSION, "sCurrentZone =  ", sCurrentZone)
						ENDIF
			 		#ENDIF
					eRestrictedAreaMonitor = RAMS_CHECK_IF_AREAS_IN_RANGE
				ELIF iRestrictedAreaCheckDelay = -1
					iRestrictedAreaCheckDelay = GET_GAME_TIMER()
				ENDIF
			ENDIF
		BREAK
		
		CASE RAMS_CHECK_IF_AREAS_IN_RANGE
		
			IF (GET_GAME_TIMER() - mRestrictedAreaList[iAreaTypeBeingChecked].iTimeDelay) > 500 //wait for the individual restricted zone type to expire so we arent iterating through them all at once
					IF NOT IS_STRING_NULL_OR_EMPTY(sCurrentZone)
					AND NOT IS_STRING_NULL_OR_EMPTY(mRestrictedAreaList[iAreaTypeBeingChecked].mRestrictedArea[iAreaBeingChecked].sZone)
						IF ARE_STRINGS_EQUAL(sCurrentZone, mRestrictedAreaList[iAreaTypeBeingChecked].mRestrictedArea[iAreaBeingChecked].sZone)
							#IF IS_DEBUG_BUILD 
	//							CPRINTLN(DEBUG_MISSION," ADDED AREA = mRestrictedAreaList[", iAreaTypeBeingChecked, "].mRestrictedArea[", p, "]")
							#ENDIF
							//player is in the Zone this area is in add it to the area to check bit
							SET_BIT(mRestrictedAreaList[iAreaTypeBeingChecked].iAreasToCheckBit, iAreaBeingChecked)
						ELSE
							//player isnt in this Areas zone anymore remove it from the check bit
							CLEAR_BIT(mRestrictedAreaList[iAreaTypeBeingChecked].iAreasToCheckBit, iAreaBeingChecked)
						ENDIF
					ELSE
						//if current zone string is null update it 
						sCurrentZone = GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					ENDIF

					iAreaBeingChecked++
				IF iAreaBeingChecked >= (mRestrictedAreaList[iAreaTypeBeingChecked].iNumAreas - 1)
					iAreaBeingChecked = 0
					//after checking all areas in Zone type list reset timer
					mRestrictedAreaList[iAreaTypeBeingChecked].iTimeDelay = GET_GAME_TIMER()
					//increment Zone type list being add
					iAreaTypeBeingChecked++
					//Have we checke dall the areas?
					IF iAreaTypeBeingChecked >= ENUM_TO_INT(RT_NO_RESTRICTION)
						//yes all areas checked reset areas to check 
						iAreaTypeBeingChecked = 0
						//reset sleep delay
						iRestrictedAreaCheckDelay = GET_GAME_TIMER()
						//sleep the state macjone
						eRestrictedAreaMonitor = RAMS_SLEEPING
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    checks to see if the player is in a restricted Zone from the restricted list
/// PARAMS:
///    zoneName - the name of the zone to check against the restricted list
/// RETURNS:
///    False if the player is not in the correct zone
FUNC BOOL IS_PLAYER_IN_RESTRICTED_ZONE(STRING zoneName)
	IF IS_STRING_NULL_OR_EMPTY(zoneName)
		//string is null so cant exist in the list
		RETURN FALSE
	ENDIF

	INT i
	FOR i=0 TO (MAX_RESTRICTED_ZONES-1)
		IF NOT IS_STRING_NULL(sRestrictedZoneList[i])
			//check the list for a matching entry
			IF ARE_STRINGS_EQUAL(sRestrictedZoneList[i], zoneName)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Uses is entity in angeled area to determine if the player in in the coords of an areas
/// PARAMS:
///    Area - the area to use as the basis of the check 
/// RETURNS:
///    true if the player is in the area
FUNC BOOL IS_PLAYER_IN_RESTRICTED_AREA(RESTRICTED_AREA &Area)
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), Area.vPos1, Area.vPos2, Area.fWidth)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the contact list is on screen 
///    But checks if the phone is on screen first
/// RETURNS:
///    If the contact list is on screen
FUNC BOOL SAFE_IS_CONTACT_LIST_ON_SCREEN()
	IF IS_PHONE_ONSCREEN()
	AND IS_CONTACTS_LIST_ON_SCREEN()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
/// 	Checks to see if the player is in any restricted area iterates through all the areas
/// 	if the player is in one of the areas the type of the area is returned via referance
/// PARAMS:
///    type - Varaible the type of area the player is in is written to
/// RETURNS:
///    true if the player is in any restricted area
FUNC BOOL IS_PLAYER_IN_ANY_RESTRICTED_AREA()

	INT i,p
	FOR i=0 TO (ENUM_TO_INT(RT_NO_RESTRICTION)-1)
		FOR p=0 TO (mRestrictedAreaList[i].iNumAreas-1)
			IF IS_BIT_SET(mRestrictedAreaList[i].iAreasToCheckBit, p) //if the bit is set in the area type bit to check then the player is in the same zone as this area
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MISSION, "PLAYER IN ZONE == ", mRestrictedAreaList[i].mRestrictedArea[p].sZone, " CHECKING AREA ", p, " IN ZONE CHECK BIT")
				#ENDIF
				IF IS_PLAYER_IN_RESTRICTED_ZONE(sCurrentZone)
					IF mRestrictedAreaList[i].eType = RT_PUBLIC_AREA
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_PUBLIC_AREA)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_LAW_ENFORCEMENT
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_LAW)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_RESIDENTAL_AREA
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_RESIDENTIAL)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_INACCESSIBLE
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_INACC)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD 
						CPRINTLN(DEBUG_MISSION, "The player is in a restricted area ") 	
					#ENDIF
					
					RETURN TRUE
				ENDIF

				IF IS_PLAYER_IN_RESTRICTED_AREA(mRestrictedAreaList[i].mRestrictedArea[p])
					IF mRestrictedAreaList[i].eType = RT_PUBLIC_AREA
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_PUBLIC_AREA)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_LAW_ENFORCEMENT
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_LAW)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_RESIDENTAL_AREA
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_RESIDENTIAL)
						ENDIF
					ELIF mRestrictedAreaList[i].eType = RT_INACCESSIBLE
						IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
							SET_HELP_TEXT_BIT(HTE_LOC_INACC)
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD 
						CPRINTLN(DEBUG_MISSION, "The player is in a restricted area ") 	
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDFOR
	RETURN FALSE
ENDFUNC




///******************************************************************************************************
///    					PHONE CALL STUFF			******				PHONE CALL STUFF
///******************************************************************************************************

/// PURPOSE:
///    Gets the zone conversation lable from the taxi dialogue
/// PARAMS:
///    ePlayer - which player the player is that the time to 
///    set the conversation lable
PROC GET_ZONE_CONV_LABLE(enumCharacterList ePlayer)
	IF NOT IS_STRING_NULL_OR_EMPTY(mPlayerPhoneCallSettings.sZoneName)
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "SanAnd") //San Andreas
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M77"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F78"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T78"
			ENDIF
			EXIT
		ENDIF
		
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Alamo")//Alamo Sea
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M101"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F1"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T1"
			ENDIF
			EXIT
		ENDIF
		
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Alta") // Alta
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M1"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F2"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T2"
			ENDIF
			EXIT
		ENDIF
		
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Airp") //Los Santos International Airport
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M48"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F49"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T49"
			ENDIF
			EXIT
		ENDIF
		
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ArmyB") // Fort Zancudo
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M28"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F29"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T29"
			ENDIF
			EXIT
		ENDIF			
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "BhamCa")//Banham Canyon
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M2"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F3"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T3"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Banning")//Banning
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M3"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F4"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T4"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Baytre")//Baytree Canyon
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M4"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F5"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T5"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Beach")//Vespucci Beach
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M93"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F94"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T94"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "BradT")//Braddock Tunnel
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M7"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F8"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T8"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "BradP")//Braddock Pass
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M6"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F7"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T7"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Burton")//Burton
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M8"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F9"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T9"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CANNY")//Raton Canyon
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M70"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F71"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T71"
			ENDIF
			EXIT
		ENDIF
			
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CCreak")//Cassidy Creek
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M10"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F11"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T11"
			ENDIF
			EXIT
		ENDIF
			
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CalafB")//Calafia Bridge
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M9"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F10"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T10"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ChamH")//Chamberlain Hills
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M11"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F12"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T12"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CHU")//Chumash
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M13"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F14"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T14"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CHIL")//Vinewood Hills
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M96"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F97"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T97"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "COSI")//Countryside
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M14"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F15"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T15"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "CMSW")//Chiliad Mountain State Wilderness
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M12"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F13"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T13"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Cypre")//Cypress Flats
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M15"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F16"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T16"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Davis")//Davis
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M16"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F17"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T17"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Desrt")//Grand Senora Desert
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M32"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F33"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T33"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "DelBe")//Del Perro Beach
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M19"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F20"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T20"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "DelPe")//Del Perro
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M18"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F19"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T19"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "DelSol")//La Puerta
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M41"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F42"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T42"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Downt")//Downtown
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M20"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F21"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T21"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "DTVine")//Downtown Vinewood
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M21"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F22"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T22"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Eclips")//Eclipse
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M24"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F25"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T25"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ELSant")//East Los Santos
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M22"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F23"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T23"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "EBuro")//El Burro Heights
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M25"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F26"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T26"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ELGorl")//El Gordo Lighthouse
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M26"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F27"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T27"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Elysian")//Elysian Island
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M27"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F28"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T28"
			ENDIF
			EXIT
		ENDIF
			
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Galli")//Galileo Park
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M31"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F32"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T32"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Galfish")//Galilee
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M29"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F30"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T30"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Greatc")//Great Chaparral
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M34"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F35"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T35"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Golf")//GWC and Golfing Society
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M35"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F36"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T36"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "GrapeS")//Grapeseed
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M33"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F34"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T34"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Hawick")//Hawick
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M37"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F38"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T38"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Harmo")//Harmony
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M36"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F37"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T37"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Heart")//Heart Attacks Beach
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M38"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F39"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T39"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "HumLab")//Humane Labs and Research
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M39"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F40"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T40"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "HORS")//Vinewood Racetrack
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M97"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F98"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T98"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Koreat")//Little Seoul
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M46"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F47"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T47"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Jail")//Bolingbroke Penitentiary
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M5"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F6"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T6"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LAct")//Land Act Reservoir
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M45"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F46"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T46"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LDam")//Land Act Dam
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M44"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F45"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T45"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Lago")//Lago Zancudo
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M43"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F44"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T44"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LegSqu")//Legion Square
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LosSF")//Los Santos Freeway
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M47"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F48"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T48"
			ENDIF
			EXIT
		ENDIF
			
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LMesa")//La Mesa
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M40"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F41"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T41"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LosPuer")//La Puerta 
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M41"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F42"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T42"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LosPFy")//La Puerta Fwy
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M42"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F43"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T43"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "LOSTMC")//Lost MC
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Mirr")//Mirror Park
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M50"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F51"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T51"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Morn")//Morningwood
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M52"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F53"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T53"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Murri")//Murrieta Heights
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M56"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F57"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T57"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "MTChil")//Mount Chiliad
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M53"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F54"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T54"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "MTJose")//Mount Josiah
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M55"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F56"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T56"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "MTGordo")//Mount Gordo
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M54"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F55"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T55"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Movie")//Richards Majestic
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M72"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F73"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T73"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "NCHU")//North Chumash
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M57"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F58"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T58"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Noose")//N.O.O.S.E - Tataviam Mountains B*2022344
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M84"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F85"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T85"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Oceana")//Pacific Ocean
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M60"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F61"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T61"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Observ")//Galileo Observatory
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M30"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F31"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T31"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Palmpow")//Palmer-Taylor Power Station
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M64"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F65"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T65"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "PBOX")//Pillbox Hill
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M66"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F67"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T67"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "PBluff")//Pacific Bluffs
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M59"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F60"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T60"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Paleto")//Paleto Bay
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M61"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F62"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T62"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "PalCov")//Paleto Cove
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M62"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F63"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T63"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "PalFor")//Paleto Forest
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M63"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F64"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T64"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "PalHigh")//Palomino Highlands
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M65"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F66"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T66"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ProcoB")//Procopio Beach
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M68"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F69"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T69"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Prol")//North Yankton
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M58"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F59"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T59"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "RTRAK")//Redwood Lights Track
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M71"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F72"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T72"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Rancho")//Rancho
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M69"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F70"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T70"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "RGLEN")//Richman Glen
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M74"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F75"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T75"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Richm")//Richman
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M73"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F74"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T74"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Rockf")//Rockford Hills
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M75"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F76"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T76"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "SANDY")//Sandy Shores
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M79"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F80"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T80"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "TongvaH")//Tongva Hills
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M87"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F88"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T88"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "TongvaV")//Tongva Valley
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M88"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F89"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T89"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "East_V")//East Vinewood
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M23"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F24"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T24"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Zenora")//Senora Freeway
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M80"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F81"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T81"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Slab")//Stab City
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M81"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F82"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T82"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "SKID")//Mission Row
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M51"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F52"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T52"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "SLSant")//South Los Santos
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M82"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F83"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T83"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Stad")//Maze Bank Arena
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M49"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F50"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T50"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Tatamo")//Tataviam Mountains
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M84"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F85"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T85"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Termina")//Terminal
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M85"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F86"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T86"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "TEXTI")//Textile City
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M86"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F87"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T87"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "WVine")//West Vinewood
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M99"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F100"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T100"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "UtopiaG")//Utopia Gardens
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M89"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F90"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T90"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Vesp")//Vespucci
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M92"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F93"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T93"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "VCana")//Vespucci Canals
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M94"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F95"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T95"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Vine")//Vinewood
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M95"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F96"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T96"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "WMirror")//West Mirror Drive
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M98"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F99"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T99"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "WindF")//Ron Alternates Wind Farm
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M76"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F77"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T77"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "Zancudo")//Zancudo River
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M100"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F101"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T101"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "SanChia")//San Chianski Mountain Range
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M78"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F79"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T79"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "STRAW")//Strawberry
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M83"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F84"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T84"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "zQ_UAR")//Davis Quartz
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M17"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F18"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T18"
			ENDIF
			EXIT
		ENDIF
		IF ARE_STRINGS_EQUAL(mPlayerPhoneCallSettings.sZoneName, "ZP_ORT")//Port of South Los Santos
			IF ePlayer = CHAR_MICHAEL
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M67"
			ELIF ePlayer = CHAR_FRANKLIN
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F68"
			ELSE
				mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T68"
			ENDIF
			EXIT
		ENDIF

		IF ePlayer = CHAR_MICHAEL
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
		ELIF ePlayer = CHAR_FRANKLIN
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
		ELSE
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
		ENDIF
	ELSE
		//Use moved lines
		IF ePlayer = CHAR_MICHAEL
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_M"
		ELIF ePlayer = CHAR_FRANKLIN
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_F"
		ELSE
			mPlayerPhoneCallSettings.sZoneConvLable = "LOC_T"
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    uses the accessor function to set the correct lable to update the phone conversation
/// PARAMS:
///    ePlayer - the player char the player is 
PROC GET_ZONE_NAME_AND_LABLE(enumCharacterList ePlayer)
	mPlayerPhoneCallSettings.sZoneName = GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	#IF IS_DEBUG_BUILD SK_PRINT("ZONE NAME = ===== = = = = = = = = ") #ENDIF
	#IF IS_DEBUG_BUILD SK_PRINT(mPlayerPhoneCallSettings.sZoneName) #ENDIF
	GET_ZONE_CONV_LABLE(ePlayer)
ENDPROC

/// PURPOSE:
///    Used to setup the phone call for the end fone call 
///    assignes voices and indices also sets zone update lable
///    sets the char conferance contact 
/// PARAMS:
///    ePlayer - the player char the player is 
///    sConvo - the conversation to do 
PROC SETUP_PHONE_CALL_STRUCT(enumCharacterList ePlayer, STRING sConvo)
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 13") #ENDIF
	SWITCH ePlayer
		CASE CHAR_MICHAEL
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 14") #ENDIF
			mPlayerPhoneCallSettings.iPlayerVoice = 0
			mPlayerPhoneCallSettings.sPlayerVoice = "MICHAEL"

			IF eGetawayID = FIB4_GETAWAY
				mPlayerPhoneCallSettings.iOtherVoice = 1
				mPlayerPhoneCallSettings.sOtherVoice = "FRANKLIN"
				mPlayerPhoneCallSettings.eContact = CHAR_FRANK_TREV_CONF
			ELSE
				mPlayerPhoneCallSettings.iOtherVoice = 3
				mPlayerPhoneCallSettings.sOtherVoice = "LESTER"
				IF eGetawayID = AGENCY_GETAWAY
					mPlayerPhoneCallSettings.eContact = CHAR_LEST_FRANK_CONF
				ELSE
					mPlayerPhoneCallSettings.eContact = CHAR_LESTER
				ENDIF
			ENDIF
			
			mPlayerPhoneCallSettings.sResponse = "FBI_3_FRESP"//can potentially remove this from teh struct
			GET_ZONE_NAME_AND_LABLE(ePlayer)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 15") #ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 16") #ENDIF
			mPlayerPhoneCallSettings.iPlayerVoice = 1
			mPlayerPhoneCallSettings.sPlayerVoice = "FRANKLIN"

			IF eGetawayID = FIB4_GETAWAY		
				mPlayerPhoneCallSettings.iOtherVoice = 0
				mPlayerPhoneCallSettings.sOtherVoice = "MICHAEL"
				mPlayerPhoneCallSettings.eContact = CHAR_MIKE_TREV_CONF
			ELSE
				mPlayerPhoneCallSettings.iOtherVoice = 3
				mPlayerPhoneCallSettings.sOtherVoice = "LESTER"
				IF eGetawayID = AGENCY_GETAWAY
					mPlayerPhoneCallSettings.eContact = CHAR_LEST_MIKE_CONF
				ELSE
					mPlayerPhoneCallSettings.eContact = CHAR_LESTER
				ENDIF
			ENDIF

			mPlayerPhoneCallSettings.sResponse = "FBI_3_MRESP"
			GET_ZONE_NAME_AND_LABLE(ePlayer)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 17") #ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 18") #ENDIF
			mPlayerPhoneCallSettings.iPlayerVoice = 2
			mPlayerPhoneCallSettings.sPlayerVoice = "TREVOR"

			IF eGetawayID = FIB4_GETAWAY
				mPlayerPhoneCallSettings.iOtherVoice = 0
				mPlayerPhoneCallSettings.sOtherVoice = "MICHAEL"
				mPlayerPhoneCallSettings.eContact = CHAR_MIKE_FRANK_CONF
			ELSE
				mPlayerPhoneCallSettings.iOtherVoice = 3
				mPlayerPhoneCallSettings.sOtherVoice = "LESTER"
				IF eGetawayID = AGENCY_GETAWAY
					mPlayerPhoneCallSettings.eContact = CHAR_LEST_MIKE_CONF
				ELSE
					mPlayerPhoneCallSettings.eContact = CHAR_LESTER
				ENDIF
			ENDIF

			mPlayerPhoneCallSettings.sResponse = "FBI_3_MRESP"
			GET_ZONE_NAME_AND_LABLE(ePlayer)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 19") #ENDIF
		BREAK
	ENDSWITCH

	IF eGetawayID = AGENCY_GETAWAY
		enumPhoneBookPresence ePhonebook
		IF ePlayer = CHAR_MICHAEL
			ePhonebook = MICHAEL_BOOK
		ELSE
			ePhonebook = FRANKLIN_BOOK
		ENDIF
		
		ADD_CONTACT_TO_PHONEBOOK(mPlayerPhoneCallSettings.eContact, ePhonebook, FALSE)
	ENDIF
	
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 20") #ENDIF
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, mPlayerPhoneCallSettings.iPlayerVoice, PLAYER_PED_ID(), mPlayerPhoneCallSettings.sPlayerVoice)
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, mPlayerPhoneCallSettings.iOtherVoice, NULL, mPlayerPhoneCallSettings.sOtherVoice)
	mPlayerPhoneCallSettings.sConv = sConvo
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 21") #ENDIF
	
ENDPROC

/// PURPOSE:
///    triggers a conference call for the placing the vehicle
/// RETURNS:
///    true when the conference call goes through 
FUNC BOOL DO_CONF_CALL()

	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 25") #ENDIF
	IF PLAYER_CALL_CHAR_CELLPHONE(s_conversation_peds, mPlayerPhoneCallSettings.eContact, sTextblockConvs, mPlayerPhoneCallSettings.sConv, CONV_PRIORITY_VERY_HIGH)
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 26") #ENDIF
		ePhoneCallState = PCS_CONV_STARTED
		TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_FBI_OFFICERS4_P3, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 27") #ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Turns the secondary option for the contacts on and off
/// PARAMS:
///    bOn - If true the secondary contact option is set 
///    if false the secondary contact option is removed
PROC CONTACT_WATCH(BOOL bOn)
	IF bOn
		STRING sLable = "PRC_MARK"
		//Player can only ring lester
		IF eGetawayID = FIN_GETAWAY
			sLable = "PRC_MARKVEH"
			SET_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
		//All three player chars involved
		ELIF eGetawayID = FIB4_GETAWAY
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				SET_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
				SET_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			ELSE
				SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			ENDIF
		//can ring lester
		ELIF eGetawayID = AGENCY_GETAWAY
			//If mike - can ring frank and lester
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				SET_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
				SET_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			//If frank - can ring Mike and lester
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
				SET_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			ELSE
				//If Trev - Can ring Mike and lester
				SET_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
				SET_LESTER_SECONDARY_CONTACT_LIST_FUNCTION_AVAILABLE(sLable)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD SK_PRINT("CONTACT_WATCH - ON") #ENDIF
		SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_ALT_CONTACT_SET))
	ELSE
		IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_ALT_CONTACT_SET))
			REMOVE_MICHAEL_SECONDARY_CONTACT_LIST_FUNCTION()
			REMOVE_FRANKLIN_SECONDARY_CONTACT_LIST_FUNCTION()
			REMOVE_TREVOR_SECONDARY_CONTACT_LIST_FUNCTION()
			REMOVE_LESTER_SECONDARY_CONTACT_LIST_FUNCTION()
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_ALT_CONTACT_SET))
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_MARK_PHONE_CALL(enumCharacterList ePlayer)
	STRING sConv
	IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
		IF eGetawayID = FIB4_GETAWAY
			sTextblockConvs = "FBIPRAU"
		ELIF eGetawayID = AGENCY_GETAWAY
			sTextblockConvs = "AHFAUD"
		ELIF eGetawayID = FIN_GETAWAY
			sTextblockConvs = "FHFAUD"
		ENDIF

		IF eGetawayID = FIB4_GETAWAY
			IF ePlayer = CHAR_MICHAEL
				sConv =  "FBI_3_MDRPC"
			ELIF ePlayer = CHAR_FRANKLIN
				sConv =   "FBI_3_FDRPC"
			ELIF ePlayer = CHAR_TREVOR
				sConv =   "FBI_3_TDRPC"
			ENDIF
		
		ELIF eGetawayID = AGENCY_GETAWAY
			IF ePlayer = CHAR_MICHAEL
				sConv =  "AH_MDRPC"
			ELIF ePlayer = CHAR_FRANKLIN
				sConv =   "AH_FDRPC"
			ELIF ePlayer = CHAR_TREVOR
				sConv =   "AH_TDRPC"
			ENDIF
		
		ELIF eGetawayID = FIN_GETAWAY
			IF ePlayer = CHAR_MICHAEL
				sConv =  "FHP_PICKCM"
			ELIF ePlayer = CHAR_FRANKLIN
				sConv =   "FHP_PICKCF"
			ELIF ePlayer = CHAR_TREVOR
				sConv =   "FHP_PICKCT"
			ENDIF
		ENDIF
	ELSE
		sTextblockConvs = "FHFAUD"
		IF ePlayer = CHAR_MICHAEL
			sConv =  "FHP_MOVEM"
		ELIF ePlayer = CHAR_FRANKLIN
			sConv =   "FHP_MOVEF"
		ELIF ePlayer = CHAR_TREVOR
			sConv =   "FHP_MOVET"
		ENDIF
	ENDIF

	RETURN sConv
ENDFUNC

/// PURPOSE:
///    Sets up the phone call with the correct inital root
///    the rest of the phone call stuff is setup from this function as well
PROC POP_PLACED_CONV()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	SETUP_PHONE_CALL_STRUCT(ePlayer, GET_MARK_PHONE_CALL(ePlayer))
ENDPROC

/// PURPOSE:
///    All phone call set up functions are called from here 
///    and the conference call is triggered
///    !!!!!! could make this function faster 
///    !!!!!! it calls the conversation setup functions every frame that the call isnt
///    !!!!!! setup. this probably quite slow
/// PARAMS:
///    eNextState - the state variable to write back to 
/// RETURNS:
///    TRUE when the player conference call has gone through 
FUNC BOOL PICK_PHONE_CALL_AND_NEXT_STATE(MISSION_STATE &eNextState)
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
		POP_PLACED_CONV()
		IF DO_CONF_CALL()
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 28") #ENDIF

			STRING scriptName = GET_THIS_SCRIPT_NAME()
			IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)
				IF eGetawayID = FIN_GETAWAY
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "The script is = ", scriptName) #ENDIF
					eNextState = MS_DROP_OFF_AGENCY_CAR
				ELSE
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "The script is = ", scriptName) #ENDIF
					eNextState = MS_DISPLAYING_SPLASH
				ENDIF
			ENDIF

			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


///******************************************************************************************************
///    					HIDING POS CHECKS			******				HIDING POS CHECKS
///******************************************************************************************************

FUNC VECTOR GET_RC_POSITION(g_eRC_MissionIDs RC)
	VECTOR vReturn
	
	//Abigail Strand
	IF RC = RC_ABIGAIL_1
		vReturn = << -1604.668, 5239.10, 3.01 >>
	ELIF RC = RC_ABIGAIL_2
		vReturn = << -1592.84, 5214.04, 3.01 >>
		
	//Barry Strand
	ELIF RC = RC_BARRY_1
		vReturn = << 190.26, -956.35, 29.63 >>
	ELIF RC = RC_BARRY_2
		vReturn = << 190.26, -956.35, 29.63 >>
	ELIF RC = RC_BARRY_3
		vReturn = << 414.00, -761.00, 29.00 >>
	ELIF RC = RC_BARRY_3A
		vReturn = << 1199.27, -1255.63, 34.23 >>
	ELIF RC = RC_BARRY_3C
		vReturn = << -468.90, -1713.06, 18.21 >>
	ELIF RC = RC_BARRY_4
		vReturn = << 237.65, -385.41, 44.40 >>

	//Dreyfuss strand
	ELIF RC = RC_DREYFUSS_1
		vReturn = << -1458.97, 485.99, 115.38 >>

	//Epsilonism strand
	ELIF RC = RC_EPSILON_1
		vReturn = << -1622.89, 4204.87, 83.30 >>
	ELIF RC = RC_EPSILON_2
		vReturn = << 242.70, 362.70, 104.74 >>
	ELIF RC = RC_EPSILON_3
		vReturn = << 1835.53, 4705.86, 38.1 >>
	ELIF RC = RC_EPSILON_4
		vReturn = << 1826.13, 4698.88, 38.92 >>
	ELIF RC = RC_EPSILON_5
		vReturn = << 637.02, 119.7093, 89.50 >>
	ELIF RC = RC_EPSILON_6
		vReturn = <<-2892.93, 3192.37, 11.66>>
	ELIF RC = RC_EPSILON_7
		vReturn = << 524.43, 3079.82, 39.48 >>
	ELIF RC = RC_EPSILON_8
		vReturn = << -697.75, 45.38, 43.03 >>

	//Extreme strand
	ELIF RC = RC_EXTREME_1
		vReturn = << -188.22, 1296.10, 302.86 >>
	ELIF RC = RC_EXTREME_2
		vReturn = << -954.19, -2760.05, 14.64 >>
	ELIF RC = RC_EXTREME_3
		vReturn = << -63.8, -809.5, 321.8 >>
	ELIF RC = RC_EXTREME_4
		vReturn = << 1731.41, 96.96, 170.39 >>

	//Fanatic strand
	ELIF RC = RC_FANATIC_1
		vReturn = << -1877.82, -440.649, 45.05>>
	ELIF RC = RC_FANATIC_2
		vReturn = << 809.66, 1279.76, 360.49 >>
	ELIF RC = RC_FANATIC_3
		vReturn = << -915.6, 6139.2, 5.5 >>

	//HAO
	ELIF RC = RC_HAO_1
		vReturn = <<-72.29, -1260.63, 28.14>>


	//Hunting
	ELIF RC = RC_HUNTING_1
		vReturn = << 1804.32, 3931.33, 32.82 >>
	ELIF RC = RC_HUNTING_2
		vReturn = <<-684.17, 5839.16, 16.09 >>

	//Josh strand
	ELIF RC = RC_JOSH_1
		vReturn = << -1104.93, 291.25, 64.30 >>
	ELIF RC = RC_JOSH_2
		vReturn = << 565.39, -1772.88, 29.77 >>
	ELIF RC = RC_JOSH_3
		vReturn = << 565.39, -1772.88, 29.77 >>
	ELIF RC = RC_JOSH_4
		vReturn = << -1104.93, 291.25, 64.30 >>

	//Maude
	ELIF RC = RC_MAUDE_1
		vReturn = << 2726.1, 4145, 44.3 >>

	//Minute Man Blues Strand
	ELIF RC = RC_MINUTE_1
		vReturn = << 327.85, 3405.70, 35.73 >>
	ELIF RC = RC_MINUTE_2
		vReturn = << 18.00, 4527.00, 105.00 >>
	ELIF RC = RC_MINUTE_3
		vReturn = <<-303.82,6211.29,31.05>>

	//Mrs. Philips strand
	ELIF RC = RC_MRS_PHILIPS_1
		vReturn = <<1972.59, 3816.43, 32.42>>
	ELIF RC = RC_MRS_PHILIPS_2
		vReturn = <<0, 0, 0>>
	
	//Nigel strand
	ELIF RC = RC_NIGEL_1
		vReturn = << -1097.16, 790.01, 164.52 >>
	ELIF RC = RC_NIGEL_1A
		vReturn = << -558.65, 284.49, 90.86 >>
	ELIF RC = RC_NIGEL_1B
		vReturn = << -1034.15, 366.08, 80.11 >>
	ELIF RC = RC_NIGEL_1C
		vReturn = << -623.91, -266.17, 37.76 >>
	ELIF RC = RC_NIGEL_1D
		vReturn = << -1096.85, 67.68, 52.95 >>
	ELIF RC = RC_NIGEL_2
		vReturn = << -1310.70, -640.22, 26.54 >>
	ELIF RC = RC_NIGEL_3
		vReturn = << -44.75, -1288.67, 28.21 >>

	//Omega strand
	ELIF RC = RC_OMEGA_1
		vReturn = << 2468.51, 3437.39, 49.90 >>
	ELIF RC = RC_OMEGA_2
		vReturn = << 2319.44, 2583.58, 46.76 >>

	//Pap strand
	ELIF RC = RC_PAPARAZZO_1
		vReturn = << -149.75, 285.81, 93.67 >>
	ELIF RC = RC_PAPARAZZO_2
		vReturn = << -70.71, 301.43, 106.79>>
	ELIF RC = RC_PAPARAZZO_3
		vReturn = << -257.22, 292.85, 90.63 >>
	ELIF RC = RC_PAPARAZZO_3A
		vReturn = << 305.52, 157.19, 102.94>>
	ELIF RC = RC_PAPARAZZO_3B
		vReturn = << 1040.96, -534.42, 60.17 >>
	ELIF RC = RC_PAPARAZZO_4
		vReturn = << -484.20, 229.68, 82.21 >>

	//Rampages
	ELIF RC = RC_RAMPAGE_1
		vReturn = << 908.00, 3643.70, 32.20 >>
	ELIF RC = RC_RAMPAGE_3
		vReturn = << 465.10, -1849.30, 27.80 >>
	ELIF RC = RC_RAMPAGE_4
		vReturn = << -161.00, -1669.70, 33.00 >>
	ELIF RC = RC_RAMPAGE_5
		vReturn = << -1298.20, 2504.14, 21.09 >>
	ELIF RC = RC_RAMPAGE_2
		vReturn = << 1181.50, -400.10, 67.50 >>

	//The Squatch
	ELIF RC = RC_THELASTONE
		vReturn = << -1298.98, 4640.16, 105.67>>

	//Tonya strand
	ELIF RC = RC_TONYA_1
	OR RC = RC_TONYA_2
	OR RC = RC_TONYA_5
		vReturn = <<-14.39, -1472.69, 29.58>>
	ELIF RC = RC_TONYA_3
		vReturn = <<0, 0, 0>>
	ELIF RC = RC_TONYA_4
		vReturn = <<0, 0, 0>>
	ENDIF
	
	RETURN vReturn
ENDFUNC


FUNC BOOL IS_POSITION_NEAR_RC_TRIGGER(VECTOR vPos, g_eRC_MissionIDs RCtoCheck)
	FLOAT fDist = VDIST2(vPos, GET_RC_POSITION(RCtoCheck))
	IF fDist <= (25*25)
		CPRINTLN(DEBUG_MISSION, "RC NEAR BY = ", GET_RC_MISSION_DISPLAY_STRING_FROM_ID(RCtoCheck))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_RC_MISSIONS_NEAR(VECTOR vPos)
	//Need to check the unlock thing for all the RC's 
	
	//Check strands first 
	//Abigail strand
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_ABIGAIL_2)
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_ABIGAIL_1)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_ABIGAIL_1)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_ABIGAIL_2)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Barry strand
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_BARRY_4)
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_BARRY_3)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_BARRY_3)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_BARRY_3A)
				RETURN TRUE
			ENDIF

			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_BARRY_3C)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_BARRY_1)
		RETURN TRUE
	ENDIF
	IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_BARRY_2)
		RETURN TRUE
	ENDIF
	
	//Drefuss
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_DREYFUSS_1)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_DREYFUSS_1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Epsilon 
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_8)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_8)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_7)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_7)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_6)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_6)
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_5)
					IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_5)
						RETURN TRUE
					ENDIF
					IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_4)
						IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_4)
							RETURN TRUE
						ENDIF
						IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_3)
							IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_3)
								RETURN TRUE
							ENDIF
							IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_2)
								IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_2)
									RETURN TRUE
								ENDIF
								IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EPSILON_1)
									IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EPSILON_1)
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Extreme
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EXTREME_4)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EXTREME_4)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EXTREME_3)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EXTREME_3)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EXTREME_2)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EXTREME_2)
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_EXTREME_1)
					IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_EXTREME_1)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Fanatic - mike
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_FANATIC_1)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_FANATIC_1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Fanatic - Trev
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_FANATIC_2)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_FANATIC_2)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Fanatic - Frank
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_FANATIC_3)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_FANATIC_3)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//HAO - frank
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_HAO_1)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_HAO_1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	//Hunting - trev
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_HUNTING_2)
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_HUNTING_1)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_HUNTING_1)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_HUNTING_2)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Josh
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_JOSH_4)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_JOSH_4)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_JOSH_3)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_JOSH_3)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_JOSH_2)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_JOSH_2)
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_JOSH_1)
					IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_JOSH_1)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Maude
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MAUDE_1)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MAUDE_1)
			RETURN TRUE
		ENDIF
	ENDIF

	//minute man 
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MINUTE_3)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MINUTE_3)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MINUTE_2)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MINUTE_2)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MINUTE_1)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MINUTE_1)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Mrs Philips
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MRS_PHILIPS_1)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MRS_PHILIPS_1)
			RETURN TRUE
		ENDIF
	ENDIF
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_MRS_PHILIPS_2)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_MRS_PHILIPS_2)
			RETURN TRUE
		ENDIF
	ENDIF

	//Nigel
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_3)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_3)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_2)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
				RETURN TRUE
			ENDIF

			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_1A)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_1B)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_1C)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_1D)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_NIGEL_1)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_NIGEL_2)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Omega
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_OMEGA_2)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_OMEGA_2)
			RETURN TRUE
		ENDIF
		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_OMEGA_1)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_OMEGA_1)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Paparazzo
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_4)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_4)
			RETURN TRUE
		ENDIF

		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_3)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_3)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_3A)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_3A)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_3B)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_3B)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_2)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_2)
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_PAPARAZZO_1)
					IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_PAPARAZZO_1)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Rampage - has a messed up order
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_2)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_RAMPAGE_2)
			RETURN TRUE
		ENDIF

		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_5)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_RAMPAGE_5)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_4)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_RAMPAGE_4)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_3)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_RAMPAGE_3)
					RETURN TRUE
				ENDIF
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_1)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_RAMPAGE_1)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_THELASTONE)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_THELASTONE)
			RETURN TRUE
		ENDIF
	ENDIF


	//Tonya
	IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_TONYA_5)
		IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_TONYA_5)
			RETURN TRUE
		ENDIF

		IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_TONYA_4)
			IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_TONYA_4)
				RETURN TRUE
			ENDIF
			IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_TONYA_3)
				IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_TONYA_3)
					RETURN TRUE
				ENDIF
				IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_TONYA_2)
					IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_TONYA_2)
						RETURN TRUE
					ENDIF
					IF NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_TONYA_1)
						IF IS_POSITION_NEAR_RC_TRIGGER(vPos, RC_TONYA_1)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determins which of the safe house help to display based on which safe house is passed in 
/// PARAMS:
///    eSafeHouse - the safe house enum to use in the checks
#if USE_CLF_DLC
PROC PICK_SAFE_HOUSE_WARNINGCLF(SAVEHOUSE_NAME_ENUM eSafeHouse)
	IF eSafeHouse = SAVEHOUSEclf_MICHAEL_BH
	OR eSafeHouse = SAVEHOUSEclf_MICHAEL_CS
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_M)//This location is too close to a safe house.
		CPRINTLN(DEBUG_MISSION, " Its Michael house" ) 
	// Franklins
	ELIF eSafeHouse = SAVEHOUSEclf_FRANKLIN_SC
	OR eSafeHouse = SAVEHOUSEclf_FRANKLIN_VH
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_F)//This location is too close to a safe house.
		CPRINTLN(DEBUG_MISSION,  " Its Franklin house" ) 
	// Trevors
	ELIF eSafeHouse = SAVEHOUSEclf_TREVOR_CS
	OR eSafeHouse = SAVEHOUSEclf_TREVOR_VB
	OR eSafeHouse = SAVEHOUSEclf_TREVOR_SC
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_T)//This location is too close to a safe house.		
		CPRINTLN(DEBUG_MISSION, " Its Trevors house" ) 
	ENDIF
ENDPROC
#endif
#if USE_NRM_DLC
PROC PICK_SAFE_HOUSE_WARNINGNRM(SAVEHOUSE_NAME_ENUM eSafeHouse)
	IF eSafeHouse = SAVEHOUSENRM_BH
	OR eSafeHouse = SAVEHOUSENRM_CHATEAU
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_M)//This location is too close to a safe house.
		CPRINTLN(DEBUG_MISSION, " Its Michael house" ) 	
	ENDIF
ENDPROC
#endif
PROC PICK_SAFE_HOUSE_WARNING(SAVEHOUSE_NAME_ENUM eSafeHouse)

#if USE_CLF_DLC
	PICK_SAFE_HOUSE_WARNINGCLF(eSafeHouse)
	exit
#endif
#if USE_NRM_DLC
	PICK_SAFE_HOUSE_WARNINGNRM(eSafeHouse)
	exit
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	IF eSafeHouse = SAVEHOUSE_MICHAEL_BH
	OR eSafeHouse = SAVEHOUSE_MICHAEL_CS
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_M)//This location is too close to a safe house.
		CPRINTLN(DEBUG_MISSION, " Its Michael house" ) 
	// Franklins
	ELIF eSafeHouse = SAVEHOUSE_FRANKLIN_SC
	OR eSafeHouse = SAVEHOUSE_FRANKLIN_VH
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_F)//This location is too close to a safe house.
		CPRINTLN(DEBUG_MISSION,  " Its Franklin house" ) 
	// Trevors
	ELIF eSafeHouse = SAVEHOUSE_TREVOR_CS
	OR eSafeHouse = SAVEHOUSE_TREVOR_VB
	OR eSafeHouse = SAVEHOUSE_TREVOR_SC
		SET_HELP_TEXT_BIT(HTE_LOC_SAFEHOUSE_T)//This location is too close to a safe house.		
		CPRINTLN(DEBUG_MISSION, " Its Trevors house" ) 
	ENDIF
#endif
#endif	
	
	
ENDPROC

/// PURPOSE:
///    Performs a check to see if the player is trying to park a vehicle within 200m of 
///    a save house
/// RETURNS:
///    TRUE if the player is within range
FUNC BOOL IS_SAFE_HOUSE_TOO_CLOSE()
	SAVEHOUSE_NAME_ENUM eSafeHouse = GET_CLOSEST_SAVEHOUSE(GET_ENTITY_COORDS(PLAYER_PED_ID()), NO_CHARACTER, TRUE)
	FLOAT dist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_sSavehouses[eSafeHouse].vSpawnCoords)
	IF ( dist <= 100*100)	
		CPRINTLN(DEBUG_MISSION, "Is safe house too close() returned TRUE  the distance to the safe house is == ", dist) 
		IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
			PICK_SAFE_HOUSE_WARNING(eSafeHouse)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if two entities are at similar heights
/// PARAMS:
///    e1 - the coords of the first entity passed in
///    e2 - the coords of the second entity passed in
/// RETURNS:
///    TRUE is the entities heights are within the defined tolerance
FUNC BOOL ARE_COORDS_AT_SAME_HEIGHT(VECTOR v1, VECTOR v2)
	FLOAT fDiff = ABSF(v1.z - v2.z)
	#IF IS_DEBUG_BUILD 
		CPRINTLN(DEBUG_MISSION, "The difference in height is:  ", fDiff) 	
	#ENDIF

	IF fDiff <= 5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HEAD_SPACE_CLEAR(VECTOR vPos)
	FLOAT Zcoord, fFloorZ
	
	VECTOR vPosAbove = vPos + <<0,0, 3.2>>//garbage truck height
	IF GET_GROUND_Z_FOR_3D_COORD(vPosAbove, Zcoord)
	AND GET_GROUND_Z_FOR_3D_COORD(vPos, fFloorZ)
		IF ABSF(Zcoord - fFloorZ) < 0.9
			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "The difference in height is:  ", ABSF( Zcoord - fFloorZ)) 	
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD 
		CPRINTLN(DEBUG_MISSION, "! The difference in height is:  ", ABSF(Zcoord - fFloorZ)) 	
	#ENDIF

	IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
		SET_HELP_TEXT_BIT(HTE_LOC_OBSTRUCTED)
	ENDIF
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Checks that the player isnt one of the nearby peds.
/// RETURNS:
///    
FUNC BOOL ARE_THERE_ANY_NEAR_PEDS()
	PED_INDEX peds[5]
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), peds)
	INT i
	FOR i=0 TO 4
		IF IS_PED_UNINJURED(peds[i])
		AND peds[i] != PLAYER_PED_ID()
			IF VDIST2(vPotentialHidingPos, GET_ENTITY_COORDS(peds[i])) <= 4*4
				#IF IS_DEBUG_BUILD SK_PRINT_INT("VDIST2(vPotentialHidingPos, GET_ENTITY_COORDS(peds[i])) <= 4*4 ped at = ", i) #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD SK_PRINT("ARE_THERE_ANY_NEAR_PEDS(): FALSE ") #ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Runs checks to see if the getaway vehicle is obstructed by a vehicle or by a ped
/// PARAMS:
///    veh - the vehicle to check an area around
/// RETURNS:
///    TRUE if this area is blocked
FUNC BOOL IS_AREA_CLEAR_AROUND_LOCATION_VEHICLE(VEHICLE_INDEX veh )
	INT iAreaClearCounter = 0
	IF veh != NULL
		IF IS_VEHICLE_OK(veh)
			VECTOR vMin, vMax
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vMin, vMax)
			
			vMin += <<-2,-1,-1>>
			vMax += <<2,1,1>>
			
			#IF IS_DEBUG_BUILD			
				IF bShowDebugText
				AND g_bTriggerDebugOn
					DRAW_DEBUG_CROSS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vMin), 0,0,255, 255)
					DRAW_DEBUG_CROSS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vMax), 0,0,255, 255)
				ENDIF
			#ENDIF
			
			//Is area clear of vehicles
			IF NOT IS_AREA_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vMin), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vMax), FALSE, TRUE, FALSE, FALSE, FALSE, veh) //need to do this properly
				iAreaClearCounter++
				CDEBUG3LN(DEBUG_MISSION, "IS_AREA_CLEAR_AROUND_LOCATION_VEHICLE - Is area clear of vehicles -  iAreaClearCounter++ ", iAreaClearCounter)
				
			ELSE
				IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
					SET_HELP_TEXT_BIT(HTE_LOC_OBSTRUCTED_VEH)//The getaway vehicle can not be placed here, the location is obstructed by a vehicle.
				ENDIF
			ENDIF
			
			//Is area clear of peds
			IF NOT ARE_THERE_ANY_NEAR_PEDS()
				iAreaClearCounter++
				CDEBUG3LN(DEBUG_MISSION, "IS_AREA_CLEAR_AROUND_LOCATION_VEHICLE - Is area clear of peds -  iAreaClearCounter++ ", iAreaClearCounter)
			ELSE
				
				IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
					SET_HELP_TEXT_BIT(HTE_LOC_PUBLIC)//The getaway vehicle needs to be placed out of sight of witnesses.
				ENDIF
			ENDIF
			
			//if both checks are true then we cant use this location
			IF iAreaClearCounter = 2
				IF IS_HEAD_SPACE_CLEAR(GET_ENTITY_COORDS(veh))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF


	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the vehicle is upright, in water, on a slant or both of the entry points are blocked
/// PARAMS:
///    veh - the vehicle to use when performing the checks
/// RETURNS:
///    TRUE if the vehicle is in a sutable position
FUNC BOOL IS_VEHICLE_UPRIGHT_AND_LEVEL(VEHICLE_INDEX veh)
	IF IS_ENTITY_UPSIDEDOWN(veh)
		SET_HELP_TEXT_BIT(HTE_VEH_UPSIDEDOWN)//The vehicle is upsidedown.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_STUCK_ON_ROOF ====== FALSE")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD SK_PRINT("IS_VEHICLE_UPRIGHT_AND_LEVEL - START") #ENDIF
	IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), veh, VS_DRIVER) //find a better way
	AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(), veh, VS_FRONT_RIGHT) //find a better way
		SET_HELP_TEXT_BIT(HTE_LOC_OBSTRUCTED)//This location is obstructed.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "IS_ENTRY_POINT_FOR_SEAT_CLEAR ====== FALSE")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_IN_WATER(veh)
		SET_HELP_TEXT_BIT(HTE_LOC_WATER)//This location is in water.
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "IS_ENTITY_IN_WATER() ====== FALSE")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	VECTOR vCarRot = GET_ENTITY_ROTATION(veh)
	IF vCarRot.x >= 10
	OR vCarRot.x <= -10
		SET_HELP_TEXT_BIT(HTE_LOC_STEEP)//This location is too steep..
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== FALSE")
		#ENDIF
		RETURN FALSE
	ELIF vCarRot.y >= 15
	OR vCarRot.y <= -15
		SET_HELP_TEXT_BIT(HTE_LOC_UNEVEN)//This location is too uneven..
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== FALSE")
		#ENDIF
		RETURN FALSE
	ELSE
		#IF IS_DEBUG_BUILD 
			CPRINTLN(DEBUG_MISSION, "ON FLAT GROUND ====== TRUE ==== ", vCarRot)
		#ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD SK_PRINT("IS_VEHICLE_UPRIGHT_AND_LEVEL - END") #ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_FOUR_DOOR_VEHICLE(VEHICLE_INDEX Veh)
	INT i = GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(Veh)
	IF  i >= 3
		#IF IS_DEBUG_BUILD SK_PRINT_INT("enough seats = ", i) #ENDIF
		RETURN TRUE
	ENDIF

	SET_HELP_TEXT_BIT(HTE_UNSUITABLE_VEH_SEATS)//seats
	#IF IS_DEBUG_BUILD SK_PRINT_INT("NOT enough seats = ", i) #ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_NEAR_ON_ROAD(VECTOR vPos)
	VECTOR vRoadNode
	IF GET_CLOSEST_VEHICLE_NODE(vPos, vRoadNode, NF_NONE)
		#IF IS_DEBUG_BUILD
			IF bShowDebugText
			AND g_bTriggerDebugOn
				DRAW_DEBUG_SPHERE(vRoadNode, 4, 255)
			ENDIF
		#ENDIF

		FLOAT fDist = VDIST2(vPos, vRoadNode)
		IF ( fDist >= 20*20)
		OR NOT ARE_COORDS_AT_SAME_HEIGHT(vPos, vRoadNode)
			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "( VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRoadNode) >= 20*20) ======== TRUE ", VDIST2(vPos, vRoadNode)) 	
			#ENDIF
			RETURN FALSE
		ELSE
			IF ( fDist < 20)
			AND ( fDist > 6)
				#IF IS_DEBUG_BUILD 
					CPRINTLN(DEBUG_MISSION, "(VDIST2(vPos, vRoadNode) >= 10*10 ) ======== FALSE ", VDIST2(vPos, vRoadNode)) 	
				#ENDIF
				
				IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
					SET_HELP_TEXT_BIT(HTE_VEH_NEAR_ROAD)//This location is to close to a road
				ENDIF
				RETURN TRUE
			ELSE
				IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
					SET_HELP_TEXT_BIT(HTE_VEH_ON_ROAD)//This location is to on a road
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " ==== There isnt a road node near return TRUE") #ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TOO_CLOSE_TO_HOSPITAL(VECTOR vPos)
	FLOAT fDist = VDIST2(vPos, g_sHospitals[GET_CLOSEST_HOSPITAL(vPos)].vSpawnCoords)
	IF ( fDist <= 200*200)
		CPRINTLN(DEBUG_MISSION, "The player near a hospital ", GET_THIS_SCRIPT_NAME()) 	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TOO_CLOSE_TO_PRECINCT(VECTOR vPos)
	FLOAT fDist = VDIST2(vPos, g_sPoliceStations[GET_CLOSEST_POLICE_STATION(vPos)].vSpawnCoords)
	IF ( fDist <= 200*200)
		CPRINTLN(DEBUG_MISSION, "The player near a police station ", GET_THIS_SCRIPT_NAME()) 	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL IS_TOO_CLOSE_TO_HOSPITAL_OR_PRECINCT(VECTOR vPos)
	IF IS_TOO_CLOSE_TO_HOSPITAL(vPos)
	OR IS_TOO_CLOSE_TO_PRECINCT(vPos)
		IF SAFE_IS_CONTACT_LIST_ON_SCREEN()
			SET_HELP_TEXT_BIT(HTE_LOC_LAW)
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_NEAR_GARAGE_RESPAWN(VECTOR vPos)
	IF vPos.z < -90.0
		CPRINTLN(DEBUG_MISSION, "The player IN a player garage vec ", GET_THIS_SCRIPT_NAME()) 	
		RETURN TRUE
	ENDIF

	IF ( VDIST2(vPos, <<-89.3770, 92.6583, 71.2349>>) <= 20*20)
	OR ( VDIST2(vPos, <<-62.0307, -1839.8585, 25.6787>>) <= 20*20)
	OR ( VDIST2(vPos, <<-234.7648, -1150.3105, 21.9224>>) <= 20*20)
		CPRINTLN(DEBUG_MISSION, "The player near a player garage respawn vec ", GET_THIS_SCRIPT_NAME()) 	
		SET_HELP_TEXT_BIT(HTE_LOC_PUBLIC_AREA)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the vehicle is near any road nodes, near a safe house, in a restricted zone 
/// PARAMS:
///    vPos - the position to use in the checks 
/// RETURNS:
///    TRUE if the position is in a suitable location
FUNC BOOL IS_AREA_QUIET(VECTOR vPos)

	IF NOT IS_SAFE_HOUSE_TOO_CLOSE()
	AND NOT IS_NEAR_GARAGE_RESPAWN(vPos)
	//			IF NOT IS_PLAYER_TO_CLOSE_TO_FBI_LOCS() //not generic
			IF NOT IS_PLAYER_IN_ANY_RESTRICTED_AREA()
				IF NOT ARE_ANY_RC_MISSIONS_NEAR(vPos)
					IF NOT IS_VEHICLE_NEAR_ON_ROAD(vPos)
					AND NOT IS_TOO_CLOSE_TO_HOSPITAL_OR_PRECINCT(vPos)
						RETURN TRUE
					ENDIF
				ELSE
					SET_HELP_TEXT_BIT(HTE_ON_MISSION)
				ENDIF
			ENDIF
		//ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

///******************************************************************************************************
///    
///    
///    					HIDING POS CHECKS			******				HIDING POS CHECKS
///   
///    
///******************************************************************************************************

///******************************************************************************************************
///    					CAR CHECKS			******				CAR CHECKS
///******************************************************************************************************


/// PURPOSE:
///    Checks to see if the vehicle is a car 
///    Cant use a bike, plane, heli, horse, boat or police vehicle
/// RETURNS:
///    TRUE if the vehicle is none of the above
FUNC BOOL IS_VEHICLE_ROAD_VEHICLE()
	IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
	OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
	OR IS_PED_ON_MOUNT(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
	OR IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
		SET_HELP_TEXT_BIT(HTE_UNSUITABLE_VEH)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to make sure the vehicle's model is not on the invalid list
/// PARAMS:
///    Name - The name of the model to check
/// RETURNS:
///    TRUE if the model being checked is on the invalid list
FUNC BOOL DOES_CURRENT_MODEL_EQUAL_INVALID(MODEL_NAMES Name)
	INT i
	FOR i=0 TO (MAX_INVALIDMODS-1)
		IF mnInvalidModels[i] = Name

			IF i = 0
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_1)
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IN_FIB_VEHICLE_BEFORE_COMPLETE))
			ELIF i = 1
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_2)
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IN_FIB_VEHICLE_BEFORE_COMPLETE))
			ELSE
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IN_FIB_VEHICLE_BEFORE_COMPLETE))
			ENDIF

			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the vehicles stats are with in a certain level
///    And performs the DOES_CURRENT_MODEL_EQUAL_INVALID 
///    Acceleration needs to be over 0.165
///    Max est speed needs to be above 31
/// RETURNS:
///    TRUE if all the checks are passed ie model acceleration and top speed
FUNC BOOL IS_VEHICLE_MODEL_SUITABLE(VEHICLE_INDEX Veh)	
	MODEL_NAMES mnTemp = GET_ENTITY_MODEL(Veh)

	IF NOT DOES_CURRENT_MODEL_EQUAL_INVALID(mnTemp)
		IF GET_VEHICLE_MODEL_ACCELERATION(mnTemp) > 0.165
		AND GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp) > 31
			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ACCELERATION  == ", GET_VEHICLE_MODEL_ACCELERATION(mnTemp)) 
				CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED  == ", GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp)) 
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD 
		CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ACCELERATION  == ", GET_VEHICLE_MODEL_ACCELERATION(mnTemp)) 
		CPRINTLN(DEBUG_MISSION, "GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED  == ", GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(mnTemp)) 
	#ENDIF
	SET_HELP_TEXT_BIT(HTE_UNSUITABLE_VEH)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is in any of the preset vehicle setups for a character
///    eg: Michaels tailgater etc.
/// RETURNS:
///    TRUE if the player is in a vehicle that matches the setup details of
///    one of the player cars
FUNC BOOL IS_PLAYER_IN_ANY_PLAYER_VEHICLE(VEHICLE_INDEX Veh)
	enumCharacterList playercar = GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(Veh)


	IF playercar = CHAR_MICHAEL
		SET_HELP_TEXT_BIT(HTE_VEH_OWNED_MIKE)
		#IF IS_DEBUG_BUILD SK_PRINT("Vehicle is owned by Mike") #ENDIF
		RETURN TRUE
	ENDIF
	
	IF playercar = CHAR_FRANKLIN
		SET_HELP_TEXT_BIT(HTE_VEH_OWNED_FRANK)
		#IF IS_DEBUG_BUILD SK_PRINT("Vehicle is owned by Frank") #ENDIF
		RETURN TRUE
	ENDIF
	
	IF playercar = CHAR_TREVOR
		SET_HELP_TEXT_BIT(HTE_VEH_OWNED_TREV)
		#IF IS_DEBUG_BUILD SK_PRINT("Vehicle is owned by Trev") #ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_IN_TEMP_NPC_VEHICLE_ID_LIST(Veh)
		SET_HELP_TEXT_BIT(HTE_UNSUITABLE_VEH)
		#IF IS_DEBUG_BUILD SK_PRINT("Vehicle is owned by an NPC") #ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is a passenger in a taxi
/// RETURNS:
///    TRUE if the player is a passenger in a taxi
FUNC BOOL IS_PLAYER_PASSENGER_IN_TAXI()
	IF IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
		IF GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT) = PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT) = PLAYER_PED_ID()
		OR GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT) = PLAYER_PED_ID()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GETAWAY_VEHICLE_HEALTHY(VEHICLE_INDEX veh)
	IF GET_ENTITY_HEALTH(veh) < 300
	OR GET_VEHICLE_ENGINE_HEALTH(veh) < 300
		SET_HELP_TEXT_BIT(HTE_HEALTH_VEH)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Performs checks to determine if the vehicle the player is in could
///    be used in the getaway
/// RETURNS:
///    TRUE if the vehicle the player is in can be used in the getaway
FUNC BOOL DO_VALID_GETAWAY_CAR_CHECK(VEHICLE_INDEX Veh)
	IF IS_VEHICLE_MODEL_SUITABLE(Veh)
	AND NOT IS_PLAYER_IN_ANY_PLAYER_VEHICLE(Veh)
	AND IS_GETAWAY_VEHICLE_HEALTHY(Veh)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is using a cargo bob to transport the vehicle 
/// RETURNS:
///    TRUE if the player is using a cargo bob to transport the vehicle 
///    or if the decor exists and is set to true
FUNC BOOL MONITOR_PLAYER_CARGO_BOBBING_CAR()
	VEHICLE_INDEX Veh
	IF IS_PLAYER_FLYING_WITH_ANY_ATTACHED_VEHICLE(Veh)
		IF NOT DECOR_EXIST_ON(Veh, "Getaway_Winched")
			DECOR_SET_BOOL(Veh, "Getaway_Winched", TRUE)
			SET_HELP_TEXT_BIT(HTE_CARGOBOBBED_VEH)
			RETURN TRUE
		ELSE
			IF DECOR_GET_BOOL(Veh, "Getaway_Winched")
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_VEHICLE_BEEN_CHECKED_BEFORE(VEHICLE_INDEX veh)
	IF DECOR_EXIST_ON(veh, "GetawayVehicleValid")
		#IF IS_DEBUG_BUILD SK_PRINT("decor - Exists on vehicle its already been checked once") #ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PREVIOUSLY_CHECKED_VEHICLE_VALID(VEHICLE_INDEX veh)
	IF DECOR_EXIST_ON(veh, "GetawayVehicleValid")
		IF DECOR_GET_BOOL(veh, "GetawayVehicleValid")
			#IF IS_DEBUG_BUILD SK_PRINT("decor - Exists on vehicle and is true so vehicle is valid") #ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears the car valid flag, the vehicle check flag and teh return to vehicle flag
PROC CLEAR_CAR_RELATED_BITS()
	CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
	CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
	CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
ENDPROC

/// PURPOSE:
///    Gets the root of the conversation to play after teh phone call after dropping off the vehicle
/// RETURNS:
///    returns a string for teh root of the conversation
FUNC STRING GET_EXITED_VEHICLE_ROOT()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	
	SWITCH ePlayer
	
	CASE CHAR_MICHAEL
		IF eGetawayID = FIB4_GETAWAY
			RETURN "FBI_3_MEXIT"
		ELIF eGetawayID = AGENCY_GETAWAY
			RETURN "AH_MEXIT"
		ELIF eGetawayID = FIN_GETAWAY
			RETURN "FHP_EXTCARM"
		ENDIF
	BREAK
	
	CASE CHAR_FRANKLIN
		IF eGetawayID = FIB4_GETAWAY
			RETURN "FBI_3_FEXIT"
		ELIF eGetawayID = AGENCY_GETAWAY
			RETURN "AH_FEXIT"
		ELIF eGetawayID = FIN_GETAWAY
			RETURN "FHP_EXTCARF"
		ENDIF
	BREAK

	CASE CHAR_TREVOR
		IF eGetawayID = FIB4_GETAWAY
			RETURN "FBI_3_TEXIT"
		ELIF eGetawayID = AGENCY_GETAWAY
			RETURN "AH_TEXIT"
		ELIF eGetawayID = FIN_GETAWAY
			RETURN "FHP_EXTCART"
		ENDIF
	BREAK
	
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Checks to see if the story missions that unlock the FIB 4 mission
/// RETURNS:
///    TRUE if the required story missions are complete
FUNC BOOL ARE_OTHER_STORY_MISSIONS_COMPLETE()

	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FRANKLIN_1)
	AND GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_1)
	AND GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Used to get the correct update conversation for if FIB 4 is unlocked or the player will have to 
///    complete more missions
/// PARAMS:
///    bMissionAvailable - Is FIB 4 Available
/// RETURNS:
///    the string to use for the update conversation
FUNC TEXT_LABEL_15 GET_UPDATE_STRING_FOR_PREP_FIB(BOOL bMissionAvailable)
	TEXT_LABEL_15 sString
	IF bMissionAvailable
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		
			CASE CHAR_FRANKLIN
				sString = "FBI4_ISAGO"
			BREAK
			
			CASE CHAR_TREVOR
				sString = "FBI4_ISAGO"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()

			CASE CHAR_FRANKLIN
				sString = "FBI4_THATSIT"
			BREAK
			
			CASE CHAR_TREVOR
				sString = "FBI4_THATSIT"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sString
ENDFUNC

/// PURPOSE:
///    Used to get the correct update conversation for if FIB 4 is unlocked or the player will have to 
///    complete more missions
/// PARAMS:
///    bMissionAvailable - Is FIB 4 Available
/// RETURNS:
///    the string to use for the update conversation
FUNC TEXT_LABEL_15 GET_UPDATE_STRING_FOR_PREP_AGENCY(BOOL bMissionAvailable)
	TEXT_LABEL_15 sString
	IF bMissionAvailable
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()

			CASE CHAR_FRANKLIN
				sString = "AHF_C8"
			BREAK
			
			CASE CHAR_MICHAEL
				sString = "AHF_C5"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()

			CASE CHAR_FRANKLIN
				sString = "AHF_C9"
			BREAK
			
			CASE CHAR_MICHAEL
				sString = "AHF_C6"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sString
ENDFUNC

/// PURPOSE:
///    Used to update conversation if the player is Franklin or Trevor
/// PARAMS:
///    sConv - the current conversation need to monitor that the conversation is at the right line to be updated
///    bMissionAvailable - Is FIB 4 Available
PROC UPDATE_PHONE_CONV_F_T(TEXT_LABEL_23 sConv, BOOL bMissionAvailable)
	IF NOT IS_STRING_NULL_OR_EMPTY(sConv)
		IF ARE_STRINGS_EQUAL(sConv, mPlayerPhoneCallSettings.sZoneConvLable)
			IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 0
				TEXT_LABEL_15 sUpdateString 
				
				IF eGetawayID = FIB4_GETAWAY
					sUpdateString = GET_UPDATE_STRING_FOR_PREP_FIB(bMissionAvailable)
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_UNLOCKED_FROM_PREP, TRUE)
					SET_DYNAMIC_BRANCH_FOR_ONGOING_CALL(sTextblockConvs, sUpdateString)
					PLAY_DYNAMIC_BRANCH()
				ELIF eGetawayID = AGENCY_GETAWAY
					sUpdateString = GET_UPDATE_STRING_FOR_PREP_AGENCY(bMissionAvailable)
					SET_DYNAMIC_BRANCH_FOR_ONGOING_CALL(sTextblockConvs, sUpdateString)
					PLAY_DYNAMIC_BRANCH()
				ENDIF
				
				ePhoneCallState = PCS_WAITING_LAST_LINE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If the player is michael he rings steve and this function registers the call to flow
/// PARAMS:
///    bMissionAvailable - Is FIB 4 Available
PROC REGISTER_PASS_CALL_LAST_PREP_M(BOOL bMissionAvailable)
	IF bMissionAvailable
		REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIB4_PREPS_DONE, CT_END_OF_MISSION, GET_CURRENT_PLAYER_PED_ENUM(), CHAR_STEVE, 3, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME)
	ELSE
		IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_1)
			REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIB4_PREPS_DONE_WAIT, CT_END_OF_MISSION, GET_CURRENT_PLAYER_PED_ENUM(), CHAR_STEVE, 3, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME,VID_BLANK,CID_TEXT_ASS1_REMINDER)		
		ELSE
			REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(CALL_FIB4_PREPS_DONE_WAIT, CT_END_OF_MISSION, GET_CURRENT_PLAYER_PED_ENUM(), CHAR_STEVE, 3, CC_END_OF_MISSION_QUEUE_TIME, CC_END_OF_MISSION_QUEUE_TIME,VID_BLANK)		
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_MISSION_AVAILABLE_CHECKS()
	IF eGetawayID = FIB4_GETAWAY
		IF (GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A)
		OR GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A)) //probaly wrong but kinda makes sense testing needed
		AND ARE_OTHER_STORY_MISSIONS_COMPLETE()
			RETURN TRUE
		ENDIF
	ELIF eGetawayID = AGENCY_GETAWAY

		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_PREP_1)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_FIB4_UPDATE_LINE(TEXT_LABEL_23 sConv)
	IF ARE_ALL_OTHER_PREPS_COMPLETE()
		IF DO_MISSION_AVAILABLE_CHECKS()
			//Mission available
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				UPDATE_PHONE_CONV_F_T(sConv, TRUE)
			ELSE
				ePhoneCallState = PCS_WAITING_LAST_LINE
			ENDIF
		ELSE
			//mission not availabe
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				UPDATE_PHONE_CONV_F_T(sConv, FALSE)
			ELSE
				ePhoneCallState = PCS_WAITING_LAST_LINE
			ENDIF
		ENDIF
	ELSE
		ePhoneCallState = PCS_WAITING_LAST_LINE
	ENDIF
ENDPROC

PROC DO_AGENCY_UPDATE_LINE(TEXT_LABEL_23 sConv)
	IF ARE_ALL_OTHER_PREPS_COMPLETE()
		UPDATE_PHONE_CONV_F_T(sConv, TRUE)
	ELSE
		UPDATE_PHONE_CONV_F_T(sConv, FALSE)
	ENDIF
ENDPROC

PROC UPDATE_LINE(TEXT_LABEL_23 sConv)
	IF eGetawayID = FIB4_GETAWAY
		DO_FIB4_UPDATE_LINE(sConv)
	ELIF eGetawayID = AGENCY_GETAWAY
		DO_AGENCY_UPDATE_LINE(sConv)
	ENDIF
ENDPROC

/// PURPOSE:
///    State machine for updating the phone call
///    lots happens in here 
PROC MONITOR_PHONE_CALL()
	IF ePhoneCallState != PCS_NULL
		TEXT_LABEL_23 sConv
		sConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		#IF IS_DEBUG_BUILD SK_PRINT_INT("GET_CURRENT_SCRIPTED_CONVERSATION_LINE() === ", GET_CURRENT_SCRIPTED_CONVERSATION_LINE()) #ENDIF
		#IF IS_DEBUG_BUILD SK_PRINT(sConv) #ENDIF
		SWITCH ePhoneCallState
			CASE PCS_CONV_STARTED
				IF eGetawayID != FIN_GETAWAY
					IF NOT IS_STRING_NULL_OR_EMPTY(sConv)
						IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() > 0
							SET_DYNAMIC_BRANCH_FOR_ONGOING_CALL("LOCAUD", mPlayerPhoneCallSettings.sZoneConvLable)
							PLAY_DYNAMIC_BRANCH()
							#IF IS_DEBUG_BUILD SK_PRINT("SET = PCS_UPDATE_LINE ") #ENDIF
							ePhoneCallState = PCS_UPDATE_LINE
						ELIF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
						AND GET_CURRENT_SCRIPTED_CONVERSATION_LINE() = 0
							SET_DYNAMIC_BRANCH_FOR_ONGOING_CALL("LOCAUD", mPlayerPhoneCallSettings.sZoneConvLable)
							PLAY_DYNAMIC_BRANCH()
							#IF IS_DEBUG_BUILD SK_PRINT("SET = PCS_UPDATE_LINE ") #ENDIF
							ePhoneCallState = PCS_UPDATE_LINE
						ENDIF
					ENDIF
				ELSE
					ePhoneCallState = PCS_NULL
				ENDIF
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					#IF IS_DEBUG_BUILD SK_PRINT("SET call interupted = PCS_WAITING_PHONE_AWAY") #ENDIF
					ePhoneCallState = PCS_WAITING_LAST_LINE
				ENDIF
			BREAK

			CASE PCS_UPDATE_LINE
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					#IF IS_DEBUG_BUILD SK_PRINT("SET call interupted = PCS_WAITING_PHONE_AWAY") #ENDIF
					ePhoneCallState = PCS_WAITING_LAST_LINE
				ENDIF
				IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
					IF GET_CURRENT_SCRIPTED_CONVERSATION_LINE() >= 0
						UPDATE_LINE(sConv)
					ENDIF
				ELSE
					ePhoneCallState = PCS_WAITING_LAST_LINE
				ENDIF
			BREAK

			CASE PCS_WAITING_LAST_LINE
				IF NOT IS_CELLPHONE_CONVERSATION_PLAYING()
					#IF IS_DEBUG_BUILD SK_PRINT("ePhoneCallState = PCS_WAITING_PHONE_AWAY ") #ENDIF
					ePhoneCallState = PCS_WAITING_PHONE_AWAY
				ENDIF
			BREAK

			CASE PCS_WAITING_PHONE_AWAY
				IF NOT IS_PHONE_ONSCREEN()
					#IF IS_DEBUG_BUILD SK_PRINT("ePhoneCallState = PCS_DO_EXITED_VEHICLE_LINE ") #ENDIF
					IF eGetawayID = AGENCY_GETAWAY
						REMOVE_CONTACT_FROM_ALL_PHONEBOOKS(mPlayerPhoneCallSettings.eContact)
					ENDIF
					ePhoneCallState = PCS_DO_EXITED_VEHICLE_LINE
				ENDIF
			BREAK
			
			CASE PCS_DO_EXITED_VEHICLE_LINE
//				IF CREATE_CONVERSATION(s_conversation_peds, sTextblockConvs, GET_EXITED_VEHICLE_ROOT(), CONV_PRIORITY_MEDIUM)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF eGetawayID = FIB4_GETAWAY ///will need to be removed and the below functions updated to return the correct calls
							IF ARE_ALL_OTHER_PREPS_COMPLETE()
								IF (GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A)
								OR GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2B)) //probaly wrong but kinda makes sence testing needed
								AND ARE_OTHER_STORY_MISSIONS_COMPLETE()
									#IF IS_DEBUG_BUILD SK_PRINT("Mission available call steve ") #ENDIF
									REGISTER_PASS_CALL_LAST_PREP_M(TRUE)
									ePhoneCallState = PCS_NULL
								ELSE
									#IF IS_DEBUG_BUILD SK_PRINT("Mission IS NOT available call steve ") #ENDIF
									REGISTER_PASS_CALL_LAST_PREP_M(FALSE)
									ePhoneCallState = PCS_NULL
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD SK_PRINT("All other preps returned FALSE = PCS_NULL ") #ENDIF
								ePhoneCallState = PCS_NULL
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD SK_PRINT_INT("Getaway id doesnt = FIB = PCS_NULL  Getaway id = ", ENUM_TO_INT(eGetawayID)) #ENDIF
							ePhoneCallState = PCS_NULL
						ENDIF
					ELSE
						IF eGetawayID = FIB4_GETAWAY ///will need to be removed and the below functions updated to return the correct calls
							IF ARE_ALL_OTHER_PREPS_COMPLETE()
								IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_ASSASSIN_1)
									Execute_Code_ID(CID_TEXT_ASS1_REMINDER)
									CPRINTLN(DEBUG_MISSION, "Execute_Code_ID from script - ", GET_THIS_SCRIPT_NAME())
								ENDIF
							ENDIF
						ENDIF
						#IF IS_DEBUG_BUILD SK_PRINT("player ped isn't Michael = PCS_NULL ") #ENDIF
						ePhoneCallState = PCS_NULL
					ENDIF
//				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    If the the car is valid and the hiding place is valid 
///    update the correct contacts' secondary function 
PROC MONITOR_CONTACTS()
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
	AND IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))

		IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_ALT_CONTACT_SET))
			#IF IS_DEBUG_BUILD SK_PRINT("GFF_ALT_CONTACT_SET bit is not set, setting secondary function") #ENDIF
			CONTACT_WATCH(TRUE)
//		ELSE
//			#IF IS_DEBUG_BUILD
//				IF (GET_GAME_TIMER() - iGenericPrintTimer) > 1500
//				AND (GET_GAME_TIMER() - iGenericPrintTimer) < 1900
//					CPRINTLN(DEBUG_SHARM_STEALTH, "Contact flag set ") 
//				ENDIF
//			#ENDIF
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
//			IF (GET_GAME_TIMER() - iGenericPrintTimer) > 1500
//			AND (GET_GAME_TIMER() - iGenericPrintTimer) < 1900
//				IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
//					CPRINTLN(DEBUG_SHARM_STEALTH, "Car isnt valid not setting contact secondary function") 
//				ELSE
//					CPRINTLN(DEBUG_SHARM_STEALTH, "Car is valid") 
//				ENDIF
//				IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
//					CPRINTLN(DEBUG_SHARM_STEALTH, "Hiding position is not valid not setting secondary function") 
//				ELSE
//					CPRINTLN(DEBUG_SHARM_STEALTH, "Hiding position is valid") 
//				ENDIF
//			ENDIF
//		#ENDIF
		IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_ALT_CONTACT_SET))
			#IF IS_DEBUG_BUILD SK_PRINT("GFF_ALT_CONTACT_SET bit is set, but one of the conditions") #ENDIF
			CONTACT_WATCH(FALSE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Resets the script back to MS_PLACE_FIRST_ITEM state
///    clears the flow blips and unsets the mission flow flag
///    it also picks the correct help text to display
/// PARAMS:
///    bCarDead - did the player destroy the car 
PROC RESET(BOOL bCarDead = FALSE)
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " RESETTING GETAWAY CAR !!!!!!!!!!!!!!!!!!!!!!!!!! Dead:",bCarDead) #ENDIF
		IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "HTE_PICK_NEW_LOC_FRANK_TREV MONITER_PLAYER_MOVING_CAR == eTriggerState = TS_PLACE_FIRST_ITEM") #ENDIF
			IF bCarDead
				SET_HELP_TEXT_BIT(HTE_PICK_CAR_FRANK_TREV)
			ELSE
				SET_HELP_TEXT_BIT(HTE_PICK_NEW_LOC_FRANK_TREV)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " HTE_PICK_NEW_LOC_MIKE MONITER_PLAYER_MOVING_CAR == eTriggerState = TS_PLACE_FIRST_ITEM") #ENDIF
			IF bCarDead
				SET_HELP_TEXT_BIT(HTE_PICK_CAR_MIKE)
			ELSE
				SET_HELP_TEXT_BIT(HTE_PICK_NEW_LOC_MIKE)
			ENDIF
		ENDIF

		IF eGetawayID = FIB4_GETAWAY
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_PREP_3_COMPLETED, FALSE)
		ELIF eGetawayID = AGENCY_GETAWAY
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PREP_2_DONE, FALSE)
		ELIF eGetawayID = FIN_GETAWAY
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_DONE, FALSE)
		ENDIF

		IF eGetawayID != FIN_GETAWAY
			vPotentialHidingPos = <<0,0,0>>
		ENDIF

		ePhoneCallState = PCS_NULL
		eResultScreenState = RSS_INIT
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CAR_INFO_GRABBED))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_DONE_LAST_PREP_REMINDER))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
		
		IF eMissionState = MS_DISPLAYING_SPLASH
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " reset result screens") #ENDIF
	    	IF HAS_SCALEFORM_MOVIE_LOADED(splash)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(splash)
			ENDIF
			SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
		ENDIF

		UPDATE_SAVED_STATE(MS_PLACE_FIRST_ITEM)
	ENDIF
ENDPROC


/// PURPOSE:
///    Monitors the player moving the vehicle away from the set vehicle position after teh vheicle is placed
PROC MONITER_PLAYER_MOVING_CAR()
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
	AND IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
		IF IS_VEHICLE_OK(viVeh)
			IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viVeh, 10)
				IF ( VDIST2(GET_ENTITY_COORDS(viVeh), vCarPosition) >= 10*10)
					RESET()
				ENDIF
			ELSE
				
			ENDIF
		ELSE
			IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
				IF IS_VEHICLE_OK(viVeh)
					SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viVeh, FALSE)
				ENDIF
				SAFE_RELEASE_VEHICLE(viVeh)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " ~NOT IS_VEHICLE_OK(GET_VEHICLE_GEN_VEHICLE_INDEX == eTriggerState = TS_PLACE_FIRST_ITEM") #ENDIF
				RESET(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL COMPARE_GETAWAY_VEH_SETUP_WITH_VEHICLE(VEHICLE_INDEX vi)

	IF IS_VEHICLE_OK(vi)
		VEHICLE_SETUP_STRUCT setup
		GET_VEHICLE_SETUP(vi, setup)
		IF setup.iPlateIndex = mDroppedOffCarStruct.iPlateIndex
		AND setup.eModel = mDroppedOffCarStruct.eModel
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "plate index and model are equal") #ENDIF
			IF NOT IS_STRING_NULL_OR_EMPTY(setup.tlPlateText)
			AND NOT IS_STRING_NULL_OR_EMPTY(mDroppedOffCarStruct.tlPlateText)
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "plate lable isnt null or empty") #ENDIF
				IF ARE_STRINGS_EQUAL(setup.tlPlateText, mDroppedOffCarStruct.tlPlateText)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Plate lables are equal") #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks to see if the player is near the getaway area (100m around the placed position) 
///    If the player is in the area and teh vehicle is alive from the vehicle gen then 
///    we grab the vehicle and set it as a vehicle gen
PROC MAINTAIN_GETAWAY_VEHICLE()
	IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
		IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>, vCarPosition)
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCarPosition, MAX_STREAM_IN_DIST)
			AND NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "Player is in range of teh streaming dis") #ENDIF
				IF IS_VEHICLE_OK(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP))
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_OK(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)) == TRUE") #ENDIF
					viVeh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)
					SET_ENTITY_AS_MISSION_ENTITY(viVeh, TRUE, TRUE)
					IF IS_VEHICLE_OK(viVeh)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_OK(viVeh) == TRUE") #ENDIF
						IF NOT DECOR_EXIST_ON(viVeh, "GetawayVehicleValid")
							IF DECOR_SET_BOOL(viVeh, "GetawayVehicleValid", TRUE)
								#IF IS_DEBUG_BUILD SK_PRINT("ADDED decor - GetawayVehicleValid = TRUE ") #ENDIF
							ENDIF
						ENDIF
						SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
						SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
					ENDIF
				ELSE	
					IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
						IF NOT IS_VEHICLE_OK( GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP))
							#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "VEHGEN isnt available resetting the getaway vehicle") #ENDIF
							RESET(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_OK(viVeh)
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCarPosition, MAX_STREAM_IN_DIST)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT ARE_VECTORS_EQUAL(<<0,0,0>>, vCarPosition)
			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCarPosition, MAX_STREAM_IN_DIST)
				IF IS_VEHICLE_OK(viVeh)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viVeh)
						SET_MISSION_VEHICLE_GEN_VEHICLE(viVeh, vCarPosition, fCarHeading, VEHGEN_MISSION_VEH_FBI4_PREP, TRUE)
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " MAINTAIN_GETAWAY_VEHICLE() viVeh == RELEASED") #ENDIF
						SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viVeh, FALSE)
						SAFE_RELEASE_VEHICLE(viVeh)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_OK(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)) == TRUE") #ENDIF
						CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
					ELSE
						RESET()
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " MAINTAIN_GETAWAY_VEHICLE() Player in vehicle not passing to vehicle gen") #ENDIF
					ENDIF
				ELSE
					IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "IS_VEHICLE_OK returned false on viVeh reseting getaway") #ENDIF
						RESET(TRUE)
					ENDIF
				ENDIF
			ELSE
			
				IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
					IF NOT IS_VEHICLE_OK(viVeh)
						#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "VEHGEN vehicle isnt available resetting the getaway vehicle") #ENDIF
						RESET(TRUE)
					ENDIF
				ELSE
					IF NOT IS_VEHICLE_OK(viVeh)
						VEHICLE_INDEX viNewVeh = GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)
						CPRINTLN(DEBUG_MISSION,"VEHGEN New vehicle detected, changing to it: ",NATIVE_TO_INT(viNewVeh))
						IF IS_VEHICLE_OK(viNewVeh) AND viNewVeh <> viVeh
							viVeh = viNewVeh
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		MONITER_PLAYER_MOVING_CAR()
	ENDIF
ENDPROC

PROC REG_PERCENTAGE_COMP()
	IF eGetawayID = FIB4_GETAWAY
		CPRINTLN(DEBUG_MISSION, "Setting FBI4 getaway prep passed")
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_FBI4_P3)
	ELIF eGetawayID = AGENCY_GETAWAY
		CPRINTLN(DEBUG_MISSION, "Setting agency getaway prep passed")
//			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_FBI4_P3)
	ELIF eGetawayID = FIN_GETAWAY
		CPRINTLN(DEBUG_MISSION, "Setting finale_heist getaway prep passed")
//			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_FBI4_P3)
	ENDIF
ENDPROC

PROC SET_FBI4_GETAWAY_PREP_PASSED()
	g_savedGlobals.sFlowCustom.iMissionsCompleted++

	IF eGetawayID = FIB4_GETAWAY
		STAT_SET_INT(FL_CO_FB4P3, g_savedGlobals.sFlowCustom.iMissionsCompleted)
		IF ARE_ALL_OTHER_PREPS_COMPLETE()
			IF GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2A)
			OR GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_2B)
			AND ARE_OTHER_STORY_MISSIONS_COMPLETE()
				CPRINTLN(DEBUG_MISSION, "All other preps and story missions complete Fbi4 unlocked from Fbi prep 3")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MISSION_FBI_4_UNLOCKED_FROM_PREP, TRUE)
			ENDIF
		ENDIF
	ENDIF

	REG_PERCENTAGE_COMP()
	SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
ENDPROC


//Placeholder quick fix - BenR.
/// PURPOSE:
///    Place holder that seems to be sticking around
///    Displays a mission passed screen doesnt have stats 
///    simply to hammer home that the player has completed the getaway task
///    Uses a state machine and sets the mission passed 
PROC DISPLAY_SPLASH(FLOW_FLAG_IDS CompletionFlag)
	MONITOR_PHONE_CALL()
	IF ePhoneCallState = PCS_NULL
		SWITCH eResultScreenState
			CASE RSS_INIT
				SET_RESULT_SCREEN_DISPLAYING_STATE(TRUE)
				splash = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
				CPRINTLN(DEBUG_MISSION, "<FBI4P3> Running MISSION PASSED screen procedure.")
				CLEAR_ALL_HELP_BITS(NUM_HELP, TRUE)
				iTimeSplashEnd = GET_GAME_TIMER() + 3000
				IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_AWARDED_PLAYER_PERCENTAGE_PASSED_MISSION))
					CPRINTLN(DEBUG_MISSION, "<FBI4P3> Giving player percentaage.")
					SET_FBI4_GETAWAY_PREP_PASSED()
				ENDIF
				SET_MISSION_FLOW_FLAG_STATE(CompletionFlag, TRUE)
				eResultScreenState = RSS_LOAD
			BREAK
			
			CASE RSS_LOAD
				IF GET_GAME_TIMER() > iTimeSplashEnd
					CPRINTLN(DEBUG_MISSION, "<FBI4P3> loading scaleform...")
					IF HAS_SCALEFORM_MOVIE_LOADED(splash)
						CPRINTLN(DEBUG_MISSION, "<FBI4P3> ...scaleform loaded.")
						
					  	BEGIN_SCALEFORM_MOVIE_METHOD(splash, "SHOW_MISSION_PASSED_MESSAGE")
					  	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("M_FB4P3_P")
					 	SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING ("M_FB4P3")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100) 							//alpha of shard
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE) 						//unknown needs to be here...
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 							//NUMBER OF STATS LINES (FOR SETTING BACKGROUND HEIGHT) 
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		 			//For when there is no medal text and 2nd white line
					  	END_SCALEFORM_MOVIE_METHOD()
						
						CPRINTLN(DEBUG_MISSION, "<FBI4P3> Starting to display screen...")
						iTimeSplashEnd = GET_GAME_TIMER() + 10000
						MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(FALSE)
						eResultScreenState = RSS_UPDATE
				  	ENDIF
				ENDIF
			BREAK
			
			CASE RSS_UPDATE
				IF HAS_SCALEFORM_MOVIE_LOADED(splash)
				//Wait for the pass sound to be ready
				AND IS_MISSION_COMPLETE_READY_FOR_UI()
					IF GET_GAME_TIMER() < iTimeSplashEnd
				  		DRAW_SCALEFORM_MOVIE(	splash,0.5,
				                                0.3,
				                                1.0,
				                                1.0,
				                                255,255,255,255)
					ELIF GET_GAME_TIMER() < iTimeSplashEnd + 100
						//Mark pass screen for transitioning out
						BEGIN_SCALEFORM_MOVIE_METHOD(splash, "TRANSITION_OUT")	
						END_SCALEFORM_MOVIE_METHOD()
						//Advance timer to begin SF movie just once
						iTimeSplashEnd -= 100
					ELIF GET_GAME_TIMER() < iTimeSplashEnd + 500
						//Draw transition out anim
						DRAW_SCALEFORM_MOVIE(	splash,0.5,
				                                0.3,
				                                1.0,
				                                1.0,
				                                255,255,255,255)
					ELSE
						eResultScreenState = RSS_CLEANUP
						CPRINTLN(DEBUG_MISSION, "<FBI4P3> ...screen finished displaying.")
					ENDIF
			    ENDIF
			BREAK
			
			CASE RSS_CLEANUP
			    IF HAS_SCALEFORM_MOVIE_LOADED(splash)
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(splash)
				ENDIF
				SET_RESULT_SCREEN_DISPLAYING_STATE(FALSE)
				CPRINTLN(DEBUG_MISSION, "<FBI4P3> Cleaned up mission passed screen.")

				
				IF eGetawayID = FIN_GETAWAY
					//This handles all the mission passed call variations with the same logic
					//used by the other preps in the Finale Heist strand.
					Execute_Code_ID(CID_BIG_SCORE_PREPE_COMPLETED) 
				ENDIF

				IF IS_VEHICLE_OK(viVeh)
					SET_VEHICLE_DOORS_LOCKED(viVeh, VEHICLELOCK_UNLOCKED)
				ENDIF
				
				SET_LAST_COMPLETED_MISSION_STAT("M_FHPE", GET_CURRENT_PLAYER_PED_BIT())  				
				
				UPDATE_SAVED_STATE(MS_MONITOR_GETAWAY_AREA)
			BREAK
		ENDSWITCH
	ENDIF
	//called at the end of the function so that when we loop back we switch state if the 
	//car is broken or moved
	MAINTAIN_GETAWAY_VEHICLE()
ENDPROC

/// PURPOSE:
///    Checks to see if its safe to start loading the text
PROC MONITOR_TEXT_LOADED()
	IF NOT IS_STREAMING_ADDITIONAL_TEXT(OBJECT_TEXT_SLOT)
//		CPRINTLN(DEBUG_MISSION, "Text should load - ", GET_THIS_SCRIPT_NAME()) 
		SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SHOULD_LOAD_TEXT))
	ELSE
		#IF IS_DEBUG_BUILD 
//			IF bShouldLoadText
				CPRINTLN(DEBUG_MISSION, "bShouldLoadText = FALSE 2") 
//			ENDIF
		#ENDIF


		CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SHOULD_LOAD_TEXT))
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if the player is involved in a switch
PROC MONITOR_FOR_PLAYER_SWITCH()
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
			IF IS_VEHICLE_OK(viVeh)
				SET_MISSION_VEHICLE_GEN_VEHICLE(viVeh, vCarPosition, fCarHeading, VEHGEN_MISSION_VEH_FBI4_PREP, TRUE)
				SAFE_RELEASE_VEHICLE(viVeh)
				CPRINTLN(DEBUG_MISSION, "Player switch is in progress GFF_IS_PLAYER_NEAR_GETAWAY_AREA flag set re set vehicle gen") 
			ENDIF
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
		ENDIF
		CLEAR_ALL_HELP_BITS(NUM_HELP, TRUE)
		CONTACT_WATCH(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Check to see if the player is in a mod shop
/// RETURNS:
///    TRUE if the player is in any of the mod shops
FUNC BOOL IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
// Car Mod - AMB1 (v_carmod)
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP)
	// Car Mod - AMB2 (v_lockup)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_05_ID2)
	// Car Mod - AMB3 (v_carmod)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_06_BT1)
	// Car Mod - AMB4 (v_carmod3)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_07_CS1)
	// Car Mod - AMB5 (v_carmod3)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_08_CS6)
	// Car Mod - AMB6 (lr_supermod_int)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_SUPERMOD)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Monitors if the player is in a mod shop
///    If they are reset the vehicle checkin flags
PROC MONITOR_PLAYER_IN_MOD_SHOP()
	IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_ENTERED_MOD_SHOP))
		IF IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
			CPRINTLN(DEBUG_MISSION, "player is browsing in a mod shop")
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_ENTERED_MOD_SHOP))
		ENDIF
	ELSE
		IF NOT IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
			CPRINTLN(DEBUG_MISSION, "player is NOT browsing in a mod shop, Clearing bits for checked veh, valid veh and valid hiding place")
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_ENTERED_MOD_SHOP))
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_COMING_OFF_MISSION()
	MONITOR_TEXT_LOADED()
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_SHOULD_LOAD_TEXT))
		IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
		AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
//			#IF IS_DEBUG_BUILD 
//				INT t = 1
//			#ENDIF
//
			REQUEST_ADDITIONAL_TEXT("GETAWY", OBJECT_TEXT_SLOT)
			IF HAS_THIS_ADDITIONAL_TEXT_LOADED("GETAWY", OBJECT_TEXT_SLOT)
//				#IF IS_DEBUG_BUILD
//					IF t != -1
//						IF (GET_GAME_TIMER() - t) > 10000
//							t = -1 
//							CPRINTLN(DEBUG_MISSION, "Loading Text for Getaway vehicle")
//						ENDIF
//					ELSE
//						t = GET_GAME_TIMER()
//					ENDIF
//				#ENDIF
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
			ELSE
//				CPRINTLN(DEBUG_MISSION, "Text Hasn't loaded - ", GET_THIS_SCRIPT_NAME()) 
			ENDIF

			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
				MONITOR_COMING_OFF_MISSION()
				eHelpTextState = HTS_WAITING_HELP
				CPRINTLN(DEBUG_MISSION, "Text has been loaded we are off mission")
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_HAS_CURRENT_VEHICLE_BEEN_CHECKED))
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_HELP_LABLE_FROM_BIT(ENUM_TO_INT(HTE_ON_MISSION)))
					CLEAR_HELP()
					CPRINTLN(DEBUG_MISSION,"SETUP_COMING_OFF_MISSION() cleared help 1")
				ENDIF
				CLEAR_HELP_TEXT_BIT(HTE_ON_MISSION)
				eMissionState = INT_TO_ENUM(MISSION_STATE, g_savedGlobals.sAmbient.iGetawayState)
				CPRINTLN(DEBUG_MISSION, "Set state to = ", g_savedGlobals.sAmbient.iGetawayState)
			ELSE
//				CPRINTLN(DEBUG_MISSION, "Text loaded flag not set - ", GET_THIS_SCRIPT_NAME()) 
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD 
				IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
					SK_PRINT("Player is trevor during Agency getaway") 
				ENDIF
			#ENDIF
//			#IF IS_DEBUG_BUILD 
//				IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
//					SK_PRINT("Text bit not set 1") 
//				ENDIF
//			#ENDIF
			
		ENDIF
	ELSE
		CPRINTLN(DEBUG_MISSION, " Text should not load ", GET_THIS_SCRIPT_NAME())
	ENDIF
ENDPROC

PROC SETUP_GOING_ON_MISSION()
	IF eMissionState != MS_DISPLAYING_SPLASH
		IF eMissionState != MS_MONITOR_GETAWAY_AREA
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT g_bFlowHelpDisplaying
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SCRIPT_CLEARED_HELP))
				CPRINTLN(DEBUG_MISSION,"SETUP_GOING_ON_MISSION() cleared help 1")
				CLEAR_HELP()
			ENDIF

			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
				IF IS_VEHICLE_OK(viVeh)
					SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viVeh, FALSE)
				ENDIF
				SAFE_RELEASE_VEHICLE(viVeh)
				#IF IS_DEBUG_BUILD SK_PRINT("Car is valid but we are not off mission releasing ") #ENDIF
				CLEAR_CAR_RELATED_BITS()
				IF eGetawayID != FIN_GETAWAY
					vPotentialHidingPos = <<0,0,0>>
				ENDIF
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_IF_PLAYER_RETURNS_TO_VEHICLE))
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("Car is not valid but we are not off mission releasing ") #ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
				CPRINTLN(DEBUG_MISSION, "Going on mission and vehicle is valid release vehicle and pass to veh gen")
				SET_MISSION_VEHICLE_GEN_VEHICLE(viVeh, vCarPosition, fCarHeading, VEHGEN_MISSION_VEH_FBI4_PREP, TRUE)
				IF IS_VEHICLE_OK(viVeh)
					SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viVeh, FALSE)
				ENDIF
				SAFE_RELEASE_VEHICLE(viVeh)
				CLEAR_CAR_RELATED_BITS()
			ENDIF

			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "We have already dropped off the getaway vehicle no need to reset car and position flags")
			#ENDIF
		ENDIF

		IF HAS_THIS_ADDITIONAL_TEXT_LOADED("GETAWY", OBJECT_TEXT_SLOT)
			CLEAR_ADDITIONAL_TEXT(OBJECT_TEXT_SLOT, FALSE)
			#IF IS_DEBUG_BUILD 
				CPRINTLN(DEBUG_MISSION, "Text is loaded  but we are on mission unloading text")
			#ENDIF
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SHOULD_LOAD_TEXT))
			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
			CONTACT_WATCH(FALSE)
			IF IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
				CLEAR_ALL_HELP_BITS(HTE_ON_FRIEND_ACT, TRUE)
			ELSE
				CLEAR_ALL_HELP_BITS(HTE_ON_MISSION, TRUE)
			ENDIF	
			
		ENDIF

		CPRINTLN(DEBUG_MISSION, "eMissionState = MS_WAIT_ON_MISSION ", GET_THIS_SCRIPT_NAME())

		eMissionState = MS_WAIT_ON_MISSION
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEAR_PLANNING_BOARD(FLOAT fDist = 50.0)
	IF eGetawayID = AGENCY_GETAWAY
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), g_sPlanningLocationData[PLN_SWEATSHOP].vPlanningLocation, fDist)
			RETURN TRUE
		ENDIF
	ELIF eGetawayID = FIN_GETAWAY
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), g_sPlanningLocationData[PLN_STRIPCLUB].vPlanningLocation, fDist)
			RETURN TRUE
		ENDIF
	ELIF eGetawayID = FIB4_GETAWAY
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), g_sPlanningLocationData[PLN_TREV_CITY].vPlanningLocation, fDist)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_OFF_MISSION()
//	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
//		IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("GETAWY", OBJECT_TEXT_SLOT)
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("Getaway vehicle thinks the text is loaded but code says no")
//			#ENDIF
//			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_TEXT_IS_LOADED))
//			CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_SHOULD_LOAD_TEXT))
//		ENDIF
//	ENDIF
	IF g_OnMissionState = MISSION_TYPE_OFF_MISSION //we are off ALL mission
	AND NOT IS_MISSION_LEADIN_ACTIVE()
	AND g_iOffMissionCutsceneRequestActive = NULL_OFFMISSION_CUTSCENE_REQUEST
	AND NOT IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
	AND NOT IS_PLAYER_NEAR_PLANNING_BOARD()
	AND NOT g_bTaxiProceduralRunning
//		CPRINTLN(DEBUG_MISSION, "Off mission = yes ", GET_THIS_SCRIPT_NAME())
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
			IF (GET_GAME_TIMER() - iGenericPrintTimer) < 2000
				IF g_OnMissionState != MISSION_TYPE_OFF_MISSION //we are off ALL mission
					 SK_PRINT_INT("Not off mission = ", ENUM_TO_INT(g_OnMissionState)) 
				ENDIF

				IF IS_MISSION_LEADIN_ACTIVE()
					 SK_PRINT("Mission leadin is active") 
				ENDIF

				IF g_iOffMissionCutsceneRequestActive != NULL_OFFMISSION_CUTSCENE_REQUEST
					 SK_PRINT("Cutscene is being requested") 
				ENDIF

				IF IS_FRIEND_ACTIVITY_SCRIPT_RUNNING()
					 SK_PRINT("Friend script is running") 
				ENDIF

				IF IS_PLAYER_NEAR_PLANNING_BOARD()
					SK_PRINT("Player near a planning board ") 
				ENDIF

				IF g_bTaxiProceduralRunning
					SK_PRINT("g_bTaxiProceduralRunning ") 
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_GETAWAY_SLEEP()
	IF eGetawayID != FIN_GETAWAY
//		CPRINTLN(DEBUG_MISSION, "Not finale hiest getaway")

		IF eGetawayID = AGENCY_GETAWAY
		AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
				SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
			ENDIF
//			CPRINTLN(DEBUG_MISSION, "Agency getaway and player is trevor")
			RETURN TRUE
		ELSE
			IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
				CLEAR_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_PLAYER_IS_TREV_AGENCY))
			ENDIF
//			CPRINTLN(DEBUG_MISSION, "Agency getaway and player is NOT trevor")
		ENDIF

		IF IS_PLAYER_OFF_MISSION()
			RETURN FALSE
		ENDIF
	ELSE 
//		CPRINTLN(DEBUG_MISSION, "IS finale hiest getaway")
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR)
//			CPRINTLN(DEBUG_MISSION, "Placing finale car")
			RETURN FALSE
		ELSE
			IF IS_PLAYER_OFF_MISSION()
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is no longer off mission 
///    also monitors for leadins being active or if there is a cutscene 
///    being loaded
///    If any of the above are happening the script unloads the text and puts the script in 
///    a suspended state that doesnt run any checks or display help text. It does however display a message
///    saying find a more discreet location for the vehicle - only if the vehicle is valid
PROC MONITOR_ON_MISSION_STATUS()
	IF NOT SHOULD_GETAWAY_SLEEP()
		IF NOT IS_RESULT_SCREEN_DISPLAYING()
			IF IS_SCREEN_FADED_IN()
				IF NOT IS_SCREEN_FADING_IN()
					SETUP_COMING_OFF_MISSION()
				ELSE
					CPRINTLN(DEBUG_MISSION, "SCREEN IS FADING IN ", GET_THIS_SCRIPT_NAME())
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "SCREEN IS FADED OUT ", GET_THIS_SCRIPT_NAME())		
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION, "Result screen is being displayed ", GET_THIS_SCRIPT_NAME())
		ENDIF
	ELSE					//we are on mission	
//		CPRINTLN(DEBUG_MISSION, "SHOULD_GETAWAY_SLEEP() = TRUE")

		IF eMissionState != MS_WAIT_ON_MISSION
			SETUP_GOING_ON_MISSION()
		ELSE
//			#IF IS_DEBUG_BUILD
//				IF iOnMissionTimerCheck = -1
//					iOnMissionTimerCheck = GET_GAME_TIMER()
//				ELSE
//					IF (GET_GAME_TIMER() - iOnMissionTimerCheck) > 20000
//						CPRINTLN(DEBUG_SHARM_STEALTH, "Getaway vehicle script is waiting - ", GET_THIS_SCRIPT_NAME())
//						IF NOT IS_MISSION_LEADIN_ACTIVE()
//							CPRINTLN(DEBUG_SHARM_STEALTH, "There is not a lead in active")
//						ELSE
//							CPRINTLN(DEBUG_SHARM_STEALTH, "There is a lead in active")
//						ENDIF
//						CPRINTLN(DEBUG_SHARM_STEALTH, "On mission flag is = ", ENUM_TO_INT(g_OnMissionState), " ", GET_THIS_SCRIPT_NAME())
//						iOnMissionTimerCheck = GET_GAME_TIMER()
//					ENDIF
//				ENDIF
//			#ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL UPDATE_SECURE_MISSION_CANDIDATE_FOR_FINALE_HEIST_PREP(INT &iCandidateId)
    SWITCH Request_Mission_Launch(iCandidateId, MCTID_MEET_CHARACTER, MISSION_TYPE_RANDOM_EVENT)
        CASE MCRET_ACCEPTED
            CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " secured a candidate ID = ", iCandidateId)
            RETURN TRUE
        BREAK

        CASE MCRET_DENIED
            CPRINTLN(DEBUG_MISSION, GET_THIS_SCRIPT_NAME(), " Didn't secured a candidate ID .")
 			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR, FALSE)
           	HANG_UP_AND_PUT_AWAY_PHONE()
			RETURN FALSE
        BREAK
        CASE MCRET_PROCESSING
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR, TRUE)
			RETURN FALSE
        BREAK
    ENDSWITCH

	RETURN FALSE
ENDFUNC



FUNC SP_MISSIONS GET_MAIN_MISSION_FOR_GETAWAY()
	IF eGetawayID = FIB4_GETAWAY
		RETURN SP_MISSION_FBI_4
	ELIF eGetawayID = AGENCY_GETAWAY
		RETURN SP_HEIST_AGENCY_3A
	ELIF eGetawayID = FIN_GETAWAY
		RETURN SP_HEIST_FINALE_2B
	ENDIF

	RETURN SP_MISSION_NONE
ENDFUNC

/// PURPOSE:
///    While on mission check to see if the player has moved the getaway vehicle or
///    destroyed the vehicle
///    !!!! needs work 
PROC MONITOR_PLAYER_MOVING_VEHICLE_ON_MISSION()
	IF NOT ARE_VECTORS_EQUAL(vCarPosition, <<0,0,0>>)
	AND MISSION_FLOW_GET_RUNNING_MISSION() != GET_MAIN_MISSION_FOR_GETAWAY()
		IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCarPosition, 60)
			IF IS_VEHICLE_OK(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP))
				IF ( VDIST2(GET_ENTITY_COORDS(GET_VEHICLE_GEN_VEHICLE_INDEX(VEHGEN_MISSION_VEH_FBI4_PREP)), vCarPosition) >= 10*10)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITER_PLAYER_MOVING_CAR On mission move ") #ENDIF
					RESET()
				ENDIF
			ELSE
				IF NOT GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_MISSION_VEH_FBI4_PREP, VEHGEN_S_FLAG_AVAILABLE)
					#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, "MONITER_PLAYER_MOVING_CAR Destroyed car and vehicle gen not available") #ENDIF
					RESET(TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    
PROC GRAB_CAR_PLACED_INFO()
//	
	#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 3") #ENDIF
	IF NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_CAR_INFO_GRABBED))
		#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 4") #ENDIF
		IF IS_VEHICLE_OK(viVeh)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 5") #ENDIF
			vCarPosition = GET_ENTITY_COORDS(viVeh)
			fCarHeading = GET_ENTITY_HEADING(viVeh)
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CAR_INFO_GRABBED))
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_PLAYER_NEAR_GETAWAY_AREA))
			GET_VEHICLE_SETUP(viVeh, mDroppedOffCarStruct)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " 6") #ENDIF
			SET_MISSION_VEHICLE_GEN_VEHICLE(viVeh, vCarPosition, fCarHeading, VEHGEN_MISSION_VEH_FBI4_PREP, TRUE)
			#IF IS_DEBUG_BUILD CPRINTLN(DEBUG_MISSION, " Car info grabbed") #ENDIF
			SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
			SET_VEHICLE_IGNORED_BY_QUICK_SAVE(viVeh)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Container function that populates arrays and other setup
///    using other setup functions
PROC POPULATE_STUFF(GETAWAY_ID id)
	eGetawayID = id
	
	IF eGetawayID = FIB4_GETAWAY
		sTextblockConvs = "FBIPRAU"
	ELIF eGetawayID = AGENCY_GETAWAY
		sTextblockConvs = "AHFAUD"
	ELIF eGetawayID = FIN_GETAWAY
		sTextblockConvs = "FHFAUD"
	ENDIF

	mnInvalidModels[0] = TRASH
	mnInvalidModels[1] = TOWTRUCK
	mnInvalidModels[2] = AMBULANCE
	mnInvalidModels[3] = BARRACKS2
	mnInvalidModels[4] = STRETCH
	mnInvalidModels[5] = PHANTOM
	mnInvalidModels[6] = PACKER
	mnInvalidModels[7] = Blazer
	mnInvalidModels[8] = Blazer2
	IF eGetawayID != FIB4_GETAWAY
		mnInvalidModels[9] = SENTINEL2
	ELSE
		mnInvalidModels[9] = DUMMY_MODEL_FOR_SCRIPT
	ENDIF

	POPULATE_RESTRICTED_AREAS()
	POPULATE_HELP()
	SET_BIT(iGetawayFlowFlag, ENUM_TO_INT(GFF_CHECK_FIRST_HELP_BIT))
	iReminderTimer = GET_GAME_TIMER()
ENDPROC

PROC SAFE_MONITOR_CONTACT_HIGHLIGHT()
	IF IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_CAR_VALID))
	AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_IS_HIDING_POS_VALID))
	AND NOT IS_BIT_SET(iGetawayFlowFlag, ENUM_TO_INT(GFF_VEHICLE_PLACED))
	AND SAFE_IS_CONTACT_LIST_ON_SCREEN()
		IF (Is_Phone_Control_Just_Pressed(PLAYER_CONTROL, INPUT_CELLPHONE_UP)
		OR Is_Phone_Control_Just_Pressed(PLAYER_CONTROL, INPUT_CELLPHONE_DOWN))
			iHighlightWindow = GET_GAME_TIMER()
		ENDIF

		IF iHighlightWindow != -1
			IF (GET_GAME_TIMER() - iHighlightWindow) < 200
				eHighlighted = GET_CURRENTLY_HIGHLIGHTED_CONTACT()
//				CPRINTLN(DEBUG_MISSION, "GETTING HIGHLIGHTED CHAR", eHighlighted)
			ELSE
				iHighlightWindow = -1
			ENDIF
		ENDIF
	ELSE
		eHighlighted = NO_CHARACTER
		iHighlightWindow = -1
	ENDIF
ENDPROC
