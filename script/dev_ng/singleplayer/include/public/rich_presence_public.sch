//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 09/03/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					 	SP Rich Presence Public Header							│
//│																				│
//│		Public commands for setting console specific rich presence values 		│
//│		for single player mission/minigame events. These are console dashboard	│
//│		status strings that can be seen when looking at friends in friend		│
//│		lists.																	│
//│																				│
//│		* 	Values stored in 													│
//│			//depot/gta5/assets/GameText/American/americanXLAST.txt				│
//│																				│
//│		* 	Values exported from DEBUG_EXPORT_SP_RICH_PRESENCE_ORDER() to 		│
//│	  		x:\gta5\build\dev\common\data\script\SP_rich_presence.txt	 		│
//│																				│
//│		* 	DebugMenu->ScriptTools->Generate SP Rich-presence Order 			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "presence_enum.sch"


STRUCT RICH_PRESENCE_DATA
	INT index
ENDSTRUCT


PROC SET_DEFAULT_RICH_PRESENCE_FOR_SP()
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_0, 0, 0, 0)	// Miguel - don't set index for PRESENCE_PRES_0 fix for Error: [script_network] Assertf(sizeOfData == numFields*(int)sizeof(int)) 
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to default SP value 0 which maps to string \"", GET_STRING_FROM_TEXT_FILE("PRESENCE_0_STR"), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		TEXT_LABEL_23 tRichPresence = "PRESENCE_0_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_0, tRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to default SP value \"", tRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Sets the current rich presence value to a string representation of a
///    			GTAV singleplayer story mission.
///   
PROC SET_RICH_PRESENCE_FOR_SP_STORY_MISSION(SP_MISSIONS paramMissionID)
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		RICH_PRESENCE_DATA sRPMission
		sRPMission.index = ENUM_TO_INT(paramMissionID)
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_1, sRPMission, SIZE_OF(sRPMission), 1)
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_23 tRichPresence = "SPM_"
			tRichPresence += ENUM_TO_INT(paramMissionID)
			tRichPresence += "_STR"
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to index ", ENUM_TO_INT(paramMissionID), " which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		TEXT_LABEL_23 tRichPresence = "SPM_"
		tRichPresence += ENUM_TO_INT(paramMissionID)
		tRichPresence += "_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_1, tRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to label \"", tRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Sets the current rich presence value to a string representation of a
///    			GTAV singleplayer random character mission.
///   
PROC SET_RICH_PRESENCE_FOR_SP_RC_MISSION(g_eRC_MissionIDs paramRCMissionID)
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		RICH_PRESENCE_DATA sRPMission
		sRPMission.index = ENUM_TO_INT(paramRCMissionID)
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_9, sRPMission, SIZE_OF(sRPMission), 1)
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_23 tRichPresence = "SPRC_"
			tRichPresence += ENUM_TO_INT(paramRCMissionID)
			tRichPresence += "_STR"
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to index ", ENUM_TO_INT(paramRCMissionID), " which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		TEXT_LABEL_23 tRichPresence = "SPRC_"
		tRichPresence += ENUM_TO_INT(paramRCMissionID)
		tRichPresence += "_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_9, tRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to label \"", tRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Sets the current rich presence value to a string representation of a
///    			GTAV singleplayer minigame.
///   
PROC SET_RICH_PRESENCE_FOR_SP_MINIGAME(SP_MINIGAMES paramMinigameID)
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		RICH_PRESENCE_DATA sRPMinigame
		sRPMinigame.index = ENUM_TO_INT(paramMinigameID)
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_8, sRPMinigame, SIZE_OF(sRPMinigame), 1)
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_23 tRichPresence = "SPMG_"
			tRichPresence += ENUM_TO_INT(paramMinigameID)
			tRichPresence += "_STR"
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to index ", ENUM_TO_INT(paramMinigameID), " which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		TEXT_LABEL_23 tRichPresence = "SPMG_"
		tRichPresence += ENUM_TO_INT(paramMinigameID)
		tRichPresence += "_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_8, tRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to label \"", tRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(tRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Sets the current rich presence value to a string representation of GTAV SP
/// 			Director Mode.
///   
PROC SET_RICH_PRESENCE_FOR_SP_DIRECTOR_MODE()
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		RICH_PRESENCE_DATA sRPMinigame
		sRPMinigame.index = 29 // Director Mode is hardcoded to minigame index 29.
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_8, sRPMinigame, SIZE_OF(sRPMinigame), 1)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to index 29 which maps to string \"", GET_STRING_FROM_TEXT_FILE("SPMG_29_STR"), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		STRING strRichPresence = "SPMG_29_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_8, strRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to label \"", strRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(strRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC


/// PURPOSE: 	Sets the current rich presence value to a string representation of GTAV SP
/// 			Rockstar Editor.
///   
PROC SET_RICH_PRESENCE_FOR_SP_ROCKSTAR_EDITOR()
	IF IS_XBOX360_VERSION()
	OR IS_XBOX_PLATFORM()
		RICH_PRESENCE_DATA sRPMinigame
		sRPMinigame.index = 30 // Rockstar Editor is hardcoded to minigame index 30.
		NETWORK_SET_RICH_PRESENCE(PRESENCE_PRES_8, sRPMinigame, SIZE_OF(sRPMinigame), 1)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set XBox360 rich presence to index 30 which maps to string \"", GET_STRING_FROM_TEXT_FILE("SPMG_30_STR"), "\".")
		#ENDIF
	ELIF IS_PS3_VERSION()
	OR IS_PLAYSTATION_PLATFORM()
		STRING strRichPresence = "SPMG_30_STR"
		NETWORK_SET_RICH_PRESENCE_STRING(PRESENCE_PRES_8, strRichPresence)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_FLOW, "Set PS3 rich presence to label \"", strRichPresence, "\" which maps to string \"", GET_STRING_FROM_TEXT_FILE(strRichPresence), "\".")
		#ENDIF
	ENDIF
ENDPROC
