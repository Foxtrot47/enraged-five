//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   savegame_public.sch                                        	//
//      AUTHOR          :   Kenneth Ross                                                //
//      DESCRIPTION     :   Includes various savegame related procs.		 		    //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_streaming.sch"		// For IS_IPL_ACTIVE - must have been getting access to it indirectly
USING "script_player.sch"
USING "mission_repeat_public.sch"


/// PURPOSE: Returns TRUE if an autosave request is currently in progress
FUNC BOOL IS_AUTOSAVE_REQUEST_IN_PROGRESS()
    RETURN (g_sAutosaveData.bRequest)
ENDFUNC


/// PURPOSE: Make an autosave request
FUNC BOOL MAKE_AUTOSAVE_REQUEST()
	IF IS_REPEAT_PLAY_ACTIVE()
		PRINTSTRING("\n[AUTOSAVE] Failed: Repeat Play is in progress.")PRINTNL()
		RETURN FALSE
	ENDIF
	
	IF g_sAutosaveData.bIgnoreScreenFade
		IF g_sAutosaveData.iQueuedRequests > 0
			PRINTSTRING("\n[AUTOSAVE] Failed: Too many requests already queued while ignoring fades.")PRINTNL()
			RETURN FALSE
		ENDIF
	ELSE
		IF g_sAutosaveData.iQueuedRequests > 1
			PRINTSTRING("\n[AUTOSAVE] Failed: Too many requests already queued.")PRINTNL()
			RETURN FALSE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	
	PRINTLN("[AUTOSAVE] Autosave request made by ", GET_THIS_SCRIPT_NAME())
	g_sAutosaveData.iQueuedRequests++
	
	RETURN TRUE
ENDFUNC


/// PURPOSE: Cancels any currently queued autosave requests. Use very carefully.
PROC CLEAR_AUTOSAVE_REQUESTS()

	PRINTLN("[AUTOSAVE] CLEAR_AUTOSAVE_REQUESTS called by ", GET_THIS_SCRIPT_NAME())
	
	#IF IS_DEBUG_BUILD
		IF NOT g_flowUnsaved.bUpdatingGameflow
	#ENDIF
	
		g_sAutosaveData.bFlushAutosaves = TRUE
		
	#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[AUTOSAVE] CLEAR_AUTOSAVE_REQUEST ignored as a debug gameflow launch is in progress.")
		ENDIF
	#ENDIF

ENDPROC


PROC SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(BOOL bIgnoreOnMissionFlag, BOOL bIgnoreScreenFade = FALSE)
	#IF IS_DEBUG_BUILD
		IF bIgnoreOnMissionFlag
			PRINTSTRING("\n SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE) called by '")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("'")PRINTNL()
		ELSE
			PRINTSTRING("\n SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE) called by '")PRINTSTRING(GET_THIS_SCRIPT_NAME())PRINTSTRING("'")PRINTNL()
		ENDIF
	#ENDIF
	g_sAutosaveData.bIgnoreOnMissionFlag = bIgnoreOnMissionFlag
	
	g_sAutosaveData.bIgnoreScreenFade = bIgnoreScreenFade
ENDPROC
#if USE_CLF_DLC
FUNC BOOL GET_SAVEGAME_ROOM_TYPEclf(SAVEHOUSE_NAME_ENUM eSavehouse, TEXT_LABEL_31 &tlRoomType)
	
	tlRoomType = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSEclf_MICHAEL_BH
			tlRoomType = "v_michael"
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_SC
			tlRoomType = "v_franklins"
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_VH
			tlRoomType = "v_franklinshouse"
		BREAK
		CASE SAVEHOUSEclf_TREVOR_CS
		CASE SAVEHOUSEclf_MICHAEL_CS
			// We need to do a check on the loaded IPL here as the room name can change
			IF IS_IPL_ACTIVE("TrevorsTrailer")
				tlRoomType = "v_trailer"
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTidy")
				tlRoomType = "V_TrailerTIDY"
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTrash")
				tlRoomType = "V_TrailerTRASH"
			ENDIF
		BREAK
		CASE SAVEHOUSEclf_TREVOR_VB
			tlRoomType = "v_trevors"
		BREAK
		CASE SAVEHOUSEclf_TREVOR_SC
			tlRoomType = "v_strip3"
		BREAK
		CASE SAVEHOUSEclf_FRANKLIN_PRO
		CASE SAVEHOUSEclf_MICHAEL_PRO
		CASE SAVEHOUSEclf_TREVOR_PRO
			tlRoomType = "v_psycheoffice"
		BREAK
	ENDSWITCH
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomType, ""))
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL GET_SAVEGAME_ROOM_TYPENRM(SAVEHOUSE_NAME_ENUM eSavehouse, TEXT_LABEL_31 &tlRoomType)
	
	tlRoomType = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSENRM_BH
			tlRoomType = "v_michael"
		BREAK
		CASE SAVEHOUSENRM_CHATEAU
			tlRoomType = "v_franklins"//temp name
		BREAK		
	ENDSWITCH
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomType, ""))
ENDFUNC
#endif

FUNC BOOL GET_SAVEGAME_ROOM_TYPE(SAVEHOUSE_NAME_ENUM eSavehouse, TEXT_LABEL_31 &tlRoomType)

#if USE_CLF_DLC
	return GET_SAVEGAME_ROOM_TYPEclf(eSavehouse,tlRoomType)
#endif

#if USE_NRM_DLC
	return GET_SAVEGAME_ROOM_TYPENRM(eSavehouse,tlRoomType)
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
	tlRoomType = ""
	
	SWITCH eSavehouse
		CASE SAVEHOUSE_MICHAEL_BH
			tlRoomType = "v_michael"
		BREAK
		CASE SAVEHOUSE_FRANKLIN_SC
			tlRoomType = "v_franklins"
		BREAK
		CASE SAVEHOUSE_FRANKLIN_VH
			tlRoomType = "v_franklinshouse"
		BREAK
		CASE SAVEHOUSE_TREVOR_CS
		CASE SAVEHOUSE_MICHAEL_CS
			// We need to do a check on the loaded IPL here as the room name can change
			IF IS_IPL_ACTIVE("TrevorsTrailer")
				tlRoomType = "v_trailer"
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTidy")
				tlRoomType = "V_TrailerTIDY"
			ELIF IS_IPL_ACTIVE("TrevorsTrailerTrash")
				tlRoomType = "V_TrailerTRASH"
			ENDIF
		BREAK
		CASE SAVEHOUSE_TREVOR_VB
			tlRoomType = "v_trevors"
		BREAK
		CASE SAVEHOUSE_TREVOR_SC
			tlRoomType = "v_strip3"
		BREAK
		CASE SAVEHOUSE_FRANKLIN_PRO
		CASE SAVEHOUSE_MICHAEL_PRO
		CASE SAVEHOUSE_TREVOR_PRO
			tlRoomType = "v_psycheoffice"
		BREAK
	ENDSWITCH

#endif
#endif	
	
	RETURN (NOT ARE_STRINGS_EQUAL(tlRoomType, ""))	
	
ENDFUNC

/// PURPOSE: Returns TRUE if the player is in their savehouse.
///    NOTE: Set eSavehouse to NUMBER_OF_SAVEHOUSE_LOCATIONS to check all savehouses



#if USE_CLF_DLC
FUNC BOOL IS_PLAYER_IN_SAVEHOUSE(SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_CLF_SAVEHOUSE)

	IF g_sShopSettings.playerInterior = NULL
		RETURN FALSE
	ENDIF

	IF eSavehouse = NUMBER_OF_CLF_SAVEHOUSE
		INT iSavehouse
		REPEAT ENUM_TO_INT(NUMBER_OF_CLF_SAVEHOUSE) iSavehouse
			IF IS_PLAYER_IN_SAVEHOUSE(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			TEXT_LABEL_31 tlRoomType
			GET_SAVEGAME_ROOM_TYPE(eSavehouse, tlRoomType)
			INTERIOR_INSTANCE_INDEX savehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_sSavehouses[eSavehouse].vSpawnCoords, tlRoomType)
			IF savehouseInterior != NULL
			AND g_sShopSettings.playerInterior = savehouseInterior
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
#if USE_NRM_DLC
FUNC BOOL IS_PLAYER_IN_SAVEHOUSE(SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_NRM_SAVEHOUSE)

	IF g_sShopSettings.playerInterior = NULL
		RETURN FALSE
	ENDIF

	IF eSavehouse = NUMBER_OF_NRM_SAVEHOUSE
		INT iSavehouse
		REPEAT ENUM_TO_INT(NUMBER_OF_NRM_SAVEHOUSE) iSavehouse
			IF IS_PLAYER_IN_SAVEHOUSE(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			TEXT_LABEL_31 tlRoomType
			GET_SAVEGAME_ROOM_TYPE(eSavehouse, tlRoomType)
			INTERIOR_INSTANCE_INDEX savehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_sSavehouses[eSavehouse].vSpawnCoords, tlRoomType)
			IF savehouseInterior != NULL
			AND g_sShopSettings.playerInterior = savehouseInterior
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#endif

#if not USE_CLF_DLC
#if not USE_NRM_DLC
FUNC BOOL IS_PLAYER_IN_SAVEHOUSE(SAVEHOUSE_NAME_ENUM eSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS)

	IF g_sShopSettings.playerInterior = NULL
		RETURN FALSE
	ENDIF

	IF eSavehouse = NUMBER_OF_SAVEHOUSE_LOCATIONS
		INT iSavehouse
		REPEAT ENUM_TO_INT(NUMBER_OF_SAVEHOUSE_LOCATIONS) iSavehouse
			IF IS_PLAYER_IN_SAVEHOUSE(INT_TO_ENUM(SAVEHOUSE_NAME_ENUM, iSavehouse))
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			TEXT_LABEL_31 tlRoomType
			GET_SAVEGAME_ROOM_TYPE(eSavehouse, tlRoomType)
			INTERIOR_INSTANCE_INDEX savehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_sSavehouses[eSavehouse].vSpawnCoords, tlRoomType)
			IF savehouseInterior != NULL
			AND g_sShopSettings.playerInterior = savehouseInterior
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#endif
#endif


/// PURPOSE: Returns TRUE if the savehouse interior is ready
FUNC BOOL IS_SAVEHOUSE_INTERIOR_READY(SAVEHOUSE_NAME_ENUM eSavehouse)

	TEXT_LABEL_31 tlRoomType
	IF GET_SAVEGAME_ROOM_TYPE(eSavehouse, tlRoomType)
		INTERIOR_INSTANCE_INDEX savehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_sSavehouses[eSavehouse].vSpawnCoords, tlRoomType)
		IF (savehouseInterior != NULL)
			IF IS_INTERIOR_READY(savehouseInterior)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

