///    Public accessors for other scripts to know about prostitutes and the player

USING "commands_vehicle.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_entity.sch"

/// PURPOSE: Will return TRUE if the player is driving with a prostitute in his passenger seat

FUNC BOOL IS_PLAYER_WITH_A_PROSTITUTE()
	
	IF g_bPlayerHasActiveProstitute
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTempCarPlayerIsIn = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(vehTempCarPlayerIsIn)
				
				IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehTempCarPlayerIsIn, VS_FRONT_RIGHT))
					RETURN TRUE
				ENDIF
				
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//EOF
