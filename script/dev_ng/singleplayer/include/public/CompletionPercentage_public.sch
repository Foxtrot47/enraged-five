USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_misc.sch"
USING "CompletionPercentage_private.sch"
//USING "Flow_Mission_Data_Public.sch"


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      HEADER NAME     :   CompletionPercentage_public.sch
//      AUTHOR          :   Steve Taylor
//      DESCRIPTION     :   Contains the initialisation and manipulation functions for 
//                          Completion Percentage tracking.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Bitmasks and shift constant used to package mission information into globals ints to save space.
//If each element uses 7 bits we can have up to 127 entries per piece of data and fit 4 pieces of data in an INT.
CONST_INT   CP_7BIT_BITMASK             (BIT0|BIT1|BIT2|BIT3|BIT4|BIT5|BIT6)

//CONST_INT CP_GAME_ENUM_A_INDEX_SHIFT  0
CONST_INT   CP_VARIATION_INDEX_SHIFT    7

CONST_INT   CP_NULL_DATA                127




#if not USE_CLF_DLC
#if not USE_NRM_DLC

PROC Fill_CompletionPercentage_Settings (enumCompletionPercentageEntries whichEntry, enumGrouping whichGroup, FLOAT whichWeighting, 
                                            BOOL MarkedAsComplete, BOOL AssignedByScript, STRING whichLabel, 
                                            INT whichGameEnumIndex = CP_NULL_DATA, INT whichVariationIndex = CP_NULL_DATA,
                                            enumChoiceMiss whichType = STANDARD_MISSION,
                                            enumSCCluster whichCluster = NO_CLUSTER)
                                            
    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = whichGroup
    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting = whichWeighting
    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = MarkedAsComplete
    
    
    //May need to put this back in, so dummy check the parameter.
    //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Assigned_as_included_by_Script = AssignedByScript
    IF AssignedByScript = TRUE
    ENDIF



    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label = whichLabel  
    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_ChoiceMission = whichType   
    g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_SocialClubClusterDisplay = whichCluster 
    
    // Pack variation and game enum index into one INT to save space.
    // Enum bits:0-6, Variation bits:7-13.
    IF whichGameEnumIndex <= CP_NULL_DATA
        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData = whichGameEnumIndex
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Game enum index too high to be packed into bits 0-6 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
    IF whichVariationIndex <= CP_NULL_DATA
        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData |= SHIFT_LEFT(whichVariationIndex, CP_VARIATION_INDEX_SHIFT)
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Variation index too high to be packed into bits 7-13 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
ENDPROC 
#ENDIF
#ENDIF

#if USE_CLF_DLC
PROC Fill_CompletionPercentage_Settings_CLF (enumCompletionPercentageEntries whichEntry, enumGrouping whichGroup, FLOAT whichWeighting, 
                                            BOOL MarkedAsComplete, BOOL AssignedByScript, STRING whichLabel, 
                                            INT whichGameEnumIndex = CP_NULL_DATA, INT whichVariationIndex = CP_NULL_DATA,
                                            enumChoiceMiss whichType = STANDARD_MISSION,
                                            enumSCCluster whichCluster = NO_CLUSTER)
                                            
    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = whichGroup
    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting = whichWeighting
    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = MarkedAsComplete
    
    
    //May need to put this back in, so dummy check the parameter.
    //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Assigned_as_included_by_Script = AssignedByScript
    IF AssignedByScript = TRUE
    ENDIF



    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label = whichLabel  
    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_ChoiceMission = whichType   
    g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_SocialClubClusterDisplay = whichCluster 
    
    // Pack variation and game enum index into one INT to save space.
    // Enum bits:0-6, Variation bits:7-13.
    IF whichGameEnumIndex <= CP_NULL_DATA
        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData = whichGameEnumIndex
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Game enum index too high to be packed into bits 0-6 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
    IF whichVariationIndex <= CP_NULL_DATA
        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData |= SHIFT_LEFT(whichVariationIndex, CP_VARIATION_INDEX_SHIFT)
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Variation index too high to be packed into bits 7-13 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
ENDPROC 
#ENDIF
#if USE_NRM_DLC
PROC Fill_CompletionPercentage_Settings_NRM (enumCompletionPercentageEntries whichEntry, enumGrouping whichGroup, FLOAT whichWeighting, 
                                            BOOL MarkedAsComplete, BOOL AssignedByScript, STRING whichLabel, 
                                            INT whichGameEnumIndex = CP_NULL_DATA, INT whichVariationIndex = CP_NULL_DATA,
                                            enumChoiceMiss whichType = STANDARD_MISSION,
                                            enumSCCluster whichCluster = NO_CLUSTER)
                                            
    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = whichGroup
    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting = whichWeighting
    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = MarkedAsComplete
    
    
    //May need to put this back in, so dummy check the parameter.
    //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Assigned_as_included_by_Script = AssignedByScript
    IF AssignedByScript = TRUE
    ENDIF



    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label = whichLabel  
    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_ChoiceMission = whichType   
    g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_SocialClubClusterDisplay = whichCluster 
    
    // Pack variation and game enum index into one INT to save space.
    // Enum bits:0-6, Variation bits:7-13.
    IF whichGameEnumIndex <= CP_NULL_DATA
        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData = whichGameEnumIndex
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Game enum index too high to be packed into bits 0-6 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
    IF whichVariationIndex <= CP_NULL_DATA
        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData |= SHIFT_LEFT(whichVariationIndex, CP_VARIATION_INDEX_SHIFT)
    ELSE
        CERRORLN(DEBUG_FLOW, "Fill_CompletionPercentage_Settings: Variation index too high to be packed into bits 7-13 of INT for CP_ENUM #", ENUM_TO_INT(whichEntry),".")
    ENDIF
    
ENDPROC 
#ENDIF
#if USE_CLF_DLC
 PROC Initialise_CompletionPercentage_Tracking_Before_Savegame_Restoration_CLF()

    g_savedGlobalsClifford.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0
    //need to register stat here for this to ensure it is set to zero at the start of a completely fresh game.
        
    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Initialising Resultant_CompletionPercentage value to 0")
        PRINTNL()
    #endif

     
    //Grouping divisor. Important.
    g_f_Expected_Number_in_Missions_Group = 22.0
    FLOAT  f_CP_GROUP_MISSIONS_Weighting = (100 / g_f_Expected_Number_in_Missions_Group) //Combined weighting divided by number of entries within this particular group.
                                                         //This segment will work out the number of entries, but I'm leaving this slightly manual to force me to check this regularly.
                                                         //A check total is dumped to the debug console and that should match the int equivalent of the divisor. Alter the expected number divisor 
                                                         //to match the the check total if they are different.


    Fill_CompletionPercentage_Settings_CLF (CP_CLFT , CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFT", ENUM_TO_INT(SP_MISSION_CLF_TRAIN)) 
    Fill_CompletionPercentage_Settings_CLF (CP_CLFF, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFF", ENUM_TO_INT(SP_MISSION_CLF_FIN)) 
	
    Fill_CompletionPercentage_Settings_CLF (CP_CLFCT, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCT", ENUM_TO_INT(SP_MISSION_CLF_IAA_TRA))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCL, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCL", ENUM_TO_INT(SP_MISSION_CLF_IAA_LIE))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCJ, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCJ", ENUM_TO_INT(SP_MISSION_CLF_IAA_JET))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCH, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCH", ENUM_TO_INT(SP_MISSION_CLF_IAA_HEL))										
    Fill_CompletionPercentage_Settings_CLF (CP_CLFCD, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCD", ENUM_TO_INT(SP_MISSION_CLF_IAA_DRO)) 
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCR, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCR", ENUM_TO_INT(SP_MISSION_CLF_IAA_RTS))										
	
	Fill_CompletionPercentage_Settings_CLF (CP_CLFKP, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFKP", ENUM_TO_INT(SP_MISSION_CLF_KOR_PRO)) 
    Fill_CompletionPercentage_Settings_CLF (CP_CLFKR, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFKR", ENUM_TO_INT(SP_MISSION_CLF_KOR_RES))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFKS, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFKS", ENUM_TO_INT(SP_MISSION_CLF_KOR_SUB)) 
 	Fill_CompletionPercentage_Settings_CLF (CP_CLFK4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFK4", ENUM_TO_INT(SP_MISSION_CLF_KOR_SAT)) 									
    Fill_CompletionPercentage_Settings_CLF (CP_CLFK5, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFK5", ENUM_TO_INT(SP_MISSION_CLF_KOR_5)) 
    
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRP, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRP", ENUM_TO_INT(SP_MISSION_CLF_RUS_PLA))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRC, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRC", ENUM_TO_INT(SP_MISSION_CLF_RUS_CAR)) 
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRV, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRV", ENUM_TO_INT(SP_MISSION_CLF_RUS_VAS))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRS, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRS", ENUM_TO_INT(SP_MISSION_CLF_RUS_SAT)) 
    Fill_CompletionPercentage_Settings_CLF (CP_CLFRJ, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRJ", ENUM_TO_INT(SP_MISSION_CLF_RUS_JET))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCLK, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCLK", ENUM_TO_INT(SP_MISSION_CLF_RUS_CLK))
    
	Fill_CompletionPercentage_Settings_CLF (CP_CLFA1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFA1", ENUM_TO_INT(SP_MISSION_CLF_ARA_1))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFAD, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFAD", ENUM_TO_INT(SP_MISSION_CLF_ARA_DEF))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFAT, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFAT", ENUM_TO_INT(SP_MISSION_CLF_ARA_FAKE))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFAF, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFAF", ENUM_TO_INT(SP_MISSION_CLF_ARA_FAKE))
	
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCSS, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSS", ENUM_TO_INT(SP_MISSION_CLF_CAS_SET))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFCSP1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSP1", ENUM_TO_INT(SP_MISSION_CLF_CAS_PR1))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFCSP2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSP2", ENUM_TO_INT(SP_MISSION_CLF_CAS_PR2))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFCSP3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSP3", ENUM_TO_INT(SP_MISSION_CLF_CAS_PR3))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFCSH, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSH", ENUM_TO_INT(SP_MISSION_CLF_CAS_HEI))
	
	Fill_CompletionPercentage_Settings_CLF (CP_CLFMD, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFMD", ENUM_TO_INT(SP_MISSION_CLF_MIL_DAM))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFMP, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFMP", ENUM_TO_INT(SP_MISSION_CLF_MIL_PLA))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFMR, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFMR", ENUM_TO_INT(SP_MISSION_CLF_MIL_RKT))
    
	Fill_CompletionPercentage_Settings_CLF (CP_CLFASSP, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSP", ENUM_TO_INT( SP_MISSION_CLF_ASS_POL))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSR, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSR", ENUM_TO_INT(SP_MISSION_CLF_ASS_RET))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSC, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSC", ENUM_TO_INT(SP_MISSION_CLF_ASS_CAB))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSG, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSG", ENUM_TO_INT( SP_MISSION_CLF_ASS_GEN))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSS, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSS", ENUM_TO_INT( SP_MISSION_CLF_ASS_SUB))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSH, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSH", ENUM_TO_INT(SP_MISSION_CLF_ASS_HEL))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSV, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSV", ENUM_TO_INT(SP_MISSION_CLF_ASS_VIN))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSN, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSN", ENUM_TO_INT(SP_MISSION_CLF_ASS_HNT))
    Fill_CompletionPercentage_Settings_CLF (CP_CLFASSY, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFASSY", ENUM_TO_INT( SP_MISSION_CLF_ASS_SKY))
	
	Fill_CompletionPercentage_Settings_CLF (CP_CLFJET1,  CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSS", ENUM_TO_INT(SP_MISSION_CLF_JET_1))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFJET2,  CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSS", ENUM_TO_INT(SP_MISSION_CLF_JET_2))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFJET3,  CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSS", ENUM_TO_INT(SP_MISSION_CLF_JET_3))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFJETREP,CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFCSS", ENUM_TO_INT(SP_MISSION_CLF_JET_REP))
   				 
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAL1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAL1", ENUM_TO_INT(SP_MISSION_CLF_RC_ALEX_GRND ))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAL2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAL2", ENUM_TO_INT(SP_MISSION_CLF_RC_ALEX_AIR))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAL3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAL3", ENUM_TO_INT(SP_MISSION_CLF_RC_ALEX_UNDW))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAL4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAL4", ENUM_TO_INT(SP_MISSION_CLF_RC_ALEX_RWRD))
   		
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCME1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCME1", ENUM_TO_INT(SP_MISSION_CLF_RC_MEL_MONT))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCME2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCME2", ENUM_TO_INT(SP_MISSION_CLF_RC_MEL_AIRP))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCME3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCME3", ENUM_TO_INT(SP_MISSION_CLF_RC_MEL_WING))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCME4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCME4", ENUM_TO_INT(SP_MISSION_CLF_RC_MEL_DIVE))
   	   		
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAG1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAG1", ENUM_TO_INT(SP_MISSION_CLF_RC_AGN_BODY))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAG2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAG2", ENUM_TO_INT(SP_MISSION_CLF_RC_AGN_RCPT))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCAG3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCAG3", ENUM_TO_INT(SP_MISSION_CLF_RC_AGN_ESCP))
   	   		
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCCL1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCCL1", ENUM_TO_INT(SP_MISSION_CLF_RC_CLA_HACK))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCCL2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCCL2", ENUM_TO_INT(SP_MISSION_CLF_RC_CLA_DRUG))
	Fill_CompletionPercentage_Settings_CLF (CP_CLFRCCL3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CLFRCCL3", ENUM_TO_INT(SP_MISSION_CLF_RC_CLA_FIN))
	

    //Calculate and store how many of each grouping we have, so that we can assign a rounded grouping value if need be...
                                             
    INT tempIndex = 0
    FLOAT tempWeightingsTotal = 0

    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
    
        IF NOT (g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_NO_GROUP)
            tempWeightingsTotal = tempWeightingsTotal + g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting
        ENDIF

        SWITCH (g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)

            CASE CP_GROUP_MISSIONS
               g_Group_Num_of_Missions ++  
            BREAK                
            
            CASE CP_GROUP_MINIGAMES
                g_Group_Num_of_Minigames ++
            BREAK

            CASE CP_GROUP_ODDJOBS
                g_Group_Num_of_Oddjobs ++
            BREAK

            CASE CP_GROUP_RANDOMCHARS
                g_Group_Num_of_RANDOMCHARS ++
            BREAK

            CASE CP_GROUP_RANDOMEVENTS
                g_Group_Num_of_RandomEvents ++
            BREAK

            CASE CP_GROUP_MISCELLANEOUS
                g_Group_Num_of_Miscellaneous ++
            BREAK

            CASE CP_GROUP_FRIENDS
                g_Group_Num_of_Friends ++
            BREAK


            DEFAULT  
                #if IS_DEBUG_BUILD 
                    PRINTNL()
                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position ")
                    PRINTINT (tempIndex)
                    PRINTNL()
                #endif
            BREAK
        ENDSWITCH
        tempIndex ++
    ENDWHILE       

    #if IS_DEBUG_BUILD 
        PRINTNL()        
        PRINTSTRING("CP_PUBLIC - Check total of all individual weightings without any group rounding is: ")
        PRINTFLOAT(tempWeightingsTotal)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Missions_Group is ")
        PRINTINT (g_Group_Num_of_Missions)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Minigames_Group is ")
        PRINTINT (g_Group_Num_of_Minigames)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Oddjobs_Group is ")
        PRINTINT (g_Group_Num_of_Oddjobs)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomChars_Group is ")
        PRINTINT (g_Group_Num_of_RandomChars)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomEvents_Group is ")
        PRINTINT (g_Group_Num_of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Miscellanous_Group is ")
        PRINTINT (g_Group_Num_of_Miscellaneous)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Friends_Group is ")
        PRINTINT (g_Group_Num_of_Friends)
		PRINTNL()

        FLOAT tempTargetTotalCheck
        tempTargetTotalCheck = ( 100 )  

        IF tempTargetTotalCheck > 100.0
        OR tempTargetTotalCheck < 100.0
            SCRIPT_ASSERT ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor.")
            PRINTNL()
            PRINTSTRING ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor. Target is ")
            PRINTFLOAT (tempTargetTotalCheck)
        ENDIF
    #endif
ENDPROC
#ENDIF
#if USE_NRM_DLC
PROC Initialise_CompletionPercentage_Tracking_Before_Savegame_Restoration_NRM()

    g_savedGlobalsnorman.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0
    //need to register stat here for this to ensure it is set to zero at the start of a completely fresh game.
        
    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Initialising Resultant_CompletionPercentage value to 0")
        PRINTNL()
    #endif

     
    //Grouping divisor. Important.
    g_f_Expected_Number_in_Missions_Group = 22.0
    FLOAT  f_CP_GROUP_MISSIONS_Weighting = (100 / g_f_Expected_Number_in_Missions_Group) //Combined weighting divided by number of entries within this particular group.
                                                         //This segment will work out the number of entries, but I'm leaving this slightly manual to force me to check this regularly.
                                                         //A check total is dumped to the debug console and that should match the int equivalent of the divisor. Alter the expected number divisor 
                                                         //to match the the check total if they are different.


    Fill_CompletionPercentage_Settings_NRM (CP_ZSTRT , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZSTRT",  	ENUM_TO_INT( SP_MISSION_NRM_SUR_START	)) 
    Fill_CompletionPercentage_Settings_NRM (CP_ZAMA  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZAMA" , 	ENUM_TO_INT( SP_MISSION_NRM_SUR_AMANDA	))	 
	Fill_CompletionPercentage_Settings_NRM (CP_ZTRA  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZTRA" , 	ENUM_TO_INT( SP_MISSION_NRM_SUR_TRACEY	)) 
    Fill_CompletionPercentage_Settings_NRM (CP_ZMIKE , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZMIKE",		ENUM_TO_INT( SP_MISSION_NRM_SUR_MICHAEL	))
	Fill_CompletionPercentage_Settings_NRM (CP_ZHOME , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZHOME",  	ENUM_TO_INT( SP_MISSION_NRM_SUR_HOME	)) 
    Fill_CompletionPercentage_Settings_NRM (CP_ZJIM  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZJIM" , 	ENUM_TO_INT( SP_MISSION_NRM_SUR_JIMMY	))	 
	Fill_CompletionPercentage_Settings_NRM (CP_ZPAR  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZPAR" , 	ENUM_TO_INT( SP_MISSION_NRM_SUR_PARTY	)) 
    Fill_CompletionPercentage_Settings_NRM (CP_ZCURE , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZCURE",		ENUM_TO_INT( SP_MISSION_NRM_SUR_CURE	))
	Fill_CompletionPercentage_Settings_NRM (CP_ZRENG , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRENG", ENUM_TO_INT(SP_MISSION_NRM_RESCUE_ENG	)) 
    Fill_CompletionPercentage_Settings_NRM (CP_ZRMED , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRMED", ENUM_TO_INT(SP_MISSION_NRM_RESCUE_MED	))
	Fill_CompletionPercentage_Settings_NRM (CP_ZRGUN , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRGUN", ENUM_TO_INT(SP_MISSION_NRM_RESCUE_GUN	))
	Fill_CompletionPercentage_Settings_NRM (CP_ZFUEL , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZFUEL", ENUM_TO_INT(SP_MISSION_NRM_SUP_FUEL		))	
	Fill_CompletionPercentage_Settings_NRM (CP_ZAMMO , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZAMMO", ENUM_TO_INT(SP_MISSION_NRM_SUP_AMMO		))	
	Fill_CompletionPercentage_Settings_NRM (CP_ZMEDS , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZMEDS", ENUM_TO_INT(SP_MISSION_NRM_SUP_MEDS		))	
	Fill_CompletionPercentage_Settings_NRM (CP_ZFOOD ,	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZFOOD", ENUM_TO_INT(SP_MISSION_NRM_SUP_FOOD		))	
	Fill_CompletionPercentage_Settings_NRM (CP_ZRA  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRA"  , ENUM_TO_INT(SP_MISSION_NRM_RADIO_A		))
	Fill_CompletionPercentage_Settings_NRM (CP_ZRB  , 	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRB"  , ENUM_TO_INT(SP_MISSION_NRM_RADIO_B		))
	Fill_CompletionPercentage_Settings_NRM (CP_ZRC  ,	CP_GROUP_MISSIONS, 	f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ZRC"  , ENUM_TO_INT(SP_MISSION_NRM_RADIO_C		))
    //Calculate and store how many of each grouping we have, so that we can assign a rounded grouping value if need be...
                                             
    INT tempIndex = 0
    FLOAT tempWeightingsTotal = 0

    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
    
        IF NOT (g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_NO_GROUP)
            tempWeightingsTotal = tempWeightingsTotal + g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting
        ENDIF

        SWITCH (g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)

            CASE CP_GROUP_MISSIONS
               g_Group_Num_of_Missions ++  
            BREAK                
            
            CASE CP_GROUP_MINIGAMES
                g_Group_Num_of_Minigames ++
            BREAK

            CASE CP_GROUP_ODDJOBS
                g_Group_Num_of_Oddjobs ++
            BREAK

            CASE CP_GROUP_RANDOMCHARS
                g_Group_Num_of_RANDOMCHARS ++
            BREAK

            CASE CP_GROUP_RANDOMEVENTS
                g_Group_Num_of_RandomEvents ++
            BREAK

            CASE CP_GROUP_MISCELLANEOUS
                g_Group_Num_of_Miscellaneous ++
            BREAK

            CASE CP_GROUP_FRIENDS
                g_Group_Num_of_Friends ++
            BREAK


            DEFAULT  
                #if IS_DEBUG_BUILD 
                    PRINTNL()
                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position ")
                    PRINTINT (tempIndex)
                    PRINTNL()
                #endif
            BREAK
        ENDSWITCH
        tempIndex ++
    ENDWHILE       

    #if IS_DEBUG_BUILD 
        PRINTNL()        
        PRINTSTRING("CP_PUBLIC - Check total of all individual weightings without any group rounding is: ")
        PRINTFLOAT(tempWeightingsTotal)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Missions_Group is ")
        PRINTINT (g_Group_Num_of_Missions)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Minigames_Group is ")
        PRINTINT (g_Group_Num_of_Minigames)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Oddjobs_Group is ")
        PRINTINT (g_Group_Num_of_Oddjobs)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomChars_Group is ")
        PRINTINT (g_Group_Num_of_RandomChars)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomEvents_Group is ")
        PRINTINT (g_Group_Num_of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Miscellanous_Group is ")
        PRINTINT (g_Group_Num_of_Miscellaneous)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Friends_Group is ")
        PRINTINT (g_Group_Num_of_Friends)
		PRINTNL()

        FLOAT tempTargetTotalCheck
        tempTargetTotalCheck = ( 100 )  

        IF tempTargetTotalCheck > 100.0
        OR tempTargetTotalCheck < 100.0
            SCRIPT_ASSERT ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor.")
            PRINTNL()
            PRINTSTRING ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor. Target is ")
            PRINTFLOAT (tempTargetTotalCheck)
        ENDIF
    #endif
ENDPROC
#ENDIF






#IF NOT USE_SP_DLC

 PROC Initialise_CompletionPercentage_Tracking_Before_Savegame_Restoration()

    g_savedGlobals.sCompletionPercentageData.g_Resultant_CompletionPercentage = 0
    //need to register stat here for this to ensure it is set to zero at the start of a completely fresh game.
        
    #if IS_DEBUG_BUILD
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Initialising Resultant_CompletionPercentage value to 0")
        PRINTNL()
    #endif


     
    //Grouping divisor. Important.
    g_f_Expected_Number_in_Missions_Group = 69.0
    FLOAT  f_CP_GROUP_MISSIONS_Weighting = (g_c_Missions_CombinedWeighting / g_f_Expected_Number_in_Missions_Group) //Combined weighting divided by number of entries within this particular group.
                                                         //This segment will work out the number of entries, but I'm leaving this slightly manual to force me to check this regularly.
                                                         //A check total is dumped to the debug console and that should match the int equivalent of the divisor. Alter the expected number divisor 
                                                         //to match the the check total if they are different.


    Fill_CompletionPercentage_Settings (CP_ARM1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ARM1", ENUM_TO_INT(SP_MISSION_ARMENIAN_1)) 
    Fill_CompletionPercentage_Settings (CP_ARM2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ARM2", ENUM_TO_INT(SP_MISSION_ARMENIAN_2)) 
    Fill_CompletionPercentage_Settings (CP_ARM3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_ARM3", ENUM_TO_INT(SP_MISSION_ARMENIAN_3))
    
//    //These have been removed - see saved Ben email  from 05.10.12 in Important emails folder.
//    Fill_CompletionPercentage_Settings (CP_BAND1, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_BAN1")
//    Fill_CompletionPercentage_Settings (CP_BAND2, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_BAN2")



    //New order for the Car Steal strand
    Fill_CompletionPercentage_Settings (CP_CAR1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CAR1", ENUM_TO_INT(SP_MISSION_CARSTEAL_1)) //I fought the law..
    Fill_CompletionPercentage_Settings (CP_CAR2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CAR2", ENUM_TO_INT(SP_MISSION_CARSTEAL_2)) //Eye in the sky
    Fill_CompletionPercentage_Settings (CP_CAR3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CAR3", ENUM_TO_INT(SP_MISSION_CARSTEAL_3)) //agent
    Fill_CompletionPercentage_Settings (CP_CAR4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CAR4", ENUM_TO_INT(SP_MISSION_CARSTEAL_4)) //Pack Man
    
//    //Up for removal...
//    Fill_CompletionPercentage_Settings (CP_CAR5, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CAR5") 



    Fill_CompletionPercentage_Settings (CP_CHN1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CHN1", ENUM_TO_INT(SP_MISSION_CHINESE_1)) 
    Fill_CompletionPercentage_Settings (CP_CHN2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_CHN2", ENUM_TO_INT(SP_MISSION_CHINESE_2))



    Fill_CompletionPercentage_Settings (CP_EXL1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_EXL1", ENUM_TO_INT(SP_MISSION_EXILE_1)) 
    Fill_CompletionPercentage_Settings (CP_EXL2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_EXL2", ENUM_TO_INT(SP_MISSION_EXILE_2))
    Fill_CompletionPercentage_Settings (CP_EXL3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_EXL3", ENUM_TO_INT(SP_MISSION_EXILE_3))

    
    
    Fill_CompletionPercentage_Settings (CP_FAM1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM1", ENUM_TO_INT(SP_MISSION_FAMILY_1))
    Fill_CompletionPercentage_Settings (CP_FAM2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM2", ENUM_TO_INT(SP_MISSION_FAMILY_2))
    Fill_CompletionPercentage_Settings (CP_FAM3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM3", ENUM_TO_INT(SP_MISSION_FAMILY_3))
    Fill_CompletionPercentage_Settings (CP_FAM4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM4", ENUM_TO_INT(SP_MISSION_FAMILY_4))
    Fill_CompletionPercentage_Settings (CP_FAM5, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM5", ENUM_TO_INT(SP_MISSION_FAMILY_5))

    //Reunite Family has been merged into one mission. A and B were merged into CP_FAM6
    Fill_CompletionPercentage_Settings (CP_FAM6, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FAM6", ENUM_TO_INT(SP_MISSION_FAMILY_6))



    Fill_CompletionPercentage_Settings (CP_FBI1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FIB1", ENUM_TO_INT(SP_MISSION_FBI_1))
    Fill_CompletionPercentage_Settings (CP_FBI2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FIB2", ENUM_TO_INT(SP_MISSION_FBI_2))
    Fill_CompletionPercentage_Settings (CP_FBI3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FIB3", ENUM_TO_INT(SP_MISSION_FBI_3))
    Fill_CompletionPercentage_Settings (CP_FBI4_P1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FB4P1", ENUM_TO_INT(SP_MISSION_FBI_4_PREP_1)) //Steal garbage truck.
    Fill_CompletionPercentage_Settings (CP_FBI4_P2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FB4P2", ENUM_TO_INT(SP_MISSION_FBI_4_PREP_2))

    Fill_CompletionPercentage_Settings (CP_FBI4_P3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FB4P3", ENUM_TO_INT(SP_MISSION_FBI_4_PREP_3)) 
    
    Fill_CompletionPercentage_Settings (CP_FBI4_P4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FB4P4", ENUM_TO_INT(SP_MISSION_FBI_4_PREP_4))
    Fill_CompletionPercentage_Settings (CP_FBI4_P5, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FB4P5", ENUM_TO_INT(SP_MISSION_FBI_4_PREP_5))
    Fill_CompletionPercentage_Settings (CP_FBI4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FIB4", ENUM_TO_INT(SP_MISSION_FBI_4)) //Main FBI heist.
    Fill_CompletionPercentage_Settings (CP_FBI5, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FIB5", ENUM_TO_INT(SP_MISSION_FBI_5))



//    //Up for removal. Monkey Business now consolidated into one mission. #891989
//    Fill_CompletionPercentage_Settings (CP_FBI5A, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FBI5A") //Deliberate NO_GROUP. Only completion of 5b counts towards completeion.
//    Fill_CompletionPercentage_Settings (CP_FBI5B, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FBI5B") //



    Fill_CompletionPercentage_Settings (CP_FRA0, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FRA0", ENUM_TO_INT(SP_MISSION_FRANKLIN_0))
    Fill_CompletionPercentage_Settings (CP_FRA1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FRA1", ENUM_TO_INT(SP_MISSION_FRANKLIN_1))
    Fill_CompletionPercentage_Settings (CP_FRA2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FRA2", ENUM_TO_INT(SP_MISSION_FRANKLIN_2))

   
   
    Fill_CompletionPercentage_Settings (CP_LM1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_LM1", ENUM_TO_INT(SP_MISSION_LAMAR))  

   
   
    Fill_CompletionPercentage_Settings (CP_LS1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_LS1", ENUM_TO_INT(SP_MISSION_LESTER_1))    
 



    Fill_CompletionPercentage_Settings (CP_MIC1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_MIC1", ENUM_TO_INT(SP_MISSION_MICHAEL_1)) 
    Fill_CompletionPercentage_Settings (CP_MIC2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_MIC2", ENUM_TO_INT(SP_MISSION_MICHAEL_2))
    Fill_CompletionPercentage_Settings (CP_MIC3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_MIC3", ENUM_TO_INT(SP_MISSION_MICHAEL_3))  //
    Fill_CompletionPercentage_Settings (CP_MIC4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_MIC4", ENUM_TO_INT(SP_MISSION_MICHAEL_4))  //Replaced Solomon 5



    Fill_CompletionPercentage_Settings (CP_PRO1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_PRO1", ENUM_TO_INT(SP_MISSION_PROLOGUE)) //Prologue



    Fill_CompletionPercentage_Settings (CP_MAR1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_MAR1", ENUM_TO_INT(SP_MISSION_MARTIN_1)) //Martin 1 Vinewood Babylon



    Fill_CompletionPercentage_Settings (CP_SOL1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_SOL1", ENUM_TO_INT(SP_MISSION_SOLOMON_1)) //Placeholder
    Fill_CompletionPercentage_Settings (CP_SOL2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_SOL2", ENUM_TO_INT(SP_MISSION_SOLOMON_2)) //Placeholder
    Fill_CompletionPercentage_Settings (CP_SOL3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_SOL3", ENUM_TO_INT(SP_MISSION_SOLOMON_3)) //Blow Back

//    //Up for removal. See bug 896600
//    Fill_CompletionPercentage_Settings (CP_SOL4, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SOL4") //Harrier
//
//    //Up for removal. Has become CP_MIC4. See bug 895833
//    Fill_CompletionPercentage_Settings (CP_SOL5, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SOL5") //Movie Premier


                                                                 
    Fill_CompletionPercentage_Settings (CP_TRV1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_TRV1", ENUM_TO_INT(SP_MISSION_TREVOR_1))
    Fill_CompletionPercentage_Settings (CP_TRV2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_TRV2", ENUM_TO_INT(SP_MISSION_TREVOR_2))
    Fill_CompletionPercentage_Settings (CP_TRV3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_TRV3", ENUM_TO_INT(SP_MISSION_TREVOR_3))
    Fill_CompletionPercentage_Settings (CP_TRV4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_TRV4", ENUM_TO_INT(SP_MISSION_TREVOR_4))

//    //Up for removal
//    Fill_CompletionPercentage_Settings (CP_TRV5, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_TRV5")
    


    //Agency Heist.
    Fill_CompletionPercentage_Settings (CP_FH1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_AH1", ENUM_TO_INT(SP_HEIST_AGENCY_1))
    Fill_CompletionPercentage_Settings (CP_FH2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_AH2", ENUM_TO_INT(SP_HEIST_AGENCY_2))
    
    //Removed by #1316328
//    Fill_CompletionPercentage_Settings (CP_FH3I, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_AH3i", ENUM_TO_INT(SP_HEIST_AGENCY_2))

    
    Fill_CompletionPercentage_Settings (CP_FH3_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_AH3a", ENUM_TO_INT(SP_HEIST_AGENCY_3A), CP_NULL_DATA, CHOICE_MISSION)
    
//    //Steve this is only done in one branch of the heist so can be completely skipped. Probably needs removing. -BenR.
//    //Now "no grouped", so removed from tracking. See bug 956277.
//    Fill_CompletionPercentage_Settings (CP_AHP1, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_AHP1", ENUM_TO_INT(SP_HEIST_AGENCY_PREP_1)) //Steal a firetruck



    //Big Score Heist
    Fill_CompletionPercentage_Settings (CP_BHB, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "FHPB", ENUM_TO_INT(SP_HEIST_FINALE_2_INTRO))
    Fill_CompletionPercentage_Settings (CP_BH1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FH1", ENUM_TO_INT(SP_HEIST_FINALE_1))
    Fill_CompletionPercentage_Settings (CP_BH3_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FH2a", ENUM_TO_INT(SP_HEIST_FINALE_2A), CP_NULL_DATA, CHOICE_MISSION) //



    //Jewel Heist
    Fill_CompletionPercentage_Settings (CP_JH1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_JH1", ENUM_TO_INT(SP_HEIST_JEWELRY_1))

    //These are not included, if the player chooses the B strand, they can be bypassed completely.
    //Fill_CompletionPercentage_Settings (CP_JHP1A, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_JHP1A")  //This relates to the 2A mission choice.
    //Fill_CompletionPercentage_Settings (CP_JHP2A, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_JHP2A")  //This also relates to the 2A mission choice.

    //Up for removal. This has been replaced with the two prep missions..  
//    Fill_CompletionPercentage_Settings (CP_JH1B, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_JH1B")
    

    //#1274040
    Fill_CompletionPercentage_Settings (CP_JH_PREP_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SC_JWL_P_CHOICE", ENUM_TO_INT(SP_HEIST_JEWELRY_PREP_1A), CP_NULL_DATA, CHOICE_MISSION) //
    





    Fill_CompletionPercentage_Settings (CP_JH2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_JH2A", ENUM_TO_INT(SP_HEIST_JEWELRY_2)) //



    //The Paleto Score Heist / Rural Bank heist    
    Fill_CompletionPercentage_Settings (CP_RH1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_RH1", ENUM_TO_INT(SP_HEIST_RURAL_1))
    Fill_CompletionPercentage_Settings (CP_RBHP1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_RHP1", ENUM_TO_INT(SP_HEIST_RURAL_PREP_1)) //Steal Military hardware.
    Fill_CompletionPercentage_Settings (CP_RH2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_RH2", ENUM_TO_INT(SP_HEIST_RURAL_2)) //



    //The Port of Los Santos Heist
    
//    //TODO: Needs removing. Mission gone. #1316328
//    Fill_CompletionPercentage_Settings (CP_DHB, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "DHPB", ENUM_TO_INT(SP_HEIST_DOCKS_1)) 
    
    Fill_CompletionPercentage_Settings (CP_DHP1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "DHP1", ENUM_TO_INT(SP_HEIST_DOCKS_PREP_1))
    Fill_CompletionPercentage_Settings (CP_DH1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_DH1", ENUM_TO_INT(SP_HEIST_DOCKS_1))
    Fill_CompletionPercentage_Settings (CP_DH2_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_DH2A", ENUM_TO_INT(SP_HEIST_DOCKS_2A), CP_NULL_DATA, CHOICE_MISSION) //



//    //Up for removal The Sharmoota Job AKA Nice House  See bug 893131
//    Fill_CompletionPercentage_Settings (CP_NH1A, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_NH1a")
//    Fill_CompletionPercentage_Settings (CP_NH1B, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_NH1b")
//    Fill_CompletionPercentage_Settings (CP_NH2_CHOICE, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_NH2", CP_NULL_DATA, CP_NULL_DATA, CHOICE_MISSION) //





    //Game Finale!

    //Prep Choice 1 mission... 
    Fill_CompletionPercentage_Settings (CP_FINALE_PREP_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SC_FINP1_CHOICE", ENUM_TO_INT(SP_HEIST_FINALE_PREP_A), CP_NULL_DATA, CHOICE_MISSION)

    //Prep Choice 2 mission... 
    Fill_CompletionPercentage_Settings (CP_FINALE_PREP_CHOICE2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SC_FINP2_CHOICE", ENUM_TO_INT(SP_HEIST_FINALE_PREP_C1), CP_NULL_DATA, CHOICE_MISSION)
    


    //Now removed.  #1169507 Finale choice back in!
    Fill_CompletionPercentage_Settings (CP_FINALE, CP_GROUP_NO_GROUP, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "M_FINC1", ENUM_TO_INT(SP_MISSION_FINALE_C1))


    //Now back in! #1169507
    Fill_CompletionPercentage_Settings (CP_FINALE_CHOICE, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "SC_FIN_K_CHOICE", ENUM_TO_INT(SP_MISSION_FINALE_A), CP_NULL_DATA, CHOICE_MISSION) //



    //Assassination Oddjobs - Text label order is weird due to changes, they are not mismatched. These have been moved from oddjobs to story missions! See 1534381
    Fill_CompletionPercentage_Settings (CP_OJ_ASS1, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "ASS1", ENUM_TO_INT(SP_MISSION_ASSASSIN_1)) //Hotel
    Fill_CompletionPercentage_Settings (CP_OJ_ASS2, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "ASS2", ENUM_TO_INT(SP_MISSION_ASSASSIN_2)) //Multi Target
    Fill_CompletionPercentage_Settings (CP_OJ_ASS3, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "ASS3", ENUM_TO_INT(SP_MISSION_ASSASSIN_3)) //Vice
    Fill_CompletionPercentage_Settings (CP_OJ_ASS4, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "ASS4", ENUM_TO_INT(SP_MISSION_ASSASSIN_4)) //bus station
    Fill_CompletionPercentage_Settings (CP_OJ_ASS5, CP_GROUP_MISSIONS, f_CP_GROUP_MISSIONS_Weighting, FALSE, FALSE, "ASS5", ENUM_TO_INT(SP_MISSION_ASSASSIN_5)) //construction














    //Random Characters
    g_f_Expected_Number_in_RandomChars_Group = 20.0
    FLOAT f_CP_GROUP_RANDOMCHARS_Weighting = (g_c_RandomChars_CombinedWeighting / g_f_Expected_Number_in_RandomChars_Group)

    //Omega 1 and 2
    Fill_CompletionPercentage_Settings (CP_RAND_C_OMG1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_OME1", ENUM_TO_INT(RC_OMEGA_1))
    Fill_CompletionPercentage_Settings (CP_RAND_C_OMG2, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_OME2", ENUM_TO_INT(RC_OMEGA_2))

    //Dreyfuss 1
    Fill_CompletionPercentage_Settings (CP_RAND_C_DREY1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_DRE1", ENUM_TO_INT(RC_DREYFUSS_1))


    //Barry 1, 2, 3 and 4. Only 3 and 4 are playable by all characters.
    Fill_CompletionPercentage_Settings (CP_RAND_C_BAR1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_BA1", ENUM_TO_INT(RC_BARRY_1))     // 1
    Fill_CompletionPercentage_Settings (CP_RAND_C_BAR2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_BA2", ENUM_TO_INT(RC_BARRY_2))     // 2
    Fill_CompletionPercentage_Settings (CP_RAND_C_BAR3, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_BA3", ENUM_TO_INT(RC_BARRY_3))     // 3
    Fill_CompletionPercentage_Settings (CP_RAND_C_BAR4, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_BA4", ENUM_TO_INT(RC_BARRY_4))     // 4

    //Epsilonism 1 to 8
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS1", ENUM_TO_INT(RC_EPSILON_1))    // 5
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS2", ENUM_TO_INT(RC_EPSILON_2))    // 6
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS3, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS3", ENUM_TO_INT(RC_EPSILON_3))    // 7
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS4, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS4", ENUM_TO_INT(RC_EPSILON_4))    // 8
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS5, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS5", ENUM_TO_INT(RC_EPSILON_5))    // 9
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS6, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS6", ENUM_TO_INT(RC_EPSILON_6))    // 10
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS7, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS7", ENUM_TO_INT(RC_EPSILON_7))    // 11
    Fill_CompletionPercentage_Settings (CP_RAND_C_EPS8, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EPS8", ENUM_TO_INT(RC_EPSILON_8))    // 12

    //Extreme 1 to 4
    Fill_CompletionPercentage_Settings (CP_RAND_C_EXT1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EXT1", ENUM_TO_INT(RC_EXTREME_1))    // 13
    Fill_CompletionPercentage_Settings (CP_RAND_C_EXT2, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EXT2", ENUM_TO_INT(RC_EXTREME_2))    // 14
    Fill_CompletionPercentage_Settings (CP_RAND_C_EXT3, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EXT3", ENUM_TO_INT(RC_EXTREME_3))    // 15
    Fill_CompletionPercentage_Settings (CP_RAND_C_EXT4, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_EXT4", ENUM_TO_INT(RC_EXTREME_4))    // 16

    //Fanatic 1, 2, 3
    Fill_CompletionPercentage_Settings (CP_RAND_C_FAN1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_FAN1", ENUM_TO_INT(RC_FANATIC_1))    // 17
    Fill_CompletionPercentage_Settings (CP_RAND_C_FAN2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_FAN2", ENUM_TO_INT(RC_FANATIC_2))    // 18
    Fill_CompletionPercentage_Settings (CP_RAND_C_FAN3, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_FAN3", ENUM_TO_INT(RC_FANATIC_3))    // 19

    //Hunting 1,2 - Used to be an oddjob
    Fill_CompletionPercentage_Settings (CP_OJ_HUN1,     CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_HUN1", ENUM_TO_INT(RC_HUNTING_1))   // 20
    Fill_CompletionPercentage_Settings (CP_OJ_HUN2,     CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_HUN2", ENUM_TO_INT(RC_HUNTING_2))   // 21

    //Josh 1 to 4
    Fill_CompletionPercentage_Settings (CP_RAND_C_JOS1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_JOS1", ENUM_TO_INT(RC_JOSH_1))       // 22
    Fill_CompletionPercentage_Settings (CP_RAND_C_JOS2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_JOS2", ENUM_TO_INT(RC_JOSH_2))       // 23
    Fill_CompletionPercentage_Settings (CP_RAND_C_JOS3, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_JOS3", ENUM_TO_INT(RC_JOSH_3))       // 24
    Fill_CompletionPercentage_Settings (CP_RAND_C_JOS4, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_JOS4", ENUM_TO_INT(RC_JOSH_4))       // 25
        
    //Minute Man 1,2,3
    Fill_CompletionPercentage_Settings (CP_RAND_C_MIN1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MIN1", ENUM_TO_INT(RC_MINUTE_1))    // 26
    Fill_CompletionPercentage_Settings (CP_RAND_C_MIN2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MIN2", ENUM_TO_INT(RC_MINUTE_2))    // 27
    Fill_CompletionPercentage_Settings (CP_RAND_C_MIN3, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MIN3", ENUM_TO_INT(RC_MINUTE_3))    // 28

    //Nigel 1, 1A to 1E, Nigel 2 and 3
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI1", ENUM_TO_INT(RC_NIGEL_1))     // 29
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG1A, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI1A", ENUM_TO_INT(RC_NIGEL_1A))   // 30
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG1B, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI1B", ENUM_TO_INT(RC_NIGEL_1B))   // 31
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG1C, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI1C", ENUM_TO_INT(RC_NIGEL_1C))   // 32
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG1D, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI1D", ENUM_TO_INT(RC_NIGEL_1D))   // 33
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI2", ENUM_TO_INT(RC_NIGEL_2))     // 34
    Fill_CompletionPercentage_Settings (CP_RAND_C_NIG3, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_NI3", ENUM_TO_INT(RC_NIGEL_3))     // 35

    //Paparazzo 1 to 4
    Fill_CompletionPercentage_Settings (CP_RAND_C_PAP1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_PA1", ENUM_TO_INT(RC_PAPARAZZO_1)) // 36
    Fill_CompletionPercentage_Settings (CP_RAND_C_PAP2, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_PA2", ENUM_TO_INT(RC_PAPARAZZO_2)) // 37
    Fill_CompletionPercentage_Settings (CP_RAND_C_PAP3, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_PA3", ENUM_TO_INT(RC_PAPARAZZO_3)) // 38
    Fill_CompletionPercentage_Settings (CP_RAND_C_PAP4, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_PA4", ENUM_TO_INT(RC_PAPARAZZO_4)) // 39

    

    
    //Tonya - Towing - Note names. These were originally oddjobs but are treated by the game like random characters.
    Fill_CompletionPercentage_Settings (CP_OJ_TOW1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_TON1", ENUM_TO_INT(RC_TONYA_1))
    Fill_CompletionPercentage_Settings (CP_OJ_TOW2, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_TON2", ENUM_TO_INT(RC_TONYA_2)) 
    Fill_CompletionPercentage_Settings (CP_OJ_TOW3, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_TON3", ENUM_TO_INT(RC_TONYA_3)) 
    Fill_CompletionPercentage_Settings (CP_OJ_TOW4, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_TON4", ENUM_TO_INT(RC_TONYA_4))
    Fill_CompletionPercentage_Settings (CP_OJ_TOW5, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_TON5", ENUM_TO_INT(RC_TONYA_5)) 
    


    //Hao 1
    Fill_CompletionPercentage_Settings (CP_RAND_C_HAO1, CP_GROUP_RANDOMCHARS, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_HAO1", ENUM_TO_INT(RC_HAO_1))

    //Maude 1
    Fill_CompletionPercentage_Settings (CP_RAND_C_MAU1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MAU1", ENUM_TO_INT(RC_MAUDE_1))

    //Sasquatch - The Last One
    Fill_CompletionPercentage_Settings (CP_RAND_C_SAS1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_SAS1", ENUM_TO_INT(RC_THELASTONE))

    //Abigail 1
    Fill_CompletionPercentage_Settings (CP_RAND_C_ABI1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_ABI1", ENUM_TO_INT(RC_ABIGAIL_1))


    //Mrs Philips 1, 2
    Fill_CompletionPercentage_Settings (CP_RAND_C_MRS1, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MRS1", ENUM_TO_INT(RC_MRS_PHILIPS_1))
    Fill_CompletionPercentage_Settings (CP_RAND_C_MRS2, CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_RANDOMCHARS_Weighting, FALSE, FALSE, "SC_RC_MRS2", ENUM_TO_INT(RC_MRS_PHILIPS_2))




    //Minigames
    g_f_Expected_Number_in_Minigames_Group = 42.0
    FLOAT   f_CP_GROUP_MINIGAMES_Weighting = (g_c_Minigames_CombinedWeighting / g_f_Expected_Number_in_Minigames_Group)
    

    //Off Road races.
    Fill_CompletionPercentage_Settings (CP_MG_OFF1, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF1", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 0)
    Fill_CompletionPercentage_Settings (CP_MG_OFF2, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF2", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 1)
    Fill_CompletionPercentage_Settings (CP_MG_OFF3, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF3", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 2)
    Fill_CompletionPercentage_Settings (CP_MG_OFF4, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF4", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 3)
    Fill_CompletionPercentage_Settings (CP_MG_OFF5, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF5", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 4)
    Fill_CompletionPercentage_Settings (CP_MG_OFF6, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_OFF6", ENUM_TO_INT(MINIGAME_OFFROAD_RACES), 5)


    //Shooting Range    //Old combine enum needs removed, hence the no_group. Replaced by gradual.
    Fill_CompletionPercentage_Settings (CP_MG_SHOOT, CP_GROUP_NO_GROUP, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "MGSRm", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 0)

    Fill_CompletionPercentage_Settings (CP_MG_SHHAN, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHHAN", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 0) //Handgun
    Fill_CompletionPercentage_Settings (CP_MG_SHSUB, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHSUB", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 1) //Submachine
    Fill_CompletionPercentage_Settings (CP_MG_SHRIF, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHRIF", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 2) //Assault
    Fill_CompletionPercentage_Settings (CP_MG_SHLMG, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHLMG", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 3) //Light Machine
    Fill_CompletionPercentage_Settings (CP_MG_SHHVY, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHHVY", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 4) //Heavy
    Fill_CompletionPercentage_Settings (CP_MG_SHSHO, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SHSHO", ENUM_TO_INT(MINIGAME_SHOOTING_RANGE), 5) //Shotguns




    //Stunt Plane Time Trials - SPECIAL EDITION ONLY!
    Fill_CompletionPercentage_Settings (CP_MG_STUNT_TT, CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "MGSP", ENUM_TO_INT(MINIGAME_STUNT_PLANES), 0)

    //Tennis
    Fill_CompletionPercentage_Settings (CP_MG_TENNIS, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_TENN", ENUM_TO_INT(MINIGAME_TENNIS), 0)

    //Golf
    Fill_CompletionPercentage_Settings (CP_MG_GOLF, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_GOLF", ENUM_TO_INT(MINIGAME_GOLF), 0)

    //Basejumping  - Dangerous to have multiple entries here...
    Fill_CompletionPercentage_Settings (CP_MG_BASEJ, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_BJUM", ENUM_TO_INT(MINIGAME_BASEJUMPING), 0)


    //Skydiving  - Dangerous to have multiple entries here. These are part of the same script as Basejumping but treated differently on the map 
    Fill_CompletionPercentage_Settings (CP_MG_SKYD, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SKYD", ENUM_TO_INT(MINIGAME_BASEJUMPING), 1)


    //Triathlon 1 Trailer Park
    Fill_CompletionPercentage_Settings (CP_MG_TRI1, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_TRI1", ENUM_TO_INT(MINIGAME_TRIATHLON), 0)

    //Triathlon 2 Muscle Beach
    Fill_CompletionPercentage_Settings (CP_MG_TRI2, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_TRI2", ENUM_TO_INT(MINIGAME_TRIATHLON), 1)
    
    //Triathlon 3 Mountain Road
    Fill_CompletionPercentage_Settings (CP_MG_TRI3, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_TRI3", ENUM_TO_INT(MINIGAME_TRIATHLON), 2)
    


    //Stripclub
    Fill_CompletionPercentage_Settings (CP_MG_STRIP, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_STRP", ENUM_TO_INT(MINIGAME_STRIPCLUB), 0)


    //Darts
    Fill_CompletionPercentage_Settings (CP_MG_DARTS, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_DART", ENUM_TO_INT(MINIGAME_DARTS), 0)



    //Sea Races
    Fill_CompletionPercentage_Settings (CP_MG_SNCA, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SNCA", ENUM_TO_INT(MINIGAME_SEA_RACES), 0) //North Coast 
    Fill_CompletionPercentage_Settings (CP_MG_SSC,  CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SSC",  ENUM_TO_INT(MINIGAME_SEA_RACES), 1) //South Coast
    Fill_CompletionPercentage_Settings (CP_MG_SCAN, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SCAA", ENUM_TO_INT(MINIGAME_SEA_RACES), 2) //Canyon
    Fill_CompletionPercentage_Settings (CP_MG_SLOS, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SCAB", ENUM_TO_INT(MINIGAME_SEA_RACES), 3) //Los Santos






    //Street Races
    Fill_CompletionPercentage_Settings (CP_MG_SRCC, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SRCC", ENUM_TO_INT(MINIGAME_STREET_RACES), 0)  //City Circuit
    Fill_CompletionPercentage_Settings (CP_MG_SRAP, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SRAP", ENUM_TO_INT(MINIGAME_STREET_RACES), 1)  //Airport
    Fill_CompletionPercentage_Settings (CP_MG_SRFW, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SRFW", ENUM_TO_INT(MINIGAME_STREET_RACES), 2)  //Freeway
    Fill_CompletionPercentage_Settings (CP_MG_SRVC, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SRVC", ENUM_TO_INT(MINIGAME_STREET_RACES), 3)  //Vespucci Canals
    Fill_CompletionPercentage_Settings (CP_MG_SRLS, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MG_SRLS", ENUM_TO_INT(MINIGAME_STREET_RACES), 4)  //S. Los Santos












    //Pilot School 1 - 11. These were originally part of the oddjob group, hence the OJ enum entries. Now minigames.
    Fill_CompletionPercentage_Settings (CP_OJ_PS0T, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS1", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 0)
    Fill_CompletionPercentage_Settings (CP_OJ_PS0L, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS2", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 1)
    Fill_CompletionPercentage_Settings (CP_OJ_PS1, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS3", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 2)
    Fill_CompletionPercentage_Settings (CP_OJ_PS2, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS4", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 3)
    Fill_CompletionPercentage_Settings (CP_OJ_PS3, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS5", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 4)
    Fill_CompletionPercentage_Settings (CP_OJ_PS4, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS6", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 5)
    Fill_CompletionPercentage_Settings (CP_OJ_PS5, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS7", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 6)
    Fill_CompletionPercentage_Settings (CP_OJ_PS6, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS8", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 7)
    Fill_CompletionPercentage_Settings (CP_OJ_PS7, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS9", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 8)
    Fill_CompletionPercentage_Settings (CP_OJ_PS8, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS10", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 9)
    Fill_CompletionPercentage_Settings (CP_OJ_PS9, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS11", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 10)
    Fill_CompletionPercentage_Settings (CP_OJ_PS10, CP_GROUP_MINIGAMES, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS12", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 11)

    //This has been cut from the game. See #1351658
    Fill_CompletionPercentage_Settings (CP_OJ_PS11, CP_GROUP_NO_GROUP, f_CP_GROUP_MINIGAMES_Weighting, FALSE, FALSE, "SC_MGPS13", ENUM_TO_INT(MINIGAME_PILOT_SCHOOL), 12)
    
    //Fill_CompletionPercentage_Settings (CP_OJ_PS12, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "MGPS12") Removed.



    

    //Oddjobs

    g_f_Expected_Number_in_Oddjobs_Group = 0.0  //This number is important! - ST.
    FLOAT   f_CP_GROUP_ODDJOBS_Weighting = (g_c_Oddjobs_CombinedWeighting / g_f_Expected_Number_in_Oddjobs_Group)
    

    //Assassination Oddjobs - Text label order is weird due to changes, they are not mismatched. These have been moved to story missions! See 1534381
    //Fill_CompletionPercentage_Settings (CP_OJ_ASS1, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "ASS1", ENUM_TO_INT(MINIGAME_ASSASSINATIONS), 0) //Hotel
    //Fill_CompletionPercentage_Settings (CP_OJ_ASS2, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "ASS2", ENUM_TO_INT(MINIGAME_ASSASSINATIONS), 1) //Multi Target
    //Fill_CompletionPercentage_Settings (CP_OJ_ASS3, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "ASS3", ENUM_TO_INT(MINIGAME_ASSASSINATIONS), 2) //Vice
    //Fill_CompletionPercentage_Settings (CP_OJ_ASS4, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "ASS4", ENUM_TO_INT(MINIGAME_ASSASSINATIONS), 3) //bus station
    //Fill_CompletionPercentage_Settings (CP_OJ_ASS5, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "ASS5", ENUM_TO_INT(MINIGAME_ASSASSINATIONS), 4) //construction




    //Bail Bond Oddjjobs - Playable by Trevor only. Were reduced to 4 missions. See #1163763 if these go back in.   TEMPORARY MINIGAME ENUMS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Fill_CompletionPercentage_Settings (CP_OJ_BB3, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_OJBB3", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 0) //Quarry
    
    Fill_CompletionPercentage_Settings (CP_OJ_BB5, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_OJBB5", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 1) //Abandoned Farm
    Fill_CompletionPercentage_Settings (CP_OJ_BB6, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_OJBB6", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 2) //Hobo Camp
    Fill_CompletionPercentage_Settings (CP_OJ_BB7, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_OJBB7", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 3) //Mountains
 





    //Drug Traffic Air 1 to 5
    Fill_CompletionPercentage_Settings (CP_OJ_DTA1, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDA1", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 0)
    Fill_CompletionPercentage_Settings (CP_OJ_DTA2, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDA2", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 1)
    Fill_CompletionPercentage_Settings (CP_OJ_DTA3, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDA3", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 2)
    Fill_CompletionPercentage_Settings (CP_OJ_DTA4, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDA4", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 3)
    Fill_CompletionPercentage_Settings (CP_OJ_DTA5, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDA5", ENUM_TO_INT(MINIGAME_TRAFFICKING_AIR), 4)




    //Drug Traffic Ground 1 to 5 (Response to Bug # 814193, Trafficking now has 20 missions*)  *subject to change - DS 
    //Having all twenty of these as completely separate enums means each one counts towards 100 percent. This might be overload. - ST. 
    Fill_CompletionPercentage_Settings (CP_OJ_DTG1, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG1", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 0)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG2, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG2", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 1)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG3, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG3", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 2)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG4, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG4", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 3)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG5, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG5", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 4)
    
    
    //Update : Currently redacted - See bug 1129611 
    Fill_CompletionPercentage_Settings (CP_OJ_DTG6, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG6", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 5)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG7, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG7", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 6)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG8, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG8", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 7)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG9, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG9", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 8)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG10, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG10", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 9)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG11, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJGD11", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 10) //sic. Daft spelling americanDebugBugstar
    Fill_CompletionPercentage_Settings (CP_OJ_DTG12, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG12", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 11)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG13, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG13", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 12)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG14, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG14", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 13)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG15, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG15", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 14)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG16, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG16", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 15)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG17, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG17", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 16)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG18, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG18", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 17)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG19, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG19", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 18)
    Fill_CompletionPercentage_Settings (CP_OJ_DTG20, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJDG20", ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND), 19)


    
    //Hunting 1,2 - Moved to random characters...
    //Fill_CompletionPercentage_Settings (CP_OJ_HUN1, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJHU1")
    //Fill_CompletionPercentage_Settings (CP_OJ_HUN2, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJHU2")



	// [AG] 16.08.13: Rampage 1 to 5 - B*1542450 - Images for Rampages were wrong on SC. Correct Internal Order is 1,3,4,5,2
	Fill_CompletionPercentage_Settings (CP_OJ_RAM1, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_RC_RAM1", ENUM_TO_INT(MINIGAME_RAMPAGES), 0) // Rednecks
	Fill_CompletionPercentage_Settings (CP_OJ_RAM3, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_RC_RAM2", ENUM_TO_INT(MINIGAME_RAMPAGES), 1) // Vagos
	Fill_CompletionPercentage_Settings (CP_OJ_RAM4, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_RC_RAM3", ENUM_TO_INT(MINIGAME_RAMPAGES), 2) // Ballas
	Fill_CompletionPercentage_Settings (CP_OJ_RAM5, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_RC_RAM4", ENUM_TO_INT(MINIGAME_RAMPAGES), 3) // Military
	Fill_CompletionPercentage_Settings (CP_OJ_RAM2, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "SC_RC_RAM5", ENUM_TO_INT(MINIGAME_RAMPAGES), 4) // Hipsters



    //Hunting - Oddjob specific.
    Fill_CompletionPercentage_Settings (CP_OJ_HUNTCHALL, CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJHU", ENUM_TO_INT(MINIGAME_HUNTING), 0) //Hunting Challenge





    //Update 17.03.13 Taxis removed from 100 percent #1271827
    //
    //ATTENTION - Unusual behaviour
    //________________________________
    //Taxi - Modified Order  - These used to point to the cluster taxi elements. Completing any 5 of the OJ_TAX below will contribute the total percentage allotted for taxis via the clustered enums.
    //See bug #1054814
    Fill_CompletionPercentage_Settings (CP_OJ_TAX1, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX1", ENUM_TO_INT(MINIGAME_TAXI), 0) //Needs Excitement
    Fill_CompletionPercentage_Settings (CP_OJ_TAX2, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX2", ENUM_TO_INT(MINIGAME_TAXI), 1)
    Fill_CompletionPercentage_Settings (CP_OJ_TAX3, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX3", ENUM_TO_INT(MINIGAME_TAXI), 2) //Deadline
    Fill_CompletionPercentage_Settings (CP_OJ_TAX4, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX4", ENUM_TO_INT(MINIGAME_TAXI), 3) //Got Your Back
    Fill_CompletionPercentage_Settings (CP_OJ_TAX5, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX5", ENUM_TO_INT(MINIGAME_TAXI), 4) //Take to the Best.
    //Fill_CompletionPercentage_Settings (CP_OJ_TAX6, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX6", ENUM_TO_INT(MINIGAME_TAXI), 5) //I Know a Way
    Fill_CompletionPercentage_Settings (CP_OJ_TAX7, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX7", ENUM_TO_INT(MINIGAME_TAXI), 6) //Cut You In.
    Fill_CompletionPercentage_Settings (CP_OJ_TAX8, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX8", ENUM_TO_INT(MINIGAME_TAXI), 7) //Got You Now.
    Fill_CompletionPercentage_Settings (CP_OJ_TAX9, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX9", ENUM_TO_INT(MINIGAME_TAXI), 8) //Clown Car.
    Fill_CompletionPercentage_Settings (CP_OJ_TAX10, CP_GROUP_NO_GROUP, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "OJTX10", ENUM_TO_INT(MINIGAME_TAXI),  9) //Follow Car



    /* Taxi clustering system removed.
    Fill_CompletionPercentage_Settings (CP_OJ_CLUSTERTAXI1, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "T_TAXICL1", ENUM_TO_INT(MINIGAME_TAXI), 0, STANDARD_MISSION, TAXI_CLUSTER)
    Fill_CompletionPercentage_Settings (CP_OJ_CLUSTERTAXI2, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "T_TAXICL2", ENUM_TO_INT(MINIGAME_TAXI), 0, STANDARD_MISSION, TAXI_CLUSTER)
    Fill_CompletionPercentage_Settings (CP_OJ_CLUSTERTAXI3, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "T_TAXICL3", ENUM_TO_INT(MINIGAME_TAXI), 0, STANDARD_MISSION, TAXI_CLUSTER)
    Fill_CompletionPercentage_Settings (CP_OJ_CLUSTERTAXI4, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "T_TAXICL4", ENUM_TO_INT(MINIGAME_TAXI), 0, STANDARD_MISSION, TAXI_CLUSTER)
    Fill_CompletionPercentage_Settings (CP_OJ_CLUSTERTAXI5, CP_GROUP_ODDJOBS, f_CP_GROUP_ODDJOBS_Weighting, FALSE, FALSE, "T_TAXICL5", ENUM_TO_INT(MINIGAME_TAXI), 0, STANDARD_MISSION, TAXI_CLUSTER)
    */


    





    //Random Events
    
    g_f_Expected_Number_in_RandomEvents_Group = 59.0
    
    g_Quarter_Threshold_Group_Num_Of_RandomEvents = 14  //Remember to update this if the number of random events changes. 
                                                        //This is used for adding the player card front / end stats sensibly.
                                                        //Search on NUM_RNDEVENTS_COMPLETED for example.

    FLOAT   f_CP_GROUP_RANDOMEVENTS_Weighting = (g_c_RandomEvents_CombinedWeighting / g_f_Expected_Number_in_RandomEvents_Group)
    



    //Newly added Random Events due to change in game finale that means no playable characters die.

    //Abandoned Vehicle 1 and 2
    Fill_CompletionPercentage_Settings (CP_RE_ABCAR1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE8A") //Not available to all characters.
    Fill_CompletionPercentage_Settings (CP_RE_ABCAR2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE8B") //Not available to all characters.


    //Countryside Gang Fight
    Fill_CompletionPercentage_Settings (CP_RE_CGANGF, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE66") //Not available to all characters.

    
    //Escape Paparazzi - now back in according to bug 1144067
    Fill_CompletionPercentage_Settings (CP_RE_ESCPAP, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE25") //Not available to all characters.


    //Gang Intimidation
    Fill_CompletionPercentage_Settings (CP_RE_GANGINT, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE28") //Not available to all characters.


    //Homeland Security
    Fill_CompletionPercentage_Settings (CP_RE_HOMESEC, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE32A") //Not available to all characters.


    //Luring Girl into Alley
    Fill_CompletionPercentage_Settings (CP_RE_LURED, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE35") //Not available to all characters.



    //PrisonerLift 1 and 2
    Fill_CompletionPercentage_Settings (CP_RE_PLIFT1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE44A") //Not available to all characters.
    Fill_CompletionPercentage_Settings (CP_RE_PLIFT2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE44B") //Not available to all characters.


    //Ungrateful Whore.
    Fill_CompletionPercentage_Settings (CP_RE_UNGHO, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE58") //Not available to all characters.



    //Pick Pocket... Removed from 100 percent completion at request of bug 976839
    Fill_CompletionPercentage_Settings (CP_RE_PICPOC, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE41")






    Fill_CompletionPercentage_Settings (CP_RE_ARR1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE1A") //re_arrests1
    Fill_CompletionPercentage_Settings (CP_RE_ARR2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE1B") //re_arrests2



    Fill_CompletionPercentage_Settings (CP_RE_ATM, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE2A") //re_atm - one common enum now. See bug 892160







     
    Fill_CompletionPercentage_Settings (CP_RE_BTC1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE3A") //re_Bike Thief City 1
    Fill_CompletionPercentage_Settings (CP_RE_BTC2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE3B") //re_Bike Thief City 2



    Fill_CompletionPercentage_Settings (CP_RE_BKD1, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5A") //re_Broken Down 1
    Fill_CompletionPercentage_Settings (CP_RE_BKD2, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5B") //re_Broken Down 2  
    Fill_CompletionPercentage_Settings (CP_RE_BKD3, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5C") //re_Broken Down 3
    Fill_CompletionPercentage_Settings (CP_RE_BKD4, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5D") //re_Broken Down 4

    //840051 requested removal. No grouped.
    Fill_CompletionPercentage_Settings (CP_RE_BKD5, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5E") //re_Broken Down 5
    

    Fill_CompletionPercentage_Settings (CP_RE_BKD6, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5F") //re_Broken Down 6
    Fill_CompletionPercentage_Settings (CP_RE_BKD7, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5G") //re_Broken Down 7  
    Fill_CompletionPercentage_Settings (CP_RE_BKD8, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5H") //re_Broken Down 8
    Fill_CompletionPercentage_Settings (CP_RE_BKD9, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5I") //re_Broken Down 9
    Fill_CompletionPercentage_Settings (CP_RE_BKD10, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE5J") //re_Broken Down 10



    Fill_CompletionPercentage_Settings (CP_RE_BUR1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE6")   //re_Burial

    Fill_CompletionPercentage_Settings (CP_RE_BUS1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE7")   //re_Bus Tour





    Fill_CompletionPercentage_Settings (CP_RE_CTHF1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE11A") //Car Thief 1
    Fill_CompletionPercentage_Settings (CP_RE_CTHF2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE11B") //Car Thief 2


    Fill_CompletionPercentage_Settings (CP_RE_CHTCIT1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE12A") //Chase Thieves City 1
    Fill_CompletionPercentage_Settings (CP_RE_CHTCIT2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE12B") //Chase Thieves City 2



    Fill_CompletionPercentage_Settings (CP_RE_CHTCO1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE13A") //Chase Thieves Country 1
    Fill_CompletionPercentage_Settings (CP_RE_CHTCO2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE13B") //Chase Thieves Country 2


    Fill_CompletionPercentage_Settings (CP_RE_CONA1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE14A") //Construction Accident


    Fill_CompletionPercentage_Settings (CP_RE_COROB1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE16") //Countryside Robbery


    Fill_CompletionPercentage_Settings (CP_RE_CRAR1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE18") //Crash Rescue


    Fill_CompletionPercentage_Settings (CP_RE_CULT1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE20") //Cult Shootout
    
    Fill_CompletionPercentage_Settings (CP_RE_DEAL1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE21") //Deal Gone Wrong

    Fill_CompletionPercentage_Settings (CP_RE_DIRT1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE4A") //Dirt Bike Thief Country


    Fill_CompletionPercentage_Settings (CP_RE_DOM1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE22") //Domestic

    Fill_CompletionPercentage_Settings (CP_RE_DRUD1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE24A") //Drunk Driver 1
    Fill_CompletionPercentage_Settings (CP_RE_DRUD2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE24B") //Drunk Driver 2

    //Now included. See bug 427950
    Fill_CompletionPercentage_Settings (CP_RE_GET1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE29") //Getaway

    Fill_CompletionPercentage_Settings (CP_RE_HITL1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE31A") 	//Hitch Lift 1
    Fill_CompletionPercentage_Settings (CP_RE_HITL2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE31B")	//Hitch Lift 2
    Fill_CompletionPercentage_Settings (CP_RE_HITL3, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE31C") 	//Hitch Lift 3
    Fill_CompletionPercentage_Settings (CP_RE_HITL4, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE31D") 		//Hitch Lift 4  - Removed, see Bridget email.
    Fill_CompletionPercentage_Settings (CP_RE_HITL5, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE31E") 	//Hitch Lift 5 - Needs new label when available.

    Fill_CompletionPercentage_Settings (CP_RE_KIDN1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE62") 	//Kidnap Girl / Snatched
    
    Fill_CompletionPercentage_Settings (CP_RE_MOUN1, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE36") 		//Mountain Dancer

    Fill_CompletionPercentage_Settings (CP_RE_MUG1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE37A") 	//Mugging 1
    Fill_CompletionPercentage_Settings (CP_RE_MUG2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE37B") 	//Mugging 2
    Fill_CompletionPercentage_Settings (CP_RE_MUG3, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE37C") 	//Mugging 3
    Fill_CompletionPercentage_Settings (CP_RE_MUG4, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE37D") 		//Mugging 4  - Removed, see Bridget email.
    //Fill_CompletionPercentage_Settings (CP_RE_MUG5, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE37E") 	//Mugging 5  782814 requested removal

    Fill_CompletionPercentage_Settings (CP_RE_BORP1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE15A") 	//Border Patrol 1
    Fill_CompletionPercentage_Settings (CP_RE_BORP2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE15B") 	//Border Patrol 2
    Fill_CompletionPercentage_Settings (CP_RE_BORP3, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE15C") 	//Border Patrol 3


    //Note: Security Van 3 is not present.
    Fill_CompletionPercentage_Settings (CP_RE_SECV1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49A") 	//Security Van 1
    Fill_CompletionPercentage_Settings (CP_RE_SECV2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49B") 	//Security Van 2

    Fill_CompletionPercentage_Settings (CP_RE_SECV4, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49D") 	//Security Van 4

    Fill_CompletionPercentage_Settings (CP_RE_SECV5, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49E") 	//Security Van 5
    Fill_CompletionPercentage_Settings (CP_RE_SECV6, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49F") 	//Security Van 6
    Fill_CompletionPercentage_Settings (CP_RE_SECV7, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49G") 	//Security Van 7

    Fill_CompletionPercentage_Settings (CP_RE_SECV8, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49H")	//Security Van 8
    Fill_CompletionPercentage_Settings (CP_RE_SECV9, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49I") 	//Security Van 9
    Fill_CompletionPercentage_Settings (CP_RE_SECV10, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49J") 	//Security Van 10
    Fill_CompletionPercentage_Settings (CP_RE_SECV11, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE49K") 	//Security Van 11



    Fill_CompletionPercentage_Settings (CP_RE_SHOP1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE51A") 	//Shop Robbery 1
    Fill_CompletionPercentage_Settings (CP_RE_SHOP2, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE51B") 	//Shop Robbery 2

    Fill_CompletionPercentage_Settings (CP_RE_STAG1, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE52") 	//Stag Do Running Man
    
    Fill_CompletionPercentage_Settings (CP_RE_WAND1, CP_GROUP_NO_GROUP, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE59") 		//Wanderer

    Fill_CompletionPercentage_Settings (CP_RE_YETAR, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE9") 	//Simeon Yetarian
	
	// New NG only REs.
	Fill_CompletionPercentage_Settings (CP_RE_DUEL, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE67") 	//RE Duel
	Fill_CompletionPercentage_Settings (CP_RE_SEAPL, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE68") 	//RE Seaplane
	Fill_CompletionPercentage_Settings (CP_RE_MONKEYPHOTO, CP_GROUP_RANDOMEVENTS, f_CP_GROUP_RANDOMEVENTS_Weighting, FALSE, FALSE, "RE69")	//RE Monkey Photography

    g_f_Expected_Number_in_Miscellaneous_Group = 12.0
    FLOAT   f_CP_GROUP_MISCELLANEOUS_Weighting = (g_c_Miscellaneous_CombinedWeighting / g_f_Expected_Number_in_Miscellaneous_Group)

    Fill_CompletionPercentage_Settings (CP_UFOP1, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_CO_UFOP1") //Omega Spaceship parts collection

    Fill_CompletionPercentage_Settings (CP_DLS1, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_CO_DLS1") //Dreyfuss Letter scraps collection.


    Fill_CompletionPercentage_Settings (CP_CINEMA, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_CIN") //Visit the in-game cinema.


    Fill_CompletionPercentage_Settings (CP_STNJMP, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_STJ") //Stunt Jumps

    Fill_CompletionPercentage_Settings (CP_UNDBRG, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_BRG") //Under the Bridge

    Fill_CompletionPercentage_Settings (CP_KNFFLT, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_KFT") //Knife Flights


    //Property Purchase
    Fill_CompletionPercentage_Settings (CP_PRPTY, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_PPY") //Property Purchase

    Fill_CompletionPercentage_Settings (CP_BUYVEH, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_VEH") //Buy vehicle from a website

    Fill_CompletionPercentage_Settings (CP_CHOP, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_CHP") //Fun with Chop the dog.

    Fill_CompletionPercentage_Settings (CP_BOOTY, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_BOO") //Booty Call

    Fill_CompletionPercentage_Settings (CP_HOOKER, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_HKR") //Hooker service

    Fill_CompletionPercentage_Settings (CP_HOLDUP, CP_GROUP_MISCELLANEOUS, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_HLD") //Shop Holdup



    //Diving Pickups
    Fill_CompletionPercentage_Settings (CP_DIVCOLL, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_DIV")

    //Sonar Collections
    Fill_CompletionPercentage_Settings (CP_SONCOLL, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_SON")


    //Yoga
    Fill_CompletionPercentage_Settings (CP_YOGA, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_YOG")


    //Cable Car Ride
    Fill_CompletionPercentage_Settings (CP_CABLE, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_CAB")


    //Car Wash
    Fill_CompletionPercentage_Settings (CP_CARWSH, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_WSH")


    //Fairground
    Fill_CompletionPercentage_Settings (CP_FAIRG, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_FRG")
    

    //Clothes Shop
    Fill_CompletionPercentage_Settings (CP_CLOTH, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_CLT")


    //Car Mod Shop
    Fill_CompletionPercentage_Settings (CP_CARMOD, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_MOD")


    //Barber Shop
    Fill_CompletionPercentage_Settings (CP_BARBER, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_BRB")


    //Tattoo Shop
    Fill_CompletionPercentage_Settings (CP_TATTOO, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_TAT")


    //Weapon Shop
    Fill_CompletionPercentage_Settings (CP_BUYGUN, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_GUN")



    //Safehouse Watch TV
    Fill_CompletionPercentage_Settings (CP_WTCHTV, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_WTV")


    //Stock Market Dealing
    Fill_CompletionPercentage_Settings (CP_STOCKS, CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_MISCELLANEOUS_Weighting, FALSE, FALSE, "SC_MSC_STK")









    g_f_Expected_Number_in_Friends_Group = 4.0   
    FLOAT   f_CP_GROUP_FRIENDS_Weighting = (g_c_Friends_CombinedWeighting / g_f_Expected_Number_in_Friends_Group)


    //Remove this when Sam swaps over to activity based enums below.
    Fill_CompletionPercentage_Settings (CP_FRIENDS, CP_GROUP_NO_GROUP, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_ARRACT") //Arrange friend activities.
    

    Fill_CompletionPercentage_Settings (CP_FR_BAR, CP_GROUP_FRIENDS, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_BAR") //Bar
    Fill_CompletionPercentage_Settings (CP_FR_CIN, CP_GROUP_FRIENDS, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_CIN") //Cinema
    Fill_CompletionPercentage_Settings (CP_FR_DAR, CP_GROUP_FRIENDS, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_DAR") //Darts
    Fill_CompletionPercentage_Settings (CP_FR_STR, CP_GROUP_FRIENDS, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_STR") //Stripclub

    //Tennis and Golf friend activity partner can be restricted by Finale choice. See bug #1423953
    Fill_CompletionPercentage_Settings (CP_FR_GOL, CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_GOL") //Golf
    Fill_CompletionPercentage_Settings (CP_FR_TEN, CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT, f_CP_GROUP_FRIENDS_Weighting, FALSE, FALSE, "SC_FR_TEN") //Tennis














    //Calculate and store how many of each grouping we have, so that we can assign a rounded grouping value if need be...
                                             
    INT tempIndex = 0
    FLOAT tempWeightingsTotal = 0


    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

    
        IF NOT (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping = CP_GROUP_NO_GROUP)

            tempWeightingsTotal = tempWeightingsTotal + g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Weighting

        ENDIF

        SWITCH (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Grouping)


            CASE CP_GROUP_MISSIONS

               g_Group_Num_of_Missions ++   

            BREAK
                
            
            CASE CP_GROUP_MINIGAMES

                g_Group_Num_of_Minigames ++

            BREAK


            CASE CP_GROUP_ODDJOBS

                g_Group_Num_of_Oddjobs ++

            BREAK


            CASE CP_GROUP_RANDOMCHARS

                g_Group_Num_of_RANDOMCHARS ++

            BREAK



            CASE CP_GROUP_RANDOMEVENTS

                g_Group_Num_of_RandomEvents ++

            BREAK

            

            CASE CP_GROUP_MISCELLANEOUS

                g_Group_Num_of_Miscellaneous ++

            BREAK


            CASE CP_GROUP_FRIENDS

                g_Group_Num_of_Friends ++

            BREAK


            DEFAULT     
            
                #if IS_DEBUG_BUILD 
                    PRINTNL()
                    PRINTSTRING ("CP_PUBLIC - reporting a suspect GROUPING entry in the CompletionPercentage tracker at position ")
                    PRINTINT (tempIndex)
                    PRINTNL()
                #endif

            BREAK





        ENDSWITCH


        tempIndex ++

    ENDWHILE       






    
    #if IS_DEBUG_BUILD 
        PRINTNL()

        
        PRINTSTRING("CP_PUBLIC - Check total of all individual weightings without any group rounding is: ")
        PRINTFLOAT(tempWeightingsTotal)
        PRINTNL()


        PRINTSTRING ("CP_PUBLIC - Number of entries in Missions_Group is ")
        PRINTINT (g_Group_Num_of_Missions)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Minigames_Group is ")
        PRINTINT (g_Group_Num_of_Minigames)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Oddjobs_Group is ")
        PRINTINT (g_Group_Num_of_Oddjobs)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomChars_Group is ")
        PRINTINT (g_Group_Num_of_RandomChars)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in RandomEvents_Group is ")
        PRINTINT (g_Group_Num_of_RandomEvents)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Miscellanous_Group is ")
        PRINTINT (g_Group_Num_of_Miscellaneous)
        PRINTNL()
        PRINTSTRING ("CP_PUBLIC - Number of entries in Friends_Group is ")
        PRINTINT (g_Group_Num_of_Friends)


        PRINTNL()


        FLOAT tempTargetTotalCheck



        tempTargetTotalCheck = ( g_c_Missions_CombinedWeighting +
                g_c_Minigames_CombinedWeighting +         
                g_c_Oddjobs_CombinedWeighting +           

                g_c_RandomChars_CombinedWeighting +      
                g_c_RandomEvents_CombinedWeighting +     

                g_c_Miscellaneous_CombinedWeighting +   
                g_c_Friends_CombinedWeighting )           



        IF tempTargetTotalCheck > 100.0
        OR tempTargetTotalCheck < 100.0

            SCRIPT_ASSERT ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor.")
            PRINTNL()
            PRINTSTRING ("Target Completion Percentage is less or greater than 100. That does not make sense! See Steve Taylor. Target is ")
            PRINTFLOAT (tempTargetTotalCheck)

        ENDIF





    #endif






ENDPROC

#ENDIF






PROC REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (enumCompletionPercentageEntries whichEntry, FLOAT whichScriptDefinedLocX = 0.0, FLOAT whichScriptDefinedLocY = 0.0)




    IF (ENUM_TO_INT(whichEntry)) < 0

       #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is less than zero!")
            PRINTNL()

            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum less than zero! Please bug owner of mission!")
  
        #endif


    ENDIF


    
    IF (ENUM_TO_INT(whichEntry)) = (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
    OR (ENUM_TO_INT(whichEntry)) > (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))

       #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is greater or equal to MAX_CP_ENTRIES!")
            PRINTNL()

            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum greater or equal to MAX_CP_ENTRIES. Please bug owner of mission!")
  
        #endif



    ELSE


        SET_PACKED_STAT_BOOL (INT_TO_ENUM(STATS_PACKED, (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry))), TRUE) 
        
        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Registering Packed Stat Bool at position ")
            PRINTINT ( (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry)) )
            PRINTNL()

        #endif

    ENDIF






    BOOL b_Temp_NeedToRecalculate = TRUE


    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE

         #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Repeat call to previously included entry: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTSTRING (" which is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
          
        
            PRINTNL()

        #endif



        //Put in miscellaenous group check here... if the element is part of that group then we know not to upload the cloud file again as they do not store any scores.
        //# 1424520
        IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS
        OR g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT

            b_Temp_NeedToRecalculate = FALSE
            
            #if IS_DEBUG_BUILD

                PRINTSTRING ("CP_PUBLIC - This element part of MISCELLANEOUS / MISC_NOT_100PERCENT and previously registered. Not recalculating, not triggering cloud upload.")
                PRINTNL()

            #endif

        ENDIF                                                                            






    ELSE

        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE
        //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Assigned_as_included_by_Script = TRUE


        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationX = whichScriptDefinedLocX
        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationY = whichScriptDefinedLocY



        IF whichEntry = CP_DLS1

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("CP_PUBLIC - Presence Event fired for Letter Scraps. ")

            #endif

            PRESENCE_EVENT_UPDATESTAT_INT (NUM_HIDDEN_PACKAGES_0, 50)

        ENDIF


                
        IF whichEntry = CP_UFOP1

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("CP_PUBLIC - Presence Event fired for UFO parts. ")

            #endif

            PRESENCE_EVENT_UPDATESTAT_INT (NUM_HIDDEN_PACKAGES_1, 50)

        ENDIF


        IF whichEntry = CP_SONCOLL

            #if IS_DEBUG_BUILD

                PRINTNL()
                PRINTSTRING ("CP_PUBLIC - Presence Event fired for Sonar Collections. ")

            #endif

            PRESENCE_EVENT_UPDATESTAT_INT (NUM_HIDDEN_PACKAGES_3, 50)

        ENDIF


        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Call made to mark this Enum Entry as COMPLETE: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTNL()
            PRINTSTRING ("This is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
            PRINTSTRING (" from the group ")


            BOOL b_IncludeDebugWeighting = FALSE

            SWITCH g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping

                CASE CP_GROUP_MISSIONS
                    PRINTSTRING ("MISSIONS")
                    b_IncludeDebugWeighting = TRUE 
                BREAK

                CASE CP_GROUP_MINIGAMES
                    PRINTSTRING ("MINIGAMES")
                    b_IncludeDebugWeighting = TRUE 
                BREAK
                
                CASE CP_GROUP_ODDJOBS
                    PRINTSTRING ("ODDJOBS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS
                    PRINTSTRING ("RANDOM CHARACTERS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS
                    PRINTSTRING ("RANDOM EVENTS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS
                    PRINTSTRING ("MISCELLANEOUS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_FRIENDS
                    PRINTSTRING ("FRIENDS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                

                
                CASE CP_GROUP_MISSIONS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISSIONS_INGAME_BUT_NOT_100PERCENT ")
                    b_IncludeDebugWeighting = FALSE 
                BREAK

                CASE CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MINIGAMES_INGAME_BUT_NOT_100PERCENT") 
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("ODDJOBS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM CHARACTERS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM EVENTS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("FRIENDS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK







                DEFAULT

                    PRINTSTRING("WEIRD RESULT! Alert Steve T.")

                BREAK


            ENDSWITCH


            IF b_IncludeDebugWeighting = FALSE
                
                PRINTNL()
                PRINTSTRING ("This is not mandatory for 100 percent completion.")

            ELSE

                PRINTNL()
                PRINTSTRING ("This added ")
                PRINTFLOAT(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting)
                PRINTSTRING(" to the Percentage Complete total")
                PRINTNL()

            ENDIF
    
        #endif



    ENDIF





        //This check will only take place when a Ground Trafficking oddjob is registered via script. Can be integrated above but easier to test here in the interim.
        /*
        IF (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData & CP_7BIT_BITMASK) = ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND)



            INT i_NumberOfGroundTraffickingCompleted = 0
            INT tempTG_Index = 0
            
             
            WHILE tempTG_Index < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))


                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempTG_Index].CP_Marked_As_Complete = TRUE

                    IF (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempTG_Index].CP_EnumAndVariationData & CP_7BIT_BITMASK) = ENUM_TO_INT(MINIGAME_TRAFFICKING_GROUND)
                        
                        i_NumberOfGroundTraffickingCompleted ++

                        #if IS_DEBUG_BUILD
                            Printstring ("CP_PUBLIC - Ground Trafficking completion found at ")
                            PrintInt (tempTG_Index)
                            Printnl()
                        #endif
                        
                    ENDIF

                ENDIF


                tempTG_Index ++


            ENDWHILE



            IF i_NumberOfGroundTraffickingCompleted > 4


                //#if IS_DEBUG_BUILD
                    //Printstring ("CP_PUBLIC - 5 or more Ground Trafficking registered as complete. Auto - registering others before recalculating percentage if applicable.")
                    //PrintInt (tempTG_Index)
                    //Printnl()
                //#endif

   
                 Currently redacted - See bug 1129611 
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG1].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG2].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG3].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG4].CP_Marked_As_Complete = TRUE


                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG5].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG6].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG7].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG8].CP_Marked_As_Complete = TRUE


                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG9].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG10].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG11].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG12].CP_Marked_As_Complete = TRUE


                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG13].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG14].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG15].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG16].CP_Marked_As_Complete = TRUE


                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG17].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG18].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG19].CP_Marked_As_Complete = TRUE
                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_DTG20].CP_Marked_As_Complete = TRUE
                

          


            ENDIF


        ENDIF
        */

        /* Taxis removed from 100 percent completion because of new link to property ownership. #1271827
        IF (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_EnumAndVariationData & CP_7BIT_BITMASK) = ENUM_TO_INT(MINIGAME_TAXI)



            INT i_NumberOfTaxiCompleted = 0
            INT tempTG_Index = 0
            
             
            WHILE tempTG_Index < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))


                IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempTG_Index].CP_Marked_As_Complete = TRUE

                    IF (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempTG_Index].CP_EnumAndVariationData & CP_7BIT_BITMASK) = ENUM_TO_INT(MINIGAME_TAXI)
                        

                        IF NOT (g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempTG_Index].CP_SocialClubClusterDisplay = TAXI_CLUSTER) 

                            i_NumberOfTaxiCompleted ++

                            #if IS_DEBUG_BUILD
                                Printstring ("CP_PUBLIC - Tributary Taxi completion found at ")
                                PrintInt (tempTG_Index)
                                Printnl()
                            #endif
                            
                            IF i_NumberOfTaxiCompleted > 0

                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI1].CP_Marked_As_Complete = TRUE
                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI1].CP_Assigned_as_included_by_Script = TRUE

                            ENDIF
                            
                            IF i_NumberOfTaxiCompleted > 1

                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI2].CP_Marked_As_Complete = TRUE
                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI2].CP_Assigned_as_included_by_Script = TRUE

                            ENDIF



                            IF i_NumberOfTaxiCompleted > 2

                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI3].CP_Marked_As_Complete = TRUE
                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI3].CP_Assigned_as_included_by_Script = TRUE

                            ENDIF


                            IF i_NumberOfTaxiCompleted > 3

                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI4].CP_Marked_As_Complete = TRUE
                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI4].CP_Assigned_as_included_by_Script = TRUE

                            ENDIF

                            
                            IF i_NumberOfTaxiCompleted > 4

                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI5].CP_Marked_As_Complete = TRUE
                                g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[CP_OJ_CLUSTERTAXI5].CP_Assigned_as_included_by_Script = TRUE

                            ENDIF

                        ENDIF


                    ENDIF

                ENDIF


                tempTG_Index ++


            ENDWHILE


  


        ENDIF
        */


    IF b_Temp_NeedToRecalculate

        RecalculateResultantPercentageTotal()

    ENDIF



ENDPROC


PROC REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL_CLF (enumCompletionPercentageEntries whichEntry, FLOAT whichScriptDefinedLocX = 0.0, FLOAT whichScriptDefinedLocY = 0.0)

    IF (ENUM_TO_INT(whichEntry)) < 0
       #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is less than zero!")
            PRINTNL()
            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum less than zero! Please bug owner of mission!")  
        #endif
    ENDIF
    
    IF (ENUM_TO_INT(whichEntry)) = (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_CLF))
    OR (ENUM_TO_INT(whichEntry)) > (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_CLF))

       #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is greater or equal to MAX_COMP_PERCENTAGE_ENTRIES_CLF!")
            PRINTNL()
            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum greater or equal to MAX_COMP_PERCENTAGE_ENTRIES_CLF. Please bug owner of mission!")
  		#endif

    ELSE
        SET_PACKED_STAT_BOOL (INT_TO_ENUM(STATS_PACKED, (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry))), TRUE)         
        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Registering Packed Stat Bool at position ")
            PRINTINT ( (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry)) )
            PRINTNL()
        #endif
    ENDIF

    BOOL b_Temp_NeedToRecalculate = TRUE
    IF g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE
         #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Repeat call to previously included entry: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTSTRING (" which is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))   
            PRINTNL()
        #endif
        //Put in miscellaenous group check here... if the element is part of that group then we know not to upload the cloud file again as they do not store any scores.
        //# 1424520
        IF g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS
        OR g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT

            b_Temp_NeedToRecalculate = FALSE            
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PUBLIC - This element part of MISCELLANEOUS / MISC_NOT_100PERCENT and previously registered. Not recalculating, not triggering cloud upload.")
                PRINTNL()
            #endif
        ENDIF   
    ELSE

        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE
        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationX = whichScriptDefinedLocX
        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationY = whichScriptDefinedLocY

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Call made to mark this Enum Entry as COMPLETE: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTNL()
            PRINTSTRING ("This is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
            PRINTSTRING (" from the group ")

            BOOL b_IncludeDebugWeighting = FALSE

            SWITCH g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping

                CASE CP_GROUP_MISSIONS
                    PRINTSTRING ("MISSIONS")
                    b_IncludeDebugWeighting = TRUE 
                BREAK

                CASE CP_GROUP_MINIGAMES
                    PRINTSTRING ("MINIGAMES")
                    b_IncludeDebugWeighting = TRUE 
                BREAK
                
                CASE CP_GROUP_ODDJOBS
                    PRINTSTRING ("ODDJOBS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS
                    PRINTSTRING ("RANDOM CHARACTERS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS
                    PRINTSTRING ("RANDOM EVENTS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS
                    PRINTSTRING ("MISCELLANEOUS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_FRIENDS
                    PRINTSTRING ("FRIENDS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                
                CASE CP_GROUP_MISSIONS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISSIONS_INGAME_BUT_NOT_100PERCENT ")
                    b_IncludeDebugWeighting = FALSE 
                BREAK

                CASE CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MINIGAMES_INGAME_BUT_NOT_100PERCENT") 
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("ODDJOBS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM CHARACTERS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM EVENTS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("FRIENDS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                DEFAULT
                    PRINTSTRING("WEIRD RESULT! Alert Steve T.")
                BREAK

            ENDSWITCH

            IF b_IncludeDebugWeighting = FALSE                
                PRINTNL()
                PRINTSTRING ("This is not mandatory for 100 percent completion.")
            ELSE
                PRINTNL()
                PRINTSTRING ("This added ")
                PRINTFLOAT(g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting)
                PRINTSTRING(" to the Percentage Complete total")
                PRINTNL()
            ENDIF    
        #endif
    ENDIF

    IF b_Temp_NeedToRecalculate
        RecalculateResultantPercentageTotal_CLF()
    ENDIF
ENDPROC
PROC REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL_NRM (enumCompletionPercentageEntries whichEntry, FLOAT whichScriptDefinedLocX = 0.0, FLOAT whichScriptDefinedLocY = 0.0)

    IF (ENUM_TO_INT(whichEntry)) < 0
       #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is less than zero!")
            PRINTNL()
            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum less than zero! Please bug owner of mission!")  
        #endif
    ENDIF
    
    IF (ENUM_TO_INT(whichEntry)) = (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_NRM))
    OR (ENUM_TO_INT(whichEntry)) > (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_NRM))

       #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Attempted to Register Entry but it is greater or equal to MAX_COMP_PERCENTAGE_ENTRIES_CLF!")
            PRINTNL()
            SCRIPT_ASSERT("CP_PUBLIC - Passed REGISTER_SCRIPT_IN_CP_TOTAL enum greater or equal to MAX_COMP_PERCENTAGE_ENTRIES_CLF. Please bug owner of mission!")
  		#endif

    ELSE
        SET_PACKED_STAT_BOOL (INT_TO_ENUM(STATS_PACKED, (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry))), TRUE)         
        #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Registering Packed Stat Bool at position ")
            PRINTINT ( (ENUM_TO_INT(PACKED_CHECKLIST_START)) + (ENUM_TO_INT(whichEntry)) )
            PRINTNL()
        #endif
    ENDIF

    BOOL b_Temp_NeedToRecalculate = TRUE
    IF g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE
         #if IS_DEBUG_BUILD
            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Repeat call to previously included entry: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTSTRING (" which is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))   
            PRINTNL()
        #endif
        //Put in miscellaenous group check here... if the element is part of that group then we know not to upload the cloud file again as they do not store any scores.
        //# 1424520
        IF g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS
        OR g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping = CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT

            b_Temp_NeedToRecalculate = FALSE            
            #if IS_DEBUG_BUILD
                PRINTSTRING ("CP_PUBLIC - This element part of MISCELLANEOUS / MISC_NOT_100PERCENT and previously registered. Not recalculating, not triggering cloud upload.")
                PRINTNL()
            #endif
        ENDIF   
    ELSE

        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE
        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationX = whichScriptDefinedLocX
        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_DefinedLocationY = whichScriptDefinedLocY

        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Call made to mark this Enum Entry as COMPLETE: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTNL()
            PRINTSTRING ("This is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
            PRINTSTRING (" from the group ")

            BOOL b_IncludeDebugWeighting = FALSE

            SWITCH g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Grouping

                CASE CP_GROUP_MISSIONS
                    PRINTSTRING ("MISSIONS")
                    b_IncludeDebugWeighting = TRUE 
                BREAK

                CASE CP_GROUP_MINIGAMES
                    PRINTSTRING ("MINIGAMES")
                    b_IncludeDebugWeighting = TRUE 
                BREAK
                
                CASE CP_GROUP_ODDJOBS
                    PRINTSTRING ("ODDJOBS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS
                    PRINTSTRING ("RANDOM CHARACTERS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS
                    PRINTSTRING ("RANDOM EVENTS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS
                    PRINTSTRING ("MISCELLANEOUS")
                    b_IncludeDebugWeighting = TRUE
                BREAK

                CASE CP_GROUP_FRIENDS
                    PRINTSTRING ("FRIENDS")
                    b_IncludeDebugWeighting = TRUE
                BREAK
                
                CASE CP_GROUP_MISSIONS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISSIONS_INGAME_BUT_NOT_100PERCENT ")
                    b_IncludeDebugWeighting = FALSE 
                BREAK

                CASE CP_GROUP_MINIGAMES_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MINIGAMES_INGAME_BUT_NOT_100PERCENT") 
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_ODDJOBS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("ODDJOBS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK
                
                CASE CP_GROUP_RANDOMCHARS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM CHARACTERS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_RANDOMEVENTS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("RANDOM EVENTS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("MISCELLANEOUS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                CASE CP_GROUP_FRIENDS_INGAME_BUT_NOT_100PERCENT
                    PRINTSTRING ("FRIENDS_INGAME_BUT_NOT_100PERCENT")
                    b_IncludeDebugWeighting = FALSE
                BREAK

                DEFAULT
                    PRINTSTRING("WEIRD RESULT! Alert Steve T.")
                BREAK

            ENDSWITCH

            IF b_IncludeDebugWeighting = FALSE                
                PRINTNL()
                PRINTSTRING ("This is not mandatory for 100 percent completion.")
            ELSE
                PRINTNL()
                PRINTSTRING ("This added ")
                PRINTFLOAT(g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Weighting)
                PRINTSTRING(" to the Percentage Complete total")
                PRINTNL()
            ENDIF    
        #endif
    ENDIF

    IF b_Temp_NeedToRecalculate
        RecalculateResultantPercentageTotal_NRM()
    ENDIF
ENDPROC





#if IS_DEBUG_BUILD
//Warning! This should only be used in debug mode!

PROC REMOVE_SCRIPT_FROM_COMPLETION_PERCENTAGE_TOTAL (enumCompletionPercentageEntries whichEntry)



    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE

         #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - Removing entry: ")
            PRINTINT (ENUM_TO_INT(whichEntry))
            PRINTSTRING (" which is ")
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
          
            PRINTNL()

        #endif


        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = FALSE
        //g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Assigned_as_included_by_Script = FALSE




        //From CompletionPercentage_private.sch    - recalculate anyway...
        RecalculateResultantPercentageTotal()


    ELSE

    
        
        #if IS_DEBUG_BUILD

            PRINTNL()
            PRINTSTRING ("CP_PUBLIC - This entry has not been registered. No action taken to remove it.")        
            PRINTNL()
            PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
          
            PRINTNL()


        #endif



        //From CompletionPercentage_private.sch
        RecalculateResultantPercentageTotal()

    ENDIF




ENDPROC


#endif



//Warning! This will completely reset the completion percentage total to zero and mark all scripts as not included in percentage calculation. They would need to be
//completed again to be re-included or re-ticked in debug widget.

PROC RESET_COMPLETION_PERCENTAGE_TRACKING()

    INT tempIndex = 0

	#if USE_CLF_DLC
	    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_CLF))
	        g_savedGlobalsClifford.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = FALSE   
	        tempIndex ++
	    ENDWHILE			
	#endif	
	#if USE_NRM_DLC
	    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES_NRM))
	        g_savedGlobalsnorman.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = FALSE   
	        tempIndex ++
	    ENDWHILE			
	#endif
		
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	    WHILE tempIndex < (ENUM_TO_INT(MAX_COMP_PERCENTAGE_ENTRIES))
	        g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[tempIndex].CP_Marked_As_Complete = FALSE   
	        tempIndex ++
	    ENDWHILE		
	#endif
	#endif
	
	RecalculateResultantPercentageTotal()
	
    #if IS_DEBUG_BUILD
        PRINTSTRING ("CP_PUBLIC - Completion Percentage Tracking has been completely reset!")
        PRINTNL()
    #endif

ENDPROC















/// PURPOSE:
///    Tells us whether a script has been registered for 100% completion total
/// PARAMS:
///    whichEntry - the mission or minigame completion enum
///    bShowDebug - if this is set print to tty [AG]:I need to set this to false for the rampage controller or TTY gets spammed
/// RETURNS:
///    
FUNC BOOL HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL (enumCompletionPercentageEntries whichEntry, BOOL bShowDebug = TRUE)



    IF g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Marked_As_Complete = TRUE

       
        IF (bShowDebug = TRUE)
            #if IS_DEBUG_BUILD
                PRINTNL()
                PRINTSTRING ("CP_PUBLIC - Entry is currently registered in CP Total. Queried Entry: ")
                PRINTINT (ENUM_TO_INT(whichEntry))
                PRINTSTRING (" which is ")
                PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
                PRINTNL()
            #endif
        ENDIF


        RETURN TRUE

    ELSE


 
        #if IS_DEBUG_BUILD

            IF (bShowDebug = TRUE)
                PRINTNL()
                PRINTSTRING ("CP_PUBLIC - This entry is not currently registered in CP Total. Queried Entry: ")
                PRINTINT (ENUM_TO_INT(whichEntry))
                PRINTSTRING (" which is ")
                PRINTSTRING (GET_STRING_FROM_TEXT_FILE(g_SavedGlobals.sCompletionPercentageData.g_CompletionPercentageList[whichEntry].CP_Label))
                PRINTNL()
            ENDIF
        #endif



        RETURN FALSE

    ENDIF


ENDFUNC












