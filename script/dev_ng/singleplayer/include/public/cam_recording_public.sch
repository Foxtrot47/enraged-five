USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

USING "script_player.sch"
USING "script_misc.sch"
USING "script_maths.sch"


STRUCT CAM_RECORDING_DATA
	ENTITY_INDEX entity
	VEHICLE_INDEX veh_cam
	
	#IF IS_DEBUG_BUILD
		WIDGET_GROUP_ID widget
	#ENDIF
	
	CAMERA_INDEX cam
	
	FLOAT f_pos_speed
	FLOAT f_rot_speed
	
	FLOAT f_current_rot_speed_x
	FLOAT f_current_rot_speed_z
	
	VECTOR v_current_offset
	VECTOR v_desired_offset
	
	VECTOR v_current_pos
	VECTOR v_desired_pos
	
	VECTOR v_current_rot
	
	INT i_cam_rec
	STRING str_cam_rec
	
	BOOL b_widgets_created
	BOOL b_inherit_roll
	BOOL b_show_helper_grid
	BOOL b_offset_is_relative
ENDSTRUCT

#IF IS_DEBUG_BUILD
	PROC SET_CAM_RECORDING_WIDGET_GROUP(CAM_RECORDING_DATA &s_cam_data, WIDGET_GROUP_ID parent_widget_group_id)
		s_cam_data.widget = parent_widget_group_id
	ENDPROC

	PROC CREATE_CAM_RECORDING_WIDGET(CAM_RECORDING_DATA &s_cam_data)
	    IF NOT s_cam_data.b_widgets_created
			IF (s_cam_data.widget != NULL)
				SET_CURRENT_WIDGET_GROUP(s_cam_data.widget)
			ELSE 
				SCRIPT_ASSERT("You have not called SET_CAM_RECORDING_WIDGET_GROUP in cam_recording_public.sch")
			ENDIF 
		
			IF s_cam_data.f_pos_speed = 0.0
				s_cam_data.f_pos_speed = 100.0
			ENDIF
			
			IF s_cam_data.f_rot_speed = 0.0
				s_cam_data.f_rot_speed = 110.0
			ENDIF
		
	        START_WIDGET_GROUP("Cam recording")
	            ADD_WIDGET_FLOAT_SLIDER("Movement speed", s_cam_data.f_pos_speed, 1.0, 500.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Rotation speed", s_cam_data.f_rot_speed, 1.0, 500.0, 1.0)
				ADD_WIDGET_BOOL("Show framing grid", s_cam_data.b_show_helper_grid)
	        STOP_WIDGET_GROUP()
	        
			s_cam_data.b_widgets_created = TRUE
			
			IF (s_cam_data.widget != NULL)
				CLEAR_CURRENT_WIDGET_GROUP(s_cam_data.widget)
			ENDIF 
	    ENDIF

	ENDPROC

	/// PURPOSE:
	///    Initialises the cam recording data and starts recording.
	/// PARAMS:
	///    s_cam_data - The cam recording data struct.
	///    veh_cam - The vehicle that will be used to record the camera information.
	///    v_start_pos - The start position for the camera.
	///    v_start_rot - The start rotation for the camera.
	///    f_start_fov - The start FOV for the camera.
	///    str_carrec_name - The name of the recording file to record to.
	///    i_carrec_index - The index of the recording file to record to.
	///    b_allow_overwrite - If TRUE, any recordings with the same name as the one given will be overwritten.
	PROC START_CAM_RECORDING(CAM_RECORDING_DATA &s_cam_data, VEHICLE_INDEX veh_cam, VECTOR v_start_pos, VECTOR v_start_rot, 
							 FLOAT f_start_fov, STRING str_carrec_name, INT i_carrec_index, BOOL b_allow_overwrite = FALSE)
		IF IS_VEHICLE_DRIVEABLE(veh_cam)
			s_cam_data.v_current_rot = v_start_rot
			s_cam_data.f_current_rot_speed_x = 0.0
			s_cam_data.f_current_rot_speed_z = 0.0
			
			s_cam_data.v_current_pos = v_start_pos
			s_cam_data.v_desired_pos = v_start_pos
			
			s_cam_data.veh_cam = veh_cam
			
			s_cam_data.i_cam_rec = i_carrec_index
			s_cam_data.str_cam_rec = str_carrec_name
			
			SET_ENTITY_COORDS_NO_OFFSET(s_cam_data.veh_cam, s_cam_data.v_current_pos)
			SET_ENTITY_ROTATION(s_cam_data.veh_cam, s_cam_data.v_current_rot)
			SET_ENTITY_COLLISION(s_cam_data.veh_cam, FALSE)
			SET_ENTITY_VISIBLE(s_cam_data.veh_cam, FALSE)
			SET_ENTITY_INVINCIBLE(s_cam_data.veh_cam, TRUE)
			SET_ENTITY_PROOFS(s_cam_data.veh_cam, TRUE, TRUE, TRUE, TRUE, TRUE)
			
			s_cam_data.cam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", s_cam_data.v_current_pos, s_cam_data.v_current_rot, f_start_fov, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)

			START_RECORDING_VEHICLE(s_cam_data.veh_cam, s_cam_data.i_cam_rec, s_cam_data.str_cam_rec, b_allow_overwrite)
			
			CREATE_CAM_RECORDING_WIDGET(s_cam_data)
		ENDIF				
	ENDPROC

	/// PURPOSE:
	///    Initialises the cam recording data relative to a given entity. This is useful if you want the camera to behave as if it was attached to something while recording (e.g. a car).
	/// PARAMS:
	///    s_cam_data - The cam recording data struct.
	///    entity_main - The entity that the cam will be attached to while recording.
	///    veh_cam - The vehicle that will be used to record the camera information.
	///    v_start_offset - The start offset relative to the given entity.
	///    v_start_rot - The start rotation for the camera.
	///    f_start_fov - The start FOV for the camera.
	///    str_carrec_name - The name of the recording file to record to.
	///    i_carrec_index - The index of the recording file to record to.
	///    b_allow_overwrite - If TRUE, any recordings with the same name as the one given will be overwritten.
	///    b_make_entity_invisible - Makes the entity the camera is attached to invisible, this is useful if the vehicle may obstruct the view of the camera.
	///    b_inherit_rotation - If TRUE the camera will inherit the rotation of the entity, if you need a more stable attachment set this to FALSE.
	PROC START_CAM_RECORDING_RELATIVE_TO_ENTITY(CAM_RECORDING_DATA &s_cam_data, ENTITY_INDEX entity_main, VEHICLE_INDEX veh_cam, 
												VECTOR v_start_offset, VECTOR v_start_rot, FLOAT f_start_fov, STRING str_carrec_name, INT i_carrec_index,
												BOOL b_allow_overwrite = FALSE, BOOL b_make_entity_invisible = FALSE, BOOL b_inherit_rotation = TRUE, BOOL b_relative_offset = TRUE)
		IF NOT IS_ENTITY_DEAD(entity_main)
		AND IS_VEHICLE_DRIVEABLE(veh_cam)
			s_cam_data.v_current_offset = v_start_offset
			s_cam_data.v_desired_offset = v_start_offset
			
			s_cam_data.entity = entity_main
			s_cam_data.b_inherit_roll = b_inherit_rotation
			s_cam_data.b_offset_is_relative = b_relative_offset
			
			SET_ENTITY_VISIBLE(entity_main, NOT b_make_entity_invisible)
			
			START_CAM_RECORDING(s_cam_data, veh_cam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entity_main, v_start_offset), v_start_rot, f_start_fov, 
								str_carrec_name, i_carrec_index, b_allow_overwrite)
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Call this every frame after calling START_CAM_RECORDING to update the recording.
	/// PARAMS:
	///    s_cam_data - The cam recording data struct.
	///    b_allow_cam_control - If TRUE, you will be able to move the camera around using the control pad while recording. The camera movements will be saved as part of the recording.
	PROC UPDATE_CAM_RECORDING(CAM_RECORDING_DATA &s_cam_data, BOOL b_allow_cam_control = TRUE)
		IF IS_VEHICLE_DRIVEABLE(s_cam_data.veh_cam)
			IF DOES_CAM_EXIST(s_cam_data.cam)
				IF b_allow_cam_control
					INT i_left_x, i_left_y, i_right_x, i_right_y
					GET_POSITION_OF_ANALOGUE_STICKS(PAD1, i_left_x, i_left_y, i_right_x, i_right_y)
					
					//Rotation speed
					IF IS_LOOK_INVERTED()
						i_right_y = -i_right_y
					ENDIF
					
					FLOAT f_desired_rot_speed_x = (TO_FLOAT(-i_right_y) / 128.0) * s_cam_data.f_rot_speed
					FLOAT f_desired_rot_speed_z = (TO_FLOAT(-i_right_x) / 128.0) * s_cam_data.f_rot_speed
					s_cam_data.f_current_rot_speed_x = s_cam_data.f_current_rot_speed_x + ((f_desired_rot_speed_x - s_cam_data.f_current_rot_speed_x) * 0.2)
					s_cam_data.f_current_rot_speed_z = s_cam_data.f_current_rot_speed_z + ((f_desired_rot_speed_z - s_cam_data.f_current_rot_speed_z) * 0.2)
					
					PRINTLN(f_desired_rot_speed_x, " ", f_desired_rot_speed_x, " ", s_cam_data.f_current_rot_speed_x, " ", s_cam_data.f_current_rot_speed_z)
					
					//Position
					VECTOR v_rot = s_cam_data.v_current_rot
					
					//If left bumper is pressed, have the cam move up/down instead of forward/backward
					IF IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER1)
						v_rot = <<s_cam_data.v_current_rot.x + 90.0, s_cam_data.v_current_rot.y, s_cam_data.v_current_rot.z>>
					ENDIF
					
					VECTOR v_dir = <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
					VECTOR v_forward = (v_dir * (TO_FLOAT(-i_left_y) / 128.0) * s_cam_data.f_pos_speed)
					
					//Sideways movement
					VECTOR v_temp_rot = <<s_cam_data.v_current_rot.y, s_cam_data.v_current_rot.x, s_cam_data.v_current_rot.z + 90.0>>
					VECTOR v_perp_dir = <<-SIN(v_temp_rot.z) * COS(v_temp_rot.x), COS(v_temp_rot.z) * COS(v_temp_rot.x), SIN(v_temp_rot.x)>>
					VECTOR v_sideways = (v_perp_dir * (TO_FLOAT(-i_left_x) / 128.0) * s_cam_data.f_pos_speed)
					
					s_cam_data.v_desired_pos = s_cam_data.v_desired_pos +@ (v_forward + v_sideways)
					s_cam_data.v_desired_offset = s_cam_data.v_desired_offset +@ (v_forward + v_sideways)
				ENDIF
				
				//Rotation update
				s_cam_data.v_current_rot.x = s_cam_data.v_current_rot.x +@ s_cam_data.f_current_rot_speed_x
				s_cam_data.v_current_rot.z = s_cam_data.v_current_rot.z +@ s_cam_data.f_current_rot_speed_z
				
				PRINTLN(s_cam_data.v_current_rot.x, " ", s_cam_data.v_current_rot.y, " ", s_cam_data.v_current_rot.z)
				
				//Position update
				s_cam_data.v_current_pos.x = s_cam_data.v_current_pos.x + ((s_cam_data.v_desired_pos.x - s_cam_data.v_current_pos.x) * 0.2)
				s_cam_data.v_current_pos.y = s_cam_data.v_current_pos.y + ((s_cam_data.v_desired_pos.y - s_cam_data.v_current_pos.y) * 0.2)
				s_cam_data.v_current_pos.z = s_cam_data.v_current_pos.z + ((s_cam_data.v_desired_pos.z - s_cam_data.v_current_pos.z) * 0.2)
				
				//Offset update
				s_cam_data.v_current_offset.x = s_cam_data.v_current_offset.x + ((s_cam_data.v_desired_offset.x - s_cam_data.v_current_offset.x) * 0.2)
				s_cam_data.v_current_offset.y = s_cam_data.v_current_offset.y + ((s_cam_data.v_desired_offset.y - s_cam_data.v_current_offset.y) * 0.2)
				s_cam_data.v_current_offset.z = s_cam_data.v_current_offset.z + ((s_cam_data.v_desired_offset.z - s_cam_data.v_current_offset.z) * 0.2)
				
				VECTOR v_final_rotation = s_cam_data.v_current_rot
				VECTOR v_final_position = s_cam_data.v_current_pos
				
				IF NOT IS_ENTITY_DEAD(s_cam_data.entity) 
					IF s_cam_data.b_inherit_roll
						VECTOR v_car_rot = GET_ENTITY_ROTATION(s_cam_data.entity)
						v_final_rotation = <<v_car_rot.x + s_cam_data.v_current_rot.x, v_car_rot.y + s_cam_data.v_current_rot.y, s_cam_data.v_current_rot.z>>
					ENDIF
					
					IF s_cam_data.b_offset_is_relative
						v_final_position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_cam_data.entity, s_cam_data.v_current_offset)
					ELSE
						v_final_position = GET_ENTITY_COORDS(s_cam_data.entity) + s_cam_data.v_current_offset
					ENDIF
				ENDIF
				
				SET_CAM_COORD(s_cam_data.cam, v_final_position)
				SET_CAM_ROT(s_cam_data.cam, v_final_rotation)
				
				SET_ENTITY_COORDS_NO_OFFSET(s_cam_data.veh_cam, v_final_position)
				SET_ENTITY_ROTATION(s_cam_data.veh_cam, v_final_rotation)
				
				IF s_cam_data.b_show_helper_grid
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				
					DRAW_DEBUG_LINE_2D(<<0.5, 0.4, 0.0>>, <<0.5, 0.6, 0.0>>, 0, 200, 0, 128)
					DRAW_DEBUG_LINE_2D(<<0.4, 0.5, 0.0>>, <<0.6, 0.5, 0.0>>, 0, 200, 0, 128)
					
					DRAW_DEBUG_LINE_2D(<<0.333, 0.0, 0.0>>, <<0.333, 1.0, 0.0>>, 0, 200, 0, 128)
					DRAW_DEBUG_LINE_2D(<<0.666, 0.0, 0.0>>, <<0.666, 1.0, 0.0>>, 0, 200, 0, 128)
					
					DRAW_DEBUG_LINE_2D(<<0.0, 0.333, 0.0>>, <<1.0, 0.333, 0.0>>, 0, 200, 0, 128)
					DRAW_DEBUG_LINE_2D(<<0.0, 0.666, 0.0>>, <<1.0, 0.666, 0.0>>, 0, 200, 0, 128)
				ENDIF
			ENDIF	
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Stops recording. If you don't call this, your recording will not be saved.
	/// PARAMS:
	///    s_cam_data - The cam recording data struct
	PROC STOP_CAM_RECORDING(CAM_RECORDING_DATA &s_cam_data)
		IF IS_VEHICLE_DRIVEABLE(s_cam_data.veh_cam)
			STOP_RECORDING_VEHICLE(s_cam_data.veh_cam)
			SET_ENTITY_VISIBLE(s_cam_data.veh_cam, TRUE)
			SET_ENTITY_COLLISION(s_cam_data.veh_cam, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(s_cam_data.entity)
			SET_ENTITY_VISIBLE(s_cam_data.entity, TRUE)
		ENDIF
		
		DESTROY_CAM(s_cam_data.cam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDPROC
#ENDIF

/// PURPOSE:
///    Requests any assets required for the cam recording.
/// PARAMS:
///    i_carrec_index - The index of the vehicle recording
///    str_carrec_name - The name of the vehicle recording
PROC REQUEST_CAM_RECORDING(INT i_carrec_index, STRING str_carrec_name)
	REQUEST_VEHICLE_RECORDING(i_carrec_index, str_carrec_name)
ENDPROC

/// PURPOSE:
///    Returns TRUE when all the camera recordings assets have loaded.
FUNC BOOL HAS_CAM_RECORDING_LOADED(INT i_carrec_index, STRING str_carrec_name)
	RETURN HAS_VEHICLE_RECORDING_BEEN_LOADED(i_carrec_index, str_carrec_name)
ENDFUNC

/// PURPOSE:
///    Removes any cam recording assets from memory.
PROC REMOVE_CAM_RECORDING(INT i_carrec_index, STRING str_carrec_name)
	REMOVE_VEHICLE_RECORDING(i_carrec_index, str_carrec_name)
ENDPROC

/// PURPOSE:
///    Call this every frame after starting playback to update.
/// PARAMS:
///    cam - The camera being rendered.
///    veh_cam - The vehicle currently playing the recording.
///    f_playback_speed - The desired playback speed.
PROC UPDATE_CAM_PLAYBACK(CAMERA_INDEX cam, VEHICLE_INDEX veh_cam, FLOAT f_playback_speed = 1.0)
	IF NOT IS_ENTITY_DEAD(veh_cam)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_cam)
			SET_PLAYBACK_SPEED(veh_cam, f_playback_speed)
			
			VECTOR v_pos = GET_ENTITY_COORDS(veh_cam)
			VECTOR v_rot = GET_ENTITY_ROTATION(veh_cam)
			
			SET_CAM_COORD(cam, v_pos)
			SET_CAM_ROT(cam, v_rot)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts playback of the cam recording.
/// PARAMS:
///    cam - The camera that will be rendered
///    veh_cam - The vehicle that will play the recording
///    str_carrec_name - The recording name
///    i_carrec_index - The recording index
///    f_cam_fov - The cam FOV: currently the recording doesn't store FOV information.
PROC START_CAM_PLAYBACK(CAMERA_INDEX cam, VEHICLE_INDEX veh_cam, STRING str_carrec_name, INT i_carrec_index)
	IF NOT IS_ENTITY_DEAD(veh_cam)
		SET_ENTITY_VISIBLE(veh_cam, FALSE)
		SET_ENTITY_COLLISION(veh_cam, FALSE)
		SET_ENTITY_INVINCIBLE(veh_cam, TRUE)
		SET_ENTITY_PROOFS(veh_cam, TRUE, TRUE, TRUE, TRUE, TRUE)
		START_PLAYBACK_RECORDED_VEHICLE(veh_cam, i_carrec_index, str_carrec_name)
		
		VECTOR v_pos = GET_ENTITY_COORDS(veh_cam)
		
		SET_CAM_COORD(cam, v_pos)
		SET_CAM_ROT(cam, GET_ENTITY_ROTATION(veh_cam))
	ELSE
		SCRIPT_ASSERT("START_CAM_PLAYBACK - Vehicle dead!")
	ENDIF
ENDPROC


/// PURPOSE:
///    Stops camera playback.
/// PARAMS:
///    veh_cam - The vehicle currently playing the recording.
PROC STOP_CAM_PLAYBACK(VEHICLE_INDEX veh_cam)
	IF IS_VEHICLE_DRIVEABLE(veh_cam)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_cam)
			STOP_PLAYBACK_RECORDED_VEHICLE(veh_cam)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Helper procedure for DISPLAY_CAM_PLAYBACK: draws a camera on screen out of debug lines.
PROC DRAW_CAMERA_BOX(VEHICLE_INDEX veh_cam, INT r, INT g, INT b, INT a)
	CONST_FLOAT CAM_LENGTH	0.3
	CONST_FLOAT CAM_WIDTH	0.15
	CONST_FLOAT CAM_HEIGHT	0.15
	
	IF IS_VEHICLE_DRIVEABLE(veh_cam)	
		VECTOR v_front_top_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_front_top_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH, CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_front_bottom_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_front_bottom_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH, CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_back_top_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_back_top_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH, -CAM_LENGTH, CAM_HEIGHT>>)
		VECTOR v_back_bottom_left_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_back_bottom_right_corner = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH, -CAM_LENGTH, -CAM_HEIGHT>>)
		VECTOR v_cone_centre = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<0.0, CAM_LENGTH, 0.0>>)
		VECTOR v_cone_top_left = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_top_right = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_bottom_left = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<-CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
		VECTOR v_cone_bottom_right = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh_cam, <<CAM_WIDTH * 0.75, CAM_LENGTH * 2.0, -CAM_HEIGHT * 0.75>>)
		
		DRAW_DEBUG_LINE(v_front_top_left_corner, v_front_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_top_right_corner, v_front_bottom_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_right_corner, v_front_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_left_corner, v_front_top_left_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_back_top_left_corner, v_back_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_top_right_corner, v_back_bottom_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_bottom_right_corner, v_back_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_back_bottom_left_corner, v_back_top_left_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_front_top_left_corner, v_back_top_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_top_right_corner, v_back_top_right_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_left_corner, v_back_bottom_left_corner, r, g, b, a)
		DRAW_DEBUG_LINE(v_front_bottom_right_corner, v_back_bottom_right_corner, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_top_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_top_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_bottom_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_centre, v_cone_bottom_right, r, g, b, a)
		
		DRAW_DEBUG_LINE(v_cone_top_left, v_cone_top_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_top_right, v_cone_bottom_right, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_bottom_right, v_cone_bottom_left, r, g, b, a)
		DRAW_DEBUG_LINE(v_cone_bottom_left, v_cone_top_left, r, g, b, a)
	ENDIF
ENDPROC

/// PURPOSE:
///    Displays the current camera position for a given vehicle currently playing a camera recording, and the overall route of the camera.
PROC DISPLAY_CAM_PLAYBACK(VEHICLE_INDEX veh_cam)
	IF IS_VEHICLE_DRIVEABLE(veh_cam)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh_cam)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
			DISPLAY_PLAYBACK_RECORDED_VEHICLE(veh_cam, RDM_WHOLELINE)
			DRAW_CAMERA_BOX(veh_cam, 0, 0, 255, 255)
		ENDIF
	ENDIF
ENDPROC

#ENDIF	//	IS_DEBUG_BUILD

