//╒═════════════════════════════════════════════════════════════════════════════╕
//│                           Cutscene Public Header                            │
//╞═════════════════════════════════════════════════════════════════════════════╡
//│                                                                             │
//│       AUTHOR:         Ben Rollinson / Lawrence Kerr                         │
//│       DATE:           08/06/11                                              │
//│       DESCRIPTION:    Public script commands related to cutscenes.          │                                 
//│                                                                             │
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "cellphone_public.sch"
USING "commands_cutscene.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"

#IF IS_DEBUG_BUILD
	USING "debug_channels_structs.sch"
#ENDIF

/// PURPOSE:    Configures scripted systems to be safe for cutscenes running.
///    
PROC SET_SCRIPTS_SAFE_FOR_CUTSCENE(BOOL bEnable, BOOL bCallCascadeShadowsInitSession = TRUE, BOOL bSetMultiHeadBlinders = TRUE, BOOL bMultiHeadBlindersInstant = FALSE, BOOL bJoinAsSpectator = FALSE, BOOL bPreventInvincibilityChanges = FALSE, BOOL bAllowCallsOverScene = FALSE)

    #IF IS_DEBUG_BUILD
        IF bEnable
            CPRINTLN(DEBUG_MISSION, "SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE) called by ", GET_THIS_SCRIPT_NAME(), ".")
        ELSE
            CPRINTLN(DEBUG_MISSION, "SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE) called by ", GET_THIS_SCRIPT_NAME(), ".")
        ENDIF
    #ENDIF
    
    //--------------CUTSCENE STARTING ROUTINES------------------
    IF bEnable
        SPECIAL_ABILITY_DEACTIVATE_FAST(PLAYER_ID())
        
        // Prevent player from being attacked by randoms they upset before the cut
        SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
        
        // Prevent player being arrested
        SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), TRUE)
    
        //Ensure the cellphone won't display signifiers on screen during a cutscene.
        HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE (TRUE)
		
		//Pause the feed queue.
		THEFEED_FLUSH_QUEUE()
		THEFEED_PAUSE()
        
        //Steve T cellphone and dialogue checks.
        //Make sure any cellphone call is terminated and the on_screen cellphone is put away before starting the cutscene.
        //This is essentially a hang_up_and_put_away_phone routine.
        IF (ENUM_TO_INT(g_Cellphone.PhoneDS)) > (ENUM_TO_INT(PDS_AWAY)) //If phone is onscreen...
		AND NOT bAllowCallsOverScene
            IF IS_MOBILE_PHONE_CALL_ONGOING()
                STOP_SCRIPTED_CONVERSATION (FALSE) //don't finish last line of conversation as this is a call....
            ENDIF

            IF NOT IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY()

                g_Cellphone.PhoneDS = PDS_AWAY

                #if IS_DEBUG_BUILD
                    cdPrintnl()
                    cdPrintstring("STATE ASSIGNMENT 35987. SET_SCRIPTS_SAFE_FOR_CUTSCENE assigns PDS_AWAY")
                    cdPrintnl()   
                #endif

            ENDIF
			
            g_ConversationStatus = CONV_STATE_HANGUPAWAY //tell dialogue handler to clean up a call and phone using HungupAwayCleanup specifically.
           
            //May have check for conv_state_free here...
        ENDIF
		
		// Set blinders for multi screen setup
		SET_MULTIHEAD_SAFE(TRUE,bMultiHeadBlindersInstant, bSetMultiHeadBlinders)
		        
        //Inform the mission stat tacking system that a cutscene is now running.
        g_bMissionStatSystemSequenceStatus = TRUE
		
		//Tell the browser to close if it is open 
		g_bBrowserQuitMessage = TRUE
		
		g_bScriptsSetSafeForCutscene = TRUE
        
    //--------------CUTSCENE ENDING ROUTINES------------------
    ELSE
        //Turn back on cellphone signifiers.
        HIDE_CELLPHONE_SIGNIFIERS_FOR_CUTSCENE (FALSE)
		
		//Unpause the feed queue.
		THEFEED_RESUME()
		
        //Inform the mission stat tracking system that a cutscene has just ended.
        g_bMissionStatSystemSequenceStatus = FALSE
        
		IF bCallCascadeShadowsInitSession
	        //Reset the effect of any calls to CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SNAP or 
	        //CASCADE_SHADOWS_SET_WORLD_HEIGHT_UPDATE
	        CASCADE_SHADOWS_INIT_SESSION()
		ENDIF
        
        // Allow peds to interact with the player as normal
        SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
        SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		
		// Reset blinders for multi screen setup
		SET_MULTIHEAD_SAFE(FALSE,bMultiHeadBlindersInstant, bSetMultiHeadBlinders)
				
		// Turn off player invinciblity
		// This has been added so the flow can keep the player invincible when triggering missions
		// That start with cut-scenes, to stop them dying between mission launch + intro start
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			AND NOT bJoinAsSpectator
			AND NOT bPreventInvincibilityChanges
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
			AND NOT bJoinAsSpectator
			AND NOT bPreventInvincibilityChanges
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			ENDIF
		ENDIF
		
		g_bScriptsSetSafeForCutscene = FALSE
    ENDIF
    
ENDPROC


/// PURPOSE:	Checks if any cutscene has streamed in. If this ever returns FALSE it will also fade the screen 
///    			out and put scripts into a failsafe mode that will encourage the cutscene to load faster. 
///    			The failsafe mode will automatically be cleared as soon as the cutscene starts playing.
///    
FUNC BOOL HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
	BOOL bCutsceneLoaded = HAS_CUTSCENE_LOADED()
	
	IF NOT g_bCutsceneFailsafeActive
		IF NOT bCutsceneLoaded
			CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Cutscene check in script ", GET_THIS_SCRIPT_NAME(), " hasn't loaded in time. Activating failsafe to encourage streaming.")
			g_bCutsceneFailsafeActive = TRUE
		ENDIF
	ENDIF

	RETURN bCutsceneLoaded
ENDFUNC


/// PURPOSE:	Checks if a specific cutscene has streamed in. If this ever returns FALSE it will also fade the screen 
///    			out and put scripts into a failsafe mode that will encourage the cutscene to load faster. The failsafe 
///    			mode will automatically be cleared as soon as the cutscene starts playing.
///   
FUNC BOOL HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(STRING SceneName)
	BOOL bCutsceneLoaded = HAS_THIS_CUTSCENE_LOADED(SceneName)
	
	IF NOT g_bCutsceneFailsafeActive
		IF NOT bCutsceneLoaded
			CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Cutscene check in script ", GET_THIS_SCRIPT_NAME(), " hasn't loaded in time. Activating failsafe to encourage streaming.")
			g_bCutsceneFailsafeActive = TRUE
		ENDIF
	ENDIF

	RETURN bCutsceneLoaded
ENDFUNC


/// PURPOSE:	Force the game into a cutscene failsafe mode that fades the screen out and configures game
///    			state to encourage a cutscene to stream in. This would usually be set by 
///    			HAS_CUTSCENE_LOADED_WITH_FAILSAFE. The failsafe 
///    			mode will automatically be cleared as soon as the cutscene starts playing.
///   
PROC MANUALLY_ACTIVATE_CUTSCENE_FAILSAFE()
	IF NOT g_bCutsceneFailsafeActive
		CPRINTLN(DEBUG_SYSTEM, "<CUT_FAILSAFE> Script ", GET_THIS_SCRIPT_NAME(), " has manually activated cutscene failsafe mode.")
		g_bCutsceneFailsafeActive = TRUE
	ENDIF
ENDPROC


//Streams in a mocap cutscene in advance so that it starts seamlessly when you want to trigger the mocap.
//Also handles removing the cutscene from memory if the player decides to drive out of the area.
//Lawrence Kerr  
proc mocap_streaming_system(vector mocap_start_pos, float start_streaming_distance, float remove_mocap_distance, string mocap_file)

    if (get_distance_between_coords(mocap_start_pos, get_entity_coords(player_ped_id())) < start_streaming_distance)
        
        request_cutscene(mocap_file)
    
    else 
        
        if is_cutscene_active() or has_cutscene_loaded()
            if (get_distance_between_coords(mocap_start_pos, get_entity_coords(player_ped_id())) > remove_mocap_distance)
                
                remove_cutscene()
                
            endif 
        endif 
        
    endif 


endproc 


#IF IS_DEBUG_BUILD

	PROC DEBUG_DISPLAY_UNAUTHORIZED_CUTSCENE_SCREEN(STRING paramCutsceneName)
		IF NOT IS_STRING_NULL_OR_EMPTY(paramCutsceneName)
			CPRINTLN(DEBUG_FLOW_CUTS, "<UNAUTH-CUT> Script ", GET_THIS_SCRIPT_NAME(), " started an unathorized cutscene screen for ", paramCutsceneName, ".")
			g_txtUnauthCutName = paramCutsceneName
			g_iUnauthCutTimeDisplayed = 0
		ELSE
			SCRIPT_ASSERT("DEBUG_DISPLAY_UNAUTHORIZED_CUTSCENE_SCREEN: Cutscene name was null or empty.")
		ENDIF
	ENDPROC

#ENDIF
