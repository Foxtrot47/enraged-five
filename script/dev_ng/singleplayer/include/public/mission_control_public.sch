USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_graphics.sch"
USING "blip_enums.sch"
USING "mission_control_private.sch"
USING "mission_stat_public.sch"
USING "blip_control_public.sch"
USING "flow_mission_data_public.sch" 
USING "flow_help_public.sch"
USING "player_ped_public.sch"
USING "vehicle_gen_private.sch"
USING "shop_public.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "flow_debug_game.sch"
#ENDIF










// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	mission_control_public.sch
//		AUTHOR			:	Ak
//		DESCRIPTION		:	Allows communication with the mission launching candidate
//							controller and the mission triggering system
//							also contains 	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

 
FUNC BOOL IS_MISSION_STATE_SAFE_FOR_FAMILY_SCENE()
	SWITCH g_OnMissionState
		CASE MISSION_TYPE_STORY
		CASE MISSION_TYPE_STORY_PREP
		CASE MISSION_TYPE_STORY_FRIENDS
		CASE MISSION_TYPE_RANDOM_EVENT
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE //anything else is allowed
ENDFUNC
 
 
PROC DO_MISSION_PASSED_OR_FAILED_STAT_UPDATE(BOOL passed,INT MissionIndex)
	IF MissionIndex != -1
		//g_sMissionStaticData[iMissionDataIndex].triggerCharBitset
		IF Is_Mission_Triggerable_By_Character(g_sMissionStaticData[MissionIndex].triggerCharBitset,CHAR_MICHAEL)
			STATSENUM e = SP0_MISSIONS_FAILED
			IF passed
				e = SP0_MISSIONS_PASSED
			ENDIF
			INT InStat
			STAT_GET_INT(e,InStat)
			InStat++
			STAT_SET_INT(e,InStat)
		ENDIF
		IF Is_Mission_Triggerable_By_Character(g_sMissionStaticData[MissionIndex].triggerCharBitset,CHAR_FRANKLIN)
			STATSENUM e = SP1_MISSIONS_FAILED
			IF passed
				e = SP1_MISSIONS_PASSED
			ENDIF
			INT InStat
			STAT_GET_INT(e,InStat)
			InStat++
			STAT_SET_INT(e,InStat)
		ENDIF
		IF Is_Mission_Triggerable_By_Character(g_sMissionStaticData[MissionIndex].triggerCharBitset,CHAR_TREVOR)
			STATSENUM e = SP2_MISSIONS_FAILED
			IF passed
				e = SP2_MISSIONS_PASSED
			ENDIF
			INT InStat
			STAT_GET_INT(e,InStat)
			InStat++
			STAT_SET_INT(e,InStat)
		ENDIF
	ENDIF
ENDPROC




// *****************************************************************************************
// **************************************Mission triggering controls************************

//TODO
/*
BOOL g_bMissionTriggerFired
INT g_iMissionTriggerableToTrigger

INT g_iRegisteredMissionTriggers
CONST_INT MAX_MISSION_TRIGGERS 64
TRIGGERABLE_MISSION_INDEX_STRUCT g_TriggerableMissions[MAX_MISSION_TRIGGERS]
*/
//Register contact point mission // STATIC_BLIP_NAME_ENUM , blip to get location from

//DLC-specific version of main function below
#IF USE_CLF_DLC
PROC REGISTER_BASE_MISSION_TRIGGER_CLF(INT &trigger_id_out, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	
	CPRINTLN(DEBUG_TRIGGER, "Configuring base trigger settings.") 
	
	// Find a free index.
	INT freeindex = -1
	INT i = 0
	REPEAT g_iRegisteredMissionTriggers i
		IF g_TriggerableMissionsTU[i].bUsed = FALSE
			freeindex = i
		ELSE
		ENDIF
	ENDREPEAT
	
	// If no indexes available to reuse then expand the list.
	IF freeindex = -1
		freeindex = g_iRegisteredMissionTriggers
		g_iRegisteredMissionTriggers++
		
		IF g_iRegisteredMissionTriggers > MAX_MISSION_TRIGGERS_TU
			SCRIPT_ASSERT("Max mission triggers CLF exceeded.")
		ENDIF
	ENDIF
	
	// Setup core data.
	g_TriggerableMissionsTU[freeindex].bUsed = TRUE
	g_TriggerableMissionsTU[freeindex].bLaunchMe = FALSE
	g_TriggerableMissionsTU[freeindex].eStrand = strand
	g_TriggerableMissionsTU[freeindex].eCandidateType = mission_type_involved
	g_TriggerableMissionsTU[freeindex].iCandidateIndex = NO_CANDIDATE_ID
	g_TriggerableMissionsTU[freeindex].eMissionID = missionID
	
	// Setup character specific settings.
	g_TriggerableMissionsTU[freeindex].iTriggerableCharBitset = char_flags
	CPRINTLN(DEBUG_TRIGGER, "Valid characters for trigger [Michael:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_MICHAEL)),
							" Franklin:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_FRANKLIN)),
							" Trevor:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_TREVOR)),
							"].")

	//Turn on the leave area flag for this mission as this trigger is activated.
	//Safeguards the player from triggering missions that activate where they are standing.
	IF NOT IS_BIT_SET(g_sMissionStaticData[missionID].settingsBitset, MF_INDEX_NO_LEAVE_AREA_CHECK)
		#IF IS_DEBUG_BUILD
			IF NOT g_bSceneAutoTrigger
				Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
			ENDIF
		#ENDIF
		#IF IS_FINAL_BUILD
			Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
		#ENDIF
	ENDIF
	
	trigger_id_out = freeindex
	CPRINTLN(DEBUG_TRIGGER, "Trigger sucessfully registered in index ", trigger_id_out, ".")
ENDPROC
#ENDIF
#IF USE_NRM_DLC
PROC REGISTER_BASE_MISSION_TRIGGER_NRM(INT &trigger_id_out, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	
	CPRINTLN(DEBUG_TRIGGER, "Configuring base trigger settings.") 
	
	// Find a free index.
	INT freeindex = -1
	INT i = 0
	REPEAT g_iRegisteredMissionTriggers i
		IF g_TriggerableMissionsTU[i].bUsed = FALSE
			freeindex = i
		ELSE
		ENDIF
	ENDREPEAT
	
	// If no indexes available to reuse then expand the list.
	IF freeindex = -1
		freeindex = g_iRegisteredMissionTriggers
		g_iRegisteredMissionTriggers++
		
		IF g_iRegisteredMissionTriggers > MAX_MISSION_TRIGGERS_TU
			SCRIPT_ASSERT("Max mission triggers NRM exceeded.")
		ENDIF
	ENDIF
	
	// Setup core data.
	g_TriggerableMissionsTU[freeindex].bUsed = TRUE
	g_TriggerableMissionsTU[freeindex].bLaunchMe = FALSE
	g_TriggerableMissionsTU[freeindex].eStrand = strand
	g_TriggerableMissionsTU[freeindex].eCandidateType = mission_type_involved
	g_TriggerableMissionsTU[freeindex].iCandidateIndex = NO_CANDIDATE_ID
	g_TriggerableMissionsTU[freeindex].eMissionID = missionID
	
	// Setup character specific settings.
	g_TriggerableMissionsTU[freeindex].iTriggerableCharBitset = char_flags
	CPRINTLN(DEBUG_TRIGGER, "Valid characters for trigger [Michael:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_MICHAEL)),
							" Franklin:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_FRANKLIN)),
							" Trevor:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_TREVOR)),
							"].")

	//Turn on the leave area flag for this mission as this trigger is activated.
	//Safeguards the player from triggering missions that activate where they are standing.
	IF NOT IS_BIT_SET(g_sMissionStaticData[missionID].settingsBitset, MF_INDEX_NO_LEAVE_AREA_CHECK)
		#IF IS_DEBUG_BUILD
			IF NOT g_bSceneAutoTrigger
				Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
			ENDIF
		#ENDIF
		#IF IS_FINAL_BUILD
			Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
		#ENDIF
	ENDIF
	
	trigger_id_out = freeindex
	CPRINTLN(DEBUG_TRIGGER, "Trigger sucessfully registered in index ", trigger_id_out, ".")
ENDPROC
#ENDIF


/// PURPOSE:
///    Register everything needed for all types of mission triggers. To be called by more specific trigger type registration procedures.
/// PARAMS:
///    trigger_id_out - this will store the index of your created trigger, used to perform operations on
///    					triggers
///    mission_type_involved - The mission type that is going to launch on triggering this.
///    							Used for conflict resolution.
PROC REGISTER_BASE_MISSION_TRIGGER(INT &trigger_id_out, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	
#IF USE_CLF_DLC
	REGISTER_BASE_MISSION_TRIGGER_CLF(trigger_id_out,missionID,strand,mission_type_involved,char_flags)
#ENDIF
#IF USE_NRM_DLC
	REGISTER_BASE_MISSION_TRIGGER_NRM(trigger_id_out,missionID,strand,mission_type_involved,char_flags)
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	CPRINTLN(DEBUG_TRIGGER, "Configuring base trigger settings.") 
	
	// Find a free index.
	INT freeindex = -1
	INT i = 0
	REPEAT g_iRegisteredMissionTriggers i
		IF g_TriggerableMissions[i].bUsed = FALSE
			freeindex = i
		ELSE
		ENDIF
	ENDREPEAT
	
	// If no indexes available to reuse then expand the list.
	IF freeindex = -1
		freeindex = g_iRegisteredMissionTriggers
		g_iRegisteredMissionTriggers++
		
		IF g_iRegisteredMissionTriggers > MAX_MISSION_TRIGGERS
			SCRIPT_ASSERT("Max mission triggers exceeded.")
		ENDIF
	ENDIF
	
	// Setup core data.
	g_TriggerableMissions[freeindex].bUsed = TRUE
	g_TriggerableMissions[freeindex].bLaunchMe = FALSE
	g_TriggerableMissions[freeindex].eStrand = strand
	g_TriggerableMissions[freeindex].eCandidateType = mission_type_involved
	g_TriggerableMissions[freeindex].iCandidateIndex = NO_CANDIDATE_ID
	g_TriggerableMissions[freeindex].eMissionID = missionID
	
	// Setup character specific settings.
	g_TriggerableMissions[freeindex].iTriggerableCharBitset = char_flags
	CPRINTLN(DEBUG_TRIGGER, "Valid characters for trigger [Michael:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_MICHAEL)),
							" Franklin:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_FRANKLIN)),
							" Trevor:",
							IS_BIT_SET(char_flags, ENUM_TO_INT(CHAR_TREVOR)),
							"].")

	//Turn on the leave area flag for this mission as this trigger is activated.
	//Safeguards the player from triggering missions that activate where they are standing.
	IF NOT IS_BIT_SET(g_sMissionStaticData[missionID].settingsBitset, MF_INDEX_NO_LEAVE_AREA_CHECK)
		#IF IS_DEBUG_BUILD
			IF NOT g_bSceneAutoTrigger
				Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
			ENDIF
		#ENDIF
		#IF IS_FINAL_BUILD
			Set_Leave_Area_Flag_For_Mission(missionID, TRUE)
		#ENDIF
	ENDIF
	
	trigger_id_out = freeindex
	CPRINTLN(DEBUG_TRIGGER, "Trigger sucessfully registered in index ", trigger_id_out, ".")
#ENDIF
#ENDIF
ENDPROC


#IF USE_CLF_DLC
PROC REGISTER_SCENE_MISSION_TRIGGER_CLF(INT &trigger_id_out, STATIC_BLIP_NAME_ENUM blip_involved, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "Setting up new SCENE mission trigger: [", 
								GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID), ", ",
								DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), "].")
	#ENDIF

	// Setup base trigger.
	REGISTER_BASE_MISSION_TRIGGER(trigger_id_out, missionID, strand, mission_type_involved, char_flags)

	// Scene trigger specific setup.
	g_TriggerableMissionsTU[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SCENE
	g_TriggerableMissionsTU[trigger_id_out].eBlip = blip_involved
	
	// Reset the blip visibility status.
	RESET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved)
	
	// Setup the blip to be character specific.
	IF g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset != 7 //BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR
		// Make blip specific to who it is for
		BOOL firstset = FALSE
		BOOL secondset = FALSE

		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_MICHAEL,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Michael.")
			firstset = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_FRANKLIN,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Franklin.")
			IF firstset = TRUE
				secondset = TRUE
			ENDIF
			firstset = TRUE
		ENDIF

		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_TREVOR)) AND (NOT secondset)
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_TREVOR,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Trevor.")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now set to be none character specific.")
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved, FALSE, CHAR_MICHAEL)
	ENDIF
	
	// Set the blips mission type.
	IF CAN_STORY_MISSION_LAUNCH_WITH_FRIENDS(missionID)
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY_FRIENDS)
	ELSE
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY)
	ENDIF
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC REGISTER_SCENE_MISSION_TRIGGER_NRM(INT &trigger_id_out, STATIC_BLIP_NAME_ENUM blip_involved, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "Setting up new SCENE mission trigger: [", 
								GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID), ", ",
								DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), "].")
	#ENDIF

	// Setup base trigger.
	REGISTER_BASE_MISSION_TRIGGER(trigger_id_out, missionID, strand, mission_type_involved, char_flags)

	// Scene trigger specific setup.
	g_TriggerableMissionsTU[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SCENE
	g_TriggerableMissionsTU[trigger_id_out].eBlip = blip_involved
	
	// Reset the blip visibility status.
	RESET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved)
	
	// Setup the blip to be character specific.
	IF g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset != 7 //BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR
		// Make blip specific to who it is for
		BOOL firstset = FALSE
		BOOL secondset = FALSE

		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_MICHAEL,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Michael.")
			firstset = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_FRANKLIN,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Franklin.")
			IF firstset = TRUE
				secondset = TRUE
			ENDIF
			firstset = TRUE
		ENDIF

		IF IS_BIT_SET(g_TriggerableMissionsTU[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_TREVOR)) AND (NOT secondset)
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_TREVOR,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Trevor.")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now set to be none character specific.")
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved, FALSE, CHAR_MICHAEL)
	ENDIF
	
	// Set the blips mission type.
	IF CAN_STORY_MISSION_LAUNCH_WITH_FRIENDS(missionID)
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY_FRIENDS)
	ELSE
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY)
	ENDIF
ENDPROC
#ENDIF

PROC REGISTER_SCENE_MISSION_TRIGGER(INT &trigger_id_out, STATIC_BLIP_NAME_ENUM blip_involved, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
#IF USE_CLF_DLC
	REGISTER_SCENE_MISSION_TRIGGER_CLF(trigger_id_out,  blip_involved,  missionID,  strand,  mission_type_involved, char_flags)
#ENDIF
#IF USE_NRM_DLC
	REGISTER_SCENE_MISSION_TRIGGER_NRM(trigger_id_out,  blip_involved,  missionID,  strand,  mission_type_involved, char_flags)
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "Setting up new SCENE mission trigger: [", 
								GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID), ", ",
								DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), "].")
	#ENDIF

	// Setup base trigger.
	REGISTER_BASE_MISSION_TRIGGER(trigger_id_out, missionID, strand, mission_type_involved, char_flags)

	// Scene trigger specific setup.
	g_TriggerableMissions[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SCENE
	g_TriggerableMissions[trigger_id_out].eBlip = blip_involved
	
	// Reset the blip visibility status.
	RESET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved)
	
	// Setup the blip to be character specific.
	IF g_TriggerableMissions[trigger_id_out].iTriggerableCharBitset != 7 //BIT_MICHAEL|BIT_FRANKLIN|BIT_TREVOR
		// Make blip specific to who it is for
		BOOL firstset = FALSE
		BOOL secondset = FALSE

		IF IS_BIT_SET(g_TriggerableMissions[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_MICHAEL))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_MICHAEL,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Michael.")
			firstset = TRUE
		ENDIF
		
		IF IS_BIT_SET(g_TriggerableMissions[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_FRANKLIN))
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_FRANKLIN,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Franklin.")
			IF firstset = TRUE
				secondset = TRUE
			ENDIF
			firstset = TRUE
		ENDIF

		IF IS_BIT_SET(g_TriggerableMissions[trigger_id_out].iTriggerableCharBitset, ENUM_TO_INT(CHAR_TREVOR)) AND (NOT secondset)
			SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved,TRUE,CHAR_TREVOR,firstset)
			CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now visible for Trevor.")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_TRIGGER, DEBUG_GET_STRING_NAME_OF_STATIC_BLIP(ENUM_TO_INT(blip_involved)), " now set to be none character specific.")
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(blip_involved, FALSE, CHAR_MICHAEL)
	ENDIF
	
	// Set the blips mission type.
	IF CAN_STORY_MISSION_LAUNCH_WITH_FRIENDS(missionID)
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY_FRIENDS)
	ELSE
		SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(blip_involved, MISSION_TYPE_STORY)
	ENDIF
#ENDIF
#ENDIF
ENDPROC

PROC REGISTER_SWITCH_MISSION_TRIGGER(INT &trigger_id_out, SP_MISSIONS missionID, STRANDS strand, m_enumMissionCandidateTypeID mission_type_involved, INT char_flags = -1)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_TRIGGER, "Setting up new SWITCH mission trigger: [", 
								GET_SP_MISSION_DISPLAY_STRING_FROM_ID(missionID), "].")
	#ENDIF
	
	// Setup base trigger.
	Register_Base_Mission_Trigger(trigger_id_out, missionID, strand, mission_type_involved, char_flags)
	
#IF USE_CLF_DLC
	g_TriggerableMissionsTU[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SWITCH
#ENDIF

#IF USE_NRM_DLC
	g_TriggerableMissionsTU[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SWITCH
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	g_TriggerableMissions[trigger_id_out].eType = MISSION_TRIGGER_TYPE_SWITCH
#ENDIF
#ENDIF
ENDPROC


//check for "should I trigger?" //
/// PURPOSE:
///    Checks if the conditions to activate the trigger with the registered index have been met
/// RETURNS:
///    FALSE - mission not triggered
///    TRUE - The trigger has been fired and the mission is clear to launch, remember to
///    		  inform the system that you are done with it using Triggered_Mission_Has_Terminated
#IF USE_CLF_DLC
FUNC BOOL Has_Mission_Trigger_Triggered_CLF(INT trigger_id_in)
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Has_Mission_Trigger_Triggered AGT: Invalid trigger ID passed.")
		RETURN FALSE
	ENDIF

	IF g_TriggerableMissionsTU[trigger_id_in].bLaunchMe
		g_TriggerableMissionsTU[trigger_id_in].bLaunchMe = FALSE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC
#ENDIF

#IF USE_NRM_DLC
FUNC BOOL Has_Mission_Trigger_Triggered_NRM(INT trigger_id_in)
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Has_Mission_Trigger_Triggered NRM: Invalid trigger ID passed.")
		RETURN FALSE
	ENDIF

	IF g_TriggerableMissionsTU[trigger_id_in].bLaunchMe
		g_TriggerableMissionsTU[trigger_id_in].bLaunchMe = FALSE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC
#ENDIF

FUNC BOOL Has_Mission_Trigger_Triggered(INT trigger_id_in)
#IF USE_CLF_DLC
	RETURN Has_Mission_Trigger_Triggered_CLF( trigger_id_in)
#ENDIF
#IF USE_NRM_DLC
	RETURN Has_Mission_Trigger_Triggered_NRM( trigger_id_in)
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS-1))
		SCRIPT_ASSERT("Has_Mission_Trigger_Triggered: Invalid trigger ID passed.")
		RETURN FALSE
	ENDIF

	IF g_TriggerableMissions[trigger_id_in].bLaunchMe
		g_TriggerableMissions[trigger_id_in].bLaunchMe = FALSE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
#ENDIF
#ENDIF
ENDFUNC


//Terminated trigger started mission
/// PURPOSE:
///    Informs the mission triggering system that the mission that previously launched after
///		recieving approval from Has_Mission_Trigger_Triggered has now terminated.
///    
/// PARAMS:
///    trigger_id_in - the trigger index that was previously launched with Has_Mission_Trigger_Triggered
#IF USE_CLF_DLC
PROC Triggered_Mission_Has_Terminated_CLF(INT trigger_id_in)
	IF NOT (IS_CURRENTLY_ON_MISSION_TO_TYPE())
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Cannot be fired when not on mission.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Invalid trigger index passed.")
		EXIT
	ENDIF
	
	g_bMissionTriggerFired = FALSE
	
	// Note that this is also changed directly in Mission_Over.
	 g_OnMissionState = MISSION_TYPE_OFF_MISSION// = FALSE
	
	// Forcibly reset the firing list.
	g_TriggerableMissionsTU[trigger_id_in].iCandidateIndex = NO_CANDIDATE_ID // This ID isn't useful anymore.
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissionsTU[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission termination informed for trigger index ", trigger_id_in, ".")
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC Triggered_Mission_Has_Terminated_NRM(INT trigger_id_in)
	IF NOT (IS_CURRENTLY_ON_MISSION_TO_TYPE())
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Cannot be fired when not on mission.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Invalid trigger index passed.")
		EXIT
	ENDIF
	
	g_bMissionTriggerFired = FALSE
	
	// Note that this is also changed directly in Mission_Over.
	 g_OnMissionState = MISSION_TYPE_OFF_MISSION// = FALSE
	
	// Forcibly reset the firing list.
	g_TriggerableMissionsTU[trigger_id_in].iCandidateIndex = NO_CANDIDATE_ID // This ID isn't useful anymore.
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissionsTU[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission termination informed for trigger index ", trigger_id_in, ".")
ENDPROC
#ENDIF

PROC Triggered_Mission_Has_Terminated(INT trigger_id_in)
#IF USE_CLF_DLC
	Triggered_Mission_Has_Terminated_CLF( trigger_id_in)
#ENDIF
#IF USE_NRM_DLC
	Triggered_Mission_Has_Terminated_NRM( trigger_id_in)
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF NOT (IS_CURRENTLY_ON_MISSION_TO_TYPE())
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Cannot be fired when not on mission.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS-1))
		SCRIPT_ASSERT("Triggered_Mission_Has_Terminated: Invalid trigger index passed.")
		EXIT
	ENDIF
	
	g_bMissionTriggerFired = FALSE
	
	// Note that this is also changed directly in Mission_Over.
	 g_OnMissionState = MISSION_TYPE_OFF_MISSION// = FALSE
	
	// Forcibly reset the firing list.
	g_TriggerableMissions[trigger_id_in].iCandidateIndex = NO_CANDIDATE_ID // This ID isn't useful anymore.
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissions[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissions[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission termination informed for trigger index ", trigger_id_in, ".")
#ENDIF
#ENDIF
ENDPROC

#IF USE_CLF_DLC
PROC Remove_Mission_Trigger_CLF(INT trigger_id_in)

	IF  ((g_bMissionTriggerFired OR IS_CURRENTLY_ON_MISSION_TO_TYPE()) AND (g_iMissionTriggerableToTrigger = trigger_id_in))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Cannot remove the launched trigger.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Invalid trigger index passed.")
		EXIT
	ENDIF
	IF NOT (g_TriggerableMissionsTU[trigger_id_in].bUsed)
		CPRINTLN(DEBUG_TRIGGER, "Unused trigger index passed. Ignoring remove request.")
		EXIT
	ENDIF
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissionsTU[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission trigger at index ", trigger_id_in, " was removed.")
ENDPROC
#ENDIF

#IF USE_NRM_DLC
PROC Remove_Mission_Trigger_NRM(INT trigger_id_in)

	IF  ((g_bMissionTriggerFired OR IS_CURRENTLY_ON_MISSION_TO_TYPE()) AND (g_iMissionTriggerableToTrigger = trigger_id_in))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Cannot remove the launched trigger.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS_TU-1))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Invalid trigger index passed.")
		EXIT
	ENDIF
	IF NOT (g_TriggerableMissionsTU[trigger_id_in].bUsed)
		CPRINTLN(DEBUG_TRIGGER, "Unused trigger index passed. Ignoring remove request.")
		EXIT
	ENDIF
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissionsTU[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissionsTU[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission trigger at index ", trigger_id_in, " was removed.")
ENDPROC
#ENDIF

// Purge a specific mission trigger.
PROC Remove_Mission_Trigger(INT trigger_id_in)
#IF USE_CLF_DLC
	Remove_Mission_Trigger_CLF(trigger_id_in)
#ENDIF

#IF USE_NRM_DLC
	Remove_Mission_Trigger_NRM(trigger_id_in)
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	IF  ((g_bMissionTriggerFired OR IS_CURRENTLY_ON_MISSION_TO_TYPE()) AND (g_iMissionTriggerableToTrigger = trigger_id_in))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Cannot remove the launched trigger.")
		EXIT
	ENDIF
	IF (trigger_id_in < 0) OR (trigger_id_in > (MAX_MISSION_TRIGGERS-1))
		SCRIPT_ASSERT("Remove_Mission_Trigger: Invalid trigger index passed.")
		EXIT
	ENDIF
	IF NOT (g_TriggerableMissions[trigger_id_in].bUsed)
		CPRINTLN(DEBUG_TRIGGER, "Unused trigger index passed. Ignoring remove request.")
		EXIT
	ENDIF
	
	//Ensure the trigger's blip is disabled.
	IF g_TriggerableMissions[trigger_id_in].eBlip != STATIC_BLIP_NAME_DUMMY_FINAL
		SET_STATIC_BLIP_ACTIVE_STATE(g_TriggerableMissions[trigger_id_in].eBlip, FALSE)
	ENDIF
	
	Reset_Mission_Trigger(trigger_id_in)
	
	CPRINTLN(DEBUG_TRIGGER, "Mission trigger at index ", trigger_id_in, " was removed.")

#ENDIF
#ENDIF
ENDPROC


FUNC INT GET_AVAILABLE_MISSION_INDEX_FOR_MISSION(SP_MISSIONS paramMissionID)
	INT iAvailableIndex
#IF USE_CLF_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU iAvailableIndex
		IF g_availableMissionsTU[iAvailableIndex].index != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.coreVars[g_availableMissionsTU[iAvailableIndex].index].iValue1 = ENUM_TO_INT(paramMissionID)
				RETURN iAvailableIndex
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF
#IF USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE_TU iAvailableIndex
		IF g_availableMissionsTU[iAvailableIndex].index != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.coreVars[g_availableMissionsTU[iAvailableIndex].index].iValue1 = ENUM_TO_INT(paramMissionID)
				RETURN iAvailableIndex
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF
#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSIONS_AVAILABLE iAvailableIndex
		IF g_availableMissions[iAvailableIndex].index != ILLEGAL_ARRAY_POSITION
			IF g_flowUnsaved.coreVars[g_availableMissions[iAvailableIndex].index].iValue1 = ENUM_TO_INT(paramMissionID)
				RETURN iAvailableIndex
			ENDIF
		ENDIF
	ENDREPEAT
#ENDIF
#ENDIF
	RETURN -1
ENDFUNC


FUNC INT GET_TRIGGER_INDEX_FOR_MISSION(SP_MISSIONS paramMissionID)
	INT iTriggerIndex
#IF USE_CLF_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].eMissionID = paramMissionID
			RETURN iTriggerIndex
		ENDIF
	ENDREPEAT
#ENDIF

#IF USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS_TU iTriggerIndex
		IF g_TriggerableMissionsTU[iTriggerIndex].eMissionID = paramMissionID
			RETURN iTriggerIndex
		ENDIF
	ENDREPEAT
#ENDIF

#IF NOT USE_CLF_DLC
#IF NOT USE_NRM_DLC
	REPEAT MAX_MISSION_TRIGGERS iTriggerIndex
		IF g_TriggerableMissions[iTriggerIndex].eMissionID = paramMissionID
			RETURN iTriggerIndex
		ENDIF
	ENDREPEAT
#ENDIF
#ENDIF
RETURN -1
ENDFUNC


ENUM SCRIPT_TYPE
	ST_STORY_MISSION,
	ST_STORY_MISSION_SEAMLESS_PREP,
	ST_RANDOM_CHARACTER,
	ST_RCM_VEHICLE,
	ST_RANDOM_EVENT,
	ST_FAMILY_SCENE,
	ST_MINIGAME,
	ST_AMBIENT_FRIEND,
	ST_PLANNING_BOARD,
	ST_SPMC_INSTANCE//mission creator single player mission instance
ENDENUM

#IF IS_DEBUG_BUILD
	FUNC STRING GET_SCRIPT_TYPE_DEBUG_STRING(SCRIPT_TYPE paramScriptType)
		SWITCH paramScriptType
			CASE ST_STORY_MISSION				RETURN "STORY_MISSION"					BREAK
			CASE ST_STORY_MISSION_SEAMLESS_PREP	RETURN "STORY_MISSION_SEAMLESS_PREP"	BREAK
			CASE ST_RANDOM_CHARACTER			RETURN "RANDOM_CHARACTER"				BREAK
			CASE ST_RCM_VEHICLE					RETURN "RCM_VEHICLE"					BREAK
			CASE ST_RANDOM_EVENT				RETURN "RANDOM_EVENT"					BREAK
			CASE ST_FAMILY_SCENE				RETURN "FAMILY_SCENE"					BREAK
			CASE ST_MINIGAME					RETURN "MINIGAME"						BREAK
			CASE ST_AMBIENT_FRIEND				RETURN "AMBIENT_FRIEND"					BREAK
			CASE ST_PLANNING_BOARD				RETURN "PLANNING_BOARD"					BREAK
			CASE ST_SPMC_INSTANCE				RETURN "SPMC_INSTANCE"					BREAK	
		ENDSWITCH
		RETURN "INVALID"
	ENDFUNC
#ENDIF

/// PURPOSE:	Contains lists of all fiddly checks that should restrict a certain type of script from triggering from open world gameplay.
///    
/// PARAMS:
///    scriptType 	An enum defining the type of script that is checking if it is safe to trigger.
/// RETURNS:		TRUE if it's safe to trigger. FALSE if it is not.
///    
FUNC BOOL IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(SCRIPT_TYPE scriptType)

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
				enumCharacterList ePlayerPed = GET_CURRENT_PLAYER_PED_ENUM()
				IF NOT IS_PLAYER_PED_PLAYABLE(ePlayerPed)
					RETURN FALSE
				ENDIF
			
				//NB. Keeping all the checks as distinct lists to make this more readable.
				SWITCH scriptType
					CASE ST_SPMC_INSTANCE // made these the same as the single player for now
					CASE ST_STORY_MISSION
						IF NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
						OR IS_PED_RAGDOLL(PLAYER_PED_ID())
						OR IS_PED_FALLING(PLAYER_PED_ID())
						OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PLAYER_CLIMBING(PLAYER_ID())
						OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR g_bPlayerIsInTaxi
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR IS_RESULT_SCREEN_DISPLAYING()
						OR Is_Player_Timetable_Scene_In_Progress()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
						
							#IF IS_DEBUG_BUILD
								IF NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player not ready for cutscene.")
								ENDIF
								IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in air.")
								ENDIF
								IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player getting into vehicle.")
								ENDIF
								IF IS_PED_RAGDOLL(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player ragdolling.")
								ENDIF
								IF IS_PED_FALLING(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player falling.")
								ENDIF
								IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player being arrested.")
								ENDIF
								IF IS_PLAYER_CLIMBING(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player climbing.")
								ENDIF
								IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in combat.")
								ENDIF
								IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player switch in progress.")
								ENDIF
								IF g_bPlayerIsInTaxi
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in taxi.")
								ENDIF
								IF g_bIsOnRampage
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player on rampage.")
								ENDIF
								IF IS_TRANSITION_ACTIVE()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") SP<->MP transition active.")
								ENDIF
								IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Transition HUD displaying.")
								ENDIF
								IF IS_RESULT_SCREEN_DISPLAYING()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Result screen displaying.")
								ENDIF
								IF IS_PLAYER_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is in a shop.")
								ENDIF
								IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is browsing items in a shop.")
								ENDIF
								IF Is_Player_Timetable_Scene_In_Progress()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Timetable scene in progress.")
								ENDIF
								IF g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") End of mission communication queued.")
								ENDIF
							#ENDIF
						
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_STORY_MISSION_SEAMLESS_PREP
						IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_RESULT_SCREEN_DISPLAYING()
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR Is_Player_Timetable_Scene_In_Progress()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
						
							#IF IS_DEBUG_BUILD
								IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player being arrested.")
								ENDIF
								IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Switch in progress.")
								ENDIF
								IF g_bIsOnRampage
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player on a rampage.")
								ENDIF
								IF IS_TRANSITION_ACTIVE()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") MP transition is active.")
								ENDIF
								IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") MP transition HUD displaying.")
								ENDIF
								IF IS_RESULT_SCREEN_DISPLAYING()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Result screen displaying.")
								ENDIF
								IF IS_PLAYER_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is in a shop.")
								ENDIF
								IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is browsing items in a shop.")
								ENDIF
								IF Is_Player_Timetable_Scene_In_Progress()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Timetable scene in progress.")
								ENDIF
								IF g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") End of mission communication queued.")
								ENDIF
							#ENDIF
							
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_RANDOM_CHARACTER
						IF NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
						OR IS_PED_RAGDOLL(PLAYER_PED_ID())
						OR IS_PED_FALLING(PLAYER_PED_ID())
						OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PLAYER_CLIMBING(PLAYER_ID())
						OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR g_bPlayerIsInTaxi
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_RESULT_SCREEN_DISPLAYING()
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR Is_Player_Timetable_Scene_In_Progress()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
						OR g_iCallInProgress != -1
						
							#IF IS_DEBUG_BUILD
								IF NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player not ready for cutscene.")
								ENDIF
								IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in air.")
								ENDIF
								IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player getting into vehicle.")
								ENDIF
								IF IS_PED_RAGDOLL(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player ragdolling.")
								ENDIF
								IF IS_PED_FALLING(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player falling.")
								ENDIF
								IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player being arrested.")
								ENDIF
								IF IS_PLAYER_CLIMBING(PLAYER_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player climbing.")
								ENDIF
								IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in combat.")
								ENDIF
								IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player switch in progress.")
								ENDIF
								IF g_bPlayerIsInTaxi
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player in taxi.")
								ENDIF
								IF g_bIsOnRampage
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player on rampage.")
								ENDIF
								IF IS_TRANSITION_ACTIVE()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") SP<->MP transition active.")
								ENDIF
								IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Transition HUD displaying.")
								ENDIF
								IF IS_RESULT_SCREEN_DISPLAYING()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Result screen displaying.")
								ENDIF
								IF IS_PLAYER_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is in a shop.")
								ENDIF
								IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player is browsing items in a shop.")
								ENDIF
								IF Is_Player_Timetable_Scene_In_Progress()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Timetable scene in progress.")
								ENDIF
								IF g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") End of mission communication queued.")
								ENDIF
								IF g_iCallInProgress != -1
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") The comms controller has a call in progress.")
								ENDIF
							#ENDIF
						
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_RCM_VEHICLE
						/* 
						   The following checks prevent an RCM mission from triggering
						   when the player enters a vehicle from the passenger seat.
						*/
						//IF NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
						//OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						//OR IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
						//OR IS_PLAYER_CLIMBING(PLAYER_ID())
						
						IF IS_PED_RAGDOLL(PLAYER_PED_ID())
						OR IS_PED_FALLING(PLAYER_PED_ID())
						OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PED_IN_COMBAT(PLAYER_PED_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR g_bPlayerIsInTaxi
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR Is_Player_Timetable_Scene_In_Progress()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
							RETURN FALSE
						ENDIF
					BREAK

					CASE ST_RANDOM_EVENT
						IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR Is_Player_Timetable_Scene_In_Progress()
						OR IS_MISSION_LEADIN_ACTIVE()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
						
							#IF IS_DEBUG_BUILD
								IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Switch in progress.")
								ENDIF
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Player has a wanted level.")
								ENDIF
								IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") MP transition HUD displaying.")
								ENDIF
								IF Is_Player_Timetable_Scene_In_Progress()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Timetable scene in progress.")
								ENDIF
								IF IS_MISSION_LEADIN_ACTIVE()
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") Mission leadin is active.")
								ENDIF
								IF g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
									CDEBUG1LN(DEBUG_TRIGGER, 	"SAFE_TO_TRIGGER_SCRIPT_TYPE(",GET_SCRIPT_TYPE_DEBUG_STRING(scriptType), 
																") End of mission communication queued.")
								ENDIF
							#ENDIF
							
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_FAMILY_SCENE
						IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
//						OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
//						OR Is_Player_Timetable_Scene_In_Progress()
						OR IS_MISSION_LEADIN_ACTIVE()
//						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
//						OR NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(g_OnMissionState)
						OR IS_AUTOSAVE_REQUEST_IN_PROGRESS()
							RETURN FALSE
						ENDIF
						
						IF IS_PLAYER_SWITCH_IN_PROGRESS()
						AND (GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT)
						AND (GET_PLAYER_SWITCH_STATE() < SWITCH_STATE_JUMPCUT_DESCENT)
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_MINIGAME						
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
							IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
							OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
							OR IS_PED_RAGDOLL(PLAYER_PED_ID())
							OR IS_PED_FALLING(PLAYER_PED_ID())
							OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
							OR IS_PLAYER_CLIMBING(PLAYER_ID())
							OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
							OR g_bIsOnRampage
							OR IS_TRANSITION_ACTIVE()
							OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
							OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
							OR IS_RESULT_SCREEN_DISPLAYING()
							OR Is_Player_Timetable_Scene_In_Progress()
							OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
								#IF IS_DEBUG_BUILD
									IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player in combat.")
									ENDIF
									IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player wanted.")
									ENDIF
									IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player in air.")
									ENDIF
									IF IS_PED_RAGDOLL(PLAYER_PED_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player ragdoll.")
									ENDIF
									IF IS_PED_FALLING(PLAYER_PED_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player falling.")
									ENDIF
									IF IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player under arrest.")
									ENDIF
									IF IS_PLAYER_CLIMBING(PLAYER_ID())
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player climbing.")
									ENDIF
									IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player switching.")
									ENDIF
									IF g_bIsOnRampage
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player on rampage.")
									ENDIF
									IF IS_TRANSITION_ACTIVE()
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player transitioning.")
									ENDIF
									IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player displaying script transition.")
									ENDIF
									IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player in shop.")
									ENDIF
									IF IS_RESULT_SCREEN_DISPLAYING()
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Player displaying results screen.")
									ENDIF
									IF Is_Player_Timetable_Scene_In_Progress()
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Timetable scene in progress.")
									ENDIF
									IF g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
										CDEBUG1LN(DEBUG_TRIGGER, "SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) Something about comms.")
									ENDIF
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ENDIF
					BREAK
					
					CASE ST_AMBIENT_FRIEND
						IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
						OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						OR NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
						OR NOT IS_SCREEN_FADED_IN()
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						OR IS_PED_RAGDOLL(PLAYER_PED_ID())
						OR IS_PED_FALLING(PLAYER_PED_ID())
						OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR g_bPlayerIsInTaxi
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR g_isPlayerDrunk
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_RESULT_SCREEN_DISPLAYING()
						OR IS_MISSION_LEADIN_ACTIVE()
						OR Is_Player_Timetable_Scene_In_Progress()
						OR g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerPed] = CPR_VERY_HIGH
							RETURN FALSE
						ENDIF
					BREAK
					
					CASE ST_PLANNING_BOARD
						IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
						OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						OR NOT IS_PLAYER_READY_FOR_CUTSCENE(PLAYER_ID())
						OR NOT IS_SCREEN_FADED_IN()
						OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						OR IS_PED_RAGDOLL(PLAYER_PED_ID())
						OR IS_PED_FALLING(PLAYER_PED_ID())
						OR IS_PED_SWIMMING(PLAYER_PED_ID())
						OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID())
						OR IS_PLAYER_CLIMBING(PLAYER_ID())
						OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						OR g_bPlayerIsInTaxi
						OR g_bIsOnRampage
						OR IS_TRANSITION_ACTIVE()
						OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						OR IS_RESULT_SCREEN_DISPLAYING()
						OR IS_MISSION_LEADIN_ACTIVE()
						OR IS_PLAYER_IN_ANY_SHOP()
						OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						OR Is_Player_Timetable_Scene_In_Progress()
							RETURN FALSE
						ENDIF
					BREAK
					
				ENDSWITCH
				
			ELSE
				RETURN FALSE
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ELSE	
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC


PROC DRAW_MARKER_FOR_TRIGGER_LOCATION(VECTOR paramLocation)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IS_ENTITY_AT_COORD(PLAYER_PED_ID(), paramLocation, <<0.1, 0.1, LOCATE_SIZE_HEIGHT>>, TRUE)
	ENDIF
//	DRAW_MARKER(	MARKER_ARROW, 
//					<<paramLocation.X, paramLocation.Y, paramLocation.Z - 0.25>>,
//					<<0,0,0>>,
//					<<0,0,0>>,
//					<<2*LOCATE_SIZE_MISSION_TRIGGER, 2*LOCATE_SIZE_MISSION_TRIGGER, 1.5>>,
//					255, 0, 0, 255)	
ENDPROC






///////////////////////////
// Playstats mission nanny calls

PROC SCRIPT_PLAYSTATS_MISSION_STARTED(STRING pMissionName, INT variant = 0, INT checkpoint = 0)
//	IF COMPARE_STRINGS(pMissionName,g_sNanMissionString) = 0
//		PLAYSTATS_MISSION_OVER(pMissionName, 0,0, TRUE)
//	ENDIF
	PRINTLN("PSTATS: attempting to start ",pMissionName)
	IF NOT IS_STRING_NULL_OR_EMPTY(g_sNanMissionString)
		
		//a mission is running, pretend it ended
		PRINTLN("PSTATS: ending ",g_sNanMissionString)
		PLAYSTATS_MISSION_OVER(g_sNanMissionString, 0,0, FALSE,TRUE)
		g_sNanMissionString = ""
	ENDIF
	
	g_sNanMissionString = pMissionName
	PLAYSTATS_MISSION_STARTED(pMissionName, variant, checkpoint, IS_REPEAT_PLAY_ACTIVE())
	PRINTLN("PSTATS: starting ",g_sNanMissionString)
	
	
ENDPROC


PROC SCRIPT_PLAYSTATS_MISSION_OVER(STRING pMissionName, INT variant = 0, INT checkpoint = 0, BOOL failed = FALSE, BOOL canceled = FALSE)
	PRINTLN("PSTATS: ending, stored : ",g_sNanMissionString, " passed ", pMissionName)
	IF IS_STRING_NULL_OR_EMPTY(g_sNanMissionString)
		PRINTLN("PSTATS: ending, stored none running")
		EXIT//can't end a mission when none is running
	ENDIF
	
	
	IF COMPARE_STRINGS(pMissionName,g_sNanMissionString) != 0
		PRINTLN("PSTATS: trying to end a different mission to the one that started: ", pMissionName ," != ", g_sNanMissionString)
		EXIT//can't end a mission you didn't start unless you're starting a new one over it
	ENDIF
	
	PRINTLN("PSTATS: calling PLAYSTATS_MISSION_OVER with ", pMissionName)
	PLAYSTATS_MISSION_OVER(pMissionName, variant, checkpoint, failed, canceled, g_bShitskipAccepted)
	g_sNanMissionString = ""
ENDPROC




















