USING "globals.sch"
USING "commands_script.sch"
USING "cellphone_public.sch"
 USING "selector_public.sch"

CONST_INT NEW_CONTEXT_INTENTION -1 //used directly as -1 in BOUNTY_STRUCT in x:\gta5\script\dev\multiplayer\globals\mp_globals_ambience.sch




FUNC BOOL CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS()

		IF g_bBrowserVisible
			RETURN TRUE		
		ENDIF

		IF NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX()) 
			//////PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : playing")
			RETURN TRUE
		ENDIF
		IF (IS_PHONE_ONSCREEN())
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : phone")
			RETURN TRUE
		ENDIF
		IF (IS_CUTSCENE_PLAYING())
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : cutscene")
			RETURN TRUE
		ENDIF
		IF (g_bSuppressContextActionsThisFrame)
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : external")
			RETURN TRUE
		ENDIF
		/*//Removed after discussion about specific uses of the MP cutscene condition
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(GET_PLAYER_INDEX())
				RETURN TRUE
			ENDIF
		ENDIF
		*/
		IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)//weapon wheel
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : wheel suppress")
			RETURN TRUE
		ENDIF
		
		IF IS_SELECTOR_ONSCREEN()
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : char select suppress")
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : selector")
			RETURN TRUE
		ENDIF
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : switch")
			RETURN TRUE
		ENDIF
		
		IF IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONhUD)
			//PRINTLN("CONTEXT_CONTROLLER_SUPPRESSION_CONDITIONS : transition")
			RETURN TRUE
		ENDIF
		
	RETURN FALSE
ENDFUNC




FUNC INT GET_CONTEXT_INTENTION_ARRAY_INDEX(INT intention_index)

	IF intention_index < 0
		RETURN -1
	ENDIF
	INT i = 0
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].iID = intention_index
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


/// PURPOSE:
///    Allows scripts to release thier intention to use the context button
/// PARAMS:
///    intention_index - a valid index returned from REGISTER_NEW_CONTEXT_INTENTION
PROC RELEASE_CONTEXT_INTENTION(INT &intention_index)
	//PRINTSTRING("RELEASE_CONTEXT_INTENTION:Attempting to release context")
	//PRINTNL()
	
	IF intention_index = NEW_CONTEXT_INTENTION
		EXIT
	ENDIF
	
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
	IF arraypos = -1
		
		PRINTLN("RELEASE_CONTEXT_INTENTION: passed an index that does not exist, value was : ",intention_index)
		intention_index = NEW_CONTEXT_INTENTION
		EXIT
	ENDIF
	
	
	//is it a valid index?
	IF arraypos > NEW_CONTEXT_INTENTION AND arraypos < MAX_CONTEXT_INTENTION
		PRINTSTRING("RELEASE_CONTEXT_INTENTION:Index valid")	
		PRINTNL()
		//is it used?
		IF g_IntentionList[arraypos].bUsed
			PRINTSTRING("RELEASE_CONTEXT_INTENTION:Used valid")
			PRINTNL()
			//clean up the state
			//IF g_IntentionList[intention_index].bActive
				PRINTSTRING("RELEASE_CONTEXT_INTENTION: registering context for deletion")
				PRINTNL()
				//release the index
				//g_IntentionList[intention_index].bUsed = FALSE
				//g_IntentionList[intention_index].bActive = FALSE
				//g_IntentionList[intention_index].bAccepted = FALSE
				//g_IntentionList[intention_index].iPriority = 0
				
				//if it was active then clear help, and deactivate
				/*
				IF g_IntentionList[intention_index].bActive 
					IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(intention_index)
						CLEAR_HELP(FALSE)
					ENDIF
					
				ENDIF*/
				//g_IntentionList[intention_index].bNoString = TRUE
				//g_IntentionList[intention_index].bActive = FALSE
				g_IntentionList[arraypos].bToBeDeleted = TRUE
				//g_IntentionList[arraypos].iID = -1
				//g_IntentionList[intention_index].bUsed = FALSE
				intention_index = NEW_CONTEXT_INTENTION
				EXIT
			//ENDIF
			
		ENDIF	
		
	#IF IS_DEBUG_BUILD
		//log string
		 
		 PRINTLN("RELEASE_CONTEXT_INTENTION complete: if this is spamming continuously then script:'",GET_THIS_SCRIPT_NAME(),"' is creating and destroying a context request every frame, this is an indication that it is being used badly.")
	#ENDIF
		
	ENDIF
	//PRINTSTRING("RELEASE_CONTEXT_INTENTION: Overwriting passed index value to NEW_CONTEXT_INTENTION")	
	//PRINTNL()
	//otherwise! 
	intention_index = NEW_CONTEXT_INTENTION
	//SCRIPT_ASSERT("RELEASE_CONTEXT_INTENTION: release failed")
	//PRINTSTRING("RELEASE_CONTEXT_INTENTION: release failed, context may have already been released or never exsisted")
	//PRINTNL()

	
ENDPROC


//CONTEXT_INTENTION g_IntentionList[MAX_CONTEXT_INTENTION]

/// PURPOSE:
///    Used by a script to register it's intention to use the context button
/// PARAMS:
///    intention_index	An int to return an ID for subsequent context actions
///    priority			An int value use to compare priority in the case of a deadlock
///    helptext 		The help text to display if top of the priority queue. NB. Leave a ~a~ in this help where the context button icon should appear.
///    noHelp 			Don't display any help text for this intention.
PROC REGISTER_CONTEXT_INTENTION(INT &intention_index, 
								CONTEXT_PRIORITY_ENUM priority,
								STRING helptext,
								BOOL noHelp = FALSE,STRING additionalSubStr = NULL, CONTEXT_STYLE_ENUM contextStyle = CS_DEFAULT,
								BOOL subStringIsPlayerName = FALSE)

	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controller")) < 1
		PRINTLN("REGISTER_CONTEXT_INTENTION: Context controller not running, skipping, should only see this if triggering a repeat ")
	ENDIF
	

	//contexts may not be asked for during a switch
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		IF NOT (intention_index = NEW_CONTEXT_INTENTION)
			RELEASE_CONTEXT_INTENTION(intention_index)
		ENDIF
		EXIT
	ENDIF

	IF NOT (intention_index = NEW_CONTEXT_INTENTION)
		#IF IS_DEBUG_BUILD
		PRINTLN("REGISTER_CONTEXT_INTENTION: intention_index != NEW_CONTEXT_INTENTION, checking index:")
		
		INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
			IF arraypos = -1
				PRINTLN("REGISTER_CONTEXT_INTENTION: intention_index was not an existing intention")
			ELSE
				//PRINTLN("REGISTER_CONTEXT_INTENTION: intention_index was an already registered ID ", intention_index)
				//SCRIPT_ASSERT("REGISTER_CONTEXT_INTENTION: Script passing an already registered ID, implies missed cleanup/release")
				RELEASE_CONTEXT_INTENTION(intention_index)
				EXIT
			ENDIF
			//intention_index = NEW_CONTEXT_INTENTION
		#ENDIF
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("REGISTER_CONTEXT_INTENTION: New context intention registered by script ", GET_THIS_SCRIPT_NAME())
		IF noHelp
			PRINTLN(" with no help label.")
		ELSE
			PRINTLN(" with help label [", helpText, "].")
		ENDIF
	#ENDIF

	INT i = 0

	//find a free index
	REPEAT MAX_CONTEXT_INTENTION i
		IF NOT g_IntentionList[i].bUsed
				//register it
				g_IntentionList[i].bUsed = TRUE
				g_IntentionList[i].iID = g_iContextIntentionIDGenerator
				++g_iContextIntentionIDGenerator
				g_IntentionList[i].bActive = FALSE
				g_IntentionList[i].bPickedUp = FALSE
				g_IntentionList[i].bAccepted = FALSE
				g_IntentionList[i].iPriority = ENUM_TO_INT(priority)
				g_IntentionList[i].helpLabel = helptext
				g_IntentionList[i].bNoString = noHelp
				g_IntentionList[i].RegisteredBy = GET_ID_OF_THIS_THREAD()
				g_IntentionList[i].bToBeDeleted = FALSE
				g_IntentionList[i].iStyle = ENUM_TO_INT(contextStyle)
				
				
				IF NOT IS_STRING_NULL_OR_EMPTY(additionalSubStr)
					g_IntentionList[i].bAdditionalHelp = TRUE
					g_IntentionList[i].additionalHelpLabel = additionalSubStr
					g_IntentionList[i].subStringIsPlayerName = subStringIsPlayerName
				ELSE
					g_IntentionList[i].bAdditionalHelp = FALSE
					g_IntentionList[i].subStringIsPlayerName = FALSE 
				ENDIF
				
				#IF IS_DEBUG_BUILD
					//log string
					g_IntentionList[i].DEBUG_sname = GET_THIS_SCRIPT_NAME()
						//log string
						//dump log
						PRINTSTRING("Dumping context system current state:")
						PRINTNL()
						INT j = 0
						REPEAT MAX_CONTEXT_INTENTION j
							IF g_IntentionList[j].bUsed
								PRINTINT(j)
								PRINTSTRING(" : ")
								PRINTSTRING(g_IntentionList[j].DEBUG_sname)
								
								PRINTSTRING(" : Prio - ") 
								
								PRINTINT(g_IntentionList[j].iPriority)
								PRINTSTRING(" : Help stored - ") 
								PRINTSTRING(g_IntentionList[j].helpLabel)
								
								IF g_IntentionList[j].bAdditionalHelp
									PRINTSTRING(" : Additional help substring - ") 
									PRINTSTRING(g_IntentionList[j].additionalHelpLabel)
								ENDIF
								
								
								PRINTNL()
							ENDIF
						ENDREPEAT
					
						IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controller")) < 1
							SCRIPT_ASSERT("Context controller request was triggered while script was not running!")
						ENDIF
					
					
				#ENDIF
				
				//TODO handle help string
				//RETURN i
				intention_index = g_IntentionList[i].iID
			EXIT
		ENDIF
	ENDREPEAT
	//
	
	#IF IS_DEBUG_BUILD
		//log string
		//dump log
		PRINTSTRING("Context system has no space left, listing potential culprits.")
		PRINTNL()
		PRINTSTRING("Multiple entries from the same script or scripts that are no longer running will indicate potential failure to clean up:")
		PRINTNL()
		PRINTNL()
		i = 0
	
		REPEAT MAX_CONTEXT_INTENTION i
			IF g_IntentionList[i].bUsed
				PRINTINT(i)
				PRINTSTRING(" : ")
				PRINTSTRING(g_IntentionList[i].DEBUG_sname)
				PRINTSTRING(" : ID ")
				PRINTINT(g_IntentionList[i].iID)
				PRINTNL()
			ENDIF
		ENDREPEAT
		
		
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("REGISTER_CONTEXT_INTENTION: failed, check the log for potential culprits")
	#ENDIF
	
	#IF IS_FINAL_BUILD
		SCRIPT_ASSERT("REGISTER_CONTEXT_INTENTION: failed, duplicate this assert in debug mode to see list of potential culprits")
	#ENDIF
	
	//otherwise return fail
	//RETURN -1
ENDPROC

FUNC BOOL IS_CONTEXT_INTENTION_TOP(INT intention_ID)

	IF intention_ID = -1
		RETURN FALSE
	ENDIF



	//g_IntentionList[MAX_CONTEXT_INTENTION]
		
	//find if this is top
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_ID)
	IF arraypos = -1
		PRINTLN("IS_CONTEXT_INTENTION_TOP intention_ID not in buffers")
		RETURN FALSE
	ENDIF
	
	
	INT i = 0
	INT iTopPrio = -1
	INT iTopPrioIndex = -1
	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed
			//IF (NOT g_IntentionList[i].bToBeDeleted) AND (NOT g_IntentionList[i].)
				IF g_IntentionList[i].iPriority > iTopPrio
					iTopPrioIndex = i
				ENDIF
			//ENDIF
		ENDIF
	ENDREPEAT
	
	IF(iTopPrioIndex = arraypos)
		RETURN TRUE
	ENDIF
	
/*
	IF(g_iCurrentlyDisplayingContextINDEX = -1)
		PRINTLN("IS_CONTEXT_INTENTION_TOP nothing on top")
		RETURN FALSE
	ENDIF
	
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_ID)
	IF arraypos = -1
		PRINTLN("IS_CONTEXT_INTENTION_TOP not in buffers")
		RETURN FALSE
	ENDIF

	
	
	IF g_iCurrentlyDisplayingContextINDEX = arraypos
		RETURN TRUE
	ENDIF


	PRINTLN("IS_CONTEXT_INTENTION_TOP Default fallthrough")
	RETURN FALSE
*/

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///     Allows registered scripts to check for the context button being activated for them.
/// PARAMS:
///    intention_index - an index returned from REGISTER_CONTEXT_INTENTION
///    justMode - perform a "just pressed check on the button?"
/// RETURNS:
///    
FUNC BOOL HAS_CONTEXT_BUTTON_TRIGGERED(INT intention_index, BOOL justMode = TRUE)
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
	IF arraypos = -1
		CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, ")  arraypos = -1")
		RETURN FALSE
	ENDIF
	
	//these may not be safe for multiplayer
	IF NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())
		CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Acceptance prevented due to (NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())")
		RETURN FALSE
	ENDIF
	IF (IS_PHONE_ONSCREEN())
		CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Acceptance prevented due to IS_PHONE_ONSCREEN()")
		RETURN FALSE
	ENDIF
	IF (IS_CUTSCENE_PLAYING())
		CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Acceptance prevented due to IS_CUTSCENE_PLAYING()")
		RETURN FALSE
	ENDIF
	//CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Checking for triggered")
	
	//is this index valid
	IF (arraypos > NEW_CONTEXT_INTENTION) AND (arraypos < MAX_CONTEXT_INTENTION)
		//is it registered
		IF (g_IntentionList[arraypos].bUsed = TRUE) AND (g_IntentionList[arraypos].bActive = TRUE)
			IF justMode
				/*//suspect this creates a problem if the script checking is before the context controller
				IF NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)	
					CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Acceptance failed due to just mode check")
					RETURN FALSE
				ENDIF 
				*/
				IF g_IntentionList[arraypos].bPickedUp
					//true has already been returned on this index
					CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): 'just' mode return suppressed")
					RETURN FALSE
				ENDIF
				
			ENDIF
			
			//has it been triggered?
			g_IntentionList[arraypos].bAccepted = TRUE
			g_IntentionList[arraypos].bPickedUp = TRUE
			CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "):Acceptance complete")
			
			RETURN TRUE
		ELSE
			IF (g_IntentionList[arraypos].bUsed = FALSE) 
				CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): Checked context ", intention_index, " not used  ")
			ENDIF
			
			/*
			IF (g_IntentionList[arraypos].bActive = FALSE)
				CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): Checked context ", intention_index, " not active, this simply means the button hasn't been pressed yet ")	
			ENDIF
			*/
			
			IF g_IntentionList[arraypos].bToBeDeleted
				CDEBUG1LN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): Checked context ", intention_index, " is also pending delete!")
			ENDIF 
		ENDIF
		
		//its not registered, fail
	ELSE
		CPRINTLN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): index being checked that is not valid!")
		CASSERTLN(DEBUG_SHOPS, "HAS_CONTEXT_BUTTON_TRIGGERED(", GET_THIS_SCRIPT_NAME(), ", ", intention_index, "): index being checked that is not valid!")
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_ANY_CONTEXT_SHOWING()
	INT i = 0

	REPEAT MAX_CONTEXT_INTENTION i
		IF g_IntentionList[i].bUsed AND g_IntentionList[i].bActive
		
			IF g_IntentionList[i].RegisteredBy != NULL
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_ANY_CONTEXT_SHOWING: TRUE: ", g_IntentionList[i].helpLabel)
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_CONTEXT_INTENTION_HELP_DISPLAYING(INT intention_index,BOOL rawarray = FALSE)
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
	IF rawarray = FALSE
		IF arraypos = -1
			RETURN FALSE
		ENDIF
	ELSE
		arraypos = intention_index
	ENDIF

	IF arraypos < 0// OR arraypos > (MAX_CONTEXT_INTENTION-1)
		PRINTLN("Context can't display; not in valid range from script ", GET_THIS_SCRIPT_NAME(),"  index ", intention_index)
		RETURN FALSE
	ENDIF

	IF g_IntentionList[arraypos].bNoString 
		PRINTLN("Context doesn't have help")
		RETURN FALSE
	ENDIF
	
	BOOL retval = FALSE
	IF NOT g_IntentionList[arraypos].bAdditionalHelp
		BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(g_IntentionList[arraypos].helpLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
		retval = END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
	ELSE
		BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(g_IntentionList[arraypos].helpLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
			
			IF g_IntentionList[arraypos].subStringIsPlayerName
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_IntentionList[arraypos].additionalHelpLabel)
			ELSE
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_IntentionList[arraypos].additionalHelpLabel)
			ENDIF
			
		retval = END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
	ENDIF
	
	RETURN retval
ENDFUNC

FUNC STRING GET_CONTEXT_INTENTION_HELP(INT intention_index,BOOL rawarray = FALSE)
	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
	STRING sReturnString
	IF rawarray = FALSE
		IF arraypos = -1
			sReturnString = ""
		ENDIF
	ELSE
		arraypos = intention_index
	ENDIF

	IF arraypos < 0// OR arraypos > (MAX_CONTEXT_INTENTION-1)
		PRINTLN("Context can't display; not in valid range from script ", GET_THIS_SCRIPT_NAME(),"  index ", intention_index)
		sReturnString = ""
	ENDIF

	IF g_IntentionList[arraypos].bNoString 
		PRINTLN("Context doesn't have help")
		sReturnString = ""
	ENDIF

	IF NOT g_IntentionList[arraypos].bAdditionalHelp
		BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(g_IntentionList[arraypos].helpLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
		IF END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
			sReturnString = Get_String_From_TextLabel(g_IntentionList[arraypos].helpLabel)
		ENDIF
	ELSE
		BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(g_IntentionList[arraypos].helpLabel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
			
			IF g_IntentionList[arraypos].subStringIsPlayerName
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_IntentionList[arraypos].additionalHelpLabel)
			ELSE
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_IntentionList[arraypos].additionalHelpLabel)
			ENDIF
			
		IF END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
			sReturnString = Get_String_From_TextLabel(g_IntentionList[arraypos].helpLabel)
		ENDIF
	ENDIF
	
	RETURN sReturnString
ENDFUNC


PROC HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE()
//	#IF IS_DEBUG_BUILD
//	IF !b_SuppressContextHelpNextUpdate
		//PRINTLN("HIDE_ALL_CONTEXT_HELP_NEXT_UPDATE has set context help to suppress, this by script: ", GET_THIS_SCRIPT_NAME())
//	ENDIF
//	#ENDIF
	
	b_SuppressContextHelpNextUpdate = TRUE 
	
ENDPROC



/// PURPOSE:
///    Does what it says on the tin, used to reset the system on mission skips or debug menu transitions
PROC FORCE_CONTEXT_SYSTEM_RESET()
/*
STRUCT CONTEXT_INTENTION
	BOOL bUsed
	INT iPriority
	
	BOOL bActive
	BOOL bAccepted
	
	BOOL bToBeDeleted
	
	TEXT_LABEL helpLabel
ENDSTRUCT

CONST_INT MAX_CONTEXT_INTENTION 6

CONTEXT_INTENTION g_IntentionList[MAX_CONTEXT_INTENTION]
*/

	g_ContextResetForced = TRUE

	INT i
	REPEAT MAX_CONTEXT_INTENTION i
		g_IntentionList[i].bUsed = FALSE
		g_IntentionList[i].iPriority = 0
		g_IntentionList[i].bActive = FALSE
		g_IntentionList[i].bAccepted = FALSE
		g_IntentionList[i].bToBeDeleted = FALSE
		g_IntentionList[i].iID = -1
		g_IntentionList[i].iStyle = 0
	ENDREPEAT


ENDPROC





PROC SUPPRESS_CONTEXT_BUTTON_ACTIONS_THIS_FRAME()
	g_bSuppressContextActionsThisFrame = TRUE
ENDPROC






