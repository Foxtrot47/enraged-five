//////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      				//
//      SCRIPT NAME     :   flip_off_public.sch                                   						//
//      AUTHOR          :   Rob Bray                                                    				//
//      DESCRIPTION     :   Common functions for flipping buddy peds off								//
//                                                                                      				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_vehicle.sch" 
USING "commands_object.sch"
USING "commands_graphics.sch"
USING "commands_physics.sch"
USING "commands_streaming.sch"
USING "commands_camera.sch"
USING "commands_audio.sch"
USING "script_player.sch"

ENUM FLIP_OFF_STATE_ENUM
	FLIP_OFF_STATE_NOT_STARTED = 0,
	FLIP_OFF_STATE_WAIT_FOR_REACT,
	FLIP_OFF_STATE_REACTING,
	FLIP_OFF_STATE_WAIT_FOR_CANCEL
ENDENUM

STRUCT FLIP_OFF_DATA
	PED_INDEX ped
	FLIP_OFF_STATE_ENUM state
	INT iReactTime
ENDSTRUCT

PROC INIT_FLIP_OFF_PED(FLIP_OFF_DATA &flipOffData, PED_INDEX ped)
	flipOffData.ped = ped
	flipOffData.state = FLIP_OFF_STATE_NOT_STARTED
ENDPROC

// cancel flip off ped
PROC CANCEL_FLIP_OFF_PED(FLIP_OFF_DATA &flipOffData)
	IF NOT IS_PED_INJURED(flipOffData.ped)
		IF IS_PED_DOING_DRIVEBY(flipOffData.ped)
			CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(flipOffData.ped)
			// temp
			//TASK_DRIVE_BY(flipOffData.ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, 1, 100, TRUE)
		ENDIF
		flipOffData.state = FLIP_OFF_STATE_NOT_STARTED
	ENDIF
ENDPROC

// is player targetting ped with a flip off
FUNC BOOL IS_PLAYER_TARGETTING_PED_FOR_FLIP_OFF(PED_INDEX targetPed, BOOL bCheckTargetCanSeePlayer)
	IF NOT IS_PED_INJURED(targetPed)
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(targetPed)
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			WEAPON_TYPE weapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weapon)
			IF weapon = WEAPONTYPE_UNARMED
				VEHICLE_INDEX targetVehicle = GET_VEHICLE_PED_IS_USING(targetPed)
				IF NOT IS_ENTITY_DEAD(targetVehicle)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), targetPed, <<25,25,2.5>>)
						IF IS_PED_DOING_DRIVEBY(PLAYER_PED_ID())
							VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(targetPed, GET_ENTITY_COORDS(PLAYER_PED_ID()))
							IF vOffset.y > -12
							OR NOT bCheckTargetCanSeePlayer
								ENTITY_INDEX aimedEntity
								IF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), aimedEntity)
									IF aimedEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(targetPed)
									OR aimedEntity = GET_ENTITY_FROM_PED_OR_VEHICLE(targetVehicle)
										RETURN TRUE
									ENDIF
								ENDIF
								
								// temp while 918205 getting fixed	
								/*
								FLOAT fXPos
								FLOAT fYPos
								GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(targetPed), fXPos, fYPos)
								
								IF fXPos > 0.3
								AND fXPos < 0.7
								AND fYPos > 0.3
								AND fYPos < 0.7								
									RETURN TRUE
								ENDIF
								*/
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// handle flip off
PROC HANDLE_FLIP_OFF_PED(FLIP_OFF_DATA &flipOffData)
	// flip off ped
	IF NOT IS_PED_INJURED(flipOffData.ped)
		SWITCH flipOffData.state
			CASE FLIP_OFF_STATE_NOT_STARTED
				IF IS_PLAYER_TARGETTING_PED_FOR_FLIP_OFF(flipOffData.ped, TRUE)
					flipOffData.iReactTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1000, 1500)
					flipOffData.state = FLIP_OFF_STATE_WAIT_FOR_REACT
				ENDIF	
			BREAK
			
			CASE FLIP_OFF_STATE_WAIT_FOR_REACT
				IF GET_GAME_TIMER() >= flipOffData.iReactTime
					IF IS_PLAYER_TARGETTING_PED_FOR_FLIP_OFF(flipOffData.ped, FALSE)
						SET_CURRENT_PED_WEAPON(flipOffData.ped, WEAPONTYPE_UNARMED, TRUE)
						SET_CURRENT_PED_VEHICLE_WEAPON(flipOffData.ped, WEAPONTYPE_UNARMED)
						TASK_DRIVE_BY(flipOffData.ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, 30, 100, TRUE)
						flipOffData.iReactTime = GET_GAME_TIMER()
						flipOffData.state = FLIP_OFF_STATE_REACTING
					ELSE
						flipOffData.state = FLIP_OFF_STATE_NOT_STARTED
					ENDIF
				ENDIF
			BREAK
			
			CASE FLIP_OFF_STATE_REACTING
				IF NOT IS_PED_DOING_DRIVEBY(flipOffData.ped)
				AND GET_GAME_TIMER() >= flipOffData.iReactTime + 500
					flipOffData.state = FLIP_OFF_STATE_NOT_STARTED
				ELSE
					IF NOT IS_PLAYER_TARGETTING_PED_FOR_FLIP_OFF(flipOffData.ped, FALSE)
					AND GET_GAME_TIMER() >= flipOffData.iReactTime + 2000
						flipOffData.iReactTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1500, 3000)
						flipOffData.state = FLIP_OFF_STATE_WAIT_FOR_CANCEL
					ENDIF
				ENDIF
			BREAK
			
			CASE FLIP_OFF_STATE_WAIT_FOR_CANCEL
				IF IS_PLAYER_TARGETTING_PED_FOR_FLIP_OFF(flipOffData.ped, FALSE) 
					flipOffData.state = FLIP_OFF_STATE_REACTING
				ELSE
					IF GET_GAME_TIMER() >= flipOffData.iReactTime
						CANCEL_FLIP_OFF_PED(flipOffData)
					ENDIF
				ENDIF
			BREAK	
		ENDSWITCH
	ENDIF
ENDPROC
