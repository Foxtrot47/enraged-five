USING "rage_builtins.sch"
USING "globals.sch"
USING "clearMissionArea_private.sch"


/// PURPOSE: Tidy up vehicles at the mission trigger location. Ideally call in a cutscene or fade. 
///    vMoveToLocation: vTriggerLocation - specify where the cleanup will occur 
///    deleteIfCharacterVehicle: If you're going to create and position the player character's default vehicle in script, set this to True.
///    Otherwise this function will reposition the character's vehicle whilst you create a duplicate.
///    eTypePreference: Specify the default vehicle type.


/// PURPOSE: Tidy up vehicles at the mission trigger location. Ideally call in a cutscene or fade.  
///    deleteIfCharacterVehicle: If you're going to create and position the player character's default vehicle in script, set this to True.
///    Otherwise this function will reposition the character's vehicle whilst you create a duplicate.
///    eTypePreference: Specify the default vehicle type.
proc RESOLVE_VEHICLES_AT_MISSION_TRIGGER(vector vMoveToLocation, float fMoveToHeading, bool deleteIfDefaultVehicle = FALSE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT, bool clearTriggerLocation = TRUE, bool playerCharacterSpecific = FALSE, bool checkIfVehicleInGarage = FALSE, bool setVehicleAsVehGen = TRUE, bool bHeliAllowed = TRUE)
	//deal with player vehicle if at critical mission location
	SCRIPT_ASSERT("this command is now deprecated - please use the RESOLVE_VEHICLES_INSIDE_ANGLED_AREA commands instead to only clear areas that interfere with the cutscene")
	vector vTriggerLocation = Get_This_Mission_Script_Start_Location(playerCharacterSpecific)
	
	RESOLVE_VEHICLES_AT_MISSION_TRIGGER_PRIVATE(vTriggerLocation,vMoveToLocation,fMoveToHeading,<<50,50,50>>,deleteIfDefaultVehicle,eTypePreference,clearTriggerLocation,checkIfVehicleInGarage, setVehicleAsVehGen, bHeliAllowed)
ENDPROC

PROC RESOLVE_VEHICLES_AT_MISSION_TRIGGER_WITH_LOCATION(vector vTriggerLocation, vector vMoveToLocation, float fMoveToHeading, bool deleteIfDefaultVehicle = FALSE,								  
													   VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT, bool clearTriggerLocation = TRUE,
													   bool checkIfVehicleInGarage = FALSE, bool setVehicleAsVehGen = TRUE)
	SCRIPT_ASSERT("this command is now deprecated - please use the RESOLVE_VEHICLES_INSIDE_ANGLED_AREA commands instead to only clear areas that interfere with the cutscene")																   
	RESOLVE_VEHICLES_AT_MISSION_TRIGGER_PRIVATE(vTriggerLocation,vMoveToLocation,fMoveToHeading,<<50,50,50>>,deleteIfDefaultVehicle,eTypePreference,clearTriggerLocation,checkIfVehicleInGarage, setVehicleAsVehGen)
ENDPROC

proc RESOLVE_VEHICLES_AT_MISSION_TRIGGER_CHECK_VEHICLE_SIZE(vector vMoveToLocation, float fMoveToHeading, vector allowable_vehicle_size, bool deleteIfDefaultVehicle = FALSE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT, bool clearTriggerLocation = TRUE, bool playerCharacterSpecific = FALSE, bool checkIfVehicleInGarage = FALSE, bool setVehicleAsVehGen = TRUE)
	//deal with player vehicle if at critical mission location
	SCRIPT_ASSERT("this command is now deprecated - please use the RESOLVE_VEHICLES_INSIDE_ANGLED_AREA commands instead to only clear areas that interfere with the cutscene")
	vector vTriggerLocation = Get_This_Mission_Script_Start_Location(playerCharacterSpecific)
	
	RESOLVE_VEHICLES_AT_MISSION_TRIGGER_PRIVATE(vTriggerLocation,vMoveToLocation,fMoveToHeading,allowable_vehicle_size,deleteIfDefaultVehicle,eTypePreference,clearTriggerLocation,checkIfVehicleInGarage, setVehicleAsVehGen, TRUE, TRUE, TRUE)
ENDPROC

Proc RESOLVE_VEHICLES_AT_MISSION_TRIGGER_CHECK_VEHICLE_TYPE(vector vMoveToLocation, float fMoveToHeading, bool deleteIfDefaultVehicle = FALSE, VEHICLE_CREATE_TYPE_ENUM eTypePreference = VEHICLE_TYPE_DEFAULT, bool clearTriggerLocation = TRUE, bool playerCharacterSpecific = FALSE, bool checkIfVehicleInGarage = FALSE, bool setVehicleAsVehGen = TRUE, bool bHeliAllowed = TRUE, bool bPlaneAllowed = TRUE, bool bBoatAllowed=TRUE)
	SCRIPT_ASSERT("this command is now deprecated - please use the RESOLVE_VEHICLES_INSIDE_ANGLED_AREA commands instead to only clear areas that interfere with the cutscene")
	vector vTriggerLocation = Get_This_Mission_Script_Start_Location(playerCharacterSpecific)
	
	RESOLVE_VEHICLES_AT_MISSION_TRIGGER_PRIVATE(vTriggerLocation,vMoveToLocation,fMoveToHeading,<<50,50,50>>,deleteIfDefaultVehicle,eTypePreference,clearTriggerLocation,checkIfVehicleInGarage, setVehicleAsVehGen, bHeliAllowed, bPlaneAllowed, bBoatAllowed)
ENDPROC

/// PURPOSE: Tidy up vehicles inside the specified angled area.  
///    Use this to define the angled area around a cutscene location which you'll want to reposition the last player's vehicle from.
///    Use multiple calls to this command if you want to define more than one unsafe area for vehicles.
///    vMoveToCoord: Where to reposition vehicle
///    bClearAngledArea: Clear up any remaining vehicles in the defined angled area
///    setVehicleAsVehGen: THIS PARAM IS DEPRECATED AND WILL DO NOTHING - PLEASE USE SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN INSTEAD
///    ignoreVehicleInPlayerGarage: Don't delete or reposition the last vehicle if it's parked in the player's garage.
///    allow_make_vehicle_healths_safe: restores the vehicle healths e.g. engine health, petrol tank health if they are bellow the danger level so they don't become undriveable
Proc RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth, vector vMoveToCoord, float fMoveToHeading,bool bClearAngledArea=TRUE, bool setVehicleAsVehGen=TRUE, bool ignoreVehicleInPlayerGarage=TRUE, bool EngineOffDoorsClosed=FALSE, bool allow_make_vehicle_healths_safe = false)
	PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA(VecCoors1, VecCoors2, AreaWidth, vMoveToCoord, fMoveToHeading, <<0,0,0>>,bClearAngledArea, setVehicleAsVehGen, ignoreVehicleInPlayerGarage,EngineOffDoorsClosed, allow_make_vehicle_healths_safe)
ENDPROC

/// PURPOSE: Tidy up vehicles inside the specified angled area with additional check to delete vehicles larger than a specified size. 
///    Use this to define the angled area around a cutscene location which you'll want to reposition the last player's vehicle from.
///    Use multiple calls to this command if you want to define more than one unsafe area for vehicles.
///    vMoveToCoord: Where to reposition vehicle
///    vMaxAllowableSize: Set the max size of a vehicle that can fit at your defined vMoveToCoord.
///    bClearAngledArea: Clear up any remaining vehicles in the defined angled area
///    setVehicleAsVehGen: THIS PARAM IS DEPRECATED AND WILL DO NOTHING - PLEASE USE SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN INSTEAD
///    ignoreVehicleInPlayerGarage: Don't delete or reposition the last vehicle if it's parked in the player's garage.
///    allow_make_vehicle_healths_safe: restores the vehicle healths e.g. engine health, petrol tank health if they are bellow the danger level so they don't become undriveable
Proc RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth, vector vMoveToCoord, float fMoveToHeading, vector vMaxAllowableSize, bool bClearAngledArea=TRUE, bool setVehicleAsVehGen=TRUE, bool ignoreVehicleInPlayerGarage=TRUE, bool EngineOffDoorsClosed=FALSE, bool allow_make_vehicle_healths_safe = false)
	PRIVATE_REPOSITION_VEHICLES_INSIDE_ANGLED_AREA(VecCoors1, VecCoors2, AreaWidth, vMoveToCoord, fMoveToHeading, vMaxAllowableSize, bClearAngledArea, setVehicleAsVehGen, ignoreVehicleInPlayerGarage,EngineOffDoorsClosed, allow_make_vehicle_healths_safe)
ENDPROC




	
//#IF USE_CLF_DLC
//	
	/// PURPOSE:
	//			resolves the players last vehicle and the spy vehicle if both or one of the entities is within the 
///    		    specified angled area check. Also tidies up vehicles inside the specified angled area with additional check to delete vehicles larger than a specified size 
///    		    Should be used instead of RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT()
///     
///    		VecCoors1 VecCoors2 = angled area coords
///    		AreaWidth =  angled area length
///    		vMoveToCoord = players last vehicle move to coord 
///         fMoveToHeading = players last vehicle move to heading 
///    		spy_veh_move_to_coord = spy vehicle move to coord  
///    	    spy_veh_move_to_heading = spy vehicle move to heading 
///         allow_make_vehicle_healths_safe: restores the vehicle healths e.g. engine health, petrol tank health if they are bellow the danger level so they don't become undriveable
proc resolve_vehicles_and_spy_vehicle_inside_angled_area_with_size_limit(VECTOR VecCoors1, VECTOR VecCoors2, FLOAT AreaWidth, vector vMoveToCoord, float fMoveToHeading, vector spy_veh_move_to_coord, float spy_veh_move_to_heading, vector vMaxAllowableSize, bool bClearAngledArea=TRUE, bool ignoreVehicleInPlayerGarage=TRUE, bool EngineOffDoorsClosed=FALSE, bool allow_make_vehicle_healths_safe = true)
	
	#IF USE_CLF_DLC
	
		private_reposition_spy_vehicle_and_last_players_vehicle_inside_angled_area(VecCoors1, VecCoors2, AreaWidth, vMoveToCoord, fMoveToHeading, spy_veh_move_to_coord, spy_veh_move_to_heading, vMaxAllowableSize, bClearAngledArea, ignoreVehicleInPlayerGarage, EngineOffDoorsClosed, allow_make_vehicle_healths_safe)
		
	#endif 
	
	#IF not USE_CLF_DLC
		UNUSED_PARAMETER(VecCoors1)
		UNUSED_PARAMETER(VecCoors2)
		UNUSED_PARAMETER(AreaWidth)
		UNUSED_PARAMETER(vMoveToCoord)
		UNUSED_PARAMETER(fMoveToHeading)
		UNUSED_PARAMETER(spy_veh_move_to_coord)
		UNUSED_PARAMETER(spy_veh_move_to_heading)
		UNUSED_PARAMETER(vMaxAllowableSize)
		UNUSED_PARAMETER(bClearAngledArea)
		UNUSED_PARAMETER(ignoreVehicleInPlayerGarage)
		UNUSED_PARAMETER(EngineOffDoorsClosed)
		UNUSED_PARAMETER(allow_make_vehicle_healths_safe)
		script_assert("private_reposition_spy_vehicle_and_last_players_vehicle_inside_angled_area() should only be used in AGENT TREVOR")
	#endif 
	
endproc 

/// PURPOSE:
///    Stops fire on specified vehicle if it is on fire. Can be used to stop vehicle fires when vehicle is involved in a cutscene or cutscene area.
/// PARAMS:
///    VehicleIndex - Vehicle index to stop fire for.
PROC STOP_VEHICLE_FIRE(VEHICLE_INDEX VehicleIndex)
	IF DOES_ENTITY_EXIST(VehicleIndex)
		IF NOT IS_ENTITY_DEAD(VehicleIndex)
			IF IS_ENTITY_ON_FIRE(VehicleIndex)
				STOP_ENTITY_FIRE(VehicleIndex) //actually stop the fire on the vehicle entity
				SET_ENTITY_HEALTH(VehicleIndex, GET_ENTITY_HEALTH(VehicleIndex) + 200)
				SET_VEHICLE_ENGINE_HEALTH(VehicleIndex, GET_VEHICLE_ENGINE_HEALTH(VehicleIndex) + 200)
				SET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex, GET_VEHICLE_PETROL_TANK_HEALTH(VehicleIndex) + 200)
				STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(VehicleIndex), 5.0) //increase the radius from 2.5 to 5.0 to capture fire closeby on the ground
				CLEAR_AREA(GET_ENTITY_COORDS(VehicleIndex), 2.5, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
