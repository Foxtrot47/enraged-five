USING "commands_misc.sch"
USING "commands_debug.sch"
USING "buildtype.sch"
#IF IS_DEBUG_BUILD
USING "commands_pad.sch"
USING "script_debug_display.sch"
#ENDIF


//===================================================
//			Scene creation/destruction stuff
//===================================================

CONST_INT 			SCENEBIT_INSTANT_CLEANUP 		BIT0


TYPEDEF FUNC BOOL SCENE_CONSTRUCTOR_FUNC( INT &iBitFlags )
TYPEDEF FUNC BOOL SCENE_DESTRUCTOR_FUNC( INT &iBitFlags )


STRUCT SCENE_DATA_STRUCT
	BOOL 						bCreated 		= FALSE
	BOOL						bStreaming		= FALSE
	BOOL 						bDestroyed 		= TRUE		// starts true as not to trigger an unwanted call to the scene's destructor
	SCENE_CONSTRUCTOR_FUNC		funcConstructor
	SCENE_DESTRUCTOR_FUNC		funcDestructor
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_23 	tlDebugName
	#ENDIF
ENDSTRUCT


PROC RESET_MISSION_SCENE_DATA( SCENE_DATA_STRUCT &sSceneData )

	sSceneData.bCreated 		= FALSE
	sSceneData.bDestroyed		= TRUE			
	sSceneData.bStreaming		= FALSE
	
	#IF IS_DEBUG_BUILD
	sSceneData.tlDebugName		= ""
	#ENDIF

ENDPROC


PROC INITIALISE_MISSION_SCENE( SCENE_DATA_STRUCT &sSceneData, SCENE_CONSTRUCTOR_FUNC funcConstructor, SCENE_DESTRUCTOR_FUNC funcDestructor #IF IS_DEBUG_BUILD , STRING strDebugName = NULL #ENDIF )

	RESET_MISSION_SCENE_DATA( sSceneData )
	sSceneData.funcConstructor	= funcConstructor
	sSceneData.funcDestructor	= funcDestructor
	#IF IS_DEBUG_BUILD
	sSceneData.tlDebugName		= strDebugName
	#ENDIF

ENDPROC


FUNC BOOL SCENE_STREAM_IN_NOW( SCENE_DATA_STRUCT &sSceneData, INT iConstructionFlags = 0 )

	sSceneData.bDestroyed 		= FALSE
	
	// If already created don't keep spamming the constructor
	IF sSceneData.bCreated

		RETURN TRUE // return true to indicate creation has been completed
		
	ELIF CALL sSceneData.funcConstructor( iConstructionFlags )

		CPRINTLN( DEBUG_MIKE_UTIL, "PROCESS_SCENE_CREATION_DESTRUCTION() - ", sSceneData.tlDebugName, " Construction complete" )

		sSceneData.bStreaming 	= FALSE
		sSceneData.bCreated 	= TRUE	
		RETURN TRUE // return true to indicate creation has been completed
	
	ELSE
		sSceneData.bStreaming 	= TRUE
		CPRINTLN( DEBUG_MIKE_UTIL, "PROCESS_SCENE_CREATION_DESTRUCTION() - ", sSceneData.tlDebugName, " Scene streaming" )
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL SCENE_STREAM_OUT_NOW( SCENE_DATA_STRUCT &sSceneData, INT iDestructionFlags = SCENEBIT_INSTANT_CLEANUP )

	sSceneData.bCreated 	= FALSE
	sSceneData.bStreaming 	= FALSE

	IF sSceneData.bDestroyed
	
		RETURN TRUE
	
	ELIF CALL sSceneData.funcDestructor( iDestructionFlags ) // Destructor returns TRUE if the scene has been completely destroyed, if not keep calling
	
		CPRINTLN( DEBUG_MIKE_UTIL, "PROCESS_SCENE_CREATION_DESTRUCTION() - ", sSceneData.tlDebugName, " Destruction complete" )
		sSceneData.bDestroyed 	= TRUE
		RETURN TRUE
		
	ELSE
		CPRINTLN( DEBUG_MIKE_UTIL, "PROCESS_SCENE_CREATION_DESTRUCTION() - ", sSceneData.tlDebugName, " Scene destroying" )
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SCENE_STREAM_BY_DISTANCE( SCENE_DATA_STRUCT &sSceneData, VECTOR vCoord, FLOAT fCreationDist = -1.0, INT iCreationFlags = 0, FLOAT fDestructionDist = -1.0, INT iDestructionFlags = SCENEBIT_INSTANT_CLEANUP )

	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), vCoord )
	
	// Manage creation
	IF ( fCreationDist = -1 OR fDist < fCreationDist ) OR ( sSceneData.bStreaming AND ( fDestructionDist = -1 OR fDist < fDestructionDist ) )
	
		RETURN SCENE_STREAM_IN_NOW( sSceneData, iCreationFlags )
	
	// Manage destruction
	ELIF fDestructionDist != -1
	AND fDist > fDestructionDist
	
		RETURN SCENE_STREAM_OUT_NOW( sSceneData, iDestructionFlags )

	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SCENE_STREAM_IN_BY_DISTANCE( SCENE_DATA_STRUCT &sSceneData, VECTOR vCoord, FLOAT fDist, INT iConstructionFlags = 0 )
	SCENE_STREAM_BY_DISTANCE( sSceneData, vCoord, fDist, iConstructionFlags, -1 )	// stream out = -1, so we only steam in here and don't stream out
	IF sSceneData.bCreated
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SCENE_STREAM_OUT_BY_DISTANCE( SCENE_DATA_STRUCT &sSceneData, VECTOR vCoord, FLOAT fDist, INT iDestructionFlags = SCENEBIT_INSTANT_CLEANUP )
	SCENE_STREAM_BY_DISTANCE( sSceneData, vCoord, 0, 0, fDist, iDestructionFlags )	// stream in = 0, so we only steam out here and don't stream it back in
	IF sSceneData.bDestroyed
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SCENE_STREAM_BY_INTERIOR( SCENE_DATA_STRUCT &sSceneData, INTERIOR_INSTANCE_INDEX interior, INT iCreationFlags = 0, INT iDestructionFlags = SCENEBIT_INSTANT_CLEANUP )

	IF GET_INTERIOR_FROM_ENTITY( PLAYER_PED_ID() ) = interior
		RETURN SCENE_STREAM_IN_NOW( sSceneData, iCreationFlags )
	ELSE
		RETURN SCENE_STREAM_OUT_NOW( sSceneData, iDestructionFlags )
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SCENE_STREAM_BY_INTERIOR_LIST( SCENE_DATA_STRUCT &sSceneData, INTERIOR_INSTANCE_INDEX &interiors[], INT iCreationFlags = 0, INT iDestructionFlags = SCENEBIT_INSTANT_CLEANUP )

	INT i
	INTERIOR_INSTANCE_INDEX interiorPlayer = GET_INTERIOR_FROM_ENTITY( PLAYER_PED_ID() )
	REPEAT COUNT_OF( interiors ) i

		IF interiorPlayer = interiors[i]
			RETURN SCENE_STREAM_IN_NOW( sSceneData, iCreationFlags )
		ENDIF
	
	ENDREPEAT
	
	RETURN SCENE_STREAM_OUT_NOW( sSceneData, iDestructionFlags )
ENDFUNC

#IF IS_DEBUG_BUILD

PROC DEBUG_DISPLAY_SCENE_CREATION_INFO( DEBUG_DISPLAY &sDebug, SCENE_DATA_STRUCT &sceneData[] )

	TEXT_LABEL_23 TL_DEBUG_PAGE_SCENES = "Scene Debug"

	INT i
// old
//------------------------------------------------------------------------------

//	REPEAT COUNT_OF( sceneData ) i
//
//		DEBUG_DISPLAY_STRING_THIS_FRAME( sDebug, sceneData[i].tlDebugName, TL_DEBUG_PAGE_SCENES )
//		DEBUG_DISPLAY_BOOL_THIS_FRAME( sDebug, " - Created",  sceneData[i].bCreated, TL_DEBUG_PAGE_SCENES )
//		DEBUG_DISPLAY_BOOL_THIS_FRAME( sDebug, " - Streaming",  sceneData[i].bStreaming, TL_DEBUG_PAGE_SCENES )
//		DEBUG_DISPLAY_SPACE_THIS_FRAME( sDebug, TL_DEBUG_PAGE_SCENES )
//
//	ENDREPEAT
	
// new
//------------------------------------------------------------------------------
	
	CONST_INT 		I_LOS_DEBUG_TABS_AFTER_ID				3 - 2 	// 3 digits minus nn. chars in "ID"
	CONST_INT 		I_LOS_DEBUG_TABS_AFTER_NAME				23 - 4 	// largest minus no. chars in "Name"
	CONST_INT 		I_LOS_DEBUG_TABS_AFTER_CREATED			7  		// no. chars in "Created"
	CONST_INT 		I_LOS_DEBUG_TABS_AFTER_STREAMING		9 		// no. chars in "Streaming"
	CONST_INT 		I_LOS_DEBUG_TABS_AFTER_DESTROYED		9		// no. chars in "Destroyed"
	
	TEXT_LABEL_63 str_debug_info
	
	str_debug_info = "ID"
	REPEAT I_LOS_DEBUG_TABS_AFTER_ID i
		str_debug_info += " "
	ENDREPEAT
	str_debug_info += "Name"
	REPEAT I_LOS_DEBUG_TABS_AFTER_NAME i
		str_debug_info += " "
	ENDREPEAT
	str_debug_info += "Created"
	REPEAT I_LOS_DEBUG_TABS_AFTER_CREATED i
		str_debug_info += " "
	ENDREPEAT
	str_debug_info += "Streaming"
	REPEAT I_LOS_DEBUG_TABS_AFTER_STREAMING i
		str_debug_info += " "
	ENDREPEAT
	str_debug_info += "Destroyed"
	REPEAT I_LOS_DEBUG_TABS_AFTER_DESTROYED i
		str_debug_info += " "
	ENDREPEAT
	DEBUG_DISPLAY_STRING_THIS_FRAME( sDebug, str_debug_info, TL_DEBUG_PAGE_SCENES )
	DEBUG_DISPLAY_SPACE_THIS_FRAME( sDebug, TL_DEBUG_PAGE_SCENES )
	
	REPEAT COUNT_OF( sceneData ) i
	
		INT j, iStringLength
		TEXT_LABEL_63 str_temp
	
		// ID
		str_debug_info 	= ""
		str_temp 		= i
		str_debug_info += str_temp
		iStringLength = GET_LENGTH_OF_LITERAL_STRING( str_temp )
		REPEAT ( ( I_LOS_DEBUG_TABS_AFTER_ID ) - iStringLength ) j
			str_debug_info += " "
		ENDREPEAT
		
		// Name
		str_temp 		= sceneData[i].tlDebugName
		str_debug_info += str_temp
		iStringLength 	= GET_LENGTH_OF_LITERAL_STRING( str_temp )
		REPEAT ( ( I_LOS_DEBUG_TABS_AFTER_NAME + 6 ) - iStringLength ) j
			str_debug_info += " "
		ENDREPEAT
		
		// Created
		IF sceneData[i].bCreated
			str_temp	= "TRUE"
		ELSE
			str_temp	= "FALSE"
		ENDIF
		str_debug_info +=  str_temp
		iStringLength 	= GET_LENGTH_OF_LITERAL_STRING( str_temp )
		REPEAT ( ( I_LOS_DEBUG_TABS_AFTER_CREATED + 6 ) -iStringLength ) j
			str_debug_info += " "
		ENDREPEAT
		
		// Streaming
		IF sceneData[i].bStreaming
			str_temp	= "TRUE"
		ELSE
			str_temp	= "FALSE"
		ENDIF
		str_debug_info +=  str_temp
		iStringLength 	= GET_LENGTH_OF_LITERAL_STRING( str_temp )
		REPEAT ( ( I_LOS_DEBUG_TABS_AFTER_STREAMING + 6 ) -iStringLength ) j
			str_debug_info += " "
		ENDREPEAT
		
		// Destroyed
		IF sceneData[i].bDestroyed
			str_temp	= "TRUE"
		ELSE
			str_temp	= "FALSE"
		ENDIF
		str_debug_info +=  str_temp
		iStringLength 	= GET_LENGTH_OF_LITERAL_STRING( str_temp )
		REPEAT ( ( I_LOS_DEBUG_TABS_AFTER_DESTROYED + 6 ) -iStringLength ) j
			str_debug_info += " "
		ENDREPEAT
		
		DEBUG_DISPLAY_STRING_THIS_FRAME( sDebug, str_debug_info, TL_DEBUG_PAGE_SCENES )
		
		
	ENDREPEAT

ENDPROC

#ENDIF
