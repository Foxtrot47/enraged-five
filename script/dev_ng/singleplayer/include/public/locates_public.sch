USING "rage_builtins.sch"
USING "commands_camera.sch"
USING "model_enums.sch"
USING "commands_audio.sch"
USING "commands_debug.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_VEHICLE.sch"
USING "commands_zone.sch"
USING "globals.sch"
USING "script_player.sch"
USING "locates_private.sch"

// COMMANDS THAT RETURN NULL
FUNC VECTOR NULL_VECTOR()
	VECTOR nullVec
	RETURN(nullVec)
ENDFUNC
FUNC VECTOR DEFAULT_LOCATES_SIZE_VECTOR()
	RETURN << 0.0, 0.0, LOCATE_SIZE_HEIGHT >>
ENDFUNC
FUNC STRING NULL_STRING()
	STRING nullString
	RETURN(nullString)
ENDFUNC

/// PURPOSE:
///    By default if the player is asked to enter a vehicle and they haven't been in it yet the locates header will override the objective if you
///    get a wanted level. This is for situations where you have to drive across the map to get to the vehicle first. If you start next to the vehicle
///    you can use this to keep the vehicle objective as the priority.
PROC DISABLE_LOCATES_WANTED_PRINTS_OUTSIDE_OF_VEHICLE(LOCATES_HEADER_DATA &locatesData, BOOL bDisable)
	IF bDisable
		SET_BIT(locatesData.iLocatesBitSet, BS_PLAYER_STARTS_RIGHT_NEXT_TO_VEHICLE)
	ELSE
		CLEAR_BIT(locatesData.iLocatesBitSet, BS_PLAYER_STARTS_RIGHT_NEXT_TO_VEHICLE)
	ENDIF
ENDPROC

/// PURPOSE:
///    By default the locate header will remove buddies from the players group on cleanup (cleanup fires when the locate is hit). Use this to disable 
///    this behaviour if necessary.
PROC DISABLE_LOCATES_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP(LOCATES_HEADER_DATA &locatesData, BOOL bDisable)
	IF bDisable
		SET_BIT(locatesData.iLocatesBitSet, BS_DONT_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP)
	ELSE
		CLEAR_BIT(locatesData.iLocatesBitSet, BS_DONT_REMOVE_BUDDIES_FROM_GROUP_ON_CLEANUP)
	ENDIF
ENDPROC

/// PURPOSE:
///    Disables the clearing of buddy tasks when they transition from the custom behaviour of entering the nearest vehicle to standard locates behaviour.
PROC DISABLE_LOCATES_CLEAR_BUDDY_TASKS_IF_ENTERING_NEAREST_VEHICLE(LOCATES_HEADER_DATA &locatesData, BOOL bDisable)
	locatesData.bDontClearTasksIfEnteringNearestVehicle = bDisable
ENDPROC

// CLEANUP COMMAND
PROC CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(LOCATES_HEADER_DATA &locatesData, BOOL bRemovePedsFromGroup = FALSE)
	CLEAR_MISSION_LOCATE_STUFF(locatesData, bRemovePedsFromGroup)
ENDPROC

// PLAYER ONLY COMMANDS
FUNC BOOL IS_PLAYER_AT_LOCATION_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, STRING sGotoInstruction,  BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ANY_3D, NULL, NULL, NULL, NULL, sGotoInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), FALSE,bLoseWantedLevel,NULL_STRING(), FALSE,0, bShowWantedText, BlipSprite))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_LOCATION_ON_FOOT(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, STRING sGotoInstruction,  BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID, BOOL bShowLocationRoute = TRUE)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ON_FOOT_3D, NULL, NULL, NULL, NULL, sGotoInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), FALSE,bLoseWantedLevel,NULL_STRING(), FALSE,0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), bShowLocationRoute))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_LOCATION_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, STRING sGotoInstruction, STRING sGetInAnyVehicleInstruction,  BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_IN_VEHICLE_3D, NULL, NULL, NULL, NULL, sGotoInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), sGetInAnyVehicleInstruction, FALSE,bLoseWantedLevel,NULL_STRING(), FALSE,iMinNumberOfSeats, bShowWantedText, BlipSprite))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_LOCATION_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, VEHICLE_INDEX vehicleID, STRING sGotoInstruction, STRING sGetInSpecificVehicleInstruction, STRING sGetBackInSpecificVehicleInstruction = NULL,  BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_IN_VEHICLE_3D, NULL, NULL, NULL, vehicleID, sGotoInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), sGetInSpecificVehicleInstruction, FALSE,bLoseWantedLevel, sGetBackInSpecificVehicleInstruction, FALSE,iMinNumberOfSeats, bShowWantedText, BlipSprite))
ENDFUNC
// ANGLED AREA PLAYER ONLY COMMANDS
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, STRING strGoToInstruction, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN (IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ANY_3D, NULL, NULL, NULL, NULL, strGoToInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), FALSE, bLoseWantedLevel, NULL_STRING(), TRUE, 0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_ON_FOOT(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, STRING strGoToInstruction, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID , BOOL bShowLocationRoute = TRUE)
	RETURN (IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ON_FOOT_3D, NULL, NULL, NULL, NULL, strGoToInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), FALSE, bLoseWantedLevel, NULL_STRING(), TRUE, 0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), bShowLocationRoute, fAngledAreaWidth))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, STRING strGoToInstruction, STRING sGetInAnyVehicleInstruction, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN (IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_IN_VEHICLE_3D, NULL, NULL, NULL, NULL, strGoToInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), sGetInAnyVehicleInstruction, FALSE,bLoseWantedLevel, NULL_STRING(), TRUE,iMinNumberOfSeats, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, VEHICLE_INDEX vehicleID, STRING strGoToInstruction, STRING sGetInSpecificVehicleInstruction, STRING sGetBackInSpecificVehicleInstruction = NULL, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN (IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_IN_VEHICLE_3D, NULL, NULL, NULL, vehicleID, strGoToInstruction, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), sGetInSpecificVehicleInstruction, FALSE,bLoseWantedLevel, sGetBackInSpecificVehicleInstruction, TRUE,iMinNumberOfSeats, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC

// BUDDY COMMANDS
FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ANY_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, NULL_STRING(), bShowBuddyRoute, bLoseWantedLevel,NULL_STRING(),FALSE,0, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
    RETURN(IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(locatesData, vecCoords, vecLocateSize, bShowCorona, Buddy, NULL, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, bShowBuddyRoute, bLoseWantedLevel, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ON_FOOT(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID, BOOL bShowLocationRoute = TRUE)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ON_FOOT_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, NULL_STRING(), bShowBuddyRoute, bLoseWantedLevel,NULL_STRING(),FALSE,0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), bShowLocationRoute))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDY_ON_FOOT(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
    RETURN(IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ON_FOOT(locatesData, vecCoords, vecLocateSize, bShowCorona, Buddy, NULL, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind1, bShowBuddyRoute, bLoseWantedLevel, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, STRING sDontLeaveBuddyBehind2, STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sNeedvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE,  BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_IN_VEHICLE_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sNeedvehicle, bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle, FALSE, iMinNumberOfSeats, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind, STRING sNeedvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
    RETURN(IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(locatesData, vecCoords, vecLocateSize, bShowCorona, Buddy, NULL, NULL, sGotoInstruction, sDontLeaveBuddyBehind, sDontLeaveBuddyBehind, sDontLeaveBuddyBehind, sDontLeaveBuddyBehind, sNeedvehicle, sGetBackInvehicle, bShowBuddyRoute, bLoseWantedLevel, iMinNumberOfSeats, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, VEHICLE_INDEX vehicleID, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, STRING sDontLeaveBuddyBehind2, STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sGetInvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE,  BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_IN_VEHICLE_3D, Buddy1, Buddy2, Buddy3, vehicleID, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sGetInvehicle, bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle, FALSE, 0, bShowWantedText, BlipSprite))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy, VEHICLE_INDEX vehicleID, STRING sGotoInstruction, STRING sPickupBuddy, STRING sGetInvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE,  BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
    RETURN(IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(locatesData, vecCoords, vecLocateSize, bShowCorona, Buddy, NULL, NULL, vehicleID, sGotoInstruction, sPickupBuddy, sPickupBuddy, sPickupBuddy, sPickupBuddy, sGetInvehicle, sGetBackInvehicle, bShowBuddyRoute, bLoseWantedLevel, bShowWantedText, BlipSprite))
ENDFUNC

// ANGLED AREA BUDDY COMMANDS
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDIES_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ANY_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, NULL_STRING(), bShowBuddyRoute, bLoseWantedLevel,NULL_STRING(),FALSE,0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, PED_INDEX Buddy, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ANY_3D, Buddy, NULL, NULL, NULL, sGoToInstruction, sDontLeaveBuddyBehind, NULL_STRING(), NULL_STRING(), NULL_STRING(), NULL_STRING(), bShowBuddyRoute, bLoseWantedLevel, NULL_STRING(), TRUE, 0, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC

FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, PED_INDEX Buddy, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind, STRING sNeedvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ANY_3D, Buddy, NULL, NULL, NULL, sGoToInstruction, sDontLeaveBuddyBehind, NULL_STRING(), NULL_STRING(), NULL_STRING(), sNeedVehicle, bShowBuddyRoute, bLoseWantedLevel, sGetBackInVehicle, TRUE, iMinNumberOfSeats, bShowWantedText, BlipSprite, NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC

/// PURPOSE:
///    For heist missions only: same as IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE but allows you to specify the names of the chosen crew members.
FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_CREW_MEMBERS_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, VEHICLE_INDEX vehicleID, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, STRING sDontLeaveBuddyBehind2, STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sGetInvehicle, STRING sGetBackInvehicle, STRING sCrewMemberName1, STRING sCrewMemberName2, STRING sCrewMemberName3, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE,  BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_IN_VEHICLE_3D, Buddy1, Buddy2, Buddy3, vehicleID, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sGetInvehicle, bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle, FALSE, 0, bShowWantedText, BlipSprite, sCrewMemberName1, sCrewMemberName2, sCrewMemberName3))
ENDFUNC

/// PURPOSE:
///    For heist missions only: same as IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS but allows you to specify the names of the chosen crew members.
FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_CREW_MEMBERS_ANY_MEANS(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sCrewMemberName1, STRING sCrewMemberName2, STRING sCrewMemberName3, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ANY_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, NULL_STRING(), bShowBuddyRoute, bLoseWantedLevel,NULL_STRING(),FALSE,0, bShowWantedText, BlipSprite, sCrewMemberName1, sCrewMemberName2, sCrewMemberName3))
ENDFUNC

/// PURPOSE:
///    For heist missions only: same as IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_IN_ANY_VEHICLE but allows you to specify the names of the chosen crew members.
FUNC BOOL IS_PLAYER_AT_LOCATION_WITH_CREW_MEMBERS_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sCrewMemberName1, STRING sCrewMemberName2, STRING sCrewMemberName3, STRING sNeedvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, vecCoords, vecLocateSize, NULL_VECTOR(), NULL_VECTOR(), bShowCorona, TM_ANY_3D, Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sNeedvehicle, bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle,FALSE,iMinNumberOfSeats, bShowWantedText, BlipSprite, sCrewMemberName1, sCrewMemberName2, sCrewMemberName3))
ENDFUNC

/// PURPOSE:
///    For heist missions only: same as IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDY_IN_ANY_VEHICLE but allows you to specify the names of the chosen crew members.
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_WITH_CREW_MEMBERS_IN_ANY_VEHICLE(LOCATES_HEADER_DATA &locatesData,  VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1,  STRING sDontLeaveBuddyBehind2,  STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sCrewMemberName1, STRING sCrewMemberName2, STRING sCrewMemberName3, STRING sNeedvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE, INT iMinNumberOfSeats = 0, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
	RETURN(IS_PLAYER_AT_LOCATE(locatesData, 
			vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_ANY_3D, 
			Buddy1, Buddy2, Buddy3, NULL, sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sNeedvehicle, 
			bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle, TRUE,iMinNumberOfSeats, bShowWantedText, BlipSprite, sCrewMemberName1, sCrewMemberName2, sCrewMemberName3, 
			TRUE, fAngledAreaWidth))
ENDFUNC

/// PURPOSE:
FUNC BOOL IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDIES_IN_VEHICLE(LOCATES_HEADER_DATA &locatesData, VECTOR vBlipCoord, VECTOR vAngledAreaCoord1, VECTOR vAngledAreaCoord2, FLOAT fAngledAreaWidth, BOOL bShowCorona, PED_INDEX Buddy1, PED_INDEX Buddy2, PED_INDEX Buddy3, VEHICLE_INDEX vehicleID, STRING sGotoInstruction, STRING sDontLeaveBuddyBehind1, STRING sDontLeaveBuddyBehind2, STRING sDontLeaveBuddyBehind3, STRING sPickupAllBuddysText, STRING sGetInvehicle, STRING sGetBackInvehicle, BOOL bShowBuddyRoute = FALSE, BOOL bLoseWantedLevel = FALSE,  BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)
    RETURN(IS_PLAYER_AT_LOCATE(locatesData, 
		vBlipCoord, DEFAULT_LOCATES_SIZE_VECTOR(), vAngledAreaCoord1, vAngledAreaCoord2, bShowCorona, TM_IN_VEHICLE_3D, 
		Buddy1, Buddy2, Buddy3, vehicleID, 
		sGotoInstruction, sDontLeaveBuddyBehind1, sDontLeaveBuddyBehind2, sDontLeaveBuddyBehind3, sPickupAllBuddysText, sGetInvehicle, 
		bShowBuddyRoute, bLoseWantedLevel, sGetBackInvehicle, TRUE, 0, bShowWantedText, BlipSprite, 
		NULL_STRING(), NULL_STRING(), NULL_STRING(), TRUE, fAngledAreaWidth))
ENDFUNC 



//Horse Commands
FUNC BOOL IS_PLAYER_AT_LOCATION_ON_HORSE(	LOCATES_HEADER_DATA &locatesData, PED_INDEX HorsePassed, VECTOR vecCoords, VECTOR vecLocateSize, BOOL bShowCorona, 
											STRING sGotoInstruction, STRING sGetInSpecificVehicleInstruction, STRING sGetBackInSpecificVehicleInstruction = NULL, 
											BOOL bLoseWantedLevel = FALSE, BOOL bShowWantedText = TRUE, BLIP_SPRITE BlipSprite = RADAR_TRACE_INVALID)

	IF NOT IS_PED_INJURED(HorsePassed)
		IF GET_MOUNT(PLAYER_PED_ID()) = HorsePassed
			IF  IS_PLAYER_AT_LOCATION_ANY_MEANS(locatesData, vecCoords, vecLocateSize, bShowCorona, sGotoInstruction,bLoseWantedLevel, bShowWantedText,BlipSprite ) 
				RETURN TRUE
			ENDIF
			IF DOES_BLIP_EXIST(locatesData.vehicleBlip)
				REMOVE_BLIP(locatesData.vehicleBlip)
			ENDIF
			SET_BIT(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
            SAFE_CLEAR_THIS_PRINT(sGetInSpecificVehicleInstruction)
            SAFE_CLEAR_THIS_PRINT(sGetBackInSpecificVehicleInstruction)
		ELSE
			//Print Text
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(locatesData)
				IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
					GOD_TEXT(locatesData, sGetInSpecificVehicleInstruction)
					SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
					CLEAR_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE) 
					PRINTSTRING("set bit BS_PRINTED_GET_IN_VEHICLE 3")
					PRINTNL()
				ELSE
					IF IS_BIT_SET(locatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
						IF NOT IS_STRING_NULL(sGetBackInSpecificVehicleInstruction)
							IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
								GOD_TEXT(locatesData, sGetBackInSpecificVehicleInstruction)
								SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
							ENDIF
						ELSE
							IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
								GOD_TEXT(locatesData, sGetInSpecificVehicleInstruction)
								SET_BIT(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
							ENDIF   
						ENDIF
					ENDIF
				ENDIF   
			ENDIF
			//Make blip
			IF NOT DOES_BLIP_EXIST(locatesData.vehicleBlip)
				IF DOES_BLIP_EXIST(locatesData.LocationBlip)
					REMOVE_BLIP(locatesData.LocationBlip)
					SAFE_CLEAR_THIS_PRINT(sGotoInstruction)
				ENDIF
				locatesData.vehicleBlip = CREATE_BLIP_FOR_ENTITY(HorsePassed, FALSE)
				SET_BLIP_DISPLAY(locatesData.vehicleBlip, DISPLAY_BLIP)
				PRINTSTRING("IS_PLAYER_AT_LOCATE_WITH_BUDDIES_IN_VEHICLE - add blip 2")
				PRINTNL()

				IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
					TURN_ON_GPS_FOR_BLIP(locatesData.vehicleBlip, locatesData)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Overrides the location blip with an entity blip. Call this after the IS_PLAYER_AT_LOCATE command, for situations where you want to use the locates header but need
///    an entity to be the objective (e.g. "go to the car", "kill the ped", etc)
/// PARAMS:
///    locatesData - The locates header data struct.
///    entity - The entity to blip.
///    blipEntity - The blip that will be visible.
///    bIsEnemy - If the entity is a ped they will be blipped red if TRUE.
PROC OVERRIDE_LOCATES_BLIP_WITH_ENTITY_BLIP(LOCATES_HEADER_DATA &locatesData, ENTITY_INDEX entity, BLIP_INDEX &blipEntity, BOOL bIsEnemy = FALSE, BOOL bDisplayGPS = TRUE)
	IF DOES_BLIP_EXIST(locatesData.LocationBlip)
		SET_BLIP_ALPHA(locatesData.LocationBlip, 0)
		SET_BLIP_ROUTE(locatesData.LocationBlip, FALSE)
		
		IF NOT DOES_BLIP_EXIST(blipEntity)
		AND NOT IS_ENTITY_DEAD(entity)
			blipEntity = CREATE_BLIP_FOR_ENTITY(entity, bIsEnemy)
			SET_BLIP_ROUTE(blipEntity, bDisplayGPS)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipEntity)
			REMOVE_BLIP(blipEntity)
		ENDIF
	ENDIF
ENDPROC

PROC STOP_VOLTIC_ROCKET(VEHICLE_INDEX viPassed)
	IF NOT IS_ENTITY_DEAD(viPassed)
		IF GET_HAS_ROCKET_BOOST(viPassed)
			IF IS_ROCKET_BOOST_ACTIVE(viPassed)
				SET_ROCKET_BOOST_ACTIVE(viPassed, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Call every frame to stop a vehicle within the given stopping distance, and also disable any in-car controls.
///    This is a replacement to setting player control to FALSE when hitting a locate, and allows for less sudden stopping.
/// PARAMS:
///    veh - The vehicle to stop.
///    fStoppingDistance - The distance (in metres) that the vehicle will take to stop.
///    iTimeStopFor - The time (in seconds) that the vehicle will remain stopped after coming to a halt (the vehicle cannot be moved during this time). 
///    fStopSpeedThreshold - Once the vehicle's speed drops below this value it's considered to have stopped.
/// RETURNS:
///    TRUE once the vehicle has stopped (the vehicle speed drops below fStopSpeedThreshold).
FUNC BOOL BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(VEHICLE_INDEX veh, FLOAT fStoppingDistance = DEFAULT_VEH_STOPPING_DISTANCE, INT iTimeToStopFor = 1, FLOAT fStopSpeedThreshold = 0.5, BOOL bControlVerticalVelocity = FALSE, BOOL bDisableExit = TRUE, BOOL bEnableShooting = FALSE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	IF bDisableExit
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	IF NOT bEnableShooting
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	ENDIF
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_FRONT_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ROLL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_UNDERCARRIAGE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_VERTICAL_FLIGHT_MODE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_BOOST)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_ASCEND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_DESCEND)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_PITCH_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_THROTTLE_UP)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_THROTTLE_DOWN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_HARD_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SUB_TURN_HARD_RIGHT)
	
	DISABLE_CINEMATIC_SLOW_MO_THIS_UPDATE()
	
	STOP_VOLTIC_ROCKET(veh)
	
	//Reset time if time has passed
	IF (GET_GAME_TIMER() - giVehicleHaltTimeOut) > 500
		BRING_VEHICLE_TO_HALT(veh, fStoppingDistance, iTimeToStopFor, bControlVerticalVelocity)
	ENDIF
	
	
	
	giVehicleHaltTimeOut = GET_GAME_TIMER()
		
	IF NOT IS_ENTITY_DEAD(veh)
		IF ABSF(GET_ENTITY_SPEED(veh)) <= fStopSpeedThreshold
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//// PURPOSE:
 ///    Sets the locates header to perform a special case: buddies will behave as if a given vehicle is mission-specific (they'll get into it first). The intended use for this
 ///    is at the start of missions where the player's last vehicle is parked near the group. Call just once, the behaviour is reset when calling CLEAR_MISSION_LOCATE_STUFF.
 /// PARAMS:
 ///    locatesData - The locates data struct.
 ///    veh - If you already know which vehicle you want the buddies to get into you can pass it here, otherwise the header will look for the closest vehicle.
PROC SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(LOCATES_HEADER_DATA &locatesData, VEHICLE_INDEX veh = NULL)
	locatesData.vehStartCar = veh
	
	IF NOT IS_BIT_SET(locatesData.iLocatesBitSet, BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START)
		SET_BIT(locatesData.iLocatesBitSet, BS_BUDDIES_WALK_TO_NEAREST_VEHICLE_AT_START)
	ENDIF
ENDPROC

/// PURPOSE:
///    If TRUE then buddies in the player's group will automatically switch to action mode if the player gets a wanted level (this is TRUE by default).
PROC SET_LOCATES_BUDDIES_TO_ACTION_MODE_ON_WANTED_LEVEL(LOCATES_HEADER_DATA &locatesData, BOOL bSetToActionMode)
	IF NOT bSetToActionMode
		SET_BIT(locatesData.iLocatesBitSet, BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE)
	ELSE
		CLEAR_BIT(locatesData.iLocatesBitSet, BS_BLOCK_BUDDIES_SETTING_TO_ACTION_MODE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Mimics the behaviour of the locates header without the necceity to need a locate, as some objectives don't require this but have different conditions that should be met in order ot achieve that objective.
///    Use the wrapper functions, MANAGE_OBJECTIVE_TARGET_COORD, MANAGE_OBJECTIVE_TARGET_VEHICLE, etc, other pass conditions can be managed by the mission script.
/// RETURNS:
///    Returns TRUE while the objective is being met, FALSE if something makes passing the objective not possible, i.e. player got out of required vehicle, or the player needs to lose thier wanted level.
FUNC BOOL MANAGE_OBJECTIVE( LOCATES_HEADER_DATA &sLocates, STRING strObjectiveText, VECTOR vCoordObjective, ENTITY_INDEX entityObjective = NULL, FLOAT fBlipRadius = 0.0, BOOL bObjectiveIsEnemy = FALSE, VEHICLE_INDEX vehRequired = NULL, STRING strGetInVehicle = NULL, STRING strGetBackInVehicle = NULL, BOOL bLoseWanted = TRUE, BOOL bShowWantedText = TRUE, BOOL bShowRoute = TRUE )

	BOOL bBypassVehicleCheck

	IF NOT DOES_ENTITY_EXIST( vehRequired ) 
		bBypassVehicleCheck = TRUE
	ENDIF
	
	IF bBypassVehicleCheck
	OR IS_VEHICLE_DRIVEABLE( vehRequired )
	
		IF bBypassVehicleCheck
		OR IS_PED_IN_VEHICLE( PLAYER_PED_ID(), vehRequired )
		
			// if player already started in vehicle
			SET_BIT( sLocates.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE) 
			
			// reset flags for instructing the player to enter the vehicle
			CLEAR_BIT( sLocates.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
			SET_BIT( sLocates.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
			
			SAFE_CLEAR_THIS_PRINT( strGetInVehicle )
			SAFE_CLEAR_THIS_PRINT( strGetBackInVehicle )
			
			// clear vehicle blip
			IF DOES_BLIP_EXIST( sLocates.vehicleBlip )
				REMOVE_BLIP( sLocates.vehicleBlip )
			ENDIF

			IF bLoseWanted 
			AND IS_PLAYER_WANTED_LEVEL_GREATER( PLAYER_ID(), 0 )
			
				// Remove objective and blip
				SAFE_CLEAR_THIS_PRINT( strObjectiveText )
				IF DOES_BLIP_EXIST( sLocates.LocationBlip )
					REMOVE_BLIP( sLocates.LocationBlip )
				ENDIF
				IF DOES_BLIP_EXIST( sLocates.buddyBlipID[0] )
					REMOVE_BLIP( sLocates.buddyBlipID[0] )
				ENDIF
				
				// show wanted text
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED( sLocates, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF )
				AND NOT IS_BIT_SET( sLocates.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL )
				
					IF bShowWantedText
						GOD_TEXT( sLocates, "LOSE_WANTED" )
					ENDIF
					
					SET_BIT( sLocates.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL )
					CLEAR_BIT( sLocates.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH )
				ENDIF
		
			ELSE
			
				// clear wanted level text
				IF IS_BIT_SET( sLocates.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
					SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
					CLEAR_BIT( sLocates.iLocatesBitSet, BS_PRINTED_LOSE_WANTED_LEVEL)
					SET_BIT( sLocates.iLocatesBitSet, BS_PLAYED_LOST_COPS_SPEECH)
				ENDIF
			
				// print text
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED( sLocates, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF )
					IF NOT IS_BIT_SET( sLocates.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED )                     
						GOD_TEXT( sLocates, strObjectiveText )
						SET_BIT( sLocates.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED )
					ENDIF
				ENDIF
				
				// blip objective, if user has passed a blip coord
				IF NOT IS_VECTOR_ZERO( vCoordObjective )
					
					// normal blip
					IF NOT DOES_BLIP_EXIST( sLocates.LocationBlip )
						sLocates.LocationBlip = CREATE_BLIP_FOR_COORD( vCoordObjective, bShowRoute )
					ENDIF
				
					// radial blip
					IF NOT DOES_BLIP_EXIST( sLocates.buddyBlipID[0] )
						IF fBlipRadius != 0.0
							sLocates.buddyBlipID[0] = CREATE_BLIP_FOR_RADIUS( vCoordObjective, fBlipRadius, FALSE )
						ENDIF
					ENDIF
				
				// entity objective, if the user has passed a vehicle index
				ELIF DOES_ENTITY_EXIST( entityObjective )
				
					IF NOT DOES_BLIP_EXIST( sLocates.LocationBlip )
						SWITCH GET_ENTITY_TYPE( entityObjective )
							CASE ET_VEHICLE
								sLocates.LocationBlip = CREATE_BLIP_FOR_VEHICLE( GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( entityObjective ), bObjectiveIsEnemy )
							BREAK
							CASE ET_PED
								sLocates.LocationBlip = CREATE_BLIP_FOR_PED( GET_PED_INDEX_FROM_ENTITY_INDEX( entityObjective ), bObjectiveIsEnemy )
							BREAK
							CASE ET_OBJECT
								sLocates.LocationBlip = CREATE_BLIP_FOR_OBJECT( GET_OBJECT_INDEX_FROM_ENTITY_INDEX( entityObjective ) )
							BREAK
						ENDSWITCH
					ENDIF
					
					
					
				ENDIF
				
				
				RETURN TRUE
				
			ENDIF
			
		ELSE

			// Remove objective and blip
			SAFE_CLEAR_THIS_PRINT( strObjectiveText )
			IF DOES_BLIP_EXIST( sLocates.LocationBlip )
				REMOVE_BLIP( sLocates.LocationBlip )
			ENDIF
			IF DOES_BLIP_EXIST( sLocates.buddyBlipID[0] )
				REMOVE_BLIP( sLocates.buddyBlipID[0] )
			ENDIF
				
			// Clear wanted instruction
			SAFE_CLEAR_THIS_PRINT("LOSE_WANTED")
	
			// print text
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED( sLocates, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF ) //or conversation_paused
				
				IF NOT IS_BIT_SET( sLocates.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE )
				
					GOD_TEXT( sLocates, strGetInVehicle )
					SET_BIT( sLocates.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE )
					CLEAR_BIT( sLocates.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE ) 
					
					#IF IS_DEBUG_BUILD
						PRINTLN("set bit BS_PRINTED_GET_IN_VEHICLE 3")
					#ENDIF
					
				ELSE
				
					IF IS_BIT_SET( sLocates.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE )
						
						IF NOT IS_BIT_SET( sLocates.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE )
							IF NOT IS_STRING_NULL( strGetBackInVehicle )
								GOD_TEXT( sLocates, strGetBackInVehicle )
							ELSE
								GOD_TEXT( sLocates, strGetInVehicle )
							ENDIF
							SET_BIT( sLocates.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE )
						ENDIF

					ENDIF
					
				ENDIF 
				
			ENDIF
			
			// blip vehicle
			IF NOT DOES_BLIP_EXIST( sLocates.vehicleBlip )
				sLocates.vehicleBlip = CREATE_BLIP_FOR_VEHICLE( vehRequired, FALSe )
			ENDIF
	
		ENDIF

	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL MANAGE_OBJECTIVE_TARGET_COORD( LOCATES_HEADER_DATA &sLocates, STRING strObjectiveText, VECTOR vObjectiveCoord, FLOAT fBlipRadius = 0.0, 
	VEHICLE_INDEX requiredVehicle = NULL, STRING strGetInVehicle = NULL, STRING strGetBackInVehicle = NULL, BOOL bLoseWanted = FALSE, BOOL bShowRoute = TRUE )
			
	RETURN MANAGE_OBJECTIVE( sLocates, strObjectiveText, vObjectiveCoord, null, fBlipRadius, FALSE, 
		requiredVehicle, strGetInVehicle, strGetBackInVehicle, bLoseWanted, TRUE, bShowRoute )
ENDFUNC


FUNC BOOL MANAGE_OBJECTIVE_TARGET_ENTITY( LOCATES_HEADER_DATA &sLocates, STRING strObjectiveText, ENTITY_INDEX entityObjective, BOOL bIsEnemy = FALSE, 
	VEHICLE_INDEX requiredVehicle = NULL, STRING strGetInVehicle = NULL, STRING strGetBackInVehicle = NULL, BOOL bLoseWanted = FALSE, BOOL bShowRoute = TRUE )
			
	RETURN MANAGE_OBJECTIVE( sLocates, strObjectiveText, <<0,0,0>>, entityObjective, 0.0, bIsEnemy, 
		requiredVehicle, strGetInVehicle, strGetBackInVehicle, bLoseWanted, TRUE, bShowRoute )
ENDFUNC

