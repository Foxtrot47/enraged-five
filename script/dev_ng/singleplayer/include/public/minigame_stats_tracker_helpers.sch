USING "minigame_stats_tracker.sch"

structMGStatsTracker		structTracker
INT							mgStatsBits
THREADID					trackerThreadID  //not everyone is going to need this...can probably change LAUNCH_MINIGAME_MISSION_TRACKER to return the threadId and check for validity on it instead of returning TRUE/FALSE

/// PURPOSE:
///    Will set the title and description of the minigame tracker/results UI
/// PARAMS:
///    sTrackerTitle - title in big letters
///    sTrackerDesc - smaller letters below the main title
PROC MG_STATS_TRACKER_SET_TITLE_AND_DESC(STRING sTrackerTitle, STRING sTrackerDesc)
	structTracker.sMGTitle = sTrackerTitle
	structTracker.sMGDesc = sTrackerDesc
ENDPROC

/// PURPOSE:
///    sets the color of the title, clamps it
/// PARAMS:
///    fTrackerRed - 
///    fTrackerBlue - 
///    fTrackerGreen - 
PROC MG_STATS_TRACKER_SET_TITLE_COLOUR(FLOAT fTrackerRed, FLOAT fTrackerBlue, FLOAT fTrackerGreen)
	structTracker.eMGTitleColour.fTitleRed = fTrackerRed
	structTracker.eMGTitleColour.fTitleBlue = fTrackerBlue
	structTracker.eMGTitleColour.fTitleGreen = fTrackerGreen
	
	IF structTracker.eMGTitleColour.fTitleRed < 0.0
		structTracker.eMGTitleColour.fTitleRed = 0.0
	ELIF structTracker.eMGTitleColour.fTitleRed > 255.0
		structTracker.eMGTitleColour.fTitleRed = 255.0
	ENDIF
	
	IF structTracker.eMGTitleColour.fTitleBlue < 0.0
		structTracker.eMGTitleColour.fTitleBlue = 0.0
	ELIF structTracker.eMGTitleColour.fTitleBlue > 255.0
		structTracker.eMGTitleColour.fTitleBlue = 255.0
	ENDIF
	
	IF structTracker.eMGTitleColour.fTitleGreen < 0.0
		structTracker.eMGTitleColour.fTitleGreen = 0.0
	ELIF structTracker.eMGTitleColour.fTitleGreen > 255.0
		structTracker.eMGTitleColour.fTitleGreen = 255.0
	ENDIF	
ENDPROC

/// PURPOSE:
///    Sets the totals at the bottom of the UI
/// PARAMS:
///    eMGMedalType - bronze/silver/gold
///    fTotal - numeric value for total
///    sTotalDesc - string description of totl
PROC MG_STATS_TRACKER_SET_TOTALS(mgStatMedals eMGMedalType, FLOAT fTotal, STRING sTotalDesc)
	structTracker.mgTotals.eMGMedal = eMGMedalType
	structTracker.mgTotals.fMGMedalTotal = fTotal
	structTracker.mgTotals.sMGMedalTotal = sTotalDesc	
ENDPROC

/// PURPOSE:
///    Sets the totals at the bottom of the UI
/// PARAMS:
///    eMGMedalType - bronze/silver/gold
///    fTotal - numeric value for total
///    sTotalDesc - string description of totl
PROC MG_STATS_TRACKER_SET_TOTALS_WITH_MEDAL_LABEL(mgStatMedals eMGMedalType, STRING sTotalDesc, STRING sMedalLabel)
	structTracker.mgTotals.eMGMedal = eMGMedalType	
	structTracker.mgTotals.sMGMedalTotal = sTotalDesc	
	structTracker.mgTotals.sMGMedalMedalLabel = sMedalLabel	
ENDPROC

/// PURPOSE:
///    Can set the medal without using the total and description in MG_STATS_TRACKER_SET_TOTALS
/// PARAMS:
///    eMGMedalType - 
PROC MG_STATS_TRACKER_SET_MEDAL(mgStatMedals eMGMedalType)
	structTracker.mgTotals.eMGMedal = eMGMedalType	
ENDPROC

/// PURPOSE:
///    Allows user to set the lenght of time that the UI will show...defualt is 10 seconds
/// PARAMS:
///    fDisplayTime - 
PROC MG_STATS_TRACKER_SET_DISPLAY_TIME(FLOAT fDisplayTime)
	structTracker.fDisplayTime = fDisplayTime	
ENDPROC

/// PURPOSE:
///    Helper function 
/// PARAMS:
///    bPassed - 
/// RETURNS:
///    
FUNC INT MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)	
	structTracker.iMGSlots++
	INT iSlotNum = structTracker.iMGSlots 
	
	structTracker.mgDataSlot[iSlotNum].fMGObj_1 = INVALID_SCALEFORM_PARAM
	structTracker.mgDataSlot[iSlotNum].fMGObj_2 = INVALID_SCALEFORM_PARAM
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = NULL
	structTracker.mgDataSlot[iSlotNum].sMGStat_2 = NULL	
	
	structTracker.mgDataSlot[iSlotNum].iMGSlotNum = iSlotNum
	structTracker.mgDataSlot[iSlotNum].iMGPassed = ENUM_TO_INT(eCheckboxType)	
	RETURN iSlotNum
ENDFUNC


/// PURPOSE:
///    Added a number and label of the number to the menu
/// PARAMS:
///    fStatNumber - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_NUMBER(FLOAT fStatNumber, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_NUMBER: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatNumber
	structTracker.mgDataSlot[iSlotNum].fMGObj_2 = fStatNumber
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

/// PURPOSE:
///    Adds a string and a label for the string to the menu
/// PARAMS:
///    sStatTitle - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_STRING(STRING sStatTitle, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_STRING: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatString
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatTitle
	structTracker.mgDataSlot[iSlotNum].sMGStat_2 = sStatDesc	
ENDPROC

/// PURPOSE:
///    Adds a string with ~a~ in it, which is where the player's name will be inserted
/// PARAMS:
///    sPlayerName - player's name
///    sStatLabel - label with ~a~ in it to denote where player's name will go
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_STRING_WITH_PLAYER_NAME(STRING sPlayerName, STRING sStatLabel, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_STRING: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatStringWithPlayerName
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sPlayerName		
	structTracker.mgDataSlot[iSlotNum].sMGStat_2 = sStatLabel		
ENDPROC

/// PURPOSE:
///    Added a percentage and label of the percentage to the menu
/// PARAMS:
///    fPercentage - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_PERCENTAGE(FLOAT fPercentage, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_PERCENTAGE: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatPercentage
	structTracker.mgDataSlot[iSlotNum].fMGObj_2 = fPercentage
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

/// PURPOSE:
///    Adds formatted time and a label for it
/// PARAMS:
///    fSeconds - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_TIME(FLOAT fSeconds, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_TIME: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatTime
	structTracker.mgDataSlot[iSlotNum].fMGObj_1 = fSeconds			
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

/// PURPOSE:
///    Added a fraction and label for the fraction
/// PARAMS:
///    fNumerator - 
///    fDenominator - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_FRACTION(FLOAT fNumerator, FLOAT fDenominator, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_FRACTION: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatFraction
	structTracker.mgDataSlot[iSlotNum].fMGObj_1 = fNumerator
	structTracker.mgDataSlot[iSlotNum].fMGObj_2 = fDenominator
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

/// PURPOSE:
///    Adds money and a label for the money
/// PARAMS:
///    fMoneyAmount - 
///    sStatDesc - 
///    bBoxChecked - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_MONEY(FLOAT fMoneyAmount, STRING sStatDesc, eMGStatsCheckboxType eCheckboxType = CHECKBOX_TYPE_HIDDEN)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_MONEY: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL(eCheckboxType) //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatDollar
	
	// For some reason, the $ amt for money is stored in slot #2. Not sure why here.
	structTracker.mgDataSlot[iSlotNum].fMGObj_1 = fMoneyAmount	
	structTracker.mgDataSlot[iSlotNum].fMGObj_2 = fMoneyAmount	
	
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

/// PURPOSE:
///    Adds a general label
/// PARAMS:
///    sStatDesc - 
PROC MG_STATS_TRACKER_ADD_DATA_SLOT_NO_CHECKBOX(STRING sStatDesc)
	IF structTracker.iMGSlots+1 >= CONST_iMaxDataSlots
		SCRIPT_ASSERT("MG_STATS_TRACKER_ADD_DATA_SLOT_NO_CHECKBOX: out of data slots, not adding!")
		EXIT
	ENDIF

	INT iSlotNum = MG_STATS_TRACKER_ADD_DATA_SLOT_GENERAL() //easiers on the eyes
	
	structTracker.mgDataSlot[iSlotNum].eMGStatType = mgStatNoBox
	structTracker.mgDataSlot[iSlotNum].sMGStat_1 = sStatDesc
ENDPROC

PROC MG_STATS_TRACKER_RESET_DATA_SLOTS()
	structTracker.iMGSlots = -1
	structTracker.fDisplayTime = 10.0	
	INT iSlotNum
	REPEAT CONST_iMaxDataSlots iSlotNum
		structTracker.mgDataSlot[iSlotNum].fMGObj_1 = INVALID_SCALEFORM_PARAM
		structTracker.mgDataSlot[iSlotNum].fMGObj_2 = INVALID_SCALEFORM_PARAM
		structTracker.mgDataSlot[iSlotNum].sMGStat_1 = NULL
		structTracker.mgDataSlot[iSlotNum].sMGStat_2 = NULL	
	ENDREPEAT
	structTracker.mgTotals.eMGMedal = mgMedalEmpty
	structTracker.mgTotals.fMGMedalTotal = -1.0
	structTracker.mgTotals.sMGMedalTotal = NULL
	structTracker.sMGTitle = NULL
	structTracker.sMGDesc = NULL	
ENDPROC


/// PURPOSE:
///    Lets the script know of minigame results UI has been asked to launch
/// RETURNS:
///    TRUE when the script is streamed in and is launched
FUNC BOOL HAS_MINIGAME_MISSION_BEEN_REQUESTED_TO_LAUNCH()  //think about returning threadId instead - SiM	
	IF IS_BITMASK_AS_ENUM_SET(mgStatsBits, mgStatsScriptRequested)
	OR IS_THREAD_ACTIVE(trackerThreadID) //small protection
		RETURN TRUE
	ENDIF
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Launches the minigame results UI
/// PARAMS:
///    fDisplayTime - How long you want the stat screen to display for. (-1.0 = forever)
/// RETURNS:
///    TRUE when the script is streamed in, is launched and the threadid is valid
FUNC BOOL LAUNCH_MINIGAME_MISSION_TRACKER(FLOAT fDisplayTime = 10.0)  //think about returning threadId instead - SiM
	IF DOES_SCRIPT_EXIST("minigame_stats_tracker")
		IF NOT IS_BITMASK_AS_ENUM_SET(mgStatsBits, mgStatsScriptRequested)
			REQUEST_SCRIPT("minigame_stats_tracker")			
			SET_BITMASK_AS_ENUM(mgStatsBits, mgStatsScriptRequested)
		ENDIF
	
		IF HAS_SCRIPT_LOADED("minigame_stats_tracker")			
			IF NOT IS_THREAD_ACTIVE(trackerThreadID) //small protection
				g_bForceKillMGStatTracker = FALSE
				structTracker.fDisplayTime = fDisplayTime
				trackerThreadID = START_NEW_SCRIPT_WITH_ARGS("minigame_stats_tracker", structTracker, SIZE_OF(structTracker), 0)																	
			ELSE				
				RETURN TRUE
			ENDIF			
		ENDIF
	ENDIF
	PRINTSTRING("IS_MINIGAME_MISSION_TRACKER_READY is waiting for script to stream in")
	PRINTNL()
	RETURN FALSE	
ENDFUNC

/// PURPOSE:
///    Kills the minigame mission tracker if it's alive
PROC KILL_MINIGAME_MISSION_TRACKER()
	g_bForceKillMGStatTracker = TRUE
ENDPROC

