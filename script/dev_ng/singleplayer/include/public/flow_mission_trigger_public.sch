USING "globals.sch"
USING "rage_builtins.sch"
USING "selector_public.sch"
USING "locates_public.sch"


/// PURPOSE: 
///    
STRUCT TS_MOVING_BLIP_STRUCT
	STATIC_BLIP_NAME_ENUM 		blipName			= STATIC_BLIP_NAME_DUMMY_FINAL
	STRING 						strWaypoint
	FLOAT						fMoveSpeedMulti		= 1.0
	INT							iTargetNode			= 0
	ENTITY_INDEX 				entity 				= NULL
ENDSTRUCT


/// PURPOSE:
///    Updates a trigger scenes blip coord correctly with safety checks.
/// PARAMS:
///    blipName - The blip name
///    vCoord - The new coord for that blip
PROC TS_SET_BLIP_COORD(STATIC_BLIP_NAME_ENUM blipName, VECTOR vCoord)
	IF DOES_BLIP_EXIST(g_GameBlips[blipName].biBlip)
		SET_BLIP_COORDS(g_GameBlips[blipName].biBlip, vCoord)
	ENDIF
	INT i = 0
	FOR i=0 TO 2
		g_GameBlips[blipName].vCoords[i] = vCoord
	ENDFOR
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    sMovingBlipStruct - 
///    entity - 
FUNC TS_MOVING_BLIP_STRUCT TS_SET_MOVING_BLIP_TO_FOLLOW_ENTITY(STATIC_BLIP_NAME_ENUM blipName, ENTITY_INDEX entity)
	TS_MOVING_BLIP_STRUCT sResult

	sResult.blipName	= blipName
	IF DOES_ENTITY_EXIST(entity)
		sResult.entity = entity
	ENDIF
	
	RETURN sResult
ENDFUNC


/// PURPOSE:
///    
/// PARAMS:
///    BlipStruct - 
///    iStartingNode - 
FUNC TS_MOVING_BLIP_STRUCT TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(STATIC_BLIP_NAME_ENUM blipName, STRING strWaypoint, FLOAT fSpeedMultiplier = 1.0, INT iStartNode = -1)

	TS_MOVING_BLIP_STRUCT sResult
	sResult.blipName		= blipName
	sResult.strWaypoint 	= strWaypoint
	sResult.fMoveSpeedMulti	= fSpeedMultiplier
	
	// move blip to the specified start node and set the target to be the next node
	IF iStartNode != -1
		VECTOR vNodeCoord
		WAYPOINT_RECORDING_GET_COORD(strWaypoint, iStartNode, vNodeCoord)
		TS_SET_BLIP_COORD(sResult.blipName, vNodeCoord)
		sResult.iTargetNode = iStartNode + 1
		
		CDEBUG1LN(DEBUG_TRIGGER, "[MOVING_BLIP] TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(): ", 
			"Moved trigger scene blip to node ", iStartNode, " at coord ", vNodeCoord,
			"Target coord set to ", sResult.iTargetNode)
		
	// No start node specified, start from where the blip currently is and find the nearest node to head to
	ELSE
		IF WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sResult.strWaypoint, g_GameBlips[sResult.blipName].vCoords[0], iStartNode)
			sResult.iTargetNode = iStartNode
			
			CDEBUG1LN(DEBUG_TRIGGER, "[MOVING_BLIP] TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(): ", 
				"Blip left at current coord, target node set to ", sResult.iTargetNode)
		ELSE
			CDEBUG1LN(DEBUG_TRIGGER, "[MOVING_BLIP] TS_SET_MOVING_BLIP_TO_FOLLOW_WAYPOINT(): ",
				"NO WAYPOINT RECORDING LOADED")
		ENDIF
	ENDIF
	
	RETURN sResult
ENDFUNC


/// PURPOSE:
///    Moves a mission blip along a way point recording. The target node will auotmatically increment as the blip moves along the recording and reaches each node.
/// PARAMS:
///    blipName - the mission blip which to alter
///    strWaypoint - Name of the waypoint recorind the blip is being moved along
///    iTargetNode - The current target node that the blip is being moved towards
/// RETURNS:
///    Returns FALSE once reaching the end of the route, otherwise will return true during processing
FUNC BOOL TS_MOVE_BLIP_ALONG_WAYPOINT_REC(STATIC_BLIP_NAME_ENUM blipName, STRING strWaypoint, INT &iTargetNode, FLOAT fSpeedMulti)
	VECTOR vNextPoint, vResult
	IF WAYPOINT_RECORDING_GET_COORD(strWaypoint, iTargetNode, vNextPoint)
		// Interpolate coord toward the target node
		IF NOT ARE_VECTORS_ALMOST_EQUAL(g_GameBlips[blipName].vCoords[0], vNextPoint, 0.1)
			FLOAT t = CLAMP(FMAX(0.2, (WAYPOINT_RECORDING_GET_SPEED_AT_POINT(strWaypoint, iTargetNode)* fSpeedMulti)) * TIMESTEP() / GET_DISTANCE_BETWEEN_COORDS(g_GameBlips[blipName].vCoords[0], vNextPoint), 0.0, 1.0)
			vResult = LERP_VECTOR(g_GameBlips[blipName].vCoords[0], vNextPoint, t)
			
		// Reached the target node, increment to the next
		ELSE
			vResult = vNextPoint
			iTargetNode++
		ENDIF
		
		// Update the blip with the new coord
		TS_SET_BLIP_COORD(blipName, vResult)
		
		// Check if we have reached the end of the route
		INT iTotalPoints
		WAYPOINT_RECORDING_GET_NUM_POINTS(strWaypoint, iTotalPoints)
		IF iTargetNode >= iTotalPoints
			iTargetNode--
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Moves 
/// RETURNS:
///    Returns FALSE when the blip is no longer updating, this normaly signifies the blip has come to the end of the route or that the entity it was following not longer exists
FUNC BOOL TS_UPDATE_MOVING_BLIP(TS_MOVING_BLIP_STRUCT &sBlipStruct)
	// Ambient update is called while the trigger scene is not streamed in and we want to fake move the blip around the map 
	IF NOT DOES_ENTITY_EXIST(sBlipStruct.entity)
		// Waypoint rec must be streamed for this to work
		IF NOT IS_STRING_NULL_OR_EMPTY(sBlipStruct.strWaypoint)
			IF GET_IS_WAYPOINT_RECORDING_LOADED(sBlipStruct.strWaypoint)
				RETURN TS_MOVE_BLIP_ALONG_WAYPOINT_REC(sBlipStruct.blipName, sBlipStruct.strWaypoint, sBlipStruct.iTargetNode, sBlipStruct.fMoveSpeedMulti)
			ENDIF
		ENDIF
	// once the trigger scene is streamed in properly we move the blip based off the entity specified
	ELSE
		IF NOT IS_ENTITY_DEAD(sBlipStruct.entity)
			TS_SET_BLIP_COORD(sBlipStruct.blipName, GET_ENTITY_COORDS(sBlipStruct.entity))
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC


PROC SET_PLAYER_LOCKED_IN_TO_TRIGGER_SCENE()
	CPRINTLN(DEBUG_TRIGGER, "Trigger scene flagged player as being locked in.")
	
	IF NOT g_bPlayerLockedInToTrigger
		INT index
		
		//Set scene vehicle proofs.
		REPEAT MAX_TRIG_SCENE_VEHICLES index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.veh[index], TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene ped proofs.
		REPEAT MAX_TRIG_SCENE_PEDS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.ped[index], TRUE, TRUE, TRUE, TRUE, TRUE)
					SET_RAGDOLL_BLOCKING_FLAGS(g_sTriggerSceneAssets.ped[index], RBF_PLAYER_IMPACT) 
					CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[index]), 6.0)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Set scene object proofs.
		REPEAT MAX_TRIG_SCENE_OBJECTS index
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[index])
				IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[index])
					SET_ENTITY_PROOFS(g_sTriggerSceneAssets.object[index], TRUE, TRUE, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		//Clear aggressive peds out of the area.
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		ENDIF
		
		//Deal with the player being in a vehicle.
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			//Stop grenades going off around the player.
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 6.0)
		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				//Bring the vehicle out of critical state if it is in one.
				IF GET_ENTITY_HEALTH(vehPlayer) < 1
					SET_ENTITY_HEALTH(vehPlayer, 1)
				ENDIF
				IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
					SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 1)
				ENDIF
				IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
					SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 1)
				ENDIF
				
				//Stop the vehicle.
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayer, 2.5, 2, 0.5)
				
				//Get the player out.
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					IF 	GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 500)
					ENDIF
				ELSE
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
				ENDIF
			ENDIF
		ENDIF
		
		BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
		
		//Clean up all random events as we lock in.
		FORCE_CLEANUP(FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		
		g_bPlayerLockedInToTrigger = TRUE
	ENDIF
ENDPROC
