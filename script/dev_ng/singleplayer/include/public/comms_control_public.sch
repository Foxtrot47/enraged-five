//╒═════════════════════════════════════════════════════════════════════════════╕
//│					  Communication Controller Public Header					│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Ben Rollinson											│
//│		DATE:			13/10/10												│
//│		DESCRIPTION: 	The public interface for the communication  			│
//│						controller system.										│									
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "comms_control_private.sch"
USING "player_ped_public.sch"
USING "charsheet_public.sch"
#IF IS_DEBUG_BUILD
	USING "flow_debug_game.sch"
#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════╡ Communication Registration  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


/// PURPOSE:	Registers a phonecall from the player to another character with the communication controller and adds that call to a 
///    			queue to be triggered as soon as the controller will allow.
///    
/// PARAMS:		eID - The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCommunicationType - The type of comminication being registered. This type will determine the phonecall's priority.
///				eCharacterFrom - A charsheet ENUM defining the player character this phonecall is being sent from.
///   			eCharacterTo - A charsheet ENUM defining the playable character this phonecall is to be sent to.
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
/// RETURNS:	The unique INT ID that references the registered phonecall in the communication controller. This will be -1 if the call failed to register.
FUNC BOOL REGISTER_CALL_FROM_PLAYER_TO_CHARACTER(	CC_CommID eID,	
													CC_CommunicationType eCommunicationType,
													enumCharacterList eCharacterFrom,
													enumCharacterList eCharacterTo,
													INT iNonPlayableSpeaker,
													INT iQueueTime,
													INT iRequeueTime,
													VectorID eRestrictedArea = VID_BLANK,
													CC_CodeID eExecuteOnComplete = CID_BLANK,
													FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
													INT iSettings = 0)
											
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The queue time was < 0.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF iReQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The queue time was < 0.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The restricted area was set to VID_MAX.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: eRestrictedArea cannot be VID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
	IF eExecuteOnComplete = CID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The code ID was set to CID_MAX.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#endif
	#endif
	
	IF eCharacterTo = eCharacterFrom
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The receiving character was the same as the sending character.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: eCharacterTo cannot be the same as eCharacterFrom. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF  eCharacterFrom <> CHAR_BLANK_ENTRY
	AND eCharacterFrom <> CHAR_MICHAEL
	AND eCharacterFrom <> CHAR_FRANKLIN
	AND eCharacterFrom <> CHAR_TREVOR
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The sending character was none playable.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: Cannot set up a phonecall to be sent from a non-playable character. Call failed to register.")
		RETURN FALSE
	ENDIF

	//Check the call queue isn't full.
	#if USE_CLF_DLC
	
	IF eExecuteOnComplete = CID_CLF_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The code ID was set to CID_MAX.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF
	#endif
	#if USE_NRM_DLC
	
	IF eExecuteOnComplete = CID_NRM_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The code ID was set to CID_MAX.")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobals.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF
	#endif
	#endif
	
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The call queue is full.")
	SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers a phonecall from the player to another character with the communication controller and adds that call to a 
///    			queue to be triggered as soon as the controller will allow.
///    
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eBranchTrue - The enum referencing the second half of the call if the branch check returns TRUE.
///    			eBranchFalse - The enum referencing the second half of the call if the branch check returns FALSE.
///    			eBranchCheck - The flow check to run to decide which branch of conversation to use at the end of the call.
///    			eCommunicationType - The type of comminication being registered. This type will determine the phonecall's priority.
///				eCharacterFrom - A charsheet ENUM defining the player character this phonecall is being sent from.
///   			eCharacterTo - A charsheet ENUM defining the playable character this phonecall is to be sent to.
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
/// RETURNS:	The unique INT ID that references the registered phonecall in the communication controller. This will be -1 if the call failed to register.
FUNC BOOL REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER(	CC_CommID eID,
															CC_CommID eBranchTrue,
															CC_CommID eBranchFalse,
															FLOW_CHECK_IDS eBranchCheck,
															CC_CommunicationType eCommunicationType,
															enumCharacterList eCharacterFrom,
															enumCharacterList eCharacterTo,
															INT iNonPlayableSpeaker,
															INT iQueueTime,
															INT iRequeueTime,
															VectorID eRestrictedArea = VID_BLANK,
															CC_CodeID eExecuteOnComplete = CID_BLANK,
															INT iSettings = 0)
											
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The queue time was < 0.")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF iReQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The queue time was < 0.")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The restricted area was set to VID_MAX.")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: eRestrictedArea cannot be VID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		IF eExecuteOnComplete = CID_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The code ID was set to CID_MAX.")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
	#endif
	#endif
	IF eCharacterTo = eCharacterFrom
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The receiving character was the same as the sending character.")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: eCharacterTo cannot be the same as eCharacterFrom. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF  eCharacterFrom <> CHAR_BLANK_ENTRY
	AND eCharacterFrom <> CHAR_MICHAEL
	AND eCharacterFrom <> CHAR_FRANKLIN
	AND eCharacterFrom <> CHAR_TREVOR
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call from player but failed. The sending character was none playable.")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: Cannot set up a phonecall to be sent from a non-playable character. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
		IF eExecuteOnComplete = CID_CLF_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The code ID was set to CID_MAX.")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
		
		//Check the call queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
											
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF 
	#endif
	#if USE_NRM_DLC
		IF eExecuteOnComplete = CID_NRM_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The code ID was set to CID_MAX.")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
		
		//Check the call queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
											
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		
		
		//Check the call queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData

			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterFrom))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eCharacterTo
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call queue entry to the global array.
			g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobals.sCommsControlData.iNoQueuedCalls++
											
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call from player registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			PRIVATE_Update_Playable_Character_Priority_Level(eCharacterFrom)
			
			RETURN TRUE
		ENDIF
	#endif
	#endif
		
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call from player but failed. The call queue is full.")
	SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_PLAYER_TO_CHARACTER: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers a phonecall with the communication controller and adds that call to a queue to be triggered as
///				soon as the controller will allow.
///    
/// PARAMS:		eID - The enum referencing which phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCommunicationType - The type of comminication being registered. This type will determine the phonecall's priority.
///				eCharacterTo - A charsheet ENUM defining the playable character this phonecall is to be sent to. CHAR_BLANK_ENTRY will allow the communication to be sent to any playable character.
///				eCharacterFrom - A charsheet ENUM defining the character this phonecall is being sent from.
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the call is completed sucessfully.
///    			eCheckBeforeSend - A check ID that defines a check that must pass before this call can be sent.
/// RETURNS:	The unique INT ID that references the registered phonecall in the communication controller. This will be -1 if the call failed to register.
FUNC BOOL REGISTER_CALL_FROM_CHARACTER_TO_PLAYER(CC_CommID eID,
												CC_CommunicationType eCommunicationType,
												INT iToPlayersBitset,
												enumCharacterList eNPCCharacter,
												INT iNonPlayableSpeaker,
												INT iQueueTime,
												INT iRequeueTime,
												CC_CommID eCallMissedTextID = COMM_NONE,
												VectorID eRestrictedArea = VID_BLANK,
												CC_CodeID eExecuteOnComplete = CID_BLANK,
												FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
												INT iSettings = 0)
											
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		IF eExecuteOnComplete = CID_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#endif
	#endif
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Call failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iNonPlayableSpeaker < 3
		IF iNonPlayableSpeaker <> ENUM_TO_INT(eNPCCharacter)
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: iNonPlayableSpeaker can not be < 0 and can not use a main character ID 0-2.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: Cannot set up a phonecall to be sent to a non-playable character. Call failed to register.")
		RETURN FALSE
	ENDIF

	#if USE_CLF_DLC
	IF eExecuteOnComplete = CID_CLF_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
		 //Check the call queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			sNewCallData.sCommData.eID = eID
			
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = eCallMissedTextID
			sNewCallData.eCommExtra2 = COMM_NONE
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCallMissedTextID != COMM_NONE
				//Missed text defined. Force text on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			ELIF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
				
			//Save the call queue entry to the global array.
			g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT			
			RETURN TRUE
		ENDIF
	#endif
	#if USE_NRM_DLC
	IF eExecuteOnComplete = CID_NRM_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
		 //Check the call queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			sNewCallData.sCommData.eID = eID
			
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = eCallMissedTextID
			sNewCallData.eCommExtra2 = COMM_NONE
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCallMissedTextID != COMM_NONE
				//Missed text defined. Force text on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			ELIF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
				
			//Save the call queue entry to the global array.
			g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT			
			RETURN TRUE
		ENDIF
	#endif
	
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
	//Check the call queue isn't full.
	IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
		CC_CallData sNewCallData
		
		sNewCallData.sCommData.eID = eID
		
		
		//If this is flagged as the last completed call as we register it. Clear the last completed call.
		IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
			g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
		ENDIF
												
		//Get a priority for this communication.
		sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
		
		//Setup call data.
		sNewCallData.sCommData.iSettings = iSettings
		sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
		sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
		sNewCallData.sCommData.iRequeueTime = iRequeueTime
		sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
		sNewCallData.iSpeakerID = iNonPlayableSpeaker
		sNewCallData.eCommExtra = eCallMissedTextID
		sNewCallData.eCommExtra2 = COMM_NONE
		sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
		sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
		sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
		CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
		CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
		
		//Force certain missed responses under certain conditions.
		IF eCallMissedTextID != COMM_NONE
			//Missed text defined. Force text on miss.
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
		ELIF eCommunicationType = CT_END_OF_MISSION
			//End of mission priority. Force requeue on miss.
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
		ENDIF
			
		//Save the call queue entry to the global array.
		g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
		g_savedGlobals.sCommsControlData.iNoQueuedCalls++
		
		CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call registered by ",
										GET_THIS_SCRIPT_NAME(),
										". ID:",
										GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
										" Priority:",
										PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
		
		//Update character queue priority levels in case this new call is the new highest priority communication.
		INT index
		REPEAT 3 index
			IF IS_BIT_SET(iToPlayersBitset, index)
				PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
			ENDIF
		ENDREPEAT
		
		RETURN TRUE
	ENDIF
	#endif
	#endif
	
		
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call but failed!")
	SCRIPT_ASSERT("REGISTER_CALL_FROM_CHARACTER_TO_PLAYER: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers a phonecall, including a question, with the communication controller and adds that call to a queue to
///				be triggered as soon as the controller will allow.
///    
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCommunicationType - The type of comminication being registered. This type will determine the phonecall's priority.
///				iToPlayersBitset - A bitset containing which player characters this communication should be sent to.
///				eCharacterFrom - A charsheet ENUM defining the character this phonecall is being sent from.
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///				eRestrictedArea - A vector ID that defines an area in the world that the player must not be in for the call to trigger.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the call is completed sucessfully.
///    			eCheckBeforeSend - A check ID that defines a check that must pass before this call can be sent.
/// RETURNS:	The unique INT ID that references the registered phonecall in the communication controller. This will be -1 if the call failed to register.
FUNC BOOL REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER(	CC_CommID eID,
																CC_QuestionCallResponse eYesResponse,
																CC_QuestionCallResponse eNoResponse,
																CC_CommunicationType eCommunicationType,
																INT iToPlayersBitset,
																enumCharacterList eNPCCharacter,
																INT iNonPlayableSpeaker,
																INT iQueueTime,
																INT iRequeueTime,
																CC_CommID eCallMissedTextID = COMM_NONE,
																VectorID eRestrictedArea = VID_BLANK,
																CC_CodeID eExecuteOnComplete = CID_BLANK,
																FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
																INT iSettings = 0)
																													
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		IF eExecuteOnComplete = CID_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
	#endif
	#endif
	
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Call failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iNonPlayableSpeaker < 3
		IF iNonPlayableSpeaker <> ENUM_TO_INT(eNPCCharacter)
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: iNonPlayableSpeaker can not be < 0 and can not use a main character ID 0-2.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
		SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: Cannot set up a phonecall to be sent to a non-playable character. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
		IF eExecuteOnComplete = CID_CLF_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
	 //Check the call queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = eCallMissedTextID
			sNewCallData.eYesResponse = eYesResponse
			sNewCallData.eNoResponse = eNoResponse
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCallMissedTextID != COMM_NONE
				//Missed text defined. Force text on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			ELIF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call with question registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif
	#if USE_NRM_DLC
		IF eExecuteOnComplete = CID_NRM_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
			SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
	 //Check the call queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = eCallMissedTextID
			sNewCallData.eYesResponse = eYesResponse
			sNewCallData.eNoResponse = eNoResponse
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCallMissedTextID != COMM_NONE
				//Missed text defined. Force text on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			ELIF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call with question registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		
		//Check the call queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.eCommExtra = eCallMissedTextID
			sNewCallData.eYesResponse = eYesResponse
			sNewCallData.eNoResponse = eNoResponse
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewCallData.sCommData.eSendCheck = eCheckBeforeSend
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_HAS_QUESTION)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCallMissedTextID != COMM_NONE
				//Missed text defined. Force text on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_TEXT_ON_MISS)
			ELIF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobals.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New call with question registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif
	#endif
	
	
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new call with question but failed!")
	SCRIPT_ASSERT("REGISTER_CALL_WITH_QUESTION_FROM_CHARACTER_TO_PLAYER: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers a phonecall, which can end in two different ways depending on the return value of a custom check function.
///    
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eBranchTrue - The enum referencing the second half of the call if the branch check returns TRUE.
///    			eBranchFalse - The enum referencing the second half of the call if the branch check returns FALSE.
///    			eBranchCheck - The flow check to run to decide which branch of conversation to use at the end of the call.
///    			eCommunicationType - The type of comminication being registered. This type will determine the phonecall's priority.
///				iToPlayersBitset - A bitset containing which player characters this communication should be sent to.
///				eCharacterFrom - A charsheet ENUM defining the character this phonecall is being sent from.
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///				eRestrictedArea - A vector ID that defines an area in the world that the player must not be in for the call to trigger.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the call is completed sucessfully.
/// RETURNS:	TRUE if the call suceeded in registering, FALSE if 
FUNC BOOL REGISTER_BRANCHED_CALL_FROM_CHARACTER_TO_PLAYER(	CC_CommID eID,
															CC_CommID eBranchTrue,
															CC_CommID eBranchFalse,
															FLOW_CHECK_IDS eBranchCheck,
															CC_CommunicationType eCommunicationType,
															INT iToPlayersBitset,
															enumCharacterList eNPCCharacter,
															INT iNonPlayableSpeaker,
															INT iQueueTime,
															INT iRequeueTime,
															VectorID eRestrictedArea = VID_BLANK,
															CC_CodeID eExecuteOnComplete = CID_BLANK,
															INT iSettings = 0)
																													
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Call failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		IF eExecuteOnComplete = CID_MAX
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
			RETURN FALSE
		ENDIF
	#endif
	#endif
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Call failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iNonPlayableSpeaker < 3
		IF iNonPlayableSpeaker <> ENUM_TO_INT(eNPCCharacter)
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
			SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: iNonPlayableSpeaker can not be < 0 and can not use a main character ID 0-2.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: Cannot set up a phonecall to be sent to a non-playable character. Call failed to register.")
		RETURN FALSE
	ENDIF
	
	#if USE_CLF_DLC
	IF eExecuteOnComplete = CID_CLF_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
		 //Check the call queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif
	#if USE_NRM_DLC
	IF eExecuteOnComplete = CID_NRM_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
		SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Call failed to register.")
		RETURN FALSE
	ENDIF
		 //Check the call queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
		//Check the call queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			//Store the IDs required by this branched call.
			sNewCallData.sCommData.eID = eID
			sNewCallData.eCommExtra = eBranchTrue
			sNewCallData.eCommExtra2 = eBranchFalse
			sNewCallData.sCommData.eSendCheck = eBranchCheck
			
			//If this is flagged as the last completed call as we register it. Clear the last completed call.
			IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Get a priority for this communication.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Calculate an initial queue time for this communication.
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = iSettings
			sNewCallData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewCallData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_BRANCHED)
			CLEAR_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Force certain missed responses under certain conditions.
			IF eCommunicationType = CT_END_OF_MISSION
				//End of mission priority. Force requeue on miss.
				SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			ENDIF
			
			//Save the call entry to the global queue.
			g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobals.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, 	"New branched call registered by ",
											GET_THIS_SCRIPT_NAME(),
											". ID:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID),
											". Check:",
											GET_DEBUG_STRING_FOR_FLOW_CHECK_ID(sNewCallData.sCommData.eSendCheck),
											". True:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra),
											". False:",
											GET_COMM_ID_DEBUG_STRING(sNewCallData.eCommExtra2),
											" Priority:",
											PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new call is the new highest priority communication.
			INT index
			REPEAT 3 index
				IF IS_BIT_SET(iToPlayersBitset, index)
					PRIVATE_Update_Playable_Character_Priority_Level(INT_TO_ENUM(enumCharacterList, index))
				ENDIF
			ENDREPEAT
			
			RETURN TRUE
		ENDIF
	#endif	
	#endif
	
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new branched call but failed!")
	SCRIPT_ASSERT("REGISTER_BRANCHED_CALL_FROM_CHARACTER_TO_PLAYER: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers a chat phonecall with the communication controller.
///    
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCharacterActivatingPlayer - A charsheet ENUM defining the playable character that must activate this chat call. CHAR_BLANK_ENTRY will allow the call to be activated by any playable character.
///				eCharacterTo - A charsheet ENUM defining the NPC character this chat call is tied to.
/// RETURNS:	The unique INT ID that references the registered chat phonecall in the communication controller. This will be -1 if the call failed to register.
FUNC BOOL REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(	CC_CommID eID,
														INT iTriggeringPlayersBitset,
														enumCharacterList eNPCCharacter,
														INT iNonPlayableSpeaker,
														INT iQueueTime,
														FLOW_CHECK_IDs eCallCheck = FLOW_CHECK_NONE,
														CC_CommunicationPriority ePriority = CPR_high)
											
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new chat call but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iTriggeringPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new chat call but failed!")
			SCRIPT_ASSERT("REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER: eCharacterActivatingPlayer cannot be the same as eCharacterTo. Chat call failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iTriggeringPlayersBitset < 1 OR iTriggeringPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new chat call but failed!")
		SCRIPT_ASSERT("REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER: Cannot set up a chat call that is activated by a non-playable character. Chat call failed to register.")
		RETURN FALSE
	ENDIF
	IF iQueueTime <= 0
		SCRIPT_ASSERT("REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER: Tried to put a call in the chat queue with an invalid queue time. Time should be > 0.")
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
		//Check the chat call queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoChatCalls < CC_MAX_CHAT_CALLS
			CC_CallData sNewCallData
			
			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = 0
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.sCommData.iPlayerCharBitset = iTriggeringPlayersBitset //Seems backwards but it needs to be this way around so the comms controller doesn't get confused. Ask BenR
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.ePriority = ePriority
			sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK
			sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK
			sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.eSendCheck = eCallCheck
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_DONT_SAVE)
					
			//Save the call queue entry to the global array.
			g_savedGlobalsClifford.sCommsControlData.sChatCalls[g_savedGlobalsClifford.sCommsControlData.iNoChatCalls] = sNewCallData
			g_savedGlobalsClifford.sCommsControlData.iNoChatCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New chat call registered by ", GET_THIS_SCRIPT_NAME() ,". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID))

			RETURN TRUE
		ENDIF 
	#endif
	#if USE_NRM_DLC
		//Check the chat call queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoChatCalls < CC_MAX_CHAT_CALLS
			CC_CallData sNewCallData
			
			//Create a hashed ID for this communication.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = 0
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.sCommData.iPlayerCharBitset = iTriggeringPlayersBitset //Seems backwards but it needs to be this way around so the comms controller doesn't get confused. Ask BenR
			sNewCallData.iSpeakerID = iNonPlayableSpeaker
			sNewCallData.sCommData.ePriority = ePriority
			sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK
			sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK
			sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewCallData.sCommData.eSendCheck = eCallCheck
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_DONT_SAVE)
					
			//Save the call queue entry to the global array.
			g_savedGlobalsnorman.sCommsControlData.sChatCalls[g_savedGlobalsnorman.sCommsControlData.iNoChatCalls] = sNewCallData
			g_savedGlobalsnorman.sCommsControlData.iNoChatCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New chat call registered by ", GET_THIS_SCRIPT_NAME() ,". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID))

			RETURN TRUE
		ENDIF 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	//Check the chat call queue isn't full.
	IF g_savedGlobals.sCommsControlData.iNoChatCalls < CC_MAX_CHAT_CALLS
		CC_CallData sNewCallData
		
		//Create a hashed ID for this communication.
		sNewCallData.sCommData.eID = eID
		
		IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
			g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
		ENDIF
		
		//Setup call data.
		sNewCallData.sCommData.iSettings = 0
		sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
		sNewCallData.sCommData.iPlayerCharBitset = iTriggeringPlayersBitset //Seems backwards but it needs to be this way around so the comms controller doesn't get confused. Ask BenR
		sNewCallData.iSpeakerID = iNonPlayableSpeaker
		sNewCallData.sCommData.ePriority = ePriority
		sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK
		sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK
		sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE
		sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
		sNewCallData.sCommData.eSendCheck = eCallCheck
		sNewCallData.eCommExtra = COMM_NONE
		sNewCallData.eCommExtra2 = COMM_NONE
		SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
		SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_DONT_SAVE)
				
		//Save the call queue entry to the global array.
		g_savedGlobals.sCommsControlData.sChatCalls[g_savedGlobals.sCommsControlData.iNoChatCalls] = sNewCallData
		g_savedGlobals.sCommsControlData.iNoChatCalls++
		
		CPRINTLN(DEBUG_COMMUNICATIONS, "New chat call registered by ", GET_THIS_SCRIPT_NAME() ,". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID))

		RETURN TRUE
	ENDIF
	#endif
	#endif
	
		
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new chat call but failed!")
	SCRIPT_ASSERT("REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER: The chat call queue is full. The call data could not be added. Does CC_MAX_CHAT_CALLS need increasing?")
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Registers an end of mission phonecall with the communication controller and adds that call to a queue to be triggered as
///				soon as the controller will allow.
///    
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCharacterTo 	- A charsheet ENUM defining the playable character this phonecall is to be sent to. CHAR_BLANK_ENTRY will allow the communication to be sent to any playable character.
///    			eCharacterFrom 	- A charsheet ENUM defining the character this phonecall is being sent from.
PROC REGISTER_END_OF_MISSION_PHONECALL(	CC_CommID eID,
										enumCharacterList eCharacterTo,
										enumCharacterList eNPCCharacter,
										INT iNonPlayableSpeaker)
										
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new end of mission call but failed. The player is doing a repeat play.")
		EXIT
	ENDIF
	
	#if USE_CLF_DLC
	//Check the call queue isn't full.
	IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
		CC_CallData sNewCallData
		
		// Create a hashed ID for the end of mission call.
		sNewCallData.sCommData.eID = eID
		
		IF g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
			g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall = COMM_NONE
		ENDIF
												
		//Get a priority for the end of mission call.
		sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(CT_END_OF_MISSION)
		
		//Calculate an initial queue time for this communication.
		//Hard code to 2 seconds for very end of mission calls that need to trigger almost immediately.
		sNewCallData.sCommData.iRequeueTime = 2000
		sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + 2000
		
		//Setup call data.
		sNewCallData.sCommData.iSettings = 0
		INT iPlayerCharBitset = 0
		SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterTo))
		sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
		sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
		sNewCallData.iSpeakerID	= iNonPlayableSpeaker
		sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK 	//No restricted area for end of mission calls.
		sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK //Do not execute a codeID for end of mission calls.
		sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE 	//No custom checks to run for end of mission calls.
		sNewCallData.eCommExtra = COMM_NONE
		sNewCallData.eCommExtra2 = COMM_NONE
		SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
		
		//Save the call queue entry to the global array.
		g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls] = sNewCallData
		g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls++
		
		CPRINTLN(DEBUG_COMMUNICATIONS, "New end of mission call registered by ",GET_THIS_SCRIPT_NAME(),". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
		
		//Update character queue priority levels in case this new text is the new highest priority communication.
		//Going to all playable characters. Update all their priorities.
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)

		//Call registered sucessfully.
		EXIT
	ENDIF 
	#endif
	#if USE_NRM_DLC
	//Check the call queue isn't full.
	IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
		CC_CallData sNewCallData
		
		// Create a hashed ID for the end of mission call.
		sNewCallData.sCommData.eID = eID
		
		IF g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
			g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall = COMM_NONE
		ENDIF
												
		//Get a priority for the end of mission call.
		sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(CT_END_OF_MISSION)
		
		//Calculate an initial queue time for this communication.
		//Hard code to 2 seconds for very end of mission calls that need to trigger almost immediately.
		sNewCallData.sCommData.iRequeueTime = 2000
		sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + 2000
		
		//Setup call data.
		sNewCallData.sCommData.iSettings = 0
		INT iPlayerCharBitset = 0
		SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterTo))
		sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
		sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
		sNewCallData.iSpeakerID	= iNonPlayableSpeaker
		sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK 	//No restricted area for end of mission calls.
		sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK //Do not execute a codeID for end of mission calls.
		sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE 	//No custom checks to run for end of mission calls.
		sNewCallData.eCommExtra = COMM_NONE
		sNewCallData.eCommExtra2 = COMM_NONE
		SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
		
		//Save the call queue entry to the global array.
		g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls] = sNewCallData
		g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls++
		
		CPRINTLN(DEBUG_COMMUNICATIONS, "New end of mission call registered by ",GET_THIS_SCRIPT_NAME(),". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
		
		//Update character queue priority levels in case this new text is the new highest priority communication.
		//Going to all playable characters. Update all their priorities.
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
		PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)

		//Call registered sucessfully.
		EXIT
	ENDIF 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		//Check the call queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedCalls < CC_MAX_QUEUED_CALLS
			CC_CallData sNewCallData
			
			// Create a hashed ID for the end of mission call.
			sNewCallData.sCommData.eID = eID
			
			IF g_savedGlobals.sCommsControlData.eLastCompletedCall = sNewCallData.sCommData.eID
				g_savedGlobals.sCommsControlData.eLastCompletedCall = COMM_NONE
			ENDIF
													
			//Get a priority for the end of mission call.
			sNewCallData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(CT_END_OF_MISSION)
			
			//Calculate an initial queue time for this communication.
			//Hard code to 2 seconds for very end of mission calls that need to trigger almost immediately.
			sNewCallData.sCommData.iRequeueTime = 2000
			sNewCallData.sCommData.iQueueTime = GET_GAME_TIMER() + 2000
			
			//Setup call data.
			sNewCallData.sCommData.iSettings = 0
			INT iPlayerCharBitset = 0
			SET_BIT(iPlayerCharBitset, ENUM_TO_INT(eCharacterTo))
			sNewCallData.sCommData.iPlayerCharBitset = iPlayerCharBitset
			sNewCallData.sCommData.eNPCCharacter = eNPCCharacter
			sNewCallData.iSpeakerID	= iNonPlayableSpeaker
			sNewCallData.sCommData.eRestrictedAreaID = VID_BLANK 	//No restricted area for end of mission calls.
			sNewCallData.sCommData.eExecuteOnCompleteID = CID_BLANK //Do not execute a codeID for end of mission calls.
			sNewCallData.sCommData.eSendCheck = FLOW_CHECK_NONE 	//No custom checks to run for end of mission calls.
			sNewCallData.eCommExtra = COMM_NONE
			sNewCallData.eCommExtra2 = COMM_NONE
			SET_BIT(sNewCallData.sCommData.iSettings, COMM_BIT_CALL_REQUEUE_ON_MISS)
			
			//Save the call queue entry to the global array.
			g_savedGlobals.sCommsControlData.sQueuedCalls[g_savedGlobals.sCommsControlData.iNoQueuedCalls] = sNewCallData
			g_savedGlobals.sCommsControlData.iNoQueuedCalls++
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New end of mission call registered by ",GET_THIS_SCRIPT_NAME(),". ID:",GET_COMM_ID_DEBUG_STRING(sNewCallData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewCallData.sCommData.ePriority))
			
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)

			//Call registered sucessfully.
			EXIT
		ENDIF
	#endif	
	#endif
	
	
		
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new end of mission call but failed!")
	SCRIPT_ASSERT("REGISTER_END_OF_MISSION_PHONECALL: The call queue is full. The call data could not be added. Does CC_MAX_QUEUED_CALLS need increasing?")
	EXIT
	
ENDPROC


/// PURPOSE:	Registers a text message with the communication controller and adds it to a queue to be triggered as soon as
///				the controller will allow.
///
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCommunicationType - The type of comminication being registered. This type will determine the text message's priority.
///				iToPlayersBitset - A bitset containing which player characters this communication should be sent to.
///				eCharacterFrom - A charsheet ENUM defining the character this text message is being sent from.
///    			eCriticalStatus - An ENUM that deinfes whether this text message will appear opened as soon as the phone is brought up. (Should be reserved for key flow text messages.)
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///				eRestrictedArea - A vector ID that defines an area in the world that the player must not be in for the text to trigger.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the text is sent sucessfully.
///    			iExecuteOnCompleteDelay - A time in milliseconds to wait after the communication is sent, before executing the code ID.
///    			eCheckBeforeSend - A check ID that defines a check that must pass before this call can be sent.
///    			iForcedID - Use this argument to force a specific ID for this text message.
///				eMissionInteraction - Does this communication unlock or trigger a mission on completion?
/// RETURNS:	The unique INT ID that references the registered text message in the communication controller. This will be -1 if the text failed to register.
FUNC BOOL REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(CC_CommID eID,
														CC_CommunicationType eCommunicationType, 
														INT iToPlayersBitset,
														enumCharacterList eNPCCharacter, 
														INT iQueueTime,
														INT iRequeueTime = 10000,
														VectorID eRestrictedArea = VID_BLANK,
														CC_CodeID eExecuteOnComplete = CID_BLANK,
														FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
														INT iSettings = 0,
														enumTxtMsgCanCallSender WhichCanCallSenderStatus = CAN_CALL_SENDER)
													
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Text failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Text failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF eExecuteOnComplete = CID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
	#endif
	#endif
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
			SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Text failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: Cannot set up a text message to be sent to a non-playable character. Text failed to register.")
		RETURN FALSE
	ENDIF
	
	#if USE_CLF_DLC
	IF eExecuteOnComplete = CID_CLF_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
		//Check the text message queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = TPART_NONE
			sNewTextData.ePart2 = TPART_NONE
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts++

			CPRINTLN(DEBUG_COMMUNICATIONS, "New text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			RETURN TRUE
		ENDIF 
	#endif
	#if USE_NRM_DLC
	IF eExecuteOnComplete = CID_NRM_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
		SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
		//Check the text message queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = TPART_NONE
			sNewTextData.ePart2 = TPART_NONE
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts++

			CPRINTLN(DEBUG_COMMUNICATIONS, "New text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			RETURN TRUE
		ENDIF 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC	
		//Check the text message queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = TPART_NONE
			sNewTextData.ePart2 = TPART_NONE
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobals.sCommsControlData.sQueuedTexts[g_savedGlobals.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobals.sCommsControlData.iNoQueuedTexts++

			CPRINTLN(DEBUG_COMMUNICATIONS, "New text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			RETURN TRUE
		ENDIF
	#endif	
	#endif
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
	SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: The text message queue is full. The text data could not be added. Does CC_MAX_QUEUED_TEXTS need increasing?")
	RETURN FALSE
ENDFUNC



/// PURPOSE:	Registers a text message with a text label composed of multiple sub strings with the communication controller. 
///    			Adds it to a queue to be triggered as soon as the controller will allow.
///
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			ePart1 - An ID referencing the first substring that should be used to compose the text label for this message.
///    			ePart2 - An ID referencing the second substring that should be used to compose the text label for this message.
///    			eCommunicationType - The type of comminication being registered. This type will determine the text message's priority.
///				iToPlayersBitset - A bitset containing which player characters this communication should be sent to.
///				eCharacterFrom - A charsheet ENUM defining the character this text message is being sent from.
///    			eCriticalStatus - An ENUM that deinfes whether this text message will appear opened as soon as the phone is brought up. (Should be reserved for key flow text messages.)
///    			iQueueTime - The time this text will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///				eRestrictedArea - A vector ID that defines an area in the world that the player must not be in for the text to trigger.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the text is sent sucessfully.
///    			iExecuteOnCompleteDelay - A time in milliseconds to wait after the communication is sent, before executing the code ID.
///    			eCheckBeforeSend - A check ID that defines a check that must pass before this call can be sent.
///    			iForcedID - Use this argument to force a specific ID for this text message.
///				eMissionInteraction - Does this communication unlock or trigger a mission on completion?
/// RETURNS:	The unique INT ID that references the registered text message in the communication controller. This will be -1 if the text failed to register.
FUNC BOOL REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(	CC_CommID eID,
																	CC_TextPart ePart1,
																	CC_TextPart ePart2,
																	CC_CommunicationType eCommunicationType, 
																	INT iToPlayersBitset,
																	enumCharacterList eNPCCharacter, 
																	INT iQueueTime,
																	INT iRequeueTime = 10000,
																	VectorID eRestrictedArea = VID_BLANK,
																	CC_CodeID eExecuteOnComplete = CID_BLANK,
																	FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
																	INT iSettings = 0,
																	enumTxtMsgCanCallSender WhichCanCallSenderStatus = CAN_CALL_SENDER)
													
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Text failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Text failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	IF eExecuteOnComplete = CID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
	#endif
	#endif
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
			SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Text failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: Cannot set up a text message to be sent to a non-playable character. Text failed to register.")
		RETURN FALSE
	ENDIF
	IF ePart1 = TPART_NONE
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: First composite parts was set blank. This should be registered as a normal text message.")
		RETURN FALSE
	ENDIF
	IF ePart1 = TPART_MAX
	OR ePart2 = TPART_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: One of the composite parts was set to TPART_MAX which is not a valid value.")
		RETURN FALSE
	ENDIF
	#if USE_CLF_DLC
	IF eExecuteOnComplete = CID_CLF_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
		//Check the text message queue isn't full.
		IF g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = ePart1
			sNewTextData.ePart2 = ePart2
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts++
			
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New composite text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Part1:", GET_TEXT_PART_DEBUG_STRING(ePart1), " Part2:", GET_TEXT_PART_DEBUG_STRING(ePart2), " Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			RETURN TRUE
		ENDIF	
	#endif
	#if USE_NRM_DLC
	IF eExecuteOnComplete = CID_NRM_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new composite text message but failed!")
		SCRIPT_ASSERT("REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Text failed to register.")
		RETURN FALSE
	ENDIF
		//Check the text message queue isn't full.
		IF g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = ePart1
			sNewTextData.ePart2 = ePart2
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts++
			
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New composite text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Part1:", GET_TEXT_PART_DEBUG_STRING(ePart1), " Part2:", GET_TEXT_PART_DEBUG_STRING(ePart2), " Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			RETURN TRUE
		ENDIF	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	
		//Check the text message queue isn't full.
		IF g_savedGlobals.sCommsControlData.iNoQueuedTexts < CC_MAX_QUEUED_TEXTS
			CC_TextMessageData sNewTextData
			
			//Create a hashed ID for this communication.
			//NOTE: All txtmsgs are stored in the same block ID
			sNewTextData.sCommData.eID = eID

			//Get a priority for this communication.
			sNewTextData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
			
			//Set an initial queue time for this communication.
			sNewTextData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
			sNewTextData.sCommData.iRequeueTime = iRequeueTime
			
			//Setup text message data.
			sNewTextData.sCommData.iSettings = iSettings
			sNewTextData.sCommData.iPlayerCharBitset = iToPlayersBitset
			sNewTextData.sCommData.eNPCCharacter = eNPCCharacter
			sNewTextData.sCommData.eRestrictedAreaID = eRestrictedArea
			sNewTextData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
			sNewTextData.sCommData.eSendCheck = eCheckBeforeSend
			sNewTextData.ePart1 = ePart1
			sNewTextData.ePart2 = ePart2
			sNewTextData.WhichCanCallSenderStatus = WhichCanCallSenderStatus
			CLEAR_BIT(sNewTextData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)
			
			//Save the text entry to the global queue.
			g_savedGlobals.sCommsControlData.sQueuedTexts[g_savedGlobals.sCommsControlData.iNoQueuedTexts] = sNewTextData
			g_savedGlobals.sCommsControlData.iNoQueuedTexts++
			
			//Update character queue priority levels in case this new text is the new highest priority communication.
			//Going to all playable characters. Update all their priorities.
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_MICHAEL)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_FRANKLIN)
			PRIVATE_Update_Playable_Character_Priority_Level(CHAR_TREVOR)
			
			CPRINTLN(DEBUG_COMMUNICATIONS, "New composite text message registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewTextData.sCommData.eID)," Part1:", GET_TEXT_PART_DEBUG_STRING(ePart1), " Part2:", GET_TEXT_PART_DEBUG_STRING(ePart2), " Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewTextData.sCommData.ePriority))
			RETURN TRUE
		ENDIF
	#endif
	#endif
	
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new text message but failed!")
	SCRIPT_ASSERT("REGISTER_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER: The text message queue is full. The text data could not be added. Does CC_MAX_QUEUED_TEXTS need increasing?")
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Registers an email with the communication controller and adds it to a queue to be triggered as soon as
///				the controller will allow.
///
/// PARAMS:		eID - The enum referencing which question phonecall this is. String data stored in comms_control_data_GTA5.sch
///    			eCommunicationType - The type of comminication being registered. This type will determine the email's priority.
///				iToPlayersBitset - A bitset containing which player characters this communication should be sent to.
///				eNPCCharacter - A charsheet ENUM defining the character this email is being sent from. Set to CHAR_BLANK_ENTRY if the email isn't bound to a char sheet character.
///    			eEmailThread - The email thead that should be progressed when this communication is sent.
///    			iQueueTime - The time this email will queue before it will be considered for triggering by the communications controller. Also used for calculating requeuing times.
///				eRestrictedArea - A vector ID that defines an area in the world that the player must not be in for the email to trigger.
///    			eExecuteOnComplete -  A code ID that defines a block of script that should be executed when the email is sent sucessfully.
///    			iExecuteOnCompleteDelay - A time in milliseconds to wait after the communication is sent, before executing the code ID.
///    			eCheckBeforeSend - A check ID that defines a check that must pass before this call can be sent.
///				eMissionInteraction - Does this communication unlock or trigger a mission on completion?
/// RETURNS:	The unique INT ID that references the registered email in the communication controller. This will be -1 if the email failed to register.
FUNC BOOL REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER(	CC_CommID eID,
													CC_CommunicationType eCommunicationType, 
													INT iToPlayersBitset,
													enumCharacterList eNPCCharacter, 
													INT iQueueTime,
													INT iRequeueTime,
													VectorID eRestrictedArea = VID_BLANK,
													CC_CodeID eExecuteOnComplete = CID_BLANK,
													FLOW_CHECK_IDS eCheckBeforeSend = FLOW_CHECK_NONE,
													INT iSettings = 0)
													
	//Run argument checks.
	IF IS_REPEAT_PLAY_ACTIVE()
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed. The player is doing a repeat play.")
		RETURN FALSE
	ENDIF
	IF iQueueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
		SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: iQueueTime cannot be < 0. Email failed to register.")
		RETURN FALSE
	ENDIF
	IF iRequeueTime < 0
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
		SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: iRequeueTime cannot be < 0. Email failed to register.")
		RETURN FALSE
	ENDIF
	IF eRestrictedArea = VID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
		SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: eRestrictedArea cannot be VID_MAX. Email failed to register.")
		RETURN FALSE
	ENDIF
	
	IF eExecuteOnComplete = CID_MAX
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
		SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: eExecuteOnComplete cannot be CID_MAX. Email failed to register.")
		RETURN FALSE
	ENDIF
	IF ENUM_TO_INT(eNPCCharacter) < 3
		IF IS_BIT_SET(iToPlayersBitset, ENUM_TO_INT(eNPCCharacter))
			CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
			SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: eCharacterTo cannot be the same as eCharacterFrom. Email failed to register.")
			RETURN FALSE
		ENDIF
	ENDIF
	IF iToPlayersBitset < 1 OR iToPlayersBitset > 7
		CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
		SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: Cannot set up an email to be sent to a non-playable character. Email failed to register.")
		RETURN FALSE
	ENDIF
	
	//Check the email message queue isn't full.
	IF g_savedGlobals.sCommsControlData.iNoQueuedEmails < CC_MAX_QUEUED_EMAILS
		CC_EmailData sNewEmailData

		//Create a hashed ID for this communication.
		//NOTE: All emails are stored in the same block ID.
		sNewEmailData.sCommData.eID = eID

		//Get a priority for this communication.
		sNewEmailData.sCommData.ePriority = PRIVATE_Get_Priority_From_Communication_Type(eCommunicationType)
		
		//Set an initial queue time for this communication.
		sNewEmailData.sCommData.iQueueTime = GET_GAME_TIMER() + iQueueTime
		sNewEmailData.sCommData.iRequeueTime = iRequeueTime
		
		//Setup email data.
		sNewEmailData.sCommData.iSettings = iSettings
		sNewEmailData.sCommData.iPlayerCharBitset = iToPlayersBitset
		sNewEmailData.sCommData.eNPCCharacter = eNPCCharacter
		sNewEmailData.sCommData.eRestrictedAreaID = eRestrictedArea
		sNewEmailData.sCommData.eExecuteOnCompleteID = eExecuteOnComplete
		sNewEmailData.sCommData.eSendCheck = eCheckBeforeSend
		CLEAR_BIT(sNewEmailData.sCommData.iSettings, COMM_BIT_FROM_CHAR_IS_PLAYER)

		//Save the email entry to the global queue.
		g_savedGlobals.sCommsControlData.sQueuedEmails[g_savedGlobals.sCommsControlData.iNoQueuedEmails] = sNewEmailData
		g_savedGlobals.sCommsControlData.iNoQueuedEmails++

		CPRINTLN(DEBUG_COMMUNICATIONS, "New email registered by ", GET_THIS_SCRIPT_NAME(), ". ID:", GET_COMM_ID_DEBUG_STRING(sNewEmailData.sCommData.eID)," Priority:",PRIVATE_Get_Debug_String_For_Communication_Priority(sNewEmailData.sCommData.ePriority))
		
		RETURN TRUE
	ELSE	//	If the email queue is at capacity do a priority check.
		
		CC_EmailData sNewEmailData = PRIVATE_Setup_New_Email_Data( eID, eCommunicationType, iToPlayersBitset, eNPCCharacter, iQueueTime, iRequeueTime, eRestrictedArea, eExecuteOnComplete, eCheckBeforeSend, iSettings )
		
		INT iLowestPriorityEmailIndex = 0
		PRIVATE_Get_Lowest_Priority_Email_From_Queue( iLowestPriorityEmailIndex )
		
		COMMS_PAIRS_COMPARISONS cpcNewMailPriority = PRIVATE_Email_Priority_Comparison( sNewEmailData, g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] )
		IF cpcNewMailPriority = CPC_HIGHER
			//	Fire off CID here if the email has one thats getting thrown.
			PRIVATE_Check_And_Fire_CID( g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] )
			g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] = sNewEmailData
			PRIVATE_Clear_Email_Data( sNewEmailData )
			CDEBUG1LN( DEBUG_COMMUNICATIONS, " - comms_control_public.sch - REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER - New mail was higher priority than the lowest priority so overwritten.")
			RETURN TRUE
		ELIF cpcNewMailPriority = CPC_SAME
			//Check CID and if still the same add new, else add CID one
			IF PRIVATE_Does_Email_Have_A_Complete_ID( sNewEmailData )
				//	Fire off CID here if the email has one thats getting thrown.
				PRIVATE_Check_And_Fire_CID( g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] )
				g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] = sNewEmailData
				PRIVATE_Clear_Email_Data( sNewEmailData )
				CDEBUG1LN( DEBUG_COMMUNICATIONS, " - comms_control_public.sch - REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER - The new email had the same priority as the lowest so replaced it.")
				RETURN TRUE
			ELSE
				IF NOT PRIVATE_Does_Email_Have_A_Complete_ID( g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] )
					g_savedGlobals.sCommsControlData.sQueuedEmails[ iLowestPriorityEmailIndex ] = sNewEmailData
					PRIVATE_Clear_Email_Data( sNewEmailData )
					CDEBUG1LN( DEBUG_COMMUNICATIONS, " - comms_control_public.sch - REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER - The new email had the same priority as the lowest so replaced it.")
					RETURN TRUE
				ENDIF
				PRIVATE_Clear_Email_Data( sNewEmailData )
				CDEBUG1LN( DEBUG_COMMUNICATIONS, " - comms_control_public.sch - REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER - Lowest priority email had same priority as new email but had CID and new one didn't - scrapped new email.")
				RETURN TRUE
			ENDIF
		ELSE
			//	Fire off CID here if scrapped email has one.
			PRIVATE_Check_And_Fire_CID( sNewEmailData )
			PRIVATE_Clear_Email_Data( sNewEmailData )
			CDEBUG1LN( DEBUG_COMMUNICATIONS, " - comms_control_public.sch - REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER - Couldnt find a space in the queue for the new email due to the presendence of the emails filling the queue currently.")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	CPRINTLN(DEBUG_COMMUNICATIONS, "Attempted to register new email but failed!")
	SCRIPT_ASSERT("REGISTER_EMAIL_FROM_CHARACTER_TO_PLAYER: The email queue is full. The email data could not be added. Does CC_MAX_QUEUED_EMAILS need increasing? - Ask Rowan Jones")
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
//	Prints the email queue to Script_COMMS
PROC PRINT_EMAIL_QUEUE()

	INT iIndex = 0
		
	CDEBUG3LN( DEBUG_COMMUNICATIONS, "  " )
	CDEBUG3LN( DEBUG_COMMUNICATIONS, " This is the email queue spew... " )
	
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails iIndex
		CDEBUG3LN( DEBUG_COMMUNICATIONS, " Email: ", GET_COMM_ID_EMAIL_DEBUG_STRING( g_savedGlobals.sCommsControlData.sQueuedEmails[iIndex].sCommData.eID ), " - eID: ", g_savedGlobals.sCommsControlData.sQueuedEmails[iIndex].sCommData.eID, " - Priority: ", g_savedGlobals.sCommsControlData.sQueuedEmails[iIndex].sCommData.ePriority, " - CID: ", g_savedGlobals.sCommsControlData.sQueuedEmails[iIndex].sCommData.eExecuteOnCompleteID )
	ENDREPEAT

ENDPROC

#ENDIF



//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ Vector ID Queries ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:		Checks if a coord is contained within the area defined by a Vector ID.
///    
/// PARAM NOTES:	vPosition - The coord being checked for containment within the Vector ID.
///    				eVectorID - An enum referencing the Vector ID having its area queried.
/// RETURN:			A BOOL that will be TRUE if the coord is contained within the Vector ID's area.
FUNC BOOL IS_COORD_IN_VECTOR_ID_AREA(VECTOR vPosition, VectorID eVectorID)

	IF eVectorID != VID_BLANK
		
		IF ENUM_TO_INT(eVectorID) >= COUNT_OF(g_sVectorIDData)
			CASSERTLN(DEBUG_COMMUNICATIONS, "IS_COORD_IN_VECTOR_ID_AREA - vectorID with index ", eVectorID, " passed in, max array size is ", COUNT_OF(g_sVectorIDData), " - bail from function to avoid array overrun.")
			RETURN FALSE
		ENDIF
		
		IF VDIST2(vPosition, g_sVectorIDData[eVectorID].position) <= (g_sVectorIDData[eVectorID].radius*g_sVectorIDData[eVectorID].radius)
			RETURN TRUE
		ELIF g_sVectorIDData[eVectorID].extensionID != VID_BLANK
			RETURN IS_COORD_IN_VECTOR_ID_AREA(vPosition, g_sVectorIDData[eVectorID].extensionID)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ Communication Queries  ╞══════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


/// PURPOSE:	Checks with the communication controller to see if a call with a given ID is currently registered.
///    
/// PARAMS:		iCommunicationID - The hashed ID of the call we want to query.
/// RETURNS:	A boolean that will be true if the queried call is registered.
FUNC BOOL IS_CALL_REGISTERED(CC_CommID paramCommID)
	INT index
	
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoChatCalls index
			IF g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
	#endif	
	#endif
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks with the communication controller to see if a text message with a given ID is currently registered.
///    
/// PARAMS:		iCommunicationID - The hashed ID of the text message we want to query.
/// RETURNS:	A boolean that will be true if the queried text message is registered.
FUNC BOOL IS_TEXT_MESSAGE_REGISTERED(CC_CommID paramCommID)
	INT index
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN TRUE
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks with the communication controller to see if an email with a given ID is currently registered.
///    
/// PARAMS:		iCommunicationID - The hashed ID of the email we want to query.
/// RETURNS:	A boolean that will be true if the queried email is registered.
FUNC BOOL IS_EMAIL_REGISTERED(CC_CommID paramCommID)
	INT index
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
	#endif
	#endif
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks with the communication controller to see if a communication with a given ID is currently registered.
///    
/// PARAMS:		iCommunicationID - The hashed ID of the communication we want to query.
/// RETURNS:	A boolean that will be true if the queried communication is registered.
FUNC BOOL IS_COMMUNICATION_REGISTERED(CC_CommID paramCommID)
	INT index
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT		
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT		
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoChatCalls index
			IF g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				RETURN TRUE
			ENDIF
		ENDREPEAT
	#endif	
	#endif
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks the status of a call that is registered with the communication controller.
///
/// PARAMS:		paramCommID - The ID of the registered phonecall that we want to query. 
/// RETURNS:	An ENUM that represents the current status of the call.
FUNC CC_CommunicationStatus GET_CALL_STATUS(CC_CommID paramCommID)
	INT iGameTime = GET_GAME_TIMER()

	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	#endif
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	#endif
	#endif
	
	SCRIPT_ASSERT("GET_CALL_STATUS: Tried to get the status of a call that is not registered.")
	RETURN CS_ERROR
ENDFUNC


/// PURPOSE:	Checks the status of a text message that is registered with the communication controller.
///
/// PARAMS:		paramCommID - The ID of the registered text message that we want to query. 
/// RETURNS:	An ENUM that represents the current status of the text message.
FUNC CC_CommunicationStatus GET_TEXT_MESSAGE_STATUS(CC_CommID paramCommID)
	INT iGameTime = GET_GAME_TIMER()

	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT 
	#endif
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	SCRIPT_ASSERT("GET_TEXT_MESSAGE_STATUS: Tried to get the status of a text message that is not registered.")
	RETURN CS_ERROR
ENDFUNC


/// PURPOSE:	Checks the status of an email that is registered with the communication controller.
///
/// PARAMS:		paramCommID - The ID of the registered email that we want to query. 
/// RETURNS:	An ENUM that represents the current status of the email.
FUNC CC_CommunicationStatus GET_EMAIL_STATUS(CC_CommID paramCommID)
	INT iGameTime = GET_GAME_TIMER()

	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT 
	#endif
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	SCRIPT_ASSERT("GET_EMAIL_STATUS: Tried to get the status of an email that is not registered.")
	RETURN CS_ERROR
ENDFUNC


/// PURPOSE:	Checks the status of a communication that is registered with the communication controller.
///
/// PARAMS:		iCommunicationID - The hashed ID of the registered phonecall or text message that we want to query. 
/// RETURNS:	An ENUM that represents the current status of the communication.
FUNC CC_CommunicationStatus GET_COMMUNICATION_STATUS(CC_CommID paramCommID)
	INT iGameTime = GET_GAME_TIMER()

	//Is this communication ID queued as a call?
	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as a text message?
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as an email?
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT
	#endif
	
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as a text message?
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as an email?
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Phonecall_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as a text message?
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Text_Message_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT	
	//Is this communication ID queued as an email?
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			RETURN PRIVATE_Get_Email_Status(paramCommID, iGameTime)
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	SCRIPT_ASSERT("GET_COMMUNICATION_STATUS: Tried to get the status of a communication that is not registered.")
	RETURN CS_ERROR
ENDFUNC


/// PURPOSE:	Gets the communication ID of the last completed phonecall.
///    
/// RETURNS:	The hashed ID of the last phonecall that was sucessfully sent by the communication controller.
FUNC CC_CommID GET_LAST_COMPLETED_CALL()
	#if USE_CLF_DLC
	 	RETURN g_savedGlobalsClifford.sCommsControlData.eLastCompletedCall
	#endif
	#if USE_NRM_DLC
	 	RETURN g_savedGlobalsnorman.sCommsControlData.eLastCompletedCall
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sCommsControlData.eLastCompletedCall
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Check if the last completed call was answered.
///    
/// RETURNS:	Returns TRUE if the last call handled by the comms controller was answered.
FUNC BOOL WAS_LAST_CALL_ANSWERED()
	#if USE_CLF_DLC
	 RETURN g_savedGlobalsClifford.sCommsControlData.bLastCallAnswered
	#endif
	#if USE_NRM_DLC
	 RETURN g_savedGlobalsnorman.sCommsControlData.bLastCallAnswered
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sCommsControlData.bLastCallAnswered
	#endif	
	#endif
ENDFUNC

/// PURPOSE:	Checks if the last completed phonecall had a response set.
///    
/// RETURNS:	A BOOL that will be true if the last phonecall that was sucessfully sent by the communication controller had a response set.
FUNC BOOL DID_LAST_CALL_HAVE_RESPONSE()
	#if USE_CLF_DLC
		RETURN g_savedGlobalsClifford.sCommsControlData.bLastCallHadResponse	
	#endif
	#if USE_NRM_DLC
		RETURN g_savedGlobalsnorman.sCommsControlData.bLastCallHadResponse	
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sCommsControlData.bLastCallHadResponse	
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Gets the last phonecall question response that was set in the communication controller.  
///    
/// RETURNS:	A BOOL that represents the last YES or NO response that was chosen by the player.  
FUNC BOOL GET_LAST_CALL_RESPONSE()
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.bLastCallResponse	 
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.bLastCallResponse	 
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.bLastCallResponse	
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Gets the communication ID of the last completed text message.
///    
/// RETURNS:	The hashed ID of the last text message that was sucessfully sent by the communication controller.
FUNC CC_CommID GET_LAST_COMPLETED_TEXT_MESSAGE()
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.eLastCompletedText	 
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.eLastCompletedText	 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.eLastCompletedText	
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Checks if the last completed text message had a response set.
///    
/// RETURNS:	A BOOL that will be true if the last text message that was sucessfully sent by the communication controller had a response set.
FUNC BOOL DID_LAST_TEXT_HAVE_RESPONSE()
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.bLastTextHadResponse	 
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.bLastTextHadResponse	 
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.bLastTextHadResponse	
	#endif
	#endif 
ENDFUNC

/// PURPOSE:	Gets the last text message question response that was set in the communication controller.  
///    
/// RETURNS:	A BOOL that represents the last YES or NO response that was chosen by the player.  
FUNC BOOL GET_LAST_TEXT_RESPONSE()
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.bLastTextResponse	 
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.bLastTextResponse	 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.bLastTextResponse
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Gets the communication ID of the last completed text message.
///    
/// RETURNS:	The hashed ID of the last text message that was sucessfully sent by the communication controller.
FUNC CC_CommID GET_LAST_COMPLETED_EMAIL()
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.eLastCompletedEmail
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.eLastCompletedEmail
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.eLastCompletedEmail	
	#endif
	#endif
ENDFUNC

/// PURPOSE:	Checks all communication queues to see if a specific player character has any calls
///    			involving them queued.
///    
/// PARAMS:		ePlayerChar - The player character to check.  
/// RETURNS:	Returns TRUE if the chracter has any communications queued involving them.
FUNC BOOL DOES_PLAYER_CHAR_HAVE_COMMUNICATION_QUEUED(enumCharacterList ePlayerChar)
	
	IF NOT IS_PLAYER_PED_PLAYABLE(ePlayerChar)
		CERRORLN(DEBUG_COMMUNICATIONS, "DOES_PLAYER_CHAR_HAVE_COMMUNICATION_QUEUED: The character passed was not playable.")
	ENDIF
	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
		IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	#endif
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
		IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
		IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	
	
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:	Checks the chat call queues to see if a specific character has any chat calls available.
///    
/// PARAMS:		eChar - The NPC ped to check for available chat calls.  
/// RETURNS:	Returns TRUE if the chracter has any chat calls available.
FUNC BOOL IS_CHAT_CALL_AVAILABLE_FOR_CHARACTER(enumCharacterList eChar)
	
	//Don't allow the player to phone characters who are occupied in a mission that is
	//currently available.
	IF PRIVATE_Is_Character_Busy_In_Mission(eChar)
		RETURN FALSE
	ENDIF

	//Check for chat calls.
	INT index
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eChar
				RETURN TRUE
			ENDIF
		ENDREPEAT		
		//Check for normal phonecalls with the quick call flag set.
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eChar
				IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_CALL_IS_QUICK)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eChar
				RETURN TRUE
			ENDIF
		ENDREPEAT		
		//Check for normal phonecalls with the quick call flag set.
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eChar
				IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_CALL_IS_QUICK)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoChatCalls index
			IF g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eChar
				RETURN TRUE
			ENDIF
		ENDREPEAT		
		//Check for normal phonecalls with the quick call flag set.
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eChar
				IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iSettings, COMM_BIT_CALL_IS_QUICK)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	#endif
	#endif
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks what the current communication priority level is for a specific player ped.
///    
/// PARAMS:		ePlayerChar - The player character to check.  
/// RETURNS:	Returns an ENUM representing the priority level for the passed character.
FUNC CC_CommunicationPriority GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(enumCharacterList ePlayerChar)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePlayerChar)
		CERRORLN(DEBUG_COMMUNICATIONS, "DOES_PLAYER_CHAR_HAVE_COMMUNICATION_WITH_PRIORITY_QUEUED: The character passed was not playable.")
		RETURN CPR_ERROR
	ENDIF
	#if USE_CLF_DLC
	 	RETURN g_savedGlobalsClifford.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
	#endif
	#if USE_NRM_DLC
	 	RETURN g_savedGlobalsnorman.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		RETURN g_savedGlobals.sCommsControlData.eCharacterPriorityLevel[ePlayerChar]
	#endif
	#endif
	
ENDFUNC


/// PURPOSE:	Checks all communication queues to see if a specific character has any calls
///    			involving them queued.
///    
/// PARAMS:		eCharacter - The character to check.  
/// RETURNS:	Returns TRUE if the chracter has any communications queued involving them.
FUNC BOOL DOES_CHAR_HAVE_COMMUNICATION_QUEUED(enumCharacterList eCharacter)

	INT index
	#if USE_CLF_DLC
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT 
	#endif
	
	#if USE_NRM_DLC
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT 
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
		IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
		IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eCharacter
			RETURN TRUE
		ENDIF
	ENDREPEAT
	#endif
	#endif
	
	
	
	//If this is a player character also check the player char bitset.
	IF IS_PLAYER_PED_PLAYABLE(eCharacter)
		IF DOES_PLAYER_CHAR_HAVE_COMMUNICATION_QUEUED(eCharacter)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:	Checks call communication queue to see if a specific character has any calls
///    			involving them queued with end of mission priority.
///    
/// PARAMS:		eCharacter - The character to check.  
/// RETURNS:	Returns TRUE if the chracter has any communications queued involving them with end of mission priority.
FUNC BOOL DOES_PLAYER_HAVE_END_OF_MISSION_CALL_QUEUED(enumCharacterList eCharacter)
	
	//If this is a player character also check the player char bitset.
	IF IS_PLAYER_PED_PLAYABLE(eCharacter)
		IF DOES_PLAYER_CHAR_HAVE_COMMUNICATION_QUEUED(eCharacter)
			INT index
			#if USE_CLF_DLC
 				REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
					IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = CPR_VERY_HIGH
						RETURN TRUE
					ENDIF
				ENDREPEAT
			#endif
			#if USE_NRM_DLC
 				REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
					IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = CPR_VERY_HIGH
						RETURN TRUE
					ENDIF
				ENDREPEAT
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
					IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.ePriority = CPR_VERY_HIGH
						RETURN TRUE
					ENDIF
				ENDREPEAT
			#endif		
			#endif
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VectorID GET_QUEUED_CALL_VECTOR_ID(INT iCallQueueIndex)
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[iCallQueueIndex].sCommData.eRestrictedAreaID	 
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[iCallQueueIndex].sCommData.eRestrictedAreaID	 
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.sQueuedCalls[iCallQueueIndex].sCommData.eRestrictedAreaID	
	#endif
	#endif
ENDFUNC


FUNC VectorID GET_QUEUED_TEXT_VECTOR_ID(INT iTextQueueIndex)
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[iTextQueueIndex].sCommData.eRestrictedAreaID
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[iTextQueueIndex].sCommData.eRestrictedAreaID
	#endif
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.sQueuedTexts[iTextQueueIndex].sCommData.eRestrictedAreaID
	#endif
	#endif
ENDFUNC


FUNC VectorID GET_QUEUED_EMAIL_VECTOR_ID(INT iEmailQueueIndex)
	#if USE_CLF_DLC
	RETURN g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[iEmailQueueIndex].sCommData.eRestrictedAreaID
	#endif
	#if USE_NRM_DLC
	RETURN g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[iEmailQueueIndex].sCommData.eRestrictedAreaID
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
	RETURN g_savedGlobals.sCommsControlData.sQueuedEmails[iEmailQueueIndex].sCommData.eRestrictedAreaID
	#endif
	#endif
ENDFUNC


FUNC BOOL IS_PLAYER_IN_ANY_COMMUNICATION_RESTRICTED_AREAS()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
		IF IS_PLAYER_PED_PLAYABLE(ePlayer)
			//Optimisation. Only bother checking further if they have vaguely important calls queued.
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			INT index
			#if USE_CLF_DLC
				REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
					IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_CALL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
					IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_TEXT_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
					IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_EMAIL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			#endif
			#if USE_NRM_DLC
				REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
					IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_CALL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
					IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_TEXT_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
					IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_EMAIL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			#endif
			
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC
				REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
					IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_CALL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
					IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_TEXT_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
					IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayer))
						IF IS_COORD_IN_VECTOR_ID_AREA(vPlayerPos, GET_QUEUED_EMAIL_VECTOR_ID(index))
							RETURN TRUE
						ENDIF
					ENDIF
				ENDREPEAT
			#endif
			#endif
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:	Checks to see if there are any mission unlocking communications in
///    			any of the communication queues.
///    
/// RETURNS:	Returns TRUE if a mission unlocking communication is found to be queued.
FUNC BOOL IS_ANY_MISSION_UNLOCKING_CALL_IN_PROGRESS()

	IF g_iCallInProgress != -1
		#if USE_CLF_DLC
			IF IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[g_iCallInProgress].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
				RETURN TRUE
			ENDIF
		#endif
		#if USE_NRM_DLC
			IF IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[g_iCallInProgress].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
				RETURN TRUE
			ENDIF
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[g_iCallInProgress].sCommData.iSettings, COMM_BIT_UNLOCKS_MISSION)
				RETURN TRUE
			ENDIF
		#endif
		#endif
	ENDIF
	RETURN FALSE
	
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ Queue Interactions ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:		Removes a communication with a given ID from the communication controller's queues.
///    
/// PARAM NOTES:	iCommunicationID - The hashed ID of the communication that is to be cancelled.
/// RETURN:			A BOOL that will be TRUE if the cancel request was sucessful and FALSE if it was not.
FUNC BOOL CANCEL_COMMUNICATION(CC_CommID paramCommID)
	INT index
	BOOL bRemoved = FALSE
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobals.sCommsControlData.iNoChatCalls index
			IF g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	#endif
	
	RETURN bRemoved
ENDFUNC


/// PURPOSE:		Removes a communication with a given ID from the communication controller's queues.
///    
/// PARAM NOTES:	iCommunicationID - The hashed ID of the communication that is to be cancelled.
/// RETURN:			A BOOL that will be TRUE if the cancel request was sucessful and FALSE if it was not.
FUNC BOOL CANCEL_COMMUNICATION_BETWEEN_CHARS(CC_CommID paramCommID, enumCharacterList ePlayerChar, enumCharacterList eSenderChar)

	IF NOT IS_PLAYER_PED_PLAYABLE(ePlayerChar)
		CERRORLN(DEBUG_COMMUNICATIONS, "CANCEL_COMMUNICATION_BETWEEN_CHARS: The ePlayerChar character passed was not playable.")
	ENDIF

	INT index
	BOOL bRemoved = FALSE
	#if USE_CLF_DLC
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			AND g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsClifford.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			AND g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsClifford.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	#if USE_NRM_DLC
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoChatCalls index
			IF g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			AND g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobalsnorman.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			AND g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobalsnorman.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	
	#if not USE_CLF_DLC
	#if not USE_NRM_DLC
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedCalls index
			IF  g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				IF g_iCallInProgress != index
					PRIVATE_Remove_Call_From_Queue(index)
					PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
					bRemoved = TRUE
				ENDIF
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoMissedCalls index
			IF g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobals.sCommsControlData.sMissedCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Missed_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
		REPEAT g_savedGlobals.sCommsControlData.iNoChatCalls index
			IF g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eID = paramCommID
			AND g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobals.sCommsControlData.sChatCalls[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Call_From_Chat_Call_Queue(paramCommID)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedTexts index
			IF g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eID = paramCommID
			AND g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedTexts[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Text_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT	
		REPEAT g_savedGlobals.sCommsControlData.iNoQueuedEmails index
			IF g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eID = paramCommID
			AND g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.eNPCCharacter = eSenderChar
			AND IS_BIT_SET(g_savedGlobals.sCommsControlData.sQueuedEmails[index].sCommData.iPlayerCharBitset, ENUM_TO_INT(ePlayerChar))
				PRIVATE_Remove_Email_From_Queue(index)
				bRemoved = TRUE
			ENDIF
		ENDREPEAT
	#endif
	#endif
	
	RETURN bRemoved
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ Delay Management ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC ADD_GLOBAL_COMMUNICATION_DELAY(INT iDelayMilliseconds)
	g_iGlobalWaitTime = GET_GAME_TIMER() + iDelayMilliseconds
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " set the global communication delay to ", iDelayMilliseconds, " ms.")
	#ENDIF
ENDPROC


PROC CLEAR_GLOBAL_COMMUNICATION_DELAY()
	g_iGlobalWaitTime = GET_GAME_TIMER()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " zeroed the global communication delay.")
	#ENDIF
ENDPROC
PROC ADD_COMMUNICATION_DELAY_FOR_CHARACTERCLF(enumCharacterList eCharacter)
	//Check character is valid.
	IF eCharacter = CHAR_CLF_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_CLF_CHARACTERS
	OR eCharacter = MAX_CLF_CHARACTERS_PLUS_DUMMY
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF
	
	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER() + CC_CHARACTER_DELAY_BETWEEN_COMMS
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " set a communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETCLF(eCharacter), ".")
	#ENDIF
ENDPROC
PROC ADD_COMMUNICATION_DELAY_FOR_CHARACTERNRM(enumCharacterList eCharacter)
	//Check character is valid.
	IF eCharacter = CHAR_NRM_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_NRM_CHARACTERS
	OR eCharacter = MAX_NRM_CHARACTERS_PLUS_DUMMY
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF
	
	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER() + CC_CHARACTER_DELAY_BETWEEN_COMMS
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " set a communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETNRM(eCharacter), ".")
	#ENDIF
ENDPROC
PROC ADD_COMMUNICATION_DELAY_FOR_CHARACTER(enumCharacterList eCharacter)

	#if USE_CLF_DLC
		ADD_COMMUNICATION_DELAY_FOR_CHARACTERCLF(eCharacter)
		exit
	#endif
	#if USE_NRM_DLC
		ADD_COMMUNICATION_DELAY_FOR_CHARACTERNRM(eCharacter)
		exit
	#endif
	
	//Check character is valid.
	IF eCharacter = CHAR_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_CHARACTERS
	OR eCharacter = MAX_CHARACTERS_MP
	OR eCharacter = MAX_CHARACTERS_SP_SAVE
	OR eCharacter = MAX_CHARACTERS_PLUS_DUMMY
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF
	
	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER() + CC_CHARACTER_DELAY_BETWEEN_COMMS
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " set a communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(eCharacter), ".")
	#ENDIF
ENDPROC
PROC CLEAR_COMMUNICATION_DELAY_FOR_CHARACTERCLF(enumCharacterList eCharacter)
	//Check character is valid.
	IF eCharacter = CHAR_CLF_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_CLF_CHARACTERS
	OR eCharacter = MAX_CLF_CHARACTERS_PLUS_DUMMY
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF

	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " cleared communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETCLF(eCharacter), ".")
	#ENDIF
ENDPROC
PROC CLEAR_COMMUNICATION_DELAY_FOR_CHARACTERNRM(enumCharacterList eCharacter)
	//Check character is valid.
	IF eCharacter = CHAR_NRM_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_NRM_CHARACTERS
	OR eCharacter = MAX_NRM_CHARACTERS_PLUS_DUMMY
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF

	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " cleared communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEETNRM(eCharacter), ".")
	#ENDIF
ENDPROC
PROC CLEAR_COMMUNICATION_DELAY_FOR_CHARACTER(enumCharacterList eCharacter)

	#if USE_CLF_DLC
		CLEAR_COMMUNICATION_DELAY_FOR_CHARACTERCLF(eCharacter)
		exit
	#endif
	#if USE_NRM_DLC
		CLEAR_COMMUNICATION_DELAY_FOR_CHARACTERNRM(eCharacter)
		exit
	#endif
	
	//Check character is valid.
	IF eCharacter = CHAR_BLANK_ENTRY
	OR eCharacter = NO_CHARACTER
	OR eCharacter = MAX_CHARACTERS
	OR eCharacter = MAX_CHARACTERS_PLUS_DUMMY
	OR eCharacter = MAX_CHARACTERS_MP
	OR eCharacter = MAX_CHARACTERS_SP_SAVE
		SCRIPT_ASSERT("ADD_COMMUNICATION_DELAY_FOR_CHARACTER: Invalid character enum specified. Failed to add comms delay for character.")
		EXIT
	ENDIF

	//Set up the delay timer.
	g_iCharWaitTime[eCharacter] = GET_GAME_TIMER()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_COMMUNICATIONS, "Script ", GET_THIS_SCRIPT_NAME(), " cleared communication delay for ", GET_CHARSHEET_DISPLAY_STRING_FROM_CHARSHEET(eCharacter), ".")
	#ENDIF
ENDPROC
