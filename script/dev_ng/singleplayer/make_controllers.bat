echo #include "csupport.h" > controllers.cpp
for /f "usebackq" %%f in (`find . -name *.sc.cpp`) do echo #include "%%f" >> controllers.cpp
echo script_def *defs[] = { >> controllers.cpp
for /f "usebackq" %%f in (`find . -name *.sc.cpp`) do echo AMP%%~nf_script_def, >> controllers.cpp
echo }; >> controllers.cpp
echo #define MODULE_NAME "SCRIPT_controllers" >> controllers.cpp
echo #include "csupportentry.h" >> controllers.cpp
