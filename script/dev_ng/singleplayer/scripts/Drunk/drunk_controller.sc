USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "drunkNoticeboard_private.sch"
USING "drunk_public.sch"
USING "net_include.sch"

#IF IS_DEBUG_BUILD
	USING "drunk_debug.sch"
#ENDIF


// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	drunk_controller.sc
//		AUTHOR			:	Keith / Alwyn
//		DESCRIPTION		:	The always-active control script to control drunk peds.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

BOOL bQuit_Drunk_Camera_Called = FALSE

INT iDrunkNotices = 0
INT iDrunkPeds = 0
INT iDrunkRequest = 0

FLOAT fDrunkCameraTimeCycleModifier = -99.0

#IF IS_DEBUG_BUILD
BOOL bDrawDebugDrunkInfo
#ENDIF



// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Drunk_Controller_Cleanup()
	
	// Reset prior to launching script
	Reset_All_Drunk_Variables_And_Terminate_All_Unneeded_Scripts()
	
	IF NOT bQuit_Drunk_Camera_Called
		Quit_Drunk_Camera_Immediately()
	ENDIF
	
	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC
// PURPOSE:	Converts any notices posted by the public functions to instead be posted by the Control
//			Script. This ensures that that the notice will remain active for one frame, but then
//			get cleaned up by the standard Control Script notices cleanup in the next frame.
PROC Convert_Public_Function_Posts_To_Be_Control_Script_Posts()
	
	//REPEAT MAX_NUMBER_OF_DRUNK_NOTICES iDrunkNotices
		IF (g_drunkNotices[iDrunkNotices].poster = UNIQUE_ID_PUBLIC_FUNCTIONS)
			g_drunkNotices[iDrunkNotices].poster = UNIQUE_ID_CONTROL_SCRIPT
		ENDIF
	//ENDREPEAT
	
ENDPROC


// PURPOSE:	The controller expects each drunk script to post a 'still drunk' notice each frame to
//			ensure it hasn't terminated. If it has terminated then clean up the control variables.
//			Check each ped in turn for a 'still drunk' notice.
PROC Maintain_Still_Drunk_Notices()
	
	
	INT noticeArrayPos = NO_DRUNK_NOTICES
	//REPEAT MAX_NUMBER_OF_DRUNK_PEDS iDrunkPeds
		IF NOT (g_drunkPeds[iDrunkPeds].uniqueID = NO_UNIQUE_DRUNK_PED_ID)
			// ...found a drunk ped, so expect a 'still drunk' notice
			noticeArrayPos = Find_Position_Of_This_Drunk_Notice_From_Poster(g_drunkPeds[iDrunkPeds].uniqueID, DNID_STILL_DRUNK)
			
			IF (noticeArrayPos = NO_DRUNK_NOTICES)
				// ...message not found, so clear up the variables for this drunk ped
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_DRUNK, "<DRUNK> this unique ped ID has not reported that it still exists: ", g_drunkPeds[iDrunkPeds].uniqueID)
				#ENDIF
				
				Clear_All_Drunk_Ped_Details_For_This_ID(g_drunkPeds[iDrunkPeds].uniqueID)
			ELSE
				// ...received the message, so remove it
				Clear_One_Drunk_Notice(noticeArrayPos)
			ENDIF
		ENDIF
	//ENDREPEAT
	
ENDPROC


// PURPOSE:	Act on this Drunk Notice directed at the Drunk COntroller
//
// INPUT PARAMS:	paramArrayIndex		Array Index into the Drunk Notices array
PROC Read_This_Drunk_Controller_Notice(INT paramArrayIndex)

	g_eDrunkNoticeIDs thisNoticeID = g_drunkNotices[paramArrayIndex].notice
	
	SWITCH (thisNoticeID)
		// Ignore these
		CASE DNID_STILL_DRUNK		// already handled by a special function
		CASE DNID_BECOME_SOBER		// aimed at 'drunk' scripts
		CASE DNID_EXTENDED_TIME		// aimed at 'drunk' scripts
		CASE DNID_HIT_ALCOHOL		// aimed at 'drunk' scripts
		CASE DNID_HIT_WEED			// aimed at 'drunk' scripts
		CASE DNID_IN_VEHICLE		// aimed at 'drunk' scripts
		CASE DNID_NO_DRUNK_NOTICE	// no message ID
			BREAK
			
		DEFAULT
			SCRIPT_ASSERT("drunk: Read_This_Drunk_Controller_Notice - Unknown Drunk Notice ID. Tell Alwyn.")
			BREAK
	ENDSWITCH

ENDPROC


// PURPOSE:	Check to see if any drunk notices are directed at the Drunk Controller and deal with them.
//			Delete any that are specifically directed at the Drunk Controller.
PROC Read_All_Drunk_Notices_Directed_At_Drunk_Controller()

	
	//REPEAT MAX_NUMBER_OF_DRUNK_NOTICES iDrunkNotices
		IF NOT (g_drunkNotices[iDrunkNotices].notice = DNID_NO_DRUNK_NOTICE)
			// Is this notice directed specifically at the Drunk Controller?
			IF (g_drunkNotices[iDrunkNotices].reader = UNIQUE_ID_CONTROL_SCRIPT)
				Read_This_Drunk_Controller_Notice(iDrunkNotices)
				Clear_One_Drunk_Notice(iDrunkNotices)
			ENDIF
		ENDIF
	//ENDREPEAT

ENDPROC


// PURPOSE:	Maintains the Drunk Request at the specified Drunk Request array position.
//			Involves: error checks; requesting, loading, running 'drunk' script; setting/clearing details.
//
// INPUT PARAMS:	paramArrayIndex		Index into drunk requests array
PROC Maintain_One_Drunk_Request(INT paramArrayIndex)
	
	IF (paramArrayindex < 0)
	OR (paramArrayIndex >= MAX_NUMBER_OF_DRUNK_REQUESTS)
		SCRIPT_ASSERT("Maintain_One_Drunk_Request: array index out of bounds")
		EXIT
	ENDIF
	
	// Make sure the ped is still alive
	IF (IS_ENTITY_DEAD(g_drunkRequests[paramArrayIndex].ped))
		// ...ped is dead, so clear out this request
		Clear_One_Drunk_Request(paramArrayIndex)
		EXIT
	ENDIF

	// Make sure this ped isn't already drunk
	IF (Is_Ped_Drunk(g_drunkRequests[paramArrayIndex].ped))
		// ...ped is already drunk, so clear out this request
		Clear_One_Drunk_Request(paramArrayIndex)
		EXIT
	ENDIF
	
	// Request the script
	IF NOT (HAS_SCRIPT_LOADED("drunk"))
		REQUEST_SCRIPT("drunk")
		EXIT
	ENDIF
	
	// Make sure there is free space for this drunk ped
	INT freeDrunkPedSlot = Get_An_Empty_Drunk_Ped_Slot()
	IF (freeDrunkPedSlot = UNKNOWN_DRUNK_ARRAY_INDEX)
		// ...there are already the maximum number of drunk peds, so clear out the details of this request
		Clear_One_Drunk_Request(paramArrayIndex)
		SCRIPT_ASSERT("All drunk ped slots are full - Tell Alwyn to increase MAX_NUMBER_OF_DRUNK_PEDS")
		EXIT
	ENDIF
	
	// Store the control details for the new drunk ped
	g_drunkPeds[freeDrunkPedSlot].uniqueID		= Get_Next_Unique_Drunk_Ped_ID()
	g_drunkPeds[freeDrunkPedSlot].myPedIndex	= g_drunkRequests[paramArrayIndex].ped
	g_drunkPeds[freeDrunkPedSlot].eDrunkLevel	= DL_verydrunk
	
//	g_drunkPeds[freeDrunkPedSlot].iAlcoholHit	= 0
//	g_drunkPeds[freeDrunkPedSlot].iWeedHit		= 0
	
	// Launch the new drunk script
	g_sDrunkRequests	drunkActivationDetails = g_drunkRequests[paramArrayIndex]
	START_NEW_SCRIPT_WITH_ARGS("drunk", drunkActivationDetails, SIZE_OF(drunkActivationDetails), DEFAULT_STACK_SIZE)
	SET_SCRIPT_AS_NO_LONGER_NEEDED("drunk")
	
	Clear_One_Drunk_Request(paramArrayIndex)

ENDPROC


// PURPOSE:	Maintains all drunk requests still outstanding
PROC Maintain_All_Drunk_Requests()
	
	//REPEAT MAX_NUMBER_OF_DRUNK_REQUESTS iDrunkRequest
		IF (g_drunkRequests[iDrunkRequest].status = DS_REQUEST_SCRIPT)
			Maintain_One_Drunk_Request(iDrunkRequest)
		ENDIF
	//ENDREPEAT
	
ENDPROC


FLOAT fSavedDrunkCamTimeoutMult[8]
FLOAT fPrevDrunkCamTimeoutMult = -1

// PURPOSE:	Maintain the globals associated with the drunk camera
PROC Maintain_Drunk_Camera()

	IF NOT (g_drunkCameraActive)
		EXIT
	ENDIF
	
	INT i
	INT gameTimer = GET_GAME_TIMER()
	IF (g_drunkCameraTimeout > gameTimer)
	OR (g_drunkCameraTimeout = DRUNK_LEVEL_CONSTANT)
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> IS_PLAYER_SWITCH_IN_PROGRESS is true, delay starting ")
			#ENDIF
			EXIT
		ENDIF
		
		// timeout multiplier
		FLOAT fCurrentDrunkCamTimeoutMult = Get_Drunk_Cam_Timeout_Multiplier()
		
		FOR i = COUNT_OF(fSavedDrunkCamTimeoutMult)-1 TO 1 STEP -1
			fSavedDrunkCamTimeoutMult[i] = fSavedDrunkCamTimeoutMult[i-1]
		ENDFOR
		fSavedDrunkCamTimeoutMult[0] = fCurrentDrunkCamTimeoutMult
		FLOAT fAverageDrunkCamTimeoutMult
		
		INT iCount = 0
		REPEAT COUNT_OF(fSavedDrunkCamTimeoutMult) i
			fAverageDrunkCamTimeoutMult += fSavedDrunkCamTimeoutMult[i]
			iCount++
		ENDREPEAT
		fAverageDrunkCamTimeoutMult /= iCount
		
		// hit count multiplier
		FLOAT fDrunkCamHitMult = Get_Drunk_Cam_Hit_Count_Multiplier()
		
		IF (g_drunkCameraActualAmplitudeScalar <> g_drunkCameraDesiredAmplitudeScalar)
			FLOAT diff = g_drunkCameraDesiredAmplitudeScalar - g_drunkCameraActualAmplitudeScalar
			g_drunkCameraActualAmplitudeScalar += diff*0.1
			
			IF ABSF(g_drunkCameraActualAmplitudeScalar-g_drunkCameraDesiredAmplitudeScalar) < 0.01
				g_drunkCameraActualAmplitudeScalar = g_drunkCameraDesiredAmplitudeScalar
			ENDIF
		ENDIF
		
		IF NOT IS_GAMEPLAY_CAM_SHAKING() AND NOT IS_PLAYER_DANCING(PLAYER_ID())
			SHAKE_GAMEPLAY_CAM("DRUNK_SHAKE", g_drunkCameraActualAmplitudeScalar * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
		ENDIF
		
		IF (GET_GAME_TIMER() % 100) = 0
			IF (g_drunkCameraTimeout = DRUNK_LEVEL_CONSTANT)
				//
			ELSE
				IF fPrevDrunkCamTimeoutMult = -1
					fPrevDrunkCamTimeoutMult = fAverageDrunkCamTimeoutMult
				ENDIF
				
				SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(g_drunkCameraActualAmplitudeScalar * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
				SET_GAMEPLAY_CAM_MOTION_BLUR_SCALING_THIS_UPDATE(g_drunkCameraMotionBlur * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
				
				CDEBUG1LN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(", g_drunkCameraActualAmplitudeScalar, " * ", fAverageDrunkCamTimeoutMult, "[", fPrevDrunkCamTimeoutMult, "] ", fDrunkCamHitMult, ")")
				fPrevDrunkCamTimeoutMult = fAverageDrunkCamTimeoutMult
			ENDIF
		ENDIF
		
		IF g_drunkCameraMotionBlur * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult < 1.0
			SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(g_drunkCameraMotionBlur * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
		ELSE
			SET_GAMEPLAY_CAM_MAX_MOTION_BLUR_STRENGTH_THIS_UPDATE(1.0)
		ENDIF
		
		IF NOT IS_CINEMATIC_CAM_SHAKING()
			SHAKE_CINEMATIC_CAM("DRUNK_SHAKE", g_drunkCameraActualAmplitudeScalar * g_drunkenCinematicCamMultiplier * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
		ENDIF
		
		SET_CINEMATIC_CAM_SHAKE_AMPLITUDE(g_drunkCameraActualAmplitudeScalar * g_drunkenCinematicCamMultiplier * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
		
		IF DOES_CAM_EXIST(g_drunkCameraIndex)
			IF IS_CAM_SHAKING(g_drunkCameraIndex)
				SET_CAM_SHAKE_AMPLITUDE(g_drunkCameraIndex, g_drunkCameraActualAmplitudeScalar * fAverageDrunkCamTimeoutMult * fDrunkCamHitMult)
			ENDIF
		ENDIF
		
		
		IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneScript)
		AND NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneName)
			START_AUDIO_SCENE(g_drunkAudioSceneName)
			g_drunkAudioSceneScript = ""
		ENDIF
		
		INVALIDATE_IDLE_CAM()
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sDrunkTimerBoost = ""
		#ENDIF
		
		IF g_drunkCameraTimeCycleModifier > 0
			IF (fDrunkCameraTimeCycleModifier <> g_drunkCameraTimeCycleModifier)
				
				IF GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() <> -1
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(): ", GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(), ") - delay calling SET_TRANSITION_TIMECYCLE_MODIFIER")
					#ENDIF
					
				ELSE
					IF NOT GET_IS_TIMECYCLE_TRANSITIONING_OUT()
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> SET_TRANSITION_TIMECYCLE_MODIFIER(\"", g_drunkCameraTimeCycleName, "\", 5.0, ", g_drunkCameraTimeCycleModifier, ")")
						#ENDIF
						
						SET_TRANSITION_TIMECYCLE_MODIFIER(g_drunkCameraTimeCycleName, 15.0)
						fDrunkCameraTimeCycleModifier = g_drunkCameraTimeCycleModifier	
						
						IF g_drunkCameraEffectStrength != 1.0
							SET_TIMECYCLE_MODIFIER_STRENGTH(g_drunkCameraEffectStrength)
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> SET_TRANSITION_TIMECYCLE_MODIFIER transitioning out (", fDrunkCameraTimeCycleModifier, " <> ", g_drunkCameraTimeCycleModifier, ", \"", g_drunkCameraTimeCycleName, "\")... ")
						#ENDIF
						
				//		IF NOT GET_IS_TIMECYCLE_TRANSITIONING_OUT()
				//			SET_TRANSITION_TIMECYCLE_MODIFIER(g_drunkCameraTimeCycleName, 15.0)
				//		ENDIF
				//		fDrunkCameraTimeCycleModifier = g_drunkCameraTimeCycleModifier
					ENDIF
				ENDIF
			ELSE
				IF GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() <> -1 AND GET_TIMECYCLE_MODIFIER_INDEX() <> -1
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> <RESET LOCAL TCM> GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(): ", GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX(), ")")
					CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> <RESET LOCAL TCM> GET_TIMECYCLE_MODIFIER_INDEX(): ", GET_TIMECYCLE_MODIFIER_INDEX(), ")")
					#ENDIF
					fDrunkCameraTimeCycleModifier = -99.0
				ENDIF
				
				SET_AUDIO_SPECIAL_EFFECT_MODE(AUDIO_SPECIAL_EFFECT_MODE_STONED)
				INT iDrunkCameraTimeoutRemaining = g_drunkCameraTimeout - gameTimer
				IF iDrunkCameraTimeoutRemaining <= (g_drunkCameraTimeoutDuration/2)
				AND g_drunkCameraTimeout != DRUNK_LEVEL_CONSTANT
					
					BOOL bPlayerHasSpeechPlaying = FALSE
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						IF IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
							bPlayerHasSpeechPlaying = TRUE
						ENDIF
					ENDIF
					
					IF Is_Ped_Drunk(PLAYER_PED_ID())
						g_drunkCameraTimeout += 1000
						
						#IF IS_DEBUG_BUILD
						INT drunkPedArrayIndex = Get_Drunk_Ped_Array_Index_For_This_Ped_Index(PLAYER_PED_ID())
						
						sDrunkTimerBoost  = "player drunk ["
						IF NOT (drunkPedArrayIndex = UNKNOWN_DRUNK_ARRAY_INDEX)
							sDrunkTimerBoost += drunkPedArrayIndex
							sDrunkTimerBoost += ", "
							sDrunkTimerBoost += g_drunkRequests[drunkPedArrayIndex].overall_msec
							sDrunkTimerBoost += "s"
						ELSE
							sDrunkTimerBoost += "invalid"
						ENDIF
						sDrunkTimerBoost += "]"
						#ENDIF
					ELIF bPlayerHasSpeechPlaying
						g_drunkCameraTimeout += 1000
						
						#IF IS_DEBUG_BUILD
						sDrunkTimerBoost  = "speech playing"
						#ENDIF
					ELIF GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() <> -1
						g_drunkCameraTimeout += 1000
						
						#IF IS_DEBUG_BUILD
						sDrunkTimerBoost  = "TCM transition ["
						sDrunkTimerBoost += GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX()
						sDrunkTimerBoost += "]"
						#ENDIF
					ELIF IS_LOCAL_PLAYER_VIEWING_CCTV()
						g_drunkCameraTimeout += 1000
						
						#IF IS_DEBUG_BUILD
						sDrunkTimerBoost  = "using CCTV"
						#ENDIF
					ELSE
						IF GET_TIMECYCLE_MODIFIER_INDEX() <> -1
							SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(TO_FLOAT(g_drunkCameraTimeoutDuration/2) / 1000.0)
						ENDIF
						
						fDrunkCameraTimeCycleModifier = -99.0
						g_drunkCameraTimeCycleModifier = 0.0
						g_drunkCameraTimeCycleName = ""
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		INT iColumn = 0
		HUD_COLOURS eTimeoutColour = HUD_COLOUR_PURE_WHITE
		IF NOT (fCurrentDrunkCamTimeoutMult = 1.0)
			eTimeoutColour = HUD_COLOUR_RED
		ENDIF
		HUD_COLOURS eHitColour = HUD_COLOUR_PURE_WHITE
		IF NOT (fDrunkCamHitMult = 1.0)
			eHitColour = HUD_COLOUR_RED
		ENDIF
		
		DrawLiteralDrunkString("Maintain_Drunk_Camera()",															iColumn, HUD_COLOUR_PURE_WHITE)		iColumn++
		
		IF (g_drunkCameraTimeout <> DRUNK_LEVEL_CONSTANT)
			INT iDrunkCameraTimeoutRemaining = g_drunkCameraTimeout - gameTimer
			DrawLiteralDrunkStringFloat("time: ",			TO_FLOAT(iDrunkCameraTimeoutRemaining) / 1000.0,		iColumn, HUD_COLOUR_PURE_WHITE)		iColumn++
		ELSE
			DrawLiteralDrunkString("time: LEVEL_CONSTANT",															iColumn, HUD_COLOUR_GREYLIGHT)		iColumn++
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sDrunkTimerBoost)
			CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> sDrunkTimerBoost: \"", sDrunkTimerBoost, "\"")
			DrawLiteralDrunkString(sDrunkTimerBoost,																iColumn, HUD_COLOUR_GREYLIGHT)		iColumn++
		ENDIF
		
		IF (g_drunkCameraActualAmplitudeScalar <> g_drunkCameraDesiredAmplitudeScalar)
			DrawLiteralDrunkStringFloat("desiredAmplitudeScalar: ",	g_drunkCameraDesiredAmplitudeScalar,			iColumn, HUD_COLOUR_GREENDARK)		iColumn++
			DrawLiteralDrunkStringFloat("actualAmplitudeScalar: ",	g_drunkCameraActualAmplitudeScalar,				iColumn, HUD_COLOUR_GREENDARK)		iColumn++
		ELSE
			DrawLiteralDrunkStringFloat("amplitudeScalar: ",		g_drunkCameraActualAmplitudeScalar,				iColumn, HUD_COLOUR_GREEN)			iColumn++
		ENDIF
		DrawLiteralDrunkStringFloat("motionBlur: ",					g_drunkCameraMotionBlur,						iColumn, HUD_COLOUR_GREEN)			iColumn++
		DrawLiteralDrunkStringInt("timeoutDuration: ",				g_drunkCameraTimeoutDuration,					iColumn, HUD_COLOUR_PURE_WHITE)		iColumn++
		DrawLiteralDrunkStringFloat("timeoutMult : ",				fCurrentDrunkCamTimeoutMult,					iColumn, eTimeoutColour)			iColumn++
		DrawLiteralDrunkStringFloat("hitMult: ",					fDrunkCamHitMult,								iColumn, eHitColour)				iColumn++
		
		
		TEXT_LABEL_63 sLiteral = "\""
		sLiteral += g_drunkCameraTimeCycleName
		sLiteral += "\" tcm: "
		DrawLiteralDrunkStringFloat(sLiteral,						g_drunkCameraTimeCycleModifier,					iColumn, HUD_COLOUR_PURPLE)			iColumn++
		
		IF NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneScript)
		OR NOT IS_STRING_NULL_OR_EMPTY(g_drunkAudioSceneName)
			
			TEXT_LABEL_63 sNewLiteral = "scene: "
			sNewLiteral += g_drunkAudioSceneScript
			sNewLiteral += ", "
			sNewLiteral += g_drunkAudioSceneName
	
			DrawLiteralDrunkString(sNewLiteral,																		iColumn, eHitColour)				iColumn++
		ENDIF
		
		
		#ENDIF
		
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_DRUNK, "<Maintain_Drunk_Camera> end drunk cam - g_drunkCameraTimeout: ", g_drunkCameraTimeout, "[gameTimer: ", gameTimer, "]")
	
	REPEAT COUNT_OF(fSavedDrunkCamTimeoutMult) i
		fSavedDrunkCamTimeoutMult[i] = 0
	ENDREPEAT
	fPrevDrunkCamTimeoutMult = -1
	fDrunkCameraTimeCycleModifier = -99
	
	Quit_Drunk_Camera_Immediately()
	bQuit_Drunk_Camera_Called= TRUE
ENDPROC


SCRIPT
	CPRINTLN(DEBUG_DRUNK, "Starting drunk_controller.sc")
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
	
	// This script needs to cleanup only when the game runs the magdemo
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO))
		CPRINTLN(DEBUG_DRUNK, "...drunk_controller.sc has been forced to cleanup (MAGDEMO)")
		Drunk_Controller_Cleanup()
	ENDIF
	
	// Additional debug items
	#IF IS_DEBUG_BUILD
		Drunk_Debug_Initialise()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawDebugDrunkInfo")
		CPRINTLN(DEBUG_DRUNK, "...drunk_controller.sc has activated g_bDrawDebugDrunkInfo = TRUE (command line param)")
		g_bDrawDebugDrunkInfo = TRUE
	ENDIF
	#ENDIF
	
	WHILE (TRUE)
		WAIT(0)
		
		#IF IS_DEBUG_BUILD
			Update_Drunk_Widget_Variables()
			
			IF (bDrawDebugDrunkInfo <> g_bDrawDebugDrunkInfo)
				IF g_bDrawDebugDrunkInfo
					bDrawDebugDrunkInfo = g_bDrawDebugDrunkInfo
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				ENDIF
			ENDIF
		#ENDIF	
		
		// Clear out any previously sent notices by the drunk controller
		Clear_All_Notices_From_This_Unique_Drunk_Ped_ID(UNIQUE_ID_CONTROL_SCRIPT)
		
		// PUBLIC_FUNCTION poster notices get converted to be CONTROL_SCRIPT
		Convert_Public_Function_Posts_To_Be_Control_Script_Posts()
		
		// Check each drunk ped to make sure they have sent a 'still drunk' notice
		Maintain_Still_Drunk_Notices()
		
		// Read and act on any other notices directed at the drunk controller
		Read_All_Drunk_Notices_Directed_At_Drunk_Controller()
		
		// Maintain peds that have requested to be drunk
		Maintain_All_Drunk_Requests()
		
		// Maintain the Drunk Camera
		Maintain_Drunk_Camera()
		
		iDrunkNotices++
		IF iDrunkNotices >= MAX_NUMBER_OF_DRUNK_NOTICES
			iDrunkNotices = 0
		ENDIF
		iDrunkPeds++
		IF iDrunkPeds >= MAX_NUMBER_OF_DRUNK_PEDS
			iDrunkPeds = 0
		ENDIF
		iDrunkRequest++
		IF iDrunkRequest >= MAX_NUMBER_OF_DRUNK_REQUESTS
			iDrunkRequest = 0
		ENDIF
		
		IF NOT Should_Drunk_Controller_Be_Running()
	
	#IF IS_DEBUG_BUILD
		AND NOT g_bDebug_KeepDrunkControllerRunning
	#ENDIF

			CPRINTLN(DEBUG_DRUNK, "...drunk_controller.sc has been forced to cleanup (not running)")
			Drunk_Controller_Cleanup()
		ENDIF
	ENDWHILE		

ENDSCRIPT

