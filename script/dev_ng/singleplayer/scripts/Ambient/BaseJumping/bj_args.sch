
//////////////////////////////////////////////////////////////////////
/* bj_args.sch														*/
/* Author: DJ Jones													*/
/* Arguments passed in to the base jump activity.					*/
//////////////////////////////////////////////////////////////////////


// Arguments for single or multiplayer script.
STRUCT BJ_ARGS
	STATIC_BLIP_NAME_ENUM staticBlip
	
	MODEL_NAMES targetModel
	MODEL_NAMES dropoffModel
	MODEL_NAMES exitModel
	
	STRING sDialogueRoot
	STRING sDialogueLabel
	
	VECTOR vTargetVehicleVectOffset
	VECTOR vExitPos
	VECTOR vTargetPos[iMAX_GATES]
	VECTOR vCutsceneStartPos
	VECTOR vCutsceneStartRot
	VECTOR vCutsceneEndPos
	VECTOR vCutsceneEndRot
	VECTOR vLookPos
	VECTOR vRemoveCarsMinCoords
	VECTOR vRemoveCarsMaxCoords
	VECTOR vDropoffPos
	VECTOR vTargetVehicleParkCoords
	
	FLOAT fLaunchHeading
	FLOAT fTargetHeading
	FLOAT fDropoffHeading
	FLOAT fExitHeading
	FLOAT fFlyawayDelay
	FLOAT fCutsceneStartFOV
	FLOAT fCutsceneEndFOV
	
	BOOL bCutsceneFade
	BOOL bHoldLastCutsceneFrame
ENDSTRUCT

// Accessor for static blip
FUNC STATIC_BLIP_NAME_ENUM BJ_GET_ARG_STATIC_BLIP(BJ_ARGS& bjArgs)
	RETURN bjArgs.staticBlip
ENDFUNC

// Mutator for static blip
PROC BJ_SET_ARG_STATIC_BLIP(BJ_ARGS& bjArgs, STATIC_BLIP_NAME_ENUM staticBlip)
	bjArgs.staticBlip = staticBlip
ENDPROC

// Accessor for dialogue label
FUNC STRING BJ_GET_ARG_DIALOGUE_LABEL(BJ_ARGS& bjArgs)
	RETURN bjArgs.sDialogueLabel
ENDFUNC

// Mutator for dialogue label
PROC BJ_SET_ARG_DIALOGUE_LABEL(BJ_ARGS& bjArgs, STRING sDialogueLabel)
	bjArgs.sDialogueLabel = sDialogueLabel
ENDPROC

// Accessor for dialogue root
FUNC STRING BJ_GET_ARG_CUTSCENE_DLG_ROOT(BJ_ARGS& bjArgs)
	RETURN bjArgs.sDialogueRoot
ENDFUNC

// Mutator for dialogue root
PROC BJ_SET_ARG_CUTSCENE_DLG_ROOT(BJ_ARGS& bjArgs, STRING sDialogueRoot)
	bjArgs.sDialogueRoot = sDialogueRoot
ENDPROC

// Accessor for target vehicle model.
FUNC MODEL_NAMES BJ_GET_ARG_TARGET_MODEL(BJ_ARGS& bjArgs)
	RETURN bjArgs.targetModel
ENDFUNC

// Mutator for target vehicle model.
PROC BJ_SET_ARG_TARGET_MODEL(BJ_ARGS& bjArgs, MODEL_NAMES targetModel)
	bjArgs.targetModel = targetModel
ENDPROC

// Accessor for exit vehicle model.
FUNC MODEL_NAMES BJ_GET_ARG_EXIT_MODEL(BJ_ARGS& bjArgs)
	RETURN bjArgs.exitModel
ENDFUNC

// Mutator for exit vehicle model.
PROC BJ_SET_ARG_EXIT_MODEL(BJ_ARGS& bjArgs, MODEL_NAMES exitModel)
	bjArgs.exitModel = exitModel
ENDPROC

// Accessor for helicopter model (when player is dropped off by a heli).
FUNC MODEL_NAMES BJ_GET_ARG_DROPOFF_MODEL(BJ_ARGS& bjArgs)
	RETURN bjArgs.dropoffModel
ENDFUNC

// Mutator for helicopter model (when player is dropped off by a heli).
PROC BJ_SET_ARG_DROPOFF_MODEL(BJ_ARGS& bjArgs, MODEL_NAMES dropoffModel)
	bjArgs.dropoffModel = dropoffModel
ENDPROC

// Accessor for helicopter position (when player is dropped off by a heli).
FUNC VECTOR BJ_GET_ARG_DROPOFF_POSITION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vDropoffPos
ENDFUNC

// Mutator for helicopter position (when player is dropped off by a heli).
PROC BJ_SET_ARG_DROPOFF_POSITION(BJ_ARGS& bjArgs, VECTOR vDropoffPos)
	bjArgs.vDropoffPos = vDropoffPos
ENDPROC

// Mutator for helicopter heading (when player is dropped off by a heli).
PROC BJ_SET_ARG_DROPOFF_HEADING(BJ_ARGS& bjArgs, FLOAT fDropoffHeading)
	bjArgs.fDropoffHeading = fDropoffHeading
ENDPROC

// Accessor for helicopter heading (when player is dropped off by a heli).
FUNC FLOAT BJ_GET_ARG_DROPOFF_HEADING(BJ_ARGS& bjArgs)
	RETURN bjArgs.fDropoffHeading
ENDFUNC

// Accessor for position of vehicle placed for player's convenience.
FUNC VECTOR BJ_GET_ARG_EXIT_POSITION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vExitPos
ENDFUNC

// Mutator for position of vehicle placed for player's convenience
PROC BJ_SET_ARG_EXIT_POSITION(BJ_ARGS& bjArgs, VECTOR vExitPos)
	bjArgs.vExitPos = vExitPos
ENDPROC

// Mutator for heading of vehicle placed for player's convenience.
PROC BJ_SET_ARG_EXIT_HEADING(BJ_ARGS& bjArgs, FLOAT fExitHeading)
	bjArgs.fExitHeading = fExitHeading
ENDPROC

// Accessor for heading of vehicle placed for player's convenience.
FUNC FLOAT BJ_GET_ARG_EXIT_HEADING(BJ_ARGS& bjArgs)
	RETURN bjArgs.fExitHeading
ENDFUNC

// Mutator for helicopter flyaway delay (when player is dropped off by a heli).
PROC BJ_SET_ARG_FLYAWAY_DELAY(BJ_ARGS& bjArgs, FLOAT fFlyawayDelay)
	bjArgs.fFlyawayDelay = fFlyawayDelay
ENDPROC

// Accessor for helicopter flyaway delay (when player is dropped off by a heli).
FUNC FLOAT BJ_GET_ARG_FLYAWAY_DELAY(BJ_ARGS& bjArgs)
	RETURN bjArgs.fFlyawayDelay
ENDFUNC

// Accessor for target position.
FUNC VECTOR BJ_GET_ARG_TARGET_POSITION(BJ_ARGS& bjArgs, INT iGate)
	IF iGate < 0 OR iGate >= iMAX_GATES
		PRINTLN("Invalid gate number passed in to BJ_GET_TARGET_POSITION! = ")
		RETURN <<0,0,0>>
	ENDIF	
	
	RETURN bjArgs.vTargetPos[iGate]
ENDFUNC

// Mutator for target position.
PROC BJ_SET_ARG_TARGET_POSITION(BJ_ARGS& bjArgs, INT iGate, VECTOR vTargetPos)
	IF iGate < 0 OR iGate >= iMAX_GATES
		PRINTLN("Invalid gate number passed in to BJ_SET_TARGET_POSITION! = ")
		EXIT
	ENDIF
	
	bjArgs.vTargetPos[iGate] = vTargetPos
ENDPROC

// Accessor for cutscene start position.
FUNC VECTOR BJ_GET_ARG_CUTSCENE_START_POSITION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vCutsceneStartPos
ENDFUNC

// Mutator for cutscene start position.
PROC BJ_SET_ARG_CUTSCENE_START_POSITION(BJ_ARGS& bjArgs, VECTOR vCutsceneStartPos)
	bjArgs.vCutsceneStartPos =  vCutsceneStartPos
ENDPROC

// Accessor for cutscene end position.
FUNC VECTOR BJ_GET_ARG_CUTSCENE_END_POSITION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vCutsceneEndPos
ENDFUNC

// Mutator for cutscene end position.
PROC BJ_SET_ARG_CUTSCENE_END_POSITION(BJ_ARGS& bjArgs, VECTOR vCutsceneEndPos)
	bjArgs.vCutsceneEndPos =  vCutsceneEndPos
ENDPROC

// Accessor for cutscene start rotation.
FUNC VECTOR BJ_GET_ARG_CUTSCENE_START_ROTATION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vCutsceneStartRot
ENDFUNC

// Mutator for cutscene start rotation.
PROC BJ_SET_ARG_CUTSCENE_START_ROTATION(BJ_ARGS& bjArgs, VECTOR vCutsceneStartRot)
	bjArgs.vCutsceneStartRot =  vCutsceneStartRot
ENDPROC

// Accessor for cutscene end rotation.
FUNC VECTOR BJ_GET_ARG_CUTSCENE_END_ROTATION(BJ_ARGS& bjArgs)
	RETURN bjArgs.vCutsceneEndRot
ENDFUNC

// Mutator for cutscene end rotation.
PROC BJ_SET_ARG_CUTSCENE_END_ROTATION(BJ_ARGS& bjArgs, VECTOR vCutsceneEndRot)
	bjArgs.vCutsceneEndRot =  vCutsceneEndRot
ENDPROC

// Accessor for launch heading.
FUNC FLOAT BJ_GET_ARG_LAUNCH_HEADING(BJ_ARGS& bjArgs)
	RETURN bjArgs.fLaunchHeading
ENDFUNC

// Accessor for car removal coords
FUNC VECTOR BJ_GET_ARG_CAR_REMOVAL_COORDS_MIN(BJ_ARGS& bjArgs)
	RETURN bjArgs.vRemoveCarsMinCoords
ENDFUNC

// Mutator for car removal coords
PROC BJ_SET_ARG_CAR_REMOVAL_COORDS_MIN(BJ_ARGS& bjArgs, VECTOR vRemoveCarsMinCoords)
	bjArgs.vRemoveCarsMinCoords =  vRemoveCarsMinCoords
ENDPROC

// Accessor for car removal coords
FUNC VECTOR BJ_GET_ARG_CAR_REMOVAL_COORDS_MAX(BJ_ARGS& bjArgs)
	RETURN bjArgs.vRemoveCarsMaxCoords
ENDFUNC

// Mutator for car removal coords
PROC BJ_SET_ARG_CAR_REMOVAL_COORDS_MAX(BJ_ARGS& bjArgs, VECTOR vRemoveCarsMaxCoords)
	bjArgs.vRemoveCarsMaxCoords =  vRemoveCarsMaxCoords
ENDPROC

// Mutator for launch heading.
PROC BJ_SET_ARG_LAUNCH_HEADING(BJ_ARGS& bjArgs, FLOAT fLaunchHeading)
	bjArgs.fLaunchHeading = fLaunchHeading
ENDPROC

// Accessor for target heading.
FUNC FLOAT BJ_GET_ARG_TARGET_HEADING(BJ_ARGS& bjArgs)
	RETURN bjArgs.fTargetHeading
ENDFUNC

// Mutator for target heading.
PROC BJ_SET_ARG_TARGET_HEADING(BJ_ARGS& bjArgs, FLOAT fTargetHeading)
	bjArgs.fTargetHeading = fTargetHeading
ENDPROC

// Accessor for cutscene start FOV.
FUNC FLOAT BJ_GET_ARG_CUTSCENE_START_FOV(BJ_ARGS& bjArgs)
	RETURN bjArgs.fCutsceneStartFOV
ENDFUNC

// Mutator for cutscene start FOV.
PROC BJ_SET_ARG_CUTSCENE_START_FOV(BJ_ARGS& bjArgs, FLOAT fCutsceneStartFOV)
	bjArgs.fCutsceneStartFOV = fCutsceneStartFOV
ENDPROC

// Accessor for cutscene end FOV.
FUNC FLOAT BJ_GET_ARG_CUTSCENE_END_FOV(BJ_ARGS& bjArgs)
	RETURN bjArgs.fCutsceneEndFOV
ENDFUNC

// Mutator for cutscene end FOV.
PROC BJ_SET_ARG_CUTSCENE_END_FOV(BJ_ARGS& bjArgs, FLOAT fCutsceneEndFOV)
	bjArgs.fCutsceneEndFOV = fCutsceneEndFOV
ENDPROC

// Accessor for target vehicle bullseye offset.
FUNC VECTOR BJ_GET_ARG_TARGET_VEHICLE_OFFSET(BJ_ARGS& bjArgs)
	RETURN bjArgs.vTargetVehicleVectOffset
ENDFUNC

// Mutator for target vehicle bullseye offset.
PROC BJ_SET_ARG_TARGET_VEHICLE_OFFSET(BJ_ARGS& bjArgs, VECTOR vTargetVehicleVectOffset)
	bjArgs.vTargetVehicleVectOffset = vTargetVehicleVectOffset
ENDPROC

// Accessor for whether to fade out after the cutscene.
FUNC BOOL BJ_GET_ARG_CUTSCENE_FADE(BJ_ARGS& bjArgs)
	RETURN bjArgs.bCutsceneFade
ENDFUNC

// Mutator for whether to fade out after the cutscene.
PROC BJ_SET_ARG_CUTSCENE_FADE(BJ_ARGS& bjArgs, BOOL bCutsceneFade)
	bjArgs.bCutsceneFade = bCutsceneFade
ENDPROC

// Accessor for whether to hold the last frame at the end of the cutscene.
FUNC BOOL BJ_GET_ARG_HOLD_LAST_CUTSCENE_FRAME(BJ_ARGS& bjArgs)
	RETURN bjArgs.bHoldLastCutsceneFrame
ENDFUNC

// Mutator for whether to hold the last frame at the end of the cutscene.
PROC BJ_SET_ARG_HOLD_LAST_CUTSCENE_FRAME(BJ_ARGS& bjArgs, BOOL bHoldLastCutsceneFrame)
	bjArgs.bHoldLastCutsceneFrame = bHoldLastCutsceneFrame
ENDPROC

