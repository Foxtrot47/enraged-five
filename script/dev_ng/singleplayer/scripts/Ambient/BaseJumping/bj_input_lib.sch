
//////////////////////////////////////////////////////////////////////
/* bj_input_lib.sch													*/
/* Authors: DJ Jones & Yomal Perera									*/
/* Input functionality for base jumping oddjob.						*/
//////////////////////////////////////////////////////////////////////


// Copy current frame's input data to last frame and clear.
PROC BJ_STORE_LAST_BUTTONS(BJ_INPUT& input)
	input.iLastButtons = input.iButtons
	input.iButtons = 0
ENDPROC

// Collect a frame of local input.
PROC BJ_GET_LOCAL_INPUT(BJ_INPUT& input)
	VECTOR vRightStick
	INT iLeftX, iLeftY, iRightX, iRightY
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	// Get analogue stick data.
	GET_ANALOG_STICK_VALUES(iLeftX, iLeftY, iRightX, iRightY, IS_PLAYER_CONTROL_ON(PLAYER_ID()), IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL))
	
//	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
	//vLeftStick.x = TO_FLOAT(iLeftX) / 128.0
	//vLeftStick.y = TO_FLOAT(iLeftY) / -128.0
	vRightStick.x = TO_FLOAT(iRightX) / 128.0
	vRightStick.y = TO_FLOAT(iRightY) / -128.0
	
	// Store the stick values.
	//BJ_SET_LEFT_STICK_XY(input, vLeftStick.x, vLeftStick.y)
	BJ_SET_RIGHT_STICK_XY(input, vRightStick.x, vRightStick.y)
	
	// Store last frame's button data.
	BJ_STORE_LAST_BUTTONS(input)
	
	// Check for buttons.
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_LAUNCH, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
	OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_VEH_LAUNCH, TRUE)
	ENDIF
	IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_CUTSCENE_SKIP, TRUE)
	ENDIF
	IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_JUMP) // Fixes B* 1861373 - Jump input was disabled in golf course - Steve R. LDS
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_RETRY, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ENDSCREEN_EXPAND)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_ENDSCREEN_RETRY, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,				INPUT_FRONTEND_ENDSCREEN_ACCEPT)
	OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,	INPUT_FRONTEND_PAUSE_ALTERNATE)		// B*2276517 - Player can now press esc to close endscreen, if player control is on during endscreen
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_ENDSCREEN_CONTINUE, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_YES, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_NO, TRUE)
	ENDIF
	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEADERBOARD)
		BJ_SET_BUTTON_PRESSED(input, BJBUTTON_LEADERBOARD, TRUE)
	ENDIF
	
ENDPROC
