
//////////////////////////////////////////////////////////////////////
/* bj_input.sch														*/
/* Authors: DJ Jones & Yomal Perera									*/
/* Input definitions for base jumping oddjob.						*/
//////////////////////////////////////////////////////////////////////

// Buttons that can be pressed during the game.
ENUM BJ_BUTTONS
	BJBUTTON_LAUNCH,
	BJBUTTON_VEH_LAUNCH,
	BJBUTTON_CUTSCENE_SKIP,
	BJBUTTON_RETRY,
	BJBUTTON_ENDSCREEN_RETRY,
	BJBUTTON_ENDSCREEN_CONTINUE,
	BJBUTTON_YES,
	BJBUTTON_NO,
	BJBUTTON_LEADERBOARD,
	BJBUTTONS
ENDENUM


// One frame's worth of input data. Stick inputs stored in world space.
STRUCT BJ_INPUT
//	FLOAT fLeftStickX
//	FLOAT fLeftStickY
	FLOAT fRightStickX
	FLOAT fRightStickY
	INT iButtons
	INT iLastButtons
	INT iInputDelayStartTime = 0
	INT iInputDelayLastCalledTime = 0
ENDSTRUCT

/*
// Accessor for left stick x position.
FUNC FLOAT BJ_GET_LEFT_STICK_X(BJ_INPUT& input)
	RETURN input.fLeftStickX
ENDFUNC

// Mutator for left stick x position.
PROC BJ_SET_LEFT_STICK_X(BJ_INPUT& input, FLOAT fLeftStickX)
	input.fLeftStickX = fLeftStickX
ENDPROC

// Accessor for left stick y position.
FUNC FLOAT BJ_GET_LEFT_STICK_Y(BJ_INPUT& input)
	RETURN input.fLeftStickY
ENDFUNC

// Mutator for left stick y position.
PROC BJ_SET_LEFT_STICK_Y(BJ_INPUT& input, FLOAT fLeftStickY)
	input.fLeftStickY = fLeftStickY
ENDPROC

// Mutator for left stick x & y positions.
PROC BJ_SET_LEFT_STICK_XY(BJ_INPUT& input, FLOAT fLeftStickX, FLOAT fLeftStickY)
	BJ_SET_LEFT_STICK_X(input, fLeftStickX)
	BJ_SET_LEFT_STICK_Y(input, fLeftStickY)
ENDPROC
*/
// Accessor for right stick x position.
FUNC FLOAT BJ_GET_RIGHT_STICK_X(BJ_INPUT& input)
	RETURN input.fRightStickX
ENDFUNC

// Mutator for right stick x position.
PROC BJ_SET_RIGHT_STICK_X(BJ_INPUT& input, FLOAT fRightStickX)
	input.fRightStickX = fRightStickX
ENDPROC

// Accessor for right stick y position.
FUNC FLOAT BJ_GET_RIGHT_STICK_Y(BJ_INPUT& input)
	RETURN input.fRightStickY
ENDFUNC

// Mutator for right stick y position.
PROC BJ_SET_RIGHT_STICK_Y(BJ_INPUT& input, FLOAT fRightStickY)
	input.fRightStickY = fRightStickY
ENDPROC

// Mutator for left stick x & y positions.
PROC BJ_SET_RIGHT_STICK_XY(BJ_INPUT& input, FLOAT fRightStickX, FLOAT fRightStickY)
	BJ_SET_RIGHT_STICK_X(input, fRightStickX)
	BJ_SET_RIGHT_STICK_Y(input, fRightStickY)
ENDPROC
/*
// Accessor for left stick position, returned as a vector.
FUNC VECTOR BJ_GET_LEFT_STICK_AS_VECTOR(BJ_INPUT& input)
	RETURN <<input.fLeftStickX, input.fLeftStickY, 0.0>>
ENDFUNC

// Mutator for left stick using a vector as an argument.
PROC BJ_SET_LEFT_STICK_AS_VECTOR(BJ_INPUT& input, VECTOR vLeftStick)
	input.fLeftStickX = vLeftStick.x
	input.fLeftStickY = vLeftStick.y
ENDPROC
*/
// Accessor for left stick position, returned as a vector.
FUNC VECTOR BJ_GET_RIGHT_STICK_AS_VECTOR(BJ_INPUT& input)
	RETURN <<input.fRightStickX, input.fRightStickY, 0.0>>
ENDFUNC

// Mutator for left stick using a vector as an argument.
PROC BJ_SET_RIGHT_STICK_AS_VECTOR(BJ_INPUT& input, VECTOR vRightStick)
	input.fRightStickX = vRightStick.x
	input.fRightStickY = vRightStick.y
ENDPROC

// Check whether a specific button is down this frame.
FUNC BOOL BJ_IS_BUTTON_PRESSED(BJ_INPUT& input, BJ_BUTTONS button)
	RETURN IS_BIT_SET(input.iButtons, ENUM_TO_INT(button))
ENDFUNC

// Check whether a specific button was pressed this frame (buffered).
FUNC BOOL BJ_IS_BUTTON_JUST_PRESSED(BJ_INPUT& input, BJ_BUTTONS button)
	RETURN IS_BIT_SET(input.iButtons, ENUM_TO_INT(button)) AND NOT IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button))
ENDFUNC

//// Check whether a specific button is being pressed this frame after a set amount of time.
//FUNC BOOL BJ_IS_BUTTON_PRESSED_WITH_DELAY(BJ_INPUT& input, BJ_BUTTONS button, INT skipDelay = CUTSCENE_SKIP_DELAY)
//	//Reset time if time has passed
//	IF (GET_GAME_TIMER() - input.iInputDelayLastCalledTime) > skipDelay
//		input.iInputDelayStartTime = GET_GAME_TIMER()
//	ENDIF
//
//	input.iInputDelayLastCalledTime = GET_GAME_TIMER()
//
//	IF (GET_GAME_TIMER() - input.iInputDelayStartTime) > skipDelay
//		IF IS_BIT_SET(input.iButtons, ENUM_TO_INT(button)) AND NOT IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button))
//			input.iInputDelayStartTime = GET_GAME_TIMER()
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

// Check whether a specific button was pressed this frame after a set amount of time (buffered).
FUNC BOOL BJ_IS_BUTTON_JUST_PRESSED_WITH_DELAY(BJ_INPUT& input, BJ_BUTTONS button, INT skipDelay = CUTSCENE_SKIP_DELAY)
	//Reset time if time has passed
	IF (GET_GAME_TIMER() - input.iInputDelayLastCalledTime) > skipDelay
		input.iInputDelayStartTime = GET_GAME_TIMER()
	ENDIF

	input.iInputDelayLastCalledTime = GET_GAME_TIMER()

	IF (GET_GAME_TIMER() - input.iInputDelayStartTime) > skipDelay
		IF IS_BIT_SET(input.iButtons, ENUM_TO_INT(button)) AND NOT IS_BIT_SET(input.iLastButtons, ENUM_TO_INT(button))
			input.iInputDelayStartTime = GET_GAME_TIMER()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// Mutator for whether a specific button is down this frame.
PROC BJ_SET_BUTTON_PRESSED(BJ_INPUT& input, BJ_BUTTONS button, BOOL bPressed)
	IF bPressed
		SET_BIT(input.iButtons, ENUM_TO_INT(button))
	ELSE
		CLEAR_BIT(input.iButtons, ENUM_TO_INT(button))
	ENDIF
ENDPROC

