//////////////////////////////////////////////////////////////////////
/* bj.sc															*/
/* Author: DJ Jones													*/
/* Single player script for base jump activity.						*/
//////////////////////////////////////////////////////////////////////


USING "cutscene_public.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "socialclub_leaderboard.sch"
USING "minigame_big_message.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "traffic_default_values.sch"
USING "mission_titles_private.sch"
USING "rgeneral_include.sch"
USING "vector_id_public.sch"
//USING "traffic.sch"
// See blip_ambient.sch and vector_id_public.sch for more includes
USING "bj_tweak.sch"
USING "bj_input.sch"
USING "bj_camera.sch"
USING "bj_ui.sch"
USING "bj_input_lib.sch"
USING "bj_camera_lib.sch"
#IF IS_DEBUG_BUILD USING "bj_debug.sch" #ENDIF
USING "bj_core.sch"
USING "bj_ui_lib.sch"

//this needed to be tweaked for NextGen
//B* 1741550
CONST_FLOAT LOAD_SCENE_FAR_CLIP 4000.0

// Turn on when volume tool is needed.
CONST_INT iUSE_Z_VOLUMES 0
#IF iUSE_Z_VOLUMES
	USING "z_volumes.sch"
#ENDIF

// Script entry point.
SCRIPT(BJ_LAUNCHER_ARGS bjLauncherArgs)

	STREAMED_MODEL assets[iBJ_STREAMS]
	MODEL_NAMES launchDriverModel
	MODEL_NAMES targetDriverModel
	BJ_ARGS bjArgs
	BJ_INPUT input
	BJ_CAMERA_SET cameraSet
	BJ_UI bjUI
	BJ_GAME_STATE gameState
	BJ_GATE_CHECK_RESULT gateCheckResult
	BJ_SCORECARD_RETURN_VALUE scorecardReturn
	SCRIPT_SCALEFORM_BIG_MESSAGE bigMessageUI
	MG_FAIL_SPLASH bjSplashStruct
	MG_FAIL_FADE_EFFECT bjDeathEffect
	BJ_JUMP_ID eJumpID
	PED_PARACHUTE_LANDING_TYPE savedLandingType = PPLT_INVALID
	TIMEOFDAY currentTime
	structTimer flowTimer
	structTimer camTimer
	structTimer inputTimer
	structTimer radioTimer
	structTimer dialogueTimer
	structTimer tEndDelayTimer 
	structTimer tmrDropoff
	structTimer tmrFade
	structTimer tmrDitch
	structTimer tmrSave
	structTimer tmrOnlookers
	structTimer tmrLandingAlpha
	structTimer tmrAnticipation
	structTimer tmrUnfreeze
	structTimer tmrCutsceneDlg
	structTimer tmrObjective
	structPedsForConversation conversationPedStruct
	VEHICLE_INDEX launchVehicle = bjLauncherArgs.myHeli
	VEHICLE_INDEX targetVehicle
	VEHICLE_INDEX targetFlatbed
	VEHICLE_INDEX extraVehicle
	VEHICLE_INDEX exitVehicle
	VEHICLE_INDEX vehDriving
	VEHICLE_INDEX vehNearby[3]
	VEHICLE_INDEX vehPlayer
	VEHICLE_COMBAT_AVOIDANCE_AREA_INDEX bjCombatAvoidanceArea
	PED_INDEX launchDriverPed = bjLauncherArgs.myPilot
	PED_INDEX targetDriverPed
	PED_INDEX onlookerArray[iBJ_MAX_ONLOOKERS]
	BLIP_INDEX curGateBlip, nextGateBlip
	SEQUENCE_INDEX jumpSequence
	SEQUENCE_INDEX seqCutscene
	CHECKPOINT_INDEX curCheckpoint
	CHECKPOINT_INDEX nextCheckpoint
	SCALEFORM_INDEX	bjUI_Leaderboard
	WEAPON_TYPE playerWeapon
	STRING bjFailString = "BJ_FAIL"
	STRING bjFailStrapline = ""
	TEXT_LABEL_15 sMessage
	VECTOR vLookCamRot
	//VECTOR vCameraPos
	VECTOR vCameraDir
	VECTOR vPlayerTlprt
	VECTOR BJ_TARGET_VEHICLE_LOCATE = <<5, 5, 10>>
	VECTOR vCameraVelocity
	VECTOR vFocalPoint, vFocalOffset
	VECTOR vBaseLookCamRot
	FLOAT fBaseLookCamFOV
	FLOAT rewardFactor = 1.0
	FLOAT fLastScreenPosX, fLastScreenPosY, fLastGutterRotation
	FLOAT fLandingDistance = -1
	INT iSoundID_01 = -1
	INT iCurrentGate, iGates
	INT iCurCheckpoint = -1
	INT iGatesHit
	INT iAccuracy, iReward
	INT iJumpID = g_savedGlobals.sBasejumpData.iLaunchRank
	INT iMessagingFlags
	INT bitDif
	INT iNumPlayerLines = 1
	INT iSceneID = -1
	INT iVehicleCount
	INT iJumpIndex
	INT iAnticipation
	INT iWindSound
	INT iParachuteTint
	INT iParachutePackTint
	INT iParachuteReserveTint
	BOOL bAllClear, bBASEJumpClear, bSkydiveClear, bPlayerDiedLanding
	BOOL bRetry, bFlyaway
	BOOL bHasLanded
	BOOL bDebugPass, bDebugFail, bDebugSkip
	BOOL bWanderFail
	BOOL bLBToggle = FALSE
	BOOL bCatchUpCam
	BOOL bCutsceneStarted
	BOOL bRestartedFromDeath
	BOOL bCutsceneDlg
	BOOL bFirstFrame = TRUE
	BOOL bCutsceneSkip
	BOOL bFinishedFailScreen = FALSE
	BOOL bPlayerFinishedInVehicle = FALSE
	BOOL bUseElevationWind
	BOOL bUseCustomWind = TRUE
	BOOL bDoesPlayerHaveOwnParachute = FALSE
	BOOL bDoesPlayerHaveReserveParachute = FALSE
	BOOL bWinScreenKeyPressed = FALSE
	
	IF DOES_ENTITY_EXIST(bjLauncherArgs.lastPlrVehicle)
		vehPlayer = bjLauncherArgs.lastPlrVehicle
	ELSE
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
	ENDIF
	
	bDoesPlayerHaveOwnParachute = HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
	
	bDoesPlayerHaveReserveParachute = GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
	
	IF bDoesPlayerHaveOwnParachute
		GET_PLAYER_PARACHUTE_TINT_INDEX(PLAYER_ID(), iParachuteTint)
		GET_PLAYER_PARACHUTE_PACK_TINT_INDEX(PLAYER_ID(), iParachutePackTint)
	ENDIF
	
	IF bDoesPlayerHaveReserveParachute
		GET_PLAYER_RESERVE_PARACHUTE_TINT_INDEX(PLAYER_ID(), iParachuteReserveTint)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayer)
		SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, TRUE, TRUE)
	ENDIF
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		BJ_SCRIPT_CLEANUP(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, conversationPedStruct, launchDriverPed, targetDriverPed, onlookerArray, iWindSound, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
	ENDIF
	
	IF iJumpID < 0
		SCRIPT_ASSERT("Did not get a valid jump, so quitting")
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_savedGlobals.sBasejumpData.iCompletedFlags, iJumpID)
		rewardFactor = 0.10
	ENDIF
	
	eJumpID = INT_TO_ENUM(BJ_JUMP_ID, iJumpID)
	
	MINIGAME_DISPLAY_MISSION_TITLE(ENUM_TO_INT(MINIGAME_BASEJUMPING))
	
	CLEAR_HELP()
	
	SET_MISSION_FLAG(TRUE)
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE)
	
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(1500)
	
	BJ_GET_JUMP_INFORMATION_BY_ID(bjArgs, eJumpID)
	
	//make sure we grab the stuff from the launcher script (bjLauncherArgs)
	IF DOES_ENTITY_EXIST(launchDriverPed)
		SET_ENTITY_AS_MISSION_ENTITY(launchDriverPed, TRUE, TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(launchVehicle)
		SET_ENTITY_AS_MISSION_ENTITY(launchVehicle, TRUE, TRUE)
		IF BJ_GET_ARG_DROPOFF_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
		AND GET_ENTITY_MODEL(launchVehicle) = BJ_GET_ARG_DROPOFF_MODEL(bjArgs)
			extraVehicle = launchVehicle
			launchVehicle = NULL
		ENDIF
	ENDIF
	
	// Main loop.
	WHILE TRUE
		IF NOT bFirstFrame
			WAIT(0)
		ELSE
			bFirstFrame = FALSE
		ENDIF
		
		// See if the user wants to quit the script.
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				bDebugPass				= TRUE
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				bDebugFail = TRUE	
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				bDebugSkip = TRUE
			ENDIF
		#ENDIF
		
		#IF iUSE_Z_VOLUMES
		#IF IS_DEBUG_BUILD
			UPDATE_ZVOLUME_WIDGETS()
		#ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD IF gameState >= BJGAMESTATE_CUTSCENE
			BJ_UPDATE_DEBUG(bjArgs, ENUM_TO_INT(gameState), iCurrentGate, iGates, launchVehicle)
		ENDIF #ENDIF
		
		// Update the last time the player was known to be alive.
		IF gameState >= BJGAMESTATE_FREE_JUMP AND gameState <= BJGAMESTATE_PROCESS_DATA
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			currentTime = GET_CURRENT_TIMEOFDAY()
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
		ENDIF
		
		BJ_HIDE_HUD_AND_DISABLE_CONTROLS_EVERY_FRAME()
		
		// Collect input.
		BJ_GET_LOCAL_INPUT(input)
		
		BJ_UPDATE_MUSIC(bjUI, gameState)
		BJ_UPDATE_AUDIO_SCENES(bjUI, gameState, eJumpID, bRetry)
		
		// Act based on the current game state.
		SWITCH gameState
			// Perform initialization, begin streaming
			CASE BJGAMESTATE_INIT
				DISABLE_SELECTOR()
				
				IF IS_SCREEN_FADING_OUT()
					BREAK					
				ENDIF
				
				BJ_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
				
				//setup for escaping death
				SET_FAIL_FADE_EFFECT_SCRIPT_CONTROL(TRUE)				
				
				REMOVE_HELP_FROM_FLOW_QUEUE("AM_H_BASEJ")
				CLEAR_PRINTS()
				CLEAR_HELP()
				IF eJumpID = BJJUMPID_HARBOR
					PRINTLN("Turning off roads for BJ")
					SET_ROADS_IN_ANGLED_AREA(<<-832.271, -1525.112, -100>>, <<-1187.833, -1876.646, 100>>, 50, TRUE, FALSE)
					bjCombatAvoidanceArea = ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<-832.271, -1525.112, -100>>, <<-1187.833, -1876.646, 100>>, 50)
				ELIF eJumpID = BJJUMPID_CRANE
					PRINTLN("Turning off roads for BJ")
					SET_ROADS_IN_ANGLED_AREA(<<-129.031, -726.381, 35>>, <<-38.185, -465.801, 100>>, 75, TRUE, FALSE)
				ELIF eJumpID = BJJUMPID_MAZE_BANK
					PRINTLN("Clearing BJ start location")
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<  -74.9632,  -827.4467,  324.9521>>,  25, TRUE)
					CLEAR_AREA(<<  -74.9632,  -827.4467,  324.9521>>, 25, TRUE)
				ELIF eJumpID = BJJUMPID_GOLF_COURSE
					PRINTLN("Clearing BJ start location")
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<  -74.9632,  -827.4467,  324.9521>>,  25, TRUE)
					CLEAR_AREA(<< -807.0730,   330.8846,  232.6766>>, 25, TRUE)	
				ENDIF
				
				IF DOES_ENTITY_EXIST(launchVehicle)
					START_TIMER_NOW(tmrFade)
				ENDIF
				
				IF eJumpID = BJJUMPID_RIVER_CLIFF
				OR eJumpID = BJJUMPID_ROCK_CLIFF
				OR eJumpID = BJJUMPID_CRANE
					IF DOES_ENTITY_EXIST(bjLauncherArgs.objLaunchPack)
						SET_ENTITY_AS_MISSION_ENTITY(bjLauncherArgs.objLaunchPack, DEFAULT, TRUE)
						DELETE_OBJECT(bjLauncherArgs.objLaunchPack)
					ENDIF
					
					//BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
					BJ_SETUP_CUTSCENE_CAMERAS(cameraSet, bjArgs, eJumpID, eJumpID = BJJUMPID_RIVER_CLIFF)
					bCutsceneStarted = TRUE
				ENDIF
				
				gameState = BJGAMESTATE_LOAD_CUTSCENE
			BREAK
			
			// Stream the surrounding area.
			CASE BJGAMESTATE_LOAD_CUTSCENE
				IF IS_SCREEN_FADED_OUT()
				OR ((eJumpID = BJJUMPID_CRANE
				OR eJumpID = BJJUMPID_RIVER_CLIFF
				OR eJumpID = BJJUMPID_ROCK_CLIFF) AND NOT bRetry)
//					IF eJumpID = BJJUMPID_RIVER_CLIFF OR eJumpID = BJJUMPID_ROCK_CLIFF
//						BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
//					ENDIF
					
					IF NOT bRetry
						IF eJumpID <> BJJUMPID_CRANE AND eJumpID <> BJJUMPID_RIVER_CLIFF AND eJumpID <> BJJUMPID_ROCK_CLIFF
							vCameraDir = BJ_GET_COURSE_INITIAL_CAMERA_ROTATION_BY_ID(eJumpID)
							vCameraDir = NORMALISE_VECTOR(<<COS(vCameraDir.z), SIN(vCameraDir.z), TAN(vCameraDir.x)>>)
							IF BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(eJumpID)
								NEW_LOAD_SCENE_START(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), vCameraDir, LOAD_SCENE_FAR_CLIP)
							ELSE
								PRINTLN("BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD returned false, so not running new load scene")
							ENDIF
						ENDIF
						BJ_INIT_JUMP(bjArgs, eJumpID, assets, launchDriverModel, targetDriverModel, bjUI_Leaderboard, iAnticipation)
					
					ELSE
//						bjUI_Leaderboard = REQUEST_SC_LEADERBOARD_UI()
						IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
							// Taken from heli look cam generation.
							vFocalOffset = 1.2 * <<SIN(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), -COS(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), 0.0>>
							vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(launchVehicle, <<1.12046, -0.317773, 1.3385>>)
							vFocalOffset = ROTATE_VECTOR_ABOUT_Z(vFocalOffset, 8.9090)
							IF BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(eJumpID)
								NEW_LOAD_SCENE_START(vFocalPoint + vFocalOffset, vFocalOffset, LOAD_SCENE_FAR_CLIP)
							ELSE
								PRINTLN("BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD returned false, so not running new load scene")
							ENDIF
						ELSE
							vCameraDir = <<-10.0, 0.0, BJ_GET_ARG_LAUNCH_HEADING(bjArgs)>>
							vCameraDir = NORMALISE_VECTOR(<<COS(vCameraDir.z), SIN(vCameraDir.z), TAN(vCameraDir.x)>>)
							IF BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(eJumpID)
								NEW_LOAD_SCENE_START(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), vCameraDir, LOAD_SCENE_FAR_CLIP)
							ELSE
								PRINTLN("BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD returned false, so not running new load scene")
							ENDIF
						ENDIF						
					ENDIF
					
					IF bRetry OR eJumpID = BJJUMPID_MAZE_BANK OR eJumpID = BJJUMPID_GOLF_COURSE OR eJumpID = BJJUMPID_NORTH_CLIFF
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
						ENDIF
						SET_ENTITY_COORDS(PLAYER_PED_ID(), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), FALSE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					
					gameState = BJGAMESTATE_STREAMING
				
				ELSE
					IF (NOT IS_SCREEN_FADING_OUT()) AND ((NOT IS_TIMER_STARTED(tmrFade)) OR GET_TIMER_IN_SECONDS(tmrFade) > 0.05)
						IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
							DO_SCREEN_FADE_OUT(iLAUNCH_FADE_OUT_TIME)
						ELSE
							DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Wait for fade out and streaming to finish.
			CASE BJGAMESTATE_STREAMING
//				IF NOT IS_SCREEN_FADED_OUT()
//				AND (eJumpID = BJJUMPID_RIVER_CLIFF OR eJumpID = BJJUMPID_ROCK_CLIFF)
//					BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
//				ENDIF
				
				IF BJ_IS_STREAMING_COMPLETE(assets, flowTimer, bjUI_Leaderboard, eJumpID, iAnticipation, (eJumpID <> BJJUMPID_CRANE AND eJumpID <> BJJUMPID_RIVER_CLIFF AND eJumpID <> BJJUMPID_ROCK_CLIFF))
					CANCEL_TIMER(flowTimer)
					BJ_SETUP_JUMP(bjArgs, conversationPedStruct, iGates, launchVehicle, targetVehicle, extraVehicle, targetFlatbed, exitVehicle, launchDriverPed, targetDriverPed, launchDriverModel, targetDriverModel, radioTimer, eJumpID, iSceneID)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
					IF (NOT bRetry) AND (NOT bCutsceneStarted)
						BJ_SETUP_CUTSCENE_CAMERAS(cameraSet, bjArgs, eJumpID)
					ENDIF
					//onlooker stuff:
					#IF IS_DEBUG_BUILD IF NOT bRetry
						BJ_SETUP_DEBUG(bjArgs, eJumpID)
					ENDIF #ENDIF
					
					gameState = BJGAMESTATE_POST_STREAM
				ENDIF
			BREAK
			
			// Wait an additional amount of time for setup to complete before fading in.
			CASE BJGAMESTATE_POST_STREAM
//				IF NOT IS_SCREEN_FADED_OUT()
//				AND (eJumpID = BJJUMPID_RIVER_CLIFF OR eJumpID = BJJUMPID_ROCK_CLIFF)
//					BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
//				ENDIF
				
				IF NOT IS_TIMER_STARTED(flowTimer) AND NOT bRetry
					ODDJOB_ENTER_CUTSCENE()
					START_TIMER_NOW(flowTimer)
				ELIF (IS_TIMER_STARTED(flowTimer) AND GET_TIMER_IN_SECONDS(flowTimer) > 0.2) OR bRetry
					IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID)) AND NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@basejump@", "Heli_door_loop")
						// nothing
					ELIF bRetry
						DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
						IF IS_TIMER_STARTED(flowTimer)
							CANCEL_TIMER(flowTimer)
						ENDIF
						IF BJ_GET_ARG_DROPOFF_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
							FREEZE_ENTITY_POSITION(extraVehicle, FALSE)
							START_TIMER_NOW(tmrDropoff)
						ENDIF
						gameState = BJGAMESTATE_CUTSCENE
					
					ELSE
						CANCEL_TIMER(flowTimer)
						IF IS_SCREEN_FADED_OUT()
							WAIT(1000)
							SET_CAM_ACTIVE_WITH_INTERP(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), iCUTSCENE_INTERP_DUR)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							DO_SCREEN_FADE_IN(iFADE_IN_TIME)
						ENDIF
						
//							IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
//								dlgLabel = BJ_GET_ARG_DIALOGUE_LABEL(bjArgs)
//							ELSE
//								dlgLabel = BJ_GET_MODIFIED_PLAYER_DIALOGUE_LABEL(bjArgs)
//							ENDIF
//							
//							BJ_PLAY_DIALOGUE_LINE(conversationPedStruct, BJ_GET_ARG_CUTSCENE_DLG_ROOT(bjArgs), dlgLabel )
						
						NEW_LOAD_SCENE_STOP()
						
						IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
							// Taken from heli look cam generation.
							vFocalOffset = 1.2 * <<SIN(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), -COS(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), 0.0>>
							vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(launchVehicle, <<1.12046, -0.317773, 1.3385>>)
							vFocalOffset = ROTATE_VECTOR_ABOUT_Z(vFocalOffset, 8.9090)
							IF BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(eJumpID)
								NEW_LOAD_SCENE_START(vFocalPoint + vFocalOffset, vFocalOffset, LOAD_SCENE_FAR_CLIP)
							ELSE
								PRINTLN("BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD returned false, so not running new load scene")
							ENDIF
						ELSE
							vCameraDir = <<-10.0, 0.0, BJ_GET_ARG_LAUNCH_HEADING(bjArgs)>>
							vCameraDir = NORMALISE_VECTOR(<<COS(vCameraDir.z), SIN(vCameraDir.z), TAN(vCameraDir.x)>>)
							IF BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(eJumpID)
								NEW_LOAD_SCENE_START(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), vCameraDir, LOAD_SCENE_FAR_CLIP)
							ELSE
								PRINTLN("BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD returned false, so not running new load scene")
							ENDIF
						ENDIF						
						
						IF (NOT IS_VECTOR_ZERO(BJ_GET_CUTSCENE_WALK_START_BY_ID(eJumpID))) AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
							IF GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
							ENDIF
							SET_ENTITY_COORDS(PLAYER_PED_ID(), BJ_GET_CUTSCENE_WALK_START_BY_ID(eJumpID))
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS(BJ_GET_CUTSCENE_WALK_START_BY_ID(eJumpID), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID)))
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
							
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
							ENDIF
							
							OPEN_SEQUENCE_TASK(seqCutscene)
								IF BJ_GET_CUTSCENE_WALK_DELAY_BY_ID(eJumpID) > 0
									TASK_STAND_STILL(NULL, BJ_GET_CUTSCENE_WALK_DELAY_BY_ID(eJumpID))
								ENDIF
								//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
								TASK_GO_STRAIGHT_TO_COORD(NULL, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
							CLOSE_SEQUENCE_TASK(seqCutscene)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqCutscene)
							CLEAR_SEQUENCE_TASK(seqCutscene)
						ENDIF
						
						IF NOT BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehNearby)
							ENDIF
							
							REPEAT COUNT_OF(vehNearby) iVehicleCount
								IF DOES_ENTITY_EXIST(vehNearby[iVehicleCount]) AND NOT IS_ENTITY_DEAD(vehNearby[iVehicleCount])
									IF VDIST2(GET_ENTITY_COORDS(vehNearby[iVehicleCount], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 2500.0
										SET_ENTITY_LOD_DIST(vehNearby[iVehicleCount], 1000)
										SET_VEHICLE_LOD_MULTIPLIER(vehNearby[iVehicleCount], 5)
										SET_ENTITY_LOAD_COLLISION_FLAG(vehNearby[iVehicleCount], TRUE)
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
						
						IF BJ_GET_ARG_DROPOFF_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
							//FREEZE_ENTITY_POSITION(extraVehicle, FALSE)
							START_TIMER_NOW(tmrDropoff)
						ENDIF
						
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						ENDIF
						
						IF BJ_GET_ANTICIPATION_DELAY_BY_ID(eJumpID, iAnticipation) > 0.0
							START_TIMER_NOW(tmrAnticipation)
						ENDIF
												
						gameState = BJGAMESTATE_CUTSCENE
					ENDIF
				ENDIF
			BREAK
			
			// Play a custcene showing the player, vehicles, and targets.
			CASE BJGAMESTATE_CUTSCENE
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				// Handle debug pass/fail.
				IF bDebugPass OR bDebugFail
					IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(300)
						WAIT(300)
					ENDIF
					
					CLEAR_HELP()
					CLEAR_PRINTS()
					RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					gameState = BJGAMESTATE_PROCESS_DATA
				
				// See if it's time to go to setup.
				ELIF IS_SCREEN_FADED_OUT()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					//LOAD_SCENE(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
					IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
						BJ_SETUP_HELI_LOOK_CAMERA(cameraSet, bjArgs, vLookCamRot, vFocalPoint, vFocalOffset, launchVehicle, vBaseLookCamRot, fBaseLookCamFOV)
						iWindSound = GET_SOUND_ID()
						IF bUseCustomWind
							PLAY_SOUND_FROM_ENTITY(iWindSound, "Helicopter_Wind_Idle", launchVehicle, "BASEJUMPS_SOUNDS")
						ENDIF
						IF bUseElevationWind
							SCRIPT_OVERRIDES_WIND_ELEVATION(TRUE, HASH("WEATHER_TYPES_HIGH_ELEVATION_BASE_JUMP_HELI"))
						ENDIF
					ELSE
						IF IS_SCRIPT_GLOBAL_SHAKING()
							STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
						ENDIF
						RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					ENDIF
					
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					CLEAR_PED_ENV_DIRT(PLAYER_PED_ID())
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
					RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
							
					gameState = BJGAMESTATE_SETUP_JUMP
				
				ELSE
					//BJ_UPDATE_GATES(bjArgs, targetVehicle, launchDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
					
					IF NOT bCutsceneDlg
						IF NOT IS_TIMER_STARTED(tmrCutsceneDlg)
							START_TIMER_NOW(tmrCutsceneDlg)
						ELIF GET_TIMER_IN_SECONDS(tmrCutsceneDlg) > 4.0
						OR (BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID)) AND GET_TIMER_IN_SECONDS(tmrCutsceneDlg) > 0.5)
							CREATE_CONVERSATION(conversationPedStruct, "OJBJAUD", BJ_GET_ARG_CUTSCENE_DLG_ROOT(bjArgs), CONV_PRIORITY_HIGH)
							bCutsceneDlg = TRUE
						ENDIF
					ENDIF
					
					// See if it's time to fade the screen out or go to setup.
					IF NOT IS_SCREEN_FADING_OUT() AND NOT bRetry
						IF BJ_UPDATE_CUTSCENE(bjArgs, eJumpID, extraVehicle, cameraSet, input, camTimer, inputTimer, tmrDropoff, tmrAnticipation, iAnticipation, bFlyaway, bCutsceneSkip, bDebugSkip)
							CLEAR_HELP()
							bDebugSkip = FALSE
							
							// Start fade out and wait (see above).
							IF BJ_GET_ARG_CUTSCENE_FADE(bjArgs)
								DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
							
							// Go immediately to setup.
							ELSE
								
								//SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								DISPLAY_RADAR(FALSE)
								DISPLAY_HUD(FALSE)
								gameState = BJGAMESTATE_SETUP_JUMP
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Load the scene near the player.
			CASE BJGAMESTATE_SETUP_JUMP
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				CLEAR_RANK_REDICTION_DETAILS() 
				
				SC_LEADERBOARD_CACHE_CLEAR_ALL()

				//BJ_UPDATE_GATES(bjArgs, targetVehicle, launchDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, FALSE, rewardFactor)
				
				IF IS_SCREEN_FADED_OUT()
					IF bRetry AND bRestartedFromDeath
						SET_CLOCK_TIME(GET_TIMEOFDAY_HOUR(currentTime), GET_TIMEOFDAY_MINUTE(currentTime), GET_TIMEOFDAY_SECOND(currentTime))
					ENDIF
					WAIT(2000)
					IF bRetry
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						RESTART_TIMER_NOW(tmrUnfreeze)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
						ENDIF
						WHILE GET_TIMER_IN_SECONDS(tmrUnfreeze) < 1.1
							WAIT(0)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
							ENDIF
						ENDWHILE
						CANCEL_TIMER(tmrUnfreeze)
					ENDIF
					DO_SCREEN_FADE_IN(iFADE_IN_TIME)
				ENDIF
				
				// Set up the first gate.
				IF DOES_ENTITY_EXIST(targetVehicle) AND NOT IS_ENTITY_DEAD(targetVehicle)
					curGateBlip = ADD_BLIP_FOR_COORD(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetVehicle, BJ_GET_ARG_TARGET_VEHICLE_OFFSET(bjArgs)))
					SET_BLIP_COLOUR(curGateBlip, BLIP_COLOUR_YELLOW)
					SHOW_HEIGHT_ON_BLIP(curGateBlip, TRUE)
					SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_TGT")
				ELSE
					curGateBlip = ADD_BLIP_FOR_COORD(BJ_GET_ARG_TARGET_POSITION(bjArgs, 0))
					SET_BLIP_COLOUR(curGateBlip, BLIP_COLOUR_YELLOW)
					IF iGates = 1
						SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_TGT")
					ELSE
						SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_CHK")
					ENDIF
				ENDIF
				
				SET_BLIP_SCALE(curGateBlip, fCUR_BLIP_SCALE)
				
				// Set up the second gate.
				IF iGates > 1
					nextGateBlip = ADD_BLIP_FOR_COORD(BJ_GET_ARG_TARGET_POSITION(bjArgs, 1))
					SET_BLIP_COLOUR(nextGateBlip, BLIP_COLOUR_YELLOW)
					SHOW_HEIGHT_ON_BLIP(curGateBlip, TRUE)
					SET_BLIP_SCALE(nextGateBlip, fNEXT_BLIP_SCALE)
					IF iGates = 2
						SET_BLIP_NAME_FROM_TEXT_FILE(nextGateBlip, "BJ_BLIP_TGT")
					ELSE
						SET_BLIP_NAME_FROM_TEXT_FILE(nextGateBlip, "BJ_BLIP_CHK")
					ENDIF
				ENDIF
				
				// If we have a target vehicle, move it to its start position.
				IF DOES_ENTITY_EXIST(targetVehicle) AND NOT IS_ENTITY_DEAD(targetVehicle)
					SET_ENTITY_LOD_DIST(targetVehicle, 2000)
					IF DOES_ENTITY_EXIST(targetDriverPed) AND NOT IS_PED_INJURED(targetDriverPed)
						//make sure we stop the car/truck/boat from whatever it was tasked for in the cutscene.
						CLEAR_PED_TASKS(targetDriverPed)
					ENDIF
					IF BJ_IS_MODEL_TRAIN(BJ_GET_ARG_TARGET_MODEL(bjArgs))
						DELETE_MISSION_TRAIN(targetVehicle)
					ELSE
						SET_ENTITY_COORDS(targetVehicle, BJ_GET_ARG_TARGET_POSITION(bjArgs, 0))
					ENDIF
				ENDIF
				
				//if current jump has been completed..
				IF IS_BIT_SET(g_savedGlobals.sBasejumpData.iCompletedFlags, ENUM_TO_INT(BJ_GET_ARG_STATIC_BLIP(bjArgs)) - ENUM_TO_INT(STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR))
					//set smoke trail color
					SET_PLAYER_CAN_LEAVE_PARACHUTE_SMOKE_TRAIL(PLAYER_ID(), TRUE)
					//set random color
					SET_PLAYER_PARACHUTE_SMOKE_TRAIL_COLOR(PLAYER_ID(), GET_RANDOM_INT_IN_RANGE(0, 256), GET_RANDOM_INT_IN_RANGE(0, 256), GET_RANDOM_INT_IN_RANGE(0, 256))
				ELSE
					PRINTLN("HAVEN'T FINISHED THIS JUMP YET! jump id is.. ", ENUM_TO_INT(BJ_GET_ARG_STATIC_BLIP(bjArgs)) - ENUM_TO_INT(STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR))
				ENDIF
				
				// If we're in a drivable launch vehicle, give player control.
				IF BJ_IS_MODEL_GROUND_VEH(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
					ODDJOB_EXIT_CUTSCENE(TRUE, TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					RESTART_TIMER_NOW(radioTimer)
					gameState = BJGAMESTATE_FREE_JUMP
					BAWSAQ_INCREMENT_MODIFIER(  BSMF_SM_PARA  )
				ELIF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
					ODDJOB_EXIT_CUTSCENE(FALSE, FALSE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					gameState = BJGAMESTATE_HELI_LOOK
					BAWSAQ_INCREMENT_MODIFIER(  BSMF_SM_PARA  )
				ELSE
					//SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
					//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ODDJOB_EXIT_CUTSCENE(TRUE, TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
					//vCatchUpPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					gameState = BJGAMESTATE_FREE_JUMP
					BAWSAQ_INCREMENT_MODIFIER(  BSMF_SM_PARA  )
				ENDIF
				
				IF eJumpID = BJJUMPID_CRANE
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, TRUE)
				ENDIF
				
				// Tell the pickup chopper to leave.
				IF (NOT bFlyaway) AND DOES_ENTITY_EXIST(extraVehicle)
					BJ_START_DROPOFF_HELI_SEQ(extraVehicle, TRUE)
					bFlyaway = TRUE
				ENDIF
				
				// We need to set up the camera here if we didn't fade out.
				IF NOT BJ_GET_ARG_CUTSCENE_FADE(bjArgs)
					IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
						BJ_SETUP_HELI_LOOK_CAMERA(cameraSet, bjArgs, vLookCamRot, vFocalPoint, vFocalOffset, launchVehicle, vBaseLookCamRot, fBaseLookCamFOV)
//						NEW_LOAD_SCENE_STOP()
//						vCameraPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(launchVehicle, <<1.63971, -1.48999, 2.2019>>)
//						vCameraDir = <<-44.8000, 0.0000, -20.2737>>
//						vCameraDir.z += GET_ENTITY_HEADING(launchVehicle)
//						vCameraDir = NORMALISE_VECTOR(<<COS(vCameraDir.z), SIN(vCameraDir.z), TAN(vCameraDir.x)>>)
//						NEW_LOAD_SCENE_START(vCameraPos, vCameraDir, LOAD_SCENE_FAR_CLIP)
					ELSE
						IF BJ_GET_ARG_HOLD_LAST_CUTSCENE_FRAME(bjArgs) AND NOT bCutsceneSkip AND NOT bRetry
							//hold camera
							STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
						ELIF eJumpID = BJJUMPID_MAZE_BANK // ugly, so sue me
						AND NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							SET_CAM_COORD(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), <<-76.7226, -829.9866, 326.0427>>)
							SET_CAM_ROT(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), <<0.8541, 0.0000, -17.0120>>)
							SET_CAM_FOV(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), 53.8830)
							SET_CAM_ACTIVE(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), TRUE)
							//vCatchUpPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						ELSE
							IF IS_SCRIPT_GLOBAL_SHAKING()
								STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
							ENDIF
							RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT_INTERP_TO_FROM_GAME, TRUE, TRUE)
						ENDIF
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					//setup the player with his own parachute, otherwise give a random one.
					IF bDoesPlayerHaveOwnParachute
						IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
							GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, 1)
						ENDIF
						SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), iParachuteTint)
						
						IF bDoesPlayerHaveReserveParachute
							IF NOT GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
								SET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
							ENDIF
							SET_PED_RESERVE_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), iParachuteReserveTint)
						ENDIF
					ELSE
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, 1)
					SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), GET_RANDOM_INT_IN_RANGE()%8)
				ENDIF
				ENDIF
				
				//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				
				// Re-request the skydiving anims, since they seem to get unloaded by this point.
				REQUEST_ANIM_DICT("skydive@base")
				REQUEST_ANIM_DICT("skydive@freefall")
				REQUEST_ANIM_DICT("skydive@parachute@chute")
				REQUEST_ANIM_DICT("skydive@parachute@")
			BREAK
			
			// Run or drive off a cliff or building.
			CASE BJGAMESTATE_FREE_JUMP
				//check if player died.
				IF IS_PED_INJURED(PLAYER_PED_ID())
					gameState = BJGAMESTATE_LOSER
					BREAK
				ELIF bDebugFail OR bDebugPass OR BJ_CHECK_WANDER_FAIL(bjArgs, eJumpID, bWanderFail)// OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
					CLEAR_HELP()
					CLEAR_PRINTS()
					gameState = BJGAMESTATE_PROCESS_DATA
				ELSE
					IF eJumpID = BJJUMPID_CRANE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-118.4, -973.1, 295.2>>, <<-117.1, -975.7, 297.7>>))
					ENDIF
					
					IF bDebugSkip
						vPlayerTlprt = BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1)
						vPlayerTlprt.z += 100.0
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerTlprt)
						bDebugSkip = FALSE
					ENDIF
					
					IF NOT bCatchUpCam
						IF eJumpID = BJJUMPID_MAZE_BANK
							IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LR)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UD)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
							OR (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							//OR VDIST2(vCatchUpPlayerPos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 0.04 // 0.2^2
								RENDER_SCRIPT_CAMS(FALSE, TRUE, 1300, TRUE, TRUE)
								//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
								bCatchUpCam = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					BJ_UPDATE_RADIO(radioTimer)
					BJ_UPDATE_GATES(bjArgs, targetVehicle, launchDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, TRUE, rewardFactor)
					BJ_SET_PLAYER_UNARMED(playerWeapon)
					
					IF IS_TIMER_STARTED(flowTimer)
						IF GET_TIMER_IN_SECONDS(flowTimer) > 10.0
							BJ_UPDATE_PLAYER_DIALOGUE(dialogueTimer, iNumPlayerLines)
						ENDIF
					ELSE
						START_TIMER_AT(flowTimer, 0)
					ENDIF
					
					IF DOES_ENTITY_EXIST(extraVehicle) AND NOT IS_ENTITY_DEAD(extraVehicle)
						IF NOT IS_ENTITY_ON_SCREEN(extraVehicle)
						AND VDIST2(GET_ENTITY_COORDS(extraVehicle), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 40000.0 // 200^2
							PED_INDEX tempPed
							tempPed = GET_PED_IN_VEHICLE_SEAT(extraVehicle)
							IF NOT IS_PED_INJURED(tempPed)
								DELETE_PED(tempPed)
							ENDIF
							DELETE_VEHICLE(extraVehicle)
						ENDIF
					ENDIF
					
					// Print an objective to drive or jump off the cliff.
					IF NOT IS_BIT_SET(iMessagingFlags, 0)
						IF IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_TIMER_STARTED(tmrObjective)
							RESTART_TIMER_NOW(tmrObjective)
						ELIF GET_TIMER_IN_SECONDS(tmrObjective) > 0.25
							sMessage = BJ_GET_FREE_JUMP_OBJECTIVE(eJumpID)
							PRINT_NOW(sMessage, DEFAULT_GOD_TEXT_TIME, 0)
							CANCEL_TIMER(tmrObjective)
							SET_BIT(iMessagingFlags, 0)
						ENDIF
					ENDIF
					
					// Print a help text to ditch the vehicle.
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						BJ_ADJUST_AUDIO_SCENE_FOR_ON_FOOT_JUMPS(bjUI, TRUE)
						vehDriving = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
						IF IS_VEHICLE_DRIVEABLE(vehDriving) AND IS_ENTITY_IN_AIR(vehDriving)
							IF NOT IS_BIT_SET(iMessagingFlags, 1)
								IF NOT IS_TIMER_STARTED(tmrDitch)
									START_TIMER_NOW(tmrDitch)
								ELIF GET_TIMER_IN_SECONDS(tmrDitch) > 0.15
									PRINT_HELP("BJ_VEHHELP")
									CANCEL_TIMER(tmrDitch)
									SET_BIT(iMessagingFlags, 1)
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(iMessagingFlags, 1)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BJ_VEHHELP")
								CLEAR_HELP()
							ENDIF
							IF IS_TIMER_STARTED(tmrDitch)
								CANCEL_TIMER(tmrDitch)
							ENDIF
						ENDIF
					ELSE
						BJ_ADJUST_AUDIO_SCENE_FOR_ON_FOOT_JUMPS(bjUI, FALSE)
						CLEAR_BIT(iMessagingFlags, 1)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BJ_VEHHELP")
							CLEAR_HELP()
						ENDIF
						IF IS_TIMER_STARTED(tmrDitch)
							CANCEL_TIMER(tmrDitch)
						ENDIF
					ENDIF
					
					// If the player is skydiving, move on to that state.
					IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) <> PPS_INVALID
						IF eJumpID = BJJUMPID_CRANE
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
						ENDIF
						CASCADE_SHADOWS_SET_AIRCRAFT_MODE(TRUE)
						SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
						gameState = BJGAMESTATE_SKYDIVING
						CANCEL_TIMER(flowTimer)
					ENDIF
				ENDIF
			BREAK
			
			// Can move camera and press a button to jump.
			CASE BJGAMESTATE_HELI_LOOK
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				// Check for changes in wind state from rag.
				#IF IS_DEBUG_BUILD
					IF bUseElevationWind <> bjw.bUseElevationWind
						bUseElevationWind = bjw.bUseElevationWind
						SCRIPT_OVERRIDES_WIND_ELEVATION(bUseElevationWind, HASH("WEATHER_TYPES_HIGH_ELEVATION_BASE_JUMP_HELI"))
					ENDIF
					
					IF bUseCustomWind <> bjw.bUseCustomWind
						bUseCustomWind = bjw.bUseCustomWind
						IF bUseCustomWind
							PLAY_SOUND_FROM_ENTITY(iWindSound, "Helicopter_Wind_Idle", launchVehicle, "BASEJUMPS_SOUNDS")
						ELSE
							STOP_SOUND(iWindSound)
						ENDIF
					ENDIF
				#ENDIF
				
				// Handle debug pass/fail.
				IF bDebugPass OR bDebugFail
					IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_IN(300)
						WAIT(300)
					ENDIF
					
					CLEAR_HELP()
					CLEAR_PRINTS()
					
					IF IS_SCRIPT_GLOBAL_SHAKING()
						STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
					ENDIF
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					gameState = BJGAMESTATE_PROCESS_DATA
				
				ELSE
					BJ_UPDATE_RADIO(radioTimer)
					BJ_UPDATE_GATES(bjArgs, targetVehicle, launchDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, TRUE, rewardFactor)
					BJ_SET_PLAYER_UNARMED(playerWeapon)
					
					IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID)) AND NOT IS_ENTITY_DEAD(launchVehicle)
						BJ_UPDATE_HELI_MOTION(launchVehicle, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
					ENDIF
					
					IF IS_TIMER_STARTED(flowTimer)
						IF GET_TIMER_IN_SECONDS(flowTimer) > 10.0
							BJ_UPDATE_PLAYER_DIALOGUE(dialogueTimer, iNumPlayerLines)
						ENDIF
					ELSE
						START_TIMER_AT(flowTimer, 0)
					ENDIF
					
					// Wait for the player to press the launch button.
					IF BJ_UPDATE_HELI_LOOK_CAMERA(bjArgs, cameraSet, input, inputTimer, vLookCamRot, vCameraVelocity, vFocalPoint, vFocalOffset) OR bDebugSkip
						CANCEL_TIMER(flowTimer)
						CLEAR_HELP()
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							// If we are launching out of a chopper, leave the vehicle now and use a fixed camera.
							IF DOES_ENTITY_EXIST(launchVehicle) AND NOT IS_ENTITY_DEAD(launchVehicle)
								SET_ENTITY_COORDS(launchVehicle, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
								SET_ENTITY_HEADING(launchVehicle, BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
								FREEZE_ENTITY_POSITION(launchVehicle, TRUE)
								iSceneId = CREATE_SYNCHRONIZED_SCENE (<<0,0,0>>, <<0,0,0>>) 
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, launchVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(launchVehicle, "Chassis"))
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@basejump@", "Heli_jump", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT)
								SET_SYNCHRONIZED_SCENE_PHASE(iSceneID, 0.6)
								
								BJ_SETUP_HELI_JUMP_CAMERA(launchVehicle, cameraSet, camTimer)
								NEW_LOAD_SCENE_STOP()
							ENDIF
						ENDIF
						
						IF bUseElevationWind
							SCRIPT_OVERRIDES_WIND_ELEVATION(FALSE, HASH("WEATHER_TYPES_HIGH_ELEVATION_BASE_JUMP_HELI"))
						ENDIF
						
						PLAY_SOUND_FROM_ENTITY(iWindSound, "Helicopter_Wind", launchVehicle, "BASEJUMPS_SOUNDS")
						
						bDebugSkip = FALSE
						gameState = BJGAMESTATE_HELI_JUMP
					ENDIF
				ENDIF
			BREAK
			
			// Player is jumping from his spot. No player control.
			CASE BJGAMESTATE_HELI_JUMP
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				BJ_UPDATE_RADIO(radioTimer)
				BJ_UPDATE_GATES(bjArgs, targetVehicle, launchDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, TRUE, rewardFactor)
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iSceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) > 0.92
						OPEN_SEQUENCE_TASK(jumpSequence)				
							TASK_FORCE_MOTION_STATE(NULL, ENUM_TO_INT(MS_PARACHUTING))
							TASK_PARACHUTE(NULL, TRUE)
						CLOSE_SEQUENCE_TASK(jumpSequence)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), jumpSequence)
						CLEAR_SEQUENCE_TASK(jumpSequence)
					ENDIF	
				ENDIF
				
				// If launching from a helicopter, wait a specified amount of time.
				IF DOES_ENTITY_EXIST(launchVehicle)
					IF BJ_UPDATE_HELI_JUMP_CAMERA(cameraSet, camTimer, vFocalPoint, vFocalOffset, vLookCamRot, vBaseLookCamRot, fBaseLookCamFOV)
						SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), launchVehicle, FALSE)
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "oddjobs@basejump@", "Heli_jump")
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DISPLAY_RADAR(TRUE)
							IF IS_SCRIPT_GLOBAL_SHAKING()
								STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
							ENDIF
							SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
							CASCADE_SHADOWS_SET_AIRCRAFT_MODE(TRUE)
							gameState = BJGAMESTATE_SKYDIVING
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Active gameplay, either in skydive or parachute state.
			CASE BJGAMESTATE_SKYDIVING
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				//bug:2437600
				#IF USE_REPLAY_RECORDING_TRIGGERS
					IF g_sSelectorUI.bFeedAddedForRecording
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
					ENDIF
				#ENDIF
				
				
				//bug:2437600
				#IF USE_REPLAY_RECORDING_TRIGGERS
					IF g_sSelectorUI.bFeedAddedForRecording
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
					ENDIF
				#ENDIF
				
				
				//check if player died.
				IF IS_PED_INJURED(PLAYER_PED_ID())
					gameState = BJGAMESTATE_LOSER
					BREAK
				ENDIF
				
				IF bDebugPass OR bDebugFail
					CLEAR_HELP()
					CLEAR_PRINTS()
					gameState = BJGAMESTATE_PROCESS_DATA
					
				ELIF BJ_IS_BUTTON_JUST_PRESSED_WITH_DELAY(input, BJBUTTON_RETRY)
				#IF USE_REPLAY_RECORDING_TRIGGERS
				and !g_sSelectorUI.bFeedAddedForRecording
				#ENDIF
				
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
					WHILE IS_SCREEN_FADING_OUT()
						BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, TRUE, rewardFactor)
						WAIT(0)
					ENDWHILE
					BJ_RESET_ON_RETRY(bjArgs, bjUI_Leaderboard, bjUI, iMessagingFlags, targetDriverPed, onlookerArray, vehPlayer, targetVehicle, targetFlatbed, extraVehicle, vehNearby, curCheckpoint, nextCheckpoint, curGateBlip, nextGateBlip, iCurrentGate, iCurCheckpoint, iGatesHit, iNumPlayerLines, iAccuracy, eJumpID, gateCheckResult, savedLandingType, tEndDelayTimer, tmrLandingAlpha, vCameraVelocity, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, bFlyaway, bDebugPass, bDebugFail, bDebugSkip, bLBToggle, bPlayerDiedLanding, bjCombatAvoidanceArea)
					bRetry = TRUE
					gameState = BJGAMESTATE_INIT
				ELSE
					BJ_UPDATE_ONLOOKERS(tmrOnlookers, onlookerArray)
					
					IF bDebugSkip
						vPlayerTlprt = BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1)
						SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vPlayerTlprt)
						bDebugSkip = FALSE
					ENDIF
					
					BJ_UPDATE_RADIO(radioTimer)
					
					// If we have a target vehicle, start it on its path
					IF BJ_IS_MODEL_TRAIN(BJ_GET_ARG_TARGET_MODEL(bjArgs))
						IF NOT DOES_ENTITY_EXIST(targetVehicle) AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), BJ_GET_ARG_TARGET_POSITION(bjArgs, 0), FALSE) <= fTRAIN_TGT_START_MOVING
							targetVehicle = CREATE_MISSION_TRAIN(6, BJ_GET_ARG_TARGET_POSITION(bjArgs, 0), TRUE)
							IF NOT IS_ENTITY_DEAD(targetVehicle)
								SET_TRAIN_CRUISE_SPEED(targetVehicle, 7.5)
							ENDIF
						ENDIF
					ELIF DOES_ENTITY_EXIST(targetVehicle) AND NOT IS_ENTITY_DEAD(targetVehicle) AND NOT IS_VECTOR_ZERO(BJ_GET_ARG_TARGET_POSITION(bjArgs, 0))
						IS_PED_INJURED(targetDriverPed)
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), targetVehicle, FALSE) <= fGROUND_TGT_START_MOVING
							VECTOR targetCoords
							IF eJumpID = BJJUMPID_CRANE
								targetCoords = <<28.8687, -299.1065, 46.5693>>
							ELSE
								targetCoords = <<-819.7591, -1512.2285, 0.1336>>
							ENDIF
							
							IF GET_SCRIPT_TASK_STATUS(targetDriverPed, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
								IF eJumpID = BJJUMPID_CRANE
									TASK_VEHICLE_MISSION_COORS_TARGET(targetDriverPed, targetVehicle, targetCoords, MISSION_GOTO, 5.0, DF_ForceStraightLine | DF_PreventBackgroundPathfinding, 5.0, 10.0)
								ELSE
									TASK_VEHICLE_MISSION_COORS_TARGET(targetDriverPed, targetVehicle, targetCoords, MISSION_GOTO, 5.0, DF_ForceStraightLine, 4.0, -1  )
								ENDIF
							ELSE
								//updating task every frame doesnt work for the boat.
								BJ_UPDATE_MOVING_TARGET_SPEED(targetVehicle)
							ENDIF						
						ENDIF
					ENDIF
					
					gateCheckResult = BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, TRUE, TRUE, rewardFactor)
					
					IF gateCheckResult = BJGATECHECK_PASS OR gateCheckResult >= BJGATECHECK_FAIL 
						PRINTLN("Checking gatecheck pass or fail...")
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						IF gateCheckResult >= BJGATECHECK_FAIL 
							PRINTLN("Failed! Or failed for being too far!")
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							IF IS_MESSAGE_BEING_DISPLAYED()
								CLEAR_PRINTS()
							ENDIF
						ENDIF
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
						
						IF eJumpID = BJJUMPID_CRANE
							CLEAR_PED_TASKS(targetDriverPed)
							SET_PED_CONFIG_FLAG(targetDriverPed, PCF_NeverReactToPedOnRoof, TRUE)
						ENDIF
						
						INT iPedCounter
						REPEAT COUNT_OF(onlookerArray) iPedCounter
							IF DOES_ENTITY_EXIST(onlookerArray[iPedCounter]) AND NOT IS_ENTITY_DEAD(onlookerArray[iPedCounter])
								CLEAR_PED_TASKS(onlookerArray[iPedCounter])
								onlookerArray[iPedCounter] = NULL
							ENDIF
						ENDREPEAT
						
						gameState = BJGAMESTATE_PROCESS_DATA
					ENDIF 
					
					BJ_UPDATE_INGAME_UI(bjUI, bjArgs, eJumpID, iGates)
				ENDIF
			BREAK
			
			CASE BJGAMESTATE_PROCESS_DATA
				IF eJumpID = BJJUMPID_HARBOR
					SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				ENDIF
				
				IF DOES_ENTITY_EXIST(targetDriverPed) AND NOT IS_ENTITY_DEAD(targetDriverPed)
					IF GET_SCRIPT_TASK_STATUS(targetDriverPed, SCRIPT_TASK_STAND_STILL) <> WAITING_TO_START_TASK
					AND GET_SCRIPT_TASK_STATUS(targetDriverPed, SCRIPT_TASK_STAND_STILL) <> PERFORMING_TASK
						CLEAR_PED_TASKS(targetDriverPed)
						TASK_STAND_STILL(targetDriverPed, -1)
					ENDIF
				ENDIF
				
				IF (NOT bDebugPass) AND (NOT bDebugFail) AND (NOT bWanderFail)
					//wait for player to stop falling or ragdolling or rolling (with timeout)
					IF NOT IS_TIMER_STARTED(tEndDelayTimer)
						IF NOT IS_TIMER_STARTED(inputTimer)
							START_TIMER_NOW(inputTimer)
						ENDIF
						// Make sure all blips are gone.
						IF DOES_BLIP_EXIST(curGateBlip)
							REMOVE_BLIP(curGateBlip)
						ENDIF
						IF DOES_BLIP_EXIST(nextGateBlip)
							REMOVE_BLIP(nextGateBlip)
						ENDIF
						IF iCurCheckpoint > -1
							DELETE_CHECKPOINT(curCheckpoint)
							DELETE_CHECKPOINT(nextCheckpoint) // No way to see if this exists
							iCurCheckpoint = -1
						ENDIF
						START_TIMER_AT(tEndDelayTimer, 0)
					ENDIF
					
					IF NOT IS_TIMER_STARTED(tmrLandingAlpha)
						START_TIMER_NOW(tmrLandingAlpha)
					ENDIF
					
					IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
					
						// [aarong] - I changed this BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs iGates - 1), tmrLandingAlpha) for below as it did not build
						BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
					ENDIF
				
					//if we have a target vehicle, check if we're "crash landing" near it
					//this is for the case the player lets go of the parachute early to land
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(targetVehicle) AND (NOT IS_ENTITY_DEAD(targetVehicle)) AND (NOT IS_ENTITY_DEAD(PLAYER_PED_ID()))
						AND (IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), targetVehicle, BJ_TARGET_VEHICLE_LOCATE))
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurrentGate), BJ_TARGET_VEHICLE_LOCATE)
						OR IS_ENTITY_IN_AIR(PLAYER_PED_ID())
							BOOL bBreak
							bBreak = FALSE
							IF IS_ENTITY_IN_AIR(PLAYER_PED_ID()) 
							AND (DOES_ENTITY_EXIST(targetVehicle) AND NOT IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), targetVehicle))
								PRINTLN("Ped is still in the air and we're close to the target (but not touching)!")
								bBreak = TRUE
							ELIF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_RAGDOLL(PLAYER_PED_ID()) 
								PRINTLN("Ped is ragdolling and we're close to the target!")
		//						PRINTLN("Player is ragdolling and we're close to the target!")
								bBreak = TRUE
	//						ELIF DOES_ENTITY_EXIST(targetVehicle) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), targetVehicle)
	//							PRINTLN("Ped is touching the target! Waiting to see if he's able to get on top before hitting the ground/water")
	//							BREAK
							ENDIF
							
							IF bBreak
								IF BJ_IS_BUTTON_JUST_PRESSED_WITH_DELAY(input, BJBUTTON_RETRY)
								#IF USE_REPLAY_RECORDING_TRIGGERS
								AND !g_sSelectorUI.bFeedAddedForRecording
								#ENDIF
									SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
									DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
									WHILE IS_SCREEN_FADING_OUT()
										BJ_UPDATE_GATES(bjArgs, targetVehicle, targetDriverPed, onlookerArray, curCheckpoint, nextCheckpoint, iCurrentGate, iCurCheckpoint, iGatesHit, iGates, iAccuracy, curGateBlip, nextGateBlip, iSoundID_01, savedLandingType, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, FALSE, TRUE, rewardFactor)
										WAIT(0)
									ENDWHILE
									BJ_RESET_ON_RETRY(bjArgs, bjUI_Leaderboard, bjUI, iMessagingFlags, targetDriverPed, onlookerArray, vehPlayer, targetVehicle, targetFlatbed, extraVehicle, vehNearby, curCheckpoint, nextCheckpoint, curGateBlip, nextGateBlip, iCurrentGate, iCurCheckpoint, iGatesHit, iNumPlayerLines, iAccuracy, eJumpID, gateCheckResult, savedLandingType, tEndDelayTimer, tmrLandingAlpha, vCameraVelocity, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, bFlyaway, bDebugPass, bDebugFail, bDebugSkip, bLBToggle, bPlayerDiedLanding, bjCombatAvoidanceArea)
									bRetry = TRUE
									gameState = BJGAMESTATE_INIT
								ENDIF
								BREAK
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//check if player died.
				IF IS_PED_INJURED(PLAYER_PED_ID())
					gameState = BJGAMESTATE_LOSER
					BREAK
				ENDIF
				
				//short wait in case player recovers landing
				IF (bDebugPass OR bDebugFail OR bWanderFail)
				OR (IS_TIMER_STARTED(tEndDelayTimer) AND GET_TIMER_IN_SECONDS(tEndDelayTimer) > 0.25)
					IF IS_TIMER_STARTED(tEndDelayTimer) AND GET_TIMER_IN_SECONDS(tEndDelayTimer) > 0.25
////////////////////THIS WILL OVERWRITE OUR RESULTS FROM BJ_UPDATE_GATES!!!!!!
						BJ_PERFORM_FINAL_LANDING_CHECK(BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurrentGate), targetVehicle, targetDriverPed, gateCheckResult, iAccuracy, fLandingDistance)
////////////////////THIS WILL OVERWRITE OUR RESULTS FROM BJ_UPDATE_GATES!!!!!!
					ENDIF
					
					CANCEL_TIMER(tEndDelayTimer)
					IF bDebugPass OR gateCheckResult = BJGATECHECK_PASS
						bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
						WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(bigMessageUI.siMovie)
							WAIT(0)
							IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
								BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
							ENDIF
							bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
						ENDWHILE
																
						IF eJumpID = BJJUMPID_HARBOR
							IF NOT IS_PED_INJURED(targetDriverPed)
								CREATE_CONVERSATION(conversationPedStruct, "OJBJAUD", "BJ_01D", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
							ENDIF
						ENDIF
						
						gameState = BJGAMESTATE_WINNER
						
						IF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_PROCESS_SCORE)
							PRINTLN("Passed!")
							bitDif = ENUM_TO_INT(STATIC_BLIP_AMBIENT_BASEJUMP_HARBOR)
							SET_BIT(g_savedGlobals.sBasejumpData.iCompletedFlags, ENUM_TO_INT(BJ_GET_ARG_STATIC_BLIP(bjArgs)) - bitDif)
							SET_STATIC_BLIP_HAS_CHECKMARK(BJ_GET_ARG_STATIC_BLIP(bjArgs), TRUE)
							
							// Check for unlockables - Extreme 4 RCM, infinite parachute pack, completion percentage
							bAllClear = TRUE
							bBASEJumpClear = TRUE
							bSkydiveClear = TRUE
							REPEAT BJJUMPIDS iJumpIndex
								IF NOT IS_BIT_SET(g_savedGlobals.sBasejumpData.iCompletedFlags, iJumpIndex)
									// heli = skydive, no heli = base jump
									IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(INT_TO_ENUM(BJ_JUMP_ID, iJumpIndex)))
										bSkydiveClear = FALSE
									ELSE
										bBaseJumpClear = FALSE
									ENDIF
									bAllClear = FALSE
								ENDIF
							ENDREPEAT
							
							// Allow Extreme 4 and infinite parachute pack?
							IF bAllClear AND (NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EXTREME4_BJUMPS_FINISHED))
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EXTREME4_BJUMPS_FINISHED, TRUE)
							ENDIF
							
							// Allow BASE jump and skydive completion percentages?
							IF bBASEJumpClear
								REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_BASEJ)
							ENDIF
							IF bSkydiveClear
								REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_MG_SKYD)
							ENDIF
							
							BJ_CALCULATE_REWARD(iGatesHit, iGates, iAccuracy, iReward, rewardFactor)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
							ENDIF
							BJ_SET_UI_FLAG(bjUI, BJUIFLAG_PROCESS_SCORE, TRUE)
							SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE)
							START_TIMER_NOW(tmrSave)
							MAKE_AUTOSAVE_REQUEST()
						ENDIF
							
					ELIF bDebugFail OR bWanderFail OR gateCheckResult >= BJGATECHECK_FAIL  
						CLEAR_HELP()
						CLEAR_PRINTS()
						IF NOT bWanderFail
							BJ_PLAY_FAIL_DIALOGUE()
						ENDIF
						IF IS_PED_INJURED(PLAYER_PED_ID())
							bPlayerDiedLanding = TRUE
						ENDIF

						IF IS_PLAYER_ONLINE()
							SETUP_MINIGAME_INSTRUCTION_INPUTS(bjUI.uiControls, FALSE, INPUT_FRONTEND_ACCEPT, "BJ_CONTINUE", INPUT_FRONTEND_X, "BJ_RETRY")
						ELSE
							BJ_SET_UI_FLAG(bjUI, BJUIFLAG_SHOWING_OFFLINE_LB_BUTTON, TRUE)
							SETUP_MINIGAME_INSTRUCTION_INPUTS(bjUI.uiControls, FALSE, INPUT_FRONTEND_ACCEPT, "BJ_CONTINUE", INPUT_FRONTEND_X, "BJ_RETRY", INPUT_FRONTEND_LEADERBOARD, "HUD_INPUT68")
						ENDIF
						gameState = BJGAMESTATE_LOSER
						
					ELSE
						PRINTLN("gateCheckResult is BJGATECHECK_NONE")
					ENDIF
					
					CANCEL_TIMER(inputTimer)
				ENDIF
				BREAK
				
			// We have a winner and are displaying the win UI.
			CASE BJGAMESTATE_WINNER	
			
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)

//				BJ_UPDATE_SPLASH_SCREEN(bigMessageUI)		
				IF NOT bDebugPass AND NOT bDebugFail
					IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
						BJ_LOAD_SOCIAL_CLUB_LEADERBOARD(eJumpID)
						IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
							SETUP_MINIGAME_INSTRUCTION_INPUTS(bjUI.uiControls, FALSE, INPUT_FRONTEND_ENDSCREEN_ACCEPT, "BJ_CONTINUE", INPUT_FRONTEND_ENDSCREEN_EXPAND, "BJ_RETRY", INPUT_FRONTEND_LEADERBOARD, "HUD_INPUT68")	
							BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					bPlayerFinishedInVehicle = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
				
				IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
					BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
				ENDIF
				
				// Update the win screen, only read the keys while player hasn't chosen to exit basejumping
				IF NOT bWinScreenKeyPressed
					scorecardReturn = BJ_UPDATE_SCORECARD_UI(eJumpID, bjUI, bjUI_Leaderboard, input, iGatesHit, iGates, iAccuracy, iReward, rewardFactor, bLBToggle)
					bWinScreenKeyPressed = (scoreCardReturn = BJSCORECARD_EXIT)
				ENDIF
						
				// If the screen is fading, wait for it to finish.
				IF IS_PED_INJURED(PLAYER_PED_ID())
					IF iJumpID = ENUM_TO_INT(BJJUMPID_HARBOR)
						SET_ROADS_IN_ANGLED_AREA(<<-832.271, -1525.112, -100>>, <<-1187.833, -1876.646, 100>>, 50, FALSE, TRUE)
					ELIF iJumpID = ENUM_TO_INT(BJJUMPID_CRANE)
						SET_ROADS_IN_ANGLED_AREA(<<-129.031, -726.381, 35>>, <<-38.185, -465.801, 100>>, 75, FALSE, TRUE)
					ENDIF
					BJ_SCRIPT_PASS(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, conversationPedStruct, launchDriverPed, targetDriverPed, onlookerArray, iWindSound, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
				
				// If the screen is not fading, update player input & state.
				ELIF (NOT IS_SCREEN_FADING_OUT()) AND ((NOT IS_AUTOSAVE_REQUEST_IN_PROGRESS()) OR GET_TIMER_IN_SECONDS(tmrSave) >= 2.0)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UD)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_UP_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_DOWN_ONLY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
					
					
					IF IS_TIMER_STARTED(inputTimer)
						// Player wants to quit (or time is up).
						IF scorecardReturn = BJSCORECARD_EXIT OR GET_TIMER_IN_SECONDS(inputTimer) >= fPOST_WIN_TIMEOUT OR 
						((NOT IS_PED_INJURED(PLAYER_PED_ID())) AND IS_PED_JACKING(PLAYER_PED_ID()) OR (!bPlayerFinishedInVehicle AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))) //check if player is getting into or jacking a veh
							//Set the end screen animating out when the timer's expired
							IF (NOT bWinScreenKeyPressed)
								ENDSCREEN_START_TRANSITION_OUT(bjUi.bjEndScreen)
								bWinScreenKeyPressed = TRUE
							ENDIF
							//Only finish when the end screen stops rendering
							IF RENDER_ENDSCREEN(bjUI.bjEndScreen)
								//last chance write if we havent yet.
								IF NOT bDebugPass AND NOT bDebugFail
									IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
										IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
											BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
										ENDIF
									ENDIF
								ENDIF
								
								ENDSCREEN_SHUTDOWN(bjUI.bjEndScreen)
								CANCEL_TIMER(tmrSave)
								IF eJumpID = BJJUMPID_HARBOR
									SET_ROADS_IN_ANGLED_AREA(<<-832.271, -1525.112, -100>>, <<-1187.833, -1876.646, 100>>, 50, FALSE, TRUE)
								ELIF eJumpID = BJJUMPID_CRANE
									SET_ROADS_IN_ANGLED_AREA(<<-129.031, -726.381, 35>>, <<-38.185, -465.801, 100>>, 75, FALSE, TRUE)
								ENDIF
								BJ_SCRIPT_PASS(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, conversationPedStruct, launchDriverPed, targetDriverPed, onlookerArray, iWindSound, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
							ENDIF
							
						// Player wants to retry.
						ELIF  scorecardReturn = BJSCORECARD_RETRY
							IF NOT bDebugPass AND NOT bDebugFail
								IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
									IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
										BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							ENDSCREEN_SHUTDOWN(bjUI.bjEndScreen)
							CANCEL_TIMER(tmrSave)
							BJ_SET_UI_FLAG(bjUI, BJUIFLAG_POPULATE_SCORECARD, FALSE)							
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
							DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
							WHILE IS_SCREEN_FADING_OUT()
								WAIT(0)
								IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
									BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
								ENDIF
							ENDWHILE
							
							BJ_RESET_ON_RETRY(bjArgs, bjUI_Leaderboard, bjUI, iMessagingFlags, targetDriverPed, onlookerArray, vehPlayer, targetVehicle, targetFlatbed, extraVehicle, vehNearby, curCheckpoint, nextCheckpoint, curGateBlip, nextGateBlip, iCurrentGate, iCurCheckpoint, iGatesHit, iNumPlayerLines, iAccuracy, eJumpID, gateCheckResult, savedLandingType, tEndDelayTimer, tmrLandingAlpha, vCameraVelocity, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, bFlyaway, bDebugPass, bDebugFail, bDebugSkip, bLBToggle, bPlayerDiedLanding, bjCombatAvoidanceArea)
							bRetry = TRUE
							gameState = BJGAMESTATE_INIT
							BREAK //make sure we dont reset the UI flags and play the EOM SFX again.
						
						ELIF bLBToggle
							IF GET_TIMER_IN_SECONDS(inputTimer) > fPOST_WIN_TIMEOUT - 5.0
								RESTART_TIMER_AT(inputTimer, fPOST_WIN_TIMEOUT - 5.0)
							ENDIF
						ENDIF
					ELSE
						START_TIMER_AT(inputTimer, 0)
					ENDIF
				ENDIF
			BREAK
			
			// We have a loser and are displaying the retry/quit controls.
			CASE BJGAMESTATE_LOSER
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					bPlayerFinishedInVehicle = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
				
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	// B*2239651 - Disabling pause_alternate during end screens (to avoid clash when exiting leaderboards)
				
				// we failed so no reward.
				IF iReward > 0
					iReward = 0
				ENDIF	
				
				IF NOT bDebugPass AND NOT bDebugFail
					IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
						BJ_LOAD_SOCIAL_CLUB_LEADERBOARD(eJumpID)
						IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
							SETUP_MINIGAME_INSTRUCTION_INPUTS(bjUI.uiControls, FALSE, INPUT_FRONTEND_ACCEPT, "BJ_CONTINUE", INPUT_FRONTEND_X, "BJ_RETRY", INPUT_FRONTEND_LEADERBOARD, "HUD_INPUT68")	
							BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_STARTED_FAIL_EFFECTS)
					
					IF IS_PED_INJURED(PLAYER_PED_ID())
						bRestartedFromDeath = TRUE
					ELSE
						bRestartedFromDeath = FALSE
					ENDIF
					
					CANCEL_MUSIC_EVENT("OJBJ_START")
					CANCEL_MUSIC_EVENT("OJBJ_JUMPED")
					CANCEL_MUSIC_EVENT("OJBJ_LANDED")
					TRIGGER_MUSIC_EVENT("OJBJ_STOP")
					
					// Make sure all blips are gone.
					IF DOES_BLIP_EXIST(curGateBlip)
						REMOVE_BLIP(curGateBlip)
					ENDIF
					IF DOES_BLIP_EXIST(nextGateBlip)
						REMOVE_BLIP(nextGateBlip)
					ENDIF
					IF eJumpID = BJJUMPID_HARBOR
						SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
					ENDIF	
					
					bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
					WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(bigMessageUI.siMovie)
						IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
							BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
						ENDIF
						WAIT(0)
						bigMessageUI.siMovie = REQUEST_MG_BIG_MESSAGE()
					ENDWHILE				
					IF bRestartedFromDeath
						MG_INIT_FAIL_FADE_EFFECT(bjDeathEffect)
					ELSE
						MG_INIT_FAIL_SPLASH_SCREEN(bjSplashStruct)
					ENDIF
					BJ_SET_UI_FLAG(bjUI, BJUIFLAG_STARTED_FAIL_EFFECTS, TRUE)
				ENDIF
					
				IF NOT bRestartedFromDeath AND IS_PED_INJURED(PLAYER_PED_ID())
					BJ_SET_UI_FLAG(bjUI, BJUIFLAG_STARTED_FAIL_EFFECTS, FALSE)
					BREAK
				ENDIF		
				
				IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
					BJ_DRAW_FINAL_GATE_POST_LANDING(BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), tmrLandingAlpha)
				ENDIF
					
				IF IS_PED_INJURED(PLAYER_PED_ID())
					SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
						CASE CHAR_MICHAEL
							bjFailStrapline = "BJ_FAIL_M"
							BREAK
						CASE CHAR_FRANKLIN
							bjFailStrapline = "BJ_FAIL_F"
							BREAK
						CASE CHAR_TREVOR
							bjFailStrapline = "BJ_FAIL_T"
							BREAK
					ENDSWITCH
				ELIF bWanderFail
					bjFailStrapline = "BJ_FAIL_02" //abandonded
				ELSE
					bjFailStrapline = "BJ_FAIL_01" //landed too far
				ENDIF
				
				bFinishedFailScreen = FALSE
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
				
				IF bRestartedFromDeath
					bFinishedFailScreen = MG_UPDATE_FAIL_FADE_EFFECT(bjDeathEffect, bjSplashStruct, bigMessageUI, bjFailString, bjFailStrapline, bRetry)				
				ELSE
					bFinishedFailScreen = MG_UPDATE_FAIL_SPLASH_SCREEN(bigMessageUI, bjSplashStruct, bjFailString, bjFailStrapline, bRetry, FAIL_SPLASH_ENABLE_PLAYER_CONTROL | FAIL_SPLASH_FADE_ON_RETRY | FAIL_SPLASH_ALLOW_ABANDON | FAIL_SPLASH_ENABLE_SOUNDS | FAIL_SPLASH_ENABLE_FX)//| FAIL_SPLASH_ANIMATE_SUB )
				ENDIF
				
				IF bFinishedFailScreen
					IF bRetry
						//last chance write if we havent yet.
						IF NOT bDebugPass AND NOT bDebugFail
							IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
								IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
									BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						PRINTLN("reset on retry!")
						BJ_RESET_ON_RETRY(bjArgs, bjUI_Leaderboard, bjUI, iMessagingFlags, targetDriverPed, onlookerArray, vehPlayer, targetVehicle, targetFlatbed, extraVehicle, vehNearby, curCheckpoint, nextCheckpoint, curGateBlip, nextGateBlip, iCurrentGate, iCurCheckpoint, iGatesHit, iNumPlayerLines, iAccuracy, eJumpID, gateCheckResult, savedLandingType, tEndDelayTimer, tmrLandingAlpha, vCameraVelocity, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation, bHasLanded, bFlyaway, bDebugPass, bDebugFail, bDebugSkip, bLBToggle, bPlayerDiedLanding, bjCombatAvoidanceArea)
						gameState = BJGAMESTATE_INIT
					ELSE
						//last chance write if we havent yet.
						IF NOT bDebugPass AND NOT bDebugFail
							IF CAN_WRITE_TO_LEADERBOARD() AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB)
								IF BJ_DO_RANK_PREDICTION(eJumpID, iAccuracy, fLandingDistance, iReward)
									BJ_SET_UI_FLAG(bjUI, BJUIFLAG_FINISHED_WRITING_TO_LB, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						BJ_SCRIPT_FAIL(bRestartedFromDeath, bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, conversationPedStruct, launchDriverPed, targetDriverPed, onlookerArray, iWindSound, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
					ENDIF
					CLEANUP_MG_BIG_MESSAGE(bigMessageUI)
				ENDIF
				
				gateCheckResult = BJGATECHECK_NONE

			BREAK
		ENDSWITCH
	ENDWHILE
ENDSCRIPT
