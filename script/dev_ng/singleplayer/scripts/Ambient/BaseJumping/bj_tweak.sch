
//////////////////////////////////////////////////////////////////////
/* bj_tweak.sch														*/
/* Author: DJ Jones													*/
/* Tweak values for base jump activity.								*/
//////////////////////////////////////////////////////////////////////


// Cutscene values
TWEAK_FLOAT fCUTSCENE_END_WAIT       	0.2   	// Delay before ending cutscene in s
TWEAK_INT   iCUTSCENE_INTERP_DUR 		10000   // Duration of cutscene cam interpolation in ms

// Look values
TWEAK_FLOAT fLOOK_SCALAR_HORIZ      	5.0   	// Speed of horizontal look cam rotation
TWEAK_FLOAT fLOOK_SCALAR_VERT       	5.0   	// Speed of vertical look cam rotation
TWEAK_FLOAT fLOOK_MAX_HORIZ         	52.6   	// Max horizontal rotation for look cam
TWEAK_FLOAT fLOOK_MAX_VERT          	10.0   	// Max rotation for cam looking up
TWEAK_FLOAT fLOOK_MIN_VERT         		-45.0   // Min rotation for cam looking down
TWEAK_FLOAT fLOOK_INIT_PITCH       		-30.0   // Initial tilt down of look cam
TWEAK_FLOAT fLOOK_FOV               	60.0   	// FOV for the look cam
TWEAK_FLOAT fHELI_SWAY_MAG           	0.006 	// Magnitude of chopper's sway motion
TWEAK_FLOAT fHELI_SWAY_FREQ          	0.25   	// Frequency of chopper's sway motion

// Jump values
TWEAK_FLOAT fJUMP_CAM_START_INTERP     	0.0  	// Wait before starting scripted interp
TWEAK_FLOAT fJUMP_CAM_GAME_INTERP    	0.0   	// Wait before starting game interp
TWEAK_FLOAT fJUMP_CAM_DUR            	1.1   	// Duration of full interp to game cam
TWEAK_FLOAT fHEAD_CAM_X              	0.0   	// Relative x offset of attached jump cam
TWEAK_FLOAT fHEAD_CAM_Y             	-0.5   	// Relative y offset of attached jump cam
TWEAK_FLOAT fHEAD_CAM_Z              	3.5   	// Relative z offset of attached jump cam
TWEAK_FLOAT fHEAD_CAM_LX             	0.0   	// Relative x look pos of attached jump cam
TWEAK_FLOAT fHEAD_CAM_LY             	0.0   	// Relative y look pos of attached jump cam
TWEAK_FLOAT fHEAD_CAM_LZ            	0.0   	// Relative z look pos of attached jump cam

// Target values
TWEAK_FLOAT fAIR_TGT_RANGE          	22.0   	// Distance for air target check
TWEAK_FLOAT fAIR_TGT_INNER_RANGE		10.0	// Distance for the inner air target check
TWEAK_FLOAT fGROUND_TGT_RG_HORIZ   		15.0  	// Horizontal dist for ground target check
TWEAK_FLOAT fGROUND_TGT_RG_VERT_ABOVE  	5.0   	// Vert above dist for ground target check
TWEAK_FLOAT fGROUND_TGT_RG_VERT_BELOW   -2.0   	// Vert below dist for ground target check
TWEAK_FLOAT fGROUND_TGT_START_MOVING	325.0 	// Vert dist from vehicle target before it starts moving
TWEAK_FLOAT fTRAIN_TGT_START_MOVING 	200.0 	// Vert dist from train before it starts moving

TWEAK_FLOAT fNEXT_TGT_DIST_DROPOFF  	0.05  	// Decrease virtual height per dist from tgt
TWEAK_FLOAT fAIR_TGT_DRAW_SCALE    		14.0  	// Draw scale of air target ring
TWEAK_FLOAT fGND_TGT_DRAW_SCALE_S   	4.0   	// Draw scale of inner ground target ring
TWEAK_FLOAT fGND_TGT_DRAW_SCALE_M   	9.0   	// Draw scale of middle ground target ring
TWEAK_FLOAT fGND_TGT_DRAW_SCALE_L   	14.0	// Draw scale of outer ground target ring
TWEAK_FLOAT fAIR_NXT_TGT_SCALE      	0.5		// Draw scale of next air target
TWEAK_FLOAT fGND_NXT_TGT_SCALE      	0.5		// Draw scale of next ground target
TWEAK_FLOAT fCUR_BLIP_SCALE         	1.2		// Draw scale for current blip
TWEAK_FLOAT fNEXT_BLIP_SCALE        	0.7		// Draw scale for next blip
TWEAK_FLOAT	fFINISH_REWARD_MULT     	1.5		// Minimum money to award for base jump
TWEAK_INT   iTARGET_RED					240		// Amount of red to use when drawing target
TWEAK_INT   iTARGET_GREEN          		200		// Amount of green to use when drawing target
TWEAK_INT   iTARGET_BLUE            	80		// Amount of blue to use when drawing target
TWEAK_INT   iTARGET_ALPHA          		170		// Amount of alpha to use when drawing target
TWEAK_INT	iCHECKPOINT_ALPHA			113		// The checkpoints appear more solid than the hoops
TWEAK_INT	iGATE_REWARD           		30		// How much cash to award player for air gate
TWEAK_INT	iALL_GATE_REWARD			100		// Reward for hitting all the gates in the course
TWEAK_INT	iFINISH_REWARD_BASE     	50     	// Minimum money to award for base jump

// UI values
TWEAK_FLOAT	fINPUT_DELAY		     	0.5   	// Delay before input becomes available
TWEAK_FLOAT fPOST_WIN_TIMEOUT       	60.0  	// Win screen disappears after timeout
TWEAK_FLOAT fPOST_LOSS_TIMEOUT       	60.0  	// Loss screen disappears after timeout
TWEAK_INT	iLAUNCH_FADE_OUT_TIME		2500	// Fade out from launcher in ms
TWEAK_INT   iFADE_OUT_TIME         		800		// Fade out time in ms
TWEAK_INT   iFADE_IN_TIME          		800     // Fade in time in ms


// Add every new tweak value here!
#IF IS_DEBUG_BUILD
PROC BJ_IGNORE_UNREFERENCED_TWEAKS()
	// Cutscene values
	fCUTSCENE_END_WAIT   = fCUTSCENE_END_WAIT
	iCUTSCENE_INTERP_DUR = iCUTSCENE_INTERP_DUR
	
	// Look values
	fLOOK_SCALAR_HORIZ = fLOOK_SCALAR_HORIZ
	fLOOK_SCALAR_VERT  = fLOOK_SCALAR_VERT
	fLOOK_MAX_HORIZ    = fLOOK_MAX_HORIZ
	fLOOK_MAX_VERT     = fLOOK_MAX_VERT
	fLOOK_MIN_VERT     = fLOOK_MIN_VERT
	fLOOK_INIT_PITCH   = fLOOK_INIT_PITCH
	fLOOK_FOV          = fLOOK_FOV
	fHELI_SWAY_MAG     = fHELI_SWAY_MAG
	fHELI_SWAY_FREQ    = fHELI_SWAY_FREQ
	
	// Jump values
	fJUMP_CAM_START_INTERP  = fJUMP_CAM_START_INTERP
	fJUMP_CAM_GAME_INTERP   = fJUMP_CAM_GAME_INTERP
	fJUMP_CAM_DUR           = fJUMP_CAM_DUR
	fHEAD_CAM_X             = fHEAD_CAM_X
	fHEAD_CAM_Y             = fHEAD_CAM_Y
	fHEAD_CAM_Z             = fHEAD_CAM_Z
	fHEAD_CAM_LX            = fHEAD_CAM_LX
	fHEAD_CAM_LY            = fHEAD_CAM_LY
	fHEAD_CAM_LZ            = fHEAD_CAM_LZ
	
	// Target values
	fAIR_TGT_RANGE            = fAIR_TGT_RANGE
	fAIR_TGT_INNER_RANGE	  = fAIR_TGT_INNER_RANGE
	fGROUND_TGT_RG_HORIZ      = fGROUND_TGT_RG_HORIZ
	fGROUND_TGT_RG_VERT_ABOVE = fGROUND_TGT_RG_VERT_ABOVE
	fGROUND_TGT_RG_VERT_BELOW = fGROUND_TGT_RG_VERT_BELOW
	fGROUND_TGT_START_MOVING  = fGROUND_TGT_START_MOVING
	fTRAIN_TGT_START_MOVING   = fTRAIN_TGT_START_MOVING
	
	fNEXT_TGT_DIST_DROPOFF = fNEXT_TGT_DIST_DROPOFF
	fAIR_TGT_DRAW_SCALE    = fAIR_TGT_DRAW_SCALE
	fGND_TGT_DRAW_SCALE_S  = fGND_TGT_DRAW_SCALE_S
	fGND_TGT_DRAW_SCALE_M  = fGND_TGT_DRAW_SCALE_M
	fGND_TGT_DRAW_SCALE_L  = fGND_TGT_DRAW_SCALE_L
	fAIR_NXT_TGT_SCALE     = fAIR_NXT_TGT_SCALE
	fGND_NXT_TGT_SCALE     = fGND_NXT_TGT_SCALE
	fFINISH_REWARD_MULT    = fFINISH_REWARD_MULT
	fCUR_BLIP_SCALE        = fCUR_BLIP_SCALE
	fNEXT_BLIP_SCALE       = fNEXT_BLIP_SCALE
	iTARGET_RED            = iTARGET_RED
	iTARGET_GREEN          = iTARGET_GREEN
	iTARGET_BLUE           = iTARGET_BLUE
	iTARGET_ALPHA          = iTARGET_ALPHA
	iGATE_REWARD           = iGATE_REWARD
	iFINISH_REWARD_BASE    = iFINISH_REWARD_BASE
	
	// UI values
	fINPUT_DELAY          = fINPUT_DELAY
	fPOST_WIN_TIMEOUT     = fPOST_WIN_TIMEOUT
	fPOST_LOSS_TIMEOUT    = fPOST_LOSS_TIMEOUT
	iLAUNCH_FADE_OUT_TIME = iLAUNCH_FADE_OUT_TIME
	iFADE_OUT_TIME        = iFADE_OUT_TIME
	iFADE_IN_TIME         = iFADE_IN_TIME
ENDPROC
#ENDIF
