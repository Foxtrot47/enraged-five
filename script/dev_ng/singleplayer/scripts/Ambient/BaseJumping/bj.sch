
//////////////////////////////////////////////////////////////////////
/* bj.sch															*/
/* Author: DJ Jones													*/
/* Definitions for base jump activity.								*/
//////////////////////////////////////////////////////////////////////


CONST_INT iBJ_STREAMS 13 // launch vehicle, target vehicle, flatbed trailer, driver/pilot, train cars
CONST_INT iMAX_GATES 12 // Max number of gates for multi-gate mode
CONST_INT iBJ_MAX_ONLOOKERS 6 //max number of onlookers to come up and point at the player on a course with ambient peds
CONST_FLOAT fBJ_PLAYER_IN_WATER_SCORECARD_DELAY 2.0

// Internal game states.
ENUM BJ_GAME_STATE
	BJGAMESTATE_INIT,
	BJGAMESTATE_LOAD_CUTSCENE,
	BJGAMESTATE_STREAMING,
	BJGAMESTATE_POST_STREAM,
	BJGAMESTATE_CUTSCENE,
	BJGAMESTATE_SETUP_JUMP,
	BJGAMESTATE_FREE_JUMP,
	BJGAMESTATE_HELI_LOOK,
	BJGAMESTATE_HELI_JUMP,
	BJGAMESTATE_SKYDIVING,
	BJGAMESTATE_PROCESS_DATA,
	BJGAMESTATE_WINNER,
	BJGAMESTATE_LOSER
ENDENUM

// The jumps.
ENUM BJ_JUMP_ID
	BJJUMPID_HARBOR,
	BJJUMPID_RACE_TRACK,
	BJJUMPID_WINDMILLS,
	BJJUMPID_NORTH_CLIFF,
	BJJUMPID_MAZE_BANK,
	BJJUMPID_CRANE,
	BJJUMPID_RIVER_CLIFF,
	BJJUMPID_RUNAWAY_TRAIN,
	BJJUMPID_GOLF_COURSE,
	BJJUMPID_1K,
	BJJUMPID_1_5K,
	BJJUMPID_CANAL,
	BJJUMPID_ROCK_CLIFF,
	BJJUMPIDS
ENDENUM
