
//////////////////////////////////////////////////////////////////////
/* bj_core.sch														*/
/* Author: DJ Jones													*/
/* Setup and cleanup functionality for base jump activity.			*/
//////////////////////////////////////////////////////////////////////

USING "cutscene_public.sch"
USING "minigames_helpers.sch"
USING "minigame_uiinputs.sch"
USING "socialclub_leaderboard.sch"
USING "minigame_big_message.sch"
USING "minigame_stats_tracker_helpers.sch"
USING "script_oddjob_funcs.sch"
USING "traffic_default_values.sch"
USING "bj_sc_leaderboard_lib.sch"
//USING "traffic.sch"
// See blip_ambient.sch and comms_control_data_gta5.sch for more includes


ENUM BJ_GATE_CHECK_RESULT
	BJGATECHECK_NONE,
	BJGATECHECK_PASS,
	BJGATECHECK_FAIL,
	BJGATECHECK_FAIL_TOO_FAR_LANDING
ENDENUM

BOOL bGutterIconDelayFlag = FALSE
BOOL bInOuterCheckpointSphere = FALSE
structTimer tGutterIconDelayTimer

// Tell whether a given model is a helicopter.
FUNC BOOL BJ_IS_MODEL_HELI(MODEL_NAMES modelName)
	SWITCH modelName
		CASE ANNIHILATOR
		CASE BUZZARD
		CASE BUZZARD2
		CASE CARGOBOB
		CASE CARGOBOB2
		CASE FROGGER
		CASE MAVERICK
		CASE POLMAV
		CASE SKYLIFT
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Tell whether a given model is a ground vehicle (bike, motorcyle or quad).
FUNC BOOL BJ_IS_MODEL_GROUND_VEH(MODEL_NAMES modelName)
	SWITCH modelName
		CASE BMX
		CASE SCORCHER
		CASE TRIBIKE
		CASE TRIBIKE2
		CASE TRIBIKE3
		CASE VADER
		CASE AKUMA
		CASE BAGGER
		CASE RUFFIAN
		CASE BATI
		CASE BATI2
		CASE BLAZER
		CASE BLAZER2
		CASE CRUISER
		CASE HEXER
		CASE NEMESIS
		CASE PCJ
		CASE POLICEB
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Tell whether a given model is a boat.
FUNC BOOL BJ_IS_MODEL_BOAT(MODEL_NAMES modelName)
	SWITCH modelName
		CASE SEASHARK
		CASE SEASHARK2
		CASE DINGHY
		CASE JETMAX
		CASE MARQUIS
		CASE SQUALO
		CASE SUNTRAP
		CASE TROPIC
		CASE PREDATOR
		
		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

// Tell whether a given model is a truck.
FUNC BOOL BJ_IS_MODEL_TRUCK(MODEL_NAMES modelName)
	SWITCH modelName
		CASE HAULER
		CASE BIFF
		CASE PACKER
		CASE TRASH
		CASE BENSON
		CASE PHANTOM
		CASE POUNDER
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// Tell whether a given model is a trailer.
FUNC BOOL BJ_IS_MODEL_TRAILER(MODEL_NAMES modelName)
	SWITCH modelName
		CASE BOATTRAILER
		CASE DOCKTRAILER
		CASE TANKER
		CASE TR2
		CASE TR3
		CASE TRAILERLOGS
		CASE TRAILERS
		CASE TRAILERS2
		CASE TRAILERS3
		CASE TRFLAT
		CASE TVTRAILER
		
		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

// Tell whether a given model is a train.
FUNC BOOL BJ_IS_MODEL_TRAIN(MODEL_NAMES modelName)
	SWITCH modelName
		CASE FREIGHT
		CASE METROTRAIN
		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT BJ_GET_PLAYER_SPEAKER_ID()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		PRINTLN(">>>>>Current player character is FRANKLIN")
		RETURN 2 	
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		PRINTLN(">>>>>Current player character is TREVOR")
		RETURN 3
	ELSE
		PRINTLN(">>>>>Current player character is MICHAEL")
		//either michael or ped dispatcher (both have spkr id of 1)
		RETURN 1
	ENDIF

ENDFUNC

PROC BJ_ADD_PILOT_FOR_DIALOGUE(structPedsForConversation& thisConversationPedStruct, PED_INDEX thisPedIndex, STRING thisVoiceName)
	INT speakerID = 0 //default for pilot
	
	IF NOT IS_PED_INJURED (thisPedIndex)
		ADD_PED_FOR_DIALOGUE(thisConversationPedStruct, speakerID, thisPedIndex, thisVoiceName, TRUE)
	ELSE
		ADD_PED_FOR_DIALOGUE(thisConversationPedStruct, speakerID, NULL, thisVoiceName)
	ENDIF
ENDPROC

PROC BJ_ADD_PLAYER_FOR_DIALOGUE(structPedsForConversation& thisConversationPedStruct, PED_INDEX thisPedIndex, STRING thisVoiceName)
	INT speakerID 
	speakerID =  BJ_GET_PLAYER_SPEAKER_ID()
	
	IF NOT IS_PED_INJURED (thisPedIndex)
		ADD_PED_FOR_DIALOGUE(thisConversationPedStruct, speakerID, thisPedIndex, thisVoiceName, TRUE)
	ELSE
		ADD_PED_FOR_DIALOGUE(thisConversationPedStruct, speakerID, NULL, thisVoiceName)
	ENDIF
	
ENDPROC

PROC BJ_CLEANUP_DIALOGUE(structPedsForConversation& thisConversationPedStruct)
	REMOVE_PED_FOR_DIALOGUE(thisConversationPedStruct, 0)
	REMOVE_PED_FOR_DIALOGUE(thisConversationPedStruct, 1)
	REMOVE_PED_FOR_DIALOGUE(thisConversationPedStruct, 2)
ENDPROC

/// PURPOSE:
///    Returns name of character voice as a string for setting up dialogue peds
/// RETURNS:
///    
FUNC STRING BJ_GET_PLAYER_DIALOGUE_VOICE_NAME()
	enumCharacterList tempChar
	tempChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	SWITCH(tempChar)
		CASE CHAR_MICHAEL
			RETURN "MICHAEL"
		CASE CHAR_TREVOR
			RETURN "TREVOR"
		CASE CHAR_FRANKLIN
			RETURN "FRANKLIN"
		DEFAULT
			SCRIPT_ASSERT("could not return player voice name!")
			RETURN ""
	ENDSWITCH
ENDFUNC

// See if the player has failed by wandering away from the jump location.
FUNC BOOL BJ_CHECK_WANDER_FAIL(BJ_ARGS& bjArgs, BJ_JUMP_ID eJumpID, BOOL& bWanderFail)
	VEHICLE_INDEX vehPlayer
	VECTOR vPlayerPos, vFirstTarget
	FLOAT fGroundZ
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
		bWanderFail = TRUE
		RETURN TRUE
	ENDIF
	
	vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		IF IS_ENTITY_IN_AIR(vehPlayer)
			RETURN FALSE
		ELSE
			IF NOT GET_GROUND_Z_FOR_3D_COORD(GET_ENTITY_COORDS(vehPlayer), fGroundZ)
				RETURN FALSE
			ELIF vPlayerPos.z >= fGroundZ + 1.5
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		RETURN FALSE
	ELSE
		IF NOT GET_GROUND_Z_FOR_3D_COORD(vPlayerPos, fGroundZ)
			RETURN FALSE
		ELIF vPlayerPos.z >= fGroundZ + 1.5
			RETURN FALSE
		ENDIF
	ENDIF
	
	vFirstTarget = BJ_GET_ARG_TARGET_POSITION(bjArgs, 0)
	
	IF vPlayerPos.z < vFirstTarget.z + 5.0
		bWanderFail = TRUE
		RETURN TRUE
	ENDIF
	
	IF eJumpID = BJJUMPID_CRANE
		IF vPlayerPos.z < 288.0
			bWanderFail = TRUE
			RETURN TRUE
		ENDIF
	ELIF eJumpID = BJJUMPID_MAZE_BANK
		IF vPlayerPos.z < 305.0
			bWanderFail = TRUE
			RETURN TRUE
		ENDIF
	ELIF eJumpID = BJJUMPID_GOLF_COURSE
		IF vPlayerPos.z < 227.0
			bWanderFail = TRUE
			RETURN TRUE
		ENDIF
	ELIF eJumpID = BJJUMPID_RIVER_CLIFF
	OR eJumpID = BJJUMPID_ROCK_CLIFF
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID)) > 3600.0 // 60^2
			bWanderFail = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays single line of dialogue from OJBJ dstar file
/// PARAMS:
///    thisConversationPedStruct - 
///    sRoot - 
///    sLabel - 
/// RETURNS:
///    
FUNC BOOL BJ_PLAY_DIALOGUE_LINE(structPedsForConversation& thisConversationPedStruct, STRING sRoot, STRING sLabel)
	RETURN PLAY_SINGLE_LINE_FROM_CONVERSATION(thisConversationPedStruct, "OJBJAUD", sRoot, sLabel, CONV_PRIORITY_VERY_HIGH)
ENDFUNC

FUNC TEXT_LABEL BJ_APPEND_DIALOGUE_ROOT_FOR_PLAYER(STRING thisRoot)
	enumCharacterList tempChar
	TEXT_LABEL returnLabel = thisRoot
	tempChar = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	SWITCH(tempChar)
		CASE CHAR_MICHAEL
			returnLabel += "M"
			BREAK
		CASE CHAR_TREVOR
			returnLabel += "F"
			BREAK
		CASE CHAR_FRANKLIN
			returnLabel += "T"
			BREAK
		DEFAULT
			SCRIPT_ASSERT("could not append player dialogue root!!")
			BREAK
	ENDSWITCH
	RETURN returnLabel
ENDFUNC

PROC BJ_UPDATE_PLAYER_DIALOGUE(/*structPedsForConversation& thisConversationPedStruct, */structTimer &dialogueTimer, /*TEXT_LABEL& usedLabels[], TEXT_LABEL thisRoot,*/ INT& iNumPlayerLines)
	IF IS_TIMER_STARTED(dialogueTimer)
		IF TIMER_DO_ONCE_WHEN_READY(dialogueTimer, 7.5 * iNumPlayerLines)
			iNumPlayerLines++
			// This is going to be replaced by AI reactions BASEJUMP_ABOUT_TO_JUMP
			PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "BASEJUMP_ABOUT_TO_JUMP")
			//TEXT_LABEL tempLabel = BJ_GENERATE_RANDOM_LABEL_FROM_ROOT(thisRoot, usedLabels)
			//BJ_PLAY_DIALOGUE_LINE(thisConversationPedStruct, thisRoot, tempLabel)
		ENDIF
	ELSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			START_TIMER_AT(dialogueTimer, 0)
		ENDIF
	ENDIF
ENDPROC

PROC BJ_PLAY_FAIL_DIALOGUE()
	PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_CURSE_MED")
ENDPROC

PROC BJ_REQUEST_TRAIN_MODELS()
	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(TANKERCAR)
	REQUEST_MODEL(FREIGHTCAR)
ENDPROC

FUNC BOOL BJ_HAVE_ALL_TRAIN_MODELS_LOADED()
	IF HAS_MODEL_LOADED(FREIGHT)
	AND HAS_MODEL_LOADED(FREIGHTCONT1)
	AND HAS_MODEL_LOADED(FREIGHTCONT2)
	AND HAS_MODEL_LOADED(FREIGHTGRAIN)
	AND HAS_MODEL_LOADED(TANKERCAR)
	AND HAS_MODEL_LOADED(FREIGHTCAR)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC NJ_SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTGRAIN)
	SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)					
ENDPROC


// Set up the player, start and target vehicles, etc.
PROC BJ_INIT_JUMP(BJ_ARGS& bjArgs, BJ_JUMP_ID eJumpID, STREAMED_MODEL& assets[], MODEL_NAMES& launchDriverModel, MODEL_NAMES& targetDriverModel, SCALEFORM_INDEX &bjUI_Leaderboard, INT& iAnticipation)
	MODEL_NAMES curModel, curTargetModel
	DISABLE_CELLPHONE(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(FALSE)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_LOD_MULTIPLIER(PLAYER_PED_ID(), 2.0)
	ENDIF
	
	SWITCH eJumpID
		CASE BJJUMPID_HARBOR
			ADD_SCENARIO_BLOCKING_AREA(<<-901.2005, 4422.4888, 19.3471>>, <<-906.8420, 4424.9702, 300.0170>>)
			BREAK
		CASE BJJUMPID_RACE_TRACK
			BREAK
		CASE BJJUMPID_WINDMILLS
			BREAK
		CASE BJJUMPID_NORTH_CLIFF
			BREAK
		CASE BJJUMPID_MAZE_BANK
			BREAK
		CASE BJJUMPID_CRANE
			BREAK
		CASE BJJUMPID_RIVER_CLIFF
			BREAK
		CASE BJJUMPID_RUNAWAY_TRAIN
			BREAK
		CASE BJJUMPID_GOLF_COURSE
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE, TRUE)
			BREAK
		CASE BJJUMPID_1K
			BREAK
		CASE BJJUMPID_1_5K
			BREAK
		CASE BJJUMPID_CANAL
			BREAK
		CASE BJJUMPID_ROCK_CLIFF 
			BREAK
	ENDSWITCH
	
	IF IS_SCREEN_FADED_OUT()
		CLEAR_AREA(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), 5000, TRUE)
	ENDIF
	
	// Stream text, UI, anything needed
	REQUEST_ADDITIONAL_TEXT("BJUMP", ODDJOB_TEXT_SLOT)
	
	// Streaming requests.
	curModel = BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID)
	IF curModel <> DUMMY_MODEL_FOR_SCRIPT
		IF BJ_IS_MODEL_HELI(curModel)
			REQUEST_ANIM_DICT("veh@helicopter@rps@base")
			launchDriverModel = BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID)
			ADD_STREAMED_MODEL(assets, launchDriverModel)
		ENDIF
		ADD_STREAMED_MODEL(assets, curModel)
	ENDIF
	
	IF BJ_GET_ARG_DROPOFF_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
		ADD_STREAMED_MODEL(assets, BJ_GET_ARG_DROPOFF_MODEL(bjArgs))
		IF BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID) <> DUMMY_MODEL_FOR_SCRIPT
			ADD_STREAMED_MODEL(assets, BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID))
		ELSE
			ADD_STREAMED_MODEL(assets, S_M_Y_PILOT_01)
		ENDIF
	ENDIF
	
	curTargetModel = BJ_GET_ARG_TARGET_MODEL(bjArgs)
	IF curTargetModel <> DUMMY_MODEL_FOR_SCRIPT
		targetDriverModel = BJ_GET_DRIVER_BY_ID(eJumpID)
		ADD_STREAMED_MODEL(assets, targetDriverModel)
		ADD_STREAMED_MODEL(assets, curTargetModel)
		ADD_STREAMED_MODEL(assets, TRFLAT)
	ENDIF
	
	IF BJ_IS_MODEL_TRAIN(curTargetModel)
		ADD_STREAMED_MODEL(assets, FREIGHT)
		ADD_STREAMED_MODEL(assets, FREIGHTCAR)
		ADD_STREAMED_MODEL(assets, FREIGHTGRAIN)
		ADD_STREAMED_MODEL(assets, FREIGHTCONT1)
		ADD_STREAMED_MODEL(assets, FREIGHTCONT2)
		ADD_STREAMED_MODEL(assets, TANKERCAR)
		ADD_STREAMED_MODEL(assets, METROTRAIN)
	ENDIF
	
	IF BJ_GET_ARG_EXIT_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
		ADD_STREAMED_MODEL(assets, BJ_GET_ARG_EXIT_MODEL(bjArgs))
	ENDIF
	
	bjUI_Leaderboard = REQUEST_SC_LEADERBOARD_UI()
	REQUEST_ALL_MODELS(assets)
	REQUEST_ANIM_DICT("oddjobs@basejump@")
	REQUEST_ANIM_DICT("skydive@freefall")
	REQUEST_ANIM_DICT("skydive@parachute@chute")
	REQUEST_ANIM_DICT("skydive@parachute@")
	REQUEST_STREAMED_TEXTURE_DICT("basejumping")
	
	// Determine which anticipation animation we'll be using.
	IF BJ_GET_ANTICIPATION_DELAY_BY_ID(eJumpID) > 0.0
		iAnticipation = GET_RANDOM_INT_IN_RANGE(0, 2)
		CERRORLN(DEBUG_MISSION, "Yes, the random number generator really is that shitty")
		SWITCH iAnticipation
			CASE 0
				REQUEST_ANIM_DICT("amb@world_human_hiker_standing@male@idle_a")
			BREAK
			CASE 1
				REQUEST_ANIM_DICT("amb@world_human_muscle_flex@arms_at_side@base")
			BREAK
//			CASE 2
//				REQUEST_ANIM_DICT("playidles_cold")
//			BREAK
		ENDSWITCH
	ENDIF
	
	IF BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID) = DUMMY_MODEL_FOR_SCRIPT
		REQUEST_ADDITIONAL_COLLISION_AT_COORD(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
	ENDIF
	
	//REGISTER_SCRIPT_WITH_AUDIO()
	//REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\BJUMP")
ENDPROC

FUNC BOOL BJ_IS_SCREEN_FADED_WITH_PLAYER_NEAR_LOAD_SCENE_COORD(BJ_JUMP_ID eJumpID)
	IF IS_SCREEN_FADED_OUT()
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), <<15, 15, 15>>)
			//screen is faded out, and we're with 15m of the player location
			RETURN TRUE
		ENDIF
		
		//set the player location if we're not near the launch location
		SET_ENTITY_COORDS(PLAYER_PED_ID(), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), FALSE)
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), <<15, 15, 15>>)
			//screen is faded out, and we're with 15m of the player location
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE

ENDFUNC

// Check for streaming and other init, return true when complete.
FUNC BOOL BJ_IS_STREAMING_COMPLETE(STREAMED_MODEL& assets[], structTimer& flowTimer, SCALEFORM_INDEX &bjUI_Leaderboard, BJ_JUMP_ID eJumpID, INT iAnticipation, BOOL bWaitForFadeOut)
	IF NOT BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
		REQUEST_ADDITIONAL_COLLISION_AT_COORD(BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
	ENDIF
	
	IF IS_TIMER_STARTED(flowTimer)
		IF GET_TIMER_IN_SECONDS(flowTimer) >= 5.0 //timeout after 5 seconds
			PRINTLN("Streaming is taking too long! Moving on without finishing!")
			RETURN TRUE
		ENDIF
	ELSE
		START_TIMER_AT(flowTimer, 0)
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT IS_NEW_LOAD_SCENE_LOADED()
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					PRINTLN("Waiting for load scene to finish...")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
		IF (NOT HAS_ANIM_DICT_LOADED("veh@helicopter@rps@base"))
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					PRINTLN("Waiting for heli-specific anims to load...")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 sAnimDict
	IF BJ_GET_ANTICIPATION_DELAY_BY_ID(eJumpID) > 0.0
		SWITCH iAnticipation
			CASE 0
				sAnimDict = "amb@world_human_hiker_standing@male@idle_a"
			BREAK
			CASE 1
				sAnimDict = "amb@world_human_muscle_flex@arms_at_side@base"
			BREAK
			CASE 2
				sAnimDict = "playidles_cold"
			BREAK
		ENDSWITCH
		
		IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
					PRINTLN("Waiting for anticipation anims to load...")
				ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ARE_MODELS_STREAMED(assets)
		IF HAS_SCALEFORM_MOVIE_LOADED(bjUI_Leaderboard)
			IF HAS_ADDITIONAL_TEXT_LOADED(ODDJOB_TEXT_SLOT)
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("basejumping")
					IF HAS_ANIM_DICT_LOADED("oddjobs@basejump@")
					AND HAS_ANIM_DICT_LOADED("skydive@freefall")
					AND HAS_ANIM_DICT_LOADED("skydive@parachute@chute")
					AND HAS_ANIM_DICT_LOADED("skydive@parachute@")
						IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
							IF (NOT bWaitForFadeOut) OR IS_SCREEN_FADED_OUT()
								RETURN TRUE
							#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
								PRINTLN("Waiting for fade out...") #ENDIF
							ENDIF
						#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
							PRINTLN("Waiting for ped creation permission...") #ENDIF
						ENDIF
					#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
						PRINTLN("Waiting for anim dictionaries to load...") #ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
					PRINTLN("Waiting for texture dictionary...") #ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
				PRINTLN("Waiting for oddjob text to load...") #ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
			PRINTLN("Waiting for scaleform movies to load...") #ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD ELIF IS_KEYBOARD_KEY_PRESSED(KEY_Z)
		PRINTLN("Waiting for models to stream...") #ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BJ_SETUP_DROPOFF_HELI(BJ_ARGS& bjArgs, VEHICLE_INDEX& thisVeh, BJ_JUMP_ID eJumpID)
	MODEL_NAMES pilotModel
	PED_INDEX pedPilot
	VEHICLE_INDEX motoHelicopter
	
	IF DOES_ENTITY_EXIST(thisVeh)
		IF NOT IS_VEHICLE_FUCKED(thisVeh) AND DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(thisVeh))
			pedPilot = GET_PED_IN_VEHICLE_SEAT(thisVeh) 
		ENDIF
		motoHelicopter = thisVeh
	ENDIF
	
	pilotModel = BJ_GET_LAUNCH_PILOT_BY_ID(eJumpID)
	IF pilotModel <> DUMMY_MODEL_FOR_SCRIPT
		// Attempt to steal heli and pilot here
		IF NOT DOES_ENTITY_EXIST(motoHelicopter)
			motoHelicopter = CREATE_VEHICLE(BJ_GET_ARG_DROPOFF_MODEL(bjArgs), (BJ_GET_ARG_DROPOFF_POSITION(bjArgs)), BJ_GET_ARG_DROPOFF_HEADING(bjArgs))
		ELSE
			SET_ENTITY_COORDS(motoHelicopter, BJ_GET_ARG_DROPOFF_POSITION(bjArgs))
			SET_ENTITY_HEADING(motoHelicopter, BJ_GET_ARG_DROPOFF_HEADING(bjArgs))
		ENDIF
		FREEZE_ENTITY_POSITION(motoHelicopter, TRUE)
		IF NOT DOES_ENTITY_EXIST(pedPilot)
			pedPilot = CREATE_PED_INSIDE_VEHICLE(motoHelicopter, PEDTYPE_MISSION, pilotModel)
		ENDIF
		SET_PED_HELMET(pedPilot, FALSE)
		BJ_SET_PILOT_COMPONENTS_BY_ID(pedPilot, eJumpID)
		
	ELSE
		IF NOT DOES_ENTITY_EXIST(motoHelicopter)
			motoHelicopter = CREATE_VEHICLE(BJ_GET_ARG_DROPOFF_MODEL(bjArgs), (BJ_GET_ARG_DROPOFF_POSITION(bjArgs)), BJ_GET_ARG_DROPOFF_HEADING(bjArgs))
		ELSE
			SET_ENTITY_COORDS(motoHelicopter, BJ_GET_ARG_DROPOFF_POSITION(bjArgs))
			SET_ENTITY_HEADING(motoHelicopter, BJ_GET_ARG_DROPOFF_HEADING(bjArgs))
		ENDIF
		FREEZE_ENTITY_POSITION(motoHelicopter, TRUE)
		IF NOT DOES_ENTITY_EXIST(pedPilot)
			pedPilot = CREATE_PED_INSIDE_VEHICLE(motoHelicopter, PEDTYPE_MISSION, S_M_Y_PILOT_01)
		ENDIF
		SET_PED_HELMET(pedPilot, FALSE)
		SET_PED_PROP_INDEX(pedPilot, ANCHOR_HEAD, 2, 0)
		SET_PED_PROP_INDEX(pedPilot, ANCHOR_EYES, 0, 0)
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(motoHelicopter, TRUE, TRUE)
	//SET_HELI_BLADES_FULL_SPEED(motoHelicopter)
	SET_HELI_BLADES_SPEED(motoHelicopter, 0.7)
	CLEAR_PED_TASKS(pedPilot)
	TASK_STAND_STILL(pedPilot, -1)
	thisVeh = motoHelicopter
ENDPROC

// Setup vehicle placed for player's convenience when he lands.
PROC BJ_SETUP_EXIT_VEHICLE(BJ_ARGS& bjArgs, VEHICLE_INDEX& exitVehicle)	
	IF NOT DOES_ENTITY_EXIST(exitVehicle)
		exitVehicle = CREATE_VEHICLE(BJ_GET_ARG_EXIT_MODEL(bjArgs), BJ_GET_ARG_EXIT_POSITION(bjArgs), BJ_GET_ARG_EXIT_HEADING(bjArgs))		
	ELIF IS_ENTITY_DEAD(exitVehicle) //1392278 - not sure how much to worry about it being disabled? maybe cleaner to just clean it up on retry - SiM
		DELETE_VEHICLE(exitVehicle)
		exitVehicle = CREATE_VEHICLE(BJ_GET_ARG_EXIT_MODEL(bjArgs), BJ_GET_ARG_EXIT_POSITION(bjArgs), BJ_GET_ARG_EXIT_HEADING(bjArgs))
	ELSE
		SET_VEHICLE_FIXED(exitVehicle)		
	ENDIF
ENDPROC

// Setup any post-streaming items here.
PROC BJ_SETUP_JUMP(BJ_ARGS& bjArgs, structPedsForConversation& thisConversationPedStruct, INT& iGates, VEHICLE_INDEX& launchVehicle, VEHICLE_INDEX& targetVehicle, VEHICLE_INDEX& extraVehicle, VEHICLE_INDEX& targetFlatbed, VEHICLE_INDEX& exitVehicle, PED_INDEX& launchDriver, PED_INDEX& targetDriver, MODEL_NAMES launchDriverModel, MODEL_NAMES targetDriverModel, structTimer& radioTimer, BJ_JUMP_ID eJumpID, INT& iSceneID)
	MODEL_NAMES curModel
	VEHICLE_INDEX vehPlayer
	VECTOR vPlayerVehCoords
	INT iIndex
	
	// Make sure the player is valid.
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF IS_VECTOR_ZERO(BJ_GET_ARG_CAR_REMOVAL_COORDS_MIN(bjArgs))// OR IS_VECTOR_ZERO(BJ_GET_ARG_CAR_REMOVAL_COORDS_MAX())
		//do nothing
	ELSE
		//turn off car generators
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(BJ_GET_ARG_CAR_REMOVAL_COORDS_MIN(bjArgs), BJ_GET_ARG_CAR_REMOVAL_COORDS_MAX(bjArgs), FALSE)
	ENDIF
	
	// Figure out how many gates we have.
	REPEAT iMAX_GATES iIndex
		IF NOT IS_VECTOR_ZERO(BJ_GET_ARG_TARGET_POSITION(bjArgs, iIndex))
			iGates = iIndex + 1
		ELSE
			iIndex = 999999
		ENDIF
	ENDREPEAT
	
	// Take away player control.
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	
	// Teleport the player's last vehicle off the top of the Maze Bank building.
	// Might need to generalize this to other jumps.
	IF eJumpID = BJJUMPID_MAZE_BANK
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 320.0 AND vPlayerVehCoords.z < 340.0
				AND vPlayerVehCoords.x > -103.0 AND vPlayerVehCoords.x < -45.0
				AND vPlayerVehCoords.y > -850.0 AND vPlayerVehCoords.y < -787.0
					SET_ENTITY_COORDS(vehPlayer, <<-89.7940, -742.6727, 43.7472>>)
					SET_ENTITY_HEADING(vehPlayer, -109.33)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	
	ELIF eJumpID = BJJUMPID_NORTH_CLIFF
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 689.4 AND vPlayerVehCoords.z < 701.8875
				AND vPlayerVehCoords.x > 404.6 AND vPlayerVehCoords.x < 413.4
				AND vPlayerVehCoords.y > 5700.6 AND vPlayerVehCoords.y < 5711.9
					SET_ENTITY_COORDS(vehPlayer, <<-215.2838, 6543.5698, 10.0967>>)
					SET_ENTITY_HEADING(vehPlayer, 145.5732)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	
	ELIF eJumpID = BJJUMPID_CRANE
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 294.5 AND vPlayerVehCoords.z < 298.9
				AND vPlayerVehCoords.x > -121.3 AND vPlayerVehCoords.x < -116.5
				AND vPlayerVehCoords.y > -978.1 AND vPlayerVehCoords.y < -973.2
					SET_ENTITY_COORDS(vehPlayer, <<-118.1021, -947.3954, 27.3296>>)
					SET_ENTITY_HEADING(vehPlayer, 341.9614)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	
	ELIF eJumpID = BJJUMPID_RIVER_CLIFF
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 182.7 AND vPlayerVehCoords.z < 202.7
				AND vPlayerVehCoords.x > -1252.7 AND vPlayerVehCoords.x < -1227.9
				AND vPlayerVehCoords.y > 4525.8 AND vPlayerVehCoords.y < 4549.3
					SET_ENTITY_COORDS(vehPlayer, <<-1237.0216, 4559.4043, 185.9418>>)
					SET_ENTITY_HEADING(vehPlayer, 172.2123)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	
	ELIF eJumpID = BJJUMPID_GOLF_COURSE
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 230.0 AND vPlayerVehCoords.z < 244.5
				AND vPlayerVehCoords.x > -809.6 AND vPlayerVehCoords.x < -747.7
				AND vPlayerVehCoords.y > 310.6 AND vPlayerVehCoords.y < 346.6
					SET_ENTITY_COORDS(vehPlayer, <<-1351.8053, 133.9500, 55.2558>>)
					SET_ENTITY_HEADING(vehPlayer, 0.8373)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	
	ELIF eJumpID = BJJUMPID_ROCK_CLIFF
		vehPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_ENTITY_DEAD(vehPlayer)
				DELETE_VEHICLE(vehPlayer)
			ELSE
				vPlayerVehCoords = GET_ENTITY_COORDS(vehPlayer)
				IF vPlayerVehCoords.z > 143.6 AND vPlayerVehCoords.z < 154.5
				AND vPlayerVehCoords.x > -777.0 AND vPlayerVehCoords.x < -758.9
				AND vPlayerVehCoords.y > 4328.3 AND vPlayerVehCoords.y < 4344.0
					SET_ENTITY_COORDS(vehPlayer, <<-765.8775, 4294.8071, 145.6581>>)
					SET_ENTITY_HEADING(vehPlayer, 349.2306)
					SET_VEHICLE_DOORS_SHUT(vehPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		// Take away player control.
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
	ENDIF
	
	curModel = BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID)
	IF curModel <> DUMMY_MODEL_FOR_SCRIPT
		PRINTLN(">>> BJ_SETUP_JUMP - SETUP PLAYE FOR LAUNCHING FROM A HELICOPTER")
		// Create launch vehicle here, attach player, put vehicle on path, etc.
		IF NOT IS_ENTITY_DEAD(launchVehicle) OR DOES_ENTITY_EXIST(launchVehicle)
			PRINTLN(">>> BJ_SETUP_JUMP - ALREADY IN A HELICOPTER! RESTORING LAUNCH VEHICLE HEALTH ")
			SET_VEHICLE_FIXED(launchVehicle)
			//SET_ENTITY_HEALTH(launchVehicle, GET_ENTITY_MAX_HEALTH(launchVehicle))
			//SET_VEHICLE_ENGINE_HEALTH(launchVehicle, 1000)
			SET_ENTITY_COORDS(launchVehicle, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
			SET_ENTITY_HEADING(launchVehicle, BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
		ELSE
			PRINTLN(">>> BJ_SETUP_JUMP - CREATING NEW LAUNCH VEHICLE ")
			
			launchVehicle = CREATE_VEHICLE(curModel, BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID), BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
		ENDIF
		
		// If this is a helicopter, set up the vehicle and the pilot.
		IF BJ_IS_MODEL_HELI(curModel)
			IF NOT DOES_ENTITY_EXIST(launchDriver) OR IS_ENTITY_DEAD(launchDriver)
				PRINTLN(">>> BJ_SETUP_JUMP - CREATING NEW PILOT FOR LAUNCH VEHICLE ")
				launchDriver = CREATE_PED_INSIDE_VEHICLE(launchVehicle, PEDTYPE_MISSION, launchDriverModel)
				SET_PED_HELMET(launchDriver, FALSE)
				BJ_SET_PILOT_COMPONENTS_BY_ID(launchDriver, eJumpID)
				SET_ENTITY_INVINCIBLE(launchDriver, TRUE)
			ENDIF
			
			//IF eJumpID <> BJJUMPID_1_5K AND eJumpID <> BJJUMPID_CANAL
				SHAKE_SCRIPT_GLOBAL("SKY_DIVING_SHAKE", 0.15)
			//ENDIF
			
			BJ_ADD_PILOT_FOR_DIALOGUE(thisConversationPedStruct, launchDriver, BJ_GET_LAUNCH_PILOT_VOICE_FOR_DIALOGUE_BY_ID(eJumpID))
			BJ_ADD_PLAYER_FOR_DIALOGUE(thisConversationPedStruct, PLAYER_PED_ID(), BJ_GET_PLAYER_DIALOGUE_VOICE_NAME())
			
			
			
			//SET_HELI_BLADES_FULL_SPEED(launchVehicle)
			SET_HELI_BLADES_SPEED(launchVehicle, 0.7)
			FREEZE_ENTITY_POSITION(launchVehicle, TRUE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), launchVehicle)
					PRINTLN(">>> BJ_SETUP_JUMP - PLAYER IS NOT INSIDE THE LAUNCH VEHICLE. DETACHING.")
					DETACH_ENTITY(PLAYER_PED_ID())
					PRINTLN(">>> BJ_SETUP_JUMP - SETTING PLAYER INTO THE LAUNCH VEHICLE.")
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), launchVehicle, VS_BACK_RIGHT)
				ELSE
					PRINTLN(">>> BJ_SETUP_JUMP - PLAYER IS INSIDE THE LAUNCH VEHICLE. MAKING SURE HES IN THE CORRECT SEAT.")
					IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), launchVehicle) = VS_BACK_LEFT
						PRINTLN(">>> BJ_SETUP_JUMP - PLAYER IS IN THE WRONG SEAT. SHUFFLING OVER TO THE NEXT SEAT")
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), launchVehicle, VS_BACK_RIGHT)
					ENDIF
				ENDIF		
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(launchVehicle)
				iSceneId = CREATE_SYNCHRONIZED_SCENE (<<0,0,0>>, <<0,0,0>>) 
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, launchVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(launchVehicle, "Chassis"))
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSceneId, "oddjobs@basejump@", "Heli_door_loop", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS)
				SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, TRUE)
       		ENDIF

//			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), launchVehicle, GET_ENTITY_BONE_INDEX_BY_NAME(launchVehicle, "Chassis"), <<0, 0, 0>>, <<0, 0, 0>>)
//			TASK_PLAY_ANIM(PLAYER_PED_ID(), "oddjobs@basejump@", "Heli_door_loop", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
//			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), launchVehicle, VS_BACK_RIGHT)
			
			START_TIMER_NOW(radioTimer)
		// If this is a ground launch vehicle, put the player inside.
		ELIF BJ_IS_MODEL_GROUND_VEH(curModel)
			BJ_ADD_PLAYER_FOR_DIALOGUE(thisConversationPedStruct, PLAYER_PED_ID(), BJ_GET_PLAYER_DIALOGUE_VOICE_NAME())
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) OR IS_PED_ON_VEHICLE(PLAYER_PED_ID()))
//				SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
//			ENDIF
//			//unfreeze player since he was waiting for collision to stream in
//			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), launchVehicle, VS_DRIVER)
			GIVE_PED_HELMET(PLAYER_PED_ID(), FALSE)
			START_TIMER_NOW(radioTimer)
		ENDIF
	
	// Put the player in place if we're fading.
	ELSE
		BJ_ADD_PLAYER_FOR_DIALOGUE(thisConversationPedStruct, PLAYER_PED_ID(), BJ_GET_PLAYER_DIALOGUE_VOICE_NAME())
		
		IF BJ_GET_ARG_CUTSCENE_FADE(bjArgs)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) OR IS_PED_ON_VEHICLE(PLAYER_PED_ID()))
				SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
			ENDIF
			SET_ENTITY_COORDS(PLAYER_PED_ID(), BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID))
			SET_ENTITY_HEADING(PLAYER_PED_ID(), BJ_GET_ARG_LAUNCH_HEADING(bjArgs))
			TASK_STAND_STILL(PLAYER_PED_ID(), -1)
			//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			//get rid of the launch vehicle if we have one, or if we grabbed one from the bj_launcher
			IF DOES_ENTITY_EXIST(launchVehicle)
				//make sure we delete any peds in the vehicle
				INT iTemp
				PED_INDEX tempPed
				FOR iTemp = -1 TO GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(launchVehicle) - 1
					IF NOT IS_VEHICLE_SEAT_FREE(launchVehicle, INT_TO_ENUM(VEHICLE_SEAT, iTemp))
						tempPed = GET_PED_IN_VEHICLE_SEAT(launchVehicle, INT_TO_ENUM(VEHICLE_SEAT, iTemp))
						DELETE_PED(tempPed)
					ENDIF
				ENDFOR
				DELETE_VEHICLE(launchVehicle)
			ENDIF
		ENDIF
	ENDIF
	
	SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, TRUE)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
	
	IF BJ_GET_ARG_DROPOFF_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
//	AND NOT DOES_ENTITY_EXIST(extraVehicle)
		BJ_SETUP_DROPOFF_HELI(bjArgs, extraVehicle, eJumpID)
	ENDIF
	
	IF BJ_GET_ARG_EXIT_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
		BJ_SETUP_EXIT_VEHICLE(bjArgs, exitVehicle)
	ENDIF
	
	// Create target vehicle, put on path, etc.
	curModel = BJ_GET_ARG_TARGET_MODEL(bjArgs)
	IF BJ_IS_MODEL_TRAIN(curModel)
		DELETE_ALL_TRAINS() 
		SET_RANDOM_TRAINS(FALSE)
//		targetVehicle = CREATE_MISSION_TRAIN(6, BJ_GET_ARG_TARGET_VEHICLE_CUTSCENE_POSITION(bjArgs), TRUE)
//		IF NOT IS_ENTITY_DEAD(targetVehicle)
//			SET_TRAIN_SPEED(targetVehicle, 20.0)
//		ENDIF
		
	ELIF curModel <> DUMMY_MODEL_FOR_SCRIPT
		PRINTLN(">>>>>about to clear area...")
		CLEAR_AREA(BJ_GET_ARG_TARGET_POSITION(bjArgs, 0), 100, TRUE)
		PRINTLN(">>>>>area should have been cleared!")
		
		IF DOES_ENTITY_EXIST(targetVehicle) AND IS_ENTITY_DEAD(targetVehicle)
			DELETE_VEHICLE(targetVehicle)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(targetVehicle)
			targetVehicle = CREATE_VEHICLE(curModel, BJ_GET_ARG_TARGET_POSITION(bjArgs, 0), BJ_GET_ARG_TARGET_HEADING(bjArgs))
			IF BJ_IS_MODEL_TRUCK(BJ_GET_ARG_TARGET_MODEL(bjArgs))
				SET_VEHICLE_COLOUR_COMBINATION(targetVehicle, 11)
			ELIF BJ_IS_MODEL_BOAT(BJ_GET_ARG_TARGET_MODEL(bjArgs))
				SET_VEHICLE_EXTRA(targetVehicle, 1, TRUE)
				SET_VEHICLE_COLOUR_COMBINATION(targetVehicle, 6)
				SET_VEHICLE_LIGHTS(targetVehicle, FORCE_VEHICLE_LIGHTS_ON)
				SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200.0)
				SET_ENTITY_LOD_DIST(targetVehicle, 1000)
			ENDIF
			SET_ENTITY_LOAD_COLLISION_FLAG(targetVehicle, TRUE)
		ELIF NOT IS_ENTITY_DEAD(targetVehicle)
			SET_VEHICLE_FIXED(targetVehicle)
			SET_ENTITY_COORDS(targetVehicle, BJ_GET_ARG_TARGET_POSITION(bjArgs, 0))
			SET_ENTITY_HEADING(targetVehicle, BJ_GET_ARG_TARGET_HEADING(bjArgs))
		ENDIF
		
		IF BJ_IS_MODEL_TRUCK(BJ_GET_ARG_TARGET_MODEL(bjArgs))
			IF DOES_ENTITY_EXIST(targetFlatbed) AND IS_ENTITY_DEAD(targetFlatbed)
				DELETE_VEHICLE(targetFlatbed)
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(targetFlatbed)
				targetFlatbed = CREATE_VEHICLE(TRFLAT, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetVehicle, <<0.0, -10.0, 0.0>>), BJ_GET_ARG_TARGET_HEADING(bjArgs))
				SET_ENTITY_LOAD_COLLISION_FLAG(targetFlatbed, TRUE)
				SET_ENTITY_LOD_DIST(targetFlatbed, 2000)
			ELIF NOT IS_ENTITY_DEAD(targetFlatbed)
				SET_VEHICLE_FIXED(targetFlatbed)
			ENDIF
			
			IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(targetVehicle)
				ATTACH_VEHICLE_TO_TRAILER(targetVehicle, targetFlatbed)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(targetDriver) AND IS_ENTITY_DEAD(targetDriver)
			DELETE_PED(targetDriver)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(targetDriver)
			targetDriver = CREATE_PED_INSIDE_VEHICLE(targetVehicle, PEDTYPE_MISSION, targetDriverModel, VS_DRIVER)
			SET_ENTITY_AS_MISSION_ENTITY(targetDriver)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(targetDriver, TRUE)
			BJ_SET_DRIVER_COMPONENTS_BY_ID(targetDriver, eJumpID)
			
			IF eJumpID = BJJUMPID_HARBOR
				ADD_PED_FOR_DIALOGUE(thisConversationPedStruct, 2, targetDriver, "EX3MERC1")
			ENDIF
			
		ELSE
			SET_ENTITY_HEALTH(targetDriver, GET_ENTITY_MAX_HEALTH(targetDriver))
			RESET_PED_VISIBLE_DAMAGE(targetDriver)
			CLEAR_PED_TASKS(targetDriver)
			
			IF (NOT IS_PED_IN_VEHICLE(targetDriver, targetVehicle)) OR GET_PED_IN_VEHICLE_SEAT(targetVehicle, VS_DRIVER) <> targetDriver
				IF IS_PED_IN_ANY_VEHICLE(targetDriver, TRUE)
					SPECIAL_FUNCTION_DO_NOT_USE(targetDriver)
				ENDIF
				SET_PED_INTO_VEHICLE(targetDriver, targetVehicle, VS_DRIVER)
			ENDIF
		ENDIF
		
		SET_ENTITY_LOAD_COLLISION_FLAG(targetDriver, TRUE)
	ENDIF
	
//	IF BJ_IS_MODEL_TRUCK(curModel)
//		TASK_VEHICLE_MISSION_COORS_TARGET(targetDriver, targetVehicle, BJ_GET_ARG_TARGET_POSITION(bjArgs, iGates - 1), MISSION_GOTO, 2.5, DRIVINGMODE_AVOIDCARS, 4.0, -1)
//	ENDIF
	
	// Change the player's clothing.
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 5, 0) 		
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 1, 0)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0) 
	ENDIF
ENDPROC

PROC BJ_START_DROPOFF_HELI_SEQ(VEHICLE_INDEX &thisVeh, BOOL bTakeOffNow = FALSE)
	SEQUENCE_INDEX mySeq
	PED_INDEX motoHeliPilot
	VECTOR vMyCoords, vDest
	FLOAT fSpeed
	
	IF NOT IS_VEHICLE_DRIVEABLE(thisVeh)
		EXIT
	ENDIF
	
	vMyCoords = GET_ENTITY_COORDS(thisVeh)
	fSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(thisVeh)
	motoHeliPilot = GET_PED_IN_VEHICLE_SEAT(thisVeh)
	FREEZE_ENTITY_POSITION(thisVeh, FALSE)
	OPEN_SEQUENCE_TASK(mySeq)
		IF NOT bTakeOffNow
			TASK_VEHICLE_MISSION_COORS_TARGET(NULL, thisVeh, GET_ENTITY_COORDS(thisVeh), MISSION_GOTO, 0.0, DRIVINGMODE_AVOIDCARS, 2.0, 0.0)
		ENDIF
		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, thisVeh, vMyCoords + 5.0 * GET_ENTITY_FORWARD_VECTOR(thisVeh) + <<0, 0, 20>>, MISSION_GOTO, 0.3 * fSpeed, DRIVINGMODE_AVOIDCARS, 5, 10)
		vDest = 1000.0 * GET_ENTITY_FORWARD_VECTOR(thisVeh)
		vDest.z = 200.0
		TASK_VEHICLE_MISSION_COORS_TARGET(NULL, thisVeh, vMyCoords + vDest, MISSION_GOTO, fSpeed, DRIVINGMODE_PLOUGHTHROUGH, 15, 10)
	CLOSE_SEQUENCE_TASK(mySeq)
	CLEAR_PED_TASKS(motoHeliPilot)
	TASK_PERFORM_SEQUENCE(motoHeliPilot, mySeq)
	CLEAR_SEQUENCE_TASK(mySeq)
ENDPROC

PROC BJ_HIDE_HUD_AND_DISABLE_CONTROLS_EVERY_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ACCURATE_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
ENDPROC

// Restore the radio overlay when it's safe.
PROC BJ_UPDATE_RADIO(structTimer& radioTimer)
	IF IS_TIMER_STARTED(radioTimer) AND GET_TIMER_IN_SECONDS(radioTimer) > 7.5
		CANCEL_TIMER(radioTimer)
	ENDIF
ENDPROC

PROC BJ_DISPLAY_COURSE_NAME()
	TEXT_LABEL_23 temp
	INT iJumpID = g_savedGlobals.sBasejumpData.iLaunchRank
	UPDATE_MISSION_NAME_DISPLAYING(temp, FALSE, FALSE, FALSE, FALSE, iJumpID)
ENDPROC

PROC BJ_SET_PLAYER_UNARMED(WEAPON_TYPE &playerWeapon)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWeapon)
		IF playerWeapon <> WEAPONTYPE_INVALID
		AND playerWeapon <> WEAPONTYPE_UNARMED
		AND playerWeapon <> WEAPONTYPE_ELECTRIC_FENCE
		AND playerWeapon <> GADGETTYPE_PARACHUTE
		AND playerWeapon <> WEAPONTYPE_OBJECT
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, INFINITE_AMMO)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GUN_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	ENDIF
ENDPROC

// Wait for cutscene to finish and move on.
FUNC BOOL BJ_UPDATE_CUTSCENE(BJ_ARGS& bjArgs, BJ_JUMP_ID eJumpID, VEHICLE_INDEX& vehDropoff, BJ_CAMERA_SET& cameraSet, BJ_INPUT& input, structTimer& camTimer, structTimer& inputTimer, structTimer& tmrDropoff, structTimer& tmrAnticipation, INT iAnticipation, BOOL& bFlyaway, BOOL& bSkip, BOOL bDebugSkip)
	IF IS_CAM_INTERPOLATING(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM))
		RESTART_TIMER_NOW(camTimer)
	ENDIF
	
	// See if the dropoff pilot should fly away.
	IF NOT bFlyaway
		IF IS_TIMER_STARTED(tmrDropoff) AND GET_TIMER_IN_SECONDS(tmrDropoff) >= BJ_GET_ARG_FLYAWAY_DELAY(bjArgs)
			BJ_START_DROPOFF_HELI_SEQ(vehDropoff)
			CANCEL_TIMER(tmrDropoff)
			bFlyaway = TRUE
		ENDIF
	ELIF IS_VEHICLE_DRIVEABLE(vehDropoff)
		SET_HELI_BLADES_SPEED(vehDropoff, 0.7)
	ENDIF
	
	// Pause after interpolation has finished, then continue.
	IF (IS_TIMER_STARTED(camTimer) AND GET_TIMER_IN_SECONDS(camTimer) > fCUTSCENE_END_WAIT)
		CANCEL_TIMER(camTimer)
		CANCEL_TIMER(inputTimer)
		RETURN TRUE
	ENDIF
	
	// Check for cutscene skip.
	IF NOT IS_TIMER_STARTED(inputTimer)
		START_TIMER_NOW(inputTimer)
	ELIF (bDebugSkip AND IS_SCREEN_FADED_IN())
	OR (GET_TIMER_IN_SECONDS(inputTimer) > fINPUT_DELAY
	AND BJ_IS_BUTTON_JUST_PRESSED(input, BJBUTTON_CUTSCENE_SKIP))
		bSkip = TRUE
		CANCEL_TIMER(camTimer)
		CANCEL_TIMER(inputTimer)
		RETURN TRUE
	ENDIF
	
	// Make sure the weapon icon doesn't show up during the cutscene.
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	
	// Have the player play an anticipation animation.
	IF IS_TIMER_STARTED(tmrAnticipation)
		// Check for helicopter
		IF BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID) <> DUMMY_MODEL_FOR_SCRIPT
			CANCEL_TIMER(tmrAnticipation)
		
		ELIF GET_TIMER_IN_SECONDS(tmrAnticipation) > BJ_GET_ANTICIPATION_DELAY_BY_ID(eJumpID, iAnticipation)
			SWITCH iAnticipation
				CASE 0
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "amb@world_human_hiker_standing@male@idle_a", "idle_a", NORMAL_BLEND_IN, -1.4, -1, AF_UPPERBODY | AF_SECONDARY)
				BREAK
				CASE 1
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "amb@world_human_muscle_flex@arms_at_side@base", "base", NORMAL_BLEND_IN, -1.4, -1, AF_UPPERBODY | AF_SECONDARY, 0.681) // 7.3
				BREAK
				CASE 2
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "playidles_cold", "blow_hands", NORMAL_BLEND_IN, -1.4, -1, AF_UPPERBODY | AF_SECONDARY, 0.537) // 6.8
				BREAK
			ENDSWITCH
			//TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@triathlon", "idle_a", NORMAL_BLEND_IN, -1.4, -1, AF_UPPERBODY | AF_SECONDARY)
			//TASK_PLAY_ANIM(PLAYER_PED_ID(), "mini@triathlon", "ig_2_gen_warmup_03", NORMAL_BLEND_IN, -1.4, -1, AF_UPPERBODY | AF_SECONDARY)
			CANCEL_TIMER(tmrAnticipation)
		ENDIF
	ENDIF
	
	// Print help once we're faded in.
	IF IS_SCREEN_FADED_IN()
		BJ_DISPLAY_COURSE_NAME()

	ELSE
		RESTART_TIMER_NOW(inputTimer)
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL BJ_HAS_PLAYER_LANDED(PED_PARACHUTE_LANDING_TYPE& savedLandingType, BOOL& bHasLanded, VEHICLE_INDEX &targetVehicle)
	IF IS_PED_INJURED(PLAYER_PED_ID())
		PRINTLN("player is injured! probably died. moving on.")
		RETURN TRUE
	ENDIF
	
	PED_INDEX ped = PLAYER_PED_ID()
	
	//if we havent landed yet, check for it and save it off.
	IF NOT bHasLanded
		PED_PARACHUTE_LANDING_TYPE tempLandingType  = GET_PED_PARACHUTE_LANDING_TYPE(ped)
		IF tempLandingType <> PPLT_INVALID AND NOT IS_ENTITY_IN_AIR(ped)
			PRINTLN("Player landed!")
			bHasLanded = TRUE
			savedLandingType = tempLandingType
		ENDIF
	ENDIF
	
#IF IS_DEBUG_BUILD 
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
		PRINTLN("***PLAYER PARACHUTE REPORT***")
		PRINTLN("Player in air?: ", IS_ENTITY_IN_AIR(ped))
		PRINTLN("Player height above ground: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(ped))
		PRINTLN("Player is ragdoll? (1 for yes): ", IS_PED_RAGDOLL(ped))
		PRINTLN("Player in water? (1 for yes): ", IS_ENTITY_IN_WATER(ped))
		PRINTLN("Player Parachute state is: ", GET_STRING_FROM_PED_PARACHUTE_STATE(GET_PED_PARACHUTE_STATE(ped)))
		PRINTLN("Player on the target vehicle? (1 for yes):", (DOES_ENTITY_EXIST(targetVehicle) AND IS_PED_ON_SPECIFIC_VEHICLE(ped, targetVehicle)))
		PRINTLN("*****************************")
	ENDIF 
#ENDIF
	
	RETURN  IS_ENTITY_IN_WATER(PLAYER_PED_ID()) 
			OR (GET_PED_PARACHUTE_STATE(ped) = PPS_LANDING AND (NOT IS_ENTITY_IN_AIR(ped)))
			OR (GET_PED_PARACHUTE_STATE(ped) = PPS_INVALID AND (NOT IS_ENTITY_IN_AIR(ped)))
//			OR ((NOT IS_ENTITY_IN_AIR(ped)) 
//			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) < 5.0) //removed for B* 1127013
			OR (DOES_ENTITY_EXIST(targetVehicle) AND (NOT IS_PED_INJURED(ped)) AND (NOT IS_ENTITY_DEAD(targetVehicle)) AND IS_PED_ON_SPECIFIC_VEHICLE(ped, targetVehicle))
ENDFUNC

//// See if the player meets conditions for having landed.
//FUNC BOOL BJ_HAS_PLAYER_LANDED(VEHICLE_INDEX& targetVehicle)
//	PED_INDEX ped = PLAYER_PED_ID()
//	
//	#IF IS_DEBUG_BUILD 
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_Z)
//		PRINTLN("***PLAYER PARACHUTE REPORT***")
//		PRINTLN("Player in air?: ", IS_ENTITY_IN_AIR(ped))
//		PRINTLN("Player height above ground: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(ped))
//		PRINTLN("Player is ragdoll? (1 for yes): ", IS_PED_RAGDOLL(ped))
//		PRINTLN("Player in water? (1 for yes): ", IS_ENTITY_IN_WATER(ped))
//		PRINTLN("Player Parachute state is: ", GET_STRING_FROM_PED_PARACHUTE_STATE(GET_PED_PARACHUTE_STATE(PLAYER_PED_ID())))
//		PRINTLN("Player on the target vehicle? (1 for yes):", (DOES_ENTITY_EXIST(targetVehicle) AND IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), targetVehicle)))
//		PRINTLN("*****************************: ", IS_ENTITY_IN_WATER(ped))
//		
//	ENDIF 
//	#ENDIF
//	IF IS_PED_RAGDOLL(ped)
//		IF NOT IS_ENTITY_IN_AIR(ped)
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	RETURN  IS_ENTITY_IN_WATER(PLAYER_PED_ID()) 
//			OR GET_PED_PARACHUTE_STATE(ped) = PPS_LANDING
//			OR (GET_PED_PARACHUTE_STATE(ped) = PPS_INVALID AND NOT IS_ENTITY_IN_AIR(ped))
////			OR ((NOT IS_ENTITY_IN_AIR(ped)) 
////			AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) < 5.0) //removed for B* 1127013
//			OR (DOES_ENTITY_EXIST(targetVehicle) AND IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), targetVehicle))
//ENDFUNC

/// PURPOSE:
///    Returns true if player is 2.0m or more below the vector passed in. 
///    
FUNC BOOL BJ_IS_PLAYER_BELOW_POSITION(VECTOR vComparePosition)
	VECTOR vPlayerPosition
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF vPlayerPosition.z < (vComparePosition.z - 2.0)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC 

FUNC BOOL BJ_HAS_PLAYER_DITCHED_PARACHUTE()
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND (GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_LANDING OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID)
	AND GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) > 50.0
	AND NOT GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays 
PROC BJ_PLAY_CHECKPOINT_FEEDBACK(INT &iGateSoundID, BOOL bGateCleared)
	iGateSoundID = GET_SOUND_ID()
	IF bGateCleared
		PLAY_SOUND_FRONTEND(iGateSoundID, "CHECKPOINT_NORMAL", "HUD_MINI_GAME_SOUNDSET")
	ELSE

		PRINT_NOW("BJ_MISSED", DEFAULT_GOD_TEXT_TIME, 1)
		
		PLAY_SOUND_FRONTEND(iGateSoundID, "CHECKPOINT_MISSED", "HUD_MINI_GAME_SOUNDSET")
	ENDIF
ENDPROC

// Give the helicopter a gentle motion.
PROC BJ_UPDATE_HELI_MOTION(VEHICLE_INDEX heliIndex, VECTOR vBasePos)
	FLOAT fXOffset = fHELI_SWAY_MAG * SIN(fHELI_SWAY_FREQ * 0.5 * TO_FLOAT(GET_GAME_TIMER()) + 1.5)
	FLOAT fYOffset = fHELI_SWAY_MAG * SIN(fHELI_SWAY_FREQ * 0.4 * TO_FLOAT(GET_GAME_TIMER()) + 3.0)
	FLOAT fZOffset = fHELI_SWAY_MAG * SIN(fHELI_SWAY_FREQ * 1.0 * TO_FLOAT(GET_GAME_TIMER()))
	
	IF NOT IS_ENTITY_DEAD(heliIndex)
		SET_ENTITY_COORDS(heliIndex, vBasePos + <<fXOffset, fYOffset, fZOffset>>)
	ENDIF
ENDPROC

PROC BJ_CALCULATE_GUTTER_ICON_POSITION(VECTOR vTargetVector, FLOAT &thisX, FLOAT &thisY)
	CONST_FLOAT SCREEN_CENTER 0.5
	CONST_FLOAT SCREEN_SAFE_RADIUS 0.75
	
	FLOAT XScreenPos, YScreenPos
	VECTOR tempVec

	IF NOT ARE_VECTORS_EQUAL(vTargetVector, <<0,0,0>>) 
		GET_HUD_SCREEN_POSITION_FROM_WORLD_POSITION(vTargetVector, XScreenPos, YScreenPos)
	ENDIF
	
	//Keep the icon within bounds specified by a radiusYScreenPos
	//first, convert from screen coords to a vector (origin is at the center of the screen (SCREEN_CENTER_X, SCREEN_CENTER_Y)
	
	//if on the right half of the screen
	IF XScreenPos >= SCREEN_CENTER
		tempVec.x = XScreenPos - SCREEN_CENTER
	//else on the left half of the screen
	ELSE
		tempVec.x = SCREEN_CENTER - XScreenPos
	ENDIF
	
	//If on the bottom half of the screen
	IF YScreenPos >= SCREEN_CENTER
		tempVec.y = YScreenPos - SCREEN_CENTER
	//else on the top half of the screen
	ELSE
		tempVec.y = SCREEN_CENTER - YScreenPos
	ENDIF
	
	tempVec.z = 0.0
	
	//normalize vector so we just have the direction
	NORMALISE_VECTOR(tempVec)
	
	//mulitply the magnitude of our direction by our set radius
	tempVec.x *= SCREEN_SAFE_RADIUS
	tempVec.y *= SCREEN_SAFE_RADIUS

	//Convert back to screen coords
	
	//if on the right half of the screen
	IF XScreenPos >= SCREEN_CENTER
		XScreenPos = SCREEN_CENTER + tempVec.x
	//else on the left half of the screen
	ELSE
		XScreenPos = SCREEN_CENTER - tempVec.x
	ENDIF
	
	//If on the bottom half of the screen
	IF YScreenPos >= SCREEN_CENTER
		 YScreenPos = SCREEN_CENTER + tempVec.y
	//else on the top half of the screen
	ELSE
		YScreenPos = SCREEN_CENTER - tempVec.y
	ENDIF
	
	thisX = XScreenPos
	thisY = YScreenPos
	
ENDPROC

PROC BJ_DRAW_GUTTER_ICON(VECTOR vTargetPos, FLOAT& fLastScreenPosX, FLOAT& fLastScreenPosY, FLOAT& fLastGutterRotation)
	
	FLOAT tempx, tempy
	//check to see if the target is already on the screen.
	IF GET_SCREEN_COORD_FROM_WORLD_COORD(vTargetPos, tempx, tempy)
//		EXIT
	ENDIF
	
	FLOAT fScreenPosX, fScreenPosY, fGutterRotation = 0
	FLOAT fBJGutterHeight = 0.05, fBJGutterWidth = 0.05
	FLOAT fAspectRatio
	INT iResX, iResY
	
	BJ_CALCULATE_GUTTER_ICON_POSITION(vTargetPos, fScreenPosX, fScreenPosY)
	
	IF fScreenPosX <> 0 AND fScreenPosY <> 0
		IF fScreenPosX >= 0.7999
			fGutterRotation = 90
		ELIF fScreenPosX <= 0.2
			fGutterRotation = -90
		ELIF fScreenPosY <= 0.2
			fGutterRotation = 0
		ELIF fScreenPosY >= 0.7999
			fGutterRotation = 180
		ELSE
			//no gutter rotation to calculate	
		ENDIF
		
		GET_SCREEN_RESOLUTION(iResX, iResY)
		fAspectRatio = TO_FLOAT(iResX) / TO_FLOAT(iResY)
		
		IF fGutterRotation = 0 OR fGutterRotation = 180
			fBJGutterHeight = 0.05 * 16.0 / 9.0
			fBJGutterWidth = 0.05 / fAspectRatio
		ELSE
			fBJGutterHeight = 0.05
			fBJGutterWidth = 0.05 * ((16.0/9.0) / fAspectRatio)
//			fAspectRatio = fBJGutterWidth
//			fBJGutterWidth = fBJGutterHeight
//			fBJGutterHeight = fAspectRatio
		ENDIF
		
		
		IF fGutterRotation <> fLastGutterRotation
		OR (fLastScreenPosX = 0.0 AND fLastScreenPosY = 0.0)
		OR (ABSF(fScreenPosX - fLastScreenPosX) < 0.04 AND ABSF(fScreenPosY - fLastScreenPosY) < 0.04)
			DRAW_SPRITE("basejumping", "Arrow_Pointer", fScreenPosX, fScreenPosY, fBJGutterWidth, fBJGutterHeight, fGutterRotation, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, iTARGET_ALPHA)
		ENDIF
		
		fLastGutterRotation = fGutterRotation
	ENDIF
	
	fLastScreenPosX = fScreenPosX
	fLastScreenPosY = fScreenPosY
ENDPROC

FUNC BOOL BJ_IS_MOVING_TARGET_IN_FRONT_OF_PLAYER(VEHICLE_INDEX targetVehicle)
	
	VECTOR vPlayerPosition 
	VECTOR vTargetVehiclePos		// targetVehicle position
	VECTOR TrashVector
	VECTOR vPlayerToVehicle 	// vector from targetVehicle to player position
	VECTOR vPlayerUnitVec	// unit vector pointing in the direction the player is pointing
	FLOAT fDotProduct
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	// get positions
	IF IS_VEHICLE_DRIVEABLE(targetVehicle)
		GET_ENTITY_MATRIX(targetVehicle, vPlayerUnitVec, TrashVector, TrashVector, vTargetVehiclePos)
		//put vehicle coord in vTargetVehiclePos and put the unit vector of its direction into vec 2
	ENDIF

	// get vec from targetVehicle to the player
	vPlayerToVehicle = vPlayerPosition - vTargetVehiclePos

	// take the z out
	vPlayerToVehicle.z = 0.0
	vPlayerUnitVec.z = 0.0

	// calculate dot product of vPlayerToVehicle and vPlayerUnitVec
	fDotProduct = DOT_PRODUCT(vPlayerToVehicle, vPlayerUnitVec)
	IF (fDotProduct < 0.0) 
		RETURN TRUE 
	ENDIF
	RETURN FALSE
	
ENDFUNC

PROC BJ_UPDATE_MOVING_TARGET_SPEED(VEHICLE_INDEX &targetVehicle)

	VECTOR vPlayerSpeedVect 
	VECTOR vPlayerPosition 
	VECTOR vTargetPosition
	VECTOR vPlayerUnitVec
	VECTOR vPlayerToVehicle
	VECTOR TrashVector
	VECTOR vOldTargetVelocity
	FLOAT fTempPlayerSpeed
	FLOAT fIdealTargetSpeed
	FLOAT fDistFromTarget
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
		IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO(PLAYER_PED_ID()))
			vPlayerSpeedVect = GET_ENTITY_SPEED_VECTOR(GET_ENTITY_ATTACHED_TO(PLAYER_PED_ID()))
		ELSE
			vPlayerSpeedVect = GET_ENTITY_SPEED_VECTOR(PLAYER_PED_ID(), TRUE)
		ENDIF
		fTempPlayerSpeed = vPlayerSpeedVect.y
		
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		vPlayerPosition.z = 0
	ENDIF

	IF DOES_ENTITY_EXIST(targetVehicle) AND IS_VEHICLE_DRIVEABLE(targetVehicle)
		vTargetPosition = GET_ENTITY_COORDS(targetVehicle)
		vTargetPosition.z = 0
		vOldTargetVelocity = GET_ENTITY_VELOCITY(targetVehicle)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(targetVehicle)
		GET_ENTITY_MATRIX(targetVehicle, vPlayerUnitVec, TrashVector, TrashVector, TrashVector)
		vPlayerUnitVec.z = 0.0
	ENDIF
	

	// get vec from thisVehicle to the player
	vPlayerToVehicle = vPlayerPosition - vTargetPosition
	vPlayerToVehicle.z = 0.0

	// calculate dot product of vPlayerToVehicle and vPlayerUnitVec
	fDistFromTarget = ABSF(DOT_PRODUCT(vPlayerToVehicle, vPlayerUnitVec))
	
	IF BJ_IS_MOVING_TARGET_IN_FRONT_OF_PLAYER(targetVehicle)
		//slow target down to let the player catch up
		fIdealTargetSpeed = CLAMP((fTempPlayerSpeed - (fDistFromTarget/fTempPlayerSpeed)), 2.0, 3.0)
//		#IF IS_DEBUG_BUILD
//			BJ_DEBUG_DRAW_LITERAL_STRING("IN FRONT OF PLAYER", 1, HUD_COLOUR_RED)
//		#ENDIF
		
	ELSE
		fIdealTargetSpeed = CLAMP((fTempPlayerSpeed + (fDistFromTarget/fTempPlayerSpeed)), 3.0, 10.0)
//		#IF IS_DEBUG_BUILD
//			BJ_DEBUG_DRAW_LITERAL_STRING("BEHIND PLAYER", 1, HUD_COLOUR_RED)		
//		#ENDIF
		//speed target up to catch up to the player
	ENDIF
	
	FLOAT fNewTargetSpeed = CLAMP(COSINE_INTERP_FLOAT(vOldTargetVelocity.y, fIdealTargetSpeed, 0.5), vOldTargetVelocity.y, fTempPlayerSpeed)
	vOldTargetVelocity.y = COSINE_INTERP_FLOAT(vOldTargetVelocity.y, fIdealTargetSpeed, 0.5)
	
	IF DOES_ENTITY_EXIST(targetVehicle) AND IS_VEHICLE_DRIVEABLE(targetVehicle) AND vOldTargetVelocity.y > 2.0 AND vOldTargetVelocity.y < GET_VEHICLE_ESTIMATED_MAX_SPEED(targetVehicle)
//		#IF IS_DEBUG_BUILD
//			BJ_DEBUG_DRAW_LITERAL_STRING_WITH_FLOAT("OLD SPEED :", vOldTargetVelocity.y, 2)  
//			BJ_DEBUG_DRAW_LITERAL_STRING_WITH_FLOAT("IDEAL SPEED:", fIdealTargetSpeed, 3)
//			BJ_DEBUG_DRAW_LITERAL_STRING_WITH_FLOAT("NEW SPEED:", fNewTargetSpeed, 4) 
//		#ENDIF

		SET_DRIVE_TASK_MAX_CRUISE_SPEED(GET_PED_IN_VEHICLE_SEAT(targetVehicle), fNewTargetSpeed)
	ENDIF

ENDPROC

FUNC BOOL BJ_IS_PLAYER_ON_TARGET_VEHICLE(VEHICLE_INDEX vehTarget, PED_INDEX pedDriver)
	VEHICLE_INDEX tempTrailer, tempCarriage
	VECTOR vTargetVehiclePos
	VECTOR vPlayerPos
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND DOES_ENTITY_EXIST(vehTarget) 
		AND IS_VEHICLE_DRIVEABLE(vehTarget)
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
		PRINTLN("Checking if vehicle is a train")
		IF BJ_IS_MODEL_TRAIN(GET_ENTITY_MODEL(vehTarget))
			PRINTLN("Checking if player is touching a section of the train")
			INT i = 0
			REPEAT 6 i
				PRINTLN("Checking carriage #", i)
				tempCarriage = GET_TRAIN_CARRIAGE(vehTarget, i)
				IF DOES_ENTITY_EXIST(tempCarriage) 
					vTargetVehiclePos = GET_ENTITY_COORDS(vehTarget)
					PRINTLN("Checking if player is touching carriage #", i)
					IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), tempCarriage)
//					OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), tempCarriage) 
//						IF vTargetVehiclePos.z < vPlayerPos.z
							PRINTLN("Player is on a train!")
							RETURN TRUE
//						ELSE
//							PRINTLN("Player z pos is lower than train z pos!")
//						ENDIF
					ELSE
						PRINTLN("Player isn't touching carriage #", i)
					ENDIF
				ELSE
					PRINTLN("Carriage #", i, " doesn't exist!")
				ENDIF
			ENDREPEAT
		ELSE
			PRINTLN("Vehicle is not a train")
		ENDIF
		
	
		PRINTLN("Checking if player is touching vehicle")
		IF DOES_ENTITY_EXIST(vehTarget)
			GET_VEHICLE_TRAILER_VEHICLE(vehTarget, tempTrailer)
			IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), vehTarget)
			OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehTarget) 
			OR (DOES_ENTITY_EXIST(pedDriver) AND (NOT IS_ENTITY_DEAD(pedDriver))
			AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedDriver))
				IF vTargetVehiclePos.z < vPlayerPos.z
					RETURN TRUE
				ELSE
					PRINTLN("Player z pos is lower than vehicle z pos!")
				ENDIF
			ELSE
				PRINTLN("Player not touching vehicle entity!")
			ENDIF
		ELSE
			PRINTLN("Vehicle is dead!")
		ENDIF
		
		PRINTLN("Checking if player is touching trailer")
		IF DOES_ENTITY_EXIST(tempTrailer) 
			IF NOT IS_ENTITY_DEAD(tempTrailer) 
				IF IS_PED_ON_SPECIFIC_VEHICLE(PLAYER_PED_ID(), tempTrailer)
					RETURN TRUE
				ELIF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), tempTrailer)
					VECTOR vPlayerOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(tempTrailer, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					vPlayerOffset.x = FMAX(ABSF(vPlayerOffset.x) - 1.305, 0.0) // Half-width of flatbed
					IF vPlayerOffset.y >= 0.0
						vPlayerOffset.y = FMAX(vPlayerOffset.y - 5.98, 0.0) // Center to front
					ELSE
						vPlayerOffset.y = FMAX(-vPlayerOffset.y - 6.21, 0.0) // Center to back
					ENDIF
					PRINTLN("vPlayerOffset.y is ", vPlayerOffset.y)
					PRINTLN("vPlayerOffset.x is ", vPlayerOffset.x)
					IF vPlayerOffset.y = 0 AND vPlayerOffset.x = 0
						RETURN TRUE
					ELSE
						PRINTLN("Player is only touching the trailer and is outside of the offset")
					ENDIF
				ELSE
					PRINTLN("Player not touching Trailer entity!")
				ENDIF
			ELSE
				PRINTLN("Trailer is dead!")
			ENDIF
		ELSE
			PRINTLN("Trailer doesn't exist!")
		ENDIF


//		IF DOES_ENTITY_EXIST(vehTarget) 
//			AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehTarget) 
//			AND vTargetVehiclePos.z < vPlayerPos.z
//			RETURN TRUE
//		ELIF DOES_ENTITY_EXIST(tempTrailer) 
//			AND NOT IS_ENTITY_DEAD(tempTrailer) 
//			AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), tempTrailer)
//			AND vTargetVehiclePos.z < vPlayerPos.z
//			RETURN TRUE
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL BJ_IS_PLAYER_CLOSE_TO_TARGET(VECTOR vTarget)
	FLOAT closeDistance = 300 //vdist
	FLOAT closeDistanceSquared = closeDistance*closeDistance
	FLOAT currentDistanceSquared = 0
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		currentDistanceSquared = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTarget)
		IF currentDistanceSquared < closeDistanceSquared
//			PRINTLN("Player is close!")
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
		
ENDFUNC

FUNC BOOL BJ_ARE_AMB_PEDS_CLOSE_TO_TARGET(VECTOR vTarget)
	vTarget = vTarget
	RETURN TRUE
ENDFUNC

FUNC INT IS_PED_IN_ARRAY(PED_INDEX thisPed, PED_INDEX &thisPedArray[])
	IF DOES_ENTITY_EXIST(thisPed)
		INT i = 0
		
		REPEAT COUNT_OF(thisPedArray) i
			IF DOES_ENTITY_EXIST(thisPedArray[i])
				IF thisPed = thisPedArray[i]
					RETURN i
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN -1
ENDFUNC

PROC BJ_COPY_PED_ARRAY(PED_INDEX &arrayToKeep[], PED_INDEX &arrayToCopy[])
	IF COUNT_OF(arrayToKeep) = COUNT_OF(arrayToCopy)
		INT i = 0
		REPEAT COUNT_OF(arrayToKeep) i
			arrayToKeep[i] = arrayToCopy[i]
		ENDREPEAT
	ELSE
		PRINTLN("WARNING! Trying to copy an array but its the wrong size!!!!!")
	ENDIF
ENDPROC

//PROC BJ_CREATE_ONLOOKERS(VECTOR vTarget, VECTOR vPlayerCoords)
//	//create a bunch of peds near the landing taget so theyre all facing the player.
//	INT i = 0
//	FLOAT x, y
//	PED_INDEX thisTempPed
//	PED_TYPE thisPedType
//
//	REPEAT 8 i 
//		x = GET_RANDOM_FLOAT_IN_RANGE(-10, 10)
//		y = GET_RANDOM_FLOAT_IN_RANGE(-10, 10)
//		IF GET_RANDOM_FLOAT_IN_RANGE()%2 = 0
//			thisPedType = PEDTYPE_CIVMALE
//		ELSE
//			thisPedType = PEDTYPE_CIVFEMALE
//		ENDIF
//		WHILE NOT CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
//		WAIT(0)
//		ENDWHILE
//		thisTempPed = CREATE_PED(thisPedType, GET_RANDOM_PED_MODEL(GET_RANDOM_BOOL()), vTarget + <<x, y, 0>>, GET_HEADING_BETWEEN_VECTORS_2D(vTarget, vPlayerCoords))
//		
//		IF NOT IS_PED_INJURED(thisTempPed)
//			VECTOR mrvect = GET_ENTITY_COORDS(thisTempPed) 
//			PRINTLN("ped exists! coords are... ", mrvect)
//		ENDIF
//		
//		IF NOT IS_PED_INJURED(thisTempPed) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
//			TASK_LOOK_AT_ENTITY(thisTempPed, PLAYER_PED_ID(), -1)
//			SET_PED_KEEP_TASK(thisTempPed, TRUE)
//		ENDIF
//	ENDREPEAT
//ENDPROC


//PROC BJ_UPDATE_ONLOOKERS(PED_INDEX &onlookerArray[], VECTOR vTarget)
////	INT i = 0
////	INT k = 0
////	INT numPeds = 0
//	FLOAT Range = 200
//	BOOL bScanRandomPeds = TRUE
//	BOOL bScanMissionPeds = FALSE
//	BOOL bCheckPlayerPeds = FALSE
//	BOOL bReturnPedsWithScriptedTasks = FALSE
//	PED_TYPE ExclusionPedType = PEDTYPE_INVALID
//	PED_INDEX ClosestPedIndex
//	//PED_INDEX pedArray[iBJ_MAX_ONLOOKERS]
//	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(2.0)
//	
//	
//	GET_CLOSEST_PED(vTarget, Range, bScanRandomPeds, bScanMissionPeds, ClosestPedIndex, bCheckPlayerPeds, bReturnPedsWithScriptedTasks, ExclusionPedType)
//	
//	IF DOES_ENTITY_EXIST(ClosestPedIndex)
//		IF NOT IS_PED_INJURED(ClosestPedIndex)
////			PRINTLN("ClosestPed is alive. Grabbing more peds!")
//			GET_PED_NEARBY_PEDS(ClosestPedIndex, pedArray, ExclusionPedType)
//		ENDIF
//	ENDIF
//	
//	IF DOES_ENTITY_EXIST(onlookerArray[0])
//		IF DOES_ENTITY_EXIST(pedArray[0])
//			
//		ENDIF
//	ELSE
//		IF DOES_ENTITY_EXIST(pedArray[0])
//			BJ_COPY_PED_ARRAY(onlookerArray, pedArray)
//		ENDIF
//	ENDIF
//	
//	SEQUENCE_INDEX seq
//	IF DOES_ENTITY_EXIST(ClosestPedIndex)
//		IF NOT IS_PED_INJURED(ClosestPedIndex)	
//			CLEAR_PED_TASKS_IMMEDIATELY(ClosestPedIndex)
//			OPEN_SEQUENCE_TASK(seq)
//				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 0)
//				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO | SLF_EXTEND_YAW_LIMIT)	
//				TASK_PLAY_ANIM(NULL, "oddjobs@suicide", "bystander_pointhigh")
//			CLOSE_SEQUENCE_TASK(seq)
//			CLEAR_PED_TASKS_IMMEDIATELY(ClosestPedIndex)
//			TASK_PERFORM_SEQUENCE(ClosestPedIndex, seq)
//			CLEAR_SEQUENCE_TASK(seq)
//		ENDIF
//	ENDIF 
//	
////	NATIVE PROC TASK_GO_STRAIGHT_TO_COORD(PED_INDEX PedIndex, VECTOR VecCoors,  FLOAT MoveBlendRatio, INT Time = DEFAULT_TIME_BEFORE_WARP, FLOAT FinalHeading = DEFAULT_NAVMESH_FINAL_HEADING)
//	
//ENDPROC

PROC BJ_EXIT_ONLOOK_STATE(PED_INDEX& pedOnlooker)
	CLEAR_PED_TASKS(pedOnlooker)
	TASK_PLAY_ANIM(pedOnlooker, "oddjobs@basejump@", "ped_a_exit")
	pedOnlooker = NULL
ENDPROC

PROC BJ_UPDATE_ONLOOKERS(structTimer& tmrOnlookerUpdate, PED_INDEX& pedsOnlooking[])
	PED_INDEX pedsNearby[iBJ_MAX_ONLOOKERS]
	SEQUENCE_INDEX seqOnlooker
	VECTOR vPedToPlayer
	FLOAT fHorizontalDist
	INT iNearbyPeds
	INT iNearbyCounter
	INT iOnlookingCounter
	BOOL bFoundSpot
	
	// Only update once per second.
	IF NOT IS_TIMER_STARTED(tmrOnlookerUpdate)
		START_TIMER_NOW(tmrOnlookerUpdate)
	ENDIF
	
	IF GET_TIMER_IN_SECONDS(tmrOnlookerUpdate) < 1.0
		EXIT
	ENDIF
	
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	// Get all peds near the player.
	iNearbyPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedsNearby)
	
	// Outro task any peds in the onlooker array who no longer qualify as an onlooker.
	REPEAT iBJ_MAX_ONLOOKERS iNearbyCounter
		IF DOES_ENTITY_EXIST(pedsOnlooking[iNearbyCounter])
		AND NOT IS_ENTITY_DEAD(pedsOnlooking[iNearbyCounter])
		AND NOT IS_PED_INJURED(pedsOnlooking[iNearbyCounter])
			IF IS_PED_IN_ARRAY(pedsOnlooking[iNearbyCounter], pedsNearby) = -1
				BJ_EXIT_ONLOOK_STATE(pedsOnlooking[iNearbyCounter])
			ELSE
				vPedToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(pedsOnlooking[iNearbyCounter])
				fHorizontalDist = vPedToPlayer.x * vPedToPlayer.x + vPedToPlayer.y * vPedToPlayer.y
				IF vPedToPlayer.z * vPedToPlayer.z < 3.0 * fHorizontalDist
					BJ_EXIT_ONLOOK_STATE(pedsOnlooking[iNearbyCounter])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Go through the new nearby array, adding any qualifiers to the onlooker array.
	IF iNearbyPeds > 0
		REPEAT iNearbyPeds iNearbyCounter
			// Make sure this is an on-foot human ped.
			IF DOES_ENTITY_EXIST(pedsNearby[iNearbyCounter]) AND (NOT IS_ENTITY_DEAD(pedsNearby[iNearbyCounter])) AND (NOT IS_PED_INJURED(pedsNearby[iNearbyCounter]))
			AND IS_PED_HUMAN(pedsNearby[iNearbyCounter])
			AND (NOT IS_PED_IN_ANY_VEHICLE(pedsNearby[iNearbyCounter], TRUE))
				// If the ped is in the onlooker array, we've already processed them this frame.
				IF IS_PED_IN_ARRAY(pedsNearby[iNearbyCounter], pedsOnlooking) = -1
					// See if the player is at the right angle above for the ped to be an onlooker.
					vPedToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(pedsNearby[iNearbyCounter])
					fHorizontalDist = vPedToPlayer.x * vPedToPlayer.x + vPedToPlayer.y * vPedToPlayer.y
					IF vPedToPlayer.z * vPedToPlayer.z > 3.0 * fHorizontalDist
						bFoundSpot = FALSE
						REPEAT COUNT_OF(pedsOnlooking) iOnlookingCounter
							IF (NOT DOES_ENTITY_EXIST(pedsOnlooking[iOnlookingCounter])) OR IS_PED_INJURED(pedsOnlooking[iOnlookingCounter])
								pedsOnlooking[iOnlookingCounter] = pedsNearby[iNearbyCounter]
								OPEN_SEQUENCE_TASK(seqOnlooker)
									TASK_PLAY_ANIM(NULL, "oddjobs@basejump@", "ped_a_intro")
									TASK_PLAY_ANIM(NULL, "oddjobs@basejump@", "ped_a_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seqOnlooker)
								TASK_PERFORM_SEQUENCE(pedsNearby[iNearbyCounter], seqOnlooker)
								CLEAR_SEQUENCE_TASK(seqOnlooker)
								iOnlookingCounter = COUNT_OF(pedsOnlooking)
								bFoundSpot = TRUE
							ENDIF
						ENDREPEAT
						
						// If the onlooker array is full, we should quit.
						IF NOT bFoundSpot
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

// Draw current gates, and optionally check for player progress.
FUNC BJ_GATE_CHECK_RESULT BJ_UPDATE_GATES(BJ_ARGS& bjArgs, VEHICLE_INDEX targetVehicle, PED_INDEX targetDriverPed, PED_INDEX &onlookerArray[], CHECKPOINT_INDEX& curCheckpoint, CHECKPOINT_INDEX& nextCheckpoint, INT& iCurGate, INT& iCurCheckpoint, INT& iGatesHit, INT iTotalGates, INT &iAccuracy, BLIP_INDEX& curGateBlip, BLIP_INDEX& nextGateBlip, INT &iGateSoundID, PED_PARACHUTE_LANDING_TYPE& savedLandingType, FLOAT& fLastScreenPosX, FLOAT& fLastScreenPosY, FLOAT& fLastGutterRotation, BOOL& bHasLanded, BOOL bCheckProgress, BOOL bAllowBlipping, FLOAT rewardFactor)
	VECTOR vPlayerFront, vPlayerSide, vPlayerUp, vPlayerPos
	VECTOR vTargetPos = BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate)
//	VECTOR vTargetVehiclePos
	VECTOR vNextTargetPos
	VECTOR vTargetToPlayer
	VECTOR vPlayerToTargetFlat
	VECTOR vPlayerForwardFlat
	VECTOR tempTargetPos
	VECTOR vTempTargetZ
	FLOAT fDistToGate
	FLOAT fFlatDistance
	FLOAT fPlayerHeight
	FLOAT fTargetZOffset
	BOOL bFinalGate = (iCurGate = iTotalGates - 1)
	BOOL bNextGate
	
	onlookerArray[0] = onlookerArray[0]
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_ENTITY_MATRIX(PLAYER_PED_ID(), vPlayerFront, vPlayerSide, vPlayerUp, vPlayerPos)
	ENDIF
	
	// Draw the final ground target.
	IF bFinalGate
		IF DOES_ENTITY_EXIST(targetVehicle) AND NOT IS_ENTITY_DEAD(targetVehicle)
			tempTargetPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(targetVehicle, BJ_GET_ARG_TARGET_VEHICLE_OFFSET(bjArgs))
			fTargetZOffset = tempTargetPos.z - vTargetPos.z //find the z offset between the target and the vehicle
			//update blip too
			IF bAllowBlipping
				IF DOES_BLIP_EXIST(curGateBlip)
					SET_BLIP_COORDS(curGateBlip, tempTargetPos)
				ELSE
					curGateBlip = ADD_BLIP_FOR_COORD(tempTargetPos)
					SET_BLIP_COLOUR(curGateBlip, BLIP_COLOUR_YELLOW)
					SET_BLIP_SCALE(curGateBlip, fCUR_BLIP_SCALE)
					SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_TGT")
				ENDIF
			ENDIF
			vTempTargetZ = BJ_GET_ARG_TARGET_POSITION(bjArgs, 0)
			tempTargetPos.z = vTempTargetZ.z + fTargetZOffset
			vTargetPos = tempTargetPos
		ELIF BJ_GET_ARG_TARGET_MODEL(bjArgs) <> DUMMY_MODEL_FOR_SCRIPT
			tempTargetPos = BJ_GET_ARG_TARGET_POSITION(bjArgs, 0)
			vTargetPos = tempTargetPos
		ELSE
			tempTargetPos = vTargetPos
		ENDIF
		
		vTargetToPlayer = vPlayerPos - vTargetPos
		
//		PRINTLN("Target position is ...",tempTargetPos)
//		VECTOR superTemp = GET_ENTITY_COORDS(targetVehicle)
//		PRINTLN("Target vehicle position is ...", superTemp)
		IF BJ_GET_ARG_TARGET_MODEL(bjArgs) = DUMMY_MODEL_FOR_SCRIPT
			DRAW_MARKER(MARKER_RING, tempTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_S, fGND_TGT_DRAW_SCALE_S, fGND_TGT_DRAW_SCALE_S>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(tempTargetPos, iTARGET_ALPHA), FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, tempTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_M, fGND_TGT_DRAW_SCALE_M, fGND_TGT_DRAW_SCALE_M>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(tempTargetPos, iTARGET_ALPHA), FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, tempTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_L, fGND_TGT_DRAW_SCALE_L, fGND_TGT_DRAW_SCALE_L>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(tempTargetPos, iTARGET_ALPHA), FALSE, FALSE)
		ENDIF
		
		IF iCurCheckpoint <> -1
			DELETE_CHECKPOINT(curCheckpoint)
			iCurCheckpoint = -1
		ENDIF
		
	// Draw the current ring target.
	ELSE
		INT iRed, iGreen, iBlue, iAlpha
		GET_HUD_COLOUR(HUD_COLOUR_NORTH_BLUE, iRed, iGreen, iBlue, iAlpha)
		
		vTargetToPlayer = vPlayerPos - vTargetPos
		vNextTargetPos = BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate + 1)
		
		DRAW_MARKER(MARKER_RING, vTargetPos, NORMALISE_VECTOR(vTargetToPlayer), <<0,0,0>>, <<fAIR_TGT_DRAW_SCALE, fAIR_TGT_DRAW_SCALE, fAIR_TGT_DRAW_SCALE>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(vTargetPos), FALSE, FALSE)
		
		// Update checkpoints if we've advanced a gate.
		IF iCurCheckpoint <> iCurGate
			IF iCurCheckpoint >= 0
				DELETE_CHECKPOINT(curCheckpoint)												
				curCheckpoint = NULL
				
				IF iCurCheckpoint < iTotalGates - 2
					DELETE_CHECKPOINT(nextCheckpoint)
					curCheckpoint = NULL
				ENDIF
			ENDIF
			
			curCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_WATER_CHEVRON_1, vTargetPos, vNextTargetPos, 9.0, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(tempTargetPos, DEFAULT, CEIL(200*(TO_FLOAT(iCHECKPOINT_ALPHA)/TO_FLOAT(iTARGET_ALPHA)))))
			
			IF iCurGate < iTotalGates - 2
				nextCheckpoint = CREATE_CHECKPOINT(CHECKPOINT_RACE_WATER_CHEVRON_1, vNextTargetPos, BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate + 2), 9.0 * fAIR_NXT_TGT_SCALE, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(tempTargetPos, DEFAULT, CEIL(200*(TO_FLOAT(iCHECKPOINT_ALPHA)/TO_FLOAT(iTARGET_ALPHA)))))
			ENDIF
			
			iCurCheckpoint = iCurGate
		ENDIF
		
		// Draw smaller version of the final gate.
		IF (iCurGate + 1 = iTotalGates - 1)
			DRAW_MARKER(MARKER_RING, vNextTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_S, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_S, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_S>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(vNextTargetPos, iTARGET_ALPHA), FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vNextTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_M, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_M, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_M>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(vNextTargetPos, iTARGET_ALPHA), FALSE, FALSE)
			DRAW_MARKER(MARKER_RING, vNextTargetPos, <<0,0,1>>, <<0,0,0>>, <<fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_L, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_L, fGND_NXT_TGT_SCALE * fGND_TGT_DRAW_SCALE_L>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(vNextTargetPos, iTARGET_ALPHA), FALSE, FALSE)
		
		// Draw smaller version of the next gate.
		ELSE
			DRAW_MARKER(MARKER_RING, vNextTargetPos, NORMALISE_VECTOR(vPlayerPos - vNextTargetPos), <<0,0,0>>, <<fAIR_NXT_TGT_SCALE * fAIR_TGT_DRAW_SCALE, fAIR_NXT_TGT_SCALE * fAIR_TGT_DRAW_SCALE, fAIR_NXT_TGT_SCALE * fAIR_TGT_DRAW_SCALE>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, MG_GET_CHECKPOINT_ALPHA(vNextTargetPos), FALSE, FALSE)
		ENDIF
			
		IF (curCheckpoint != NULL)
			SET_CHECKPOINT_RGBA(curCheckpoint, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(vTargetPos, DEFAULT, CEIL(200*(TO_FLOAT(iCHECKPOINT_ALPHA)/TO_FLOAT(iTARGET_ALPHA)))))
		ENDIF
		IF (nextCheckpoint != NULL)
			SET_CHECKPOINT_RGBA(nextCheckpoint, iRed, iGreen, iBlue, MG_GET_CHECKPOINT_ALPHA(vNextTargetPos, DEFAULT, CEIL(200*(TO_FLOAT(iCHECKPOINT_ALPHA)/TO_FLOAT(iTARGET_ALPHA)))))
		ENDIF	
	ENDIF
	
	// We're done if we just want to draw the targets.
	IF NOT bCheckProgress
		RETURN BJGATECHECK_NONE
	ENDIF
	
	IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_SKYDIVING OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		FLOAT tempx, tempy
		//check to see if the target is already on the screen.
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vTargetPos, tempx, tempy)
			IF bGutterIconDelayFlag
				BJ_DRAW_GUTTER_ICON(vTargetPos, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation)
				IF IS_TIMER_STARTED(tGutterIconDelayTimer)
					CANCEL_TIMER(tGutterIconDelayTimer)
				ENDIF
			ELSE
				IF IS_TIMER_STARTED(tGutterIconDelayTimer)
					IF GET_TIMER_IN_SECONDS_SAFE(tGutterIconDelayTimer) > 0.5
						CANCEL_TIMER(tGutterIconDelayTimer)
						bGutterIconDelayFlag = TRUE
					ENDIF
				ELSE
					START_TIMER_NOW(tGutterIconDelayTimer)	
				ENDIF
			ENDIF
		ELSE
			IF bGutterIconDelayFlag
				BJ_DRAW_GUTTER_ICON(vTargetPos, fLastScreenPosX, fLastScreenPosY, fLastGutterRotation)
				IF IS_TIMER_STARTED(tGutterIconDelayTimer)
					IF GET_TIMER_IN_SECONDS_SAFE(tGutterIconDelayTimer) > 0.5
						CANCEL_TIMER(tGutterIconDelayTimer)
						bGutterIconDelayFlag = FALSE
					ENDIF
				ELSE
					START_TIMER_NOW(tGutterIconDelayTimer)	
				ENDIF
			ELSE
				IF IS_TIMER_STARTED(tGutterIconDelayTimer)
					CANCEL_TIMER(tGutterIconDelayTimer)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		bGutterIconDelayFlag = FALSE
	ENDIF
	
	// See if the player has landed.
	IF BJ_HAS_PLAYER_LANDED(savedLandingType, bHasLanded, targetVehicle)
		IF DOES_BLIP_EXIST(curGateBlip)
			REMOVE_BLIP(curGateBlip)
		ENDIF
		IF DOES_BLIP_EXIST(nextGateBlip)
			REMOVE_BLIP(nextGateBlip)
		ENDIF
		
		// If the player landed prematurely, it's a fail.
		IF NOT bFinalGate
			RETURN BJGATECHECK_FAIL
		ENDIF
			
		fFlatDistance = SQRT(vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y)

		//if we're close enough or we're on top of the target vehicle
		IF (vTargetToPlayer.z < fGROUND_TGT_RG_VERT_ABOVE AND vTargetToPlayer.z > fGROUND_TGT_RG_VERT_BELOW AND fFlatDistance < fGROUND_TGT_RG_HORIZ) 
		OR BJ_IS_PLAYER_ON_TARGET_VEHICLE(targetVehicle, targetDriverPed)
			//Set the landing accuracy now. If we landed on the vehicle, it will get overidden with 100% later
			iAccuracy = CLAMP_INT(CEIL(100.0 * (fGROUND_TGT_RG_HORIZ - fFlatDistance) / fGROUND_TGT_RG_HORIZ) + 4, 0, 100)
			RETURN BJGATECHECK_PASS
		ELSE
			RETURN BJGATECHECK_FAIL_TOO_FAR_LANDING
		ENDIF
	ELIF BJ_HAS_PLAYER_DITCHED_PARACHUTE()
		IF GET_PLAYER_INVINCIBLE(PLAYER_ID())
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
		ENDIF
	
	// If the player is airborn and we aren't on the last gate, check the current gate.
	ELIF NOT bFinalGate
		// See if we have cleared this gate.
		fDistToGate = VMAG2(vTargetToPlayer)
		IF fDistToGate < fAIR_TGT_RANGE * fAIR_TGT_RANGE
			
			// If we have moved through the inner sphere, or the target is behind us, we can consider this done.
			IF fDistToGate < fAIR_TGT_INNER_RANGE * fAIR_TGT_INNER_RANGE
				bNextGate = TRUE
			ELSE
				IF DOT_PRODUCT(vPlayerFront, -vTargetToPlayer / SQRT(fDistToGate)) < 0.08
					bNextGate = TRUE
				ENDIF
			ENDIF
			
			IF bNextGate
				iGatesHit += 1
				BJ_PLAY_CHECKPOINT_FEEDBACK(iGateSoundID, TRUE)
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, FLOOR(iGATE_REWARD*rewardFactor))
				bInOuterCheckpointSphere = FALSE
			ENDIF
				
			// Otherwise note that we have entered the outer sphere
			IF (NOT bNextGate) AND (NOT bInOuterCheckpointSphere)
				bInOuterCheckpointSphere = TRUE
			ENDIF
			
		// See if we have no chance of clearing this gate (skydive).
		ELIF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_SKYDIVING
		OR   GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
			IF vTargetToPlayer.z < -fAIR_TGT_RANGE
				BJ_PLAY_CHECKPOINT_FEEDBACK(iGateSoundID, FALSE)
				
				bNextGate = TRUE
			ENDIF
		
		// See if we have no chance of clearing this gate (parachute).
		ELSE
			// Adjust the player's effective height based on whether he is facing the current target.
			// This will lower the player by 0m if facing the target, 64m if facing directly away, with a ramp between.
			vPlayerForwardFlat = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
			vPlayerForwardFlat.z = 0.0
			vPlayerForwardFlat = NORMALISE_VECTOR(vPlayerForwardFlat)
			vPlayerToTargetFlat = <<-vTargetToPlayer.x, -vTargetToPlayer.y, 0.0>>
			vPlayerToTargetFlat = NORMALISE_VECTOR(vPlayerToTargetFlat)
			fPlayerHeight = vPlayerPos.z - 32.0 + 32.0 * DOT_PRODUCT_XY(vPlayerForwardFlat, vPlayerToTargetFlat)
			
			// Get the distance in the xy plane and scale the previous height adjustment.
			fFlatDistance = SQRT(vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y)
			IF vPlayerPos.z - fPlayerHeight < fFlatDistance
				fPlayerHeight -= fNEXT_TGT_DIST_DROPOFF * (fPlayerHeight - vPlayerPos.z + fFlatDistance)
				
				// Make sure this new height is high enough to clear the current gate.
				IF fPlayerHeight < vTargetPos.z - fAIR_TGT_RANGE
					BJ_PLAY_CHECKPOINT_FEEDBACK(iGateSoundID, FALSE)
					bNextGate = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// Check to see if we have moved outside the outer sphere
		IF bInOuterCheckpointSphere
			// If we have, reward
			IF VMAG2(vTargetToPlayer) >= fAIR_TGT_RANGE * fAIR_TGT_RANGE 
				iGatesHit += 1
				BJ_PLAY_CHECKPOINT_FEEDBACK(iGateSoundID, TRUE)
				CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, FLOOR(iGATE_REWARD*rewardFactor))
				bNextGate = TRUE
				bInOuterCheckpointSphere = FALSE
			ENDIF
		ENDIF
		
		// Go to the next gate.
		IF bNextGate
			// Set the next blip as the current.
			iCurGate += 1
			IF DOES_BLIP_EXIST(curGateBlip)
				REMOVE_BLIP(curGateBlip)
			ENDIF
			IF DOES_BLIP_EXIST(nextGateBlip)
				PRINTLN("2. nextGateBlip exists, so we're giving the blip to curGateBlip")
				curGateBlip = nextGateBlip
				nextGateBlip = NULL
				SET_BLIP_SCALE(curGateBlip, fCUR_BLIP_SCALE)
				IF iCurGate >= iTotalGates - 1
					SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_TGT")
				ELSE
					SET_BLIP_NAME_FROM_TEXT_FILE(curGateBlip, "BJ_BLIP_CHK")
				ENDIF
				
			ELSE
				SCRIPT_ASSERT("Going to next gate but the next blip hasn't been created!")
			ENDIF
			
			// Blip the next next gate.
			IF iCurGate < iTotalGates - 1
				PRINTLN("3. Creating a new blip for nextGateBlip")
				IF NOT DOES_BLIP_EXIST(nextGateBlip)
					nextGateBlip = ADD_BLIP_FOR_COORD(BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate + 1))
					SET_BLIP_COLOUR(nextGateBlip, BLIP_COLOUR_YELLOW)
					SET_BLIP_SCALE(nextGateBlip, fNEXT_BLIP_SCALE)
					IF iCurGate + 1 >= iTotalGates - 1
						SET_BLIP_NAME_FROM_TEXT_FILE(nextGateBlip, "BJ_BLIP_TGT")
					ELSE
						SET_BLIP_NAME_FROM_TEXT_FILE(nextGateBlip, "BJ_BLIP_CHK")
					ENDIF
				ENDIF
			ELSE
				nextGateBlip = NULL
			ENDIF
			
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate), -1)
		ENDIF
	ENDIF
	
	RETURN BJGATECHECK_NONE
ENDFUNC

PROC BJ_DRAW_FINAL_GATE_POST_LANDING(VECTOR vPosition, structTimer landingTimer)
	IF NOT IS_TIMER_STARTED(landingTimer)
		EXIT
	ENDIF
	
	FLOAT fTime = GET_TIMER_IN_SECONDS(landingTimer)
	
	IF fTime > 1.4
		EXIT
	ENDIF
	
	INT iAlpha = iTARGET_ALPHA - CEIL(iTARGET_ALPHA * fTime / 1.4)
	
	DRAW_MARKER(MARKER_RING, vPosition, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_S, fGND_TGT_DRAW_SCALE_S, fGND_TGT_DRAW_SCALE_S>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, iAlpha, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vPosition, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_M, fGND_TGT_DRAW_SCALE_M, fGND_TGT_DRAW_SCALE_M>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, iAlpha, FALSE, FALSE)
	DRAW_MARKER(MARKER_RING, vPosition, <<0,0,1>>, <<0,0,0>>, <<fGND_TGT_DRAW_SCALE_L, fGND_TGT_DRAW_SCALE_L, fGND_TGT_DRAW_SCALE_L>>, iTARGET_RED, iTARGET_GREEN, iTARGET_BLUE, iAlpha, FALSE, FALSE)
ENDPROC

FUNC INT BJ_CALCULATE_ACCURACY(BJ_ARGS& bjArgs, INT& iCurGate)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vTargetPos = BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurGate)
	VECTOR vTargetToPlayer = vPlayerPos - vTargetPos
	
	FLOAT fFlatDistance = SQRT(vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y)
	
	PRINTLN(">>>>> Calculating accuracy")
	RETURN CLAMP_INT(IMIN(CEIL(100.0 * (fGROUND_TGT_RG_HORIZ - fFlatDistance) / fGROUND_TGT_RG_HORIZ) + 4, 100), 0, 100)

ENDFUNC

PROC BJ_PERFORM_FINAL_LANDING_CHECK(VECTOR vTargetPos, VEHICLE_INDEX targetVehicle, PED_INDEX targetDriver, BJ_GATE_CHECK_RESULT& gateCheckResult, INT& iAccuracy, FLOAT &fLandingDistance) 
	PRINTLN("++++~~~~====%####>>>> BASEJUMP PERFORMING FINAL LANDING CHECK <<<<####%====~~~~++++")
	
	
	//--- check player integrity first
	
	PED_INDEX ped = PLAYER_PED_ID()
	VEHICLE_INDEX targetTrailer = NULL
	
	BOOL bIsPedInjured = IS_PED_INJURED(ped)
	PRINTLN("Initial ped injured check... (1 for yes): ", bIsPedInjured)
	IF bIsPedInjured
		gateCheckResult = BJGATECHECK_FAIL
		PRINTLN("Exiting early since the player is injured!")
		EXIT
	ELSE
		PRINTLN("Player passed injured check! Moving on...")
	ENDIF
	
	//--- start landing checks!
	
	PED_PARACHUTE_STATE pedPPS = GET_PED_PARACHUTE_STATE(ped)
	PED_PARACHUTE_LANDING_TYPE pedPPLT = GET_PED_PARACHUTE_LANDING_TYPE(ped)
	BOOL bIsInAir = IS_ENTITY_IN_AIR(ped)
	BOOL bIsInRagdoll = IS_PED_RAGDOLL(ped)
	BOOL bIsTargetAVehicle = DOES_ENTITY_EXIST(targetVehicle)
	BOOL bIsPedOnTargetVehicle = (bIsTargetAVehicle AND IS_PED_ON_SPECIFIC_VEHICLE(ped, targetVehicle))
	BOOL bIsPedOnTargetDriver = (bIsTargetAVehicle AND (NOT IS_PED_INJURED(targetDriver)) AND IS_ENTITY_TOUCHING_ENTITY(ped, targetDriver))
	BOOL bIsTargetATrailer = (bIsTargetAVehicle AND GET_VEHICLE_TRAILER_VEHICLE(targetVehicle, targetTrailer))
	BOOL bIsPedOnTargetTrailer = (bIsTargetATrailer AND (NOT IS_ENTITY_DEAD(targetTrailer)) AND IS_PED_ON_SPECIFIC_VEHICLE(ped, targetTrailer))
	BOOL bIsTargetACoord = (NOT bIsTargetAVehicle AND NOT IS_VECTOR_ZERO(vTargetPos))
	
	
	//--- check pass conditions
	
	//--- check for just a target vector with no vehicle
	
	IF bIsTargetACoord
		VECTOR vPlayerPos = GET_ENTITY_COORDS(ped)
		VECTOR vTargetToPlayer = vPlayerPos - vTargetPos
		FLOAT fFlatDistance = SQRT(vTargetToPlayer.x * vTargetToPlayer.x + vTargetToPlayer.y * vTargetToPlayer.y)
		fLandingDistance = fFlatDistance
		IF (vTargetToPlayer.z < fGROUND_TGT_RG_VERT_ABOVE AND vTargetToPlayer.z > fGROUND_TGT_RG_VERT_BELOW AND fFlatDistance < fGROUND_TGT_RG_HORIZ) 
			iAccuracy = CLAMP_INT(CEIL(100.0 * (fGROUND_TGT_RG_HORIZ - fFlatDistance) / fGROUND_TGT_RG_HORIZ) + 4, 0, 100)
			gateCheckResult  = BJGATECHECK_PASS
		ELSE
			PRINTLN("Player failed for landing too far away from the target coordinate")
			gateCheckResult = BJGATECHECK_FAIL_TOO_FAR_LANDING
		ENDIF
	ENDIF
	
	//--- check for the player on the vehicle or trailer
	
	IF bIsTargetAVehicle 
		IF BJ_IS_PLAYER_ON_TARGET_VEHICLE(targetVehicle, targetDriver)
//		IF bIsPedOnTargetVehicle OR bIsPedOnTargetDriver
//			iAccuracy = 100
//			gateCheckResult  = BJGATECHECK_PASS
//		ELIF bIsTargetATrailer
//			IF bIsPedOnTargetTrailer
//				iAccuracy = 100
//				gateCheckResult  = BJGATECHECK_PASS
//			ELSE
//				iAccuracy = 0
//				PRINTLN("Player failed for landing too far away from the target vehicle")
//				gateCheckResult  = BJGATECHECK_FAIL_TOO_FAR_LANDING
//			ENDIF
//		ELSE
//			iAccuracy = 0
//			PRINTLN("Player failed for landing too far away from the target vehicle")
//			gateCheckResult  = BJGATECHECK_FAIL_TOO_FAR_LANDING
//		ENDIF
			fLandingDistance = 0
			iAccuracy = 100
			gateCheckResult  = BJGATECHECK_PASS
		ELSE
			iAccuracy = 0
			PRINTLN("Player failed for landing too far away from the target vehicle")
			gateCheckResult  = BJGATECHECK_FAIL_TOO_FAR_LANDING
		ENDIF
	ENDIF
	
	//--- check for explicit fail conditions
	
	//--- print out report
	
	PRINTLN("***PLAYER PARACHUTE LANDING REPORT***")
	PRINTLN("Player in air?: ", bIsInAir)
	PRINTLN("Player height above ground: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(ped))
	IF bIsTargetATrailer
		VEHICLE_INDEX tempTrailer
		GET_VEHICLE_TRAILER_VEHICLE(targetVehicle, tempTrailer)
		VECTOR vPlayerPos = GET_ENTITY_COORDS(ped)
		VECTOR vTargetVehiclePos = GET_ENTITY_COORDS(tempTrailer)		
		PRINTLN("Trailer height above ground: ", GET_ENTITY_HEIGHT_ABOVE_GROUND(targetVehicle))
		PRINTLN("Player Z: ", vPlayerPos.z)
		PRINTLN("Trailer z: ", vTargetVehiclePos.z)
	ENDIF
	
	PRINTLN("Player is ragdoll? (1 for yes): ", bIsInRagdoll )
	PRINTLN("Player Parachute state is: ", GET_STRING_FROM_PED_PARACHUTE_STATE(pedPPS))
	PRINTLN("Player Parachute Landing Type is: ", GET_STRING_FROM_PED_LANDING_TYPE(pedPPLT))
	PRINTLN("Target is a coordinate? (1 for yes):", bIsTargetACoord)
	PRINTLN("Target is a vehicle? (1 for yes):", bIsTargetAVehicle)
	PRINTLN("Player on the target vehicle? (1 for yes):", bIsPedOnTargetVehicle)
	PRINTLN("Player on the target driver? (1 for yes):", bIsPedOnTargetDriver)
	PRINTLN("Target has a trailer? (1 for yes):", bIsTargetATrailer)
	PRINTLN("Player on the target vehicle? (1 for yes):", bIsPedOnTargetTrailer)
	PRINTLN("*****************************")
	
ENDPROC

FUNC BOOL BJ_CALCULATE_REWARD(INT& iGatesHit, INT iTotalGates, INT& iAccuracy, INT& iReward, FLOAT rewardFactor)
	INT iAllGatesBonus 
	
	IF iTotalGates > 1 AND iGatesHit = iTotalGates-1
		iAllGatesBonus = iALL_GATE_REWARD
	ELSE
		iAllGatesBonus = 0
	ENDIF
	
	iReward = 0
	
	iReward += CEIL((rewardFactor * iFINISH_REWARD_BASE) + (rewardFactor * iAllGatesBonus))  //Base award plus bonus
	iReward += CEIL((rewardFactor*fFINISH_REWARD_MULT) * TO_FLOAT(iAccuracy))//landing award
	
	CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iReward )
	RETURN TRUE

ENDFUNC

FUNC BOOL BJ_SHOULD_WAIT_FOR_PLAYER_IN_WATER(structTimer &myPlayerInWaterTimer)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		IF IS_TIMER_STARTED(myPlayerInWaterTimer)
			IF GET_TIMER_IN_SECONDS(myPlayerInWaterTimer) >= fBJ_PLAYER_IN_WATER_SCORECARD_DELAY
				CANCEL_TIMER(myPlayerInWaterTimer)
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELSE
			START_TIMER_NOW(myPlayerInWaterTimer)
			RETURN TRUE
		ENDIF
	ELSE
		CANCEL_TIMER(myPlayerInWaterTimer)
		RETURN FALSE
	ENDIF
ENDFUNC

PROC BJ_RESET_ON_RETRY(BJ_ARGS& bjArgs, SCALEFORM_INDEX& bjUI_Leaderboard, BJ_UI& bjUI, INT& iMessagingFlags, PED_INDEX& targetDriverPed, PED_INDEX& pedOnlookers[], VEHICLE_INDEX& vehPlayer, VEHICLE_INDEX& targetVehicle, VEHICLE_INDEX& targetFlatbed, VEHICLE_INDEX& extraVehicle, VEHICLE_INDEX& vehNearby[], CHECKPOINT_INDEX& curCheckpoint, CHECKPOINT_INDEX& nextCheckpoint, BLIP_INDEX curGateBlip, BLIP_INDEX nextGateBlip, INT& iCurrentGate, INT& iCurCheckpoint, INT& iGatesHit, INT& iNumPlayerLines, INT &iAccuracy, BJ_JUMP_ID eJumpID, BJ_GATE_CHECK_RESULT &gateCheckResult, PED_PARACHUTE_LANDING_TYPE& savedLandingType, structTimer& tEndDelayTimer, structTimer& tmrLandingAlpha, VECTOR& vCameraVelocity, FLOAT& fLastScreenPosX, FLOAT& fLastScreenPosY, FLOAT& fLastGutterRotation, BOOL& bHasLanded, BOOL& bFlyaway, BOOL& bDebugPass, BOOL& bDebugFail, BOOL& bDebugSkip, BOOL &bLBToggle, BOOL &bPlayerDiedLanding, VEHICLE_COMBAT_AVOIDANCE_AREA_INDEX &bjCombatAvoidanceArea)
	VEHICLE_INDEX vehPlayerCurrent
	
	REMOVE_VEHICLE_COMBAT_AVOIDANCE_AREA(bjCombatAvoidanceArea)

	CLEAR_RANK_REDICTION_DETAILS() 

	SC_LEADERBOARD_CACHE_CLEAR_ALL()

	BJ_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	UNUSED_PARAMETER(bjUI_Leaderboard)
	
	ENDSCREEN_SHUTDOWN(bjUI.bjEndScreen)
	
	iAccuracy = 0
	gateCheckResult = BJGATECHECK_NONE
	PED_INDEX pedTargetDriver
	
	//reset landing detection data
	vCameraVelocity = <<0,0,0>>
	savedLandingType = PPLT_INVALID
	bHasLanded = FALSE
	bFlyaway = FALSE
	
	CLEANUP_MINIGAME_INSTRUCTIONS(bjUI.uiControls)
	
	IF IS_TIMER_STARTED(tmrLandingAlpha)
		CANCEL_TIMER(tmrLandingAlpha)
	ENDIF
	
	IF IS_TIMER_STARTED(tEndDelayTimer)
		CANCEL_TIMER(tEndDelayTimer)
	ENDIF
	
	IF DOES_ENTITY_EXIST(targetVehicle)
		IF BJ_IS_MODEL_TRAIN(BJ_GET_ARG_TARGET_MODEL(bjArgs))
			//SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(targetVehicle)
			DELETE_MISSION_TRAIN(targetVehicle)
		ELSE
			IF NOT IS_ENTITY_DEAD(targetVehicle)
				pedTargetDriver = GET_PED_IN_VEHICLE_SEAT(targetVehicle, VS_DRIVER)
				IF DOES_ENTITY_EXIST(pedTargetDriver)
					DELETE_PED(pedTargetDriver)
				ENDIF
			ENDIF
			
			DELETE_VEHICLE(targetVehicle)
		ENDIF
	ENDIF
	
	INT iPedCounter
	REPEAT COUNT_OF(pedOnlookers) iPedCounter
		IF DOES_ENTITY_EXIST(pedOnlookers[iPedCounter]) AND NOT IS_ENTITY_DEAD(pedOnlookers[iPedCounter])
			CLEAR_PED_TASKS(pedOnlookers[iPedCounter])
			pedOnlookers[iPedCounter] = NULL
		ENDIF
	ENDREPEAT
	
	//if the vehicle has a trailer...
	IF DOES_ENTITY_EXIST(targetFlatbed)
		//SET_VEHICLE_AS_NO_LONGER_NEEDED(targetFlatbed)
		DELETE_VEHICLE(targetFlatbed)
	ENDIF
		
	IF DOES_ENTITY_EXIST(extraVehicle)
		IF NOT IS_VEHICLE_FUCKED(extraVehicle) AND DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(extraVehicle))
			PED_INDEX tempPed = GET_PED_IN_VEHICLE_SEAT(extraVehicle)
			DELETE_PED(tempPed)
		ENDIF
		DELETE_VEHICLE(extraVehicle)
	ENDIF
		
	IF NOT IS_ENTITY_DEAD(targetDriverPed)
		SET_PED_AS_NO_LONGER_NEEDED(targetDriverPed)
//		targetDriverPed = NULL
	ENDIF
	
	//take care of any vehicle the player grabbed while on a free jump
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		vehPlayerCurrent = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED | ECF_JUMP_OUT)
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), FALSE, TRUE)
		IF DOES_ENTITY_EXIST(vehPlayerCurrent)
			SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(vehPlayerCurrent) + <<2.0, 0.0, 0.0>>)
			
			IF DOES_ENTITY_EXIST(vehPlayer) AND vehPlayerCurrent = vehPlayer
				SET_PLAYERS_LAST_VEHICLE(vehPlayer)
				SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayer, BJ_GET_SAFE_PLAYER_VEHICLE_COORDS(eJumpID), BJ_GET_SAFE_PLAYER_VEHICLE_HEADING(eJumpID))
				SET_ENTITY_VELOCITY(vehPlayerCurrent, <<0,0,0>>)
				SET_ENTITY_COORDS(vehPlayerCurrent, BJ_GET_SAFE_PLAYER_VEHICLE_COORDS(eJumpID))
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCurrent)
				SET_ENTITY_HEADING(vehPlayerCurrent, BJ_GET_SAFE_PLAYER_VEHICLE_HEADING(eJumpID))
			
			ELSE
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayerCurrent)
					SET_ENTITY_AS_MISSION_ENTITY(vehPlayerCurrent)
				ENDIF
				
				DELETE_VEHICLE(vehPlayerCurrent)
			ENDIF
		ENDIF
	ENDIF
	
	iCurrentGate = 0
	iGatesHit = 0
	iNumPlayerLines = 1
	
	fLastScreenPosX = 0.0
	fLastScreenPosY = 0.0
	fLastGutterRotation = 0.0
	
	bDebugPass = FALSE
	bDebugFail = FALSE
	bDebugSkip = FALSE
	bLBToggle = FALSE
	bPlayerDiedLanding = FALSE
	
	IF iCurCheckpoint > -1
		DELETE_CHECKPOINT(curCheckpoint)
		DELETE_CHECKPOINT(nextCheckpoint) // No way to see if this exists
		iCurCheckpoint = -1
	ENDIF
	
	IF DOES_BLIP_EXIST(curGateBlip)
		REMOVE_BLIP(curGateBlip)
	ENDIF
	IF DOES_BLIP_EXIST(nextGateBlip)
		REMOVE_BLIP(nextGateBlip)
	ENDIF
	
	INT iCounter
	
	REPEAT COUNT_OF(vehNearby) iCounter
		IF DOES_ENTITY_EXIST(vehNearby[iCounter]) AND NOT IS_ENTITY_DEAD(vehNearby[iCounter])
			SET_VEHICLE_LOD_MULTIPLIER(vehNearby[iCounter], 1.0)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehNearby[iCounter], FALSE)
		ENDIF
		vehNearby[iCounter] = NULL
	ENDREPEAT
	
	BJ_CLEAR_UI_FLAGS(bjUI)
	iMessagingFlags = 0

	MG_STATS_TRACKER_RESET_DATA_SLOTS()
	KILL_MINIGAME_MISSION_TRACKER()
	DO_SCREEN_FADE_OUT(iFADE_OUT_TIME)
ENDPROC

/// PURPOSE:
///    converts BJ_UI_FLAG to a string so we can use it with TRIGGER_MUSIC_EVENT and PREPARE_MUSIC_EVENT
/// PARAMS:
///    thisFlag - 	BJUIFLAG_MUSIC_EVENT_OJBJ_START,
///					BJUIFLAG_MUSIC_EVENT_OJBJ_JUMPED,
///					BJUIFLAG_MUSIC_EVENT_OJBJ_LANDED,
///					BJUIFLAG_MUSIC_EVENT_OJBJ_STOP
/// RETURNS:
///    Name of music event
///    
FUNC STRING BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJ_UI_FLAG thisFlag)
	SWITCH (thisFlag)
		CASE BJUIFLAG_MUSIC_TRIG_OJBJ_START
		CASE BJUIFLAG_MUSIC_PREP_OJBJ_START
			RETURN "OJBJ_START"
		CASE BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED
		CASE BJUIFLAG_MUSIC_PREP_OJBJ_JUMPED
			RETURN "OJBJ_JUMPED"
		CASE BJUIFLAG_MUSIC_TRIG_OJBJ_LANDED
		CASE BJUIFLAG_MUSIC_PREP_OJBJ_LANDED
			RETURN "OJBJ_LANDED"
		CASE BJUIFLAG_MUSIC_TRIG_OJBJ_STOP
		CASE BJUIFLAG_MUSIC_PREP_OJBJ_STOP
			RETURN "OJBJ_STOP"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC BJ_UPDATE_MUSIC(BJ_UI &bjUI, BJ_GAME_STATE gameState)
	IF gameState < BJGAMESTATE_WINNER
		//if the prepared flag is true, or if we're in the right state.
		IF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_MUSIC_TRIG_OJBJ_START)
			IF gameState >= BJGAMESTATE_CUTSCENE
			AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
				IF MG_PREPARE_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_PREP_OJBJ_START), bjUI.uiFlags, BJUIFLAG_MUSIC_PREP_OJBJ_START)
//					PRINTLN("Attempting to trigger music event: OJBJ_START")
					MG_TRIGGER_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_TRIG_OJBJ_START), bjUI.uiFlags, BJUIFLAG_MUSIC_TRIG_OJBJ_START)
				ENDIF
			ENDIF
	//+++++++ 
		ELIF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED)
			IF gameState = BJGAMESTATE_SKYDIVING
				IF MG_PREPARE_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_PREP_OJBJ_JUMPED), bjUI.uiFlags, BJUIFLAG_MUSIC_PREP_OJBJ_JUMPED)
//					PRINTLN("Attempting to trigger music event: OJBJ_JUMPED")
					MG_TRIGGER_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED), bjUI.uiFlags, BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED)
				ENDIF
			ELIF gameState = BJGAMESTATE_HELI_JUMP
				IF MG_PREPARE_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_PREP_OJBJ_JUMPED), bjUI.uiFlags, BJUIFLAG_MUSIC_PREP_OJBJ_JUMPED)	
//					PRINTLN("Attempting to trigger music event: OJBJ_JUMPED")
					MG_TRIGGER_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED), bjUI.uiFlags, BJUIFLAG_MUSIC_TRIG_OJBJ_JUMPED)
				ENDIF
			ENDIF
	//+++++++ 
		ELIF gameState = BJGAMESTATE_PROCESS_DATA AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_MUSIC_TRIG_OJBJ_LANDED)
			IF MG_PREPARE_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_PREP_OJBJ_LANDED), bjUI.uiFlags, BJUIFLAG_MUSIC_PREP_OJBJ_LANDED)
//				PRINTLN("Attempting to trigger music event: OJBJ_LANDED")
				MG_TRIGGER_MUSIC_EVENT(BJ_CONVERT_UI_FLAG_TO_MUSIC_EVENT_STRING(BJUIFLAG_MUSIC_TRIG_OJBJ_LANDED), bjUI.uiFlags, BJUIFLAG_MUSIC_TRIG_OJBJ_LANDED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    converts BJ_UI_FLAG to a string so we can use it with TRIGGER_MUSIC_EVENT and PREPARE_MUSIC_EVENT
/// PARAMS:
///    thisFlag - 	BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START,
//					BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED,
//					BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED,
//					BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_STOP
/// RETURNS:
///    Name of audio scene
///    
FUNC STRING BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(BJ_JUMP_ID eJumpID, BJ_UI_FLAG thisFlag)
	STRING sChoice = ""
	SWITCH (thisFlag)
		CASE BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_CUTSCENE
			RETURN "BASEJUMPS_OVERVIEW_CUTSCENE"
		CASE BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START
			IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
				sChoice = "BASEJUMPS_PREP_FOR_JUMP_HELI"
			ELSE
				//if we're not in a heli, but we're in the launch vehicle, play moto audio scene.
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND BJ_IS_MODEL_GROUND_VEH(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
					sChoice = "BASEJUMPS_PREP_FOR_JUMP_MOTO"
				ELSE
					sChoice = "BASEJUMPS_PREP_FOR_JUMP_ON_FOOT"				
				ENDIF
			ENDIF
			RETURN sChoice
		CASE BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED
			RETURN "BASEJUMPS_SKYDIVE"
		CASE BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED
			RETURN "BASEJUMPS_OPEN_PARACHUTE"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC BJ_UPDATE_AUDIO_SCENES(BJ_UI &bjUI, BJ_GAME_STATE gameState, BJ_JUMP_ID eJumpID, BOOL bRetry)
	IF gameState < BJGAMESTATE_WINNER
		//if the prepared flag is true, or if we're in the right state.
		
		IF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_CUTSCENE)
			IF gameState = BJGAMESTATE_CUTSCENE AND NOT bRetry
			AND (IS_SCREEN_FADED_IN() OR IS_SCREEN_FADING_IN())
				IF MG_START_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_CUTSCENE), "", bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_CUTSCENE)
					//do nothing
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START)
			IF gameState > BJGAMESTATE_CUTSCENE
				IF MG_START_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START), BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_CUTSCENE), bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START)
					//do nothing
				ENDIF
			ENDIF
	//+++++++ 
		ELIF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED)
			IF gameState = BJGAMESTATE_SKYDIVING
				IF MG_START_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED), BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START), bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED)
					//do nothing
				ENDIF
			ELIF gameState = BJGAMESTATE_HELI_JUMP
				IF MG_START_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED), BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START), bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED)
					//do nothing
				ENDIF
			ENDIF
	//+++++++ 
		ELIF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED)
			IF gameState = BJGAMESTATE_SKYDIVING
				PED_PARACHUTE_STATE parachuteTest = GET_PED_PARACHUTE_STATE(PLAYER_PED_ID())
				IF parachuteTest = PPS_DEPLOYING OR parachuteTest = PPS_PARACHUTING
					IF MG_START_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED), BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_JUMPED), bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED)
						//do nothing
					ENDIF	
				ENDIF
			ENDIF
	//+++++++ 
		ELIF gameState = BJGAMESTATE_PROCESS_DATA AND NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_STOP)
			MG_STOP_AUDIO_SCENE(BJ_CONVERT_UI_FLAG_TO_AUDIO_SCENE_STRING(eJumpID, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_DEPLOYED), bjUI.uiFlags, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_STOP)
		ELIF NOT BJ_GET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_STOP) AND NOT IS_PLAYER_PLAYING(PLAYER_ID())
			STOP_AUDIO_SCENES()
		ENDIF
	ENDIF
ENDPROC

//// PURPOSE:
 ///    Stops the current audio scene and clears the audio scene flag. the rest is taken care of by the update_audio_scene funcs (above)
 ///    This is used for on foot jumps where the player can get on and off a motorcycle
 /// PARAMS:
 ///    bjUI - 
 ///    bPlayerNowOnMoto - 
PROC BJ_ADJUST_AUDIO_SCENE_FOR_ON_FOOT_JUMPS(BJ_UI &bjUI, BOOL bPlayerNowOnMoto)
	IF bPlayerNowOnMoto
		IF IS_AUDIO_SCENE_ACTIVE("BASEJUMPS_PREP_FOR_JUMP_ON_FOOT")
			STOP_AUDIO_SCENE("BASEJUMPS_PREP_FOR_JUMP_ON_FOOT")
			PRINTLN("STOPPED AUDIO SCENE: BASEJUMPS_PREP_FOR_JUMP_ON_FOOT")
			BJ_SET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START, FALSE)
		ENDIF
	ELSE		
		IF IS_AUDIO_SCENE_ACTIVE("BASEJUMPS_PREP_FOR_JUMP_MOTO")
			STOP_AUDIO_SCENE("BASEJUMPS_PREP_FOR_JUMP_MOTO")
			PRINTLN("STOPPED AUDIO SCENE: BASEJUMPS_PREP_FOR_JUMP_MOTO")
			BJ_SET_UI_FLAG(bjUI, BJUIFLAG_AUDIO_SCENE_TRIG_OJBJ_START, FALSE)
		ENDIF
	ENDIF		
ENDPROC

PROC BJ_UPDATE_SPLASH_SCREEN(SCRIPT_SCALEFORM_BIG_MESSAGE& bigMessageUI)
	UPDATE_SCALEFORM_BIG_MESSAGE(bigMessageUI)
ENDPROC

// Clean up UI, objects, players, cameras, audio, and network.
PROC BJ_SCRIPT_CLEANUP(BJ_ARGS& bjArgs, SCALEFORM_INDEX& bjUI_Leaderboard, VEHICLE_INDEX& launchVehicle, VEHICLE_INDEX& targetVehicle, VEHICLE_INDEX& extraVehicle, VEHICLE_INDEX& vehNearby[], VEHICLE_INDEX& vehPlayer, structPedsForConversation& thisConversationPedStruct, PED_INDEX& launchDriverPed, PED_INDEX& targetDriverPed, PED_INDEX& pedOnlookers[], INT iSoundID, INT& iNumPlayerLines, BLIP_INDEX curGateBlip, BLIP_INDEX nextGateBlip, BJ_JUMP_ID eJumpID, VEHICLE_COMBAT_AVOIDANCE_AREA_INDEX& bjCombatAvoidanceArea, BOOL bDoesPlayerHaveOwnParachute)
	
	CLEAR_RANK_REDICTION_DETAILS() 

	SC_LEADERBOARD_CACHE_CLEAR_ALL()
	
	BJ_CLEANUP_SOCIAL_CLUB_LEADERBOARD()
	
	BJ_CLEANUP_LEADERBOARD(bjUI_Leaderboard)
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE)
	
	CLEANUP_SC_LEADERBOARD_UI(bjUI_Leaderboard)
	
	REMOVE_VEHICLE_COMBAT_AVOIDANCE_AREA(bjCombatAvoidanceArea)
	
	PED_INDEX pedDriver
	
	CANCEL_MUSIC_EVENT("OJBJ_START")
	CANCEL_MUSIC_EVENT("OJBJ_JUMPED")
	CANCEL_MUSIC_EVENT("OJBJ_LANDED")
	TRIGGER_MUSIC_EVENT("OJBJ_STOP")
	STOP_AUDIO_SCENE("BASEJUMPS_OPEN_PARACHUTE")
	STOP_AUDIO_SCENE("BASEJUMPS_PREP_FOR_JUMP_ON_FOOT")
	STOP_AUDIO_SCENE("BASEJUMPS_PREP_FOR_JUMP_MOTO")
	STOP_AUDIO_SCENE("BASEJUMPS_SKYDIVE")
	STOP_AUDIO_SCENE("BASEJUMPS_OPEN_PARACHUTE")
	STOP_AUDIO_SCENES()
	
//	SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
	
//	IF iSoundID <> -1
//		STOP_SOUND(iSoundID)
//		RELEASE_SOUND_ID(iSoundID)
//	ENDIF
	
	iSoundID = iSoundID
	
	// Allow the respawn controller to process fades again.

	g_bBlockDeathFadeout = FALSE
	g_bBlockWastedTitle = FALSE
	g_bBlockUltraSloMoOnDeath = FALSE
	
	MG_DO_FAIL_EFFECT(FALSE, TRUE)
	MG_DO_FAIL_OUT_EFFECT(FALSE, TRUE)

	SET_CURRENT_MINIGAME_TO_BYPASS_RESPAWN_CUTSCENE(FALSE)
	SET_FADE_OUT_AFTER_DEATH(TRUE)
	PAUSE_DEATH_ARREST_RESTART(FALSE)
	IGNORE_NEXT_RESTART(FALSE)

//	structTimer fadeTimer
	// Wait for the screen to fade out.
	WHILE IS_SCREEN_FADING_OUT() WAIT(0) ENDWHILE
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	ENDIF
	
	IF DOES_BLIP_EXIST(curGateBlip)
		REMOVE_BLIP(curGateBlip)
	ENDIF
	IF DOES_BLIP_EXIST(nextGateBlip)
		REMOVE_BLIP(nextGateBlip)
	ENDIF
	
	MISSION_FLOW_CLEAR_DISPLAY_MISSION_TITLE()
	
	DISABLE_CELLPHONE(FALSE)
	
	SET_PLAYER_CAN_LEAVE_PARACHUTE_SMOKE_TRAIL(PLAYER_ID(), FALSE)
	
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_GOLF_COURSE, TRUE)
	
	// Kill widgets.
	#IF IS_DEBUG_BUILD
		BJ_CLEANUP_WIDGETS()
	#ENDIF
	
	//remove vehicle generator block
	IF IS_VECTOR_ZERO(BJ_GET_ARG_CAR_REMOVAL_COORDS_MIN(bjArgs))// OR IS_VECTOR_ZERO(BJ_GET_ARG_CAR_REMOVAL_COORDS_MAX())
		//do nothing
	ELSE
		CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	// Remove dialogue ped
	BJ_CLEANUP_DIALOGUE(thisConversationPedStruct)
	
	KILL_MINIGAME_MISSION_TRACKER()
	
	SET_FAKE_MINIMAP_MAX_ALTIMETER_HEIGHT(0)
	CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
	
	SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(FALSE)
	
	// Restore control.
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	IF eJumpID = BJJUMPID_CRANE
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLadderClimbing, FALSE)
	ENDIF
	
	// Reset player ped.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTakeOffParachutePack, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
		//CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
		IF NOT bDoesPlayerHaveOwnParachute
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		ENDIF
		SET_PED_LOD_MULTIPLIER(PLAYER_PED_ID(), 1.0)
		SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	INT iPedCounter
	REPEAT COUNT_OF(pedOnlookers) iPedCounter
		IF DOES_ENTITY_EXIST(pedOnlookers[iPedCounter]) AND NOT IS_ENTITY_DEAD(pedOnlookers[iPedCounter])
			CLEAR_PED_TASKS(pedOnlookers[iPedCounter])
			pedOnlookers[iPedCounter] = NULL
		ENDIF
	ENDREPEAT
	
	// Restore the player's last vehicle.
	IF DOES_ENTITY_EXIST(vehPlayer) AND vehPlayer <> GET_PLAYERS_LAST_VEHICLE() AND NOT IS_ENTITY_DEAD(vehPlayer)
		SET_PLAYERS_LAST_VEHICLE(vehPlayer)
		SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayer, BJ_GET_SAFE_PLAYER_VEHICLE_COORDS(eJumpID), BJ_GET_SAFE_PLAYER_VEHICLE_HEADING(eJumpID))
	ENDIF
	
	// Release the vehicles & driver/pilot.
	IF NOT IS_PED_INJURED(launchDriverPed)
		IF NOT IS_ENTITY_ON_SCREEN(launchDriverPed)
			PRINTLN(">>>>> DELETING LAUNCH VEHICLE PED!")
			DELETE_PED(launchDriverPed)
		ELSE
			PRINTLN(">>>>> SETTING LAUNCH VEHICLE PED AS NO LONGER NEEDED!")
			SET_PED_AS_NO_LONGER_NEEDED(launchDriverPed)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(targetDriverPed)
		CLEAR_PED_TASKS(targetDriverPed)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(targetDriverPed, TRUE)
//		SET_PED_CONFIG_FLAG(targetDriverPed, PCF_NeverReactToPedOnRoof, TRUE)
//		SET_PED_CONFIG_FLAG(targetDriverPed, PCF_DisablePanicInVehicle, TRUE)
		  

//		SET_PED_AS_NO_LONGER_NEEDED(targetDriverPed)
	ENDIF
	IF DOES_ENTITY_EXIST(launchVehicle)
		IF BJ_IS_MODEL_HELI(BJ_GET_LAUNCH_VEHICLE_BY_ID(eJumpID))
			IF NOT IS_ENTITY_DEAD(launchVehicle) AND NOT IS_PED_INJURED(PLAYER_PED_ID()) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), launchVehicle)
				SPECIAL_FUNCTION_DO_NOT_USE(PLAYER_PED_ID())
			ENDIF
			IF NOT IS_ENTITY_ON_SCREEN(launchVehicle)
				PRINTLN(">>>>> DELETING LAUNCH VEHICLE!")
				DELETE_VEHICLE(launchVehicle)
			ELSE
				PRINTLN(">>>>> SETTING LAUNCH VEHICLE AS NO LONGER NEEDED!")
				SET_VEHICLE_AS_NO_LONGER_NEEDED(launchVehicle)
			ENDIF
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(launchVehicle)
		ENDIF
		
	ENDIF
	IF DOES_ENTITY_EXIST(targetVehicle)
		IF BJ_IS_MODEL_TRAIN(BJ_GET_ARG_TARGET_MODEL(bjArgs))
			SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(targetVehicle)
			SET_RANDOM_TRAINS(TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(extraVehicle) AND NOT IS_ENTITY_DEAD(extraVehicle)
		pedDriver = GET_PED_IN_VEHICLE_SEAT(extraVehicle, VS_DRIVER)
		IF DOES_ENTITY_EXIST(pedDriver)
			DELETE_PED(pedDriver)
		ENDIF
		DELETE_VEHICLE(extraVehicle)
	ENDIF
	
	PRINTLN(">>>>> CLEANING UP BASEJUMPING!")

	INT i
	
	REPEAT COUNT_OF(vehNearby) i
		IF DOES_ENTITY_EXIST(vehNearby[i]) AND NOT IS_ENTITY_DEAD(vehNearby[i])
			SET_VEHICLE_LOD_MULTIPLIER(vehNearby[i], 1.0)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehNearby[i], FALSE)
		ENDIF
	ENDREPEAT
	
	iNumPlayerLines = 1
	
	// Restore cameras & UI.
	CLEAR_HELP()
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	SET_FRONTEND_RADIO_ACTIVE(TRUE)
	ENABLE_SELECTOR()
	
	// Reseting this here, in case MG_UPDATE_FAIL_SPLASH_SCREEN failed to cleanup. (Very important this gets reset)
	SET_NO_LOADING_SCREEN(FALSE)

	// Clean audio.
	//RELEASE_NAMED_SCRIPT_AUDIO_BANK("SCRIPT\\BJUMP")
	//UNREGISTER_SCRIPT_WITH_AUDIO()
//	START_TIMER_NOW(fadeTimer)
//	WHILE GET_TIMER_IN_SECONDS(fadeTimer) < 1.0 WAIT(0) ENDWHILE
//	WAIT(1000)
	/*
	// Fade in.
	IF IS_SCREEN_FADED_OUT()
		// Restore original clothing.
//		RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
		//wait until we timeout or the player has streamed in clothing variation assets.
		DO_SCREEN_FADE_IN(iFADE_IN_TIME)
	ENDIF
	
	// Wait for the screen to fade in.
	WHILE IS_SCREEN_FADING_IN() WAIT(0) ENDWHILE
	*/
	g_savedGlobals.sBasejumpData.iLaunchRank = -1 //reset launch rank
	
	// Kill the script.
	TERMINATE_THIS_THREAD()
ENDPROC

// The player successfully hit the last target.
PROC BJ_SCRIPT_PASS(BJ_ARGS& bjArgs, SCALEFORM_INDEX& bjUI_Leaderboard, VEHICLE_INDEX& launchVehicle, VEHICLE_INDEX& targetVehicle, VEHICLE_INDEX& extraVehicle, VEHICLE_INDEX& vehNearby[], VEHICLE_INDEX vehPlayer, structPedsForConversation& thisConversationPedStruct, PED_INDEX& launchDriverPed, PED_INDEX& targetDriverPed, PED_INDEX& pedOnlookers[], INT iSoundID, INT& iNumPlayerLines, BLIP_INDEX curGateBlip, BLIP_INDEX nextGateBlip, BJ_JUMP_ID eJumpID, VEHICLE_COMBAT_AVOIDANCE_AREA_INDEX& bjCombatAvoidanceArea, BOOL bDoesPlayerHaveOwnParachute)
	BJ_SCRIPT_CLEANUP(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, thisConversationPedStruct, launchDriverPed, targetDriverPed, pedOnlookers, iSoundID, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
ENDPROC

// The player missed the last target.
PROC BJ_SCRIPT_FAIL(BOOL bPlayerDied, BJ_ARGS& bjArgs, SCALEFORM_INDEX&	bjUI_Leaderboard, VEHICLE_INDEX& launchVehicle, VEHICLE_INDEX& targetVehicle, VEHICLE_INDEX& extraVehicle, VEHICLE_INDEX& vehNearby[], VEHICLE_INDEX vehPlayer, structPedsForConversation& thisConversationPedStruct, PED_INDEX& launchDriverPed, PED_INDEX& targetDriverPed, PED_INDEX& pedOnlookers[], INT iSoundID, INT& iNumPlayerLines, BLIP_INDEX curGateBlip, BLIP_INDEX nextGateBlip, BJ_JUMP_ID eJumpID, VEHICLE_COMBAT_AVOIDANCE_AREA_INDEX& bjCombatAvoidanceArea, BOOL bDoesPlayerHaveOwnParachute)
	IF bPlayerDied
		BJ_SCRIPT_CLEANUP(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, thisConversationPedStruct, launchDriverPed, targetDriverPed, pedOnlookers, iSoundID, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
	ELSE		
		BJ_SCRIPT_CLEANUP(bjArgs, bjUI_Leaderboard, launchVehicle, targetVehicle, extraVehicle, vehNearby, vehPlayer, thisConversationPedStruct, launchDriverPed, targetDriverPed, pedOnlookers, iSoundID, iNumPlayerLines, curGateBlip, nextGateBlip, eJumpID, bjCombatAvoidanceArea, bDoesPlayerHaveOwnParachute)
	ENDIF
ENDPROC
