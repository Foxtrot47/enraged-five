
//////////////////////////////////////////////////////////////////////
/* bj_camera.sch													*/
/* Authors: DJ Jones	& Yomal Perera								*/
/* Camera definitions for base jumping oddjob.						*/
//////////////////////////////////////////////////////////////////////


// List of available cameras.
ENUM BJ_CAMERA_ID
	BJCAMERAID_PRIMARY_CAM,
	BJCAMERAID_SECONDARY_CAM,
	BJCAMERAIDS
ENDENUM

// Keep track of interp to game cam.
ENUM BJ_CAMERA_INTERP_STATE
	BJCAMERAINTERPSTATE_NOT_STARTED,
	BJCAMERAINTERPSTATE_SCRIPTED_INTERP,
	BJCAMERAINTERPSTATE_INTERP_TO_GAME,
	BJCAMERAINTERPSTATE_FINISHED
ENDENUM


// Set of all available cameras.
STRUCT BJ_CAMERA_SET
	CAMERA_INDEX cameras[BJCAMERAIDS]
	BJ_CAMERA_INTERP_STATE bjCamInterpState
ENDSTRUCT


// Accessor for camera index.
FUNC CAMERA_INDEX BJ_GET_CAMERA_OBJECT(BJ_CAMERA_SET& cameraSet, BJ_CAMERA_ID camID)
	IF camID >= BJCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to BJ_GET_CAMERA_OBJECT!")
		RETURN NULL
	ENDIF
	
	RETURN cameraSet.cameras[camID]
ENDFUNC

// Mutator for camera index.
PROC BJ_SET_CAMERA_OBJECT(BJ_CAMERA_SET& cameraSet, BJ_CAMERA_ID camID, CAMERA_INDEX camIndex, BOOL bCheckValid = TRUE)
	IF camID >= BJCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to BJ_SET_CAMERA_OBJECT!")
		EXIT
	ENDIF
	
	IF bCheckValid AND NOT DOES_CAM_EXIST(camIndex)
		SCRIPT_ASSERT("Tried to assign nonexistent entity to camera! NOT GONNA HAPPEN!")
		EXIT
	ENDIF
	
	cameraSet.cameras[camID] = camIndex
ENDPROC

// Mutator for current active camera.
PROC BJ_SET_ACTIVE_CAMERA(BJ_CAMERA_SET& cameraSet, BJ_CAMERA_ID activeCam, BOOL bTurnOn = FALSE)
	IF activeCam >= BJCAMERAIDS
		SCRIPT_ASSERT("Passed an invalid camera ID to BJ_SET_ACTIVE_CAMERA!")
		EXIT
	ENDIF

	SET_CAM_ACTIVE(cameraSet.cameras[activeCam], bTurnOn)
ENDPROC
