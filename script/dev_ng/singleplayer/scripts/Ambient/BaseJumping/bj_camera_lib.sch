
//////////////////////////////////////////////////////////////////////
/* bj_camera_lib.sch												*/
/* Author: DJ Jones													*/
/* Camera functionality for base jump activity.						*/
//////////////////////////////////////////////////////////////////////


// Use a custom near clip for the PC, because extreme aspect ratios (5760x1200) are possible, B*2075120
FUNC FLOAT BJ_GET_NEAR_CLIP()
	IF IS_PC_VERSION()
		RETURN 0.5
	ENDIF
	
	RETURN 0.840
ENDFUNC

// Set up initial cutscene cameras.
PROC BJ_SETUP_CUTSCENE_CAMERAS(BJ_CAMERA_SET& cameraSet, BJ_ARGS& bjArgs, BJ_JUMP_ID eJumpID, BOOL bArtificialPullUp = FALSE)
	VECTOR vCamPos
	VECTOR vCamRot
	FLOAT fFOV
	
	vCamPos = BJ_GET_ARG_CUTSCENE_START_POSITION(bjArgs)
	vCamRot = BJ_GET_ARG_CUTSCENE_START_ROTATION(bjArgs)
	fFOV = BJ_GET_ARG_CUTSCENE_START_FOV(bjArgs)
	
	IF NOT DOES_CAM_EXIST(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM))
		BJ_SET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, vCamRot))
	ELSE
		SET_CAM_COORD(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), vCamPos)
		SET_CAM_ROT(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), vCamRot)
	ENDIF
	SET_CAM_FOV(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), fFOV)
	
	SET_CAM_ACTIVE(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), TRUE)
	
	IF bArtificialPullUp
		VECTOR vPullUpPos = vCamPos
		vPullUpPos.z += 8.0
		SET_CAM_PARAMS(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), vPullUpPos, vCamRot, fFOV, BJ_GET_CUTSCENE_LERP_DURATION_BY_ID(eJumpID))
	ENDIF
	
	vCamPos = BJ_GET_ARG_CUTSCENE_END_POSITION(bjArgs)
	vCamRot = BJ_GET_ARG_CUTSCENE_END_ROTATION(bjArgs)
	fFOV = BJ_GET_ARG_CUTSCENE_END_FOV(bjArgs)
	
	IF NOT DOES_CAM_EXIST(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM))
		BJ_SET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, vCamPos, vCamRot))
	ELSE
		SET_CAM_COORD(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), vCamPos)
		SET_CAM_ROT(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), vCamRot)
	ENDIF
	SET_CAM_FOV(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), fFOV)
	
	IF NOT IS_SCREEN_FADED_OUT()
		// We always start with a cutscene, so activate the interp camera here.
		SET_CAM_ACTIVE_WITH_INTERP(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), BJ_GET_CUTSCENE_LERP_DURATION_BY_ID(eJumpID))
	ENDIF
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", 0.2)
	DISPLAY_RADAR(FALSE)
	DISPLAY_HUD(FALSE)
ENDPROC

// Set up look camera.
PROC BJ_SETUP_HELI_LOOK_CAMERA(BJ_CAMERA_SET& cameraSet, BJ_ARGS& bjArgs, VECTOR& vLookCameraRot, VECTOR& vFocalPoint, VECTOR& vFocalOffset, VEHICLE_INDEX vehLaunch, VECTOR& vBaseRot, FLOAT& fBaseFOV)
	CAMERA_INDEX camIndex = BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM)
	VECTOR vCurFocalOffset
	VECTOR vBaseRotCopy
	VECTOR vBasePos
	
	IF DOES_ENTITY_EXIST(vehLaunch) AND NOT IS_ENTITY_DEAD(vehLaunch)
		vLookCameraRot = <<10.2986, 0, 8.9090>> // Start facing 8.9090 degrees left of center, 10.2986 above center
		
		vFocalOffset = 1.2 * <<SIN(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), -COS(-97.4239 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)), 0.0>>
		vFocalPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehLaunch, <<1.12046, -0.317773, 1.3385>>)
		
		vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(vFocalOffset, vLookCameraRot.z)
		vBasePos = vFocalPoint + vCurFocalOffset
		vBaseRot = <<-33.0000 + vLookCameraRot.x, 0.0, -88.515 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs)>>
		fBaseFOV = 26.0
		
		vBaseRotCopy = vBaseRot
		vBaseRotCopy.z += vLookCameraRot.z
		
		SET_CAM_COORD(camIndex, vBasePos)
		SET_CAM_ROT(camIndex, vBaseRotCopy)
		SET_CAM_FOV(camIndex, fBaseFOV)
		
		SET_CAM_NEAR_CLIP(camIndex, BJ_GET_NEAR_CLIP())
	ENDIF
	
	SET_CAM_ACTIVE(camIndex, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

// Update look camera position based on input.
FUNC BOOL BJ_UPDATE_HELI_LOOK_CAMERA(BJ_ARGS& bjArgs, BJ_CAMERA_SET& cameraSet, BJ_INPUT& input, structTimer& inputTimer, VECTOR& vLookCameraRot, VECTOR& vCameraVelocity, VECTOR vFocalPoint, VECTOR vFocalOffset)
	CAMERA_INDEX camIndex = BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM)
	VECTOR vRightStick = BJ_GET_RIGHT_STICK_AS_VECTOR(input)
	
	IF IS_LOOK_INVERTED()
		vRightStick.y *= -1.0
	ENDIF
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		// Apply player input forces to the acceleration values.
		vCameraVelocity.z -= vRightStick.x * GET_FRAME_TIME() * 100.0
		vCameraVelocity.x += vRightStick.y * GET_FRAME_TIME() * 100.0
		
		// Apply dampening to the acceleration values.
		IF ABSF(vCameraVelocity.z) > 0.001
			vCameraVelocity.z -= vCameraVelocity.z * GET_FRAME_TIME() * 4.0
		ELSE
			vCameraVelocity.z = 0.0
		ENDIF
		
		IF ABSF(vCameraVelocity.x) > 0.001
			vCameraVelocity.x -= vCameraVelocity.x * GET_FRAME_TIME() * 5.0
		ELSE
			vCameraVelocity.x = 0.0
		ENDIF
	
	ELSE
	
		// Apply player input forces to the acceleration values.
		vCameraVelocity.z = -vRightStick.x * 130.0
		vCameraVelocity.x = vRightStick.y * 130.0
	
	ENDIF
	
	// Update the look offsets based on our velocity.
	vLookCameraRot.z += vCameraVelocity.z * GET_FRAME_TIME()
	IF vLookCameraRot.z > 0.5 * 43.7465 // Range
		vLookCameraRot.z = 0.5 * 43.7465
		vCameraVelocity.z = 0.0
	ELIF vLookCameraRot.z < -0.5 * 43.7465
		vLookCameraRot.z = -0.5 * 43.7465
		vCameraVelocity.z = 0.0
	ENDIF
	
	vLookCameraRot.x += vCameraVelocity.x * GET_FRAME_TIME()
	IF vLookCameraRot.x > 0.5 * 21.6 // Range
		vLookCameraRot.x = 0.5 * 21.6
		vCameraVelocity.x = 0.0
	ELIF vLookCameraRot.x < -0.5 * 21.6
		vLookCameraRot.x = -0.5 * 21.6
		vCameraVelocity.x = 0.0
	ENDIF
	
	VECTOR vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(vFocalOffset, vLookCameraRot.z)
	
	SET_CAM_COORD(camIndex, vFocalPoint + vCurFocalOffset)
	SET_CAM_ROT(camIndex, <<-33.0000 + vLookCameraRot.x, 0.0, -88.515 + BJ_GET_ARG_LAUNCH_HEADING(bjArgs) + vLookCameraRot.z>>)
	
	SET_CAM_NEAR_CLIP(camIndex, BJ_GET_NEAR_CLIP())
	
	// Keep track of input delay.
	IF NOT IS_SCREEN_FADED_IN()
	OR NOT IS_TIMER_STARTED(inputTimer)
		RESTART_TIMER_NOW(inputTimer)
	
	ELSE
		// Show help text.
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP_FOREVER("BJ_VLOOKHLP")
		ENDIF
		
		// See if the player is jumping.
		IF (IS_TIMER_STARTED(inputTimer) AND GET_TIMER_IN_SECONDS(inputTimer) > fINPUT_DELAY)
			IF BJ_IS_BUTTON_JUST_PRESSED(input, BJBUTTON_VEH_LAUNCH)
				CANCEL_TIMER(inputTimer)
				DETACH_CAM(camIndex)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BJ_SETUP_HELI_JUMP_CAMERA(VEHICLE_INDEX vehHeli, BJ_CAMERA_SET& cameraSet, structTimer& camTimer)
	VECTOR vCameraRot
	
	CAMERA_INDEX camIndex1 = BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM)
	CAMERA_INDEX camIndex2 = BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM)
	
	//SET_CAM_NEAR_CLIP(camIndex1, BJ_GET_NEAR_CLIP())
	SET_CAM_NEAR_CLIP(camIndex2, BJ_GET_NEAR_CLIP())
	
	// This works for The 1.5k if we need to author new cams
//	#IF IS_DEBUG_BUILD
//		VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehHeli, <<1020.0807, 3955.2156, 1357.3297>>)
//		FLOAT fHeading = -31.8206 - GET_ENTITY_HEADING(vehHeli)
//		CERRORLN(DEBUG_MISSION, "Jump cam offset: ", vOffset)
//		CERRORLN(DEBUG_MISSION, "Jump cam heading: ", fHeading)
//	#ENDIF
	
//	SET_CAM_COORD(camIndex1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehHeli, <<1.63971, -1.48999, 2.2019>>))
//	vCameraRot = <<-54.9000, 0.0000, -31.8206>>
//	vCameraRot.z += GET_ENTITY_HEADING(vehHeli)
//	SET_CAM_ROT(camIndex1, vCameraRot)
//	SET_CAM_FOV(camIndex1, 50.0000)
//	SET_CAM_NEAR_CLIP(camIndex1, BJ_GET_NEAR_CLIP())
	
	VECTOR vCamPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehHeli, <<2.0, -0.3, -7.7824>>)
//	VECTOR vCamPos = GET_CAM_COORD(camIndex1)
//	vCamPos.z -= 6.0
	SET_CAM_COORD(camIndex2, vCamPos)
	//SET_CAM_COORD(camIndex2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehHeli, <<1.6552, -3.2150, -5.2824>>))
	
	vCameraRot = GET_CAM_ROT(camIndex1)
	//vCameraRot.z += GET_ENTITY_HEADING(vehHeli)
	vCameraRot.x = 0.0
	SET_CAM_ROT(camIndex2, vCameraRot)
	//SET_CAM_FOV(camIndex2, 50.0000)
	SET_CAM_FOV(camIndex2, GET_CAM_FOV(camIndex1))
	
	//SET_CAM_ACTIVE(camIndex2, FALSE)
	//SET_CAM_ACTIVE(camIndex1, TRUE)
	
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	
	cameraSet.bjCamInterpState = BJCAMERAINTERPSTATE_NOT_STARTED
	RESTART_TIMER_NOW(camTimer)
ENDPROC

// Loop smoothly from scripted to gameplay cam.
FUNC BOOL BJ_UPDATE_HELI_JUMP_CAMERA(BJ_CAMERA_SET& cameraSet, structTimer& camTimer, VECTOR vFocalPoint, VECTOR vFocalOffset, VECTOR vLookCameraRot, VECTOR& vBaseLookCamRot, FLOAT& fBaseLookCamFOV)
	VECTOR vCurFocalOffset
	
	SWITCH cameraSet.bjCamInterpState
		CASE BJCAMERAINTERPSTATE_NOT_STARTED
			IF GET_TIMER_IN_SECONDS(camTimer) >= fJUMP_CAM_START_INTERP
				vLookCameraRot.z = CLAMP(vLookCameraRot.z, -8.9090, 8.9090)
				vBaseLookCamRot.z += vLookCameraRot.z
				vCurFocalOffset = ROTATE_VECTOR_ABOUT_Z(vFocalOffset, vLookCameraRot.z)
				
				SET_CAM_PARAMS(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), vFocalPoint + vCurFocalOffset, vBaseLookCamRot, fBaseLookCamFOV, 300)
				SET_CAM_ACTIVE_WITH_INTERP(BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_SECONDARY_CAM), BJ_GET_CAMERA_OBJECT(cameraSet, BJCAMERAID_PRIMARY_CAM), FLOOR(1000.0 * (fJUMP_CAM_DUR - fJUMP_CAM_START_INTERP)), GRAPH_TYPE_ACCEL)
				cameraSet.bjCamInterpState = BJCAMERAINTERPSTATE_SCRIPTED_INTERP
			ENDIF
		BREAK
		
		CASE BJCAMERAINTERPSTATE_SCRIPTED_INTERP
			IF GET_TIMER_IN_SECONDS(camTimer) >= fJUMP_CAM_GAME_INTERP
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				RENDER_SCRIPT_CAMS(FALSE, TRUE, FLOOR(1000.0 * (fJUMP_CAM_DUR - fJUMP_CAM_GAME_INTERP)), FALSE)
				cameraSet.bjCamInterpState = BJCAMERAINTERPSTATE_INTERP_TO_GAME
			ENDIF
		BREAK
		
		CASE BJCAMERAINTERPSTATE_INTERP_TO_GAME
			IF GET_TIMER_IN_SECONDS(camTimer) >= fJUMP_CAM_DUR
				cameraSet.bjCamInterpState = BJCAMERAINTERPSTATE_FINISHED
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE BJCAMERAINTERPSTATE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
