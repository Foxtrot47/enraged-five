
//////////////////////////////////////////////////////////////////////
/* bj_debug.sch														*/
/* Author: DJ Jones													*/
/* Debug output & drawing for base jump activity.					*/
//////////////////////////////////////////////////////////////////////


// Widgets for viewing and tweaking values.
STRUCT BJ_WIDGETS
	// Widget groups.
	WIDGET_GROUP_ID parentGroup
	WIDGET_GROUP_ID cutsceneGroup
	WIDGET_GROUP_ID cameraGroup
	WIDGET_GROUP_ID launchGroup
	WIDGET_GROUP_ID lookGroup
	WIDGET_GROUP_ID jumpGroup
	WIDGET_GROUP_ID targetGroup
		WIDGET_GROUP_ID targetPropGroup
		WIDGET_GROUP_ID target1Group
		WIDGET_GROUP_ID target2Group
		WIDGET_GROUP_ID target3Group
		WIDGET_GROUP_ID target4Group
		WIDGET_GROUP_ID target5Group
		WIDGET_GROUP_ID target6Group
		WIDGET_GROUP_ID target7Group
		WIDGET_GROUP_ID target8Group
		WIDGET_GROUP_ID target9Group
		WIDGET_GROUP_ID target10Group
		WIDGET_GROUP_ID target11Group
		WIDGET_GROUP_ID target12Group
	WIDGET_GROUP_ID uiGroup
	WIDGET_GROUP_ID debugGroup
	WIDGET_GROUP_ID parachuteGroup
	WIDGET_GROUP_ID audioGroup
	
	// Launch location.
	VECTOR vLaunchPos
	VECTOR vLaunchPosLast
	FLOAT fLaunchHeading
	FLOAT fLaunchHeadingLast
	
	//cam pos rot
	VECTOR vTmpCamPos
	VECTOR vTmpCamRot
	FLOAT fTmpCamFOV
	BOOL bUpdateDebugCam
	
	TEXT_LABEL sParachuteState, sLandingState
	BOOL bUpdateParachuteState, bUpdateLandingState
	BOOL bHitLS1, bHitLS2, bHitLS3, bHitLS4, bHitLS5, bHitLS6
	BOOL bHitPS1, bHitPS2, bHitPS3, bHitPS4, bHitPS5 

	
	// Game state data.
//	INT iGameState
	
	// Debug draw data.
	BOOL bDebugDraw
	BOOL bDebugDrawLast
	BOOL bDrawPlayerToTarget
	BOOL bDrawFullPath
	
	// Audio data.
	BOOL bUseElevationWind
	BOOL bUseCustomWind
ENDSTRUCT


BJ_WIDGETS bjw

PROC BJ_DEBUG_DRAW_LITERAL_STRING(STRING sLiteral, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	INT red, green, blue, iAlpha
	GET_HUD_COLOUR(eColour, red, green, blue, iAlpha)
	
	SET_TEXT_SCALE(0.45, 0.45)
	SET_TEXT_COLOUR(red, green, blue, iAlpha)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.7795, 0.0305*TO_FLOAT(iColumn+1), "STRING", sLiteral)
ENDPROC

PROC BJ_DEBUG_DRAW_LITERAL_STRING_WITH_INT(STRING sLiteral, INT sInt, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += sInt
	
	BJ_DEBUG_DRAW_LITERAL_STRING(sNewLiteral, iColumn, eColour)
ENDPROC

PROC BJ_DEBUG_DRAW_LITERAL_STRING_WITH_FLOAT(STRING sLiteral, FLOAT sFloat, INT iColumn, HUD_COLOURS eColour = HUD_COLOUR_PURE_WHITE)
	TEXT_LABEL_63 sNewLiteral = sLiteral
	sNewLiteral += GET_STRING_FROM_FLOAT(sFloat)
	
	BJ_DEBUG_DRAW_LITERAL_STRING(sNewLiteral, iColumn, eColour)
ENDPROC

FUNC STRING GET_STRING_FROM_PED_LANDING_TYPE(PED_PARACHUTE_LANDING_TYPE thisType)
	SWITCH thisType
		CASE PPLT_INVALID 
			RETURN "PPLT_INVALID"
		BREAK
		CASE PPLT_SLOW 
			RETURN "PPLT_SLOW"
		BREAK
		CASE PPLT_REGULAR 
			RETURN "PPLT_REGULAR"
		BREAK
		CASE PPLT_FAST 
			RETURN "PPLT_FAST"
		BREAK
		CASE PPLT_CRASH 
			RETURN "PPLT_CRASH"
		BREAK
		CASE PPLT_WATER 
			RETURN "PPLT_WATER"
		BREAK
	ENDSWITCH
	
	RETURN "OMGZWTF"
ENDFUNC

FUNC STRING GET_STRING_FROM_PED_PARACHUTE_STATE(PED_PARACHUTE_STATE thisState)
	SWITCH thisState
		CASE PPS_INVALID 
			RETURN "PPS_INVALID"
		BREAK
		CASE PPS_SKYDIVING 
			RETURN "PPS_SKYDIVING"
		BREAK
		CASE PPS_DEPLOYING 
			RETURN "PPS_DEPLOYING"
		BREAK
		CASE PPS_PARACHUTING 
			RETURN "PPS_PARACHUTING"
		BREAK
		CASE PPS_LANDING 
			RETURN "PPS_LANDING"
		BREAK
	ENDSWITCH
	
	RETURN "OMGZWTF"
ENDFUNC

// Create widgets for debugging purposes.
PROC BJ_SETUP_DEBUG(BJ_ARGS& bjArgs, BJ_JUMP_ID eJumpID)
	// Initialize launch location.
	bjw.vLaunchPos = BJ_GET_COURSE_PLAYER_LOCATION_BY_ID(eJumpID)
	bjw.fLaunchHeading = BJ_GET_ARG_LAUNCH_HEADING(bjArgs)
	bjw.vLaunchPosLast = bjw.vLaunchPos
	bjw.fLaunchHeadingLast = bjw.fLaunchHeading
	
	// Initialize debug draw settings.
	bjw.bDrawPlayerToTarget = TRUE
	bjw.bDrawFullPath = TRUE
	bjw.bUpdateDebugCam = FALSE
	
	// Initialize audio settings.
	bjw.bUseElevationWind = FALSE
	bjw.bUseCustomWind = TRUE
	
	// The main widget group.
	bjw.parentGroup = START_WIDGET_GROUP("Base Jumping")
		// Cutscene controls.
		bjw.cutsceneGroup = START_WIDGET_GROUP("Cutscene")
			ADD_WIDGET_FLOAT_SLIDER("Cutscene End Wait Time", fCUTSCENE_END_WAIT, 0.0, 10.0, 0.1)
			ADD_WIDGET_INT_SLIDER("Cutscene Interp Duration", iCUTSCENE_INTERP_DUR, 0, 10000, 10)
			bjw.cameraGroup = START_WIDGET_GROUP("Camera")
				ADD_WIDGET_VECTOR_SLIDER("Cam Pos", bjw.vTmpCamPos, -5000, 5000, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Cam Rot", bjw.vTmpCamRot, -5000, 5000, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Cam FOV", bjw.fTmpCamFOV, 0, 100, 0.1)
				ADD_WIDGET_BOOL("Update Camera", bjw.bUpdateDebugCam)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		// Launch controls.
		bjw.launchGroup = START_WIDGET_GROUP("Launch")
			ADD_WIDGET_FLOAT_SLIDER("Launch Position X", bjw.vLaunchPos.x, -8000.0, 8000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Launch Position Y", bjw.vLaunchPos.y, -8000.0, 8000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Launch Position Z", bjw.vLaunchPos.z, 0.0, 2500.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Launch Heading", bjw.fLaunchHeading, 0.0, 360.0, 0.1)
		STOP_WIDGET_GROUP()
		
		// Look controls.
		bjw.cutsceneGroup = START_WIDGET_GROUP("Look Mode")
			ADD_WIDGET_FLOAT_SLIDER("Horizontal Cam Scalar", fLOOK_SCALAR_HORIZ, 0.0, 1000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Vertical Cam Scalar", fLOOK_SCALAR_VERT, 0.0, 1000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Horizontal Rot Max", fLOOK_MAX_HORIZ, 0.0, 180.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Vertical Rot Max", fLOOK_MAX_VERT, 0.0, 90.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Vertical Rot Min", fLOOK_MIN_VERT, -90.0, 0.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Look Initial Pitch", fLOOK_INIT_PITCH, -180.0, 180.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Look FOV", fLOOK_FOV, 5.0, 120.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Heli Sway Magnitude", fHELI_SWAY_MAG, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Heli Sway Frequency", fHELI_SWAY_FREQ, 0.0, 20.0, 0.1)
		STOP_WIDGET_GROUP()
		
		// Jump controls.
		bjw.jumpGroup = START_WIDGET_GROUP("Jump")
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Start Interp", fJUMP_CAM_START_INTERP, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Game Interp", fJUMP_CAM_GAME_INTERP, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Duration", fJUMP_CAM_DUR, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached X", fHEAD_CAM_X, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached Y", fHEAD_CAM_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached Z", fHEAD_CAM_Z, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached LX", fHEAD_CAM_LX, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached LY", fHEAD_CAM_LY, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Jump Cam Attached LZ", fHEAD_CAM_LZ, -100.0, 100.0, 0.01)
		STOP_WIDGET_GROUP()
		
		// Target controls.
		bjw.targetGroup = START_WIDGET_GROUP("Targets")
			// Properties for all targets.
			bjw.targetPropGroup = START_WIDGET_GROUP("Properties")
				ADD_WIDGET_FLOAT_SLIDER("Air Target Range", fAIR_TGT_RANGE, 0.0, 1000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Air Target Inner Range", fAIR_TGT_INNER_RANGE, 0.0, 1000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Horiz Range", fGROUND_TGT_RG_HORIZ, 0.0, 1000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Vertical Range (above target)", fGROUND_TGT_RG_VERT_ABOVE, 0.0, 50.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Vertical Range (below target)", fGROUND_TGT_RG_VERT_BELOW, -50.0, 0.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Target Distance Dropoff", fNEXT_TGT_DIST_DROPOFF, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Air Target Scale", fAIR_TGT_DRAW_SCALE, 0.0, 200.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Scale Small", fGND_TGT_DRAW_SCALE_S, 0.0, 200.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Scale Medium", fGND_TGT_DRAW_SCALE_M, 0.0, 200.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Ground Target Scale Large", fGND_TGT_DRAW_SCALE_L, 0.0, 200.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("Air Next Target Scale", fAIR_NXT_TGT_SCALE, 0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Ground Next Target Scale", fGND_NXT_TGT_SCALE, 0.0, 2.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Current Blip Scale", fCUR_BLIP_SCALE, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Next Blip Scale", fNEXT_BLIP_SCALE, 0.0, 3.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Finish Reward Multiplier", fFINISH_REWARD_MULT, 0.0, 20.0, 0.1)
				ADD_WIDGET_INT_SLIDER("Target Red", iTARGET_RED, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Target Green", iTARGET_GREEN, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Target Blue", iTARGET_BLUE, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Target Alpha", iTARGET_ALPHA, 0, 255, 1)
				ADD_WIDGET_INT_SLIDER("Gate Reward", iGATE_REWARD, 0, 1000, 1)
				ADD_WIDGET_INT_SLIDER("Finish Reward Base", iFINISH_REWARD_BASE, 0, 1000, 1)
			STOP_WIDGET_GROUP()
			
			// Target 1.
			bjw.target1Group = START_WIDGET_GROUP("Target 1")
				ADD_WIDGET_FLOAT_SLIDER("Target 1 X", bjArgs.vTargetPos[0].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 1 Y", bjArgs.vTargetPos[0].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 1 Z", bjArgs.vTargetPos[0].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 2.
			bjw.target2Group = START_WIDGET_GROUP("Target 2")
				ADD_WIDGET_FLOAT_SLIDER("Target 2 X", bjArgs.vTargetPos[1].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 2 Y", bjArgs.vTargetPos[1].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 2 Z", bjArgs.vTargetPos[1].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 3.
			bjw.target3Group = START_WIDGET_GROUP("Target 3")
				ADD_WIDGET_FLOAT_SLIDER("Target 3 X", bjArgs.vTargetPos[2].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 3 Y", bjArgs.vTargetPos[2].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 3 Z", bjArgs.vTargetPos[2].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 4.
			bjw.target4Group = START_WIDGET_GROUP("Target 4")
				ADD_WIDGET_FLOAT_SLIDER("Target 4 X", bjArgs.vTargetPos[3].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 4 Y", bjArgs.vTargetPos[3].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 4 Z", bjArgs.vTargetPos[3].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 5.
			bjw.target5Group = START_WIDGET_GROUP("Target 5")
				ADD_WIDGET_FLOAT_SLIDER("Target 5 X", bjArgs.vTargetPos[4].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 5 Y", bjArgs.vTargetPos[4].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 5 Z", bjArgs.vTargetPos[4].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 6.
			bjw.target6Group = START_WIDGET_GROUP("Target 6")
				ADD_WIDGET_FLOAT_SLIDER("Target 6 X", bjArgs.vTargetPos[5].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 6 Y", bjArgs.vTargetPos[5].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 6 Z", bjArgs.vTargetPos[5].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 7.
			bjw.target7Group = START_WIDGET_GROUP("Target 7")
				ADD_WIDGET_FLOAT_SLIDER("Target 7 X", bjArgs.vTargetPos[6].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 7 Y", bjArgs.vTargetPos[6].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 7 Z", bjArgs.vTargetPos[6].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 8.
			bjw.target8Group = START_WIDGET_GROUP("Target 8")
				ADD_WIDGET_FLOAT_SLIDER("Target 8 X", bjArgs.vTargetPos[7].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 8 Y", bjArgs.vTargetPos[7].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 8 Z", bjArgs.vTargetPos[7].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 9.
			bjw.target9Group = START_WIDGET_GROUP("Target 9")
				ADD_WIDGET_FLOAT_SLIDER("Target 9 X", bjArgs.vTargetPos[8].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 9 Y", bjArgs.vTargetPos[8].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 9 Z", bjArgs.vTargetPos[8].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 10.
			bjw.target10Group = START_WIDGET_GROUP("Target 10")
				ADD_WIDGET_FLOAT_SLIDER("Target 10 X", bjArgs.vTargetPos[9].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 10 Y", bjArgs.vTargetPos[9].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 10 Z", bjArgs.vTargetPos[9].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 11.
			bjw.target11Group = START_WIDGET_GROUP("Target 11")
				ADD_WIDGET_FLOAT_SLIDER("Target 11 X", bjArgs.vTargetPos[10].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 11 Y", bjArgs.vTargetPos[10].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 11 Z", bjArgs.vTargetPos[10].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
			
			// Target 12.
			bjw.target12Group = START_WIDGET_GROUP("Target 12")
				ADD_WIDGET_FLOAT_SLIDER("Target 12 X", bjArgs.vTargetPos[11].x, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 12 Y", bjArgs.vTargetPos[11].y, -8000.0, 8000.0, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Target 12 Z", bjArgs.vTargetPos[11].z, -500.0, 2500.0, 1.0)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		// UI controls.
		bjw.uiGroup = START_WIDGET_GROUP("UI")
			ADD_WIDGET_FLOAT_SLIDER("Input Delay", fINPUT_DELAY, 0.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Post Win Timeout", fPOST_WIN_TIMEOUT, 0.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Post Loss Timeout", fPOST_LOSS_TIMEOUT, 0.0, 100.0, 1.0)
			ADD_WIDGET_INT_SLIDER("Fade Out Time", iFADE_OUT_TIME, 0, 10000, 10)
			ADD_WIDGET_INT_SLIDER("Fade In Time", iFADE_IN_TIME, 0, 10000, 10)
		STOP_WIDGET_GROUP()
		
		// Debug draw controls.
		bjw.uiGroup = START_WIDGET_GROUP("Debug Draw")
			ADD_WIDGET_BOOL("Enable Debug Draw", bjw.bDebugDraw)
			ADD_WIDGET_BOOL("Draw Player to Target", bjw.bDrawPlayerToTarget)
			ADD_WIDGET_BOOL("Draw Full Path", bjw.bDrawFullPath)
		STOP_WIDGET_GROUP()

		// Parachute/Landing info
		bjw.parachuteGroup = START_WIDGET_GROUP("Parachute States")
			ADD_WIDGET_BOOL("Update current parachute state:", bjw.bUpdateParachuteState)
			ADD_TEXT_WIDGET(bjw.sLandingState)
			ADD_WIDGET_BOOL("Hit PPS_INVALID", 		bjw.bHitPS1)
			ADD_WIDGET_BOOL("Hit PPS_SKYDIVING", 	bjw.bHitPS2)
			ADD_WIDGET_BOOL("Hit PPS_DEPLOYING", 	bjw.bHitPS3)
			ADD_WIDGET_BOOL("Hit PPS_PARACHUTING",	bjw.bHitPS4)
			ADD_WIDGET_BOOL("Hit PPS_LANDING", 		bjw.bHitPS5)
			
			ADD_WIDGET_BOOL("Update current landing state:", bjw.bUpdateLandingState)
			ADD_TEXT_WIDGET(bjw.sParachuteState)
			ADD_WIDGET_BOOL("Hit PPLT_INVALID", 	bjw.bHitLS1)
			ADD_WIDGET_BOOL("Hit PPLT_SLOW", 		bjw.bHitLS2)
			ADD_WIDGET_BOOL("Hit PPLT_REGULAR", 	bjw.bHitLS3)
			ADD_WIDGET_BOOL("Hit PPLT_FAST", 		bjw.bHitLS4)
			ADD_WIDGET_BOOL("Hit PPLT_CRASH", 		bjw.bHitLS5)
			ADD_WIDGET_BOOL("Hit PPLT_WATER", 		bjw.bHitLS6)
		STOP_WIDGET_GROUP()
		
		// Audio controls.
		bjw.audioGroup = START_WIDGET_GROUP("Audio Options")
			ADD_WIDGET_BOOL("Use Elevation Wind", bjw.bUseElevationWind)
			ADD_WIDGET_BOOL("Use Custom Wind", bjw.bUseCustomWind)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

// Delete all existing bj hockey widgets.
PROC BJ_CLEANUP_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(bjw.debugGroup)
		DELETE_WIDGET_GROUP(bjw.debugGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.uiGroup)
		DELETE_WIDGET_GROUP(bjw.uiGroup)
	ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target9Group)
			DELETE_WIDGET_GROUP(bjw.target9Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target8Group)
			DELETE_WIDGET_GROUP(bjw.target8Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target7Group)
			DELETE_WIDGET_GROUP(bjw.target7Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target6Group)
			DELETE_WIDGET_GROUP(bjw.target6Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target5Group)
			DELETE_WIDGET_GROUP(bjw.target5Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target4Group)
			DELETE_WIDGET_GROUP(bjw.target4Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target3Group)
			DELETE_WIDGET_GROUP(bjw.target3Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target2Group)
			DELETE_WIDGET_GROUP(bjw.target2Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.target1Group)
			DELETE_WIDGET_GROUP(bjw.target1Group)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(bjw.targetPropGroup)
			DELETE_WIDGET_GROUP(bjw.targetPropGroup)
		ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.targetGroup)
		DELETE_WIDGET_GROUP(bjw.targetGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.jumpGroup)
		DELETE_WIDGET_GROUP(bjw.jumpGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.lookGroup)
		DELETE_WIDGET_GROUP(bjw.lookGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.launchGroup)
		DELETE_WIDGET_GROUP(bjw.launchGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.cutsceneGroup)
		DELETE_WIDGET_GROUP(bjw.cutsceneGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.cameraGroup)
		DELETE_WIDGET_GROUP(bjw.cameraGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.parachuteGroup)
		DELETE_WIDGET_GROUP(bjw.parachuteGroup)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(bjw.parentGroup)
		DELETE_WIDGET_GROUP(bjw.parentGroup)
	ENDIF
ENDPROC

// Update widget variable values.
PROC BJ_UPDATE_WIDGETS(BJ_ARGS& bjArgs, INT& iGates, INT iGameState, VEHICLE_INDEX launchVehicle)
	INT iIndex
	
	
	
	// Update game state.
/*	bjw.*/iGameState = iGameState
	
	IF bjw.bUpdateDebugCam
		SET_CAM_COORD(GET_RENDERING_CAM(), bjw.vTmpCamPos)
		SET_CAM_ROT(GET_RENDERING_CAM(), bjw.vTmpCamRot)
		SET_CAM_FOV(GET_RENDERING_CAM(), bjw.fTmpCamFOV)
	ENDIF
	
	IF bjw.bUpdateParachuteState
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
			PED_PARACHUTE_STATE thisPPS = GET_PED_PARACHUTE_STATE(PLAYER_PED_ID())
			bjw.sParachuteState = "PPS: "
			bjw.sParachuteState += GET_STRING_FROM_PED_PARACHUTE_STATE(thisPPS)
			SWITCH thisPPS
				CASE PPS_INVALID
					bjw.bHitPS1 = TRUE
				BREAK				
			    CASE PPS_SKYDIVING
					bjw.bHitPS2 = TRUE
				BREAK
			    CASE PPS_DEPLOYING
					bjw.bHitPS3 = TRUE
				BREAK
			    CASE PPS_PARACHUTING
					bjw.bHitPS4 = TRUE
				BREAK
			    CASE PPS_LANDING
					bjw.bHitPS5 = TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF

	IF bjw.bUpdateLandingState
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			PED_PARACHUTE_LANDING_TYPE thisPPLT = GET_PED_PARACHUTE_LANDING_TYPE(PLAYER_PED_ID())
			bjw.sLandingState = "PPLT is: "
			bjw.sLandingState += GET_STRING_FROM_PED_LANDING_TYPE(thisPPLT)
			SWITCH thisPPLT
				CASE PPLT_INVALID
					bjw.bHitLS1 = TRUE
				BREAK				
			    CASE PPLT_SLOW
					bjw.bHitLS2 = TRUE
				BREAK
			    CASE PPLT_REGULAR
					bjw.bHitLS3 = TRUE
				BREAK
			    CASE PPLT_FAST
					bjw.bHitLS4 = TRUE
				BREAK
			    CASE PPLT_CRASH
					bjw.bHitLS5 = TRUE
				BREAK
				CASE PPLT_WATER
					bjw.bHitLS6 = TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	// Check for a change in launch position.
	IF NOT ARE_VECTORS_EQUAL(bjw.vLaunchPosLast, bjw.vLaunchPos)
		IF DOES_ENTITY_EXIST(launchVehicle)
			SET_ENTITY_COORDS(launchVehicle, bjw.vLaunchPos)
		ENDIF
		bjw.vLaunchPosLast = bjw.vLaunchPos
	ENDIF
	
	// Check for a change in launch heading.
	IF bjw.fLaunchHeadingLast <> bjw.fLaunchHeading
		IF DOES_ENTITY_EXIST(launchVehicle)
			SET_ENTITY_HEADING(launchVehicle, bjw.fLaunchHeading)
		ENDIF
		bjw.fLaunchHeadingLast = bjw.fLaunchHeading
	ENDIF
	
	// Make sure that if gates are added or removed, we update the total gates.
	REPEAT iMAX_GATES iIndex
		IF NOT IS_VECTOR_ZERO(BJ_GET_ARG_TARGET_POSITION(bjArgs, iIndex))
			iGates = iIndex + 1
		ELSE
			iIndex = 999999
		ENDIF
	ENDREPEAT
ENDPROC

// Draw stuff to help.
PROC BJ_DEBUG_DRAW(BJ_ARGS& bjArgs, INT iCurrentGate, INT iGates)
	VECTOR vTargetBottom
	INT i
	
	IF bjw.bDrawPlayerToTarget
		vTargetBottom = BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurrentGate)
		vTargetBottom.z -= fAIR_TGT_RANGE
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), BJ_GET_ARG_TARGET_POSITION(bjArgs, iCurrentGate))
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTargetBottom, 255, 50, 0, 255)
	ENDIF
	
	FOR i = iCurrentGate TO iGates - 1
		IF bjw.bDrawFullPath AND i > iCurrentGate
			DRAW_DEBUG_LINE(BJ_GET_ARG_TARGET_POSITION(bjArgs, i-1), BJ_GET_ARG_TARGET_POSITION(bjArgs, i), 255, 0, 255, 255)
		ENDIF
		
		IF i < iGates - 1
			DRAW_DEBUG_SPHERE(BJ_GET_ARG_TARGET_POSITION(bjArgs, i), fAIR_TGT_RANGE, 0, 255, PICK_INT(i = iCurrentGate, 255, 100), 128)
			DRAW_DEBUG_SPHERE(BJ_GET_ARG_TARGET_POSITION(bjArgs, i), fAIR_TGT_INNER_RANGE, 255, 0, PICK_INT( i = iCurrentGate, 255, 100), 128)
		ELSE
			DRAW_MARKER(MARKER_CYLINDER, BJ_GET_ARG_TARGET_POSITION(bjArgs, i), <<1,0,0>>, <<0,0,0>>, <<2.0 * fGROUND_TGT_RG_HORIZ, 2.0 * fGROUND_TGT_RG_HORIZ, 2.0 * fGROUND_TGT_RG_VERT_ABOVE>>, 0, 255, PICK_INT(i = iCurrentGate, 255, 100), 128)
		ENDIF
	ENDFOR
ENDPROC

// Main debug update function.
PROC BJ_UPDATE_DEBUG(BJ_ARGS& bjArgs, INT iGameState, INT iCurrentGate, INT& iGates, VEHICLE_INDEX launchVehicle)
	// Update widgets.
	BJ_UPDATE_WIDGETS(bjArgs, iGates, iGameState, launchVehicle)
	
	// Shortcut buttons.
	//IF IS_BUTTON_JUST_PRESSED(PAD1, LEFTSHOULDER1)
	//	bjw.bDebugDraw = NOT bjw.bDebugDraw
	//ENDIF
	
	// Turn on debug draw and draw.
	IF bjw.bDebugDraw
		IF NOT bjw.bDebugDrawLast
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			bjw.bDebugDrawLast = TRUE
		ENDIF
		BJ_DEBUG_DRAW(bjArgs, iCurrentGate, iGates)
	
	// Turn off debug draw.
	ELIF bjw.bDebugDrawLast
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		bjw.bDebugDrawLast = FALSE
	ENDIF
ENDPROC
