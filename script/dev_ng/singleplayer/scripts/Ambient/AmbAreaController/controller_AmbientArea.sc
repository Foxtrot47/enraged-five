

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// controller_AmbientArea.sc
USING "controller_AmbientArea.sch"

PROC SCRIPT_CLEANUP()	
	TERMINATE_THIS_THREAD()
ENDPROC



SCRIPT
	//PRINTLN("***************************** controller_AmbientArea.sc - STARTING *****************************")
	
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_MAGDEMO | FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINTLN("********** controller_AmbientArea.sc has been forced to cleanup (SP to MP) **********")
		SCRIPT_CLEANUP()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		//In debug builds ambient scripts may be disabled for tests.
		IF g_bDebugBlockAmbientScripts
			PRINTLN("********** controller_AmbientArea.sc has been forced to cleanup. Ambient events disabled on command line **********")
			SCRIPT_CLEANUP()
		ENDIF
	#ENDIF
	
	// Run our setup so we have all the data we need in order to run.
	AMBIENTAREA_INIT_GLOBAL_DATA()
	
	// Which item we're due to update next.
	INT iCurUpdateIndex = 0
	
	WHILE TRUE
		WAIT(AMB_AREA_CONTROLLER_WAIT)
		
		// We're only updating AMB_AREAS_PER_LOOP. This tracks that.
		INT iNumUpdatedThisLoop = 0
		
		// We'll need constant updates on player's position.
		VECTOR vPlayerPos = <<0,0,0>>
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		// Update until (iNumUpdatedThisLoop = AMB_AREAS_PER_LOOP)
		WHILE (iNumUpdatedThisLoop != AMB_AREAS_PER_LOOP)
			//PRINTLN("Updating AmbientArea at index: ", iCurUpdateIndex)
			// Update the event at g_AmbientAreaData[iCurUpdateIndex].
			
			IF AMBIENT_AREA_IS_RUNNING(g_AmbientAreaData[iCurUpdateIndex])
				// We're running:
				IF AMBIENT_AREA_SHOULD_TERMINATE(INT_TO_ENUM(AMBIENT_AREAS, iCurUpdateIndex), vPlayerPos)
					CLEAR_BITMASK_ENUM_AS_ENUM(g_AmbientAreaData[iCurUpdateIndex].ambAreaFlags, eAmbientArea_Running)
					IF (g_AmbientAreaData[iCurUpdateIndex].thisThread != NULL)
						IF IS_THREAD_ACTIVE(g_AmbientAreaData[iCurUpdateIndex].thisThread)
							PRINTLN("Should terminate, killing thread!")
							
							//SCRIPT_ASSERT("Telling the thread to quit!")
							FORCE_CLEANUP_FOR_THREAD_WITH_THIS_ID(g_AmbientAreaData[iCurUpdateIndex].thisThread)
						ENDIF
						g_AmbientAreaData[iCurUpdateIndex].thisThread = NULL
					ENDIF
				ENDIF
			ELSE
				// Not running:
				IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(g_AmbientAreaData[iCurUpdateIndex].ambAreaFlags, eAmbientArea_ForceDeactivate)
					// We don't have the flag to force deactivate this event. Check to see if it should launch.
					IF AMBIENT_AREA_CAN_LAUNCH(g_AmbientAreaData[iCurUpdateIndex], vPlayerPos)
						AMBIENT_AREA_LAUNCH(INT_TO_ENUM(AMBIENT_AREAS, iCurUpdateIndex))
					ENDIF
				ENDIF
			ENDIF
			
			// We've updated one item. Increment our counter.
			iNumUpdatedThisLoop += 1
			iCurUpdateIndex += 1
			
			// If iCurUpdateIndex is out of range, set it back to 0 and start again.
			IF (iCurUpdateIndex = ENUM_TO_INT(NUMBER_OF_AMB_AREAS))
				iCurUpdateIndex = 0
			ENDIF
		ENDWHILE
	ENDWHILE
ENDSCRIPT
