// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	carwash2.sc
//		AUTHOR			:	Aaron Gandaa
//
// *****************************************************************************************
// *****************************************************************************************
         
//----------------------
//	INCLUDES
//----------------------
USING "carwash_shared.sch"
USING "brains.sch"
   
//----------------------
//	VARIABLES
//----------------------
CARWASH_HANDLER carWash
ASSET_REQUESTER asRequest
FLOAT fOriginalDirtLevel
//BOOL bSkipped = FALSE
BOOL bInCarWash = FALSE
BOOL bCarWashBlocked = FALSE
BOOL bHasRoof = FALSE

SHAPETEST_INDEX shapeTest
COST_HALO carWashHalo[2]
COSTHALO_HANDLER carWashHaloHandler
INT iCandidateID = NO_CANDIDATE_ID

FLOAT fCamera2Backshift = 0.75

VECTOR vShapetestOffset	= <<0.1, 0.0, -1.0>>
FLOAT fShapetestWidth	= 2.1
FLOAT fShapetestHeight	= 3.0

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_WidgetGroup
	BOOL bQuitScript = FALSE 
	BOOL bDirtyCar = FALSE 
	BOOL bShowDebug = FALSE
	BOOL bNetPlayersDrivingCarwash[NUM_NETWORK_PLAYERS]
#ENDIF

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
 
/// PURPOSE:
///    Setups Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	INT i 
	TEXT_LABEL lbl
	
	SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	m_WidgetGroup = START_WIDGET_GROUP("Carwash")	
		ADD_WIDGET_BOOL("Quit Script", bQuitScript)
		ADD_WIDGET_BOOL("Dirty Car", bDirtyCar)	
		ADD_WIDGET_BOOL("Blocked", bCarWashBlocked)	
		ADD_WIDGET_BOOL("Halo Custom", carWashHalo[0].bCustomCheck)	
		ADD_WIDGET_BOOL("Show Debug", bShowDebug)
		ADD_WIDGET_BOOL("Has Roof", bHasRoof)
		ADD_WIDGET_FLOAT_SLIDER("Camera 2 Shift", fCamera2Backshift, -2.0, 2.0, 0.05)

		ADD_WIDGET_FLOAT_SLIDER("Box offset x", vShapetestOffset.x, -3.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box offset z", vShapetestOffset.z, -3.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box size x", fShapetestWidth, 0.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box size z", fShapetestHeight, 0.0, 3.0, 0.1)

		SETUP_DEBUG_CARWASH_HANDLER_WIDGETS(carWash)
		SETUP_DEBUG_CARWASH_CONSTANT_WIDGETS()
		START_WIDGET_GROUP("Net Carwash Drivers")
			ADD_WIDGET_BOOL("In Carwash", bInCarWash)	
			REPEAT COUNT_OF(bNetPlayersDrivingCarwash) i
				lbl = "Player" 
				lbl += i
				ADD_WIDGET_BOOL(lbl, bNetPlayersDrivingCarwash[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC 

#ENDIF
 
//----------------------
//	SCRIPT FUNCTIONS
//----------------------

PROC SCRIPT_CLEANUP(BOOL bSkip = FALSE)
	DELETE_CARWASH_HANDLER(carWash)
	CPRINTLN(DEBUG_MISSION, "Carwash Cleanup")
	SHARED_CARWASH_SCRIPT_CLEANUP(carWash, bSkip)
	UNLOAD_REQUESTED_ASSETS(asRequest)
	IF (g_bInMultiplayer = FALSE)
		MISSION_OVER(iCandidateID)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC SETUP_CAMERA_2()
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	
	VECTOR vMin, vMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vMin, vMax)
	IF vMin.y < -3.8
		SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[2], <<-699.9690, -929.1818, 19.2126>>, <<-0.8962, -0.0038, -179.6509>>, <<-699.9516, -932.0400, 19.1679>>, <<-0.8962, -0.0038, -179.6509>>, 6500, 1.5)
	ELSE
		SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[2], <<-700.0016, -932.0601, 18.7178>>, <<-8.6749, 0.0000, -179.6145>>, <<-700.0532, -936.7521, 18.6515>>, <<3.9366, -0.0000, 179.6510>>, 6500, 1.5)
	ENDIF
	
	carWash.carWashCamera[2].vPosition[0].y += fCamera2Backshift
	carWash.carWashCamera[2].vPosition[1].y += fCamera2Backshift
	
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[2], 50.0)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[2], 1.107178)
ENDPROC

PROC SCRIPT_SETUP()

	// setup the track and cameras
	SETUP_CARWASH_TRACK(carWash.cTrack, <<-700.1, -922.2, 18.4>>, <<-700.1, -943.6, 18.7>>) 
	SETUP_CARWASH_BLOCK_POINTS(carWash.cTrack, <<-700.1, -926.6, 18.7>>, <<-699.934387,-939.570618,18.014494>>)
	SETUP_CARWASH_BLOCK_MIN_MAX(carWash.cTrack, <<-699.93677, -940.40271, 20>>, <<-699.87805, -926.03876, 20>>)
	CALCULATE_MIN_MAX_FROM_POSITION_AND_RADIUS(<<-701.149963,-947.121887,18.491688>>, 6.20, carWash.vNoPedWalkMin, carWash.vNoPedWalkMax)
	
	SET_CARWASH_WAYPOINT_REC(carWash, "carwash2_r")
	carWash.fFinalCameraInterpolate = 0.6
	carWash.iLocationID = CARWASH_LOCID_SHORT
	 
	carWash.cTrack.vVehicleSnapPoint = <<-700.0682, -921.1490, 18.7028>>
	
	carWash.fBoostSpeed = 4.0
	
	// setup camera
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[0], <<-701.0261, -918.9723, 21.3734>>, <<1.9948, -0.0000, -121.2536>>, <<-701.1696, -918.9987, 20.3130>>, <<-2.3997, -0.0000, -166.3155>>, PICK_INT(g_bInMultiplayer, 5000, 8500))
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[0], 50.0)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[0], 0.105457)
	
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[1], <<-701.5583, -942.6389, 20.2026>>, <<-4.1156, 0.0000, -23.5382>>, <<-698.8515, -942.3990, 20.1769>>, <<-4.1156, 0.0000, 16.0096>>, 8500)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[1], 50.0)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[1], 0.74) // 0.676683)
	
	IS_ENTITY_OK(PLAYER_PED_ID()) // need to entity alive check before doing this or we get asserts
		
	// in mp this script is setup as soon as the player is in range of the carwash as multiple people can see it running
	// so we need to force the camera when the player is actually in the carwash and not now so it takes into account their vehicle
	IF NOT g_bInMultiplayer
		SETUP_CAMERA_2()
	ENDIF
	
	//SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[6], <<-699.0652, -951.9319, 18.9507>>, <<7.8579, -0.0000, 8.7840>>, <<-698.817, -952.476, 19.427>>, <<0.7437, 0.0000, 23.8817>>, 7200, 0.45)
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[6], <<-699.0652, -951.9319, 18.9507>>, <<7.8579, -0.0000, 8.7840>>, <<-698.650, -952.932, 19.260>>, <<0.7437, 0.0000, 23.8817>>, 7200, 0.45)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[6], 40.0220)
	
	// snap car into place if sp
	//IF NOT g_bInMultiplayer
		//SNAP_VEHICLE_TO_CARWASH_START(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), carWash, FALSE)
	//ENDIF
	
	// Figure out if we're seated high above the 
	VECTOR vHeadPos = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
	FLOAT fGroundHeight
	IF GET_GROUND_Z_FOR_3D_COORD(vHeadPos, fGroundHeight) AND vHeadPos.z - fGroundHeight > 1.35
		carWash.bTallVehicle = TRUE
	ENDIF
	   
	// if not in multiplayer we need to setup camera before switching ipls
	IF NOT g_bInMultiplayer
		ENABLE_CAMERA_STRUCT(carWash.carwashCamera[0], carWash.cameraID, carWash.bTallVehicle, TRUE)
	ENDIF
	
	SET_CARWASH_IPL(carWash, BUILDINGNAME_IPL_CARWASH_SHORT)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REQUEST_AND_LOAD_CARWASH_ASSETS_MP(asRequest, carWash.sWayPointRec)
	ELSE
		REQUEST_AND_LOAD_CARWASH_ASSETS(asRequest, carWash.sWayPointRec)
	ENDIF
	
	CREATE_CARWASH_JETS(carWash.soapJets, <<-699.96997, -927.7, 20.8279>>, 0.0, 4.85, FALSE, 0.3)
	SET_CARWASH_JETS_PARTICLES(carWash.soapJets, "ent_amb_car_wash_jet_soap")
	CARWASH_NETWORK_WAIT()
	 

	//CREATE_CARWASH_VERTROLLER(carWash.vertRoller, <<-699.96997, -935.0, 19.9>>, 180.0, 3.6, 0.2)
	CREATE_CARWASH_VERTROLLER(carWash.vertRoller, << -699.97, -935, 17.9 >>, 180.0, 3.6, 0.2)  // 18.2145
	
	CREATE_CARWASH_HROLLER(carWash.horzRoller, <<-699.96997, -931.7, 21.3>>, 0.0, 18.564493)
	CARWASH_NETWORK_WAIT()
	
	CREATE_CARWASH_JETS(carWash.waterJets, <<-699.96997, -938.8, 20.8279>>, 180.0, 4.85, FALSE, 0.3)
	SET_CARWASH_JETS_PARTICLES(carWash.waterJets, "ent_amb_car_wash_jet")
	CARWASH_NETWORK_WAIT()
	
	RELEASE_CARWASH_MODEL_ASSETS(asRequest)
	
	// Disable car wash scenarios
	SET_SCENARIO_TYPE_ENABLED("DRIVE", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)

	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF	
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CPRINTLN(DEBUG_MISSION, "[CARWASH] - BANK:", NETWORK_GET_VC_BANK_BALANCE(), " WALLET:", NETWORK_GET_VC_WALLET_BALANCE(), " BALANCE:", NETWORK_GET_VC_BALANCE())
	ENDIF
	
ENDPROC

//----------------------
//	UPDATE FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    	Updates Debug Widgets and draw debug stuff
PROC UPDATE_DEBUG_WIDGETS()
	INT i
	VEHICLE_INDEX veh 

	IF (bQuitScript)
		SCRIPT_CLEANUP()
		bQuitScript = FALSE
	ENDIF
	
	IF (bDirtyCar)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			IF IS_ENTITY_OK(veh)
				 SET_VEHICLE_DIRT_LEVEL(veh, 14.0)
			ENDIF
		ENDIF
		bDirtyCar = FALSE
	ENDIF
	
	IF (bShowDebug)
		DRAW_DEBUG_CARWASH(carWash)
	ENDIF
	
	// tell us who is driving in the carwash
	REPEAT COUNT_OF(bNetPlayersDrivingCarwash) i
		bNetPlayersDrivingCarwash[i] = GlobalPlayerBD[i].bDrivingThroughCarwash
	ENDREPEAT
ENDPROC

#ENDIF

FUNC BOOL SINGLEPLAYER_CANDIDATE_ID_CHECK()
	INT iTimer = GET_GAME_TIMER() + 100
	
	WHILE (GET_GAME_TIMER() < iTimer)
		CPRINTLN(DEBUG_MISSION, "Carwash - Candidate Check", iTimer - GET_GAME_TIMER())
		
		SWITCH Request_Mission_Launch(iCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_MINIGAME)
			CASE MCRET_ACCEPTED
				CPRINTLN(DEBUG_MISSION, "Carwash - Candidate Accepted")
		    	RETURN TRUE
		  	CASE MCRET_DENIED
				CPRINTLN(DEBUG_MISSION, "Carwash - Candidate Denied")
		    	RETURN FALSE
		ENDSWITCH
		WAIT(0)
	ENDWHILE
	
	RETURN FALSE
ENDFUNC

PROC SINGLEPLAYER_SCRIPT()
	VEHICLE_INDEX washVehicleID
	
	IS_ENTITY_OK(PLAYER_PED_ID())
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_MISSION, "Carwash is shutting down player is not in vehicle")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "Carwash trying to get candidate ID")
	iCandidateID = NO_CANDIDATE_ID
	IF NOT SINGLEPLAYER_CANDIDATE_ID_CHECK()
		SCRIPT_CLEANUP()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	RC_START_CUTSCENE_MODE(<<0, 0, 0>>, DEFAULT, DEFAULT, DEFAULT, FALSE)
	STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
	SCRIPT_SETUP()
	CLEAR_HELP()
	
	washVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	carwash.fForwardSpeed = 1.75
	SET_VEHICLE_THROUGH_CARWASH(washVehicleID, carwash)
	fOriginalDirtLevel = GET_VEHICLE_DIRT_LEVEL(washVehicleID)
	bHasRoof = DOES_VEHICLE_HAVE_ROOF(washVehicleID)
	
	WHILE (TRUE)
	
		IF NOT IS_ENTITY_OK(washVehicleID) OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR NOT IS_VEHICLE_DRIVEABLE(washVehicleID)
			REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			CHARGE_PLAYER_FOR_CARWASH(CARWASH_LOCID_SHORT)
			CPRINTLN(DEBUG_MISSION, "Carwash - Emergency Breakout - Car wash car is not okay")
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
				SCRIPT_CLEANUP()
			ENDIF	
			
			UPDATE_DEBUG_WIDGETS()
		#ENDIF	
		
		DISABLE_CONTROL_ACTIONS_FOR_CARWASH()
		UPDATE_CARWASH_PARTS(carWash)
		UPDATE_VEHICLE_THROUGH_CARWASH(carwash, washVehicleID, fOriginalDirtLevel)
		
		IF UPDATE_CARWASH_HANDLER_CAMERAS(carwash, washVehicleID)
			RC_END_CUTSCENE_MODE()
			CPRINTLN(DEBUG_MISSION, "Carwash - Wash Program Complete")
			REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			
			IF IS_SCREEN_FADED_OUT()
				SCRIPT_CLEANUP(TRUE)
			ELSE
				CHARGE_PLAYER_FOR_CARWASH(CARWASH_LOCID_SHORT)
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF		
		
		// handle quitting
		IF (carwash.bSkipped = FALSE)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				SAFE_FADE_SCREEN_OUT_TO_BLACK(500, FALSE)
				carwash.bSkipped = TRUE
				REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			ENDIF
		ELIF IS_SCREEN_FADED_OUT()
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDPROC

PROC MULTIPLAYER_SCRIPT()
	INT iNextClosedTime
	BOOL bNoWay
	BOOL bOldInCarWash
	VEHICLE_INDEX veh
	VECTOR vPlayerLocTest
	 
	SHAPETEST_STATUS sTestStatus
	ENTITY_INDEX hitentity
	VECTOR vhit, vnorm
	INT ihits
	
	CPRINTLN(DEBUG_MISSION, "[CARWASH] - Waiting for screen to be faded in and transition to be over")
	WHILE IS_SCREEN_FADED_OUT() OR IS_TRANSITION_ACTIVE()
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_MISSION, "[CARWASH] - Done Waiting for screen to be faded in and transition to be over")
	
	SCRIPT_SETUP()
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	// setup cost halo
	SETUP_COST_HALO(carWashHalo[0], HALO_VEHICLE, "", GET_POSITION_FROM_STATIC_BLIP(STATIC_BLIP_AMBIENT_CARWASH_SHORT), 15, "", FALSE, 2.5)		// short
	SETUP_COST_HALO_DIRECTION_TOLERANCE(carWashHalo[0], carWash.cTrack.vDirection, CARWASH_HEAD_TOLERANCE)
	SET_STRINGS_FOR_CARWASH_HALO_HANDLER(carWashHaloHandler, TRUE)
	carWash.fForwardSpeed = 1.5
	
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID())
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				CPRINTLN(DEBUG_MISSION, "MP Launcher Carwash: This MP Thread should Terminate")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	// need to turn back control here just incase
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF	
		
		// check vehicle validity and update track interpolate
		bNoWay = FALSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			bNoWay = IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH(veh) OR IS_ANY_VEHICLE_MAIN_DOOR_BROKEN(veh)
			bHasRoof = DOES_VEHICLE_HAVE_ROOF(veh)  
			DUMMY_REFERENCE_BOOL(bHasRoof)
		ENDIF
		 
		UPDATE_CARWASH_PARTS(carWash)
		
		// update halos	
		bOldInCarWash = bInCarWash
		bInCarWash = IS_PED_IN_CAR_WITH_CARWASH_DRIVER(PLAYER_PED_ID())
		IF (bOldInCarWash = FALSE) AND (bInCarWash = TRUE)
			CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST ENTERED THE CARWASH")
		ENDIF
		
		IF (bOldInCarWash = TRUE) AND (bInCarWash = FALSE)
			CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST LEFT THE CARWASH")
		ENDIF
		//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: RUNNING CARWASH bNoWay: ",bNoWay," bHasRoof: ",bHasRoof )
		IF (bInCarwash)
			// force camera state to 0
			IF (bOldInCarWash <> bInCarWash)
				carwash.iCurrentCameraState = 0
				SETUP_CAMERA_2()
			ENDIF
			
			UPDATE_VEHICLE_THROUGH_CARWASH(carwash, veh, fOriginalDirtLevel)
			IF UPDATE_CARWASH_HANDLER_CAMERAS(carwash, veh)
				IF (carwash.bCarwashAborted = FALSE)
					CHARGE_PLAYER_FOR_CARWASH(CARWASH_LOCID_SHORT)
				ENDIF
				REMOVE_CARWASH_STUCK_CHECK(veh)
				STOP_ALL_CARWASH_SOUNDS(carwash)
				
				FORCE_CLEAR_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	// need to turn back control here just incase
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ELSE // handle opening
			SHUTDOWN_CARWASH_CAMERA(carWash.cameraID, TRUE)
			FORCE_CLEAR_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID())
			//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: CHECKING ENTRY")
			IF (bOldInCarWash = TRUE) AND (bInCarWash = FALSE)
				CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST LEFT THE CARWASH")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
				
			IF IS_PLAYER_IN_COST_HALO(carWashHalo[0]) //AND AM_I_DRIVING_CLOSEST_CAR_TO_CARWASH(carWashHalo[0], vehCheck)
				//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IN CORONA")
				// Calculate shapetest box
				VECTOR 	vShapetestCentre	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS((carWash.cTrack.vBlockMinMax[0] + carWash.cTrack.vBlockMinMax[1]) / 2.0, carWash.cTrack.fTrackHeading, vShapetestOffset)
				VECTOR	vShapetestDimension	= <<fShapetestWidth, VDIST(carWash.cTrack.vBlockMinMax[0], carWash.cTrack.vBlockMinMax[1]), fShapetestHeight>>
				FLOAT	fShapetestHeading	= carWash.cTrack.fTrackHeading
				
				// Debug draw shapetest box
				#IF IS_DEBUG_BUILD
					IF bShowDebug
						DRAW_DEBUG_BOX_WITH_HEADING(vShapetestCentre, vShapetestDimension, fShapetestHeading)
					ENDIF
				#ENDIF

				// Check shapetest box
				IF (shapeTest = NULL)
					IF (GET_GAME_TIMER() > iNextClosedTime)
//						shapeTest = START_SHAPE_TEST_CAPSULE(carWash.cTrack.vBlockMinMax[0], carWash.cTrack.vBlockMinMax[1], 2.0, SCRIPT_INCLUDE_VEHICLE, veh)
						shapeTest = START_SHAPE_TEST_BOX(vShapetestCentre, vShapetestDimension, <<0.0, 0.0, fShapetestHeading>>, EULER_XYZ, SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT, veh)
					ENDIF
				ELSE
					sTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, ihits, vhit, vnorm, hitentity)
					IF (sTestStatus = SHAPETEST_STATUS_RESULTS_READY)
						
						bCarWashBlocked = FALSE
						IF (iHits > 0)
							//DRAW_DEBUG_SPHERE(vhit, 0.0625)
							//DRAW_DEBUG_LINE(vhit + <<0, 0, 3>> , vhit - <<0, 0, 3>>)
							
							IF CAN_THIS_ENTITY_BLOCK_THE_CARWASH(hitentity)
								bCarWashBlocked = TRUE
							ELSE
								SCRIPT_ASSERT("Carwash shapetest: Had to discount hits based on first hit object, might need to reduce size of hit box, otherwise other valid hits could be missed.")
							ENDIF
						ENDIF
						shapeTest = NULL
					ENDIF
					
					IF (sTestStatus = SHAPETEST_STATUS_NONEXISTENT)
						shapeTest = NULL
					ENDIF
					
					iNextClosedTime = GET_GAME_TIMER() + BLOCKED_SHAPE_CHECK_INTERVAL
				ENDIF

				// Print shapetest result object
				#IF IS_DEBUG_BUILD
					IF bShowDebug
						IF DOES_ENTITY_EXIST(hitentity) AND iHits > 0
							TEXT_LABEL_63 tBlockedStatus
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[0], 0.1,										0, 0, 0)
							DRAW_DEBUG_LINE(carWash.cTrack.vBlockMinMax[0], GET_ENTITY_COORDS(hitentity, FALSE),		0, 0, 0)
							DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(hitentity, FALSE), 0.1,									0, 0, 0)
							DRAW_DEBUG_LINE(carWash.cTrack.vBlockMinMax[1], GET_ENTITY_COORDS(hitentity, FALSE),		0, 0, 0)
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[1], 0.1,										0, 0, 0)
							tBlockedStatus = "Blockage = "
							tBlockedStatus += GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(hitentity))
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.6, "STRING", tBlockedStatus)
						ELSE
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[0], 0.1,										0, 200, 100)
						ENDIF
					ENDIF
				#ENDIF
				
			ENDIF
			
			carWashHalo[0].bCustomCheck = bCarWashBlocked
			IF NOT bCarWashBlocked AND IS_PLAYER_IN_COST_HALO(carWashHalo[0])
				INT iCarCount = COUNT_NUMBER_OF_CARS_IN_CARWASH_HALO(carWashHalo[0])
				IF (iCarCount > 1)
					//carWashHalo[0].bCustomCheck = NOT CAN_PLAYER_TRIGGER_CARWASH_HALO_IN_CAR(carWashHalo[0])
				ENDIF
			ENDIF			
			
			IF UPDATE_CARWASH_COSTHALO_HANDLER(carWashHalo, carWashHaloHandler, bNoWay, FALSE, FALSE)
				//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: UPDATE_CARWASH_COSTHALO_HANDLER checked")
				IF IS_ENTITY_OK(veh)
					// mark me as driving into carwash
					CLEAR_CYLINDER_OF_FIRE(carwash.cTrack.vTrackPoint[0], carwash.cTrack.vTrackPoint[1], 3.0, FALSE)
					SNAP_VEHICLE_TO_CARWASH_START(veh, carwash, TRUE)
					SET_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID(), TRUE)
					SET_VEHICLE_THROUGH_CARWASH(veh, carwash)
					fOriginalDirtLevel = GET_VEHICLE_DIRT_LEVEL(veh)
					carwash.iCurrentCameraState = 0
					carwash.bThrottleBoosted = FALSE
					bCarWashBlocked = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		vPlayerLocTest = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			vPlayerLocTest.z = 18.5
		ENDIF
		
		// shut down script when player gets too far
		IF VDIST2(vPlayerLocTest, <<-700.6, -933.4, 18.5>>) > (CAR_WASH_ACTIVE_RANGE*CAR_WASH_ACTIVE_RANGE)
			CPRINTLN(DEBUG_MISSION, "Carwash is terminating out of script range")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDPROC	


//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT
	CPRINTLN(DEBUG_MISSION, "CARWASH SETUP - 101")
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("carwash2")) > 1
    	CPRINTLN(DEBUG_MISSION, "Carwash is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		CPRINTLN(DEBUG_MISSION, "CAR WASH 2:: PROCESS_PRE_GAME started in MP")
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ELSE
		CPRINTLN(DEBUG_MISSION, "CAR WASH 2:: PROCESS_PRE_GAME started in SP")
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		MULTIPLAYER_SCRIPT()
	ELSE
		SINGLEPLAYER_SCRIPT()
	ENDIF
ENDSCRIPT


