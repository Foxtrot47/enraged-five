// *****************************************************************************************
// *****************************************************************************************
// 
//		MISSION NAME	:	carwash1.sc
//		AUTHOR			:	Aaron Gandaa
//
// *****************************************************************************************
// *****************************************************************************************
          
//----------------------
//	INCLUDES
//----------------------
USING "carwash_shared.sch"
USING "brains.sch"
   
//----------------------
//	VARIABLES
//----------------------
CARWASH_HANDLER carWash

ASSET_REQUESTER asRequest
FLOAT fOriginalDirtLevel
// BOOL bSkipped = FALSE
BOOL bInCarWash = FALSE
BOOL bCarWashBlocked = FALSE
BOOL bHasRoof = FALSE
BOOL bAudioLoaded = FALSE

SHAPETEST_INDEX shapeTest
COST_HALO carWashHalo[2]
COSTHALO_HANDLER carWashHaloHandler


INT iInitialCutTime = 4000
INT iInitialDelay = 3000

VECTOR vShapetestOffset	= <<0.2, 0.0, -1.4>>//<<0.2, -0.5, 0.0>>
FLOAT fShapetestWidth	= 2.2//2.2
FLOAT fShapetestHeight	= 3.0//2.4
	
//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_WidgetGroup
	BOOL bQuitScript = FALSE 
	BOOL bDirtyCar = FALSE 
	BOOL bShowDebug = FALSE
	BOOL bDebugDeposit = FALSE
	BOOL bNetPlayersDrivingCarwash[NUM_NETWORK_PLAYERS]
	INT iDamageBitField
#ENDIF

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Setups Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	INT i 
	TEXT_LABEL lbl
	
	SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	m_WidgetGroup = START_WIDGET_GROUP("Carwash")	
		ADD_WIDGET_BOOL("Quit Script", bQuitScript)
		ADD_WIDGET_BOOL("Dirty Car", bDirtyCar)	
		ADD_WIDGET_BOOL("Show Debug", bShowDebug)
		ADD_WIDGET_BOOL("Blocked", bCarWashBlocked)	
		ADD_WIDGET_BOOL("In Car Wash", bInCarWash)	
		ADD_WIDGET_BOOL("Halo Custom", carWashHalo[0].bCustomCheck)	
		ADD_WIDGET_BOOL("Debug Deposit", bDebugDeposit)
		ADD_WIDGET_BOOL("Has Roof", bHasRoof)
		ADD_WIDGET_INT_SLIDER("Initial Time", iInitialCutTime, 1000, 10000, 100)
		ADD_WIDGET_INT_SLIDER("Initial Delay", iInitialDelay, 0, 10000, 100)
		ADD_WIDGET_FLOAT_SLIDER("Box offset x", vShapetestOffset.x, -3.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box offset z", vShapetestOffset.z, -3.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box size x", fShapetestWidth, 0.0, 3.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("Box size z", fShapetestHeight, 0.0, 3.0, 0.1)

		ADD_WIDGET_INT_READ_ONLY("Damage Bitfield", iDamageBitField)
		SETUP_DEBUG_CARWASH_HANDLER_WIDGETS(carWash)
		SETUP_DEBUG_CARWASH_CONSTANT_WIDGETS()
		START_WIDGET_GROUP("Net Carwash Drivers")
			ADD_WIDGET_BOOL("In Carwash", bInCarWash)	
			REPEAT COUNT_OF(bNetPlayersDrivingCarwash) i
				lbl = "Player" 
				lbl += i
				ADD_WIDGET_BOOL(lbl, bNetPlayersDrivingCarwash[i])
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC 

#ENDIF

//----------------------
//	SCRIPT FUNCTIONS
//----------------------

PROC SCRIPT_CLEANUP(BOOL bSkip = FALSE)
	DELETE_CARWASH_HANDLER(carWash)
	CPRINTLN(DEBUG_MISSION, "Carwash Cleanup")
	SHARED_CARWASH_SCRIPT_CLEANUP(carWash, bSkip)
	UNLOAD_REQUESTED_ASSETS(asRequest)
	TERMINATE_THIS_THREAD()
ENDPROC
   
PROC SCRIPT_SETUP()

	// setup the track and cameras
	CPRINTLN(DEBUG_MISSION, "[CARWASH] - CARWASH 1 - SCRIPT_SETUP")
	SETUP_CARWASH_TRACK(carWash.cTrack, <<47.6782, -1391.8621, 28.4115>>, <<-5.4552, -1391.8621, 28.2989>>) 
	SETUP_CARWASH_BLOCK_POINTS(carWash.cTrack, <<45.1, -1391.9, 28.7>>, <<1.1, -1391.9, 28.7>>)
	SETUP_CARWASH_BLOCK_MIN_MAX(carWash.cTrack, <<-1.61825, -1392.04785, 30>>, <<48.67, -1392.05, 30.00>>)
	CALCULATE_MIN_MAX_FROM_POSITION_AND_RADIUS( <<-7.030046,-1392.319824,28.336014>>, 5.25, carWash.vNoPedWalkMin, carWash.vNoPedWalkMax)
	
	SET_CARWASH_WAYPOINT_REC(carWash, "carwash1")
	carWash.fFinalCameraInterpolate = 0.8
	carWash.iLocationID = CARWASH_LOCID_LONG
	
	carWash.cTrack.vVehicleSnapPoint = <<58.3062, -1392.1101, 28.9928>>
	
	// setup camera
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[0], <<57.0218, -1388.3550, 31.8075>>, <<17.5680, -0.0024, 117.8700>>, <<56.4795, -1388.6339, 30.1578>>, <<-4.4388, -0.0024, 117.2989>>, PICK_INT(g_bInMultiplayer, iInitialCutTime, 7000))
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[0], 34.4668)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[0], -0.02) //-0.092466)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[1], <<28.9995, -1390.5526, 30.1807>>, <<-4.2368, -0.0125, -108.3608>>, <<31.8706, -1390.6946, 30.0388>>, <<-1.9457, -0.0125, -107.5000>>, 6000)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[1], 39.9774)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[1], 0.105879)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[2], <<45.3286, -1389.8623, 29.3692>>, <<2.1520, -0.0125, 108.8446>>, <<45.3630, -1392.8889, 29.3736>>, <<2.1520, -0.0125, 81.5301>>, 6000)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[2], 39.9774)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[2], 0.310310)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[3], <<24.6852, -1395.2002, 31.5041>>, <<-14.0934, -0.2150, -45.2810>>, <<22.9172, -1395.2319, 30.0956>>, <<-12.3702, -0.2150, -2.4293>>, 7000)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[3], 28.1555)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[3], 0.548743)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[4], <<27.0372, -1391.9624, 31.6367>>, <<-13.3479, -0.2150, 88.6287>>, <<27.5526, -1391.9666, 29.4647>>, <<1.0232, -0.2150, 89.3674>>, 9000)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[4], 28.1555)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[4], 0.848018)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[5], <<0.5057, -1394.3345, 29.1631>>, <<2.9577, 0.0000, -66.2075>>, <<0.5057, -1394.3345, 29.1631>>, <<0.1806, -0.0000, 20.2611>>, 7500, 0.7)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[5], 28.1555)
	SETUP_CAMERA_SWITCH_RATIO(carWash.carwashCamera[5], 1.096745)
	
	SETUP_CAMERA_STRUCT_INTERPOLATE(carWash.carwashCamera[6], <<-13.0479, -1392.9730, 28.7546>>, <<7.3208, -0.0144, -74.4871>>, <<-12.9594, -1392.9198, 29.2710>>, <<0.1462, -0.0144, -64.7351>>, 6000, 0.45)
	SETUP_CAMERA_STRUCT_FOV(carWash.carwashCamera[6], 39.32082)
	 
	// snap car into place if sp
	//IF NOT g_bInMultiplayer
		//SNAP_VEHICLE_TO_CARWASH_START(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), carWash)
	//ENDIF
	
	// Figure out if we're seated high above the ground
	IS_ENTITY_OK(PLAYER_PED_ID()) // need to entity alive check before doing this...
	
	VECTOR vHeadPos = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0,0,0>>)
	FLOAT fGroundHeight
	IF GET_GROUND_Z_FOR_3D_COORD(vHeadPos, fGroundHeight) AND vHeadPos.z - fGroundHeight > 1.35
		carWash.bTallVehicle = TRUE
	ENDIF
	
	// if not in multiplayer we need to setup camera before switching ipls
	IF NOT g_bInMultiplayer
		ENABLE_CAMERA_STRUCT(carWash.carwashCamera[0], carWash.cameraID, carWash.bTallVehicle, TRUE)
	ENDIF
	 
	// we load the props here
	SET_CARWASH_IPL(carWash, BUILDINGNAME_IPL_CARWASH_LONG)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REQUEST_AND_LOAD_CARWASH_ASSETS_MP(asRequest, carWash.sWayPointRec)
	ELSE
		REQUEST_AND_LOAD_CARWASH_ASSETS(asRequest, carWash.sWayPointRec)
	ENDIF
	// setup carwash parts
	CREATE_CARWASH_JETS(carWash.soapJets, <<37.0, -1391.8398, 30.9>>, 270.0, 4)
	SET_CARWASH_JETS_PARTICLES(carWash.soapJets, "ent_amb_car_wash_jet_soap")
	CARWASH_NETWORK_WAIT()
	
	
	//CREATE_CARWASH_VERTROLLER(carWash.vertRoller, <<20.9, -1391.8398, 28.3279>>, 90.0, 3.0)
	
	CREATE_CARWASH_VERTROLLER(carWash.vertRoller, << 20.9, -1391.84, 28.3268 >>, 90.0, 3.25)
	CARWASH_NETWORK_WAIT()
	
	CREATE_CARWASH_HROLLER(carWash.horzRoller, <<28.3, -1391.8398, 30.8>>, 90.0, 28.911366)
	CARWASH_NETWORK_WAIT()
	
	CREATE_CARWASH_JETS(carWash.waterJets, <<13.65, -1391.8398, 30.9>>, 90.0, 4)
	SET_CARWASH_JETS_PARTICLES(carWash.waterJets, "ent_amb_car_wash_jet")
	CARWASH_NETWORK_WAIT()
	
	CREATE_CARWASH_JETS(carWash.steamJets, <<6.692545,-1391.8398,32.214699>>, 90.0, 5.23, TRUE)
	SET_CARWASH_JETS_PARTICLES(carWash.steamJets, "ent_amb_car_wash_steam")
	CARWASH_NETWORK_WAIT()
	
	RELEASE_CARWASH_MODEL_ASSETS(asRequest)
	
	// Disable car wash scenarios
	SET_SCENARIO_TYPE_ENABLED("DRIVE", FALSE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)

	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF	
	
ENDPROC

//----------------------
//	UPDATE FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    	Updates Debug Widgets and draw debug stuff
PROC UPDATE_DEBUG_WIDGETS()
	INT i
	VECTOR vcMin, vcMax
	VEHICLE_INDEX veh 

	IF (bQuitScript)
		SCRIPT_CLEANUP()
		bQuitScript = FALSE
	ENDIF
	
	IF (bDirtyCar)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			IF IS_ENTITY_OK(veh)
				 SET_VEHICLE_DIRT_LEVEL(veh, 14.0)
			ENDIF
		ENDIF
		bDirtyCar = FALSE
	ENDIF
	
	IF (bShowDebug)
		DRAW_DEBUG_CARWASH(carWash)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		veh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
		IF IS_ENTITY_OK(veh)
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(veh), vcMin, vcMax)
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMin.x, vcMin.y, 0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMax.x, vcMin.y, 0>>))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMax.x, vcMin.y, 0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMax.x, vcMax.y, 0>>))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMax.x, vcMax.y, 0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMin.x, vcMax.y, 0>>))
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMin.x, vcMax.y, 0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, <<vcMin.x, vcMin.y, 0>>))
		ENDIF
		
		UPDATE_VEHICLE_DOOR_DAMAGE_BIT_FIELD(veh, iDamageBitField)
	ENDIF
		
	IF bDebugDeposit
		IF (g_bInMultiplayer) AND NETWORK_IS_GAME_IN_PROGRESS()
			 IF DEPOSIT_VC(COST_CARWASH)
			 	//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_INTERNET_BANK_BALANCE,1)
			 	bDebugDeposit = FALSE
			 ENDIF
		ELSE
			bDebugDeposit = FALSE
		ENDIF
	ENDIF
	
	// tell us who is driving in the carwash
	REPEAT COUNT_OF(bNetPlayersDrivingCarwash) i
		bNetPlayersDrivingCarwash[i] = GlobalPlayerBD[i].bDrivingThroughCarwash
	ENDREPEAT
ENDPROC

#ENDIF

PROC SINGLEPLAYER_SCRIPT()
	VEHICLE_INDEX washVehicleID
	
	IS_ENTITY_OK(PLAYER_PED_ID())
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CPRINTLN(DEBUG_MISSION, "Carwash is shutting down player is not in vehicle")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	RC_START_CUTSCENE_MODE(<<0, 0, 0>>, DEFAULT, DEFAULT, DEFAULT, FALSE)
	STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
	SCRIPT_SETUP()
	CLEAR_HELP()
	
	washVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	SET_VEHICLE_THROUGH_CARWASH(washVehicleID, carwash)
	fOriginalDirtLevel = GET_VEHICLE_DIRT_LEVEL(washVehicleID)
	bHasRoof = DOES_VEHICLE_HAVE_ROOF(washVehicleID)  
	DUMMY_REFERENCE_BOOL(bHasRoof)
	
	WHILE (TRUE)
	
		IF NOT IS_ENTITY_OK(washVehicleID) OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR NOT IS_VEHICLE_DRIVEABLE(washVehicleID)
			REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			CHARGE_PLAYER_FOR_CARWASH()
			CPRINTLN(DEBUG_MISSION, "Carwash - Emergency Breakout - Car wash car is not okay")
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
				SCRIPT_CLEANUP()
			ENDIF	
			
			UPDATE_DEBUG_WIDGETS()
		#ENDIF	
		
		DISABLE_CONTROL_ACTIONS_FOR_CARWASH()
		UPDATE_CARWASH_PARTS(carWash)
		UPDATE_VEHICLE_THROUGH_CARWASH(carwash, washVehicleID, fOriginalDirtLevel)
		
		IF UPDATE_CARWASH_HANDLER_CAMERAS(carwash, washVehicleID)
			RC_END_CUTSCENE_MODE()
			CPRINTLN(DEBUG_MISSION, "Carwash - Wash Program Complete")
			REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			
			IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
				CPRINTLN(DEBUG_MISSION, "Carwash - Wash Program Complete - Someone tried to skip while just as we completed")
				SCRIPT_CLEANUP(TRUE)
			ELSE
				CHARGE_PLAYER_FOR_CARWASH()
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF		
		
		// handle quitting
		IF (carwash.bSkipped = FALSE)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
				SAFE_FADE_SCREEN_OUT_TO_BLACK(500, TRUE)
				carwash.bSkipped = TRUE
				REMOVE_CARWASH_STUCK_CHECK(washVehicleID)
			ENDIF
		ELIF IS_SCREEN_FADED_OUT()
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDPROC

PROC MULTIPLAYER_SCRIPT()
	INT iNextClosedTime
	BOOL bNoWay
	BOOL bOldInCarWash
	
	VEHICLE_INDEX veh 
	VECTOR vPlayerLocTest
	
	SHAPETEST_STATUS sTestStatus
	ENTITY_INDEX hitentity
	VECTOR vhit, vnorm
	INT ihits
	
	//BREAK_ON_NATIVE_COMMAND("CLEAR_AREA_OF_PROJECTILES", FALSE)
	
	CPRINTLN(DEBUG_MISSION, "[CARWASH] - Waiting for screen to be faded in and transition to be over")
	WHILE IS_SCREEN_FADED_OUT() OR IS_TRANSITION_ACTIVE()
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_MISSION, "[CARWASH] - Done Waiting for screen to be faded in and transition to be over")
	
	SCRIPT_SETUP()	
	
	// setup cost halo
	SETUP_COST_HALO(carWashHalo[0], HALO_VEHICLE, "", GET_POSITION_FROM_STATIC_BLIP(STATIC_BLIP_AMBIENT_CARWASH_LONG), 15, "", FALSE, 2.0)		// long
	SETUP_COST_HALO_DIRECTION_TOLERANCE(carWashHalo[0], carWash.cTrack.vDirection, CARWASH_HEAD_TOLERANCE)
	SET_STRINGS_FOR_CARWASH_HALO_HANDLER(carWashHaloHandler, TRUE)
	
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID())

		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				CPRINTLN(DEBUG_MISSION, "MP Launcher Carwash: This MP Thread should Terminate")
				IF IS_PED_IN_CAR_WITH_CARWASH_DRIVER(PLAYER_PED_ID())
					IF IS_ENTITY_OK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
								SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<51.6073, -1401.6653, 28.4046>>)
								SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 344.3805)
							ENDIF
						ENDIF
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	// need to turn back control here just incase
				ENDIF
				
				SCRIPT_CLEANUP()
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF	

		// check vehicle validity and update track interpolate
		bNoWay = FALSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			veh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			bNoWay = IS_VEHICLE_NOT_ALLOWED_IN_CAR_WASH(veh) OR IS_ANY_VEHICLE_MAIN_DOOR_BROKEN(veh)
			bHasRoof = DOES_VEHICLE_HAVE_ROOF(veh)  
		ENDIF
		
		UPDATE_CARWASH_PARTS(carWash)
		UPDATE_EMERGENCY_PLAYER_WARP_OUT_FOR_CARWASH()
		
		// update halos	
		bOldInCarWash = bInCarWash
		bInCarWash = IS_PED_IN_CAR_WITH_CARWASH_DRIVER(PLAYER_PED_ID())
		IF (bOldInCarWash = FALSE) AND (bInCarWash = TRUE)
			CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST ENTERED THE CARWASH")
		ENDIF
		
		IF (bOldInCarWash = TRUE) AND (bInCarWash = FALSE)
			CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST LEFT THE CARWASH")
		ENDIF
		
		//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: RUNNING CARWASH bNoWay: ",bNoWay," bHasRoof: ",bHasRoof )
		
		IF (bInCarwash)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT bAudioLoaded
					IF REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\CARWASH")	
						PRINTLN("[CARWASH] Audio bank loaded")
						bAudioLoaded = True
					ENDIF
				ENDIF
			ENDIF
			// force camera state to 0
			IF (bOldInCarWash <> bInCarWash)
				carwash.iCurrentCameraState = 0
			ENDIF
			
			UPDATE_VEHICLE_THROUGH_CARWASH(carwash, veh, fOriginalDirtLevel)
			IF UPDATE_CARWASH_HANDLER_CAMERAS(carwash, veh)
				IF (carwash.bCarwashAborted = FALSE)
					CHARGE_PLAYER_FOR_CARWASH()
				ENDIF
				REMOVE_CARWASH_STUCK_CHECK(veh)
				
				STOP_ALL_CARWASH_SOUNDS(carwash)
				RESET_COLLISIONS_FOR_CARWASH(carwash, veh)
				FORCE_CLEAR_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				bCarWashBlocked = FALSE
			ENDIF
		ELSE // handle opening
			SHUTDOWN_CARWASH_CAMERA(carWash.cameraID, TRUE)
			FORCE_CLEAR_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID())
			
			IF (bOldInCarWash = TRUE) AND (bInCarWash = FALSE)
				CPRINTLN(DEBUG_MISSION, "[CARWASH]: WE'VE JUST LEFT THE CARWASH")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		
			//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: CHECKING ENTRY")
			IF IS_PLAYER_IN_COST_HALO(carWashHalo[0])
				//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IN CORONA")
				// Calculate shapetest box
				VECTOR 	vShapetestCentre	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS((carWash.cTrack.vBlockMinMax[0] + carWash.cTrack.vBlockMinMax[1]) / 2.0, carWash.cTrack.fTrackHeading, vShapetestOffset)
				VECTOR	vShapetestDimension	= <<fShapetestWidth, VDIST(carWash.cTrack.vBlockMinMax[0], carWash.cTrack.vBlockMinMax[1]), fShapetestHeight>>
				FLOAT	fShapetestHeading	= carWash.cTrack.fTrackHeading
				
				// Debug draw shapetest box
				#IF IS_DEBUG_BUILD
					IF bShowDebug
						DRAW_DEBUG_BOX_WITH_HEADING(vShapetestCentre, vShapetestDimension, fShapetestHeading)
					ENDIF
				#ENDIF

				// Check shapetest box
				IF (shapeTest = NULL)
					IF (GET_GAME_TIMER() > iNextClosedTime)
//						shapeTest = START_SHAPE_TEST_CAPSULE(carWash.cTrack.vBlockMinMax[0], carWash.cTrack.vBlockMinMax[1], 2.0,			SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT, veh)
						shapeTest = START_SHAPE_TEST_BOX(vShapetestCentre, vShapetestDimension, <<0.0, 0.0, fShapetestHeading>>, EULER_XYZ, SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_OBJECT, veh)
					ENDIF
				ELSE
					sTestStatus = GET_SHAPE_TEST_RESULT(shapeTest, ihits, vhit, vnorm, hitentity)
					IF (sTestStatus = SHAPETEST_STATUS_RESULTS_READY)
						
						bCarWashBlocked = FALSE
						IF (iHits > 0)
							//DRAW_DEBUG_SPHERE(vhit, 0.0625)
							//DRAW_DEBUG_LINE(vhit + <<0, 0, 3>> , vhit - <<0, 0, 3>>)
							
							IF CAN_THIS_ENTITY_BLOCK_THE_CARWASH(hitentity)
								bCarWashBlocked = TRUE
							ELSE
								SCRIPT_ASSERT("Carwash shapetest: Had to discount hits based on first hit object, might need to reduce size of hit box, otherwise other valid hits could be missed.")
							ENDIF
						ENDIF
						shapeTest = NULL
					ENDIF
					
					IF (sTestStatus = SHAPETEST_STATUS_NONEXISTENT)
						shapeTest = NULL
					ENDIF
					
					iNextClosedTime = GET_GAME_TIMER() + BLOCKED_SHAPE_CHECK_INTERVAL
				ENDIF

				// Print shapetest result object
				#IF IS_DEBUG_BUILD
					IF bShowDebug
						IF DOES_ENTITY_EXIST(hitentity) AND iHits > 0
							TEXT_LABEL_63 tBlockedStatus
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[0], 0.1,										0, 0, 0)
							DRAW_DEBUG_LINE(carWash.cTrack.vBlockMinMax[0], GET_ENTITY_COORDS(hitentity, FALSE),		0, 0, 0)
							DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(hitentity, FALSE), 0.1,									0, 0, 0)
							DRAW_DEBUG_LINE(carWash.cTrack.vBlockMinMax[1], GET_ENTITY_COORDS(hitentity, FALSE),		0, 0, 0)
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[1], 0.1,										0, 0, 0)
							tBlockedStatus = "Blockage = "
							tBlockedStatus += GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(hitentity))
							DISPLAY_TEXT_WITH_LITERAL_STRING(0.3, 0.6, "STRING", tBlockedStatus)
						ELSE
							DRAW_DEBUG_SPHERE(carWash.cTrack.vBlockMinMax[0], 0.1,								0, 200, 100)
						ENDIF
					ENDIF
				#ENDIF
				
			ENDIF
		
			carWashHalo[0].bCustomCheck = bCarWashBlocked
			IF NOT bCarWashBlocked AND IS_PLAYER_IN_COST_HALO(carWashHalo[0])
				INT iCarCount = COUNT_NUMBER_OF_CARS_IN_CARWASH_HALO(carWashHalo[0])
				IF (iCarCount > 0)
					//carWashHalo[0].bCustomCheck = NOT CAN_PLAYER_TRIGGER_CARWASH_HALO_IN_CAR(carWashHalo[0])
				ENDIF
			ENDIF
			//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: IN CORONA bCarWashBlocked: ",bCarWashBlocked)
			IF UPDATE_CARWASH_COSTHALO_HANDLER(carWashHalo, carWashHaloHandler, bNoWay, FALSE, FALSE)
				//CPRINTLN(DEBUG_MISSION, "[CARWASH] - CDM: UPDATE_CARWASH_COSTHALO_HANDLER checked")
				IF IS_ENTITY_OK(veh)
					// mark me as driving into carwash
					CLEAR_CYLINDER_OF_FIRE(carwash.cTrack.vTrackPoint[0], carwash.cTrack.vTrackPoint[1], 3.0, FALSE)
					SNAP_VEHICLE_TO_CARWASH_START(veh, carwash)
					SET_NET_PED_DRIVING_CARWASH_STATUS(PLAYER_PED_ID(), TRUE)
					SET_VEHICLE_THROUGH_CARWASH(veh, carwash, iInitialDelay) // THIS DELAY IS NEEDED AS WE NEED TO STOP THE CAR FOR A COUPLE OF FRAMES IN CASE THE PLAYER IS REVERSING.
					fOriginalDirtLevel = GET_VEHICLE_DIRT_LEVEL(veh)
					carwash.iCurrentCameraState = 0
					bCarWashBlocked = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		vPlayerLocTest = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			vPlayerLocTest.z = 28.323212
		ENDIF
		
		// shut down script when player gets too far
		IF VDIST2(vPlayerLocTest, <<20.801605,-1392.830444,28.323212>>) > (CAR_WASH_ACTIVE_RANGE*CAR_WASH_ACTIVE_RANGE)
			CPRINTLN(DEBUG_MISSION, "Carwash is terminating out of script range")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDPROC	

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT
	CPRINTLN(DEBUG_MISSION, "CARWASH SETUP - 1141")
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("carwash1")) > 1
    	CPRINTLN(DEBUG_MISSION, "Carwash is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
		CPRINTLN(DEBUG_MISSION, "CAR WASH 1:: PROCESS_PRE_GAME started in MP - X01")
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ELSE
		CPRINTLN(DEBUG_MISSION, "CAR WASH 1:: PROCESS_PRE_GAME started in SP")
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
			SCRIPT_CLEANUP()
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		MULTIPLAYER_SCRIPT()
	ELSE
		SINGLEPLAYER_SCRIPT()
	ENDIF
ENDSCRIPT


