// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Family Scene T1 file for use - family_scene_t1.sc					 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//

USING "dialogue_public.sch"
USING "timer_public.sch"
USING "family_public.sch"

//-	private headers	-//
USING "family_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
	USING "family_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T1_SCENE_MEMBERS
	PS_T1_FLOYD = 0,
	PS_T1_WADE,
	
	MAX_T1_SCENE_MEMBERS,
	NO_T1_SCENE_MEMBER
ENDENUM
ENUM PS_T1_SCENE_VEHICLES
	PS_TV_BLANK,
	
	MAX_SCENE_VEHICLES,
	NO_FAMILY_VEHICLE
ENDENUM

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structFamilyGriefing sFamilyGriefing

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				family_ped[MAX_T1_SCENE_MEMBERS]

PED_INDEX				family_extra_ped[MAX_T1_SCENE_MEMBERS]
MODEL_NAMES				eFamilyExtraPedModel[MAX_T1_SCENE_MEMBERS]
VECTOR					vFamilyExtraPedCoordOffset[MAX_T1_SCENE_MEMBERS]
FLOAT					fFamilyExtraPedHeadOffset[MAX_T1_SCENE_MEMBERS]

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX			family_veh[MAX_SCENE_VEHICLES]
INT						iFamily_enter_veh_stage[MAX_T1_SCENE_MEMBERS]

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			family_prop[MAX_T1_SCENE_MEMBERS]
MODEL_NAMES				eFamilyPropModel[MAX_T1_SCENE_MEMBERS]
PED_BONETAG				eFamilyPropAttachBonetag[MAX_T1_SCENE_MEMBERS]
VECTOR					vFamilyPropAttachOffset[MAX_T1_SCENE_MEMBERS]
VECTOR					vFamilyPropAttachRotation[MAX_T1_SCENE_MEMBERS]

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vFamily_scene_t1_coord
FLOAT				fFamily_scene_t1_head

VECTOR				vFamily_coordOffset[MAX_T1_SCENE_MEMBERS]
FLOAT				fFamily_headOffset[MAX_T1_SCENE_MEMBERS]

	//- Pickups(INT) -//
INT					family_sfxStage[MAX_T1_SCENE_MEMBERS], family_sfxID[MAX_T1_SCENE_MEMBERS]
TEXT_LABEL_63		family_sfxBank[MAX_T1_SCENE_MEMBERS]

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tDialogueAnimDicts[MAX_T1_SCENE_MEMBERS], tDialogueAnimClips[MAX_T1_SCENE_MEMBERS]
TEXT_LABEL			tCreatedTrevor1ConvLabels[5]

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//
BOOL bUpdateInteriorForPlayer = FALSE
structTimer sUpdatePlayerInteriorTimer
INTERIOR_INSTANCE_INDEX iInteriorForPlayer, iInteriorForSafehouse, iInteriorForMember[MAX_T1_SCENE_MEMBERS]

	//- Timers(structTimer) -//
structTimer sSpeechTimer, sFireTimer
INT iRandSpeechCount[MAX_T1_SCENE_MEMBERS]

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//
structPedsForConversation	MyLocalPedStruct

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	family_scene_t1_widget, family_scene_t1_subwidget
TEXT_WIDGET_ID	family_anim_widget[MAX_FAMILY_MEMBER], family_animFlag_widget[MAX_FAMILY_MEMBER]
BOOL			bToggle_Family_Scene_T1_Widget

INT				iCurrentFamilyEvent[MAX_FAMILY_MEMBER]

BOOL			bJumpToFamilyMember[MAX_FAMILY_MEMBER]
FLOAT			fPlayerDebugJumpAngle[MAX_FAMILY_MEMBER], fPlayerDebugJumpDistance[MAX_FAMILY_MEMBER]

FLOAT			fFamily_synch_scenePhase[MAX_FAMILY_MEMBER]

BOOL 			bMove_peds, bDraw_peds, bSave_peds
BOOL 			bMove_vehs
#ENDIF
#IF NOT IS_DEBUG_BUILD
BOOL 			bMove_peds
#ENDIF

	//- other Ints(INT) -//
INT				iFamily_synch_scene[MAX_t1_SCENE_MEMBERS]

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC enumFamilyMember Get_Family_Member_From_Trevor1_Scene_Member(PS_T1_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
		CASE PS_T1_FLOYD
			RETURN FM_TREVOR_1_FLOYD
		BREAK
		CASE PS_T1_WADE
			RETURN FM_TREVOR_1_WADE
		BREAK
		
		CASE MAX_T1_SCENE_MEMBERS
			RETURN MAX_FAMILY_MEMBER
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_MEMBER
ENDFUNC

FUNC PS_T1_SCENE_VEHICLES Get_FamilyScene_Vehicle_From_Trevor1_Scene_Member(PS_T1_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
//		CASE PS_T1_RON		
//			MODEL_NAMES dummyModel
//			eVehPed = PRIVATE_Get_CharList_From_FamilyMember(FM_TREVOR_RON, dummyModel)
//			RETURN PS_TV_DAUGHTER
//		BREAK
		
		CASE MAX_T1_SCENE_MEMBERS
			RETURN MAX_SCENE_VEHICLES
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_VEHICLE
ENDFUNC


#IF IS_DEBUG_BUILD
FUNC STRING Get_String_From_Family_Scene_T1_Scene_Members(PS_T1_SCENE_MEMBERS this_family_scene_t1_member)
	SWITCH this_family_scene_t1_member
		CASE PS_T1_FLOYD
			RETURN "PS_T1_FLOYD"
		BREAK
		CASE PS_T1_WADE
			RETURN "PS_T1_WADE"
		BREAK
		
		CASE MAX_T1_SCENE_MEMBERS
			RETURN "MAX_T1_SCENE_MEMBERS"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Family_Scene_T1_Scene_Members(null) "
ENDFUNC
#ENDIF


//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Family_Scene_T1_Cleanup()
	PRIVATE_GenericFamilySceneCleanup(iFamily_synch_scene,
			family_veh,			family_ped,
			family_prop,		eFamilyPropModel,
			family_extra_ped,	eFamilyExtraPedModel,
			family_sfxStage, family_sfxID, family_sfxBank)
	
	INT iCleanup
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	REPEAT MAX_T1_SCENE_MEMBERS iCleanup
		enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iCleanup))
		IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("cleanup (g_eCurrentFamilyEvent[")
			PRINTSTRING(Get_String_From_FamilyMember(eFamilyMember))
			PRINTSTRING("] =  ")
			PRINTSTRING(Get_String_From_FamilyEvent(NO_FAMILY_EVENTS))
			PRINTSTRING(")")
			PRINTNL()
			#ENDIF
			
			Update_Previous_Event_For_FamilyMember(eFamilyMember)
			g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
		ENDIF
		
		SetFamilyMemberModelAsNoLongerNeeded(eFamilyMember)
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(family_scene_t1_subwidget)
		DELETE_WIDGET_GROUP(family_scene_t1_subwidget)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(family_scene_t1_widget)
		DELETE_WIDGET_GROUP(family_scene_t1_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Initialise_Trevor_Family_Prop_Variables(PS_T1_SCENE_MEMBERS eMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eMember)
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
//		CASE FE_T1_FLOYD_hangs_up_and_wanders
			eFamilyPropModel[eMember]			= PROP_PHONE_ING_03
			eFamilyPropAttachBonetag[eMember]	= BONETAG_PH_R_HAND
			vFamilyPropAttachOffset[eMember]	= <<0,0,0>>
			vFamilyPropAttachRotation[eMember]	= <<0,0,0>>
		BREAK
		
		CASE FE_T1_FLOYD_cleaning
			eFamilyPropModel[eMember] = Prop_Scourer_01
			eFamilyPropAttachBonetag[eMember] = BONETAG_PH_R_HAND
			vFamilyPropAttachOffset[eMember] = <<0,0,0>>
			vFamilyPropAttachRotation[eMember] = <<0,0,0>>
		BREAK
		
		DEFAULT
			eFamilyExtraPedModel[eMember]		= DUMMY_MODEL_FOR_SCRIPT
			vFamilyExtraPedCoordOffset[eMember]	= <<0,0,0>>
			fFamilyExtraPedHeadOffset[eMember]	= 0.0
			
			eFamilyPropModel[eMember]			= DUMMY_MODEL_FOR_SCRIPT
			eFamilyPropAttachBonetag[eMember]	= BONETAG_NULL
			vFamilyPropAttachOffset[eMember]	= <<0,0,0>>
			vFamilyPropAttachRotation[eMember]	= <<0,0,0>>
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Family_Scene_T1_Variables(structFamilyScene thisFamilyScene)
	
	//- enums -//
	//- vectors -//
	vFamily_scene_t1_coord = thisFamilyScene.sceneCoord
	fFamily_scene_t1_head = thisFamilyScene.sceneHead
	
	//- floats -//
	//- ints -//
	//-- structs : PS_T1_STRUCT --//
	
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_1_FLOYD)
	
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_1_FLOYD, g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD],
			vFamily_coordOffset[PS_T1_FLOYD], fFamily_headOffset[PS_T1_FLOYD])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_1_WADE, g_eCurrentFamilyEvent[FM_TREVOR_1_WADE],
			vFamily_coordOffset[PS_T1_WADE], fFamily_headOffset[PS_T1_WADE])
	
	PS_T1_SCENE_MEMBERS eMember
	REPEAT MAX_T1_SCENE_MEMBERS eMember
		Initialise_Trevor_Family_Prop_Variables(eMember)
		family_sfxID[eMember] = iCONST_FAMILY_SFX_INVALID
		iFamily_synch_scene[eMember] = -1
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	VECTOR vFM_TREVOR_1_FLOYD
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_1_FLOYD],	vFM_TREVOR_1_FLOYD)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_1_FLOYD, fPlayerDebugJumpAngle[PS_T1_FLOYD], fPlayerDebugJumpDistance[PS_T1_FLOYD])
	
	VECTOR vFM_TREVOR_1_WADE
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_1_WADE],	vFM_TREVOR_1_WADE)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_1_WADE, fPlayerDebugJumpAngle[PS_T1_WADE], fPlayerDebugJumpDistance[PS_T1_WADE])
	#ENDIF
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Family_Scene_T1()
	INT iSetup
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
//	REPEAT MAX_T1_SCENE_MEMBERS iSetup
//		IF NOT (IS_STRING_NULL(sFamilyAnimDict[iSetup]) OR ARE_STRINGS_EQUAL(sFamilyAnimDict[iSetup], ""))
//			REQUEST_ANIM_DICT(sFamilyAnimDict[iSetup])
//		ENDIF
//	ENDREPEAT
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	//- create any script vehicles -//
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	
	REPEAT MAX_T1_SCENE_MEMBERS iSetup
		iInteriorForMember[iSetup] = INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, -1)	//GET_INTERIOR_FROM_COLLISION(vFamily_scene_t1_coord+vFamily_coordOffset[iSetup])	//GET_INTERIOR_AT_COORDS_WITH_TYPE(vFamily_scene_t1_coord+vFamily_coordOffset[iSetup], "V_Trevors")	//GET_INTERIOR_FROM_fakeyfudge_obj(vSceneMemberCoord)
	ENDREPEAT
	
	//- load additional text and mission text link-//
	//- setup audio malarky for family -//
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "TREVOR")
	
	
	#IF IS_DEBUG_BUILD
	IF NOT g_savedGlobals.sFlow.isGameflowActive
	OR NOT IS_BIT_SET(g_savedGlobals.sFlow.strandSavedVars[STRAND_PROLOGUE].savedBitflags, SAVED_BITS_STRAND_ACTIVATED)
		VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		DOOR_NAME_ENUM eDoor
		REPEAT NUMBER_OF_DOORS eDoor
			DOOR_DATA_STRUCT sData = GET_DOOR_DATA(eDoor)
			
			IF VDIST2(vPlayerCoord, sData.coords) < (100.0*100.0)
				SET_DOOR_STATE(eDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
				
				CPRINTLN(DEBUG_FAMILY, "unlock door ", ENUM_TO_INT(eDoor), " - ", sData.dbg_name)
			ENDIF
		ENDREPEAT
		
		CPRINTLN(DEBUG_FAMILY, "DEBUG - unlock all doors for this scene!")
	ELSE
		CPRINTLN(DEBUG_FAMILY, "DEBUG - dont touch the doors - in flow!!")
	ENDIF
	#ENDIF
ENDPROC


PROC CreateTrevor1FamilyVeh(PS_T1_SCENE_MEMBERS eSceneMember, PS_T1_SCENE_VEHICLES &eSceneVehicle)
	eSceneVehicle = Get_FamilyScene_Vehicle_From_Trevor1_Scene_Member(eSceneMember)
	IF (eSceneVehicle < MAX_SCENE_VEHICLES)
		enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
		
		CreateThisFamilyVehicle(family_veh[eSceneVehicle],
				eFamilyMember,
				vFamily_scene_t1_coord, fFamily_scene_t1_head, FALSE
				
				#IF IS_DEBUG_BUILD
				, family_scene_t1_widget, bMove_vehs
				#ENDIF
				)
	ENDIF
ENDPROC

PROC CreateTrevor1FamilyProp(PS_T1_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
	
	CreateThisFamilyProp(family_prop[eSceneMember], family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_t1_coord, fFamily_scene_t1_head,
			eFamilyPropModel[eSceneMember], eFamilyPropAttachBonetag[eSceneMember],
			vFamilyPropAttachOffset[eSceneMember], vFamilyPropAttachRotation[eSceneMember]
			
			#IF IS_DEBUG_BUILD
			, family_scene_t1_widget, bMove_vehs
			#ENDIF
			)
ENDPROC

PROC CreateTrevor1FamilyExtraPed(PS_T1_SCENE_MEMBERS eSceneMember)
	
	#IF IS_DEBUG_BUILD
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
	#ENDIF
	
	CreateThisFamilyExtraPed(family_extra_ped[eSceneMember], family_ped[eSceneMember], eFamilyExtraPedModel[eSceneMember],
			vFamilyExtraPedCoordOffset[eSceneMember], fFamilyExtraPedHeadOffset[eSceneMember]
			
			#IF IS_DEBUG_BUILD
			, family_scene_t1_widget, bMove_vehs,
			eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember]
			#ENDIF
			)
	
ENDPROC
FUNC BOOL Is_Trevor1_Scene_Member_Active(PS_T1_SCENE_MEMBERS eSceneMember)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
	IF Is_Scene_Member_Active(family_ped[eSceneMember], eFamilyMember,
			vFamily_scene_t1_coord+vFamily_coordOffset[eSceneMember],
			WRAP(fFamily_scene_t1_head+fFamily_headOffset[eSceneMember], 0, 360),
				iInteriorForPlayer, iInteriorForMember[eSceneMember],
			MyLocalPedStruct, ENUM_TO_INT(eSceneMember)+1,
//			sFireTimer,
			RELGROUPHASH_FAMILY_T)
		
		PS_T1_SCENE_VEHICLES eSceneVehicle
		CreateTrevor1FamilyVeh(eSceneMember, eSceneVehicle)
		CreateTrevor1FamilyProp(eSceneMember)
		CreateTrevor1FamilyExtraPed(eSceneMember)
		
		IF (g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS)
		OR (g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY)
			IF (eSceneVehicle < MAX_SCENE_VEHICLES)
				IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
					DELETE_VEHICLE(family_veh[eSceneVehicle])
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(family_prop[eSceneMember])
				IF NOT IS_ENTITY_DEAD(family_extra_ped[eSceneMember])
					IF IS_ENTITY_ATTACHED(family_prop[eSceneMember])
						DETACH_ENTITY(family_prop[eSceneMember])
					ENDIF
				ENDIF
				DELETE_OBJECT(family_prop[eSceneMember])
			ENDIF
			IF DOES_ENTITY_EXIST(family_extra_ped[eSceneMember])
				DELETE_PED(family_extra_ped[eSceneMember])
			ENDIF
			
			DELETE_PED(family_ped[eSceneMember])
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PED_INJURED(family_ped[eSceneMember])
			RETURN TRUE
		ENDIF
	ELSE
		bUpdateInteriorForPlayer = TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Initialises the family widget
PROC Create_Family_Scene_T1_Widget()
	family_scene_t1_widget = START_WIDGET_GROUP("Family Scene T1")
		
		ADD_WIDGET_BOOL("bToggle_Family_Scene_T1_Widget", bToggle_Family_Scene_T1_Widget)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Initialises the mission widget
PROC Create_Family_Scene_T1_Subwidget()
	STRING sFamilyMemberName[MAX_T1_SCENE_MEMBERS]
	
	PS_T1_SCENE_MEMBERS eSceneMember
	REPEAT MAX_T1_SCENE_MEMBERS eSceneMember
		sFamilyMemberName[eSceneMember] = Get_String_From_Family_Scene_T1_Scene_Members(eSceneMember)
	ENDREPEAT
	
	family_scene_t1_subwidget = Create_Family_Scene_Widget_Group(MAX_T1_SCENE_MEMBERS,
			sFamilyMemberName,
//			iCurrentFamilyEvent,
			family_anim_widget, family_animFlag_widget, //sFamilyAnimDict, sFamilyAnim,
			vFamily_coordOffset, fFamily_headOffset,
			bJumpToFamilyMember, 
			fPlayerDebugJumpAngle, fPlayerDebugJumpDistance, fFamily_synch_scenePhase,
			bMove_peds, bDraw_peds, bSave_peds, bEdit_speech_bounds)
	
ENDPROC

PTFX_ID family_ptfx[MAX_T1_SCENE_MEMBERS]
PROC Update_Trevor_Family_Member_Widget(PS_T1_SCENE_MEMBERS eSceneMember, enumFamilyMember eFamilyMember)
	Update_Family_Member_Widget(eFamilyMember,
			//sFamilyAnimDict[eSceneMember], sFamilyAnim[eSceneMember],
			family_anim_widget[eSceneMember],family_animFlag_widget[eSceneMember],
			vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember],
			fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember],
			family_ptfx[eSceneMember], family_sfxStage[eSceneMember], family_sfxID[eSceneMember], family_sfxBank[eSceneMember],
			sSpeechTimer, iRandSpeechCount[eSceneMember], family_prop[eSceneMember])
	Initialise_Trevor_Family_Prop_Variables(eSceneMember)
	SetFamilyMemberComponentVariation(family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
	SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_T, FALSE)
	
	iFamily_enter_veh_stage[eSceneMember] = 0
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
		DETACH_SYNCHRONIZED_SCENE(iFamily_synch_scene[eSceneMember])
		iFamily_synch_scene[eSceneMember] = -1
	ENDIF
	
	tDialogueAnimDicts[eSceneMember] = ""
	tDialogueAnimClips[eSceneMember] = ""
	
	INT iConv
	REPEAT COUNT_OF(tCreatedTrevor1ConvLabels) iConv
		tCreatedTrevor1ConvLabels[iConv] = ""
	ENDREPEAT
	
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Family_Scene_T1_Widget()
	IF NOT bToggle_Family_Scene_T1_Widget
		IF DOES_WIDGET_GROUP_EXIST(family_scene_t1_subwidget)
			DELETE_WIDGET_GROUP(family_scene_t1_subwidget)
		ENDIF
		
		IF g_bUpdatedFamilyEvents
//		AND g_iDebugSelectedFriendConnDisplay > 0
			bToggle_Family_Scene_T1_Widget = TRUE
		ENDIF
	ELSE
		IF NOT DOES_WIDGET_GROUP_EXIST(family_scene_t1_subwidget)
			
//			enumFamilyMember eFamMem
//			TEXT_LABEL_63 str
			
			SET_CURRENT_WIDGET_GROUP(family_scene_t1_widget)
				Create_Family_Scene_T1_Subwidget()
			CLEAR_CURRENT_WIDGET_GROUP(family_scene_t1_widget)
		ELSE
	
			INT iPed
			
			IF bMove_peds
			
				REPEAT MAX_T1_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iPed))
					
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						IF IS_PED_IN_ANY_VEHICLE(family_ped[iPed])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[iPed])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_t1_coord+vFamily_coordOffset[iPed])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_t1_head+fFamily_headOffset[iPed])
							ENDIF
						ELSE
							VECTOR vFamMemCoord = vFamily_scene_t1_coord+vFamily_coordOffset[iPed]
							VECTOR vFamMemCoordGround = vFamMemCoord
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[iPed])
								IF (GET_SCRIPT_TASK_STATUS(family_ped[iPed], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK)
								
									SET_ENTITY_COORDS(family_ped[iPed], vFamily_scene_t1_coord+vFamily_coordOffset[iPed])
									SET_ENTITY_HEADING(family_ped[iPed], fFamily_scene_t1_head+fFamily_headOffset[iPed])
								ENDIF
							ELSE
								SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[iPed], vFamily_scene_t1_coord+vFamily_coordOffset[iPed], <<0,0,fFamily_scene_t1_head+fFamily_headOffset[iPed]>>)
								SET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[iPed], fFamily_synch_scenePhase[iPed])
								
								IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed])
									SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed], TRUE)
								ENDIF
								
								vFamMemCoordGround += <<0,0,-1>>
							ENDIF
							
							DRAW_DEBUG_BOX(vFamMemCoordGround - <<0.1, 0.1, 0.1>>,
									vFamMemCoordGround + <<0.1, 0.1, 0.1>>)
							
							FLOAT fPlayerCoord_groundZ = 0.0
							FLOAT  fAnimPos_heightDiff
							
							TEXT_LABEL str = "height: "
							
							IF GET_GROUND_Z_FOR_3D_COORD(vFamMemCoord, fPlayerCoord_groundZ)
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_BLUE)
							ELSE
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_RED)
							ENDIF
							
							
							iFamily_enter_veh_stage[iPed] = 0
						ENDIF
					ENDIF
					
					IF bJumpToFamilyMember[iPed]
						
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_t1_coord_vDebug_coordOffset = vFamily_scene_t1_coord+vDebug_coordOffset
						FLOAT fFamily_scene_t1_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_t1_coord_vDebug_coordOffset, fFamily_scene_t1_coord_vDebug_coordOffset)
							vFamily_scene_t1_coord_vDebug_coordOffset.z = fFamily_scene_t1_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_t1_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(-vPlayerHeading.x, -vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
					ENDIF
					
					IF (g_eCurrentFamilyEvent[eFamilyMember] <> INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Previous_Event_For_FamilyMember(eFamilyMember)
						
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Trevor_Family_Member_Widget(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iPed), eFamilyMember)
						
						PS_T1_SCENE_MEMBERS eSceneMemberUpdate
						REPEAT MAX_T1_SCENE_MEMBERS eSceneMemberUpdate
							enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, eSceneMemberUpdate))
							
							IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
								Update_Previous_Event_For_FamilyMember(eFamilyMember)
								
								iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
								Update_Trevor_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
								
							ENDIF
						ENDREPEAT
					ENDIF
				ENDREPEAT
				
			ELSE
				REPEAT MAX_T1_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iPed))
					iCurrentFamilyEvent[iPed] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMember])
					
					IF bJumpToFamilyMember[iPed]
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed]+vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_t1_coord_vDebug_coordOffset = vFamily_scene_t1_coord+vDebug_coordOffset
						FLOAT fFamily_scene_t1_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_t1_coord_vDebug_coordOffset, fFamily_scene_t1_coord_vDebug_coordOffset)
							vFamily_scene_t1_coord_vDebug_coordOffset.z = fFamily_scene_t1_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_t1_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vPlayerHeading.x, vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						bJumpToFamilyMember[iPed] = FALSE
					ENDIF
				ENDREPEAT
			ENDIF
			IF bDraw_peds
				FAMILY_COMP_NAME_ENUM eFamCompArray[NUM_PED_COMPONENTS]		//
				REPEAT MAX_T1_SCENE_MEMBERS iPed
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						DEBUG_GetOutfitFromFamilyMember(family_ped[iPed], eFamCompArray)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bSave_peds
				OPEN_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("	//- PRIVATE_Get_FamilyMember_Init_Offset() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_T1_SCENE_MEMBERS iPed
						IF NOT ARE_VECTORS_EQUAL(vFamily_coordOffset[iPed], <<0,0,0>>)
							enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iPed))
							
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_T1_")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
							SAVE_STRING_TO_DEBUG_FILE("		//")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SAVE_STRING_TO_DEBUG_FILE("	vInitOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	fInitHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_STRING_TO_DEBUG_FILE("	//- Get_DebugJumpOffset_From_FamilyEvent() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_T1_SCENE_MEMBERS iPed
						IF bJumpToFamilyMember[iPed]
							enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, iPed))
							
							VECTOR vDebug_jumpOffset= <<0,0,0>>
							Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
							
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_T1_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN ")SAVE_VECTOR_TO_DEBUG_FILE(vDebug_jumpOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_NEWLINE_TO_DEBUG_FILE()
				CLOSE_DEBUG_FILE()
				bSave_peds = FALSE
			ENDIF
			
			IF g_bUpdatedFamilyEvents
				PS_T1_SCENE_MEMBERS eSceneMember
				SWITCH g_eDebugSelectedMember
					CASE FM_TREVOR_1_FLOYD
						eSceneMember = PS_T1_FLOYD
					BREAK
					CASE FM_TREVOR_1_WADE
						eSceneMember = PS_T1_WADE
					BREAK
					
					DEFAULT
						eSceneMember = NO_T1_SCENE_MEMBER
					BREAK
				ENDSWITCH
				
				IF (eSceneMember <> NO_T1_SCENE_MEMBER)
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					bJumpToFamilyMember[eSceneMember] = TRUE
					
					DO_SCREEN_FADE_OUT(0)
					LOAD_ALL_OBJECTS_NOW()
					DO_SCREEN_FADE_IN(0)
					
			//		Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
					
					PRIVATE_Set_Current_Family_Member_Event(g_eDebugSelectedMember, g_eCurrentFamilyEvent[g_eDebugSelectedMember])
					Update_Trevor_Family_Member_Widget(eSceneMember, g_eDebugSelectedMember)
					
					IF NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
						CLEAR_PED_TASKS(family_ped[eSceneMember])
						
						IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMember])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_t1_coord+vFamily_coordOffset[eSceneMember])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_t1_head+fFamily_headOffset[eSceneMember])
							ENDIF
						ELSE
							SET_ENTITY_COORDS(family_ped[eSceneMember], vFamily_scene_t1_coord+vFamily_coordOffset[eSceneMember])
							SET_ENTITY_HEADING(family_ped[eSceneMember], fFamily_scene_t1_head+fFamily_headOffset[eSceneMember])
						ENDIF
					ENDIF
					
					PS_T1_SCENE_MEMBERS eSceneMemberUpdate
					REPEAT MAX_T1_SCENE_MEMBERS eSceneMemberUpdate
						enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Trevor1_Scene_Member(INT_TO_ENUM(PS_T1_SCENE_MEMBERS, eSceneMemberUpdate))
						
						IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
							Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
							
							iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
							Update_Trevor_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
							
							IF NOT IS_ENTITY_DEAD(family_ped[eSceneMemberUpdate])
								CLEAR_PED_TASKS(family_ped[eSceneMemberUpdate])
								
								IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMemberUpdate])
									VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMemberUpdate])
									
									IF NOT IS_ENTITY_DEAD(familyVeh)
										SET_ENTITY_COORDS(familyVeh, vFamily_scene_t1_coord+vFamily_coordOffset[eSceneMemberUpdate])
										SET_ENTITY_HEADING(familyVeh, fFamily_scene_t1_head+fFamily_headOffset[eSceneMemberUpdate])
									ENDIF
								ELSE
									SET_ENTITY_COORDS(family_ped[eSceneMemberUpdate], vFamily_scene_t1_coord+vFamily_coordOffset[eSceneMemberUpdate])
									SET_ENTITY_HEADING(family_ped[eSceneMemberUpdate], fFamily_scene_t1_head+fFamily_headOffset[eSceneMemberUpdate])
								ENDIF
							ENDIF
							
						ENDIF
					ENDREPEAT
					
					g_bUpdatedFamilyEvents = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Family_Scene_T1_Debug_Options()
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
		IF g_bDrawDebugFamilyStuff
			bSave_peds = TRUE
		ELSE
			PRINTSTRING("toggle g_bDrawDebugFamilyStuff to save family_scene_t1.sc info")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

PROC Control_Trevor_FamilyEvent_Tasks(PS_T1_SCENE_MEMBERS eSceneMember, enumFamilyEvents eFamilyEvent)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
	
	SWITCH eFamilyEvent
		/* TREVOR'S SAFEHOUSE */
		CASE FE_T1_FLOYD_cleaning
			IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord+<<0,0,1>>, fFamily_scene_t1_head,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
					family_prop[eSceneMember], NULL,
					bMove_peds)

				PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t1_coord, fFamily_scene_t1_head,
						iFamily_synch_scene[eSceneMember], TRUE, FALSE)
				IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
						"FLOYD_CLEANING", "", NULL, family_sfxBank[eSceneMember])
				ENDIF
				
				IF (eFamilyPropModel[eSceneMember] <> Prop_Scourer_01)
					Initialise_Trevor_Family_Prop_Variables(eSceneMember)
				ENDIF
			ENDIF
		BREAK
		CASE FE_T1_FLOYD_cries_in_foetal_position
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_t1_coord, fFamily_scene_t1_head,
					iFamily_synch_scene[eSceneMember], TRUE, TRUE)
		BREAK
		CASE FE_T1_FLOYD_cries_on_sofa
			PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord, fFamily_scene_t1_head,
					iFamily_synch_scene[eSceneMember],
					TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
			
			PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "CRYING", sSpeechTimer)
		BREAK
		CASE FE_T1_FLOYD_pineapple
//			PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//					vFamily_scene_t1_coord, fFamily_scene_t1_head,
//					iFamily_synch_scene[eSceneMember],
//					TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
			
			enumFamilyEvents eDesiredFamilyEvent
			IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_MESS_1) != BUILDINGSTATE_DESTROYED
				eDesiredFamilyEvent = FE_T1_FLOYD_cleaning
			ELSE
				eDesiredFamilyEvent = FE_ANY_wander_family_event
			ENDIF
					
			PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord, fFamily_scene_t1_head,
					iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					eDesiredFamilyEvent,
					"FMTAUD", "", "", tCreatedTrevor1ConvLabels,
					MyLocalPedStruct, sSpeechTimer)

		BREAK
		CASE FE_T1_FLOYD_on_phone_to_girlfriend
			IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t1_coord, fFamily_scene_t1_head)
				PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			ENDIF
			
			PRIVATE_Update_Family_Hangup(family_ped[eSceneMember], eFamilyMember,
					FE_T1_FLOYD_hangs_up_and_wanders,
					"FMTAUD", "FL_IG_3b", tCreatedTrevor1ConvLabels,
					MyLocalPedStruct, sSpeechTimer, eFamilyEvent)
			
		BREAK
		CASE FE_T1_FLOYD_hangs_up_and_wanders
			PRIVATE_Update_Family_Hangup_And_Wander(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord, fFamily_scene_t1_head,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					iFamily_enter_veh_stage[eSceneMember],
					family_prop[eSceneMember], eFamilyPropModel[eSceneMember], TRUE)
		BREAK
		CASE FE_T1_FLOYD_hiding_from_Trevor_a
		CASE FE_T1_FLOYD_hiding_from_Trevor_b
		CASE FE_T1_FLOYD_hiding_from_Trevor_c
			PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord, fFamily_scene_t1_head,
					iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					FE_ANY_wander_family_event,
					"FMTAUD", "", "FL_IG_4", tCreatedTrevor1ConvLabels,
					MyLocalPedStruct, sSpeechTimer)
			
			
			//hack for #1574080!!
			IF eFamilyEvent = FE_T1_FLOYD_hiding_from_Trevor_c
				IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
					
//					SEQUENCE_INDEX siseq
//					OPEN_SEQUENCE_TASK(siseq)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1146.1421, -1512.5297, 10.6327>>, PEDMOVE_WALK)
//						TASK_GO_STRAIGHT_TO_COORD(NULL, <<-1148.4043, -1513.9510, 10.6327>>, PEDMOVE_WALK)
//					CLOSE_SEQUENCE_TASK(siseq)
//					TASK_PERFORM_SEQUENCE(family_ped[eSceneMember], siseq)
//					CLEAR_SEQUENCE_TASK(siseq)
					
					TASK_FLUSH_ROUTE()
					TASK_EXTEND_ROUTE( <<-1146.1421, -1512.5297, 10.6327>> )
					TASK_EXTEND_ROUTE( <<-1151.2673, -1515.9131, 10.63272>> )
					TASK_EXTEND_ROUTE( <<-1152.9175, -1517.3339, 10.6327>> )
					TASK_FOLLOW_POINT_ROUTE( family_ped[eSceneMember], PEDMOVE_WALK, TICKET_SINGLE )
					
					SETTIMERA(0)
					
					PRINTLN("sequence fudge!")
					
					WHILE NOT IS_PED_INJURED(family_ped[eSceneMember])
						IF (GET_SCRIPT_TASK_STATUS(family_ped[eSceneMember], SCRIPT_TASK_FOLLOW_POINT_ROUTE) = FINISHED_TASK)
							PRINTLN("sequence task complete!")
							
							EXIT
						ENDIF
						
						IF TIMERA() > 5000
							PRINTLN("sequence time complete!")
							
							EXIT
						ENDIF
						
						IF VDIST(GET_ENTITY_COORDS(family_ped[eSceneMember]), <<-1152.9175, -1517.3339, 10.6327>>) < 1.0
							PRINTLN("sequence distance complete!")
							
							EXIT
						ENDIF
						
						WAIT(0)
					ENDWHILE
					
				ENDIF
			ENDIF
			
		BREAK
		CASE FE_T1_FLOYD_is_sleeping
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_t1_coord, fFamily_scene_t1_head,
					iFamily_synch_scene[eSceneMember], TRUE, FALSE)
			//PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
		BREAK
//		CASE FE_T1_FLOYD_with_wade_post_trevor3
//			PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//		BREAK
		CASE FE_T1_FLOYD_with_wade_post_docks1
			PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t1_coord, fFamily_scene_t1_head)
			
			enumFamilyDoors eFamilyDoorEnum
			PRIVATE_Update_Family_Doors(FE_T1_FLOYD_with_wade_post_docks1, eFamilyDoorEnum)
			
			IF (eSceneMember = PS_T1_FLOYD)
				IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
						"PORT_OF_LS_FLOYD_WADE_SHOWER", "FLOYD_WADE_SHOWER_MASTER", family_ped[eSceneMember], family_sfxBank[eSceneMember])
				ENDIF
			ENDIF
		BREAK
		CASE FE_ANY_find_family_event
			PRIVATE_Update_Family_Find_Event(family_ped[eSceneMember], eFamilyMember, vFamily_scene_t1_coord)
		BREAK
		CASE FE_ANY_wander_family_event
			OBJECT_INDEX family_chair
			family_chair = NULL
			PRIVATE_Update_Family_Wander_Event(family_ped[eSceneMember], eFamilyMember,
					iFamily_enter_veh_stage[eSceneMember],
					family_prop[eSceneMember], eFamilyPropModel[eSceneMember],
					family_prop[eSceneMember], eFamilyPropModel[eSceneMember],
					family_chair,
					iFamily_synch_scene[eSceneMember])
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			//
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in Control_Trevor_FamilyEvent_Tasks()")
			PRINTNL()
			#ENDIF
			
			SCRIPT_ASSERT("invalid eFamilyEvent in Control_Trevor_FamilyEvent_Tasks()")
			
		BREAK
	ENDSWITCH
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
		Initialise_Trevor_Family_Prop_Variables(eSceneMember)
		
		SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_T, TRUE)
		
		PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember])
		
		Initialise_Trevor_Family_Prop_Variables(eSceneMember)
		family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
		
		#IF IS_DEBUG_BUILD
		VECTOR vDebugJumpOffset
		Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember],	vDebugJumpOffset)
		Get_DebugJumpAngle_From_Vector(vDebugJumpOffset, fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember])
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL Moniter_A_Trevor1_Scene_Member(PS_T1_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eSceneMember)
	
	#IF IS_DEBUG_BUILD
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), vFamily_scene_t1_coord,						HUD_COLOUR_BLUE,30.0/255.0)
//	DrawDebugFamilySphere(vFamily_scene_t1_coord, 0.5,																HUD_COLOUR_BLUE,30.0/255.0)
//	
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),	HUD_COLOUR_YELLOW)
	
	TEXT_LABEL_63 str
	VECTOR xyzDraw = GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE)
	
	str = Get_String_From_Family_Scene_T1_Scene_Members(eSceneMember)
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 0, HUD_COLOUR_BLACK)
	
	str = "event: "
	str += Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember])
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 1, HUD_COLOUR_BLUE)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_bUpdatedFamilyEvents
	#ENDIF
		Control_Trevor_FamilyEvent_Tasks(eSceneMember, g_eCurrentFamilyEvent[eFamilyMember])
	#IF IS_DEBUG_BUILD
	ELSE
					
		PRINTSTRING("don't Control  ")
		PRINTSTRING(Get_String_From_Family_Scene_T1_Scene_Members(eSceneMember))
		PRINTSTRING(" FamilyEvent Tasks ")
		PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
		PRINTSTRING(", g_bUpdatedFamilyEvents = ")
		PRINTBOOL(g_bUpdatedFamilyEvents)
		PRINTNL()
		
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Moniter_All_Scene_Members()
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	bUpdateInteriorForPlayer = TRUE
	BOOL bCheckEachFrameForFamilyMemberSpeech = FALSE
	
	PS_T1_SCENE_MEMBERS eMember
	REPEAT MAX_T1_SCENE_MEMBERS eMember
		IF Is_Trevor1_Scene_Member_Active(eMember)
			Moniter_A_Trevor1_Scene_Member(eMember)
			
			IF NOT bCheckEachFrameForFamilyMemberSpeech
				IF NOT Is_Dialogue_Anim_Drinking(family_ped[eMember], tDialogueAnimDicts[eMember], tDialogueAnimClips[eMember], iFamily_synch_scene[eMember], sSpeechTimer)
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eMember)
					IF Play_This_Family_Speech(family_ped[eMember],
							g_eCurrentFamilyEvent[eFamilyMember],
							MyLocalPedStruct, "FMTAUD", tCreatedTrevor1ConvLabels,
							sSpeechTimer, iRandSpeechCount[eMember])
						bCheckEachFrameForFamilyMemberSpeech = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	TimedStopFamilyFires(family_ped, sFireTimer)
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugForceCreateFamily
		
		INT iDebugForceCreateFamily =  0
		REPEAT MAX_T1_SCENE_MEMBERS eMember
			enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor1_Scene_Member(eMember)
			IF (g_eCurrentFamilyEvent[g_eDebugSelectedMember] = g_eCurrentFamilyEvent[eFamilyMember])
				IF DOES_ENTITY_EXIST(family_ped[eMember])
					PRINTstring("ped[ ")
					PRINTSTRING(Get_String_From_Family_Scene_T1_Scene_Members(eMember))
					PRINTSTRING("] exists")PRINTNL()
					iDebugForceCreateFamily++
				ELSE
					PRINTstring("ped[ ")
					PRINTSTRING(Get_String_From_Family_Scene_T1_Scene_Members(eMember))
					PRINTSTRING("] doesnt exist")PRINTNL()
					iDebugForceCreateFamily = -9999
				ENDIF
			ELSE
				PRINTstring("ped[ ")
				PRINTSTRING(Get_String_From_Family_Scene_T1_Scene_Members(eMember))
				PRINTSTRING("] has different event ")
				PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
				PRINTNL()
				
			ENDIF
		ENDREPEAT
		PRINTNL()
		
		IF iDebugForceCreateFamily > 0
			g_bDebugForceCreateFamily = FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
			iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_t1_coord, "V_Trevors")
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT(structFamilyScene thisFamilyScene)
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Family_Scene_T1_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Family_Scene_T1_Variables(thisFamilyScene)
	Setup_Family_Scene_T1()
	
	#IF IS_DEBUG_BUILD
	Create_Family_Scene_T1_Widget()
	#ENDIF
	
	WHILE IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(CHAR_TREVOR, vFamily_scene_t1_coord, 1.5)
		WAIT(0)
		
		Moniter_All_Scene_Members()
		Moniter_Player_Griefing(BIT_TREVOR, sFamilyGriefing, vFamily_scene_t1_coord, <<11.5, 10.0, 7.5>>, family_veh)
		
		#IF IS_DEBUG_BUILD
		Watch_Family_Scene_T1_Widget()
		Family_Scene_T1_Debug_Options()
		#ENDIF
	ENDWHILE
	
	Family_Scene_T1_Cleanup()
ENDSCRIPT
