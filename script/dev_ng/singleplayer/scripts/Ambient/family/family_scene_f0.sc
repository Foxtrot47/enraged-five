// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Family Scene F file for use - family_scene_f0.sc					 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "family_public.sch"

//-	private headers	-//
USING "family_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
	USING "family_debug.sch"
#ENDIF

// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_F_SCENE_MEMBERS
	PS_F_AUNT = 0,
	PS_F_LAMAR,
	PS_F_STRETCH,
	
	MAX_F_SCENE_MEMBERS,
	NO_F_SCENE_MEMBER
ENDENUM
ENUM PS_F_SCENE_VEHICLES
	PS_FV_POLICE,
	
	MAX_SCENE_VEHICLES,
	NO_FAMILY_VEHICLE
ENDENUM

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structFamilyGriefing sFamilyGriefing

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bFamily_Scene_F0_in_progress				= TRUE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				family_ped[MAX_F_SCENE_MEMBERS]

INT						iFamily_enter_veh_stage[MAX_F_SCENE_MEMBERS]

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX			family_veh[MAX_SCENE_VEHICLES]

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			family_chair[MAX_F_SCENE_MEMBERS]

OBJECT_INDEX			family_prop[MAX_F_SCENE_MEMBERS]
MODEL_NAMES				eFamilyPropModel[MAX_F_SCENE_MEMBERS]
PED_BONETAG				eFamilyPropAttachBonetag[MAX_F_SCENE_MEMBERS]
VECTOR					vFamilyPropAttachOffset[MAX_F_SCENE_MEMBERS]
VECTOR					vFamilyPropAttachRotation[MAX_F_SCENE_MEMBERS]

OBJECT_INDEX			sec_light_prop
MODEL_NAMES				eSecLightPropModel[2]
VECTOR					vSecLightPropAttachOffset
VECTOR					vSecLightPropAttachRotation
structTimer				sSecLightTime
BOOL					bSecLightSound = FALSE
INT						iSecLightSound = -1

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vFamily_scene_f_coord
FLOAT				fFamily_scene_f_head

VECTOR				vFamily_coordOffset[MAX_F_SCENE_MEMBERS]
FLOAT				fFamily_headOffset[MAX_F_SCENE_MEMBERS]

	//- Pickups(INT) -//
INT					family_sfxStage[MAX_F_SCENE_MEMBERS], family_sfxID[MAX_F_SCENE_MEMBERS]
TEXT_LABEL_63		family_sfxBank[MAX_F_SCENE_MEMBERS]

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63		tDialogueAnimDicts[MAX_F_SCENE_MEMBERS], tDialogueAnimClips[MAX_F_SCENE_MEMBERS]
TEXT_LABEL			tCreatedFranklinConvLabels[5]

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//
BOOL bUpdateInteriorForPlayer = FALSE
structTimer sUpdatePlayerInteriorTimer
INTERIOR_INSTANCE_INDEX iInteriorForPlayer, iInteriorForSafehouse, iInteriorForMember[MAX_F_SCENE_MEMBERS]

	//- Timers(structTimer) -//
structTimer sSpeechTimer, sFireTimer
INT iRandSpeechCount[MAX_F_SCENE_MEMBERS]

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//
structPedsForConversation	MyLocalPedStruct

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	family_scene_f0_widget, family_scene_f0_subwidget, sec_light_widget
TEXT_WIDGET_ID	family_anim_widget[MAX_FAMILY_MEMBER], family_animFlag_widget[MAX_FAMILY_MEMBER]
BOOL			bToggle_Family_Scene_F0_Widget

INT				iCurrentFamilyEvent[MAX_FAMILY_MEMBER]

BOOL			bJumpToFamilyMember[MAX_FAMILY_MEMBER]
FLOAT			fPlayerDebugJumpAngle[MAX_FAMILY_MEMBER], fPlayerDebugJumpDistance[MAX_FAMILY_MEMBER]

FLOAT			fFamily_synch_scenePhase[MAX_FAMILY_MEMBER]

BOOL 			bMove_peds, bDraw_peds, bSave_peds
BOOL 			bMove_vehs	//, bUpdate_chair_attach
#ENDIF
//#IF NOT IS_DEBUG_BUILD
//BOOL 			bUpdate_chair_attach
//#ENDIF

	//- other Ints(INT) -//
INT			iFamily_synch_scene[MAX_F_SCENE_MEMBERS]

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC enumFamilyMember Get_Family_Member_From_Franklin_Scene_Member(PS_F_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
		CASE PS_F_AUNT
			RETURN FM_FRANKLIN_AUNT
		BREAK
		CASE PS_F_LAMAR
			RETURN FM_FRANKLIN_LAMAR
		BREAK
		CASE PS_F_STRETCH
			RETURN FM_FRANKLIN_STRETCH
		BREAK
		
		CASE MAX_F_SCENE_MEMBERS
			RETURN MAX_FAMILY_MEMBER
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_MEMBER
ENDFUNC

FUNC PS_F_SCENE_VEHICLES Get_FamilyScene_Vehicle_From_Franklin_Scene_Member(PS_F_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
		CASE PS_F_LAMAR		FALLTHRU
		CASE PS_F_STRETCH
			RETURN PS_FV_POLICE
		BREAK
		
		CASE MAX_F_SCENE_MEMBERS
			RETURN MAX_SCENE_VEHICLES
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_VEHICLE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING Get_String_From_Family_Scene_F0_Scene_Members(PS_F_SCENE_MEMBERS this_family_scene_f0_member)
	SWITCH this_family_scene_f0_member
		CASE PS_F_AUNT
			RETURN "PS_F_AUNT"
		BREAK
		CASE PS_F_LAMAR
			RETURN "PS_F_LAMAR"
		BREAK
		CASE PS_F_STRETCH
			RETURN "PS_F_STRETCH"
		BREAK
		
		CASE MAX_F_SCENE_MEMBERS
			RETURN "MAX_F_SCENE_MEMBERS"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Family_Scene_F0_Scene_Members(null) "
ENDFUNC
#ENDIF

//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Family_Scene_F0_Cleanup()

	PED_INDEX family_extra_ped[MAX_F_SCENE_MEMBERS]
	MODEL_NAMES eFamilyExtraPedModel[MAX_F_SCENE_MEMBERS]
	
	PRIVATE_GenericFamilySceneCleanup(iFamily_synch_scene,
			family_veh, family_ped,
			family_prop,		eFamilyPropModel,
			family_extra_ped,	eFamilyExtraPedModel,
			family_sfxStage, family_sfxID, family_sfxBank)
	
	INT iCleanup
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
//	REPEAT MAX_SCENE_VEHICLES iCleanup
//		IF DOES_ENTITY_EXIST(family_veh[iCleanup])
//			DELETE_VEHICLE(family_veh[iCleanup])
//		ENDIF
//		
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(family_veh[iCleanup])
//	ENDREPEAT
	
	//- mark objects as no longer needed -//
	SET_OBJECT_AS_NO_LONGER_NEEDED(sec_light_prop)
	
	//- mark models as no longer needed -//
	SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[0])
	SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[1])
	SET_MODEL_AS_NO_LONGER_NEEDED(POLICE)
	
	//- remove anims from the memory -//
	REPEAT MAX_F_SCENE_MEMBERS iCleanup
		
		enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iCleanup))
		IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
			#IF IS_DEBUG_BUILD
			PRINTSTRING("cleanup (g_eCurrentFamilyEvent[")
			PRINTSTRING(Get_String_From_FamilyMember(eFamilyMember))
			PRINTSTRING("] =  ")
			PRINTSTRING(Get_String_From_FamilyEvent(NO_FAMILY_EVENTS))
			PRINTSTRING(")")
			PRINTNL()
			#ENDIF
			
			Update_Previous_Event_For_FamilyMember(eFamilyMember)
			g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
			
		ENDIF
		
		SetFamilyMemberModelAsNoLongerNeeded(eFamilyMember)
	ENDREPEAT
	
	//- remove anims from the memory -//
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(sec_light_widget)
		DELETE_WIDGET_GROUP(sec_light_widget)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(family_scene_f0_subwidget)
		DELETE_WIDGET_GROUP(family_scene_f0_subwidget)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(family_scene_f0_widget)
		DELETE_WIDGET_GROUP(family_scene_f0_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	PRINTLN("g_eArm1PrestreamDenise = ARM1_PD_3_release")
	g_eArm1PrestreamDenise		= ARM1_PD_3_release
	
	PRINTLN("g_eLam1PrestreamLamStretch = ARM1_PD_3_release")
	g_eLam1PrestreamLamStretch	= ARM1_PD_3_release
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Initialise_Franklin_Family_Prop_Variables(PS_F_SCENE_MEMBERS eMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eMember)
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
	
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
			eFamilyPropModel[eMember]			= S_Prop_hdphones	//PROP_HEADPHONES_01
			eFamilyPropAttachBonetag[eMember]	= BONETAG_HEAD
			vFamilyPropAttachOffset[eMember]	= <<-0.0,0.0,-0.0>>
			vFamilyPropAttachRotation[eMember]	= <<0,0,-0.0>>
		BREAK
	
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			
//			SWITCH eMember
//				CASE PS_F_LAMAR
//					eFamilyExtraPedModel[eMember]		= S_M_Y_COP_01
//					vFamilyExtraPedCoordOffset[eMember]	= <<0,-6,-1>>+<<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>
//					fFamilyExtraPedHeadOffset[eMember]	= 150+get_random_float_in_range(-10,10)
//				BREAK
//				CASE PS_F_STRETCH
//					eFamilyExtraPedModel[eMember]		= S_F_Y_COP_01
//					vFamilyExtraPedCoordOffset[eMember]	= <<0,-6,-1>>+<<get_random_float_in_range(-1,1), get_random_float_in_range(-1,1), 0.0>>
//					fFamilyExtraPedHeadOffset[eMember]	= 150+get_random_float_in_range(-10,10)
//				BREAK
//			ENDSWITCH
//			
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			
//			SWITCH eMember
//				CASE PS_F_LAMAR
//					eFamilyPropModel[eMember]			= PROP_FISH_SLICE_01 //Bug Change: 1105432 
//					eFamilyPropAttachBonetag[eMember]	= BONETAG_PH_R_HAND
//					vFamilyPropAttachOffset[eMember]	= <<0,0,0>>
//					vFamilyPropAttachRotation[eMember]	= <<0,0,0>>
//				BREAK
//				CASE PS_F_STRETCH
////					eFamilyPropModel[eMember]			= PROP_BBQ_3
////					eFamilyPropAttachBonetag[eMember]	= BONETAG_NULL
////					vFamilyPropAttachOffset[eMember]	= <<-0.780,0.450,-0.870>>
////					vFamilyPropAttachRotation[eMember]	= <<0,0,-21.000>>
//				BREAK
//			ENDSWITCH
//			
//		BREAK
		
		DEFAULT
			eFamilyPropModel[eMember]			= DUMMY_MODEL_FOR_SCRIPT
			eFamilyPropAttachBonetag[eMember]	= BONETAG_NULL
			vFamilyPropAttachOffset[eMember]	= <<0,0,0>>
			vFamilyPropAttachRotation[eMember]	= <<0,0,0>>
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Family_Scene_F0_Variables(structFamilyScene thisFamilyScene)
	
	//- enums -//
	//- vectors -//
	vFamily_scene_f_coord = thisFamilyScene.sceneCoord
	fFamily_scene_f_head = thisFamilyScene.sceneHead
	
//	FLOAT fFRANKLIN_0_dist2 = VDIST2(vFamily_scene_f_coord, << -14.3064, -1435.9974, 30.1160 >>)
//	FLOAT fFRANKLIN_1_dist2 = VDIST2(vFamily_scene_f_coord, <<-1.2632, 531.5870, 174.3424>>)
//	
//	IF (fFRANKLIN_1_dist2 < fFRANKLIN_0_dist2)
//		TRUE = FALSE
//	ENDIF	
	
	//- floats -//
	//- ints -//
	//-- structs : PS_F_STRUCT --//
	
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_FRANKLIN_AUNT)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_FRANKLIN_LAMAR)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_FRANKLIN_STRETCH)
	
	PRIVATE_Get_FamilyMember_Init_Offset(FM_FRANKLIN_AUNT, g_eCurrentFamilyEvent[FM_FRANKLIN_AUNT],
			vFamily_coordOffset[PS_F_AUNT], fFamily_headOffset[PS_F_AUNT])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_FRANKLIN_LAMAR, g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR],
			vFamily_coordOffset[PS_F_LAMAR], fFamily_headOffset[PS_F_LAMAR])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_FRANKLIN_STRETCH, g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH],
			vFamily_coordOffset[PS_F_STRETCH], fFamily_headOffset[PS_F_STRETCH])
	
//	PRIVATE_Get_FamilyMember_Anim(FM_FRANKLIN_AUNT, g_eCurrentFamilyEvent[FM_FRANKLIN_AUNT],
//			sFamilyAnimDict[PS_F_AUNT], sFamilyAnim[PS_F_AUNT])
//	PRIVATE_Get_FamilyMember_Anim(FM_FRANKLIN_LAMAR, g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR],
//			sFamilyAnimDict[PS_F_LAMAR], sFamilyAnim[PS_F_LAMAR])
//	PRIVATE_Get_FamilyMember_Anim(FM_FRANKLIN_STRETCH, g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH],
//			sFamilyAnimDict[PS_F_STRETCH], sFamilyAnim[PS_F_STRETCH])
	
	PS_F_SCENE_MEMBERS eMember
	REPEAT MAX_F_SCENE_MEMBERS eMember
		Initialise_Franklin_Family_Prop_Variables(eMember)
		family_sfxID[eMember] = iCONST_FAMILY_SFX_INVALID
		iFamily_synch_scene[eMember] = -1
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	VECTOR vFM_LAMAR_1_AUNT
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_FRANKLIN_AUNT],	vFM_LAMAR_1_AUNT)
	Get_DebugJumpAngle_From_Vector(vFM_LAMAR_1_AUNT, fPlayerDebugJumpAngle[PS_F_AUNT], fPlayerDebugJumpDistance[PS_F_AUNT])

	VECTOR vFM_LAMAR_1_LAMAR
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR],	vFM_LAMAR_1_LAMAR)
	Get_DebugJumpAngle_From_Vector(vFM_LAMAR_1_LAMAR, fPlayerDebugJumpAngle[PS_F_LAMAR], fPlayerDebugJumpDistance[PS_F_LAMAR])

	VECTOR vFM_LAMAR_1_STRETCH
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH],	vFM_LAMAR_1_STRETCH)
	Get_DebugJumpAngle_From_Vector(vFM_LAMAR_1_STRETCH, fPlayerDebugJumpAngle[PS_F_STRETCH], fPlayerDebugJumpDistance[PS_F_STRETCH])

	
	#ENDIF
	
	eSecLightPropModel[0]		= Prop_WallLight_LD_01
	eSecLightPropModel[1]		= Prop_WallLight_LD_01b
	vSecLightPropAttachOffset	= <<-22.94551, -1431.17932, 32.64677>> - vFamily_scene_f_coord
	vSecLightPropAttachRotation	= <<15, 0 ,2.57-fFamily_scene_f_head+360>>
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Family_Scene_F0()
	INT iSetup
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
//	REPEAT MAX_F_SCENE_MEMBERS iSetup
//		IF NOT (IS_STRING_NULL(sFamilyAnimDict[iSetup]) OR ARE_STRINGS_EQUAL(sFamilyAnimDict[iSetup], ""))
//			REQUEST_ANIM_DICT(sFamilyAnimDict[iSetup])
//		ENDIF
//	ENDREPEAT
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	//- create any script vehicles -//
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//

//	VECTOR vPlayerHeading = vFamily_coordOffset[PS_F_AUNT] - vDebug_coordOffset
//	SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_f_coord+vDebug_coordOffset)
//	SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vPlayerHeading.x, vPlayerHeading.y))

	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	
	REPEAT MAX_F_SCENE_MEMBERS iSetup
		iInteriorForMember[iSetup] = NULL    //INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, -1)	//GET_INTERIOR_FROM_COLLISION(vFamily_scene_f_coord+vFamily_coordOffset[iSetup])	//GET_INTERIOR_AT_COORDS_WITH_TYPE(vFamily_scene_f_coord+vFamily_coordOffset[iSetup], "V_Franklins")	//GET_INTERIOR_FROM_fakeyfudge_obj(vSceneMemberCoord)
	ENDREPEAT
	
	//- load scene(if required) -//
	//- load additional text and mission text link-//
	//- setup audio malarky for family -//
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
	
ENDPROC


PROC CreateFranklinFamilyVeh(PS_F_SCENE_MEMBERS eSceneMember)	//, PS_F_SCENE_VEHICLES &eSceneVehicle)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
	
	PS_F_SCENE_VEHICLES eSceneVeh = Get_FamilyScene_Vehicle_From_Franklin_Scene_Member(eSceneMember)
	
	IF eSceneVeh < MAX_SCENE_VEHICLES
	
		CreateThisFamilyVehicle(family_veh[eSceneVeh],
					eFamilyMember,
					vFamily_scene_f_coord, fFamily_scene_f_head, FALSE
					
					#IF IS_DEBUG_BUILD
					, family_scene_f0_widget, bMove_vehs
					#ENDIF
					
					)
	ENDIF
ENDPROC

PROC CreateFranklinFamilyProp(PS_F_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
	
	CreateThisFamilyProp(family_prop[eSceneMember], family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_f_coord, fFamily_scene_f_head,
			eFamilyPropModel[eSceneMember], eFamilyPropAttachBonetag[eSceneMember],
			vFamilyPropAttachOffset[eSceneMember], vFamilyPropAttachRotation[eSceneMember]
			
			#IF IS_DEBUG_BUILD
			, family_scene_f0_widget, bMove_vehs
			#ENDIF
			
			)
	
ENDPROC

//PROC CreateFranklinFamilyExtraPed(PS_F_SCENE_MEMBERS eSceneMember)
//	
//	#IF IS_DEBUG_BUILD
//	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
//	#ENDIF
//	
//	CreateThisFamilyExtraPed(family_extra_ped[eSceneMember], family_ped[eSceneMember], eFamilyExtraPedModel[eSceneMember],
//			vFamilyExtraPedCoordOffset[eSceneMember], fFamilyExtraPedHeadOffset[eSceneMember]
//				
//			#IF IS_DEBUG_BUILD
//			, family_scene_f0_widget, bMove_vehs,
//			eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember]
//			#ENDIF
//			
//			)
//	
//ENDPROC
FUNC BOOL Is_Franklin_Scene_Member_Active(PS_F_SCENE_MEMBERS eSceneMember)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
	IF Is_Scene_Member_Active(family_ped[eSceneMember], eFamilyMember,
			vFamily_scene_f_coord+vFamily_coordOffset[eSceneMember],
			WRAP(fFamily_scene_f_head+fFamily_headOffset[eSceneMember], 0, 360),
			iInteriorForPlayer, iInteriorForMember[eSceneMember],
			MyLocalPedStruct, ENUM_TO_INT(eSceneMember)+1,
//			sFireTimer,
			RELGROUPHASH_FAMILY_F)
		
		CreateFranklinFamilyVeh(eSceneMember)
		CreateFranklinFamilyProp(eSceneMember)
//		CreateFranklinFamilyExtraPed(eSceneMember)
		
		IF (g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS)
		OR (g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY)
//			IF (eSceneVehicle < MAX_SCENE_VEHICLES)
//				IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
//					DELETE_VEHICLE(family_veh[eSceneVehicle])
//				ENDIF
//			ENDIF
			IF DOES_ENTITY_EXIST(family_prop[eSceneMember])
			//	IF NOT IS_ENTITY_DEAD(family_extra_ped[eSceneMember])
					IF IS_ENTITY_ATTACHED(family_prop[eSceneMember])
						DETACH_ENTITY(family_prop[eSceneMember])
					ENDIF
			//	ENDIF
				DELETE_OBJECT(family_prop[eSceneMember])
			ENDIF
//			IF DOES_ENTITY_EXIST(family_extra_ped[eSceneMember])
//				DELETE_PED(family_extra_ped[eSceneMember])
//			ENDIF
			
			DELETE_PED(family_ped[eSceneMember])
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PED_INJURED(family_ped[eSceneMember])
			RETURN TRUE
		ENDIF
	ELSE
		bUpdateInteriorForPlayer = TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Setup_Sec_Light_Prop()
	FLOAT		fPlayerDistFromSecLight2		= VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vFamily_scene_f_coord+vSecLightPropAttachOffset)
	
	FLOAT		fDISTANCE_TO_CREATE_SEC_LIGHT 	= (fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT*1.00)
	FLOAT		fDISTANCE_TO_DELETE_SEC_LIGHT 	= (fDISTANCE_TO_CREATE_SEC_LIGHT*1.25)
	
	IF NOT DOES_ENTITY_EXIST(sec_light_prop)
		IF fPlayerDistFromSecLight2 > (fDISTANCE_TO_CREATE_SEC_LIGHT*fDISTANCE_TO_CREATE_SEC_LIGHT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[0])
			RETURN FALSE
		ENDIF
		
		REQUEST_MODEL(eSecLightPropModel[0])
		IF NOT HAS_MODEL_LOADED(eSecLightPropModel[0])
			RETURN FALSE
		ENDIF
		
		sec_light_prop = CREATE_OBJECT(eSecLightPropModel[0], vFamily_scene_f_coord+vSecLightPropAttachOffset)
		SET_ENTITY_ROTATION(sec_light_prop, <<0,0,fFamily_scene_f_head>>+vSecLightPropAttachRotation)
		
		CANCEL_TIMER(sSecLightTime)
		
		FREEZE_ENTITY_POSITION(sec_light_prop, TRUE)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[0])
	ENDIF
	
	IF fPlayerDistFromSecLight2 > (fDISTANCE_TO_DELETE_SEC_LIGHT*fDISTANCE_TO_DELETE_SEC_LIGHT)
		DELETE_OBJECT(sec_light_prop)
		SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[0])
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	#ENDIF
	
	FLOAT fLightRadius = 6.5
	VECTOR vLightCoord
	vLightCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sec_light_prop, <<0,-(fLightRadius-0.5),0>>)
	
	PED_INDEX closestPed = NULL
	IF GET_CLOSEST_PED(vLightCoord, fLightRadius, TRUE, TRUE, closestPed, TRUE, TRUE)
		IF NOT IS_ENTITY_DEAD(closestPed)
			
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(vLightCoord, fLightRadius, HUD_COLOUR_BLUELIGHT, 064/256)
			#ENDIF
			
			VECTOR vClosestPedVelocity
			vClosestPedVelocity = GET_ENTITY_VELOCITY(closestPed)
			
			FLOAT fClosestPedVelocityMag2
			fClosestPedVelocityMag2 = VMAG2(vClosestPedVelocity)
			
			#IF IS_DEBUG_BUILD
			str  = "vel.x: "
			str += GET_STRING_FROM_FLOAT(vClosestPedVelocity.x)
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 0, HUD_COLOUR_PURPLELIGHT)
			
			str  = "vel.y: "
			str += GET_STRING_FROM_FLOAT(vClosestPedVelocity.y)
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 1, HUD_COLOUR_PURPLELIGHT)
			
			str  = "vel.z: "
			str += GET_STRING_FROM_FLOAT(vClosestPedVelocity.z)
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 2, HUD_COLOUR_PURPLELIGHT)
			
			str  = "vel mag: "
			str += GET_STRING_FROM_FLOAT(VMAG(vClosestPedVelocity))
			DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 3, HUD_COLOUR_PURPLEDARK)
			#ENDIF
			
			BOOL bCheckVelocity = FALSE
			IF (closestPed = PLAYER_PED_ID())
				IF IS_VALID_INTERIOR(iInteriorForPlayer)
				AND IS_INTERIOR_READY(iInteriorForPlayer)
					#IF IS_DEBUG_BUILD
					str  = "player inside"
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 4, HUD_COLOUR_REDLIGHT)
					#ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
					str  = "player outside"
					DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(closestPed), 4, HUD_COLOUR_GREENLIGHT)
					#ENDIF
					
					bCheckVelocity = TRUE
				ENDIF
			ELSE
				bCheckVelocity = TRUE
			ENDIF
			
			IF bCheckVelocity
				IF (fClosestPedVelocityMag2 >= (1.0*1.0))
					RESTART_TIMER_NOW(sSecLightTime)
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			DrawDebugFamilySphere(vLightCoord, fLightRadius, HUD_COLOUR_REDLIGHT, 064/256)
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		DrawDebugFamilySphere(vLightCoord, fLightRadius, HUD_COLOUR_GREYDARK, 064/256)
		#ENDIF
	ENDIF
	
	IF IS_TIMER_STARTED(sSecLightTime)
		#IF IS_DEBUG_BUILD
		str  = "time: "
		str += GET_STRING_FROM_FLOAT(GET_TIMER_IN_SECONDS(sSecLightTime))
		DrawDebugFamilyTextWithOffset(str, vFamily_scene_f_coord+vSecLightPropAttachOffset, 0, HUD_COLOUR_BLUELIGHT)
		#ENDIF
		
		IF GET_ENTITY_MODEL(sec_light_prop) != eSecLightPropModel[0]
			REQUEST_MODEL(eSecLightPropModel[0])
			IF HAS_MODEL_LOADED(eSecLightPropModel[0])
				DELETE_OBJECT(sec_light_prop)
				sec_light_prop = CREATE_OBJECT(eSecLightPropModel[0], vFamily_scene_f_coord+vSecLightPropAttachOffset)
				SET_ENTITY_ROTATION(sec_light_prop, <<0,0,fFamily_scene_f_head>>+vSecLightPropAttachRotation)
				
				FREEZE_ENTITY_POSITION(sec_light_prop, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[0])
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		str  = "time: "
		str += "OFF"
		DrawDebugFamilyTextWithOffset(str, vFamily_scene_f_coord+vSecLightPropAttachOffset, 0, HUD_COLOUR_REDLIGHT)
		#ENDIF
		
		IF GET_ENTITY_MODEL(sec_light_prop) != eSecLightPropModel[1]
			REQUEST_MODEL(eSecLightPropModel[1])
			IF HAS_MODEL_LOADED(eSecLightPropModel[1])
				DELETE_OBJECT(sec_light_prop)
				sec_light_prop = CREATE_OBJECT(eSecLightPropModel[1], vFamily_scene_f_coord+vSecLightPropAttachOffset)
				SET_ENTITY_ROTATION(sec_light_prop, <<0,0,fFamily_scene_f_head>>+vSecLightPropAttachRotation)
				
				FREEZE_ENTITY_POSITION(sec_light_prop, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(eSecLightPropModel[1])
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_ENTITY_MODEL(sec_light_prop) = eSecLightPropModel[0]
		#IF IS_DEBUG_BUILD
		str  = "model: "
		str += GET_MODEL_NAME_FOR_DEBUG(eSecLightPropModel[0])
		str += " - light on"
		DrawDebugFamilyTextWithOffset(str, vFamily_scene_f_coord+vSecLightPropAttachOffset, 1, HUD_COLOUR_BLUELIGHT)
		#ENDIF
		
		IF bSecLightSound
			iSecLightSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iSecLightSound, "FRANKLINS_HOUSE_SECURITY_LIGHT_ON", sec_light_prop)
			bSecLightSound = FALSE
		ENDIF
		
		SET_ENTITY_LIGHTS(sec_light_prop, FALSE)
		
		IF TIMER_DO_ONCE_WHEN_READY(sSecLightTime, 5.0)
			SET_ENTITY_LIGHTS(sec_light_prop, TRUE)
			
			iSecLightSound = GET_SOUND_ID()
			PLAY_SOUND_FROM_ENTITY(iSecLightSound, "FRANKLINS_HOUSE_SECURITY_LIGHT_OFF", sec_light_prop)
			bSecLightSound = TRUE
		ENDIF
	ELIF GET_ENTITY_MODEL(sec_light_prop) = eSecLightPropModel[1]
		#IF IS_DEBUG_BUILD
		str  = "model: "
		str += GET_MODEL_NAME_FOR_DEBUG(eSecLightPropModel[1])
		str += " - light off"
		DrawDebugFamilyTextWithOffset(str, vFamily_scene_f_coord+vSecLightPropAttachOffset, 1, HUD_COLOUR_REDLIGHT)
		#ENDIF
		
		SET_ENTITY_LIGHTS(sec_light_prop, TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		str  += "model: "
		str += GET_MODEL_NAME_FOR_DEBUG(eSecLightPropModel[1])
		str += " - unknown"
		
		DrawDebugFamilyTextWithOffset(str, vFamily_scene_f_coord+vSecLightPropAttachOffset, 1, HUD_COLOUR_REDLIGHT)
		#ENDIF
		
		SET_ENTITY_LIGHTS(sec_light_prop, TRUE)
	ENDIF
	
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Initialises the family widget
PROC Create_Family_Scene_F0_Widget()
	
	family_scene_f0_widget = START_WIDGET_GROUP("Family Scene F - city")
		ADD_WIDGET_BOOL("bToggle_Family_Scene_F0_Widget", bToggle_Family_Scene_F0_Widget)
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Initialises the mission widget
PROC Create_family_scene_f0_subwidget()
	STRING sFamilyMemberName[MAX_F_SCENE_MEMBERS]
	
	PS_F_SCENE_MEMBERS eSceneMember
	REPEAT MAX_F_SCENE_MEMBERS eSceneMember
		sFamilyMemberName[eSceneMember] = Get_String_From_Family_Scene_F0_Scene_Members(eSceneMember)
	ENDREPEAT
	
	family_scene_f0_subwidget = Create_Family_Scene_Widget_Group(MAX_F_SCENE_MEMBERS,
			sFamilyMemberName,
//			iCurrentFamilyEvent,
			family_anim_widget, family_animFlag_widget, //sFamilyAnimDict, sFamilyAnim,
			vFamily_coordOffset, fFamily_headOffset,
			bJumpToFamilyMember,  
			fPlayerDebugJumpAngle, fPlayerDebugJumpDistance, fFamily_synch_scenePhase,
			bMove_peds, bDraw_peds, bSave_peds, bEdit_speech_bounds)
	sec_light_widget = START_WIDGET_GROUP("sec_light_widget")
		ADD_WIDGET_VECTOR_SLIDER("vSecLightPropAttachOffset",
				vSecLightPropAttachOffset, -100.0, 100.0, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("vSecLightPropAttachRotation",
				vSecLightPropAttachRotation, -180.0, 180.0, 1.0)
	STOP_WIDGET_GROUP()
	
ENDPROC

PTFX_ID family_ptfx[MAX_F_SCENE_MEMBERS]
PROC Update_Franklin_Family_Member_Widget(PS_F_SCENE_MEMBERS eSceneMember, enumFamilyMember eFamilyMember)
	Update_Family_Member_Widget(eFamilyMember,
			//sFamilyAnimDict[eSceneMember], sFamilyAnim[eSceneMember], 
			family_anim_widget[eSceneMember],family_animFlag_widget[eSceneMember],
			vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember],
			fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember],
			family_ptfx[eSceneMember], family_sfxStage[eSceneMember], family_sfxID[eSceneMember], family_sfxBank[eSceneMember],
			sSpeechTimer, iRandSpeechCount[eSceneMember], family_prop[eSceneMember])
	Initialise_Franklin_Family_Prop_Variables(eSceneMember)
	SetFamilyMemberComponentVariation(family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
	SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_F, FALSE)
	
//	iFamily_enter_veh_stage[eSceneMember] = 0
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
		DETACH_SYNCHRONIZED_SCENE(iFamily_synch_scene[eSceneMember])
		iFamily_synch_scene[eSceneMember] = -1
	ENDIF
	
	tDialogueAnimDicts[eSceneMember] = ""
	tDialogueAnimClips[eSceneMember] = ""
	
	INT iConv
	REPEAT COUNT_OF(tCreatedFranklinConvLabels) iConv
		tCreatedFranklinConvLabels[iConv] = ""
	ENDREPEAT
	
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Family_Scene_F0_Widget()
	
	
	IF NOT bToggle_Family_Scene_F0_Widget
		IF DOES_WIDGET_GROUP_EXIST(family_scene_f0_subwidget)
			DELETE_WIDGET_GROUP(family_scene_f0_subwidget)
		ENDIF
		
		IF g_bUpdatedFamilyEvents
//		AND g_iDebugSelectedFriendConnDisplay > 0
			bToggle_Family_Scene_F0_Widget = TRUE
		ENDIF
	ELSE
		IF NOT DOES_WIDGET_GROUP_EXIST(family_scene_f0_subwidget)
			
//			enumFamilyMember eFamMem
//			TEXT_LABEL_63 str
			
			SET_CURRENT_WIDGET_GROUP(family_scene_f0_widget)
				Create_family_scene_f0_subwidget()
			CLEAR_CURRENT_WIDGET_GROUP(family_scene_f0_widget)
		ELSE
	
			INT iPed
			
			IF bMove_peds
			
				REPEAT MAX_F_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iPed))
					
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						IF IS_PED_IN_ANY_VEHICLE(family_ped[iPed])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[iPed])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_f_coord+vFamily_coordOffset[iPed])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_f_head+fFamily_headOffset[iPed])
							ENDIF
						ELSE
							VECTOR vFamMemCoord = vFamily_scene_f_coord+vFamily_coordOffset[iPed]
							VECTOR vFamMemCoordGround = vFamMemCoord
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[iPed])
								IF (GET_SCRIPT_TASK_STATUS(family_ped[iPed], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK)
								
									SET_ENTITY_COORDS(family_ped[iPed], vFamily_scene_f_coord+vFamily_coordOffset[iPed])
									SET_ENTITY_HEADING(family_ped[iPed], fFamily_scene_f_head+fFamily_headOffset[iPed])
								ENDIF
							ELSE
								SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[iPed], vFamily_scene_f_coord+vFamily_coordOffset[iPed], <<0,0,fFamily_scene_f_head+fFamily_headOffset[iPed]>>)
								SET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[iPed], fFamily_synch_scenePhase[iPed])
								
								IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed])
									SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed], TRUE)
								ENDIF
								
								vFamMemCoordGround += <<0,0,-1>>
							ENDIF
							
							DRAW_DEBUG_BOX(vFamMemCoordGround - <<0.1, 0.1, 0.1>>,
									vFamMemCoordGround + <<0.1, 0.1, 0.1>>)
							
							FLOAT fPlayerCoord_groundZ = 0.0
							FLOAT  fAnimPos_heightDiff
							
							TEXT_LABEL str = "height: "
							
							IF GET_GROUND_Z_FOR_3D_COORD(vFamMemCoord, fPlayerCoord_groundZ)
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_BLUE)
							ELSE
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_RED)
							ENDIF
							
							
//							iFamily_enter_veh_stage[iPed] = 0
						ENDIF
					ENDIF
					
					IF bJumpToFamilyMember[iPed]
						
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_f_coord_vDebug_coordOffset = vFamily_scene_f_coord+vDebug_coordOffset
						FLOAT fFamily_scene_f_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_f_coord_vDebug_coordOffset, fFamily_scene_f_coord_vDebug_coordOffset)
							vFamily_scene_f_coord_vDebug_coordOffset.z = fFamily_scene_f_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_f_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(-vPlayerHeading.x, -vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
					ENDIF
					
					IF (g_eCurrentFamilyEvent[eFamilyMember] <> INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Previous_Event_For_FamilyMember(eFamilyMember)
						
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Franklin_Family_Member_Widget(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iPed), eFamilyMember)
						
						PS_F_SCENE_MEMBERS eSceneMemberUpdate
						REPEAT MAX_F_SCENE_MEMBERS eSceneMemberUpdate
							enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, eSceneMemberUpdate))
							
							IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
								Update_Previous_Event_For_FamilyMember(eFamilyMember)
								
								iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
								Update_Franklin_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
								
							ENDIF
						ENDREPEAT
					ENDIF
				ENDREPEAT
				
//				IF (Get_Mission_Flow_Flag_State(FLOWFLAG_SEC_LIGHTS_UNLOCKED) <> bFLOWFLAG_SEC_LIGHTS_UNLOCKED)
//					Set_Mission_Flow_Flag_State(FLOWFLAG_SEC_LIGHTS_UNLOCKED, bFLOWFLAG_SEC_LIGHTS_UNLOCKED)
//				ENDIF
				
				IF DOES_ENTITY_EXIST(sec_light_prop)
					SET_ENTITY_COORDS(sec_light_prop, vFamily_scene_f_coord+vSecLightPropAttachOffset)
					SET_ENTITY_ROTATION(sec_light_prop, <<0,0,fFamily_scene_f_head>>+vSecLightPropAttachRotation)
				ENDIF
				
			ELSE
				REPEAT MAX_F_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iPed))
					iCurrentFamilyEvent[iPed] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMember])
					
					IF bJumpToFamilyMember[iPed]
						
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_f_coord_vDebug_coordOffset = vFamily_scene_f_coord+vDebug_coordOffset
						FLOAT fFamily_scene_f_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_f_coord_vDebug_coordOffset, fFamily_scene_f_coord_vDebug_coordOffset)
							vFamily_scene_f_coord_vDebug_coordOffset.z = fFamily_scene_f_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_f_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vPlayerHeading.x, vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						bJumpToFamilyMember[iPed] = FALSE
					ENDIF
				ENDREPEAT
				
//				IF (Get_Mission_Flow_Flag_State(FLOWFLAG_SEC_LIGHTS_UNLOCKED) <> bFLOWFLAG_SEC_LIGHTS_UNLOCKED)
//					bFLOWFLAG_SEC_LIGHTS_UNLOCKED = Get_Mission_Flow_Flag_State(FLOWFLAG_SEC_LIGHTS_UNLOCKED)
//				ENDIF
			ENDIF
			IF bDraw_peds
				FAMILY_COMP_NAME_ENUM eFamCompArray[NUM_PED_COMPONENTS]		//
				REPEAT MAX_F_SCENE_MEMBERS iPed
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						DEBUG_GetOutfitFromFamilyMember(family_ped[iPed], eFamCompArray)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bSave_peds
				OPEN_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("	//- PRIVATE_Get_FamilyMember_Init_Offset() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_F_SCENE_MEMBERS iPed
						IF NOT ARE_VECTORS_EQUAL(vFamily_coordOffset[iPed], <<0,0,0>>)
							enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iPed))
							
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_F_")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
							SAVE_STRING_TO_DEBUG_FILE("		//")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SAVE_STRING_TO_DEBUG_FILE("	vInitOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	fInitHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_STRING_TO_DEBUG_FILE("	//- Get_DebugJumpOffset_From_FamilyEvent() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_F_SCENE_MEMBERS iPed
						IF bJumpToFamilyMember[iPed]
							enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, iPed))
							
							VECTOR vDebug_jumpOffset = <<0,0,0>>
							Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
							
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_F_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN ")SAVE_VECTOR_TO_DEBUG_FILE(vDebug_jumpOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_NEWLINE_TO_DEBUG_FILE()
				CLOSE_DEBUG_FILE()
				bSave_peds = FALSE
			ENDIF
			
			IF g_bUpdatedFamilyEvents
				PS_F_SCENE_MEMBERS eSceneMember
				SWITCH g_eDebugSelectedMember
					CASE FM_FRANKLIN_AUNT
						eSceneMember = PS_F_AUNT
					BREAK
					CASE FM_FRANKLIN_LAMAR
						eSceneMember = PS_F_LAMAR
					BREAK
					CASE FM_FRANKLIN_STRETCH
						eSceneMember = PS_F_STRETCH
					BREAK
					
					DEFAULT
						eSceneMember = NO_F_SCENE_MEMBER
					BREAK
				ENDSWITCH
				
				IF (eSceneMember <> NO_F_SCENE_MEMBER)
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					bJumpToFamilyMember[eSceneMember] = TRUE
					
					DO_SCREEN_FADE_OUT(0)
					LOAD_ALL_OBJECTS_NOW()
					DO_SCREEN_FADE_IN(0)
					
					SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_F,	DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
					SET_DOOR_STATE(DOORNAME_F_HOUSE_SC_B,	DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
					
			//		Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
					
					PRIVATE_Set_Current_Family_Member_Event(g_eDebugSelectedMember, g_eCurrentFamilyEvent[g_eDebugSelectedMember])
					Update_Franklin_Family_Member_Widget(eSceneMember, g_eDebugSelectedMember)
					
					IF NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
						CLEAR_PED_TASKS(family_ped[eSceneMember])
						
						IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMember])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_f_coord+vFamily_coordOffset[eSceneMember])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_f_head+fFamily_headOffset[eSceneMember])
							ENDIF
						ELSE
							SET_ENTITY_COORDS(family_ped[eSceneMember], vFamily_scene_f_coord+vFamily_coordOffset[eSceneMember])
							SET_ENTITY_HEADING(family_ped[eSceneMember], fFamily_scene_f_head+fFamily_headOffset[eSceneMember])
						ENDIF
					ENDIF
					
					PS_F_SCENE_MEMBERS eSceneMemberUpdate
					REPEAT MAX_F_SCENE_MEMBERS eSceneMemberUpdate
						enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Franklin_Scene_Member(INT_TO_ENUM(PS_F_SCENE_MEMBERS, eSceneMemberUpdate))
						
						IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
							Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
							
							iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
							Update_Franklin_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
							
							IF NOT IS_ENTITY_DEAD(family_ped[eSceneMemberUpdate])
								CLEAR_PED_TASKS(family_ped[eSceneMemberUpdate])
								
								IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMemberUpdate])
									VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMemberUpdate])
									
									IF NOT IS_ENTITY_DEAD(familyVeh)
										SET_ENTITY_COORDS(familyVeh, vFamily_scene_f_coord+vFamily_coordOffset[eSceneMemberUpdate])
										SET_ENTITY_HEADING(familyVeh, fFamily_scene_f_head+fFamily_headOffset[eSceneMemberUpdate])
									ENDIF
								ELSE
									SET_ENTITY_COORDS(family_ped[eSceneMemberUpdate], vFamily_scene_f_coord+vFamily_coordOffset[eSceneMemberUpdate])
									SET_ENTITY_HEADING(family_ped[eSceneMemberUpdate], fFamily_scene_f_head+fFamily_headOffset[eSceneMemberUpdate])
								ENDIF
							ENDIF
							
						ENDIF
					ENDREPEAT
					
					g_bUpdatedFamilyEvents = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Family_Scene_F0_Debug_Options()
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
		IF g_bDrawDebugFamilyStuff
			bSave_peds = TRUE
		ELSE
			PRINTSTRING("toggle g_bDrawDebugFamilyStuff to save family_scene_f0.sc info")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************


PROC Control_Franklin_FamilyEvent_Tasks(PS_F_SCENE_MEMBERS eSceneMember, enumFamilyEvents eFamilyEvent)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
	
	SWITCH eFamilyEvent
		/* FRANKLIN'S HOUSE */
		CASE FE_F_AUNT_pelvic_floor_exercises
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_f_coord+<<0,0,1>>, fFamily_scene_f_head)
			PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			
			PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "EXERCISING", sSpeechTimer)
			
			IF (g_eCurrentSafehouseActivity = SA_FRANKLIN_SOFA)
				PLAY_PED_AMBIENT_SPEECH(family_ped[eSceneMember], "GENERIC_INSULT_MED")
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FAMILY_MEMBER_BUSY)
			ENDIF
		BREAK
		CASE FE_F_AUNT_in_face_mask
			IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_f_coord, fFamily_scene_f_head)
				PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						vFamily_scene_f_coord+<<0,0,1>>, fFamily_scene_f_head)
			ENDIF
			
			PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			
			IF (g_eCurrentSafehouseActivity = SA_FRANKLIN_SOFA)
				PLAY_PED_AMBIENT_SPEECH(family_ped[eSceneMember], "GENERIC_INSULT_MED")
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FAMILY_MEMBER_BUSY)
			ENDIF
		BREAK
		CASE FE_F_AUNT_watching_TV
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_f_coord+<<0,0,1>>, fFamily_scene_f_head)
			PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_f_coord, fFamily_scene_f_head, iFamily_enter_veh_stage[eSceneMember])
			
			IF (g_eArm1PrestreamDenise = ARM1_PD_2_request)
				g_eArm1PrestreamDenise = ARM1_PD_3_release
			ENDIF
		BREAK
		CASE FE_F_AUNT_returned_to_aunts
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_f_coord+<<0,0,1>>, fFamily_scene_f_head)
		BREAK
		CASE FE_F_AUNT_listens_to_selfhelp_tapes_x
//			IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
//				IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
//					SET_ENTITY_COORDS(family_chair[eSceneMember],
//							vFamily_scene_f_coord+vFamily_coordOffset[eSceneMember])
//					SET_ENTITY_ROTATION(family_chair[eSceneMember],
//							<<0,0,fFamily_scene_f_head+fFamily_headOffset[eSceneMember]>>)
//				ENDIF
//					
//			ENDIF
			
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_f_coord, fFamily_scene_f_head,
					iFamily_synch_scene[eSceneMember], TRUE, TRUE)
			PRIVATE_Set_Family_SyncSceneProp(family_prop[eSceneMember],
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					"", "_Head_Phones",
					iFamily_synch_scene[eSceneMember])
					
					
			IF NOT DOES_ENTITY_EXIST(family_chair[eSceneMember])
				VECTOR vecPos
				MODEL_NAMES ObjectModel
				vecPos = <<-9.7881, -1431.9719, 30.9968>>
				ObjectModel = V_RES_FA_CHAIR02
				
				IF IS_VALID_INTERIOR(iInteriorForMember[eSceneMember])
					IF IS_INTERIOR_READY(iInteriorForMember[eSceneMember])
						family_chair[eSceneMember] = GET_CLOSEST_OBJECT_OF_TYPE(vecPos, 10.0, ObjectModel, TRUE)
						
						IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
							FORCE_ROOM_FOR_ENTITY(family_chair[eSceneMember], iInteriorForMember[eSceneMember], GET_ROOM_KEY_FROM_ENTITY(family_ped[eSceneMember]))
							SET_ENTITY_COLLISION(family_chair[eSceneMember], FALSE)
						ELSE
							PRINTSTRING("NOT DOES_ENTITY_EXIST")PRINTNL()
						ENDIF
					ELSE
						PRINTSTRING("NOT IS_INTERIOR_READY")PRINTNL()
					ENDIF
				ELSE
					PRINTSTRING("NOT IS_VALID_INTERIOR")PRINTNL()
				ENDIF
			ELSE
				PRIVATE_Set_Family_SyncSceneProp(family_chair[eSceneMember],
						tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						"", "_Chair",
						iFamily_synch_scene[eSceneMember])
			ENDIF
			
			
			
			IF (g_eCurrentSafehouseActivity = SA_FRANKLIN_SOFA)
				PLAY_PED_AMBIENT_SPEECH(family_ped[eSceneMember], "GENERIC_INSULT_MED")
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FAMILY_MEMBER_BUSY)
			ENDIF
			
		BREAK
		
//		CASE FE_F_LAMAR_and_STRETCH_chill_outside
//			PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//					vFamily_scene_f_coord, fFamily_scene_f_head,
//					iFamily_synch_scene[eSceneMember],
//					TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
//					
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_bbq_outside
//			IF (eSceneMember = PS_F_LAMAR)
//				PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
//						eFamilyMember, eFamilyEvent,
//					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//					 	vFamily_scene_f_coord, fFamily_scene_f_head,
//						iFamily_synch_scene[PS_F_LAMAR], TRUE, FALSE)
//			ELSE
//				PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
//						eFamilyMember, eFamilyEvent,
//					 	tDialogueAnimDicts[PS_F_LAMAR], tDialogueAnimClips[eSceneMember],
//					 	tDialogueAnimClips[PS_F_LAMAR], "_LAMAR",
//						iFamily_synch_scene[PS_F_LAMAR])
//			ENDIF
//			
//			IF (eFamilyPropModel[eSceneMember] = PROP_BBQ_3)
//				PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
//						"LAMAR_AND_STRETCH_BBQ_OUTSIDE", "LAMAR_AND_STRETCH_BBQ_MASTER", family_prop[eSceneMember])
//			ENDIF
//			
//			IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
//			ENDIF
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_arguing
//			IF (eSceneMember = PS_F_LAMAR)
//				PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
//						eFamilyMember, eFamilyEvent,
//					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//					 	vFamily_scene_f_coord, fFamily_scene_f_head,
//						iFamily_synch_scene[PS_F_LAMAR], TRUE, FALSE)
//			ELSE
//				PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
//						eFamilyMember, eFamilyEvent,
//					 	tDialogueAnimDicts[PS_F_LAMAR], tDialogueAnimClips[eSceneMember],
//					 	tDialogueAnimClips[PS_F_LAMAR], "_LAMAR",
//						iFamily_synch_scene[PS_F_LAMAR])
//			ENDIF
//		BREAK
//		CASE FE_F_LAMAR_and_STRETCH_shout_at_cops
//			IF PRIVATE_Update_Family_Cop(family_ped[eSceneMember], eFamilyMember, family_extra_ped[eSceneMember])
//				IF (eSceneMember = PS_F_LAMAR)
//					PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
//							eFamilyMember, eFamilyEvent,
//						 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//						 	vFamily_scene_f_coord, fFamily_scene_f_head,
//							iFamily_synch_scene[PS_F_LAMAR], TRUE, FALSE)
//				ELSE
//					PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
//							eFamilyMember, eFamilyEvent,
//						 	tDialogueAnimDicts[PS_F_LAMAR], tDialogueAnimClips[eSceneMember],
//						 	tDialogueAnimClips[PS_F_LAMAR], "_LAMAR",
//							iFamily_synch_scene[PS_F_LAMAR])
//				ENDIF
//			ENDIF
//			
//			// // // // // // // // // // // // 
//			INT iRandCount, iSpeechBit
//			Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit)
//			IF (iRandSpeechCount[eSceneMember] >= iRandCount)
//				IF NOT IS_ANY_SPEECH_PLAYING(family_ped[eSceneMember])
//					IF DOES_ENTITY_EXIST(family_veh[PS_FV_POLICE])
//						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
//						TASK_ENTER_VEHICLE(family_extra_ped[eSceneMember], family_veh[PS_FV_POLICE])
//						
//						PS_F_SCENE_MEMBERS eOtherSceneMember
//						REPEAT MAX_F_SCENE_MEMBERS eOtherSceneMember
//							IF (eOtherSceneMember <> eSceneMember)
//								enumFamilyMember eOtherFamilyMember
//								eOtherFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eOtherSceneMember)
//								
//								IF (g_eCurrentFamilyEvent[eOtherFamilyMember] = eFamilyEvent)
//									PRIVATE_Set_Current_Family_Member_Event(eOtherFamilyMember, FE_ANY_wander_family_event)
//									TASK_ENTER_VEHICLE(family_extra_ped[eOtherSceneMember], family_veh[PS_FV_POLICE],
//											DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//								ENDIF
//							ENDIF
//						ENDREPEAT
//					ENDIF
//				ENDIF
//			ENDIF
//			// // // // // // // // // // // // 
//			
//		BREAK
		CASE FE_F_LAMAR_and_STRETCH_wandering
			IF NOT DOES_ENTITY_EXIST(family_veh[PS_FV_POLICE])
				MODEL_NAMES m_eJACKAL
				VECTOR m_vJACKAL
				
				m_eJACKAL = JACKAL
				m_vJACKAL = <<-38.2037, -1459.5112, 30.3994>>
				
				
				IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMember], TRUE)
					VEHICLE_INDEX tempVeh
					tempVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember], TRUE)
					IF GET_ENTITY_MODEL(tempVeh) = m_eJACKAL
						family_veh[PS_FV_POLICE] = tempVeh
						SET_ENTITY_AS_MISSION_ENTITY(family_veh[PS_FV_POLICE], TRUE, TRUE)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(family_veh[PS_FV_POLICE])	
					REQUEST_MODEL(m_eJACKAL)
					PRIVATE_Update_Family_Wander(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
							vFamily_scene_f_coord, fFamily_scene_f_head)
					family_veh[PS_FV_POLICE] = GET_CLOSEST_VEHICLE(m_vJACKAL, 5.0,
							m_eJACKAL, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)
					
					IF NOT DOES_ENTITY_EXIST(family_veh[PS_FV_POLICE])
						IF NOT IS_PED_INJURED(family_ped[PS_F_LAMAR])
						AND IS_PED_SITTING_IN_ANY_VEHICLE(family_ped[PS_F_LAMAR])
							family_veh[PS_FV_POLICE] = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
							SET_ENTITY_AS_MISSION_ENTITY(family_veh[PS_FV_POLICE], TRUE, TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(family_ped[PS_F_STRETCH])
						AND IS_PED_SITTING_IN_ANY_VEHICLE(family_ped[PS_F_STRETCH])
							family_veh[PS_FV_POLICE] = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
							SET_ENTITY_AS_MISSION_ENTITY(family_veh[PS_FV_POLICE], TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(family_veh[PS_FV_POLICE])	
					IF HAS_MODEL_LOADED(m_eJACKAL)
						IF NOT IS_AREA_OCCUPIED(m_vJACKAL-<<3,3,3>>, m_vJACKAL+<<3,3,3>>, false, true, false, false, false)
							family_veh[PS_FV_POLICE] = CREATE_VEHICLE(m_eJACKAL, m_vJACKAL, 93.9805)
							SET_MODEL_AS_NO_LONGER_NEEDED(m_eJACKAL)
						ENDIF
					ENDIF
				ELSE
					SET_ENTITY_AS_MISSION_ENTITY(family_veh[PS_FV_POLICE], TRUE, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(m_eJACKAL)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_DEAD(family_veh[PS_FV_POLICE])
				//
			ELIF NOT IS_PED_SITTING_IN_VEHICLE(family_ped[eSceneMember], family_veh[PS_FV_POLICE])
				SWITCH eFamilyMember
					CASE FM_FRANKLIN_LAMAR
						IF NOT IS_ENTITY_ON_SCREEN(family_ped[eSceneMember])
							SET_PED_INTO_VEHICLE(family_ped[eSceneMember], family_veh[PS_FV_POLICE], VS_DRIVER)
						ELIF (GET_SCRIPT_TASK_STATUS(family_ped[eSceneMember], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
							TASK_ENTER_VEHICLE(family_ped[eSceneMember], family_veh[PS_FV_POLICE], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
						ENDIF
					BREAK
					CASE FM_FRANKLIN_STRETCH
						IF NOT IS_ENTITY_ON_SCREEN(family_ped[eSceneMember])
							SET_PED_INTO_VEHICLE(family_ped[eSceneMember], family_veh[PS_FV_POLICE], VS_FRONT_RIGHT)
						ELIF (GET_SCRIPT_TASK_STATUS(family_ped[eSceneMember], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK)
							TASK_ENTER_VEHICLE(family_ped[eSceneMember], family_veh[PS_FV_POLICE], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT)
						ENDIF
					BREAK
				ENDSWITCH
			ELIF IS_VEHICLE_SEAT_FREE(family_veh[PS_FV_POLICE], VS_DRIVER)
			OR IS_VEHICLE_SEAT_FREE(family_veh[PS_FV_POLICE], VS_FRONT_RIGHT)
				//
				PRINTSTRING("VS_DRIVER or VS_FRONT_RIGHT free???")PRINTNL()
				
				IF IS_VEHICLE_SEAT_FREE(family_veh[PS_FV_POLICE], VS_FRONT_RIGHT)
					IF (GET_SCRIPT_TASK_STATUS(family_ped[eSceneMember], SCRIPT_TASK_VEHICLE_TEMP_ACTION) <> PERFORMING_TASK)
						TASK_VEHICLE_TEMP_ACTION(family_ped[eSceneMember], family_veh[PS_FV_POLICE], TEMPACT_BRAKE, 500)
					ENDIF
				ENDIF
				
//				CLEAR_PED_TASKS(family_ped[eSceneMember])
			ELSE
				SWITCH eFamilyMember
					CASE FM_FRANKLIN_LAMAR
						PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember,
								family_veh[PS_FV_POLICE], FM_FRANKLIN_LAMAR,
								eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
								vFamily_scene_f_coord, fFamily_scene_f_head, iFamily_synch_scene[eSceneMember],
								AUTODOOR_MAX,
								"FMFAUD", "", "", tCreatedFranklinConvLabels, -1.0, 
								MyLocalPedStruct, sSpeechTimer, "",
								
								#IF USE_TU_CHANGES
								iInteriorForPlayer,
								#ENDIF
								
								family_ped[PS_F_STRETCH])
					BREAK
					CASE FM_FRANKLIN_STRETCH
						//
					BREAK
				ENDSWITCH
				
				IF (g_eLam1PrestreamLamStretch = ARM1_PD_2_request)
					g_eLam1PrestreamLamStretch = ARM1_PD_3_release
				ENDIF
				
			ENDIF
		BREAK
		
		CASE FE_ANY_find_family_event
			PRIVATE_Update_Family_Find_Event(family_ped[eSceneMember],
					eFamilyMember, vFamily_scene_f_coord, 40.0)
		BREAK
		CASE FE_ANY_wander_family_event
			IF eFamilyMember = FM_FRANKLIN_AUNT
			ELSE
				PRIVATE_Update_Family_Wander_Event(family_ped[eSceneMember], eFamilyMember,
						iFamily_enter_veh_stage[eSceneMember],
						family_prop[eSceneMember], eFamilyPropModel[eSceneMember],
						family_prop[eSceneMember], eFamilyPropModel[eSceneMember],
						family_chair[eSceneMember],
						iFamily_synch_scene[eSceneMember])
				
				IF g_eLastMissionPassed = SP_MISSION_LAMAR
					IF NOT HasTimeToWaitPassedFromLastPassedMission(60.0)
						Set_Current_Event_For_FamilyMember(eFamilyMember, FE_F_LAMAR_and_STRETCH_wandering)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			//
		BREAK
		
		DEFAULT
			
			#IF IS_DEBUG_BUILD
			PRINTSTRING("invalid eFamilyEvent ")
			PRINTSTRING(Get_String_From_FamilyEvent(eFamilyEvent))
			PRINTSTRING(" in Control_Franklin_FamilyEvent_Tasks()")
			PRINTNL()
			#ENDIF
			
			SCRIPT_ASSERT("invalid eFamilyEvent in Control_Franklin_FamilyEvent_Tasks()")
		BREAK
	ENDSWITCH
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
		PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember])
		
		SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_F, TRUE)
		
		Initialise_Franklin_Family_Prop_Variables(eSceneMember)
		family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
		
		#IF IS_DEBUG_BUILD
		VECTOR vDebugJumpOffset
		Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember],	vDebugJumpOffset)
		Get_DebugJumpAngle_From_Vector(vDebugJumpOffset, fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember])
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL Moniter_A_Franklin_Scene_Member(PS_F_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eSceneMember)
	
	#IF IS_DEBUG_BUILD
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), vFamily_scene_f_coord,						HUD_COLOUR_BLUE,30.0/255.0)
//	DrawDebugFamilySphere(vFamily_scene_f_coord, 0.5,																	HUD_COLOUR_BLUE,30.0/255.0)
//	
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),	HUD_COLOUR_YELLOW)
	
	TEXT_LABEL_63 str
	VECTOR xyzDraw = GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE)
	
	str = Get_String_From_Family_Scene_F0_Scene_Members(eSceneMember)
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 0, HUD_COLOUR_BLACK)
	
	str = "event: "
	str += Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember])
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 1, HUD_COLOUR_BLUE)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_bUpdatedFamilyEvents
	#ENDIF
		Control_Franklin_FamilyEvent_Tasks(eSceneMember, g_eCurrentFamilyEvent[eFamilyMember])
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTSTRING("don't Control  ")
		PRINTSTRING(Get_String_From_Family_Scene_F0_Scene_Members(eSceneMember))
		PRINTSTRING(" FamilyEvent Tasks ")
		PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
		PRINTSTRING(", g_bUpdatedFamilyEvents = ")
		PRINTBOOL(g_bUpdatedFamilyEvents)
		PRINTNL()
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Moniter_All_Scene_Members()
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	bUpdateInteriorForPlayer = FALSE
	BOOL bCheckEachFrameForFamilyMemberSpeech = FALSE
	
	PS_F_SCENE_MEMBERS eMember
	REPEAT MAX_F_SCENE_MEMBERS eMember
		IF Is_Franklin_Scene_Member_Active(eMember)
			Moniter_A_Franklin_Scene_Member(eMember)
			
			IF NOT bCheckEachFrameForFamilyMemberSpeech
				enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eMember)
				IF NOT Is_Dialogue_Anim_Drinking(family_ped[eMember], tDialogueAnimDicts[eMember], tDialogueAnimClips[eMember], iFamily_synch_scene[eMember], sSpeechTimer)
					IF Play_This_Family_Speech(family_ped[eMember],
							g_eCurrentFamilyEvent[eFamilyMember],
							MyLocalPedStruct, "FMFAUD", tCreatedFranklinConvLabels,
							sSpeechTimer, iRandSpeechCount[eMember])
						bCheckEachFrameForFamilyMemberSpeech = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	TimedStopFamilyFires(family_ped, sFireTimer)
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugForceCreateFamily
		
		INT iDebugForceCreateFamily =  0
		REPEAT MAX_F_SCENE_MEMBERS eMember
			enumFamilyMember eFamilyMember = Get_Family_Member_From_Franklin_Scene_Member(eMember)
			IF (g_eCurrentFamilyEvent[g_eDebugSelectedMember] = g_eCurrentFamilyEvent[eFamilyMember])
				IF DOES_ENTITY_EXIST(family_ped[eMember])
					PRINTSTRING("ped[ ")
					PRINTSTRING(Get_String_From_Family_Scene_F0_Scene_Members(eMember))
					PRINTSTRING("] exists")PRINTNL()
					iDebugForceCreateFamily++
				ELSE
					PRINTSTRING("ped[ ")
					PRINTSTRING(Get_String_From_Family_Scene_F0_Scene_Members(eMember))
					PRINTSTRING("] doesnt exist")PRINTNL()
					iDebugForceCreateFamily = -9999
				ENDIF
			ELSE
				PRINTSTRING("ped[ ")
				PRINTSTRING(Get_String_From_Family_Scene_F0_Scene_Members(eMember))
				PRINTSTRING("] has different event ")
				PRINTSTRING(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
				PRINTNL()
				
			ENDIF
		ENDREPEAT
		PRINTNL()
		
		IF iDebugForceCreateFamily > 0
			g_bDebugForceCreateFamily = FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
			iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_f_coord, "v_franklins")
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT(structFamilyScene thisFamilyScene)
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Family_Scene_F0_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Family_Scene_F0_Variables(thisFamilyScene)
	Setup_Family_Scene_F0()
	
	#IF IS_DEBUG_BUILD
	Create_Family_Scene_F0_widget()
	#ENDIF
	
	WHILE bFamily_Scene_F0_in_progress
		WAIT(0)
		
		bFamily_Scene_F0_in_progress = FALSE
		
		IF IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(CHAR_FRANKLIN, vFamily_scene_f_coord, 1.5)
		OR (g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR] = FAMILY_MEMBER_BUSY AND g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH] = FAMILY_MEMBER_BUSY AND g_eLastMissionPassed = SP_MISSION_LAMAR AND HasTimeToWaitPassedFromLastPassedMission(60.0))
		OR (g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR] = FE_ANY_find_family_event OR g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH] = FE_ANY_find_family_event)
		OR (g_eCurrentFamilyEvent[FM_FRANKLIN_LAMAR] = FE_F_LAMAR_and_STRETCH_wandering OR g_eCurrentFamilyEvent[FM_FRANKLIN_STRETCH] = FE_F_LAMAR_and_STRETCH_wandering)
			Moniter_All_Scene_Members()
			Moniter_Player_Griefing(BIT_FRANKLIN, sFamilyGriefing, vFamily_scene_f_coord, <<15.0, 15.0, 7.5>>, family_veh)
			
			bFamily_Scene_F0_in_progress = TRUE
		ELSE
			
			PS_F_SCENE_MEMBERS eMember
			REPEAT MAX_F_SCENE_MEMBERS eMember
				Is_Franklin_Scene_Member_Active(eMember)
			ENDREPEAT
		ENDIF
		
		IF Setup_Sec_Light_Prop()
			bFamily_Scene_F0_in_progress = TRUE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		Watch_Family_Scene_F0_Widget()
		Family_Scene_F0_Debug_Options()
		#ENDIF
	ENDWHILE
	
	Family_Scene_F0_Cleanup()
ENDSCRIPT
