// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___																					 ___
// ___ 				Author:  Alwyn Roberts				Date: 21/04/2010		 		 ___
// _________________________________________________________________________________________
// ___ 																					 ___
// ___ 		(A1)  - Family Scene T0 file for use - family_scene_t0.sc					 ___
// ___ 																					 ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//-	commands headers	-//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers	-//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//-	public headers	-//

USING "dialogue_public.sch"
USING "timer_public.sch"
USING "family_public.sch"

//-	private headers	-//
USING "family_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//-	debug headers	-//
	USING "shared_debug.sch"
	USING "family_debug.sch"
#ENDIF


// *******************************************************************************************
//	CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//	ENUMERATIONS
// *******************************************************************************************
ENUM PS_T0_SCENE_MEMBERS
	PS_T0_RON = 0,
	PS_T0_MICHAEL,
	PS_T0_TREVOR,
	PS_T0_WIFE,
	
	PS_T0_MOTHER,
	
	MAX_T0_SCENE_MEMBERS,
	NO_T0_SCENE_MEMBER
ENDENUM
ENUM PS_T0_SCENE_VEHICLES
	PS_TV_BLANK = 0,
	
	MAX_SCENE_VEHICLES,
	NO_FAMILY_VEHICLE
ENDENUM

// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
structFamilyGriefing sFamilyGriefing

// *******************************************************************************************
//	VARIABLES
// *******************************************************************************************
	//- Flags(INT and BOOL) -//
BOOL					bSaidHiToWife							= FALSE
BOOL					bCreatedBarrelHide						= FALSE

	//- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX				family_ped[MAX_T0_SCENE_MEMBERS]

	//- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX			family_veh[MAX_SCENE_VEHICLES]
INT						iFamily_enter_veh_stage[MAX_T0_SCENE_MEMBERS]

	//- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

	//- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX			family_chair[MAX_T0_SCENE_MEMBERS]

OBJECT_INDEX			family_prop_l[MAX_T0_SCENE_MEMBERS]
MODEL_NAMES				eFamilyPropLModel[MAX_T0_SCENE_MEMBERS]
PED_BONETAG				eFamilyPropLAttachBonetag[MAX_T0_SCENE_MEMBERS]
VECTOR					vFamilyPropLAttachOffset[MAX_T0_SCENE_MEMBERS]
VECTOR					vFamilyPropLAttachRotation[MAX_T0_SCENE_MEMBERS]

OBJECT_INDEX			family_prop_r[MAX_T0_SCENE_MEMBERS]
MODEL_NAMES				eFamilyPropRModel[MAX_T0_SCENE_MEMBERS]
PED_BONETAG				eFamilyPropRAttachBonetag[MAX_T0_SCENE_MEMBERS]
VECTOR					vFamilyPropRAttachOffset[MAX_T0_SCENE_MEMBERS]
VECTOR					vFamilyPropRAttachRotation[MAX_T0_SCENE_MEMBERS]

OBJECT_INDEX			family_sill_prop
MODEL_NAMES				eFamilySillPropModel
PED_BONETAG				eFamilySillPropAttachBonetag
VECTOR					vFamilySillPropAttachOffset
VECTOR					vFamilySillPropAttachRotation

SCENARIO_BLOCKING_INDEX scenarioBlockID

	//- Other Models(MODEL_NAMES) -//

	//- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

	//- Blips (BLIP_INDEX) -//

	//- Coordinates(VECTORS and FLOATS) -//
VECTOR				vFamily_scene_t0_coord
FLOAT				fFamily_scene_t0_head

VECTOR				vFamily_coordOffset[MAX_T0_SCENE_MEMBERS]
FLOAT				fFamily_headOffset[MAX_T0_SCENE_MEMBERS]

	//- Pickups(INT) -//
BOOL				family_b_ptfx[MAX_T0_SCENE_MEMBERS]
PTFX_ID				family_ptfx[MAX_T0_SCENE_MEMBERS], family_other_ptfx
INT					family_sfxStage[MAX_T0_SCENE_MEMBERS], family_sfxID[MAX_T0_SCENE_MEMBERS]
TEXT_LABEL_63		family_sfxBank[MAX_T0_SCENE_MEMBERS]

	//- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63			tDialogueAnimDicts[MAX_T0_SCENE_MEMBERS], tDialogueAnimClips[MAX_T0_SCENE_MEMBERS]
TEXT_LABEL				tCreatedTrevor0ConvLabels[5]
TEXT_LABEL				tCreatedTrevor0MotherConvLabel = ""

	//- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

	//- Cameras(CAMERA_INDEX AND INT) -//

	//- Textures(TXD_ID and TEXTURE_ID) -//
BOOL bUpdateInteriorForPlayer = FALSE
structTimer sUpdatePlayerInteriorTimer
INTERIOR_INSTANCE_INDEX iInteriorForPlayer, iInteriorForSafehouse, iInteriorForMember[MAX_T0_SCENE_MEMBERS]

	//- Timers(structTimer) -//
structTimer sSpeechTimer, sFireTimer
INT iRandSpeechCount[MAX_T0_SCENE_MEMBERS]

	//- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

	//- Audio(INT and STRING) -//
structPedsForConversation	MyLocalPedStruct

#IF IS_DEBUG_BUILD
	//- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID	family_scene_t0_widget, family_scene_t0_subwidget
TEXT_WIDGET_ID	family_anim_widget[MAX_FAMILY_MEMBER], family_animFlag_widget[MAX_FAMILY_MEMBER]
BOOL			bToggle_Family_Scene_T0_Widget

INT				iCurrentFamilyEvent[MAX_FAMILY_MEMBER]

BOOL			bJumpToFamilyMember[MAX_FAMILY_MEMBER]
FLOAT			fPlayerDebugJumpAngle[MAX_FAMILY_MEMBER], fPlayerDebugJumpDistance[MAX_FAMILY_MEMBER]

FLOAT			fFamily_synch_scenePhase[MAX_FAMILY_MEMBER]

BOOL 			bMove_peds, bDraw_peds, bSave_peds
BOOL 			bMove_vehs, bUpdate_chair_attach
#ENDIF

#IF NOT IS_DEBUG_BUILD
BOOL 			bMove_peds, bUpdate_chair_attach
#ENDIF


	//- other Ints(INT) -//
INT				iFamily_synch_scene[MAX_T0_SCENE_MEMBERS]

	//- other Floats(FLOAT) -//

	//- TEMP RUBBISH -//

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC enumFamilyMember Get_Family_Member_From_Trevor0_Scene_Member(PS_T0_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
		CASE PS_T0_RON
			RETURN FM_TREVOR_0_RON
		BREAK
		CASE PS_T0_MICHAEL
			RETURN FM_TREVOR_0_MICHAEL
		BREAK
		CASE PS_T0_TREVOR
			RETURN FM_TREVOR_0_TREVOR
		BREAK
		CASE PS_T0_WIFE
			RETURN FM_TREVOR_0_WIFE
		BREAK
		CASE PS_T0_MOTHER
			RETURN FM_TREVOR_0_MOTHER
		BREAK
		
		CASE MAX_T0_SCENE_MEMBERS
			RETURN MAX_FAMILY_MEMBER
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_MEMBER
ENDFUNC

FUNC PS_T0_SCENE_VEHICLES Get_FamilyScene_Vehicle_From_Trevor0_Scene_Member(PS_T0_SCENE_MEMBERS eSceneMember)
	SWITCH eSceneMember
//		CASE PS_T0_RON
//			MODEL_NAMES dummyModel
//			eVehPed = PRIVATE_Get_CharList_From_FamilyMember(FM_TREVOR_0_RON, dummyModel)
//			RETURN PS_TV_DAUGHTER
//		BREAK
		
		CASE MAX_T0_SCENE_MEMBERS
			RETURN MAX_SCENE_VEHICLES
		BREAK
	ENDSWITCH
	
	RETURN NO_FAMILY_VEHICLE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING Get_String_From_Family_Scene_T0_Scene_Members(PS_T0_SCENE_MEMBERS this_family_scene_t0_member)
	SWITCH this_family_scene_t0_member
		CASE PS_T0_RON
			RETURN "PS_T0_RON"
		BREAK
		CASE PS_T0_MICHAEL
			RETURN "PS_T0_MICHAEL"
		BREAK
		CASE PS_T0_TREVOR
			RETURN "PS_T0_TREVOR"
		BREAK
		CASE PS_T0_WIFE
			RETURN "PS_T0_WIFE"
		BREAK
		CASE PS_T0_MOTHER
			RETURN "PS_T0_MOTHER"
		BREAK
		
		CASE MAX_T0_SCENE_MEMBERS
			RETURN "MAX_T0_SCENE_MEMBERS"
		BREAK
	ENDSWITCH
	
	RETURN "Get_String_From_Family_Scene_T0_Scene_Members(null) "
ENDFUNC
#ENDIF

PROC CreateBarrelHide(BOOL bHide)
	IF bHide
		IF NOT bCreatedBarrelHide
			CREATE_MODEL_HIDE(vFamily_scene_t0_coord, 25.0, PROP_BARREL_01A, FALSE)
			bCreatedBarrelHide = TRUE
		ENDIF
	ELSE
		IF bCreatedBarrelHide
			REMOVE_MODEL_HIDE(vFamily_scene_t0_coord, 25.0, PROP_BARREL_01A, TRUE)
			bCreatedBarrelHide = FALSE
		ENDIF
	ENDIF
ENDPROC


//PURPOSE:	Called on completion of the mission to release memory used for models etc
PROC Family_Scene_T0_Cleanup()
	PRIVATE_Cleanup_Family_Chairs(family_ped, family_chair)
	
	PED_INDEX family_extra_ped[MAX_T0_SCENE_MEMBERS]
	MODEL_NAMES eFamilyExtraPedModel[MAX_T0_SCENE_MEMBERS]
	
	PRIVATE_GenericFamilySceneCleanup(iFamily_synch_scene,
			family_veh,			family_ped,
			family_prop_l,		eFamilyPropLModel,
			family_extra_ped,	eFamilyExtraPedModel,
			family_sfxStage, family_sfxID, family_sfxBank)
	
	CreateBarrelHide(FALSE)
	
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlockID)
	
	INT iCleanup
	
	REPEAT MAX_T0_SCENE_MEMBERS iCleanup
		IF DOES_ENTITY_EXIST(family_prop_r[iCleanup])
			IF IS_ENTITY_ATTACHED(family_prop_r[iCleanup])
				DETACH_ENTITY(family_prop_r[iCleanup])
			ENDIF
		ENDIF
		IF ShouldDeleteFamilyEntity(family_prop_r[iCleanup], "family_prop_r", iCleanup)
			DELETE_OBJECT(family_prop_r[iCleanup])
		ENDIF
		IF (eFamilyPropRModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eFamilyPropRModel[iCleanup])
		ENDIF
	ENDREPEAT
	
	
	//- mark peds as no longer needed -//
	//- mark vehicle as no longer needed -//
//	REPEAT MAX_SCENE_VEHICLES iCleanup
//		IF DOES_ENTITY_EXIST(family_veh[iCleanup])
//			DELETE_VEHICLE(family_veh[iCleanup])
//		ENDIF
//		
//		SET_VEHICLE_AS_NO_LONGER_NEEDED(family_veh[iCleanup])
//	ENDREPEAT
	
	//- mark objects as no longer needed -//
	//- mark models as no longer needed -//
	//- remove anims from the memory -//
	REPEAT MAX_T0_SCENE_MEMBERS iCleanup
		IF (eFamilyPropLModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eFamilyPropLModel[iCleanup])
		ENDIF
		IF (eFamilyExtraPedModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(eFamilyExtraPedModel[iCleanup])
		ENDIF
		
		enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iCleanup))
		IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "cleanup (g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] =  ", Get_String_From_FamilyEvent(NO_FAMILY_EVENTS), ")")
			#ENDIF
			
			Update_Previous_Event_For_FamilyMember(eFamilyMember)
			g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
		ENDIF
		
		SetFamilyMemberModelAsNoLongerNeeded(eFamilyMember)
	ENDREPEAT
	
	//- remove anims from the memory -//
//	REPEAT MAX_T0_SCENE_MEMBERS iCleanup
//		IF NOT (IS_STRING_NULL(sFamilyAnimDict[iCleanup]) OR ARE_STRINGS_EQUAL(sFamilyAnimDict[iCleanup], ""))
//			REMOVE_ANIM_DICT(sFamilyAnimDict[iCleanup])
//		ENDIF
//	ENDREPEAT
	
	//- remove vehicle recordings from the memory -//
	//- clear sequences -//
	//- destroy all scripted cams -//
	//- enable the cellphone -//
	//- reset wanted multiplier -//
	
	#IF IS_DEBUG_BUILD
	//- delete the mission widget -//
	IF DOES_WIDGET_GROUP_EXIST(family_scene_t0_subwidget)
		DELETE_WIDGET_GROUP(family_scene_t0_subwidget)
	ENDIF
	IF DOES_WIDGET_GROUP_EXIST(family_scene_t0_widget)
		DELETE_WIDGET_GROUP(family_scene_t0_widget)
	ENDIF
	#ENDIF
	
	//- kill mission text link -//
	
	SET_GAME_PAUSED(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Initialise_Trevor_Family_Prop_Variables(PS_T0_SCENE_MEMBERS eMember)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eMember)
	
	eFamilyPropLModel[eMember]				= DUMMY_MODEL_FOR_SCRIPT
	eFamilyPropLAttachBonetag[eMember]		= BONETAG_NULL
	vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
	vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
	
	eFamilyPropRModel[eMember]				= DUMMY_MODEL_FOR_SCRIPT
	eFamilyPropRAttachBonetag[eMember]		= BONETAG_NULL
	vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
	vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
	
	SWITCH g_eCurrentFamilyEvent[eFamilyMember]
		CASE FE_T0_RON_monitoring_police_frequency
		CASE FE_T0_RON_listens_to_radio_broadcast
		CASE FE_T0_RONEX_trying_to_pick_up_signals
			eFamilyPropRModel[eMember]				= Prop_CS_Hand_Radio
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_PH_R_HAND
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_RON_stares_through_binoculars
			eFamilyPropLModel[eMember]				= PROP_BINOC_01
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar	FALLTHRU
		CASE FE_T0_RONEX_working_a_moonshine_sill
			eFamilyPropLModel[eMember]				= Prop_Ceramic_Jug_Cork
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
			
			eFamilyPropRModel[eMember]				= Prop_Ceramic_Jug_01
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_PH_R_HAND
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
			
			IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_RONEX_working_a_moonshine_sill)
				eFamilySillPropModel				= PROP_STILL
				eFamilySillPropAttachBonetag		= BONETAG_NULL
				vFamilySillPropAttachOffset			= <<-0.120,-0.954, -0.772>>
				vFamilySillPropAttachRotation		= <<0,0,-6.000>>
			ENDIF
		BREAK
		
		CASE FE_T0_RONEX_doing_target_practice
			eFamilyPropLModel[eMember]				= PROP_CS_BEER_BOT_01
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_NULL
			vFamilyPropLAttachOffset[eMember]		= <<-6.463, 6.136, 0.324>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,-42.000>>
			
			eFamilyPropRModel[eMember]				= PROP_CS_BEER_BOT_01
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_NULL
			vFamilyPropRAttachOffset[eMember]		= <<-6.644, 5.982, 0.324>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,-3.000>>
		BREAK
		
		CASE FE_T0_TREVOR_blowing_shit_up
			eFamilyPropLModel[eMember]				= PROP_WHISKEY_BOTTLE
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
			
			eFamilyPropRModel[eMember]				= GET_WEAPONTYPE_MODEL(WEAPONTYPE_GRENADE)
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_NULL
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		
		CASE FE_T0_MICHAEL_drinking_beer
			eFamilyPropLModel[eMember]				= PROP_CS_BEER_BOT_01
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			eFamilyPropLModel[eMember]				= PROP_PHONE_ING
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_RON_smoking_crystal				FALLTHRU
		CASE FE_T0_TREVOR_smoking_crystal
			eFamilyPropLModel[eMember]				= P_CS_LIGHTER_01
			eFamilyPropLAttachBonetag[eMember]		= BONETAG_PH_L_HAND
			vFamilyPropLAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropLAttachRotation[eMember]		= <<0,0,0>>
			
			eFamilyPropRModel[eMember]				= PROP_CS_CRACKPIPE
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_PH_R_HAND
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			eFamilyPropRModel[eMember]				= PROP_CS_TROWEL
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_PH_R_HAND
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
			eFamilyPropRModel[eMember]				= PROP_SCOURER_01
			eFamilyPropRAttachBonetag[eMember]		= BONETAG_PH_R_HAND
			vFamilyPropRAttachOffset[eMember]		= <<0,0,0>>
			vFamilyPropRAttachRotation[eMember]		= <<0,0,0>>
		BREAK
		
		DEFAULT
			
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE:	Initializes all the variables used within the scope of the script
PROC Initialise_Family_Scene_T0_Variables(structFamilyScene thisFamilyScene)
	
	//- enums -//
	//- vectors -//
	vFamily_scene_t0_coord = thisFamilyScene.sceneCoord
	fFamily_scene_t0_head = thisFamilyScene.sceneHead
	
	//- floats -//
	//- ints -//
	//-- structs : PS_T0_STRUCT --//
	
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_RON)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_MICHAEL)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_TREVOR)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_WIFE)
	INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_MOTHER)
	
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_RON, g_eCurrentFamilyEvent[FM_TREVOR_0_RON],
			vFamily_coordOffset[PS_T0_RON], fFamily_headOffset[PS_T0_RON])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_MICHAEL, g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL],
			vFamily_coordOffset[PS_T0_MICHAEL], fFamily_headOffset[PS_T0_MICHAEL])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_TREVOR, g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR],
			vFamily_coordOffset[PS_T0_TREVOR], fFamily_headOffset[PS_T0_TREVOR])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_WIFE, g_eCurrentFamilyEvent[FM_TREVOR_0_WIFE],
			vFamily_coordOffset[PS_T0_WIFE], fFamily_headOffset[PS_T0_WIFE])
	PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_MOTHER, g_eCurrentFamilyEvent[FM_TREVOR_0_MOTHER],
			vFamily_coordOffset[PS_T0_MOTHER], fFamily_headOffset[PS_T0_MOTHER])
	
	PS_T0_SCENE_MEMBERS eMember
	REPEAT MAX_T0_SCENE_MEMBERS eMember
		Initialise_Trevor_Family_Prop_Variables(eMember)
		family_sfxID[eMember] = iCONST_FAMILY_SFX_INVALID
		iFamily_synch_scene[eMember] = -1
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	VECTOR vFM_TREVOR_0_RON
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_RON],	vFM_TREVOR_0_RON)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_0_RON, fPlayerDebugJumpAngle[PS_T0_RON], fPlayerDebugJumpDistance[PS_T0_RON])

	VECTOR vFM_TREVOR_0_MICHAEL
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL],	vFM_TREVOR_0_MICHAEL)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_0_MICHAEL, fPlayerDebugJumpAngle[PS_T0_MICHAEL], fPlayerDebugJumpDistance[PS_T0_MICHAEL])

	VECTOR vFM_TREVOR_0_TREVOR
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR],	vFM_TREVOR_0_TREVOR)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_0_TREVOR, fPlayerDebugJumpAngle[PS_T0_TREVOR], fPlayerDebugJumpDistance[PS_T0_TREVOR])

	VECTOR vFM_TREVOR_0_WIFE
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_WIFE],	vFM_TREVOR_0_WIFE)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_0_WIFE, fPlayerDebugJumpAngle[PS_T0_WIFE], fPlayerDebugJumpDistance[PS_T0_WIFE])

	VECTOR vFM_TREVOR_0_MOTHER
	Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_MOTHER],	vFM_TREVOR_0_MOTHER)
	Get_DebugJumpAngle_From_Vector(vFM_TREVOR_0_MOTHER, fPlayerDebugJumpAngle[PS_T0_MOTHER], fPlayerDebugJumpDistance[PS_T0_MOTHER])
	
	#ENDIF
	
ENDPROC

// *******************************************************************************************
//	SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Load all the required models etc, and init peds, vehicles etc
PROC Setup_Family_Scene_T0()
	INT iSetup
	
	//- request models - peds -//
	//- request models - vehicles -//
	//- request models - objects -//
	//- request models - weapons -//
	//- request anims and ptfx --//
//	REPEAT MAX_T0_SCENE_MEMBERS iSetup
//		IF NOT (IS_STRING_NULL(sFamilyAnimDict[iSetup]) OR ARE_STRINGS_EQUAL(sFamilyAnimDict[iSetup], ""))
//			REQUEST_ANIM_DICT(sFamilyAnimDict[iSetup])
//		ENDIF
//	ENDREPEAT
	
	//- request vehicle recordings -//
	//- request interior models -//
	//- wait for assets to load -//
	//- create any script vehicles -//
	//- create any script peds -//
	//- create the peds for hot-swap -//
	//- set the players initial coords and heading -//
	//- create any script objects -//
	//- create any sequence tasks -//
	//- setup any scripted coverpoints -//
	//- add any initial taxi locations -//
	//- load scene(if required) -//
	
	REPEAT MAX_T0_SCENE_MEMBERS iSetup
		iInteriorForMember[iSetup] = NULL    //INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX, -1)	//GET_INTERIOR_FROM_COLLISION(vFamily_scene_t0_coord+vFamily_coordOffset[iSetup])	//GET_INTERIOR_AT_COORDS_WITH_TYPE(vFamily_scene_t0_coord+vFamily_coordOffset[iSetup], "V_Trailer")	//GET_INTERIOR_FROM_fakeyfudge_obj(vSceneMemberCoord)
	ENDREPEAT
	
	//- load additional text and mission text link-//
	//- setup audio malarky for family -//
	
	scenarioBlockID = ADD_SCENARIO_BLOCKING_AREA(
			<<1974.104492,3821.068115,35.083961>>-<<2.000000,2.000000,2.000000>>,
			<<1974.104492,3821.068115,35.083961>>+<<2.000000,2.000000,2.000000>>)
	
ENDPROC


PROC CreateTrevor0FamilyVeh(PS_T0_SCENE_MEMBERS eSceneMember, PS_T0_SCENE_VEHICLES &eSceneVehicle)
	eSceneVehicle = Get_FamilyScene_Vehicle_From_Trevor0_Scene_Member(eSceneMember)
	IF (eSceneVehicle < MAX_SCENE_VEHICLES)
		enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
		
		CreateThisFamilyVehicle(family_veh[eSceneVehicle],
				eFamilyMember,
				vFamily_scene_t0_coord, fFamily_scene_t0_head, FALSE
				
				#IF IS_DEBUG_BUILD
				, family_scene_t0_widget, bMove_vehs
				#ENDIF
				
				)
	ENDIF
ENDPROC

PROC CreateTrevor0FamilyProp(PS_T0_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
	
	CreateThisFamilyProp(family_prop_l[eSceneMember], family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_t0_coord, fFamily_scene_t0_head,
			eFamilyPropLModel[eSceneMember], eFamilyPropLAttachBonetag[eSceneMember],
			vFamilyPropLAttachOffset[eSceneMember], vFamilyPropLAttachRotation[eSceneMember]
			
			#IF IS_DEBUG_BUILD
			, family_scene_t0_widget, bMove_vehs
			#ENDIF
			)
	CreateThisFamilyProp(family_prop_r[eSceneMember], family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_t0_coord, fFamily_scene_t0_head,
			eFamilyPropRModel[eSceneMember], eFamilyPropRAttachBonetag[eSceneMember],
			vFamilyPropRAttachOffset[eSceneMember], vFamilyPropRAttachRotation[eSceneMember]
			
			#IF IS_DEBUG_BUILD
			, family_scene_t0_widget, bMove_vehs
			#ENDIF
			)
ENDPROC

//PROC CreateTrevor0FamilyExtraPed(PS_T0_SCENE_MEMBERS eSceneMember)
//	#IF IS_DEBUG_BUILD
//	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
//	#ENDIF
//	
//	CreateThisFamilyExtraPed(family_extra_ped[eSceneMember], family_ped[eSceneMember], eFamilyExtraPedModel[eSceneMember],
//			vFamilyExtraPedCoordOffset[eSceneMember], fFamilyExtraPedHeadOffset[eSceneMember]
//			
//			#IF IS_DEBUG_BUILD
//			, family_scene_t0_widget, bMove_vehs,
//			eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember]
//			#ENDIF
//			)
//ENDPROC

FUNC BOOL Is_Trevor0_Scene_Member_Active(PS_T0_SCENE_MEMBERS eSceneMember)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
	IF Is_Scene_Member_Active(family_ped[eSceneMember], eFamilyMember,
			vFamily_scene_t0_coord+vFamily_coordOffset[eSceneMember],
			WRAP(fFamily_scene_t0_head+fFamily_headOffset[eSceneMember], 0, 360),
			iInteriorForPlayer, iInteriorForMember[eSceneMember],
			MyLocalPedStruct, ENUM_TO_INT(eSceneMember)+1,
//			sFireTimer,
			RELGROUPHASH_FAMILY_T)
		
		PS_T0_SCENE_VEHICLES eSceneVehicle
		CreateTrevor0FamilyVeh(eSceneMember, eSceneVehicle)
		CreateTrevor0FamilyProp(eSceneMember)
		
		IF (eFamilyMember = FM_TREVOR_0_RON)
			IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_RONEX_working_a_moonshine_sill)
				CreateThisFamilyProp(family_sill_prop, family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_t0_coord, fFamily_scene_t0_head,
						eFamilySillPropModel, eFamilySillPropAttachBonetag,
						vFamilySillPropAttachOffset, vFamilySillPropAttachRotation
						
						#IF IS_DEBUG_BUILD
						, family_scene_t0_widget, bMove_vehs
						#ENDIF
						)
			ELIF (g_savedGlobals.sFamilyData.ePreviousFamilyEvent[eFamilyMember] = FE_T0_RONEX_working_a_moonshine_sill)
				IF DOES_ENTITY_EXIST(family_sill_prop)
					CPRINTLN(DEBUG_FAMILY, "WAIT TO DELETE sill prop - ron was working a moonshine sill, but is walking away")
				ELSE
					CPRINTLN(DEBUG_FAMILY, "ALREADY DELETED sill prop - ron was working a moonshine sill, but is walking away")
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(family_sill_prop)
					
					CPRINTLN(DEBUG_FAMILY, "delete sill prop - ron isnt working a moonshine sill")
					
					DELETE_OBJECT(family_sill_prop)
				ENDIF
			ENDIF
		ENDIF
		
//		CreateTrevor0FamilyExtraPed(eSceneMember)
		
		IF (g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS)
		OR (g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY)
//			IF DOES_ENTITY_EXIST(family_ped[eSceneMember]) AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_ped[eSceneMember], FALSE)
//				IF (eSceneVehicle < MAX_SCENE_VEHICLES)
//					IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
//						DELETE_VEHICLE(family_veh[eSceneVehicle])
//					ENDIF
//				ENDIF
//				IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
//					IF NOT IS_ENTITY_DEAD(family_extra_ped[eSceneMember])
//						IF IS_ENTITY_ATTACHED(family_prop_l[eSceneMember])
//							DETACH_ENTITY(family_prop_l[eSceneMember])
//						ENDIF
//					ENDIF
//					DELETE_OBJECT(family_prop_l[eSceneMember])
//				ENDIF
//				IF DOES_ENTITY_EXIST(family_extra_ped[eSceneMember])
//					DELETE_PED(family_extra_ped[eSceneMember])
//				ENDIF
//				
//				DELETE_PED(family_ped[eSceneMember])
//			ENDIF
			RETURN FALSE
		ENDIF
		
		IF NOT IS_PED_INJURED(family_ped[eSceneMember])
			RETURN TRUE
		ENDIF
	ELSE
		bUpdateInteriorForPlayer = TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//	DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:	Initialises the family widget
PROC Create_Family_Scene_T0_Widget()
	family_scene_t0_widget = START_WIDGET_GROUP("Family Scene T0")
		
		ADD_WIDGET_BOOL("bToggle_Family_Scene_T0_Widget", bToggle_Family_Scene_T0_Widget)
		
	STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:	Initialises the mission widget
PROC Create_Family_Scene_T0_Subwidget()
	STRING sFamilyMemberName[MAX_T0_SCENE_MEMBERS]
	
	PS_T0_SCENE_MEMBERS eSceneMember
	REPEAT MAX_T0_SCENE_MEMBERS eSceneMember
		sFamilyMemberName[eSceneMember] = Get_String_From_Family_Scene_T0_Scene_Members(eSceneMember)
	ENDREPEAT
	
	family_scene_t0_subwidget = Create_Family_Scene_Widget_Group(MAX_T0_SCENE_MEMBERS,
			sFamilyMemberName,
//			iCurrentFamilyEvent,
			family_anim_widget, family_animFlag_widget, //sFamilyAnimDict, sFamilyAnim,
			vFamily_coordOffset, fFamily_headOffset,
			bJumpToFamilyMember,  
			fPlayerDebugJumpAngle, fPlayerDebugJumpDistance, fFamily_synch_scenePhase,
			bMove_peds, bDraw_peds, bSave_peds, bEdit_speech_bounds)
	
ENDPROC

PROC Update_Trevor_Family_Member_Widget(PS_T0_SCENE_MEMBERS eSceneMember, enumFamilyMember eFamilyMember)
	PRIVATE_Cleanup_Family_Chairs(family_ped, family_chair)
	
	Update_Family_Member_Widget(eFamilyMember,
			//sFamilyAnimDict[eSceneMember], sFamilyAnim[eSceneMember],
			family_anim_widget[eSceneMember],family_animFlag_widget[eSceneMember],
			vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember],
			fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember],
			family_ptfx[eSceneMember], family_sfxStage[eSceneMember], family_sfxID[eSceneMember], family_sfxBank[eSceneMember],
			sSpeechTimer, iRandSpeechCount[eSceneMember], family_prop_l[eSceneMember])
	Initialise_Trevor_Family_Prop_Variables(eSceneMember)
	SetFamilyMemberComponentVariation(family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
	SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_T, FALSE)
	
	iFamily_enter_veh_stage[eSceneMember] = 0
	
		
	IF (family_other_ptfx <> NULL)
		REMOVE_PARTICLE_FX(family_other_ptfx)
		family_other_ptfx = NULL
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
		DETACH_SYNCHRONIZED_SCENE(iFamily_synch_scene[eSceneMember])
	ENDIF
	iFamily_synch_scene[eSceneMember] = -1
	
	tDialogueAnimDicts[eSceneMember] = ""
	tDialogueAnimClips[eSceneMember] = ""
	
	tCreatedTrevor0MotherConvLabel = ""
	
	INT iConv
	REPEAT COUNT_OF(tCreatedTrevor0ConvLabels) iConv
		tCreatedTrevor0ConvLabels[iConv] = ""
	ENDREPEAT
	
ENDPROC

//PURPOSE:	Moniters for any changes in the mission widget
PROC Watch_Family_Scene_T0_Widget()
	IF NOT bToggle_Family_Scene_T0_Widget
		IF DOES_WIDGET_GROUP_EXIST(family_scene_t0_subwidget)
			DELETE_WIDGET_GROUP(family_scene_t0_subwidget)
		ENDIF
		
		IF g_bUpdatedFamilyEvents
//		AND g_iDebugSelectedFriendConnDisplay > 0
			bToggle_Family_Scene_T0_Widget = TRUE
		ENDIF
	ELSE
		IF NOT DOES_WIDGET_GROUP_EXIST(family_scene_t0_subwidget)
			
//			enumFamilyMember eFamMem
//			TEXT_LABEL_63 str
			
			SET_CURRENT_WIDGET_GROUP(family_scene_t0_widget)
				Create_Family_Scene_T0_Subwidget()
			CLEAR_CURRENT_WIDGET_GROUP(family_scene_t0_widget)
		ELSE
	
			INT iPed
			
			IF bMove_peds
			
				REPEAT MAX_T0_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iPed))
					
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						IF IS_PED_IN_ANY_VEHICLE(family_ped[iPed])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[iPed])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_t0_coord+vFamily_coordOffset[iPed])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_t0_head+fFamily_headOffset[iPed])
							ENDIF
						ELSE
							VECTOR vFamMemCoord = vFamily_scene_t0_coord+vFamily_coordOffset[iPed]
							VECTOR vFamMemCoordGround = vFamMemCoord
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[iPed])
								IF (GET_SCRIPT_TASK_STATUS(family_ped[iPed], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK)
								
									SET_ENTITY_COORDS(family_ped[iPed], vFamily_scene_t0_coord+vFamily_coordOffset[iPed])
									SET_ENTITY_HEADING(family_ped[iPed], fFamily_scene_t0_head+fFamily_headOffset[iPed])
								ENDIF
							ELSE
								SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[iPed], vFamily_scene_t0_coord+vFamily_coordOffset[iPed], <<0,0,fFamily_scene_t0_head+fFamily_headOffset[iPed]>>)
								SET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[iPed], fFamily_synch_scenePhase[iPed])
								
								IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed])
									SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed], TRUE)
								ENDIF
								
								vFamMemCoordGround += <<0,0,-1>>
							ENDIF
							
							DRAW_DEBUG_BOX(vFamMemCoordGround - <<0.1, 0.1, 0.1>>,
									vFamMemCoordGround + <<0.1, 0.1, 0.1>>)
							
							FLOAT fPlayerCoord_groundZ = 0.0
							FLOAT  fAnimPos_heightDiff
							
							TEXT_LABEL str = "height: "
							
							IF GET_GROUND_Z_FOR_3D_COORD(vFamMemCoord, fPlayerCoord_groundZ)
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_BLUE)
							ELSE
								fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
								
								str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
								DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0, HUD_COLOUR_RED)
							ENDIF
							
							
							iFamily_enter_veh_stage[iPed] = 0
						ENDIF
					ENDIF
					
					IF bJumpToFamilyMember[iPed]
						
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_t0_coord_vDebug_coordOffset = vFamily_scene_t0_coord+vDebug_coordOffset
						FLOAT fFamily_scene_t0_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_t0_coord_vDebug_coordOffset, fFamily_scene_t0_coord_vDebug_coordOffset)
							vFamily_scene_t0_coord_vDebug_coordOffset.z = fFamily_scene_t0_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_t0_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(-vPlayerHeading.x, -vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
					ENDIF
					
					IF (g_eCurrentFamilyEvent[eFamilyMember] <> INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Previous_Event_For_FamilyMember(eFamilyMember)
						
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
						Update_Trevor_Family_Member_Widget(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iPed), eFamilyMember)
						
						PS_T0_SCENE_MEMBERS eSceneMemberUpdate
						REPEAT MAX_T0_SCENE_MEMBERS eSceneMemberUpdate
							enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, eSceneMemberUpdate))
							
							IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
								Update_Previous_Event_For_FamilyMember(eFamilyMember)
								
								iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
								Update_Trevor_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
								
							ENDIF
						ENDREPEAT
					ENDIF
				ENDREPEAT
				
			ELSE
				REPEAT MAX_T0_SCENE_MEMBERS iPed
					enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iPed))
					iCurrentFamilyEvent[iPed] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMember])
					
					IF bJumpToFamilyMember[iPed]
						
						VECTOR vDebug_jumpOffset = <<0,0,0>>
						Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
						
						VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
						VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
						
						VECTOR vFamily_scene_t0_coord_vDebug_coordOffset = vFamily_scene_t0_coord+vDebug_coordOffset
						FLOAT fFamily_scene_t0_coord_vDebug_coordOffset = 0.0
						IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_t0_coord_vDebug_coordOffset, fFamily_scene_t0_coord_vDebug_coordOffset)
							vFamily_scene_t0_coord_vDebug_coordOffset.z = fFamily_scene_t0_coord_vDebug_coordOffset
						ENDIF
						
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_t0_coord_vDebug_coordOffset)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vPlayerHeading.x, vPlayerHeading.y))
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						bJumpToFamilyMember[iPed] = FALSE
					ENDIF
				ENDREPEAT
			ENDIF
			IF bDraw_peds
				FAMILY_COMP_NAME_ENUM eFamCompArray[NUM_PED_COMPONENTS]		//
				REPEAT MAX_T0_SCENE_MEMBERS iPed
					IF NOT IS_ENTITY_DEAD(family_ped[iPed])
						DEBUG_GetOutfitFromFamilyMember(family_ped[iPed], eFamCompArray)
					ENDIF
				ENDREPEAT
			ENDIF
			
			IF bSave_peds
				OPEN_DEBUG_FILE()
					
					SAVE_STRING_TO_DEBUG_FILE("	//- PRIVATE_Get_FamilyMember_Init_Offset() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_T0_SCENE_MEMBERS iPed
						enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iPed))
						
						IF NOT ARE_VECTORS_EQUAL(vFamily_coordOffset[iPed], <<0,0,0>>)
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_T0_")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
							SAVE_STRING_TO_DEBUG_FILE("		//")
							SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SAVE_STRING_TO_DEBUG_FILE("	vInitOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	fInitHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_STRING_TO_DEBUG_FILE("	//- Get_DebugJumpOffset_From_FamilyEvent() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
					
					REPEAT MAX_T0_SCENE_MEMBERS iPed
						IF bJumpToFamilyMember[iPed]
							enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, iPed))
							
							VECTOR vDebug_jumpOffset = <<0,0,0>>
							Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
							
							SAVE_STRING_TO_DEBUG_FILE("CASE FE_T0_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("	RETURN ")SAVE_VECTOR_TO_DEBUG_FILE(vDebug_jumpOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_NEWLINE_TO_DEBUG_FILE()
						ENDIF
					ENDREPEAT
					
					SAVE_NEWLINE_TO_DEBUG_FILE()
				CLOSE_DEBUG_FILE()
				bSave_peds = FALSE
			ENDIF
			
			IF g_bUpdatedFamilyEvents
				PS_T0_SCENE_MEMBERS eSceneMember
				SWITCH g_eDebugSelectedMember
					CASE FM_TREVOR_0_RON
						eSceneMember = PS_T0_RON
					BREAK
					CASE FM_TREVOR_0_MICHAEL
						eSceneMember = PS_T0_MICHAEL
					BREAK
					CASE FM_TREVOR_0_TREVOR
						eSceneMember = PS_T0_TREVOR
					BREAK
					CASE FM_TREVOR_0_WIFE
						eSceneMember = PS_T0_WIFE
					BREAK
					CASE FM_TREVOR_0_MOTHER
						eSceneMember = PS_T0_MOTHER
					BREAK
					
					DEFAULT
						eSceneMember = NO_T0_SCENE_MEMBER
					BREAK
				ENDSWITCH
				
				IF (eSceneMember <> NO_T0_SCENE_MEMBER)
					
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
					bJumpToFamilyMember[eSceneMember] = TRUE
					
					DO_SCREEN_FADE_OUT(0)
					LOAD_ALL_OBJECTS_NOW()
					DO_SCREEN_FADE_IN(0)
					
					SET_DOOR_STATE(DOORNAME_T_TRAILER_CS,	DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
					
			//		Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
					
					PRIVATE_Set_Current_Family_Member_Event(g_eDebugSelectedMember, g_eCurrentFamilyEvent[g_eDebugSelectedMember])
					Update_Trevor_Family_Member_Widget(eSceneMember, g_eDebugSelectedMember)
					
					IF NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
						CLEAR_PED_TASKS(family_ped[eSceneMember])
						
						IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMember])
							VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
							
							IF NOT IS_ENTITY_DEAD(familyVeh)
								SET_ENTITY_COORDS(familyVeh, vFamily_scene_t0_coord+vFamily_coordOffset[eSceneMember])
								SET_ENTITY_HEADING(familyVeh, fFamily_scene_t0_head+fFamily_headOffset[eSceneMember])
							ENDIF
						ELSE
							SET_ENTITY_COORDS(family_ped[eSceneMember], vFamily_scene_t0_coord+vFamily_coordOffset[eSceneMember])
							SET_ENTITY_HEADING(family_ped[eSceneMember], fFamily_scene_t0_head+fFamily_headOffset[eSceneMember])
						ENDIF
					ENDIF
					
					PS_T0_SCENE_MEMBERS eSceneMemberUpdate
					REPEAT MAX_T0_SCENE_MEMBERS eSceneMemberUpdate
						enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Trevor0_Scene_Member(INT_TO_ENUM(PS_T0_SCENE_MEMBERS, eSceneMemberUpdate))
						
						IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
							Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
							
							iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
							Update_Trevor_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
							
							IF NOT IS_ENTITY_DEAD(family_ped[eSceneMemberUpdate])
								CLEAR_PED_TASKS(family_ped[eSceneMemberUpdate])
								
								IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMemberUpdate])
									VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMemberUpdate])
									
									IF NOT IS_ENTITY_DEAD(familyVeh)
										SET_ENTITY_COORDS(familyVeh, vFamily_scene_t0_coord+vFamily_coordOffset[eSceneMemberUpdate])
										SET_ENTITY_HEADING(familyVeh, fFamily_scene_t0_head+fFamily_headOffset[eSceneMemberUpdate])
									ENDIF
								ELSE
									SET_ENTITY_COORDS(family_ped[eSceneMemberUpdate], vFamily_scene_t0_coord+vFamily_coordOffset[eSceneMemberUpdate])
									SET_ENTITY_HEADING(family_ped[eSceneMemberUpdate], fFamily_scene_t0_head+fFamily_headOffset[eSceneMemberUpdate])
								ENDIF
							ENDIF
							
						ENDIF
					ENDREPEAT
					
					g_bUpdatedFamilyEvents = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Family_Scene_T0_Debug_Options()
	
	IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
		IF g_bDrawDebugFamilyStuff
			bSave_peds = TRUE
		ELSE
			CPRINTLN(DEBUG_FAMILY, "toggle g_bDrawDebugFamilyStuff to save family_scene_t0.sc info")
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

// *******************************************************************************************
//	GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

PROC Control_Trevor_FamilyEvent_Tasks(PS_T0_SCENE_MEMBERS eSceneMember, enumFamilyEvents eFamilyEvent)
	
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
	
	SWITCH eFamilyEvent
		/* TREVOR'S TRAILER - 1 */
		CASE FE_T0_RON_monitoring_police_frequency
			IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
				IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
						"RON_MONITORING_POLICE_FREQUENCY", "AFT_RON_MONITORING_POLICE_FREQUENCY_MASTER", family_prop_r[eSceneMember], family_sfxBank[eSceneMember])
				ENDIF
			ENDIF
			
			IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], vFamily_scene_t0_coord, fFamily_scene_t0_head)
				PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			ENDIF
			
			IF (g_eCurrentSafehouseActivity = SA_TREVOR_BEER)
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			ENDIF
		BREAK
		CASE FE_T0_RON_listens_to_radio_broadcast
		CASE FE_T0_RONEX_trying_to_pick_up_signals
			
			IF (eFamilyEvent = FE_T0_RON_listens_to_radio_broadcast)
				IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
						"RON_LISTENS_TO_RADIO_BROADCAST", "AFT_RON_LISTENS_TO_RADIO_BROADCAST_MASTER", family_prop_r[eSceneMember], family_sfxBank[eSceneMember])
				ENDIF
			ELIF (eFamilyEvent = FE_T0_RONEX_trying_to_pick_up_signals)
				IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
						"RON_EX_TRYING_TO_PICK_UP_SIGNALS", "AFT_RON_EX_TRYING_TO_PICK_UP_SIGNALS_MASTER", family_prop_r[eSceneMember], family_sfxBank[eSceneMember])
				ENDIF
			ENDIF
			
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_synch_scene[eSceneMember], TRUE, FALSE)
			
			IF (g_eCurrentSafehouseActivity = SA_TREVOR_BEER)
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			ENDIF
		BREAK
		CASE FE_T0_RON_ranting_about_government_LAYING
			/*	#382427
			Ron starts off with (LAYING) anim on couch.
			Then trigger (ENTER) go from (LAYING) ->(talking) or (BASE) anims
			(talking) anims loop – beg/end in Base pose.
			*/
			
			PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					FE_T0_RON_ranting_about_government_SITTING,
					"", "", "", tCreatedTrevor0ConvLabels,
					MyLocalPedStruct, sSpeechTimer)
		BREAK
		CASE FE_T0_RON_ranting_about_government_SITTING
			/*	#382427
			Ron starts off with (LAYING) anim on couch.
			Then trigger (ENTER) go from (LAYING) ->(talking) or (BASE) anims
			(talking) anims loop – beg/end in Base pose.
			*/
			
			PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_synch_scene[eSceneMember],
					TRUE, TRUE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
		BREAK
		CASE FE_T0_RON_smoking_crystal
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"RON_SMOKING_CRYSTAL", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			
			IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord, fFamily_scene_t0_head)
				PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
				
				IF NOT IS_ANY_SPEECH_PLAYING(family_ped[eSceneMember])
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					INT iRandCount, iSpeechBit, iPlayerBitset
					Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
					IF (iRandSpeechCount[eSceneMember] >= iRandCount)
						
						CONST_INT iCrystalShipTime 30000	//20000
						
						SET_PED_TO_RAGDOLL(family_ped[eSceneMember], iCrystalShipTime-5000, iCrystalShipTime, TASK_RELAX)
						
						IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
							DETACH_ENTITY(family_prop_l[eSceneMember])
						ENDIF
						IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
							DETACH_ENTITY(family_prop_r[eSceneMember])
						ENDIF
						
						Make_Ped_Drunk(family_ped[eSceneMember], iCrystalShipTime*3)
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
					ENDIF
				ENDIF
			ENDIF
			
			PRIVATE_Update_Family_AnimPtfx(family_ped[eSceneMember], eFamilyEvent,
					"cs_mich1_lighter_sparks", family_ptfx[eSceneMember], family_b_ptfx[eSceneMember],
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					iFamily_synch_scene[eSceneMember], "spark", 4, FALSE, family_prop_l[eSceneMember])
			PRIVATE_Update_Family_AnimPtfx(family_ped[eSceneMember], eFamilyEvent,
					"cs_mich1_lighter_flame", family_other_ptfx, family_b_ptfx[eSceneMember],
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					iFamily_synch_scene[eSceneMember], "flame", 5, FALSE, family_prop_l[eSceneMember])
			
		BREAK
		CASE FE_T0_RON_drinks_moonshine_from_a_jar
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"RON_DRINKING_MOONSHINE_FROM_A_JAR", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
					
			
//			IF NOT DOES_ENTITY_EXIST(family_prop_r)
//				CPRINTLN(DEBUG_FAMILY, "family_prop_r[", GET_MODEL_NAME_FOR_DEBUG(eFamilyPropRModel), "] doesnt exist")
//			ELSE
//				CPRINTLN(DEBUG_FAMILY, "family_prop_r[", GET_MODEL_NAME_FOR_DEBUG(eFamilyPropRModel), "] exists")
//				
//				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(family_prop_r), 0.0, 255, 0, 0, 128)
//				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(family_prop_r), GET_ENTITY_COORDS(family_ped[eSceneMember]), 255, 0, 0, 128)
//			ENDIF
			
		BREAK
		CASE FE_T0_RON_stares_through_binoculars
			IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
					family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
				PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
			ENDIF
		BREAK
		
		CASE FE_T0_MICHAEL_depressed_head_in_hands
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_synch_scene[eSceneMember], TRUE, TRUE)
		BREAK
		CASE FE_T0_MICHAEL_sunbathing
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
					eFamilyMember, eFamilyEvent,
				 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
				 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_synch_scene[eSceneMember], TRUE, TRUE)
		BREAK
		CASE FE_T0_MICHAEL_drinking_beer
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"MICHAEL_DRINKING_BEER", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
		BREAK
		CASE FE_T0_MICHAEL_on_phone_to_therapist
			PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			PRIVATE_Update_Family_Hangup(family_ped[eSceneMember], eFamilyMember,
					FE_T0_MICHAEL_hangs_up_and_wanders,
					"FMTAUD", "MIC_IG_4", tCreatedTrevor0ConvLabels,
					MyLocalPedStruct, sSpeechTimer, eFamilyEvent)
			
			IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_T0_MICHAEL_hangs_up_and_wanders)
				eFamilyEvent = FE_T0_MICHAEL_hangs_up_and_wanders
			ENDIF
		BREAK
		CASE FE_T0_MICHAEL_hangs_up_and_wanders
			PRIVATE_Update_Family_Hangup_And_Wander(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord, fFamily_scene_t0_head,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					iFamily_enter_veh_stage[eSceneMember],
					family_prop_l[eSceneMember], eFamilyPropLModel[eSceneMember], TRUE)
		BREAK
		
		CASE FE_T0_TREVOR_and_kidnapped_wife_walk
			IF (eSceneMember = PS_T0_TREVOR)
				PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[PS_T0_TREVOR], TRUE, FALSE)
			ELSE
				PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[PS_T0_TREVOR], tDialogueAnimClips[eSceneMember],
					 	tDialogueAnimClips[PS_T0_TREVOR], "_TREVOR",
						iFamily_synch_scene[PS_T0_TREVOR])
				
				IF (g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR] <> eFamilyEvent)
					iFamily_enter_veh_stage[eSceneMember] = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
				ENDIF
			ENDIF
			
			CreateBarrelHide(TRUE)
		BREAK
		CASE FE_T0_TREVOR_and_kidnapped_wife_stare
			IF (eSceneMember = PS_T0_TREVOR)
				IF PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[PS_T0_TREVOR], TRUE, FALSE)
					IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
					ENDIF
				ENDIF
			ELSE
				IF PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[PS_T0_TREVOR], tDialogueAnimClips[eSceneMember],
					 	tDialogueAnimClips[PS_T0_TREVOR], "_TREVOR",
						iFamily_synch_scene[PS_T0_TREVOR])
					IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
					ENDIF
				ENDIF
				
				IF (g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR] <> eFamilyEvent)
					iFamily_enter_veh_stage[eSceneMember] = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
				ENDIF
			ENDIF
			
			IF (g_eCurrentSafehouseActivity = SA_TREVOR_BEER)
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			ENDIF
		BREAK
		CASE FE_T0_TREVOR_smoking_crystal
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"TREVOR_SMOKING_CRYSTAL", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
			PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
		BREAK
		#IF NOT IS_JAPANESE_BUILD
		CASE FE_T0_TREVOR_doing_a_shit
			IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], vFamily_scene_t0_coord, fFamily_scene_t0_head)
				PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
			ENDIF
			
			PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"TREVOR_POO", "TREVORS_POO_FAIL", family_ped[eSceneMember], family_sfxBank[eSceneMember])
//			PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
//					"TREVOR_POO", "TREVORS_POO_SUCCESS", family_ped[eSceneMember])
			PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE)) < (35.0*35.0) //30.0f radius on replay recording
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
			ENDIF
		BREAK
		#ENDIF
		/*
		CASE FE_T0_TREVOR_and_kidnapped_wife_laugh
			IF (eSceneMember = PS_T0_TREVOR)
				IF NOT DOES_ENTITY_EXIST(family_ped[PS_T0_WIFE])
					//
				ELSE
					PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
							eFamilyMember, eFamilyEvent,
						 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
							iFamily_synch_scene[PS_T0_TREVOR], TRUE, TRUE)
				ENDIF
			ELSE
				PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[PS_T0_TREVOR], tDialogueAnimClips[eSceneMember],
					 	tDialogueAnimClips[PS_T0_TREVOR], "_TREVOR",
						iFamily_synch_scene[PS_T0_TREVOR])
				IF (g_eCurrentFamilyEvent[FM_TREVOR_0_TREVOR] <> eFamilyEvent)
					iFamily_enter_veh_stage[eSceneMember] = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(family_ped[PS_T0_TREVOR])
			AND DOES_ENTITY_EXIST(family_ped[PS_T0_WIFE])
			
				//#1587503
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[PS_T0_TREVOR])
					FLOAT fScenePhase
					fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[PS_T0_TREVOR])
					
					IF NOT ARE_STRINGS_EQUAL(tDialogueAnimClips[PS_T0_TREVOR], "IDLE_A_TREVOR")
						
						CPRINTLN(DEBUG_FAMILY, "fScenePhase \"")
						CPRINTLN(DEBUG_FAMILY, tDialogueAnimClips[PS_T0_TREVOR])
						CPRINTLN(DEBUG_FAMILY, "\": ")
						CPRINTLN(DEBUG_FAMILY, fScenePhase)
						CprintNL()
						
					ELSE
						
						FLOAT fStartDialogue, fEndDialogue
						fStartDialogue = 0.3
						fEndDialogue = 0.8
						
						CPRINTLN(DEBUG_FAMILY, "fScenePhase \"")
						CPRINTLN(DEBUG_FAMILY, tDialogueAnimClips[PS_T0_TREVOR])
						CPRINTLN(DEBUG_FAMILY, "\": ")
						CPRINTLN(DEBUG_FAMILY, fScenePhase)
						
						CPRINTLN(DEBUG_FAMILY, " [")
						CPRINTLN(DEBUG_FAMILY, fStartDialogue)
						CPRINTLN(DEBUG_FAMILY, " <> ")
						CPRINTLN(DEBUG_FAMILY, fEndDialogue)
						
						IF (fScenePhase >= fStartDialogue) AND (fScenePhase <= fEndDialogue)
							CPRINTLN(DEBUG_FAMILY, " play...")
						ELSE
							
						ENDIF
						
						CPRINTLN(DEBUG_FAMILY, "]")
						CprintNL()
						
	//					
						IF (fScenePhase >= fStartDialogue) AND (fScenePhase <= fEndDialogue)
							IF Play_This_Family_Speech(family_ped[eSceneMember], eFamilyEvent,
									MyLocalPedStruct, "FMTAUD", tCreatedTrevor0ConvLabels,
									sSpeechTimer, iRandSpeechCount[eSceneMember],
									5, "TRV_IG_5")
								//
							ENDIF
						ENDIF
					ENDIF
				ELSE
				
					
					CPRINTLN(DEBUG_FAMILY, "fScenePhase \"", tDialogueAnimClips[PS_T0_TREVOR], "\": NULL")
					
				ENDIF
			ENDIF
		BREAK
		*/
		CASE FE_T0_TREVOR_blowing_shit_up
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"TREVOR_BLOWING_SHIT_UP", "", NULL, family_sfxBank[eSceneMember])
			ENDIF

			PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_synch_scene[eSceneMember],
					TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
				FLOAT fFamilySynchScenePhase
				fFamilySynchScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember])
				
				CONST_FLOAT fCONST_ExplodeGrenadePhase	0.60
				
				IF NOT DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
					
					
					
					IF (eFamilyproprModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT)
						
						CPRINTLN(DEBUG_FAMILY, "model is dummy")
						
						IF fFamilySynchScenePhase < fCONST_ExplodeGrenadePhase
							eFamilyproprModel[eSceneMember] = GET_WEAPONTYPE_MODEL(WEAPONTYPE_GRENADE)
						ENDIF
					ELSE
						
						CPRINTLN(DEBUG_FAMILY, "creating prop")
						
//						CreateThisFamilypropr(family_prop_r[eSceneMember], family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_scene_t0_coord, fFamily_scene_t0_head,
//								eFamilyproprModel[eSceneMember], eFamilyproprAttachBonetag[eSceneMember],
//								vFamilyproprAttachOffset[eSceneMember], vFamilyproprAttachRotation[eSceneMember]
//								
//								#IF IS_DEBUG_BUILD
//								, family_scene_t0_widget, bMove_vehs
//								#ENDIF
//								)
					ENDIF
				ELSE
					
					
					CPRINTLN(DEBUG_FAMILY, "prop exists...")
					
					IF PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
							tDialogueAnimDicts[eSceneMember], "",
							"", "GRENADE_THROWING_grenade",
							iFamily_synch_scene[eSceneMember])
					ENDIF
					
					IF fFamilySynchScenePhase > fCONST_ExplodeGrenadePhase
						ADD_EXPLOSION(GET_ENTITY_COORDS(family_prop_r[eSceneMember]),EXP_TAG_GRENADE)
						DELETE_OBJECT(family_prop_r[eSceneMember])
						eFamilyproprModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
				ENDIF
			
			ELSE
				CPRINTLN(DEBUG_FAMILY, "synch scene not running...")
			ENDIF
			
			CreateBarrelHide(TRUE)
		BREAK
		CASE FE_T0_TREVOR_passed_out_naked_drunk
			PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[eSceneMember], TRUE, FALSE)
		BREAK
		
		CASE FE_T0_RONEX_outside_looking_lonely
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
			
			CreateBarrelHide(TRUE)
		BREAK
		CASE FE_T0_RONEX_working_a_moonshine_sill
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"RON_WORKING_A_MOONSHINE_SILL", "AFT_RON_EX_MOONSHINE_SILL_LOOP_MASTER", family_sill_prop, family_sfxBank[eSceneMember])
			ENDIF

			PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					vFamily_scene_t0_coord, fFamily_scene_t0_head,
					iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					FE_ANY_wander_family_event,
					"FMTAUD", "", "RONEX_IG_3", tCreatedTrevor0ConvLabels,
					MyLocalPedStruct, sSpeechTimer)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
			AND DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
				BOOL bPlayPTFX
				bPlayPTFX = TRUE
				
				IF (iFamily_enter_veh_stage[eSceneMember] = 3)
					IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[eSceneMember])
					AND IS_ENTITY_ATTACHED_TO_ENTITY(family_prop_l[eSceneMember], family_ped[eSceneMember])
						IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) >= 0.3
							bPlayPTFX = FALSE
						ENDIF
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) >= 0.6
							DETACH_ENTITY(family_prop_l[eSceneMember])
						ENDIF
					ENDIF
				ENDIF
				
				IF bPlayPTFX
					PRIVATE_Update_Family_Ptfx_Looped(family_prop_r[eSceneMember], eFamilyEvent,
							"scr_fam_moonshine_pour", family_ptfx[eSceneMember], 0)
				ELSE
					IF DOES_PARTICLE_FX_LOOPED_EXIST(family_ptfx[eSceneMember])
						STOP_PARTICLE_FX_LOOPED(family_ptfx[eSceneMember])
					ENDIF
				ENDIF
			ENDIF
			
			CreateBarrelHide(TRUE)
		BREAK
		CASE FE_T0_RONEX_doing_target_practice
			VECTOR VecCenterCoors, VecPropCoord
			FLOAT radius
			
			IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
				VecPropCoord = GET_ENTITY_COORDS(family_prop_l[eSceneMember])
			ELIF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
				VecPropCoord = GET_ENTITY_COORDS(family_prop_r[eSceneMember])
			ELSE
				VecPropCoord = GET_ENTITY_COORDS(family_ped[eSceneMember])
			ENDIF
			
			VecCenterCoors = (GET_ENTITY_COORDS(family_ped[eSceneMember])+VecPropCoord) / 2.0
			radius = VDIST(VecCenterCoors, GET_ENTITY_COORDS(family_ped[eSceneMember])) - 0.1
			
			IF NOT IS_POSITION_OCCUPIED(VecCenterCoors, radius,
					FALSE,	//BOOL bBuildingFlag,
					TRUE,	//BOOL bVehicleFlag,
					TRUE,	//BOOL bPedFlag,
					FALSE,	//BOOL bObjectFlag,
					FALSE,	//BOOL bDummyFlag,
					family_ped[eSceneMember])
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilySphere(VecCenterCoors, radius, HUD_COLOUR_BLUE,0.25)
				#ENDIF
				
				IF PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[eSceneMember], TRUE, FALSE)
				ENDIF
			ELSE
				
				#IF IS_DEBUG_BUILD
				DrawDebugFamilySphere(VecCenterCoors, radius, HUD_COLOUR_RED,0.25)
				#ENDIF
				
				PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
						vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[eSceneMember],
						TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
						WALK_BLEND_IN, WALK_BLEND_OUT)
			ENDIF

			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
				//
			ELSE
				
				
				IF IS_PED_RAGDOLL(family_ped[eSceneMember])
					//damaged by vehicle...
					
					CLEAR_PED_TASKS(family_ped[eSceneMember])
					iFamily_enter_veh_stage[eSceneMember] = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_find_family_event)
					CASSERTLN(DEBUG_FAMILY, "ped is ragdolling...")
				ENDIF
				
				IF NOT HAS_PED_GOT_WEAPON(family_ped[eSceneMember], WEAPONTYPE_PISTOL)
					GIVE_WEAPON_TO_PED(family_ped[eSceneMember], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				ELSE
					FLOAT fPhaseTime
					fPhaseTime = GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember])
					
					BOOL bBottlesUntouched
					bBottlesUntouched = FALSE
					
					IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str
						str  = "family_prop_l "
						str += GET_ENTITY_HEALTH(family_prop_l[eSceneMember])
						#ENDIF
						
						IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(family_prop_l[eSceneMember], PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_prop_l[eSceneMember], FALSE), 0, HUD_COLOUR_BLUE)
							#ENDIF
							
							bBottlesUntouched = TRUE
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_prop_l[eSceneMember], FALSE), 0, HUD_COLOUR_RED)
							#ENDIF
							
							FREEZE_ENTITY_POSITION(family_prop_l[eSceneMember], FALSE)
						ENDIF
					ELSE
//						#IF IS_DEBUG_BUILD
//						DrawDebugFamilyTextWithOffset("family_prop_l doesnt exist", GET_ENTITY_COORDS(family_prop_l[eSceneMember], FALSE), 0, HUD_COLOUR_BLACK)
//						#ENDIF
						
						bBottlesUntouched = TRUE
					ENDIF
					IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
						#IF IS_DEBUG_BUILD
						TEXT_LABEL_63 str
						str  = "family_prop_r "
						str += GET_ENTITY_HEALTH(family_prop_r[eSceneMember])
						#ENDIF
						
						IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(family_prop_r[eSceneMember], PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_prop_r[eSceneMember], FALSE), 0, HUD_COLOUR_BLUE)
							#ENDIF
							
							bBottlesUntouched = TRUE
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_prop_r[eSceneMember], FALSE), 0, HUD_COLOUR_RED)
							#ENDIF
							
							FREEZE_ENTITY_POSITION(family_prop_r[eSceneMember], FALSE)
						ENDIF
					ELSE
//						#IF IS_DEBUG_BUILD
//						DrawDebugFamilyTextWithOffset("family_prop_r doesnt exist", GET_ENTITY_COORDS(family_prop_r, FALSE), 0, HUD_COLOUR_BLACK)
//						#ENDIF
						
						bBottlesUntouched = TRUE
					ENDIF
					
					IF NOT bBottlesUntouched
						//both bottles gone...
						iFamily_enter_veh_stage[eSceneMember] = 0
						PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
						CPRINTLN(DEBUG_FAMILY, "both bottles 'touched'...")
					ENDIF
					
					
					FLOAT ReturnStartPhase, ReturnEndPhase
					ReturnStartPhase = -1
					ReturnEndPhase = -1
					IF FIND_ANIM_EVENT_PHASE(tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
							"shoot", ReturnStartPhase, ReturnEndPhase)
						
						IF  (ReturnStartPhase > 0)
						AND (ReturnEndPhase > 0)
							IF (fPhaseTime >= ReturnStartPhase)
							AND (fPhaseTime <= (ReturnEndPhase+0.1))
								IF (iFamily_enter_veh_stage[eSceneMember] = 0)
									VECTOR vGunOffset, vShotCoord
									vGunOffset = <<0.1,0.0,0.043>>
									
									IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
										vShotCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(family_prop_l[eSceneMember],
												vGunOffset+<<2,0,0>>)
										FREEZE_ENTITY_POSITION(family_prop_l[eSceneMember], FALSE)
									ELIF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
										vShotCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(family_prop_r[eSceneMember],
												vGunOffset+<<2,0,0>>)
										FREEZE_ENTITY_POSITION(family_prop_r[eSceneMember], FALSE)
									ELSE
										//both bottles gone...
										iFamily_enter_veh_stage[eSceneMember] = 0
										PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
										
										CWARNINGLN(DEBUG_FAMILY, "both bottles gone...")
									ENDIF
									
									SET_PED_RESET_FLAG(family_ped[eSceneMember], PRF_SupressGunfireEvents, TRUE)
									
									SET_PED_SHOOTS_AT_COORD(family_ped[eSceneMember], vShotCoord)
									iFamily_enter_veh_stage[eSceneMember]++
								ELSE
									#IF IS_DEBUG_BUILD
									DrawDebugFamilyTextWithOffset("iFamily_enter_veh_stage <> 0", GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 4, HUD_COLOUR_BLACK)
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								TEXT_LABEL_63 str
								str  = ""
								str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
								str +=  " > "
								str += GET_STRING_FROM_FLOAT(fPhaseTime)
								str +=  " > "
								str += GET_STRING_FROM_FLOAT((ReturnEndPhase+0.1))
								DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 4, HUD_COLOUR_BLACK)
								#ENDIF
								
								iFamily_enter_veh_stage[eSceneMember] = 0
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							DrawDebugFamilyTextWithOffset("invalid ReturnStartPhase", GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 4, HUD_COLOUR_BLACK)
							#ENDIF
							
							iFamily_enter_veh_stage[eSceneMember] = 0
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						DrawDebugFamilyTextWithOffset("NOT FIND_ANIM_EVENT_PHASE", GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 4, HUD_COLOUR_BLACK)
						#ENDIF
					
						iFamily_enter_veh_stage[eSceneMember] = 0
					ENDIF
				ENDIF
			ENDIF
			
			CreateBarrelHide(TRUE)
		BREAK
		
		CASE FE_T0_KIDNAPPED_WIFE_cleaning
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"KIDNAPPED_WIFE_CLEANING", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_does_garden_work
			IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
					"KIDNAPPED_WIFE_DOES_GARDEN_WORK", "", NULL, family_sfxBank[eSceneMember])
			ENDIF
			PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
					tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					vFamily_scene_t0_coord+<<0,0,1>>, fFamily_scene_t0_head)
			
			CreateBarrelHide(TRUE)
		BREAK
		CASE FE_T0_KIDNAPPED_WIFE_talks_to_Michael
			IF (eSceneMember = PS_T0_WIFE)
				PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
						eFamilyMember, eFamilyEvent,
					 	tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
					 	vFamily_scene_t0_coord, fFamily_scene_t0_head,
						iFamily_synch_scene[PS_T0_WIFE], TRUE, TRUE)
				
				IF (g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL] <> eFamilyEvent)
					iFamily_enter_veh_stage[eSceneMember] = 0
					PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(family_ped[eSceneMember])
					PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
							eFamilyMember, eFamilyEvent,
						 	tDialogueAnimDicts[PS_T0_WIFE], tDialogueAnimClips[eSceneMember],
						 	tDialogueAnimClips[PS_T0_WIFE], "_PATRICIA",
							iFamily_synch_scene[PS_T0_WIFE])
				ELSE
					PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
							vFamily_scene_t0_coord, fFamily_scene_t0_head,
							iFamily_synch_scene[eSceneMember],
							TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
										
				ENDIF
			ENDIF
			
			IF (g_eCurrentSafehouseActivity = SA_TREVOR_BEER)
				PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, FE_ANY_wander_family_event)
			ENDIF
		BREAK
		
		CASE FE_T0_MOTHER_duringRandomChar
			PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
			SET_DOOR_STATE(DOORNAME_T_TRAILER_CS, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
/*
<"FMT_5_1a" description="Player returns to his trailer before completing the mission - v1" random="False">>
<"FMT_5_1aa" description="Player returns to his trailer before completing the mission - v1b" random="False">>

<"FMT_5_1b" description="Player returns to his trailer before completing the mission - v2" random="False">>
<"FMT_5_1ba" description="Player returns to his trailer before completing the mission - v2b" random="False">>

<"FMT_5_1c" description="Player returns to his trailer before completing the mission - v3" random="False">>
<"FMT_5_1ca" description="Player returns to his trailer before completing the mission - v3b" random="False">>

<"FMT_5_1d" description="Player returns to his trailer before completing the mission - v4" random="False">>
<"FMT_5_1da" description="Player returns to his trailer before completing the mission - v4b" random="False">>

<"FMT_5_1e" description="Player returns to his trailer before completing the mission - v5" random="False">>
<"FMT_5_1ea" description="Player returns to his trailer before completing the mission - v5b" random="False">>

*/
			
			IF IS_STRING_NULL_OR_EMPTY(tCreatedTrevor0MotherConvLabel)
				IF Play_This_Family_Speech(family_ped[eSceneMember], eFamilyEvent,
						MyLocalPedStruct, "FMTAUD", tCreatedTrevor0ConvLabels,
						sSpeechTimer, iRandSpeechCount[eSceneMember],
						7.5, "FMT_5_1")
					tCreatedTrevor0MotherConvLabel = tCreatedTrevor0ConvLabels[0]
				ENDIF
			ELSE
				
				INT iMotherRandSpeechCount
				iMotherRandSpeechCount = iRandSpeechCount[eSceneMember]-1
				IF Play_This_Family_Speech(family_ped[eSceneMember], eFamilyEvent,
						MyLocalPedStruct, "FMTAUD", tCreatedTrevor0ConvLabels,
						sSpeechTimer, iMotherRandSpeechCount,
						7.5, tCreatedTrevor0MotherConvLabel)
					tCreatedTrevor0MotherConvLabel = ""
				ENDIF
			ENDIF
			
		BREAK
//		CASE FE_T0_MOTHER_something_b
//			PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//		BREAK
//		CASE FE_T0_MOTHER_something_c
//			PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//		BREAK
		
		CASE FE_ANY_find_family_event
			PRIVATE_Update_Family_Find_Event(family_ped[eSceneMember], eFamilyMember, vFamily_scene_t0_coord)
		BREAK
		CASE FE_ANY_wander_family_event
			PRIVATE_Update_Family_Wander_Event(family_ped[eSceneMember], eFamilyMember,
					iFamily_enter_veh_stage[eSceneMember],
					family_prop_l[eSceneMember], eFamilyPropLModel[eSceneMember],
					family_prop_r[eSceneMember], eFamilyPropRModel[eSceneMember],
					family_chair[eSceneMember],
					iFamily_synch_scene[eSceneMember])
		BREAK
		
		CASE FAMILY_MEMBER_BUSY	FALLTHRU
		CASE NO_FAMILY_EVENTS
			//
		BREAK
		
		DEFAULT
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in Control_Trevor_FamilyEvent_Tasks()")
			#ENDIF
			
			CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent in Control_Trevor_FamilyEvent_Tasks()")
			
		BREAK
	ENDSWITCH
	
	IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
		PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember])
		
		SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_T, TRUE)
		
		Initialise_Trevor_Family_Prop_Variables(eSceneMember)
		family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
		
		#IF IS_DEBUG_BUILD
		VECTOR vDebugJumpOffset
		Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember],	vDebugJumpOffset)
		Get_DebugJumpAngle_From_Vector(vDebugJumpOffset, fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember])
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL Moniter_A_Trevor0_Scene_Member(PS_T0_SCENE_MEMBERS eSceneMember)
	enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eSceneMember)
	
	#IF IS_DEBUG_BUILD
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), vFamily_scene_t0_coord,						HUD_COLOUR_BLUE,30.0/255.0)
//	DrawDebugFamilySphere(vFamily_scene_t0_coord, 0.5,																HUD_COLOUR_BLUE,30.0/255.0)
//	
//	DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),	HUD_COLOUR_YELLOW)
	
	TEXT_LABEL_63 str
	VECTOR xyzDraw = GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE)
	
	str = Get_String_From_Family_Scene_T0_Scene_Members(eSceneMember)
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 0, HUD_COLOUR_BLACK)
	
	str = "event: "
	str += Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember])
	DrawDebugFamilyTextWithOffset(str, xyzDraw, 1, HUD_COLOUR_BLUE)
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_bUpdatedFamilyEvents
	#ENDIF
		Control_Trevor_FamilyEvent_Tasks(eSceneMember, g_eCurrentFamilyEvent[eFamilyMember])
	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_FAMILY, "don't Control  ", Get_String_From_Family_Scene_T0_Scene_Members(eSceneMember), " FamilyEvent Tasks ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), ", g_bUpdatedFamilyEvents = ", g_bUpdatedFamilyEvents)
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

// *******************************************************************************************
//	MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Moniter_All_Scene_Members()
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	bUpdateInteriorForPlayer = FALSE
	BOOL bCheckEachFrameForFamilyMemberSpeech = FALSE
	UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
			iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_t0_coord, "v_trailer")
					
	PS_T0_SCENE_MEMBERS eMember
	REPEAT MAX_T0_SCENE_MEMBERS eMember
		IF Is_Trevor0_Scene_Member_Active(eMember)
			
			enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eMember)
			MODEL_NAMES eModelIfNoChar = DUMMY_MODEL_FOR_SCRIPT
			enumCharacterList eFamilyChar = PRIVATE_Get_CharList_From_FamilyMember(eFamilyMember, eModelIfNoChar)
			IF (eFamilyChar = GET_CURRENT_PLAYER_PED_ENUM())
				IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
					g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
					
					IF DOES_ENTITY_EXIST(family_prop_l[eMember])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(family_prop_l[eMember], PLAYER_PED_ID())
							DETACH_ENTITY(family_prop_l[eMember])
							CWARNINGLN(DEBUG_FAMILY, "detach family_prop_l[eMember] from player!!!")
						ELSE
							STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eMember], NORMAL_BLEND_OUT, TRUE)
							CWARNINGLN(DEBUG_FAMILY, "stop synch family_prop_l[eMember] from player!!!")
						ENDIF
						
						IF DOES_ENTITY_EXIST(family_prop_l[eMember])
							FREEZE_ENTITY_POSITION(family_prop_l[eMember], FALSE)
							SET_ENTITY_DYNAMIC(family_prop_l[eMember], TRUE)
							SET_ENTITY_COLLISION(family_prop_l[eMember], TRUE)
							
							APPLY_FORCE_TO_ENTITY(family_prop_l[eMember], APPLY_TYPE_IMPULSE, << 0.0, 0.0, -0.1 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
						ENDIF
						
						eFamilyPropLModel[eMember] = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
					IF DOES_ENTITY_EXIST(family_prop_r[eMember])
						IF IS_ENTITY_ATTACHED_TO_ENTITY(family_prop_r[eMember], PLAYER_PED_ID())
							DETACH_ENTITY(family_prop_r[eMember])
							CWARNINGLN(DEBUG_FAMILY, "detach family_prop_r[eMember] from player!!!")
						ELSE
							STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eMember], NORMAL_BLEND_OUT, TRUE)
							CWARNINGLN(DEBUG_FAMILY, "stop synch family_prop_r[eMember] from player!!!")
						ENDIF
						
						IF DOES_ENTITY_EXIST(family_prop_r[eMember])
							FREEZE_ENTITY_POSITION(family_prop_r[eMember], FALSE)
							SET_ENTITY_DYNAMIC(family_prop_r[eMember], TRUE)
							SET_ENTITY_COLLISION(family_prop_r[eMember], TRUE)
							
							APPLY_FORCE_TO_ENTITY(family_prop_r[eMember], APPLY_TYPE_IMPULSE, << 0.0, 0.0, -0.1 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
						ENDIF
						
						eFamilyPropRModel[eMember] = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
					
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_FAMILY, "<", GET_THIS_SCRIPT_NAME(), "> g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] = NO_FAMILY_EVENTS for ", "\"eFamilyChar = GET_CURRENT_PLAYER_PED_ENUM!!!\"")
					#ENDIF
					
					IF eFamilyChar = CHAR_TREVOR
						INT iLegsDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG)
						
						IF (iLegsDrawable = 25)
							
							CWARNINGLN(DEBUG_FAMILY, "start changing Trev out of toilet outfit!!!")
							
							WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P2_SWEAT_PANTS, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_FEET, FEET_P2_REDWINGS, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P2_DUMMY, FALSE)
							OR NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P2_NONE, FALSE)
								WAIT(0)
							ENDWHILE
							
							CWARNINGLN(DEBUG_FAMILY, "finished changing Trev out of toilet outfit!!!")
							
						ENDIF
						
					ENDIF
					
					CWARNINGLN(DEBUG_FAMILY, "eFamilyChar = GET_CURRENT_PLAYER_PED_ENUM!!!")
				ENDIF
			ENDIF
			
			Moniter_A_Trevor0_Scene_Member(eMember)
			
			IF NOT bCheckEachFrameForFamilyMemberSpeech
				//#1273022
				IF NOT g_savedGlobals.sFamilyData.bHeardTrevorCountry
					IF GET_MISSION_COMPLETE_STATE(SP_MISSION_EXILE_1)
					AND IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
						INT iFakeSpeechCount = 0
						IF Play_This_Family_Speech(family_ped[eMember],
								g_eCurrentFamilyEvent[eFamilyMember],
								MyLocalPedStruct, "FMTAUD", tCreatedTrevor0ConvLabels,
								sSpeechTimer, iFakeSpeechCount, 8.0, "FMT_COUNTRY")
							bCheckEachFrameForFamilyMemberSpeech = TRUE
							g_savedGlobals.sFamilyData.bHeardTrevorCountry = TRUE
						ENDIF
					ENDIF
				ENDIF
					
				IF (eMember = PS_T0_WIFE)
					IF NOT bSaidHiToWife
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT IS_PED_INJURED(family_ped[eMember])
							BOOL bSafeToSaidHiTokidwifeener = TRUE
							
							VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
							VECTOR vMemberCoord = GET_ENTITY_COORDS(family_ped[eMember])

							CONST_FLOAT fMAX_DIST_FOR_SPEECH_2D	5.0
							IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, vMemberCoord, FALSE) > fMAX_DIST_FOR_SPEECH_2D)
								bSafeToSaidHiTokidwifeener = FALSE
							ENDIF
							
							IF bSafeToSaidHiTokidwifeener
								PLAY_PED_AMBIENT_SPEECH(family_ped[eMember], "GENERIC_HI", SPEECH_PARAMS_FORCE)
								RESTART_TIMER_NOW(sSpeechTimer)
								
								bSaidHiToWife = TRUE
								CPRINTLN(DEBUG_FAMILY, "bSaidHiToWife")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT Is_Dialogue_Anim_Drinking(family_ped[eMember], tDialogueAnimDicts[eMember], tDialogueAnimClips[eMember], iFamily_synch_scene[eMember], sSpeechTimer)
					IF Play_This_Family_Speech(family_ped[eMember],
							g_eCurrentFamilyEvent[eFamilyMember],
							MyLocalPedStruct, "FMTAUD", tCreatedTrevor0ConvLabels,
							sSpeechTimer, iRandSpeechCount[eMember], 3.0)
						bCheckEachFrameForFamilyMemberSpeech = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
					iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_t0_coord, "v_trailer")
			bUpdateInteriorForPlayer = TRUE
		ENDIF
	ENDREPEAT
	TimedStopFamilyFires(family_ped, sFireTimer)
	
	#IF IS_DEBUG_BUILD
	IF g_bDebugForceCreateFamily
		
		INT iDebugForceCreateFamily =  0
		REPEAT MAX_T0_SCENE_MEMBERS eMember
			enumFamilyMember eFamilyMember = Get_Family_Member_From_Trevor0_Scene_Member(eMember)
			IF (g_eCurrentFamilyEvent[g_eDebugSelectedMember] = g_eCurrentFamilyEvent[eFamilyMember])
				IF DOES_ENTITY_EXIST(family_ped[eMember])
					CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_T0_Scene_Members(eMember), "] exists")
					iDebugForceCreateFamily++
				ELSE
					CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_T0_Scene_Members(eMember), "] doesnt exist")
					iDebugForceCreateFamily = -9999
				ENDIF
			ELSE
				CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_T0_Scene_Members(eMember), "] has different event ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
				
			ENDIF
		ENDREPEAT
		
		IF iDebugForceCreateFamily > 0
			g_bDebugForceCreateFamily = FALSE
		ENDIF
	ENDIF
	#ENDIF
	
//	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())		//#1453993 / #1486430
	
	RETURN TRUE
ENDFUNC

// *******************************************************************************************
//	MAIN SCRIPT
// *******************************************************************************************
SCRIPT(structFamilyScene thisFamilyScene)
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Family_Scene_T0_Cleanup()
	ENDIF
	
	WAIT(0)
	
	Initialise_Family_Scene_T0_Variables(thisFamilyScene)
	Setup_Family_Scene_T0()
	
	#IF IS_DEBUG_BUILD
	Create_Family_Scene_T0_Widget()
	#ENDIF
	
	WHILE IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(CHAR_MICHAEL, vFamily_scene_t0_coord, 1.5)
	OR IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(CHAR_TREVOR, vFamily_scene_t0_coord, 1.5)
		WAIT(0)
		
		Moniter_All_Scene_Members()
		Moniter_Player_Griefing(BIT_TREVOR, sFamilyGriefing, vFamily_scene_t0_coord, <<12.5,12.5,10>>, family_veh)
		
		#IF IS_DEBUG_BUILD
		Watch_Family_Scene_T0_Widget()
		Family_Scene_T0_Debug_Options()
		#ENDIF
	ENDWHILE
	
	Family_Scene_T0_Cleanup()
ENDSCRIPT
