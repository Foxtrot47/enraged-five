// _________________________________________________________________________________________
// _________________________________________________________________________________________
// ___                                                                                   ___
// ___              Author:  Alwyn Roberts          Date: 21/04/2010                     ___
// _________________________________________________________________________________________
// ___                                                                                   ___
// ___      (A1)  - Family Scene M file for use - family_scene_m.sc                      ___
// ___                                                                                   ___
// _________________________________________________________________________________________


//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT   USE_TU_CHANGES  1


USING "rage_builtins.sch"
USING "globals.sch"

USING "model_enums.sch"

//- commands headers    -//
USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_interiors.sch"
USING "commands_misc.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"

//- script headers  -//
USING "script_blips.sch"
USING "script_player.sch"
USING "script_ped.sch"
USING "script_misc.sch"

//- public headers  -//
USING "dialogue_public.sch"
USING "timer_public.sch"
USING "fake_cellphone_public.sch"
USING "family_public.sch"

//- private headers -//
USING "family_private.sch"
USING "family_scene_private.sch"

#IF IS_DEBUG_BUILD
//- debug headers   -//
    USING "shared_debug.sch"
    USING "family_debug.sch"
#ENDIF

// *******************************************************************************************
//  CONSTANTS
// *******************************************************************************************

// *******************************************************************************************
//  ENUMERATIONS
// *******************************************************************************************
ENUM PS_M_SCENE_MEMBERS
    PS_M_SON = 0,
    PS_M_DAUGHTER,
    PS_M_WIFE,
    PS_M_MEXMAID,
    PS_M_GARDENER,
    
    MAX_M_SCENE_MEMBERS,
    NO_M_SCENE_MEMBER
ENDENUM
ENUM PS_M_SCENE_VEHICLES
    PS_MV_SON_bike = 0,
    PS_MV_DAUGHTER_issi,
    PS_MV_WIFE_sentinal,
    
    MAX_SCENE_VEHICLES,
    NO_FAMILY_VEHICLE
ENDENUM
BOOL bAmbientVehicleOutside[MAX_SCENE_VEHICLES]

// *******************************************************************************************
//  STRUCTURES
// *******************************************************************************************
FAKE_CELLPHONE_DATA sDaughterFakeCellphoneData
structFamilyGriefing sFamilyGriefing

// *******************************************************************************************
//  VARIABLES
// *******************************************************************************************
        //- Flags(INT and BOOL) -//
BOOL                    bFamily_Scene_M_in_progress                 = TRUE
BOOL                    bSaidHiToGardener                           = FALSE //1445877
BOOL                    bRespondedToTraceyPurging                   = FALSE //1445744
BOOL                    bForceCreation                              = FALSE

    //- People(PED_INDEX and MODEL_NAMES) -//
PED_INDEX               family_ped[MAX_M_SCENE_MEMBERS]

PED_INDEX               family_extra_ped[MAX_M_SCENE_MEMBERS]
MODEL_NAMES             eFamilyExtraPedModel[MAX_M_SCENE_MEMBERS]
VECTOR                  vFamilyExtraPedCoordOffset[MAX_M_SCENE_MEMBERS]
FLOAT                   fFamilyExtraPedHeadOffset[MAX_M_SCENE_MEMBERS]

    //- Vehicles(VEHICLE_INDEX and MODEL_NAMES) -//
VEHICLE_INDEX           family_veh[MAX_SCENE_VEHICLES]
INT                     iFamily_enter_veh_stage[MAX_M_SCENE_MEMBERS]    // iFamily_extra_enter_veh_stage[MAX_M_SCENE_MEMBERS]

    //- Hotswap stuff(SELECTOR_PED_STRUCT and SELECTOR_CAM_STRUCT) -//

    //- Objects(OBJECT_INDEX and MODEL_NAMES and INTERIOR_INSTANCE_INDEX) -//
OBJECT_INDEX            family_chair[MAX_M_SCENE_MEMBERS]
OBJECT_INDEX            family_door[MAX_M_SCENE_MEMBERS]

OBJECT_INDEX            family_prop_l[MAX_M_SCENE_MEMBERS], family_prop_r[MAX_M_SCENE_MEMBERS]
MODEL_NAMES             eFamilyPropLModel[MAX_M_SCENE_MEMBERS], eFamilyPropRModel[MAX_M_SCENE_MEMBERS]
PED_BONETAG             eFamilyPropLAttachBonetag[MAX_M_SCENE_MEMBERS], eFamilyPropRAttachBonetag[MAX_M_SCENE_MEMBERS]
VECTOR                  vFamilyPropLAttachOffset[MAX_M_SCENE_MEMBERS], vFamilyPropRAttachOffset[MAX_M_SCENE_MEMBERS]
VECTOR                  vFamilyPropLAttachRotation[MAX_M_SCENE_MEMBERS], vFamilyPropRAttachRotation[MAX_M_SCENE_MEMBERS]

OBJECT_INDEX            family_sink_prop
MODEL_NAMES             eFamilySinkPropModel
PED_BONETAG             eFamilySinkPropAttachBonetag
VECTOR                  vFamilySinkPropAttachOffset
VECTOR                  vFamilySinkPropAttachRotation

ROPE_INDEX              lawnMowerRope

OBJECT_INDEX            world_chalk_board_prop, script_chalk_board_prop
MODEL_NAMES             eChalkBoardPropModel
VECTOR                  vChalkBoardPropAttachOffset
VECTOR                  vChalkBoardPropAttachRotation

    //- Other Models(MODEL_NAMES) -//

    //- Sequences(SEQUENCE_INDEX and SCRIPTTASKSTATUS) -//

    //- Blips (BLIP_INDEX) -//

    //- Coordinates(VECTORS and FLOATS) -//
VECTOR              vFamily_scene_m_coord
FLOAT               fFamily_scene_m_head

VECTOR              vFamily_coordOffset[MAX_M_SCENE_MEMBERS]
FLOAT               fFamily_headOffset[MAX_M_SCENE_MEMBERS]

    //- Pickups(INT) -//
BOOL                family_b_ptfx[MAX_M_SCENE_MEMBERS]
PTFX_ID             family_ptfx[MAX_M_SCENE_MEMBERS], family_other_ptfx[2]
INT                 family_sfxStage[MAX_M_SCENE_MEMBERS], family_sfxID[MAX_M_SCENE_MEMBERS]
TEXT_LABEL_63       family_sfxBank[MAX_M_SCENE_MEMBERS]

INT                 iFamily_navmeshBlock[MAX_M_SCENE_MEMBERS]

    //- Text Label(TEXT_LABEL) -//
TEXT_LABEL_63       tDialogueAnimDicts[MAX_M_SCENE_MEMBERS], tDialogueAnimClips[MAX_M_SCENE_MEMBERS]
TEXT_LABEL          tCreatedMichaelConvLabels[5]

    //- Cover Points(COVERPOINT_INDEX and COVERPOINT_USAGE COVERPOINT_HEIGHT and COVERPOINT_ARC) -//

    //- Cameras(CAMERA_INDEX AND INT) -//

    //- Textures(TXD_ID and TEXTURE_ID) -//
BOOL bUpdateInteriorForPlayer = FALSE
structTimer sUpdatePlayerInteriorTimer
INTERIOR_INSTANCE_INDEX iInteriorForPlayer, iInteriorForSafehouse, iInteriorForMember[MAX_M_SCENE_MEMBERS]
enumFamilyDoors current_family_doors[MAX_M_SCENE_MEMBERS]

DECAL_ID familyDecal

    //- Timers(structTimer) -//
structTimer sSpeechTimer, sFireTimer
INT iComingHomeFamilySpeechStage
INT iRandSpeechCount[MAX_M_SCENE_MEMBERS]

    //- Emails(INT and EMAIL_ID and EMAIL_PHOTO_ID) -//

    //- Audio(INT and STRING) -//
structPedsForConversation   MyLocalPedStruct
INT iDAUGHTER_workout_with_mp3_dialogue = 0
structTimer sDAUGHTER_special_case_timer

#IF IS_DEBUG_BUILD
    //- Widget stuff(WIDGET_ID and BOOL and INT and FLOAT and VECTOR) -//
WIDGET_GROUP_ID family_scene_m_widget, family_scene_m_subwidget, chalk_board_widget
TEXT_WIDGET_ID  family_anim_widget[MAX_M_SCENE_MEMBERS], family_animFlag_widget[MAX_M_SCENE_MEMBERS], chalk_board_textWidget
BOOL            bToggle_Family_Scene_M_Widget

INT             iCurrentFamilyEvent[MAX_M_SCENE_MEMBERS]

BOOL            bJumpToFamilyMember[MAX_M_SCENE_MEMBERS]
FLOAT           fPlayerDebugJumpAngle[MAX_M_SCENE_MEMBERS], fPlayerDebugJumpDistance[MAX_M_SCENE_MEMBERS]

FLOAT           fFamily_synch_scenePhase[MAX_M_SCENE_MEMBERS]

BOOL            bMove_peds, bDraw_peds, bSave_peds 
BOOL            bMove_vehs, bMove_prop, bUpdate_chair_attach

INT             iMichaelScheduleStage

#ENDIF
#IF NOT IS_DEBUG_BUILD
BOOL            bMove_peds, bUpdate_chair_attach
#ENDIF

    //- other Ints(INT) -//
INT             iFamily_synch_scene[MAX_M_SCENE_MEMBERS]
INT             intention_index

    //- other Floats(FLOAT) -//

    //- TEMP RUBBISH -//

// *******************************************************************************************
//  GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

FUNC enumFamilyMember Get_Family_Member_From_Michael_Scene_Member(PS_M_SCENE_MEMBERS eSceneMember)
    SWITCH eSceneMember
        CASE PS_M_SON
            RETURN FM_MICHAEL_SON
        BREAK
        CASE PS_M_DAUGHTER
            RETURN FM_MICHAEL_DAUGHTER
        BREAK
        CASE PS_M_WIFE
            RETURN FM_MICHAEL_WIFE
        BREAK
        CASE PS_M_MEXMAID
            RETURN FM_MICHAEL_MEXMAID
        BREAK
        CASE PS_M_GARDENER
            RETURN FM_MICHAEL_GARDENER
        BREAK
//      CASE PS_M_MICHAEL
//          RETURN FM_TREVOR_0_MICHAEL
//      BREAK
        
        CASE MAX_M_SCENE_MEMBERS
            RETURN MAX_FAMILY_MEMBER
        BREAK
    ENDSWITCH
    
    RETURN NO_FAMILY_MEMBER
ENDFUNC

FUNC PS_M_SCENE_VEHICLES Get_FamilyScene_Vehicle_From_Michael_Scene_Member(PS_M_SCENE_MEMBERS eSceneMember)
    
    SWITCH eSceneMember
        CASE PS_M_SON
            
            IF (g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_Borrows_sisters_car)
                RETURN PS_MV_DAUGHTER_issi
            ENDIF
            
            RETURN PS_MV_SON_bike
        BREAK
        CASE PS_M_DAUGHTER
            RETURN PS_MV_DAUGHTER_issi
        BREAK
        CASE PS_M_WIFE
            RETURN PS_MV_WIFE_sentinal
        BREAK
        
        CASE MAX_M_SCENE_MEMBERS
            RETURN MAX_SCENE_VEHICLES
        BREAK
    ENDSWITCH
    
    RETURN NO_FAMILY_VEHICLE
ENDFUNC


#IF IS_DEBUG_BUILD
FUNC STRING Get_String_From_Family_Scene_M_Scene_Members(PS_M_SCENE_MEMBERS eSceneMember)
    SWITCH eSceneMember
        CASE PS_M_SON
            RETURN "PS_M_SON"
        BREAK
        CASE PS_M_DAUGHTER
            RETURN "PS_M_DAUGHTER"
        BREAK
        CASE PS_M_WIFE
            RETURN "PS_M_WIFE"
        BREAK
        CASE PS_M_MEXMAID
            RETURN "PS_M_MEXMAID"
        BREAK
        CASE PS_M_GARDENER
            RETURN "PS_M_GARDENER"
        BREAK
//      CASE PS_M_MICHAEL
//          RETURN "PS_M_MICHAEL"
//      BREAK
        
        CASE MAX_M_SCENE_MEMBERS
            RETURN "MAX_M_SCENE_MEMBERS"
        BREAK
    ENDSWITCH
    
    RETURN "Get_String_From_Family_Scene_M_Scene_Members(null), "
ENDFUNC
FUNC STRING Get_String_From_Family_Scene_M_Scene_vehicles(PS_M_SCENE_VEHICLES this_family_scene_m_vehicle)
    SWITCH this_family_scene_m_vehicle
        CASE PS_MV_SON_bike
            RETURN "PS_MV_SON_bike"
        BREAK
        CASE PS_MV_DAUGHTER_issi
            RETURN "PS_MV_DAUGHTER_issi"
        BREAK
        CASE PS_MV_WIFE_sentinal
            RETURN "PS_MV_WIFE_sentinal"
        BREAK
        
        CASE MAX_SCENE_VEHICLES
            RETURN "MAX_SCENE_VEHICLES"
        BREAK
        CASE NO_FAMILY_VEHICLE
            RETURN "NO_FAMILY_VEHICLE"
        BREAK
    ENDSWITCH
    
    RETURN "Get_String_From_Family_Scene_M_Scene_vehicles(null), "
ENDFUNC
#ENDIF

//PURPOSE:  Called on completion of the mission to release memory used for models etc
PROC Family_Scene_M_Cleanup()
    
    
    PRIVATE_Cleanup_Family_Chairs(family_ped, family_chair)
        
    INT iCleanup
    
//  REPEAT COUNT_OF(family_ped) iCleanup
//      IF DOES_ENTITY_EXIST(family_ped[iCleanup])
//          SAVE_STRING_TO_DEBUG_FILE("family_ped[")
//          SAVE_INT_TO_DEBUG_FILE(iCleanup)
//          SAVE_STRING_TO_DEBUG_FILE("] exists")
//          SAVE_NEWLINE_TO_DEBUG_FILE()
//      ELSE
//          SAVE_STRING_TO_DEBUG_FILE("family_ped[")
//          SAVE_INT_TO_DEBUG_FILE(iCleanup)
//          SAVE_STRING_TO_DEBUG_FILE("] DOESNT exist")
//          SAVE_NEWLINE_TO_DEBUG_FILE()
//      ENDIF
//  ENDREPEAT
//  SAVE_NEWLINE_TO_DEBUG_FILE()
    
    PRIVATE_GenericFamilySceneCleanup(iFamily_synch_scene,
            family_veh, family_ped,
            family_prop_l, eFamilyPropLModel,
            family_extra_ped, eFamilyExtraPedModel,
            family_sfxStage, family_sfxID, family_sfxBank)
    
    IF intention_index != NEW_CONTEXT_INTENTION
        RELEASE_CONTEXT_INTENTION(intention_index)
    ENDIF
    REPEAT COUNT_OF(family_prop_r) iCleanup
        SET_OBJECT_AS_NO_LONGER_NEEDED(family_door[iCleanup])
        IF ShouldDeleteFamilyEntity(family_prop_r[iCleanup], "family_prop_r", -1)
            DELETE_OBJECT(family_prop_r[iCleanup])
        ENDIF
        IF (eFamilyPropRModel[iCleanup] <> DUMMY_MODEL_FOR_SCRIPT)
            SET_MODEL_AS_NO_LONGER_NEEDED(eFamilyPropRModel[iCleanup])
        ENDIF
    ENDREPEAT
    IF ShouldDeleteFamilyEntity(family_sink_prop, "family_sink_prop", -1)
        DELETE_OBJECT(family_sink_prop)
    ENDIF
    IF (eFamilySinkPropModel <> DUMMY_MODEL_FOR_SCRIPT)
        SET_MODEL_AS_NO_LONGER_NEEDED(eFamilySinkPropModel)
    ENDIF
    
    
    //- mark peds as no longer needed -//
    //- mark vehicle as no longer needed -//
    
    //- mark objects as no longer needed -//
    SET_OBJECT_AS_NO_LONGER_NEEDED(script_chalk_board_prop)
    world_chalk_board_prop = NULL
    CLEAR_AREA_OF_OBJECTS(vFamily_scene_m_coord+vChalkBoardPropAttachOffset, 5)
    
    RELEASE_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
    
    //- mark models as no longer needed -//
    IF (eChalkBoardPropModel <> DUMMY_MODEL_FOR_SCRIPT)
        SET_MODEL_AS_NO_LONGER_NEEDED(eChalkBoardPropModel)
    ENDIF

    //- mark models as no longer needed -//
    //- remove anims from the memory -//
    REPEAT MAX_M_SCENE_MEMBERS iCleanup
        
        enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iCleanup))
        IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "cleanup (g_eCurrentFamilyEvent[", Get_String_From_FamilyMember(eFamilyMember), "] =  ", Get_String_From_FamilyEvent(NO_FAMILY_EVENTS), ")")
            #ENDIF
            
            Update_Previous_Event_For_FamilyMember(eFamilyMember)
            g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
        ENDIF
        
        SetFamilyMemberModelAsNoLongerNeeded(eFamilyMember)
    ENDREPEAT
    
    //- remove anims from the memory -//
    //- remove vehicle recordings from the memory -//
    //- clear sequences -//
    //- destroy all scripted cams -//
    //- enable the cellphone -//
    RELEASE_NPC_PHONE_RENDERTARGET()
    
    IF NOT IS_SCENARIO_TYPE_ENABLED("PROP_HUMAN_SEAT_CHAIR")
        SET_SCENARIO_TYPE_ENABLED("PROP_HUMAN_SEAT_CHAIR", TRUE)
    ENDIF
    
    //- reset wanted multiplier -//
    
    #IF IS_DEBUG_BUILD
    //- delete the mission widget -//
    IF DOES_WIDGET_GROUP_EXIST(chalk_board_widget)
        DELETE_WIDGET_GROUP(chalk_board_widget)
    ENDIF
    IF DOES_WIDGET_GROUP_EXIST(family_scene_m_subwidget)
        DELETE_WIDGET_GROUP(family_scene_m_subwidget)
    ENDIF
    IF DOES_WIDGET_GROUP_EXIST(family_scene_m_widget)
        DELETE_WIDGET_GROUP(family_scene_m_widget)
    ENDIF
    #ENDIF
    
    //- kill mission text link -//
    
    SET_GAME_PAUSED(FALSE)
    TERMINATE_THIS_THREAD()
ENDPROC

PROC Initialise_Michael_Family_Prop_Variables(PS_M_SCENE_MEMBERS eMember)
            
    eFamilyPropLModel[eMember]          = DUMMY_MODEL_FOR_SCRIPT
    eFamilyPropLAttachBonetag[eMember]  = BONETAG_NULL
    vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
    eFamilyPropRModel[eMember]          = DUMMY_MODEL_FOR_SCRIPT
    eFamilyPropRAttachBonetag[eMember]  = BONETAG_NULL
    vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
    vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
    
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eMember)
    
    SWITCH g_eCurrentFamilyEvent[eFamilyMember]
        CASE FE_M_FAMILY_on_laptops
        CASE FE_M7_SON_on_laptop_looking_for_jobs
            
            SWITCH eFamilyMember
                CASE FM_MICHAEL_SON
                    eFamilyPropLModel[eMember] = Prop_Laptop_Jimmy
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.789,-0.130,0.709>>
                    vFamilyPropLAttachRotation[eMember] = <<180.0,180.0,0.0>>
                BREAK
                CASE FM_MICHAEL_DAUGHTER
                    eFamilyPropLModel[eMember] = PROP_NPC_PHONE
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
                    vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
                BREAK
                CASE FM_MICHAEL_WIFE
                    eFamilyPropLModel[eMember] = P_NOVEL_01_S
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
                    vFamilyPropLAttachOffset[eMember] = <<0,0,-0.03>>
                    vFamilyPropLAttachRotation[eMember] = <<0,6,0>>
                    
                //  V_michael_book  //#1445485
                    
                BREAK
            ENDSWITCH
        BREAK
        CASE FE_M7_FAMILY_finished_breakfast
            
            SWITCH eFamilyMember
                CASE FM_MICHAEL_SON
                    eFamilyPropLModel[eMember] = Prop_Laptop_Jimmy
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.789,-0.130,0.709>>
                    vFamilyPropLAttachRotation[eMember] = <<180.0,180.0,0.0>>
                BREAK
                CASE FM_MICHAEL_DAUGHTER
                    eFamilyPropLModel[eMember] = V_Res_TT_Bowl
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.273,0.716,0.763>>
                    vFamilyPropLAttachRotation[eMember] = <<0.0,0.0,0.0>>
                BREAK
                CASE FM_MICHAEL_WIFE
                    eFamilyPropLModel[eMember] = V_Res_MCofCup
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<-0.195,-0.535,0.732>>
                    vFamilyPropLAttachRotation[eMember] = <<0.0,0.0,33.000>>
                BREAK
            ENDSWITCH
        BREAK
        CASE FE_M7_FAMILY_finished_pizza
            
            SWITCH eFamilyMember
                CASE FM_MICHAEL_SON
                    eFamilyPropLModel[eMember] = Prop_Laptop_Jimmy
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.789,-0.130,0.709>>
                    vFamilyPropLAttachRotation[eMember] = <<180.0,180.0,0.0>>
                BREAK
                CASE FM_MICHAEL_DAUGHTER
                    eFamilyPropLModel[eMember] = V_Res_TT_PizzaPlate
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.273,0.716,0.763>>
                    vFamilyPropLAttachRotation[eMember] = <<0.0,0.0,0.0>>
                BREAK
                CASE FM_MICHAEL_WIFE
                    eFamilyPropLModel[eMember] = PROP_PIZZA_BOX_01
                    eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
                    vFamilyPropLAttachOffset[eMember] = <<0.300,-0.463,0.732>>
                    vFamilyPropLAttachRotation[eMember] = <<0.0,0.0,156.000>>
                BREAK
            ENDSWITCH
        BREAK
        
        CASE FE_M_SON_smoking_weed_in_a_bong
            eFamilyPropRModel[eMember] = PROP_BONG_01
            eFamilyPropRAttachBonetag[eMember] = BONETAG_PH_R_HAND
            vFamilyPropRAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropLModel[eMember] = P_CS_LIGHTER_01
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M2_SON_gaming_loop
        CASE FE_M7_SON_gaming
//      CASE FE_M2_SON_gaming_exit
//      CASE FE_M7_SON_gaming_exit
            eFamilyPropLModel[eMember]          = PROP_CONTROLLER_01
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropRModel[eMember]          = PROP_HEADSET_01
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_L_HAND
            vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
        BREAK
        
        CASE FE_M_DAUGHTER_screaming_at_dad
            eFamilyExtraPedModel[eMember] = A_M_Y_HIPSTER_01
            vFamilyExtraPedCoordOffset[eMember] = <<0.55,0.4,-0.9>>
            fFamilyExtraPedHeadOffset[eMember] = 130.0
        BREAK
        CASE FE_M_DAUGHTER_on_phone_to_friends
        CASE FE_M_DAUGHTER_on_phone_LOCKED
        CASE FE_M7_DAUGHTER_studying_on_phone
            eFamilyPropLModel[eMember] = PROP_NPC_PHONE
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK   
        CASE FE_M_DAUGHTER_watching_TV_sober
            eFamilyPropLModel[eMember] = PROP_NPC_PHONE
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M7_DAUGHTER_studying_does_nails
            eFamilyPropLModel[eMember] = PROP_CS_NAIL_FILE
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M_DAUGHTER_Coming_home_drunk
            eFamilyPropRModel[eMember]          = PROP_CS_BEER_BOT_01
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_R_HAND
            vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
        BREAK

//      CASE FE_M_WIFE_getting_nails_done
//          eFamilyExtraPedModel[eMember] = S_F_M_Sweatshop_01
//          vFamilyExtraPedCoordOffset[eMember] = <<0.550.4-0.9>>
//          fFamilyExtraPedHeadOffset[eMember] = 130.0
//      BREAK
//      CASE FE_M_WIFE_getting_botox_done
//          eFamilyExtraPedModel[eMember] = S_F_M_Sweatshop_01
//          vFamilyExtraPedCoordOffset[eMember] = <<0.550.4-0.9>>
//          fFamilyExtraPedHeadOffset[eMember] = -130.0
//      BREAK
        CASE FE_M2_WIFE_doing_yoga
        CASE FE_M7_WIFE_doing_yoga
            eFamilyPropLModel[eMember] = PROP_YOGA_MAT_01
            eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember] = <<0,0,-1.022>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,90>>
        BREAK
        CASE FE_M2_WIFE_with_shopping_bags_enter
        CASE FE_M7_WIFE_with_shopping_bags_enter
            eFamilyPropLModel[eMember]          = PROP_CS_SHOPPING_BAG
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropRModel[eMember]          = PROP_CS_SHOPPING_BAG
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_R_HAND
            vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
        BREAK
//      CASE FE_M7_WIFE_shopping_with_son
        CASE FE_M7_WIFE_shopping_with_daughter
            
            SWITCH eFamilyMember
                CASE FM_MICHAEL_SON
                CASE FM_MICHAEL_DAUGHTER
                    eFamilyPropLModel[eMember]          = PROP_CS_SHOPPING_BAG
                    eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_L_HAND
                    vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
                    eFamilyPropRModel[eMember]          = PROP_CS_SHOPPING_BAG
                    eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_R_HAND
                    vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
                BREAK
                CASE FM_MICHAEL_WIFE
                    eFamilyPropLModel[eMember]          = PROP_CS_SHOPPING_BAG
                    eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_L_HAND
                    vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
                    eFamilyPropRModel[eMember]          = PROP_CS_SHOPPING_BAG
                    eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_R_HAND
                    vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
                BREAK
            ENDSWITCH
        BREAK
        CASE FE_M_WIFE_gets_drink_in_kitchen
            eFamilyPropLModel[eMember]          = P_TUMBLER_01_S
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropRModel[eMember]          = P_WHISKEY_BOTTLE_S    //P_WHISKEY_NOTOP
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_L_HAND
            vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M2_WIFE_phones_man_OR_therapist
        CASE FE_M7_WIFE_phones_man_OR_therapist
        CASE FE_M_WIFE_hangs_up_and_wanders
            eFamilyPropLModel[eMember]          = PROP_NPC_PHONE
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        
        CASE FE_M_WIFE_passed_out_BED
        CASE FE_M2_WIFE_passed_out_SOFA
        CASE FE_M7_WIFE_passed_out_SOFA
            eFamilyPropLModel[eMember]          = PROP_WINE_GLASS
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_L_HAND
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropRModel[eMember]          = PROP_WINE_BOT_01
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_NULL
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_WIFE_passed_out_BED)
                vFamilyPropRAttachOffset[eMember]   = <<-0.713,-0.222,-0.639>>-<<0,0,0.03>>
                vFamilyPropRAttachRotation[eMember] = <<0,-90.000,-117.599>>
            ELSE
                vFamilyPropRAttachOffset[eMember]   = <<-0.047,0.741,-0.456>>-<<0,0,0.03>>
                vFamilyPropRAttachRotation[eMember] = <<0,-90.000,-81.599>>
            ENDIF
        BREAK
        
        #IF NOT IS_JAPANESE_BUILD
        CASE FE_M2_WIFE_using_vibrator
//      CASE FE_M_WIFE_using_vibrator_END
        CASE FE_M7_WIFE_using_vibrator
            eFamilyPropLModel[eMember] = PROP_CS_DILDO_01
            eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        #ENDIF
        
        CASE FE_M7_WIFE_Making_juice
            eFamilyPropLModel[eMember] = P_W_GRASS_GLS_S
            eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        
        CASE FE_M2_MEXMAID_clean_surface_a  FALLTHRU
        CASE FE_M2_MEXMAID_clean_surface_b  FALLTHRU
        CASE FE_M2_MEXMAID_clean_surface_c  FALLTHRU
        CASE FE_M7_MEXMAID_clean_surface    FALLTHRU
        CASE FE_M_WIFE_screams_at_mexmaid   
            SWITCH eFamilyMember
                CASE FM_MICHAEL_MEXMAID
                    eFamilyPropLModel[eMember]              = PROP_SCOURER_01
                    eFamilyPropLAttachBonetag[eMember]      = BONETAG_PH_R_HAND
                    vFamilyPropLAttachOffset[eMember]       = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember]     = <<0,0,0>>
                    
                    IF (g_eCurrentFamilyEvent[eFamilyMember] <> FE_M2_MEXMAID_clean_surface_b)
                    AND (g_eCurrentFamilyEvent[eFamilyMember] <> FE_M_WIFE_screams_at_mexmaid)
                        eFamilyPropRModel[eMember]              = Prop_Blox_Spray
                        eFamilyPropRAttachBonetag[eMember]      = BONETAG_PH_L_HAND
                        vFamilyPropRAttachOffset[eMember]       = <<0,0,0>>
                        vFamilyPropRAttachRotation[eMember]     = <<0,0,0>>
                    ENDIF
                BREAK
            ENDSWITCH
        BREAK
        CASE FE_M2_MEXMAID_clean_window             FALLTHRU
        CASE FE_M7_MEXMAID_clean_window             FALLTHRU
//      CASE FE_M2_MEXMAID_cleans_booze_pot_other   FALLTHRU
//      CASE FE_M7_MEXMAID_cleans_booze_pot_other   FALLTHRU
        CASE FE_M_MEXMAID_MIC4_clean_window
            SWITCH eFamilyMember
                CASE FM_MICHAEL_MEXMAID
                    eFamilyPropLModel[eMember]          = PROP_SCOURER_01
                    eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_R_HAND
                    vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
                BREAK
            ENDSWITCH
        BREAK
        
        CASE FE_M_MEXMAID_does_the_dishes
            SWITCH eFamilyMember
                CASE FM_MICHAEL_MEXMAID
                    eFamilyPropLModel[eMember]          = V_RES_TT_PLATE01
                    eFamilyPropLAttachBonetag[eMember]  = BONETAG_PH_L_HAND
                    vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
                    
                    eFamilyPropRModel[eMember]          = PROP_SCOURER_01
                    eFamilyPropRAttachBonetag[eMember]  = BONETAG_PH_R_HAND
                    vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
                    vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
                    
                    eFamilySinkPropModel                = Prop_CS_Sink_Filler
                    eFamilySinkPropAttachBonetag        = BONETAG_NULL
                    vFamilySinkPropAttachOffset         = <<0.571,0.218,0>>
                    vFamilySinkPropAttachRotation       = <<0,0,-75>>
                BREAK
            ENDSWITCH
        BREAK
        
        CASE FE_M_MEXMAID_stealing_stuff
        CASE FE_M_MEXMAID_stealing_stuff_caught
            eFamilyPropLModel[eMember] = Prop_CS_fork   //Prop_Cleaver  //V_RET_GC_KNIFE01  #1241199
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        
//      CASE FE_M_GARDENER_with_leaf_blower //done in scenario
//          eFamilyPropLModel[eMember] = PROP_LEAF_BLOWER_01
//          eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
//          vFamilyPropLAttachOffset[eMember] = <<00.30.3>>
//          vFamilyPropLAttachRotation[eMember] = <<03090>>
//      BREAK
//      CASE FE_M_GARDENER_trimming_hedges
//          eFamilyPropLModel[eMember] = PROP_HEDGE_TRIMMER_01
//          eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
//          vFamilyPropLAttachOffset[eMember] = <<00.30.3>>
//          vFamilyPropLAttachRotation[eMember] = <<0-3090>>
//      BREAK
        CASE FE_M_GARDENER_cleaning_pool
            eFamilyPropLModel[eMember] = PROP_POOLSKIMMER
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M_GARDENER_mowing_lawn
            eFamilyPropLModel[eMember] = PROP_LAWNMOWER_01
            eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember] = << GET_RANDOM_INT_IN_RANGE(-1, 1), GET_RANDOM_INT_IN_RANGE(-1 ,1), GET_RANDOM_INT_IN_RANGE(-1 ,1)>>
            vFamilyPropLAttachRotation[eMember] = <<0 ,0, GET_RANDOM_INT_IN_RANGE(-180, 180)>>
        BREAK
        CASE FE_M_GARDENER_watering_flowers
            eFamilyPropLModel[eMember] = PROP_WATERINGCAN
            eFamilyPropLAttachBonetag[eMember] = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember] = <<-0.237,0.636,0.168>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
        CASE FE_M_GARDENER_smoking_weed
            eFamilyPropLModel[eMember] = P_AMB_JOINT_01
            eFamilyPropLAttachBonetag[eMember] = BONETAG_PH_R_HAND
            vFamilyPropLAttachOffset[eMember] = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
        BREAK
//      CASE FE_M_GARDENER_spraying_for_weeds
//          eFamilyPropLModel[eMember] = PROP_SPRAY_BACKPACK_01
//          eFamilyPropLAttachBonetag[eMember] = BONETAG_ROOT
//          vFamilyPropLAttachOffset[eMember] = <<0-0.10.3>>
//          vFamilyPropLAttachRotation[eMember] = <<00180>>
//      BREAK
        
        DEFAULT
            eFamilyExtraPedModel[eMember]       = DUMMY_MODEL_FOR_SCRIPT
            vFamilyExtraPedCoordOffset[eMember] = <<0,0,0>>
            fFamilyExtraPedHeadOffset[eMember]  = 0.0
            
            eFamilyPropLModel[eMember]          = DUMMY_MODEL_FOR_SCRIPT
            eFamilyPropLAttachBonetag[eMember]  = BONETAG_NULL
            vFamilyPropLAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropLAttachRotation[eMember] = <<0,0,0>>
            
            eFamilyPropRModel[eMember]          = DUMMY_MODEL_FOR_SCRIPT
            eFamilyPropRAttachBonetag[eMember]  = BONETAG_NULL
            vFamilyPropRAttachOffset[eMember]   = <<0,0,0>>
            vFamilyPropRAttachRotation[eMember] = <<0,0,0>>
        BREAK
    ENDSWITCH
    
    vChalkBoardPropAttachOffset     = << 15.9154,4.45155,2.06741 >>
    vChalkBoardPropAttachRotation   = <<0,0,-21.8314>>

ENDPROC

FUNC BOOL Initialise_Michael_Family_Vehicle_Variables()
    bAmbientVehicleOutside[PS_MV_SON_bike]          = FALSE
    
    PED_REQUEST_SCENE_ENUM ePedReq = Get_Player_Timetable_Scene_In_Progress()
    IF (ePedReq = PR_SCENE_M2_CARSLEEP_a)
    OR (ePedReq = PR_SCENE_M7_FAKEYOGA)
        bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
        bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
        
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ")   //", Get_String_From_Ped_Request_Scene_Enum(ePedReq))
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    
    SWITCH GetMichaelScheduleStage()
        CASE MSS_M4_WithoutFamily
        CASE MSS_M6_Exiled
        CASE MSS_M9_killedMichael
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ")   //stage without family/exile")
            #ENDIF
            
            RETURN FALSE
        BREAK
    ENDSWITCH
        
    IF g_eCurrentFamilyEvent[FM_MICHAEL_SON]        = FE_M_SON_Borrows_sisters_car
    OR g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER]   = FE_M_DAUGHTER_Going_out_in_her_car
    
//  OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]       = FE_M_WIFE_leaving_in_car_v2
//  OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]       = FE_M_WIFE_MD_leaving_in_car_v3
    OR g_eCurrentFamilyEvent[FM_MICHAEL_WIFE]       = FE_M_WIFE_leaving_in_car
        bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
        bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
        
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ")   //current event")
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    #IF IS_DEBUG_BUILD
    IF g_savedGlobals.sFlow.isGameflowActive
    #ENDIF
        IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_5)
        AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_6)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ")   //between fam5 and fam6")
            #ENDIF
            
            RETURN FALSE
        ENDIF
        
        //  Armenian 3 – Complications
        //  Franklin tries to repo Michael son’s car.
        //  Amanda Tracey Jimmy and Michael all at home.
        IF IS_MISSION_AVAILABLE(SP_MISSION_ARMENIAN_3)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = TRUE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = TRUE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //arm3 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Family 1 – Father and Son
        //  Lethal Weapon/Matrix fight on a yacht being transported on freeway.
        //  Tracey and Amanda
        IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam1 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Family 2 – Daddy’s Little Girl
        //  Michael takes son for bike ride has to go find daughter who is out on yacht
        //  Amanda and Jimmy
        IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_2)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam2 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Family 3 – Marriage Counselling
        //  Michael & Franklin pull down the house of Michael’s wife’s tennis coach.
        //  Amanda
        IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_3)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam3 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        IF (g_eLastMissionPassed = SP_MISSION_FAMILY_3)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam3 was last passed")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Lester 1
        //  Give a phone to an important person and use it to kill a man live on TV.
        //  Tracey
        IF IS_MISSION_AVAILABLE(SP_MISSION_LESTER_1)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = TRUE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //lester1 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Trevor 1 – Mr. Philips
        //  Trevor goes around Salton Sea collecting debts/selling scores
        //  Amanda (mission triggered at Michaels mansion by Franklin at end of Jewel heist)
        IF IS_MISSION_AVAILABLE(SP_MISSION_TREVOR_1)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE     //#1249819
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //trev1 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Family 4 – Fame or Shame
        //  Trevor and Michael go to save Tracey from embarrassing herself at a Fame or Shame audition. Chase Lazlow through the city in a lorry.
        //  Amanda Michael Jimmy
        IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_4)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam4 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Family 5 – Did somebody say yoga
        //  Michael gets involved with a drug deal ends up driving home drugged
        //  Amanda Jimmy
        IF IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_5)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = TRUE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam5 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  Michael 4 – Movie Premier
        //  When Tracey and Amanda don’t show up for the Movie Premeir Michael rushes home to find Merryweather have them held captive.
        //  Tracey Amanda
        IF IS_MISSION_AVAILABLE(SP_MISSION_MICHAEL_4)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = TRUE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = TRUE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //mic4 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        //  The Jewel Store Job Finale - End Cutscene (see Trevor 1)
        //  Finale cutscene to trigger trevor 1
        //  Amanda
        IF IS_MISSION_AVAILABLE(SP_HEIST_JEWELRY_2)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = TRUE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //jewel2 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        
        IF IS_MISSION_AVAILABLE(SP_MISSION_ME_TRACEY)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //fam3 is available")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        IF (g_eLastMissionPassed = SP_MISSION_ME_TRACEY)
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
            
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ") //me_tracey was last passed")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
    #IF IS_DEBUG_BUILD
    ENDIF
    #ENDIF
    
    INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 3)
    SWITCH iRandom
        CASE 0
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = TRUE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
        BREAK
        CASE 1
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = TRUE
        BREAK
        CASE 2
            bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]     = FALSE
            bAmbientVehicleOutside[PS_MV_WIFE_sentinal]     = FALSE
        BREAK
        
        DEFAULT
            CASSERTLN(DEBUG_FAMILY, "invalid iRandom")
        BREAK
    ENDSWITCH
    
    #IF IS_DEBUG_BUILD
    CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Vehicle_Variables(", bAmbientVehicleOutside[PS_MV_DAUGHTER_issi], ", ", bAmbientVehicleOutside[PS_MV_WIFE_sentinal], ")   //iRandom")
    #ENDIF
    
    RETURN (bAmbientVehicleOutside[PS_MV_DAUGHTER_issi] OR bAmbientVehicleOutside[PS_MV_WIFE_sentinal])
ENDFUNC

//PURPOSE:  Initializes all the variables used within the scope of the script
PROC Initialise_Family_Scene_M_Variables(structFamilyScene thisFamilyScene)
    
    //- enums -//
    //- vectors -//
    vFamily_scene_m_coord = thisFamilyScene.sceneCoord
    fFamily_scene_m_head = thisFamilyScene.sceneHead
    
    //- floats -//
    //- ints -//
    //-- structs : PS_M_STRUCT --//
    INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_SON)
    INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_DAUGHTER)
    INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_WIFE)
    INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_MEXMAID)
    IF NOT IS_MISSION_AVAILABLE(SP_MISSION_FAMILY_1)
        INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_MICHAEL_GARDENER)
    ENDIF
    INITIALISE_EVENT_FOR_THIS_FAMILY_MEMBER(FM_TREVOR_0_MICHAEL)
    
    Initialise_Michael_Family_Vehicle_Variables()
    
    PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_SON, g_eCurrentFamilyEvent[FM_MICHAEL_SON],
            vFamily_coordOffset[PS_M_SON], fFamily_headOffset[PS_M_SON])
    PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_DAUGHTER, g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER],
            vFamily_coordOffset[PS_M_DAUGHTER], fFamily_headOffset[PS_M_DAUGHTER])
    PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_WIFE, g_eCurrentFamilyEvent[FM_MICHAEL_WIFE],
            vFamily_coordOffset[PS_M_WIFE], fFamily_headOffset[PS_M_WIFE])
    PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_MEXMAID, g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID],
            vFamily_coordOffset[PS_M_MEXMAID], fFamily_headOffset[PS_M_MEXMAID])
    PRIVATE_Get_FamilyMember_Init_Offset(FM_MICHAEL_GARDENER, g_eCurrentFamilyEvent[FM_MICHAEL_GARDENER],
            vFamily_coordOffset[PS_M_GARDENER], fFamily_headOffset[PS_M_GARDENER])
//  PRIVATE_Get_FamilyMember_Init_Offset(FM_TREVOR_0_MICHAEL, g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL],
//          vFamily_coordOffset[PS_M_MICHAEL], fFamily_headOffset[PS_M_MICHAEL])
    
    
    
    eFamilySinkPropModel                = DUMMY_MODEL_FOR_SCRIPT
    eFamilySinkPropAttachBonetag        = BONETAG_NULL
    vFamilySinkPropAttachOffset         = <<0,0,0>>
    vFamilySinkPropAttachRotation       = <<0,0,0>>
    
    PS_M_SCENE_MEMBERS eMember
    REPEAT MAX_M_SCENE_MEMBERS eMember
        Initialise_Michael_Family_Prop_Variables(eMember)
        family_sfxID[eMember] = iCONST_FAMILY_SFX_INVALID
        iFamily_synch_scene[eMember] = -1
		family_sfxID[eMember] = -1
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
    VECTOR vFM_MICHAEL_SON
    Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_SON], vFM_MICHAEL_SON)
    Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_SON, fPlayerDebugJumpAngle[PS_M_SON], fPlayerDebugJumpDistance[PS_M_SON])
    
    VECTOR vFM_MICHAEL_DAUGHTER
    Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER],    vFM_MICHAEL_DAUGHTER)
    Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_DAUGHTER, fPlayerDebugJumpAngle[PS_M_DAUGHTER], fPlayerDebugJumpDistance[PS_M_DAUGHTER])
    
    VECTOR vFM_MICHAEL_WIFE
    Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_WIFE],    vFM_MICHAEL_WIFE)
    Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_WIFE, fPlayerDebugJumpAngle[PS_M_WIFE], fPlayerDebugJumpDistance[PS_M_WIFE])
    
    VECTOR vFM_MICHAEL_MEXMAID
    Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_MEXMAID], vFM_MICHAEL_MEXMAID)
    Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_MEXMAID, fPlayerDebugJumpAngle[PS_M_MEXMAID], fPlayerDebugJumpDistance[PS_M_MEXMAID])
    
    VECTOR vFM_MICHAEL_GARDENER
    Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_GARDENER],    vFM_MICHAEL_GARDENER)
    Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_GARDENER, fPlayerDebugJumpAngle[PS_M_GARDENER], fPlayerDebugJumpDistance[PS_M_GARDENER])

//  VECTOR vFM_MICHAEL_MICHAEL
//  Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL],    vFM_MICHAEL_MICHAEL)
//  Get_DebugJumpAngle_From_Vector(vFM_MICHAEL_MICHAEL, fPlayerDebugJumpAngle[PS_M_MICHAEL], fPlayerDebugJumpDistance[PS_M_MICHAEL])

    #ENDIF
    
    intention_index = NEW_CONTEXT_INTENTION
    
    PRIVATE_Cleanup_Family_Doors()
    
ENDPROC

// *******************************************************************************************
//  SETUP FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:  Load all the required models etc and init peds vehicles etc
PROC Setup_Family_Scene_M()
    INT iSetup
    
    //- request models - peds -//
    //- request models - vehicles -//
    //- request models - objects -//
    //- request models - weapons -//
    //- request anims and ptfx --//
    //- request vehicle recordings -//
    //- request interior models -//
    //- wait for assets to load -//
    //- create any script vehicles -//
    //- create any script peds -//
    //- create the peds for hot-swap -//
    //- set the players initial coords and heading -//
    //- create any script objects -//
    //- create any sequence tasks -//
    //- setup any scripted coverpoints -//
    //- add any initial taxi locations -//
    //- load scene(if required) -//
    
    REPEAT MAX_M_SCENE_MEMBERS iSetup
        iInteriorForMember[iSetup] = NULL    //INT_TO_NATIVE(INTERIOR_INSTANCE_INDEX -1)    //GET_INTERIOR_FROM_COLLISION(vFamily_scene_m_coord+vFamily_coordOffset[iSetup])    //GET_INTERIOR_AT_COORDS_WITH_TYPE(vFamily_scene_m_coord+vFamily_coordOffset[iSetup], "V_Michael") //GET_INTERIOR_FROM_fakeyfudge_obj(vSceneMemberCoord)
    ENDREPEAT
    
//  IF (eFamilyMember = FM_TREVOR_0_MICHAEL)
//      IF (g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL] = FE_M_MICHAEL_MIC2_washing_face)
//          SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_MICHAEL_BH, FALSE)
//      ENDIF
//  ENDIF
    
    
    //- load additional text and mission text link-//
    //- setup audio malarky for family -//
    ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0 ,PLAYER_PED_ID(), "MICHAEL")
//  
ENDPROC


PROC CreateMichaelFamilyVeh(PS_M_SCENE_MEMBERS eSceneMember, PS_M_SCENE_VEHICLES &eSceneVehicle)
//  enumCharacterList eVehPed
    
    IF IS_ENTITY_DEAD(PLAYER_PED_ID())
        EXIT
    ENDIF
    
    eSceneVehicle = Get_FamilyScene_Vehicle_From_Michael_Scene_Member(eSceneMember) //, eVehPed)
    IF (eSceneVehicle < MAX_SCENE_VEHICLES)
        enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
        
        BOOL bParkedWithoutEvent
        bParkedWithoutEvent = FALSE
        IF eSceneMember = PS_M_DAUGHTER
            IF bAmbientVehicleOutside[PS_MV_DAUGHTER_issi]
                bParkedWithoutEvent = TRUE
                
                IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
                    VECTOR Centre = GET_ENTITY_COORDS(family_veh[eSceneVehicle], FALSE)
                    FLOAT Radius = 30.0
                    MODEL_NAMES VehicleModelHashKey = GET_ENTITY_MODEL(family_veh[eSceneVehicle])
                    INT SearchFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES
                    
                    VEHICLE_INDEX dupeFamily_veh = GET_RANDOM_VEHICLE_FRONT_BUMPER_IN_SPHERE(Centre, Radius, VehicleModelHashKey, SearchFlags, family_veh[eSceneVehicle])
                    
                    IF dupeFamily_veh <> family_veh[eSceneVehicle]
                        IF NOT IS_ENTITY_DEAD(family_veh[eSceneVehicle])
                            IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_veh[eSceneVehicle])
                                IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), family_veh[eSceneVehicle])
                                    CPRINTLN(DEBUG_FAMILY, "delete PS_MV_DAUGHTER_issi for a dupe")
                                    
                                    DELETE_VEHICLE(family_veh[eSceneVehicle])
                                    bAmbientVehicleOutside[PS_MV_DAUGHTER_issi] = FALSE
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                
            ENDIF
        ELIF eSceneMember = PS_M_WIFE
            IF bAmbientVehicleOutside[PS_MV_WIFE_sentinal]
                bParkedWithoutEvent = TRUE
                
                IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
                    VECTOR Centre = GET_ENTITY_COORDS(family_veh[eSceneVehicle], FALSE)
                    FLOAT Radius = 30.0
                    MODEL_NAMES VehicleModelHashKey = GET_ENTITY_MODEL(family_veh[eSceneVehicle])
                    INT SearchFlags = VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES
                    
                    VEHICLE_INDEX dupeFamily_veh = GET_RANDOM_VEHICLE_FRONT_BUMPER_IN_SPHERE(Centre, Radius, VehicleModelHashKey, SearchFlags, family_veh[eSceneVehicle])
                    
                    IF dupeFamily_veh <> family_veh[eSceneVehicle]
                        IF NOT IS_ENTITY_DEAD(family_veh[eSceneVehicle])
                            IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_veh[eSceneVehicle])
                                IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), family_veh[eSceneVehicle])
                                    CPRINTLN(DEBUG_FAMILY, "delete PS_MV_WIFE_sentinal for a dupe")
                                    
                                    DELETE_VEHICLE(family_veh[eSceneVehicle])
                                    bAmbientVehicleOutside[PS_MV_WIFE_sentinal] = FALSE
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                
            ENDIF
        ENDIF
        
        CreateThisFamilyVehicle(family_veh[eSceneVehicle],
                eFamilyMember,
                vFamily_scene_m_coord, fFamily_scene_m_head, bParkedWithoutEvent
                
                #IF IS_DEBUG_BUILD
                , family_scene_m_widget, bMove_vehs
                #ENDIF
                
                )
    ENDIF
ENDPROC

PROC CreateMichaelFamilyProp(PS_M_SCENE_MEMBERS eSceneMember)
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
    
    CreateThisFamilyProp(family_prop_l[eSceneMember], family_ped[eSceneMember], eFamilyMember,
            g_eCurrentFamilyEvent[eFamilyMember],
            vFamily_scene_m_coord, fFamily_scene_m_head,
            eFamilyPropLModel[eSceneMember], eFamilyPropLAttachBonetag[eSceneMember],
            vFamilyPropLAttachOffset[eSceneMember], vFamilyPropLAttachRotation[eSceneMember]
            #IF IS_DEBUG_BUILD
            , family_scene_m_widget, bMove_prop
            #ENDIF
            )
            
    CreateThisFamilyProp(family_prop_r[eSceneMember], family_ped[eSceneMember], eFamilyMember,
            g_eCurrentFamilyEvent[eFamilyMember],
            vFamily_scene_m_coord, fFamily_scene_m_head,
            eFamilyPropRModel[eSceneMember], eFamilyPropRAttachBonetag[eSceneMember],
            vFamilyPropRAttachOffset[eSceneMember], vFamilyPropRAttachRotation[eSceneMember]
            #IF IS_DEBUG_BUILD
            , family_scene_m_widget, bMove_prop
            #ENDIF
            )
            
    IF (eSceneMember = PS_M_MEXMAID)
        CreateThisFamilyProp(family_sink_prop, family_ped[eSceneMember],
                eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember],
                vFamily_scene_m_coord, fFamily_scene_m_head,
                eFamilySinkPropModel, eFamilySinkPropAttachBonetag,
                vFamilySinkPropAttachOffset, vFamilySinkPropAttachRotation
                #IF IS_DEBUG_BUILD
                , family_scene_m_widget, bMove_prop
                #ENDIF
                )
    ENDIF
ENDPROC

PROC CreateMichaelFamilyExtraPed(PS_M_SCENE_MEMBERS eSceneMember)
    
    #IF IS_DEBUG_BUILD
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
    #ENDIF
    
    CreateThisFamilyExtraPed(family_extra_ped[eSceneMember], family_ped[eSceneMember], eFamilyExtraPedModel[eSceneMember],
            vFamilyExtraPedCoordOffset[eSceneMember], fFamilyExtraPedHeadOffset[eSceneMember]
            
            #IF IS_DEBUG_BUILD
            , family_scene_m_widget, bMove_peds,
            eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember]
            #ENDIF
            
            )
            
ENDPROC
FUNC BOOL Is_Michael_Scene_Member_Active(PS_M_SCENE_MEMBERS eSceneMember)
    
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
    

    IF (bAmbientVehicleOutside[PS_MV_DAUGHTER_issi] OR bAmbientVehicleOutside[PS_MV_WIFE_sentinal])
//      enumCharacterList eVehPed
        PS_M_SCENE_VEHICLES eSceneVehicle = Get_FamilyScene_Vehicle_From_Michael_Scene_Member(eSceneMember) //, eVehPed)
        
        CreateMichaelFamilyVeh(eSceneMember, eSceneVehicle)
    ENDIF
    
    IF (g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS)
    OR (g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY)
    
    OR (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_WIFE_playing_tennis)
        
        IF GetFamilyMemberFromScene(family_ped[eSceneMember], eFamilyMember,
                MyLocalPedStruct, ENUM_TO_INT(eSceneMember)+1, RELGROUPHASH_FAMILY_M)
            
            PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember],
                    vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember])
            
            RETURN FALSE
        ENDIF
        
//      enumCharacterList eVehPed
        PS_M_SCENE_VEHICLES eSceneVehicle = Get_FamilyScene_Vehicle_From_Michael_Scene_Member(eSceneMember) //, eVehPed)
        IF (bAmbientVehicleOutside[PS_MV_DAUGHTER_issi] OR bAmbientVehicleOutside[PS_MV_WIFE_sentinal])
            //CreateMichaelFamilyVeh(eSceneMember, eSceneVehicle)
        ELSE
            IF (eSceneVehicle < MAX_SCENE_VEHICLES)
                
                BOOL bIsThisCarInUse = FALSE
                IF eSceneVehicle = PS_MV_DAUGHTER_issi
                    IF eFamilyMember = FM_MICHAEL_SON
                        IF (g_eCurrentFamilyEvent[FM_MICHAEL_DAUGHTER] = FE_M_DAUGHTER_Going_out_in_her_car)
                            bIsThisCarInUse = TRUE
                        ENDIF
                    ENDIF
                    IF eFamilyMember = FM_MICHAEL_DAUGHTER
                        IF (g_eCurrentFamilyEvent[FM_MICHAEL_SON] = FE_M_SON_Borrows_sisters_car)
                            bIsThisCarInUse = TRUE
                        ENDIF
                    ENDIF
                ENDIF
                
                IF NOT bIsThisCarInUse
                    IF DOES_ENTITY_EXIST(family_veh[eSceneVehicle])
                        IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), family_veh[eSceneVehicle])
                            IF NOT IS_ENTITY_DEAD(family_veh[eSceneMember])
                            AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_veh[eSceneMember])
                                CPRINTLN(DEBUG_FAMILY, "DELETE_VEHICLE(family_veh[", ENUM_TO_INT(eFamilyMember), "])")
                                
                                IF ShouldDeleteFamilyEntity(family_veh[eSceneMember], "family_veh", ENUM_TO_INT(eSceneMember))
                                    DELETE_VEHICLE(family_veh[eSceneMember])
                                ELSE
                                    SET_VEHICLE_AS_NO_LONGER_NEEDED(family_veh[eSceneMember])
                                ENDIF
                                
                                bAmbientVehicleOutside[eSceneVehicle] = FALSE
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
        IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
            IF IS_ENTITY_ATTACHED(family_prop_l[eSceneMember])
                DETACH_ENTITY(family_prop_l[eSceneMember])
            ENDIF
            IF NOT IS_ENTITY_DEAD(family_prop_l[eSceneMember])
            AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_prop_l[eSceneMember])
            AND eFamilyPropLModel[eSceneMember] != PROP_BONG_01
                DELETE_OBJECT(family_prop_l[eSceneMember])
            ENDIF
        ENDIF
        IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
            IF IS_ENTITY_ATTACHED(family_prop_r[eSceneMember])
                DETACH_ENTITY(family_prop_r[eSceneMember])
            ENDIF
            IF NOT IS_ENTITY_DEAD(family_prop_r[eSceneMember])
            AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_prop_r[eSceneMember])
            AND eFamilyPropRModel[eSceneMember] != PROP_BONG_01
                DELETE_OBJECT(family_prop_r[eSceneMember])
            ENDIF
        ENDIF
        IF DOES_ENTITY_EXIST(family_extra_ped[eSceneMember])
            IF NOT IS_ENTITY_DEAD(family_extra_ped[eSceneMember])
            AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_extra_ped[eSceneMember])
                DELETE_PED(family_extra_ped[eSceneMember])
            ENDIF
        ENDIF
        
        IF IS_PED_REGISTERED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, family_ped[eSceneMember])
            UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MICHAEL_MANSION_GATE, family_ped[eSceneMember])
            
            IF IS_BIT_SET(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].settingsBitset, BIT_AUTODOOR_LOCK_OPEN)
                FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_MICHAEL_MANSION_GATE, FALSE)
            ENDIF
            
            IF NOT IS_BIT_SET(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
                SET_BIT(g_sAutoDoorData[AUTODOOR_MICHAEL_MANSION_GATE].settingsBitset, BIT_AUTODOOR_OPEN_ON_WANTED)
            ENDIF
        ENDIF
        
        IF DOES_ENTITY_EXIST(family_ped[eSceneMember])
            IF NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
            AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_ped[eSceneMember])
                IF ShouldDeleteFamilyEntity(family_ped[eSceneMember], "family_ped", ENUM_TO_INT(eSceneMember))
                    DELETE_PED(family_ped[eSceneMember])
                ELSE
                    SET_PED_KEEP_TASK(family_ped[eSceneMember], TRUE)
                    SET_PED_AS_NO_LONGER_NEEDED(family_ped[eSceneMember])
                ENDIF
            ENDIF
            
            PRIVATE_Cleanup_Family_Stream_And_Sfx(family_sfxStage[eSceneMember], family_sfxID[eSceneMember], family_sfxBank[eSceneMember])
        ENDIF
        
        RETURN FALSE
    ELSE
        IF Is_Scene_Member_Active(family_ped[eSceneMember], eFamilyMember,
                vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember],
                WRAP(fFamily_scene_m_head+fFamily_headOffset[eSceneMember], 0 ,360),
                iInteriorForPlayer, iInteriorForMember[eSceneMember],
                MyLocalPedStruct, ENUM_TO_INT(eSceneMember)+1,
//              sFireTimer,
                RELGROUPHASH_FAMILY_M)
            
            PS_M_SCENE_VEHICLES eSceneVehicle
            CreateMichaelFamilyVeh(eSceneMember, eSceneVehicle)
            CreateMichaelFamilyProp(eSceneMember)
            CreateMichaelFamilyExtraPed(eSceneMember)
            
            IF NOT IS_PED_INJURED(family_ped[eSceneMember])
                RETURN  TRUE
            ENDIF
        ELSE
            IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                IF iFamily_synch_scene[eSceneMember] != -1
                AND IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
                    STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                ENDIF
                IF IS_ENTITY_ATTACHED(family_prop_l[eSceneMember])
                    DETACH_ENTITY(family_prop_l[eSceneMember])
                ENDIF
            ENDIF
            IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                IF iFamily_synch_scene[eSceneMember] != -1
                AND IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
                    STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                ENDIF
                IF IS_ENTITY_ATTACHED(family_prop_r[eSceneMember])
                    DETACH_ENTITY(family_prop_r[eSceneMember])
                ENDIF
            ENDIF
            
            UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
                    iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_m_coord, "v_michael")
            bUpdateInteriorForPlayer = TRUE
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL Setup_Chalk_Board_Prop()
    enumMichaelScheduleStage thisMichaelScheduleStage = GetMichaelScheduleStage()
    
    #IF IS_DEBUG_BUILD
    IF bToggle_Family_Scene_M_Widget
        IF bMove_peds
            thisMichaelScheduleStage = INT_TO_ENUM(enumMichaelScheduleStage, iMichaelScheduleStage)
        ENDIF
    ENDIF
    #ENDIF
    
    SWITCH thisMichaelScheduleStage
        CASE MSS_M2_WithFamily
            IF NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FAMILY_3)
                eChalkBoardPropModel = V_ILev_MChalkBrd_1   //Up to Fam 3 should read Tennis Lesson.  Dance Class.  
            ELSE
                eChalkBoardPropModel = V_ILev_MChalkBrd_5   //#1573144: daddy drink count (with numbers scored off) Whisky – 10   Beer – 8  Vodka – 3
            ENDIF
        BREAK
        CASE MSS_M4_WithoutFamily
            eChalkBoardPropModel = V_ILev_MChalkBrd_2       //Fam 3 to Fam 5 should say Yoga. Meditation. Centeredness. 
        BREAK
        CASE MSS_M6_Exiled
            eChalkBoardPropModel = V_ILev_MChalkBrd_3       //Fam 5 to  Fam 6 should just be a smear or a kind of depressed person squiggle.
        BREAK
        CASE MSS_M7_ReunitedWithFamily
            eChalkBoardPropModel = V_ILev_MChalkBrd_4       //after Fam 6 onwards maybe College Prep Fat Camp.
        BREAK
        
        DEFAULT
            CASSERTLN(DEBUG_FAMILY, "invalid thisMichaelScheduleStage in Setup_Chalk_Board_Prop()")
            eChalkBoardPropModel = DUMMY_MODEL_FOR_SCRIPT
            RETURN FALSE
        BREAK
    ENDSWITCH
    
    
    FLOAT       fPlayerDistFromChalkBoard2      = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vFamily_scene_m_coord+vChalkBoardPropAttachOffset)
    
    FLOAT       fDISTANCE_TO_CREATE_CHALK_BOARD     = (fCONST_DISTANCE_TO_START_FAMILY_SCENE_SCRIPT*1.00)
    FLOAT       fDISTANCE_TO_DELETE_CHALK_BOARD     = (fDISTANCE_TO_CREATE_CHALK_BOARD*1.25)
    
    IF NOT DOES_ENTITY_EXIST(world_chalk_board_prop)
        IF fPlayerDistFromChalkBoard2 > (fDISTANCE_TO_CREATE_CHALK_BOARD*fDISTANCE_TO_CREATE_CHALK_BOARD)
            RETURN FALSE
        ENDIF
        
        world_chalk_board_prop = GET_CLOSEST_OBJECT_OF_TYPE(
                vFamily_scene_m_coord+vChalkBoardPropAttachOffset,
                10.0 ,V_ILev_MChalkBrd_1, TRUE)
        
        IF NOT DOES_ENTITY_EXIST(world_chalk_board_prop)
            RETURN FALSE
        ENDIF
        
        #IF IS_DEBUG_BUILD
        VECTOR vWorldChalkBoardOff, vWorldChalkBoardRot
        vWorldChalkBoardOff = GET_ENTITY_COORDS(world_chalk_board_prop) - vFamily_scene_m_coord
        vWorldChalkBoardRot = GET_ENTITY_ROTATION(world_chalk_board_prop) - <<0,0,fFamily_scene_m_head>>
        
        IF (vWorldChalkBoardRot.z < -360.0) vWorldChalkBoardRot.z += 360.0 ENDIF
        
        IF NOT ARE_VECTORS_ALMOST_EQUAL(vWorldChalkBoardOff, vChalkBoardPropAttachOffset, 0.01)
            SAVE_STRING_TO_DEBUG_FILE("vChalkBoardPropAttachOffset = ")
            SAVE_VECTOR_TO_DEBUG_FILE(vWorldChalkBoardOff)
            SAVE_NEWLINE_TO_DEBUG_FILE()
            
            CPRINTLN(DEBUG_FAMILY, "    vWorldChalkBoardOff[", vWorldChalkBoardOff, "] <> vChalkBoardPropAttachOffset[", vChalkBoardPropAttachOffset, "]: ", VDIST(vWorldChalkBoardOff, vChalkBoardPropAttachOffset))
            
            CASSERTLN(DEBUG_FAMILY, "Setup_Chalk_Board_Prop - update vWorldChalkBoardOff")
        ENDIF
        IF NOT ARE_VECTORS_ALMOST_EQUAL(vWorldChalkBoardRot, vChalkBoardPropAttachRotation, 0.001)
            SAVE_STRING_TO_DEBUG_FILE("vChalkBoardPropAttachRotation = ")
            SAVE_VECTOR_TO_DEBUG_FILE(vWorldChalkBoardRot)
            SAVE_NEWLINE_TO_DEBUG_FILE()
            
            CPRINTLN(DEBUG_FAMILY, "    vWorldChalkBoardRot[", vWorldChalkBoardRot, "] <> vChalkBoardPropAttachRotation[", vChalkBoardPropAttachRotation, "]: ", VDIST(vWorldChalkBoardRot, vChalkBoardPropAttachRotation))
            
            CASSERTLN(DEBUG_FAMILY, "Setup_Chalk_Board_Prop - update vWorldChalkBoardRot")
        ENDIF
        
        #ENDIF
        
        SET_ENTITY_VISIBLE(world_chalk_board_prop, FALSE)
    ENDIF
    
    IF NOT DOES_ENTITY_EXIST(script_chalk_board_prop)
        IF fPlayerDistFromChalkBoard2 > (fDISTANCE_TO_CREATE_CHALK_BOARD*fDISTANCE_TO_CREATE_CHALK_BOARD)
            SET_MODEL_AS_NO_LONGER_NEEDED(eChalkBoardPropModel)
            RETURN FALSE
        ENDIF
        
        REQUEST_MODEL(eChalkBoardPropModel)
        IF NOT HAS_MODEL_LOADED(eChalkBoardPropModel)
            RETURN FALSE
        ENDIF
        
        script_chalk_board_prop = CREATE_OBJECT_NO_OFFSET(eChalkBoardPropModel, vFamily_scene_m_coord+vChalkBoardPropAttachOffset)
        SET_ENTITY_ROTATION(script_chalk_board_prop, <<0,0,fFamily_scene_m_head>>+vChalkBoardPropAttachRotation)
        
        FREEZE_ENTITY_POSITION(script_chalk_board_prop, TRUE)
        
        SET_MODEL_AS_NO_LONGER_NEEDED(eChalkBoardPropModel)
        RETURN TRUE
    ELSE
        IF (GET_ENTITY_MODEL(script_chalk_board_prop) <> eChalkBoardPropModel)
            DELETE_OBJECT(script_chalk_board_prop)
            SET_MODEL_AS_NO_LONGER_NEEDED(eChalkBoardPropModel)
            RETURN FALSE
        ENDIF
        
        IF fPlayerDistFromChalkBoard2 > (fDISTANCE_TO_DELETE_CHALK_BOARD*fDISTANCE_TO_DELETE_CHALK_BOARD)
            DELETE_OBJECT(script_chalk_board_prop)
            SET_MODEL_AS_NO_LONGER_NEEDED(eChalkBoardPropModel)
            RETURN FALSE
        ENDIF
        
        RETURN TRUE
    ENDIF
ENDFUNC

#IF IS_DEBUG_BUILD
// *******************************************************************************************
//  DEBUGGING FUNCTIONS AND PROCEDURES
// *******************************************************************************************
//PURPOSE:  Initialises the family widget
PROC Create_Family_Scene_M_Widget()
    family_scene_m_widget = START_WIDGET_GROUP("Family Scene M")
        
        ADD_WIDGET_BOOL("bToggle_Family_Scene_M_Widget", bToggle_Family_Scene_M_Widget)
        
        
//START_WIDGET_GROUP("son orientation")
//  ADD_WIDGET_FLOAT_SLIDER("pitch", M7_FAMILY_watching_TVpitch_son, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_FLOAT_SLIDER("heading", M7_FAMILY_watching_TVheading_son, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_VECTOR_SLIDER("vecOffset", M7_FAMILY_watching_TVvecOffset_son, -10, 10, 0.01)
//STOP_WIDGET_GROUP()
//START_WIDGET_GROUP("dau orientation")
//  ADD_WIDGET_FLOAT_SLIDER("pitch", M7_FAMILY_watching_TVpitch_dau, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_FLOAT_SLIDER("heading", M7_FAMILY_watching_TVheading_dau, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_VECTOR_SLIDER("vecOffset", M7_FAMILY_watching_TVvecOffset_dau, -10, 10, 0.01)
//STOP_WIDGET_GROUP()
//START_WIDGET_GROUP("wife orientation")
//  ADD_WIDGET_FLOAT_SLIDER("pitch", M7_FAMILY_watching_TVpitch_wife, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_FLOAT_SLIDER("heading", M7_FAMILY_watching_TVheading_wife, -CONST_PI, CONST_PI, 0.01)
//  ADD_WIDGET_VECTOR_SLIDER("vecOffset", M7_FAMILY_watching_TVvecOffset_wife, -10, 10, 0.01)
//STOP_WIDGET_GROUP()
        
    STOP_WIDGET_GROUP()
ENDPROC

//PURPOSE:  Initialises the mission widget
PROC Create_family_scene_m_subwidget()
    STRING sFamilyMemberName[MAX_M_SCENE_MEMBERS]
    PS_M_SCENE_MEMBERS eSceneMember
    REPEAT MAX_M_SCENE_MEMBERS eSceneMember
        sFamilyMemberName[eSceneMember] = Get_String_From_Family_Scene_M_Scene_Members(eSceneMember)
    ENDREPEAT
    
    family_scene_m_subwidget = Create_Family_Scene_Widget_Group(MAX_M_SCENE_MEMBERS,
            sFamilyMemberName,
//          iCurrentFamilyEvent,
            family_anim_widget, family_animFlag_widget, //sFamilyAnimDict sFamilyAnim
            vFamily_coordOffset, fFamily_headOffset,
            bJumpToFamilyMember  ,
            fPlayerDebugJumpAngle, fPlayerDebugJumpDistance, fFamily_synch_scenePhase,
            bMove_peds, bDraw_peds, bSave_peds, bEdit_speech_bounds)
    
    chalk_board_widget = START_WIDGET_GROUP("chalk_board_widget")
        ADD_WIDGET_INT_SLIDER("iMichaelScheduleStage", iMichaelScheduleStage, 0 ,100, 1)
        
        chalk_board_textWidget = ADD_TEXT_WIDGET("eChalkBoardPropModel")
        
        ADD_WIDGET_FLOAT_SLIDER("vChalkBoardPropAttachOffset x",
                vChalkBoardPropAttachOffset.x,
                vChalkBoardPropAttachOffset.x-10.0, vChalkBoardPropAttachOffset.x+10.0,
                0.01)
        ADD_WIDGET_FLOAT_SLIDER("vChalkBoardPropAttachOffset y",
                vChalkBoardPropAttachOffset.y,
                vChalkBoardPropAttachOffset.y-10.0, vChalkBoardPropAttachOffset.y+10.0,
                0.01)
        ADD_WIDGET_FLOAT_SLIDER("vChalkBoardPropAttachOffset z",
                vChalkBoardPropAttachOffset.z,
                vChalkBoardPropAttachOffset.z-10.0, vChalkBoardPropAttachOffset.z+10.0,
                0.01)
        
        ADD_WIDGET_VECTOR_SLIDER("vChalkBoardPropAttachRotation", vChalkBoardPropAttachRotation, -180.0 ,180.0 ,1.0)
        ADD_WIDGET_BOOL("bMove_peds", bMove_peds)
    STOP_WIDGET_GROUP()
    
ENDPROC

PROC Update_Michael_Family_Member_Widget(PS_M_SCENE_MEMBERS eSceneMember, enumFamilyMember eFamilyMember)
    SET_OBJECT_AS_NO_LONGER_NEEDED(family_door[eSceneMember])
    PRIVATE_Cleanup_Family_Chairs(family_ped, family_chair)
    
    Update_Family_Member_Widget(eFamilyMember,
            //sFamilyAnimDict[eSceneMember] sFamilyAnim[eSceneMember] ,
            family_anim_widget[eSceneMember],family_animFlag_widget[eSceneMember],
            vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember],
            fPlayerDebugJumpAngle[eSceneMember] ,fPlayerDebugJumpDistance[eSceneMember],
            family_ptfx[eSceneMember], family_sfxStage[eSceneMember], family_sfxID[eSceneMember], family_sfxBank[eSceneMember],
            sSpeechTimer, iRandSpeechCount[eSceneMember], family_prop_l[eSceneMember])
    IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
        DELETE_OBJECT(family_prop_r[eSceneMember])
    ENDIF
    SET_OBJECT_AS_NO_LONGER_NEEDED(family_prop_r[eSceneMember])


    IF (eSceneMember = PS_M_MEXMAID)
        eFamilySinkPropModel                = DUMMY_MODEL_FOR_SCRIPT
        eFamilySinkPropAttachBonetag        = BONETAG_NULL
        vFamilySinkPropAttachOffset         = <<0,0,0>>
        vFamilySinkPropAttachRotation       = <<0,0,0>>
    ENDIF
    
    Initialise_Michael_Family_Prop_Variables(eSceneMember)
    SetFamilyMemberComponentVariation(family_ped[eSceneMember], eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember])
    SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_M, FALSE)
    
    IF (family_other_ptfx[0] <> NULL)
        REMOVE_PARTICLE_FX(family_other_ptfx[0])
        family_other_ptfx[0] = NULL
    ENDIF
    IF (family_other_ptfx[1] <> NULL)
        REMOVE_PARTICLE_FX(family_other_ptfx[1])
        family_other_ptfx[1] = NULL
    ENDIF
    IF intention_index != NEW_CONTEXT_INTENTION
        RELEASE_CONTEXT_INTENTION(intention_index)
    ENDIF
    
	IF family_sfxID[eSceneMember] != -1
	ENDIF
	family_sfxID[eSceneMember] = -1
	
    tDialogueAnimDicts[eSceneMember] = ""
    tDialogueAnimClips[eSceneMember] = ""
    
    INT iConv
    REPEAT COUNT_OF(tCreatedMichaelConvLabels) iConv
        tCreatedMichaelConvLabels[iConv] = ""
    ENDREPEAT
    
    iFamily_enter_veh_stage[eSceneMember] = 0
    
    PS_M_SCENE_MEMBERS eMember
    REPEAT MAX_M_SCENE_MEMBERS eMember
        iFamily_enter_veh_stage[eMember] = 0
        
        IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iFamily_navmeshBlock[eSceneMember])
            REMOVE_NAVMESH_BLOCKING_OBJECT(iFamily_navmeshBlock[eSceneMember])
        ENDIF
        
    ENDREPEAT
    
    IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
        DETACH_SYNCHRONIZED_SCENE(iFamily_synch_scene[eSceneMember])
        iFamily_synch_scene[eSceneMember] = -1
    ENDIF
	
ENDPROC

//PURPOSE:  Moniters for any changes in the mission widget
PROC Watch_Family_Scene_M_Widget()
    
    
    IF NOT bToggle_Family_Scene_M_Widget
        IF DOES_WIDGET_GROUP_EXIST(family_scene_m_subwidget)
            DELETE_WIDGET_GROUP(family_scene_m_subwidget)
        ENDIF
        
        IF g_bUpdatedFamilyEvents
//      AND g_iDebugSelectedFriendConnDisplay > 0
            bToggle_Family_Scene_M_Widget = TRUE
        ENDIF
    ELSE
        IF NOT DOES_WIDGET_GROUP_EXIST(family_scene_m_subwidget)
            
//          enumFamilyMember eFamMem
//          TEXT_LABEL_63 str
            
            SET_CURRENT_WIDGET_GROUP(family_scene_m_widget)
                Create_Family_Scene_M_Subwidget()
            CLEAR_CURRENT_WIDGET_GROUP(family_scene_m_widget)
        ELSE
            
            IF (eChalkBoardPropModel <> DUMMY_MODEL_FOR_SCRIPT)
                SET_CONTENTS_OF_TEXT_WIDGET(chalk_board_textWidget, GET_MODEL_NAME_FOR_DEBUG(eChalkBoardPropModel))
            ELSE
                SET_CONTENTS_OF_TEXT_WIDGET(chalk_board_textWidget, "DUMMY_MODEL_FOR_SCRIPT")
            ENDIF
            
            INT iPed
            IF bMove_peds
            
                REPEAT MAX_M_SCENE_MEMBERS iPed
                    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed))
                    
                    IF NOT IS_ENTITY_DEAD(family_ped[iPed])
                        IF IS_PED_IN_ANY_VEHICLE(family_ped[iPed])
                            VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[iPed])
                            
                            IF NOT IS_ENTITY_DEAD(familyVeh)
                                SET_ENTITY_COORDS(familyVeh, vFamily_scene_m_coord+vFamily_coordOffset[iPed])
                                SET_ENTITY_HEADING(familyVeh, fFamily_scene_m_head+fFamily_headOffset[iPed])
                            ENDIF
                        ELSE
                            VECTOR vFamMemCoord = vFamily_scene_m_coord+vFamily_coordOffset[iPed]
                            VECTOR vFamMemCoordGround = vFamMemCoord
                            IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[iPed])
                                IF (GET_SCRIPT_TASK_STATUS(family_ped[iPed], SCRIPT_TASK_SYNCHRONIZED_SCENE) <> PERFORMING_TASK)
                                
                                    SET_ENTITY_COORDS(family_ped[iPed], vFamily_scene_m_coord+vFamily_coordOffset[iPed])
                                    SET_ENTITY_HEADING(family_ped[iPed], fFamily_scene_m_head+fFamily_headOffset[iPed])
                                ENDIF
                            ELSE
                                SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[iPed], vFamily_scene_m_coord+vFamily_coordOffset[iPed], <<0,0,fFamily_scene_m_head+fFamily_headOffset[iPed]>>)
                                SET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[iPed], fFamily_synch_scenePhase[iPed])
                                
                                IF NOT IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed])
                                    SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[iPed], TRUE)
                                ENDIF
                                
                                vFamMemCoordGround += <<0,0,-1>>
                            ENDIF
                            
                            DRAW_DEBUG_BOX(vFamMemCoordGround - <<0.1, 0.1, 0.1>>,
                                    vFamMemCoordGround + <<0.1, 0.1, 0.1>>)
                            
                            FLOAT fPlayerCoord_groundZ = 0.0
                            FLOAT  fAnimPos_heightDiff
                            
                            TEXT_LABEL str = "height: "
                            
                            IF GET_GROUND_Z_FOR_3D_COORD(vFamMemCoord ,fPlayerCoord_groundZ)
                                fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
                                
                                str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
                                DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0 ,HUD_COLOUR_BLUE)
                            ELSE
                                fAnimPos_heightDiff = (vFamMemCoordGround.z - 1) - fPlayerCoord_groundZ
                                
                                str += GET_STRING_FROM_FLOAT(fAnimPos_heightDiff)
                                DrawDebugFamilyTextWithOffset(str, vFamMemCoordGround, 0 ,HUD_COLOUR_RED)
                            ENDIF
                            
                            
                            //iFamily_enter_veh_stage[iPed] = 0
                        ENDIF
                    ENDIF
                    
                    IF bJumpToFamilyMember[iPed]
                        IF IS_PLAYER_PLAYING(PLAYER_ID())
                            IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
                                SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
                                CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
                                SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
                            ENDIF
                        ENDIF
                        
                        IF g_bWarpPlayerOnFamilyDebug
                            VECTOR vDebug_jumpOffset = <<0,0,0>>
                            Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
                        
                            VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
                            VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
                            
                            VECTOR vFamily_scene_m_coord_vDebug_coordOffset = vFamily_scene_m_coord+vDebug_coordOffset
                            
                            
                            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M7_SON_coming_back_from_a_bike_ride)
                                VECTOR vDebugJumpOffset = <<0,0,0>>
                                Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember], vDebugJumpOffset)
                            
                                vFamily_scene_m_coord_vDebug_coordOffset.z += vDebugJumpOffset.z
                            ENDIF
                            
                            FLOAT fFamily_scene_m_coord_vDebug_coordOffset = 0.0
                            IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_m_coord_vDebug_coordOffset, fFamily_scene_m_coord_vDebug_coordOffset)
                                vFamily_scene_m_coord_vDebug_coordOffset.z = fFamily_scene_m_coord_vDebug_coordOffset
                            ENDIF
                            
                            IF IS_PLAYER_PLAYING(PLAYER_ID())
                                SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_m_coord_vDebug_coordOffset)
                                SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(-vPlayerHeading.x, -vPlayerHeading.y))
                            ENDIF
                            
                            SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
                        ENDIF
                    ENDIF
                    
                    IF (g_eCurrentFamilyEvent[eFamilyMember] <> INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
                        Update_Previous_Event_For_FamilyMember(eFamilyMember)
                        
                        PRIVATE_Set_Current_Family_Member_Event(eFamilyMember, INT_TO_ENUM(enumFamilyEvents, iCurrentFamilyEvent[iPed]))
                        Update_Michael_Family_Member_Widget(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed), eFamilyMember)
                        
                        PS_M_SCENE_MEMBERS eSceneMemberUpdate
                        REPEAT MAX_M_SCENE_MEMBERS eSceneMemberUpdate
                            enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, eSceneMemberUpdate))
                            
                            IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
                                Update_Previous_Event_For_FamilyMember(eFamilyMember)
                                
                                iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
                                Update_Michael_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
                                
                            ENDIF
                        ENDREPEAT
                    ENDIF
                ENDREPEAT
                
//              IF (Get_Mission_Flow_Flag_State(GetMichaelScheduleStage()) <> bGetMichaelScheduleStage())
//                  Set_Mission_Flow_Flag_State(GetMichaelScheduleStage() bGetMichaelScheduleStage())
//              ENDIF
                
                IF DOES_ENTITY_EXIST(script_chalk_board_prop)
                    SET_ENTITY_COORDS(script_chalk_board_prop, vFamily_scene_m_coord+vChalkBoardPropAttachOffset)
                    SET_ENTITY_ROTATION(script_chalk_board_prop, <<0,0,fFamily_scene_m_head>>+vChalkBoardPropAttachRotation)
                    
                    DrawDebugFamilyLine(GET_ENTITY_COORDS(script_chalk_board_prop), vFamily_scene_m_coord, HUD_COLOUR_ORANGE)
                ENDIF
            ELSE
                REPEAT MAX_M_SCENE_MEMBERS iPed
                    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed))
                    iCurrentFamilyEvent[iPed] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMember])
                    
                    IF bJumpToFamilyMember[iPed]
                        IF IS_PLAYER_PLAYING(PLAYER_ID())
                            IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
                                SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
                                CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
                                SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
                            ENDIF
                        ENDIF
                        
                        IF g_bWarpPlayerOnFamilyDebug
                            VECTOR vDebug_jumpOffset = <<0,0,0>>
                            Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
                        
                            VECTOR vDebug_coordOffset = vFamily_coordOffset[iPed] + vDebug_jumpOffset
                            VECTOR vPlayerHeading = vFamily_coordOffset[iPed] - vDebug_coordOffset
                            
                            VECTOR vFamily_scene_m_coord_vDebug_coordOffset = vFamily_scene_m_coord+vDebug_coordOffset
                            
                            
                            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M7_SON_coming_back_from_a_bike_ride)
                                VECTOR vDebugJumpOffset = <<0,0,0>>
                                Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember], vDebugJumpOffset)
                                
                                vFamily_scene_m_coord_vDebug_coordOffset.z += vDebugJumpOffset.z
                            ENDIF
                            
                            FLOAT fFamily_scene_m_coord_vDebug_coordOffset = 0.0
                            IF GET_GROUND_Z_FOR_3D_COORD(vFamily_scene_m_coord_vDebug_coordOffset, fFamily_scene_m_coord_vDebug_coordOffset)
                                vFamily_scene_m_coord_vDebug_coordOffset.z = fFamily_scene_m_coord_vDebug_coordOffset
                            ENDIF
                            
                            IF IS_PLAYER_PLAYING(PLAYER_ID())
                                SET_ENTITY_COORDS(PLAYER_PED_ID(), vFamily_scene_m_coord_vDebug_coordOffset)
                                SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_FROM_VECTOR_2D(vPlayerHeading.x, vPlayerHeading.y))
                            ENDIF
                            
                            SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
                        ENDIF
                        
                        bJumpToFamilyMember[iPed] = FALSE
                    ENDIF
                    
                    IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[iPed])
                        //
                    ELSE
                        fFamily_synch_scenePhase[iPed] = GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[iPed])
                    ENDIF
                ENDREPEAT
                
                iMichaelScheduleStage = ENUM_TO_INT(GetMichaelScheduleStage())
                
            ENDIF
            IF bDraw_peds
                g_bDrawDebugFamilyStuff = TRUE
                FAMILY_COMP_NAME_ENUM eFamCompArray[NUM_PED_COMPONENTS]     //
                FAMILY_COMP_NAME_ENUM ePC_head, ePC_hair, ePC_torso, ePC_leg
                FAMILY_COMP_NAME_ENUM ePC_feet, ePC_hand, ePC_special, ePC_special2
                FAMILY_COMP_NAME_ENUM ePC_decl, ePC_berd, ePC_teeth, ePC_jbib
                FAMILY_PROP_BIT_ENUM ePC_prop
                
                REPEAT MAX_M_SCENE_MEMBERS iPed
                    IF NOT IS_ENTITY_DEAD(family_ped[iPed])
                        DEBUG_GetOutfitFromFamilyMember(family_ped[iPed], eFamCompArray)
                        
                        PED_COMPONENT ePedCompID
                        REPEAT NUM_PED_COMPONENTS ePedCompID
                            SWITCH ePedCompID
                                CASE PED_COMP_HEAD
                                    VECTOR vHeadCoord
                                    vHeadCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_HEAD, <<0,0,-0.15>>)
                                    
                                    DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompArray[ePedCompID]), vHeadCoord, 0 ,HUD_COLOUR_RED)
                                BREAK
                                CASE PED_COMP_BERD      BREAK
                                CASE PED_COMP_HAIR
                                    VECTOR vHairCoord
                                    vHairCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_HEAD, <<0,0,0.15>>)
                                    
                                    DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompArray[ePedCompID]), vHairCoord, 0 ,HUD_COLOUR_RED)
                                BREAK
                                CASE PED_COMP_TORSO
                                    VECTOR vTorsoCoord
                                    vTorsoCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_R_CLAVICLE, <<0,0,0>>)
                                    
                                    DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompArray[ePedCompID]), vTorsoCoord, 0 ,HUD_COLOUR_RED)
                                BREAK
                                
                                CASE PED_COMP_LEG
                                    VECTOR vLegCoord
                                    vLegCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_R_CALF, <<0,0,0>>)
                                    
                                    DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompArray[ePedCompID]), vLegCoord, 0 ,HUD_COLOUR_RED)
                                BREAK
                                CASE PED_COMP_HAND      BREAK
                                CASE PED_COMP_FEET
                                    VECTOR vFeetCoord
                                    vFeetCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_R_FOOT, <<0,0,0>>)
                                    
                                    DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompArray[ePedCompID]), vFeetCoord, 0 ,HUD_COLOUR_RED)
                                BREAK
                                CASE PED_COMP_TEETH     BREAK
                                
                                CASE PED_COMP_SPECIAL   BREAK
                                CASE PED_COMP_SPECIAL2  BREAK
                                CASE PED_COMP_DECL      BREAK
                                CASE PED_COMP_JBIB      BREAK
                            ENDSWITCH
                            
                        ENDREPEAT
                        
                        BOOL bFoundFamilyComp = FALSE
                        
                        FAMILY_COMP_NAME_ENUM eFamCompID
                        REPEAT MAX_FAMILY_COMP eFamCompID
                            
                            enumFamilyMember eFamMember = NO_FAMILY_MEMBER
                            PED_COMPONENT eThisPedCompID = INT_TO_ENUM(PED_COMPONENT, -1)
                            INT iDrawableId = -1, iTextureID = -1
                            
                            GetComponentForFamilyMember(eFamCompID, eFamMember, eThisPedCompID, iDrawableId, iTextureID)
                            
                            IF (eFamMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed)))
                                IF (eThisPedCompID = INT_TO_ENUM(PED_COMPONENT, NUM_PED_COMPONENTS))    //is an outfit
                                    
                                    GetOutfitForFamilyMember(eFamCompID,
                                            ePC_head, ePC_hair, ePC_torso, ePC_leg,
                                            ePC_feet, ePC_hand, ePC_special, ePC_special2,
                                            ePC_decl, ePC_berd, ePC_teeth, ePC_jbib,
                                            ePC_prop)
                                    
                                    IF  (eFamCompArray[PED_COMP_head] = ePC_head)
                                    AND (eFamCompArray[PED_COMP_hair] = ePC_hair)
                                    AND (eFamCompArray[PED_COMP_torso] = ePC_torso)
                                    AND (eFamCompArray[PED_COMP_leg] = ePC_leg)
                                    
                                    AND (eFamCompArray[PED_COMP_feet] = ePC_feet)
                                    AND (eFamCompArray[PED_COMP_hand] = ePC_hand)
                                    AND (eFamCompArray[PED_COMP_special] = ePC_special)
                                    AND (eFamCompArray[PED_COMP_special2] = ePC_special2)
                                    
                                    AND (eFamCompArray[PED_COMP_decl] = ePC_decl)
                                    AND (eFamCompArray[PED_COMP_berd] = ePC_berd)
                                    AND (eFamCompArray[PED_COMP_teeth] = ePC_teeth)
                                    AND (eFamCompArray[PED_COMP_jbib] = ePC_jbib)
//                                      SAVE_STRING_TO_DEBUG_FILE("found ")
//                                      SAVE_STRING_TO_DEBUG_FILE(Get_string_From_FamilyCompName(eFamCompID))
//                                      SAVE_STRING_TO_DEBUG_FILE("... does it match?")
//                                      SAVE_NEWLINE_TO_DEBUG_FILE()
                                        
                                        VECTOR vOutfitCoord
                                        vOutfitCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_HEAD, <<0,0,0.3>>)
                                        DrawDebugFamilyTextWithOffset(Get_string_From_FamilyCompName(eFamCompID), vOutfitCoord, 0 ,HUD_COLOUR_GREEN)
                                        
                                        bFoundFamilyComp = TRUE
                                    ENDIF
                                ENDIF
                            ENDIF
                            
                        ENDREPEAT
                        
                        IF NOT bFoundFamilyComp
//                          SAVE_STRING_TO_DEBUG_FILE("NOT found ")
//                          SAVE_STRING_TO_DEBUG_FILE("... does not match?!?")
//                          SAVE_NEWLINE_TO_DEBUG_FILE()
                            
                            VECTOR vOutfitCoord
                            vOutfitCoord = GET_PED_BONE_COORDS(family_ped[iPed], BONETAG_HEAD, <<0,0,0.3>>)
                            DrawDebugFamilyTextWithOffset("NO OUTFIT???", vOutfitCoord, 0 ,HUD_COLOUR_GREEN)
                        ENDIF
                    ENDIF
                ENDREPEAT
            ENDIF
            
            IF bSave_peds
                OPEN_DEBUG_FILE()
                    
                    SAVE_STRING_TO_DEBUG_FILE(" //- PRIVATE_Get_FamilyMember_Init_Offset() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
                    
                    REPEAT MAX_M_SCENE_MEMBERS iPed
                        enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed))
                        
                        IF (g_eCurrentFamilyEvent[eFamilyMember] <> NO_FAMILY_EVENTS)
                        AND (g_eCurrentFamilyEvent[eFamilyMember] <> FAMILY_MEMBER_BUSY)
                            IF NOT ARE_VECTORS_EQUAL(vFamily_coordOffset[iPed], <<0,0,0>>)
                                SAVE_STRING_TO_DEBUG_FILE("CASE FE_M_")
                                SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(eFamilyMember))
                                SAVE_STRING_TO_DEBUG_FILE("     //")
                                SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
                                SAVE_NEWLINE_TO_DEBUG_FILE()
                                
                            ENDIF
                            
                            PS_M_SCENE_MEMBERS eMatchingSceneMember = NO_M_SCENE_MEMBER
                            
                            INT iPrevPed
                            REPEAT iPed iPrevPed
                                CPRINTLN(DEBUG_FAMILY, "  [", Get_String_From_FamilyMember(eFamilyMember), ", ", iPrevPed, "], ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[iPrevPed]), " / ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
                                
                                IF (eMatchingSceneMember = NO_M_SCENE_MEMBER)
                                    IF (g_eCurrentFamilyEvent[iPrevPed] = g_eCurrentFamilyEvent[eFamilyMember])
                                        eMatchingSceneMember = INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPrevPed)
                                    ENDIF
                                ENDIF
                                
                            ENDREPEAT
                            
                            IF (eMatchingSceneMember <> NO_M_SCENE_MEMBER)
                                
                                SAVE_STRING_TO_DEBUG_FILE(" /*")SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" vInitOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" fInitHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" */")SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" ")SAVE_NEWLINE_TO_DEBUG_FILE()
                                
                                SAVE_STRING_TO_DEBUG_FILE(" IF PRIVATE_Get_FamilyMember_Init_Offset(")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyMember(Get_Family_Member_From_Michael_Scene_Member(eMatchingSceneMember)))SAVE_STRING_TO_DEBUG_FILE(" eFamilyEvent vInitOffset fInitHead)")SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE("     vInitOffset += ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed] - vFamily_coordOffset[eMatchingSceneMember])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE("     fInitHead   += ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed] - fFamily_headOffset[eMatchingSceneMember])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE("     RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" ENDIF")SAVE_NEWLINE_TO_DEBUG_FILE()
                                
                            ELSE
                                SAVE_STRING_TO_DEBUG_FILE(" vInitOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vFamily_coordOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" fInitHead = ")SAVE_FLOAT_TO_DEBUG_FILE(fFamily_headOffset[iPed])SAVE_NEWLINE_TO_DEBUG_FILE()
                                SAVE_STRING_TO_DEBUG_FILE(" RETURN TRUE")SAVE_NEWLINE_TO_DEBUG_FILE()
                            ENDIF
                            
                            SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
                            SAVE_NEWLINE_TO_DEBUG_FILE()
                        ENDIF
                    ENDREPEAT
                    
                    SAVE_STRING_TO_DEBUG_FILE(" //- Get_DebugJumpOffset_From_FamilyEvent() -//")SAVE_NEWLINE_TO_DEBUG_FILE()
                    
                    REPEAT MAX_M_SCENE_MEMBERS iPed
                        IF bJumpToFamilyMember[iPed]
                            enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, iPed))
                            
                            VECTOR vDebug_jumpOffset = <<0,0,0>>
                            Get_Vector_From_DebugJumpAngle(fPlayerDebugJumpAngle[iPed], fPlayerDebugJumpDistance[iPed], vDebug_jumpOffset)
                            
                            SAVE_STRING_TO_DEBUG_FILE("CASE FE_M_")SAVE_STRING_TO_DEBUG_FILE(Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))SAVE_NEWLINE_TO_DEBUG_FILE()
                            SAVE_STRING_TO_DEBUG_FILE(" RETURN ")SAVE_VECTOR_TO_DEBUG_FILE(vDebug_jumpOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
                            SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
                            SAVE_NEWLINE_TO_DEBUG_FILE()
                        ENDIF
                    ENDREPEAT
                    
                    SAVE_NEWLINE_TO_DEBUG_FILE()
                CLOSE_DEBUG_FILE()
                bSave_peds = FALSE
            ENDIF
            
            IF g_bUpdatedFamilyEvents
                PS_M_SCENE_MEMBERS eSceneMember
                SWITCH g_eDebugSelectedMember
                    CASE FM_MICHAEL_SON
                        eSceneMember = PS_M_SON
                    BREAK
                    CASE FM_MICHAEL_DAUGHTER
                        eSceneMember = PS_M_DAUGHTER
                    BREAK
                    CASE FM_MICHAEL_WIFE
                        eSceneMember = PS_M_WIFE
                    BREAK
                    CASE FM_MICHAEL_MEXMAID
                        eSceneMember = PS_M_MEXMAID
                    BREAK
                    CASE FM_MICHAEL_GARDENER
                        eSceneMember = PS_M_GARDENER
                    BREAK
//                  CASE FM_TREVOR_0_MICHAEL
//                      eSceneMember = PS_M_MICHAEL
//                  BREAK
                    
                    DEFAULT
                        eSceneMember = NO_M_SCENE_MEMBER
                    BREAK
                ENDSWITCH
                
                IF (eSceneMember <> NO_M_SCENE_MEMBER)
                    bJumpToFamilyMember[eSceneMember] = TRUE
                    
                    DO_SCREEN_FADE_OUT(0)
                    LOAD_ALL_OBJECTS_NOW()
                    DO_SCREEN_FADE_IN(0)
                    
                    SET_DOOR_STATE(DOORNAME_M_MANSION_F_L,  DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_F_R,  DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_G1,   DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_R_L1, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_R_R1, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_R_L2, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    SET_DOOR_STATE(DOORNAME_M_MANSION_R_R2, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
                    
            //      Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
                    
                    PRIVATE_Set_Current_Family_Member_Event(g_eDebugSelectedMember, g_eCurrentFamilyEvent[g_eDebugSelectedMember])
                    Update_Michael_Family_Member_Widget(eSceneMember, g_eDebugSelectedMember)
                    
                    IF NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
                        CLEAR_PED_TASKS(family_ped[eSceneMember])
                        
                        IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMember])
                            VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMember])
                            
                            IF NOT IS_ENTITY_DEAD(familyVeh)
                                SET_ENTITY_COORDS(familyVeh, vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember])
                                SET_ENTITY_HEADING(familyVeh, fFamily_scene_m_head+fFamily_headOffset[eSceneMember])
                            ENDIF
                        ELSE
                            SET_ENTITY_COORDS(family_ped[eSceneMember], vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember])
                            SET_ENTITY_HEADING(family_ped[eSceneMember], fFamily_scene_m_head+fFamily_headOffset[eSceneMember])
                        ENDIF
                    ENDIF
                    
                    PS_M_SCENE_MEMBERS eSceneMemberUpdate
                    REPEAT MAX_M_SCENE_MEMBERS eSceneMemberUpdate
                        enumFamilyMember eFamilyMemberUpdate = Get_Family_Member_From_Michael_Scene_Member(INT_TO_ENUM(PS_M_SCENE_MEMBERS, eSceneMemberUpdate))
                        
                        IF (iCurrentFamilyEvent[eSceneMemberUpdate] <> ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate]))
                            Update_Previous_Event_For_FamilyMember(g_eDebugSelectedMember)
                            
                            iCurrentFamilyEvent[eSceneMemberUpdate] = ENUM_TO_INT(g_eCurrentFamilyEvent[eFamilyMemberUpdate])
                            Update_Michael_Family_Member_Widget(eSceneMemberUpdate, eFamilyMemberUpdate)
                            
                            IF NOT IS_ENTITY_DEAD(family_ped[eSceneMemberUpdate])
                                CLEAR_PED_TASKS(family_ped[eSceneMemberUpdate])
                                
                                IF IS_PED_IN_ANY_VEHICLE(family_ped[eSceneMemberUpdate])
                                    VEHICLE_INDEX familyVeh = GET_VEHICLE_PED_IS_IN(family_ped[eSceneMemberUpdate])
                                    
                                    IF NOT IS_ENTITY_DEAD(familyVeh)
                                        SET_ENTITY_COORDS(familyVeh, vFamily_scene_m_coord+vFamily_coordOffset[eSceneMemberUpdate])
                                        SET_ENTITY_HEADING(familyVeh, fFamily_scene_m_head+fFamily_headOffset[eSceneMemberUpdate])
                                    ENDIF
                                ELSE
                                    SET_ENTITY_COORDS(family_ped[eSceneMemberUpdate], vFamily_scene_m_coord+vFamily_coordOffset[eSceneMemberUpdate])
                                    SET_ENTITY_HEADING(family_ped[eSceneMemberUpdate], fFamily_scene_m_head+fFamily_headOffset[eSceneMemberUpdate])
                                ENDIF
                            ENDIF
                            
                        ENDIF
                    ENDREPEAT
                    
                    g_bUpdatedFamilyEvents = FALSE
                ENDIF
            ENDIF
            
        ENDIF
    ENDIF
ENDPROC

//PURPOSE:  Moniters for any keyboard inputs that would alter the mission (debug pass etc)
PROC Family_Scene_M_Debug_Options()
    
    IF IS_KEYBOARD_KEY_PRESSED(KEY_C)
        IF g_bDrawDebugFamilyStuff
            bSave_peds = TRUE
        ELSE
            CPRINTLN(DEBUG_FAMILY, "toggle g_bDrawDebugFamilyStuff to save family_scene_m.sc info")
        ENDIF
    ENDIF
    
ENDPROC
#ENDIF

// *******************************************************************************************
//  GENERAL FUNCTIONS AND PROCEDURES
// *******************************************************************************************

// #833431 - Cleanup amanda tracey and maid when michael gets on bike
FUNC BOOL MAGDEMO_Delete_When_Player_Biking(PED_INDEX PedIndex, enumFamilyMember eFamilyMember, enumFamilyEvents eFamilyEvent)
    IF NOT g_bMagDemoActive
        RETURN FALSE
    ENDIF
    
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        
        IF IS_ENTITY_ON_SCREEN(PedIndex)
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "MAGDEMO_Delete_When_Player_Biking(\"", Get_String_From_FamilyMember(eFamilyMember), "\") - ON SCREEN")
            #ENDIF
            
            RETURN FALSE
        ENDIF
        
        IF IS_PED_IN_ANY_VEHICLE(PedIndex)
            CONST_FLOAT iDIST_SAFE_TO_DELETE_DRIVING_FAMILY_MEMBER  40.0
            
            IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(family_ped[eFamilyMember]))
                    < iDIST_SAFE_TO_DELETE_DRIVING_FAMILY_MEMBER*iDIST_SAFE_TO_DELETE_DRIVING_FAMILY_MEMBER)
                
                #IF IS_DEBUG_BUILD
                CPRINTLN(DEBUG_FAMILY, "MAGDEMO_Delete_When_Player_Biking(\"", Get_String_From_FamilyMember(eFamilyMember), "\") - dist:", VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(family_ped[eFamilyMember])))
                #ENDIF
                
                RETURN FALSE
            ENDIF
        ENDIF
        
        #IF IS_DEBUG_BUILD
        CPRINTLN(DEBUG_FAMILY, "MAGDEMO_Delete_When_Player_Biking(\"", Get_String_From_FamilyMember(eFamilyMember), "\")... NO_FAMILY_EVENTS")
        #ENDIF
        
        g_eCurrentFamilyEvent[eFamilyMember] = NO_FAMILY_EVENTS
        RETURN TRUE
    ENDIF
    
    eFamilyEvent = eFamilyEvent
    RETURN FALSE
ENDFUNC

FUNC BOOL Update_Family_Scene_Doors_For_Event_Change(PS_M_SCENE_MEMBERS eSceneMember)
    
    MODEL_NAMES ObjectModel = DUMMY_MODEL_FOR_SCRIPT
    VECTOR vecPos = <<0,0,0>>
    DOOR_NAME_ENUM eDoorName = DUMMY_DOORNAME
    DOOR_HASH_ENUM eDoorHash = DUMMY_DOORHASH
    TEXT_LABEL_63 roomName = ""
    
    BOOL bIgnoreInRoomCheck = FALSE
    
    IF (eSceneMember = PS_M_SON)
        PRIVATE_Get_Family_Door_Attributes(FD_0_MICHAEL_SON,
                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
    ELIF (eSceneMember = PS_M_DAUGHTER)
        PRIVATE_Get_Family_Door_Attributes(FD_1_MICHAEL_DAUGHTER,
                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
    ELSE
        RETURN FALSE
    ENDIF
    
    IF DOES_ENTITY_EXIST(family_door[eSceneMember])
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 str = Get_String_From_Family_Scene_M_Scene_Members(eSceneMember)
        str += " door exists!"
        
        CPRINTLN(DEBUG_FAMILY, str)
        #ENDIF
        
        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
        DELETE_OBJECT(family_door[eSceneMember])
        
        IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
            DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_LOCKED_THIS_FRAME, default, TRUE)
            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.0, default, TRUE)
        ENDIF
    ENDIF
    
    IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
        #IF IS_DEBUG_BUILD
        TEXT_LABEL_63 str = Get_String_From_Family_Scene_M_Scene_Members(eSceneMember)
        str += " chair exists!"
        
        CPRINTLN(DEBUG_FAMILY, str)
        #ENDIF
        
        DELETE_OBJECT(family_chair[eSceneMember])
    ENDIF
    
    RETURN TRUE
ENDFUNC

PROC Control_Michael_FamilyEvent_Tasks(PS_M_SCENE_MEMBERS eSceneMember, enumFamilyEvents eFamilyEvent)
    CDEBUG3LN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Control_Michael_FamilyEvent_Tasks.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
    
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
    INT iRandCount, iSpeechBit, iPlayerBitset
    
    SWITCH eFamilyEvent
        /* MICHAEL'S HOUSE */
        CASE FE_M_FAMILY_on_laptops
            
            IF (eSceneMember = PS_M_SON)
                IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "FAMILY_ON_LAPTOPS", "", NULL, family_sfxBank[eSceneMember])
                ENDIF
            ENDIF
            IF (eSceneMember = PS_M_DAUGHTER)
                
                IF NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sDaughterFakeCellphoneData)
                    REQUEST_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
                ELSE
                    DRAW_FAKE_CELLPHONE_SCREEN(sDaughterFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
                ENDIF
            ENDIF
            
            IF (eSceneMember = PS_M_WIFE)
                IF PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[eSceneMember], TRUE, FALSE)
                    
                    IF NOT DOES_ENTITY_EXIST(family_chair[eSceneMember])
                        VECTOR vecPos
                        MODEL_NAMES ObjectModel
                        ObjectModel = V_ILEV_M_DINECHAIR
                        vecPos = <<-796.65,181.23,71.83>>
                        
                        IF IS_VALID_INTERIOR(iInteriorForMember[eSceneMember])
                            IF IS_INTERIOR_READY(iInteriorForMember[eSceneMember])
                                family_chair[eSceneMember] = GET_CLOSEST_OBJECT_OF_TYPE(vecPos, 10.0, ObjectModel, TRUE)
                                
                                IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
                                    FORCE_ROOM_FOR_ENTITY(family_chair[eSceneMember], iInteriorForMember[eSceneMember], GET_ROOM_KEY_FROM_ENTITY(family_ped[eSceneMember]))
                                    SET_ENTITY_COLLISION(family_chair[eSceneMember], FALSE)
                                ELSE
                                    CPRINTLN(DEBUG_FAMILY, "NOT DOES_ENTITY_EXIST")
                                ENDIF
                            ELSE
                                CPRINTLN(DEBUG_FAMILY, "NOT IS_INTERIOR_READY")
                            ENDIF
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, "NOT IS_VALID_INTERIOR")
                        ENDIF
                    ELSE
                        PRIVATE_Set_Family_SyncSceneProp(family_chair[eSceneMember],
                                tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                                "_Amanda", "_Chair",
                                iFamily_synch_scene[eSceneMember])
                    ENDIF
                
                    
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "_Amanda", "_Novel",
                            iFamily_synch_scene[eSceneMember])
                ENDIF
            ELSE
                IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                    
                    IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
                        SET_ENTITY_COORDS(family_chair[eSceneMember],
                                vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember])
                        SET_ENTITY_ROTATION(family_chair[eSceneMember],
                                <<0,0,fFamily_scene_m_head+fFamily_headOffset[eSceneMember]>>)
                    ENDIF
                    
                    PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            vFamily_scene_m_coord, fFamily_scene_m_head)
                ENDIF
            ENDIF
            
        BREAK
        
        CASE FE_M_FAMILY_MIC4_locked_in_room
            PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            
            IF (eSceneMember = PS_M_DAUGHTER)
                PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            ENDIF
        BREAK
        
        CASE FE_M7_FAMILY_finished_breakfast
        CASE FE_M7_FAMILY_finished_pizza
            IF (eSceneMember = PS_M_SON)
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[PS_M_SON], TRUE, TRUE)
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "_Jimmy", "_Prop_Laptop_01a",
                        iFamily_synch_scene[eSceneMember])
                
                IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                ENDIF
            ELIF (eSceneMember = PS_M_DAUGHTER)
                PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[PS_M_SON], tDialogueAnimClips[eSceneMember],
                        tDialogueAnimClips[PS_M_SON], "_JIMMY",
                        iFamily_synch_scene[PS_M_SON])
                
                IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                ENDIF
            ELSE
                IF PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[PS_M_SON], tDialogueAnimClips[eSceneMember],
                        tDialogueAnimClips[PS_M_SON], "_JIMMY",
                        iFamily_synch_scene[PS_M_SON])
                
                    IF NOT bAmandasBreakfastChairSettled
                        IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
            IF (eSceneMember != PS_M_SON)
            AND NOT DOES_ENTITY_EXIST(family_ped[PS_M_SON])
            AND (g_eCurrentSafehouseActivity = SA_NONE)
                IF NOT bForceCreation
                    MODEL_NAMES eSonModel
                    enumCharacterList eSonID
                    
                    eSonModel = DUMMY_MODEL_FOR_SCRIPT
                    eSonID = PRIVATE_Get_CharList_From_FamilyMember(FM_MICHAEL_SON, eSonModel)
                    REQUEST_NPC_PED_MODEL(eSonID)
                    IF HAS_NPC_PED_MODEL_LOADED(eSonID)
                        IF NOT CREATE_NPC_PED_ON_FOOT(family_ped[PS_M_SON], eSonID,
                                vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember],
                                WRAP(fFamily_scene_m_head+fFamily_headOffset[eSceneMember], 0 ,360))
                            
                            VECTOR vvv
                            FLOAT hhh
                            vvv = vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember]
                            hhh = WRAP(fFamily_scene_m_head+fFamily_headOffset[eSceneMember], 0 ,360)
                            CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son [prev:", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_SON]), "](", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " creating the ped [",  GET_PLAYER_PED_STRING(eSonID), ", ",  vvv, ", ",  hhh, "]...) ")
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son [prev:", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_SON]), "](", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " CREATED THE PED!!!)")
                            
                            g_eCurrentFamilyEvent[FM_MICHAEL_SON] = eFamilyEvent
                            
                            PRIVATE_SetDefaultFamilyMemberAttributes(family_ped[PS_M_SON], RELGROUPHASH_FAMILY_M)
                            SetFamilyMemberComponentVariation(family_ped[PS_M_SON], FM_MICHAEL_SON, g_eCurrentFamilyEvent[FM_MICHAEL_SON])
                            SetFamilyMemberConfigFlag(family_ped[PS_M_SON], FM_MICHAEL_SON, RELGROUPHASH_FAMILY_M, FALSE)
                            
                            SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(family_ped[PS_M_SON], TRUE)
                            
                            ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ENUM_TO_INT(FM_MICHAEL_SON)+1,
                                    family_ped[PS_M_SON], Get_VoiceID_From_FamilyMember(FM_MICHAEL_SON))
                            
                            iInteriorForMember[PS_M_SON] = GET_INTERIOR_FROM_ENTITY(family_ped[PS_M_SON])
                            bForceCreation = TRUE
                        ENDIF
                    ELSE
                        CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son [prev:", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[FM_MICHAEL_SON]), "](", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " not loaded???), breaking the anim")
                    ENDIF
                ENDIF
            ENDIF
        BREAK
        CASE FE_M7_FAMILY_watching_TV
            IF (eSceneMember = PS_M_SON)
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[PS_M_SON], TRUE, TRUE)
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ELIF (eSceneMember = PS_M_DAUGHTER)
                PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[PS_M_SON], tDialogueAnimClips[eSceneMember],
                        tDialogueAnimClips[PS_M_SON], "_JIMMY",
                        iFamily_synch_scene[PS_M_SON])
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ELSE
                PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[PS_M_SON], tDialogueAnimClips[eSceneMember],
                        tDialogueAnimClips[PS_M_SON], "_JIMMY",
                        iFamily_synch_scene[PS_M_SON])
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
            
            IF (eSceneMember != PS_M_SON)
            AND NOT DOES_ENTITY_EXIST(family_ped[PS_M_SON])
            AND (g_eCurrentSafehouseActivity = SA_NONE)
                IF NOT bForceCreation
                    MODEL_NAMES eSonModel
                    enumCharacterList eSonID
                    
                    eSonModel = DUMMY_MODEL_FOR_SCRIPT
                    eSonID = PRIVATE_Get_CharList_From_FamilyMember(FM_MICHAEL_SON, eSonModel)
                    REQUEST_NPC_PED_MODEL(eSonID)
                    IF HAS_NPC_PED_MODEL_LOADED(eSonID)
                        IF NOT CREATE_NPC_PED_ON_FOOT(family_ped[PS_M_SON], eSonID,
                                vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember],
                                WRAP(fFamily_scene_m_head+fFamily_headOffset[eSceneMember], 0 ,360))
                            
                            VECTOR vvv
                            FLOAT hhh
                            vvv = vFamily_scene_m_coord+vFamily_coordOffset[eSceneMember]
                            hhh = WRAP(fFamily_scene_m_head+fFamily_headOffset[eSceneMember], 0 ,360)
                            CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son(", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " creating the ped [",  GET_PLAYER_PED_STRING(eSonID), ", ",  vvv, ", ",  hhh, "]...) ")
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son(", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " CREATED THE PED!!!)")
                            
                            g_eCurrentFamilyEvent[FM_MICHAEL_SON] = eFamilyEvent
                            
                            PRIVATE_SetDefaultFamilyMemberAttributes(family_ped[PS_M_SON], RELGROUPHASH_FAMILY_M)
                            SetFamilyMemberComponentVariation(family_ped[PS_M_SON], FM_MICHAEL_SON, g_eCurrentFamilyEvent[FM_MICHAEL_SON])
                            SetFamilyMemberConfigFlag(family_ped[PS_M_SON], FM_MICHAEL_SON, RELGROUPHASH_FAMILY_M, FALSE)
                            
                            SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(family_ped[PS_M_SON], TRUE)
                            
                            ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, ENUM_TO_INT(FM_MICHAEL_SON)+1,
                                    family_ped[PS_M_SON], Get_VoiceID_From_FamilyMember(FM_MICHAEL_SON))
                            
                            iInteriorForMember[PS_M_SON] = GET_INTERIOR_FROM_ENTITY(family_ped[PS_M_SON])
                            bForceCreation = TRUE
                        ENDIF
                    ELSE
                        CPRINTLN(DEBUG_FAMILY, Get_String_From_FamilyEvent(eFamilyEvent), " & ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), " has a non-existant son(", GET_MODEL_NAME_FOR_DEBUG(GET_NPC_PED_MODEL(eSonID)), " not loaded???), breaking the anim")
                    ENDIF
                ENDIF
            ENDIF
            
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        
        CASE FE_M_SON_sleeping
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            //PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
            PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
        BREAK
        CASE FE_M2_SON_gaming_loop
        CASE FE_M7_SON_gaming
            PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "SON_GAMING", "", NULL, family_sfxBank[eSceneMember])
            ENDIF
            
            bUpdateInteriorForPlayer = TRUE
            
            FLOAT fForceTVOffPhase
            fForceTVOffPhase = 0.7
            
            IF (eFamilyEvent = FE_M2_SON_gaming_loop)
                PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        FE_ANY_wander_family_event,
                        "FMMAUD", "", "JIM_IG_2c", tCreatedMichaelConvLabels,
                        MyLocalPedStruct, sSpeechTimer)
                fForceTVOffPhase = 0.7
            ELIF (eFamilyEvent = FE_M7_SON_gaming)
                PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        FE_ANY_wander_family_event,
                        "FMMAUD", "", "FAMR_IG_1", tCreatedMichaelConvLabels,
                        MyLocalPedStruct, sSpeechTimer)
                fForceTVOffPhase = 0.10
            ENDIF
            
            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "", "_Controller",
                    iFamily_synch_scene[eSceneMember])
            
            //1271401
            IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
                IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) <= 0.7
                OR (iFamily_enter_veh_stage[eSceneMember] <= 1)
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "", "_Headset",
                            iFamily_synch_scene[eSceneMember])
                ELSE
                    IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                        IF IS_ENTITY_ATTACHED(family_prop_r[eSceneMember])
                            DETACH_ENTITY(family_prop_r[eSceneMember])
                        ENDIF
                        
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eSceneMember], NORMAL_BLEND_IN, TRUE)
                        FREEZE_ENTITY_POSITION(family_prop_r[eSceneMember], TRUE)
                    ENDIF
                ENDIF

                IF (iFamily_enter_veh_stage[eSceneMember] < 3)
                    PRIVATE_Update_Tv_Globals(eFamilyMember, eFamilyEvent)
                    
                    CPRINTLN(DEBUG_FAMILY, "TV loop [forced on: ", iFamily_enter_veh_stage[eSceneMember], ", ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]), "]")
                    
                ELSE
                    IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) <= fForceTVOffPhase
                        CPRINTLN(DEBUG_FAMILY, "TV exit [forced on: ", iFamily_enter_veh_stage[eSceneMember], ", ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]), "]")
                    ELSE
                        IF IS_THIS_TV_FORCED_ON(TV_LOC_JIMMY_BEDROOM)
                            CPRINTLN(DEBUG_FAMILY, "TV on [forced off: ", iFamily_enter_veh_stage[eSceneMember], ", ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]), "]")
                            
                            RELEASE_TV_FOR_PLAYER_CONTROL(TV_LOC_JIMMY_BEDROOM)
                            STOP_TV_PLAYBACK(DEFAULT, TV_LOC_JIMMY_BEDROOM)
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, "TV off [NOT forced off: ", iFamily_enter_veh_stage[eSceneMember], ", ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]), "]")
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        BREAK
        CASE FE_M_SON_rapping_in_the_shower
            PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head)
//          PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
//          bUpdateInteriorForPlayer = TRUE
            
//          PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
//                  "FAMILY_SOUNDS_01", "MICHAELS_HOUSE_DAUGHTER_SHOWER", family_ped[eSceneMember])
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "SON_RAPPING_IN_THE_SHOWER", "AFT_SON_RAPPING_IN_THE_SHOWER_MASTER", family_ped[eSceneMember], family_sfxBank[eSceneMember])
        BREAK
        CASE FE_M_SON_Borrows_sisters_car
            PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember,
                    family_veh[PS_MV_DAUGHTER_issi], FM_MICHAEL_DAUGHTER,
                    eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember],
                    AUTODOOR_MICHAEL_MANSION_GATE,
                    "FMMAUD", "FMM_0_3", "FMM_0_3", tCreatedMichaelConvLabels, -1.0, 
                    MyLocalPedStruct, sSpeechTimer, "GENERIC_INSULT_MED"
                    #IF USE_TU_CHANGES
                    , iInteriorForPlayer
                    #ENDIF
                    )
            
//          INT iVeh
//          REPEAT COUNT_OF(family_veh) iVeh
//              IF NOT DOES_ENTITY_EXIST(family_veh[iVeh])
//                  CPRINTLN(DEBUG_FAMILY, "family_veh ", iVeh, " doesnt exist")
//              ELSE
//                  CPRINTLN(DEBUG_FAMILY, "family_veh ", iVeh, " exists")
//              ENDIF
//          ENDREPEAT
        BREAK
        CASE FE_M7_SON_going_for_a_bike_ride
            PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember,
                    family_veh[PS_MV_SON_bike], FM_MICHAEL_SON,
                    eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember],
                    AUTODOOR_MICHAEL_MANSION_GATE,
                    "FMMAUD", "FMM7_0_3", "FMM7_0_3", tCreatedMichaelConvLabels, -1.0, 
                    MyLocalPedStruct, sSpeechTimer, "GENERIC_INSULT_MED"
                    #IF USE_TU_CHANGES
                    , iInteriorForPlayer
                    #ENDIF
                    )
        BREAK
        CASE FE_M7_SON_coming_back_from_a_bike_ride
            PRIVATE_Update_Family_DrivingHome(family_ped[eSceneMember], eFamilyMember,
                    family_veh[PS_MV_SON_bike], FM_MICHAEL_SON,
                    eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head, <<-825.5043, 182.7998, 70.7502>>,
                    AUTODOOR_MICHAEL_MANSION_GATE, FE_ANY_find_family_event,
                    "FMMAUD", "FMM7_0_4", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
        BREAK

        CASE FE_M_SON_watching_porn
            IF NOT PRIVATE_Update_Family_AskForCash(GET_CURRENT_PLAYER_PED_ENUM(),
                    family_ped[eSceneMember], eFamilyMember, eFamilyEvent, BIT_MICHAEL,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    intention_index, "FMMAUD", "FMM_0_4b", tCreatedMichaelConvLabels,
                    iFamily_synch_scene[eSceneMember],
                    MyLocalPedStruct, sSpeechTimer)
                PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
            
            PRIVATE_Play_Family_Stream(family_sfxStage[eSceneMember], "AFT_SON_PORN", GET_ENTITY_COORDS(family_ped[eSceneMember]))
            
            PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M_SON_in_room_asks_for_munchies
//          IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,    
//                  tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                  vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
//              PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                      vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
//          ENDIF
            
            
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
        BREAK
        CASE FE_M_SON_phone_calls_in_room
            PRIVATE_Update_Family_ON_PHONE(family_ped[eSceneMember])
            PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            bUpdateInteriorForPlayer = TRUE
            
            Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
            IF (iRandSpeechCount[eSceneMember] >= iRandCount)
                Play_This_Family_Speech(family_ped[eSceneMember],
                        g_eCurrentFamilyEvent[eFamilyMember],
                        MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels,
                        sSpeechTimer, iRandSpeechCount[eSceneMember],
                        DEFAULT, "FMM_0_6x")
            ENDIF
        BREAK
        CASE FE_M_SON_on_ecstasy_AND_friendly
//          IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                  tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                  vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
                PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                        SLOW_BLEND_IN, SLOW_BLEND_OUT)
//          ENDIF
        BREAK
        CASE FE_M_SON_Fighting_with_sister_A
            enumFamilyEvents eDesiredFamilyEvent_jimFightTraA
            eDesiredFamilyEvent_jimFightTraA = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_jimFightTraA = FE_M_SON_phone_calls_in_room
            ELIF (eSceneMember = PS_M_DAUGHTER)
                eDesiredFamilyEvent_jimFightTraA = FE_M_DAUGHTER_on_phone_LOCKED
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_jimFightTraA,
                    "FMMAUD", "TRA_IG_12", "TRA_IG_9a", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            
            IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                IF (eSceneMember = PS_M_SON)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_0_MICHAEL_SON,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR2",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ELIF (eSceneMember = PS_M_DAUGHTER)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_1_MICHAEL_DAUGHTER,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "TRACY", "DOOR1",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
            
                Update_Family_Scene_Doors_For_Event_Change(eSceneMember)
                PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember,
                        g_eCurrentFamilyEvent[eFamilyMember],
                        vFamily_coordOffset[eSceneMember],
                        fFamily_headOffset[eSceneMember])
                Initialise_Michael_Family_Prop_Variables(eSceneMember)
            ENDIF
        BREAK
        CASE FE_M_SON_Fighting_with_sister_B
            enumFamilyEvents eDesiredFamilyEvent_jimFightTraB
            eDesiredFamilyEvent_jimFightTraB = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_jimFightTraB = FE_M_SON_phone_calls_in_room
            ELIF (eSceneMember = PS_M_DAUGHTER)
                eDesiredFamilyEvent_jimFightTraB = FE_M_DAUGHTER_on_phone_LOCKED
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_jimFightTraB,
                    "FMMAUD", "TRA_IG_12", "TRA_IG_9b", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            
            IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                IF (eSceneMember = PS_M_SON)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_0_MICHAEL_SON,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR2",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ELIF (eSceneMember = PS_M_DAUGHTER)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_1_MICHAEL_DAUGHTER,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "TRACY", "DOOR1",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                Update_Family_Scene_Doors_For_Event_Change(eSceneMember)
                
                PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember,
                        g_eCurrentFamilyEvent[eFamilyMember],
                        vFamily_coordOffset[eSceneMember],
                        fFamily_headOffset[eSceneMember])
                Initialise_Michael_Family_Prop_Variables(eSceneMember)
            ENDIF
        BREAK
        CASE FE_M_SON_Fighting_with_sister_C
            enumFamilyEvents eDesiredFamilyEvent_jimFightTraC
            eDesiredFamilyEvent_jimFightTraC = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_jimFightTraC = FE_M_SON_phone_calls_in_room
            ELIF (eSceneMember = PS_M_DAUGHTER)
                eDesiredFamilyEvent_jimFightTraC = FE_M_DAUGHTER_on_phone_LOCKED
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_jimFightTraC ,
                    "FMMAUD", "TRA_IG_12", "TRA_IG_9c", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            
            IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                IF (eSceneMember = PS_M_SON)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_0_MICHAEL_SON,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR2",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ELIF (eSceneMember = PS_M_DAUGHTER)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_1_MICHAEL_DAUGHTER,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "TRACY", "DOOR1",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                Update_Family_Scene_Doors_For_Event_Change(eSceneMember)
                
                PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember,
                        g_eCurrentFamilyEvent[eFamilyMember],
                        vFamily_coordOffset[eSceneMember],
                        fFamily_headOffset[eSceneMember])
                Initialise_Michael_Family_Prop_Variables(eSceneMember)
            ENDIF
        BREAK
        CASE FE_M_SON_Fighting_with_sister_D
            enumFamilyEvents eDesiredFamilyEvent_jimFightTraD
            eDesiredFamilyEvent_jimFightTraD = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_jimFightTraD = FE_M_SON_phone_calls_in_room
            ELIF (eSceneMember = PS_M_DAUGHTER)
                eDesiredFamilyEvent_jimFightTraD = FE_M_DAUGHTER_on_phone_LOCKED
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_jimFightTraD,
                    "FMMAUD", "TRA_IG_12", "TRA_IG_9d", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            
            IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                IF (eSceneMember = PS_M_SON)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_0_MICHAEL_SON,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR2",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ELIF (eSceneMember = PS_M_DAUGHTER)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_1_MICHAEL_DAUGHTER,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "TRACY", "DOOR1",
                            iFamily_synch_scene[PS_M_SON], TRUE)
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                Update_Family_Scene_Doors_For_Event_Change(eSceneMember)
                
                PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember,
                        g_eCurrentFamilyEvent[eFamilyMember],
                        vFamily_coordOffset[eSceneMember],
                        fFamily_headOffset[eSceneMember])
                Initialise_Michael_Family_Prop_Variables(eSceneMember)
            ENDIF
        BREAK
        CASE FE_M_SON_smoking_weed_in_a_bong
            
//          IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], vFamily_scene_m_coord, fFamily_scene_m_head)
//              PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                      vFamily_scene_m_coord, fFamily_scene_m_head)
//          ENDIF
            
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, FALSE)
            
            PRIVATE_Update_Family_AnimPtfx(family_ped[eSceneMember], eFamilyEvent,
                    "scr_fam_bong_smoke", family_ptfx[eSceneMember], family_b_ptfx[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    iFamily_synch_scene[eSceneMember], "exhale", 5, FALSE)
            PRIVATE_Update_Family_AnimPtfx(family_ped[eSceneMember], eFamilyEvent,
                    "cs_mich1_lighter_flame", family_other_ptfx[0], family_b_ptfx[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    iFamily_synch_scene[eSceneMember], "flame", 6, FALSE , family_prop_l[eSceneMember])
            
            IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
                MODEL_NAMES ObjectModel //= DUMMY_MODEL_FOR_SCRIPT
                VECTOR vecPos //= <<0,0,0>>
                DOOR_NAME_ENUM eDoorName //= DUMMY_DOORNAME
                DOOR_HASH_ENUM eDoorHash //= DUMMY_DOORHASH
                TEXT_LABEL_63 roomName
                
                BOOL bIgnoreInRoomCheck //= FALSE
                
                PRIVATE_Get_Family_Door_Attributes(FD_0_MICHAEL_SON,
                        ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(eDoorHash), ObjectModel, vecPos)
            ELSE
                FLOAT fM_MANSION_SON_open_ratio
                fM_MANSION_SON_open_ratio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON))
                CPRINTLN(DEBUG_FAMILY, "M_MANSION_SON open ratio: ", DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_SON)))
                
                IF ABSF(fM_MANSION_SON_open_ratio) < 0.15
                    PRIVATE_Update_Family_Ptfx_Looped(family_ped[eSceneMember], eFamilyEvent,
                            "scr_fam_door_smoke", family_other_ptfx[1], 7)
                ELSE
                    #IF IS_DEBUG_BUILD
                    TEXT_LABEL_63 str
                    str  = ("stop \"")
                    str += ("scr_fam_door_smoke")
                    str += ("\", ratio is ")
                    str += GET_STRING_FROM_FLOAT(ABSF(fM_MANSION_SON_open_ratio))
                    
                    IF DOES_PARTICLE_FX_LOOPED_EXIST(family_other_ptfx[1])
                        str += (" exists")
                        DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 7, HUD_COLOUR_YELLOW)
                    ELSE
                        str += (" gone")
                        DrawDebugFamilyTextWithOffset(str, GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), 7, HUD_COLOUR_RED)
                    ENDIF
                    #ENDIF
                    
                    IF DOES_PARTICLE_FX_LOOPED_EXIST(family_other_ptfx[1])
                        STOP_PARTICLE_FX_LOOPED(family_other_ptfx[1])
                    ENDIF
                ENDIF
            ENDIF
            
            IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "SON_SMOKING_WEED_IN_A_BONG", "", NULL, family_sfxBank[eSceneMember])
            ENDIF
        BREAK
        CASE FE_M_SON_raids_fridge_for_food
            
            IF PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            ENDIF
            
            OBJECT_INDEX nullObj
            PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], nullObj, eFamilyMember,
                    FD_3_MICHAEL_FRIDGE_L,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "", "_Fridge_L",
                    iFamily_synch_scene[eSceneMember], FALSE)
            PRIVATE_Set_Family_SyncSceneDoor(family_chair[eSceneMember], nullObj, eFamilyMember,
                    FD_4_MICHAEL_FRIDGE_R,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "", "_Fridge_R",
                    iFamily_synch_scene[eSceneMember], FALSE)
            
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M7_SON_jumping_jacks
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
                IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
                    PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                            vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head, SLOW_BLEND_IN, SLOW_BLEND_OUT)
                ENDIF
                
                PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "BREATHING", sSpeechTimer)
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
                
//              Get_SpeechLabel_From_FamilyEvent(eFamilyEvent, iRandCount, iSpeechBit, iPlayerBitset)
//              IF (iRandSpeechCount[eSceneMember] >= iRandCount)
//                  PRIVATE_Update_Family_Hangup(family_ped[eSceneMember], eFamilyMember,
//                          FE_M7_SON_gaming,
//                          "FMMAUD", "", tCreatedMichaelConvLabels,
//                          MyLocalPedStruct, sSpeechTimer, eFamilyEvent)
//              ENDIF
            ENDIF
        BREAK
        CASE FE_M7_SON_on_laptop_looking_for_jobs
            IF (eSceneMember = PS_M_SON)
                IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "PRM7_ROUNDTABLE",      //"FAMILY_ON_LAPTOPS",
                        "", NULL, family_sfxBank[eSceneMember])
                ENDIF
            ENDIF
            
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "_Jimmy", "_Lap_Top",
                    iFamily_synch_scene[eSceneMember])
                    
            IF NOT DOES_ENTITY_EXIST(family_chair[eSceneMember])
                VECTOR vecPos
                MODEL_NAMES ObjectModel
                vecPos = <<-796.655,181.225,71.836>>
                ObjectModel = v_ilev_m_dinechair
                
                IF IS_VALID_INTERIOR(iInteriorForMember[eSceneMember])
                    IF IS_INTERIOR_READY(iInteriorForMember[eSceneMember])
                        family_chair[eSceneMember] = GET_CLOSEST_OBJECT_OF_TYPE(vecPos, 5.0, ObjectModel, TRUE)
                        
                        IF DOES_ENTITY_EXIST(family_chair[eSceneMember])
                            FORCE_ROOM_FOR_ENTITY(family_chair[eSceneMember], iInteriorForMember[eSceneMember], GET_ROOM_KEY_FROM_ENTITY(family_ped[eSceneMember]))
                            SET_ENTITY_COLLISION(family_chair[eSceneMember], FALSE)
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, "NOT DOES_ENTITY_EXIST")
                        ENDIF
                    ELSE
                        CPRINTLN(DEBUG_FAMILY, "NOT IS_INTERIOR_READY")
                    ENDIF
                ELSE
                    CPRINTLN(DEBUG_FAMILY, "NOT IS_VALID_INTERIOR")
                ENDIF
            ELSE
                PRIVATE_Set_Family_SyncSceneProp(family_chair[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "_Jimmy", "_Chair",
                        iFamily_synch_scene[eSceneMember])
            ENDIF
        BREAK
        CASE FE_M2_SON_watching_TV
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M7_SON_watching_TV_with_tracey
            IF (eSceneMember = PS_M_SON)
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[PS_M_SON], TRUE, TRUE)
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ELSE
                PRIVATE_Update_Family_SynchSceneArrayMatch(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[PS_M_SON], tDialogueAnimClips[eSceneMember],
                        tDialogueAnimClips[PS_M_SON], "_JIMMY",
                        iFamily_synch_scene[PS_M_SON])
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        
        CASE FE_M2_DAUGHTER_sunbathing
            IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], vFamily_scene_m_coord, fFamily_scene_m_head)
                    PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
                ENDIF
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
        BREAK
        CASE FE_M7_DAUGHTER_sunbathing
            IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
            //  IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], vFamily_scene_m_coord, fFamily_scene_m_head)
                    PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            //  ENDIF
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_workout_with_mp3
            MAGDEMO_Delete_When_Player_Biking(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
            
            
            IF IS_STRING_NULL_OR_EMPTY("base_face")
                IF (iDAUGHTER_workout_with_mp3_dialogue = 0)
                    IF Play_This_Family_Speech(family_ped[eSceneMember],
                            g_eCurrentFamilyEvent[eFamilyMember],
                            MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels,
                            sSpeechTimer, iRandSpeechCount[eSceneMember],
                            5.300, "TRA_IG_MD", "base_face")
                        //TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), family_ped[eSceneMember] 0500)
                        iDAUGHTER_workout_with_mp3_dialogue = 2
                    ENDIF
                ENDIF
            ELSE
                
                SWITCH iDAUGHTER_workout_with_mp3_dialogue
                    CASE 0
                        #IF IS_DEBUG_BUILD
                        DrawDebugFamilySphere(GET_ENTITY_COORDS(family_ped[eSceneMember]), 5.300, HUD_COLOUR_PINK, 0.1)
                        #ENDIF
                        
                        IF Play_This_Family_Speech(family_ped[eSceneMember],
                                g_eCurrentFamilyEvent[eFamilyMember],
                                MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels,
                                sSpeechTimer, iRandSpeechCount[eSceneMember],
                                5.300+2.0, "TRA_IG_MD", "base_face")
                            CANCEL_TIMER(sDAUGHTER_special_case_timer)
                            
                            REQUEST_ANIM_DICT("timetable@tracy@ig_5@base")
                            iDAUGHTER_workout_with_mp3_dialogue = 1
                        ENDIF
                    BREAK
                    CASE 1
                        
                        IF NOT HAS_ANIM_DICT_LOADED("timetable@tracy@ig_5@base")
                            
                            #IF IS_DEBUG_BUILD
                            CPRINTLN(DEBUG_FAMILY, "NOT HAS_ANIM_DICT_LOADED(\"", "timetable@tracy@ig_5@base", "\")")
                            #ENDIF
                            
                            REQUEST_ANIM_DICT("timetable@tracy@ig_5@base")
                        ELIF (GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(family_ped[eSceneMember]), FALSE) > 5.300)
                            #IF IS_DEBUG_BUILD
                            DrawDebugFamilySphere(GET_ENTITY_COORDS(family_ped[eSceneMember]), 5.300, HUD_COLOUR_PINK, 0.1)
                            #ENDIF
//                      ELIF NOT IS_ENTITY_ON_SCREEN(family_ped[eSceneMember])
                        ELIF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(family_ped[eSceneMember]), 0.5)
                        

                            #IF IS_DEBUG_BUILD
                            CPRINTLN(DEBUG_FAMILY, "NOT IS_SPHERE_VISIBLE(\"", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), "\")")
                            #ENDIF
                            
                            #IF IS_DEBUG_BUILD
                            DrawDebugFamilySphere(GET_ENTITY_COORDS(family_ped[eSceneMember]), 5.300, HUD_COLOUR_PURPLE, 0.2)
                            #ENDIF
                        ELIF NOT IS_TIMER_STARTED(sDAUGHTER_special_case_timer)
                            RESTART_TIMER_NOW(sDAUGHTER_special_case_timer)
                        ELIF NOT TIMER_DO_WHEN_READY(sDAUGHTER_special_case_timer, 0.5)
                            
                            #IF IS_DEBUG_BUILD
                            CPRINTLN(DEBUG_FAMILY, "sDAUGHTER_special_case_timer:(", GET_TIMER_IN_SECONDS(sDAUGHTER_special_case_timer), ")")
                            #ENDIF
                            
                        ELSE
                            //unpause "exit" dialogue
                            PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
                            
                            //TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), PedIndex 4000)
                            TASK_PLAY_ANIM(family_ped[eSceneMember], "timetable@tracy@ig_5@base", "base_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
                            
                            
                            #IF IS_DEBUG_BUILD
                            CPRINTLN(DEBUG_FAMILY, "TASK_PLAY_ANIM(\"", "timetable@tracy@ig_5@base", "\" \"", "base_face", "\" AF_SECONDARY)")
                            #ENDIF
                            
                            CANCEL_TIMER(sDAUGHTER_special_case_timer)
                            RESTART_TIMER_NOW(sSpeechTimer)
                            iDAUGHTER_workout_with_mp3_dialogue = 2
                        ENDIF
                        
                    BREAK
                    CASE 2
                        //
                    BREAK
                    
                    DEFAULT
                        CASSERTLN(DEBUG_FAMILY, "invalid iDAUGHTER_workout_with_mp3_dialogue")
                    BREAK
                ENDSWITCH
            ENDIF
            
            IF NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iFamily_navmeshBlock[eSceneMember])
                iFamily_navmeshBlock[eSceneMember] = ADD_NAVMESH_BLOCKING_OBJECT(GET_ENTITY_COORDS(family_ped[eSceneMember]), <<2,2,2>>, 0)
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_Going_out_in_her_car
            PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember,
                    family_veh[PS_MV_DAUGHTER_issi], FM_MICHAEL_DAUGHTER,
                    eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember],
                    AUTODOOR_MICHAEL_MANSION_GATE,
                    "FMMAUD", "FMM_1_2", "", tCreatedMichaelConvLabels, -1.0 ,
                    MyLocalPedStruct, sSpeechTimer, "GENERIC_INSULT_MED"
                    #IF USE_TU_CHANGES
                    , iInteriorForPlayer
                    #ENDIF
                    )
        BREAK
        CASE FE_M_DAUGHTER_walks_to_room_music
            PRIVATE_Update_Family_Walking(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M_DAUGHTER_dancing_practice
            MAGDEMO_Delete_When_Player_Biking(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "DAUGHTER_MD_DANCING_PRACTICE", "", NULL, family_sfxBank[eSceneMember])
            
            PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "EXERCISING", sSpeechTimer)
            
            IF NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iFamily_navmeshBlock[eSceneMember])
                iFamily_navmeshBlock[eSceneMember] = ADD_NAVMESH_BLOCKING_OBJECT(GET_ENTITY_COORDS(family_ped[eSceneMember]), <<2,2,2>>, 0)
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_purges_in_the_bathroom
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)

                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                            eFamilyMember, eFamilyEvent,
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            vFamily_scene_m_coord, fFamily_scene_m_head,
                            iFamily_synch_scene[eSceneMember], TRUE, TRUE)
                
                TEXT_LABEL_63 tDialogueAnimFacial
                tDialogueAnimFacial  = tDialogueAnimClips[eSceneMember]
                tDialogueAnimFacial += "_face"
                PRIVATE_Update_Family_Facial(family_ped[eSceneMember], tDialogueAnimDicts[eSceneMember], tDialogueAnimFacial)
                
                PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
                PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "FAMILY_SOUNDS_02", "", NULL, family_sfxBank[eSceneMember])
                
                IF NOT IS_ANY_SPEECH_PLAYING(family_ped[eSceneMember])
                    IF PRIVATE_Update_Family_AnimPtfx(family_ped[eSceneMember], eFamilyEvent,
                            "scr_tracey_puke", family_ptfx[eSceneMember], family_b_ptfx[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            iFamily_synch_scene[eSceneMember], "exhale", 5, FALSE)

                        IF family_sfxID[eSceneMember] = -1
							RELEASE_SOUND_ID(family_sfxID[eSceneMember])
	                        family_sfxID[eSceneMember] = GET_SOUND_ID()
	                        PLAY_SOUND_FROM_ENTITY(family_sfxID[eSceneMember], "MICHAELS_HOUSE_DAUGHTER_SICK_ONESHOT", family_ped[eSceneMember])
							
							CPRINTLN(DEBUG_FAMILY, "PLAY_SOUND_FROM_ENTITY: ", family_sfxID[eSceneMember], " [fam stage :", family_sfxStage[eSceneMember], "]")
						ELSE
							CPRINTLN(DEBUG_FAMILY, "playing_sound_from_entity: ", family_sfxID[eSceneMember], " [fam stage :", family_sfxStage[eSceneMember], "]")
                        ENDIF
						
                    ELSE
                        IF HAS_SOUND_FINISHED(family_sfxID[eSceneMember])
                            STOP_SOUND(family_sfxID[eSceneMember])
                            RELEASE_SOUND_ID(family_sfxID[eSceneMember]) //Added by Steve T by request of George W. in bug 2488579
                            family_sfxID[eSceneMember] = -1

                        ENDIF
                    ENDIF
                ELSE
                    IF HAS_SOUND_FINISHED(family_sfxID[eSceneMember])
                        STOP_SOUND(family_sfxID[eSceneMember])
                        RELEASE_SOUND_ID(family_sfxID[eSceneMember]) //Added by Steve T by request of George W. in bug 2488579
                        family_sfxID[eSceneMember] = -1
                    ENDIF
                ENDIF
                
                IF NOT bRespondedToTraceyPurging
                    IF (iRandSpeechCount[eSceneMember] > 0)
                        
                        IF Is_Ped_Playing_Family_Speech("FMMAUD", "TRA_MICRE",
                                    MyLocalPedStruct, CONV_PRIORITY_AMBIENT_MEDIUM, tCreatedMichaelConvLabels)
                            RESTART_TIMER_NOW(sSpeechTimer)
                            bRespondedToTraceyPurging = TRUE
                        ENDIF
                    ENDIF
                ENDIF
                
                bUpdateInteriorForPlayer = TRUE
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_shower
            PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head)
//          PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
//                  "FAMILY_SOUNDS_01", "MICHAELS_HOUSE_DAUGHTER_SHOWER", family_ped[eSceneMember])
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "DAUGHTER_SHOWER", "AFT_DAUGHTER_SHOWER_MASTER", family_ped[eSceneMember], family_sfxBank[eSceneMember])
        BREAK
        CASE FE_M_DAUGHTER_watching_TV_sober
//          IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
//              PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                      tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                      vFamily_scene_m_coord, fFamily_scene_m_head)
//          ENDIF
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
            
            IF (eSceneMember = PS_M_DAUGHTER)
                
                IF NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sDaughterFakeCellphoneData)
                    REQUEST_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
                ELSE
                    DRAW_FAKE_CELLPHONE_SCREEN(sDaughterFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
                ENDIF
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_watching_TV_drunk
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, FALSE)
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M_DAUGHTER_screaming_at_dad
            BOOL bSetDaughterScreamingAtDadSyncSceneDoor
            bSetDaughterScreamingAtDadSyncSceneDoor = TRUE
            
            IF NOT PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_DAUGHTER],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    FE_M_DAUGHTER_sex_sounds_from_room,
                    "FMMAUD", "", "TRA_IG_4", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            ENDIF
            
            IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                IF NOT IS_PED_INJURED(family_extra_ped[eSceneMember])
                    PRIVATE_Update_Family_SynchSceneMatch(family_extra_ped[eSceneMember], //eFamilyMember, eFamilyEvent,
                            iFamily_synch_scene[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "TRACY", "BOY")
                    
                ENDIF
                IF bSetDaughterScreamingAtDadSyncSceneDoor
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_1_MICHAEL_DAUGHTER,
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "TRACY", "DOOR", iFamily_synch_scene[PS_M_DAUGHTER], TRUE)
                            
                    IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[PS_M_DAUGHTER])
                        //
//                      RETURN FALSE
                    ELSE
                        IF IS_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[PS_M_DAUGHTER])
                            //
                        ELSE
//                          IF NOT bDeleteDoor
//                              //
//                          ELSE
                                //
                                IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[PS_M_DAUGHTER]) >= 0.99 
                                    
                                    MODEL_NAMES ObjectModel //= DUMMY_MODEL_FOR_SCRIPT
                                    VECTOR vecPos //= <<0,0,0>>
                                    DOOR_NAME_ENUM eDoorName //= DUMMY_DOORNAME
                                    DOOR_HASH_ENUM eDoorHash //= DUMMY_DOORHASH
                                    TEXT_LABEL_63 roomName
                                    
                                    BOOL bIgnoreInRoomCheck //= FALSE
                                    
                                    PRIVATE_Get_Family_Door_Attributes(FD_1_MICHAEL_DAUGHTER,
                                            ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                                    
                                    IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
                                        ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(eDoorHash), ObjectModel, vecPos)
                                    ENDIF
                                    
                                    IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(eDoorHash))
                                        DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(eDoorHash), DOORSTATE_FORCE_LOCKED_THIS_FRAME)
                                        DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(eDoorHash), 0.0)
                                    ENDIF
                                ENDIF
//                          ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_DAUGHTER_sex_sounds_from_room)
                RESTART_TIMER_NOW(sDAUGHTER_special_case_timer)
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_sniffs_drugs_in_toilet
            PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "FAMILY_SOUNDS_02", "MICHAELS_HOUSE_DAUGHTER_SNIFF_DRUGS", family_ped[eSceneMember], family_sfxBank[eSceneMember])
            
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M_DAUGHTER_sex_sounds_from_room
            PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            
            IF NOT IS_TIMER_STARTED(sDAUGHTER_special_case_timer)
            OR TIMER_DO_WHEN_READY(sDAUGHTER_special_case_timer, 5.0)
                PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "DAUGHTER_SEX_SOUNDS_FROM_ROOM", "AFT_DAUGHTER_SEX_vb", family_ped[eSceneMember], family_sfxBank[eSceneMember])
            ENDIF
            
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M_DAUGHTER_crying_over_a_guy
            IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
                
                FLOAT fBlendin
                fBlendin = NORMAL_BLEND_IN
                IF NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimDicts[eSceneMember])
                AND NOT IS_STRING_NULL_OR_EMPTY(tDialogueAnimClips[eSceneMember])
                    fBlendin = REALLY_SLOW_BLEND_IN
                ENDIF
                
                PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                        fBlendin, NORMAL_BLEND_OUT)
            ENDIF
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "FAMILY_SOUNDS_01", "MICHAELS_HOUSE_DAUGHTER_CRY", family_ped[eSceneMember], family_sfxBank[eSceneMember])
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M_DAUGHTER_Coming_home_drunk
            IF NOT PRIVATE_Update_Family_AskForCash(GET_CURRENT_PLAYER_PED_ENUM(),
                    family_ped[eSceneMember], eFamilyMember, eFamilyEvent, BIT_MICHAEL,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    intention_index, "FMMAUD", "TRA_IG_14", tCreatedMichaelConvLabels,
                    iFamily_synch_scene[eSceneMember],
                    MyLocalPedStruct, sSpeechTimer, DEFAULT, P_BankNote_S)
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[eSceneMember], TRUE, FALSE)
            ENDIF
        BREAK
        CASE FE_M_DAUGHTER_sleeping
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            //PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
            
            PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "SLEEPING", sSpeechTimer)
        BREAK
        CASE FE_M_DAUGHTER_couchsleep
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            //PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
            
            PRIVATE_Update_Family_Context_Speech(family_ped[eSceneMember], "SLEEPING", sSpeechTimer)
        BREAK
        CASE FE_M_DAUGHTER_on_phone_to_friends
        CASE FE_M_DAUGHTER_on_phone_LOCKED
        CASE FE_M7_DAUGHTER_studying_on_phone
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
                IF NOT IS_ANY_SPEECH_PLAYING(family_ped[eSceneMember])
                    PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                            vFamily_scene_m_coord, fFamily_scene_m_head,
                            iFamily_synch_scene[eSceneMember],
                            TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
                ELSE
                    PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                            eFamilyMember, eFamilyEvent,
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            vFamily_scene_m_coord, fFamily_scene_m_head,
                            iFamily_synch_scene[eSceneMember], TRUE, FALSE)
                ENDIF
                PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            ENDIF
            
            IF (eSceneMember = PS_M_DAUGHTER)
                
                IF NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sDaughterFakeCellphoneData)
                    REQUEST_FAKE_CELLPHONE_MOVIE(sDaughterFakeCellphoneData)
                ELSE
                    DRAW_FAKE_CELLPHONE_SCREEN(sDaughterFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_MENU)
                ENDIF
            ENDIF
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_DAUGHTER_on_phone_LOCKED)
                PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            ENDIF
        BREAK
        CASE FE_M7_DAUGHTER_studying_does_nails
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            ENDIF
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "DAUGHTER_STUDYING_DOES_NAILS", "", NULL, family_sfxBank[eSceneMember])
        BREAK
        
        CASE FE_M_WIFE_screams_at_mexmaid
            enumFamilyEvents eDesiredFamilyEvent_amaScreamMaid
            eDesiredFamilyEvent_amaScreamMaid = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_WIFE)
                eDesiredFamilyEvent_amaScreamMaid = FE_ANY_wander_family_event
            ELIF (eSceneMember = PS_M_MEXMAID)
                eDesiredFamilyEvent_amaScreamMaid = FE_M2_MEXMAID_clean_surface_b
    
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "", "_SCOURER",
                        iFamily_synch_scene[eSceneMember])
                        
                        
                IF (iFamily_enter_veh_stage[eSceneMember] = 1)
                    FORCE_ROOM_FOR_ENTITY(family_ped[eSceneMember], iInteriorForMember[eSceneMember], GET_HASH_KEY("V_Michael_G_Kitche"))
                ENDIF
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_WIFE],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_amaScreamMaid,
                    "FMMAUD", "", "AM_IG_9", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
            
            IF (iFamily_enter_veh_stage[eSceneMember] < 1)
                IF (eSceneMember = PS_M_MEXMAID)
                    Play_This_Family_Speech(family_ped[eSceneMember],
                            g_eCurrentFamilyEvent[eFamilyMember],
                            MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels,
                            sSpeechTimer, iRandSpeechCount[eSceneMember],
                            DEFAULT, "MA_SHOUT")
                ENDIF
            ENDIF
        BREAK
        CASE FE_M2_WIFE_in_face_mask
        CASE FE_M7_WIFE_in_face_mask
            IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head)
                PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            ENDIF
            
            PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M_WIFE_playing_tennis
            Control_Michael_FamilyEvent_Tasks(eSceneMember, FAMILY_MEMBER_BUSY)
        BREAK
        CASE FE_M2_WIFE_doing_yoga
        CASE FE_M7_WIFE_doing_yoga
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)

                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[eSceneMember], TRUE, FALSE)
            ENDIF
        BREAK
//      CASE FE_M_WIFE_getting_nails_done
//          PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//      BREAK
        CASE FE_M_WIFE_leaving_in_car
            PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember,
                    family_veh[PS_MV_WIFE_sentinal], FM_MICHAEL_WIFE,
                    eFamilyEvent, iFamily_enter_veh_stage[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember],
                    AUTODOOR_MICHAEL_MANSION_GATE,
                    "FMMAUD", "AM_IG_MD", "AM_IG_MD2", tCreatedMichaelConvLabels, 0.8,
                    MyLocalPedStruct, sSpeechTimer, "DISMISS_MICHAEL"
                    #IF USE_TU_CHANGES
                    , iInteriorForPlayer
                    #ENDIF
                    )
        BREAK
//      CASE FE_M_WIFE_leaving_in_car_v2
//          PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember
//                  family_veh[PS_MV_WIFE] FM_MICHAEL_WIFE
//                  eFamilyEvent iFamily_enter_veh_stage[eSceneMember]
//                  vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember]
//                  AUTODOOR_MICHAEL_MANSION_GATE
//                  "FMMAUD", "AM_IG_MD", "AM_IG_MD2" 0.8
//                  MyLocalPedStruct, sSpeechTimer)
//      BREAK
//      CASE FE_M_WIFE_MD_leaving_in_car_v3
//          MAGDEMO_Delete_When_Player_Biking(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//          PRIVATE_Update_Family_DrivingAway(family_ped[eSceneMember], eFamilyMember
//                  family_veh[PS_MV_WIFE] FM_MICHAEL_WIFE
//                  eFamilyEvent iFamily_enter_veh_stage[eSceneMember]
//                  vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember]
//                  AUTODOOR_MICHAEL_MANSION_GATE
//                  "FMMAUD", "AM_IG_MD", "AM_IG_MD2" 0.8
//                  MyLocalPedStruct, sSpeechTimer)
//      BREAK
        
        CASE FE_M2_WIFE_with_shopping_bags_enter
        CASE FE_M7_WIFE_with_shopping_bags_enter
            STRING strShopEnter, strShopIdle
            strShopEnter = ""
            strShopIdle = ""
            IF (eFamilyEvent = FE_M2_WIFE_with_shopping_bags_enter)
                strShopEnter = "AM_IG_7"
                strShopIdle = "AM_IG_7"
            ELIF (eFamilyEvent = FE_M7_WIFE_with_shopping_bags_enter)
                strShopEnter = "AMr_IG_7"
                strShopIdle = "AMr_IG_7"
            ENDIF
            
            PRIVATE_Update_Family_ComingHome(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember],
                    iFamily_synch_scene[eSceneMember], tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    FE_ANY_find_family_event,
                    iInteriorForSafehouse, "v_michael_g_front",
                    "FMMAUD", strShopEnter, strShopIdle, tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer, iRandSpeechCount[eSceneMember])
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "WIFE_WITH_SHOPPING_BAGS_ENTER", "", NULL, family_sfxBank[eSceneMember])
                    
            IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
            AND ARE_STRINGS_EQUAL(tDialogueAnimClips[eSceneMember], "IG_7_ENTER")
                
                CPRINTLN(DEBUG_FAMILY, "stage: ", iFamily_enter_veh_stage[eSceneMember], ", phase: ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]))
                
                IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) <= 0.45
                OR (iFamily_enter_veh_stage[eSceneMember] <= 1)
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[PS_M_WIFE], family_chair[PS_M_WIFE], eFamilyMember,
                            FD_8_MICHAEL_FRONTDOOR_R,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "", "_Door_R",
                            iFamily_synch_scene[PS_M_WIFE], TRUE)
                ELSE
                    
                    IF iFamily_enter_veh_stage[eSceneMember] = 2
                        
                        IF DOES_ENTITY_EXIST(family_door[PS_M_WIFE])
                            CPRINTLN(DEBUG_FAMILY, "family_door[PS_M_WIFE] exists")
                            
                            
                            MODEL_NAMES ObjectModel
                            VECTOR vecPos
                            DOOR_NAME_ENUM eDoorName
                            DOOR_HASH_ENUM eDoorHash
                            TEXT_LABEL_63 roomName
                            BOOL bIgnoreInRoomCheck
                            
                            ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                            vecPos = <<0,0,0>>
                            eDoorName = DUMMY_DOORNAME
                            eDoorHash = DUMMY_DOORHASH
                            bIgnoreInRoomCheck = FALSE
                            
                            PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                                    ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                            
                            REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                            
                            DELETE_OBJECT(family_door[PS_M_WIFE])
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, "family_door[PS_M_WIFE] doesnt exists")
                        ENDIF
                        
                        
                    ENDIF
                    
                    IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_M_MANSION_F_R))
                        //
                    ELSE
                        IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R)) = DOORSTATE_FORCE_LOCKED_THIS_FRAME
                            DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_UNLOCKED)
                            
                            CPRINTLN(DEBUG_FAMILY, "unlock DOORHASH_M_MANSION_F_R")
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
            IF ARE_STRINGS_EQUAL(tDialogueAnimClips[eSceneMember], "IG_7_ENTER")
                IF (GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[PS_M_WIFE]) < 0.95)
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "", "_Bag_L",
                            iFamily_synch_scene[PS_M_WIFE])
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "", "_Bag_R",
                            iFamily_synch_scene[PS_M_WIFE])
                ELSE
                    #IF IS_DEBUG_BUILD
                    IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                        DrawDebugFamilyDoorTextWithOffset(NO_FAMILY_DOORS, "family_prop_l taskless", GET_ENTITY_COORDS(family_prop_l[eSceneMember], FALSE), 0, HUD_COLOUR_ORANGE, 1)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                        DrawDebugFamilyDoorTextWithOffset(NO_FAMILY_DOORS, "family_prop_r taskless", GET_ENTITY_COORDS(family_prop_r[eSceneMember], FALSE), 0, HUD_COLOUR_ORANGE, 1)
                    ENDIF
                    #ENDIF
                ENDIF
            ELSE
                #IF IS_DEBUG_BUILD
                IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                    DrawDebugFamilyDoorTextWithOffset(NO_FAMILY_DOORS, "family_prop_l exists", GET_ENTITY_COORDS(family_prop_l[eSceneMember], FALSE), 0, HUD_COLOUR_ORANGE, 1)
                ENDIF
                IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                    DrawDebugFamilyDoorTextWithOffset(NO_FAMILY_DOORS, "family_prop_r exists", GET_ENTITY_COORDS(family_prop_r[eSceneMember], FALSE), 0, HUD_COLOUR_ORANGE, 1)
                ENDIF
                #ENDIF
            ENDIF
        BREAK
        
        CASE FE_M_WIFE_gets_drink_in_kitchen
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
                
                IF eFamilyPropLModel[eSceneMember] <> P_TUMBLER_01_S
                OR eFamilyPropRModel[eSceneMember] <> P_WHISKEY_BOTTLE_S
                    IF eFamilyPropLModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                    OR eFamilyPropRModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                        CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Prop_Variables [",
                                Get_String_From_FamilyEvent(eFamilyEvent), ", MODEL_ID_",
                                ENUM_TO_INT(eFamilyPropLModel[eSceneMember]), ", MODEL_ID_",
                                ENUM_TO_INT(eFamilyPropRModel[eSceneMember]), "]")
                    ELSE
                        CPRINTLN(DEBUG_FAMILY, "Initialise_Michael_Family_Prop_Variables [",
                                Get_String_From_FamilyEvent(eFamilyEvent), ", ",
                                GET_MODEL_NAME_FOR_DEBUG(eFamilyPropLModel[eSceneMember]), ", ",
                                GET_MODEL_NAME_FOR_DEBUG(eFamilyPropRModel[eSceneMember]), "]")
                    ENDIF
                    
                    Initialise_Michael_Family_Prop_Variables(eSceneMember)
                ELSE
                    IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                    AND GET_ENTITY_MODEL(family_prop_l[eSceneMember]) != eFamilyPropLModel[eSceneMember]
                        //
                        CPRINTLN(DEBUG_FAMILY, "DELETE_OBJECT [",
                                Get_String_From_FamilyEvent(eFamilyEvent), ", ",
                                GET_ENTITY_MODEL(family_prop_l[eSceneMember]), "]")
                        DELETE_OBJECT(family_prop_l[eSceneMember])
                        eFamilyPropLModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                    AND GET_ENTITY_MODEL(family_prop_r[eSceneMember]) != eFamilyPropRModel[eSceneMember]
                        //
                        CPRINTLN(DEBUG_FAMILY, "DELETE_OBJECT [",
                                Get_String_From_FamilyEvent(eFamilyEvent), ", ",
                                GET_ENTITY_MODEL(family_prop_r[eSceneMember]), "]")
                        DELETE_OBJECT(family_prop_r[eSceneMember])
                        eFamilyPropRModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                    ENDIF
                ENDIF
                
                IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "WIFE_GETS_DRUNK_IN_KITCHEN", "", NULL, family_sfxBank[eSceneMember])
                ENDIF
                
                PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                        eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord, fFamily_scene_m_head,
                        iFamily_synch_scene[eSceneMember], TRUE, FALSE)
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "", "_glass",
                        iFamily_synch_scene[eSceneMember])
                PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "", "_bottle",
                        iFamily_synch_scene[eSceneMember])
            ENDIF
        BREAK
        CASE FE_M2_WIFE_sunbathing
        CASE FE_M7_WIFE_sunbathing
            IF PRIVATE_Update_Family_GotoCoord(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember], iFamily_synch_scene[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember], bMove_peds)
            
                IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember], bUpdate_chair_attach)
                    IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            vFamily_scene_m_coord, fFamily_scene_m_head)
                        PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
                    ENDIF
                    PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
                    
                    IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                        IF IS_ENTITY_ATTACHED(family_prop_l[eSceneMember])
                            CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(family_prop_r[eSceneMember])")
                            DETACH_ENTITY(family_prop_l[eSceneMember])
                        ENDIF
                        IF NOT IS_ENTITY_DEAD(family_prop_l[eSceneMember])
                        AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_prop_l[eSceneMember])
                            CASSERTLN(DEBUG_FAMILY, "DELETE_OBJECT(family_prop_l[eSceneMember])")
                            DELETE_OBJECT(family_prop_l[eSceneMember])
                        ENDIF
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[eSceneMember])
                        IF IS_ENTITY_ATTACHED(family_prop_r[eSceneMember])
                            CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(family_prop_r[eSceneMember])")
                            DETACH_ENTITY(family_prop_r[eSceneMember])
                        ENDIF
                        IF NOT IS_ENTITY_DEAD(family_prop_r[eSceneMember])
                        AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(family_prop_r[eSceneMember])
                            CASSERTLN(DEBUG_FAMILY, "DELETE_OBJECT(family_prop_r[eSceneMember])")
                            DELETE_OBJECT(family_prop_r[eSceneMember])
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        BREAK
//      CASE FE_M_WIFE_getting_botox_done
//          PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//      BREAK
        CASE FE_M_WIFE_passed_out_BED
        CASE FE_M2_WIFE_passed_out_SOFA
        CASE FE_M7_WIFE_passed_out_SOFA
            IF NOT PRIVATE_Update_Family_DialogueAnim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
                PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            ENDIF
            PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
        BREAK
//      CASE FE_M_WIFE_screaming_at_son_P1
//          BOOL bSonFakesScenario
//          bSonFakesScenario = FALSE
//          
//          enumFamilyEvents eDesiredFamilyEvent_amaFightJimA
//          eDesiredFamilyEvent_amaFightJimA = NO_FAMILY_EVENTS
//          IF (eSceneMember = PS_M_WIFE)
//              eDesiredFamilyEvent_amaFightJimA = FE_M_WIFE_gets_drink_in_kitchen
//              
//              IF iFamily_enter_veh_stage[eSceneMember] = 0
//              AND iFamily_enter_veh_stage[PS_M_SON] > 0
//                  iFamily_enter_veh_stage[eSceneMember] = iFamily_enter_veh_stage[PS_M_SON]
//              ENDIF
//              
//          ELIF (eSceneMember = PS_M_SON)
//              eDesiredFamilyEvent_amaFightJimA = FE_M_SON_phone_calls_in_room
//              
//              IF iFamily_enter_veh_stage[eSceneMember] = 0
//                  IF iFamily_enter_veh_stage[PS_M_WIFE] = 0
//                      bSonFakesScenario = TRUE
//                  ELSE
//                      iFamily_enter_veh_stage[eSceneMember] = iFamily_enter_veh_stage[PS_M_WIFE]
//                  ENDIF
//              ENDIF
//          ENDIF
//          
//          IF bSonFakesScenario
//              
//              
//              CPRINTLN(DEBUG_FAMILY, "bSonFakesScenario son:", iFamily_synch_scene[PS_M_SON], " wife: ", iFamily_synch_scene[PS_M_WIFE])
//              
//              PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                      vFamily_scene_m_coord, fFamily_scene_m_head,
//                      iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
//                      tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                      eDesiredFamilyEvent_amaFightJimA,
//                      "FMMAUD", "", "AM_IG_2a", tCreatedMichaelConvLabels,
//                      MyLocalPedStruct, sSpeechTimer)
//          ELSE
//              
//              
//              CPRINTLN(DEBUG_FAMILY, "NOT bSonFakesScenario son:", iFamily_synch_scene[PS_M_SON], " wife: ", iFamily_synch_scene[PS_M_WIFE])
//              
//              PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                      vFamily_scene_m_coord, fFamily_scene_m_head,
//                      iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_WIFE],
//                      tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                      eDesiredFamilyEvent_amaFightJimA,
//                      "FMMAUD", "", "AM_IG_2a", tCreatedMichaelConvLabels,
//                      MyLocalPedStruct, sSpeechTimer)
//          ENDIF
//          
//          IF (eSceneMember = PS_M_WIFE)
//          AND iFamily_enter_veh_stage[PS_M_SON] > 0
//              PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
//                      FD_0_MICHAEL_SON,
//                      tDialogueAnimDicts[eSceneMember],
//                      tDialogueAnimClips[eSceneMember], "AMANDA", "DOOR",
//                      iFamily_synch_scene[PS_M_WIFE], TRUE)
//          ENDIF
//      BREAK
        CASE FE_M_WIFE_screaming_at_son_P2
            enumFamilyEvents eDesiredFamilyEvent_amaFightJimB
            eDesiredFamilyEvent_amaFightJimB = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_WIFE)
                eDesiredFamilyEvent_amaFightJimB = FE_M_WIFE_gets_drink_in_kitchen
            ELIF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_amaFightJimB = FE_M_SON_phone_calls_in_room
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_amaFightJimB,
                    "FMMAUD", "", "AM_IG_2b", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
                    
            IF (eSceneMember = PS_M_SON)
                PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                        FD_0_MICHAEL_SON,
                        tDialogueAnimDicts[eSceneMember],
                        tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR",
                        iFamily_synch_scene[PS_M_SON], TRUE)
            ENDIF
        BREAK
        CASE FE_M_WIFE_screaming_at_son_P3
            enumFamilyEvents eDesiredFamilyEvent_amaFightJimC
            eDesiredFamilyEvent_amaFightJimC = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_WIFE)
                eDesiredFamilyEvent_amaFightJimC = FE_M_WIFE_gets_drink_in_kitchen
            ELIF (eSceneMember = PS_M_SON)
                eDesiredFamilyEvent_amaFightJimC = FE_M_SON_phone_calls_in_room
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_SON],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_amaFightJimC,
                    "FMMAUD", "", "AM_IG_2c", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
                    
            IF (eSceneMember = PS_M_SON)
                PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                        FD_0_MICHAEL_SON,
                        tDialogueAnimDicts[eSceneMember],
                        tDialogueAnimClips[eSceneMember], "JIMMY", "DOOR",
                        iFamily_synch_scene[PS_M_SON], TRUE)
            ENDIF
        BREAK
        CASE FE_M_WIFE_screaming_at_daughter
            enumFamilyEvents eDesiredFamilyEvent_amaFightTra
            eDesiredFamilyEvent_amaFightTra = NO_FAMILY_EVENTS
            IF (eSceneMember = PS_M_WIFE)
                eDesiredFamilyEvent_amaFightTra = NO_FAMILY_EVENTS
            ELIF (eSceneMember = PS_M_DAUGHTER)
                eDesiredFamilyEvent_amaFightTra = FE_M_DAUGHTER_on_phone_LOCKED
            ENDIF
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[PS_M_DAUGHTER],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    eDesiredFamilyEvent_amaFightTra,
                    "FMMAUD", "", "AM_IG_3", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
                    
            IF (eSceneMember = PS_M_DAUGHTER)
                PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                        FD_1_MICHAEL_DAUGHTER,
                        tDialogueAnimDicts[eSceneMember],
                        tDialogueAnimClips[eSceneMember], "TRACY", "DOOR",
                        iFamily_synch_scene[PS_M_DAUGHTER], TRUE)
            ENDIF
            
            IF eSceneMember = PS_M_WIFE
                IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                    TASK_FOLLOW_NAV_MESH_TO_COORD(family_ped[eSceneMember],
                            <<-800.7849, 182.5544, 73.6800>>,
                            PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS)
                ENDIF
            ELSE
            ENDIF
        BREAK
        CASE FE_M2_WIFE_phones_man_OR_therapist
        CASE FE_M7_WIFE_phones_man_OR_therapist
            STRING strPhone
            strPhone = ""
            IF (eFamilyEvent = FE_M2_WIFE_phones_man_OR_therapist)
                strPhone = "AM_IG_11"
            ELIF (eFamilyEvent = FE_M7_WIFE_phones_man_OR_therapist)
                strPhone = "AMr_IG_11"
            ENDIF
            
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            PRIVATE_Update_Family_Hangup(family_ped[eSceneMember], eFamilyMember,
                    FE_M_WIFE_hangs_up_and_wanders,
                    "FMMAUD", strPhone, tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer, eFamilyEvent)
            
            eFamilyEvent = g_eCurrentFamilyEvent[eFamilyMember]
        BREAK
        CASE FE_M_WIFE_hangs_up_and_wanders
            PRIVATE_Update_Family_Hangup_And_Wander(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    iFamily_enter_veh_stage[eSceneMember],
                    family_prop_l[eSceneMember], eFamilyPropLModel[eSceneMember], TRUE)
        BREAK
        #IF NOT IS_JAPANESE_BUILD
        CASE FE_M2_WIFE_using_vibrator
        CASE FE_M7_WIFE_using_vibrator
            STRING strVibrator
            strVibrator = ""
            IF (eFamilyEvent = FE_M2_WIFE_using_vibrator)
                strVibrator = "AM_IG_6"
            ELIF (eFamilyEvent = FE_M7_WIFE_using_vibrator)
                strVibrator = "AMr_IG_6"
            ENDIF
            
            CONST_INT iARGUE_3_playExit             3
            
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    FE_M_WIFE_using_vibrator_END,
                    "FMMAUD", "", strVibrator, tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer)
                
            IF (iFamily_enter_veh_stage[eSceneMember] < iARGUE_3_playExit)
                PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                        "WIFE_USING_VIBRATOR", "AFT_WIFE_USING_VIBRATOR_MASTER", family_prop_l[eSceneMember], family_sfxBank[eSceneMember])
            ELSE
                IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) <= 0.6
                    PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                            "WIFE_USING_VIBRATOR", "AFT_WIFE_USING_VIBRATOR_MASTER", family_prop_l[eSceneMember], family_sfxBank[eSceneMember])
                ELSE
                    IF (family_sfxID[eSceneMember] <> iCONST_FAMILY_SFX_INVALID)
                        STOP_SOUND(family_sfxID[eSceneMember])
                        RELEASE_SOUND_ID(family_sfxID[eSceneMember]) //Added by Steve T by request of George W. in bug 2488579
                        family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
                        
                        //CASSERTLN(DEBUG_FAMILY, "STOP_SOUND")
                    ENDIF
                ENDIF
            ENDIF
            IF (iFamily_enter_veh_stage[eSceneMember] < iARGUE_3_playExit)
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "", "_Vibrator",
                        iFamily_synch_scene[eSceneMember])
            ELSE
                IF GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) <= 0.8
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "", "_Vibrator",
                            iFamily_synch_scene[eSceneMember])
                ELSE
                    IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], INSTANT_BLEND_IN, TRUE)
                        DETACH_ENTITY(family_prop_l[eSceneMember])
                        
                    //  APPLY_FORCE_TO_ENTITY(family_prop_l[eSceneMember], APPLY_TYPE_IMPULSE, << 0.0, 0.0, -0.1 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
                        FREEZE_ENTITY_POSITION(family_prop_l[eSceneMember], FALSE)
                        SET_ENTITY_DYNAMIC(family_prop_l[eSceneMember], TRUE)
                
                        SET_OBJECT_AS_NO_LONGER_NEEDED(family_prop_l[eSceneMember])
                        eFamilyPropLModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                        
                        CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(family_prop_l)")
                    ENDIF
                ENDIF
            ENDIF
            
        /*  //
            IF DOES_ENTITY_EXIST(family_prop[eSceneMember])
                
                
                SET_ENTITY_RECORDS_COLLISIONS(family_prop[eSceneMember], TRUE)
                IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(family_prop[eSceneMember])
                    
                    SET_ENTITY_RECORDS_COLLISIONS(family_prop[eSceneMember], FALSE)
                    
                    STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop[eSceneMember], INSTANT_BLEND_IN, TRUE)
                    ACTIVATE_PHYSICS(family_prop[eSceneMember])
                    
                    VECTOR vForceNormal
                    vForceNormal = GET_COLLISION_NORMAL_OF_LAST_HIT_FOR_ENTITY(family_prop[eSceneMember])
                    APPLY_FORCE_TO_ENTITY(family_prop[eSceneMember], APPLY_TYPE_IMPULSE, -vForceNormal, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
                    
                    SET_OBJECT_AS_NO_LONGER_NEEDED(family_prop[eSceneMember])
                    
                    CASSERTLN(DEBUG_FAMILY, "family_prop")
                ENDIF
            ENDIF
            // */
            
            IF (g_eCurrentFamilyEvent[eFamilyMember] = FE_M_WIFE_using_vibrator_END)
                IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                    STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], INSTANT_BLEND_IN, TRUE)
                    DETACH_ENTITY(family_prop_l[eSceneMember])
                    
                //  APPLY_FORCE_TO_ENTITY(family_prop_l[eSceneMember], APPLY_TYPE_IMPULSE, << 0.0, 0.0, -0.1 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, TRUE, FALSE)
                    FREEZE_ENTITY_POSITION(family_prop_l[eSceneMember], FALSE)
                    SET_ENTITY_DYNAMIC(family_prop_l[eSceneMember], TRUE)
            
                    SET_OBJECT_AS_NO_LONGER_NEEDED(family_prop_l[eSceneMember])
                    
                    CPRINTLN(DEBUG_FAMILY, "DETACH_ENTITY(family_prop_l)")
                ENDIF
                IF (family_sfxID[eSceneMember] <> iCONST_FAMILY_SFX_INVALID)
                    #IF IS_DEBUG_BUILD
                    CPRINTLN(DEBUG_FAMILY, " iSfxID = ")
                    CPRINTLN(DEBUG_FAMILY, family_sfxID[eSceneMember])
                    #ENDIF
                    
                    STOP_SOUND(family_sfxID[eSceneMember])
                    RELEASE_SOUND_ID(family_sfxID[eSceneMember]) //Added by Steve T by request of George W. in bug 2488579
                    family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
                    
                    //CASSERTLN(DEBUG_FAMILY, "STOP_SOUND")
                ENDIF
            ENDIF
            
            bUpdateInteriorForPlayer = TRUE
        BREAK
        CASE FE_M_WIFE_using_vibrator_END
            
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
        BREAK
        #ENDIF
        CASE FE_M2_WIFE_sleeping
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
            //PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
        BREAK
        CASE FE_M7_WIFE_sleeping
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
            //PRIVATE_Update_Family_Sleeping(family_ped[eSceneMember])
        BREAK
        CASE FE_M7_WIFE_Making_juice
            PRIVATE_Update_Family_Argue(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_synch_scene[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    FE_ANY_wander_family_event,
                    "FMMAUD", "", "FAMR_IG_6", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer, 0.4)
            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "_amanda", "_glass",
                    iFamily_synch_scene[eSceneMember])
            
            IF iFamily_enter_veh_stage[eSceneMember] > 0
                REQUEST_MODEL(PROP_WHEAT_GRASS_HALF)
                IF HAS_MODEL_LOADED(PROP_WHEAT_GRASS_HALF)
                AND ARE_STRINGS_EQUAL(tDialogueAnimClips[eSceneMember], "Ugh_kale_Amanda")
                
                    //
                    
                    IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
                    AND GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]) >= 0.99
                            
                        //
                        
                        IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                        AND eFamilyPropLModel[eSceneMember] = P_W_GRASS_GLS_S
                            
                            VECTOR VecNewCoors, VecNewRot
                            VecNewCoors = GET_ENTITY_COORDS(family_prop_l[eSceneMember])
                            VecNewRot = GET_ENTITY_ROTATION(family_prop_l[eSceneMember])
                            DELETE_OBJECT(family_prop_l[eSceneMember])
                            family_prop_l[eSceneMember] = CREATE_OBJECT(PROP_WHEAT_GRASS_HALF, VecNewCoors)
                            SET_ENTITY_ROTATION(family_prop_l[eSceneMember], VecNewRot)
                            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                                    "_amanda", "_glass",
                                    iFamily_synch_scene[eSceneMember])
                            
                            eFamilyPropLModel[eSceneMember] = PROP_WHEAT_GRASS_HALF
                        ELSE
                            CPRINTLN(DEBUG_FAMILY, "KALE: DOES_ENTITY_EXIST(family_prop_l[eSceneMember]):", DOES_ENTITY_EXIST(family_prop_l[eSceneMember]), ", eFamilyPropLModel[eSceneMember]:", GET_MODEL_NAME_FOR_DEBUG(eFamilyPropLModel[eSceneMember]), " = P_W_GRASS_GLS_S")
                        ENDIF
                    ELSE
                        CPRINTLN(DEBUG_FAMILY, "KALE: GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]): ", GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember]))
                    ENDIF
                ELSE
                    CPRINTLN(DEBUG_FAMILY, "KALE: HAS_MODEL_LOADED(PROP_WHEAT_GRASS_HALF):", HAS_MODEL_LOADED(PROP_WHEAT_GRASS_HALF), ", ARE_STRINGS_EQUAL(tDialogueAnimClips[eSceneMember]:\"", tDialogueAnimClips[eSceneMember], "\", \"Ugh_kale_Amanda\")")
                ENDIF
            ELSE
                CPRINTLN(DEBUG_FAMILY, "KALE: iFamily_enter_veh_stage[eSceneMember]: ", iFamily_enter_veh_stage[eSceneMember])
            ENDIF
        BREAK
//      CASE FE_M7_WIFE_shopping_with_son
        CASE FE_M7_WIFE_shopping_with_daughter
            STRING strShopping, strIdling
            strShopping = ""
            strIdling = ""
            IF eSceneMember = PS_M_WIFE
                IF (eFamilyEvent = FE_M7_WIFE_shopping_with_daughter)
                    strShopping = "FAMR_IG_7"
                    strIdling = ""
//              ELIF (eFamilyEvent = FE_M7_WIFE_shopping_with_son)
//                  strShopping = "FAMR_IG_8"
//                  strIdling = ""
                ENDIF
            ELSE
                IF iFamily_enter_veh_stage[eSceneMember] = 0
                AND iFamily_enter_veh_stage[PS_M_WIFE] > 0
                    iFamily_enter_veh_stage[eSceneMember] = iFamily_enter_veh_stage[PS_M_WIFE]
                ENDIF
            ENDIF
            
            PRIVATE_Update_Family_ComingHome(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_enter_veh_stage[eSceneMember],
                    family_prop_l[eSceneMember], family_prop_r[eSceneMember],
                    iFamily_synch_scene[PS_M_WIFE], tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    FE_ANY_find_family_event,
                    iInteriorForSafehouse, "v_michael_g_front",
                    "FMMAUD", strShopping, strIdling, tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer, iRandSpeechCount[eSceneMember])
            
            /*
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[PS_M_WIFE],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])
            */
            IF (eSceneMember = PS_M_WIFE)
                IF NOT ARE_STRINGS_EQUAL(tDialogueAnimClips[eSceneMember], "AmandaBase_Amanda")
                    PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                            FD_7_MICHAEL_FRONTDOOR_L,
                            tDialogueAnimDicts[eSceneMember],
                            tDialogueAnimClips[eSceneMember], "Amanda", "Door_L",
                            iFamily_synch_scene[PS_M_WIFE], TRUE)
                    
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "Amanda", "Bag_01",
                            iFamily_synch_scene[PS_M_WIFE])
                    PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                            tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "Amanda", "Bag_02",
                            iFamily_synch_scene[PS_M_WIFE])
                ELSE
                    IF DOES_ENTITY_EXIST(family_door[PS_M_WIFE])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_7_MICHAEL_FRONTDOOR_L,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_WIFE])
                        
                        DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                        DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, default, TRUE)

                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_DAUGHTER])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_DAUGHTER])
                        
                        DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                        DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, default, TRUE)

                    ENDIF
                    
                    IF DOES_ENTITY_EXIST(family_prop_l[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                ENDIF
            ELIF (eSceneMember = PS_M_DAUGHTER)
                PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                        FD_8_MICHAEL_FRONTDOOR_R,
                        tDialogueAnimDicts[eSceneMember],
                        tDialogueAnimClips[eSceneMember], "Tracy", "Door_R",
                        iFamily_synch_scene[PS_M_WIFE], TRUE)
                
                IF eFamilyPropLModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                OR eFamilyPropRModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                    Initialise_Michael_Family_Prop_Variables(eSceneMember)
                ENDIF
                
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "Tracy", "Bag_03",
                        iFamily_synch_scene[PS_M_WIFE])
                PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "Tracy", "Bag_04",
                        iFamily_synch_scene[PS_M_WIFE])
                
                IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                    IF DOES_ENTITY_EXIST(family_door[PS_M_WIFE])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_7_MICHAEL_FRONTDOOR_L,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_WIFE])
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_door[PS_M_DAUGHTER])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_DAUGHTER])
                    ENDIF
                    
                    IF DOES_ENTITY_EXIST(family_prop_l[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_l[PS_M_DAUGHTER])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_DAUGHTER])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[eSceneMember], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    
                    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                    DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, default, TRUE)

                    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                    DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, default, TRUE)

                ENDIF
            ELIF (eSceneMember = PS_M_SON)
                PRIVATE_Set_Family_SyncSceneDoor(family_door[eSceneMember], family_chair[eSceneMember], eFamilyMember,
                        FD_8_MICHAEL_FRONTDOOR_R,
                        tDialogueAnimDicts[eSceneMember],
                        tDialogueAnimClips[eSceneMember], "Jimmy", "Door_R",
                        iFamily_synch_scene[PS_M_WIFE], TRUE)
                
                IF eFamilyPropLModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                OR eFamilyPropRModel[eSceneMember] = DUMMY_MODEL_FOR_SCRIPT
                    Initialise_Michael_Family_Prop_Variables(eSceneMember)
                ENDIF
                
                PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "Jimmy", "Bag_03",
                        iFamily_synch_scene[PS_M_WIFE])
                PRIVATE_Set_Family_SyncSceneProp(family_prop_r[eSceneMember],
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        "Jimmy", "Bag_04",
                        iFamily_synch_scene[PS_M_WIFE])
                
                IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                    IF DOES_ENTITY_EXIST(family_door[PS_M_WIFE])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_7_MICHAEL_FRONTDOOR_L,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_WIFE])
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_door[PS_M_SON])
                        MODEL_NAMES ObjectModel
                        VECTOR vecPos
                        DOOR_NAME_ENUM eDoorName
                        DOOR_HASH_ENUM eDoorHash
                        TEXT_LABEL_63 roomName
                        
                        ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                        vecPos = <<0,0,0>>
                        eDoorName = DUMMY_DOORNAME
                        eDoorHash = DUMMY_DOORHASH
                        roomName = ""
                        
                        BOOL bIgnoreInRoomCheck
                        bIgnoreInRoomCheck = FALSE
                        PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                                ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                        
                        REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                        DELETE_OBJECT(family_door[PS_M_SON])
                    ENDIF
                    
                    IF DOES_ENTITY_EXIST(family_prop_l[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[PS_M_WIFE], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_WIFE])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[PS_M_WIFE], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_l[PS_M_SON])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_l[PS_M_SON], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    IF DOES_ENTITY_EXIST(family_prop_r[PS_M_SON])
                        STOP_SYNCHRONIZED_ENTITY_ANIM(family_prop_r[PS_M_SON], NORMAL_BLEND_OUT, TRUE)
                    ENDIF
                    
                    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                    DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, default, TRUE)

                    DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
                    DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, default, TRUE)

                ENDIF
            ENDIF
            
            IF eSceneMember = PS_M_WIFE
            ELSE
                IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
                    TASK_FOLLOW_NAV_MESH_TO_COORD(family_ped[eSceneMember],
                            <<-799.1418, 183.7505, 71.6055>>,
                            PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS)
                ENDIF
            ENDIF
        BREAK
//      CASE FE_M7_WIFE_on_phone
//          PRIVATE_Update_Family_ON_PHONE(family_ped[eSceneMember])
//      BREAK
        
//      CASE FE_M_MEXMAID_cooking_for_son
//          PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//      BREAK
        CASE FE_M2_MEXMAID_cleans_booze_pot_other
        CASE FE_M7_MEXMAID_cleans_booze_pot_other
            PRIVATE_Update_Family_GoToCoordCleaning(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head, iFamily_synch_scene[eSceneMember],
                    family_prop_r[eSceneMember], PROP_SCOURER_01, eFamilyPropRModel[eSceneMember], BONETAG_PH_R_HAND,
                    iFamily_enter_veh_stage[eSceneMember], iFamily_navmeshBlock[eSceneMember],
                    bMove_peds)
            
            PRIVATE_Update_Family_Decals(eFamilyEvent, familyDecal, vFamily_scene_m_coord)
            
        BREAK
        CASE FE_M2_MEXMAID_clean_surface_a
        CASE FE_M2_MEXMAID_clean_surface_c
        CASE FE_M7_MEXMAID_clean_surface
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "MEXMAID_CLEAN_SURFACE", "", NULL, family_sfxBank[eSceneMember])
            #IF IS_DEBUG_BUILD
            Draw_Family_Member_Cleaning_Debug_Info(family_ped[eSceneMember], eFamilyEvent)
            #ENDIF
            
        BREAK
        CASE FE_M2_MEXMAID_clean_surface_b
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "", "_SCOURER",
                    iFamily_synch_scene[eSceneMember])
                    
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "MEXMAID_CLEAN_SURFACE", "", NULL, family_sfxBank[eSceneMember])
            #IF IS_DEBUG_BUILD
            Draw_Family_Member_Cleaning_Debug_Info(family_ped[eSceneMember], eFamilyEvent)
            #ENDIF
            
        BREAK
//      CASE FE_M_MEXMAID_MIC4_clean_surface
//          PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                  tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                  vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
//          
//          PRIVATE_Update_Family_Decals(eFamilyEvent, familyDecal, vFamily_scene_m_coord)
//          
//          #IF IS_DEBUG_BUILD
//          Draw_Family_Member_Cleaning_Debug_Info(family_ped[eSceneMember], eFamilyEvent)
//          #ENDIF
//          
//      BREAK
        CASE FE_M2_MEXMAID_clean_window
        CASE FE_M7_MEXMAID_clean_window
            MAGDEMO_Delete_When_Player_Biking(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            
            IF NOT g_bMagDemoActive
                //use SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE instead
                PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
            ENDIF
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "MEXMAID_CLEAN_WINDOW", "", NULL, family_sfxBank[eSceneMember])
            
            #IF IS_DEBUG_BUILD
            Draw_Family_Member_Cleaning_Debug_Info(family_ped[eSceneMember], eFamilyEvent)
            #ENDIF
            
        BREAK
        CASE FE_M_MEXMAID_MIC4_clean_window
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            
//          IF NOT g_bMagDemoActive
//              //use SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE instead
//              PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
//          ENDIF
            
            PRIVATE_Update_Family_Decals(eFamilyEvent, familyDecal, vFamily_scene_m_coord)
            
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "MEXMAID_CLEAN_WINDOW", "", NULL, family_sfxBank[eSceneMember])
            
            #IF IS_DEBUG_BUILD
            Draw_Family_Member_Cleaning_Debug_Info(family_ped[eSceneMember], eFamilyEvent)
            #ENDIF
            
        BREAK
        CASE FE_M_MEXMAID_does_the_dishes
//          PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
//                  eFamilyMember, eFamilyEvent,
//                  tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                  vFamily_scene_m_coord, fFamily_scene_m_head,
//                  iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_Update_Family_SynchScene(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember],
                    TRUE, FALSE, tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember])

            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "MEXMAID_DOES_THE_DISHES", "", NULL, family_sfxBank[eSceneMember])

        BREAK
//      CASE FE_M_MEXMAID_watching_TV
//          
////            IF PRIVATE_Update_Family_SatInChair(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, family_chair[eSceneMember]
////                    #IF IS_DEBUG_BUILD
////                     bUpdate_chair_attach
////                    #ENDIF
////                    )
//              PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
//                      eFamilyMember, eFamilyEvent,
//                      tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                      vFamily_scene_m_coord, fFamily_scene_m_head,
//                      iFamily_synch_scene[eSceneMember], TRUE, FALSE)
//              PRIVATE_ForceFamilyMemberBoundsOrientationThisFrame(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
////            ENDIF
//          
//          PRIVATE_FamilyMemberWatchingTv(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                  vFamily_scene_m_coord, fFamily_scene_m_head,
//                  iFamily_enter_veh_stage[eSceneMember])
//      BREAK
        CASE FE_M_MEXMAID_stealing_stuff
            PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            PRIVATE_Update_Family_Hangup(family_ped[eSceneMember], eFamilyMember,
                    FE_M_MEXMAID_stealing_stuff_caught,
                    "FMMAUD", "MA_IG_8", tCreatedMichaelConvLabels,
                    MyLocalPedStruct, sSpeechTimer, eFamilyEvent)
        BREAK
        CASE FE_M_MEXMAID_stealing_stuff_caught
            PRIVATE_Update_Family_Hangup_And_Wander(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    iFamily_enter_veh_stage[eSceneMember],
                    family_prop_l[eSceneMember], eFamilyPropLModel[eSceneMember], TRUE)
        BREAK
        
        CASE FE_M_GARDENER_with_leaf_blower
            PRIVATE_Update_Family_Scenario(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M_GARDENER_planting_flowers
            PRIVATE_Update_Family_Scenario(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        CASE FE_M_GARDENER_cleaning_pool
            IF NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
            OR VDIST2(GET_ENTITY_COORDS(family_ped[eSceneMember]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > (3.5*3.5)
                PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            ELSE
                PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
                        vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head,
                        SLOW_BLEND_IN, SLOW_BLEND_OUT)
            ENDIF
            IF PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "GARDENER_CLEANING_POOL", "", NULL, family_sfxBank[eSceneMember])
            ENDIF
        BREAK
        CASE FE_M_GARDENER_mowing_lawn
            PRIVATE_Update_Family_SynchSceneArray(family_ped[eSceneMember],
                    eFamilyMember, eFamilyEvent,
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    vFamily_scene_m_coord, fFamily_scene_m_head,
                    iFamily_synch_scene[eSceneMember], TRUE, TRUE)
            PRIVATE_Set_Family_SyncSceneProp(family_prop_l[eSceneMember],
                    tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                    "", "_LAWNMOWER",
                    iFamily_synch_scene[eSceneMember])
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "GARDEN_MOWER", "", NULL, family_sfxBank[eSceneMember])
        
            IF IS_SYNCHRONIZED_SCENE_RUNNING(iFamily_synch_scene[eSceneMember])
                
                VECTOR vSynchSceneOffset
                FLOAT fSynchSceneHead
                IF PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, eFamilyEvent, vSynchSceneOffset, fSynchSceneHead)
                    VECTOR vSynchSceneRot
                    vSynchSceneRot = <<-2.1600, -9.3600, 0.0000>>
                    SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[eSceneMember], 
                            vFamily_scene_m_coord+vSynchSceneOffset,
                            <<0,0,fFamily_scene_m_head+fSynchSceneHead>>+vSynchSceneRot)
                            
                    #IF IS_DEBUG_BUILD
                    IF IS_KEYBOARD_KEY_JUST_RELEASED(KEY_J)
                        
                        START_WIDGET_GROUP("vSynchSceneRot")
                            ADD_WIDGET_VECTOR_SLIDER("vSynchSceneOffset", vSynchSceneOffset, -30,30,0.001)
                            ADD_WIDGET_FLOAT_SLIDER("fSynchSceneHead", fSynchSceneHead, -360,360,0.01)
                            ADD_WIDGET_FLOAT_SLIDER("vSynchSceneRot.x", vSynchSceneRot.x, -90,90,0.01)
                            ADD_WIDGET_FLOAT_SLIDER("vSynchSceneRot.y", vSynchSceneRot.y, -90,90,0.01)
                        STOP_WIDGET_GROUP()
                        
                        SET_SYNCHRONIZED_SCENE_LOOPED(iFamily_synch_scene[eSceneMember], TRUE)
                        WHILE NOT IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
                            
                            SET_SYNCHRONIZED_SCENE_ORIGIN(iFamily_synch_scene[eSceneMember], 
                                    vFamily_scene_m_coord+vSynchSceneOffset,
                                    <<0,0,fFamily_scene_m_head+fSynchSceneHead>>+vSynchSceneRot)
                            
                            WAIT(0)
                        ENDWHILE
                        
                        SAVE_STRING_TO_DEBUG_FILE("vInitOffset = ")
                        SAVE_VECTOR_TO_DEBUG_FILE(vSynchSceneOffset)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("fInitHead = ")
                        SAVE_FLOAT_TO_DEBUG_FILE(fSynchSceneHead)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                        SAVE_STRING_TO_DEBUG_FILE("vSynchSceneRot = ")
                        SAVE_VECTOR_TO_DEBUG_FILE(vSynchSceneRot)
                        SAVE_NEWLINE_TO_DEBUG_FILE()
                    ENDIF
                    #ENDIF
                ENDIF
                
                IF DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                    FLOAT fPhaseTime
                    fPhaseTime = GET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember])
                    
                    VECTOR vHandOffset, vMowerOffset
                    VECTOR vHandCoord, vMowerCoord
                    vHandOffset = <<0.046,0,0>>
                    vMowerOffset = <<-0.120,-0.360,0.300>>
                    
                    vHandCoord = GET_PED_BONE_COORDS(family_ped[eSceneMember], BONETAG_PH_R_HAND, vHandOffset)
                    vMowerCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(family_prop_l[eSceneMember], vMowerOffset)
                    
                    #IF IS_DEBUG_BUILD
                    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
                        START_WIDGET_GROUP("FE_M_GARDENER_mowing_lawn")
                            ADD_WIDGET_FLOAT_SLIDER("fPhaseTime", fPhaseTime, 0, 1, 0.01)
                            ADD_WIDGET_VECTOR_SLIDER("vHandOffset", vHandOffset, -0.1, 0.1, 0.001)
                            ADD_WIDGET_VECTOR_SLIDER("vMowerOffset", vMowerOffset, -1, 1, 0.01)
                        STOP_WIDGET_GROUP()
                        WAIT(0)
                        WHILE DOES_ENTITY_EXIST(family_prop_l[eSceneMember])
                        AND NOT IS_ENTITY_DEAD(family_ped[eSceneMember])
                            SET_SYNCHRONIZED_SCENE_PHASE(iFamily_synch_scene[eSceneMember], fPhaseTime)
                            
                            vHandCoord = GET_PED_BONE_COORDS(family_ped[eSceneMember], BONETAG_PH_R_HAND, vHandOffset)
                            vMowerCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(family_prop_l[eSceneMember], vMowerOffset)
                            
                            DRAW_DEBUG_SPHERE(vHandCoord,   0.05,   0,  0,  0,  128)
                            DRAW_DEBUG_SPHERE(vMowerCoord,  0.05,   255,255,255,128)
                            DRAW_DEBUG_LINE(vHandCoord, vMowerCoord)
                            
                            IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
                                
                                SAVE_STRING_TO_DEBUG_FILE("vHandOffset = ")
                                SAVE_VECTOR_TO_DEBUG_FILE(vHandOffset)
                                SAVE_STRING_TO_DEBUG_FILE(", vMowerOffset = ")
                                SAVE_VECTOR_TO_DEBUG_FILE(vMowerOffset)
                                SAVE_NEWLINE_TO_DEBUG_FILE()
                                
                                EXIT
                            ENDIF
                            
                            WAIT(0)
                        ENDWHILE
                    ENDIF
                    #ENDIF
                    
                    vHandCoord = GET_PED_BONE_COORDS(family_ped[eSceneMember], BONETAG_PH_R_HAND, vHandOffset)
                    vMowerCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(family_prop_l[eSceneMember], vMowerOffset)
                    
                    FLOAT ReturnStartPhase, ReturnEndPhase
                    ReturnStartPhase = -1
                    ReturnEndPhase = -1
                    IF FIND_ANIM_EVENT_PHASE(tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
                            "shoot", ReturnStartPhase, ReturnEndPhase)
                        CASSERTLN(DEBUG_FAMILY, "\"shoot\" found in FE_M_GARDENER_mowing_lawn anim")
                    ELSE
                        
                        SWITCH GET_HASH_KEY(tDialogueAnimClips[eSceneMember])
                            CASE HASH("")
                            CASE HASH("Base")
                            CASE HASH("IG_4_BASE")
                                ReturnStartPhase = -1
                                ReturnEndPhase = -1
                            BREAK
                            CASE HASH("idle_a")
                            CASE HASH("IG_4_IDLE_A")
                                ReturnStartPhase = 0.0
                                ReturnEndPhase = 1.0
                            BREAK
                            CASE HASH("idle_b")
                            CASE HASH("IG_4_IDLE_B")
                                ReturnStartPhase = -1
                                ReturnEndPhase = -1
                            BREAK
                            CASE HASH("idle_c")
                            CASE HASH("IG_4_IDLE_C")
                                ReturnStartPhase = -1
                                ReturnEndPhase = -1
                            BREAK
                            
                            DEFAULT
                                CPRINTLN(DEBUG_FAMILY, "FE_M_GARDENER_mowing_lawn has unknown clip \"", tDialogueAnimClips[eSceneMember], "\"")
                            BREAK
                        ENDSWITCH
                    ENDIF
                    
                    IF  (ReturnStartPhase >= 0)
                    AND (ReturnEndPhase >= 0)
                        #IF IS_DEBUG_BUILD
                        TEXT_LABEL_63 str
                        str  = tDialogueAnimClips[eSceneMember]
                        str += ": "
                        str += GET_STRING_FROM_FLOAT(ReturnStartPhase)
                        str += " > "
                        str += GET_STRING_FROM_FLOAT(fPhaseTime)
                        str += " > "
                        str += GET_STRING_FROM_FLOAT(ReturnEndPhase)
                        #ENDIF
                        
                        IF (fPhaseTime >= ReturnStartPhase)
                        AND (fPhaseTime <= (ReturnEndPhase))
                            IF NOT DOES_ROPE_EXIST(lawnMowerRope)
                                #IF IS_DEBUG_BUILD
                                str += " DOESNT EXIST"
                                #ENDIF
                                
                                ROPE_LOAD_TEXTURES()
                                
                                lawnMowerRope = ADD_ROPE(vMowerCoord, <<0,0,0>>, 0.4, PHYSICS_ROPE_DEFAULT)
                            ELSE
                                #IF IS_DEBUG_BUILD
                                str += " exists"
                                #ENDIF
                                
                                PIN_ROPE_VERTEX(lawnMowerRope, 0, vHandCoord)
                                PIN_ROPE_VERTEX(lawnMowerRope, GET_ROPE_VERTEX_COUNT(lawnMowerRope) - 1, vMowerCoord)
                            ENDIF
                            
                            #IF IS_DEBUG_BUILD
                            DrawDebugFamilySphere(vHandCoord, 0.05, HUD_COLOUR_GREEN)
                            DrawDebugFamilySphere(vMowerCoord, 0.05, HUD_COLOUR_GREEN)
                            DrawDebugFamilyLine(vHandCoord, vMowerCoord, HUD_COLOUR_GREEN)
                            DrawDebugFamilyLine(vHandCoord, vMowerCoord, HUD_COLOUR_GREEN)
                            DrawDebugFamilyTextWithOffset(str, vMowerCoord, 0.0, HUD_COLOUR_GREEN)
                            #ENDIF
                        ELSE
                            IF DOES_ROPE_EXIST(lawnMowerRope)
                                DELETE_ROPE(lawnMowerRope)
                                
                                ROPE_UNLOAD_TEXTURES()                              
                            ENDIF
                            
                            #IF IS_DEBUG_BUILD
                            DrawDebugFamilySphere(vHandCoord, 0.05, HUD_COLOUR_YELLOW)
                            DrawDebugFamilySphere(vMowerCoord, 0.05, HUD_COLOUR_YELLOW)
                            DrawDebugFamilyLine(vHandCoord, vMowerCoord, HUD_COLOUR_YELLOW)
                            DrawDebugFamilyTextWithOffset(str, vMowerCoord, 0.0, HUD_COLOUR_YELLOW)
                            #ENDIF
                        ENDIF
                    ELSE
                        #IF IS_DEBUG_BUILD
                        TEXT_LABEL_63 str
                        str  = tDialogueAnimClips[eSceneMember]
                        str += ": "
                        str += GET_STRING_FROM_FLOAT(fPhaseTime)
                        str += " shouldnt have rope"
                        #ENDIF
                        
                        IF DOES_ROPE_EXIST(lawnMowerRope)
                            DELETE_ROPE(lawnMowerRope)
                        ENDIF
                        
                        #IF IS_DEBUG_BUILD
                        DrawDebugFamilySphere(vHandCoord, 0.05, HUD_COLOUR_RED, 0.5)
                        DrawDebugFamilySphere(vMowerCoord, 0.05, HUD_COLOUR_RED)
                        DrawDebugFamilyLine(vHandCoord, vMowerCoord, HUD_COLOUR_RED, 0.5)
                        DrawDebugFamilyTextWithOffset(str, vMowerCoord, 0.0, HUD_COLOUR_RED)
                        #ENDIF
                    ENDIF
                    
                ENDIF
            ENDIF
        BREAK
        CASE FE_M_GARDENER_watering_flowers
            PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
            PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
                    "GARDEN_WATER_PLANTS", "MICHAELS_GARDEN_WATER", family_ped[eSceneMember], family_sfxBank[eSceneMember])
                    
            IF (iInteriorForPlayer = iInteriorForMember[eSceneMember])
                PRIVATE_Update_Family_Ptfx_Looped(family_ped[eSceneMember], eFamilyEvent,
                                "scr_pts_gardner_watering", family_ptfx[eSceneMember], 4)
            ELSE
                IF DOES_PARTICLE_FX_LOOPED_EXIST(family_ptfx[eSceneMember])
                    STOP_PARTICLE_FX_LOOPED(family_ptfx[eSceneMember])
                ENDIF
            ENDIF
        BREAK
//      CASE FE_M_GARDENER_spraying_for_weeds
//          PRIVATE_Update_Family_Anim(family_ped[eSceneMember], eFamilyMember, eFamilyEvent)
//          PRIVATE_Update_Family_Sfx(eFamilyEvent, family_sfxStage[eSceneMember], family_sfxID[eSceneMember],
//                  "GARDEN_WATER_PLANTS", "MICHAELS_GARDEN_SPRAY", family_ped[eSceneMember])
//      BREAK
        CASE FE_M_GARDENER_on_phone
            PRIVATE_Update_Family_ON_PHONE(family_ped[eSceneMember])
        BREAK
        CASE FE_M_GARDENER_smoking_weed
//          PRIVATE_Update_Family_AnimArray(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                  tDialogueAnimDicts[eSceneMember], tDialogueAnimClips[eSceneMember],
//                  vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
            PRIVATE_Update_Family_Scenario(family_ped[eSceneMember], eFamilyMember, eFamilyEvent, iFamily_enter_veh_stage[eSceneMember])
        BREAK
        
//      CASE FE_M_MICHAEL_MIC2_washing_face
//          PRIVATE_Update_Family_AnimAdvanced(family_ped[eSceneMember], eFamilyMember, eFamilyEvent,
//                  vFamily_scene_m_coord+<<0,0,1>>, fFamily_scene_m_head)
//          PRIVATE_Update_Family_Doors(eFamilyEvent, current_family_doors[eSceneMember])
//          
//          IF (g_eCurrentFamilyEvent[FM_TREVOR_0_MICHAEL] = FE_M_MICHAEL_MIC2_washing_face)
//              SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_MICHAEL_BH, TRUE)   //1141457
//          ENDIF
//          
//      BREAK
        
        CASE FE_ANY_find_family_event
            PRIVATE_Update_Family_Find_Event(family_ped[eSceneMember], eFamilyMember, vFamily_scene_m_coord)
        BREAK
        CASE FE_ANY_wander_family_event
            PRIVATE_Update_Family_Wander_Event(family_ped[eSceneMember], eFamilyMember,
                    iFamily_enter_veh_stage[eSceneMember],
                    family_prop_l[eSceneMember], eFamilyPropLModel[eSceneMember],
                    family_prop_r[eSceneMember], eFamilyPropRModel[eSceneMember],
                    family_chair[eSceneMember],
                    iFamily_synch_scene[eSceneMember])
            
            IF (g_eCurrentSafehouseActivity = SA_MICHAEL_SOFA)
                IF (iFamily_enter_veh_stage[eSceneMember] = 2)      //iWander - Scenario
                    IF PED_HAS_USE_SCENARIO_TASK(family_ped[eSceneMember])
                        NAVMESH_ROUTE_RESULT enavmesh_route_result
                        FLOAT fOut_DistanceRemaining
                        INT iOut_ThisIsLastRouteSection
                        enavmesh_route_result = GET_NAVMESH_ROUTE_DISTANCE_REMAINING(family_ped[eSceneMember], fOut_DistanceRemaining,  iOut_ThisIsLastRouteSection)
                        
                        IF (iOut_ThisIsLastRouteSection <= 0)
                            CWARNINGLN(DEBUG_FAMILY, "#1529435 - remove from sofa if watching TV")
                            
                            enavmesh_route_result = enavmesh_route_result
                            CLEAR_PED_TASKS(family_ped[eSceneMember])
                            iFamily_enter_veh_stage[eSceneMember] = 0
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        BREAK
        
        CASE FAMILY_MEMBER_BUSY FALLTHRU
        CASE NO_FAMILY_EVENTS
            //
        BREAK
        
        DEFAULT
            #IF IS_DEBUG_BUILD
            CPRINTLN(DEBUG_FAMILY, "invalid eFamilyEvent ", Get_String_From_FamilyEvent(eFamilyEvent), " in Control_Michael_FamilyEvent_Tasks()")
            #ENDIF
            
            CASSERTLN(DEBUG_FAMILY, "invalid eFamilyEvent in Control_Michael_FamilyEvent_Tasks()")
            
        BREAK
    ENDSWITCH
    
    
    IF g_eCurrentFamilyEvent[eFamilyMember] = FAMILY_MEMBER_BUSY
        IF (eFamilyEvent = FE_M2_WIFE_with_shopping_bags_enter)
        OR (eFamilyEvent = FE_M7_WIFE_with_shopping_bags_enter)
        OR (eFamilyEvent = FE_M7_WIFE_shopping_with_daughter)
//      OR (eFamilyEvent = FE_M7_WIFE_shopping_with_son)
            
            IF DOES_ENTITY_EXIST(family_door[PS_M_WIFE])
                MODEL_NAMES ObjectModel
                VECTOR vecPos
                DOOR_NAME_ENUM eDoorName
                DOOR_HASH_ENUM eDoorHash
                TEXT_LABEL_63 roomName
                
                ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                vecPos = <<0,0,0>>
                eDoorName = DUMMY_DOORNAME
                eDoorHash = DUMMY_DOORHASH
                roomName = ""
                
                BOOL bIgnoreInRoomCheck
                bIgnoreInRoomCheck = FALSE
                PRIVATE_Get_Family_Door_Attributes(FD_7_MICHAEL_FRONTDOOR_L,
                        ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                
                PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                        ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                
                DELETE_OBJECT(family_door[PS_M_WIFE])
            ENDIF
            IF DOES_ENTITY_EXIST(family_door[PS_M_DAUGHTER])
                MODEL_NAMES ObjectModel
                VECTOR vecPos
                DOOR_NAME_ENUM eDoorName
                DOOR_HASH_ENUM eDoorHash
                TEXT_LABEL_63 roomName
                
                ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                vecPos = <<0,0,0>>
                eDoorName = DUMMY_DOORNAME
                eDoorHash = DUMMY_DOORHASH
                roomName = ""
                
                BOOL bIgnoreInRoomCheck
                bIgnoreInRoomCheck = FALSE
                PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                        ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                
                REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                DELETE_OBJECT(family_door[PS_M_DAUGHTER])
            ENDIF
            IF DOES_ENTITY_EXIST(family_door[PS_M_SON])
                MODEL_NAMES ObjectModel
                VECTOR vecPos
                DOOR_NAME_ENUM eDoorName
                DOOR_HASH_ENUM eDoorHash
                TEXT_LABEL_63 roomName
                
                ObjectModel = DUMMY_MODEL_FOR_SCRIPT
                vecPos = <<0,0,0>>
                eDoorName = DUMMY_DOORNAME
                eDoorHash = DUMMY_DOORHASH
                roomName = ""
                
                BOOL bIgnoreInRoomCheck
                bIgnoreInRoomCheck = FALSE
                PRIVATE_Get_Family_Door_Attributes(FD_8_MICHAEL_FRONTDOOR_R,
                        ObjectModel, vecPos, eDoorName, eDoorHash, roomName, bIgnoreInRoomCheck)
                
                REMOVE_MODEL_HIDE(vecPos, 1.5, ObjectModel, FALSE)
                DELETE_OBJECT(family_door[PS_M_SON])
            ENDIF
            DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_L), 0.0, default, TRUE)

            DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, default, TRUE)
            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_M_MANSION_F_R), 0.0, default, TRUE)
            
        ENDIF
    ENDIF
    
    IF (g_eCurrentFamilyEvent[eFamilyMember] <> eFamilyEvent)
    AND (eFamilyEvent <> FE_M_MEXMAID_stealing_stuff)
        
        IF eFamilyEvent = FE_M_DAUGHTER_screaming_at_dad
//      OR eFamilyEvent = FE_M_WIFE_screaming_at_son_P1
        OR eFamilyEvent = FE_M_WIFE_screaming_at_son_P2
        OR eFamilyEvent = FE_M_WIFE_screaming_at_son_P3
        OR eFamilyEvent = FE_M_WIFE_screaming_at_daughter
            Update_Family_Scene_Doors_For_Event_Change(eSceneMember)
        ENDIF
        
        PRIVATE_Get_FamilyMember_Init_Offset(eFamilyMember, g_eCurrentFamilyEvent[eFamilyMember], vFamily_coordOffset[eSceneMember], fFamily_headOffset[eSceneMember])
        
        SetFamilyMemberConfigFlag(family_ped[eSceneMember], eFamilyMember, RELGROUPHASH_FAMILY_M, TRUE)
        
        IF (eFamilyPropLModel[eFamilyMember] <> DUMMY_MODEL_FOR_SCRIPT)
            family_prop_l[eFamilyMember] = NULL
        ENDIF
        IF (eFamilyPropRModel[eFamilyMember] <> DUMMY_MODEL_FOR_SCRIPT)
            family_prop_r[eFamilyMember] = NULL
        ENDIF
        
        Initialise_Michael_Family_Prop_Variables(eSceneMember)
        family_sfxID[eSceneMember] = iCONST_FAMILY_SFX_INVALID
        
        #IF IS_DEBUG_BUILD
        VECTOR vDebugJumpOffset
        Get_DebugJumpOffset_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember],  vDebugJumpOffset)
        Get_DebugJumpAngle_From_Vector(vDebugJumpOffset, fPlayerDebugJumpAngle[eSceneMember], fPlayerDebugJumpDistance[eSceneMember])
        #ENDIF
    ENDIF
ENDPROC

FUNC BOOL Moniter_A_Michael_Scene_Member(PS_M_SCENE_MEMBERS eSceneMember)
    CDEBUG3LN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Moniter_A_Michael_Scene_Member.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
    
    enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eSceneMember)
    
    #IF IS_DEBUG_BUILD
//  DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), vFamily_scene_m_coord,                      HUD_COLOUR_BLUE,30.0/255.0)
//  DrawDebugFamilySphere(vFamily_scene_m_coord, 0.5,                                                                   HUD_COLOUR_BLUE,30.0/255.0)
//  
//  DrawDebugFamilyLine(GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE),  HUD_COLOUR_YELLOW)
    
    TEXT_LABEL_63 str
    VECTOR xyzDraw = GET_ENTITY_COORDS(family_ped[eSceneMember], FALSE)
    
    str = Get_String_From_Family_Scene_M_Scene_Members(eSceneMember)
    DrawDebugFamilyTextWithOffset(str, xyzDraw, 0, HUD_COLOUR_BLACK)
    
    str = "event: "
    str += Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember])
    DrawDebugFamilyTextWithOffset(str, xyzDraw, 1, HUD_COLOUR_BLUE)
    #ENDIF
    
    #IF IS_DEBUG_BUILD
    IF NOT g_bUpdatedFamilyEvents
    #ENDIF
        Control_Michael_FamilyEvent_Tasks(eSceneMember, g_eCurrentFamilyEvent[eFamilyMember])
    #IF IS_DEBUG_BUILD
    ELSE
                    
        CPRINTLN(DEBUG_FAMILY, "don't Control  ", Get_String_From_Family_Scene_M_Scene_Members(eSceneMember), ", familyEvent Tasks ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]), ", g_bUpdatedFamilyEvents = ", g_bUpdatedFamilyEvents)
        
    ENDIF
    #ENDIF
    
    RETURN FALSE
ENDFUNC

// *******************************************************************************************
//  MISSION SPECIFIC FUNCTIONS AND PROCEDURES
// *******************************************************************************************
FUNC BOOL Moniter_All_Scene_Members()
    
    CDEBUG3LN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Moniter_All_Scene_Members.GET_CURRENT_STACK_SIZE(", GET_CURRENT_STACK_SIZE() ," max ", GET_ALLOCATED_STACK_SIZE(), ")")
    
    BOOL bCheckEachFrameForFamilyMemberSpeech = FALSE
    
    UpdateInteriorForPlayer(bUpdateInteriorForPlayer, sUpdatePlayerInteriorTimer,
            iInteriorForPlayer, iInteriorForSafehouse, vFamily_scene_m_coord, "v_michael")

    
    PS_M_SCENE_MEMBERS eMember
    REPEAT MAX_M_SCENE_MEMBERS eMember
        IF Is_Michael_Scene_Member_Active(eMember)
            Moniter_A_Michael_Scene_Member(eMember)
            
            IF NOT bCheckEachFrameForFamilyMemberSpeech
                enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eMember)
                IF NOT Is_Dialogue_Anim_Drinking(family_ped[eMember], tDialogueAnimDicts[eMember], tDialogueAnimClips[eMember], iFamily_synch_scene[eMember], sSpeechTimer)
                    
                    IF g_bMagDemoActive
                        ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
                    ENDIF
                    
                    IF (eMember = PS_M_GARDENER)
                        IF NOT bSaidHiToGardener
                            BOOL bSafeToSaidHiToGardener = TRUE
                            
                            VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
                            VECTOR vMemberCoord = GET_ENTITY_COORDS(family_ped[eMember])
    
                            CONST_FLOAT fMAX_DIST_FOR_SPEECH_2D 5.0
                            IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, vMemberCoord, FALSE) > fMAX_DIST_FOR_SPEECH_2D)
                                bSafeToSaidHiToGardener = FALSE
                            ENDIF
                            
                            IF bSafeToSaidHiToGardener
                                PLAY_PED_AMBIENT_SPEECH(family_ped[eMember], "GENERIC_HI", SPEECH_PARAMS_FORCE)
                                RESTART_TIMER_NOW(sSpeechTimer)
                                
                                bSaidHiToGardener = TRUE
                                CPRINTLN(DEBUG_FAMILY, "bSaidHiToGardener")
                            ENDIF
                        ENDIF
                    ENDIF
                    
                    IF Play_This_Family_Speech(family_ped[eMember],
                            g_eCurrentFamilyEvent[eFamilyMember],
                            MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels,
                            sSpeechTimer, iRandSpeechCount[eMember])
                        bCheckEachFrameForFamilyMemberSpeech = TRUE
                        iComingHomeFamilySpeechStage = 99
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            
        ENDIF
    ENDREPEAT
    TimedStopFamilyFires(family_ped, sFireTimer)
    
    Play_Coming_Home_Family_Speech(BIT_MICHAEL, iComingHomeFamilySpeechStage,
            iInteriorForPlayer, iInteriorForSafehouse,
            MyLocalPedStruct, "FMMAUD", tCreatedMichaelConvLabels, sSpeechTimer)
                    
    #IF IS_DEBUG_BUILD
    IF g_bDebugForceCreateFamily
        
        INT iDebugForceCreateFamily =  0
        REPEAT MAX_M_SCENE_MEMBERS eMember
            enumFamilyMember eFamilyMember = Get_Family_Member_From_Michael_Scene_Member(eMember)
            IF (g_eCurrentFamilyEvent[g_eDebugSelectedMember] = g_eCurrentFamilyEvent[eFamilyMember])
                IF DOES_ENTITY_EXIST(family_ped[eMember])
                    CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_M_Scene_Members(eMember), "] exists")
                    
                    iDebugForceCreateFamily++
                ELSE
                    CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_M_Scene_Members(eMember), "] doesnt exist")
                    
                    iDebugForceCreateFamily = -9999
                ENDIF
            ELSE
                CPRINTLN(DEBUG_FAMILY, "ped[ ", Get_String_From_Family_Scene_M_Scene_Members(eMember), "] has different event ", Get_String_From_FamilyEvent(g_eCurrentFamilyEvent[eFamilyMember]))
            ENDIF
        ENDREPEAT
        
        
        IF iDebugForceCreateFamily > 0
            g_bDebugForceCreateFamily = FALSE
        ENDIF
    ENDIF
    #ENDIF
    
    RETURN TRUE
ENDFUNC

// *******************************************************************************************
//  MAIN SCRIPT
// *******************************************************************************************
SCRIPT(structFamilyScene thisFamilyScene)
    IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
        Family_Scene_M_Cleanup()
    ENDIF
    
    WAIT(0)
    
    Initialise_Family_Scene_M_Variables(thisFamilyScene)
    Setup_Family_Scene_M()
    
    #IF IS_DEBUG_BUILD
    Create_Family_Scene_M_Widget()
    #ENDIF
    
    WHILE bFamily_Scene_M_in_progress
        WAIT(0)
        
        IF g_bMagDemoKillFamilySceneM
            g_bMagDemoKillFamilySceneM = FALSE
            Family_Scene_M_Cleanup()
        ENDIF
        
        bFamily_Scene_M_in_progress = FALSE
        
        IF IS_FAMILY_SCENE_ALLOWED_TO_RUN_AND_IN_RANGE(CHAR_MICHAEL, vFamily_scene_m_coord, 1.5)
            Moniter_All_Scene_Members()
            
            IF (NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_STARTED))
            OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_TREVOR_EXILE_FINISHED)
                Moniter_Player_Griefing(BIT_MICHAEL, sFamilyGriefing, vFamily_scene_m_coord, <<30,30,15>>, family_veh)
            ENDIF
            
            bFamily_Scene_M_in_progress = TRUE
        
        ELSE
            PS_M_SCENE_MEMBERS eMember
            REPEAT MAX_M_SCENE_MEMBERS eMember
                Is_Michael_Scene_Member_Active(eMember)
            ENDREPEAT
        ENDIF
        
        IF Setup_Chalk_Board_Prop()
            
            BOOL bAllowSceneToRunForChalkBoard = TRUE
            
            IF IS_MISSION_TRIGGER_ACTIVE()
                bAllowSceneToRunForChalkBoard = FALSE
            ENDIF
            
            IF bAllowSceneToRunForChalkBoard
                bFamily_Scene_M_in_progress = TRUE
            ENDIF
        ENDIF

        #IF IS_DEBUG_BUILD
        Watch_Family_Scene_M_Widget()
        Family_Scene_M_Debug_Options()
        #ENDIF
    ENDWHILE
    
    Family_Scene_M_Cleanup()
ENDSCRIPT
