

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "Flow_Mission_Data_Public.sch"
USING "flow_public_game.sch"
USING "flow_help_public.sch"
USING "rc_helper_functions.sch"
USING "rc_area_public.sch"


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	AF_Intro_T_Sandy.sc											//
//		AUTHOR			:	Tom Waters													//
//		DESCRIPTION		:	Handles Trevor's Sandy Shores hangar/helipad intro			// 
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT AIRFIELD_INTRO_RECHECK_DELAY_TIME		1500	// delay time for rechecking safe conditions
CONST_INT AIRFIELD_INTRO_RECHECK_DELAY_TIME_SHORT	100	// delay time for rechecking safe conditions

ENUM AIRFIELD_INTRO_STATE_ENUM
	AFI_STATE_TRIGGER_INTRO,
	AFI_STATE_PLAY_INTRO_CUTSCENE,
	AFI_STATE_COMPLETE
ENDENUM
AIRFIELD_INTRO_STATE_ENUM eSafehouseIntroState = AFI_STATE_TRIGGER_INTRO

//ENUM AIRFIELD_INTRO_CUTSCENE_STAGE_ENUM
//	AFI_CUTSCENE_STAGE_SETUP,
//	AFI_CUTSCENE_STAGE_OVERVIEW_SHOT,
//	AFI_CUTSCENE_STAGE_HANGAR_SHOT,
//	AFI_CUTSCENE_STAGE_HELIPAD_SHOT,
//	AFI_CUTSCENE_STAGE_CLEANUP
//ENDENUM
//AIRFIELD_INTRO_CUTSCENE_STAGE_ENUM	eCutsceneStage = AFI_CUTSCENE_STAGE_SETUP

//BOOL bIsCutsceneActive
//CAMERA_INDEX camIndex_Cutscene
//INT iMissionCandidateID = NO_CANDIDATE_ID
//INT iTimer_Cutscene_Main
//INT iTimer_Cutscene_SkipDelay
//INT iTimer_NewLoadScene_FailSafe
TEST_POLY tpTrevorSandyArea
//VEHICLE_INDEX viPlayerVehicle
#IF IS_DEBUG_BUILD
	BOOL bDebugLaunchPrints = FALSE // Set to true to see launch criteria debug - DO NOT CHECK IN WHILE TRUE
#ENDIF

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

///// PURPOSE:
/////    uses candidate ID system to try to get permission to run (ensures nothing else will interfere with the intro once running)
///// RETURNS:
/////    TRUE if the safehouse intro has permission to run, FALSE otherwise
//FUNC BOOL REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO()
//
//	m_enumMissionCandidateReturnValue eLaunchRequest = Request_Mission_Launch(iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY)
//	
//	#IF IS_DEBUG_BUILD
//		IF (eLaunchRequest = MCRET_DENIED)
//			CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_DENIED: frame = ", GET_FRAME_COUNT())
//		ENDIF
//		IF (eLaunchRequest = MCRET_PROCESSING)
//			CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_PROCESSING: frame = ", GET_FRAME_COUNT())
//		ENDIF
//		IF (eLaunchRequest = MCRET_ACCEPTED)
//			CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_ACCEPTED: iMissionCandidateID : ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
//		ENDIF
//	#ENDIF
//	
//	IF eLaunchRequest = MCRET_ACCEPTED
//		RETURN TRUE
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC

/// PURPOSE:
///    ignores roll
/// PARAMS:
///    vRot - the roatation to convert
/// RETURNS:
///    direction VECTOR
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
	  //RETURN << COS(vRot.Z+90), SIN(vRot.Z+90), 0.0 >>	// original test which only takes into account the vRot z value
ENDFUNC

/// PURPOSE:
///    Check the conditions which mean we shouldn't go ahead with the intro
/// RETURNS:
///    TRUE if the script is safe to continue
FUNC BOOL IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY()
	
	// added to prevent cutscene going through prior to player actually gaining control
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_PLAYER_PLAYING returned false : frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
//	// check Trevor has unlocked the airfield - shouldn't need this as you get the stuff immediately after Trevor 2
//	IF NOT Get_Mission_Flow_Flag_State()
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT) is false.: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
	// only do the intro if the player is Trevor
	IF (GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR)
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
//	// test the new candidate id system to see if mission_type_story can launch against the current running scripts.
//	IF NOT CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY) return FALSE: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF GET_MISSION_FLAG()
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as mission flag is already set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF IS_CURRENTLY_ON_MISSION_TO_TYPE()
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_CURRENTLY_ON_MISSION_TO_TYPE() is set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
	IF g_bIsOnRampage
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bIsOnRampage is set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
//	IF IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as currently on friend activity is TRUE: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF NOT IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME)
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_IT_SAFE_TO_TRIGGER_SCRIPT_TYPE(ST_MINIGAME) returned FALSE: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
	/*IF g_bTriggerSceneActive //removed since this prevents the safehouse intros playing if a mission blip is in the house grounds.
		//#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bTriggerSceneActive returned TRUE: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF*/
	
	IF g_bMissionOverStatTrigger
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bMissionOverStatTrigger is set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	// point in the lead in where the player can't back out of a mission trigger
//	IF g_bPlayerLockedInToTrigger
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bPlayerLockedInToTrigger is set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
//	IF GET_RANDOM_EVENT_FLAG()
//		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied as random event flag is already set: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
//		RETURN FALSE
//	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied player has wanted level is TRUE: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied IS_CUTSCENE_PLAYING: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	// cutscene has been kicking in during player switches recently
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied IS_PLAYER_SWITCH_IN_PROGRESS.: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	// fix issue where cutscene would interrupt timetable scene
	IF Is_Player_Timetable_Scene_In_Progress()
		#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied Is_Player_Timetable_Scene_In_Progress.: frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
		RETURN FALSE
	ENDIF
	
	// added to stop a safehouse intro playing during the setup of a debug launched mission
	#IF IS_DEBUG_BUILD
		IF (g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow)
			#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "Script denied g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow): frame = ", GET_FRAME_COUNT()) ENDIF #ENDIF
			RETURN FALSE
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD IF bDebugLaunchPrints CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY: ", "return TRUE : game_timer = ", GET_GAME_TIMER()) ENDIF #ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    handle running through the safehouse intro cutscene
PROC STATE_PLAY_INTRO_CUTSCENE()

//	CLEAR_PRINTS()
//	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//		CLEAR_HELP(TRUE)
//	ENDIF
	//CV@  Removing text from ron if the player has already reached the hanger. #bug: 1241552
	CANCEL_COMMUNICATION(TEXT_TREV_LOST_HANGER)
	
	ADD_HELP_TO_FLOW_QUEUE("SHI_T_SANDY_2")
	WAIT(0)
	ADD_HELP_TO_FLOW_QUEUE("SHI_T_SANDY_3")
	
	eSafehouseIntroState = AFI_STATE_COMPLETE

//	INT iTimerCameraTransition = 9000	// DEFAULT_HELP_TEXT_TIME - used to use this but bug wanted second knocking off 1080420
//	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//	
//		SWITCH eCutsceneStage
//			CASE AFI_CUTSCENE_STAGE_SETUP
//			
//				IF RC_IS_CUTSCENE_OK_TO_START(FALSE)	
//				
//					IF IS_NEW_LOAD_SCENE_ACTIVE()
//					AND IS_NEW_LOAD_SCENE_LOADED()
//					OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 9000)	// fail safe for IS_NEW_LOAD_SCENE_LOADED() not returning TRUE
//						
//						#IF IS_DEBUG_BUILD
//							IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 9000)
//								CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", " STATE_PLAY_INTRO_CUTSCENE - IS_NEW_LOAD_SCENE_LOADED timed out, FRAME COUNT : ", GET_FRAME_COUNT())
//							ENDIF
//						#ENDIF
//						
//						CLEAR_PRINTS()
//						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//							CLEAR_HELP(TRUE)
//						ENDIF
//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//						RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE)
//						SET_WIDESCREEN_BORDERS(FALSE, 0)	// Required to display help text for the safehouse tutorial
//						DISPLAY_HUD(TRUE) 					// Required to display help text for the safehouse tutorial
//						SET_FRONTEND_RADIO_ACTIVE(FALSE)
//						iTimer_Cutscene_Main = GET_GAME_TIMER()
//						iTimer_Cutscene_SkipDelay = GET_GAME_TIMER()
//						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
//							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								viPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//								IF IS_ENTITY_ALIVE(viPlayerVehicle)
//									SET_ENTITY_VISIBLE(viPlayerVehicle, FALSE)
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						IF NOT DOES_CAM_EXIST(camIndex_Cutscene)
//							camIndex_Cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)			
//						ENDIF
//						IF DOES_CAM_EXIST(camIndex_Cutscene)
//							// Pan down, use an accel/decel interp.
//							SET_CAM_PARAMS(camIndex_Cutscene, << 1817.7863, 3286.4565, 44.7419 >>, << -9.2160, -0.0000, 96.2676 >>, 50.0, iTimerCameraTransition)
//							SET_CAM_PARAMS(camIndex_Cutscene, << 1817.5864, 3286.4346, 45.9803 >>, << -9.2160, -0.0000, 96.2676 >>, 50.0, iTimerCameraTransition, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//						ENDIF
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//						PRINT_HELP("SHI_T_SANDY_1")		// Trevor has now taken over The Lost's hangar.
//						bIsCutsceneActive = TRUE
//						eCutsceneStage = AFI_CUTSCENE_STAGE_OVERVIEW_SHOT	
//					ELSE
//						//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", " STATE_PLAY_INTRO_CUTSCENE - waiting on  IS_NEW_LOAD_SCENE_LOADED, FRAME COUNT : ", GET_FRAME_COUNT())
//					ENDIF
//				ELSE
//					//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", " STATE_PLAY_INTRO_CUTSCENE - attempting to start cutscene, FRAME COUNT : ", GET_FRAME_COUNT())
//				ENDIF
//			BREAK
//			
//			CASE AFI_CUTSCENE_STAGE_OVERVIEW_SHOT
//				IF (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition
//					IF DOES_CAM_EXIST(camIndex_Cutscene)
//						// Pan down, use an accel/decel interp.
//						SET_CAM_PARAMS(camIndex_Cutscene, << 1751.7053, 3280.7161, 50.4102 >>, << -23.3133, -0.0000, 37.3022 >>, 44.2, iTimerCameraTransition)
//						SET_CAM_PARAMS(camIndex_Cutscene, << 1747.2379, 3275.9890, 46.3828 >>, << -5.6642, -0.0000, 23.1546 >>, 44.2, iTimerCameraTransition, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//						PRINT_HELP("SHI_T_SANDY_2")		// Planes you purchase from websites or that you land inside will be stored here. ~BLIP_HANGAR~
//						iTimer_Cutscene_Main = GET_GAME_TIMER()
//					ENDIF
//					eCutsceneStage = AFI_CUTSCENE_STAGE_HANGAR_SHOT
//				ENDIF
//			BREAK
//
//			CASE AFI_CUTSCENE_STAGE_HANGAR_SHOT
//				IF (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition
//					IF DOES_CAM_EXIST(camIndex_Cutscene)
//						// Pan down, use an accel/decel interp.
//						SET_CAM_PARAMS(camIndex_Cutscene, <<1768.0890, 3255.5837, 52.9282>>, <<-17.4980, 0.0000, -142.5211>>, 44.2, iTimerCameraTransition)
//						SET_CAM_PARAMS(camIndex_Cutscene, <<1769.5732, 3258.3174, 47.7916>>, <<-15.1458, 0.0000, -150.4120>>, 44.2, iTimerCameraTransition, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//						PRINT_HELP("SHI_T_SANDY_3")		// Helicopters purchased from websites or that you land here will be stored here. ~BLIP_HELIPAD~
//						iTimer_Cutscene_Main = GET_GAME_TIMER()
//					ENDIF
//					eCutsceneStage = AFI_CUTSCENE_STAGE_HELIPAD_SHOT
//				ENDIF
//			BREAK
//			
//			CASE AFI_CUTSCENE_STAGE_HELIPAD_SHOT
//				IF (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition
//					eCutsceneStage = AFI_CUTSCENE_STAGE_CLEANUP
//				ENDIF
//			BREAK
//			
//			CASE AFI_CUTSCENE_STAGE_CLEANUP
//				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//					CLEAR_HELP(TRUE)
//				ENDIF
//				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
//				ENDIF
//				IF DOES_ENTITY_EXIST(viPlayerVehicle)
//					SET_ENTITY_VISIBLE(viPlayerVehicle, TRUE)
//				ENDIF
//
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				RC_END_CUTSCENE_MODE()
//				RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				IF DOES_CAM_EXIST(camIndex_Cutscene)
//					SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
//					DESTROY_CAM	(camIndex_Cutscene)
//				ENDIF
//				NEW_LOAD_SCENE_STOP()
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)		
//				bIsCutsceneActive = FALSE
//				//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", " STATE_PLAY_INTRO_CUTSCENE - cutscene finished")
//				eSafehouseIntroState = AFI_STATE_COMPLETE
//			BREAK
//		ENDSWITCH
//		
//		//handle cutscene skip
//		IF eCutsceneStage > AFI_CUTSCENE_STAGE_SETUP
//		AND eCutsceneStage < AFI_CUTSCENE_STAGE_CLEANUP
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
//			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//				IF (GET_GAME_TIMER() - iTimer_Cutscene_SkipDelay) > 1000	// don't allow instant skip to prevent accidental skip
//					//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", " STATE_PLAY_INTRO_CUTSCENE - cutscene skipped")
//					eCutsceneStage = AFI_CUTSCENE_STAGE_CLEANUP
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

/// PURPOSE:
///    Does any necessary cleanup and terminates the script's thread.
PROC Script_Cleanup()
	// only do the cutscene cleanup if the mission candidate ID was set
//	IF iMissionCandidateID != NO_CANDIDATE_ID
//		IF bIsCutsceneActive
//			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//				CLEAR_HELP(TRUE)
//			ENDIF
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
//			ENDIF
//			RC_END_CUTSCENE_MODE()
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			IF DOES_CAM_EXIST(camIndex_Cutscene)
//				SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
//				DESTROY_CAM	(camIndex_Cutscene)
//			ENDIF
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
//			//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : Script Cleanup - bIsCutsceneActive cleanup done")
//		ENDIF
//		NEW_LOAD_SCENE_STOP()
//		CLEAR_FOCUS()
//		CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "MISSION_OVER(iMissionCandidateID) : iMissionCandidateID = ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
//		MISSION_OVER(iMissionCandidateID)
//	ENDIF
	
	//Ensure all world brains reactivate. Ensures minigame and object triggers near savehouses
	//aren't blocked after the safehouse intro.
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : Terminate thread - Script Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : script terminated")
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

SCRIPT	
	// ensure the script is going to be relaunched on a new sp session (so this comes before HAS_FORCE_CLEANUP_OCCURRED)
	Register_Script_To_Relaunch_List(LAUNCH_BIT_AF_INTRO_T_SANDY)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "forced cleanup occured")	
		Script_Cleanup()
	ENDIF
	
	// Hold up whilst updating gameflow via debug menu
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			WAIT(0)
		ENDIF
	#ENDIF

	CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "initialised")		
	eSafehouseIntroState = AFI_STATE_TRIGGER_INTRO	
//	iMissionCandidateID = NO_CANDIDATE_ID
	
	//Initialise Sandy Shores airfield area Poly Check
	OPEN_TEST_POLY(tpTrevorSandyArea)		// clockwise starting by repair shack building
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1701.7803, 3225.1482, 40.0202>>)
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1678.9091, 3287.6023, 39.9648>>)
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1710.4376, 3326.4993, 40.1797>>)
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1738.8787, 3335.2500, 40.1493>>)
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1768.3873, 3312.1008, 40.2875>>)
		ADD_TEST_POLY_VERT(tpTrevorSandyArea, <<1819.2413, 3225.4495, 42.9019>>)
	CLOSE_TEST_POLY(tpTrevorSandyArea)	//must be called once verts have been added.
	
	WHILE TRUE
	
		SWITCH eSafehouseIntroState
			
			// Wait for the player to enter the safehouse area to trigger the intro
			CASE AFI_STATE_TRIGGER_INTRO	
				IF IS_TREVOR_SANDY_INTRO_SAFE_TO_PLAY()
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vPos, vCentreProperty 
						vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						vCentreProperty = << 1749.71, 3267.83, 40.24 >>
						
						// initial check to see if player is close to the property's grounds before doing more expensive poly check	
						IF IS_COORD_IN_RANGE_OF_COORD_2D(vPos, vCentreProperty, 75.0)	
							// more specific check to see when the player enter's the property's grounds
							IF IS_POINT_IN_POLY_2D(tpTrevorSandyArea, vPos)			
								// height check to make sure the player isn't just flying over the property
								IF vPos.Z > 39.0
								AND vPos.Z < 43.0
									// once we get in here, nothing should interfere with the safehouse intro playing out.
//									IF REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO()
//										eCutsceneStage = AFI_CUTSCENE_STAGE_SETUP	
//										bIsCutsceneActive = FALSE
//										iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
//										// start streaming in the first camera shot area
//										VECTOR vInitialCamPos
//										vInitialCamPos = << 5.4, 557.3, 180.6 >>
//										FLOAT fInitialCamHeading
//										fInitialCamHeading = 227.6652
//										NEW_LOAD_SCENE_START(vInitialCamPos, <<COS(fInitialCamHeading+90), SIN(fInitialCamHeading+90), 0>>, 25.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues)
										eSafehouseIntroState = AFI_STATE_PLAY_INTRO_CUTSCENE									
										CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_TRIGGER_INTRO -> ", "AFI_STATE_PLAY_INTRO_CUTSCENE")
//									ENDIF
								ELSE
									// player isn't in correct trigger area Z height so delay recheck
									//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_TRIGGER_INTRO - ", "step 4 : inside property but z value is out of range")							
									WAIT(AIRFIELD_INTRO_RECHECK_DELAY_TIME_SHORT)									
								ENDIF
							ELSE								
								// player isn't in trigger area so delay recheck
								//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_TRIGGER_INTRO - ", "step 3 : inside property but z value is out of range")
								WAIT(AIRFIELD_INTRO_RECHECK_DELAY_TIME_SHORT)
							ENDIF
						ELSE							
							// player isn't in trigger area so delay recheck
							//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_TRIGGER_INTRO - ", "step 1: delay not close to property")
							WAIT(AIRFIELD_INTRO_RECHECK_DELAY_TIME)
						ENDIF
					ENDIF
				ELSE					
					// delay rechecking conditions, so this isn't spamming every frame
					//CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_TRIGGER_INTRO - ", "delay not safe to play intro")
					WAIT(AIRFIELD_INTRO_RECHECK_DELAY_TIME)
				ENDIF
			BREAK
			
			// Play the intro cutscene
			CASE AFI_STATE_PLAY_INTRO_CUTSCENE
				STATE_PLAY_INTRO_CUTSCENE()
			BREAK
				
			CASE AFI_STATE_COMPLETE
				CPRINTLN(DEBUG_AMBIENT, "AF_Intro_T_Sandy.sc : ", "AFI_STATE_COMPLETE - ", "finished ready to terminate and removed from relaunch list")
				REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_AF_INTRO_T_SANDY)	
				Script_Cleanup()
			BREAK
		
		ENDSWITCH
		
//		#IF IS_DEBUG_BUILD
//			IF IS_TEST_POLY_VALID(tpTrevorSandyArea)
//				DISPLAY_POLY(tpTrevorSandyArea, 150, 0, 0, 100)
//			ENDIF
//			//DEBUG_Check_Debug_Keys()
//		#ENDIF
		
		WAIT(0)

	ENDWHILE

ENDSCRIPT
