

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "Flow_Mission_Data_Public.sch"
USING "flow_public_game.sch"
USING "flow_help_public.sch"
USING "rc_helper_functions.sch"
USING "rc_area_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	SH_Intro_M_Home.sc											//
//		AUTHOR			:	Ahron Mason													//
//		DESCRIPTION		:	Handles Michael's home safehouse intro 						// 
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME		1000	// delay time for rechecking safe conditions
CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT	100		// delay time for rechecking safe conditions
CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_LONG	3000	// delay time for rechecking safe conditions

ENUM SAFEHOUSE_INTRO_STATE_ENUM
	SHI_STATE_INITIAL_OUT_OF_AREA_CHECK,
	SHI_STATE_TRIGGER_PRELOAD,
	SHI_STATE_TRIGGER_INTRO,
	SHI_STATE_DELAY_FOR_INTERIOR_PINNING,
	SHI_STATE_PLAY_INTRO_CUTSCENE,
	SHI_STATE_COMPLETE
ENDENUM
SAFEHOUSE_INTRO_STATE_ENUM eSafehouseIntroState = SHI_STATE_TRIGGER_INTRO

ENUM SAFEHOUSE_INTRO_CUTSCENE_STAGE_ENUM
	SHI_CUTSCENE_STAGE_SETUP,
	SHI_CUTSCENE_STAGE_GARAGE_SHOT,
	SHI_CUTSCENE_STAGE_SAVE_GAME_SHOT,
	SHI_CUTSCENE_STAGE_WARDROBE_SHOT,
	SHI_CUTSCENE_STAGE_LIVING_ROOM_SHOT,
	SHI_CUTSCENE_STAGE_KITCHEN_SHOT,
	SHI_CUTSCENE_STAGE_CLEANUP
ENDENUM
SAFEHOUSE_INTRO_CUTSCENE_STAGE_ENUM	eCutsceneStage = SHI_CUTSCENE_STAGE_SETUP

BOOL bIsCutsceneActive
BOOL bLockedInToIntro
BOOL bRequested_SafehouseInterior_Pin_To_Memory
BOOL bRequested_GarageInterior_Pin_To_Memory
BOOL bSceneHasBeenPreloaded
BOOL bAppliedFirstPersonTransitionFlash = FALSE
BOOL bDebounceSkipButton = FALSE
CAMERA_INDEX camIndex_Cutscene
INT iMissionCandidateID = NO_CANDIDATE_ID
INT iTimer_Cutscene_Main
INT iTimer_Cutscene_SkipDelay
INT iTimer_NewLoadScene_FailSafe
INTERIOR_INSTANCE_INDEX interiorInstanceIndex_MichaelHouse
INTERIOR_INSTANCE_INDEX interiorInstanceIndex_MichaelGarage
TEST_POLY tpMichaelHomeArea

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    uses candidate ID system to try to get permission to run (ensures nothing else will interfere with the intro once running)
/// RETURNS:
///    TRUE if the safehouse intro has permission to run, FALSE otherwise
FUNC BOOL REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO()

	m_enumMissionCandidateReturnValue eLaunchRequest = Request_Mission_Launch(iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY)
	
	#IF IS_DEBUG_BUILD
		IF (eLaunchRequest = MCRET_DENIED)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_DENIED: frame = ", GET_FRAME_COUNT())
		ENDIF
		IF (eLaunchRequest = MCRET_PROCESSING)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_PROCESSING: frame = ", GET_FRAME_COUNT())
		ENDIF
		IF (eLaunchRequest = MCRET_ACCEPTED)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_ACCEPTED: iMissionCandidateID : ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
		ENDIF
	#ENDIF
	
	IF eLaunchRequest = MCRET_ACCEPTED
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    ignores roll
/// PARAMS:
///    vRot - the roatation to convert
/// RETURNS:
///    direction VECTOR
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
	  //RETURN << COS(vRot.Z+90), SIN(vRot.Z+90), 0.0 >>	// original test which only takes into account the vRot z value
ENDFUNC

/// PURPOSE:
///    check Michael's housr interior is pinned in memory
/// RETURNS:
///    true if it's valid and ready
FUNC BOOL IS_INTERIOR_PINNED_IN_MEMORY(INTERIOR_INSTANCE_INDEX &interiorIndex)
	IF IS_VALID_INTERIOR(interiorIndex)
		IF IS_INTERIOR_READY(interiorIndex)	
			SET_INTERIOR_ACTIVE(interiorIndex, TRUE)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOUSE_INTERIOR_PINNED_IN_MEMORY: ", "return TRUE : game_timer = ", GET_GAME_TIMER())	
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check the conditions which mean we shouldn't go ahead with the intro
/// RETURNS:
///    TRUE if the script is safe to continue
FUNC BOOL IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY()
	
	// check Michael has unlocked the safehouse (has to be double checked here, since this script is launched when the phonecall which activates the flowflag is queued, rather than when it's finished
	/*IF NOT HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_ARM3)
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State: game_timer = ", GET_GAME_TIMER())	
		RETURN FALSE
	ENDIF*/
	
	// added to prevent cutscene going through prior to player actually gaining control
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_PLAYER_PLAYING returned false : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// only do the intro if the player is Michael
	IF (GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL)
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	IF g_bIsOnRampage
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bIsOnRampage is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF

	/*IF g_bTriggerSceneActive 	//removed since this prevents the safehouse intros playing if a mission blip is in the house grounds.
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bTriggerSceneActive returned TRUE: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF*/
		
	IF g_bMissionOverStatTrigger
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bMissionOverStatTrigger is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// point in the lead in where the player can't back out of a mission trigger
	IF g_bPlayerLockedInToTrigger
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bPlayerLockedInToTrigger is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF

	/* B*1475172 - cut allowed to go through with wanted rating now
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied player has wanted level is TRUE: frame = ", GET_FRAME_COUNT())
			RETURN FALSE
		ENDIF
	ENDIF*/
	
	// taken from the bail bond launcher hibenation checks, might not be required?
	IF IS_CUTSCENE_PLAYING()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied IS_CUTSCENE_PLAYING : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// cutscene has been kicking in during player switches recently
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied IS_PLAYER_SWITCH_IN_PROGRESS : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// fix issue where cutscene would interrupt timetable scene
	IF Is_Player_Timetable_Scene_In_Progress()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied Is_Player_Timetable_Scene_In_Progress : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// B*1470522 - allow flow phonecalls to play out, anything lower priority will get interrupted
	IF g_iCallInProgress != -1
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[g_iCallInProgress].sCommData.ePriority >= CPR_MEDIUM
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied flow phonecall in progress : frame = ", GET_FRAME_COUNT())
			RETURN FALSE
		ENDIF
	ENDIF
	
	// added to stop a safehouse intro playing during the setup of a debug launched mission
	#IF IS_DEBUG_BUILD
		IF (g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "Script denied g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow). : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
		ENDIF
	#ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY: ", "return TRUE : frame = ", GET_FRAME_COUNT())
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Setup the intial load scene request (used to stream in the cutscene shot)
/// RETURNS:
///    TRUE when NEW_LOAD_SCENE_START returns TRUE
FUNC BOOL REQUEST_INITIAL_LOAD_SCENE()

	// begin streaming in at the first camera shot (keep values in sync with those in STATE_PLAY_INTRO_CUTSCENE)
	VECTOR vInitialCamPos, vCutsceneCamRot, vLoadSceneDir
	vInitialCamPos =  << -842.1, 178.6, 72.2 >>
	vCutsceneCamRot = << 6.6, -0.0, -96.1 >>
	vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCutsceneCamRot)
	
	//#IF IS_DEBUG_BUILD DRAW_DEBUG_LINE(vInitialCamPos, vInitialCamPos + vLoadSceneDir * 10, 255, 0, 0, 255) #ENDIF
	
	IF NEW_LOAD_SCENE_START(vInitialCamPos, vLoadSceneDir, 30.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues
	OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 3000)	// fail safe for NEW_LOAD_SCENE_START() not returning TRUE
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REQUEST_INITIAL_LOAD_SCENE: ", "return TRUE : frame = ", GET_FRAME_COUNT())
		#IF IS_DEBUG_BUILD 
			IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 3000)
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REQUEST_INITIAL_LOAD_SCENE: ", " return TRUE because of FAIL SAFE TIMER : frame = ", GET_FRAME_COUNT())
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC SET_PLAYER_LOCKED_IN_TO_INTRO(BOOL paramLockedIn)
	IF paramLockedIn
		IF NOT bLockedInToIntro
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Locking player in to safehouse intro.")
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
			bLockedInToIntro = TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Claering player's lock-in to safehouse intro.")
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		bLockedInToIntro = FALSE
	ENDIF
	
ENDPROC


PROC REPOSITION_PLAYER_FOR_INTRO()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-840.6724, 159.0602, 66.2181>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 305.1602)
	ENDIF
	
	//Reposition player vehicle.
	VEHICLE_INDEX vehPlayer = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(vehPlayer)
		IF IS_VEHICLE_DRIVEABLE(vehPlayer)
			SET_ENTITY_AS_MISSION_ENTITY(vehPlayer, FALSE, TRUE)
			SET_VEHICLE_ENGINE_ON(vehPlayer, FALSE, TRUE)
			
			//Bring the vehicle out of critical state if it is in one.
			IF GET_ENTITY_HEALTH(vehPlayer) < 1
				SET_ENTITY_HEALTH(vehPlayer, 10)
			ENDIF
			IF GET_VEHICLE_ENGINE_HEALTH(vehPlayer) < 1
				SET_VEHICLE_ENGINE_HEALTH(vehPlayer, 10)
			ENDIF
			IF GET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer) < 1
				SET_VEHICLE_PETROL_TANK_HEALTH(vehPlayer, 10)
			ENDIF

			CLEAR_AREA(<<-857.1225, 157.4701, 63.7638>>, 5.0, TRUE, FALSE)
			SET_ENTITY_COORDS(vehPlayer, <<-857.1225, 157.4701, 63.7638>>)
			SET_ENTITY_HEADING(vehPlayer, 352.5659)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
			SET_MISSION_VEHICLE_GEN_VEHICLE(vehPlayer, <<-857.1225, 157.4701, 63.7638>>, 352.5659)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    handle running through the safehouse intro cutscene
PROC STATE_PLAY_INTRO_CUTSCENE()
	CONST_INT CAM_INFO_INITIAL		0
	CONST_INT CAM_INFO_INTERPOLATE	1
	VECTOR vCamPosition[2]
	VECTOR vCamRotation[2]
	FLOAT fCamFOV[2]
	VECTOR vLoadSceneDir
	FLOAT fStreamingClip = 20.0
	INT iTimerCameraTransition = 9000	// DEFAULT_HELP_TEXT_TIME - used to use this but bug wanted second knocking off 1080420		
	INT iTimeLoadFailSafe = 11000
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SWITCH eCutsceneStage
			CASE SHI_CUTSCENE_STAGE_SETUP
			
				IF RC_IS_CUTSCENE_OK_TO_START(FALSE)
				
					// bug 1188935
					//IF IS_NEW_LOAD_SCENE_ACTIVE()
					//AND IS_NEW_LOAD_SCENE_LOADED()
					//OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 5000)	// iTimeLoadFailSafe)	// fail safe for IS_NEW_LOAD_SCENE_LOADED() not returning TRUE
					
						#IF IS_DEBUG_BUILD
							IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 5000)	// iTimeLoadFailSafe)
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_CUTSCENE_STAGE_SETUP - load scene failed advance due to fail safe timer = ", iTimer_NewLoadScene_FailSafe, " : FRAME COUNT : ", GET_FRAME_COUNT())
							ENDIF
						#ENDIF
						
						IF IS_SCREEN_FADED_OUT()
						OR IS_SCREEN_FADING_OUT()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
						ENDIF
						
						CLEAR_PRINTS()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE)
						SET_WIDESCREEN_BORDERS(FALSE, 0)	// Required to display help text for the safehouse tutorial
						DISPLAY_HUD(TRUE) 					// Required to display help text for the safehouse tutorial			
						SET_FRONTEND_RADIO_ACTIVE(FALSE)
						iTimer_Cutscene_Main = GET_GAME_TIMER()
						iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
						iTimer_Cutscene_SkipDelay = GET_GAME_TIMER()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						
						// B* 1475172 - allow cut to go through if player was wanted
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						CLEAR_AREA_OF_COPS(<< -808.79742, 169.31934, 70.95580 >>, 150.0)	// pos = center of property
						CLEAR_AREA_OF_PROJECTILES(<< -808.79742, 169.31934, 70.95580 >>, 100.0)
						
						// set initial cam shot values of garage
						vCamPosition[CAM_INFO_INITIAL] = << -842.1, 178.6, 72.2 >>
						vCamRotation[CAM_INFO_INITIAL] = << 6.6, -0.0, -96.1 >>
						fCamFOV[CAM_INFO_INITIAL] = 39.8
						vCamPosition[CAM_INFO_INTERPOLATE] = << -842.0, 178.8, 72.2 >>
						vCamRotation[CAM_INFO_INTERPOLATE] = << 6.6, -0.0, -96.1 >>
						fCamFOV[CAM_INFO_INTERPOLATE] = 39.8
						vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRotation[CAM_INFO_INITIAL])
						
						// setup the garage shot
						IF NOT DOES_CAM_EXIST(camIndex_Cutscene)
							camIndex_Cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)			
						ENDIF
						IF DOES_CAM_EXIST(camIndex_Cutscene)
							// Slow pan of the exterior, showing the house and garage (linear interp).
							SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INITIAL], vCamRotation[CAM_INFO_INITIAL], fCamFOV[CAM_INFO_INITIAL], iTimerCameraTransition)
							SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INTERPOLATE], vCamRotation[CAM_INFO_INTERPOLATE], fCamFOV[CAM_INFO_INTERPOLATE], iTimerCameraTransition,GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						ENDIF
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						NEW_LOAD_SCENE_STOP()
						
						REPOSITION_PLAYER_FOR_INTRO()

						bIsCutsceneActive = TRUE
						PRINT_HELP("SHI_M_HOUSE_1", iTimerCameraTransition + 10000)	// This is Michael's safehouse, you can save vehicles by parking them in the garage.
						TRIGGER_MUSIC_EVENT("MICHAELS_HOUSE")
						eCutsceneStage = SHI_CUTSCENE_STAGE_GARAGE_SHOT		
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_GARAGE_SHOT, FRAME COUNT : ", GET_FRAME_COUNT())
					//ELSE
					//	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()	
					//		REQUEST_INITIAL_LOAD_SCENE()
					//		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_SETUP - new load scene re-requested in , FRAME COUNT : ", GET_FRAME_COUNT())
					//	ENDIF
					//	CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - waitin on new load scene, FRAME COUNT : ", GET_FRAME_COUNT())
					//ENDIF
				ELSE
					//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - attempting to start cutscene, FRAME COUNT : ", GET_FRAME_COUNT())
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_GARAGE_SHOT
				// set cam shot values for bed room
				vCamPosition[CAM_INFO_INITIAL] = << -810.7, 179.2, 77.3 >>
				vCamRotation[CAM_INFO_INITIAL] = << -10.7, 0.0, 60.7 >>
				fCamFOV[CAM_INFO_INITIAL] = 50.0
				vCamPosition[CAM_INFO_INTERPOLATE] = << -810.7, 179.2, 77.2 >>
				vCamRotation[CAM_INFO_INTERPOLATE] = << -10.7, 0.0, 60.7 >>
				fCamFOV[CAM_INFO_INTERPOLATE] = 50.0
				vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRotation[CAM_INFO_INITIAL])
				fStreamingClip = 12.0

				IF (IS_NEW_LOAD_SCENE_ACTIVE()
				AND IS_NEW_LOAD_SCENE_LOADED()
				AND IS_VALID_INTERIOR(interiorInstanceIndex_MichaelHouse)
				AND IS_VALID_INTERIOR(interiorInstanceIndex_MichaelGarage)
				AND IS_INTERIOR_READY(interiorInstanceIndex_MichaelHouse)
				AND IS_INTERIOR_READY(interiorInstanceIndex_MichaelGarage)
				AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition)
				OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
					
					#IF IS_DEBUG_BUILD
						IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_CUTSCENE_STAGE_GARAGE_SHOT - load scene / interior pin failed advance due to fail safe timer = ", iTimer_NewLoadScene_FailSafe, " : FRAME COUNT : ", GET_FRAME_COUNT())
						ENDIF
					#ENDIF
					
					// setup the save game shot
					IF DOES_CAM_EXIST(camIndex_Cutscene)
						// wait for the interior to be loaded before changing the shot
						//IF IS_INTERIOR_PINNED_IN_MEMORY(interiorInstanceIndex_MichaelHouse)
						//AND IS_INTERIOR_PINNED_IN_MEMORY(interiorInstanceIndex_MichaelGarage)
						
							SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INITIAL], vCamRotation[CAM_INFO_INITIAL], fCamFOV[CAM_INFO_INITIAL], iTimerCameraTransition)
							SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INTERPOLATE], vCamRotation[CAM_INFO_INTERPOLATE], fCamFOV[CAM_INFO_INTERPOLATE], iTimerCameraTransition, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
							
							NEW_LOAD_SCENE_STOP()
							PRINT_HELP("SHI_M_HOUSE_2", iTimerCameraTransition + 10000)	// Sleep in the bed to save your game.
							iTimer_Cutscene_Main = GET_GAME_TIMER()
							iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
							eCutsceneStage = SHI_CUTSCENE_STAGE_SAVE_GAME_SHOT
							//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_SAVE_GAME_SHOT, FRAME COUNT : ", GET_FRAME_COUNT())
						//ELSE
						//	//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_GARAGE_SHOT - waiting on interior pinning to memorty , FRAME COUNT : ", GET_FRAME_COUNT())
						//ENDIF
					ENDIF
				ELSE
					// Set the next shot loading
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()					
						NEW_LOAD_SCENE_START(vCamPosition[CAM_INFO_INITIAL], vLoadSceneDir, fStreamingClip, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - NEW_LOAD_SCENE_START for bedroom ", " Pos : ", vCamPosition[CAM_INFO_INITIAL],
										" vLoadSceneDir : ", vLoadSceneDir, " fStreamingFarClip : ", fStreamingClip)	
					ENDIF
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_SAVE_GAME_SHOT
				// set cam shot values for wardrobe
				vCamPosition[CAM_INFO_INITIAL] = << -814.4, 174.1, 76.7 >>
				vCamRotation[CAM_INFO_INITIAL] = << -1.2, -0.0, -70.6 >>
				fCamFOV[CAM_INFO_INITIAL] = 50.1
				vCamPosition[CAM_INFO_INTERPOLATE] = << -814.4, 174.1, 76.7 >>
				vCamRotation[CAM_INFO_INTERPOLATE] = << 18.9, -0.0, -70.6 >>
				fCamFOV[CAM_INFO_INTERPOLATE] = 50.1
				vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRotation[CAM_INFO_INITIAL])
				fStreamingClip = 10.0
				
				IF (IS_NEW_LOAD_SCENE_ACTIVE()
				AND IS_NEW_LOAD_SCENE_LOADED()
				AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition)
				OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
					
					#IF IS_DEBUG_BUILD
						IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_CUTSCENE_STAGE_GARAGE_SHOT - load scene / interior pin failed advance due to fail safe timer = ", iTimer_NewLoadScene_FailSafe, " : FRAME COUNT : ", GET_FRAME_COUNT())
						ENDIF
					#ENDIF
					
					// setup the wardrobe shot
					IF DOES_CAM_EXIST(camIndex_Cutscene)
						
						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INITIAL], vCamRotation[CAM_INFO_INITIAL], fCamFOV[CAM_INFO_INITIAL], iTimerCameraTransition)
						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INTERPOLATE], vCamRotation[CAM_INFO_INTERPOLATE], fCamFOV[CAM_INFO_INTERPOLATE], iTimerCameraTransition, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
						
						NEW_LOAD_SCENE_STOP()
						
						PRINT_HELP("SHI_M_HOUSE_3", iTimerCameraTransition + 10000)	// You can change clothes in the wardrobe.
						iTimer_Cutscene_Main = GET_GAME_TIMER()
						iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
						eCutsceneStage = SHI_CUTSCENE_STAGE_WARDROBE_SHOT
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_WARDROBE_SHOT, FRAME COUNT : ", GET_FRAME_COUNT())
					ENDIF					
				ELSE
					// Set the next shot loading
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()						
						NEW_LOAD_SCENE_START(vCamPosition[CAM_INFO_INITIAL], vLoadSceneDir, fStreamingClip, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - NEW_LOAD_SCENE_START for warbrobe ", " Pos : ", vCamPosition[CAM_INFO_INITIAL],
										" vLoadSceneDir : ", vLoadSceneDir, " fStreamingFarClip : ", fStreamingClip)	
					ENDIF
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_WARDROBE_SHOT
				// set cam shot values for living room
				vCamPosition[CAM_INFO_INITIAL] = << -803.8, 168.2, 73.4 >>
				vCamRotation[CAM_INFO_INITIAL] = << -10.5, 0.0, -18.4 >>
				fCamFOV[CAM_INFO_INITIAL] = 54.0
				vCamPosition[CAM_INFO_INTERPOLATE] = << -803.8, 168.3, 73.6 >>
				vCamRotation[CAM_INFO_INTERPOLATE] = << -10.5, 0.0, -18.4 >>
				fCamFOV[CAM_INFO_INTERPOLATE] = 54.0
				vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRotation[CAM_INFO_INITIAL])
				fStreamingClip = 25.0
				
				IF (IS_NEW_LOAD_SCENE_ACTIVE()
				AND IS_NEW_LOAD_SCENE_LOADED()
				AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition)
				OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
					
					#IF IS_DEBUG_BUILD
						IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_CUTSCENE_STAGE_GARAGE_SHOT - load scene / interior pin failed advance due to fail safe timer = ", iTimer_NewLoadScene_FailSafe, " : FRAME COUNT : ", GET_FRAME_COUNT())
						ENDIF
					#ENDIF
					
					// setup the living room shot
					IF DOES_CAM_EXIST(camIndex_Cutscene)

						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INITIAL], vCamRotation[CAM_INFO_INITIAL], fCamFOV[CAM_INFO_INITIAL], (iTimerCameraTransition / 2))
						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INTERPOLATE], vCamRotation[CAM_INFO_INTERPOLATE], fCamFOV[CAM_INFO_INTERPOLATE], (iTimerCameraTransition / 2), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					
						NEW_LOAD_SCENE_STOP()

						PRINT_HELP("SHI_M_HOUSE_4", iTimerCameraTransition + 10000)	// Walk around the house to find various activities to take part in.
						iTimer_Cutscene_Main = GET_GAME_TIMER()
						iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
						eCutsceneStage = SHI_CUTSCENE_STAGE_LIVING_ROOM_SHOT
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_LIVING_ROOM_SHOT, FRAME COUNT : ", GET_FRAME_COUNT())
					ENDIF
				ELSE
					// Set the next shot loading
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()						
						NEW_LOAD_SCENE_START(vCamPosition[CAM_INFO_INITIAL], vLoadSceneDir, fStreamingClip, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - NEW_LOAD_SCENE_START for warbrobe ", " Pos : ", vCamPosition[CAM_INFO_INITIAL],
										" vLoadSceneDir : ", vLoadSceneDir, " fStreamingFarClip : ", fStreamingClip)	
					ENDIF
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_LIVING_ROOM_SHOT
				// set cam shot values for kitchen
				vCamPosition[CAM_INFO_INITIAL] = <<-803.5740, 181.4357, 73.2662>>
				vCamRotation[CAM_INFO_INITIAL] = <<-8.1845, 0.0000, -48.1518>>
				fCamFOV[CAM_INFO_INITIAL] = 41.6
				vCamPosition[CAM_INFO_INTERPOLATE] = <<-803.6697, 181.5426, 73.2662>>
				vCamRotation[CAM_INFO_INTERPOLATE] = <<-8.1845, 0.0000, -48.1518>>
				fCamFOV[CAM_INFO_INTERPOLATE] = 41.6
				vLoadSceneDir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRotation[CAM_INFO_INITIAL])
				fStreamingClip = 12.0
								
				IF (IS_NEW_LOAD_SCENE_ACTIVE()
				AND IS_NEW_LOAD_SCENE_LOADED()
				AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) > (iTimerCameraTransition / 2)
				OR (GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe > iTimeLoadFailSafe))
				
					#IF IS_DEBUG_BUILD
						IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > iTimeLoadFailSafe)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_CUTSCENE_STAGE_GARAGE_SHOT - load scene / interior pin failed advance due to fail safe timer = ", iTimer_NewLoadScene_FailSafe, " : FRAME COUNT : ", GET_FRAME_COUNT())
						ENDIF
					#ENDIF
					
					// setup the kitchen shot
					IF DOES_CAM_EXIST(camIndex_Cutscene)
						
						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INITIAL], vCamRotation[CAM_INFO_INITIAL], fCamFOV[CAM_INFO_INITIAL], (iTimerCameraTransition / 2))
						SET_CAM_PARAMS(camIndex_Cutscene, vCamPosition[CAM_INFO_INTERPOLATE], vCamRotation[CAM_INFO_INTERPOLATE], fCamFOV[CAM_INFO_INTERPOLATE], (iTimerCameraTransition / 2), GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
					
						NEW_LOAD_SCENE_STOP()
						
						// iTimer_Cutscene_Main = GET_GAME_TIMER() // B*1438141 - don't reset the timer here since the load scene might have taken longer than half the scene
						iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
						
						eCutsceneStage = SHI_CUTSCENE_STAGE_KITCHEN_SHOT
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_KITCHEN_SHOT, FRAME COUNT : ", GET_FRAME_COUNT())
					ENDIF					
				ELSE
					// Set the next shot loading
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()						
						NEW_LOAD_SCENE_START(vCamPosition[CAM_INFO_INITIAL], vLoadSceneDir, fStreamingClip, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - NEW_LOAD_SCENE_START for warbrobe ", " Pos : ", vCamPosition[CAM_INFO_INITIAL],
										" vLoadSceneDir : ", vLoadSceneDir, " fStreamingFarClip : ", fStreamingClip)	
					ENDIF
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_KITCHEN_SHOT
				// B*2057708 - need to manually apply the first person transition flash 300ms before cut from scripted cams
				IF bAppliedFirstPersonTransitionFlash = FALSE
					IF ((GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
					AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) >= (iTimerCameraTransition - 300))
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_KITCHEN_SHOT, trigger first person transition flash FRAME COUNT : ", GET_FRAME_COUNT())
						bAppliedFirstPersonTransitionFlash = TRUE
					ENDIF				
				ENDIF
				IF (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition
					eCutsceneStage = SHI_CUTSCENE_STAGE_CLEANUP
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_CLEANUP, FRAME COUNT : ", GET_FRAME_COUNT())
				ENDIF
			BREAK
		
			CASE SHI_CUTSCENE_STAGE_CLEANUP
				SET_PLAYER_LOCKED_IN_TO_INTRO(FALSE)
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP(TRUE)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				RC_END_CUTSCENE_MODE()		
				RESET_ADAPTATION()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF DOES_CAM_EXIST(camIndex_Cutscene)
					SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
					DESTROY_CAM	(camIndex_Cutscene)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
				IF bRequested_SafehouseInterior_Pin_To_Memory
					IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelHouse)
						UNPIN_INTERIOR(interiorInstanceIndex_MichaelHouse)
					ENDIF				
					bRequested_SafehouseInterior_Pin_To_Memory = FALSE
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE : cleanup : bRequested_SafehouseInterior_Pin_To_Memory = FALSE: frame = ", GET_FRAME_COUNT())
				ENDIF
				IF bRequested_GarageInterior_Pin_To_Memory
					IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelGarage)
						UNPIN_INTERIOR(interiorInstanceIndex_MichaelGarage)
					ENDIF					
					bRequested_GarageInterior_Pin_To_Memory = FALSE
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE : cleanup : bRequested_GarageInterior_Pin_To_Memory = FALSE: frame = ", GET_FRAME_COUNT())
				ENDIF
				NEW_LOAD_SCENE_STOP()
				TRIGGER_MUSIC_EVENT("MICHAELS_HOUSE_STOP")
				bIsCutsceneActive = FALSE
				//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - cutscene finished")
				eSafehouseIntroState = SHI_STATE_COMPLETE
			BREAK
		ENDSWITCH
		
		//handle cutscene skip
		IF eCutsceneStage > SHI_CUTSCENE_STAGE_SETUP
		AND eCutsceneStage < SHI_CUTSCENE_STAGE_CLEANUP

			HIDE_HUD_AND_RADAR_THIS_FRAME()	
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
			
			IF NOT bDebounceSkipButton
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					IF (GET_GAME_TIMER() - iTimer_Cutscene_SkipDelay) > 1000	// don't allow instant skip to prevent accidental skip
						bDebounceSkipButton = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - cutscene skipped")
					eCutsceneStage = SHI_CUTSCENE_STAGE_CLEANUP
				ENDIF
			ENDIF
		
		ENDIF
		
	ENDIF
ENDPROC


/// PURPOSE:
///    unpins interiorInstanceIndex_MichaelHouse & interiorInstanceIndex_MichaelGarage
///    if bRequested_SafehouseInterior_Pin_To_Memory / bRequested_GarageInterior_Pin_To_Memory is TRUE
PROC UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY()
	IF bRequested_SafehouseInterior_Pin_To_Memory
		IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelHouse)
			UNPIN_INTERIOR(interiorInstanceIndex_MichaelHouse)
		ENDIF 
		bRequested_SafehouseInterior_Pin_To_Memory = FALSE
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY : unpinned, bRequested_SafehouseInterior_Pin_To_Memory set FALSE: frame = ", GET_FRAME_COUNT())
	ENDIF
	IF bRequested_GarageInterior_Pin_To_Memory
		IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelGarage)
			UNPIN_INTERIOR(interiorInstanceIndex_MichaelGarage)
		ENDIF
		bRequested_GarageInterior_Pin_To_Memory = FALSE
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY : unpinned, bRequested_GarageInterior_Pin_To_Memory set FALSE: frame = ", GET_FRAME_COUNT())
	ENDIF
ENDPROC

/// PURPOSE:
///    Does any necessary cleanup and terminates the script's thread.
PROC Script_Cleanup()
	// only do the cutscene cleanup if the mission candidate ID was set
	IF iMissionCandidateID != NO_CANDIDATE_ID
		IF bIsCutsceneActive
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			RC_END_CUTSCENE_MODE()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			IF DOES_CAM_EXIST(camIndex_Cutscene)
				SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
				DESTROY_CAM	(camIndex_Cutscene)
			ENDIF
			NEW_LOAD_SCENE_STOP()
			TRIGGER_MUSIC_EVENT("MICHAELS_HOUSE_STOP")
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			//CPRINTLN(DEBUG_AMBIENT, "SH_Intro_M_Home.sc : Script Cleanup - bIsCutsceneActive cleanup done")
		ENDIF
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " MISSION_OVER(iMissionCandidateID) : iMissionCandidateID = ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
		MISSION_OVER(iMissionCandidateID)
	ENDIF
	UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY()
	
	//Ensure all world brains reactivate. Ensures minigame and object triggers near savehouses
	//aren't blocked after the safehouse intro.
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	CPRINTLN(DEBUG_AMBIENT, " SH_Intro_M_Home.sc : Terminate thread - Script Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			//CPRINTLN(DEBUG_AMBIENT, "SH_Intro_M_Home.sc : script terminated")
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

SCRIPT
	// ensure the script is going to be relaunched on a new sp session (so this comes before HAS_FORCE_CLEANUP_OCCURRED)
	Register_Script_To_Relaunch_List(LAUNCH_BIT_SH_INTRO_M_HOME)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " forced cleanup occured")	
		Script_Cleanup()
	ENDIF
	
	// Hold up whilst updating gameflow via debug menu
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			WAIT(0)
		ENDIF
	#ENDIF
	
	VECTOR vPlayerPos
	eSafehouseIntroState = SHI_STATE_INITIAL_OUT_OF_AREA_CHECK	
	iMissionCandidateID = NO_CANDIDATE_ID
	
	bSceneHasBeenPreloaded = FALSE
	
	//Michael's safehouse interior needs pinning to memory ready for the cutscene
	interiorInstanceIndex_MichaelHouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-809.1374, 179.9676, 71.1531>>, "v_michael")
	interiorInstanceIndex_MichaelGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-812.1, 188.2, 73.2>>, "v_michael_garage")	
	bRequested_SafehouseInterior_Pin_To_Memory = FALSE
	bRequested_GarageInterior_Pin_To_Memory = FALSE
	
	//Initialise Michael's home area Poly Check
	OPEN_TEST_POLY(tpMichaelHomeArea)		//from north point at the main gate running clockwise
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -844.14935, 162.55867, 65.89324 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -849.66248, 166.86949, 66.54545 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -846.06409, 198.59845, 76.91820 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -796.52472, 194.14273, 77.94861 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -774.87134, 195.20262, 77.13674 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -768.13275, 190.18369, 76.24245 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -761.38745, 139.44395, 66.47416 >>)	// tennis court corner
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -764.73773, 137.70203, 66.47416 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -800.03119, 141.21796, 61.96946 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -851.14587, 139.22386, 60.13711 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -850.16095, 150.52716, 64.38181 >>)
		ADD_TEST_POLY_VERT(tpMichaelHomeArea, << -844.14899, 155.82861, 65.77842 >>)		
	CLOSE_TEST_POLY(tpMichaelHomeArea)	//must be called once verts have been added.
	
	CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " initialised")
	
	WHILE TRUE

		SWITCH eSafehouseIntroState
			
			// B*1301796 - Player loaded an autosave after completing Armenian 3 and got the safehouse tutorial then SWITCHED away to Michael getting pills for Amanda (see video)
			//				ensure the player has left the area initially
			CASE SHI_STATE_INITIAL_OUT_OF_AREA_CHECK
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())

					// wait for the player to have been initial spotted by Alwyn's script
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) <= 0)
					
						vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
						IF NOT IS_POINT_IN_POLY_2D(tpMichaelHomeArea, vPlayerPos)	
							eSafehouseIntroState = SHI_STATE_TRIGGER_PRELOAD	
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_INITIAL_OUT_OF_AREA_CHECK -> ", "SHI_STATE_TRIGGER_INTRO : frame = ", GET_FRAME_COUNT())
						ELSE
							// player is in the property delay the recheck
							//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_INITIAL_OUT_OF_AREA_CHECK - ", "inside property delay recheck")							
							WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_LONG)	
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//Try and stream in the area around Micahel's house as the player approaches the intro zone.
			//We'll test to see if it's loaded just before the cutscene starts, if it hasn't we'll emergency 
			//fade out to avoid seeing popins on the first cut.
			CASE SHI_STATE_TRIGGER_PRELOAD
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-871.925903,162.000549,52.992241>>, <<-741.594849,155.087280,113.025475>>, 90.000000)
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Player entered initial streaming area.")
						
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Stopped previously running new load scene.")
							NEW_LOAD_SCENE_STOP()
						ENDIF
						
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Starting new load scene for Michael house exterior.")
						NEW_LOAD_SCENE_START_SPHERE(<<-817.6152, 178.5613, 71.2275>>, 200.0)
						iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
						eSafehouseIntroState = SHI_STATE_TRIGGER_INTRO
					ENDIF
				ENDIF
			BREAK
			
			// Wait for the player to enter the safehouse area to trigger the intro
			CASE SHI_STATE_TRIGGER_INTRO
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					IF NOT bLockedInToIntro
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-871.925903,162.000549,52.992241>>, <<-741.594849,155.087280,113.025475>>, 90.000000)
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Player left initial streaming area.")
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Stopped previously running new load scene.")
								NEW_LOAD_SCENE_STOP()
							ENDIF
							eSafehouseIntroState = SHI_STATE_TRIGGER_PRELOAD
						ENDIF
					ENDIF
				
					VECTOR vCentreProperty 
					vCentreProperty = << -808.79742, 169.31934, 70.95580 >>	// 80.95580 is height on top of the house, now using ground level
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					
					// initial check to see if player is close to the property's grounds before doing more expensive poly check	
					IF IS_COORD_IN_RANGE_OF_COORD_2D(vPlayerPos, vCentreProperty, 100.0) OR bLockedInToIntro	// N*1263203 increased from 90 to aid interior pinning
							
						// check the game state is safe to launch the cutscene e.g. not on mission
						IF IS_MICHAEL_HOME_INTRO_SAFE_TO_PLAY() OR bLockedInToIntro
						
							//pin the interior into memory ready for the cutscene (including exterior shot where light could be seen turning on)
							// do this prior to the height check since you can approach from a low level
							IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelHouse)
								IF NOT IS_INTERIOR_READY(interiorInstanceIndex_MichaelHouse)	
									PIN_INTERIOR_IN_MEMORY(interiorInstanceIndex_MichaelHouse)	
									bRequested_SafehouseInterior_Pin_To_Memory = TRUE
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_SETUP - house interior pinned in memory request. frame = ", GET_FRAME_COUNT())
								ENDIF
							ENDIF
							// B*1469856 - garage is seperate interior so needs pinning aswell
							IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelGarage)
								IF NOT IS_INTERIOR_READY(interiorInstanceIndex_MichaelGarage)	
									PIN_INTERIOR_IN_MEMORY(interiorInstanceIndex_MichaelGarage)	
									bRequested_GarageInterior_Pin_To_Memory = TRUE
									CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_SETUP - garage interior pinned in memory request. frame = ", GET_FRAME_COUNT())
								ENDIF
							ENDIF
							
							IF g_bPlayerIsInTaxi
							// B* 2048926 - arrived in a taxi (skip), already preloaded scene.
								bSceneHasBeenPreloaded = TRUE
							ENDIF
							
							// height check to make sure the player isn't just flying over the property
							IF (vPlayerPos.Z > 59.0	AND vPlayerPos.Z < 90.0) OR bLockedInToIntro
												
								// more specific check to see when the player enter's the property's grounds
								IF IS_POINT_IN_POLY_2D(tpMichaelHomeArea, vPlayerPos) OR bLockedInToIntro
								
									// once we get past the request, nothing should interfere with the safehouse intro playing out.
									IF REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO() OR bLockedInToIntro
									
										IF NOT bLockedInToIntro
											SET_PLAYER_LOCKED_IN_TO_INTRO(TRUE)
										ENDIF
											
										IF IS_NEW_LOAD_SCENE_ACTIVE() OR bSceneHasBeenPreloaded 
											IF bSceneHasBeenPreloaded OR IS_NEW_LOAD_SCENE_LOADED() OR (GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 12000
												CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Michael's house exterior was loaded.")
												NEW_LOAD_SCENE_STOP()	// stop existing load scene ready for my request
												iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
												
												SET_PLAYER_LOCKED_IN_TO_INTRO(TRUE)
												
												// try skipping over load scene of exterior shot as per bug 1188935
												eCutsceneStage = SHI_CUTSCENE_STAGE_SETUP
												bIsCutsceneActive = FALSE
												iTimer_Cutscene_Main = GET_GAME_TIMER()
												iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
							
												eSafehouseIntroState = SHI_STATE_DELAY_FOR_INTERIOR_PINNING	
												CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO -> ", "SHI_STATE_DELAY_FOR_INTERIOR_PINNING : frame = ", GET_FRAME_COUNT())
											ELSE
												IF NOT IS_SCREEN_FADING_OUT()
												OR IS_SCREEN_FADED_OUT()
													CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Michael's house exterior did not load in time. Running emergency fade out.")
													DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
												ENDIF
												SET_PLAYER_LOCKED_IN_TO_INTRO(TRUE)
											ENDIF
										ELSE
											IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
												CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "Michael's house exterior new load scene not active. Starting now.")
												NEW_LOAD_SCENE_START_SPHERE(<<-817.6152, 178.5613, 71.2275>>, 200.0)
												iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
											ELSE
												// delay switch in progress so delay recheck
												//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 5 : delay switch in progress")							
												WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									// player isn't in trigger area so delay recheck
									//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 4 : delay close but not inside property")							
									WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)									
								ENDIF
							ELSE
								// player isn't in trigger area Z height check so delay recheck																
								//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 3 : close to property but z value is out of range")	
								WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)
							ENDIF
						ELSE
							// intro isn't safe to trigger so unpin interior if this script pinned it
							UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY()
							//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 2 : delay close but not inside property")		
							WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)
						ENDIF						
					ELSE						
						// player isn't in trigger area so delay recheck
						UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY()
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 1: delay not close to property")
						WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME)
					ENDIF
				ELSE						
					// delay rechecking conditions, so this isn't spamming every frame
					UNPIN_MICHAELS_HOUSE_INTERIOR_FROM_MEMORY()
					//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "delay not safe to play intro")
					WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME)
				ENDIF
			BREAK
			
			CASE SHI_STATE_DELAY_FOR_INTERIOR_PINNING
			
				IF IS_VALID_INTERIOR(interiorInstanceIndex_MichaelHouse)
				AND IS_INTERIOR_READY(interiorInstanceIndex_MichaelHouse)
				AND IS_VALID_INTERIOR(interiorInstanceIndex_MichaelGarage)
				AND IS_INTERIOR_READY(interiorInstanceIndex_MichaelGarage)
				OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 500)	// fail safe for IS_INTERIOR_READY() not returning TRUE quick enough, can't delay long as per B*1188935
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "SHI_STATE_DELAY_FOR_INTERIOR_PINNING - ", " done IS_INTERIOR_READY(interiorInstanceIndex_MichaelHouse) & IS_INTERIOR_READY(interiorInstanceIndex_MichaelGarage) frame = ", IS_INTERIOR_READY(interiorInstanceIndex_MichaelHouse))
					NEW_LOAD_SCENE_STOP()	// stop existing load scene ready for my request
					iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
					eCutsceneStage = SHI_CUTSCENE_STAGE_SETUP
					bIsCutsceneActive = FALSE
					iTimer_Cutscene_Main = GET_GAME_TIMER()
					iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
					eSafehouseIntroState = SHI_STATE_PLAY_INTRO_CUTSCENE	
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_DELAY_FOR_INTERIOR_PINNING -> ", "SHI_STATE_PLAY_INTRO_CUTSCENE : frame = ", GET_FRAME_COUNT())
				ELSE
					PIN_INTERIOR_IN_MEMORY(interiorInstanceIndex_MichaelHouse)
					PIN_INTERIOR_IN_MEMORY(interiorInstanceIndex_MichaelGarage)
					bRequested_SafehouseInterior_Pin_To_Memory = TRUE
					bRequested_GarageInterior_Pin_To_Memory = TRUE
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "SHI_STATE_DELAY_FOR_INTERIOR_PINNING - ", "house & garage interior NOT pinned in memory yet so re-request. frame = ", GET_FRAME_COUNT())
				ENDIF
			BREAK
			
			// Play the intro cutscene
			CASE SHI_STATE_PLAY_INTRO_CUTSCENE
			
				STATE_PLAY_INTRO_CUTSCENE()				
			BREAK
			
			//intro has finished
			CASE SHI_STATE_COMPLETE
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_COMPLETE - ", "finished ready to terminate and removed from relaunch list")
				REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_SH_INTRO_M_HOME)
				SET_BIT(g_iExtraMissionFlags[SP_MISSION_FAMILY_1], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
				SET_BIT(g_iExtraMissionFlags[SP_MISSION_FAMILY_2], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
				SET_BIT(g_iExtraMissionFlags[SP_MISSION_FAMILY_3], MF_EXTRA_INDEX_TEMP_ZERO_LEAVE_AREA)
				Script_Cleanup()
			BREAK
		
		ENDSWITCH	
			
		#IF IS_DEBUG_BUILD
			//IF IS_TEST_POLY_VALID(tpMichaelHomeArea)
			//	DISPLAY_POLY(tpMichaelHomeArea, 0, 150, 0, 100)
			//ENDIF
			//DEBUG_Check_Debug_Keys()
		#ENDIF
		
		WAIT(0)

	ENDWHILE

ENDSCRIPT
