

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "Flow_Mission_Data_Public.sch"
USING "flow_public_game.sch"
USING "flow_help_public.sch"
USING "rc_helper_functions.sch"
USING "rc_area_public.sch"
USING "player_scene_schedule.sch"


//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		MISSION NAME	:	SH_Intro_F_Hills.sc								//			//
//		AUTHOR			:	Ahron Mason													//
//		DESCRIPTION		:	Handles Franklin's hills safehouse intro 					// 
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME		1500	// delay time for rechecking safe conditions
CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT	100	// delay time for rechecking safe conditions
CONST_INT SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_LONG	3000	// delay time for rechecking safe conditions

ENUM SAFEHOUSE_INTRO_STATE_ENUM
	SHI_STATE_INITIAL_OUT_OF_AREA_CHECK,
	SHI_STATE_TRIGGER_INTRO,
	SHI_STATE_PLAY_INTRO_CUTSCENE,
	SHI_STATE_COMPLETE
ENDENUM
SAFEHOUSE_INTRO_STATE_ENUM eSafehouseIntroState = SHI_STATE_TRIGGER_INTRO

ENUM SAFEHOUSE_INTRO_CUTSCENE_STAGE_ENUM
	SHI_CUTSCENE_STAGE_SETUP,
	SHI_CUTSCENE_STAGE_GARAGE_SHOT,
	SHI_CUTSCENE_STAGE_CLEANUP
ENDENUM
SAFEHOUSE_INTRO_CUTSCENE_STAGE_ENUM	eCutsceneStage = SHI_CUTSCENE_STAGE_SETUP

BOOl bIsCutsceneActive
BOOL bAppliedFirstPersonTransitionFlash = FALSE
BOOL bDebounceSkipButton = FALSE
CAMERA_INDEX camIndex_Cutscene
INT iMissionCandidateID = NO_CANDIDATE_ID
INT iTimer_Cutscene_Main
INT iTimer_Cutscene_SkipDelay
INT iTimer_NewLoadScene_FailSafe
TEST_POLY tpFranklinHillsArea

/// PURPOSE:
///    Test for if two coords 2D are within fRange metres of each other
/// PARAMS:
///    v1 - first coord
///    v2 - second coord
///    fRange - distance
/// RETURNS:
///    TRUE if two coords are withing fRange of each other in 2D
FUNC BOOL IS_COORD_IN_RANGE_OF_COORD_2D(VECTOR v1, VECTOR v2, FLOAT fRange)
	VECTOR vDiff = v2 - v1
	RETURN ((vDiff.x * vDiff.x) + (vDiff.y * vDiff.y)) <= (fRange * fRange)
ENDFUNC

/// PURPOSE:
///    uses candidate ID system to try to get permission to run (ensures nothing else will interfere with the intro once running)
/// RETURNS:
///    TRUE if the safehouse intro has permission to run, FALSE otherwise
FUNC BOOL REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO()

	m_enumMissionCandidateReturnValue eLaunchRequest = Request_Mission_Launch(iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY)
	
	#IF IS_DEBUG_BUILD
		IF (eLaunchRequest = MCRET_DENIED)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_DENIED: frame = ", GET_FRAME_COUNT())
		ENDIF
		IF (eLaunchRequest = MCRET_PROCESSING)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_PROCESSING: frame = ", GET_FRAME_COUNT())
		ENDIF
		IF (eLaunchRequest = MCRET_ACCEPTED)
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO: ", "MCRET_ACCEPTED: iMissionCandidateID : ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
		ENDIF
	#ENDIF
	
	IF eLaunchRequest = MCRET_ACCEPTED
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    ignores roll
/// PARAMS:
///    vRot - the roatation to convert
/// RETURNS:
///    direction VECTOR
FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
      RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
	  //RETURN << COS(vRot.Z+90), SIN(vRot.Z+90), 0.0 >>	// original test which only takes into account the vRot z value
ENDFUNC

/// PURPOSE:
///    Check the conditions which mean we shouldn't go ahead with the intro
/// RETURNS:
///    TRUE if the script is safe to continue
FUNC BOOL IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY()
	
	// added to prevent cutscene going through prior to player actually gaining control
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		//CPRINTLN(DEBUG_AMBIENT, "SH_Intro_M_Home.sc : ", "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_PLAYER_PLAYING returned false : frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// check Franklin has unlocked the safehouse (has to be double checked here, since this script is launched when the phonecall which activates the flowflag is queued, rather than when it's finished
	IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT)
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State(FLOWFLAG_FRANKLIN_MOVED_TO_HILLS_APARTMENT) is false.: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// only do the intro if the player is Franklin
	IF (GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN)
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as Get_Mission_Flow_Flag_State: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	IF g_bIsOnRampage
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bIsOnRampage is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF

	/*IF g_bTriggerSceneActive //removed since this prevents the safehouse intros playing if a mission blip is in the house grounds.
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bTriggerSceneActive returned TRUE: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF*/
	
	IF g_bMissionOverStatTrigger
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bMissionOverStatTrigger is set: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// point in the lead in where the player can't back out of a mission trigger
	IF g_bPlayerLockedInToTrigger
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied as g_bPlayerLockedInToTrigger is set: frame = ", GET_FRAME_COUNT())	
		RETURN FALSE
	ENDIF
	
	/* B*1475172 - cut allowed to go through with wanted rating now
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied player has wanted level is TRUE: frame = ", GET_FRAME_COUNT())
			RETURN FALSE
		ENDIF
	ENDIF*/
	
	IF IS_CUTSCENE_PLAYING()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied IS_CUTSCENE_PLAYING: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// cutscene has been kicking in during player switches recently
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied IS_PLAYER_SWITCH_IN_PROGRESS.: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// fix issue where cutscene would interrupt timetable scene
	IF Is_Player_Timetable_Scene_In_Progress()
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied Is_Player_Timetable_Scene_In_Progress.: frame = ", GET_FRAME_COUNT())
		RETURN FALSE
	ENDIF
	
	// B*1470522 - allow flow phonecalls to play out, anything lower priority will get interrupted
	IF g_iCallInProgress != -1
		IF g_savedGlobals.sCommsControlData.sQueuedCalls[g_iCallInProgress].sCommData.ePriority >= CPR_MEDIUM
			CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied flow phonecall in progress : frame = ", GET_FRAME_COUNT())
			RETURN FALSE
		ENDIF
	ENDIF
	
	// added to stop a safehouse intro playing during the setup of a debug launched mission
	#IF IS_DEBUG_BUILD
		IF (g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow)
			//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "Script denied g_flowUnsaved.bFlowControllerBusy OR g_flowUnsaved.bUpdatingGameflow): frame = ", GET_FRAME_COUNT())
			RETURN FALSE
		ENDIF
	#ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY: ", "return TRUE : game_timer = ", GET_GAME_TIMER())
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    handle running through the safehouse intro cutscene
PROC STATE_PLAY_INTRO_CUTSCENE()
	INT iTimerCameraTransition = 9000	// DEFAULT_HELP_TEXT_TIME - used to use this but bug wanted second knocking off 1080420
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		SWITCH eCutsceneStage
			CASE SHI_CUTSCENE_STAGE_SETUP
			
				IF RC_IS_CUTSCENE_OK_TO_START(FALSE)	
								
					IF IS_NEW_LOAD_SCENE_ACTIVE()
					AND IS_NEW_LOAD_SCENE_LOADED()
					OR ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 9000)	// fail safe for IS_NEW_LOAD_SCENE_LOADED() not returning TRUE
						
						#IF IS_DEBUG_BUILD
							IF ((GET_GAME_TIMER() - iTimer_NewLoadScene_FailSafe) > 9000)
								CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - IS_NEW_LOAD_SCENE_LOADED timed out, FRAME COUNT : ", GET_FRAME_COUNT())
							ENDIF
						#ENDIF
						
						CLEAR_PRINTS()
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP(TRUE)
						ENDIF						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, FALSE, FALSE, FALSE, FALSE)
						SET_WIDESCREEN_BORDERS(FALSE, 0)	// Required to display help text for the safehouse tutorial
						DISPLAY_HUD(TRUE) 					// Required to display help text for the safehouse tutorial
						SET_FRONTEND_RADIO_ACTIVE(FALSE)
						iTimer_Cutscene_Main = GET_GAME_TIMER()
						iTimer_Cutscene_SkipDelay = GET_GAME_TIMER()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						// B* 1475172 - allow cut to go through if player was wanted
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						CLEAR_AREA_OF_COPS(<< -2.62564, 528.32562, 178.39198 >>, 150.0)	// pos = center of property
						CLEAR_AREA_OF_PROJECTILES(<< -2.62564, 528.32562, 178.39198 >>, 100.0)
						
						IF NOT DOES_CAM_EXIST(camIndex_Cutscene)
							camIndex_Cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)			
						ENDIF
						IF DOES_CAM_EXIST(camIndex_Cutscene)
							// Pan down, use an accel/decel interp.
							SET_CAM_PARAMS(camIndex_Cutscene, << 5.4, 557.3, 180.6 >>, << -7.4, 0.0, 165.7 >>, 44.2, iTimerCameraTransition)
							SET_CAM_PARAMS(camIndex_Cutscene, << 5.3, 557.5, 178.6 >>, << -7.4, -0.0, -136.4 >>, 44.2, iTimerCameraTransition, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
						ENDIF
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						PRINT_HELP("SHI_F_HILLS_1")		// This is Franklin's new safehouse, you can save vehicles by parking them in the garage.
						bIsCutsceneActive = TRUE
						eCutsceneStage = SHI_CUTSCENE_STAGE_GARAGE_SHOT	
					ELSE
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - waiting on  IS_NEW_LOAD_SCENE_LOADED, FRAME COUNT : ", GET_FRAME_COUNT())
					ENDIF
				ELSE
					//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - attempting to start cutscene, FRAME COUNT : ", GET_FRAME_COUNT())
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_GARAGE_SHOT
				// B*2057708 - need to manually apply the first person transition flash 300ms before cut from scripted cams
				IF bAppliedFirstPersonTransitionFlash = FALSE
					IF ((GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON)
					AND (GET_GAME_TIMER() - iTimer_Cutscene_Main) >= (iTimerCameraTransition - 300))
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - SHI_CUTSCENE_STAGE_KITCHEN_SHOT, trigger first person transition flash FRAME COUNT : ", GET_FRAME_COUNT())
						bAppliedFirstPersonTransitionFlash = TRUE
					ENDIF				
				ENDIF
				IF (GET_GAME_TIMER() - iTimer_Cutscene_Main) > iTimerCameraTransition
					eCutsceneStage = SHI_CUTSCENE_STAGE_CLEANUP
					CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "STATE_PLAY_INTRO_CUTSCENE - eCutsceneStage -> SHI_CUTSCENE_STAGE_CLEANUP, FRAME COUNT : ", GET_FRAME_COUNT())
				ENDIF
			BREAK
			
			CASE SHI_CUTSCENE_STAGE_CLEANUP
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP(TRUE)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				RC_END_CUTSCENE_MODE()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF DOES_CAM_EXIST(camIndex_Cutscene)
					SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
					DESTROY_CAM	(camIndex_Cutscene)
				ENDIF
				NEW_LOAD_SCENE_STOP()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)		
				bIsCutsceneActive = FALSE
				//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - cutscene finished")
				eSafehouseIntroState = SHI_STATE_COMPLETE
			BREAK
		ENDSWITCH
		
		//handle cutscene skip
		IF eCutsceneStage > SHI_CUTSCENE_STAGE_SETUP
		AND eCutsceneStage < SHI_CUTSCENE_STAGE_CLEANUP
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()	
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
			
			IF NOT bDebounceSkipButton
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					IF (GET_GAME_TIMER() - iTimer_Cutscene_SkipDelay) > 1000	// don't allow instant skip to prevent accidental skip
						bDebounceSkipButton = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
					//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " STATE_PLAY_INTRO_CUTSCENE - cutscene skipped")
					eCutsceneStage = SHI_CUTSCENE_STAGE_CLEANUP
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Does any necessary cleanup and terminates the script's thread.
PROC Script_Cleanup()
	// only do the cutscene cleanup if the mission candidate ID was set
	IF iMissionCandidateID != NO_CANDIDATE_ID
		IF bIsCutsceneActive
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP(TRUE)
			ENDIF
			RC_END_CUTSCENE_MODE()
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			IF DOES_CAM_EXIST(camIndex_Cutscene)
				SET_CAM_ACTIVE(camIndex_Cutscene, FALSE)
				DESTROY_CAM	(camIndex_Cutscene)
			ENDIF
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)	
			//CPRINTLN(DEBUG_AMBIENT, "SH_Intro_F_Hills.sc : Script Cleanup - bIsCutsceneActive cleanup done")
		ENDIF
		NEW_LOAD_SCENE_STOP()
		CLEAR_FOCUS()
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "MISSION_OVER(iMissionCandidateID) : iMissionCandidateID = ", iMissionCandidateID, " frame = ", GET_FRAME_COUNT())
		MISSION_OVER(iMissionCandidateID)
	ENDIF
	
	//#1460336
	INT iOneOffBit
	IF GET_ONE_OFF_BIT_FOR_PED_REQUEST_SCENE(PR_SCENE_F1_NEWHOUSE, iOneOffBit)
		IF NOT IS_BIT_SET(g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, iOneOffBit)
			SET_BIT(g_SavedGlobals.sPlayerSceneData.g_iSeenOneOffSceneBit, iOneOffBit)
		ENDIF
	ENDIF
	
	//Ensure all world brains reactivate. Ensures minigame and object triggers near savehouses
	//aren't blocked after the safehouse intro.
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	REACTIVATE_ALL_OBJECT_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	
	CPRINTLN(DEBUG_AMBIENT, "SH_Intro_F_Hills.sc : Terminate thread - Script Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			//CPRINTLN(DEBUG_AMBIENT, "SH_Intro_F_Hills.sc : script terminated")
			Script_Cleanup()
		ENDIF
	ENDPROC
#ENDIF

SCRIPT	
	// ensure the script is going to be relaunched on a new sp session (so this comes before HAS_FORCE_CLEANUP_OCCURRED)
	Register_Script_To_Relaunch_List(LAUNCH_BIT_SH_INTRO_F_HILLS)

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "forced cleanup occured")	
		Script_Cleanup()
	ENDIF
	
	// Hold up whilst updating gameflow via debug menu
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			WAIT(0)
		ENDIF
	#ENDIF
		
	VECTOR vPlayerPos
	eSafehouseIntroState = SHI_STATE_INITIAL_OUT_OF_AREA_CHECK	
	iMissionCandidateID = NO_CANDIDATE_ID
	
	//Initialise Franklin's hills apartment area Poly Check
	OPEN_TEST_POLY(tpFranklinHillsArea)		//from most north east point heading clockwise
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 18.04353, 554.79254, 176.23518 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 20.19232, 549.81354, 175.03096 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 28.21084, 545.16644, 175.22356 >>)
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 29.46619, 542.40906, 175.22392 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 28.43769, 540.62604, 175.22446 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 30.00231, 537.15118, 175.54428 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 25.26361, 534.65143, 171.22798 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << 31.40030, 521.60004, 169.62772 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -5.66947, 504.50677, 169.62770 >>)	// pass the pool
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -10.31680, 506.92307, 169.62770 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -11.23373, 508.84964, 169.62770 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -17.29933, 512.38690, 174.62801 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -25.41821, 529.34204, 170.86879 >>)	
		ADD_TEST_POLY_VERT(tpFranklinHillsArea, << -12.27683, 534.62604, 171.22781 >>)	
	CLOSE_TEST_POLY(tpFranklinHillsArea)	//must be called once verts have been added.
	
	CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "initialised")
	
	WHILE TRUE
	
		SWITCH eSafehouseIntroState
			
			// B*1301796 - ensure the player has left the area initially
			CASE SHI_STATE_INITIAL_OUT_OF_AREA_CHECK
			
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())

					// wait for the player to have been initial spotted by Alwyn's script
					IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("startup_positioning")) <= 0)
					
						vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						
						IF NOT IS_POINT_IN_POLY_2D(tpFranklinHillsArea, vPlayerPos)	
							eSafehouseIntroState = SHI_STATE_TRIGGER_INTRO	
							CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_INITIAL_OUT_OF_AREA_CHECK -> ", "SHI_STATE_TRIGGER_INTRO : frame = ", GET_FRAME_COUNT())
						ELSE
							// player is in the property delay the recheck
							//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_INITIAL_OUT_OF_AREA_CHECK - ", "inside property delay recheck")							
							WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_LONG)	
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			// Wait for the player to enter the safehouse area to trigger the intro
			CASE SHI_STATE_TRIGGER_INTRO
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					VECTOR vCentreProperty 
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					vCentreProperty = << -2.62564, 528.32562, 178.39198 >>
						
					// initial check to see if player is close to the property's grounds before doing more expensive poly check	
					IF IS_COORD_IN_RANGE_OF_COORD_2D(vPlayerPos, vCentreProperty, 65.0)
					
						// height check to make sure the player isn't just flying over the property
						IF vPlayerPos.Z > 168.0
						AND vPlayerPos.Z < 181.0
								
							// check the game state is safe to launch the cutscene e.g. not on mission
							IF IS_FRANKLIN_HILLS_INTRO_SAFE_TO_PLAY()
						
								// more specific check to see when the player enter's the property's grounds
								IF IS_POINT_IN_POLY_2D(tpFranklinHillsArea, vPlayerPos)	
								
									// once we get in here, nothing should interfere with the safehouse intro playing out.
									IF REQUEST_PERMISSION_TO_RUN_SAFEHOUSE_INTRO()
									
										g_bSafehouseTutorialIsActive = TRUE // This flag controls deleting Chop while the tutorial is running
										eCutsceneStage = SHI_CUTSCENE_STAGE_SETUP	
										bIsCutsceneActive = FALSE
										iTimer_NewLoadScene_FailSafe = GET_GAME_TIMER()
										// start streaming in the first camera shot area
										VECTOR vInitialCamPos
										vInitialCamPos = << 5.4, 557.3, 180.6 >>
										FLOAT fInitialCamHeading
										fInitialCamHeading = 227.6652
										NEW_LOAD_SCENE_START(vInitialCamPos, <<COS(fInitialCamHeading+90), SIN(fInitialCamHeading+90), 0>>, 25.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)	// added longswitch flag since it appears to improving streaming issues)
										eSafehouseIntroState = SHI_STATE_PLAY_INTRO_CUTSCENE									
										CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "SHI_STATE_TRIGGER_INTRO -> ", "SHI_STATE_PLAY_INTRO_CUTSCENE")
									ENDIF
								ELSE
									// player isn't in trigger area so delay recheck
									//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "SHI_STATE_TRIGGER_INTRO - ", "step 4 : close to propert but not in trigger area")							
									WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)									
								ENDIF
							ELSE								
								// intro isn't safe to trigger
								//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 3 : intro isn't safe to trigger")								
								WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)
							ENDIF
						ELSE							
							// player isn't in trigger area Z height check so delay recheck
							//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 2 : delay close but Z height outside range")								
							WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME_SHORT)
						ENDIF
					ELSE
						// player isn't in trigger area so delay recheck
						//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "step 1: delay not close to property")
						WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME)
					ENDIF
				ELSE
					// delay rechecking conditions, so this isn't spamming every frame
					//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " SHI_STATE_TRIGGER_INTRO - ", "delay player injured")
					WAIT(SAFEHOUSE_INTRO_RECHECK_DELAY_TIME)
				ENDIF
			BREAK
			
			// Play the intro cutscene
			CASE SHI_STATE_PLAY_INTRO_CUTSCENE
			
				STATE_PLAY_INTRO_CUTSCENE()
			BREAK
				
			CASE SHI_STATE_COMPLETE
			
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "SHI_STATE_COMPLETE - ", "finished ready to terminate and removed from relaunch list")
				REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_SH_INTRO_F_HILLS)	
				g_bSafehouseTutorialIsActive = FALSE // This flag controls deleting Chop while the tutorial is running
				Script_Cleanup()
			BREAK
		
		ENDSWITCH
		
		/*#IF IS_DEBUG_BUILD
			IF IS_TEST_POLY_VALID(tpFranklinHillsArea)
				DISPLAY_POLY(tpFranklinHillsArea, 150, 0, 0, 100)
			ENDIF
			//DEBUG_Check_Debug_Keys()
		#ENDIF*/
		
		WAIT(0)

	ENDWHILE

ENDSCRIPT
