// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	FairgroundHub.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	This Controls Entry and Exit of Fairground
//
// *****************************************************************************************
// *****************************************************************************************
 
//----------------------
//	INCLUDES
//----------------------
USING "cost_halo.sch"
USING "Fairground_Shared.sch"

//----------------------
//	ENUM
//----------------------

//----------------------
//	CONSTANTS
//----------------------
CONST_INT RIDE_BIGWHEEL  	0
CONST_INT RIDE_RCOASTER		1
CONST_INT MAX_RIDES			2

CONST_FLOAT RIDE_TRIGGER_DIST 	2.0
CONST_FLOAT RIDE_TRIGGER_PLUS	0.25

//----------------------
//	STRUCT
//----------------------

//----------------------
//	VARIABLES
//----------------------
COSTHALO_HANDLER levHandler
COSTHALO_HANDLER bigHandler
COST_HALO ridelev[1]
COST_HALO ridebig[1]

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_WidgetGroup
	BOOL bQuitScript = FALSE 
	BOOL bWarpToFairground = FALSE
#ENDIF

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Setups Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	//SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	m_WidgetGroup = START_WIDGET_GROUP("Fairground")	
		ADD_WIDGET_BOOL("Quit Script", bQuitScript)
		ADD_WIDGET_BOOL("Warp To Fairground", bWarpToFairground)
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC 

#ENDIF

//----------------------
//	SCRIPT FUNCTIONS
//----------------------


/// PURPOSE:
///    Setups up the script
PROC SCRIPT_SETUP()

	//IF in director mode, don't charge for rides.
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		SETUP_COST_HALO(ridebig[0], 
						HALO_PED, 
						"Bigwheel", 
						GET_POSITION_FROM_STATIC_BLIP(STATIC_BLIP_AMBIENT_FGROUND_BIGWHEEL), 
						0, 
						"", 
						FALSE, 
						6.2)
		SETUP_COST_AREA(ridelev[0], 
						HALO_PED, 
						"Rollercoaster", 
						<<-1651.641113,-1134.324951,21.903982>>, 
						<<-1640.984497,-1121.590332,16.503132>>, 
						6.750000, 
						0, 
						"", 
						FALSE)
	ELSE
		SETUP_COST_HALO(ridebig[0], 
						HALO_PED, 
						"Bigwheel", 
						GET_POSITION_FROM_STATIC_BLIP(STATIC_BLIP_AMBIENT_FGROUND_BIGWHEEL), 
						COST_BIGWHEEL, 
						"", 
						FALSE, 
						6.2)
		SETUP_COST_AREA(ridelev[0], 
						HALO_PED, 
						"Rollercoaster", 
						<<-1651.641113,-1134.324951,21.903982>>, 
						<<-1640.984497,-1121.590332,16.503132>>, 
						6.750000, 
						COST_ROLLERCOASTER, 
						"", 
						FALSE)
	ENDIF
	
	levHandler.sActivateString = "LEV_RIDEHLP"
	levHandler.sBrokenString = "LEV_BROKEN"
	levHandler.sPoorString = "LEV_NOMONEY"
	levHandler.sWantedString = "LEV_WANTED"
	levHandler.sNoWayString = NULL
	levHandler.bAllowOnMission = TRUE
	
	
	bigHandler.sActivateString = "BIGW_RIDEHLP"
	bigHandler.sBrokenString = "BIGW_BROKEN"
	bigHandler.sPoorString = "BIGW_NOMONEY"
	bigHandler.sWantedString = "BIGW_WANTED"
	bigHandler.sNoWayString = NULL
	levHandler.bAllowOnMission = TRUE
ENDPROC

/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	SHUTDOWN_COST_HALO(ridebig[0])
	SHUTDOWN_COST_HALO(ridelev[0])
	CLEAR_COSTHALO_HANDLER_HELP(levHandler)
	CLEAR_COSTHALO_HANDLER_HELP(bigHandler)
	
	CPRINTLN(DEBUG_AMBIENT, "Fairground - Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	UPDATE FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    		Updates Debug Widgets and draw debug stuff
PROC UPDATE_DEBUG_WIDGETS()
	IF (bQuitScript)
		SCRIPT_CLEANUP()
	ENDIF
	
	IF (bWarpToFairground)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1658.7, -1101.7, 13.5>>)
		bWarpToFairground = FALSE
	ENDIF
ENDPROC

#ENDIF

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(coords_struct in_coords)
	INT i
	VECTOR vInCoords = in_coords.vec_coord[0] 	// Update world point
	OBJECT_INDEX rollerCarTurnOffProp[4]
	// just to stop asserts
	IS_VECTOR_ZERO(vInCoords)
	
	CPRINTLN(DEBUG_AMBIENT, "FAIRGROUND SETUP - 101")
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY|FORCE_CLEANUP_FLAG_DIRECTOR)
		SCRIPT_CLEANUP()
	ENDIF
	
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("fairgroundhub")) > 1
    	CPRINTLN(DEBUG_AMBIENT, "Fairground is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	// Load Clifford Version
	IF g_bLoadedClifford 
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fairgroundhubCLF")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("fairgroundhubCLF"))
			WHILE NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("fairgroundhubCLF"))
				WAIT(0)
			ENDWHILE
			START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("fairgroundhubCLF"),in_coords, SIZE_OF(in_coords),DEFAULT_STACK_SIZE)
		ENDIF
		
		//Shutdown bed save script since we're switching to AGT
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SCRIPT_SETUP()
	WAIT(3000)
	
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF		
	
	IF NOT DOES_ENTITY_EXIST(rollerCarTurnOffProp[0])
		rollerCarTurnOffProp[0] = GET_CLOSEST_OBJECT_OF_TYPE(<<-1643.5242, -1124.6805, 17.4326>>, 0.3, PROP_ROLLER_CAR_01)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(rollerCarTurnOffProp[1])
		rollerCarTurnOffProp[1] = GET_CLOSEST_OBJECT_OF_TYPE(<<-1645.1455, -1126.6127, 17.4326>>, 0.3, PROP_ROLLER_CAR_02)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(rollerCarTurnOffProp[2])
		rollerCarTurnOffProp[2] = GET_CLOSEST_OBJECT_OF_TYPE(<<-1646.7676, -1128.5453, 17.4326>>, 0.3, PROP_ROLLER_CAR_02)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(rollerCarTurnOffProp[3])
		rollerCarTurnOffProp[3] = GET_CLOSEST_OBJECT_OF_TYPE(<<-1648.3896, -1130.4784, 17.4326>>, 0.3, PROP_ROLLER_CAR_02)
	ENDIF
	
	i = 0
	REPEAT COUNT_OF(rollerCarTurnOffProp) i
		IF DOES_ENTITY_EXIST(rollerCarTurnOffProp[i])
			FREEZE_ENTITY_POSITION(rollerCarTurnOffProp[i], TRUE)
			SET_ENTITY_INVINCIBLE(rollerCarTurnOffProp[i], TRUE)
			CPRINTLN(DEBUG_AMBIENT, "Fairground - Roller Car Get:", i)
		ENDIF
	ENDREPEAT
	
	
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID()) 
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF		
		
		IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vInCoords, FALSE) > (RC_BRAIN_ACTIVATION_RANGE_NORMAL + 20.0))
			CPRINTLN(DEBUG_AMBIENT, "Fairground - Terminating Out Of Range")
			SCRIPT_CLEANUP()
		ENDIF	
		
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
				CPRINTLN(DEBUG_AMBIENT, "Fairground - Terminating as we are on a mission")
				SCRIPT_CLEANUP()
			ELIF ridelev[0].iCost = 0
				ridelev[0].iCost = COST_ROLLERCOASTER
				ridebig[0].iCost = COST_BIGWHEEL
				levHandler.bTriggerHelpShown = FALSE
				bigHandler.bTriggerHelpShown = FALSE
			ENDIF
		ENDIF
		
		IF (GET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL) <> BUILDINGSTATE_NORMAL) 
			CPRINTLN(DEBUG_AMBIENT, "Fairground - Restoring Ferris Wheel IPL")
			SET_BUILDING_STATE(BUILDINGNAME_IPL_FERRIS_WHEEL, BUILDINGSTATE_NORMAL) 
		ENDIF
	
		// update halos
		IF UPDATE_COSTHALO_HANDLER(rideLev, levHandler, DEFAULT, DEFAULT, DEFAULT, FRIEND_STACK_SIZE)
			CPRINTLN(DEBUG_AMBIENT, "Fairground - Terminating - Running Ride - Leviathan")
			SCRIPT_CLEANUP()
		ENDIF
		
		// update halos
		IF UPDATE_COSTHALO_HANDLER(rideBig, bigHandler, DEFAULT, DEFAULT, DEFAULT, FRIEND_STACK_SIZE)
			CPRINTLN(DEBUG_AMBIENT, "Fairground - Terminating - Running Ride - Big Wheel")
			SCRIPT_CLEANUP()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
ENDSCRIPT




