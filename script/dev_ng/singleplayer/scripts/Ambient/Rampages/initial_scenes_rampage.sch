//------------------------------------------------------------------------------------------
//
//		FILE NAME		:	initial_scenes_rampage.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Setup for Rampage Cutscenes Goes Here
//
//------------------------------------------------------------------------------------------

//----------------------
//	INCLUDES
//----------------------
USING "globals.sch"
USING "RC_Helper_Functions.sch"
USING "rc_launcher_public.sch"
USING "rgeneral_include.sch"
USING "fake_cellphone_public.sch"
USING "gunclub_shop_private.sch"

VECTOR vHipsterPos = <<1180.969, -402.381, 67.200>>
VECTOR vHipsterRot = <<5.0, 0.0, 57.330>>

FAKE_CELLPHONE_DATA sFakeCellphoneData

INT iFocusPushTimer = -1

//Uncomment these to edit hint camera for rampages
//VECTOR vEntityHint = << 0,0,0 >>
//FLOAT fHintScalar = 0.0
//FLOAT fHintVOffset = 0.0
//FLOAT fHintHOffset = 0.0
//FLOAT fHintPitch = 0.0
//FLOAT fHintFOV = 30
//BOOL bResetFocusCam = FALSE
//BOOL bStartRampage = FALSE

//----------------------
//	STRUCT
//----------------------
	
/// PURPOSE: Tries to block any scenarios Rampage 2 launch area
FUNC SCENARIO_BLOCKING_INDEX Rampage1_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(<<911.534912,3642.110840,31.647379>>, 8.0)
ENDFUNC

/// PURPOSE: Tries to block any scenarios Rampage 2 launch area
FUNC SCENARIO_BLOCKING_INDEX Rampage2_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(<<1182.137451,-400.478699,66.924141>>, 8.0)
ENDFUNC

/// PURPOSE: Tries to block any scenarios Rampage 4 launch area
FUNC SCENARIO_BLOCKING_INDEX Rampage4_Scenario_Blocker()
	RETURN ADD_SCENARIO_BLOCKING_AREA(<<-177.062805,-1679.715454,33.162628>>, <<-157.062805,-1659.715454,37.162628>>)
ENDFUNC

/// PURPOSE:
///    Registers entities we got from the script args with the mocaps
PROC PRESTREAM_RAMPAGE_INTRO_CUTSCENE_PED_VARIATIONS(g_structRCScriptArgs& sData, STRING& sSceneHandles[])
	
	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage Ped Cutscene Variations Now!!")
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID(), GET_ENTITY_MODEL(PLAYER_PED_ID()))
			CPRINTLN(DEBUG_RANDOM_CHAR, "Trevor Rampage Pre Launcher Cutscene Variation Set")
		ENDIF
		
		SWITCH (sData.eMissionID)
			CASE RC_RAMPAGE_1
				CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage 1 Ped Cutscene Variations")
				
				IF IS_ENTITY_ALIVE(sData.pedID[0])
					sSceneHandles[0] = "TriggerHappy_WhiteTrash"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[0], sData.pedID[0])//, GET_ENTITY_MODEL(sData.pedID[0]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[0], "Ped 0")
				ENDIF
	
				IF IS_ENTITY_ALIVE(sData.pedID[1])
					sSceneHandles[1] = "Nervous_WhiteTrash"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[1], sData.pedID[1])//, GET_ENTITY_MODEL(sData.pedID[1]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[1], "Ped 1")
				ENDIF
			BREAK
			
			CASE RC_RAMPAGE_2
				CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage 2 Ped Cutscene Variations")
				
				IF IS_ENTITY_ALIVE(sData.pedID[0])
					sSceneHandles[0] = "Rampage_Hipster"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[0], sData.pedID[0])//, GET_ENTITY_MODEL(sData.pedID[0]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[0], "Ped 0")
				ENDIF
			BREAK
			
			CASE RC_RAMPAGE_3
				CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage 3 Ped Cutscene Variations")
				
				IF IS_ENTITY_ALIVE(sData.pedID[0])
					sSceneHandles[0] = "Armed_Mexican_Goon"
//					SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL, 1, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[0], sData.pedID[0])//, GET_ENTITY_MODEL(sData.pedID[0]))
					SET_CUTSCENE_PED_COMPONENT_VARIATION(sSceneHandles[0], PED_COMP_SPECIAL, 1, 0)
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[0], "Ped 0")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sData.pedID[1])
					sSceneHandles[1] = "Mexican_Goon"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[1], sData.pedID[1])//, GET_ENTITY_MODEL(sData.pedID[1]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[1], "Ped 1")
				ENDIF
			BREAK
		
			CASE RC_RAMPAGE_4
				CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage 4 Ped Cutscene Variations")
				
				IF IS_ENTITY_ALIVE(sData.pedID[0])
					sSceneHandles[0] = "Thug"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[0], sData.pedID[0])//, GET_ENTITY_MODEL(sData.pedID[0]))
					SET_CUTSCENE_PED_COMPONENT_VARIATION( sSceneHandles[0], PED_COMP_DECL, 0, 1 )
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[0], "Ped 0")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sData.pedID[1])
					sSceneHandles[1] = "Armed_Thug"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[1], sData.pedID[1])//, GET_ENTITY_MODEL(sData.pedID[1]))
					SET_CUTSCENE_PED_COMPONENT_VARIATION( sSceneHandles[1], PED_COMP_DECL, 1, 0 )
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[1], "Ped 1")
				ENDIF
			BREAK
			
			CASE RC_RAMPAGE_5
				CPRINTLN(DEBUG_RANDOM_CHAR, "Set Rampage 5 Ped Cutscene Variations")
				
				IF IS_ENTITY_ALIVE(sData.pedID[0])
					sSceneHandles[0] = "Army_Guy_Right"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[0], sData.pedID[0])//, GET_ENTITY_MODEL(sData.pedID[0]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[0], "Ped 0")
				ENDIF
				
				IF IS_ENTITY_ALIVE(sData.pedID[1])
					sSceneHandles[1] = "Army_Guy_Left"
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED(sSceneHandles[1], sData.pedID[1])//, GET_ENTITY_MODEL(sData.pedID[1]))
					CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage Pre Launcher Cutscene Variation Set - Scene Handle:", sSceneHandles[1], "Ped 1")
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: FINALIZE_RAMPAGE_SCENE_PED()
//	PURPOSE: CREATES A SCENE PED
//------------------------------------------------------------------------------
FUNC BOOL FINALIZE_RAMPAGE_SCENE_PED(PED_INDEX &mPed)	
	IF NOT DOES_ENTITY_EXIST(mPed)
		RETURN FALSE 
	ENDIF
	
	SET_PED_MONEY(mPed, 0)
	SET_PED_NAME_DEBUG(mPed, "RAMP:SCENE PED")
	SET_PED_CAN_BE_TARGETTED(mPed, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mPed, TRUE)

	RETURN TRUE // succesfully  ped
ENDFUNC

//------------------------------------------------------------------------------
//	FUNCTION: FINALIZE_RAMPAGE_SCENE_PED()
//	PURPOSE: CREATES A SCENE PED
//------------------------------------------------------------------------------
FUNC BOOL CREATE_RAMPAGE_SCENE_PED(PED_INDEX &mPed, MODEL_NAMES pedmodel, VECTOR pos, FLOAT head, BOOL bGrounded = FALSE)
	IF DOES_ENTITY_EXIST(mPed)
		DELETE_PED(mPed)
		mPed = NULL
	ENDIF
	
	mPed = CREATE_PED(PEDTYPE_MISSION, pedModel, pos, head)
	IF NOT DOES_ENTITY_EXIST(mPed)
		RETURN FALSE 
	ENDIF
	
	IF (bGrounded)
		SET_ENTITY_COORDS_GROUNDED(mPed, pos)
		SET_ENTITY_HEADING(mPed, head)
	ENDIF
		
	SET_PED_MONEY(mPed, 0)
	SET_PED_NAME_DEBUG(mPed, "RAMP:SCENE PED")
	SET_PED_CAN_BE_TARGETTED(mPed, FALSE)
//	SET_PED_RELATIONSHIP_GROUP_HASH(mPed, RELGROUPHASH_PLAYER)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mPed, TRUE)
	RETURN TRUE // succesfully  ped
ENDFUNC

//------------------------------------------------------------------------------
//	FUNCTION: CREATE_RAMPAGE_SCENE_VEHICLE()
//	PURPOSE: CREATES A SCENE PED
//------------------------------------------------------------------------------
FUNC BOOL CREATE_RAMPAGE_SCENE_VEHICLE(VEHICLE_INDEX &mVehicle, MODEL_NAMES mdl, VECTOR pos, FLOAT head)
	IF DOES_ENTITY_EXIST(mVehicle)
		DELETE_VEHICLE(mVehicle)
		mVehicle = NULL
	ENDIF
	
	mVehicle = CREATE_VEHICLE(mdl, pos, head)
	IF NOT DOES_ENTITY_EXIST(mVehicle)
		RETURN FALSE 
	ENDIF

	RETURN TRUE // succesfully  ped
ENDFUNC

//------------------------------------------------------------------------------
//	FUNCTION: CREATE_RAMPAGE_WEAPON()
//	PURPOSE: CREATES A WEAPON OBJECT
//------------------------------------------------------------------------------
FUNC BOOL CREATE_RAMPAGE_WEAPON(OBJECT_INDEX &mObj, WEAPON_TYPE wpn, VECTOR pos, FLOAT head)
	IF DOES_ENTITY_EXIST(mObj)
		DELETE_OBJECT(mObj)
		mObj = NULL
	ENDIF
	MODEL_NAMES eMdl = GET_WEAPONTYPE_MODEL( wpn )
	REQUEST_MODEL( eMdl )
	
	GUNCLUB_WEAPON_COMP_DATA_STRUCT sCompData
	
	INT iTimer = GET_GAME_TIMER() + 10000
	
	WHILE GET_GAME_TIMER() < iTimer
//		mObj = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(PLAYER_PED_ID(), wpn)
		
		IF( HAS_MODEL_LOADED( eMdl ) )
			mObj = CREATE_WEAPON_OBJECT( wpn, -1, pos, TRUE )
			SET_MODEL_AS_NO_LONGER_NEEDED( eMdl )
			
			// Remove any engraved components
			IF DOES_GUNSHOP_WEAPON_HAVE_MOD_TYPE_COMP(sCompData, wpn, gun_root)
				IF HAS_WEAPON_GOT_WEAPON_COMPONENT(mObj, GET_GUNSHOP_WEAPON_COMP_FROM_INDEX(sCompData, wpn, gun_root, 0))
					REMOVE_WEAPON_COMPONENT_FROM_WEAPON_OBJECT(mObj, GET_GUNSHOP_WEAPON_COMP_FROM_INDEX(sCompData, wpn, gun_root, 0))
				ELIF HAS_WEAPON_GOT_WEAPON_COMPONENT(mObj, GET_GUNSHOP_WEAPON_COMP_FROM_INDEX(sCompData, wpn, gun_root, 1))
					REMOVE_WEAPON_COMPONENT_FROM_WEAPON_OBJECT(mObj, GET_GUNSHOP_WEAPON_COMP_FROM_INDEX(sCompData, wpn, gun_root, 1))
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mObj)
				SET_ENTITY_COORDS(mObj, pos)
				SET_ENTITY_HEADING(mObj, head)
				RETURN TRUE
			ENDIF
		ENDIF
		WAIT(0)
	ENDWHILE
	SET_MODEL_AS_NO_LONGER_NEEDED( eMdl )
	
	RETURN FALSE // succesfully  ped
ENDFUNC

//------------------------------------------------------------------------------
//	FUNCTION: FINALIZE_RAMPAGE_SCENE_VEHICLE()
//	PURPOSE: CREATES A SCENE PED
//------------------------------------------------------------------------------
FUNC BOOL FINALIZE_RAMPAGE_SCENE_VEHICLE(VEHICLE_INDEX &mPed)	
	IF NOT DOES_ENTITY_EXIST(mPed)
		RETURN FALSE 
	ENDIF
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mPed, TRUE)
	RETURN TRUE // succesfully  ped
ENDFUNC

//Helper function for creating props in the initial scene setups.
FUNC BOOL CREATE_RAMPAGE_SCENE_PROP(OBJECT_INDEX &obj, MODEL_NAMES model, VECTOR vPos, VECTOR vRot)
	
	IF DOES_ENTITY_EXIST(obj)
		DELETE_OBJECT(obj)
		obj = NULL
	ENDIF

	obj = CREATE_OBJECT(model, vPos)
	IF NOT DOES_ENTITY_EXIST(obj)
		RETURN FALSE
	ENDIF
	
	SET_ENTITY_ROTATION(obj, vRot)
	RETURN TRUE
ENDFUNC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_1()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 1 INTRO
//------------------------------------------------------------------------------
PROC FORCE_NON_SAME_FACE(g_structRCScriptArgs& sData)	
	INT iAttempts = 0
	
	IF NOT IS_ENTITY_ALIVE(sData.pedID[1])
		EXIT 
	ENDIF
	
	WHILE GET_PED_TEXTURE_VARIATION(sData.pedID[1], PED_COMP_HEAD) = GET_PED_TEXTURE_VARIATION(sData.pedID[0], PED_COMP_HEAD)
		SET_PED_RANDOM_COMPONENT_VARIATION(sData.pedID[1])
		IF (iAttempts > 10)
			EXIT 
		ENDIF
	ENDWHILE
ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_1()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 1 INTRO
//------------------------------------------------------------------------------
PROC SetupScene_RAMPAGE_1(g_structRCScriptArgs& sData)	
	VECTOR v
	CPRINTLN(DEBUG_RANDOM_CHAR, "Setting up Initial Scene For Rampage 1 [010]")
	STRING animdictname = "misstrvram_1"
	PED_BONETAG attachBone = BONETAG_PH_R_HAND
	VECTOR bottleRotOffset = <<0, 0, 0>>
	VECTOR bottlePosOffset = <<0, 0, -0.1>>
	
	//-------------------------------------------------------------
	// Request Models
	//-------------------------------------------------------------
	MODEL_NAMES pedmodel = IG_RAMP_HIC
	MODEL_NAMES chairmodel = PROP_TABLE_03B_CHR
	MODEL_NAMES bottlemodel = PROP_BEER_BOTTLE
	
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "LOADING ASSETS FOR INITIAL SCENE")
	REQUEST_MODEL(pedmodel) 
	REQUEST_MODEL(chairmodel)
	REQUEST_MODEL(bottlemodel)
	REQUEST_ANIM_DICT(animdictname)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
	
	WHILE NOT HAS_MODEL_LOADED(pedmodel) 
	OR NOT HAS_MODEL_LOADED(chairmodel)
	OR NOT HAS_MODEL_LOADED(bottlemodel)
	OR NOT HAS_ANIM_DICT_LOADED(animdictname)
	OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
		WAIT(0)
	ENDWHILE
	
	//-------------------------------------------------------------
	//	Create Scene Ped
	//-------------------------------------------------------------
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING ENTITIES FOR INITIAL SCENE")
	
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[0], pedmodel, <<907.997986,3643.826416,32.292427>>, -173.045258)
		WAIT(0)
	ENDWHILE
	
	IF IS_ENTITY_ALIVE(sData.pedID[0])
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HEAD, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_JBIB, 0, 0)
		v = GET_PED_BONE_COORDS(sData.pedID[0], attachBone, <<0, 0, 0>>)
		SET_PED_NAME_DEBUG(sData.pedID[0], "RAMP:PED 0")
		CREATE_SCENE_PROP(sData.objID[1], bottleModel, v, 0)
		ATTACH_ENTITY_TO_ENTITY(sData.objID[1], sData.pedID[0], GET_PED_BONE_INDEX(sData.pedID[0], attachBone), bottlePosOffset, bottleRotOffset, TRUE, TRUE)
		SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(sData.objID[1], TRUE)	
		
		FREEZE_ENTITY_POSITION(sData.pedID[0], TRUE)
		SET_PED_CONFIG_FLAG(sData.pedID[0], PCF_DisableExplosionReactions, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(sData.pedID[0], RELGROUPHASH_PLAYER)
	ENDIF
	
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[1], pedmodel, <<909.312622,3643.506592,32.295200>>, 153.012695)
		WAIT(0)
	ENDWHILE
	
	IF IS_ENTITY_ALIVE(sData.pedID[1])
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HEAD, 1, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAIR, 1, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TORSO, 1, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_LEG, 1, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_JBIB, 0, 0)
		v = GET_PED_BONE_COORDS(sData.pedID[1], attachBone, <<0, 0, 0>>)
		SET_PED_NAME_DEBUG(sData.pedID[1], "RAMP:PED 1")
		
		CREATE_SCENE_PROP(sData.objID[2], bottleModel, v, 0)
		ATTACH_ENTITY_TO_ENTITY(sData.objID[2], sData.pedID[1], GET_PED_BONE_INDEX(sData.pedID[1], attachBone), bottlePosOffset, bottleRotOffset, TRUE, TRUE)
		SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(sData.objID[2], TRUE)
		
		FREEZE_ENTITY_POSITION(sData.pedID[1], TRUE)
		SET_PED_CONFIG_FLAG(sData.pedID[1], PCF_DisableExplosionReactions, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(sData.pedID[1], RELGROUPHASH_PLAYER)
	ENDIF
	
//	FORCE_NON_SAME_FACE(sData)	
	
	sData.objID[3] = GET_CLOSEST_OBJECT_OF_TYPE(<<909.3884, 3643.6523, 31.6990>>, 1.0, PROP_TABLE_03B_CHR)
	IF DOES_ENTITY_EXIST( sData.objID[3] )
		FREEZE_ENTITY_POSITION( sData.objID[3], TRUE )
	ENDIF
	
	sData.objID[4] = GET_CLOSEST_OBJECT_OF_TYPE(<<908.0437, 3643.6819, 31.7073>>, 1.0, PROP_TABLE_03B_CHR)
	IF DOES_ENTITY_EXIST( sData.objID[4] )
		FREEZE_ENTITY_POSITION( sData.objID[4], TRUE )
	ENDIF
	
	// setup synchronized scene
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneIndex)
		sData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<<910.13, 3643.74, 31.69>>, <<0, 0, RAD_TO_DEG(-0.26)>>)
		
		// ped one
		IF IS_ENTITY_ALIVE(sData.pedID[0])
        	TASK_SYNCHRONIZED_SCENE(sData.pedID[0], sData.iSyncSceneIndex, animdictname, "redneck_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
		ENDIF
		
		IF IS_ENTITY_ALIVE(sData.pedID[1])
        	TASK_SYNCHRONIZED_SCENE(sData.pedID[1], sData.iSyncSceneIndex, animdictname, "redneck_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
		ENDIF
	
		SET_SYNCHRONIZED_SCENE_LOOPED(sData.iSyncSceneIndex, TRUE)
		CPRINTLN(DEBUG_RANDOM_CHAR, "SYNCHRONIZED SCENE ASSIGNED")
	ENDIF
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE")
	IF NOT CREATE_RAMPAGE_WEAPON(sData.objID[0], WEAPONTYPE_ASSAULTRIFLE, <<907.52, 3644.32, 31.86>>, -164.89)
		CPRINTLN(DEBUG_RANDOM_CHAR, "WEAPON LOAD FAILED - WEAPONTYPE_SMG - FAILED")
	ELSE
		CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE - COMPLETE")
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(pedmodel) 
	SET_MODEL_AS_NO_LONGER_NEEDED(chairmodel)
	
	sData.sIntroCutscene = "TRVRAM_1"
	sData.bAllowVehicleActivation = FALSE

ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_2()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 2 INTRO
//------------------------------------------------------------------------------
PROC SetupScene_RAMPAGE_2(g_structRCScriptArgs& sData)		
	//-------------------------------------------------------------
	// Request Models
	//-------------------------------------------------------------
	MODEL_NAMES pedmodel = IG_RAMP_HIPSTER //A_M_Y_HIPSTER_03
	MODEL_NAMES chairmodel = PROP_CHAIR_01A
	MODEL_NAMES phonemodel = PROP_NPC_PHONE	//Prop_Phone_ING
	STRING animdictname = "misstrvram_2"
	
	PED_BONETAG attachBone = BONETAG_PH_L_HAND
//	VECTOR v
	VECTOR phoneRotOffset = <<0, 0, 0>>
	VECTOR phonePosOffset = <<0, 0, 0>>
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "LOADING ASSETS FOR INITIAL SCENE")
	REQUEST_WEAPON_ASSET(WEAPONTYPE_SAWNOFFSHOTGUN)
	
	REQUEST_MODEL(pedmodel) 
	REQUEST_MODEL(chairmodel)
	REQUEST_MODEL(phonemodel)
	REQUEST_ANIM_DICT(animdictname)
	REQUEST_FAKE_CELLPHONE_MOVIE(sFakeCellphoneData)
	WHILE NOT HAS_MODEL_LOADED(pedmodel)
	OR NOT HAS_MODEL_LOADED(chairmodel)
	OR NOT HAS_MODEL_LOADED(phonemodel)
	OR NOT HAS_ANIM_DICT_LOADED(animdictname)
	OR NOT HAS_FAKE_CELLPHONE_MOVIE_LOADED(sFakeCellphoneData)
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING ENTITIES FOR INITIAL SCENE")
	
	//-------------------------------------------------------------
	//	Scene props
	//-------------------------------------------------------------
	CLEAR_AREA_OF_OBJECTS(<<1180.0205, -401.7374, 66.8767>>, 0.75)
	
	/*
	WHILE NOT CREATE_RAMPAGE_SCENE_PROP(sData.objId[1], chairmodel, <<1180.0205, -401.7374, 66.8767>>, <<0, 0, DEG_TO_RAD(0.87)>>)
		WAIT(0)
	ENDWHILE
	
	FREEZE_ENTITY_POSITION(sData.objId[1], TRUE)
	SET_ENTITY_COLLISION(sData.objId[1], FALSE)
	*/
	
	sData.objID[2] = GET_CLOSEST_OBJECT_OF_TYPE(<<1179.7780, -401.7374, 66.8697>>, 1.0, PROP_CHAIR_01A)
	IF DOES_ENTITY_EXIST( sData.objID[2] )
		CPRINTLN( DEBUG_RAMPAGE, "SetupScene_RAMPAGE_2: FREEZING CHAIR POSITION" )
		SET_ENTITY_COORDS( sData.objID[2], <<1180.05, -401.83, 66.88>> )
		SET_ENTITY_ROTATION( sData.objID[2], <<0,0,50>> )
		FREEZE_ENTITY_POSITION( sData.objID[2], TRUE )
	ENDIF
	
	sData.objID[3] = GET_CLOSEST_OBJECT_OF_TYPE(<<1181.2432, -402.5309, 67.2564>>, 1.0, PROP_TABLE_01)
	IF DOES_ENTITY_EXIST( sData.objID[3] )
		CPRINTLN( DEBUG_RAMPAGE, "SetupScene_RAMPAGE_2: FREEZING TABLE POSITION" )
		FREEZE_ENTITY_POSITION( sData.objID[3], TRUE )
	ENDIF
	
	//-------------------------------------------------------------
	//	Create Scene Ped
	//-------------------------------------------------------------
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[0], pedmodel, <<1181.547363,-400.095398,67.575699>>, -124.292786)
		WAIT(0)
	ENDWHILE
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[0])
	FREEZE_ENTITY_POSITION(sData.pedID[0], true)
	SET_PED_DEFAULT_COMPONENT_VARIATION(sData.pedID[0])
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAIR, 0, 0)
//	v = GET_PED_BONE_COORDS(sData.pedID[1], attachBone, <<0, 0, 0>>)
	CREATE_SCENE_PROP(sData.objID[1], phonemodel, <<1181.547363,-400.095398,65.575699>>, 0)
	ATTACH_ENTITY_TO_ENTITY(sData.objID[1], sData.pedID[0], GET_PED_BONE_INDEX(sData.pedID[0], attachBone), phonePosOffset, phoneRotOffset, TRUE, FALSE)
	DRAW_FAKE_CELLPHONE_SCREEN( sFakeCellphoneData, FALSE, FAKE_CELLPHONE_SCREEN_GENERIC_CALL )
	
	// setup synchronized scene
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneIndex)
//		sData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(<<1180.850, -402.057, 67.218>>, <<0, 0, 66.960>>)
		sData.iSyncSceneIndex = CREATE_SYNCHRONIZED_SCENE(vHipsterPos, vHipsterRot)
		
		// ped one
		IF IS_ENTITY_ALIVE(sData.pedID[0])
        	TASK_SYNCHRONIZED_SCENE(sData.pedID[0], sData.iSyncSceneIndex, animdictname, "hipster_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
		ENDIF
		
		SET_SYNCHRONIZED_SCENE_LOOPED(sData.iSyncSceneIndex, TRUE)
		CPRINTLN(DEBUG_RANDOM_CHAR, "SYNCHRONIZED SCENE ASSIGNED")
	ENDIF
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE")
	IF NOT CREATE_RAMPAGE_WEAPON(sData.objID[0], WEAPONTYPE_SAWNOFFSHOTGUN, <<1162.2, -395.4, 72.9>>, RAD_TO_DEG(-0.71))
		CPRINTLN(DEBUG_RANDOM_CHAR, "WEAPON LOAD FAILED - WEAPONTYPE_SAWNOFFSHOTGUN - FAILED")
	ELSE
		CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE - COMPLETE")
	ENDIF

	CPRINTLN(DEBUG_RANDOM_CHAR, "Setting up Initial Scene For Rampage 2")
	sData.sIntroCutscene = "TRVRAM_2_CONCAT"
	sData.bAllowVehicleActivation = FALSE
	
	SET_MODEL_AS_NO_LONGER_NEEDED(pedmodel) 
	SET_MODEL_AS_NO_LONGER_NEEDED(chairmodel)
	SET_MODEL_AS_NO_LONGER_NEEDED(phonemodel)
ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_3()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 3 INTRO
//------------------------------------------------------------------------------
PROC SetupScene_RAMPAGE_3(g_structRCScriptArgs& sData)	
	//-------------------------------------------------------------
	// Request Models
	//-------------------------------------------------------------
	MODEL_NAMES pedmodel = IG_RAMP_MEX
	STRING animdictname = "misstrvram_3"
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "LOADING ASSETS FOR INITIAL SCENE")
	REQUEST_MODEL(pedmodel) 
	REQUEST_ANIM_DICT(animdictname)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_MICROSMG,ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS),WEAPON_COMPONENT_SCLIP2|WEAPON_COMPONENT_SCOPE)
	WHILE NOT HAS_MODEL_LOADED(pedmodel) 
	OR NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_MICROSMG)
	OR NOT HAS_ANIM_DICT_LOADED(animdictname) 
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING ENTITIES FOR INITIAL SCENE")

	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[0], pedmodel,  <<464.6125,-1848.7673,27.8160>>, 177.39, TRUE) 
		WAIT(0)
	ENDWHILE
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[0])
	
	// this is the gun guy
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL2, 1, 0)	// GUN...
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_JBIB, 0, 0)
	
	SET_PED_NAME_DEBUG(sData.pedID[0], "RAMP:PED 0")
	TASK_PLAY_ANIM(sData.pedID[0], animdictname, "mexicans_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)

	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[1], pedmodel,  <<465.7820,-1849.0730,27.8170>>, 129.92, TRUE) 
		WAIT(0)
	ENDWHILE
	
	//FORCE_NON_SAME_FACE(sData)	
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[1])
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TORSO, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_LEG, 0, 1)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_JBIB, 0, 0)
	
	SET_PED_NAME_DEBUG(sData.pedID[1], "RAMP:PED 1")
	TASK_PLAY_ANIM(sData.pedID[1], animdictname, "mexicans_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)
	
	/*
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE")
	IF NOT CREATE_RAMPAGE_WEAPON(sData.objID[0], WEAPONTYPE_MICROSMG, <<468.24, -1858.13, 32.0>>, RAD_TO_DEG(-0.85))
		CPRINTLN(DEBUG_RANDOM_CHAR, "WEAPON LOAD FAILED - WEAPONTYPE_MICROSMG - FAILED")
	ELSE
		GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT( sData.objID[0], WEAPONCOMPONENT_AT_SCOPE_MACRO )
		GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT( sData.objID[0], WEAPONCOMPONENT_MICROSMG_CLIP_02 )
		CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE - COMPLETE")
	ENDIF
	*/
	
	IF CREATE_RAMPAGE_WEAPON(sData.objID[0], WEAPONTYPE_MICROSMG, <<467.0, -1855.4, 32.0>>, RAD_TO_DEG(-0.85))
		GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT( sData.objID[0], WEAPONCOMPONENT_AT_SCOPE_MACRO )
		GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT( sData.objID[0], WEAPONCOMPONENT_MICROSMG_CLIP_02 )
		CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE - COMPLETE")
	ENDIF
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "Setting up Initial Scene For Rampage 3")
	sData.sIntroCutscene = "TRVRAM_3"
	sData.bAllowVehicleActivation = FALSE
	
	SET_MODEL_AS_NO_LONGER_NEEDED(pedmodel) 
ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_4()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 4 INTRO
//------------------------------------------------------------------------------
PROC SetupScene_RAMPAGE_4(g_structRCScriptArgs& sData)	

	MODEL_NAMES pedmodel = IG_RAMP_GANG
	STRING animdictname = "misstrvram_4"
	
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "LOADING ASSETS FOR INITIAL SCENE")
	REQUEST_MODEL(pedmodel) 
	REQUEST_ANIM_DICT(animdictname)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
	
	WHILE NOT HAS_MODEL_LOADED(pedmodel) 
	OR NOT HAS_ANIM_DICT_LOADED(animdictname) 
		WAIT(0)
	ENDWHILE

	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING ENTITIES FOR INITIAL SCENE")
	
	//-------------------------------------------------------------
	//	Create Scene Ped
	//-------------------------------------------------------------
	
	// this guy does not have the gun - he also has his foot up
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[0], pedmodel, <<-161.37, -1669.63, 33.10>>, RAD_TO_DEG(1.03), TRUE) 
		WAIT(0)
	ENDWHILE
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[0])
	SET_PED_NAME_DEBUG(sData.pedID[0], "RP0_NOGUN")

	SET_PED_DEFAULT_COMPONENT_VARIATION(sData.pedID[0])
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TORSO, 0, 1)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_DECL, 0, 1)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_JBIB, 0, 0)

	TASK_PLAY_ANIM(sData.pedID[0], animdictname, "thugs_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)
	
	// this guy does have the gun and has his foot down
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[1], pedmodel, <<-161.93,-1670.39, 33.14>>, RAD_TO_DEG(0.20), TRUE) 
		WAIT(0)
	ENDWHILE
	//FORCE_NON_SAME_FACE(sData)	
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[1])
	SET_PED_NAME_DEBUG(sData.pedID[1], "RP1_GUN")
	//SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TORSO, 1, 1)

	SET_PED_DEFAULT_COMPONENT_VARIATION(sData.pedID[1])
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TORSO, 1, 1)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_DECL, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_JBIB, 0, 0)
	
	GIVE_WEAPON_TO_PED(sData.pedID[1], WEAPONTYPE_ASSAULTRIFLE, 100, TRUE)
	
	TASK_PLAY_ANIM(sData.pedID[1], animdictname, "thugs_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)	
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "Setting up Initial Scene For Rampage 4")
	sData.sIntroCutscene = "TRVRAM_4"
	sData.bAllowVehicleActivation = FALSE

	SET_MODEL_AS_NO_LONGER_NEEDED(pedmodel) 
ENDPROC

//------------------------------------------------------------------------------
//	FUNCTION: SetupScene_RAMPAGE_5()
//	PURPOSE: PLACES PEDS AND CARS FOR RAMPAGE 5 INTRO
//------------------------------------------------------------------------------
PROC SetupScene_RAMPAGE_5(g_structRCScriptArgs& sData)
	STRING animdictname = "misstvrram_5"
	
	MODEL_NAMES pedmodel = S_M_Y_MARINE_03
	MODEL_NAMES carmodel = CRUSADER
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "LOADING ASSETS FOR INITIAL SCENE")
	REQUEST_MODEL(pedmodel) 
	REQUEST_MODEL(carmodel) 
	REQUEST_ANIM_DICT(animdictname)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_GRENADELAUNCHER)
	
	WHILE NOT HAS_MODEL_LOADED(pedmodel) 
	OR NOT HAS_MODEL_LOADED(carmodel) 
	OR NOT HAS_ANIM_DICT_LOADED(animdictname) 
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING ENTITIES FOR INITIAL SCENE")
	
	//-------------------------------------------------------------
	//	Create Scene Ped
	//-------------------------------------------------------------
	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[0], pedmodel, <<-1299.40, 2506.24, 21.07>>, -122.79) 
		WAIT(0)
	ENDWHILE
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[0])
	SET_PED_NAME_DEBUG(sData.pedID[0], "RAMP:PED 0")
	TASK_PLAY_ANIM(sData.pedID[0], animdictname, "marines_idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)
	
	// fed up with this gas mask bug
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HEAD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[0], PED_COMP_JBIB, 0, 0)

	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_HEAD)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_EYES)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_EARS)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_MOUTH)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_LEFT_HAND)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_RIGHT_HAND)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_LEFT_WRIST)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_RIGHT_WRIST)
	CLEAR_PED_PROP(sData.pedID[0], ANCHOR_HIP)

	WHILE NOT CREATE_RAMPAGE_SCENE_PED(sData.pedID[1], pedmodel, <<-1299.49, 2505.53, 21.05>>, -101.94) 
		WAIT(0)
	ENDWHILE
	
	FINALIZE_RAMPAGE_SCENE_PED(sData.pedID[1])

	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HEAD, 1, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAIR, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TORSO, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_LEG, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_HAND, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_FEET, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_TEETH, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_SPECIAL2, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_DECL, 0, 0)
	SET_PED_COMPONENT_VARIATION(sData.pedID[1], PED_COMP_JBIB, 0, 0)

	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_HEAD)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_EYES)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_EARS)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_MOUTH)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_LEFT_HAND)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_RIGHT_HAND)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_LEFT_WRIST)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_RIGHT_WRIST)
	CLEAR_PED_PROP(sData.pedID[1], ANCHOR_HIP)

	SET_PED_NAME_DEBUG(sData.pedID[1], "RAMP:PED 1")
	TASK_PLAY_ANIM(sData.pedID[1], animdictname, "marines_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT | AF_LOOPING)
	
	WHILE NOT CREATE_RAMPAGE_SCENE_VEHICLE(sData.vehID[0], carmodel, <<-1298.1597, 2504.1533, 21.062>>, 165.48) 
		WAIT(0)
	ENDWHILE
	SET_VEHICLE_ON_GROUND_PROPERLY(sData.vehID[0])
	FINALIZE_RAMPAGE_SCENE_VEHICLE(sData.vehID[0])
	
	CPRINTLN(DEBUG_RANDOM_CHAR, " PRE CHECK - VEHICLE EXTRA IS:", IS_VEHICLE_EXTRA_TURNED_ON(sData.vehID[0], 1))					
	SET_VEHICLE_EXTRA(sData.vehID[0], 1, FALSE)
	CPRINTLN(DEBUG_RANDOM_CHAR, " CHECK - VEHICLE EXTRA IS:", IS_VEHICLE_EXTRA_TURNED_ON(sData.vehID[0], 1))	
	SET_VEHICLE_EXTRA(sData.vehID[0], 1, TRUE)
	CPRINTLN(DEBUG_RANDOM_CHAR, " POST CHECK - VEHICLE EXTRA IS:", IS_VEHICLE_EXTRA_TURNED_ON(sData.vehID[0], 1))	
	FREEZE_ENTITY_POSITION(sData.vehID[0], TRUE)
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE")
	IF NOT CREATE_RAMPAGE_WEAPON(sData.objID[0], WEAPONTYPE_GRENADELAUNCHER, <<-1297.84, 2505.91, 19.34>>, RAD_TO_DEG(-0.50))
		CPRINTLN(DEBUG_RANDOM_CHAR, "WEAPON LOAD FAILED - WEAPONTYPE_GRENADELAUNCHER - FAILED")
	ELSE
		CPRINTLN(DEBUG_RANDOM_CHAR, "CREATING WEAPON FOR INITIAL SCENE - COMPLETE")
		SET_ENTITY_COLLISION(sData.objID[0], FALSE)
		SET_ENTITY_COORDS(sData.objID[0], <<-1297.84, 2505.91, 19.34>>)
		SET_ENTITY_HEADING(sData.objID[0], RAD_TO_DEG(-0.50))
		FREEZE_ENTITY_POSITION(sData.objID[0],TRUE)
	ENDIF
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "Setting up Initial Scene For Rampage 5")
	sData.sIntroCutscene = "TRVRAM_5_CON"
	sData.bAllowVehicleActivation = FALSE

	SET_MODEL_AS_NO_LONGER_NEEDED(pedmodel) 
	SET_MODEL_AS_NO_LONGER_NEEDED(carmodel) 
	//REMOVE_ANIM_DICT(animdictname)
ENDPROC

FUNC BOOL FOCUS_PUSH_LEADIN(g_structRCScriptArgs& sData)
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	
	IF iFocusPushTimer < 0
		//Exit early if the player exits a vehicle within 5 meters of lead in most likely from getting out of a car
		IF VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sData.pedID[1]) ) < 25
			RETURN TRUE
		ENDIF
		SWITCH sData.eMissionID
			CASE RC_RAMPAGE_1
//				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], vEntityHint, TRUE, -1, 2500 )
//				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( fHintPitch )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( fHintHOffset )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( fHintVOffset )
//				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( fHintScalar )
//				SET_GAMEPLAY_HINT_FOV( fHintFOV )
				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], <<0.1, 0.0, 0.9>>, TRUE, -1, 2500 )
				TASK_LOOK_AT_ENTITY( PLAYER_PED_ID(), sData.pedID[0], -1 )
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE( TRUE )
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( -5.0 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( 0.10 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( 0.00 )
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.3 )
				SET_GAMEPLAY_HINT_FOV( 40.0 )
			BREAK
			CASE RC_RAMPAGE_3
//				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], vEntityHint, TRUE, -1, 2500 )
//				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( fHintPitch )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( fHintHOffset )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( fHintVOffset )
//				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( fHintScalar )
//				SET_GAMEPLAY_HINT_FOV( fHintFOV )
				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], <<0.0, -0.1, 0.9>>, TRUE, -1, 2500 )
				TASK_LOOK_AT_ENTITY( PLAYER_PED_ID(), sData.pedID[0], -1 )
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE( TRUE )
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( -10.0 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( -0.9 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( 0.0 )
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.35 )
				SET_GAMEPLAY_HINT_FOV( 40.0 )
			BREAK
			CASE RC_RAMPAGE_4
//				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], vEntityHint, TRUE, -1, 2500 )
//				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( fHintPitch )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( fHintHOffset )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( fHintVOffset )
//				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( fHintScalar )
//				SET_GAMEPLAY_HINT_FOV( fHintFOV )
				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[1], <<0.0, 0.0, 0.9>>, TRUE, -1, 2500 )
				TASK_LOOK_AT_ENTITY( PLAYER_PED_ID(), sData.pedID[1], -1 )
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE( TRUE )
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( -8.0 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( -0.9 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( 0.1 )
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.35 )
				SET_GAMEPLAY_HINT_FOV( 40.0 )
			BREAK
			CASE RC_RAMPAGE_5
//				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], vEntityHint, TRUE, -1, 2500 )
//				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( fHintPitch )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( fHintHOffset )
//				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( fHintVOffset )
//				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( fHintScalar )
//				SET_GAMEPLAY_HINT_FOV( fHintFOV )
				SET_GAMEPLAY_ENTITY_HINT( sData.pedID[1], <<0.0, 0.0, 0.0>>, TRUE, -1, 2500 )
				TASK_LOOK_AT_ENTITY( PLAYER_PED_ID(), sData.pedID[1], -1 )
				SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE( TRUE )
				SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( -5.0 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( 0.15 )
				SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( 0.0 )
				SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.3 )
				SET_GAMEPLAY_HINT_FOV( 40.0 )
			BREAK
		ENDSWITCH
		iFocusPushTimer =  (GET_GAME_TIMER() + 2500)
	ELSE
//#IF IS_DEBUG_BUILD
//		IF bResetFocusCam
//			iFocusPushTimer = -1
//			bResetFocusCam = FALSE
//			IF IS_GAMEPLAY_HINT_ACTIVE()
//				STOP_GAMEPLAY_HINT()
//			ENDIF
//		ENDIF
//		IF bStartRampage
//			RETURN TRUE
//		ENDIF
//#ENDIF
		IF GET_GAME_TIMER() > iFocusPushTimer
			iFocusPushTimer = -1
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

INT preleadinState = 0
INT iPreleadinSyncScene[3]
structPedsForConversation 	convStruct

INT iLeadInState = 0
INT iLeadInSyncScene
	
FUNC BOOL LEADIN_RAMPAGE_2(g_structRCScriptArgs& sData)
	RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
	
//#IF IS_DEBUG_BUILD
//	IF bResetFocusCam
//		iFocusPushTimer = -1
//		bResetFocusCam = FALSE
//		IF IS_GAMEPLAY_HINT_ACTIVE()
//			STOP_GAMEPLAY_HINT()
//		ENDIF
//	ENDIF
//	IF bStartRampage
//		RETURN TRUE
//	ENDIF
//#ENDIF

	SWITCH iLeadInState
		CASE 0
			IF IS_ENTITY_ALIVE( sData.pedID[0] )
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iLeadInState++
			ENDIF
		BREAK
		CASE 1
			IF IS_ENTITY_ALIVE( sData.pedID[0] )
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(convStruct, "RAMP2AU", "RAMP2_LEADIN", "RAMP2_LEADIN_3", CONV_PRIORITY_HIGH)
						iLeadInSyncScene = CREATE_SYNCHRONIZED_SCENE(vHipsterPos, vHipsterRot)
						TASK_SYNCHRONIZED_SCENE(sData.pedID[0], iLeadInSyncScene, "misstrvram_2", "hipster_cunts_in_america", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
						SET_GAMEPLAY_ENTITY_HINT( sData.pedID[0], <<0.0, 0.0, 0.5>>, TRUE, -1, 2500 )
						TASK_LOOK_AT_ENTITY( PLAYER_PED_ID(), sData.pedID[0], -1 )
						SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE( TRUE )
						SET_GAMEPLAY_HINT_BASE_ORBIT_PITCH_OFFSET( 0.0 )
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_SIDE_OFFSET( 0.10 )
						SET_GAMEPLAY_HINT_CAMERA_RELATIVE_VERTICAL_OFFSET( 0.0 )
						SET_GAMEPLAY_HINT_FOLLOW_DISTANCE_SCALAR( 0.35 )
						SET_GAMEPLAY_HINT_FOV( 40.0 )
						iLeadInState++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_ALIVE( sData.pedID[0] )
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() //GET_SYNCHRONIZED_SCENE_PHASE(iLeadInSyncScene) >= 0.95
					iLeadInState++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC preleadin_RAMPAGE_2(g_structRCScriptArgs& sData)
	STRING animdictname = "misstrvram_2"
	
	SWITCH preleadinState
		CASE 0
			REQUEST_ANIM_DICT(animdictname)
			IF HAS_ANIM_DICT_LOADED(animdictname)
				preleadinState ++
			ENDIF
		BREAK
		CASE 1
			IF IS_ENTITY_ALIVE( sData.pedID[0] )
				IF GET_DISTANCE_BETWEEN_ENTITIES( sData.pedID[0], PLAYER_PED_ID() ) < 30.0
				AND IS_ENTITY_ON_SCREEN( sData.pedID[0] )
					ADD_PED_FOR_DIALOGUE(convStruct,0,sData.pedID[0],"RAMPAGEHIPSTER")
					preleadinState ++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			iPreleadinSyncScene[0] = CREATE_SYNCHRONIZED_SCENE(vHipsterPos, vHipsterRot)
			
			// ped one
			IF IS_ENTITY_ALIVE(sData.pedID[0])
	        	PLAY_SINGLE_LINE_FROM_CONVERSATION(convStruct, "RAMP2AU", "RAMP2_LEADIN", "RAMP2_LEADIN_1", CONV_PRIORITY_MEDIUM)
				TASK_SYNCHRONIZED_SCENE(sData.pedID[0], iPreleadinSyncScene[0], animdictname, "hipster_post_post_irony", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iPreleadinSyncScene[0], FALSE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iPreleadinSyncScene[0], TRUE)
			CPRINTLN(DEBUG_RANDOM_CHAR, "preleadin_RAMPAGE_2: playing hipster_post_post_irony ", GET_GAME_TIMER())
			preleadinState ++
		BREAK
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPreleadinSyncScene[0])
				IF GET_SYNCHRONIZED_SCENE_PHASE(iPreleadinSyncScene[0]) >= 0.95
					CPRINTLN(DEBUG_RANDOM_CHAR, "preleadin_RAMPAGE_2: hipster_post_post_irony finished ", GET_GAME_TIMER())
					preleadinState ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			iPreleadinSyncScene[1] = CREATE_SYNCHRONIZED_SCENE(vHipsterPos, vHipsterRot)
			
			// ped one
			IF IS_ENTITY_ALIVE(sData.pedID[0])
	        	PLAY_SINGLE_LINE_FROM_CONVERSATION(convStruct, "RAMP2AU", "RAMP2_LEADIN", "RAMP2_LEADIN_2", CONV_PRIORITY_MEDIUM)
				TASK_SYNCHRONIZED_SCENE(sData.pedID[0], iPreleadinSyncScene[1], animdictname, "hipster_self_hating_hipster", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iPreleadinSyncScene[1], FALSE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iPreleadinSyncScene[1], TRUE)
			CPRINTLN(DEBUG_RANDOM_CHAR, "preleadin_RAMPAGE_2: playing hipster_self_hating_hipster ", GET_GAME_TIMER())
			preleadinState ++
		BREAK
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iPreleadinSyncScene[1])
				IF GET_SYNCHRONIZED_SCENE_PHASE(iPreleadinSyncScene[1]) >= 0.95
					CPRINTLN(DEBUG_RANDOM_CHAR, "preleadin_RAMPAGE_2: hipster_self_hating_hipster finished ", GET_GAME_TIMER())
					preleadinState ++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			iPreleadinSyncScene[2] = CREATE_SYNCHRONIZED_SCENE(vHipsterPos, vHipsterRot)
			
			// ped one
			IF IS_ENTITY_ALIVE(sData.pedID[0])
//	        	PLAY_SINGLE_LINE_FROM_CONVERSATION(convStruct, "RAMP2AU", "RAMP2_LEADIN", "RAMP2_LEADIN_1", CONV_PRIORITY_MEDIUM)
				TASK_SYNCHRONIZED_SCENE(sData.pedID[0], iPreleadinSyncScene[2], animdictname, "hipster_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iPreleadinSyncScene[2], TRUE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iPreleadinSyncScene[2], FALSE)
			CPRINTLN(DEBUG_RANDOM_CHAR, "preleadin_RAMPAGE_2: playing hipster_idle")
			preleadinState ++
		BREAK
		CASE 7
			//Do nothing here, keep playing idle anim until rampage is triggered
		BREAK
	ENDSWITCH
ENDPROC
