// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	rampage3.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Vagos / Micro-SMG
//
// *****************************************************************************************
// *****************************************************************************************
  
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.	   
	   
//----------------------
//	INCLUDES
//----------------------
USING "rampagev4_include.sch"
USING "mission_control_public.sch"
USING "commands_recording.sch"

//----------------------
//	GLOBALS
//----------------------
g_structRCScriptArgs sLaunchDataLocal
  
//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(g_structRCScriptArgs sLaunchDataIn)
   
   	sLaunchDataLocal = sLaunchDataIn
	RC_SET_ENTITY_PROOFS_FOR_CUTSCENE(sLaunchDataLocal, FALSE, FALSE)
	
	SET_MISSION_FLAG(TRUE)

	SET_RAMPAGE_FROM_STATIC_BLIP(STATIC_BLIP_MINIGAME_RAMPAGE3)
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS))
		Random_Character_Failed()
		RAMPAGE_CLEANUP(TRUE)
	ENDIF
	 
	RAMPAGE_SETUP(sLaunchDataLocal)

	// Fade in over half a second and let player be controllable.
	#IF IS_DEBUG_BUILD
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	#ENDIF
	
	WHILE (TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("SF_R2") // this is the second rampage in game order
		UPDATE_MISSION_NAME_DISPLAYING(sLaunchDataLocal.sIntroCutscene, FALSE, TRUE)
		UPDATE_RAMPAGE()
		WAIT(0) // wait 1 frame
	ENDWHILE
ENDSCRIPT


	
