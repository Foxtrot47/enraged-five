// *****************************************************************************************
//
//		MISSION NAME	:	launcher_Rampage.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	Launcher script that determines which rampage mission to setup.
//							This launcher script should be attached to world points for each
//							of the rampage locations
//
// *****************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

      
//----------------------
//	INCLUDES
//----------------------
USING "cutscene_public.sch"
USING "RC_launcher_public.sch"
USING "RC_Setup_public.sch"
USING "RC_Helper_Functions.sch"
USING "initial_scenes_rampage.sch"
USING "rgeneral_include.sch"
     
//----------------------
//	CONSTANTS
//----------------------
CONST_INT SCRIPT_VERSION				006
CONST_INT RAMPAGE_WANTED_LEVEL_BARRIER	0

CONST_FLOAT RAMPAGE_ACTIVATE_RANGE 		5.0
CONST_FLOAT	HELP_TEXT_RESET_DISTANCE    25.0
CONST_FLOAT WORLD_POINT_COORD_TOLERANCE 5.0

//----------------------
//	VARIABLES
//----------------------
INT iFirstRBlipIndex = ENUM_TO_INT(STATIC_BLIP_MINIGAME_RAMPAGE1)
INT iCutsceneLoadRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST // ID to register off-mission cutscene load request with cutscene_controller.
INT iCurrentRampage = -1

BOOL bDisplayHelp = TRUE
BOOL bDisplayPoliceHelp = TRUE
BOOL bDisplayTriggerHelp = TRUE
STRING sSceneHandles[3]

VECTOR vPlayerPos

#IF IS_DEBUG_BUILD	
	WIDGET_GROUP_ID m_WidgetGroup
	BOOL bQuitScript = FALSE 
#ENDIF

SCENARIO_BLOCKING_INDEX mScenarioBlocker = NULL

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
//	SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	
	m_WidgetGroup = START_WIDGET_GROUP("Launcher Rampage")
		ADD_WIDGET_BOOL("Quit Script", bQuitScript)
		
		//Uncomment these to edit hint focus camera for rampages
//		ADD_WIDGET_VECTOR_SLIDER( "HINT ENTITY OFFSET", vEntityHint, -1.0, 1.0, 0.01 )
//		ADD_WIDGET_FLOAT_SLIDER( "HINT SCALAR", fHintScalar, 0, 1.0, 0.01 )
//		ADD_WIDGET_FLOAT_SLIDER( "HINT VERTICAL", fHintVOffset, -1.0, 1.0, 0.01 )
//		ADD_WIDGET_FLOAT_SLIDER( "HINT HORIZONTAL", fHintHOffset, -1.0, 1.0, 0.01 )
//		ADD_WIDGET_FLOAT_SLIDER( "HINT PITCH", fHintPitch, -90.0, 90.0, 1.0 )
//		ADD_WIDGET_FLOAT_SLIDER( "HINT FOV", fHintFOV, 0, 50.0, 1.0 )
//		ADD_WIDGET_BOOL( "RESET HINT", bResetFocusCam )
//		ADD_WIDGET_BOOL( "START RAMPAGE", bStartRampage )
	STOP_WIDGET_GROUP()
ENDPROC 

/// PURPOSE:
///    		Removes Debug Widgets
PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC 

#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE:
///    Gets The First Rampage Index within x meters from the player
/// PARAMS:
///    dist - range check
/// RETURNS:
///    Rampage Index
FUNC INT GET_RAMPAGE_IN_RANGE_OF_PLAYER(FLOAT dist)
	INT i
	VECTOR v 
	VECTOR plyrPos 
	
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
		RETURN -1
	ENDIF
	
	plyrPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	REPEAT NUM_OF_RAMPAGES i
		v = g_GameBlips[iFirstRBlipIndex + i].vCoords[0]
		IF (GET_DISTANCE_BETWEEN_COORDS(plyrPos, v) <= dist) AND (ABSF(plyrPos.z - v.z) < 1.5)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Checks to see if the player is x dist away from any rampage
/// PARAMS:
///    dist - range to check
/// RETURNS:
///    True if so
FUNC BOOL IS_IN_RANGE_OF_RAMPAGE(INT i, FLOAT dist)
	VECTOR v 
	
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	v = g_GameBlips[iFirstRBlipIndex + i].vCoords[0]
	IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), v) <= dist)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if there are any script-specific reasons that would prevent this rampage from running now.
///    Also checks if it is unlocked or not
/// PARAMS:
///    ind - index to check
/// RETURNS:
///    TRUE if there is a restriction, FALSE if the script is okay to continue.
FUNC BOOL IS_THERE_A_SPECIFIC_RAMPAGE_RESTRICTION()
	
	BOOL restrict = FALSE
	
	// Player is ok
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
		RETURN restrict 
	ENDIF
	
	IF g_RandomChars[RC_RAMPAGE_5].rcLeaveAreaCheck
		RETURN restrict
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), RAMPAGE_WANTED_LEVEL_BARRIER)
		IF bDisplayPoliceHelp
			CLEAR_HELP()
			PRINT_HELP("RAMP_HELP_CRIM")
			bDisplayPoliceHelp = FALSE
		ENDIF
		restrict = TRUE
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		// "Player must be on-foot to trigger this rampage"
		IF bDisplayHelp
			CLEAR_HELP()
			PRINT_HELP("RAMP_HELP_FOOT")
			bDisplayHelp = FALSE
		ENDIF
		
		restrict = TRUE
	ELIF IS_SELECTOR_ONSCREEN( FALSE )
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
	OR IS_PLAYER_PED_SWITCH_IN_PROGRESS() 
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RAMP_HELP_TRIG")
			IF IS_SELECTOR_ONSCREEN( FALSE )
				CPRINTLN( DEBUG_RANDOM_CHAR, "Launcher Rampage - Clearing RAMP_HELP_TRIG help as selector on screen." )
			ELIF IS_PLAYER_SWITCH_IN_PROGRESS()
				CPRINTLN( DEBUG_RANDOM_CHAR, "Launcher Rampage - Clearing RAMP_HELP_TRIG help as player switch in progress." )
			ELIF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				CPRINTLN( DEBUG_RANDOM_CHAR, "Launcher Rampage - Clearing RAMP_HELP_TRIG help as player ped switch in progress." )
			ENDIF
			CLEAR_HELP()
			bDisplayTriggerHelp = FALSE
		ENDIF
		restrict = TRUE
	ELIF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)	
		IF NOT bDisplayTriggerHelp
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RAMP_HELP_TRIG")
				CLEAR_HELP()
				PRINT_HELP_FOREVER("RAMP_HELP_TRIG")	
				CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher Rampage - Rampage display help")
			ENDIF
			bDisplayTriggerHelp = TRUE
		ENDIF
		
		restrict = NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)		
	ENDIF
	
	RETURN restrict
ENDFUNC

PROC FORCE_FREEZE_AND_RELEASE_RAMPAGE1_CHAIRS(g_structRCScriptArgs& sData)
	// Force Freeze the chairs and release them ourselves
	IF (sData.eMissionID = RC_RAMPAGE_1)
		PRINT_LAUNCHER_DEBUG(" RAMPAGE 1 - FORCE FREEZE CHAIRS AND PRE-RELEASE FOR B*2124538")
		
		IF DOES_ENTITY_EXIST( sData.objID[3] )
			FREEZE_ENTITY_POSITION( sData.objID[3], TRUE )
			SET_OBJECT_AS_NO_LONGER_NEEDED( sData.objID[3] )
			sData.objID[3] = NULL
		ENDIF
	
		IF DOES_ENTITY_EXIST( sData.objID[4] )
			FREEZE_ENTITY_POSITION( sData.objID[4], TRUE )
			SET_OBJECT_AS_NO_LONGER_NEEDED( sData.objID[4] )
			sData.objID[4] = NULL
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Does any necessary cleanup and terminates the launcher's thread.
/// PARAMS:
///    sData - launcher data struct
///    bCleanupEntities - do we want to cleanup the entities in the launcher struct
PROC SCRIPT_CLEANUP(g_structRCScriptArgs& sData, BOOL bCleanupEntities = TRUE)

	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	FORCE_FREEZE_AND_RELEASE_RAMPAGE1_CHAIRS(sData)
	
	IF bCleanupEntities
		PRINT_LAUNCHER_DEBUG(" SCRIPT TERMINATING: Cleaning up entities in Launcher")
		RC_CleanupSceneEntities(sData, FALSE)
	ENDIF
	
	// Unload launcher animation dictionary
	REMOVE_LAUNCHER_ANIM_DICT(sData.sAnims)
	
	// Clear any cutscene requests with controller.
	IF iCutsceneLoadRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATING: Ending off-mission cutscene request")
		END_OFFMISSION_CUTSCENE_REQUEST(iCutsceneLoadRequestID)
	ENDIF

	IF (mScenarioBlocker != NULL)
		IF (sData.eMissionID = RC_RAMPAGE_4) OR (sData.eMissionID = RC_RAMPAGE_2)
			REMOVE_SCENARIO_BLOCKING_AREA(mScenarioBlocker)
			CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Unblocking Scenario Area")
		ENDIF
	ENDIF
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	IF bDisplayTriggerHelp
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RAMP_HELP_TRIG")
			CPRINTLN( DEBUG_RANDOM_CHAR, "Launcher RCRampage - CLEAR_HELP - RAMP_HELP_TRIG" ) 
			CLEAR_HELP()
			bDisplayTriggerHelp = FALSE
		ELSE
			CDEBUG1LN( DEBUG_RANDOM_CHAR, "IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(RAMP_HELP_TRIG) = FALSE")
		ENDIF
	ELSE
		CDEBUG1LN( DEBUG_RANDOM_CHAR, "bDisplayTriggerHelp = FALSE")
	ENDIF
	
	RC_LAUNCHER_END()
	
	// Kill the thread
	CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Terminating .")
	PRINT_LAUNCHER_DEBUG("SCRIPT TERMINATED")
	bIsRampLauncherRunning = FALSE
	TERMINATE_THIS_THREAD()
ENDPROC
 
//------------------------------------------------------------------------------
//	FUNCTION: LOAD_INITAL_SCENE()
//	PURPOSE: Sets up initial scene
//------------------------------------------------------------------------------
PROC LOAD_INITIAL_SCENE(g_structRCScriptArgs& sData)
 
	SWITCH (sData.eMissionID)
		CASE RC_RAMPAGE_1
			SetupScene_RAMPAGE_1(sData)
			mScenarioBlocker = Rampage1_Scenario_Blocker()
		BREAK
		CASE RC_RAMPAGE_2
			SetupScene_RAMPAGE_2(sData)
			mScenarioBlocker = Rampage2_Scenario_Blocker()
		BREAK
		CASE RC_RAMPAGE_3
			SetupScene_RAMPAGE_3(sData)
		BREAK
		CASE RC_RAMPAGE_4
			SetupScene_RAMPAGE_4(sData)
			mScenarioBlocker = Rampage4_Scenario_Blocker()
		BREAK
		CASE RC_RAMPAGE_5
			SetupScene_RAMPAGE_5(sData)
		BREAK
		DEFAULT
			CPRINTLN(DEBUG_RANDOM_CHAR, "Rampage does not have an initial scene")
			SCRIPT_ASSERT("Rampage does not have an intial scene")
		BREAK
	ENDSWITCH
	

	IF (mScenarioBlocker <> NULL)
		CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Blocking Scenario Area")
	ENDIF
	
	sData.triggerType = RC_TRIG_CHAR
	IF sData.eMissionID = RC_RAMPAGE_2
		sData.activationRange = 15.0
	ELSE
		sData.activationRange = 12.0
	ENDIF
	sData.bPedsCritical = TRUE
ENDPROC

/// PURPOSE:
///    Returns the Script name given a rampage index
/// PARAMS:
///    i - index number
/// RETURNS:
///    Script Name
FUNC STRING GET_SCRIPT_NAME_FROM_RAMPAGE_INDEX(INT i)
	SWITCH (i)
		CASE 0
			RETURN "Rampage1"
		CASE 1
			RETURN "Rampage3"
		CASE 2
			RETURN "Rampage4"
		CASE 3
			RETURN "Rampage5"
		CASE 4
			RETURN "Rampage2"
	ENDSWITCH
	
	SCRIPT_ASSERT("[ERROR]:Script Name hasn't been defined")
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns the Script name given a rampage index
/// PARAMS:
///    i - index number
/// RETURNS:
///    Script Name
FUNC g_eRC_MissionIDs GET_MISSION_ID_FROM_RAMPAGE_INDEX(INT i)
	SWITCH (i)
		CASE 0
			RETURN RC_RAMPAGE_1
		CASE 1
			RETURN RC_RAMPAGE_3
		CASE 2
			RETURN RC_RAMPAGE_4
		CASE 3
			RETURN RC_RAMPAGE_5
		CASE 4
			RETURN RC_RAMPAGE_2
	ENDSWITCH
	
	SCRIPT_ASSERT("[ERROR]:Script Name hasn't been defined")
	RETURN NO_RC_MISSION
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    		Updates Debug Widgets and draw debug stuff
PROC UPDATE_DEBUG_WIDGETS(g_structRCScriptArgs& sData)

	IF (bQuitScript)
		SCRIPT_CLEANUP(sData, TRUE)
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE:
///    Updates the Ambient Version of the Launcher
/// PARAMS:
///    sData - Script Arg Data
///    vInCoords - Position We're Checking For
PROC UPDATE_AMBIENT_LAUNCHER(g_structRCScriptArgs& sData, VECTOR vInCoords)
	CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Running Ambient Launcher - Pos", vInCoords)
	
	// Clears area of non-mission entities and blood decals
	// 
	
	WHILE (TRUE)
	
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			PRINT_LAUNCHER_DEBUG("  Ambient - Player isn't within World Point Range")
			SCRIPT_CLEANUP(sData)
		ENDIF		
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS(sData)
		#ENDIF
		
		// Rampages are only available for Trevor (#331111) - RyanP
		IF (GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR)
			PRINT_LAUNCHER_DEBUG("  Ambient - Player is not Trevor")
			SCRIPT_CLEANUP(sData)
		ENDIF
	
		// find the closest rampage
		iCurrentRampage = GET_RAMPAGE_IN_RANGE_OF_PLAYER(RAMPAGE_ACTIVATE_RANGE)
		IF (iCurrentRampage <> -1)
			//CPRINTLN(DEBUG_RAMPAGE, "Launcher RCRampage - Rampage ", iCurrentRampage + 1, " is within ", RAMPAGE_ACTIVATE_RANGE, " meters - checking restrictions")
			IF NOT IS_THERE_A_SPECIFIC_RAMPAGE_RESTRICTION()
				CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Rampage Ambient ", iCurrentRampage + 1, " is ready to launch")
				
				// activate rampage, mark it as ready to play and uncomplete it
				sData.eMissionID = GET_MISSION_ID_FROM_RAMPAGE_INDEX(iCurrentRampage)
				sData.sScriptName = GET_SCRIPT_NAME_FROM_RAMPAGE_INDEX(iCurrentRampage)
				sData.triggerType = RC_TRIG_LOCATE_POINT
				sData.activationRange = RAMPAGE_ACTIVATE_RANGE
				
				IF (sData.eMissionID = NO_RC_MISSION)
					PRINT_LAUNCHER_DEBUG("  -  Ambient Rampage Mission ID is invalid")
					SCRIPT_CLEANUP(sData)
				ENDIF			
				
				IF (IS_STRING_NULL_OR_EMPTY(sData.sScriptName))
					PRINT_LAUNCHER_DEBUG("  -  Ambient Rampage Mission ID is invalid")
					SCRIPT_CLEANUP(sData)
				ENDIF
				
				// activate rampage, mark it as ready to play and uncomplete it
				SET_BIT(g_savedGlobals.sRandomChars.savedRC[sData.eMissionID].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
				SET_BIT(g_savedGlobals.sRandomChars.savedRC[sData.eMissionID].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
				CLEAR_BIT(g_savedGlobals.sRandomChars.savedRC[sData.eMissionID].rcFlags,  ENUM_TO_INT(RC_FLAG_COMPLETED))
				
				g_RandomChars[sData.eMissionID].rcIsAwaitingTrigger = FALSE
				
				//We can't clear the leave area flag for repeat plays. We can't allow the launcher
				//to trigger before the repeat has been setup.
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					//Clear the leave area flag to allow instant restarts after completing a Rampage.
					g_RandomChars[sData.eMissionID].rcLeaveAreaCheck = FALSE
				ENDIF
				
				IF NOT CAN_RC_LAUNCH(sData.eMissionID)
					PRINT_LAUNCHER_DEBUG(" RC Ambient Can't Launch - Can_RC_Launch Faled")
					SCRIPT_CLEANUP(sData)
				ELIF NOT LAUNCH_RC_MISSION(sData)
					PRINT_LAUNCHER_DEBUG(" RC Ambient Can't Launch - Launch_RC_Mission Failed")
					Script_Cleanup(sData)
				ELSE
					//Waits in a loop until we can terminate the launcher - flagged from mission at the moment
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					CLEAR_AREA(vInCoords, sData.activationRange, TRUE)
					PRINT_LAUNCHER_DEBUG(" RC Ambient Launcher Waiting To Terminate")
					g_RandomChars[sData.eMissionID].rcIsAwaitingTrigger = FALSE
					IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sData.eMissionID)
						PRINT_LAUNCHER_DEBUG(" Ambient - Ready To Terminate")
						Script_Cleanup(sData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RAMP_HELP_TRIG")
				CLEAR_HELP()
			ENDIF
			bDisplayTriggerHelp = FALSE
		ENDIF		
		
		WAIT(0)
	ENDWHILE
ENDPROC

//Check to see if any peds are attacking the player
FUNC BOOL CHECK_PEDS_ATTACKING_PLAYER(g_structRCScriptArgs& sData)
	IF sData.eMissionID = RC_RAMPAGE_3
	OR sData.eMissionID = RC_RAMPAGE_4
	OR sData.eMissionID = RC_RAMPAGE_5
		IF COUNT_PEDS_IN_COMBAT_WITH_TARGET( PLAYER_PED_ID() ) > 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the Random Character Version of the Launcher
/// PARAMS:
///    sData - Script Arg Data
///    vInCoords - Position We're Checking For
PROC UPDATE_RC_LAUNCHER(g_structRCScriptArgs& sData, VECTOR vInCoords)
	
	BOOL bHasCutsceneLoaded = FALSE
	CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Running RC Launcher - Pos", vInCoords)
	
	// Determine which RC mission we are attempting to launch (currently done on a launcher by launcher basis)
	g_eRC_MissionIDs eRCMissions[5]
	eRCMissions[0] = RC_RAMPAGE_1
	eRCMissions[1] = RC_RAMPAGE_2
	eRCMissions[2] = RC_RAMPAGE_3
	eRCMissions[3] = RC_RAMPAGE_4
	eRCMissions[4] = RC_RAMPAGE_5
	
	//Pick which mission activated us
	IF NOT (DETERMINE_RC_TO_LAUNCH(eRCMissions, sData, vInCoords, WORLD_POINT_COORD_TOLERANCE))
		PRINT_LAUNCHER_DEBUG(" Didn't know which RC To Launch")
		SCRIPT_CLEANUP(sData)
	ENDIF
	
	// Check with the Random Character Controller to see if this script is allowed to launch
	IF NOT CAN_RC_LAUNCH(sData.eMissionID)
		PRINT_LAUNCHER_DEBUG(" RC Can't Launch")
		SCRIPT_CLEANUP(sData)
	ENDIF
	
	// Halt launcher as we are incorrect character
	IF Random_Character_Blocked_Due_To_Character(sData.eMissionID)
		WHILE(TRUE)
			WAIT(0)
			IF SHOULD_RC_TERMINATE_WHEN_INCORRECT_CHAR(sData.eMissionID)
				Script_Cleanup(sData)
			ENDIF
		ENDWHILE
	ENDIF
	
	// The script is allowed to launch so set up the initial scene
	LOAD_INITIAL_SCENE(sData)
	
	// Clears area of non-mission entities and blood decals
	CLEAR_AREA(vInCoords, sData.activationRange, TRUE)
	
	// Loop to check conditions - should script terminate? or is player close enough to trigger mission?
	WHILE (TRUE)
		WAIT(0)
		
		INT iObjLoop
		
		IF sData.eMissionID = RC_RAMPAGE_1
		OR sData.eMissionID = RC_RAMPAGE_2
			IF IS_ENTITY_ALIVE( sData.pedID[0] )
				IF IS_EXPLOSION_IN_SPHERE( EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sData.pedID[0]), 5.1 )

					// In rampage 1, Delete bottles in peds hands before deleting them
					IF sData.eMissionID = RC_RAMPAGE_1
						REPEAT COUNT_OF(sData.objID) iObjLoop
							IF DOES_ENTITY_EXIST(sData.objID[iObjLoop])
								IF (IS_ENTITY_ALIVE(sData.pedID[0]) AND IS_ENTITY_ATTACHED_TO_ENTITY(sData.objID[iObjLoop], sData.pedID[0]))
								OR (IS_ENTITY_ALIVE(sData.pedID[1]) AND IS_ENTITY_ATTACHED_TO_ENTITY(sData.objID[iObjLoop], sData.pedID[1]))
									DELETE_OBJECT(sData.objID[iObjLoop])
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF

					SET_PED_TO_RAGDOLL(sData.pedID[0], 100, 500, TASK_RELAX)
					APPLY_DAMAGE_TO_PED(sData.pedID[0], 1000, TRUE)
					IF IS_ENTITY_ALIVE( sData.pedID[1] )
						SET_PED_TO_RAGDOLL(sData.pedID[1], 100, 500, TASK_RELAX)
						APPLY_DAMAGE_TO_PED(sData.pedID[1], 1000, TRUE)
					ENDIF
					PRINT_LAUNCHER_DEBUG("Sitting Rampage attacked with explosives, cleaning up")
					Script_Cleanup(sData)
				ENDIF
			ENDIF
			IF IS_ENTITY_ALIVE( sData.pedID[1] )
				IF IS_EXPLOSION_IN_SPHERE( EXP_TAG_DONTCARE, GET_ENTITY_COORDS(sData.pedID[1]), 5.1 )
					
					// In rampage 1, Delete bottles in peds hands before deleting them
					IF sData.eMissionID = RC_RAMPAGE_1
						REPEAT COUNT_OF(sData.objID) iObjLoop
							IF DOES_ENTITY_EXIST(sData.objID[iObjLoop])
								IF (IS_ENTITY_ALIVE(sData.pedID[0]) AND IS_ENTITY_ATTACHED_TO_ENTITY(sData.objID[iObjLoop], sData.pedID[0]))
								OR (IS_ENTITY_ALIVE(sData.pedID[1]) AND IS_ENTITY_ATTACHED_TO_ENTITY(sData.objID[iObjLoop], sData.pedID[1]))
									DELETE_OBJECT(sData.objID[iObjLoop])
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF

					SET_PED_TO_RAGDOLL(sData.pedID[1], 100, 500, TASK_RELAX)
					APPLY_DAMAGE_TO_PED(sData.pedID[1], 1000, TRUE)
					IF IS_ENTITY_ALIVE( sData.pedID[0] )
						SET_PED_TO_RAGDOLL(sData.pedID[0], 100, 500, TASK_RELAX)
						APPLY_DAMAGE_TO_PED(sData.pedID[0], 1000, TRUE)
					ENDIF
					PRINT_LAUNCHER_DEBUG("Sitting Rampage attacked with explosives, cleaning up")
					Script_Cleanup(sData)
				ENDIF
			ENDIF
		ENDIF
		
		//If any peds are attacking the player, cause rampage peds to flee for bug 1409012
		IF CHECK_PEDS_ATTACKING_PLAYER(sData)
			FORCE_FREEZE_AND_RELEASE_RAMPAGE1_CHAIRS(sData)
			PRINT_LAUNCHER_DEBUG(" RC combat happening in area, tell peds to flee")
			MAKE_ALL_INITIAL_SCENE_PEDS_FLEE(sData)
			Script_Cleanup(sData)
		ENDIF
		
		//Is the player still in activation range
		IF NOT IS_RC_FINE_AND_IN_RANGE(sData)
			PRINT_LAUNCHER_DEBUG(" RC Is Not Fine And In Range")
			Script_Cleanup(sData)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS(sData)
		#ENDIF
		
		IF IS_RC_MISSION_AVAILABLE(RC_RAMPAGE_1)
			IF IS_SCENARIO_GROUP_ENABLED("Rampage1")
				PRINT_LAUNCHER_DEBUG(" Turning Off Scenario Group For Rampage 1")
				SET_SCENARIO_GROUP_ENABLED("Rampage1", FALSE)
			ENDIF	
		ENDIF
		
		//PRINT_LAUNCHER_DEBUG(" Is Fine And In Range")
		
		// Update launcher blip + preload intro
		SET_RC_AWAITING_TRIGGER(sData.eMissionID)
		MANAGE_PRELOADING_RC_CUTSCENE(iCutsceneLoadRequestID, sData.sIntroCutscene, vInCoords)
		IF (bHasCutsceneLoaded = FALSE) AND (iCutsceneLoadRequestID <> NULL_OFFMISSION_CUTSCENE_REQUEST)
			PRESTREAM_RAMPAGE_INTRO_CUTSCENE_PED_VARIATIONS(sData, sSceneHandles)
			IF HAS_CUTSCENE_LOADED()
				PRINT_LAUNCHER_DEBUG(" Cutscene Loaded")
				bHasCutsceneLoaded = TRUE
			ENDIF			
		ENDIF
		
		vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		IF NOT ((sData.eMissionID = RC_RAMPAGE_1) AND (vPlayerPos.z > 34.0))
		AND NOT ((sData.eMissionID = RC_RAMPAGE_2) AND (vPlayerPos.z > 71.0))
		AND NOT ((sData.eMissionID = RC_RAMPAGE_3) AND (vPlayerPos.z > 30.0))
			//Is the player close enough to start the mission off
			IF ARE_RC_TRIGGER_CONDITIONS_MET(sData)
				IF (sData.eMissionID = RC_RAMPAGE_2)
					IF LEADIN_RAMPAGE_2(sData)
						PRINT_LAUNCHER_DEBUG(" Trigger Conditions Met")
						
						RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
						
						//Launch the mission script
						IF NOT LAUNCH_RC_MISSION(sData)
							PRINT_LAUNCHER_DEBUG(" RC Can't Launch")
							Script_Cleanup(sData)
						ENDIF
							
						// force terminate for ambient missions
						PRINT_LAUNCHER_DEBUG(" RC Launcher Waiting To Terminate")
						IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sData.eMissionID)	
							PRINT_LAUNCHER_DEBUG(" Ready To Terminate")
							Script_Cleanup(sData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
						ENDIF
					ENDIF
				ELSE
					IF FOCUS_PUSH_LEADIN(sData)
						PRINT_LAUNCHER_DEBUG(" Trigger Conditions Met")
						
						RC_PLAYER_TRIGGER_SCENE_LOCK_IN()
						
						//Launch the mission script
						IF NOT LAUNCH_RC_MISSION(sData)
							PRINT_LAUNCHER_DEBUG(" RC Can't Launch")
							Script_Cleanup(sData)
						ENDIF
							
						// force terminate for ambient missions
						PRINT_LAUNCHER_DEBUG(" RC Launcher Waiting To Terminate")
						IF IS_RC_LAUNCHER_SAFE_TO_TERMINATE(sData.eMissionID)	
							PRINT_LAUNCHER_DEBUG(" Ready To Terminate")
							Script_Cleanup(sData, FALSE)			//No need to clean up entities, as we should have passed them over to the mission script
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF (sData.eMissionID = RC_RAMPAGE_2)
					preleadin_RAMPAGE_2( sData )
				ENDIF
				IF IS_GAMEPLAY_HINT_ACTIVE()
					iFocusPushTimer = -1
					STOP_GAMEPLAY_HINT()
					TASK_CLEAR_LOOK_AT( PLAYER_PED_ID() )
				ENDIF
			ENDIF
		ENDIF
	ENDWHILE
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT(coords_struct in_coords)
	
#IF IS_DEBUG_BUILD
	SETUP_DEBUG_WIDGETS()
#ENDIF
	
	// Launcher priority for streaming requests
	SET_THIS_IS_A_TRIGGER_SCRIPT(TRUE)
	
	RC_LAUNCHER_START()
	
	g_structRCScriptArgs sRCLauncherData	 	// Scene information to pass to mission script
	VECTOR vInCoords = in_coords.vec_coord[0] 	// Update world point
	
	CPRINTLN(DEBUG_RANDOM_CHAR, "Launcher RCRampage - Version:", SCRIPT_VERSION, " Initializing.")
	//BREAK_ON_NATIVE_COMMAND("CLEAR_AREA_OF_VEHICLES", FALSE)
	
	//Reset all basic values of the data, so each scene has to set them up correctly
	bIsRampLauncherRunning = TRUE
	RC_Reset_LauncherData(sRCLauncherData)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
		PRINT_LAUNCHER_DEBUG(" Force cleanup [TERMINATING]")
		SCRIPT_CLEANUP(sRCLauncherData)
	ENDIF
		
	// Moved the main loop to this function as we have two different methods dependant on whether we have done all rampages or not
	IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
		UPDATE_RC_LAUNCHER(sRCLauncherData, vInCoords)
	ELSE
		UPDATE_AMBIENT_LAUNCHER(sRCLauncherData, vInCoords)
	ENDIF

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT


