// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	rampage_controller.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	This handles the unlocking of rampages and turning on off blips
//
// *****************************************************************************************
// *****************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "commands_brains.sch"
USING "flow_public_core.sch"
USING "flow_public_game.sch"

//----------------------
//	ENUM
//----------------------
/* this is commented out on purpose it's just so i can remember what order the medals go in
ENUM RAMPAGE_MEDAL
	RAMPAGE_NOMEDAL,		// 0
	RAMPAGE_BRONZE,			// 1
	RAMPAGE_SILVER,			// 2
	RAMPAGE_GOLD			// 3
ENDENUM
*/

//----------------------
//	CONSTANTS
//----------------------
CONST_INT RAMPCTRL_VERSION_NUMBER 001

//----------------------
//	VARIABLES
//----------------------
INT iFirstRBlipIndex = ENUM_TO_INT(STATIC_BLIP_MINIGAME_RAMPAGE1)
INT iFirstRCPercentIndex = ENUM_TO_INT(CP_OJ_RAM1)
INT iFirstRCIndex = ENUM_TO_INT(RC_RAMPAGE_1)

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID mRampctrlWidgetGroup
	BOOL bAllRampageComplete
	BOOL bDebugKillScript
	BOOL bRampageCompleteFlags[NUM_OF_RAMPAGES]
#ENDIF

//----------------------
//	FUNCTIONS
//----------------------

/// PURPOSE:
///    Checks to see if a mission is happening or we are not trevor
/// RETURNS:
///    True if that is the case
FUNC BOOL ARE_WE_RUNNING_A_MISSION_OR_SOMETHING()
	RETURN (IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_MINIGAME)) OR (g_bIsOnRampage) OR (GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR) OR g_RandomChars[RC_RAMPAGE_5].rcLeaveAreaCheck
ENDFUNC

/// PURPOSE:
///    Removes all Rampage Blips
PROC ACTIVATE_RAMPAGE_BLIPS(BOOL ok)
	INT i 
	STATIC_BLIP_NAME_ENUM blipname
	
	REPEAT NUM_OF_RAMPAGES i
		blipname = INT_TO_ENUM(STATIC_BLIP_NAME_ENUM, iFirstRBlipIndex + i)
		SET_STATIC_BLIP_ACTIVE_STATE(blipname, ok)
		SET_STATIC_BLIP_CHARACTER_VISIBILITY(blipname, TRUE, CHAR_TREVOR)
		SET_STATIC_BLIP_HIDDEN_IN_MISSION(blipname, TRUE)
		SET_STATIC_BLIP_COLOUR(blipname, BLIP_COLOUR_TREVOR)
		
		IF (g_savedGlobals.sRampageData.playerData[i].iMedalIndex >= ENUM_TO_INT(RAMPAGE_BRONZE))
		OR (GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED) = TRUE)
		OR (HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(INT_TO_ENUM(enumCompletionPercentageEntries, iFirstRCPercentIndex + i), FALSE))
		OR (IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[INT_TO_ENUM(g_eRC_MissionIDs, iFirstRCIndex + i)].rcFlags,  ENUM_TO_INT(RC_FLAG_COMPLETED)))
				
			SET_STATIC_BLIP_HAS_CHECKMARK(blipname, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    SETS UP ALL RAMPAGE DEBUG WIDGETS
	PROC SETUP_RAMPCTRL_DEBUG_WIDGETS()
		INT i
		TEXT_LABEL nme
		
		mRampctrlWidgetGroup = START_WIDGET_GROUP("Rampage Controller")	
			ADD_WIDGET_BOOL("Force Quit Script", bDebugKillScript)
			ADD_WIDGET_BOOL("Are All Rampages Complete", bAllRampageComplete)
			ADD_WIDGET_BOOL("Is Launcher Runing", bIsRampLauncherRunning)
			
			START_WIDGET_GROUP("Rampage Complete Flags")
				REPEAT COUNT_OF(bRampageCompleteFlags) i
					nme = "Rampage"
					nme += (i + 1)
					ADD_WIDGET_BOOL(nme, bRampageCompleteFlags[i])
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	ENDPROC 

	/// PURPOSE: 
	/// 	Cleans up the Widgets
	PROC CLEANUP_RAMPCTRL_DEBUG_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(mRampctrlWidgetGroup)
			DELETE_WIDGET_GROUP(mRampctrlWidgetGroup)
		ENDIF
	ENDPROC 

#ENDIF

//----------------------
//	SCRIPT FUNCTIONS
//----------------------

/// PURPOSE:
///    Cleanups and Terminates the script
PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Terminating") 
		CLEANUP_RAMPCTRL_DEBUG_WIDGETS()
	#ENDIF
	
	ACTIVATE_RAMPAGE_BLIPS(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	DEBUG FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD

	/// PURPOSE:
	///    Updates The Debug Widgets
	PROC UPDATE_RAMPCTRL_DEBUG_WIDGETS()
		INT i 
		
		bAllRampageComplete = GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
		IF (bDebugKillScript)
			CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Killed By Debug Widget") 
			SCRIPT_CLEANUP()
		ENDIF

		REPEAT COUNT_OF(bRampageCompleteFlags) i
			bRampageCompleteFlags[i] = 
				HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(INT_TO_ENUM(enumCompletionPercentageEntries, iFirstRCPercentIndex + i), FALSE) 
				OR IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[INT_TO_ENUM(g_eRC_MissionIDs, iFirstRCIndex + i)].rcFlags,  ENUM_TO_INT(RC_FLAG_COMPLETED))
		ENDREPEAT
		
	ENDPROC
#ENDIF

//----------------------
//	SCRIPT LOOP
//----------------------
SCRIPT
	INT i 
	
	// setup blip index values here
	iFirstRBlipIndex = ENUM_TO_INT(STATIC_BLIP_MINIGAME_RAMPAGE1)

	// Check to see if an instance of this script is running if so abort
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("rampage_controller")) > 1
    	CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF	
	
	// Setup widgets
	#IF IS_DEBUG_BUILD
		SETUP_RAMPCTRL_DEBUG_WIDGETS()
	#ENDIF
	
	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		SCRIPT_CLEANUP()
	ENDIF
	
	CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Initializing v", RAMPCTRL_VERSION_NUMBER) 
	IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_ALL_RAMPAGES_UNLOCKED)
		CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - All Rampages Not Unlocked") 
		SCRIPT_CLEANUP()
	ENDIF

	// register the script so we can relaunch it on save
	REGISTER_SCRIPT_TO_RELAUNCH_LIST(LAUNCH_BIT_RAMPAGE_CONTROLLER)
	
	// mark all rampages as complete
	REPEAT NUM_OF_RAMPAGES i
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(INT_TO_ENUM(enumCompletionPercentageEntries, iFirstRCPercentIndex + i)) 
		SET_BIT(g_savedGlobals.sRandomChars.savedRC[INT_TO_ENUM(g_eRC_MissionIDs, iFirstRCIndex + i)].rcFlags,  ENUM_TO_INT(RC_FLAG_COMPLETED))
	ENDREPEAT
	
	//Show blips.
	ACTIVATE_RAMPAGE_BLIPS(TRUE)
	
	CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Initial Setup Complete") 
	
	WHILE (TRUE)
	
		// debug display and widgets
		#IF IS_DEBUG_BUILD	
			UPDATE_RAMPCTRL_DEBUG_WIDGETS()
		#ENDIF
		
		WAIT(0)
		
		// Try and display "Rampages available" help until it sucessfully displays once.
		// Also use the flow queue to ensure this doesn't display on-mission, doesn't conflict with 
		// other off-mission help, and only displays to Trevor. #1058438
		IF NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_RAMPAGES_UNLOCKED)
			SWITCH GET_FLOW_HELP_MESSAGE_STATUS("RAMP_NEW_ALL")
				CASE FHS_EXPIRED
					ADD_HELP_TO_FLOW_QUEUE("RAMP_NEW_ALL", FHP_MEDIUM, 0, 2000, DEFAULT_HELP_TEXT_TIME, BIT_TREVOR)
				BREAK
				CASE FHS_DISPLAYED
					SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_RAMPAGES_UNLOCKED)
				BREAK
			ENDSWITCH
		ENDIF
		
		// if a mission starts we kill all the blips 
		IF (ARE_WE_RUNNING_A_MISSION_OR_SOMETHING())
			CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Waiting Till Mission Is Complete") 
			ACTIVATE_RAMPAGE_BLIPS(FALSE)
			
			WHILE ARE_WE_RUNNING_A_MISSION_OR_SOMETHING()
				#IF IS_DEBUG_BUILD	
					UPDATE_RAMPCTRL_DEBUG_WIDGETS()
				#ENDIF		
				
				WAIT(0)
			ENDWHILE
			
			CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Mission Is Complete")
			CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Waiting For autosave to finish")
			WHILE IS_AUTO_SAVE_IN_PROGRESS()
				WAIT(0)
			ENDWHILE
			
			CPRINTLN(DEBUG_RAMPAGE, "Rampage Controller - Autosave finished")
			ACTIVATE_RAMPAGE_BLIPS(TRUE)
		ENDIF
		
	ENDWHILE
ENDSCRIPT
