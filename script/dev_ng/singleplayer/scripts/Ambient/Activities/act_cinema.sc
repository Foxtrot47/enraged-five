//World Point template

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_event.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "script_blips.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "AggroSupport.sch"
USING "LineActivation.sch"
USING "finance_control_public.sch"
USING "context_control_public.sch"
USING "commands_debug.sch"
USING "flow_public_core_override.sch"
USING "friendActivity_public.sch"
USING "cutscene_public.sch"
//USING "net_cutscene.sch"
USING "net_FPS_Cam.sch"
USING "FM_Unlocks_Header.sch"
USING "net_hud_displays.sch"
//USING "net_invite_menu.sch"
USING "net_spawn.sch"
USING "CompletionPercentage_public.sch"
USING "tv_control_public.sch"
USING "cheat_handler.sch"
USING "timer_public.sch"
USING "net_spectator_cam.sch"
USING "net_cash_transactions.sch"
USING "script_misc.sch"

//vars for controlable first person view
//FIRST_PERSON_CAMERA fpsCam
//REINIT_FIRST_PERSON_CAMERA_STRUCT reinitCam

FIRST_PERSON_CAM_STRUCT newFPCam

SCRIPT_TIMER netStoredTime

//INVITE_ONLY_MENU_CONTROL_STRUCT inviteOnlyMenu
///INVITE_MENU_STRUCT sInviteMenu

//BOOL bInitInviteOptions
STRUCT CINEMA_INFO
	VECTOR vSeatPos
	FLOAT fSeatHeading
	VECTOR vLookCamPos
	VECTOR vLookCamRot
	VECTOR vWarpExitLoc
	FLOAT fWarpExitHeading
	enumActivityLocation eLocation
	VECTOR vScreenCentre
	INT iVoiceChannel
	VECTOR vWarpInsideCoords
	VECTOR vScenarioActivationPoint
ENDSTRUCT
CINEMA_INFO cinemaInfo

VECTOR vInput
//	FLOAT fInput

INT iContextButtonIntention = NEW_CONTEXT_INTENTION
INT iScriptTransactionIndex

//BOOL bIgnoreTimeConstraints
//BOOL bActivityTimeHelpPrinted

//BOOL bActivityWantedText

BOOL startedFadeOut
BOOL bWarpStarted
//BOOL bGotoCleanUpAfterCorona
BOOL bDisplayedBEHelp

//SCALEFORM_INSTRUCTIONAL_BUTTONS instructionalButtons
//SPRITE_PLACEMENT aSpriteCinemaControls
//SCALEFORM_INDEX scaleformCinemaControls
BOOL bUpdateCinemaControls = FALSE

STRING sAudioScene = "NULL"

CAMERA_INDEX camSCTV

//BLIP_INDEX blipDoor

INTERIOR_INSTANCE_INDEX interiorCinema

//CAMERA_INDEX cameraCinema
INT iRenderTarget = -1


SCRIPT_TIMER LoadMovieTimeOut
//BINK_MOVIE_ID MovieId

INT iEnterTimeStamp

SCRIPT_TIMER cashDisplayTimer

INT iSlowServerPlayerLoop
BOOL bAtLeast1PlayerInCinema
BOOL bPlayerLeftCinemaBecauseMovieEnded = FALSE

//FLOAT fMovieVolume

SCRIPT_TIMER spawnPedsTimeout

//FLOAT fMinYChange	= -4
//FLOAT fMaxYChange = 4
//FLOAT fMinXChange	= -45
//FLOAT fMaxXChange = 45
//FLOAT fCamSpeed = 4.0
//FLOAT fCamChangeY
//FLOAT fCamChangeX
//CONST_INT STICK_DEAD_ZONE 28

CONST_INT CINEMA_BS_EXITED							0
CONST_INT CINEMA_BS_DISPLAYED_EXIT_HELP				1
CONST_INT CINEMA_BS_MOVIE_STARTED					2
CONST_INT CINEMA_BS_SHOW_EXIT_WARNING				3
CONST_INT CINEMA_BS_PRESSED_CANCEL					4
CONST_INT CINEMA_BS_TAKEN_MONEY						5
CONST_INT CINEMA_BS_SCTV_CAMERA_CREATED				6
CONST_INT CINEMA_BS_SET_VOICE_CHANNEL				7
CONST_INT CINEMA_BS_SETUP_SPECIFIC_SPAWN			8
CONST_INT CINEMA_BS_KICKED_OUT_CLOSED				9
CONST_INT CINEMA_BS_SCTV_INTERIOR_LOAD_REQUESTED	10
CONST_INT CINEMA_BS_SCTV_NEEDS_CLEANUP				11
INT iCinemaBS

CONST_INT LOCAL_STAGE_INIT					0
CONST_INT LOCAL_STAGE_WAIT_TO_ENTER			1
CONST_INT LOCAL_STAGE_HANDLE_ENTERING		2	
CONST_INT LOCAL_STAGE_WATCHING_MOVIE		3
CONST_INT LOCAL_STAGE_SCTV					4
CONST_INT LOCAL_STAGE_LEFT_CINEMA			5
CONST_INT LOCAL_STAGE_END					6
INT iLocalStage

CONST_INT iBS_STORE_ACTIVE					0
INT iBoolsBitSet

ENUM CINEMA_SHOWING_ENUM
	CINEMA_SHOWING_NONE = 0,
	CINEMA_SHOWING_MORNING,
	CINEMA_SHOWING_AFTERNOON,
	CINEMA_SHOWING_EVENING,
	CINEMA_SHOWING_MULTIPLAYER
ENDENUM

CINEMA_SHOWING_ENUM eCurrentCinemaShowing = CINEMA_SHOWING_NONE

CONST_INT CINEMA_TIME_MORNING_START		10
CONST_INT CINEMA_TIME_MORNING_END		13
CONST_INT CINEMA_TIME_AFTERNOON_START	13
CONST_INT CINEMA_TIME_AFTERNOON_END		17
CONST_INT CINEMA_TIME_EVENING_START		17
CONST_INT CINEMA_TIME_EVENING_END		22

CONST_INT ACTIVITY_COST 20
INT iParticipants = 1

BOOL bStartedInMP
BOOL bCinemaTookAwayPlayerControl

BOOL bSetActAvailable

INT iEnterLeaveBS
INT iSlowParticipantLoop



WEAPON_TYPE currentWeapon

//STRUCT WARNING_SCREEN_STRUCT
//	STRING sQuestionText
//	FE_WARNING_FLAGS fButtonFlags
//ENDSTRUCT
//WARNING_SCREEN_STRUCT warningScreen

STRUCT SEAT_DETAILS
	VECTOR vCamPos
	VECTOR vCamRot
	FLOAT fCamFOV
	VECTOR vSeatPos
	FLOAT fSeatHeading
ENDSTRUCT
SEAT_DETAILS thisPlayersSeat

INT seatOrdering[NUM_NETWORK_PLAYERS]

// The server broadcast data.
//// Everyone can read this data, only the server can update it. 
//STRUCT ServerBroadcastData
//	//SCRIPT_TIMER MovieStartTimer
//	//FLOAT fMovieStartPosition
//	//INT iChannel
//ENDSTRUCT
//ServerBroadcastData serverBD

STRUCT ServerBroadcastData
	INT iSeats[NUM_NETWORK_PLAYERS]
	INT iAssignedSeatBS
	BOOL bSpawnedPeds
ENDSTRUCT
ServerBroadcastData serverBD

STRUCT PlayerBroadcastData
	BOOL bInCinema
	BOOL bWarpedInside
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

BOOL bIsCinemaOwnerInPlayerGroup

#IF IS_DEBUG_BUILD
	INT iCurrentSeat
	INT iDebugSeat
	BOOL bRefocusCam
	BOOL bRefocusedCam
#ENDIF

/// PURPOSE:
///    Needed so Esc key on PC doesn't quit the cinema
///    and so we can use right mouse button to quit.
/// RETURNS:
///    
FUNC BOOL IS_CANCEL_JUST_PRESSED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			RETURN TRUE
		ENDIF
	
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			RETURN TRUE
		ENDIF
	
	ELSE
		
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			RETURN TRUE
		ENDIF
	
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Disables/enables the interior
/// PARAMS:
///    bIsDisabled - TRUE to disable, FALSE to enable.
PROC DISABLE_CINEMA_INTERIOR( BOOL bIsDisabled )

	SWITCH cinemaInfo.eLocation

		CASE ALOC_cinema_downtown
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_DOWNTOWN, bIsDisabled )
		BREAK
		
		CASE ALOC_cinema_vinewood
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_VINEWOOD, bIsDisabled )
		BREAK
		
		CASE ALOC_cinema_morningwood
			SET_INTERIOR_DISABLED(INTERIOR_V_CINEMA_MORNINGWOOD, bIsDisabled )
		BREAK
	
	ENDSWITCH
		
ENDPROC



FUNC BOOL IS_CINEMA_OWNER_IN_PLAYER_GROUP()

	enumCharacterList eCharacterPlayer = GET_CURRENT_PLAYER_PED_ENUM()
	enumCharacterList eCharacterFriendA = CHAR_BLOCKED // Using CHAR)_BLOCKED as a default value as the player shouldn't be set to this value normally.
	enumCharacterList eCharacterFriendB = CHAR_BLOCKED
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
		IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
			eCharacterFriendA = GET_PLAYER_PED_ENUM(FRIEND_A_PED_ID())
			
			// Fix for B* 1875291 - NO_CHARACTER is used by the property script so we need to change this value if not a player ped.
			IF eCharacterFriendA = NO_CHARACTER
				eCharacterFriendA = CHAR_BLOCKED
			ENDIF
			
		ENDIF
		
		IF DOES_ENTITY_EXIST(FRIEND_B_PED_ID())
			eCharacterFriendB = GET_PLAYER_PED_ENUM(FRIEND_B_PED_ID())
			
			// Fix for B* 1875291 - NO_CHARACTER is used by the property script so we need to change this value if not a player ped.
			IF eCharacterFriendB = NO_CHARACTER
				eCharacterFriendB = CHAR_BLOCKED
			ENDIF
			
		ENDIF
	ENDIF
	
	SWITCH cinemaInfo.eLocation

		CASE ALOC_cinema_downtown		
	
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_DOWNTOWN) = eCharacterPlayer
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_DOWNTOWN - CINEMA IS OWNED BY PLAYER" )
				RETURN TRUE
			ENDIF
			
									
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_DOWNTOWN) = eCharacterFriendA
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_DOWNTOWN - CINEMA IS OWNED BY FRIEND A" )
				RETURN TRUE
			ENDIF
					
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_DOWNTOWN) = eCharacterFriendB
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_DOWNTOWN - CINEMA IS OWNED BY FRIEND B" )
				RETURN TRUE
			ENDIF	
							
		BREAK
		
		CASE ALOC_cinema_vinewood
		
			CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_VINEWOOD - CINEMA CAN'T BE OWNED" )
		
			// The cinema the player owns in Vinewood isn't the one you can use, so we
			// always return false here.
			// This is because the cinema you can visit got changed to the Chinese theater
			// but the cinema you can buy didn't get changed - it's next door.
		
			/*
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_VINEWOOD) = eCharacterPlayer
				CPRINTLN( DEBUG_AMBIENT, "PROPERTY_CINEMA_VINEWOOD - CINEMA IS OWNED BY PLAYER" )
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_VINEWOOD) = eCharacterFriendA
				CPRINTLN( DEBUG_AMBIENT, "PROPERTY_CINEMA_VINEWOOD - CINEMA IS OWNED BY FRIEND A" )
				RETURN TRUE
			ENDIF
				
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_VINEWOOD) = eCharacterFriendB
				CPRINTLN( DEBUG_AMBIENT, "PROPERTY_CINEMA_VINEWOOD - CINEMA IS OWNED BY FRIEND B" )
				RETURN TRUE
			ENDIF
			
			*/
			
			RETURN FALSE
		
		BREAK
		
		CASE ALOC_cinema_morningwood
						
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_MORNINGWOOD) = eCharacterPlayer
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_MORNINGWOOD - CINEMA IS OWNED BY PLAYER" )
				RETURN TRUE
			ENDIF
			
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_MORNINGWOOD) = eCharacterFriendA
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_MORNINGWOOD - CINEMA IS OWNED BY FRIEND A" )
				RETURN TRUE
			ENDIF
				
			IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CINEMA_MORNINGWOOD) = eCharacterFriendB
				CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: PROPERTY_CINEMA_MORNINGWOOD - CINEMA IS OWNED BY FRIEND B" )
				RETURN TRUE
			ENDIF	
			
		BREAK
		
	ENDSWITCH
	
	CPRINTLN( DEBUG_AMBIENT, "ACT_CINEMA: CURRENT CINEMA ISN'T OWNED BY ANY CHAR IN THE CURRENT GROUP" )

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Pays for and logs the transaction against the correct cinema location.
PROC PAY_FOR_CINEMA()

	// Cinema is free for owner
	IF IS_CINEMA_OWNER_IN_PLAYER_GROUP()
		EXIT
	ENDIF

	SWITCH cinemaInfo.eLocation

		CASE ALOC_cinema_downtown
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_PROP_DCINEMA, ACTIVITY_COST*iParticipants)
			BREAK
		
		CASE ALOC_cinema_vinewood
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_PROP_VCINEMA, ACTIVITY_COST*iParticipants)
			BREAK
		
		CASE ALOC_cinema_morningwood
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_PROP_MCINEMA, ACTIVITY_COST*iParticipants)
		BREAK
	
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    Checks the time of day and returns the cinema showing slot.
/// RETURNS:
///    
FUNC CINEMA_SHOWING_ENUM GET_CINEMA_SHOWING()

		INT iTimeOfDay = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			IF iTimeOfDay >= CINEMA_TIME_MORNING_START
			AND iTimeOfDay < CINEMA_TIME_EVENING_END
				//CPRINTLN( DEBUG_AMBIENT, "CINEMA - MORNING SHOWING" )
				RETURN CINEMA_SHOWING_MULTIPLAYER
			ENDIF
		
		ELSE
		
			//CPRINTLN( DEBUG_AMBIENT, "CINEMA - TIME = ", iTimeOfDay )

			IF iTimeOfDay >= CINEMA_TIME_MORNING_START
			AND iTimeOfDay < CINEMA_TIME_MORNING_END
				//CPRINTLN( DEBUG_AMBIENT, "CINEMA - MORNING SHOWING" )
				RETURN CINEMA_SHOWING_MORNING
			ENDIF
			
			IF iTimeOfDay >= CINEMA_TIME_AFTERNOON_START
			AND iTimeOfDay < CINEMA_TIME_AFTERNOON_END
				//CPRINTLN( DEBUG_AMBIENT, "CINEMA - AFTERNOON SHOWING" )
				RETURN CINEMA_SHOWING_AFTERNOON
			ENDIF
			
			IF iTimeOfDay >= CINEMA_TIME_EVENING_START
			AND iTimeOfDay <= CINEMA_TIME_EVENING_END
				//CPRINTLN( DEBUG_AMBIENT, "CINEMA - EVENING SHOWING" )
				//
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
					RETURN CINEMA_SHOWING_EVENING
				ELSE
					RETURN CINEMA_SHOWING_AFTERNOON
				ENDIF
			ENDIF
			
		ENDIF
		
		//CPRINTLN( DEBUG_AMBIENT, "CINEMA - NO SHOWING" )
		RETURN CINEMA_SHOWING_NONE
ENDFUNC


PROC SET_CINEMA_AUDIO_SCENE()

	sAudioScene = "NULL"
		
	SWITCH cinemaInfo.eLocation

		CASE ALOC_cinema_downtown
			sAudioScene = "CINEMA_DOWNTOWN"
		BREAK
		
		CASE ALOC_cinema_vinewood
			sAudioScene = "CINEMA_VINEWOOD"
		BREAK
		
		CASE ALOC_cinema_morningwood
			sAudioScene = "CINEMA_MORNINGWOOD"
		BREAK
	
	ENDSWITCH
	
	IF NOT ARE_STRINGS_EQUAL("NULL", sAudioScene)
		IF NOT IS_AUDIO_SCENE_ACTIVE(sAudioScene)
			START_AUDIO_SCENE( sAudioScene )
		ENDIF
	ENDIF
	
ENDPROC



PROC STOP_CINEMA_AUDIO_SCENE()

	IF NOT ARE_STRINGS_EQUAL("NULL", sAudioScene)
		IF IS_AUDIO_SCENE_ACTIVE(sAudioScene)
			STOP_AUDIO_SCENE( sAudioScene )
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE( "LEAVE_CINEMA")
		STOP_AUDIO_SCENE(  "LEAVE_CINEMA" )
	ENDIF
		
ENDPROC

PROC ACTIVATE_SCENARIO_GROUP()
	SWITCH cinemaInfo.eLocation 
		CASE ALOC_cinema_downtown
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Textile")
				IF NOT IS_SCENARIO_GROUP_ENABLED("Cinema_Textile")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Textile",TRUE)
					PRINTLN("Player turning on scenario group: Cinema_Textile")
				ENDIF
			ENDIF	
		BREAK
		CASE ALOC_cinema_morningwood
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Morningwood")
				IF NOT IS_SCENARIO_GROUP_ENABLED("Cinema_Morningwood")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Morningwood",TRUE)
					PRINTLN("Player turning on scenario group: Cinema_Morningwood")
				ENDIF
			ENDIF	
		BREAK
		CASE ALOC_cinema_vinewood
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Downtown")
				IF NOT IS_SCENARIO_GROUP_ENABLED("Cinema_Downtown")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Downtown",TRUE)
					PRINTLN("Player turning on scenario group: Cinema_Downtown")
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH
ENDPROC

PROC DEACTIVATE_SCENARIO_GROUP()
	SWITCH cinemaInfo.eLocation 
		CASE ALOC_cinema_downtown
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Textile")
				IF IS_SCENARIO_GROUP_ENABLED("Cinema_Textile")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Textile",FALSE)
					PRINTLN("Player turning off scenario group: Cinema_Textile")
				ENDIF
			ENDIF	
		BREAK
		CASE ALOC_cinema_morningwood
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Morningwood")
				IF IS_SCENARIO_GROUP_ENABLED("Cinema_Morningwood")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Morningwood",FALSE)
					PRINTLN("Player turning off scenario group: Cinema_Morningwood")
				ENDIF
			ENDIF	
		BREAK
		CASE ALOC_cinema_vinewood
			IF DOES_SCENARIO_GROUP_EXIST("Cinema_Downtown")
				IF IS_SCENARIO_GROUP_ENABLED("Cinema_Downtown")
					SET_SCENARIO_GROUP_ENABLED("Cinema_Downtown",FALSE)
					PRINTLN("Player turning off scenario group: Cinema_Downtown")
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_CONTEXT_OVERRIDE_HELP()
	IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ACTCIN",g_sMptunables.icinema_expenditure_modifier)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC INIT_SCTV_CINEMA()
	ACTIVATE_SCENARIO_GROUP()
ENDPROC

PROC CLEANUP_SCTV_CINEMA()
	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SCTV_NEEDS_CLEANUP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === CLEANUP_SCTV_CINEMA")
		#ENDIF
		IF DOES_CAM_EXIST(camSCTV)
			DESTROY_CAM(camSCTV)
		ENDIF
		IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SCTV_CAMERA_CREATED)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		UNPIN_INTERIOR(interiorCinema)
		DISABLE_CINEMA_INTERIOR(TRUE)
		DEACTIVATE_SCENARIO_GROUP()
		ENABLE_SPECTATOR_FADES()
		CLEAR_BIT(iCinemaBS,CINEMA_BS_SCTV_INTERIOR_LOAD_REQUESTED)
		CLEAR_BIT(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
		CLEAR_BIT(iCinemaBS,CINEMA_BS_SCTV_CAMERA_CREATED)
		CLEAR_BIT(iCinemaBS,CINEMA_BS_SCTV_NEEDS_CLEANUP)
	ENDIF
ENDPROC

PROC MISSION_CLEANUP()
	PRINTSTRING("ACT_CINEMA: doing mission cleanup")
	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
		//IF MovieId != NULL
		//	STOP_BINK_MOVIE(MovieId)
		//	RELEASE_BINK_MOVIE(MovieId)
		//ENDIF
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
		RESTORE_STANDARD_CHANNELS()
		SET_STORE_ENABLED(TRUE)
		STOP_CINEMA_AUDIO_SCENE()
	
		ENABLE_MOVIE_SUBTITLES(FALSE)
		
		RESET_HUD_COMPONENT_VALUES(NEW_HUD_SUBTITLE_TEXT)
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CLEANUP_SCTV_CINEMA()
	ENDIF
	
	//Reset the global (previously used by chop)
	g_bPlayerIsInCinema = FALSE
	
	CLEAR_FIRST_PERSON_CAMERA(newFPCam)
	IF iRenderTarget != -1
		IF IS_NAMED_RENDERTARGET_REGISTERED("cinscreen")
			RELEASE_NAMED_RENDERTARGET("cinscreen")
		ENDIF
	ENDIF
	IF bCinemaTookAwayPlayerControl
		IF NOT bStartedInMP
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), cinemaInfo.vWarpExitLoc)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), cinemaInfo.fWarpExitHeading)
			ENDIF
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			LOAD_SCENE(cinemaInfo.vWarpExitLoc)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(),FALSE)
		ENDIF
	ENDIF
	IF hasActivityStarted(cinemaInfo.eLocation)
		IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())	
			FREEZE_ENTITY_POSITION(FRIEND_A_PED_ID(), FALSE)
			SET_ENTITY_COORDS(FRIEND_A_PED_ID(), cinemaInfo.vWarpExitLoc+<<1,0,0>>)
			SET_ENTITY_HEADING(FRIEND_A_PED_ID(), cinemaInfo.fWarpExitHeading)
			SET_PED_AS_GROUP_MEMBER(FRIEND_A_PED_ID(), PLAYER_GROUP_ID())
		ENDIF
		IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())	
			FREEZE_ENTITY_POSITION(FRIEND_B_PED_ID(), FALSE)
			SET_ENTITY_COORDS(FRIEND_B_PED_ID(), cinemaInfo.vWarpExitLoc+<<-1,0,0>>)
			SET_ENTITY_HEADING(FRIEND_B_PED_ID(), cinemaInfo.fWarpExitHeading)
			SET_PED_AS_GROUP_MEMBER(FRIEND_B_PED_ID(), PLAYER_GROUP_ID())
		ENDIF
		
		// Tell friend activity cinema has ended (passing AR_playerDraw indicates the movie watched was Meltdown)
		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_MICHAEL_4)
		AND GET_CINEMA_SHOWING() = CINEMA_SHOWING_EVENING
			finishActivity(cinemaInfo.eLocation, AR_playerDraw)
			PRINTLN("ACT_CINEMA: FRIEND ACTIVITY - PLAYER WATCHED MELTDOWN")
		ELSE
			
			IF bPlayerLeftCinemaBecauseMovieEnded
				PRINTLN("ACT_CINEMA: FRIEND ACTIVITY - PLAYER WATCHED WHOLE FILM")
				finishActivity(cinemaInfo.eLocation, AR_playerWon )
			ELSE
				PRINTLN("ACT_CINEMA: FRIEND ACTIVITY - PLAYER LEFT EARLY")
				finishActivity(cinemaInfo.eLocation, AR_playerQuit)
			ENDIF
			
		ENDIF
	//ELSE
	//	DO_SCREEN_FADE_IN(500)
	ENDIF
	
	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
		CLEAR_SPECIFIC_SPAWN_LOCATION()
		CLEAR_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
	ENDIF
	IF startedFadeOut
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
			DO_SCREEN_FADE_IN(500)
		ENDIF
		startedFadeOut = FALSE
	ENDIF
	IF bStartedInMP
		UNPause_Objective_Text()
		SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CINEMA, FALSE)
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), cinemaInfo.vWarpExitLoc)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), cinemaInfo.fWarpExitHeading)
				ENDIF
				PRINTLN("ACT_CINEMA: Player was still in the cinema when script terminated. Forcing out of cinema")
			ENDIF
			playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = FALSE
		ENDIF
		IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
			NET_PRINT("ACT_CINEMA: Clearing voice channel: ") NET_NL()
			NETWORK_CLEAR_VOICE_CHANNEL()
			NETWORK_SET_LOCAL_PLAYER_SYNC_LOOK_AT(FALSE)
			CLEAR_BIT(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
		ENDIF
	ENDIF
	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
	//	ENABLE_SELECTOR()
	ENDIF
	IF bSetActAvailable
		REMOVE_ACTIVITY_FROM_DISPLAY_LIST(cinemaInfo.vWarpExitLoc,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CINEMA))
		bSetActAvailable = FALSE
	ENDIF
	
	CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CINEMA)
	
	CLEANUP_MENU_ASSETS()
	// make sure that we have released use of the context button if we still have it
	CLEAR_CONTEXT_OVERRIDE_HELP()
	RELEASE_CONTEXT_INTENTION(iContextButtonIntention) 
	TERMINATE_THIS_THREAD()
	DEACTIVATE_SCENARIO_GROUP()
ENDPROC

/// PURPOSE: Do necessary pre game start ini
PROC PROCESS_PRE_GAME()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits until it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			serverBD.iSeats[i] = -1
		ENDREPEAT
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
		// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
		// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
		IF NOT Wait_For_First_Network_Broadcast()
			PRINTSTRING("ACT_CINEMA: Failed to receive an initial network broadcast. Cleaning up.")
			PRINTNL()
			MISSION_CLEANUP()
		ENDIF
		
		bStartedInMP = TRUE
		PRINTSTRING("ACT_CINEMA: set started in MP game")
		PRINTNL()
	ELSE
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|DEFAULT_FORCE_CLEANUP_FLAGS| FORCE_CLEANUP_FLAG_DIRECTOR)
			PRINTSTRING("ACT_CINEMA: going straight to end as force cleanup has occured")
			PRINTNL()
			MISSION_CLEANUP()
		ENDIF
	ENDIF
ENDPROC

PROC SET_LOCAL_STAGE(INT iStage)
	SWITCH iStage
		CASE LOCAL_STAGE_INIT
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_INIT")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_WAIT_TO_ENTER
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_WAIT_TO_ENTER")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_HANDLE_ENTERING
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_HANDLE_ENTERING")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_WATCHING_MOVIE
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_WATCHING_MOVIE")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_SCTV
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_SCTV")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_LEFT_CINEMA
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_LEFT_CINEMA")
			PRINTNL()
		BREAK
		CASE LOCAL_STAGE_END
			PRINTSTRING("ACT_CINEMA: Set local stage to be : ")
			PRINTSTRING("LOCAL_STAGE_END")
			PRINTNL()
		BREAK
	ENDSWITCH
	iLocalStage = iStage
ENDPROC

PROC INIT_CURRENT_CINEMA_DATA()
	IF VDIST2(vInput, <<394.368, -711.8287, 28.2877>>) < 8 // Cinema, Downtown
		cinemaInfo.vSeatPos = << 377.1530, -717.5670, 10.0536 >>
		cinemaInfo.fSeatHeading = 177.6
		cinemaInfo.vLookCamPos = << 377.1530, -717.5670, 12.0536 >>
		cinemaInfo.vLookCamRot = << 7.5652, 0.0000, -179.1837 >>
		cinemaInfo.vWarpExitLoc = << 394.2567, -713.5439, 28.2853 >>
		cinemaInfo.fWarpExitHeading =  276.6273
		cinemaInfo.eLocation = ALOC_cinema_downtown
		cinemaInfo.vScreenCentre = <<377.1794, -733.5583, 14.5136>>
		cinemaInfo.iVoiceChannel = FM_VOICE_CHANNEL_CINEMA_1
		cinemaInfo.vWarpInsideCoords = <<385.5010, -703.5972, 14.8930>>
		cinemaInfo.vScenarioActivationPoint = <<376.7,-722.8,10.8>>
		seatOrdering[0] = 2
		seatOrdering[1] = 13
		seatOrdering[2] = 1
		seatOrdering[3] = 15
		seatOrdering[4] = 14
		seatOrdering[5] = 0
		seatOrdering[6] = 3
		seatOrdering[7] = 10
		seatOrdering[8] = 6
		seatOrdering[9] = 7
		seatOrdering[10] = 23
		seatOrdering[11] = 24
		seatOrdering[12] = 4
		seatOrdering[13] = 5
		seatOrdering[14] = 16
		seatOrdering[15] = 11
		seatOrdering[16] = 12
		seatOrdering[17] = 19
//		seatOrdering[18] = 22
//		seatOrdering[19] = 21
//		seatOrdering[20] = 20
//		seatOrdering[21] = 18
//		seatOrdering[22] = 17
//		seatOrdering[23] = 25
//		seatOrdering[24] = 26
//		seatOrdering[25] = 31
//		seatOrdering[26] = 30
//		seatOrdering[27] = 27
//		seatOrdering[28] = 28
//		seatOrdering[29] = 29
//		seatOrdering[30] = 8
//		seatOrdering[31] = 9
		PRINTSTRING("\nact_cinema - SETUP_CURRENT_CINEMA() - cinema is Downtown\n")
	ELIF VDIST2(vInput, <<283.7833, 200.4280, 103.3739>>) < 8 // Cinema, Vinewood
		cinemaInfo.vSeatPos = <<320.9934, 265.2515, 82.1221>> 
		cinemaInfo.fSeatHeading = -179.5
		cinemaInfo.vLookCamPos = << 320.9934, 265.2515, 84.1221 >>
		cinemaInfo.vLookCamRot = << 7.6308, -0.0000, 177.8563 >>
		cinemaInfo.vWarpExitLoc = <<301.2162, 202.1357, 103.3797>>
		cinemaInfo.fWarpExitHeading =  156.5144
		cinemaInfo.eLocation = ALOC_cinema_vinewood
		cinemaInfo.vScreenCentre = <<320.223206,247.586487,86.662674>>
		cinemaInfo.iVoiceChannel = FM_VOICE_CHANNEL_CINEMA_2
		cinemaInfo.vWarpInsideCoords = <<329.0103, 277.3879, 86.5655>>
		cinemaInfo.vScenarioActivationPoint = <<318.5567, 264.5815, 81.9749>>
		seatOrdering[0] = 3
		seatOrdering[1] = 8
		seatOrdering[2] = 2
		seatOrdering[3] = 7
		seatOrdering[4] = 20
		seatOrdering[5] = 5
		seatOrdering[6] = 6
		seatOrdering[7] = 4
		seatOrdering[8] = 22
		seatOrdering[9] = 0
		seatOrdering[10] = 1
		seatOrdering[11] = 23
		seatOrdering[12] = 24
		seatOrdering[13] = 25
		seatOrdering[14] = 21
		seatOrdering[15] = 11
		seatOrdering[16] = 12
		seatOrdering[17] = 9
//		seatOrdering[18] = 10
//		seatOrdering[19] = 16
//		seatOrdering[20] = 15
//		seatOrdering[21] = 26
//		seatOrdering[22] = 27
//		seatOrdering[23] = 28
//		seatOrdering[24] = 29
//		seatOrdering[25] = 17
//		seatOrdering[26] = 18
//		seatOrdering[27] = 31
//		seatOrdering[28] = 30
//		seatOrdering[29] = 14
//		seatOrdering[30] = 13
//		seatOrdering[31] = 19
		PRINTSTRING("\nact_cinema - SETUP_CURRENT_CINEMA() - cinema is Vinewood\n")
	ELIF VDIST2(vInput, <<-1422.673, -214.3225, 45.9932>>) < 8 // Cinema, Morningwood
		cinemaInfo.vSeatPos = << -1425.5645, -244.3000, 15.8053 >>
		cinemaInfo.fSeatHeading = 177
		cinemaInfo.vLookCamPos = << -1425.5645, -244.3000, 17.8053 >>
		cinemaInfo.vLookCamRot = << 7.4186, -0.0000, 177.3244 >>
		cinemaInfo.vWarpExitLoc = <<-1423.4724, -214.2539, 45.5004>>
		cinemaInfo.fWarpExitHeading =  353.8757
		cinemaInfo.eLocation = ALOC_cinema_morningwood
		cinemaInfo.vScreenCentre = <<-1426.543701,-258.994781,20.708384>>
		cinemaInfo.iVoiceChannel = FM_VOICE_CHANNEL_CINEMA_3
		cinemaInfo.vWarpInsideCoords = <<-1418.5088, -234.6433, 18.9931>>
		cinemaInfo.vScenarioActivationPoint = <<-1426.4,-248.5,16.1>>
		seatOrdering[0] = 3
		seatOrdering[1] = 8
		seatOrdering[2] = 2
		seatOrdering[3] = 7
		seatOrdering[4] = 20
		seatOrdering[5] = 5
		seatOrdering[6] = 6
		seatOrdering[7] = 4
		seatOrdering[8] = 22
		seatOrdering[9] = 0
		seatOrdering[10] = 1
		seatOrdering[11] = 23
		seatOrdering[12] = 24
		seatOrdering[13] = 25
		seatOrdering[14] = 21
		seatOrdering[15] = 11
		seatOrdering[16] = 12
		seatOrdering[17] = 9
//		seatOrdering[18] = 10
//		seatOrdering[19] = 16
//		seatOrdering[20] = 15
//		seatOrdering[21] = 26
//		seatOrdering[22] = 27
//		seatOrdering[23] = 28
//		seatOrdering[24] = 29
//		seatOrdering[25] = 17
//		seatOrdering[26] = 18
//		seatOrdering[27] = 31
//		seatOrdering[28] = 30
//		seatOrdering[29] = 14
//		seatOrdering[30] = 13
//		seatOrdering[31] = 19
		PRINTSTRING("\nact_cinema - SETUP_CURRENT_CINEMA() - cinema is Morningwood\n")
	ENDIF
ENDPROC

/// PURPOSE:
///    For the issue of working out euler exactly on the ATAN wrap (0.0,0.0,1.0) or (0.0,0.0,-1.0)
/// PARAMS:
///    aFloat - The Z coord of the forward vector.
/// RETURNS:
///    
FUNC FLOAT VERY_SMALL_FLOAT(FLOAT aFloat)
	//
	IF aFloat = 1.0
		RETURN 0.999999
	ENDIF
	IF aFloat = -1.0
		RETURN -0.999999
	ENDIF
RETURN aFloat

ENDFUNC

FUNC VECTOR ComputeEulersFromVector(VECTOR vForwardVector)
	
	VECTOR RotationEulers
	
//	RotationEulers.x = ATAN2(vForwardVector.x, vForwardVector.y)
//	RotationEulers.z = ATAN2(vForwardVector.z, SQRT((vForwardVector.x*vForwardVector.x)+(vForwardVector.y*vForwardVector.y)))

	vForwardVector.z = VERY_SMALL_FLOAT(vForwardVector.z)
	RotationEulers.z = ATAN2(-vForwardVector.x, vForwardVector.y)
	RotationEulers.x = ASIN(vForwardVector.z)
	RotationEulers.y = 0.0
	
	RETURN RotationEulers
ENDFUNC

FUNC VECTOR GET_CAMERA_ROTATION_FROM_TWO_POINTS(VECTOR vStartPos, VECTOR vEndPos)
	VECTOR vReturnValue
	
//	VECTOR vCamTarget = (vEndPos-vStartPos)
//	FLOAT fRotX = ATAN2(vCamTarget.z,  VMAG(<<vCamTarget.x, vCamTarget.y, 0.0>>))
//	vCamTarget = NORMALISE_VECTOR(vCamTarget)
//	FLOAT fRotZ = GET_ANGLE_BETWEEN_2D_VECTORS(0.0, 1.0, vCamTarget.x, vCamTarget.y)
//	VECTOR vTemp
//	vTemp = CROSS_PRODUCT(<<0.0,1.0,0.0>>, vCamTarget)
//	IF vTemp.z > 0
//		fRotZ = ABSF(fRotZ)
//	ELSE
//		fRotZ = (ABSF(fRotZ) * -1)
//	ENDIF
//	vReturnValue.x = fRotx
//	vReturnValue.z = fRotZ
	
//	VECTOR vNormStart = NORMALISE_VECTOR(vStartPos)
//	VECTOR vNormEnd = NORMALISE_VECTOR(vEndPos)
//	FLOAT angle = ACOS( DOT_PRODUCT(vNormStart,vEndPos))
//	IF ABSF(angle <= 0.001)
//		RETURN <<0,0,1>>
//	ENDIF
//	IF ABSF(angle)
//	NORMALISE(vNormStart)
//	NORMALISE(vNormEnd)
	VECTOR vCamTarget = (vEndPos-vStartPos)
	VECTOR vNorm = NORMALISE_VECTOR(vCamTarget)
	vReturnValue = ComputeEulersFromVector(vNorm)
	//GET_ROTA
	RETURN vReturnValue
ENDFUNC

FUNC SEAT_DETAILS GET_SEAT_DATA(INT iIndex)
	//seated
	//-0--1----------6--7--
	//----2--3--4--5-------
	//-8-9-----------14-15
	//----10-11-12-13------
	//16-17----------21-22-
	//----18-19-20-------
	FLOAT fCamZOffset = 1.2
	SEAT_DETAILS seatData
	//standing
	SWITCH cinemaInfo.eLocation
		CASE ALOC_cinema_downtown
			SWITCH iIndex
				CASE 0
					seatData.vCamPos = << 372.289825, -716.910095, (10.497251+fCamZOffset) >>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					//<< 0.0, 0.0000, 178 >>
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 372.289825, -716.910095, 10.497251>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 1
					seatData.vCamPos = << 374.049194, -716.904480, (10.391301+fCamZOffset)>>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 374.049194, -716.904480, 10.391301>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 2
					seatData.vCamPos = << 375.663330, -716.821472, (10.462126+fCamZOffset)>>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 375.663330, -716.821472, 10.462126>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 3
					seatData.vCamPos = << 376.861206, -716.873291, (10.485737+fCamZOffset)>>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 376.861206, -716.873291, 10.485737>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 4
					seatData.vCamPos = << 377.462616, -716.889404, (10.510453+fCamZOffset)>>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 377.462616, -716.889404, 10.510453>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 5
					seatData.vCamPos = << 379.163452, -717.018311, (10.490685+fCamZOffset)>>
					seatData.vCamRot =<<9.3121, 0.0000, 174.7176>>
					seatData.fCamFOV = 35
					seatData.vSeatPos = << 379.163452, -717.018311, 10.490685>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 6
					seatData.vCamPos = << 371.872986, -720.420959, (10.490905+fCamZOffset)>>
					seatData.vCamRot = <<10.9324, 0.0000, -161.6771>>
					seatData.fCamFOV = 38
					seatData.vSeatPos = << 371.872986, -720.420959, 10.391301>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 7
					seatData.vCamPos = << 370.766327, -720.524048, (10.451876+fCamZOffset)>>
					seatData.vCamRot = <<10.6360, -0.0000, -158.0298>>
					seatData.fCamFOV = 38
					seatData.vSeatPos = << 370.766327, -720.524048,10.451876>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 8
					seatData.vCamPos = << 370.171295, -720.591675, (10.477077+fCamZOffset)>>
					seatData.vCamRot = <<10.9668, -0.0000, -154.9859>>
					seatData.fCamFOV = 38
					seatData.vSeatPos = << 370.171295, -720.591675, 10.477077>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 9
					seatData.vCamPos = << 369.021179, -720.644104, (10.460794+fCamZOffset)>>
					seatData.vCamRot = <<11.4466, 0.0000, -151.1129>>
					seatData.fCamFOV = 34
					seatData.vSeatPos = << 369.021179, -720.644104, 10.460794>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 10
					seatData.vCamPos = << 372.445374, -720.369690, (10.465681+fCamZOffset)>>
					seatData.vCamRot = <<10.8786, -0.0000, -163.6012>>
					seatData.fCamFOV = 45
					seatData.vSeatPos = << 372.445374, -720.369690, 10.465681>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 11
					seatData.vCamPos = << 380.509827, -721.362244, (10.442063+fCamZOffset)>>
					seatData.vCamRot = <<12.7966, -0.0000, 167.9809>>
					seatData.fCamFOV = 43
					seatData.vSeatPos = << 380.509827, -721.362244, 10.442063>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 12
					seatData.vCamPos = << 381.095581, -721.386841, (10.408819+fCamZOffset)>>
					seatData.vCamRot = <<12.7995, 0.0000, 166.8188>>
					seatData.fCamFOV = 43
					seatData.vSeatPos = << 381.095581, -721.386841, 10.408819>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 13
					seatData.vCamPos = << 377.599976, -720.191345, (10.351383+fCamZOffset)>>
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 45
					seatData.vSeatPos = << 377.599976, -720.191345, 10.351383>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 14
					seatData.vCamPos = << 378.807190, -720.233765, (10.468294+fCamZOffset)>>
					seatData.vCamRot = <<11.9675, -0.0000, 174.5444>>
					seatData.fCamFOV = 45
					seatData.vSeatPos = << 378.807190, -720.233765, 10.468294>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 15
					seatData.vCamPos = << 378.197540, -720.221252, (10.485231+fCamZOffset)>>
					seatData.vCamRot = <<11.9395, -0.0000, 177.0469>>
					seatData.fCamFOV = 43.5883
					seatData.vSeatPos = << 378.197540, -720.221252, 10.485231>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 16
					seatData.vCamPos = << 379.384033, -720.262329, (10.468241+fCamZOffset)>>
					seatData.vCamRot = <<11.9212, -0.0000, 173.1490>>
					seatData.fCamFOV = 44
					seatData.vSeatPos = << 379.384033, -720.262329, 10.468241>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 17
					seatData.vCamPos = << 381.093475, -720.334839, (10.452884+fCamZOffset)>>
					seatData.vCamRot = <<12.0421, 0.0000, 166.5301>>
					seatData.fCamFOV = 41
					seatData.vSeatPos = << 381.093475, -720.334839, 10.452884>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 18
					seatData.vCamPos = << 380.537079, -720.324341, (10.467115+fCamZOffset)>>
					seatData.vCamRot = <<11.3040, 0.0000, 169.0678>>
					seatData.fCamFOV = 42
					seatData.vSeatPos = << 380.537079, -720.324341, 10.467115>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 19
					seatData.vCamPos = << 380.536438, -722.717529, (10.397058+fCamZOffset)>>
					seatData.vCamRot = <<14.0036, 0.0000, 167.8718>>
					seatData.fCamFOV = 50
					seatData.vSeatPos = << 380.536438, -722.717529, 10.391301>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 20
					seatData.vCamPos = << 381.111816, -723.741882, (10.298868+fCamZOffset)>>
					seatData.vCamRot = <<15.2916, 0.0000, 165.1738>>
					seatData.fCamFOV = 52
					seatData.vSeatPos = << 381.111816, -723.741882, 10.298868>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 21
					seatData.vCamPos = << 380.522949, -723.719421, (10.315695+fCamZOffset)>>
					seatData.vCamRot = <<16.2153, 0.0000, 167.4701>>
					seatData.fCamFOV = 52
					seatData.vSeatPos = << 380.522949, -723.719421, 10.315695>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 22
					seatData.vCamPos = << 381.112488, -722.664795, (10.353852+fCamZOffset)>>
					seatData.vCamRot = <<13.1758, 0.0000, 165.9021>>
					seatData.fCamFOV = 50
					seatData.vSeatPos = << 381.112488, -722.664795, 10.353852>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 23
					seatData.vCamPos = << 371.897522, -722.711243, (10.380223+fCamZOffset)>>
					seatData.vCamRot = <<14.3705, 0.0000, -158.9876>>
					seatData.fCamFOV = 45
					seatData.vSeatPos = << 371.897522, -722.711243, 10.380223>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 24
					seatData.vCamPos = << 372.486115, -722.787964, (10.335785+fCamZOffset)>>
					seatData.vCamRot = <<13.9337, 0.0000, -161.5393>>
					seatData.fCamFOV = 48.5
					seatData.vSeatPos = << 372.486115, -722.787964, 10.335785>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 25
					seatData.vCamPos = << 371.898712, -725.053040, (10.191216+fCamZOffset)>>
					seatData.vCamRot = <<20.1102, 0.0000, -158.2016>>
					seatData.fCamFOV = 56
					seatData.vSeatPos = << 371.898712, -725.053040, 10.191216>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 26
					seatData.vCamPos = << 372.451904, -725.037720, (10.230280+fCamZOffset)>>
					seatData.vCamRot = <<18.3191, -0.0000, -159.9798>>
					seatData.fCamFOV = 54.7944
					seatData.vSeatPos = << 372.451904, -725.037720, 10.230280>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 27
					seatData.vCamPos = << 377.632813, -724.854858, (10.211521+fCamZOffset)>>
					seatData.vCamRot = <<17.9222, 0.0000, 178.7022>>
					seatData.fCamFOV = 64.3802
					seatData.vSeatPos = << 377.632813, -724.854858, 10.211521>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 28
					seatData.vCamPos = << 378.196014, -724.884644, (10.225228+fCamZOffset)>>
					seatData.vCamRot = <<18.6424, 0.0000, 177.6024>>
					seatData.fCamFOV = 63.3742
					seatData.vSeatPos = << 378.196014, -724.884644, 10.225228>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 29
					seatData.vCamPos = << 380.557709, -724.933044, (10.226647+fCamZOffset)>>
					seatData.vCamRot = <<18.0177, -0.0000, 166.7067>>
					seatData.fCamFOV = 61.1889
					seatData.vSeatPos = << 380.557709, -724.933044,10.226647>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 30
					seatData.vCamPos = << 375.320007, -724.869812, (10.236370+fCamZOffset)>>
					seatData.vCamRot = <<16.6198, 0.0000, -171.5697>>
					seatData.fCamFOV = 59.1973
					seatData.vSeatPos = << 375.320007, -724.869812, 10.236370>>
					seatData.fSeatHeading = 178
				BREAK
				CASE 31
					seatData.vCamPos = << 374.783997, -724.974792, (10.235142+fCamZOffset)>>
					seatData.vCamRot = <<17.7571, -0.0000, -169.4913>>
					seatData.fCamFOV = 59.8659
					seatData.vSeatPos = << 374.783997, -724.974792, 10.235142>>
					seatData.fSeatHeading = 178
				BREAK
			ENDSWITCH
		BREAK
		CASE ALOC_cinema_vinewood
			SWITCH iIndex
//				CASE 0
//					seatData.vCamPos = <<312.985138, 255.309464, (82.084526+fCamZOffset)>> 
//					seatData.vCamRot = <<18.7145, 0.0000, -146.4130>>
//					seatData.fCamFOV = 60.0480
//					seatData.vSeatPos = <<312.985138, 255.309464, 82.084526>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 1
//					seatData.vCamPos = <<311.804749, 255.397034, (82.349045+fCamZOffset)>> 
//					seatData.vCamRot = <<18.1450, 0.0000, -141.3365>>
//					seatData.fCamFOV = 58.0427
//					seatData.vSeatPos = <<311.804749, 255.397034, 82.349045>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
				CASE 0//2
					seatData.vCamPos = <<314.542480, 264.742767, (82.424187+fCamZOffset)>> 
					seatData.vCamRot = <<9.2307, -0.0000, -163.5652>>
					seatData.fCamFOV = 31.2465
					seatData.vSeatPos = <<314.542480, 264.742767, 82.424187>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 1//3
					seatData.vCamPos = <<315.146881, 264.625031, (82.544693+fCamZOffset)>> 
					seatData.vCamRot = <<9.0841, -0.0000, -165.6775>>
					seatData.fCamFOV = 31.6778
					seatData.vSeatPos = <<315.146881, 264.625031, 82.544693>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 2//4
					seatData.vCamPos = <<316.855957, 264.733124, (82.303383+fCamZOffset)>> 
					seatData.vCamRot = <<9.3184, 0.0000, -170.1787>>
					seatData.fCamFOV = 32.1031
					seatData.vSeatPos = <<316.855957, 264.733124, 82.303383>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 3//5
					seatData.vCamPos = <<318.556671, 264.633881, (82.417770+fCamZOffset)>> 
					seatData.vCamRot = <<9.1300, 0.0000, -175.3555>>
					seatData.fCamFOV = 32.7728
					seatData.vSeatPos = <<318.556671, 264.633881, 82.417770>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 4//6
					seatData.vCamPos = <<319.725616, 264.764954, (82.253479+fCamZOffset)>> 
					seatData.vCamRot = <<9.0963, 0.0000, -178.9855>>
					seatData.fCamFOV = 33.9548
					seatData.vSeatPos = <<319.725616, 264.764954, 82.253479>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 5//7
					seatData.vCamPos = <<314.682983, 260.141205, (82.229774+fCamZOffset)>> 
					seatData.vCamRot = <<12.6364, -0.0000, -160.6232>>
					seatData.fCamFOV = 40.6578
					seatData.vSeatPos = <<314.682983, 260.141205, 82.229774>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 6//8
					seatData.vCamPos = <<315.279602, 260.129211, (82.348740+fCamZOffset)>> 
					seatData.vCamRot = <<12.5850, -0.0000, -162.5030>>
					seatData.fCamFOV = 40.2106
					seatData.vSeatPos = <<315.279602, 260.129211, 82.348740>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 7//9
					seatData.vCamPos = <<314.667084, 261.256439, (82.281876+fCamZOffset)>> 
					seatData.vCamRot = <<11.2958, -0.0000, -161.0049>>
					seatData.fCamFOV = 36.7070
					seatData.vSeatPos = <<314.667084, 261.256439, 82.281876>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 8//10
					seatData.vCamPos = <<315.279266, 261.297791, (82.255669+fCamZOffset)>> 
					seatData.vCamRot = <<12.2435, 0.0000, -163.0314>>
					seatData.fCamFOV = 37.5906
					seatData.vSeatPos = <<315.279266, 261.297791, 82.255669>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 9//11
					seatData.vCamPos = <<312.984894, 261.118103, (82.399330+fCamZOffset)>> 
					seatData.vCamRot = <<11.2778, 0.0000, -155.6107>>
					seatData.fCamFOV = 36.9313
					seatData.vSeatPos = <<312.984894, 261.118103, 82.399330>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 10//12
					seatData.vCamPos = <<313.534729, 261.191803, (82.395164+fCamZOffset)>> 
					seatData.vCamRot = <<11.2399, 0.0000, -157.2202>>
					seatData.fCamFOV = 37.5653
					seatData.vSeatPos = <<313.534729, 261.191803, 82.395164>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 11//13
					seatData.vCamPos = <<314.721283, 258.851654, (82.325218+fCamZOffset)>> 
					seatData.vCamRot = <<14.9193, -0.0000, -158.4475>>
					seatData.fCamFOV = 47.0107
					seatData.vSeatPos = <<314.721283, 258.851654, 82.325218>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 12//14
					seatData.vCamPos = <<315.289429, 258.867004, (82.299316+fCamZOffset)>> 
					seatData.vCamRot = <<14.4042, -0.0000, -161.1789>>
					seatData.fCamFOV = 46.0020
					seatData.vSeatPos = <<315.289429, 258.867004, 82.299316>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 13//15
					seatData.vCamPos = <<315.276581, 257.698944, (82.453506+fCamZOffset)>> 
					seatData.vCamRot = <<15.5013, -0.0000, -159.6257>>
					seatData.fCamFOV = 49.9605
					seatData.vSeatPos = <<315.276581, 257.698944, 82.453506>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 14//16
					seatData.vCamPos = <<314.709839, 257.789307, (82.200951+fCamZOffset)>> 
					seatData.vCamRot = <<16.4339, 0.0000, -157.6264>>
					seatData.fCamFOV = 49.0059
					seatData.vSeatPos = <<314.709839, 257.789307, 82.200951>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 15//17
					seatData.vCamPos = <<313.594391, 260.001740, (82.364540+fCamZOffset)>> 
					seatData.vCamRot = <<12.4146, -0.0000, -156.2165>>
					seatData.fCamFOV = 40.2261
					seatData.vSeatPos = <<313.594391, 260.001740, 82.364540>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 16//18
					seatData.vCamPos = <<313.007843, 259.960785, (82.335617+fCamZOffset)>> 
					seatData.vCamRot = <<14.0373, -0.0000, -151.6856>>
					seatData.fCamFOV = 38.9120
					seatData.vSeatPos = <<313.007843, 259.960785, 82.335617>> 
					seatData.fSeatHeading = -179.5
				BREAK
//				CASE 19
//					seatData.vCamPos = <<311.843964, 259.858276, (82.345467+fCamZOffset)>> 
//					seatData.vCamRot = <<12.6687, -0.0000, -150.7585>>
//					seatData.fCamFOV = 39.0677
//					seatData.vSeatPos = <<311.843964, 259.858276, 82.345467>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
				CASE 17//20
					seatData.vCamPos = <<312.999451, 258.750214, (82.284874+fCamZOffset)>> 
					seatData.vCamRot = <<15.1249, -0.0000, -152.2532>>
					seatData.fCamFOV = 45.0003
					seatData.vSeatPos = <<312.999451, 258.750214, 82.284874>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 18//21
					seatData.vCamPos = <<313.576904, 258.844666, (82.276123+fCamZOffset)>> 
					seatData.vCamRot = <<14.8591, -0.0000, -153.6585>>
					seatData.fCamFOV = 45.2251
					seatData.vSeatPos = <<313.576904, 258.844666, 82.276123>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 19//22
					seatData.vCamPos = <<313.586609, 257.728058, (82.119804+fCamZOffset)>> 
					seatData.vCamRot = <<17.1165, -0.0000, -153.0840>>
					seatData.fCamFOV = 48.0088
					seatData.vSeatPos = <<313.586609, 257.728058, 82.119804>> 
					seatData.fSeatHeading = -179.5
				BREAK
//				CASE 23
//					seatData.vCamPos = <<312.999054, 257.688843, (82.234436+fCamZOffset)>> 
//					seatData.vCamRot = <<17.2230, -0.0000, -150.6844>>
//					seatData.fCamFOV = 47.1060
//					seatData.vSeatPos = <<312.999054, 257.688843, 82.234436>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 24
//					seatData.vCamPos = <<311.840271, 257.594421, (82.069832+fCamZOffset)>> 
//					seatData.vCamRot = <<18.1083, 0.0000, -145.5764>>
//					seatData.fCamFOV = 45.1777
//					seatData.vSeatPos = <<311.840271, 257.594421, 82.069832>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 25
//					seatData.vCamPos = <<312.976227, 256.451324, (82.098366+fCamZOffset)>> 
//					seatData.vCamRot = <<19.9102, 0.0000, -148.5161>>
//					seatData.fCamFOV = 53.5330
//					seatData.vSeatPos = <<312.976227, 256.451324, 82.098366>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 26
//					seatData.vCamPos = <<311.806610, 256.466095, (82.396004+fCamZOffset)>> 
//					seatData.vCamRot = <<17.3154, -0.0000, -145.0639>>
//					seatData.fCamFOV = 50.0194
//					seatData.vSeatPos = <<311.806610, 256.466095, 82.396004>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
				CASE 20//27
					seatData.vCamPos = <<320.454102, 260.357483, (82.412720+fCamZOffset)>> 
					seatData.vCamRot = <<10.7720, 0.0000, 179.1309>>
					seatData.fCamFOV = 44.5733
					seatData.vSeatPos = <<320.454102, 260.357483, 82.412720>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 21//28
					seatData.vCamPos = <<321.036804, 256.664917, (82.191971+fCamZOffset)>> 
					seatData.vCamRot = <<14.8740, -0.0000, 176.0925>>
					seatData.fCamFOV = 60.6120
					seatData.vSeatPos = <<321.036804, 256.664917, 82.191971>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 22//29
					seatData.vCamPos = <<320.257477, 264.724640, (82.300560+fCamZOffset)>> 
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = <<320.257477, 264.724640, 82.300560>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 23//30
					seatData.vCamPos = <<322.010559, 264.711121, (82.346603+fCamZOffset)>> 
					seatData.vCamRot = GET_CAMERA_ROTATION_FROM_TWO_POINTS(seatData.vCamPos,cinemaInfo.vScreenCentre)
					seatData.fCamFOV = 35
					seatData.vSeatPos = <<322.010559, 264.711121, 82.346603>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 24//31
					seatData.vCamPos = <<321.611786, 261.441559, (82.394264+fCamZOffset)>> 
					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
					seatData.fCamFOV = 40.7381
					seatData.vSeatPos = <<321.611786, 261.441559, 82.394264>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 25//32
					seatData.vCamPos = <<322.199707, 261.377655, (82.366188+fCamZOffset)>> 
					seatData.vCamRot = <<11.6148, 0.0356, 173.0097>>
					seatData.fCamFOV = 40.7381
					seatData.vSeatPos = <<322.199707, 261.377655, 82.366188>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 26//33
					seatData.vCamPos = <<323.355286, 261.353485, (82.239189+fCamZOffset)>> 
					seatData.vCamRot = <<12.1585, 0.0507, 168.9420>>
					seatData.fCamFOV = 40.7381
					seatData.vSeatPos = <<323.355286, 261.353485, 82.239189>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 27//34
					seatData.vCamPos = <<323.925476, 261.326324, (82.386810+fCamZOffset)>> 
					seatData.vCamRot = <<10.7088, -0.0459, 166.7500>>
					seatData.fCamFOV = 40.7381
					seatData.vSeatPos = <<323.925476, 261.326324, 82.386810>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 28//35
					seatData.vCamPos = <<323.336975, 259.009399, (82.309479+fCamZOffset)>> 
					seatData.vCamRot = <<16.2681, -0.0245, 167.8727>>
					seatData.fCamFOV = 47.4776
					seatData.vSeatPos = <<323.336975, 259.009399, 82.309479>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 29//36
					seatData.vCamPos = <<323.888672, 258.997009, (82.233665+fCamZOffset)>> 
					seatData.vCamRot = <<15.3019, 0.0119, 165.6251>>
					seatData.fCamFOV = 46.0227
					seatData.vSeatPos = <<323.888672, 258.997009, 82.233665>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 30//37
					seatData.vCamPos = <<323.891327, 257.895691, (82.222717+fCamZOffset)>> 
					seatData.vCamRot = <<17.0406, -0.0508, 164.8351>>
					seatData.fCamFOV = 50.3730
					seatData.vSeatPos = <<323.891327, 257.895691, 82.222717>> 
					seatData.fSeatHeading = -179.5
				BREAK
				CASE 31//38
					seatData.vCamPos = <<323.338959, 257.897491, (82.090118+fCamZOffset)>> 
					seatData.vCamRot = <<17.4491, -0.0635, 167.2266>>
					seatData.fCamFOV = 51.4625
					seatData.vSeatPos = <<323.338959, 257.897491, 82.090118>> 
					seatData.fSeatHeading = -179.5
				BREAK
//				CASE 39
//					seatData.vCamPos = <<326.486145, 258.822937, (82.205063+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<326.486145, 258.822937, 82.205063>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 40
//					seatData.vCamPos = <<327.041504, 258.780060, (82.276276+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<327.041504, 258.780060, 82.276276>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 41
//					seatData.vCamPos = <<326.451355, 260.101746, (82.357185+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<326.451355, 260.101746, 82.357185>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 42
//					seatData.vCamPos = <<327.058990, 260.092041, (82.290123+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<327.058990, 260.092041, 82.290123>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 43
//					seatData.vCamPos = <<313.588745, 255.426132, (82.054375+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<313.588745, 255.426132,82.054375>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 44
//					seatData.vCamPos = <<317.631287, 255.600510, (82.049896+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<317.631287, 255.600510, 82.049896>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 45
//					seatData.vCamPos = <<318.133453, 255.629242, (82.069077+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<318.133453, 255.629242, 82.069077>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 46
//					seatData.vCamPos = <<320.484863, 255.577057, (82.076775+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<320.484863, 255.577057, 82.076775>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
//				CASE 47
//					seatData.vCamPos = <<321.008179, 255.567307, (82.133789+fCamZOffset)>> 
//					seatData.vCamRot = <<11.2188, -0.0000, 175.1725>>
//					seatData.fCamFOV = 40.7381
//					seatData.vSeatPos = <<321.008179, 255.567307, 82.133789>> 
//					seatData.fSeatHeading = -179.5
//				BREAK
			ENDSWITCH
		BREAK
		CASE ALOC_cinema_morningwood
			SWITCH iIndex
				CASE 0
					seatData.vCamPos = << -1429.050537, -254.810852, (15.920204+fCamZOffset) >>
					seatData.vCamRot = <<19.2615, -0.0000, 163.6450>>
					seatData.fCamFOV = 68.1721
					seatData.vSeatPos = << -1429.050537, -254.810852, 15.920204 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 1
					seatData.vCamPos = << -1428.560303, -254.773499, (15.859696+fCamZOffset) >>
					seatData.vCamRot = <<21.2606, 0.0000, -173.0890>>
					seatData.fCamFOV = 90.6714
					seatData.vSeatPos = << -1428.560303, -254.773499, 15.859696 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 2
					seatData.vCamPos = << -1423.340576, -253.670715, (15.904275+fCamZOffset) >>
					seatData.vCamRot = <<21.6562, 0.0000, 166.4629>>
					seatData.fCamFOV = 73.8127
					seatData.vSeatPos = << -1423.340576, -253.670715, 15.904275 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 3
					seatData.vCamPos = << -1422.787109, -252.696899, (15.8053+fCamZOffset) >>
					seatData.vCamRot = <<21.2120, -0.0000, 163.4131>>
					seatData.fCamFOV = 63.8607
					seatData.vSeatPos = << -1422.787109, -252.696899, 15.8053 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 4
					seatData.vCamPos = << -1431.402710, -252.703918, (15.897184+fCamZOffset) >>
					seatData.vCamRot = <<21.9209, 0.0000, -158.9840>>
					seatData.fCamFOV = 60.2128
					seatData.vSeatPos = << -1431.402710, -252.703918, 15.897184 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 5
					seatData.vCamPos = << -1429.104370, -247.892242, (16.175653+fCamZOffset) >>
					seatData.vCamRot = <<12.9943, 0.0000, -171.0946>>
					seatData.fCamFOV = 42.5781
					seatData.vSeatPos = << -1429.104370, -247.892242, 16.175653 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 6
					seatData.vCamPos = << -1428.560791, -247.822433, (16.171135+fCamZOffset) >>
					seatData.vCamRot = <<12.0150, 0.0000, -172.6889>>
					seatData.fCamFOV = 44.0369
					seatData.vSeatPos = << -1428.560791, -247.822433, 16.171135 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 7
					seatData.vCamPos = << -1431.956055, -250.338867, (16.063385+fCamZOffset) >>
					seatData.vCamRot = <<16.2370, -0.0000, -158.8608>>
					seatData.fCamFOV = 47.4492
					seatData.vSeatPos = <<-1431.956055, -250.338867, 16.063385 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 8
					seatData.vCamPos = << -1424.482544, -246.819565, (16.249380+fCamZOffset) >>
					seatData.vCamRot = <<11.7682, 0.0000, 173.2415>>
					seatData.fCamFOV = 40.1148
					seatData.vSeatPos = << -1424.482544, -246.819565, 16.249380 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 9
					seatData.vCamPos = << -1425.051880, -246.770447, (16.239958+fCamZOffset) >>
					seatData.vCamRot = <<9.9640, 0.0000, 174.7147>>
					seatData.fCamFOV = 40.4319
					seatData.vSeatPos = << -1425.051880, -246.770447, 16.239958 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 10
					seatData.vCamPos = << -1425.635132, -246.721985, (16.240086+fCamZOffset) >>
					seatData.vCamRot = <<11.0050, 0.0000, 176.6019>>
					seatData.fCamFOV = 39.9151
					seatData.vSeatPos = << -1425.635132, -246.721985, 16.240086 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 11
					seatData.vCamPos = << -1426.201538, -246.707962, (16.229538+fCamZOffset) >>
					seatData.vCamRot = <<10.6480, 0.0000, 178.9974>>
					seatData.fCamFOV = 40.3505
					seatData.vSeatPos = << -1426.201538, -246.707962, 16.229538 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 12
					seatData.vCamPos = << -1429.112061, -250.199844, (16.057077+fCamZOffset) >>
					seatData.vCamRot = <<15.8229, -0.0000, -169.8990>>
					seatData.fCamFOV = 51.2850
					seatData.vSeatPos = << -1429.112061, -250.199844, 16.057077>>
					seatData.fSeatHeading = 177
				BREAK
				CASE 13
					seatData.vCamPos = << -1422.752075, -249.166077, (16.119595+fCamZOffset) >>
					seatData.vCamRot = <<14.9408, 0.0000, 164.8112>>
					seatData.fCamFOV = 46.3151
					seatData.vSeatPos = << -1422.752075, -249.166077, 16.119595 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 14
					seatData.vCamPos = << -1423.342773, -249.143524, (16.145742+fCamZOffset) >>
					seatData.vCamRot = <<14.8875, -0.0000, 168.0431>>
					seatData.fCamFOV = 48.2545
					seatData.vSeatPos = << -1423.342773, -249.143524, 16.145742 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 15
					seatData.vCamPos = << -1432.007324, -246.925064, (16.231108+fCamZOffset) >>
					seatData.vCamRot = <<10.6825, 0.0000, -160.9277>>
					seatData.fCamFOV = 36.7552
					seatData.vSeatPos = << -1432.007324, -246.925064, 16.231108 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 16
					seatData.vCamPos = << -1432.119019, -243.447556, (16.257864+fCamZOffset) >>
					seatData.vCamRot = <<9.7801, -0.0000, -164.4297>>
					seatData.fCamFOV = 31.1913
					seatData.vSeatPos = << -1432.119019, -243.447556, 16.257864 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 17
					seatData.vCamPos = << -1431.545776, -243.407272, (16.264511+fCamZOffset) >>
					seatData.vCamRot = <<9.6411, 0.0000, -165.3944>>
					seatData.fCamFOV = 30.6452
					seatData.vSeatPos = << -1431.545776, -243.407272, 16.264511 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 18
					seatData.vCamPos = << -1429.832764, -243.449982, (16.259052+fCamZOffset) >>
					seatData.vCamRot = <<10.3535, -0.0000, -170.3195>>
					seatData.fCamFOV = 32.4638
					seatData.vSeatPos = << -1429.832764, -243.449982, 16.259052 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 19
					seatData.vCamPos = << -1426.996948, -243.405518, (16.260147+fCamZOffset) >>
					seatData.vCamRot = <<10.0163, 0.0000, -178.3346>>
					seatData.fCamFOV = 35
					seatData.vSeatPos = << -1426.996948, -243.405518, 16.260147 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 20
					seatData.vCamPos = << -1426.429199, -243.394348, (16.265541+fCamZOffset) >>
					seatData.vCamRot = <<9.8815, -0.0000, 179.5795>>
					seatData.fCamFOV = 35
					seatData.vSeatPos = << -1426.429199, -243.394348, 16.265541 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 21
					seatData.vCamPos = << -1424.644043, -243.442017, (16.255047+fCamZOffset) >>
					seatData.vCamRot = <<9.8396, -0.0000, 173.0362>>
					seatData.fCamFOV = 35
					seatData.vSeatPos = << -1424.644043, -243.442017, 16.255047 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 22
					seatData.vCamPos = << -1433.651978, -249.374527, (16.136774+fCamZOffset) >>
					seatData.vCamRot = <<13.7212, -0.0000, -152.3666>>
					seatData.fCamFOV = 42.6996
					seatData.vSeatPos = << -1433.651978,-249.374527, 16.136774 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 23
					seatData.vCamPos = << -1433.130737, -249.358917, (16.138220+fCamZOffset) >>
					seatData.vCamRot = <<14.5802, -0.0000, -154.0087>>
					seatData.fCamFOV = 44.3415
					seatData.vSeatPos = << -1433.130737, -249.358917, 16.138220 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 24
					seatData.vCamPos = << -1420.214722, -250.473602, (16.033491+fCamZOffset) >>
					seatData.vCamRot = <<16.2658, -0.0000, 154.2034>>
					seatData.fCamFOV = 49.9236
					seatData.vSeatPos = << -1420.214722, -250.473602, 16.033491 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 25
					seatData.vCamPos = << -1419.639404, -250.453400, (16.047110+fCamZOffset) >>
					seatData.vCamRot = <<18.0073, 0.0000, 150.2106>>
					seatData.fCamFOV = 46.1796
					seatData.vSeatPos = << -1419.639404, -250.453400, 16.047110 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 26
					seatData.vCamPos = << -1432.014404, -251.537949, (15.997274+fCamZOffset) >>
					seatData.vCamRot = <<20.0852, 0.0000, -156.1049>>
					seatData.fCamFOV = 56.7451
					seatData.vSeatPos = << -1432.014404, -251.537949, 15.997274 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 27
					seatData.vCamPos = << -1431.403809, -251.528305, (15.980005+fCamZOffset) >>
					seatData.vCamRot = <<19.6972, 0.0000, -159.6045>>
					seatData.fCamFOV = 52.9959
					seatData.vSeatPos = << -1431.403809, -251.528305, 15.980005 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 28
					seatData.vCamPos = << -1419.901245, -247.003067, (16.259096+fCamZOffset) >>
					seatData.vCamRot = <<11.3249, 0.0000, 155.5347>>
					seatData.fCamFOV = 36.7464
					seatData.vSeatPos = << -1419.901245, -247.003067, 16.259096 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 29
					seatData.vCamPos = << -1420.455322, -246.950592, (16.237331+fCamZOffset) >>
					seatData.vCamRot = <<11.6390, 0.0000, 158.4869>>
					seatData.fCamFOV = 38.5080
					seatData.vSeatPos = << -1420.455322, -246.950592, 16.237331 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 30
					seatData.vCamPos = << -1425.614624, -252.528275, (15.951679+fCamZOffset) >>
					seatData.vCamRot = <<18.7509, 0.0000, 176.4076>>
					seatData.fCamFOV = 70.2974
					seatData.vSeatPos = << -1425.614624, -252.528275, 15.951679 >>
					seatData.fSeatHeading = 177
				BREAK
				CASE 31
					seatData.vCamPos = << -1422.745605, -246.858246, (16.213160+fCamZOffset) >>
					seatData.vCamRot = <<12.3960, 0.0000, 166.8724>>
					seatData.fCamFOV = 39.0291
					seatData.vSeatPos = << -1422.745605, -246.858246, 16.213160 >>
					seatData.fSeatHeading = 177
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN seatData
ENDFUNC

PROC ASSIGN_SEATS()
	INT i, j
	REPEAT NUM_NETWORK_PLAYERS i
		IF playerBD[i].bInCinema
			IF NOT IS_BIT_SET(serverBD.iAssignedSeatBS,i)
				REPEAT NUM_NETWORK_PLAYERS j
					IF serverBD.iSeats[j] = -1
						SET_BIT(serverBD.iAssignedSeatBS,i)
						serverBD.iSeats[j] = i
						NET_PRINT("Assigning a seat in the cinema for a player") NET_NL()
						EXIT
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.iAssignedSeatBS,i)
				REPEAT NUM_NETWORK_PLAYERS j
					IF serverBD.iSeats[j] = i
						CLEAR_BIT(serverBD.iAssignedSeatBS,i)
						serverBD.iSeats[j] = -1
						EXIT
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SPAWN_SCENARIO_PEDS()
	IF playerBD[iSlowServerPlayerLoop].bWarpedInside = TRUE
		bAtLeast1PlayerInCinema = TRUE	
		IF NOT serverBD.bSpawnedPeds 
			IF NOT ARE_VECTORS_EQUAL(cinemaInfo.vScenarioActivationPoint,<<0,0,0>>)
				SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(cinemaInfo.vScenarioActivationPoint,15,5)
				PRINTLN("ACT_CINEMA: Server Setting scenario peds for cinema to spawn") 
				serverBD.bSpawnedPeds = TRUE
			ELSE
				PRINTLN("ACT_CINEMA: cinemaInfo.vScenarioActivationPoint = <<0,0,0>>????") 
				serverBD.bSpawnedPeds = TRUE
			ENDIF
		ENDIF
	ENDIF
	iSlowServerPlayerLoop++
	IF iSlowServerPlayerLoop >= NUM_NETWORK_PLAYERS
		iSlowServerPlayerLoop = 0 
		IF serverBD.bSpawnedPeds = TRUE
			IF NOT bAtLeast1PlayerInCinema
				serverBD.bSpawnedPeds = FALSE	
				PRINTLN("ACT_CINEMA: Server clearing peds in cinema spawn flag") 
			ENDIF
		ENDIF
		bAtLeast1PlayerInCinema = FALSE
	ENDIF
ENDPROC

FUNC BOOL GET_CLIENT_SEAT()
	INT i
	IF IS_BIT_SET(serverBD.iAssignedSeatBS,PARTICIPANT_ID_TO_INT())
		REPEAT NUM_NETWORK_PLAYERS i
			IF serverBD.iSeats[i] = PARTICIPANT_ID_TO_INT()
				thisPlayersSeat = GET_SEAT_DATA(seatOrdering[i])
				
				#IF IS_DEBUG_BUILD
					iCurrentSeat= seatOrdering[i]
					iDebugSeat = seatOrdering[i]
					PRINTLN("ACT_CINEMA: Setting player to be in seat # ", iCurrentSeat)
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_OK(PLAYER_INDEX thePlayer)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RETURN IS_NET_PLAYER_OK(thePlayer)
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LOADED_AND_CREATED_CINEMA_SCENE()
	//BOOL bReturn = TRUE
//	IF NOT g_bInMultiplayer AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
//		blipDoor = CREATE_BLIP_FOR_COORD(vInput)
//		SET_BLIP_SPRITE(blipDoor, RADAR_TRACE_CINEMA)
//		SET_BLIP_NAME_FROM_TEXT_FILE(blipDoor,"MP_ACT_CIN")
//	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ACTIVITY_TIME_ACCEPTABLE()
	
	//IF bIgnoreTimeConstraints
	//	CPRINTLN( DEBUG_AMBIENT, "CINEMA - DEBUG! IGNORNING TIME CONSTRAINTS!" )
	//	RETURN TRUE
	
	//ELSE
	
		IF GET_CINEMA_SHOWING() != CINEMA_SHOWING_NONE
		//CPRINTLN( DEBUG_AMBIENT, "CINEMA - TIME IS ACCEPTABLE" )
			RETURN TRUE
		ENDIF
		
	//ENDIF
	
	//CPRINTLN( DEBUG_AMBIENT, "CINEMA - TIME IS NOT ACCEPTABLE" )
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CINEMA_UNLOCKED()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//IF g_FMMC_STRUCT.g_InvitedToAmbientActivity
		//	RETURN TRUE
		//ENDIF
		RETURN IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CINEMA)
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_NET_FREE_FOR_CINEMA()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_INVALID
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ACTIVITY_COST_ACCEPTABLE()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) >= g_sMptunables.icinema_expenditure_modifier
		OR NETWORK_CAN_SPEND_MONEY(g_sMptunables.icinema_expenditure_modifier,FALSE,TRUE,FALSE,-1) //Use OR to catch cash values over SCRIPT_MAX_INT32.
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) >= ACTIVITY_COST*iParticipants
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT DETERMINE_MOVIE_TIME(#IF IS_DEBUG_BUILD BOOL bPrint = TRUE #ENDIF)
	//(about 11.40)
	INT iMovieLength = 700000
	FLOAT fTimeSinceTVStarted
	FLOAT fModTime
	INT iSeconds,iMinutes,iHours,iDays,iMonths,iYears
	TIMEOFDAY timeWhenShowStarted
	TIMEOFDAY timeNow
	INT iTotalSeconds
	timeNow = GET_CURRENT_TIMEOFDAY()
	SET_TIMEOFDAY_HOUR(timeWhenShowStarted, 12)
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(timeNow,timeWhenShowStarted,iSeconds,iMinutes,iHours,iDays,iMonths,iYears)
	#IF IS_DEBUG_BUILD 
	IF bPrint
		NET_PRINT_STRING_INT("Time since movie started in seconds: ", iSeconds) NET_NL()
		NET_PRINT_STRING_INT("Time since movie started in minutes: ", iMinutes) NET_NL()
		NET_PRINT_STRING_INT("Time since movie started in hours: ", iHours) NET_NL()
	ENDIF
	#ENDIF
	iTotalSeconds = iSeconds + 60*iMinutes + iHours*60*60
	fTimeSinceTVStarted = TO_FLOAT(iTotalSeconds*(GET_MILLISECONDS_PER_GAME_MINUTE()/60))
	//TO_FLOAT(GET_NET_TIMER_DIFFERENCE(netStoredTime,serverBD.MovieStartTimer))
	#IF IS_DEBUG_BUILD 
	IF bPrint
	NET_PRINT_STRING_FLOAT("fTimeSinceTVStarted = ", fTimeSinceTVStarted) NET_NL()
	ENDIF
	#ENDIF
	fModTime = fTimeSinceTVStarted%iMovieLength
	#IF IS_DEBUG_BUILD 
	IF bPrint
	NET_PRINT_STRING_FLOAT("fModTime = ",fModTime ) NET_NL()
	ENDIF
	#ENDIF
	RETURN (fModTime/iMovieLength)*100
ENDFUNC

FUNC BOOL IS_PLAYER_AT_CINEMA()
		
		IF IS_PED_ON_FOOT( PLAYER_PED_ID())
		
			SWITCH cinemaInfo.eLocation
				
				CASE ALOC_cinema_downtown
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<393.782684,-707.920105,27.783785>>, <<393.790588,-715.769348,31.285950>>, 2.250000)
						RETURN TRUE
					ENDIF
				BREAK
				CASE ALOC_cinema_vinewood
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<284.272491,199.157990,103.273933>>, <<283.350037,201.643570,106.123932>>, 1.250000)
					OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<314.478790,190.304138,102.891182>>, <<312.205353,188.985092,105.686943>>, 1.250000)
						RETURN TRUE
					ENDIF
				BREAK
				CASE ALOC_cinema_morningwood
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1419.397095,-215.492615,45.734562>>, <<-1427.096680,-215.040161,49.067776>>, 2.750000)
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
		ELSE
		
		// Can now automatically start the cinema activity if you're in a vehicle and on a friend activity.
		
			IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
			AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
				RETURN FALSE
			ENDIF

			IF NOT canStartActivity(cinemaInfo.eLocation)
				RETURN FALSE
			ENDIF
			
			IF NOT IS_ACTIVITY_TIME_ACCEPTABLE()
				RETURN FALSE
			ENDIF
			
			IF NOT IS_ACTIVITY_COST_ACCEPTABLE()		
				RETURN FALSE
			ENDIF
			
			IF g_bPlayerIsInTaxi
				PRINTLN(GET_THIS_SCRIPT_NAME(), " - IS_PLAYER_AT_CINEMA - PLAYER IS IN A TAXI")		
				RETURN FALSE
			ENDIF
			
			VEHICLE_INDEX vPlayerVehicle
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				vPlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				
				IF NOT DOES_ENTITY_EXIST(vPlayerVehicle)
					RETURN FALSE
				ENDIF
				
				IF VDIST( vInput, GET_PLAYER_COORDS(PLAYER_ID()) ) < 10.0
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vPlayerVehicle, 7.5)
						RETURN TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_SCTV_CINEMA_DATA(VECTOR &vCoords, VECTOR &vRot, FLOAT &fFOV)
	SWITCH cinemaInfo.eLocation
		CASE ALOC_cinema_downtown
			vCoords = <<368.8175, -702.3699, 16.7201>>
			vRot = <<-9.9537, 2.9472, -155.7807>>
			fFOV = 31.6024
		BREAK
		CASE ALOC_cinema_vinewood
			vCoords = <<310.2254, 278.9754, 90.1912>>
			vRot = <<-13.3590, 4.3553, -151.7323>>
			fFOV = 31.5767
		BREAK
		CASE ALOC_cinema_morningwood
			vCoords = <<-1436.2330, -229.5830, 22.9699>>
			vRot = <<-11.8237, 2.5506, -153.9601>>
			fFOV = 31.5763
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_ANYONE_IN_GAME_UNLOCKED_ACTION_MOVIE()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BIT_SET(GlobalplayerBD_FM[i].iCinemaUnlockBS,CINEMA_UNLOCK_SP_ACTION_MOVIE)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL START_THE_MOVIE()
	MODEL_NAMES cinemaScreen = V_ILEV_CIN_SCREEN

	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("cinscreen")
		REGISTER_NAMED_RENDERTARGET("cinscreen")
		LINK_NAMED_RENDERTARGET(cinemaScreen)
		IF NOT IS_NAMED_RENDERTARGET_LINKED(cinemaScreen)
			PRINTLN("RELEASE_NAMED_RENDERTARGET: releasing the cinema screen link failed")
			RELEASE_NAMED_RENDERTARGET("cinscreen")
			RETURN FALSE
		ENDIF
	ENDIF
	iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID("cinscreen")
	SET_TEXT_RENDER_ID(iRenderTarget)
	RESTORE_STANDARD_CHANNELS()
//	MovieId = SET_BINK_MOVIE(theMovie)
//	IF MovieId != NULL
//		
//		DRAW_BINK_MOVIE(MovieId, 0.5,0.5,1.0,1.0,0.0,255,255,255,255)
//		fMovieTime = DETERMINE_MOVIE_TIME()
//		SET_BINK_MOVIE_TIME(MovieId , fMovieTime)
//		PLAY_BINK_MOVIE(MovieId)
//		RESET_NET_TIMER(LoadMovieTimeOut)
//		RETURN TRUE
//	ENDIF
//	
//	IF HAS_NET_TIMER_EXPIRED(LoadMovieTimeOut,60000)
//		SCRIPT_ASSERT("WARNING- GAME UNABLE TO LOAD MOVIE. ENTERING ANYWAY! ")
//		RESET_NET_TIMER(LoadMovieTimeOut)
//		RETURN TRUE
//	ENDIF
	
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	
	IF eCurrentCinemaShowing = CINEMA_SHOWING_MORNING
		SET_TV_CHANNEL_PLAYLIST( TVCHANNELTYPE_CHANNEL_1, "PL_CINEMA_CARTOON", TRUE)
	ELIF eCurrentCinemaShowing = CINEMA_SHOWING_AFTERNOON
		SET_TV_CHANNEL_PLAYLIST( TVCHANNELTYPE_CHANNEL_1, "PL_CINEMA_ARTHOUSE", TRUE)
	ELIF eCurrentCinemaShowing = CINEMA_SHOWING_EVENING
		SET_TV_CHANNEL_PLAYLIST( TVCHANNELTYPE_CHANNEL_1, "PL_CINEMA_ACTION", TRUE)
	ELSE
		IF HAS_ANYONE_IN_GAME_UNLOCKED_ACTION_MOVIE()
			SET_TV_CHANNEL_PLAYLIST_AT_HOUR( TVCHANNELTYPE_CHANNEL_1, "PL_CINEMA_MULTIPLAYER", 10)
		ELSE
			SET_TV_CHANNEL_PLAYLIST_AT_HOUR( TVCHANNELTYPE_CHANNEL_1, "PL_CINEMA_MULTIPLAYER_NO_MELTDOWN", 10)
		ENDIF
	ENDIF
		
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)
	
	SET_CINEMA_AUDIO_SCENE()
	
	SET_TV_VOLUME(-5.0)
	
	ENABLE_MOVIE_SUBTITLES(TRUE)
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - LOCAL_STAGE_WATCHING_MOVIE - START_THE_MOVIE()")		
	RETURN TRUE
ENDFUNC

PROC DRAW_TV_TEXTURE_TO_RENDERTARGET()
	
	SET_HUD_COMPONENT_POSITION(NEW_HUD_SUBTITLE_TEXT, 0, -0.0375)
	
	//IF MovieId != NULL
	SET_TEXT_RENDER_ID(iRenderTarget)
	//DRAW_BINK_MOVIE(MovieId, 0.5,0.5,1.0,1.0,0.0,255,255,255,255)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	
	
	IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("MOVIE_ARTHOUSE"))
	
	#IF IS_JAPANESE_BUILD
		OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("MOVIE_CARTOON"))
	#ENDIF
	
		DRAW_TV_CHANNEL(0.5,0.5,0.7375,1.0,0.0,255,255,255,255) // Standard wide screen  1.77:1
	ELSE
		DRAW_TV_CHANNEL(0.5,0.5,1.0,1.0,0.0,255,255,255,255) // Full wide screen 2.40:1
	ENDIF
	
	//PRINTLN("ACT_CINEMA: SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)")
	//ENDIF
ENDPROC

PROC DISPLAY_INSTRUCTIONAL_BUTTONS()

	INT iScreenX
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		bUpdateCinemaControls = FALSE
	ENDIF
	
	
//	IF HAS_SCALEFORM_MOVIE_LOADED(scaleformCinemaControls)
	IF NOT bUpdateCinemaControls
		IF LOAD_MENU_ASSETS()
			CLEAR_MENU_DATA()
//			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons)
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "TXM_EXIT") //"IB_CIN_EXIT"
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "INPUTGROUP_LOOK") //"IB_CIN_LOOK"
			ADD_MENU_HELP_KEY_INPUT( INPUT_SNIPER_ZOOM, "HUD_INPUT91") //"IB_CIN_ZOOM"
			bUpdateCinemaControls = TRUE
	//		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(scaleformCinemaControls, aSpriteCinemaControls, instructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(instructionalButtons))
		ELSE
	//		scaleformCinemaControls = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
//			LOAD_MENU_ASSETS()
		ENDIF
	ELSE
		DRAW_MENU_HELP_SCALEFORM(iScreenX)
	ENDIF
	
ENDPROC

FUNC BOOL IS_ANYONE_IN_CINEMA()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF playerBD[i].bInCinema 
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_STARTING_JOB()
	//IF NOT bGotoCleanUpAfterCorona
		//If the player enters a corona
		IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
			//Set the clean up flag
		//	bGotoCleanUpAfterCorona = TRUE
		//ENDIF
	//ELSE
		//Check to see if they quit
	///	IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
	//		bGotoCleanUpAfterCorona = FALSE
			RETURN TRUE
		ENDIF
	//ENDIF
	RETURN FALSE
ENDFUNC

//PROC HANDLE_TV_VOLUME()
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)	
//		fMovieVolume++
//
//		IF fMovieVolume > 0.0
//			fMovieVolume = 0.0
//		ENDIF
//		
////		SET_BINK_MOVIE_VOLUME(MovieId, fMovieVolume)
//		
//		PRINTNL()
//		PRINTSTRING ("Act_Cinema: fMovieVolume ")
//		PRINTFLOAT (fMovieVolume)
//		PRINTNL()
//	ENDIF
//					
//	//turn movie volume down.
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)	
//	
//		fMovieVolume--
//		
//		IF fMovieVolume < -24.0
//			fMovieVolume = -24.0
//		ENDIF
//		
//		SET_BINK_MOVIE_VOLUME(MovieId,fMovieVolume)
//		
//		PRINTNL()
//		PRINTSTRING ("Act_Cinema: fMovieVolume ")
//		PRINTFLOAT (fMovieVolume)
//		PRINTNL()
//	ENDIF
//ENDPROC
//
//PROC MAINTAIN_TV_TIME()
//	IF MovieId != NULL
//		IF ABSF(GET_BINK_MOVIE_TIME(MovieId) - DETERMINE_MOVIE_TIME(#IF IS_DEBUG_BUILD FALSE #ENDIF)) > 0.25
//			FLOAT fMovieTime
//			fMovieTime = DETERMINE_MOVIE_TIME()
//			NET_PRINT_STRING_FLOAT("GET_BINK_MOVIE_TIME(MovieId) : ", GET_BINK_MOVIE_TIME(MovieId)) NET_NL()
//			NET_PRINT_STRING_FLOAT("DETERMINE_MOVIE_TIME() : ", fMovieTime)
//			SET_BINK_MOVIE_TIME(MovieId , fMovieTime)
//			NET_PRINT("Reseting Movie time as it has gone out of sync") NET_NL()
//		ENDIF
//	ENDIF
//ENDPROC

PROC RESET_FLAGS()
	iCinemaBS = 0
	startedFadeOut = FALSE
	bWarpStarted = FALSE
	RESET_NET_TIMER(LoadMovieTimeOut)
ENDPROC

#IF IS_DEBUG_BUILD
PROC TEST_SEAT_POSITIONS()
	IF iLocalStage = LOCAL_STAGE_WATCHING_MOVIE
		IF iCurrentSeat != iDebugSeat
			thisPlayersSeat =  GET_SEAT_DATA(iDebugSeat)
			IF NET_WARP_TO_COORD(thisPlayersSeat.vSeatPos,thisPlayersSeat.fSeatHeading,FALSE,FALSE,FALSE,FALSE,FALSE)
				CLEAR_FIRST_PERSON_CAMERA(newFPCam)
				//IF DOES_SCENARIO_EXIST_IN_AREA(thisPlayersSeat.vSeatPos,0.5,TRUE)
				//	TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(),thisPlayersSeat.vSeatPos,0.5,-1)
					TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"prop_human_seat_chair_upright",thisPlayersSeat.vSeatPos,thisPlayersSeat.fSeatHeading,-1)
					//INIT_FIRST_PERSON_CAMERA(fpsCam,reinitCam,thisPlayersSeat.vCamPos,thisPlayersSeat.vCamRot,thisPlayersSeat.fCamFOV,20,20,10,10,14,-1,FALSE,10,10,0.1,FALSE,0,5)
					INIT_FIRST_PERSON_CAMERA(newFPCam,thisPlayersSeat.vCamPos,thisPlayersSeat.vCamRot,thisPlayersSeat.fCamFOV,20,10,3)
					iCurrentSeat = iDebugSeat
				//ELSE
				//	PRINTSTRING("ACT_CINEMA: waiting for scenario to exist")
				//	PRINTNL()
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RUN_DEBUG()
	//Do any debug processing
	TEST_SEAT_POSITIONS()
	
	IF NOT bRefocusedCam
		IF bRefocusCam
			REFOCUS_FIRST_PERSON_CAMERA_ON_WORLD_POSITION(newFPCam, <<387.1216, -731.6599, 12.2018>>)
			bRefocusedCam = TRUE
		ENDIF
	ELSE
		IF NOT bRefocusCam
			REFOCUS_FIRST_PERSON_CAMERA_ON_WORLD_POSITION(newFPCam, <<0,0,0>>)
			bRefocusedCam = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC CREATE_WIDGETS()
	START_WIDGET_GROUP("act_cinema")
		ADD_WIDGET_INT_SLIDER("iSeat", iDebugSeat, 0,31,1)
		ADD_WIDGET_BOOL("Refocus Cam", bRefocusCam)
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

PROC TIDY_UP_IF_DEAD()
	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
		HIDE_MY_PLAYER_BLIP(FALSE)
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
		RESTORE_STANDARD_CHANNELS()
	ENDIF
	CLEAR_FIRST_PERSON_CAMERA(newFPCam)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		DO_SCREEN_FADE_IN(500)
		startedFadeOut = FALSE
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		bCinemaTookAwayPlayerControl = FALSE
		SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
	ELSE
		DO_SCREEN_FADE_IN(500)
		startedFadeOut = FALSE
		IF bCinemaTookAwayPlayerControl
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		ENDIF
		bCinemaTookAwayPlayerControl = FALSE
		playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = FALSE
		//CLEANUP_MP_CUTSCENE()
		SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
	ENDIF
ENDPROC

PROC COUNT_PARTICIPANTS()
	IF canStartActivity(cinemaInfo.eLocation)
		IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())
			iParticipants++
		ENDIF
		IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())
			iParticipants++
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MP_DISPLAY_FLAG()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			IF IS_CINEMA_UNLOCKED()
				IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),cinemaInfo.vWarpExitLoc) <= 100
					IF NOT bSetActAvailable
						IF ADD_NAMED_ACTIVITY_TO_DISPLAY_LIST(cinemaInfo.vWarpExitLoc,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CINEMA),"")
							bSetActAvailable = TRUE
						ENDIF
					ENDIF
				ELSE
					IF bSetActAvailable
						REMOVE_ACTIVITY_FROM_DISPLAY_LIST(cinemaInfo.vWarpExitLoc,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CINEMA))
						bSetActAvailable = FALSE
					ENDIF
				ENDIF
			ELSE
				IF bSetActAvailable
					REMOVE_ACTIVITY_FROM_DISPLAY_LIST(cinemaInfo.vWarpExitLoc,GET_ACTIVITY_NAME_FROM_FMMC_TYPE(FMMC_TYPE_CINEMA))
					bSetActAvailable = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_WANTED()
	
	//IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	//	RETURN FALSE
	//ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC CLEAR_CINEMA_DENIED_HELP()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("NOCIN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TIMCIN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN2_JPN")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_CIN_WANT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TIMCINMULTI")
		CLEAR_HELP()
	ENDIF

ENDPROC

//FUNC STRING GET_IN_CINEMA_HELP()
	//IF NETWORK_IS_GAME_IN_PROGRESS()
	//	RETURN "MP_CIN_H0b"
	//ENDIF
//	RETURN "MP_CIN_H0"
//ENDFUNC

FUNC VECTOR GET_POSITION_INFRONT_OF_CURRENT_RENDERING_CAM(VECTOR vStart, FLOAT fDistance = 1000.0)
	VECTOR vEnd
	//end vector - pointing north
	vEnd	= <<0.0, 1.0, 0.0>>
	//point it in the same direction as the camera
	GET_VECTOR_FROM_ROTATION(vEnd,GET_CAM_ROT(newFPCam.theCam))
	//GET_FINAL_RENDERED_CAM_ROT()
	//Make the normilised roted vector 400 times larger
	vEnd.x *= fDistance
	vEnd.y *= fDistance
	vEnd.z *= fDistance
	//add it on to the start vector to get the end vector coordinates
	vEnd += vStart
	RETURN vEnd
ENDFUNC

PROC HANDLE_MOVING_HEAD()
	VECTOR vLookAtCoord = GET_POSITION_INFRONT_OF_CURRENT_RENDERING_CAM(newFPCam.vInitCamPos,60)
	TASK_LOOK_AT_COORD(PLAYER_PED_ID(),vLookAtCoord,1000,SLF_DEFAULT)
	//NET_PRINT_STRING_VECTOR("ACT_CINEMA: Telling platyer to look at :", vLookAtCoord) NET_NL()
ENDPROC

PROC SET_ENTER_LEAVING_FLAGS_ON_ENTERING()
	INT iParticipant
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NOT IS_BIT_SET(iEnterLeaveBS,iParticipant)
			IF playerBD[iParticipant].bInCinema
				SET_BIT(iEnterLeaveBS,iParticipant)
			ENDIF
		ELSE
			IF NOT playerBD[iParticipant].bInCinema
				CLEAR_BIT(iEnterLeaveBS,iParticipant)
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


PROC MAINTAIN_ENTER_LEAVING_TICKER()	
	IF NOT IS_BIT_SET(iEnterLeaveBS,iSlowParticipantLoop)
		IF playerBD[iSlowParticipantLoop].bInCinema
			IF iSlowParticipantLoop != PARTICIPANT_ID_TO_INT()
				PLAYER_INDEX thePlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSlowParticipantLoop))
				PRINT_TICKER_WITH_PLAYER_NAME("MP_CIN_TICKIN",thePlayer)
			ENDIF
			SET_BIT(iEnterLeaveBS,iSlowParticipantLoop)
		ENDIF
	ELSE
		IF NOT playerBD[iSlowParticipantLoop].bInCinema
			IF iSlowParticipantLoop != PARTICIPANT_ID_TO_INT()
				PLAYER_INDEX thePlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iSlowParticipantLoop))
				PRINT_TICKER_WITH_PLAYER_NAME("MP_CIN_TICKOUT",thePlayer)
			ENDIF
			CLEAR_BIT(iEnterLeaveBS,iSlowParticipantLoop)
		ENDIF
	ENDIF
	
	iSlowParticipantLoop++
	IF iSlowParticipantLoop >= NETWORK_GET_NUM_PARTICIPANTS()
		iSlowParticipantLoop = 0
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CINEMA_CLEANUP()
	IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_EXITED)
	//IF NOT IS_PLAYER_TELEPORT_ACTIVE()
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		AND iLocalStage != LOCAL_STAGE_WATCHING_MOVIE
			RETURN TRUE
		ENDIF
		IF g_bInMultiplayer
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF
		IF iLocalStage = LOCAL_STAGE_WATCHING_MOVIE
			IF IS_PLAYER_OK(PLAYER_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(newFPCam.vInitCamPos,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30
					SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
				ENDIF
			ENDIF
		ENDIF
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			//Block the cinema while in Director Mode
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DISPLAY_NOT_ENOUGH_MONEY_HELP()
		
		
		IF eCurrentCinemaShowing = CINEMA_SHOWING_MORNING
			
			#IF IS_JAPANESE_BUILD
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN1_JPN")
					CLEAR_HELP()
					PRINT_HELP("CASHCIN1_JPN")
				ENDIF
			#ENDIF
			
			#IF NOT IS_JAPANESE_BUILD
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN1")
					CLEAR_HELP()
					PRINT_HELP("CASHCIN1")
				ENDIF
			#ENDIF
		
		ELIF eCurrentCinemaShowing = CINEMA_SHOWING_AFTERNOON
		
			#IF IS_JAPANESE_BUILD
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN2_JPN")
					CLEAR_HELP()
					PRINT_HELP("CASHCIN2_JPN")
				ENDIF
			#ENDIF
			
			#IF NOT IS_JAPANESE_BUILD
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN2")
					CLEAR_HELP()
					PRINT_HELP("CASHCIN2")
				ENDIF
			#ENDIF
			
		ELIF eCurrentCinemaShowing = CINEMA_SHOWING_EVENING
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN3")
				CLEAR_HELP()
				PRINT_HELP("CASHCIN3")
			ENDIF							
		
		ELIF eCurrentCinemaShowing = CINEMA_SHOWING_MULTIPLAYER
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CASHCIN")
				CLEAR_HELP()
				PRINT_HELP("CASHCIN")
			ENDIF
			
		ENDIF
		
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)
	vInput = in_coords.vec_coord[0]
//	fInput = in_coords.headings[0]
	
	
	PRINTNL()
	PRINTSTRING(GET_THIS_SCRIPT_NAME())
	PRINTSTRING(" launched at ")
	PRINTVECTOR(vInput)
	PRINTSTRING(" with heading ")
	PRINTFLOAT(in_coords.headings[0])
	PRINTNL()
	
	//warningScreen.sQuestionText = "MP_ACT_LEAVE"
	
	#IF IS_DEBUG_BUILD
		CREATE_WIDGETS()
	#ENDIF
	INIT_CURRENT_CINEMA_DATA()
	PROCESS_PRE_GAME()
	IF NOT g_bInMultiplayer
		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_ACTIVITY_ALWAYS)
			IF g_bActivityLaunchedFromDebugMenu
				//bIgnoreTimeConstraints = TRUE
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("act_cinema")) = 1
					g_bActivityLaunchedFromDebugMenu = FALSE
				ENDIF
			ELSE
				IF NOT Get_Mission_Flow_Flag_State(FLOWFLAG_ALLOW_CINEMA_ACTIVITY)
					PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - FLOWFLAG_ALLOW_CINEMA_ACTIVITY is FALSE. Cinema will clean up.")
					MISSION_CLEANUP()
				ENDIF
			ENDIF
		ELSE
		
			// Added this due to B* 1585280 - If a switch scene places you near a cinema it'll shut down because a switch gets flagged as a mission type.
			// So I've added an exception for switch mission flags.
		
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_SWITCH)
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - RUN_ON_ACTIVITY_ALWAYS is FALSE, However, switch is active, so it's probably ok to continue.")
			ELSE
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - RUN_ON_ACTIVITY_ALWAYS is FALSE. Cinema will clean up.")
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY) is ", IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY))
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG) is ", IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG))
				MISSION_CLEANUP()
			ENDIF
		ENDIF
	ELSE
		//bIgnoreTimeConstraints = TRUE
	ENDIF
	
	COUNT_PARTICIPANTS()
	
	WHILE TRUE
		WAIT(0)
		#IF IS_DEBUG_BUILD
			RUN_DEBUG()
		#ENDIF
		
		INT iTimeOfDay = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		INT iTimeOfDayMinute = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
		
//		IF IS_PLAYER_STARTING_JOB()
//			PRINTLN("IS_PLAYER_STARTING_JOB(): player starting a job script shoudl terminate")
//		ENDIF
		//exit if not in world braind activation range
		IF SHOULD_CINEMA_CLEANUP()
			PRINTSTRING("ACT_CINEMA: Ending script SHOULD_CINEMA_CLEANUP()")
			PRINTNL()
			SET_LOCAL_STAGE(LOCAL_STAGE_END)
		ENDIF
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			REINIT_NET_TIMER(netStoredTime)
			MAINTAIN_MP_DISPLAY_FLAG()
			//IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				//IF NOT HAS_NET_TIMER_STARTED(serverBD.MovieStartTimer)
					//IF IS_ANYONE_IN_CINEMA()
						//START_NET_TIMER(serverBD.MovieStartTimer)
						//NET_PRINT("ACT_CINEMA: server started the timer") NET_NL()
					//ENDIF
				//ENDIF
			//ENDIF
		ENDIF
		
		IF bStartedInMP
				// If we have a match end event, bail.
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				PRINTSTRING("ACT_CINEMA: Ending script SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()")
				PRINTNL()
				MISSION_CLEANUP()
			ENDIF
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				PRINTSTRING("ACT_CINEMA: Ending script not NOT NETWORK_IS_GAME_IN_PROGRESS()")
				PRINTNL()
				MISSION_CLEANUP()
			ENDIF
			//IF g_FMMC_STRUCT.g_InvitedToAmbientActivity
			///	IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), cinemaInfo.vWarpExitLoc) > 20
			//		g_FMMC_STRUCT.g_InvitedToAmbientActivity = FALSE
			//	ENDIF
			//ENDIF
		ENDIF
		
		SWITCH iLocalStage
			CASE LOCAL_STAGE_INIT
				IF LOADED_AND_CREATED_CINEMA_SCENE()
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						SET_LOCAL_STAGE(LOCAL_STAGE_WAIT_TO_ENTER)
					ELSE
						IF IS_PLAYER_SCTV(PLAYER_ID())
							INIT_SCTV_CINEMA()
							SET_LOCAL_STAGE(LOCAL_STAGE_SCTV)
						ELSE
							SET_LOCAL_STAGE(LOCAL_STAGE_WAIT_TO_ENTER)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE LOCAL_STAGE_WAIT_TO_ENTER
				
				RESET_FLAGS()
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
						IF NOT g_sShopSettings.bProcessStoreAlert
							CLEAR_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
							//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							PRINTLN("  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CLEARED")
						ENDIF
						BREAK
					ENDIF
				ENDIF
				
				IF IS_PLAYER_OK(PLAYER_ID())
				
					// Main checks
					IF NOT IS_PLAYER_AT_CINEMA()
					//OR NOT IS_PED_ON_FOOT(PLAYER_PED_ID())
					OR IS_PHONE_ONSCREEN()
					OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONhUD)
					OR NOT IS_SKYSWOOP_AT_GROUND()
					OR ((IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)) AND NOT canStartActivity(cinemaInfo.eLocation))
					OR IS_CUSTOM_MENU_ON_SCREEN()
					OR IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
					OR IS_PED_RAGDOLL(PLAYER_PED_ID())
					OR IS_BROWSER_OPEN()	
					OR IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
					OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
					OR GET_PLAYER_RIVAL_ENTITY_TYPE(PLAYER_ID()) = eRIVALENTITYTYPE_SMUGGLER
					OR GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
					OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					OR IS_PLAYER_AN_ANIMAL(PLAYER_ID())
//					OR IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID()) 
//					OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant = TRUE
						//CPRINTLN(DEBUG_AMBIENT, "CINEMA - MAIN CHECK FAILED")
						
						CLEAR_CINEMA_DENIED_HELP()
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						BREAK				
					ENDIF
					
					IF IS_PLAYER_AT_CINEMA()
					AND IS_PLAYER_USING_BALLISTIC_EQUIPMENT(PLAYER_ID())
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						AND NOT bDisplayedBEHelp
							BEGIN_TEXT_COMMAND_DISPLAY_HELP("SHOP_JUGG_NONE")
								ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("THEMOVIETH")
							END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, FALSE)
							bDisplayedBEHelp = TRUE
						ENDIF
						
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						BREAK
					ENDIF
										
					eCurrentCinemaShowing = GET_CINEMA_SHOWING()
					
					// Cinema unlocked?
					IF NOT IS_CINEMA_UNLOCKED() 
						
						//CPRINTLN(DEBUG_AMBIENT, "CINEMA - UNLOCK CHECK FAILED")
						
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("NOCIN")
							CLEAR_HELP()
							PRINT_HELP("NOCIN")
						ENDIF
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						BREAK
					ENDIF
					
					IF NOT IS_ACTIVITY_TIME_ACCEPTABLE() 		
					OR NOT IS_NET_FREE_FOR_CINEMA()
					
						//CPRINTLN(DEBUG_AMBIENT, "CINEMA - TIME CHECK FAILED")
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
						
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TIMCINMULTI")
								CLEAR_HELP()
								PRINT_HELP_FOREVER("TIMCINMULTI")
							ENDIF
							
						ELSE
						
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TIMCIN")
								CLEAR_HELP()
								PRINT_HELP_FOREVER("TIMCIN")
							ENDIF
						
						ENDIF
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						BREAK
					ENDIF
					
					
					IF IS_PLAYER_WANTED()
							
						//CPRINTLN(DEBUG_AMBIENT, "CINEMA - WANTED CHECK FAILED")
						
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_CIN_WANT")
							CLEAR_HELP()
							PRINT_HELP("MP_CIN_WANT")
						ENDIF
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						BREAK
					ENDIF
						
					IF NOT IS_ACTIVITY_COST_ACCEPTABLE()
					
						IF NOT NETWORK_IS_GAME_IN_PROGRESS()
							//CPRINTLN(DEBUG_AMBIENT, "CINEMA - COST CHECK FAILED")
							
							DISPLAY_NOT_ENOUGH_MONEY_HELP()
							CLEAR_CONTEXT_OVERRIDE_HELP()
							RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
							
							BREAK
						ENDIF
						
					ENDIF
					
					// Can activate cinema straight way if on a friend activity and in a vehicle.
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
					OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG))
					AND canStartActivity(cinemaInfo.eLocation)
						SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED )
						DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(),TRUE)
						CLEAR_CONTEXT_OVERRIDE_HELP()
						RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
						startActivity(cinemaInfo.eLocation)
							
						DISABLE_CINEMA_INTERIOR(FALSE) // Cinema interior is disabled by default, so we have to re-enable it.

						SET_LOCAL_STAGE(LOCAL_STAGE_HANDLE_ENTERING)	
						BREAK
				
					ENDIF
											
					IF iContextButtonIntention = NEW_CONTEXT_INTENTION
					
						CLEAR_CINEMA_DENIED_HELP()
					
						eCurrentCinemaShowing = GET_CINEMA_SHOWING()
						bIsCinemaOwnerInPlayerGroup = IS_CINEMA_OWNER_IN_PLAYER_GROUP()
					
						// Display different help depending on which showing is currently active.
						IF eCurrentCinemaShowing = CINEMA_SHOWING_MORNING
							
							#IF IS_JAPANESE_BUILD
								IF NOT bIsCinemaOwnerInPlayerGroup
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN1_JPN")
								ELSE
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN1F_JPN")
								ENDIF
							#ENDIF
							
							#IF NOT IS_JAPANESE_BUILD
								IF NOT bIsCinemaOwnerInPlayerGroup
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN1")
								ELSE
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN1F")
								ENDIF
							#ENDIF
							
						ELIF eCurrentCinemaShowing = CINEMA_SHOWING_AFTERNOON
						
							#IF IS_JAPANESE_BUILD
								IF NOT bIsCinemaOwnerInPlayerGroup
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN2_JPN")
								ELSE
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN2F_JPN")	
								ENDIF
							#ENDIF
							
							#IF NOT IS_JAPANESE_BUILD
								IF NOT bIsCinemaOwnerInPlayerGroup
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN2")
								ELSE
									REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN2F")
								ENDIF
							#ENDIF
							
						ELIF eCurrentCinemaShowing = CINEMA_SHOWING_EVENING
							IF NOT bIsCinemaOwnerInPlayerGroup
								REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN3")
							ELSE
								REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "ACTCIN3F")
							ENDIF
						ELIF  eCurrentCinemaShowing = CINEMA_SHOWING_MULTIPLAYER
							PRINT_HELP_FOREVER_WITH_NUMBER("ACTCIN",g_sMptunables.icinema_expenditure_modifier)
							REGISTER_CONTEXT_INTENTION(iContextButtonIntention, CP_MAXIMUM_PRIORITY, "",TRUE)
						ENDIF

						BREAK
						
					ELSE
					
						IF HAS_CONTEXT_BUTTON_TRIGGERED(iContextButtonIntention)
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
								
								IF NOT IS_ACTIVITY_COST_ACCEPTABLE()
									IF NOT IS_BIT_SET(iBoolsBitSet, iBS_STORE_ACTIVE)
										LAUNCH_STORE_CASH_ALERT()
										SET_BIT(iBoolsBitSet, iBS_STORE_ACTIVE)
										//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, FALSE)
										PRINTLN("  -->  CONTACT REQUESTS - LAUNCH_STORE_CASH_ALERT - CALLED")
									ENDIF
									BREAK
								ELSE
										playerBD[PARTICIPANT_ID_TO_INT()].bInCinema = TRUE
										SET_ENTER_LEAVING_FLAGS_ON_ENTERING()
										Pause_Objective_Text()
										SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CINEMA, TRUE)
										
										SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CINEMA)
								ENDIF
							ELSE
								
								SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED )
								
								DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(),TRUE)
								
								g_bPlayerIsInCinema = TRUE // B*2019260 - Used to tell the Chop script when the player is in a cinema
							ENDIF
								
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							CLEAR_CONTEXT_OVERRIDE_HELP()
							RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
							IF canStartActivity(cinemaInfo.eLocation)
								startActivity(cinemaInfo.eLocation)
							ENDIF
							
							DISABLE_CINEMA_INTERIOR(FALSE) // Cinema interior is disabled by default, so we have to re-enable it.
							
							bPlayerLeftCinemaBecauseMovieEnded = FALSE
							
							SET_LOCAL_STAGE(LOCAL_STAGE_HANDLE_ENTERING)	
							
							BREAK

						ELSE
							
							// Check that the show time is still valid. (Player could be waiting on the trigger for a while)
							// If not reset the context intention so we update the help text.
							IF GET_CINEMA_SHOWING() != eCurrentCinemaShowing
								CLEAR_CONTEXT_OVERRIDE_HELP()
								RELEASE_CONTEXT_INTENTION(iContextButtonIntention)
							ENDIF
							
							BREAK
								
						ENDIF
						
					ENDIF
								
				ENDIF
				
				BREAK
			
			
			CASE LOCAL_STAGE_HANDLE_ENTERING
			
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() // For B*1983849
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				SUPPRESS_AGITATION_EVENTS_NEXT_FRAME()
				
				IF bWarpStarted
					IF IS_PLAYER_OK(PLAYER_ID())
						IF NETWORK_IS_GAME_IN_PROGRESS()
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()//Cleans up any on calls
							ENABLE_MOVIE_KEYFRAME_WAIT(TRUE)
							PRINTLN("@@@@@@@@@@@@@@ ENABLE_MOVIE_KEYFRAME_WAIT(TRUE) @@@@@@@@@@@@@@")
							CLEAR_ALL_HELP_MESSAGES()
							IF GET_CLIENT_SEAT()
								IF NET_WARP_TO_COORD(cinemaInfo.vWarpInsideCoords,thisPlayersSeat.fSeatHeading,FALSE,FALSE,FALSE,FALSE,FALSE)
								//IF NET_WARP_TO_COORD(thisPlayersSeat.vSeatPos,thisPlayersSeat.fSeatHeading,FALSE,FALSE,FALSE,FALSE,FALSE)
									ACTIVATE_SCENARIO_GROUP()
									//TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"prop_human_seat_chair_upright",thisPlayersSeat.vSeatPos,thisPlayersSeat.fSeatHeading,-1)
									playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = TRUE
									PRINTLN("ACT_CINEMA: Setting playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = TRUE") 
									//INIT_FIRST_PERSON_CAMERA(newFPCam,thisPlayersSeat.vCamPos,thisPlayersSeat.vCamRot,thisPlayersSeat.fCamFOV,20,10,3)
									INIT_FIRST_PERSON_CAMERA(newFPCam,thisPlayersSeat.vCamPos,thisPlayersSeat.vCamRot,thisPlayersSeat.fCamFOV,20,10,3, 20.0, FALSE, 0, -1.0, TRUE)
									//SCRIPT_PLAYSTATS_MISSION_STARTED(GET_MISSION_NAME_FROM_TYPE(FMMC_TYPE_CINEMA)) 
									HIDE_MY_PLAYER_BLIP(TRUE)
									
									SET_LOCAL_STAGE(LOCAL_STAGE_WATCHING_MOVIE)
								ELSE
									PRINTSTRING("ACT_CINEMA: waiting for NET_WARP_TO_COORD")
									PRINTNL()
								ENDIF
							ELSE
								PRINTSTRING("ACT_CINEMA: waiting for server to assign seat")
								PRINTNL()
							ENDIF
						ELSE
							HIDE_HUD_AND_RADAR_THIS_FRAME()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
							
							IF IS_INTERIOR_READY(interiorCinema)
							AND IS_NEW_LOAD_SCENE_LOADED()
								//IF IS_INTERIOR_SCENE()
									//FIRST_PERSON_CAM_STRUCT& fpCam, VECTOR vInitPos, VECTOR vInitRot, FLOAT fInitFov, INT iLookXLimit = 20, INT iLookYLimit = 10, INT iRollLimit = 3, FLOAT fMaxZoom = 20.0, BOOL bLockMiniMap = FALSE, INT iLockAngle = 0, FLOAT fNearClip = -1.0, BOOL bDisableHandshake = FALSE
									HUD_FORCE_WEAPON_WHEEL(FALSE)
									INIT_FIRST_PERSON_CAMERA(newFPCam,cinemaInfo.vLookCamPos,cinemaInfo.vLookCamRot,35,20,10,3, 20.0, FALSE, 0, -1.0, TRUE)
								
									RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), interiorCinema)
									//INIT_FIRST_PERSON_CAMERA(fpsCam,reinitCam,cinemaInfo.vLookCamPos,cinemaInfo.vLookCamRot,35,20,20,10,10,14,-1,FALSE,10,10,0.1,FALSE,0,5)
									
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)	//Disabled using multihead blinders
									
									SET_LOCAL_STAGE(LOCAL_STAGE_WATCHING_MOVIE)
									PRINTLN(GET_THIS_SCRIPT_NAME(), " - SET_LOCAL_STAGE(LOCAL_STAGE_WATCHING_MOVIE)")
									
								//ENDIF
							ELSE
								PRINTNL()
								PRINTSTRING("act_cinema - IS_INTERIOR_READY(interiorCinema) = FALSE")
								PRINTNL()
							ENDIF
						ENDIF
					ELSE
						
						IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
						AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
							DO_SCREEN_FADE_IN(500)
						ENDIF
						startedFadeOut = FALSE
						SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
					ENDIF
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS()
						//IF IS_PLAYER_OK_TO_START_MP_CUTSCENE()
							IF NOT startedFadeOut
								DO_SCREEN_FADE_OUT(500)
								startedFadeOut = TRUE
							ELSE
								IF IS_PLAYER_OK(PLAYER_ID())
									IF IS_SCREEN_FADED_OUT()
										bCinemaTookAwayPlayerControl = TRUE
										//MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE,TRUE,TRUE,TRUE)
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
										//START_MP_CUTSCENE(TRUE)
										//HUD_FORCE_WEAPON_WHEEL(FALSE)
										GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),currentWeapon)
										IF currentWeapon != WEAPONTYPE_UNARMED
											SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
											PRINTLN("ACT_CINEMA: Setting current weapontype to unarmed")
										ENDIF
										bWarpStarted = TRUE
									ENDIF
								ELSE
									DO_SCREEN_FADE_IN(500)
									startedFadeOut = FALSE
									HIDE_MY_PLAYER_BLIP(FALSE)
									SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
								ENDIF
							ENDIF
						//ENDIF
						
					ELSE
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						IF NOT startedFadeOut
							DO_SCREEN_FADE_OUT(500)
							startedFadeOut = TRUE
						ELSE
							IF IS_PLAYER_OK(PLAYER_ID())
							
								SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTalk, TRUE)
							
								IF IS_SCREEN_FADED_OUT()
									bCinemaTookAwayPlayerControl = TRUE
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
									//SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
									SET_ENTITY_COORDS(PLAYER_PED_ID(),cinemaInfo.vSeatPos,TRUE,FALSE,FALSE,TRUE)
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
									SET_ENTITY_HEADING(PLAYER_PED_ID(), cinemaInfo.fSeatHeading)
									
									IF NOT IS_PED_INJURED(FRIEND_A_PED_ID())
										REMOVE_PED_FROM_GROUP(FRIEND_A_PED_ID())
										SET_ENTITY_COORDS(FRIEND_A_PED_ID(), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(cinemaInfo.vSeatPos, cinemaInfo.fSeatHeading, <<1.0, 0.0, 0.0>>),TRUE,FALSE,FALSE,TRUE)
										FREEZE_ENTITY_POSITION(FRIEND_A_PED_ID(), TRUE)
										SET_ENTITY_HEADING(FRIEND_A_PED_ID(), cinemaInfo.fSeatHeading)
									ENDIF
									
									IF NOT IS_PED_INJURED(FRIEND_B_PED_ID())
										REMOVE_PED_FROM_GROUP(FRIEND_B_PED_ID())
										SET_ENTITY_COORDS(FRIEND_B_PED_ID(), GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(cinemaInfo.vSeatPos, cinemaInfo.fSeatHeading, <<-1.0, 0.0, 0.0>>),TRUE,FALSE,FALSE,TRUE)
										FREEZE_ENTITY_POSITION(FRIEND_B_PED_ID(), TRUE)
										SET_ENTITY_HEADING(FRIEND_B_PED_ID(), cinemaInfo.fSeatHeading)
									ENDIF
									//HUD_FORCE_WEAPON_WHEEL(FALSE)
									
									IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
										NEW_LOAD_SCENE_START(cinemaInfo.vSeatPos, <<0.0, 0.0, cinemaInfo.fSeatHeading>>, 10.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE )
									ENDIF
									
									interiorCinema = GET_INTERIOR_AT_COORDS(cinemaInfo.vSeatPos) 
			       					PIN_INTERIOR_IN_MEMORY(interiorCinema)
									
									bWarpStarted = TRUE
								ENDIF
							ELSE
								
								IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
								AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
									DO_SCREEN_FADE_IN(500)
								ENDIF
								startedFadeOut = FALSE
								SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE LOCAL_STAGE_WATCHING_MOVIE
				
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() // For B*1983849
			
				//PRINTLN(GET_THIS_SCRIPT_NAME(), " - LOCAL_STAGE_WATCHING_MOVIE")
				IF IS_PLAYER_OK(PLAYER_ID())
				
					SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
					SUPPRESS_AGITATION_EVENTS_NEXT_FRAME()
					
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),currentWeapon)
					IF currentWeapon != WEAPONTYPE_UNARMED
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						PRINTLN("ACT_CINEMA: Setting current weapontype to unarmed")
					ENDIF

//GFX_ORDER_AFTER_HUD_PRIORITY_LOW
//GFX_ORDER_AFTER_HUD
//GFX_ORDER_AFTER_HUD_PRIORITY_HIGH
					//PRINTLN(GET_THIS_SCRIPT_NAME(), " - LOCAL_STAGE_WATCHING_MOVIE - IS_PLAYER_OK")
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_TODOBOX)
					DISABLE_SELECTOR_THIS_FRAME()
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
					//IF IS_SCRIPT_HUD_DISPLAYING (HUDPART_TRANSITIONhUD)
					//	SET_LOCAL_STAGE(LOCAL_STAGE_END)
					//	EXIT
					//ENDIF
					IF g_bInMultiplayer
						SET_IDLE_KICK_DISABLED_THIS_FRAME()
						MAINTAIN_ENTER_LEAVING_TICKER()
						playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = TRUE
						MPGlobals.invitedPlayers.vInviteLoc = cinemaInfo.vWarpExitLoc
						MPGlobalsAmbience.playerBounty.bPausePlayerBountyNextFrame = TRUE
						IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
							NET_PRINT_STRING_INT("ACT_CINEMA: Setting voice channel to: ",cinemaInfo.iVoiceChannel) NET_NL()
							NETWORK_SET_LOCAL_PLAYER_SYNC_LOOK_AT(TRUE)
							NETWORK_SET_VOICE_CHANNEL(cinemaInfo.iVoiceChannel)
							SET_BIT(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
						ENDIF
						DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
						DISABLE_CELLPHONE_INTERNET_APP_THIS_FRAME_ONLY()
						IF HAS_NET_TIMER_STARTED(cashDisplayTimer)
							IF NOT HAS_NET_TIMER_EXPIRED(cashDisplayTimer,7000,TRUE)
								SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
								SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
								SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
								SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
							ENDIF
							
							IF NOT IS_BIT_SET(iCinemaBS, CINEMA_BS_TAKEN_MONEY)
								//GIVE_LOCAL_PLAYER_FM_CASH(-ACTIVITY_COST,1,TRUE,0)<<-- SHOULD THIS BE BACK IN?
								INT iCost
								iCost = g_sMptunables.icinema_expenditure_modifier
								INT iDifference 
								iDifference = 0
								//If can't afford, adjust cost 
								IF NOT NETWORK_CAN_SPEND_MONEY2(iCost, FALSE, TRUE, FALSE, iDifference)
									iCost -= iDifference
								ENDIF
								IF iCost > 0 
									IF USE_SERVER_TRANSACTIONS()
										TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_CINEMA, iCost, iScriptTransactionIndex, FALSE, TRUE)
										g_cashTransactionData[iScriptTransactionIndex].cashInfo.iLocation = ENUM_TO_INT(cinemaInfo.eLocation)
									ELSE
										NETWORK_SPENT_CINEMA(iCost,ENUM_TO_INT(cinemaInfo.eLocation),FALSE, TRUE)
									ENDIF
								ENDIF 
							
								
								//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_CINEMA,ACTIVITY_COST)
								SET_PACKED_STAT_BOOL( PACKED_MP_WENT_TO_CINEMA, TRUE)
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_TIMES_CINEMA,1)
								PRINTLN("ACT_CINEMA: INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_TIMES_CINEMA,1)")
								iEnterTimeStamp = GET_CLOUD_TIME_AS_INT()
								SET_BIT(iCinemaBS, CINEMA_BS_TAKEN_MONEY)
							ENDIF
						ELSE
							START_NET_TIMER(cashDisplayTimer)
						ENDIF
						DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
					ELSE
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
						
						IF HAS_NET_TIMER_STARTED(cashDisplayTimer)
							IF NOT HAS_NET_TIMER_EXPIRED(cashDisplayTimer,7000,TRUE)
								IF NOT bIsCinemaOwnerInPlayerGroup
									SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
									SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
								ENDIF
							ENDIF
						ELSE
							START_NET_TIMER(cashDisplayTimer)
						ENDIF
						
						SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTalk, TRUE)
					ENDIF
					
					IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
						IF g_bInMultiplayer
							IF DOES_SCENARIO_EXIST_IN_AREA(thisPlayersSeat.vSeatPos,0.5,FALSE)
								IF NOT HAS_NET_TIMER_STARTED(spawnPedsTimeout)
									START_NET_TIMER(spawnPedsTimeout,TRUE)
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(PLAYER_PED_ID(),thisPlayersSeat.vSeatPos,0.5,-1)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
									NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								ENDIF
								IF serverBD.bSpawnedPeds
								OR HAS_NET_TIMER_EXPIRED_READ_ONLY(spawnPedsTimeout,10000,TRUE)
									//TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(),thisPlayersSeat.vSeatPos,0.5,-1)
									IF START_THE_MOVIE()
										SET_STORE_ENABLED(FALSE)
										DO_SCREEN_FADE_IN(500)
										startedFadeOut = FALSE
										SET_BIT(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
										RESET_NET_TIMER(spawnPedsTimeout)
									#IF IS_DEBUG_BUILD
									ELSE
										PRINTLN("ACT_CINEMA: Waiting for START_THE_MOVIE()")
									#ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									PRINTLN("ACT_CINEMA: Waiting for serverBD.bSpawnedPeds")
								#ENDIF
								ENDIF
							ELSE
								PRINTSTRING("ACT_CINEMA: waiting for scenario to exist")
								PRINTNL()
							ENDIF
						ELSE
							PRINTLN(GET_THIS_SCRIPT_NAME(), " - LOCAL_STAGE_WATCHING_MOVIE - NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_MOVIE_STARTED)")
							IF START_THE_MOVIE()
								
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								IF IS_PED_DUCKING(PLAYER_PED_ID()) // forces player to not crouch
									SET_PED_DUCKING(PLAYER_PED_ID(), FALSE)
								ENDIF
								SET_STORE_ENABLED(FALSE)
								DO_SCREEN_FADE_IN(500)
								startedFadeOut = FALSE
								SET_BIT(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
								
								PAY_FOR_CINEMA()
								
							ENDIF
						ENDIF
					ELSE
					
						IF IS_BIT_SET(iCinemaBS,CINEMA_BS_EXITED)
							//DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
							IF IS_SCREEN_FADED_OUT()
							OR IS_PLAYER_STARTING_JOB()
								IF NOT NETWORK_IS_GAME_IN_PROGRESS()
									IF NOT HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_CINEMA)
										REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_CINEMA)
										MAKE_AUTOSAVE_REQUEST()
									ENDIF
									SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
									RESTORE_STANDARD_CHANNELS()
									CLEAR_FIRST_PERSON_CAMERA(newFPCam)
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										SET_ENTITY_COORDS(PLAYER_PED_ID(), cinemaInfo.vWarpExitLoc)
										SET_ENTITY_HEADING(PLAYER_PED_ID(), cinemaInfo.fWarpExitHeading)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									ENDIF
									FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
									SET_GAMEPLAY_CAM_RELATIVE_HEADING()
									LOAD_SCENE(cinemaInfo.vWarpExitLoc)
									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									bCinemaTookAwayPlayerControl = FALSE
									IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
									OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
										SET_LOCAL_STAGE(LOCAL_STAGE_END)
									ELSE
										DO_SCREEN_FADE_IN(500)
										startedFadeOut = FALSE
										SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
									ENDIF
								ELSE
									IF IS_PLAYER_STARTING_JOB()
									OR WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE,FALSE, FALSE, FALSE)
										HIDE_MY_PLAYER_BLIP(FALSE)
										REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_CINEMA)
									//cinemaInfo.vWarpExitLoc,cinemaInfo.fWarpExitHeading,FALSE,FALSE,FALSE,FALSE,FALSE)
										IF IS_BIT_SET(iCinemaBS,CINEMA_BS_KICKED_OUT_CLOSED)
											PRINT_HELP("MP_CIN_KICK")
										ENDIF
										CLEAR_BIT(iCinemaBS,CINEMA_BS_KICKED_OUT_CLOSED)
										SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
										RESTORE_STANDARD_CHANNELS()
										CLEAR_FIRST_PERSON_CAMERA(newFPCam)
										//DO_SCREEN_FADE_IN(500)
										bCinemaTookAwayPlayerControl = FALSE
										
										IF NOT IS_PLAYER_STARTING_JOB()
											CLEAR_SPECIFIC_SPAWN_LOCATION()
											NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
											DO_SCREEN_FADE_IN(500)
										ENDIF
										
										CLEAR_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
										//CLEANUP_MP_CUTSCENE()
										CLEAR_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
																				
										SET_LOCAL_STAGE(LOCAL_STAGE_LEFT_CINEMA)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							
							IF NOT IS_SCRIPT_HUD_DISPLAYING (HUDPART_TRANSITIONhUD)
							AND IS_SKYSWOOP_AT_GROUND()
								//MAINTAIN_TV_TIME()
								
								IF NOT IS_CUSTOM_MENU_ON_SCREEN()
								AND NOT IS_PAUSE_MENU_ACTIVE()
								AND NOT IS_PHONE_ONSCREEN()
									DISPLAY_INSTRUCTIONAL_BUTTONS()
								ELIF bUpdateCinemaControls
									bUpdateCinemaControls = FALSE
								ENDIF
								
								DRAW_TV_TEXTURE_TO_RENDERTARGET()
							ENDIF
							
							IF NOT g_bInMultiplayer
								DISABLE_CELLPHONE_THIS_FRAME_ONLY()
							ENDIF
//							IF IS_PLAYER_STARTING_JOB()
//								PRINTLN("IS_PLAYER_STARTING_JOB() = TRUE abandon cinema")
//							ENDIF
							IF NOT IS_PAUSE_MENU_ACTIVE()
							AND NOT IS_SCRIPT_HUD_DISPLAYING (HUDPART_TRANSITIONhUD)
							AND IS_SKYSWOOP_AT_GROUND()
							OR IS_PLAYER_STARTING_JOB()
								UPDATE_FIRST_PERSON_CAMERA(newFPCam)
								IF NOT IS_PHONE_ONSCREEN()
									//IF NOT IS_BIT_SET(inviteOnlyMenu.iBS, INVONLY_MENU_BS_DRAWING_MENU)
										//HANDLE_TV_VOLUME()
										//IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_DISPLAYED_EXIT_HELP)
											//PRINT_HELP(GET_IN_CINEMA_HELP())
										//	SET_BIT(iCinemaBS,CINEMA_BS_DISPLAYED_EXIT_HELP)
										//ENDIF
									//ELSE
									//	IF IS_BIT_SET(iCinemaBS,CINEMA_BS_DISPLAYED_EXIT_HELP)
									//		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_IN_CINEMA_HELP())
									//			CLEAR_HELP()
									//		ENDIF
									//		CLEAR_BIT(iCinemaBS,CINEMA_BS_DISPLAYED_EXIT_HELP)
									//	ENDIF
									//ENDIF
									IF g_bInMultiplayer
//										IF NOT HAS_NET_TIMER_STARTED(cashDisplayTimer)
//											IF IS_SCREEN_FADED_IN()
//												
//											ENDIF
//										ENDIF
										IF IS_NET_PLAYER_OK(PLAYER_ID())
											HANDLE_MOVING_HEAD()
										ENDIF
									//ELSE
//										IF NOT HAS_NET_TIMER_STARTED(cashDisplayTimer)
//											IF IS_SCREEN_FADED_IN()
//												START_NET_TIMER(cashDisplayTimer)
//											ENDIF
//										ENDIF
									ENDIF
									IF NOT IS_CUSTOM_MENU_ON_SCREEN() //NOT IS_BIT_SET(inviteOnlyMenu.iBS, INVONLY_MENU_BS_DRAWING_MENU)
										IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
//											warningScreen.fButtonFlags = FE_WARNING_YESNO
//											SET_WARNING_MESSAGE_WITH_HEADER("MP_ACT_LEAVEH",warningScreen.sQuestionText,warningScreen.fButtonFlags)
//											DISABLE_CELLPHONE_THIS_FRAME_ONLY()
//											IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
//												IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
//												//AND NOT IS_BIT_SET(inviteOnlyMenu.iButtonBS,INVONLY_BUT_BS_CANCEL)
//													SET_BIT(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
//													IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
//														
//													ENDIF
//													CLEAR_BIT(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
//												ENDIF
//											ELSE
//												IF IS_BIT_SET(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
//													CLEAR_BIT(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
//												ENDIF
//												//CLEAR_BIT(inviteOnlyMenu.iButtonBS,INVONLY_BUT_BS_CANCEL)
//											ENDIF
//											IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
//												DO_SCREEN_FADE_OUT(0)
//												startedFadeOut = TRUE
//												IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_IN_CINEMA_HELP())
//													CLEAR_HELP()
//												ENDIF
//												IF NETWORK_IS_GAME_IN_PROGRESS()
//													SETUP_SPECIFIC_SPAWN_LOCATION(cinemaInfo.vWarpExitLoc, cinemaInfo.fWarpExitHeading, 10, FALSE)
//													SET_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
//												ENDIF
//												SET_BIT(iCinemaBS,CINEMA_BS_EXITED)	
//											ENDIF
										ELSE
										
											IF IS_CANCEL_JUST_PRESSED()
											OR IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER")) // Single player gets kicked out when the movie is over - They always get to watch the movie all the way through.
											OR (g_bInMultiplayer AND ((iTimeOfDay < CINEMA_TIME_MORNING_START AND iTimeOfDayMinute >= 0) AND (iTimeOfDay >= CINEMA_TIME_EVENING_END -22 AND iTimeOfDayMinute >= 30)))// Multiplayer gets kicked out if the cinema closes. //They always get to watch the movie all the way through.
											OR IS_PLAYER_STARTING_JOB()
//												IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_PRESSED_CANCEL) ///NOT IS_BIT_SET(inviteOnlyMenu.iBS, INVONLY_MENU_BS_DRAWING_MENU)
//												//AND NOT IS_BIT_SET(inviteOnlyMenu.iButtonBS,INVONLY_BUT_BS_CANCEL)
//												
//													SET_BIT(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)	
//													SET_BIT(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
//												ENDIF

												IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
													bPlayerLeftCinemaBecauseMovieEnded = TRUE
												ENDIF

												IF IS_CANCEL_JUST_PRESSED()
													PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
												ENDIF
													
												IF ((iTimeOfDay < CINEMA_TIME_MORNING_START
												AND iTimeOfDayMinute >= 0)
												AND (iTimeOfDay >= CINEMA_TIME_EVENING_END -22
												AND iTimeOfDayMinute >= 30))
													SET_BIT(iCinemaBS,CINEMA_BS_KICKED_OUT_CLOSED)
												ENDIF
												STOP_CINEMA_AUDIO_SCENE()
				
												IF NOT IS_AUDIO_SCENE_ACTIVE( "LEAVE_CINEMA")
													START_AUDIO_SCENE( "LEAVE_CINEMA")
												ENDIF

												DO_SCREEN_FADE_OUT(500)
												startedFadeOut = TRUE
												//IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_IN_CINEMA_HELP())
												//	CLEAR_HELP()
												//ENDIF
												IF NETWORK_IS_GAME_IN_PROGRESS()
													IF NOT IS_PLAYER_STARTING_JOB()
														SETUP_SPECIFIC_SPAWN_LOCATION(cinemaInfo.vWarpExitLoc, cinemaInfo.fWarpExitHeading, 10, FALSE)
														SET_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
													ENDIF
												ELSE
													SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
													SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)	//Disable using multihead blinders
												ENDIF
												g_bPlayerIsInCinema = FALSE		//Reset the global cinema bool
												SET_BIT(iCinemaBS,CINEMA_BS_EXITED)	
											ELSE	
												IF IS_BIT_SET(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
													CLEAR_BIT(iCinemaBS,CINEMA_BS_PRESSED_CANCEL)
												ENDIF
												//CLEAR_BIT(inviteOnlyMenu.iButtonBS,INVONLY_BUT_BS_CANCEL)
											ENDIF
											//IF g_bInMultiplayer
												//IF NOT IS_BIT_SET(inviteOnlyMenu.iBS, INVONLY_MENU_BS_DRAWING_MENU)
												//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
												//		SET_BIT(inviteOnlyMenu.iBS, INVONLY_MENU_BS_DRAWING_MENU)
												//	ENDIF
												//ENDIF
											//ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					TIDY_UP_IF_DEAD()
				ENDIF
			BREAK
			CASE LOCAL_STAGE_SCTV
				MAINTAIN_ENTER_LEAVING_TICKER()
				PLAYER_INDEX playerSpecTarget
				IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
					IF IS_PED_A_PLAYER(GET_SPECTATOR_SELECTED_PED())
						playerSpecTarget = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED())
					ENDIF
				ENDIF
				IF NATIVE_TO_INT(playerSpecTarget) > -1
				AND IS_NET_PLAYER_OK(playerSpecTarget)
				AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerSpecTarget, MPAM_TYPE_CINEMA)
					IF playerBD[NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(playerSpecTarget))].bInCinema
						ACTIVATE_SCENARIO_GROUP()
					ELSE
						DEACTIVATE_SCENARIO_GROUP()
					ENDIF
					
					IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_SCTV_NEEDS_CLEANUP)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === SET_BIT(iCinemaBS,CINEMA_BS_SCTV_NEEDS_CLEANUP)")
						#ENDIF
						SET_BIT(iCinemaBS,CINEMA_BS_SCTV_NEEDS_CLEANUP)
					ENDIF
					IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_SCTV_INTERIOR_LOAD_REQUESTED)
						DISABLE_CINEMA_INTERIOR(FALSE) // Cinema interior is disabled by default, so we have to re-enable it.
						interiorCinema = GET_INTERIOR_AT_COORDS(cinemaInfo.vSeatPos) 
		       			PIN_INTERIOR_IN_MEMORY(interiorCinema)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === SET_BIT(iCinemaBS,CINEMA_BS_SCTV_INTERIOR_LOAD_REQUESTED)")
						#ENDIF
						SET_BIT(iCinemaBS,CINEMA_BS_SCTV_INTERIOR_LOAD_REQUESTED)
					ELSE
						IF IS_INTERIOR_READY(interiorCinema)
							IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_SCTV_CAMERA_CREATED)
								IF NOT IS_SCREEN_FADED_OUT()
									DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === DO_SCREEN_FADE_OUT(0)")
									#ENDIF
									DO_SCREEN_FADE_OUT(0)
								ELSE
									VECTOR vPos, vRot
									FLOAT fFOV
									
									vPos = <<0.0, 0.0, 0.0>>
									vRot = <<0.0, 0.0, 0.0>>
									fFOV = 0.0
									
									GET_SCTV_CINEMA_DATA(vPos, vRot, fFOV)
								
									IF DOES_CAM_EXIST(camSCTV)
										DESTROY_CAM(camSCTV)
									ENDIF
									camSCTV = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
									
									SET_CAM_PARAMS(camSCTV, vPos, vRot, fFOV)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									
									IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
										FORCE_ROOM_FOR_GAME_VIEWPORT(interiorCinema, GET_KEY_FOR_ENTITY_IN_ROOM(GET_SPECTATOR_SELECTED_PED()))
									ENDIF
									
									ENABLE_SPECTATOR_FADES()
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === SET_BIT(iCinemaBS,CINEMA_BS_SCTV_CAMERA_CREATED)")
									#ENDIF
									SET_BIT(iCinemaBS,CINEMA_BS_SCTV_CAMERA_CREATED)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
									IF START_THE_MOVIE()
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== CINEMA === SET_BIT(iCinemaBS,CINEMA_BS_MOVIE_STARTED)")
										#ENDIF
										SET_BIT(iCinemaBS,CINEMA_BS_MOVIE_STARTED)
									ENDIF
								ELSE
									//MAINTAIN_TV_TIME()
									DRAW_TV_TEXTURE_TO_RENDERTARGET()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CLEANUP_SCTV_CINEMA()
				ENDIF
			BREAK
			CASE LOCAL_STAGE_LEFT_CINEMA
			
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME() // For B*1983849
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					HIDE_MY_PLAYER_BLIP(FALSE)
					IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
						NET_PRINT("ACT_CINEMA: Clearing voice channel: ") NET_NL()
						NETWORK_CLEAR_VOICE_CHANNEL()
						NETWORK_SET_LOCAL_PLAYER_SYNC_LOOK_AT(FALSE)
						CLEAR_BIT(iCinemaBS,CINEMA_BS_SET_VOICE_CHANNEL)
					ENDIF
					#IF IS_DEBUG_BUILD
						g_bFM_IgnoreRankOnNextLaunchedActivity = FALSE
					#ENDIF
					playerBD[PARTICIPANT_ID_TO_INT()].bInCinema = FALSE
					
					CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CINEMA)
					
					UNPause_Objective_Text()
					//UPDATE_FMMC_END_OF_MISSION_STATUS(ciFMMC_END_OF_MISSION_STATUS_PASSED, FMMC_TYPE_CINEMA)
					SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CINEMA, FALSE)
					INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_CINEMA,GET_CLOUD_TIME_AS_INT() - iEnterTimeStamp )
					PRINTLN("ACT_CINEMA: INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_CINEMA,GET_CLOUD_TIME_AS_INT() - iEnterTimeStamp ))")
					playerBD[PARTICIPANT_ID_TO_INT()].bWarpedInside = FALSE
					RESET_NET_TIMER(cashDisplayTimer)	
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_WATCH_A_MOVIE)
				ELSE
					RESET_NET_TIMER(cashDisplayTimer)
					DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(),FALSE)
					UNPIN_INTERIOR(interiorCinema)
				ENDIF
				
				SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
				RESTORE_STANDARD_CHANNELS()
				SET_STORE_ENABLED(TRUE)
				IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
					CLEAR_SPECIFIC_SPAWN_LOCATION()
					CLEAR_BIT(iCinemaBS,CINEMA_BS_SETUP_SPECIFIC_SPAWN)
				ENDIF
				IF IS_BIT_SET(iCinemaBS,CINEMA_BS_SHOW_EXIT_WARNING)
				//	ENABLE_SELECTOR()
				ENDIF
				IF iRenderTarget != -1
					IF IS_NAMED_RENDERTARGET_REGISTERED("cinscreen")
						RELEASE_NAMED_RENDERTARGET("cinscreen")
					ENDIF
				ENDIF
				
//				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(scaleformCinemaControls)
				CLEANUP_MENU_ASSETS()
				bUpdateCinemaControls = FALSE
				
				ENABLE_MOVIE_KEYFRAME_WAIT(FALSE)
				PRINTLN("@@@@@@@@@@@@@@ ENABLE_MOVIE_KEYFRAME_WAIT(FALSE) @@@@@@@@@@@@@@")
				
				DEACTIVATE_SCENARIO_GROUP()
				
				DISABLE_CINEMA_INTERIOR(TRUE)
			
				IF IS_PLAYER_OK(PLAYER_ID())
					//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, <<LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS>>)
						SET_LOCAL_STAGE(LOCAL_STAGE_WAIT_TO_ENTER)
					//ENDIF
				ENDIF
			BREAK
			CASE LOCAL_STAGE_END
				PRINTLN("<", GET_THIS_SCRIPT_NAME(), "> - LOCAL_STAGE_END. Cinema will clean up.")
				MISSION_CLEANUP()
			BREAK
		ENDSWITCH
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// Process server game logic	
			// NOTE: one one group of switch statements as this script "should" only be run by one person
			IF NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					ASSIGN_SEATS()
					
					SPAWN_SCENARIO_PEDS()
				ENDIF
			ENDIF
		ENDIF
	ENDWHILE
ENDSCRIPT


