

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 12/06/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							Flow Start Accept Screen							│
//│																				│
//│		Asks the player to allow the game to continue after a fresh install.	│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_hud.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "flow_public_core.sch"
USING "savegame_public.sch"


SCALEFORM_INDEX sfMessage
SCALEFORM_INDEX sfButtonHelp

BOOL bInstallScreenAccepted = FALSE


PROC CLEANUP_SCRIPT()
	CPRINTLN(DEBUG_FLOW, "<FLOW-START> Cleaning up.")
	IF sfMessage != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMessage)
	ENDIF
	IF sfButtonHelp != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfButtonHelp)
	ENDIF
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_NO_LOADING_SCREEN(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	CPRINTLN(DEBUG_FLOW, "<FLOW-START> Flow start accept script started.")

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> Flow start accept script forced to clean up.")
		CLEANUP_SCRIPT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			CPRINTLN(DEBUG_FLOW, "<FLOW-START> Automatically accepting as a flow launch is in progress.")
			SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_INSTALL_SCREEN_FINISHED)
			CLEANUP_SCRIPT()
		ENDIF
	#ENDIF
	
	IF NOT g_bRestoredSaveThisSession
	AND NOT IS_PC_VERSION()
	AND (HAS_GAME_INSTALLED_THIS_SESSION() #IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("SimulateInstallComplete") #ENDIF)

		SET_NO_LOADING_SCREEN(TRUE)
		
		//Fade screen out.
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				CPRINTLN(DEBUG_FLOW, "<FLOW-START> Fading screen out.")
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
		
		//Load scaleform and text.
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> Starting scaleform loading...")
		sfMessage = REQUEST_SCALEFORM_MOVIE("MP_BIG_MESSAGE_FREEMODE")
		sfButtonHelp = REQUEST_SCALEFORM_MOVIE("INSTRUCTIONAL_BUTTONS")
		WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(sfMessage)
		OR NOT HAS_SCALEFORM_MOVIE_LOADED(sfButtonHelp)
			WAIT(0)
		ENDWHILE
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> ...Scaleform loaded.")
		
		//Setup message scaleform.
		BEGIN_SCALEFORM_MOVIE_METHOD(sfMessage, "SHOW_CENTERED_MP_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("INSTALL_COMP")
		END_SCALEFORM_MOVIE_METHOD()
		
		//Setup instructional button scaleform.
		BEGIN_SCALEFORM_MOVIE_METHOD(sfButtonHelp, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(sfButtonHelp, "SET_DATA_SLOT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INSTRUCTIONAL_BUTTONS(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HUD_CONTINUE")
		END_SCALEFORM_MOVIE_METHOD()
		BEGIN_SCALEFORM_MOVIE_METHOD(sfButtonHelp, "DRAW_INSTRUCTIONAL_BUTTONS")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		END_SCALEFORM_MOVIE_METHOD()

		//Fade screen in.
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> Starting to wait for screen fade...")
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
		SHUTDOWN_LOADING_SCREEN()
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> ...Screen fade finished.")
		
		//Main loop. Draw message and button help. Listen for input.
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> Drawing screen and waiting for user input...")
		WHILE NOT bInstallScreenAccepted
			HIDE_LOADING_ON_FADE_THIS_FRAME()
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
			
			//Draw scaleform.
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfMessage, 255,255,255,0)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfButtonHelp, 255,255,255,0)
			
			//Check for "Continue" input.
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				bInstallScreenAccepted = TRUE
			ENDIF
			WAIT(0)
		ENDWHILE
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> ...User input received.")
		
		SET_NO_LOADING_SCREEN(FALSE)
		
		CPRINTLN(DEBUG_FLOW, "<FLOW-START> Starting autosave request.")
		SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE, TRUE)
		MAKE_AUTOSAVE_REQUEST()
		//Finished.
	ENDIF
	
	g_flowUnsaved.bFlowControllerBusy = FALSE
	
	SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_INSTALL_SCREEN_FINISHED)
	CPRINTLN(DEBUG_FLOW, "<FLOW-START> Autosave request finished.")

	CLEANUP_SCRIPT()
ENDSCRIPT
