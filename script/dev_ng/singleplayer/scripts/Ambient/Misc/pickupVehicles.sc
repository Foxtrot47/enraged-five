

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//╒═════════════════════════════════════════════════════════════════════════════╕
//│					  		Pickup Vehicle Controller							│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│		AUTHOR:			Adam Westwood											│
//│		DATE:			15/03/13												│
//│		DESCRIPTION: 	An ambient thread for adding pickup functionality		│
//│						to various vehicles in GTA V.							│									
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"
USING "flow_public_game.sch"

PROC Script_Cleanup()
	CPRINTLN(DEBUG_FLOW, "<VEH-PICKUP> Pickup vehicle controller terminated.")
	TERMINATE_THIS_THREAD()
ENDPROC

VEHICLE_INDEX vehPlayer
OBJECT_INDEX objClosestContainer
INT iPickUpCoolDown
BOOL bInitialiseCooldown
BOOL bPickedUp
//INT iTimeSinceLastHandlerCheck

FUNC BOOL MANAGE_MY_TIMER(INT&start_time, INT time) 
	INT current_time 
	current_time = GET_GAME_TIMER()
	
	IF ((current_time - start_time) > time) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT
	CPRINTLN(DEBUG_FLOW, "<VEH-PICKUP> Pickup vehicle controller started.")
	REGISTER_SCRIPT_TO_RELAUNCH_LIST(LAUNCH_BIT_FLOW_PICKUP_VEHICLES)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_DEBUG_MENU
			REMOVE_SCRIPT_FROM_RELAUNCH_LIST(LAUNCH_BIT_FLOW_PICKUP_VEHICLES)
		ENDIF
		Script_Cleanup()
	ENDIF

	WHILE TRUE
		WAIT(0)

		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_setup")) = 0
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ELSE
					vehPlayer = NULL
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(vehPlayer)
				IF IS_VEHICLE_DRIVEABLE(vehPlayer)
					IF IS_VEHICLE_MODEL(vehPlayer, HANDLER)
						SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT) // Needed for PC to prevent other controls interfering.
						IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehPlayer)
							IF bPickedUp = FALSE
								IF NOT DOES_ENTITY_EXIST(objClosestContainer)
								OR (DOES_ENTITY_EXIST(objClosestContainer) AND  GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(vehPlayer),15.0,PROP_CONTR_03B_LD) != objClosestContainer)
									objClosestContainer = GET_CLOSEST_OBJECT_OF_TYPE(GET_ENTITY_COORDS(vehPlayer),15.0,PROP_CONTR_03B_LD)
									//PRINTSTRING("GET_CLOSEST_OBJECT_OF_TYPE")PRINTNL()
								ENDIF
								IF DOES_ENTITY_EXIST(objClosestContainer)
									IF MANAGE_MY_TIMER(iPickUpCoolDown,1000)
										IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehPlayer,objClosestContainer)
											IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)  // Input context is used by the code side to drop containers, so needs to be the same in script.
												ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(vehPlayer,objClosestContainer)
												bInitialiseCooldown = TRUE
												bPickedUp = TRUE
											ENDIF
										ELSE
											//PRINTSTRING("HANDLER ISN'T LINED UP WITH CONTAINER")PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF bInitialiseCooldown = TRUE
								iPickUpCoolDown = GET_GAME_TIMER()
								bInitialiseCooldown = FALSE
								bPickedUp = FALSE
							ENDIF
							IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT) // Input context is used by the code side to drop containers, so needs to be the same in script.
								//DETACH_CONTAINER_FROM_HANDLER_FRAME(vehPlayer)
							ENDIF
						ENDIF
					ELSE
						//PRINTSTRING("VEHICLE MODEL IS NOT THE HANDLER")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDWHILE
ENDSCRIPT
