// Checks for player flying under bridges


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "brains.sch"

USING "script_player.sch"
USING "flow_public_core.sch"
USING "replay_public.sch"
USING "mission_stat_public.sch"
USING "commands_recording.sch"

USING "achievement_public.sch"
USING "flyUnderBridges.sch"


ENUM underBridgeStageFlag
	waitingForInteraction = 0,
	checkForExit
ENDENUM
underBridgeStageFlag underBridgeStage = waitingForInteraction

ENUM splashStageFlag
	loadScaleform,
	printSplash,
	animateSplash,
	cleanupSplash
ENDENUM

splashStageFlag splashStage = loadScaleform
BOOL bPrintSplash
SCALEFORM_INDEX sfMovie

INT currentBridge, iPrintedCompletion

BOOL bEnteredArea1, bEnteredArea2, bExitViaArea1, bExitViaArea2, bPassingThroughAreas, bIsKnife

VECTOR vTriggerPoint[SP_NUMBER_OF_CHALLENGES]
FLOAT fStartChecking = 200*200
VECTOR vArea1Min, vArea1Max
VECTOR vArea2Min, vArea2Max
FLOAT fAreaWidth

VECTOR vInput
#if IS_DEBUG_BUILD

BOOL bCheckBridgeCompletion, bBlipAllBridges, bAddBitFieldWidgets, bBitFieldWidgetsCreated
WIDGET_GROUP_ID widgetGroup
BLIP_INDEX blipBridge[SP_NUMBER_OF_CHALLENGES]
#ENDIF

PROC SETUP_BRIDGE_INFO()
	vTriggerPoint[SP_LA_RIVER_01]	= <<1083, -231, 60>>
	
	vTriggerPoint[SP_LA_RIVER_02]	= <<1024, -325, 60>>
	
	//Aqueduct Pipes
	vTriggerPoint[SP_LA_RIVER_03]	= <<910, -401, 43>>
	
	vTriggerPoint[SP_LA_RIVER_04]	= <<721, -457, 26>>
	
	vTriggerPoint[SP_LA_RIVER_05]	= <<643, -579, 26>>

	vTriggerPoint[SP_LA_RIVER_06]	= <<590, -851, 26>>
	
	vTriggerPoint[SP_LA_RIVER_07]	= <<590, -1023, 16>>
	
	vTriggerPoint[SP_LA_RIVER_08]	= <<582, -1205, 24>>
	
	//Aqueduct Rail Track
	vTriggerPoint[SP_LA_RIVER_09]	= <<608, -1335, 16>>
	
	vTriggerPoint[SP_LA_RIVER_10]	= <<640, -1434, 16>>
	
	vTriggerPoint[SP_LA_RIVER_11]	= <<671, -1742, 20>>
	
	vTriggerPoint[SP_LA_RIVER_12]	= <<651, -2046, 16>>
	
	vTriggerPoint[SP_LA_RIVER_13]	= <<603, -2505, 9>>
	
	vTriggerPoint[SP_LA_RIVER_14]	= <<673, -2582, 4>>
	
	vTriggerPoint[SP_LA_RIVER_15]	= <<728, -2594, 10>>
	
	vTriggerPoint[SP_LA_RIVER_16]	= <<794, -2624, 27>>
	
	vTriggerPoint[SP_HIGHWAY_CS3]		= <<-2663, 2594, 7.5>>
	
	vTriggerPoint[SP_DOUBLEARCH_CS3]	= <<-1902, 4617, 30>>
	
	vTriggerPoint[SP_RAILARCH_CS1]		= <<-513, 4427, 40>>
	 
	vTriggerPoint[SP_RAILLOW_CS4]		= <<126, 3366, 40>>

	vTriggerPoint[SP_ROADLOW_CS4]		= <<143, 3418, 36>>

	vTriggerPoint[SP_RAILLOW_CS2]		= <<2822, 4978, 40>>
	
	vTriggerPoint[SP_RAILLOW_CS3]		= <<-162, 4249, 40>>
	
	vTriggerPoint[SP_MADISON_CS3]		= <<-408, 2964, 20>>
	
	vTriggerPoint[SP_ROADLOW_CS6]		= <<-181, 2862, 38>>
	
	vTriggerPoint[SP_RAILLOW_CH3]		= <<2558, 2201, 24>>
	
	vTriggerPoint[SP_SEAARCH_CH3]		= <<2950, 803, 8>>
	
	vTriggerPoint[SP_RAILHIGH_CH3]		= <<2369, -409, 80>>
	
	vTriggerPoint[SP_RAILFREE_CH3]		= <<1906, -755, 84 >>
	
	vTriggerPoint[SP_ROADSUSP_PO1]		= <<-403, -2333, 40>>
	
	vTriggerPoint[SP_RAILLOW_CH1]		= <<-1429, 2649, 10>>
	
	vTriggerPoint[SP_RAILLOW_PO1]		= <<219, -2315, 5>>
	
	vTriggerPoint[SP_ROADLOW_PO1]		= <<350, -2315, 5>>
	
	vTriggerPoint[SP_KNIFE_SM01]		= <<-1848, -333, 75>>
	
	vTriggerPoint[SP_KNIFE_2]		= <<-693, -608, 69>>//<<-794, -595, 65>>
	
	vTriggerPoint[SP_KNIFE_3]		= <<-1461, -582, 53>>//<<-890, -1261, 40>>

	vTriggerPoint[SP_KNIFE_4]		= <<-1553, -546, 59>>//<<-969, -1304, 40>>
	
	//Chemical Vats
	vTriggerPoint[SP_DOMES_PO1]		= <<338, -2758, 23>>
	
	vTriggerPoint[SP_RAILROAD_CS2]		= <<1985, 6201, 53>>
	
	vTriggerPoint[SP_HELI_UNDER]		= <<-713, -1538, 13>>
	
	vTriggerPoint[SP_HELI_OVER]		= <<-659, -1518, 13>>
	
	vTriggerPoint[SP_CANAL_TRACK]		= <<-620, -1502, 16>>
	
	vTriggerPoint[SP_SPAGH_LOW]		= <<-445, -1575, 26>>
	
	vTriggerPoint[SP_CHINOZ_BRIDGE]		= <<-373, -1680, 19>>
	
	vTriggerPoint[SP_SC1_ARCH_BRIDGE]		= <<-212, -1805, 29>>
	
	vTriggerPoint[SP_LOW_CAST_IRON]		= <<47, -2040, 18>>
	
	vTriggerPoint[SP_CS_FREEWAY]			= <<-3080, 766, 25>>
	
	vTriggerPoint[SP_CH1_RIVER]			= <<-1478, 2400, 20>>

	vTriggerPoint[SP_CH3_OVERPASS]			= <<2308, 1124, 78>>
	
	vTriggerPoint[SP_CH3_RAILBRIDGE]		= <<2349, 1174, 79>>
	
	vTriggerPoint[SP_KNIFE_5]		= <<-1186, -365, 46>>

	vTriggerPoint[SP_KNIFE_6]		= <<-916, -407, 93>>
	
	vTriggerPoint[SP_KNIFE_7]		= <<-726, 235, 105>>
	
	vTriggerPoint[SP_KNIFE_8]		= <<-774, 286, 112>>

	vTriggerPoint[SP_KNIFE_9]		= <<271, 134, 125>>
	
	vTriggerPoint[SP_KNIFE_10]		= <<377, -28, 125>>
	
	vTriggerPoint[SP_KNIFE_11]		= <<121, -703, 150>>

	vTriggerPoint[SP_KNIFE_12]		= <<-204, -784, 74>>
	
	vTriggerPoint[SP_KNIFE_13]		= <<-287, -774, 72>>
	
	vTriggerPoint[SP_KNIFE_14]		= <<-272, -824, 71>>
		
	vTriggerPoint[SP_KNIFE_15]		= <<-230, -723, 80>>

	vTriggerPoint[SP_BRIDGE_47]	= <<1822, 2044, 62>>
	
	vTriggerPoint[SP_BRIDGE_48]	= <<2410, 2907, 44>>

	vTriggerPoint[SP_BRIDGE_49]	= <<2686, 4858, 36>>

 	vTriggerPoint[SP_BRIDGE_50]	= <<-1046, 4751, 244>>
	
	vTriggerPoint[SP_EXTRA_SUSP_1]	= <<-213, -2463, 38>>
	
	vTriggerPoint[SP_EXTRA_SUSP_2]	= <<-597, -2192, 38>>
	
	vTriggerPoint[SP_FREEWAY_1]	= <<1036, -980, 41>>
	
	vTriggerPoint[SP_FREEWAY_2]	= <<980, -837, 42>>
	
	//vTriggerPoint[SP_FREEWAY_3]	= <<-597, -2192, 38>>
	vTriggerPoint[SP_FREEWAY_3]	= <<1208, -1173, 38>>
	
	
ENDPROC

PROC GET_CURRENT_BRIDGE_INFO(INT bridgeIndex)
	SWITCH bridgeIndex
		CASE SP_LA_RIVER_01
			vArea1Min		= <<1103.013916,-233.037369,56.130039>>
			vArea1Max		= <<1073.190918,-214.847794,66.059296>>
			fAreaWidth		= 30
			vArea2Min 		= <<1093.588623,-248.592606,56.886391>>
			vArea2Max		= <<1063.774414,-230.142532,66.678467>>
		BREAK
		CASE SP_LA_RIVER_02
			vArea1Min		= <<1044.181885,-324.590363,49.334076>>
			vArea1Max		= <<1016.709717,-307.738251,64.813431>>
			fAreaWidth		= 30
			vArea2Min 		= <<1007.983154,-320.615875,48.454296>>
			vArea2Max		= <<1036.006714,-337.420410,64.480797>>
		BREAK
		CASE SP_LA_RIVER_03
			vArea1Min		= <<916.598999,-419.878204,35.627480>>
			vArea1Max		= <<910.379333,-383.882568,47.543388>>
			fAreaWidth		= 7
			vArea2Min 		= <<912.136169,-420.516144,35.380337>>
			vArea2Max		= <<906.895203,-384.677887,47.249256>>
		BREAK
		CASE SP_LA_RIVER_04
			vArea1Min		= <<757.718872,-472.923950,19.253498>>
			vArea1Max		= <<696.593628,-420.211456,35.460842>>
			fAreaWidth		= 20.75
			vArea2Min 		= <<744.911438,-480.737335,19.065138>>
			vArea2Max		= <<682.561401,-429.553345,37.026600>>
		BREAK
		CASE SP_LA_RIVER_05
			vArea1Min		= <<680.367676,-581.179199,14.214504>>
			vArea1Max		= <<599.810059,-528.305908,33.409580>>
			fAreaWidth		= 45.000000
			vArea2Min 		= <<667.369202,-610.535645,13.854013>>
			vArea2Max		= <<582.843262,-556.781799,33.403355>>
		BREAK
		CASE SP_LA_RIVER_06
			vArea1Min		= <<644.249695,-844.750427,12.367073>>
			vArea1Max		= <<526.860840,-845.252075,35.989601>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<644.365906,-859.387756,12.596766>>
			vArea2Max		= <<526.861511,-857.520752,36.328571>>
		BREAK
		CASE SP_LA_RIVER_07
			vArea1Min		= <<634.971985,-1011.640198,10.925943>>
			vArea1Max		= <<539.650085,-1024.017090,35.958515>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<634.961243,-1029.123047,10.618461>>
			vArea2Max		= <<543.489319,-1038.261475,35.959301>>
		BREAK
		CASE SP_LA_RIVER_08
			vArea1Min		= <<645.722290,-1191.215332,10.451977>>
			vArea1Max		= <<524.301758,-1197.166870,39.611725>>
			fAreaWidth	= 50.000000
			vArea2Min 		= <<645.722290,-1228.966431,10.980150>>
			vArea2Max		= <<521.937866,-1217.607300,39.471722>>
		BREAK
		CASE SP_LA_RIVER_09
			vArea1Min		= <<642.121643,-1295.730103,9.005976>> 
			vArea1Max		= <<568.170166,-1375.350830,20.129887>>
			fAreaWidth		= 7.000000
			vArea2Min 		= <<644.777222,-1298.168335,9.128529>>
			vArea2Max		= <<571.634155,-1378.644409,20.358023>>
		BREAK
		CASE SP_LA_RIVER_10
			vArea1Min		= <<686.567505,-1444.709839,9.065001>>
			vArea1Max		= <<598.932800,-1444.437988,25.688457>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<682.302673,-1429.871948,9.890836>> 
			vArea2Max		= <<593.821655,-1432.995483,25.600724>>
		BREAK
		CASE SP_LA_RIVER_11
			vArea1Min		= <<718.761658,-1734.312866,9.082874>>
			vArea1Max		= <<615.001709,-1725.897339,27.545851>>
			fAreaWidth		= 30.000000
			vArea2Min 		= <<717.535461,-1748.645630,9.363478>>
			vArea2Max		= <<614.217957,-1734.847534,27.357079>>
		BREAK
		CASE SP_LA_RIVER_12
			vArea1Min		= <<694.316467,-2049.805664,0.009695>>
			vArea1Max		= <<618.684509,-2040.013672,25.834118>>
			fAreaWidth		= 30.000000
			vArea2Min 		= <<693.183594,-2063.225098,0.429037>> 
			vArea2Max		= <<607.594360,-2055.326416,26.918158>>
		BREAK
		CASE SP_LA_RIVER_13
			vArea1Min		= <<642.667053,-2494.551270,0.468485>>
			vArea1Max		= <<570.180664,-2513.958740,11.787938>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<647.033875,-2506.202148,0.583701>> 
			vArea2Max		= <<571.141541,-2522.975342,10.450453>>
		BREAK
		CASE SP_LA_RIVER_14
			vArea1Min		= <<691.823547,-2558.218506,0.363352>>
			vArea1Max		= <<645.886292,-2600.311279,9.898791>>
			fAreaWidth		= 10.250000
			vArea2Min 		= <<695.792847,-2561.033691,0.346731>>
			vArea2Max		= <<656.391907,-2601.971680,9.643657>>
		BREAK
		CASE SP_LA_RIVER_15
			vArea1Min		= <<723.625427,-2562.170654,0.255647>>
			vArea1Max		= <<720.751953,-2619.769531,15.732105>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<735.764954,-2561.935059,0.311182>> 
			vArea2Max		= <<736.121399,-2618.766602,15.790609>>
		BREAK
		CASE SP_LA_RIVER_16
			vArea1Min		= <<891.438660,-2603.120117,0>> 
			vArea1Max		= <<704.493164,-2634.793213,45>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<893.257813,-2616.235107,0>> 
			vArea2Max		= <<707.726135,-2647.695557,45>>
		BREAK
		CASE SP_HIGHWAY_CS3
			vArea1Min			= <<-2669.586914,2491.959961,2.043799>>
			vArea1Max			= <<-2617.764648,2841.594727,14.082197>>
			fAreaWidth			= 26.500000
			vArea2Min 			= <<-2687.605713,2494.868164,2.608733>>
			vArea2Max			= <<-2637.084717,2846.875244,14.159884>>
		BREAK
		CASE SP_DOUBLEARCH_CS3
			vArea1Min		= <<-1986.172607,4521.798828,0>>
			vArea1Max		= <<-1809.902588,4699.551270,53.508797>>
			fAreaWidth		= 17.000000
			vArea2Min 		= <<-1995.668091,4531.259277,0>>
			vArea2Max		= <<-1817.543457,4708.394531,53.509171>>
		BREAK
		CASE SP_RAILARCH_CS1
			vArea1Min			= <<-526.026489,4472.442383,0>>
			vArea1Max			= <<-505.571442,4335.724609,86.733109>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-519.928101,4476.345703,0>>
			vArea2Max			= <<-500.731323,4336.389160,86.712891>>
		BREAK
		CASE SP_RAILLOW_CS4
			vArea1Min			= <<98.161499,3384.489014,45.451691>>
			vArea1Max			= <<154.974030,3350.694092,30.033585>>
			fAreaWidth			= 8.000000
			vArea2Min 			= <<152.680176,3346.793457,45.021557>>
			vArea2Max			= <<95.571877,3380.090576,30.432842>> 
		BREAK
		CASE SP_ROADLOW_CS4
			vArea1Min			= <<147.860611,3406.796143,38.036716>>
			vArea1Max			= <<126.132935,3416.926758,30.029865>>
			fAreaWidth			= 14.50000
			vArea2Min 			= <<130.091629,3425.417236,38.056725>>
			vArea2Max			= <<151.870346,3415.391357,30.057804>>
		BREAK
		CASE SP_RAILLOW_CS2
			vArea1Min			= <<2830.972168,4967.139648,34.560127>>
			vArea1Max			= <<2818.718750,4992.298340,51.290901>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<2826.766846,4964.185059,34.106361>>
			vArea2Max			= <<2814.216309,4989.983398,51.218491>>
		BREAK
		CASE SP_RAILLOW_CS3
			vArea1Min			= <<-151.576416,4264.416992,31.047348>>
			vArea1Max			= <<-193.196228,4224.604004,43.872551>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-148.384186,4261.071289,31.574089>>
			vArea2Max			= <<-190.471878,4222.076172,43.954426>>
		BREAK
		CASE SP_MADISON_CS3
			vArea1Min			= <<-426.691864,2964.271973,14.848002>>
			vArea1Max			= <<-396.229797,2959.277588,23.506374>>
			fAreaWidth			= 7.000000
			vArea2Min 			= <<-425.028290,2967.861328,15.226991>>
			vArea2Max			= <<-395.607269,2962.606689,24.380791>>
		BREAK
		CASE SP_ROADLOW_CS6
			vArea1Min			= <<-192.341370,2864.916260,30.725950>>
			vArea1Max			= <<-170.150940,2857.128174,43.941822>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-192.012863,2871.602539,29.999426>>
			vArea2Max			= <<-169.595749,2863.838379,44.032505>>
		BREAK
		CASE SP_RAILLOW_CH3
			vArea1Min			= <<2539.185059,2228.771729,18.610205>>
			vArea1Max			= <<2574.373291,2169.401367,27.265978>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<2543.707520,2231.401611,18.331003>>
			vArea2Max			= <<2578.078857,2171.583740,27.260571>>
		BREAK
		CASE SP_SEAARCH_CH3
			vArea1Min			= <<2954.086670,815.720886,0.037901>>
			vArea1Max			= <<2933.189209,796.468811,12.983917>>
			fAreaWidth			= 35.000000
			vArea2Min 			= <<2966.122559,806.888855,0.544056>> 
			vArea2Max			= <<2950.801270,786.781555,11.745959>>
		BREAK
		CASE SP_RAILHIGH_CH3
			vArea1Min			= <<2329.672607,-459.664764,70.242767>>
			vArea1Max			= <<2413.383789,-361.218842,91.778862>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<2324.751953,-455.523773,70.251450>>
			vArea2Max			= <<2407.409424,-356.200317,91.430832>>
		BREAK
		CASE SP_RAILFREE_CH3
			vArea1Min			= <<1943.428467,-753.250977,80.179047>>
			vArea1Max			= <<1850.364502,-760.958740,93.025215>>
			fAreaWidth			= 7.000000
			vArea2Min 			= <<1943.365967,-758.287048,80.322914>>
			vArea2Max			= <<1851.353516,-765.180664,92.935455>>
		BREAK
		CASE SP_ROADSUSP_PO1
			vArea1Min			= <<-655.146729,-2138.093506,-0.339008>>
			vArea1Max			= <<-146.027893,-2493.724365,54.675671>>
			fAreaWidth			= 47.750000
			vArea2Min 			= <<-672.221313,-2162.674561,-0.082912>>
			vArea2Max			= <<-163.555618,-2519.058105,54.722488>>
		BREAK
		CASE SP_RAILLOW_CH1
			vArea1Min			= <<-1483.000366,2691.427734,-10>>
			vArea1Max			= <<-1377.168213,2600.768799,15.955276>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<-1478.152100,2696.687988,-10>>
			vArea2Max			= <<-1378.503174,2608.697754,15.609236>>
		BREAK
		CASE SP_RAILLOW_PO1
			vArea1Min			= <<222.151871,-2343.486572,0.039199>>
			vArea1Max			= <<222.684906,-2297.406982,7.088753>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<216.959015,-2343.486572,0.207734>>
			vArea2Max			= <<216.602036,-2295.445068,7.424279>>
		BREAK
		CASE SP_ROADLOW_PO1
			vArea1Min			= <<346.462158,-2244.374023,0.159779>>
			vArea1Max			= <<346.834717,-2389.590576,7.852059>>
			fAreaWidth			= 20.000000
			vArea2Min 			= <<359.609039,-2244.467773,0.129684>>
			vArea2Max			= <<355.405426,-2390.258301,7.080691>>
		BREAK
		CASE SP_KNIFE_SM01
			vArea1Min			= <<-1859.679932,-322.635742,56.163681>>
			vArea1Max			= <<-1836.613647,-335.414124,96.116104>>
			fAreaWidth			= 7.500000
			vArea2Min 			= <<-1860.269775,-327.863434,57.542999>>
			vArea2Max			= <<-1837.270874,-339.222717,95.693253>>
		BREAK
		CASE SP_KNIFE_2
			vArea1Min			= <<-680.263245,-600.817993,69.112892>>
			vArea1Max			= <<-706.661255,-600.751526,30.476036>>
			fAreaWidth			= 31.500000
			vArea2Min 			= <<-680.607727,-618.365845,69.274963>>
			vArea2Max			= <<-706.359619,-618.238464,30.312347>>
		BREAK
		CASE SP_KNIFE_3
			vArea1Min			= <<-1468.096069,-591.715759,67.055176>>
			vArea1Max			= <<-1454.699951,-573.451782,29.567360>>
			fAreaWidth			= 11.750000
			vArea2Min 			= <<-1474.902954,-591.121460,67.080910>>
			vArea2Max			= <<-1457.173096,-568.131592,29.440590>>
		BREAK
		CASE SP_KNIFE_4
			vArea1Min			= <<-1544.957764,-537.147522,72.443474>>
			vArea1Max			= <<-1564.616211,-551.182922,32.861633>>
			fAreaWidth			= 11.750000
			vArea2Min 			= <<-1541.007935,-541.549377,71.619720>>
			vArea2Max			= <<-1561.218872,-555.868042,32.927902>> 
		BREAK
		CASE SP_DOMES_PO1
			vArea1Min			= <<333.210785,-2727.273682,20.716625>>
			vArea1Max			= <<333.429688,-2791.608887,41.990227>>
			fAreaWidth			= 20.000000
			vArea2Min 			= <<343.112732,-2727.235840,20.236126>>
			vArea2Max			= <<343.667816,-2791.602295,41.379284>>
		BREAK
		CASE SP_RAILROAD_CS2
			vArea1Min			= <<1928.071289,6228.355469,43.493977>>
			vArea1Max			= <<2039.882324,6167.397461,55.464050>> 
			fAreaWidth			= 13.000000
			vArea2Min 			= <<1931.820190,6235.633789,43.373817>>
			vArea2Max			= <<2039.597534,6176.525391,55.255970>> 
		BREAK
		CASE SP_HELI_UNDER
			vArea1Min			= <<-736.430908,-1590.920776,10.808919>>
			vArea1Max			= <<-710.811035,-1516.349487,-0.098598>> 
			fAreaWidth			= 15.000000
			vArea2Min 			= <<-727.230713,-1585.221191,11.780270>>
			vArea2Max			= <<-700.020142,-1511.782593,-0.341655>> 
			BREAK
		CASE SP_HELI_OVER
			vArea1Min			= <<-686.377502,-1548.552612,12.337475>>
			vArea1Max			= <<-669.328979,-1507.062866,-0.788618>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-654.203003,-1536.145996,9.191055>>
			vArea2Max			= <<-645.995422,-1500.219360,-2.406948>> 
		BREAK
		CASE SP_CANAL_TRACK
			vArea1Min			= <<-624.234375,-1537.045288,12.601933>>
			vArea1Max			= <<-622.174927,-1472.876587,-0.292606>> 
			fAreaWidth			= 8.000000
			vArea2Min 			= <<-615.400330,-1536.650024,12.402705>>
			vArea2Max			= <<-619.738525,-1472.937134,-0.243267>> 
		BREAK
		CASE SP_SPAGH_LOW
			vArea1Min			= <<-492.505707,-1632.457153,24.330700>>
			vArea1Max			= <<-418.208801,-1487.452148,0>>  
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-486.201599,-1636.094971,24.208052>>
			vArea2Max			= <<-398.764832,-1490.440308,0>>  
		BREAK
		CASE SP_CHINOZ_BRIDGE
			vArea1Min			= <<-359.354095,-1639.692749,13.134555>>
			vArea1Max			= <<-388.495483,-1690.945190,-0.183164>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-378.151825,-1705.659668,12.471957>>
			vArea2Max			= <<-348.959106,-1654.411011,0.193478>> 
		BREAK
		CASE SP_SC1_ARCH_BRIDGE
			vArea1Min			= <<-243.443573,-1814.622803,25.694649>>
			vArea1Max			= <<-183.998734,-1780.644653,-0.085802>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-235.131912,-1822.093750,25.865416>>
			vArea2Max			= <<-175.510468,-1788.274780,-0.506062>>
		BREAK
		CASE SP_LOW_CAST_IRON
			vArea1Min		=  <<84.555367,-2046.158813,13.307674>>
			vArea1Max		=  <<17.931641,-2045.151855,-0.031946>> 
			fAreaWidth		=  25.000000
			vArea2Min 		=  <<17.907875,-2035.772949,12.627057>>
			vArea2Max		=  <<84.572067,-2034.183838,0.048385>> 
		BREAK
		CASE SP_CS_FREEWAY
			vArea1Min		= <<-3064.972656,780.167725,18.706421>>
			vArea1Max		= <<-3093.957520,757.288635,29.196960>> 
			fAreaWidth		= 5.000000
			vArea2Min 		= <<-3063.053711,778.016541,18.671673>>
			vArea2Max		= <<-3092.174561,754.915588,29.145802>>
		BREAK
		CASE SP_CH1_RIVER
			vArea1Min		= <<-1471.516602,2406.815186,2.485338>>
			vArea1Max		= <<-1489.422119,2404.390381,21.769384>> 
			fAreaWidth		= 15.000000
			vArea2Min 		= <<-1468.889404,2400.668213,2.603960>>
			vArea2Max		= <<-1487.055054,2398.087402,21.837677>>
		BREAK
		CASE SP_CH3_OVERPASS
			vArea1Min		= <<2326.570068,1096.030518,76.314575>>
			vArea1Max		= <<2290.332031,1136.130737,58.857056>> 
			fAreaWidth		= 21.000000
			vArea2Min 		= <<2334.453369,1103.066772,76.266029>>
			vArea2Max		= <<2297.845703,1142.896851,58.842430>>
		BREAK
		CASE SP_CH3_RAILBRIDGE
			vArea1Min		= <<2379.442139,1150.775513,58.796318>>
			vArea1Max		= <<2327.657959,1212.366333,72.833298>> 
			fAreaWidth		= 12.000000
			vArea2Min 		= <<2374.063721,1146.282104,58.833305>>
			vArea2Max		= <<2320.894531,1209.596069,72.792992>>
		BREAK
		CASE SP_KNIFE_5
			vArea1Min		= <<-1179.405151,-355.255432,56.530029>>
			vArea1Max		= <<-1198.064087,-357.836304,35.355511>>
			fAreaWidth		= 12.500000
			vArea2Min 		= <<-1178.385254,-361.878418,56.150814>>
			vArea2Max		= <<-1197.103760,-364.700439,36.494755>> 
		BREAK
		CASE SP_KNIFE_6
			vArea1Min		= <<-921.384583,-384.939972,137.018127>>
			vArea1Max		= <<-912.432373,-429.228973,36.701126>> 
			fAreaWidth		= 16.000000
			vArea2Min 		= <<-914.165771,-387.944366,137.079361>>
			vArea2Max		= <<-906.253357,-424.691040,47.118820>>  
		BREAK
		CASE SP_KNIFE_7
			vArea1Min		= <<-740.256409,246.452850,132.292191>>
			vArea1Max		= <<-718.360229,201.004150,80.955711>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-726.642883,253.067642,132.331940>>
			vArea2Max		= <<-705.585815,210.433563,78.705727>>
		BREAK
		CASE SP_KNIFE_8
			vArea1Min		= <<-771.206787,268.272888,132.168915>>
			vArea1Max		= <<-778.341675,313.114807,84.270538>> 
			fAreaWidth		= 16.000000
			vArea2Min 		= <<-770.803528,310.862518,137.416138>>
			vArea2Max		= <<-763.068054,269.044037,83.314743>> 
		BREAK
		CASE SP_KNIFE_9
			vArea1Min		= <<259.220520,135.414612,136.708267>>
			vArea1Max		= <<279.239624,128.137939,100.823303>>
			fAreaWidth		= 16.000000
			vArea2Min 		= <<261.969360,142.967636,136.688919>>
			vArea2Max		= <<281.720337,134.955078,100.770416>>
		BREAK
		CASE SP_KNIFE_10
			vArea1Min		= <<393.547974,-30.871658,152.663452>>
			vArea1Max		= <<369.962189,-23.884607,88.357758>> 
			fAreaWidth		= 8.000000
			vArea2Min 		= <<390.535797,-36.088818,152.781250>>
			vArea2Max		= <<368.127472,-28.818884,88.694473>> 
		BREAK
		CASE SP_KNIFE_11
			vArea1Min		= <<114.313911,-648.429749,261.848785>>
			vArea1Max		= <<131.078156,-733.768372,39.343933>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<124.846703,-646.657532,261.848785>>
			vArea2Max		= <<140.050247,-737.427002,39.349304>> 
		BREAK
		CASE SP_KNIFE_12
			vArea1Min		= <<-215.918991,-823.843628,126.022392>>
			vArea1Max		= <<-193.223679,-761.778137,27.913818>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-225.396957,-820.393738,126.081223>>
			vArea2Max		= <<-202.943253,-758.257019,27.477341>>
		BREAK
		CASE SP_KNIFE_13
			vArea1Min		= <<-296.472504,-802.081543,95.401085>>
			vArea1Max		= <<-278.135193,-747.784119,50.400459>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-305.460236,-798.836914,95.481941>>
			vArea2Max		= <<-285.737640,-745.095886,49.576508>>
		BREAK
		CASE SP_KNIFE_14
			vArea1Min		= <<-292.303436,-823.356873,95.376205>>
			vArea1Max		= <<-258.599060,-835.563232,27.979462>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-288.920624,-814.022034,95.375565>>
			vArea2Max		= <<-255.211594,-826.256042,27.737495>>
		BREAK
		CASE SP_KNIFE_15
			vArea1Min		= <<-256.358856,-714.783752,110.161659>>
			vArea1Max		= <<-212.905441,-730.532043,32.219460>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-253.772324,-705.663208,110.173569>>
			vArea2Max		= <<-210.058777,-722.674805,32.465363>>
		BREAK
		CASE SP_BRIDGE_47
			vArea1Min		= <<1808.214478,1949.245850,71.737068>>
			vArea1Max		= <<1837.906250,2127.283203,52.804909>>
			fAreaWidth		= 9.750000
			vArea2Min 		= <<1802.785889,1950.261719,71.740021>>
			vArea2Max		= <<1831.994385,2127.432617,52.838928>>
		BREAK
		CASE SP_BRIDGE_48
			vArea1Min		= <<2388.732910,2931.940918,46.626808>>
			vArea1Max		= <<2426.680664,2883.066162,36.215237>>
			fAreaWidth		= 10.000000
			vArea2Min 		= <<2392.546631,2934.867188,46.626797>>
			vArea2Max		= <<2430.332520,2885.908447,36.281479>>
		BREAK
		CASE SP_BRIDGE_49
			vArea1Min		= <<2700.055664,4836.380859,42.078537>>
			vArea1Max		= <<2685.673340,4893.380371,30.908669>>
			fAreaWidth		= 20.750000
			vArea2Min		= <<2685.671875,4821.652832,42.184708>>
			vArea2Max		= <<2672.812256,4880.356445,31.133106>>
		BREAK
		CASE SP_BRIDGE_50
			vArea1Min		= <<-1053.446411,4766.245117,234.325119>>
			vArea1Max		= <<-1040.263428,4737.156738,204.491638>>
			fAreaWidth		= 5.000000
			vArea2Min 		= <<-1051.414307,4767.192871,234.429306>>
			vArea2Max		= <<-1037.954224,4738.354492,204.528152>>
		BREAK
		CASE SP_FREEWAY_1
			vArea1Min		= <<1001.145081,-987.113770,42.624561>>
			vArea1Max		= <<1078.645020,-963.743530,28.933376>> 
			fAreaWidth		= 14.000000
			vArea2Min 		= <<1002.864197,-992.898621,42.624561>>
			vArea2Max		= <<1080.483643,-969.903381,28.883770>> 
		BREAK
		CASE SP_FREEWAY_2
			vArea1Min		= <<952.504150,-847.661499,43.018440>>
			vArea1Max		= <<1021.093079,-844.506287,29.719673>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<945.549438,-835.977661,43.156578>>
			vArea2Max		= <<1016.363892,-835.386108,29.729702>> 
		BREAK
		CASE SP_FREEWAY_3
			vArea1Min		= <<1212.476685,-1183.012573,46.473896>>
			vArea1Max		= <<1267.188721,-1161.996704,32.482971>> 
			fAreaWidth		= 40.000000
			vArea2Min 		= <<1206.009644,-1157.062988,47.936985>>
			vArea2Max		= <<1258.374023,-1140.679932,32.429031>> 
		BREAK
	ENDSWITCH
ENDPROC

PROC RESET_BRIDGE_CHECKS()
	bEnteredArea1 			= FALSE
	bEnteredArea2 			= FALSE
	bExitViaArea1 			= FALSE
	bExitViaArea2			= FALSE
	bPassingThroughAreas	= FALSE
	underBridgeStage = waitingForInteraction
ENDPROC


FUNC INT CHECK_BRIDGE_COMPLETION()
	INT completeCount, completeKnives, completeBridges
	
	completeBridges	= GET_NUMBER_OF_COMPLETED_UNDER_BRIDGES()
	completeKnives = GET_NUMBER_OF_COMPLETED_KNIFE_FLIGHTS()
	
	//Update achievement progress
	IF (completeBridges+completeKnives)>0
		SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH29),completeBridges+completeKnives)
	ENDIF
	
	IF bIsKnife
		completeCount = completeKnives
		IF completeKnives >= 8 //NUMBER_OF_KNIVES/2
			PRINTLN("<flyUnderBridges> - At least half of all knives are complete, award achievement.")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_KNFFLT)
		ELSE
			PRINTLN("<flyUnderBridges> - ", completeKnives, " knives are complete, no achievement yet.")
		ENDIF
		

	ELSE
		completeCount = completeBridges
		IF completeBridges >= 50
			PRESENCE_EVENT_UPDATESTAT_INT(SP0_WATER_CANNON_DEATHS, 100)
			PRINTLN("<flyUnderBridges> - PRESENCE_EVENT_UPDATESTAT_INT 100")
		ELIF completeBridges >= 38
			PRESENCE_EVENT_UPDATESTAT_INT(SP0_WATER_CANNON_DEATHS, 75)
			PRINTLN("<flyUnderBridges> - PRESENCE_EVENT_UPDATESTAT_INT 75")
		ELIF completeBridges >= 25
			PRINTLN("<flyUnderBridges> - At least half of all bridges are complete, award percentage.")
			REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL (CP_UNDBRG)
			PRESENCE_EVENT_UPDATESTAT_INT(SP0_WATER_CANNON_DEATHS, 50)
			PRINTLN("<flyUnderBridges> - PRESENCE_EVENT_UPDATESTAT_INT 50")
		ELIF completeBridges >= 13
			PRESENCE_EVENT_UPDATESTAT_INT(SP0_WATER_CANNON_DEATHS, 25)
			PRINTLN("<flyUnderBridges> - PRESENCE_EVENT_UPDATESTAT_INT 25")
		ENDIF
		
	ENDIF
	
	CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT]: ACH29 - Close Shave - :", completeKnives + completeBridges, "of", SP_NUMBER_OF_KNIVES + SP_NUMBER_OF_BRIDGES)
	IF (completeKnives = SP_NUMBER_OF_KNIVES) AND (completeBridges = SP_NUMBER_OF_BRIDGES)
		AWARD_ACHIEVEMENT(ACH29) // close shave
	ENDIF
	
	RETURN completeCount
ENDFUNC

PROC DRAW_FLY_UNDER_SPLASH()
	IF NOT IS_RESULT_SCREEN_DISPLAYING()
		IF HAS_SCALEFORM_MOVIE_LOADED(sfMovie)
			DRAW_SCALEFORM_MOVIE(sfMovie,0.5, 0.5, 1.0, 1.0, 100, 100, 100, 255 )
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_SCALEFORM()
	IF HAS_SCALEFORM_MOVIE_LOADED(sfMovie)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMovie)
	ENDIF
ENDPROC

BOOL bShouldTerminate = FALSE

PROC GET_CURRENT_BRIDGE()
	INT i
	
	REPEAT SP_NUMBER_OF_CHALLENGES i
		IF ARE_VECTORS_ALMOST_EQUAL(vInput, vTriggerPoint[i])
			IF i = SP_EXTRA_SUSP_1
			OR i = SP_EXTRA_SUSP_2
				PRINTLN("<flyUnderBridges> - Bridge is ROADSUSP_PO1")
				fStartChecking = 700*700
				currentBridge = SP_ROADSUSP_PO1
			ELSE
				currentBridge = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF currentBridge < 32
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, currentBridge)
			bShouldTerminate = TRUE
		ENDIF
	ELIF currentBridge < 64
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, (currentBridge - 32))
			bShouldTerminate = TRUE
		ENDIF
	ELSE 
		IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, (currentBridge - 64))
			bShouldTerminate = TRUE
		ENDIF
	ENDIF
	
	IF bShouldTerminate
		PRINTLN("<flyUnderBridges> - Bridge ", currentBridge, " has already been completed.")
		//TERMINATE_THIS_THREAD()	
	ELSE
		GET_CURRENT_BRIDGE_INFO(currentBridge)
	ENDIF
	
ENDPROC


PROC PRINT_SPLASH()
	//INT iSplashSoundID
	SWITCH splashStage
		CASE loadScaleform
			IF bPrintSplash
			AND NOT bShouldTerminate
				IF NOT IS_RESULT_SCREEN_DISPLAYING()
					sfMovie = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
					WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(sfMovie))
					WAIT(0)
					ENDWHILE
					BEGIN_SCALEFORM_MOVIE_METHOD(sfMovie, "SHOW_BRIDGES_KNIVES_PROGRESS")
					
						IF bIsKnife 
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FU_KNIFE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SP_NUMBER_OF_KNIVES)
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FU_TITLE")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(SP_NUMBER_OF_BRIDGES)
						ENDIF
						//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FU_PASS")
						IF bIsKnife 
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FU_CHALL_KN")
						ELSE
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FU_CHALLENGE")	
						ENDIF
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CHECK_BRIDGE_COMPLETION())						
					END_SCALEFORM_MOVIE_METHOD()
					iPrintedCompletion = CHECK_BRIDGE_COMPLETION()
					g_bCompletedBridgesCount = iPrintedCompletion
					//iSplashSoundID = GET_SOUND_ID()
					//PLAY_SOUND_FRONTEND(iSplashSoundID, "CHECKPOINT_UNDER_THE_BRIDGE", "HUD_MINI_GAME_SOUNDSET")
					SETTIMERB(0)
					PLAY_SOUND_FRONTEND( -1,"UNDER_THE_BRIDGE", "HUD_AWARDS")
				ENDIF
				splashStage = printSplash
			ENDIF
		BREAK
		CASE printSplash
			IF TIMERB() > DEFAULT_GOD_TEXT_TIME/2
			OR IS_SCREEN_FADED_OUT()
			OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_RESULT_SCREEN_DISPLAYING()
			OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR IS_REPLAY_BEING_PROCESSED()
			OR IS_MINIGAME_SPLASH_SHOWING() 
				BEGIN_SCALEFORM_MOVIE_METHOD(sfMovie, "SHARD_ANIM_OUT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE)) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
				END_SCALEFORM_MOVIE_METHOD()
				SETTIMERB(0)
				splashStage = animateSplash
			ELSE
				DRAW_FLY_UNDER_SPLASH()
				IF g_bCompletedBridgesCount > iPrintedCompletion
					splashStage = cleanupSplash
				ENDIF
			ENDIF
		BREAK
		CASE animateSplash
			IF TIMERB() > 500
			OR IS_SCREEN_FADED_OUT()
			OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_RESULT_SCREEN_DISPLAYING()
			OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR IS_REPLAY_BEING_PROCESSED()
			OR IS_MINIGAME_SPLASH_SHOWING() 
				splashStage = cleanupSplash
			ELSE
				DRAW_FLY_UNDER_SPLASH()
				IF g_bCompletedBridgesCount > iPrintedCompletion
					splashStage = cleanupSplash
				ENDIF
			ENDIF
		BREAK
		CASE cleanupSplash
			
			CLEANUP_SCALEFORM()
			bPrintSplash = FALSE
			GET_CURRENT_BRIDGE()
			bShouldTerminate = TRUE
			splashStage = loadScaleform
		BREAK
	ENDSWITCH
ENDPROC

PROC STORE_BRIDGE_AS_COMPLETE()
	//Check if the current bridge has already been completed
	BOOL bAlreadySet
	IF currentBridge < 32
		bAlreadySet= IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, currentBridge)
	ELIF currentBridge < 64
		bAlreadySet = IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, (currentBridge - 32))
	ELSE
		bAlreadySet = IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, (currentBridge - 64))
	ENDIF
	
	IF bAlreadySet
		//Exit as this stat has already been incremented
		CPRINTLN(debug_ambient,"<flyUnderBridges> - This bridge has already been completed, skipping stat increase")
	ELSE
		CDEBUG1LN(debug_ambient,"<flyUnderBridges> - Storing bridge complete for bridge ",currentBridge)
		IF NOT bShouldTerminate
			IF bIsKnife
				STAT_INCREMENT(SP_KNIFE_FLIGHTS_COUNT,1.0)
			ELSE
				STAT_INCREMENT(SP_UNDER_THE_BRIDGE_COUNT,1.0)
			ENDIF
			INCREMENT_PLAYER_PED_STAT(GET_CURRENT_PLAYER_PED_ENUM(), PS_FLYING_ABILITY, 3)
			MAKE_AUTOSAVE_REQUEST()
		ENDIF
		IF currentBridge < 32
			SET_BIT(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, currentBridge)
		ELIF currentBridge < 64
			SET_BIT(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, (currentBridge - 32))
		ELSE
			SET_BIT(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, (currentBridge - 64))
		ENDIF
		g_bUnderTheBridgeTriggered = TRUE
		CHECK_BRIDGE_COMPLETION()
		bPrintSplash = TRUE
	ENDIF 
		
	#IF IS_DEBUG_BUILD
		IF DOES_BLIP_EXIST(blipBridge[currentBridge])
			SET_BLIP_COLOUR(blipBridge[currentBridge], BLIP_COLOUR_GREEN)
		ENDIF
	#ENDIF
ENDPROC

INT iTerminateTimer

PROC TERMINATE_IF_ALREADY_COMPLETE()
	IF GET_GAME_TIMER() - iTerminateTimer > 500
		IF currentBridge = SP_ROADSUSP_PO1
			IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, currentBridge)
				bShouldTerminate = TRUE
			ENDIF
		ENDIF
		//CDEBUG3LN(DEBUG_MISSION, "<flyUnderBridges> Terminate check") //Commenting out to prevent constant debug spam.
		iTerminateTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC

PROC SETUP_DEBUG()
	#if IS_DEBUG_BUILD
		widgetGroup = START_WIDGET_GROUP("flyUnderBridges")
			ADD_WIDGET_BOOL("Add bit field widgets", bAddBitFieldWidgets)
			ADD_WIDGET_BOOL("Recheck bridge completion", bCheckBridgeCompletion)
			ADD_WIDGET_BOOL("Blip all bridges", bBlipAllBridges)
			ADD_WIDGET_BOOL("Test splash", bPrintSplash)
		STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC 

PROC RUN_DEBUG()
	#if IS_DEBUG_BUILD
	INT iTemp
	TEXT_LABEL_23 blipName, blipNumberString
		IF bAddBitFieldWidgets
			IF NOT bBitFieldWidgetsCreated
				SET_CURRENT_WIDGET_GROUP(widgetGroup)
				ADD_BIT_FIELD_WIDGET("Bridges Flown Under", g_savedGlobals.sAmbient.iBridgesFlownUnderFlags)
				ADD_BIT_FIELD_WIDGET("Bridges Flown Under (Continued)", g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2)
				ADD_BIT_FIELD_WIDGET("Bridges Flown Under (Continued Again)", g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3)
				CLEAR_CURRENT_WIDGET_GROUP(widgetGroup)
				bBitFieldWidgetsCreated = TRUE
			ENDIF
			bAddBitFieldWidgets = FALSE
		ENDIF
		IF bCheckBridgeCompletion
			CHECK_BRIDGE_COMPLETION()	
			bCheckBridgeCompletion = FALSE
		ENDIF
		IF bBlipAllBridges
			REPEAT SP_NUMBER_OF_CHALLENGES iTemp
				IF DOES_BLIP_EXIST(blipBridge[iTemp])
					REMOVE_BLIP(blipBridge[iTemp])
				ELSE
					blipBridge[iTemp] = ADD_BLIP_FOR_COORD(vTriggerPoint[iTemp])
					blipName = "Bridge "
					blipNumberString = GET_STRING_FROM_INT(iTemp)
					blipName += blipNumberString
					
					SET_BLIP_NAME_FROM_ASCII(blipBridge[iTemp], blipName)
					IF iTemp < 32
						IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, iTemp)
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_GREEN)
						ELSE
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_RED)
						ENDIF
					ELIF iTemp < 64
						IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, (iTemp - 32))
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_GREEN)
						ELSE
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_RED)
						ENDIF
					ELSE 
						IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, (iTemp - 64))
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_GREEN)
						ELSE
							SET_BLIP_COLOUR(blipBridge[iTemp], BLIP_COLOUR_RED)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			bBlipAllBridges = FALSE
		ENDIF
	#ENDIF
ENDPROC 

FUNC BOOL DOES_PLAYER_MEET_FLIGHT_REQUIREMENTS()
	IF bIsKnife
		IF IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			IF GET_ENTITY_UPRIGHT_VALUE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > -0.6
			AND GET_ENTITY_UPRIGHT_VALUE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) < 0.6
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOES_PLAYER_MEET_FLIGHT_REQUIREMENTS()")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC




SCRIPT (coords_struct in_coords)
vInput = in_coords.vec_coord[0]
PRINTLN("<flyUnderBridges> - Launched at ", vInput)
IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO)
	TERMINATE_THIS_THREAD()	
ENDIF
IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL) //B* - 2061273
OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
OR IS_REPEAT_PLAY_ACTIVE()		//B* 2049712 - 'ding' in the plane scene
	TERMINATE_THIS_THREAD()
ENDIF 


CHECK_BRIDGE_COMPLETION()
SETUP_BRIDGE_INFO()
GET_CURRENT_BRIDGE()
SETUP_DEBUG()


PRINTLN("<flyUnderBridges> - Launched and checking bridge number ", currentBridge)

WHILE TRUE
WAIT(0)
	RUN_DEBUG() 
	PRINT_SPLASH()	
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			TERMINATE_IF_ALREADY_COMPLETE()
			SWITCH underBridgeStage
			
				CASE waitingForInteraction
//					IF TIMERA() > (1000/NUMBER_OF_BRIDGES) // Check each area only once per second.
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vTriggerPoint[currentBridge]) < fStartChecking // Wait until player is within fStartChecking before starting checks.
								IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
								OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
									IF currentBridge = SP_KNIFE_SM01		
									OR currentBridge = SP_KNIFE_2			
									OR currentBridge = SP_KNIFE_3		
									OR currentBridge = SP_KNIFE_4	
									OR currentBridge = SP_KNIFE_5
									OR currentBridge = SP_KNIFE_6
									OR currentBridge = SP_KNIFE_7
									OR currentBridge = SP_KNIFE_8
									OR currentBridge = SP_KNIFE_9
									OR currentBridge = SP_KNIFE_10
									OR currentBridge = SP_KNIFE_11
									OR currentBridge = SP_KNIFE_12
									OR currentBridge = SP_KNIFE_13
									OR currentBridge = SP_KNIFE_14
									OR currentBridge = SP_KNIFE_15
										bIsKnife = TRUE
									ELSE
										bIsKnife = FALSE
									ENDIF
									IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										IF DOES_PLAYER_MEET_FLIGHT_REQUIREMENTS()
											IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
												bEnteredArea1 = TRUE
												underBridgeStage = checkForExit
											ENDIF
											IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
												bEnteredArea2 = TRUE
												underBridgeStage = checkForExit
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
//								IF NOT bEnteredArea1
//								OR NOT bEnteredArea2
//									currentBridge++
//									IF currentBridge = NUMBER_OF_BRIDGES
//										currentBridge = 0
//									ENDIF
//								ENDIF
							ENDIF
						ENDIF
						SETTIMERA(0)
//					ENDIF
				BREAK
				
				
				CASE checkForExit
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
						AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
							RESET_BRIDGE_CHECKS()
							PRINTLN("<flyUnderBridges> - Player is no longer in a flying vehicle. Reset & wait.")
						ELSE
							IF NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								RESET_BRIDGE_CHECKS()
								PRINTLN("<flyUnderBridges> - Player's vehicle is no longer driveable. Reset & wait.")
							ELIF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								RESET_BRIDGE_CHECKS()
								PRINTLN("<flyUnderBridges> - Player's vehicle is touching the ground. Reset & wait.")
							ENDIF
						ENDIF
					
				
						IF bEnteredArea1
						
							IF bPassingThroughAreas
								IF bExitViaArea2
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										//Passed Through
										RESET_BRIDGE_CHECKS()
										STORE_BRIDGE_AS_COMPLETE()
										REPLAY_RECORD_BACK_FOR_TIME(3)
										PRINTLN("<flyUnderBridges> - Succesful! bExitViaArea2 for bridge number ", currentBridge, ".")
									ENDIF
								ELSE
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										bExitViaArea2 = TRUE
									ELSE
										IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
										AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
											//Never passed through
											RESET_BRIDGE_CHECKS()
											PRINTLN("<flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									bPassingThroughAreas = TRUE
								ELSE
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										//Never passed through
										RESET_BRIDGE_CHECKS()
										PRINTLN("<flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
									ENDIF
								ENDIF
							ENDIF
							
						ELIF bEnteredArea2
							IF bPassingThroughAreas
								IF bExitViaArea1
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										//Passed Through
										RESET_BRIDGE_CHECKS()
										STORE_BRIDGE_AS_COMPLETE()
										REPLAY_RECORD_BACK_FOR_TIME(3)
										PRINTLN("<flyUnderBridges> - Succesful! bExitViaArea1 for bridge number ", currentBridge, ".")
									ENDIF
								ELSE
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										bExitViaArea1 = TRUE
									ELSE
										IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
										AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
											//Never passed through
											RESET_BRIDGE_CHECKS()
											PRINTLN("<flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									bPassingThroughAreas = TRUE
								ELSE
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
										//Never passed through
										RESET_BRIDGE_CHECKS()
										PRINTLN("<flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK

			ENDSWITCH
		ENDIF
	ELSE
		IF splashStage = loadScaleform
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
ENDWHILE

ENDSCRIPT
