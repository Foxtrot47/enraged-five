

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //                                             
//      AUTHOR          :                                 			                     //
//      DESCRIPTION     :   Choose and emergency service                                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "cellphone_public.sch"
USING "dialogue_public.sch"
USING "lineactivation.sch"

// Variables ----------------------------------------------//

ENUM EMERGENCY_MAIN_ENUM
	WAIT_FOR_CALL = 0,
	LOAD_ASSETS,
	POLICE_ACTION,
	AMBULANCE_ACTION,
	FIRE_ACTION,
	CLEANUP
ENDENUM

EMERGENCY_MAIN_ENUM emergency_main_stages = WAIT_FOR_CALL

ENUM callStageFlag
	callphoneup,
	callselectservice
ENDENUM
callStageFlag callStage = callphoneup



VECTOR vNearestRoadNode

INT counter_time_started_cellphone
  
INCIDENT_INDEX tempIncident

// Functions ----------------------------------------------//
 
PROC take_call()
	FLOAT fTemp
	
	SWITCH callStage
			
		CASE callphoneup

			callStage = callselectservice
			counter_time_started_cellphone = GET_GAME_TIMER()
			
		BREAK
		
		CASE callselectservice
			
			WHILE NOT HAS_CELLPHONE_CALL_FINISHED()
				WAIT(0)		
				IF CHECK_RESPONSE_TO_CELLPHONE_PROMPT() = RESPONDED_AMBULANCE
					emergency_main_stages = AMBULANCE_ACTION
				ENDIF

				IF CHECK_RESPONSE_TO_CELLPHONE_PROMPT() = RESPONDED_FIRE
					emergency_main_stages = FIRE_ACTION
				ENDIF
				
				IF CHECK_RESPONSE_TO_CELLPHONE_PROMPT() = RESPONDED_POLICE
					emergency_main_stages = POLICE_ACTION
				ENDIF	
				IF GET_GAME_TIMER() > counter_time_started_cellphone+30000
					PRINTLN("emergencyCall - Call has timed out.")
					counter_time_started_cellphone = GET_GAME_TIMER()
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
			ENDWHILE
			
			IF emergency_main_stages = WAIT_FOR_CALL //Cleans up script if player hangs up
				emergency_main_stages = CLEANUP
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//GET_CLOSEST_VEHICLE_NODE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vNearestRoadNode)
				
				GET_POSITION_FOR_HANDOOVER_SCENE(PLAYER_PED_ID(), vNearestRoadNode, fTemp)
			ENDIF
			
			counter_time_started_cellphone = GET_GAME_TIMER()
		 BREAK
			 
	ENDSWITCH
	
	
 ENDPROC         
               
PROC emergencyCall_Cleanup() 	//Unload assests and reset flags
	emergency_main_stages = WAIT_FOR_CALL
	callStage = callphoneup
	PRINTSTRING ("emergencycall.sc - Cleanup")
	PRINTNL()
	TERMINATE_THIS_THREAD()
ENDPROC


PROC CHECK_FOR_TIMEOUT()
	IF GET_GAME_TIMER() > (counter_time_started_cellphone + 30000)
		PRINTSTRING ("\nemergencycall.sc - CREATE_INCIDENT has timed out.\n")
		emergency_main_stages = CLEANUP
	ENDIF
ENDPROC

			 
// Mission Script -----------------------------------------//


SCRIPT 

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	emergency_main_stages = CLEANUP
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
	WAIT(0)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())

		SWITCH emergency_main_stages
		
			CASE WAIT_FOR_CALL
				take_call()
			BREAK
			
			CASE LOAD_ASSETS
			
			BREAK
			
			CASE POLICE_ACTION
				CHECK_FOR_TIMEOUT()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF CREATE_INCIDENT_WITH_ENTITY(DT_POLICE_VEHICLE_REQUEST, PLAYER_PED_ID(), 2, 3.0, tempIncident)
						PRINTSTRING ("emergencycall.sc - CASE STAGE - POLICE_ACTION - NETWORK")
						PRINTNL()
						counter_time_started_cellphone = GET_GAME_TIMER()
						emergency_main_stages = CLEANUP
					ENDIF
				ELSE
					IF CREATE_INCIDENT(DT_POLICE_VEHICLE_REQUEST, vNearestRoadNode, 2, 3.0, tempIncident)  
						PRINTSTRING ("emergencycall.sc - CASE STAGE - POLICE_ACTION")
						PRINTNL()
						counter_time_started_cellphone = GET_GAME_TIMER()
						emergency_main_stages = CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE AMBULANCE_ACTION
				CHECK_FOR_TIMEOUT()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
					AND GB_GET_GANGOPS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = GOV_PARAMEDIC
					AND g_bFM_GangOpsParamedicPlayerCanCallForAmbulance
						IF g_bFM_GangOpsParamedicPlayerHasRequestedAmbulance = FALSE
							PRINTLN("[FM_GANGOPS] - [GOV_PARAMEDIC] - Emergency call - player has requested an ambulance, setting g_bFM_GangOpsParamedicPlayerHasRequestedAmbulance")
							g_bFM_GangOpsParamedicPlayerHasRequestedAmbulance = TRUE
						ENDIF
						emergency_main_stages = CLEANUP
					ELSE
						IF CREATE_INCIDENT_WITH_ENTITY(DT_AMBULANCE_DEPARTMENT, PLAYER_PED_ID(), 2, 3.0, tempIncident)
							PRINTSTRING ("emergencycall.sc - CASE STAGE - AMBULANCE_ACTION - NETWORK")
							PRINTNL()
							counter_time_started_cellphone = GET_GAME_TIMER()
							emergency_main_stages = CLEANUP
						ENDIF
					ENDIF
				ELSE
					IF CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, vNearestRoadNode, 2, 3.0, tempIncident) 
						PRINTSTRING ("emergencycall.sc - CASE STAGE - AMBULANCE_ACTION")
						PRINTNL()
						counter_time_started_cellphone = GET_GAME_TIMER()
						emergency_main_stages = CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE FIRE_ACTION
				CHECK_FOR_TIMEOUT()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF CREATE_INCIDENT_WITH_ENTITY(DT_FIRE_DEPARTMENT, PLAYER_PED_ID(), 4, 3.0, tempIncident)
						PRINTSTRING ("emergencycall.sc - CASE STAGE - FIRE_ACTION - NETWORK")
						PRINTNL()
						counter_time_started_cellphone = GET_GAME_TIMER()
						emergency_main_stages = CLEANUP
					ENDIF
				ELSE
					IF CREATE_INCIDENT(DT_FIRE_DEPARTMENT, vNearestRoadNode, 4, 3.0, tempIncident)
						IF g_sTriggerSceneAssets.id = GET_HASH_KEY("AGENCY_PREP_1")
						OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("agency_prep1")) > 0 AND IS_REPEAT_PLAY_ACTIVE())
							g_sTriggerSceneAssets.id = GET_HASH_KEY("AHP1_TRUCKCALLED")
						ELSE
							//The player has called 911 to request a fire truck before the text message in Agency Prep 1)
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2) AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_PREP_1)
								g_bCalled911BeforeText = TRUE
							ENDIF
						ENDIF
						PRINTSTRING ("emergencycall.sc - CASE STAGE - FIRE_ACTION")
						PRINTNL()
						counter_time_started_cellphone = GET_GAME_TIMER()
						emergency_main_stages = CLEANUP
					ENDIF
				ENDIF
			BREAK
			
			CASE CLEANUP
				IF GET_GAME_TIMER() > counter_time_started_cellphone + 60000 
				OR NOT IS_INCIDENT_VALID(tempIncident)
					emergencyCall_Cleanup()
				ELSE
					IF IS_PLAYER_PLAYING(PLAYER_ID())
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							IF IS_INCIDENT_VALID(tempIncident)
								DELETE_INCIDENT(tempIncident)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
						
		ENDSWITCH
	
	ENDIF
	
ENDWHILE
ENDSCRIPT	



