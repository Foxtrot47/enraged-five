//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   stripperhome.sc                                             //
//      AUTHOR          :   Rob Bray    
//		Now Maintained 	:	John R. Diaz
//      DESCRIPTION     :   Take a dancer home from the strip club		                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
///
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "script_drawing.sch"
USING "script_blips.sch"
USING "minigames_helpers.sch"
USING "dialogue_public.sch"
USING "stripclub_public.sch" 
USING "rgeneral_include.sch"
USING "comms_control_public.sch"
USING "flow_processing_game.sch"
USING "oddjob_aggro.sch"
USING "script_oddjob_funcs.sch"
USING "sclub_stats.sch"
USING "common_sex.sch"

// Enums
ENUM STRIPPER_HOME_STAGE_ENUM
	DEBUG_SKIP_MODE = -1,
    STAGE_GO_TO_HOUSE = 0,
    STAGE_ENTER_HOUSE,
	PICK_UP_BOOTY_CALL,
	FIND_QUITE_LOCATION_FOR_BOOTYCALL,
	DROP_OFF_BOOTY_CALL,
	SEND_STRIPPER_BACK_TO_WORK,
	BOOTYCALL_FAILED
ENDENUM

ENUM ENTER_HOUSE_STAGE_ENUM
	ENTER_HOUSE_INIT = 0,	
	ENTER_HOUSE_PARK_VEHICLE,
	
	ENTER_HOUSE_SHOT_ALPHA,
	ENTER_HOUSE_SHOT_ALPHA_TO_BETA,
	ENTER_HOUSE_BETA_HOLD,
	ENTER_HOUSE_SHOT_TOD,
	ENTER_HOUSE_SHOT_HOLD_TOD,
	ENTER_HOUSE_SHOT_CUT_TO_AFTER,
	ENTER_HOUSE_SHOT_LERP_TO_GAMEPLAY,
	ENTER_HOUSE_SHOT_CATCHUP,
	ENTER_HOUSE_SHOT_OVER_SHOULDER,
	
	
	ENTER_HOUSE_PAN,
	ENTER_HOUSE_FADE_OUT,	
	ENTER_HOUSE_DONE
ENDENUM

ENUM FIND_QUITE_SPOT_ENUM
	DRIVE_TO_SPOT = 0,
	INIT_SEX,
	HAVE_SEX
ENDENUM

ENUM FAIL_REASON_ENUM
	FAIL_STRIPPER_DEAD = 0,
	FAIL_STRIPPER_ABANDONED,
	FAIL_STRIPPER_CANCELLED,
	FAIL_STRIPPER_WALKING,
	FAIL_STRIPPER_ATTACKED
ENDENUM

ENUM GO_TO_HOUSE_ENUM
	GO_TO_HOUSE_INIT,
	GO_TO_HOUSE_MEET,
	GO_TO_HOUSE_GREET,
	GO_TO_HOUSE_OBJ,
	GO_TO_HOUSE_FOOT,
	GO_TO_HOUSE_DRIVE,
	GO_TO_HOUSE_STOP,
	GO_TO_HOUSE_WALK_TO_DOOR,
	GO_TO_HOUSE_LOSE_COPS,
	GO_TO_HOUSE_RETURN
ENDENUM

ENUM HOUSE_BANTER_BIT_FLAGS
	HOUSE_BANTER_NEAR_HOUSE = BIT0,
	HOUSE_BANTER_NEED_CAR = BIT1,
	HOUSE_BANTER_TOO_FAST = BIT2,
	HOUSE_BANTER_TOO_SLOW = BIT3,	
	HOUSE_BANTER_NEW_CAR = BIT4,
	HOUSE_BANTER_ONE_OFF = BIT5,
	HOUSE_BANTER_BJD = BIT6,
	HOUSE_BANTER_BJ_FINISHED = BIT7,
	HOUSE_BANTER_BJ = BIT8,
	HOUSE_BANTER_SEX = BIT9,
	HOUSE_BANTER_NICE_CAR = BIT10,
	HOUSE_BANTER_SEX_MALE = BIT11,
	HOUSE_BANTER_LOSE_COPS = BIT12,
	HOUSE_BANTER_ONE_OFF_1 = BIT13,
	HOUSE_BANTER_ONE_OFF_2 = BIT14,
	HOUSE_BANTER_ONE_OFF_3 = BIT15,
	HOUSE_BANTER_ONE_OFF_DONE = BIT16,
	HOUSE_BANTER_CRASHED_CAR = BIT17,
	HOUSE_BANTER_STOLEN_CAR = BIT18,
	HOUSE_BANTER_PLAYER_RESPONSE = BIT19
ENDENUM

ENUM HOUSE_GENERAL_BIT_FLAGS
	HOUSE_MEET_UP_TEXT 				= BIT0,	
	HOUSE_MEET_IS_NEAR 				= BIT1,
	HOUSE_MEET_BJ_STREAMED 			= BIT2,
	HOUSE_MEET_BJ_ACTIVE 			= BIT3,
	HOUSE_MEET_BJ_TRIGGERED 		= BIT4,
	HOUSE_MEET_REQUEST_MODELS 		= BIT5,
	HOUSE_MEET_INIT_CANCEL 			= BIT6,
	HOUSE_MEET_INIT_CANCEL_HELP 	= BIT7,
	HOUSE_MEET_REQUEST_IDLE_ANIM	= BIT8,
	HOUSE_MEET_GIRL_CANCEL 			= BIT9,
	HOUSE_MEET_STRIPPER_CREATED 	= BIT10,
	HOUSE_MEET_STRIPPER_TAKE_IN 	= BIT11,
	HOUSE_MEET_AGGROED				= BIT12,
	HOUSE_DANCER_GOING_BACK_CLUB	= BIT13,
	HOUSE_WAITING_TO_CLOSE_DOOR		= BIT14,
	HOUSE_REQUEST_HAPPY_FACE_ANIMS	= BIT15,
	HOUSE_GEN_LUKE_MODE_SKIP_ON		= BIT16,
	HOUSE_GEN_HOME_SCENE_PLAYING	= BIT17,
	HOUSE_GEN_PLAY_SEX_ANIM			= BIT18
	
ENDENUM

// Script-specific entitie

//Vehicle Saving
VEHICLE_SAVE_ARGS			VehicleSaveArgsBootyCall
BOOL						bVehicleSaved


REL_GROUP_HASH 				RELGROUPHASH_NEUTRAL_TO_PLAYER


TIMEOFDAY					eTODCALLMade
STRIPPER_HOME_STAGE_ENUM 	currentMissionStage = STAGE_GO_TO_HOUSE
ENTER_HOUSE_STAGE_ENUM 		enterHouseStage
BOOTY_CALL_CONTACT_ENUM 	stripperID[MAX_NUMBER_STRIPPERS_TAKE_HOME]
GO_TO_HOUSE_ENUM			eGoToHouseState = GO_TO_HOUSE_INIT
GO_TO_HOUSE_ENUM			ePreviousGoToHouseState
FIND_QUITE_SPOT_ENUM        stateFindingSpot
structPedsForConversation 	stripperConversation



structTimer					tBanter
structTimer					tCarCrash
structTimer					tSpeed
structTimer					tBJ
structTimer					tWalking
structTimer					tExitWait
structTimer					tAggroTimer



//Aggro Checks
AGGRO_ARGS					tAggroArgs

// Peds
PED_INDEX stripperPed[MAX_NUMBER_STRIPPERS_TAKE_HOME]
STRING sAfterSexAnimDict
STRING sThisWeatherType
//	Models
STREAMED_MODEL			takeHomeModels[MAX_NUMBER_STRIPPERS_TAKE_HOME]

// Blips
BLIP_INDEX houseVehicleBlip
BLIP_INDEX houseFootBlip
BLIP_INDEX stripperHomeBlip[MAX_NUMBER_STRIPPERS_TAKE_HOME]

// Cameras
CONST_INT NUM_BOOTY_CALL_CAMS 	4
CAMERA_INDEX homeCam[NUM_BOOTY_CALL_CAMS]
structTimelapse tTimeLapse

//SCENARIO BLOCKING VOLUMES
SCENARIO_BLOCKING_INDEX sbiSHome
// Vectors
VECTOR vPlayerWalkToPos
VECTOR vHouseDoorPos
VECTOR vHouseInVehiclePos
VECTOR vHouseOnFootPos
VECTOR vAfterCutPos
VECTOR vPickUpGirlSpot
VECTOR vStarttWalkLoc
VECTOR vHeliPos

// Floats
FLOAT fAfterCutRot
FLOAT fVehicleHeading
FLOAT fHeliHeading
FLOAT fInitDistance = -1

// Ints
INT iTODSkipHour
INT iBanterBitFlags
INT candidateID = NO_CANDIDATE_ID
INT iGeneralBitFlags
INT iVehicleHealth
INT iRecordBlockPersistTimer = 0
BOOL bDropOffHome = TRUE
BOOL bIsMP = FALSE
BOOL bStoppingCar = FALSE

// Other variables
SEQUENCE_INDEX sequence
VEHICLE_INDEX iTakeHomeVehicle

CONST_FLOAT		CONST_fCleanupDistance	100.00
CONST_FLOAT		CONST_fAbandoned	200.0
CONST_FLOAT		CONST_fMetUp		3.0
CONST_FLOAT 	CONST_fYParkOffset 	-5.0
CONST_FLOAT 	CONST_fTrigger 		3.0
CONST_FLOAT		CONST_fRoadNodeAreaWidth	15.0
CONST_FLOAT 	CONST_fVehicleTrigger 		10.0
CONST_FLOAT 	CONST_fVehicleHomeTrigger 	1.0
CONST_FLOAT		CONST_fDoorTrigger	5.0
CONST_FLOAT 	CONST_fNearHouse 	75.0
CONST_FLOAT		CONST_fMaxWalkDist  100.0
CONST_FLOAT		CONST_fMaxWalkTime  45.0
CONST_INT 		CONST_iBJDuration 	30000
CONST_INT		CONST_iMaxNumSexts	2
CONST_INT		CONST_iTimeSextDelay 60000	//in milliseconds

CONST_FLOAT		CONST_fHomeCam1stShotTime	1.0
CONST_FLOAT		CONST_fHomePanUpTime	6.0
CONST_INT		CONST_iHomePanUpTODTime	6000
CONST_INT		CONST_iPanToPlayerTime	6000
CONST_INT		CONST_iNumHoursToSkip	8
CONST_INT		CONST_iShowDebugInfo	0

#IF IS_DEBUG_BUILD
	// do a debug print prefixed by Strip Club
	PROC DEBUG_PRINT_STRIPPER_HOME(STRING debugString)
		IF g_bDebugDisplayStripClubAndBootyCall
			PRINTSTRING("Stripper Home: ")
			PRINTSTRING(debugString)
			PRINTNL()
		ENDIF
	ENDPROC
	
	PROC DEBUG_SHOW_STRIPPER_HOME_COORDS()
//		DRAW_DEBUG_SPHERE(vHouseInVehiclePos,CONST_fVehicleHomeTrigger,255,255,0,120)	//vehicle spot
//		DRAW_DEBUG_SPHERE(vHouseOnFootPos,CONST_fDoorTrigger,0,0,255,120)	//cutscene  spot
//		DRAW_DEBUG_SPHERE(vHouseDoorPos,CONST_fDoorTrigger,0,255,0,120)		//door spot
		
	ENDPROC
	
	MODEL_NAMES eDebugTakeHomeVehicle = tailgater	
	VEHICLE_INDEX iDebugTakeHomeVehicle
	BOOL bRequestedDebugVehicle
	VECTOR vDebugTakeHomeVehicle = <<92.18, -1279.44, 28.77>>
	FLOAT fDebugTakeHomeVehicle = 92.23	
#ENDIF

FUNC STRING GET_BOOTY_CALL_END_FACE_ANIM()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		RETURN "facials@p_m_zero@variations@happy"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN "facials@p_m_one@variations@happy"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		RETURN "facials@p_m_two@variations@happy"
	ENDIF
	
	RETURN "facials@p_m_zero@variations@happy"
ENDFUNC

FUNC STRING GET_RANDOM_HAPPY_FACE_MOOD()

	INT iRandomSeed = GET_RANDOM_INT_IN_RANGE()
	
	IF iRandomSeed <= 21666
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[HAPPY FACE] # 1" )
		RETURN "mood_happy_1"
	
	ELIF iRandomSeed > 21666 AND iRandomSeed <= 43333
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[HAPPY FACE] # 2" )
		RETURN "mood_happy_2"
		
	ELSE	
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[HAPPY FACE] # 3" )
		RETURN "mood_happy_3"
	ENDIF
ENDFUNC

FUNC TEXT_LABEL_23 FIXUP_CONVO_LABELS(TEXT_LABEL_23 sBaseLabel)
	sBaseLabel += "_"

	INT iToAdd = ENUM_TO_INT(stripperID[0])
	sBaseLabel += iToAdd 

	CDEBUG1LN(DEBUG_BOOTY_CALL,"*******Triggering Convo Label = ", sBaseLabel)
			
	RETURN sBaseLabel
ENDFUNC


PROC FIXUP_CONVO_LABELS_FOR_PLAYER(TEXT_LABEL_23& sBaseLabel, BOOL bAppenedStripper)
	sBaseLabel += "_"
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		sBaseLabel += "T"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		sBaseLabel += "M"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		sBaseLabel += "F"
	ENDIF	
	
	IF bAppenedStripper
		sBaseLabel = FIXUP_CONVO_LABELS(sBaseLabel)
	ENDIF
ENDPROC

// can do stripper home god text?
FUNC BOOL CAN_DO_STRIPPER_HOME_GOD_TEXT()
	IF IS_MESSAGE_BEING_DISPLAYED() // no messages
	OR IS_SCRIPTED_CONVERSATION_ONGOING()
	OR g_ConversationStatus <> CONV_STATE_FREE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


FUNC BOOL CREATE_CONVERSATION_FOR_BOOTY_CALL( STRING sRoot, BOOL bPlayer = FALSE, BOOL bAppendStripper = FALSE,BOOL sReadFromSCLUB = FALSE)
	
	IF CAN_DO_STRIPPER_HOME_GOD_TEXT()
		TEXT_LABEL_23 sBaseLabel
		sBaseLabel = sRoot
		
		IF bPlayer 
			FIXUP_CONVO_LABELS_FOR_PLAYER(sBaseLabel,bAppendStripper)
		ELSE
			sBaseLabel = FIXUP_CONVO_LABELS(sBaseLabel)
		ENDIF
		
		STRING sGroupID
		IF sReadFromSCLUB
			sGroupID = "SCAUD"
		ELSE
			sGroupID = "SHAUD"
		ENDIF
		
		IF CREATE_CONVERSATION(stripperConversation, sGroupID, sBaseLabel, CONV_PRIORITY_MEDIUM)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC PLAY_BOOTY_CALL_GREETING()

	IF stripperID[0] = BC_TAXI_LIZ		
		CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_GREET")
	ELSE
		PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"GENERIC_HI_FLIRTY",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
	ENDIF
	
	CDEBUG1LN(DEBUG_BOOTY_CALL,"[AUDIO] Booty call greeted the player." )
ENDPROC

PROC CREATE_STRIPPER_BLIP(INT index)
	
	IF DOES_BLIP_EXIST(stripperHomeBlip[index])
		REMOVE_BLIP(stripperHomeBlip[index])
	ENDIF
	
	stripperHomeBlip[index] = CREATE_BLIP_FOR_PED(stripperPed[index])
	TEXT_LABEL sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(stripperID[index])
	SET_BLIP_NAME_FROM_TEXT_FILE(stripperHomeBlip[index], sStripperName)
	
	CDEBUG1LN(DEBUG_BOOTY_CALL,"Added blip to booty call." )
ENDPROC

PROC CLEAR_STRIPPER_BLIPS()
	INT i
	REPEAT MAX_NUMBER_STRIPPERS_TAKE_HOME i
		IF DOES_BLIP_EXIST(stripperHomeBlip[i])
			REMOVE_BLIP(stripperHomeBlip[i])
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Removing Stripper Blips" )
		ENDIF
	ENDREPEAT
ENDPROC
// clear blips
PROC CLEAR_ALL_BLIPS()
	IF DOES_BLIP_EXIST(houseFootBlip)
		REMOVE_BLIP(houseFootBlip)
	ENDIF
	IF DOES_BLIP_EXIST(houseVehicleBlip)
		REMOVE_BLIP(houseVehicleBlip)
	ENDIF
	
	CLEAR_STRIPPER_BLIPS()
ENDPROC

FUNC STRING GET_SPECIFIC_WEATHER_FOR_BOOTY_CALL()

	STRING sWeatherType

	SWITCH stripperID[0]
		CASE BC_STRIPPER_JULIET
			sWeatherType = "CLEARING"
		BREAK
		
		CASE BC_STRIPPER_NIKKI
			sWeatherType = "OVERCAST"
		BREAK
		
		CASE BC_STRIPPER_SAPPHIRE
			sWeatherType = "CLOUDS"
		BREAK
		
		CASE BC_STRIPPER_INFERNUS
			sWeatherType = "CLEAR"
		BREAK
		
		CASE BC_TAXI_LIZ
			sWeatherType = "EXTRASUNNY"
		BREAK
		
		CASE BC_HITCHER_GIRL
			sWeatherType = "RAIN"
		BREAK
		
		DEFAULT
			sWeatherType = "EXTRASUNNY"
		BREAK
		
	ENDSWITCH
		
	CDEBUG1LN(DEBUG_BOOTY_CALL,"[WEATHER ] =  ", sWeatherType)	
	RETURN sWeatherType
ENDFUNC
PROC PRINT_HELP_WITH_STRIPPER_NAMES(STRING sHelpMessage, BOOTY_CALL_CONTACT_ENUM BootyCallID, BOOL bForever = FALSE)
	
	TEXT_LABEL_23 sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(BootyCallID)

	IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED(sHelpMessage, sStripperName)
		PRINTLN("Print help ", sHelpMessage, " using name ", sStripperName)
		IF NOT bForever
			PRINT_HELP_WITH_STRING(sHelpMessage, sStripperName)
		ELSE
			PRINT_HELP_FOREVER_WITH_STRING(sHelpMessage, sStripperName)
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_OBJECTIVE_WITH_STRIPPER_NAMES(STRING sObjMessage, BOOTY_CALL_CONTACT_ENUM BootyCallID)
	
	TEXT_LABEL_23 sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(BootyCallID)

	
	PRINTLN("Print Objective ", sObjMessage, " using name ", sStripperName)
	PRINT_STRING_IN_STRING_NOW( sObjMessage ,sStripperName, DEFAULT_GOD_TEXT_TIME, 1 )
		
ENDPROC

PROC GET_MORNING_AFTER_SEX_DICTIONARY()

	sAfterSexAnimDict = "move_p_m_one_idles@generic"
	
	REQUEST_ANIM_DICT(sAfterSexAnimDict)
	
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[MORNING_AFTER_ANIM] - Requested: ",sAfterSexAnimDict )

ENDPROC
/// PURPOSE: Sets the player to walk into the frame and play an idle anim post cutscene
///    
PROC PLAY_MORNING_AFTER_ANIM()
	

	IF NOT HAS_ANIM_DICT_LOADED(sAfterSexAnimDict)
	
		CDEBUG1LN(DEBUG_BOOTY_CALL, "[MORNING_AFTER_ANIM] - Waiting for assets: ", sAfterSexAnimDict)
	ELSE
		
	//	VECTOR vPosIntoFrame = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_HEADING(PLAYER_PED_ID()),<<0.0,2.5,0.0>>)
		INT iWhichAnim = GET_RANDOM_INT_IN_RANGE(0, 50000)	
		
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	
		SEQUENCE_INDEX iSequenceIDPlayerOutro
		OPEN_SEQUENCE_TASK(iSequenceIDPlayerOutro)
		
		//	TASK_GO_STRAIGHT_TO_COORD(NULL,vAfterCutPos,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,fAfterCutRot)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,vAfterCutPos,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT, fAfterCutRot)
			IF iWhichAnim < 15000
				TASK_PLAY_ANIM(NULL,sAfterSexAnimDict,"fidget_rub_hands",WALK_BLEND_IN, -1, -1, AF_EXIT_AFTER_INTERRUPTED | AF_ABORT_ON_PED_MOVEMENT)//, 0.757)
				CDEBUG1LN(DEBUG_BOOTY_CALL, "[MORNING_AFTER_ANIM] - Rub hands")
					
			
			ELIF iWhichAnim >= 15000 AND iWhichAnim < 35000
				TASK_PLAY_ANIM(NULL,sAfterSexAnimDict,"fidget_rub_chin",WALK_BLEND_IN, -1, -1, AF_EXIT_AFTER_INTERRUPTED | AF_ABORT_ON_PED_MOVEMENT)//, 0.757)
				CDEBUG1LN(DEBUG_BOOTY_CALL, "[MORNING_AFTER_ANIM] - Hand hips")
					
			
			ELSE
				TASK_PLAY_ANIM(NULL,sAfterSexAnimDict,"fidget_arm_swing",WALK_BLEND_IN, -1, -1, AF_EXIT_AFTER_INTERRUPTED | AF_ABORT_ON_PED_MOVEMENT)//, 0.757)
				CDEBUG1LN(DEBUG_BOOTY_CALL, "[MORNING_AFTER_ANIM] - Muscle Flex")
			ENDIF
			
		
			CLOSE_SEQUENCE_TASK(iSequenceIDPlayerOutro)
		TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), iSequenceIDPlayerOutro)
		CLEAR_SEQUENCE_TASK(iSequenceIDPlayerOutro)
	
		SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_GEN_PLAY_SEX_ANIM)

	ENDIF
	
ENDPROC

//If player takes too long to pick up the booty call, send her back to work
FUNC BOOL SHOULD_PLAY_BOOTY_CALL_RETURN_TO_WORK_CUTSCENE()

	//Rule out non strippers
	IF stripperID[0] = BC_TAXI_LIZ
	OR stripperID[0] = BC_HITCHER_GIRL
	OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(stripperPed[0],<<96.12,-1284.91,29.43>> ) > 10
//	OR IS_ENTITY_OCCLUDED(stripperPed[0])
		CDEBUG1LN(DEBUG_BOOTY_CALL, "[SHOULD_PLAY_RETURN_TO_WORK_CUTSCENE] - FALSE")
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[SHOULD_PLAY_RETURN_TO_WORK_CUTSCENE] - TRUE")
	RETURN TRUE
ENDFUNC

PROC SETUP_BACK_TO_WORK_CAM()
	IF NOT IS_ENTITY_OCCLUDED(stripperPed[0])
	AND CAN_PLAYER_START_CUTSCENE()
		ODDJOB_ENTER_CUTSCENE(DEFAULT,DEFAULT,FALSE)
		homeCam[0] = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",<<90.1007, -1281.5734, 28.6661>>, <<2.4422, 0.0, -113.4013>>, 45.0,TRUE)
		//POINT_CAM_AT_COORD(scriptedCamSex[XCAM_PASSENGER_WINDOW],vCameraLookAt)
		//POINT_CAM_AT_ENTITY(scriptedCamSex[XCAM_PASSENGER_WINDOW],iPlayersVehicle,GET_0_VEC())
		//ATTACH_CAM_TO_ENTITY(scriptedCamSex[XCAM_PASSENGER_WINDOW], iPlayersVehicle, <<2.0,-0.55, 0.85>>)

		CDEBUG1LN(DEBUG_BOOTY_CALL, "[SETUP_BACK_TO_WORK_CAM] - Script cam active")
		
		RENDER_SCRIPT_CAMS(TRUE,FALSE)
	ELSE
		CDEBUG1LN(DEBUG_BOOTY_CALL, "[SETUP_BACK_TO_WORK_CAM] - Cam not created.")
	ENDIF
ENDPROC

PROC SEND_BOOTY_CALL_BACK_TO_WORK()
	
	//Unlock the back door
	SETUP_BACK_TO_WORK_CAM()
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<96.12,-1284.91,29.43>> , 5.0,PROP_MAGENTA_DOOR)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_MAGENTA_DOOR,<<96.12,-1284.91,29.43>> ,FALSE, 0.0)
	ENDIF
	SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_WAITING_TO_CLOSE_DOOR)
	
	FREEZE_ENTITY_POSITION(stripperPed[0],FALSE)
	
	SET_PED_CONFIG_FLAG(stripperPed[0], PCF_OpenDoorArmIK, TRUE) 
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[CONFIG_FLAG] - Enabling Open Door ArmIK.")
	
	SEQUENCE_INDEX iBCSeq
	OPEN_SEQUENCE_TASK(iBCSeq)
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<98.2041, -1291.2522, 28.2688>>, PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING) //office
		TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<107.6303, -1304.7424, 27.7688>>, PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)	//dressing room
		TASK_PLAY_ANIM(NULL, "mini@strip_club@idles@stripper", "stripper_idle_01", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 15000, AF_LOOPING)
	CLOSE_SEQUENCE_TASK(iBCSeq)
	TASK_PERFORM_SEQUENCE(stripperPed[0], iBCSeq)
	CLEAR_SEQUENCE_TASK(iBCSeq)
	
	SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_DANCER_GOING_BACK_CLUB)
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[SEND_BOOTY_CALL_BACK_TO_WORK] - Dancer should be heading towards the club.")

	
ENDPROC

FUNC BOOL DID_LOCK_STRIP_CLUB_BACKDOOR_BEHIND_STRIPPER()

	IF IS_ENTITY_DEAD(stripperPed[0])
		RETURN TRUE
	ENDIF
		
	IF IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_DANCER_GOING_BACK_CLUB)
		
		SET_PED_RESET_FLAG(stripperPed[0], PRF_SearchForClosestDoor, TRUE)
		#IF IS_DEBUG_BUILD
			#IF CONST_iShowDebugInfo
//				DRAW_DEBUG_TEXT_2D("PRF_SearchForClosestDoor Flag Set", <<0.5, 0.2, 0.0>>)
			#ENDIF
		#ENDIF
		
		//If player is too far, or stripper has made it back inside the club.
		IF IS_ENTITY_AT_COORD(stripperPed[0], <<96.8033, -1287.5966, 28.2688>>, <<CONST_fDoorTrigger, CONST_fDoorTrigger, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT)
		OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),<<96.8033, -1287.5966, 28.2688>>) > CONST_fCleanupDistance
			
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<96.12,-1284.91,29.43>> , 5.0,PROP_MAGENTA_DOOR)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_MAGENTA_DOOR,<<96.12,-1284.91,29.43>> ,TRUE, 0.0)
			ENDIF
			
			IF DOES_CAM_EXIST(homeCam[0])
				IF IS_CAM_RENDERING(homeCam[0])
					ODDJOB_EXIT_CUTSCENE(DEFAULT,DEFAULT,FALSE)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					DESTROY_CAM(homeCam[0] ,TRUE)
					
					CDEBUG1LN(DEBUG_BOOTY_CALL, "[SEND_BOOTY_CALL_BACK_TO_WORK] - Cam destroyed.")
				ENDIF
			ENDIF
			
			DELETE_PED(stripperPed[0])
			CDEBUG1LN(DEBUG_BOOTY_CALL, "[LOCK_STRIP_CLUB_BACKDOOR_BEHIND_STRIPPER] - Back door is LOCKED.")
			
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_BOOTY_CALL, "[LOCK_STRIP_CLUB_BACKDOOR_BEHIND_STRIPPER] - Waiting to CLOSE BACK DOOR at <<96.12,-1284.91,29.43>> ")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC DO_STAGE_SEND_STRIPPER_TO_THE_CLUB()
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[DO_STAGE_SEND_STRIPPER_TO_THE_CLUB] - MAIN")
	
	//Check if the dancer is going back to the club-------------------------------
	IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_DANCER_GOING_BACK_CLUB)
		IF SHOULD_PLAY_BOOTY_CALL_RETURN_TO_WORK_CUTSCENE()
			SEND_BOOTY_CALL_BACK_TO_WORK()
		ELSE
			CDEBUG1LN(DEBUG_BOOTY_CALL, "[DO_STAGE_SEND_STRIPPER_TO_THE_CLUB] - Setting to BOOTYCALL_FAILED.")
			currentMissionStage = BOOTYCALL_FAILED
		ENDIF
	ENDIF
	
	//Wait to lock door then call fail
	IF IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_WAITING_TO_CLOSE_DOOR)
		IF DID_LOCK_STRIP_CLUB_BACKDOOR_BEHIND_STRIPPER()
			currentMissionStage = BOOTYCALL_FAILED
		ENDIF
	ENDIF

	
	
ENDPROC

// cleanup script
PROC MISSION_CLEANUP()

	DEBUG_MESSAGE("Terminating stripperhome")
	CDEBUG1LN(DEBUG_BOOTY_CALL, "[CLEANUP] Called ")
	
	//Cleanup Widget
//	g_bTurnOnLukeHCameraDebug = FALSE
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	CLEAR_ALL_BLIPS()
	CLEAR_PRINTS()
	REMOVE_SCENARIO_BLOCKING_AREA(sbiSHome)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHouseInVehiclePos-<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, vHouseInVehiclePos+<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, TRUE)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vHouseInVehiclePos-<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, vHouseInVehiclePos+<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>,CONST_fRoadNodeAreaWidth )
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	

	IF DOES_ENTITY_EXIST(stripperPed[0])
		IF NOT IS_ENTITY_DEAD(stripperPed[0])
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_DANCER_GOING_BACK_CLUB)
			
				TASK_WANDER_STANDARD(stripperPed[0])
			ENDIF
			
			SET_PED_KEEP_TASK(stripperPed[0], TRUE)
			IF NOT DOES_RELATIONSHIP_GROUP_EXIST(RELGROUPHASH_NEUTRAL_TO_PLAYER)
				ADD_RELATIONSHIP_GROUP("BootyCall", RELGROUPHASH_NEUTRAL_TO_PLAYER)
			ENDIF
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,RELGROUPHASH_NEUTRAL_TO_PLAYER,RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,RELGROUPHASH_PLAYER,RELGROUPHASH_NEUTRAL_TO_PLAYER)
			SET_PED_RELATIONSHIP_GROUP_HASH(stripperPed[0],RELGROUPHASH_NEUTRAL_TO_PLAYER)
		ENDIF
		
		SET_PED_AS_NO_LONGER_NEEDED(stripperPed[0])
	ENDIF
	
	IF candidateID != NO_CANDIDATE_ID
		Mission_Over(candidateID)
	ENDIF
	
	IF bVehicleSaved
		ODDJOB_RESTORE_SAVED_VEHICLE(VehicleSaveArgsBootyCall)
	ENDIF		
	
	SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	
	SET_NEXT_STRIPPER_BOOTY_CALL_AVAILABLE_TIME(ENUM_TO_INT(stripperID[0]))
	
	g_bootyCallData.bootyCallState = BOOTY_CALL_INACTIVE
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE( FALSE )
	TERMINATE_THIS_THREAD()
ENDPROC

// mission passed
PROC MISSION_PASSED()
	INC_BOOTYCALL_STAT_NUM_TIMES_FUCKED_BOOTYCALL()
	REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_BOOTY)
	STRIPCLUB_WRITE_TO_LEADERBOARD(FALSE)
	MISSION_CLEANUP()
ENDPROC

// mission failed
PROC MISSION_FAILED(FAIL_REASON_ENUM reasonForFail)
	
	IF ENUM_TO_INT(stripperID[0]) > -1 AND ENUM_TO_INT(stripperID[0]) < COUNT_OF(BOOTY_CALL_CONTACT_ENUM)
	AND reasonForFail != FAIL_STRIPPER_CANCELLED //you called to cancel, it's okay
		SET_LIKE_FOR_STRIPPER_ID( stripperID[0], GET_LIKE_FOR_STRIPPER_ID(stripperID[0])/2) //lose half the strippers like
	ENDIF
	
	CLEAR_ALL_BLIPS()

	SWITCH reasonForFail
		CASE FAIL_STRIPPER_DEAD
		
			PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_D" , stripperID[0])
			SET_LIKE_FOR_STRIPPER_ID( stripperID[0] , 0)
			SET_STRIPPER_ATTACKED_ON_BOOTY_CALL(stripperID[0])
			
			#IF IS_DEBUG_BUILD				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Player killed: ", GET_BOOTY_CALL_NAME_AS_STRING(stripperID[0]) )
			#ENDIF
			currentMissionStage = BOOTYCALL_FAILED
		BREAK
		
		CASE FAIL_STRIPPER_ABANDONED
			CLEAR_PRINTS()
			PRINT_HELP_WITH_STRIPPER_NAMES("SCLUB_HOME_A", stripperID[0])
			currentMissionStage = SEND_STRIPPER_BACK_TO_WORK
		BREAK
		
		CASE FAIL_STRIPPER_CANCELLED
			//Commenting out per B*1318362
			//PRINT_NOW("SCLUB_HOME_C", DEFAULT_GOD_TEXT_TIME, 1)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[FAIL] Booty Call - Sending back to work" )
			currentMissionStage = SEND_STRIPPER_BACK_TO_WORK
		BREAK
		
		CASE FAIL_STRIPPER_ATTACKED
			PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_D3" , stripperID[0])
			currentMissionStage = BOOTYCALL_FAILED
		BREAK
		
		CASE FAIL_STRIPPER_WALKING
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(stripperPed[0] , <<92.5861, -1282.8604, 28.2548>> ) < 10.0

				CDEBUG1LN(DEBUG_BOOTY_CALL,"[FAIL] Booty Call - Walking - Sending back to work" )
				currentMissionStage = SEND_STRIPPER_BACK_TO_WORK
			ELSE
				currentMissionStage = BOOTYCALL_FAILED
			ENDIF
		BREAK
	ENDSWITCH

	
ENDPROC

PROC HANDLE_BOOTY_CALL_AGGRO(EAggro &eAggroType)//, AGGRO_ARGS &aggroArgs)
	SWITCH eAggroType
		CASE EAggro_Aiming//----------------------------------------------
		CASE EAggro_ShotNear//--------------------------------------------
		CASE EAggro_Attacked//--------------------------------------------
            //SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
			//SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_AGGROED)
			//INCREMENT_STRIPPER_NUM_BOOTY_DENIALS_BY_PLAYER(takeHomeInfo.stripperID[0])
		
			RESET_BOOTY_CALL_TO_INACTIVE(TRUE)
			
        BREAK
		
		CASE EAggro_Wanted//----------------------------------------------
        BREAK
		
	ENDSWITCH
	
ENDPROC
FUNC BOOL HAS_PLAYER_AGGROED_BOOTY_CALL_PED(PED_INDEX ped, AGGRO_ARGS &aggroArgs, structTimer &timer)
	EAggro aggroReason
	VEHICLE_INDEX pedVeh
    
	IF NOT IS_TIMER_STARTED(timer)
		START_TIMER_NOW(timer)
	ENDIF

	IF GET_TIMER_IN_SECONDS(timer) > 3
	
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		
		IF DO_AGGRO_CHECK(ped, pedVeh,aggroArgs, aggroReason)  
			HANDLE_BOOTY_CALL_AGGRO(aggroReason)
			
			IF DOES_ENTITY_EXIST(ped)
				IF NOT IS_ENTITY_DEAD(ped)
					IF NOT DOES_RELATIONSHIP_GROUP_EXIST(RELGROUPHASH_NEUTRAL_TO_PLAYER)
						ADD_RELATIONSHIP_GROUP("BootyCall", RELGROUPHASH_NEUTRAL_TO_PLAYER)
					ENDIF
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_NEUTRAL_TO_PLAYER,RELGROUPHASH_PLAYER)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER,RELGROUPHASH_NEUTRAL_TO_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(ped, RELGROUPHASH_NEUTRAL_TO_PLAYER)
					
					TASK_SMART_FLEE_PED(ped, PLAYER_PED_ID(),500,-1)
				ENDIF
			ENDIF

			RESTART_TIMER_NOW( timer )
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Agrro Timer restarrted")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SET_BOOTY_CALL_SEX_CAM_SHAKE()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		
		IF GET_TIMER_IN_SECONDS_SAFE( tSpeed ) >= 0.45 
			
			STOP_CAM_SHAKING(homeCam[1])
			SHAKE_CAM(homeCam[1], "VIBRATE_SHAKE", 0.15)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 250, 150)
			
			RESTART_TIMER_NOW(tSpeed)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEX CAM] - Trevor Hit")
		ENDIF
	
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		
		IF GET_TIMER_IN_SECONDS_SAFE( tSpeed ) >= 0.75 
			STOP_CAM_SHAKING(homeCam[1])
			
			SHAKE_CAM(homeCam[1], "VIBRATE_SHAKE", 0.1)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 300, 100)
		
			RESTART_TIMER_NOW(tSpeed)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEX CAM] - Michael Hit")
	
		ENDIF
		
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		
		IF GET_TIMER_IN_SECONDS_SAFE( tSpeed ) >= 0.6 
			STOP_CAM_SHAKING(homeCam[1])
			
			SHAKE_CAM(homeCam[1], "VIBRATE_SHAKE", 0.2)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 400, 250)
		
			RESTART_TIMER_NOW(tSpeed)	
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEX CAM] - Franklin Hit")
	
		ENDIF
		
	ENDIF

ENDPROC


FUNC BOOL CAN_USE_SEX_IN_CAR_METHOD()
	SWITCH stripperID[0]
		CASE BC_STRIPPER_JULIET
		CASE BC_STRIPPER_NIKKI
		CASE BC_STRIPPER_CHASTITY
		CASE BC_STRIPPER_CHEETAH
		CASE BC_STRIPPER_SAPPHIRE
		CASE BC_STRIPPER_INFERNUS
		CASE BC_STRIPPER_FUFU
		CASE BC_STRIPPER_PEACH
			IF bIsMP
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// get start pos for take-home stripper
PROC GET_INIT_TAKE_HOME_STRIPPER_LOC(VECTOR &vStorePos, FLOAT &fStoreRot, BOOL bPickUpFromHome)

	vPickUpGirlSpot = vPickUpGirlSpot
	SWITCH stripperID[0]
		CASE BC_STRIPPER_JULIET
		CASE BC_STRIPPER_NIKKI
		CASE BC_STRIPPER_CHASTITY
		CASE BC_STRIPPER_CHEETAH
		CASE BC_STRIPPER_SAPPHIRE
		CASE BC_STRIPPER_INFERNUS
		CASE BC_STRIPPER_FUFU
		CASE BC_STRIPPER_PEACH
			IF NOT bPickUpFromHome
				vStorePos = <<92.8829, -1281.1230, 28.1845>>
				fStoreRot = 15.0
			ELSE
				vStorePos = vAfterCutPos
				fStoreRot = fAfterCutRot
			ENDIF
			vPickUpGirlSpot = vStorePos			
		BREAK
		
		//CASE BC_FIXED_CAR
		CASE BC_TAXI_LIZ
			IF NOT bPickUpFromHome
				vStorePos = << -1615.9908, 187.4827, 59.1337 >>
				fStoreRot = 217.5474
			ELSE
				vStorePos = vAfterCutPos
				fStoreRot = fAfterCutRot
			ENDIF
			vPickUpGirlSpot = vStorePos
		BREAK
		
		CASE BC_HITCHER_GIRL
			IF NOT bPickUpFromHome
				vStorePos = << 1538.4315, 3776.8496, 33.5178 >>
				fStoreRot = 225.8821
			ELSE
				vStorePos = vAfterCutPos
				fStoreRot = fAfterCutRot
			ENDIF
			vPickUpGirlSpot = vStorePos
		BREAK
	ENDSWITCH
	
ENDPROC

// get on foot and in vehicle locations to trigger mission pass
PROC GET_STRIPPER_HOME_POSITIONS()
	SWITCH stripperID[0]
		//Strip Club 1
		CASE BC_STRIPPER_NIKKI
			vPlayerWalkToPos = <<128.1002, -1895.0007, 22.4811>>
			vHouseDoorPos = <<128.1059, -1896.8188, 22.6792>>
			vHouseInVehiclePos = <<133.9411, -1881.8903, 22.5257>>
			vHouseOnFootPos = <<130.1663, -1893.0575, 22.3748>>
			vAfterCutPos =  <<133.4958, -1891.6410, 22.4252>>//<<132.6200, -1893.2034, 22.4094>>//<<132.2298, -1894.2551, 22.4046>>//<<131.6968, -1895.2684, 22.3858>>
			fAfterCutRot = 330.5705
			fVehicleHeading = 244
			vHeliPos = <<146.2930, -1888.0493, 22.2193>>
			fHeliHeading = fVehicleHeading
		BREAK

		CASE BC_STRIPPER_JULIET
		
			//ghetto
			vPlayerWalkToPos = <<-161.9628, -1636.5010, 33.0339>> 
			vHouseDoorPos = <<-159.9415, -1637.3070, 33.0339>> // << 117.84, -1084.93, 28.43 >>
			vHouseInVehiclePos = <<-178.6316, -1629.5216, 32.1789>>
			vHouseOnFootPos = <<-166.1453, -1633.1024, 32.6574>> //<< 116.15, -1503.96, 28.61 >>
			vAfterCutPos =  <<-166.6636, -1633.2289, 32.6567>> //<< 118.01, -1051.65, 28.69 >>
			fAfterCutRot = 108.072 //150.2975
			fVehicleHeading = 180.9811 //-23.88
			vHeliPos = vHouseInVehiclePos
			fHeliHeading = fVehicleHeading
		BREAK
//		CASE BC_STRIPPER_CHASTITY
//			//More upscale
//			vHouseDoorPos = << -587.7217, -819.0717, 25.1546 >>
//			vHouseInVehiclePos = << -580.28, -823.50, 26.02 >>
//			vHouseOnFootPos = << -586.9907, -818.2007, 25.1889 >>
//			vAfterCutPos = << -588.0719, -820.5512, 26.3 >> 
//			fAfterCutRot = 192.7169 
//			fVehicleHeading = 0.0
//		BREAK
//		CASE BC_STRIPPER_CHEETAH
//			//More upscale venice
//			vHouseDoorPos = << -986.4089, -1122.4113, 3.5502 >>
//			vHouseInVehiclePos = << -987.9908, -1112.8301, 1.1375 >>
//			vHouseOnFootPos = << -983.0440, -1128.8718, 1.1503 >>
//			vAfterCutPos = << -990.6027, -1118.0056, 2.2 >>
//			fAfterCutRot = 14.6192 
//			fVehicleHeading = -64.75
//		BREAK

		//Strip Club 2
		CASE BC_STRIPPER_SAPPHIRE
			vPlayerWalkToPos = <<-198.3824, 87.8785, 68.7436>>
			vHouseDoorPos = << -197.2292, 86.3497, 68.7561 >>
			vHouseInVehiclePos = <<-200.9078, 113.5370, 68.5518>>
			vHouseOnFootPos = <<-200.1384, 96.9809, 68.5203>>
			vAfterCutPos = <<-199.5068, 95.7042, 68.5193>>
			fAfterCutRot = 48.99
			fVehicleHeading = 64.3
			vHeliPos = vHouseInVehiclePos
			fHeliHeading = fVehicleHeading
		BREAK
		
		CASE BC_STRIPPER_INFERNUS
			vPlayerWalkToPos = <<-849.0348, 510.6906, 89.8218>>
			vHouseDoorPos = << -849.0408, 508.5767, 89.8218 >>
			vHouseInVehiclePos = <<-846.1005, 520.2202, 89.6217>>
			vHouseOnFootPos = <<-851.8972, 512.7300, 89.6217>>
			vAfterCutPos = <<-851.8746, 513.6746, 89.6217>>
			fAfterCutRot = 92.365
			fVehicleHeading = 293
			vHeliPos = <<-860.4819, 514.2496, 88.1473>>
			fHeliHeading = fVehicleHeading
		BREAK
//		CASE BC_STRIPPER_FUFU // hard to park here
//			vHouseDoorPos = << -516.55, 593.24, 120.84 >>
//			vHouseInVehiclePos = <<-519.9916, 575.0677, 119.9987>>
//			vHouseOnFootPos = << -512.26, 581.28, 120.77 >>
//			vAfterCutPos = << -516.55, 593.24, 120.84 >>
//			fAfterCutRot = -167.00
//			fVehicleHeading = 87.45
//		BREAK
//		CASE BC_STRIPPER_PEACH
//			vHouseDoorPos = << -456.7136, 92.7175, 62.5518 >>   
//			vHouseInVehiclePos = <<-449.4174, 99.9586, 62.1617>>
//			vHouseOnFootPos = << -456.8262, 107.5538, 63.3997 >>
//			vAfterCutPos =<< -457.1387, 104.2222, 63.3476 >>
//			fAfterCutRot = 348.7793
//			fVehicleHeading = 180.00
//		BREAK
		
		// non-stripper homes
//		CASE BC_FIXED_CAR
//			vHouseDoorPos = << -194.48, 141.08, 69.42 >>   
//			vHouseInVehiclePos = << -192.70, 132.82, 68.69 >>
//			vHouseOnFootPos = << -195.63, 137.95, 69.05 >>
//			vAfterCutPos = << -197.2565, 133.8592, 68.6438 >>
//			fAfterCutRot = 162.78
//		BREAK
		
		CASE BC_TAXI_LIZ
			vPlayerWalkToPos = <<-28.2427, -1555.8925, 29.6918>>
			vHouseDoorPos = << -24.8589, -1556.8461, 29.6819 >>
			vHouseInVehiclePos = << -41.8174, -1575.6086, 28.2831 >>
			vHouseOnFootPos = <<-25.3404, -1556.3406, 29.6919>>//<<-27.7515, -1570.0551, 28.3246>>
			vAfterCutPos = << -27.7382, -1570.5724, 29.3 >>
			fAfterCutRot = 181.3520
			fVehicleHeading = 47.9206 
			vHeliPos = vHouseInVehiclePos
			fHeliHeading = fVehicleHeading
		BREAK
		
		CASE BC_HITCHER_GIRL
			vPlayerWalkToPos = <<3313.4871, 5175.8306, 18.6190>>
			vHouseDoorPos = <<3310.8157, 5176.3306, 18.6190>>
			vHouseInVehiclePos = <<3334.3206, 5161.1221, 17.2996>>
			vHouseOnFootPos = <<3317.7876, 5171.7070, 17.4471>>
			vAfterCutPos = <<3318.0757, 5171.8052, 17.4385>>
			fAfterCutRot = 236.4579
			vHeliPos = <<3322.9268, 5148.6074, 17.3141>>
			fHeliHeading = 310.8648
		BREAK
		
		/*
		CASE STRIPPER_IVANA
		CASE STRIPPER_JO
		CASE STRIPPER_KRISTEN
		CASE STRIPPER_LISA
			vHouseDoorPos = <<1160.36, -1680.43, 34.39>>
			vHouseInVehiclePos = <<1162.72, -1704.46, 35.08>>
			vHouseOnFootPos = <<1159.90, -1700.96, 34.44>>
			vAfterCutPos = <<1159.84, -1683.97, 34.42>>
			fAfterCutRot = -178.29	
		BREAK
		*/
	ENDSWITCH
ENDPROC

// get attribs for home cam
PROC GET_HOME_CAMERA_ATTRIBUTES(VECTOR &vCamPos[NUM_BOOTY_CALL_CAMS], VECTOR &vCamRot[NUM_BOOTY_CALL_CAMS], FLOAT &fCamFOV[NUM_BOOTY_CALL_CAMS])
	
	SWITCH stripperID[0]
		CASE BC_STRIPPER_NIKKI

			vCamPos[0] = <<140.9085, -1885.5271, 25.6959>>
			vCamRot[0] = <<-9.0308, -0.0000, 126.3829>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<141.7120, -1884.9351, 30.7241>>
			vCamRot[1] = <<4.7025, -0.0000, 129.5531>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] = <<139.189865,-1885.865356,24.967369>>
			vCamRot[2] = <<-3.535496,0.081819,135.130814>>
			fCamFOV[2] = 50.0
			
			vCamPos[3] = <<133.9669, -1893.2922, 23.9573>>
			vCamRot[3] = <<-1.8669, -0.0329, -5.5828>>
			fCamFOV[3] = 50.0
		BREAK
		CASE BC_STRIPPER_JULIET

			vCamPos[0] = <<-186.5408, -1629.3278, 35.8275>>
			vCamRot[0] = <<-1.5588, 0.0000, -118.8025>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<-186.1923, -1629.5049, 48.7132>>
			vCamRot[1] = <<-1.2745, -0.0000, -116.9616>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] = <<-184.959503,-1632.193237,36.323029>>
			vCamRot[2] = <<-3.262451,0.073971,-100.675636>>
			fCamFOV[2] = 40.0
			
			vCamPos[3] = <<-164.7078, -1632.2499, 34.0944>>
			vCamRot[3] = <<-3.7303, 0.0222, 108.1824>>
			fCamFOV[3] = 40.0
		BREAK
		
		CASE BC_STRIPPER_SAPPHIRE
			vCamPos[0] = <<-208.6886, 112.8581, 73.0855>>
			vCamRot[0] = <<-0.1055, 0.0000, -156.0082>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<-208.9068, 113.3747, 85.2889>>
			vCamRot[1] = <<3.8438, -0.0000, -157.0767>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] =<<-206.235718,106.940987,72.075546>>
			vCamRot[2] = <<-11.583810,0.084975,-158.970230>>
			fCamFOV[2] = 45.0
			
			vCamPos[3] = <<-197.6886, 94.7063, 69.8140>>
			vCamRot[3] = <<0.5155, -0.0266, 48.8652>>
			fCamFOV[3] = 45.0
		BREAK
		CASE BC_STRIPPER_INFERNUS
			vCamPos[0] = <<-871.6285, 520.8466, 93.1945>>
			vCamRot[0] = <<-0.1377, -1.3933, -105.9309>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<-871.5255, 521.1066, 104.6356>>
			vCamRot[1] = <<3.1118, -1.3933, -105.9309>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] = <<-866.643616,514.612061,92.902748>>
			vCamRot[2] = <<-5.957637,-1.324997,-101.906235>>
			fCamFOV[2] = 40.0
			
			vCamPos[3] = <<-849.5522, 514.2322, 91.0095>>
			vCamRot[3] = <<-1.6260, -1.3198, 92.3739>>
			fCamFOV[3] = 40.0
			
		BREAK
		
		CASE BC_TAXI_LIZ
			vCamPos[0] =<<-27.6980, -1584.6459, 31.1034>>
			vCamRot[0] = <<0.0044, -0.0000, -1.9117>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<-27.6980, -1584.6459, 40.4849>>
			vCamRot[1] = <<7.9810, 0.0000, -1.9117>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] = <<-28.639044,-1579.740356,30.824232>>
			vCamRot[2] = <<-7.527712,0.030218,-14.330508>>
			fCamFOV[2] = 40.0
			
			vCamPos[3] = <<-28.6719, -1568.0903, 29.7492>>
			vCamRot[3] = <<-1.5414, -0.0000, -168.6005>>
			fCamFOV[3] = 40.0
		BREAK
		
		CASE BC_HITCHER_GIRL
			vCamPos[0] = <<3323.6306, 5148.8608, 20.9592>>
			vCamRot[0] = <<1.1547, 1.0064, 21.8733>>
			fCamFOV[0] = 40.0
			
			vCamPos[1] = <<3323.8440, 5148.6719, 28.2303>>
			vCamRot[1] = <<11.4168, 1.0064, 22.8025>>
			fCamFOV[1] = 40.0
			
			vCamPos[2] = <<3328.903809,5157.818359,20.579899>>
			vCamRot[2] = <<-3.634001,1.039937,30.448088>>
			fCamFOV[2] = 40.0
			
			vCamPos[3] = <<3315.3682, 5171.7715, 18.9066>>
			vCamRot[3] = <<-2.8773, 1.0064, -103.0585>>
			fCamFOV[3] = 40.0
		BREAK
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_PLAYER_WARP_POS_POST_SEX()
	SWITCH stripperID[0]
		CASE BC_STRIPPER_NIKKI
			RETURN <<130.7462, -1896.5129, 22.3246>>
		BREAK
		
		CASE BC_STRIPPER_JULIET
			RETURN <<-162.4315, -1635.1072, 32.6529>>
		BREAK
		
		CASE BC_STRIPPER_SAPPHIRE
			RETURN <<-197.6542, 92.3329, 68.5846>>
		BREAK
		
		CASE BC_STRIPPER_INFERNUS
			RETURN <<-848.9675, 510.0480, 89.8218>>
		BREAK
		
		CASE BC_TAXI_LIZ
			RETURN <<-28.5266, -1565.7926, 28.9180>>
		BREAK
		
		CASE BC_HITCHER_GIRL
			RETURN <<3315.0449, 5174.0801, 17.6765>>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC FLOAT GET_PLAYER_WARP_HEADING_POST_SEX()
	SWITCH stripperID[0]
		CASE BC_STRIPPER_NIKKI
			RETURN 325.6
		BREAK
		
		CASE BC_STRIPPER_JULIET
			RETURN 52.89
		BREAK
		
		CASE BC_STRIPPER_SAPPHIRE
			RETURN 14.5
		BREAK
		
		CASE BC_STRIPPER_INFERNUS
			RETURN 43.85
		BREAK
		
		CASE BC_TAXI_LIZ
			RETURN 193.5
		BREAK
		
		CASE BC_HITCHER_GIRL
			RETURN 233.2
		BREAK
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC PRINT_PICK_UP_BOOTY_CALL(BOOL bPickUpFromHouse)
	
	SWITCH stripperID[0]
		CASE BC_STRIPPER_JULIET
		CASE BC_STRIPPER_NIKKI
		CASE BC_STRIPPER_CHASTITY
		CASE BC_STRIPPER_CHEETAH
		CASE BC_STRIPPER_SAPPHIRE
		CASE BC_STRIPPER_INFERNUS
		CASE BC_STRIPPER_FUFU
		CASE BC_STRIPPER_PEACH
			IF bPickUpFromHouse
				PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_MEE4" , stripperID[0])
			ELSE
				PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_MEET" , stripperID[0])
			ENDIF
		BREAK
		
		CASE BC_TAXI_LIZ
			PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_LIZ" , stripperID[0])

		BREAK
		
		CASE BC_HITCHER_GIRL
			PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_HITCH" , stripperID[0])
		BREAK
	ENDSWITCH
	
ENDPROC

PROC PRINT_TAKE_BOOTY_CALL_HOME(BOOL bDropOffAtHome)
	
	SWITCH stripperID[0]
		CASE BC_STRIPPER_JULIET
		CASE BC_STRIPPER_NIKKI
		CASE BC_STRIPPER_CHASTITY
		CASE BC_STRIPPER_CHEETAH
		CASE BC_STRIPPER_SAPPHIRE
		CASE BC_STRIPPER_INFERNUS
		CASE BC_STRIPPER_FUFU
		CASE BC_STRIPPER_PEACH
			IF bDropOffAtHome
				PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_GO" , stripperID[0])
			ELSE
				PRINT_NOW("SCLUB_CLUB_GO", DEFAULT_GOD_TEXT_TIME, 1)
			ENDIF
		BREAK
		
		//CASE BC_FIXED_CAR
		CASE BC_TAXI_LIZ
		CASE BC_HITCHER_GIRL
			PRINT_NOW("SCLUB_HOME_GO3", DEFAULT_GOD_TEXT_TIME, 1)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GIVE_CALL_AND_LEAVE_TASK(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		
		SET_PED_PATH_AVOID_FIRE(ped, TRUE)

		IF GET_SCRIPT_TASK_STATUS( ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			CLEAR_SEQUENCE_TASK(sequence)
			OPEN_SEQUENCE_TASK(sequence)
				TASK_USE_MOBILE_PHONE_TIMED(NULL,8000) 
				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHouseDoorPos, 1.5/*PEDMOVE_WALK*/, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT)
			CLOSE_SEQUENCE_TASK(sequence)
			TASK_PERFORM_SEQUENCE(ped, sequence)
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Chick got phone & walk task" )
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_STRIPPER_BEING_LEFT_BEHIND(PED_INDEX pedStripper, BOOL bOnFootZDist = FALSE)

	VECTOR vPlayerCoords, vStripperCoords
	vPlayerCoords = GET_ENTITY_COORDS( PLAYER_PED_ID() )
	vStripperCoords = GET_ENTITY_COORDS( pedStripper )
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehPlayersLast = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF GET_PED_GROUP_INDEX(pedStripper) = PLAYER_GROUP_ID()
			IF IS_VEHICLE_DRIVEABLE( vehPlayersLast )
				IF NOT IS_PED_IN_VEHICLE(pedStripper, vehPlayersLast)
				AND VDIST(vPlayerCoords, vStripperCoords ) > 10.0
				
				
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bOnFootZDist
		AND IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_IS_NEAR)
		FLOAT fZDif = ABSF( vPlayerCoords.z - vStripperCoords.z )
		
		IF fZDif > 8.0
			RETURN TRUE
		ENDIF		
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC HANDLE_STRIPPER_TEXT_FOR_BEING_STOOD_UP()
	IF IS_FREEMODE()
		EXIT //stripper does not contact player in freemode
	ENDIF
	
	CC_TextPart ePartStripper
	ePartStripper = GET_TEXT_PARTID_FOR_STRIPPER(stripperID[0])

	CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEXT] Queueing sext to be sent ", GET_COMM_ID_DEBUG_STRING(TEXT_SEXT), " ", GET_TEXT_PART_DEBUG_STRING(ePartStripper), " ", GET_TEXT_PART_DEBUG_STRING(TPART_SEXT_STUP), ".")
	
	REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(
		TEXT_SEXT,
		ePartStripper,
		TPART_SEXT_STUP,
		CT_FRIEND,
		GET_CURRENT_PLAYER_PED_BIT(),
		GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(stripperID[0]),
		0)
							
ENDPROC


PROC SETUP_BOOTYCALL_WAIT_TIME(BOOL bHandleNearFail = FALSE)
	//DEBUG TIME OF DAY DEADLINES
	IF NOT Is_TIMEOFDAY_Valid(eTODCALLMade)
		eTODCALLMade = GET_CURRENT_TIMEOFDAY()
		ADD_TIME_TO_TIMEOFDAY(eTODCALLMade , 0, 0,GET_RANDOM_INT_IN_RANGE(BOOTY_CALL_WAIT_FOR_PLAYER_TIME_MIN, BOOTY_CALL_WAIT_FOR_PLAYER_TIME_MAX) )
		CDEBUG1LN(DEBUG_BOOTY_CALL,"Time of Day Setup")

	
	ELSE
		//Debug Print the TOD that the call will end if the player takes too long
		#IF IS_DEBUG_BUILD
			#IF CONST_iShowDebugInfo
//				TEXT_LABEL_63 sDebugTOD
//					
//				sDebugTOD = TIMEOFDAY_TO_TEXT_LABEL(eTODCALLMade)
//				sDebugTOD += "Booty Call Ends"
//				DRAW_DEBUG_TEXT_2D( sDebugTOD , <<0.1, 0.28, 0.0>>)
			#ENDIF
		#ENDIF
		
		IF bHandleNearFail
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
				IF IS_NOW_AFTER_TIMEOFDAY(eTODCALLMade)
				AND IS_ENTITY_OCCLUDED(stripperPed[0])
						IF stripperID[0] = BC_TAXI_LIZ
						OR stripperID[0] = BC_HITCHER_GIRL
							TEXT_LABEL_63 sCallLabel = "SC_CANCEL_"
							
							sCallLabel += ENUM_TO_INT(stripperID[0])
							
							ADD_PED_FOR_DIALOGUE(stripperConversation, 1, NULL, GET_BOOTY_CALL_NAME_AS_STRING(stripperID[0]))
							
							CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER( stripperConversation, GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(stripperID[0]),"SHAUD" ,sCallLabel, CONV_PRIORITY_MEDIUM)
						ELSE
							HANDLE_STRIPPER_TEXT_FOR_BEING_STOOD_UP()
						ENDIF
					
					GIVE_CALL_AND_LEAVE_TASK(stripperPed[0])
					
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
				ENDIF
			
			//Chick has called and cancelled now process.
			ELSE
				IF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
					CLEAR_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Player interupted phonecall" )
				
				//There's a bug in here somewhere, when the player receives a call, it auto hangs up and cancels the mission.
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND HAS_CELLPHONE_CALL_FINISHED()
				AND NOT WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
				
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Player cancelled the booty call & not hung up 1 "  ) 
					RESET_BOOTY_CALL_TO_INACTIVE(TRUE)
					MISSION_FAILED(FAIL_STRIPPER_CANCELLED)
				ENDIF
			ENDIF
		ENDIF
			
	ENDIF
	
	
	
ENDPROC
FUNC TEXT_LABEL_63 GET_BOOTY_CALL_CANCEL_LINE()
	
	TEXT_LABEL_63 sBootyCallLabel
	
	sBootyCallLabel = "BC_DECR_"
	sBootyCallLabel += ENUM_TO_INT(stripperID[0])
	
	CDEBUG1LN(DEBUG_BOOTY_CALL,"[CELLPHONE] Cancel Line = ", sBootyCallLabel)
	RETURN sBootyCallLabel
	
ENDFUNC

FUNC STRING GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL()
	
	STRING sWaypointRecordingName
	
	IF stripperID[0] = BC_STRIPPER_NIKKI
		sWaypointRecordingName = "BC_NIKKI_H_F"
	ENDIF
	
	RETURN sWaypointRecordingName
ENDFUNC

PROC REQUEST_SPECIFIC_ASSETS_FOR_BOOTYCALL()

	IF NOT IS_STRING_NULL_OR_EMPTY(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())
		REQUEST_WAYPOINT_RECORDING(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())
		CDEBUG1LN(DEBUG_BOOTY_CALL,"Requesting Waypoint Recording:  ", GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL() )
	ENDIF

ENDPROC

FUNC BOOL HAVE_SPECIFIC_ASSETS_FOR_BOOTYCALL_LOADED()
	IF IS_STRING_NULL_OR_EMPTY(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())	
		RETURN TRUE
	ELSE
		IF GET_IS_WAYPOINT_RECORDING_LOADED(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Waiting to Load.....Waypoint Recording ",  GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL()  )
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


// init script
FUNC BOOL INIT_MISSION(TAKE_HOME_STRIPPER_INFO& takeHomeInfo, BOOL bPickUpFromHome)
	VECTOR vStorePos
	FLOAT fStoreRot
	
	
	
	GET_STRIPPER_HOME_POSITIONS()
	GET_INIT_TAKE_HOME_STRIPPER_LOC(vStorePos, fStoreRot, bPickUpFromHome)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF (fInitDistance = -1)
			fInitDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Init Distance is: ", fInitDistance)
		ENDIF
		
		IF (candidateID = NO_CANDIDATE_ID)
			candidateID = takeHomeInfo.myCandidateID
		ENDIF
		
		//Set up the booty call ped--------------------------------------------------------------------------------------
		INT i
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_REQUEST_MODELS)
		
			ADD_STREAMED_MODEL(takeHomeModels, GET_BOOTYCALL_MODEL_ENUM(takeHomeInfo.stripperID[0]))
			REQUEST_ALL_MODELS(takeHomeModels)
			SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_REQUEST_MODELS)
			REQUEST_ANIM_DICT("mini@strip_club@idles@stripper")
			REQUEST_ANIM_DICT("gestures@f@standing@casual")
			REQUEST_SPECIFIC_ASSETS_FOR_BOOTYCALL()
			RETURN FALSE
	
		ELIF NOT ARE_MODELS_STREAMED(takeHomeModels)
		OR NOT HAS_ANIM_DICT_LOADED("mini@strip_club@idles@stripper")
		OR NOT HAVE_SPECIFIC_ASSETS_FOR_BOOTYCALL_LOADED()
		
			RETURN FALSE
		ENDIF
		
		IF bIsMp
			IF NOT CAN_REGISTER_MISSION_PEDS(1)
				PRINTLN("Can't register bootycall ped")
				RETURN FALSE
			ENDIF
		ENDIF
		
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_STRIPPER_CREATED)
			DEBUG_MESSAGE("Creating stripper and adding to group")
			#IF IS_DEBUG_BUILD
			DEBUG_MESSAGE("INIT_MISSION: takeHomeInfo.stripperID[", GET_STRING_FROM_INT(0), "]: ", GET_BOOTY_CALL_NAME_AS_STRING(takeHomeInfo.stripperID[0]))		
			#ENDIF
	
			stripperID[0] = takeHomeInfo.stripperID[0]
	
			stripperPed[0] = CREATE_PED(PEDTYPE_MISSION, GET_BOOTYCALL_MODEL_ENUM(takeHomeInfo.stripperID[0]), vStorePos, fStoreRot) // takeHomeInfo.vInitPos[i], takeHomeInfo.fInitRot[i])
			SETUP_SCLUB_STRIPPER_VARIATION(stripperPed[0], stripperID[0])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(stripperPed[i], TRUE)
			SET_PED_KEEP_TASK(stripperPed[0], TRUE)
			SET_PED_DIES_WHEN_INJURED(stripperPed[0], TRUE)	
			SET_JACKET_VARIATION_ON_STRIPPER(stripperPed[0])
			
			TASK_PLAY_ANIM(stripperPed[0], "mini@strip_club@idles@stripper", "stripper_idle_01", NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_LOOPING)
			TASK_LOOK_AT_ENTITY(stripperPed[i], PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			//SET_PED_AS_GROUP_MEMBER(stripperPed[i], PLAYER_GROUP_ID())
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Add Ped 1" ) 
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(stripperPed[0], VS_FRONT_RIGHT)
			SET_AMBIENT_VOICE_NAME(stripperPed[0],GET_BOOTY_CALL_NAME_AS_STRING(takeHomeInfo.stripperID[0]))
			ADD_PED_FOR_DIALOGUE(stripperConversation, 1, stripperPed[0], GET_BOOTY_CALL_NAME_AS_STRING(takeHomeInfo.stripperID[0]))
	
			
			SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_STRIPPER_CREATED)
			
			//DEBUG BYPASS
			#IF IS_DEBUG_BUILD
			IF g_bTurnOnLukeHCameraDebug
				IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_HOME_SCENE_PLAYING)
					currentMissionStage = DEBUG_SKIP_MODE
				ENDIF
				RETURN TRUE
			ENDIF
			#ENDIF
		ENDIF
		
		
		//If player isn't in area-------------------------------------------------------------------------------------------------------------------
		IF VDIST( GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos) > CONST_fCleanupDistance
		
		
			IF CAN_DO_STRIPPER_HOME_GOD_TEXT()
				IF NOT IS_BITMASK_AS_ENUM_SET( iGeneralBitFlags, HOUSE_MEET_UP_TEXT )
					
					PRINT_PICK_UP_BOOTY_CALL(bPickUpFromHome)
					
					CREATE_STRIPPER_BLIP(0)
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_UP_TEXT)
				ENDIF
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_INIT_CANCEL)
			AND NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
				IF IS_CALLING_CONTACT(GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(takeHomeInfo.stripperID[0]))
					TEXT_LABEL_63 sCallLabel = "BC_PLYRC_"
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "TREVOR")
						sCallLabel += "T"
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "MICHAEL")
						sCallLabel += "M"
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
						sCallLabel += "F"
					ENDIF
					
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[CELLPHONE] Cancel Player Line = ", sCallLabel)
					ADD_PED_FOR_DIALOGUE(stripperConversation, 3, NULL, GET_BOOTY_CALL_NAME_AS_STRING(takeHomeInfo.stripperID[0]))
					
					//Get booty call response line
					TEXT_LABEL_63 sBootyCallCancelLabel
					sBootyCallCancelLabel = GET_BOOTY_CALL_CANCEL_LINE()
					
					PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES(stripperConversation,GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(takeHomeInfo.stripperID[0]),"BCAUD",sCallLabel,sCallLabel,sBootyCallCancelLabel,sBootyCallCancelLabel,CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
						
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_INIT_CANCEL)
				ENDIF
			
				//Player takes too long to pick up booty call.
				IF IS_TIMEOFDAY_VALID( eTODCALLMade )
					IF IS_NOW_AFTER_TIMEOFDAY(eTODCALLMade)
						IF takeHomeInfo.stripperID[0] = BC_TAXI_LIZ
						OR takeHomeInfo.stripperID[0] = BC_HITCHER_GIRL
							TEXT_LABEL_63 sCallLabel = "SC_CANCEL_"
							
							sCallLabel += ENUM_TO_INT(takeHomeInfo.stripperID[0])
							
							ADD_PED_FOR_DIALOGUE(stripperConversation, 1, NULL, GET_BOOTY_CALL_NAME_AS_STRING(takeHomeInfo.stripperID[0]))
							
							CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER( stripperConversation, GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(takeHomeInfo.stripperID[0]),"SHAUD" ,sCallLabel, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
						ELSE
							HANDLE_STRIPPER_TEXT_FOR_BEING_STOOD_UP()
						ENDIF
						SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
					ENDIF
				ENDIF
				
			ELIF WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
				CLEAR_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_INIT_CANCEL)
				CLEAR_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_GIRL_CANCEL)
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Player interupted phonecall" )
				
			//ELIF NOT IS_CALLING_CONTACT(GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(takeHomeInfo.stripperID[0]))
			ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND HAS_CELLPHONE_CALL_FINISHED()
			AND NOT WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
			
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Player cancelled the booty call & not hung up 2 " ) 
				INCREMENT_STRIPPER_NUM_BOOTY_DENIALS_BY_PLAYER(takeHomeInfo.stripperID[0])
				RESET_BOOTY_CALL_TO_INACTIVE(TRUE)
				MISSION_FAILED(FAIL_STRIPPER_CANCELLED)
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_INIT_CANCEL_HELP)
				IF NOT IS_PHONE_ONSCREEN()
					PRINT_HELP_WITH_STRIPPER_NAMES( "SCLUB_HOME_C_H" ,stripperID[0])
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_INIT_CANCEL_HELP)
				ENDIF
			ENDIF
			
			//If my current distance is greater then my initail distance, plus some buffer, then I abbandon the stripper
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos) > (fInitDistance  + (CONST_fAbandoned*3))
				MISSION_FAILED(FAIL_STRIPPER_ABANDONED)
			ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos) < fInitDistance 
				//update initial distance as you approach the stripper
				fInitDistance = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				FLOAT fDebugThis = VDIST( GET_ENTITY_COORDS(PLAYER_PED_ID()), vStorePos)
				CDEBUG1LN(DEBUG_BOOTY_CALL,"INIT MISSION - 1442, Dist = ",fDebugThis  )
			#ENDIF
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
	
	//Setup Player Conversations---------------------------------------------------------------------------------
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "TREVOR")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(stripperConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC CHECK_FAIL_CONDITIONS()
	
	IF IS_PED_INJURED(stripperPed[0])
		IF NOT DOES_ENTITY_EXIST(stripperPed[0])
			PRINTLN("Bootycall ", 0, " doesn't exist")
		ELSE
			IF IS_ENTITY_DEAD(stripperPed[0])
				PRINTLN("Bootycall ", 0, " is dead")
			ENDIF
		ENDIF
		
		DEBUG_MESSAGE("failling mission due to stripper being injured")
		MISSION_FAILED(FAIL_STRIPPER_DEAD)
	ENDIF

ENDPROC

PROC ADD_STRIPPERS_TO_GROUP()
	IF NOT IS_PED_IN_GROUP(stripperPed[0])
	AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(stripperPed[0]) ) < 10.0
		SET_PED_AS_GROUP_MEMBER(stripperPed[0], PLAYER_GROUP_ID())
		SET_PED_GROUP_MEMBER_PASSENGER_INDEX(stripperPed[0], INT_TO_ENUM(VEHICLE_SEAT, 0))
		CDEBUG1LN(DEBUG_BOOTY_CALL,"Adding strippers to player's group" )
	ENDIF	
ENDPROC

PROC HANDLE_NICE_RIDE_CONVERSATION()
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_NICE_CAR)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PLAYER_DRIVING_A_NICE_RIDE(stripperID[0])	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "NICE_CAR", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_NICE_CAR)
					PRINTLN("PLAYING NICE CAR LINE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// setup camera
PROC SETUP_HOME_CAM()

	INT iCamIterator
	VECTOR vHomeCamPos[NUM_BOOTY_CALL_CAMS], vHomeCamRot[NUM_BOOTY_CALL_CAMS]
	FLOAT fHomeCamFOV[NUM_BOOTY_CALL_CAMS]
	
	GET_HOME_CAMERA_ATTRIBUTES(vHomeCamPos, vHomeCamRot, fHomeCamFOV)
	
	REPEAT NUM_BOOTY_CALL_CAMS iCamIterator
	
		IF NOT DOES_CAM_EXIST(homeCam[iCamIterator])
		
			homeCam[iCamIterator] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,vHomeCamPos[iCamIterator],vHomeCamRot[iCamIterator],fHomeCamFOV[iCamIterator],FALSE)
			SHAKE_CAM(homeCam[iCamIterator],"HAND_SHAKE",0.3)
	
		ENDIF
		
	ENDREPEAT
	
	//Set up TOD cam based on the position 
	tTimeLapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
	
    ADD_CAM_SPLINE_NODE(tTimeLapse.splineCamera, vHomeCamPos[0], vHomeCamRot[0], CONST_iHomePanUpTODTime)
    ADD_CAM_SPLINE_NODE(tTimeLapse.splineCamera, vHomeCamPos[1], vHomeCamRot[1], CONST_iHomePanUpTODTime)
    SET_CAM_FOV(tTimeLapse.splineCamera, fHomeCamFOV[0])

ENDPROC

PROC SET_ENTITY_FACING(ENTITY_INDEX entity, vector vLookAt)
	VECTOR vEntity = GET_ENTITY_COORDS(entity)
	float fAngle
	float dX = vLookAt.x - vEntity.x
	float dY = vLookAt.y - vEntity.y
	
	if dY != 0
		fAngle = ATAN2(dX,dY)
	ELSE
		if dX < 0
			fAngle = -90
		ELSE
			fAngle = 90
		ENDIF
	ENDIF
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as GET_ENTITY_HEADING()
	fAngle *= -1.0
	
	SET_ENTITY_HEADING(entity, fAngle)
ENDPROC

// go to door
PROC GIVE_WALK_TO_DOOR_TASK(PED_INDEX ped,VECTOR vWalkToPos)
	IF NOT IS_PED_INJURED(ped)
		
		SET_PED_PATH_AVOID_FIRE(ped, TRUE)

		IF GET_SCRIPT_TASK_STATUS( ped, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			CLEAR_SEQUENCE_TASK(sequence)
			OPEN_SEQUENCE_TASK(sequence)
				IF IS_PED_IN_ANY_VEHICLE(ped)
					TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_WAIT_FOR_ENTRY_POINT_TO_BE_CLEAR )
				ENDIF
				
				//Player only
				IF ped = PLAYER_PED_ID()
				
					FLOAT fDistanceToWalkToPos = VDIST( GET_ENTITY_COORDS(ped), vPlayerWalkToPos)
					FLOAT fDistanceToFrontDoor = VDIST( GET_ENTITY_COORDS(ped), vHouseDoorPos )
				
					IF fDistanceToFrontDoor < fDistanceToWalkToPos
						TASK_TURN_PED_TO_FACE_ENTITY(NULL,stripperPed[0])
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())
							TASK_FOLLOW_WAYPOINT_RECORDING(NULL,GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL(),0,EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vPlayerWalkToPos, PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_DONT_AVOID_OBJECTS)
						ENDIF
					ENDIF		
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[COORDS]DistanceTo Walk Pos = ",fDistanceToWalkToPos, " Dist to Door =" , fDistanceToFrontDoor  )
				ENDIF
				
				IF ped != PLAYER_PED_ID()
					//If you have a waypoint recording use it, if not proceed to a simple nav task
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL())
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL,GET_WAYPOINT_RECORDING_NAME_FOR_BOOTY_CALL(),0,EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS | EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS)
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWalkToPos, PEDMOVE_WALK, -1, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
					ENDIF
				
				
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(ped, vWalkToPos) > 5.0
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "gestures@f@standing@casual", "gesture_come_here_soft")
						TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(3000,6000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "gestures@f@standing@casual", "gesture_come_here_soft")
						TASK_STAND_STILL(NULL,GET_RANDOM_INT_IN_RANGE(3000,6000))
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "gestures@f@standing@casual", "gesture_come_here_soft")
						TASK_PLAY_ANIM(NULL, "mini@strip_club@idles@stripper", "stripper_idle_01", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
					ENDIF
				ENDIF
			CLOSE_SEQUENCE_TASK(sequence)
			TASK_PERFORM_SEQUENCE(ped, sequence)
			
			PRINTLN("Telling ped to walk to door at ", vWalkToPos)
		ENDIF
	ENDIF
ENDPROC

PROC MOVE_VEHICLE_TO_PARK_COORD(VEHICLE_INDEX iPlayerPedVeh)
	IF NOT IS_ENTITY_DEAD(iPlayerPedVeh)
		FREEZE_ENTITY_POSITION(iPlayerPedVeh, TRUE)
		VECTOR vVehParkFrom = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vHouseInVehiclePos, fVehicleHeading, <<0, CONST_fYParkOffset, 0>>)
		vVehParkFrom.z += 1.0
		SET_ENTITY_COORDS(iPlayerPedVeh, vVehParkFrom)
		SET_VEHICLE_ON_GROUND_PROPERLY(iPlayerPedVeh)
		SET_ENTITY_HEADING(iPlayerPedVeh, fVehicleHeading)
		FREEZE_ENTITY_POSITION(iPlayerPedVeh, FALSE)
	ENDIF
ENDPROC

// park car
PROC GIVE_PARK_TASK(PED_INDEX iPlayerPed, VEHICLE_INDEX iPlayerPedVeh)
	IF NOT IS_PED_INJURED(iPlayerPed) AND NOT IS_ENTITY_DEAD(iPlayerPedVeh)
		TASK_VEHICLE_PARK(iPlayerPed, iPlayerPedVeh, vHouseInVehiclePos, fVehicleHeading, PARK_TYPE_PULL_OVER)	
	ENDIF
ENDPROC

FUNC INT GET_NEXT_STRIPPER_BANTER_IN_SEQUENCE()

	
	IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF)
		SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF)
		RETURN 0
		
	ELIF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_1)
		SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_1)
		RETURN 1
	
	ELIF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_2)
		SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_2)
		RETURN 2
	
	ELIF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_3)
		SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_3)
		RETURN 3
		
	ELIF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_DONE)
		SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_DONE)
		CDEBUG1LN(DEBUG_BOOTY_CALL,"GET_NEXT_STRIPPER_BANTER_IN_SEQUENCE - Banter Finished" )
	ENDIF
	
	RETURN -1
ENDFUNC

PROC DO_STAGE_MANAGE_STRIPPER_BANTER(FLOAT fStripperRange, VECTOR vHousePos)
	
	//Handle player smashing into a new car
	IF NOT IS_BITMASK_AS_ENUM_SET( iBanterBitFlags, HOUSE_BANTER_STOLEN_CAR)
		IF DID_BOOTYCALL_WITNESS_PLAYER_STEALING_CAR(stripperPed[0] )
		
			IF stripperID[0] = BC_TAXI_LIZ		
				CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_STEALC")
			ELSE
				PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"GENERIC_SHOCKED_MED",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
			ENDIF
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty call witnessed player stealing car." )
			
			//Piggy backing on this timer, figure it can't hurt, as it just will probably delay dialogue
			RESTART_TIMER_NOW(tCarCrash)
			SET_BITMASK_AS_ENUM( iBanterBitFlags, HOUSE_BANTER_STOLEN_CAR )
		ENDIF
		
	ELIF (GET_TIMER_IN_SECONDS(tCarCrash) > 10.0)
		CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_STOLEN_CAR)
		RESTART_TIMER_NOW(tCarCrash)
	ENDIF
			
			
	IF fStripperRange >	5.0
	OR NOT CAN_DO_STRIPPER_HOME_GOD_TEXT()
	OR IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_BJ_STREAMED)
		EXIT
	ENDIF
	
	IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_NEAR_HOUSE)
	AND NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_BJ_FINISHED)
		IF VDIST(vHousePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) < CONST_fNearHouse
		
			IF stripperID[0] = BC_TAXI_LIZ	
			OR stripperID[0] = BC_HITCHER_GIRL	
				IF CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_NEAR")
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_NEAR_HOUSE)
				ENDIF
			ELSE
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_NEAR_HOUSE)	
			ENDIF
			
			CLEAR_AREA_OF_VEHICLES(vHouseInVehiclePos,5.0)
		ENDIF
	ENDIF
	
	

	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	VEHICLE_INDEX iHousePlayerVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	IF NOT IS_VEHICLE_DRIVEABLE(iHousePlayerVehicle)
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_NEW_CAR)
			PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "NEED_A_VEHICLE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
			SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_NEW_CAR)
			RESTART_TIMER_NOW(tBanter)
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
			CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
		ENDIF
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_TOO_SLOW)
			CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_SLOW)
		ENDIF
	ELSE
		FLOAT fHouseVehicleSpeed = GET_ENTITY_SPEED(iHousePlayerVehicle)
		FLOAT fEstVehicleMaxSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(iHousePlayerVehicle)
		
		IF fHouseVehicleSpeed < (fEstVehicleMaxSpeed*0.9)
		OR fHouseVehicleSpeed > (fEstVehicleMaxSpeed*0.1)
			RESTART_TIMER_NOW(tSpeed)
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
			IF (GET_TIMER_IN_SECONDS(tBanter) > 15.0)
			AND (GET_TIMER_IN_SECONDS(tSpeed) > 5.0)
				PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "DRIVEN_FAST", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
				RESTART_TIMER_NOW(tBanter)
			ENDIF
		ELIF (GET_TIMER_IN_SECONDS(tBanter) < 5.0)
			CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
			RESTART_TIMER_NOW(tBanter)
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_TOO_SLOW)
			IF (GET_TIMER_IN_SECONDS(tBanter) > 15.0)
			AND (GET_TIMER_IN_SECONDS(tSpeed) > 5.0)
				PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "DRIVEN_SLOW", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_FAST)
				RESTART_TIMER_NOW(tBanter)
			ENDIF
		ELIF (GET_TIMER_IN_SECONDS(tBanter) < 5.0)
			CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_TOO_SLOW)
			RESTART_TIMER_NOW(tBanter)
		ENDIF
		
		//B*1130487 - player crashing------------
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_CRASHED_CAR)
			IF DID_PLAYERS_VEHICLE_TAKE_DAMAGE_WHILE_DRIVING(stripperPed[0],iVehicleHealth )
				IF GET_TIMER_IN_SECONDS(tCarCrash) > 5.0
					PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "CRASH_GENERIC_DRIVEN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_CRASHED_CAR)
					RESTART_TIMER_NOW(tBanter)
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Player's car took damage" )
				ENDIF
			ENDIF
		ELIF (GET_TIMER_IN_SECONDS(tCarCrash) > 5.0)
			CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_CRASHED_CAR)
			RESTART_TIMER_NOW(tCarCrash)
		ENDIF
		
		//Play Banter
		
		IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF_DONE)
			IF (GET_TIMER_IN_SECONDS(tBanter) > 30.0)
			OR (GET_TIMER_IN_SECONDS(tBanter) > 10.0 AND NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_ONE_OFF))
				PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "BOOTY_FLIRT", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[DIALOGUE] Booty Flirt triggered" )
				
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_ONE_OFF)
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_PLAYER_RESPONSE)
				RESTART_TIMER_NOW(tBanter)
				
			ELIF IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_PLAYER_RESPONSE)
				
				IF NOT IS_AMBIENT_SPEECH_PLAYING(stripperPed[0])
				AND GET_TIMER_IN_SECONDS(tBanter) > 4.0
				
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "BOOTY_FLIRT_RESP", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					CLEAR_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_PLAYER_RESPONSE)
					
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[DIALOGUE] Booty Flirt PLAYER RESPONSE triggered" )
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
					#IF CONST_iShowDebugInfo
//						TEXT_LABEL_23 sDebugBanterTimer
//						sDebugBanterTimer = "Banter In: "
//						sDebugBanterTimer += ROUND(30.0 - GET_TIMER_IN_SECONDS_SAFE(tBanter))
//						DRAW_DEBUG_TEXT_2D(sDebugBanterTimer, <<0.1, 0.16,0.0>>)
					#ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_STAGE_GET_CORRECT_BJ_TRIGGER()
	//add in radio stuff whenever we can
	RETURN TRUE
ENDFUNC

FUNC BOOL DO_STAGE_CAN_DO_BLOW_JOB()
	IF NOT CAN_DO_STRIPPER_HOME_GOD_TEXT()
		RETURN FALSE
	ENDIF	
	
	VECTOR vStorePos
	FLOAT fStoreRot
	GET_INIT_TAKE_HOME_STRIPPER_LOC(vStorePos, fStoreRot, FALSE)
	
	#IF IS_DEBUG_BUILD
		#IF CONST_iShowDebugInfo
			//Draw Info to Screen------------------------------------------------------------
//			IF IS_STRIPPER_IN_VEHICLE_SHE_CAN_GIVE_A_BJ_IN()
//				DRAW_DEBUG_TEXT_2D("VEHICLE BJ : YES ", <<0.1, 0.2,0.0>>)
//			ELSE
//				DRAW_DEBUG_TEXT_2D("VEHICLE BJ : NO ", <<0.1, 0.2,0.0>>,255,0,0)
//			ENDIF
//			
//			TEXT_LABEL_23 sDebugSexts
//			sDebugSexts = "# Sexts:  "
//			sDebugSexts += 	GET_NUM_SEXTS_SENT_BY_STRIPPER(stripperID[0])
//			IF GET_NUM_SEXTS_SENT_BY_STRIPPER(stripperID[0]) = 0
//				DRAW_DEBUG_TEXT_2D(sDebugSexts, <<0.1, 0.22,0.0>>, 255,0,0)
//			ELSE
//				DRAW_DEBUG_TEXT_2D(sDebugSexts, <<0.1, 0.22,0.0>>)
//			ENDIF
//			
//			TEXT_LABEL_31 sDebugHouseDistance
//			sDebugHouseDistance = "House Dist:  "
//			sDebugHouseDistance += 	ROUND(VDIST(vHouseInVehiclePos, GET_ENTITY_COORDS(PLAYER_PED_ID())))
//			DRAW_DEBUG_TEXT_2D(sDebugHouseDistance, <<0.1, 0.24,0.0>>)
//			
//			TEXT_LABEL_31 sDebugStorePos
//			sDebugStorePos = "Store Pos Dist:  "
//			sDebugStorePos += 	ROUND(VDIST(vStorePos, GET_ENTITY_COORDS(PLAYER_PED_ID())))
//			DRAW_DEBUG_TEXT_2D(sDebugStorePos, <<0.1, 0.26,0.0>>)
			//End Info------------------------------------------------------------------------
			
			IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB))
				RETURN TRUE	
			ENDIF
		#ENDIF
	#ENDIF
	
	IF (VDIST(vHouseInVehiclePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > CONST_fNearHouse*2
	AND VDIST(vStorePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) > CONST_fNearHouse/2)
	AND (GET_NUM_SEXTS_SENT_BY_STRIPPER(stripperID[0]) > 1)
	AND IS_STRIPPER_IN_VEHICLE_SHE_CAN_GIVE_A_BJ_IN()
	//OR DO_STAGE_GET_CORRECT_BJ_TRIGGER())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_STAGE_MANAGE_BLOW_JOB()
	IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_BJ_TRIGGERED)
		IF DO_STAGE_CAN_DO_BLOW_JOB()
			SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_BJ_TRIGGERED)
		ELSE
			EXIT
		ENDIF
	ENDIF
		
	STRING sBJAnimDictionary = GET_ANIM_DICT_FOR_SEX_FOR_VEHICLE_PED_IS_IN(stripperPed[0])
	IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_BJ_ACTIVE)
		IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_BJ_STREAMED)
			IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_BJ)
				IF stripperID[0] = BC_TAXI_LIZ
				OR stripperID[0] = BC_HITCHER_GIRL
					CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_BJ")
				ENDIF
				SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_BJ)
			ELSE			
				REQUEST_ANIM_DICT(sBJAnimDictionary)
				SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_BJ_STREAMED)
			ENDIF
		ELIF HAS_ANIM_DICT_LOADED(sBJAnimDictionary)
			TASK_PED_TO_HAVE_SEX_IN_VEHICLE(stripperPed[0],TRUE)

			SET_PED_KEEP_TASK(stripperPed[0], TRUE)
			START_TIMER_NOW(tBJ)
			SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_BJ_ACTIVE)
		ENDIF
	ELSE
		IF IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_BJ_STREAMED)
			BOOL bCleanupBJ = FALSE
			BOOL bDoingMoneyShot = IS_ENTITY_PLAYING_ANIM(stripperPed[0], sBJAnimDictionary, GET_ANIM_NAME_FOR_SEX_STATE(SEX_ACTION_TO_PROP_P2,TRUE,TRUE,IS_VEHICLE_LOW_FOR_SEX(iTakeHomeVehicle)))
			BOOL bPlayerShootingFromCar = IS_PED_SHOOTING(PLAYER_PED_ID())
			BOOL bBJIsComplete = (GET_SCRIPT_TASK_STATUS(stripperPed[0], SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK)
			BOOL bPlayerInVeh = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			BOOL bHasCarCollidedWithAnything = FALSE
			
			IF bPlayerInVeh
				IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					bHasCarCollidedWithAnything	= (HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND (GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) > 5.0))
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			OR (GET_TIMER_IN_SECONDS(tBJ) > 35.0)
			OR (VDIST(vHouseInVehiclePos, GET_ENTITY_COORDS(PLAYER_PED_ID())) < CONST_fNearHouse)
			OR bPlayerShootingFromCar
			OR bHasCarCollidedWithAnything
				IF NOT bBJIsComplete
				AND NOT bDoingMoneyShot
					TASK_PLAY_ANIM(stripperPed[0], sBJAnimDictionary, GET_ANIM_NAME_FOR_SEX_STATE(SEX_ACTION_TO_PROP_P2,TRUE,TRUE,IS_VEHICLE_LOW_FOR_SEX(iTakeHomeVehicle)), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
				ENDIF
				IF bPlayerShootingFromCar
					iRecordBlockPersistTimer = 2000	//B* 2463715
				ENDIF
				bCleanupBJ = TRUE
			ELIF bBJIsComplete
				bCleanupBJ = TRUE
			ENDIF
			
			IF bDoingMoneyShot
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			ENDIF
			
			IF bCleanupBJ
			AND bBJIsComplete
			AND NOT bDoingMoneyShot
				IF NOT bPlayerShootingFromCar
				AND bPlayerInVeh
					IF bHasCarCollidedWithAnything
						SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_ENTITY_HEALTH(PLAYER_PED_ID())-CEIL(GET_PED_MAX_HEALTH(PLAYER_PED_ID())*0.1))
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(),"GENERIC_CURSE_HIGH",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					ELSE	
						PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(),"SEX_CLIMAX",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)	
					ENDIF
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_BJ_FINISHED)
				ENDIF
				REMOVE_ANIM_DICT(sBJAnimDictionary)
				CLEAR_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_BJ_STREAMED)
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_BJD)
				IF (GET_TIMER_IN_SECONDS(tBJ) > ((CONST_iBJDuration/1000)/3))
					PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(),"SEX_GENERIC",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"SEX_ORAL",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_BJD)
				ENDIF
			ENDIF
			
			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2246108
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_HOUSE_VEHICLE_BLIP(VECTOR vGoToPos)
		
	IF DOES_BLIP_EXIST(houseFootBlip)
		REMOVE_BLIP(houseFootBlip)
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	VEHICLE_INDEX playersVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	
	IF DOES_ENTITY_EXIST(playersVehicle) AND NOT IS_ENTITY_DEAD(playersVehicle)
		IF IS_PED_IN_ANY_VEHICLE(stripperPed[0])
			IF IS_PED_IN_VEHICLE(stripperPed[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))	
				
				IF NOT DOES_BLIP_EXIST(houseVehicleBlip)
					//GET_GROUND_Z_FOR_3D_COORD(vGoToPos)
					houseVehicleBlip = CREATE_BLIP_FOR_COORD(vGoToPos, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_STRIPPER_FROM_GROUP()
	IF DOES_ENTITY_EXIST(stripperPed[0]) AND NOT IS_ENTITY_DEAD(stripperPed[0])
		STOP_PED_SPEAKING(stripperPed[0],TRUE)
		IF IS_PED_IN_GROUP(stripperPed[0])
			PRINTLN("remvoing from group")
			REMOVE_PED_FROM_GROUP(stripperPed[0])
			CPRINTLN( DEBUG_BOOTY_CALL, "REMOVING STRIPPER FROM GROUP!!!" )
			DEBUG_PRINTCALLSTACK()
		ENDIF
	ENDIF
ENDPROC

PROC DO_STAGE_HANDLE_TRANSPORTATION_MODE()
	CLEAR_STRIPPER_BLIPS()
	
	RESTART_TIMER_NOW(tBanter)
	
	IF IS_PED_ON_FOOT(PLAYER_PED_ID())
		// player is on foot
		IF DOES_BLIP_EXIST(houseVehicleBlip)
			REMOVE_BLIP(houseVehicleBlip)
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(houseFootBlip)
			houseFootBlip = CREATE_BLIP_FOR_COORD(vHouseOnFootPos)
			CDEBUG1LN(DEBUG_BOOTY_CALL,"DO_STAGE_HANDLE_TRANSPORTATION_MODE" )
		ENDIF
		
		DEBUG_MESSAGE("DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_FOOT")
		
		RESTART_TIMER_NOW(tWalking)
		vStarttWalkLoc = GET_ENTITY_COORDS(stripperPed[0])
		eGoToHouseState = GO_TO_HOUSE_FOOT
		CDEBUG1LN(DEBUG_BOOTY_CALL,"->GO_TO_HOUSE_FOOT" )
	ELSE
		RESTART_TIMER_NOW(tCarCrash)
		MANAGE_HOUSE_VEHICLE_BLIP(vHouseInVehiclePos)
		DEBUG_MESSAGE("DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_DRIVE")
		eGoToHouseState = GO_TO_HOUSE_DRIVE
		CDEBUG1LN(DEBUG_BOOTY_CALL,"->GO_TO_HOUSE_DRIVE" )
	ENDIF
ENDPROC

// stage: go to the house
PROC DO_STAGE_GO_TO_HOUSE(BOOL bTakeHomeMethod)
	
	// check mission fail conditions
	CHECK_FAIL_CONDITIONS()
	TEXT_LABEL sStripperName

	IF IS_ENTITY_DEAD(stripperPed[0])
		EXIT //should never happen because of the above check, not even sure why this is here
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHouseInVehiclePos + <<1.0, 1.0, 0>>*10.0, vHouseInVehiclePos+ <<-1.0, -1.0, 0>>*10.0, FALSE)
	
	FLOAT fDistToStripper = VDIST( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(stripperPed[0]))
	
	//PRINTLN("Distance to bootycall ", fDistToStripper ," dist till house retrun ", (CONST_fAbandoned/2))
	
	IF eGoToHouseState != GO_TO_HOUSE_RETURN
		IF fDistToStripper >= (CONST_fAbandoned/2)
		AND CAN_DO_STRIPPER_HOME_GOD_TEXT()
		AND NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_IS_NEAR)
		
		OR IS_STRIPPER_BEING_LEFT_BEHIND(stripperPed[0])
		
			PRINTLN("Setting to GO_TO_HOUSE_RETURN")
			
			
			
			IF DOES_BLIP_EXIST(houseFootBlip)
				REMOVE_BLIP(houseFootBlip)
			ENDIF
			IF DOES_BLIP_EXIST(houseVehicleBlip)
				REMOVE_BLIP(houseVehicleBlip)
			ENDIF	
			CREATE_STRIPPER_BLIP(0)
		
			PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_R" , stripperID[0])
		
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Stripper is being left behind - GO_TO_HOUSE_RETURN" )
			eGoToHouseState = GO_TO_HOUSE_RETURN
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		IF eGoToHouseState != GO_TO_HOUSE_LOSE_COPS
			IF DOES_BLIP_EXIST(houseFootBlip)
				REMOVE_BLIP(houseFootBlip)
			ENDIF
			IF DOES_BLIP_EXIST(houseVehicleBlip)
				REMOVE_BLIP(houseVehicleBlip)
			ENDIF
			eGoToHouseState = GO_TO_HOUSE_LOSE_COPS
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_AGGROED_BOOTY_CALL_PED(stripperPed[0], tAggroArgs, tAggroTimer)
		MISSION_FAILED(FAIL_STRIPPER_ATTACKED)
	ENDIF
	
			
	//Check for player taking too long before picking up the booty call.
	IF eGoToHouseState <= GO_TO_HOUSE_MEET
		SETUP_BOOTYCALL_WAIT_TIME(TRUE)
	ENDIF
	
	SWITCH eGoToHouseState
		CASE GO_TO_HOUSE_INIT
			ePreviousGoToHouseState = GO_TO_HOUSE_INIT
			IF CAN_DO_STRIPPER_HOME_GOD_TEXT()
				IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_UP_TEXT)
					
					//B*1329405 - freeze sripper to prevent her floating
					FREEZE_ENTITY_POSITION(stripperPed[0],TRUE)
					PRINT_OBJECTIVE_WITH_STRIPPER_NAMES("SCLUB_HOME_MEET", stripperID[0])
					CREATE_STRIPPER_BLIP(0)
					
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_UP_TEXT)
				ENDIF
				
				DEBUG_MESSAGE("DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_MEET")
				CDEBUG1LN(DEBUG_BOOTY_CALL,"DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_MEET" )
				eGoToHouseState = GO_TO_HOUSE_MEET
			ENDIF
		BREAK
			
		CASE GO_TO_HOUSE_MEET
			ePreviousGoToHouseState = GO_TO_HOUSE_MEET
			
			sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(stripperID[0])
			
			
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iTakeHomeVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				iVehicleHealth = GET_ENTITY_HEALTH(iTakeHomeVehicle)
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(stripperPed[0]), <<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>)
			OR (IS_VEHICLE_DRIVEABLE(iTakeHomeVehicle) AND GET_DISTANCE_BETWEEN_ENTITIES(iTakeHomeVehicle,stripperPed[0] ) <= CONST_fVehicleTrigger)
			
				FREEZE_ENTITY_POSITION(stripperPed[0],FALSE)
			
				IF DOES_ENTITY_EXIST(iTakeHomeVehicle) 
				AND ((GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(iTakeHomeVehicle) < 1)
				OR (NOT bTakeHomeMethod AND NOT IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(iTakeHomeVehicle)))) // you are having sex with her in the car, make sure player is using a car
					
					
					IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SCLUB_SMALL_CAR", sStripperName)		
					
						PRINT_HELP_WITH_STRIPPER_NAMES("SCLUB_SMALL_CAR", stripperID[0])					
						PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "NEED_A_VEHICLE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					ENDIF
				ELSE
					CLEAR_HELP()
					CLEAR_PED_TASKS(stripperPed[0])
	
					ADD_STRIPPERS_TO_GROUP()
					
					IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_IS_NEAR)
						SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_IS_NEAR)
					ENDIF
					
					PLAY_BOOTY_CALL_GREETING()
					
//					IF DOES_ENTITY_EXIST(iTakeHomeVehicle)
//						BRING_VEHICLE_TO_HALT(iTakeHomeVehicle, CONST_fTrigger, 2)
//					ENDIF
					CLEAR_ALL_BLIPS()
					
					// Disable all mod shops
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_01_AP, TRUE )
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_05_ID2, TRUE )
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_06_BT1, TRUE )
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_07_CS1, TRUE )
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_08_CS6, TRUE )
					SET_SHOP_IS_TEMPORARILY_UNAVAILABLE( CARMOD_SHOP_SUPERMOD, TRUE )
					
					DEBUG_MESSAGE("DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_GREET")
					CDEBUG1LN(DEBUG_BOOTY_CALL,"DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_GREET" )
					eGoToHouseState = GO_TO_HOUSE_GREET
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SCLUB_SMALL_CAR", sStripperName)	
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_GREET
			ePreviousGoToHouseState = GO_TO_HOUSE_GREET
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF IS_PED_IN_ANY_VEHICLE(stripperPed[0])
					
					CDEBUG1LN(DEBUG_BOOTY_CALL,"DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_OBJ" )
					DEBUG_MESSAGE("DO_STAGE_GO_TO_HOUSE: Moving to GO_TO_HOUSE_OBJ")
					eGoToHouseState = GO_TO_HOUSE_OBJ
				ENDIF
			ELSE
				PLAY_PED_AMBIENT_SPEECH(stripperPed[0], "NEED_A_VEHICLE", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				eGoToHouseState = GO_TO_HOUSE_OBJ
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_OBJ
			ePreviousGoToHouseState = GO_TO_HOUSE_OBJ
			// To Fix Bug # 914284 - DS
			HANDLE_NICE_RIDE_CONVERSATION()
			
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CAN_DO_STRIPPER_HOME_GOD_TEXT()
					SET_ROADS_IN_AREA(vHouseInVehiclePos-<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, vHouseInVehiclePos+<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, FALSE)					
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vHouseInVehiclePos-<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, vHouseInVehiclePos+<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, FALSE)
					CLEAR_AREA_OF_VEHICLES(vHouseInVehiclePos, 10.0)
					CLEAR_AREA_OF_PEDS(vHouseInVehiclePos, 10.0)
					sbiSHome = ADD_SCENARIO_BLOCKING_AREA((vHouseInVehiclePos - <<35,35,35>>), (vHouseInVehiclePos + <<35,35,35>>))
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[SCENARIO BLOCKING] - ADDING BLOCKING VOLUME around house" )
					
					RESTART_TIMER_NOW(tBanter)
					IF bTakeHomeMethod
					//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							PRINT_TAKE_BOOTY_CALL_HOME(TRUE)
					//	ENDIF
						DO_STAGE_HANDLE_TRANSPORTATION_MODE()
					ELSE
						currentMissionStage = FIND_QUITE_LOCATION_FOR_BOOTYCALL
						CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1748 - go to FIND_QUITE_LOCATION_FOR_BOOTYCALL" )
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_FOOT
			ePreviousGoToHouseState = GO_TO_HOUSE_FOOT
			DO_STAGE_MANAGE_STRIPPER_BANTER(fDistToStripper, vHouseOnFootPos)
			
			#IF NOT IS_JAPANESE_BUILD
			DO_STAGE_MANAGE_BLOW_JOB()
			#ENDIF
			
			IF (GET_TIMER_IN_SECONDS(tWalking) > CONST_fMaxWalkTime
			OR VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), vStarttWalkLoc) > (CONST_fMaxWalkDist*CONST_fMaxWalkDist))
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(),vHouseOnFootPos) > CONST_fMaxWalkDist
				
				IF stripperID[0] = BC_TAXI_LIZ		
					CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_WALK")
				ELSE
					PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"GENERIC_FUCK_YOU",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				ENDIF
				MISSION_FAILED(FAIL_STRIPPER_WALKING)
			ELSE		
				#IF IS_DEBUG_BUILD
					DEBUG_SHOW_STRIPPER_HOME_COORDS()
				#ENDIF
				
				IF IS_STRIPPER_BEING_LEFT_BEHIND(stripperPed[0], TRUE)
				
					PRINTLN("Setting to GO_TO_HOUSE_RETURN")
					
					
					IF DOES_BLIP_EXIST(houseFootBlip)
						REMOVE_BLIP(houseFootBlip)
					ENDIF
					IF DOES_BLIP_EXIST(houseVehicleBlip)
						REMOVE_BLIP(houseVehicleBlip)
					ENDIF	
					CREATE_STRIPPER_BLIP(0)
				
					PRINT_OBJECTIVE_WITH_STRIPPER_NAMES( "SCLUB_HOME_R" , stripperID[0])
				
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Stripper is being left behind - GO_TO_HOUSE_RETURN" )
					eGoToHouseState = GO_TO_HOUSE_RETURN
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHouseInVehiclePos, <<CONST_fTrigger, CONST_fTrigger, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHouseDoorPos,<<CONST_fDoorTrigger, CONST_fDoorTrigger, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_ON_FOOT)
					IF CAN_PLAYER_START_CUTSCENE()
						currentMissionStage = STAGE_ENTER_HOUSE
					ENDIF
				ELIF NOT IS_PED_ON_FOOT(PLAYER_PED_ID())
					DO_STAGE_HANDLE_TRANSPORTATION_MODE()
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1784 - DO_STAGE_HANDLE_TRANSPORTATION_MODE" )
				ENDIF
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_DRIVE
			
			ePreviousGoToHouseState = GO_TO_HOUSE_DRIVE
			DO_STAGE_MANAGE_STRIPPER_BANTER(fDistToStripper, vHouseInVehiclePos)
			
			#IF NOT IS_JAPANESE_BUILD
			DO_STAGE_MANAGE_BLOW_JOB()
			#ENDIF
			
			ADD_STRIPPERS_TO_GROUP()
			
			#IF IS_DEBUG_BUILD
				DEBUG_SHOW_STRIPPER_HOME_COORDS()
			#ENDIF
			// advance stage if there
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHouseInVehiclePos, <<CONST_fVehicleHomeTrigger, CONST_fVehicleHomeTrigger, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
				eGoToHouseState = GO_TO_HOUSE_STOP
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 2450 - GO_TO_HOUSE_STOP" )
				
			ELSE 
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					DO_STAGE_HANDLE_TRANSPORTATION_MODE()
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1839 - DO_STAGE_HANDLE_TRANSPORTATION_MODE" )
				ELSE
					MANAGE_HOUSE_VEHICLE_BLIP(vHouseInVehiclePos)
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCLUB_HOME_WALK")
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
		
		//Bring the player's vehicle to a stop.
		CASE GO_TO_HOUSE_STOP
		
			#IF IS_DEBUG_BUILD
				DEBUG_SHOW_STRIPPER_HOME_COORDS()
			#ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 3.0, 2, 0.5)
							REMOVE_STRIPPER_FROM_GROUP()
						GIVE_WALK_TO_DOOR_TASK(stripperPed[0],vHouseDoorPos)
						
						IF stripperID[0] = BC_HITCHER_GIRL
						OR stripperID[0] = BC_TAXI_LIZ
							CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_GO_DOOR")
						ELSE
							CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_FOLLOWD",FALSE,FALSE,TRUE)
						ENDIF
						eGoToHouseState = GO_TO_HOUSE_WALK_TO_DOOR
						CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1829 - GO_TO_HOUSE_WALK_TO_DOOR" )
					ENDIF	
				ENDIF
			ELIF IS_PED_ON_FOOT(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(stripperPed[0])

				DO_STAGE_HANDLE_TRANSPORTATION_MODE()
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1833 - DO_STAGE_HANDLE_TRANSPORTATION_MODE" )
				CLEAR_HELP()
			ENDIF
		BREAK
		
		
		CASE GO_TO_HOUSE_WALK_TO_DOOR
			ePreviousGoToHouseState = GO_TO_HOUSE_WALK_TO_DOOR
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_BITMASK_AS_ENUM_SET( iGeneralBitFlags, HOUSE_MEET_STRIPPER_TAKE_IN)
				sStripperName = GET_STRIPPER_NAME_AS_TEXT_LABEL(stripperID[0])
				PRINT_STRING_IN_STRING_NOW("SCLUB_FOLLOW_H", sStripperName, DEFAULT_GOD_TEXT_TIME, 1)
				
				CLEAR_ALL_BLIPS()
				CREATE_STRIPPER_BLIP(0)
				SET_BITMASK_AS_ENUM( iGeneralBitFlags, HOUSE_MEET_STRIPPER_TAKE_IN)
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1872 - Stripper walking home...waiting for you" )
			ENDIF
			
			// advance stage if there
			#IF IS_DEBUG_BUILD
				DEBUG_SHOW_STRIPPER_HOME_COORDS()
			#ENDIF
			
			IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHouseOnFootPos, <<CONST_fDoorTrigger, CONST_fDoorTrigger, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHouseDoorPos, <<CONST_fDoorTrigger, CONST_fDoorTrigger, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_ON_FOOT))
				AND IS_PED_ON_FOOT(PLAYER_PED_ID())
	
				IF CAN_PLAYER_START_CUTSCENE()
					currentMissionStage = STAGE_ENTER_HOUSE
					CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1884 - STAGE_ENTER_HOUSE" )
//				ELSE
//					CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1886 - Can't start cutscene" )
				ENDIF
//			ELSE
//				CDEBUG1LN(DEBUG_BOOTY_CALL,"Line - 1889 - Waiting on player" )
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_LOSE_COPS
			IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_LOSE_COPS)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_AMBIENT_SPEECH_PLAYING(stripperPed[0])
				
				IF ePreviousGoToHouseState > GO_TO_HOUSE_MEET
				OR VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(stripperPed[0])) < (CONST_fTrigger*CONST_fTrigger)
					PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"GENERIC_FRIGHTENED_MED",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_LOSE_COPS)
				ENDIF
			ENDIF
		
			IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				eGoToHouseState = ePreviousGoToHouseState
			ENDIF
		BREAK
		
		CASE GO_TO_HOUSE_RETURN
			IF fDistToStripper > CONST_fAbandoned
				CDEBUG1LN(DEBUG_BOOTY_CALL,"FAIL________ Distance to sripper is father then ",CONST_fAbandoned )
				MISSION_FAILED(FAIL_STRIPPER_ABANDONED)
				
			ELIF fDistToStripper <= (CONST_fAbandoned/2) AND NOT IS_STRIPPER_BEING_LEFT_BEHIND(stripperPed[0], TRUE)
				CLEAR_PRINTS()
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"Returned - Going to prev state - > ", ePreviousGoToHouseState )
				eGoToHouseState = ePreviousGoToHouseState
				
				IF IS_PED_ON_FOOT( PLAYER_PED_ID() )
					DO_STAGE_HANDLE_TRANSPORTATION_MODE()
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
/// PURPOSE: Girl says sex lines
///    
PROC PLAY_BOOTY_CALL_SEX_SPEECH()
	IF NOT IS_ENTITY_DEAD(stripperPed[0])
		IF stripperID[0] = BC_TAXI_LIZ
		OR stripperID[0] = BC_HITCHER_GIRL
			ADD_PED_FOR_DIALOGUE(stripperConversation, 1, NULL, GET_BOOTY_CALL_NAME_AS_STRING(stripperID[0]))
			CREATE_CONVERSATION_FOR_BOOTY_CALL("SC_SEX")	
		ELSE
			PLAY_PED_AMBIENT_SPEECH(stripperPed[0],"SEX_ORAL",SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)	
		ENDIF
	ENDIF
ENDPROC
PROC DO_STAGE_DEBUG_MODE()
	//Teleport everyone here, not in the debugger
	IF NOT IS_ENTITY_DEAD(stripperPed[0])
	//	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),stripperPed[0]) > 20
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			SET_ENTITY_COORDS_GROUNDED(stripperPed[0],GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_HEADING(PLAYER_PED_ID()), <<0.0,2.0,0.0>>))
			
			currentMissionStage = STAGE_ENTER_HOUSE
			enterHouseStage = ENTER_HOUSE_INIT
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[DEBUG] Everyone should be in place -> enterHouseStage = ENTER_HOUSE_IN ")
		
		ELSE
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[DEBUG] Loading scene")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_BOOTY_CALL,"[DEBUG] Booty Call Not Valid ")
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_HELI( VEHICLE_INDEX viVeh )
	IF IS_VEHICLE_MODEL( viVeh, BLIMP )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, CARGOBOB )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, CARGOBOB2 )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, CARGOBOB3 )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, BUZZARD )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, BUZZARD2 )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, MAVERICK )
		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, POLMAV )
		RETURN TRUE
		
//	ELIF IS_VEHICLE_MODEL( viVeh, SWIFT )
//		RETURN TRUE
		
//	ELIF IS_VEHICLE_MODEL( viVeh, VALKYRIE )
//		RETURN TRUE
		
	ELIF IS_VEHICLE_MODEL( viVeh, ANNIHILATOR )
		RETURN TRUE
	ELIF IS_VEHICLE_MODEL( viVeh, FROGGER )
		RETURN TRUE
	ELIF IS_VEHICLE_MODEL( viVeh, FROGGER2 )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// stage: walking into the house
PROC DO_STAGE_ENTER_HOUSE()
	//SKIP THE CUTSCENE
	
	IF enterHouseStage < ENTER_HOUSE_FADE_OUT AND  enterHouseStage >= ENTER_HOUSE_SHOT_ALPHA
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(3000)
			
			DO_SCREEN_FADE_OUT(1000)  
			enterHouseStage = ENTER_HOUSE_FADE_OUT
			DEBUG_MESSAGE("Moving to ENTER_HOUSE_FADE_OUT")	
			
			CDEBUG1LN(DEBUG_BOOTY_CALL,"Moving to ENTER_HOUSE_FADE_OUT - > ", ePreviousGoToHouseState )
		ENDIF
	ENDIF
	
	IF NOT bVehicleSaved AND enterHouseStage >= ENTER_HOUSE_SHOT_TOD
		//B* 1679720: if vehicle big and stripper is Infernus, move vehicle to safe spot
		//CPRINTLN(debug_booty_call,"##### veh big: ",IS_BIG_VEHICLE(VehicleSaveArgsBootyCall.playerVehicle)," stripperID: ",stripperID[0]=BC_STRIPPER_INFERNUS)
		VEHICLE_INDEX vehPlayersLastCar = GET_LAST_DRIVEN_VEHICLE()
		IF IS_VEHICLE_DRIVEABLE(vehPlayersLastCar)
			AND NOT IS_ENTITY_DEAD( vehPlayersLastCar )
			IF GET_ENTITY_MODEL( vehPlayersLastCar ) = BLIMP2
				bVehicleSaved = FALSE
				//bVehicleSaved = ODDJOB_SAVE_VEHICLE(VehicleSaveArgsBootyCall)
			ELIF IS_BIG_VEHICLE(GET_LAST_DRIVEN_VEHICLE()) AND stripperID[0]=BC_STRIPPER_INFERNUS
				bVehicleSaved = ODDJOB_SAVE_VEHICLE_AND_WARP(VehicleSaveArgsBootyCall, <<-865,516.3,90>>, TRUE,15)
			ELIF IS_VEHICLE_HELI(GET_LAST_DRIVEN_VEHICLE())
				bVehicleSaved = ODDJOB_SAVE_VEHICLE_AND_WARP(VehicleSaveArgsBootyCall, vHeliPos, TRUE, fVehicleHeading)
				CPRINTLN( DEBUG_BOOTY_CALL, "MOVING HELI" )
			ELSE
				CPRINTLN( DEBUG_BOOTY_CALL, "NOT A HELI" )
				bVehicleSaved = ODDJOB_SAVE_VEHICLE_AND_WARP(VehicleSaveArgsBootyCall, vHouseInVehiclePos, TRUE,fVehicleHeading)
			ENDIF
		ENDIF
		CPRINTLN( DEBUG_BOOTY_CALL, "Moved vehicle!" )
	//	CDEBUG1LN(DEBUG_BOOTY_CALL,"[SAVE] Player vehicle saved at  ",vHouseInVehiclePos )
	ENDIF
	
	IF enterHouseStage > ENTER_HOUSE_INIT
		AND enterHouseStage < ENTER_HOUSE_DONE
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF
	
	SWITCH enterHouseStage
		CASE ENTER_HOUSE_INIT
			
			CLEAR_HELP()
			CLEAR_PRINTS()
			CLEAR_ALL_BLIPS()
			
			REQUEST_SCRIPT_AUDIO_BANK("TIME_LAPSE")
	
			ODDJOB_ENTER_CUTSCENE(SPC_REMOVE_PROJECTILES | SPC_REMOVE_EXPLOSIONS | SPC_REMOVE_FIRES,TRUE,FALSE)
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_HOME_SCENE_PLAYING)
				SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_GEN_HOME_SCENE_PLAYING)
			ENDIF
			
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_REQUEST_IDLE_ANIM)
				GET_MORNING_AFTER_SEX_DICTIONARY()
				SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_REQUEST_IDLE_ANIM)
			ENDIF
			
			//Request happy face anims
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_REQUEST_HAPPY_FACE_ANIMS)
				REQUEST_ANIM_DICT(GET_BOOTY_CALL_END_FACE_ANIM())
				SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_REQUEST_HAPPY_FACE_ANIMS)
			ENDIF
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_REQUEST_IDLE_ANIM)
				REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BOUNCER_IDLE())
				SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_MEET_REQUEST_IDLE_ANIM)
			ENDIF
			
			
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iTakeHomeVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				GIVE_PARK_TASK(PLAYER_PED_ID(), iTakeHomeVehicle)
				
				enterHouseStage = ENTER_HOUSE_PARK_VEHICLE
			ELSE
				REMOVE_STRIPPER_FROM_GROUP()
				GIVE_WALK_TO_DOOR_TASK(PLAYER_PED_ID(),vPlayerWalkToPos)
				GIVE_WALK_TO_DOOR_TASK(stripperPed[0],vHouseDoorPos)
				
				enterHouseStage = ENTER_HOUSE_SHOT_ALPHA
			ENDIF
			// setup cam
			SETUP_HOME_CAM()
		BREAK
		
		CASE ENTER_HOUSE_PARK_VEHICLE
			IF NOT IS_ENTITY_DEAD(iTakeHomeVehicle)
				IF VDIST(GET_ENTITY_COORDS(iTakeHomeVehicle), vHouseInVehiclePos) < 2.0
	
					REMOVE_STRIPPER_FROM_GROUP()
					GIVE_WALK_TO_DOOR_TASK(PLAYER_PED_ID(),vPlayerWalkToPos)
					GIVE_WALK_TO_DOOR_TASK(stripperPed[0],vHouseDoorPos)
//					SET_ENTITY_LOD_DIST(stripperPed[0], 80)
//					SET_PED_LOD_MULTIPLIER(stripperPed[0], 3.0)
//					REMOVE_PED_FOR_DIALOGUE(stripperConversation,1)
				
					NEW_LOAD_SCENE_STOP()
					
					enterHouseStage = ENTER_HOUSE_SHOT_ALPHA
				ELIF GET_GAME_TIMER() >= 10000
					enterHouseStage = ENTER_HOUSE_DONE
				ENDIF
			ENDIF
		BREAK
		
		//1st Shot------------------
		CASE ENTER_HOUSE_SHOT_ALPHA
			IF NOT IS_CAM_RENDERING(homeCam[0])
			
				SET_CAM_ACTIVE(homeCam[0],TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				IF NOT IS_ENTITY_DEAD( iTakeHomeVehicle )
					IF GET_ENTITY_MODEL( iTakeHomeVehicle ) = BLIMP2
						SET_ENTITY_VISIBLE( iTakeHomeVehicle, FALSE )
					ENDIF
				ENDIF
				
			
				RESTART_TIMER_NOW(tBJ)
				
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] ALPHA - Cam Rendering")
				enterHouseStage = ENTER_HOUSE_SHOT_ALPHA_TO_BETA
			ENDIF
		BREAK
		
		CASE ENTER_HOUSE_SHOT_ALPHA_TO_BETA
		
			IF GET_TIMER_IN_SECONDS_SAFE(tBJ) > CONST_fHomeCam1stShotTime
		//	OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vPlayerWalkToPos) < 5.0
				SET_CAM_ACTIVE_WITH_INTERP(homeCam[1],homeCam[0], CONST_iHomePanUpTODTime)
			
		//		SET_CAM_ACTIVE(tTimeLapse.splineCamera, TRUE)
  				SET_TODS_CUTSCENE_RUNNING(tTimeLapse, TRUE)
				
				INT iCurrentHour
                iCurrentHour = GET_CLOCK_HOURS()
			    iTODSkipHour = iCurrentHour + CONST_iNumHoursToSkip
				
				
				
				IF iTODSkipHour > 23
					iTODSkipHour -= 24
				ENDIF
				
				tTimeLapse.iTimeWindowStart = iCurrentHour
				tTimeLapse.iTimeWindowEnd = iTODSkipHour
	
				
//				
					
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[TOD]Setting ToD to:  ", iTODSkipHour, "from ", iCurrentHour)
			
			//	RESTART_TIMER_NOW(tBJ)
				CANCEL_TIMER(tBJ)
				enterHouseStage = ENTER_HOUSE_BETA_HOLD
			ENDIF
		BREAK
		
		CASE ENTER_HOUSE_BETA_HOLD
			
			IF NOT IS_CAM_INTERPOLATING(homeCam[1]) AND IS_CAM_RENDERING(homeCam[1])
			
				IF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_SEX)
					
					
					IF DOES_ENTITY_EXIST(iTakeHomeVehicle) AND NOT IS_ENTITY_DEAD(iTakeHomeVehicle)
						SET_VEHICLE_DOORS_SHUT(iTakeHomeVehicle, TRUE)
					ENDIF
					
					//START_PLAYER_TELEPORT(PLAYER_ID(),GET_PLAYER_WARP_POS_POST_SEX() ,GET_PLAYER_WARP_HEADING_POST_SEX(), FALSE,TRUE,FALSE)
					SET_ENTITY_COORDS_GROUNDED( PLAYER_PED_ID(), GET_PLAYER_WARP_POS_POST_SEX() )
					SET_ENTITY_HEADING( PLAYER_PED_ID(), GET_PLAYER_WARP_HEADING_POST_SEX() )
					
					PLAY_BOOTY_CALL_SEX_SPEECH()
					
					SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_SEX)
					
					
					
				ELIF NOT IS_BITMASK_AS_ENUM_SET(iBanterBitFlags, HOUSE_BANTER_SEX_MALE)
					
					//Player says sex lines
					IF IS_ENTITY_DEAD(stripperPed[0])
						CDEBUG1LN(DEBUG_BOOTY_CALL,"[ERROR] Shorty is dead")
					ENDIF
	
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_AMBIENT_SPEECH_PLAYING(stripperPed[0])
						
						PLAY_BOOTY_CALL_SEX_SPEECH()
						
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())	
							PLAY_PED_AMBIENT_SPEECH_NATIVE(PLAYER_PED_ID(),"SEX_CLIMAX","SPEECH_PARAMS_FORCE_SHOUTED_CLEAR")
						ENDIF
						
						CDEBUG1LN(DEBUG_BOOTY_CALL,"[TOD] Restarting timer for TOD")
						
						SET_BITMASK_AS_ENUM(iBanterBitFlags, HOUSE_BANTER_SEX_MALE)
				
						//Advance time of day
						RESTART_TIMER_NOW(tBJ)
						
						sThisWeatherType = GET_SPECIFIC_WEATHER_FOR_BOOTY_CALL()
					
						
						enterHouseStage = ENTER_HOUSE_SHOT_TOD
						CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] BETA INTERP - ENTER_HOUSE_SHOT_TOD")
					ENDIF
					
				ENDIF	
			ENDIF
		
		BREAK
		CASE ENTER_HOUSE_SHOT_TOD
//			IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iTODSkipHour, 0, "", "", tTimeLapse)		
			
			IF GET_TIMER_IN_SECONDS_SAFE(tBJ) > 1.0
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[TOD] Timer:  ", GET_TIMER_IN_SECONDS_SAFE(tBJ)*0.2)
				IF SKIP_TO_TIME_DURING_PHASE_WITH_SOUND(iTODSkipHour,0,sThisWeatherType,"",	tTimeLapse,GET_TIMER_IN_SECONDS_SAFE(tBJ)*0.2)
					
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF DOES_ENTITY_EXIST( iTakeHomeVehicle )
							IF GET_ENTITY_MODEL( iTakeHomeVehicle ) <> BLIMP2
								CLEAR_AREA(vHouseDoorPos, 20.0, TRUE )
							ENDIF
						ELSE
							CLEAR_AREA(vHouseDoorPos, 20.0, TRUE)
						ENDIF
					
						IF DOES_ENTITY_EXIST(stripperPed[0])
							SET_ENTITY_VISIBLE(stripperPed[0], FALSE)
						ENDIF
						
						SET_PED_PATH_AVOID_FIRE(PLAYER_PED_ID(), FALSE)
						RESTART_TIMER_NOW(tBJ)
						enterHouseStage = ENTER_HOUSE_SHOT_HOLD_TOD
						CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] BETA INTERP - ENTER_HOUSE_SHOT_HOLD_TOD")
				
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE ENTER_HOUSE_SHOT_HOLD_TOD
			CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] HOLDING TOD SHOT ", GET_TIMER_IN_SECONDS_SAFE(tBJ) )
			SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			IF GET_TIMER_IN_SECONDS_SAFE(tBJ) > 1.5
			
				IF bVehicleSaved
					ODDJOB_RESTORE_SAVED_VEHICLE(VehicleSaveArgsBootyCall)
				ENDIF	
				RESTART_TIMER_NOW(tBJ)
				enterHouseStage = ENTER_HOUSE_SHOT_CUT_TO_AFTER
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] BETA INTERP - ENTER_HOUSE_SHOT_CUT_TO_AFTER")
			
			ENDIF
		BREAK
		
		
		CASE ENTER_HOUSE_SHOT_CUT_TO_AFTER
			IF NOT IS_CAM_RENDERING(homeCam[2])
				
				CASCADE_SHADOWS_INIT_SESSION()
				CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
				
				SET_CAM_ACTIVE_WITH_INTERP(homeCam[2],homeCam[1], CONST_iPanToPlayerTime)
			
				RESTART_TIMER_NOW(tBJ)
				enterHouseStage = ENTER_HOUSE_SHOT_LERP_TO_GAMEPLAY
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] CAM GAMMA - Lerping to player")
			ENDIF
		BREAK
		
		CASE ENTER_HOUSE_SHOT_LERP_TO_GAMEPLAY
			//Queue anim halfway through the lerp
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_PLAY_SEX_ANIM)
				IF GET_TIMER_IN_SECONDS_SAFE(tBJ) >= 1.25
					PLAY_MORNING_AFTER_ANIM()
				ENDIF
			ENDIF
			
			//Camera is done lerping
			IF GET_TIMER_IN_SECONDS_SAFE(tBJ) > CONST_fHomePanUpTime
				SET_CAM_ACTIVE(homeCam[3],TRUE)
				
				IF NOT IS_ENTITY_DEAD( iTakeHomeVehicle )
					IF GET_ENTITY_MODEL( iTakeHomeVehicle ) = BLIMP2
						SET_ENTITY_VISIBLE( iTakeHomeVehicle, TRUE )
					ENDIF
					SET_ENTITY_AS_MISSION_ENTITY(iTakeHomeVehicle, TRUE, TRUE)				
					SET_VEHICLE_ENGINE_HEALTH(iTakeHomeVehicle,1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(iTakeHomeVehicle,1000)
					SET_VEHICLE_FIXED(iTakeHomeVehicle)
				ENDIF
				RESTART_TIMER_NOW(tBJ)
				enterHouseStage = ENTER_HOUSE_SHOT_OVER_SHOULDER
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] CAM GAMMA - Cleaning up catchup")
			ENDIF
		BREAK

		CASE ENTER_HOUSE_SHOT_OVER_SHOULDER
			IF GET_TIMER_IN_SECONDS_SAFE(tBJ) > 2.0		
				
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				
				enterHouseStage = ENTER_HOUSE_DONE
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] CAM GAMMA - Cleaning up catchup")
			ENDIF	
		BREAK
		
		CASE ENTER_HOUSE_FADE_OUT
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[NEW FORREST CAM] Player skipped scene")
				IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_PLAY_SEX_ANIM)
					START_PLAYER_TELEPORT(PLAYER_ID(),vAfterCutPos ,fAfterCutRot, FALSE,TRUE,TRUE)
					PLAY_MORNING_AFTER_ANIM()
				ENDIF
				
				IF NOT IS_ENTITY_DEAD( iTakeHomeVehicle )
					IF GET_ENTITY_MODEL( iTakeHomeVehicle ) = BLIMP2
						SET_ENTITY_VISIBLE( iTakeHomeVehicle, TRUE )
					ENDIF
				ENDIF
				SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				IF NOT IS_ENTITY_DEAD(iTakeHomeVehicle)
					SET_ENTITY_AS_MISSION_ENTITY(iTakeHomeVehicle, TRUE, TRUE)				
					SET_VEHICLE_ENGINE_HEALTH(iTakeHomeVehicle,1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(iTakeHomeVehicle,1000)
					SET_VEHICLE_FIXED(iTakeHomeVehicle)
				ENDIF
			
				RESTART_TIMER_NOW(tBJ)
				enterHouseStage = ENTER_HOUSE_DONE
			ENDIF
		BREAK
		
		CASE ENTER_HOUSE_DONE
			//CPRINTLN(DEBUG_BOOTY_CALL,"Enter_house_done here")
			IF NOT IS_SCREEN_FADING_OUT()
			AND GET_TIMER_IN_SECONDS_SAFE(tBJ) >= 1.0
				//CPRINTLN(DEBUG_BOOTY_CALL, "Enter_house_done 1st IF")
				ODDJOB_EXIT_CUTSCENE(DEFAULT,DEFAULT,FALSE)
				
				SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
				SPECIAL_ABILITY_CHARGE_ABSOLUTE(PLAYER_ID(),30,TRUE)
				
				
				IF IS_PED_MODEL(PLAYER_PED_ID(), GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
					SHRINK_ADD_SEX_TIMESTAMP()
				ENDIF
				
				SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vHouseInVehiclePos-<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>, vHouseInVehiclePos+<<CONST_fTrigger, CONST_fTrigger, CONST_fTrigger>>,CONST_fRoadNodeAreaWidth)
				//CPRINTLN(debug_booty_call,"Is player car dead? ",IS_ENTITY_DEAD(iTakeHomeVehicle))
				IF NOT IS_ENTITY_DEAD(iTakeHomeVehicle)
					SET_ENTITY_AS_MISSION_ENTITY(iTakeHomeVehicle, TRUE, TRUE)
					
					IF GET_ENTITY_MODEL( iTakeHomeVehicle ) <> BLIMP2
						SET_ENTITY_COORDS(iTakeHomeVehicle, vHouseInVehiclePos)
						SET_ENTITY_HEADING(iTakeHomeVehicle, fVehicleHeading)
						
						//B* 1679720: Moving large vehicles outside Infernus' house
						//CPRINTLN(DEBUG_BOOTY_CALL,"Is vehicle big: ",IS_BIG_VEHICLE(iTakeHomeVehicle))
						IF IS_BIG_VEHICLE(iTakeHomeVehicle)
						AND stripperID[0]=BC_STRIPPER_INFERNUS
							SET_ENTITY_COORDS(iTakeHomeVehicle, <<-865,516.3,90>>)
							SET_ENTITY_HEADING(iTakeHomeVehicle, 15)
						ELIF IS_VEHICLE_HELI(iTakeHomeVehicle)
							SET_ENTITY_COORDS(iTakeHomeVehicle, vHeliPos)
							SET_ENTITY_HEADING(iTakeHomeVehicle, fHeliHeading)
							CPRINTLN( DEBUG_BOOTY_CALL, "SETTING HELI POS" )
						endif
						
						SET_VEHICLE_ON_GROUND_PROPERLY(iTakeHomeVehicle)
					ENDIF
				ENDIF
				
				IF NOT IS_SCREEN_FADED_OUT()
					SET_TODS_CUTSCENE_RUNNING(tTimeLapse, FALSE,FALSE,2000,TRUE,FALSE)
				ELSE
					ADD_TO_CLOCK_TIME(CONST_iNumHoursToSkip, 0, 0)
					ADVANCE_FRIEND_TIMERS(TO_FLOAT(CONST_iNumHoursToSkip))
					DO_SCREEN_FADE_IN(2000)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					SET_TODS_CUTSCENE_RUNNING(tTimeLapse, FALSE)
					
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[FADE] Fade in called!!!!!!!!!!!!!!!!!!!!!!!")
				ENDIF
				
				IF DOES_ENTITY_EXIST(stripperPed[0])
					DELETE_PED(stripperPed[0])
				ENDIF
				
				IF NOT IS_BOOTY_CALL_ID_ACTIVATED(stripperID[0])
					IF  stripperID[0] = BC_STRIPPER_INFERNUS
						PRINT_HELP_WITH_STRIPPER_NAMES("SCLUB_PHON_HELP_ALT", stripperID[0])
					ELSE
						PRINT_HELP_WITH_STRIPPER_NAMES("SCLUB_PHON_HELP", stripperID[0])
					ENDIF
					SET_BOOTY_CALL_ID_ACTIVATED(stripperID[0], TRUE)
					WAIT(4000)
				ENDIF
		
				
				//SEXTING-----------------------------------------------------------------------------------------------------------
				INT iNumSexts 
				INCREMENT_NUM_SEXTS_SENT_BY_STRIPPER(stripperID[0])
				iNumSexts = GET_NUM_SEXTS_SENT_BY_STRIPPER(stripperID[0])
				CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEXT] Num sexts =  ",iNumSexts, ".")
					
				IF iNumSexts <= CONST_iMaxNumSexts
					CC_TextPart ePartStripper
					CC_TextPart ePartCount
					ePartStripper = GET_TEXT_PARTID_FOR_STRIPPER(stripperID[0])
					ePartCount = GET_TEXT_PARTID_FOR_SEXT_COUNT(iNumSexts)	//keep this to 1 or 2 with iNumSexts % 2
					
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEXT] Queueing sext to be sent for iNumsexts =  ",iNumSexts, " ", GET_COMM_ID_DEBUG_STRING(TEXT_SEXT), " ", GET_TEXT_PART_DEBUG_STRING(ePartStripper), " ", GET_TEXT_PART_DEBUG_STRING(ePartCount), ".")
					
					REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(
						TEXT_SEXT,
						ePartStripper,
						ePartCount,
						CT_FRIEND,
						GET_CURRENT_PLAYER_PED_BIT(),
						GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(stripperID[0]),
						CONST_iTimeSextDelay)	//will text a minute later
				ENDIF
				
				MISSION_PASSED()
				
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_SECLUDED_SPOT()
	PED_INDEX closest_PED_to_player
	VEHICLE_INDEX closest_VEHICLE_to_player
	VEHICLE_INDEX iPlayersVehicle
	VECTOR players_current_pos
	FLOAT min_dist_to_interupt_sex  = 25.0
	
	VECTOR vInitPos
	FLOAT fInitRot
	
	GET_INIT_TAKE_HOME_STRIPPER_LOC(vInitPos, fInitRot, FALSE)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			iPlayersVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		
		players_current_pos = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
		
		// check for road
		VECTOR node_pos
		IF GET_CLOSEST_MAJOR_VEHICLE_NODE (players_current_pos, node_pos)
			FLOAT fDistToClosestNode = VDIST (players_current_pos, node_pos)
			IF ( fDistToClosestNode <= 10.0)//20.0)
				RETURN FALSE
			ELSE
				PRINTLN("Distance to closest rode node is ", fDistToClosestNode)
			ENDIF
		ENDIF
		
		IF VDIST(players_current_pos, vHouseInVehiclePos) < 20 OR 
		   VDIST(players_current_pos, vInitPos) < 20
		   	//player is too close to the starting and ending positions
			RETURN FALSE
		ENDIF
		
		// check for cops
		IF IS_COP_VEHICLE_IN_AREA_3D(players_current_pos - <<20, 20, 20>>, players_current_pos + <<20, 20, 20>>)
			RETURN FALSE
		ENDIF
		
		// check for nearby ped
		GET_CLOSEST_PED(players_current_pos, min_dist_to_interupt_sex, TRUE, TRUE, closest_PED_to_player)
		IF NOT (closest_PED_to_player = NULL)
			IF NOT IS_ENTITY_DEAD(closest_PED_to_player)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(closest_PED_to_player, PLAYER_PED_ID())
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			
			closest_VEHICLE_to_player = GET_RANDOM_VEHICLE_IN_SPHERE(players_current_pos, min_dist_to_interupt_sex, DUMMY_MODEL_FOR_SCRIPT,-1 )
			IF NOT (closest_VEHICLE_to_player = NULL)
				
				IF NOT (closest_VEHICLE_to_player = iPlayersVehicle)
					// check it is satisfactory model
					IF IS_VEHICLE_DRIVEABLE (closest_VEHICLE_to_player)
						closest_PED_to_player = GET_PED_IN_VEHICLE_SEAT(closest_VEHICLE_to_player,VS_DRIVER )
						
						IF NOT (closest_PED_to_player = NULL)
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					
				ENDIF
			ELSE
				// no VEHICLEs found
				
			ENDIF
		ENDIF
		
		// check if inside
		INTERIOR_INSTANCE_INDEX current_interior
		current_interior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
		IF NOT (current_interior = NULL)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL INIT_SERVICE_ANIMS()
	
	
	IF HAS_ANIM_DICT_LOADED(GET_ANIM_DICT_FOR_SEX_FOR_VEHICLE_PED_IS_IN(stripperPed[0]))
	    TASK_PED_TO_HAVE_SEX_IN_VEHICLE(stripperPed[0],FALSE)
	    TASK_PED_TO_HAVE_SEX_IN_VEHICLE(PLAYER_PED_ID(),FALSE,FALSE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DO_STAGE_FIND_QUITE_SPOT()
	// check mission fail conditions
	CHECK_FAIL_CONDITIONS()
	
	CLEAR_ALL_BLIPS()
	
	SWITCH stateFindingSpot
		CASE DRIVE_TO_SPOT
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				IF IS_PED_IN_ANY_VEHICLE(stripperPed[0])
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SCLUB_SECLUDED")
						PRINT_HELP_FOREVER("SCLUB_SECLUDED")
					ENDIF
					
					IF IS_SECLUDED_SPOT() AND GET_ENTITY_SPEED(PLAYER_PED_ID()) = 0
						PRINTLN("Found secluded spot")
						CLEAR_HELP()
						REQUEST_ANIM_DICT(GET_ANIM_DICT_FOR_SEX_FOR_VEHICLE_PED_IS_IN(stripperPed[0]))
						stateFindingSpot = INIT_SEX
					ENDIF
					
				ELSE
				ENDIF
			ELSE
			ENDIF
		BREAK
		
		CASE INIT_SEX
			IF INIT_SERVICE_ANIMS()
				stateFindingSpot = HAVE_SEX
			ENDIF
		BREAK
		
		CASE HAVE_SEX
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()	//Fix for bug 2246108
				
				IF GET_CLOCK_HOURS() > 20 OR GET_CLOCK_HOURS() < 4
					bDropOffHome = FALSE //drop the stripper off at the club
				ENDIF
				
				//Restore 3rd Person Cam
				SET_FOLLOW_PED_CAM_VIEW_MODE(ExitViewMode)
				
				PRINT_TAKE_BOOTY_CALL_HOME(bDropOffHome)
				currentMissionStage = DROP_OFF_BOOTY_CALL
			ENDIF
		BREAK
	
	ENDSWITCH
ENDPROC

PROC DO_STAGE_DROP_OFF_BOOTY_CALL(BOOL bDropOffAtHome)
	
	// check mission fail conditions
	CHECK_FAIL_CONDITIONS()
	
	VECTOR vDropOffSpot
	FLOAt fStoreRot
	
	IF bDropOffAtHome
		vDropOffSpot = vHouseInVehiclePos
	ELSE
		GET_INIT_TAKE_HOME_STRIPPER_LOC(vDropOffSpot, fStoreRot, FALSE)
	ENDIF
	
	MANAGE_HOUSE_VEHICLE_BLIP(vDropOffSpot)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(stripperPed[0])
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(vehTemp) AND NOT IS_ENTITY_DEAD(vehTemp)
				IF bStoppingCar
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTemp, 4.0)
						REMOVE_STRIPPER_FROM_GROUP()
						TASK_LEAVE_ANY_VEHICLE(stripperPed[0],0, ECF_WAIT_FOR_ENTRY_POINT_TO_BE_CLEAR)
						MISSION_PASSED()
					ENDIF
				ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDropOffSpot, <<4.0, 4.0, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
					bStoppingCar = TRUE
				ENDIF
			ENDIF
		ELSE
		ENDIF
	ELSE
	ENDIF
ENDPROC

// debug
#IF IS_DEBUG_BUILD
	PROC DO_DEBUG_SKIPS()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_CLEANUP()
			DEBUG_MESSAGE("HAS_FORCE_CLEANUP_OCCURRED")
			CDEBUG1LN(DEBUG_BOOTY_CALL, "[CLEANUP] F Key Pressed ****************** ")
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			CLEAR_SMALL_PRINTS()
			IF currentMissionStage = STAGE_GO_TO_HOUSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					//*SET_ENTITY_COORDS*/ SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vHouseInVehiclePos)
					MOVE_VEHICLE_TO_PARK_COORD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					LOAD_SCENE(vHouseInVehiclePos)
				ELSE
					IF IS_ENTITY_DEAD(iDebugTakeHomeVehicle)
						IF NOT bRequestedDebugVehicle
							REQUEST_MODEL(eDebugTakeHomeVehicle)
							bRequestedDebugVehicle = TRUE
						ENDIF
					ENDIF
					/*SET_ENTITY_COORDS*/ 
					//SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), vHouseOnFootPos)
				ENDIF
			ENDIF
		ELIF bRequestedDebugVehicle
			IF IS_ENTITY_DEAD(iDebugTakeHomeVehicle)
				IF HAS_MODEL_LOADED(eDebugTakeHomeVehicle)
					IF stripperID[0] = BC_TAXI_LIZ
					//OR stripperID[0] = BC_FIXED_CAR
						vDebugTakeHomeVehicle = <<-1612.5968, 183.5961, 58.7792>>
						
					ELIF stripperID[0] = BC_HITCHER_GIRL
						vDebugTakeHomeVehicle = << 1538.4315, 3776.8496, 33.5178 >>
					ENDIF
					iDebugTakeHomeVehicle = CREATE_VEHICLE(eDebugTakeHomeVehicle, vDebugTakeHomeVehicle, fDebugTakeHomeVehicle)
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(iDebugTakeHomeVehicle)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), iDebugTakeHomeVehicle, VS_DRIVER) 
					SET_PED_INTO_VEHICLE(stripperPed[0], iDebugTakeHomeVehicle, VS_FRONT_RIGHT) 
					bRequestedDebugVehicle = FALSE

				ENDIF
			ENDIF
		ENDIF
		
		
		IF g_bTurnOnLukeHCameraDebug	
			
		//	DEBUG_PRINT_STATES()
		//	DEBUG_SHOW_STRIPPER_HOME_COORDS()
			
			IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_LUKE_MODE_SKIP_ON)
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vHouseInVehiclePos) <= 15.0
					
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			
					//Create chick
					IF NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_GEN_HOME_SCENE_PLAYING)
						currentMissionStage = DEBUG_SKIP_MODE
					ENDIF
					
					SET_BITMASK_AS_ENUM(iGeneralBitFlags, HOUSE_GEN_LUKE_MODE_SKIP_ON)
					CDEBUG1LN(DEBUG_BOOTY_CALL,"[DEBUG] Luke Mode Engaged - skipping to final scene" )
				ENDIF
			ENDIF
		ENDIF
		
	ENDPROC
#ENDIF

// main script
SCRIPT(TAKE_HOME_STRIPPER_INFO takeHomeInfo)
	SET_SCRIPT_AS_NO_LONGER_NEEDED("stripperhome")	
	SET_MISSION_FLAG(TRUE)
	SET_ALL_NEUTRAL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE) //don't want people bothering me in the middle of a bootycall
	
	DEBUG_MESSAGE("Launching stripper home script")
		
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_RANDOM_EVENTS | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		DEBUG_MESSAGE("HAS_FORCE_CLEANUP_OCCURRED")
		MISSION_CLEANUP()
	ENDIF
	
	bIsMP = takeHomeInfo.bIsMp
	
	IF bIsMp
		// This marks the script as a net script, and handles any instancing setup. 
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE)
		
		// This makes sure the net script is active, waits untull it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		RESERVE_NETWORK_MISSION_PEDS(1)
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
	ENDIF
	
	stripperID[0] = takeHomeInfo.stripperID[0]
	
	g_StripclubGlobals.eCurrentBootyCall = stripperID[0]
	
	BOOL bPickUpAtHome = FALSE
	BOOL bHaveSexAtHouse = TRUE //having sex at house is the original method, having sex in car is new method
	
	INT iCurrentHour = GET_CLOCK_HOURS()
	
	IF CAN_USE_SEX_IN_CAR_METHOD() AND takeHomeInfo.bIsBootyCall //called stripper on phone, use sex in car method
		bHaveSexAtHouse = FALSE
		PRINTLN("Current Hour ", iCurrentHour)
		IF iCurrentHour > 14 AND iCurrentHour < 22
			bPickUpAtHome = TRUE //pick up stripper at home for sex in car method
		ENDIF
	ENDIF
	
	
	WHILE NOT INIT_MISSION(takeHomeInfo, bPickUpAtHome) //Going to the stripper starting location
	AND currentMissionStage != BOOTYCALL_FAILED
	AND currentMissionStage != SEND_STRIPPER_BACK_TO_WORK
	AND (currentMissionStage != DEBUG_SKIP_MODE OR NOT IS_BITMASK_AS_ENUM_SET(iGeneralBitFlags, HOUSE_MEET_STRIPPER_CREATED))
		//DEBUG_MESSAGE("waiting on INIT_MISSION")
		SETUP_BOOTYCALL_WAIT_TIME()
		
		#IF IS_DEBUG_BUILD
			// do debug skips
			DO_DEBUG_SKIPS()
	    #ENDIF
		
		WAIT(0)
		//CDEBUG1LN(DEBUG_BOOTY_CALL,"[INIT] Waiting for pro to be created" )
	ENDWHILE
	
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(0))
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_STRIPPER_MODEL(1))
	
	IF NOT bHaveSexAtHouse
		currentMissionStage = PICK_UP_BOOTY_CALL
	ENDIF
	
    WHILE TRUE
       	WAIT(0)
		
		IF iRecordBlockPersistTimer > 0
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				iRecordBlockPersistTimer -= ROUND(GET_FRAME_TIME()*1000)
			ENDIF
		// run appropriate stage
		SWITCH currentMissionStage
			CASE DEBUG_SKIP_MODE
				DO_STAGE_DEBUG_MODE()
			BREAK
			
			//states for taking girl home for sex
			CASE STAGE_GO_TO_HOUSE
				DO_STAGE_GO_TO_HOUSE(TRUE) //picking girl up from her starting location and driving her home
			BREAK
			
			CASE STAGE_ENTER_HOUSE
				DO_STAGE_ENTER_HOUSE() //cutscene of player and stripper walking into the house and cleanup
			BREAK
			
			//States for bootycalls, drive to quite spot, and fuck
			CASE PICK_UP_BOOTY_CALL
			// pick girl up from a location, strippper : 2am - 6am - Club
			//                                           6am - 2pm - no pickup
			//                                           2pm - 2am - home
			//
			//                               Others    : the other booty calls will use the take home method
				DO_STAGE_GO_TO_HOUSE(FALSE)
			BREAK
			
			CASE FIND_QUITE_LOCATION_FOR_BOOTYCALL
			//drive the girl to a quite spot and fuck
				DO_STAGE_FIND_QUITE_SPOT()
			BREAK
			
			CASE DROP_OFF_BOOTY_CALL
			//after the fucking drop her off, stripper: 8pm - 4am - Club
			//                                          4am - 8pm - Home
			//                                Others    : the other booty calls will use the take home method
				DO_STAGE_DROP_OFF_BOOTY_CALL(bDropOffHome)
			BREAK
			
			//B*1143123
			CASE SEND_STRIPPER_BACK_TO_WORK
			
				DO_STAGE_SEND_STRIPPER_TO_THE_CLUB()
			BREAK
			
			CASE BOOTYCALL_FAILED
				IF NOT IS_TIMER_STARTED(tExitWait)
					START_TIMER_NOW(tExitWait)
				ELIF TIMER_DO_ONCE_WHEN_READY(tExitWait, 7.5)
					MISSION_CLEANUP()
				ENDIF
			BREAK
			
		ENDSWITCH
		
		// init
		#IF IS_DEBUG_BUILD
			// do debug skips
			DO_DEBUG_SKIPS()
	    #ENDIF
		
	ENDWHILE
ENDSCRIPT
