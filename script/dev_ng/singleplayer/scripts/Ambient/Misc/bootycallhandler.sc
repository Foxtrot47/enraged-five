//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   bootycallhandler.sc                                         //
//      AUTHOR          :   Rob Bray         
//		Now Maintained 	:	John Diaz
//      DESCRIPTION     :   Script always runs. Handle booty calls to/from strippers	//
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
///    


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "shared_debug_text.sch"
USING "cellphone_public.sch"
USING "dialogue_public.sch"
USING "stripclub_public.sch" 
USING "comms_control_public.sch"
USING "net_common_functions.sch"

// Constants
CONST_INT BOOTY_CALL_QUEUE_TIME		6000
structPedsForConversation bootyCallConversation	
INT candidateID = NO_CANDIDATE_ID

// Debug only
#IF IS_DEBUG_BUILD
    WIDGET_GROUP_ID stripClubWidgets
#ENDIF

#IF IS_DEBUG_BUILD	
	// do a debug print prefixed by Booty Call Handler
	PROC DEBUG_PRINT_BOOTY_CALL_HANDLER(STRING debugString)
		IF g_bDebugDisplayStripClubAndBootyCall
			PRINTSTRING("Booty Call Handler: ")
			PRINTSTRING(debugString)
			PRINTNL()
		ENDIF
	ENDPROC
#ENDIF


// check if can do booty call
FUNC BOOL CAN_START_NEW_BOOTY_CALL()
	IF NOT CAN_TAKE_HOME_STRIPPER()
	OR g_bStripClubRunning
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// check if can make phone call
FUNC BOOL CAN_MAKE_BOOTY_CALL(BOOL bAllowPhoneOnscreen)
	IF (IS_PHONE_ONSCREEN() AND NOT bAllowPhoneOnscreen)
	OR IS_CALLING_ANY_CONTACT()
	OR g_bootyCallData.iBootyCallID <> -1
	OR NOT IS_PLAYER_PED_PLAYABLE(GET_CURRENT_PLAYER_PED_ENUM())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// pick an active, available stripper to be calling the player
FUNC INT PICK_BOOTY_CALL_TO_BE_CALLING()
	INT i 
	INT iCurrentSlot = 0
	BOOTY_CALL_CONTACT_ENUM bootyCallList[COUNT_OF(BOOTY_CALL_CONTACT_ENUM)]
	
	REPEAT COUNT_OF(BOOTY_CALL_CONTACT_ENUM) i
		IF g_savedGlobals.sStripClubData[GET_CURRENT_PLAYER_PED_ENUM()].bootyCall[i].bActivated // activated as a booty call
		AND GET_GAME_TIMER() >= g_bootyCallData.iNextAvailableTime[i] // past next available time
			// if an active booty call, then add to the pool of possible girls to be calling
			bootyCallList[iCurrentSlot] = INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i)
			iCurrentSlot++
		ENDIF
	ENDREPEAT
	
	IF iCurrentSlot > 0
		// get a random number from 0 to the number of filled slots
		INT iRandomSlot = GET_RANDOM_INT_IN_RANGE(0, iCurrentSlot)
		RETURN ENUM_TO_INT(bootyCallList[iRandomSlot])
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINT_BOOTY_CALL_HANDLER("Returning an invalid caller, could not find an available, active booty call")
	#ENDIF
	RETURN -1
ENDFUNC

// launch take home script
PROC START_BOOTY_CALL(BOOTY_CALL_CONTACT_ENUM bootyCallID)	
	REQUEST_SCRIPT("stripperhome")
	WHILE NOT HAS_SCRIPT_LOADED("stripperhome")
		PRINTLN("Waiting for stripperhome to load")
		WAIT(0)
	ENDWHILE		

	IF CAN_TAKE_HOME_STRIPPER()
	
		IF IS_FREEMODE()
		
			TAKE_HOME_STRIPPER_INFO takeHomeInfo
			takeHomeInfo.stripperID[0] = bootyCallID
			takeHomeInfo.iNumStrippers = 1
			takeHomeInfo.myCandidateID = NO_CANDIDATE_ID
			takeHomeInfo.bIsBootyCall = TRUE
			takeHomeInfo.bIsMp = TRUE
			START_NEW_SCRIPT_WITH_ARGS("stripperhome", takeHomeInfo, SIZE_OF(TAKE_HOME_STRIPPER_INFO), DEFAULT_STACK_SIZE)
			SET_NEXT_STRIPPER_BOOTY_CALL_AVAILABLE_TIME(g_bootyCallData.iStripperCalling)
			g_bootyCallData.bootyCallState = BOOTY_CALL_SCRIPT_RUNNING
			
		ELSE
			candidateID = NO_CANDIDATE_ID
			m_enumMissionCandidateReturnValue eCandidateReturnValue = Request_Mission_Launch(candidateID, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME_FRIENDS)		
			WHILE eCandidateReturnValue = MCRET_PROCESSING
				eCandidateReturnValue = Request_Mission_Launch(candidateID, MCTID_CONTACT_POINT, MISSION_TYPE_MINIGAME_FRIENDS)		
				PRINTLN("Request_Mission_Launch is processing")
				WAIT(0)
			ENDWHILE
		
			IF eCandidateReturnValue = MCRET_ACCEPTED
				SET_STRIPPER_NUM_BOOTY_DENIALS_BY_PLAYER(bootyCallID, 0)
			
				// launch
				TAKE_HOME_STRIPPER_INFO takeHomeInfo
				takeHomeInfo.stripperID[0] = bootyCallID
				takeHomeInfo.iNumStrippers = 1		
				takeHomeInfo.myCandidateID = candidateID
				takeHomeInfo.bIsBootyCall = TRUE
				takeHomeInfo.bIsMp = FALSE
				START_NEW_SCRIPT_WITH_ARGS("stripperhome", takeHomeInfo, SIZE_OF(TAKE_HOME_STRIPPER_INFO), DEFAULT_STACK_SIZE)
				SET_NEXT_STRIPPER_BOOTY_CALL_AVAILABLE_TIME(g_bootyCallData.iStripperCalling)
				g_bootyCallData.bootyCallState = BOOTY_CALL_SCRIPT_RUNNING		
				PRINTLN("Launching stripperhome")
			ELSE
				PRINTLN("Not able to launch stripperhome, am azlready on mission!")		
				SET_SCRIPT_AS_NO_LONGER_NEEDED("stripperhome")
				g_bootyCallData.bootyCallState = BOOTY_CALL_INACTIVE		
			ENDIF
		ENDIF
	ELSE		
		PRINTLN("Not able to launch stripperhome, not allowed!")		
		SET_SCRIPT_AS_NO_LONGER_NEEDED("stripperhome")
		g_bootyCallData.bootyCallState = BOOTY_CALL_INACTIVE		
	ENDIF
	
	g_bootyCallData.iStripperCalling = -1
	SET_NEXT_CALL_PLAYER_TIME()
ENDPROC

FUNC enumPhonebookPresence GET_CURRENT_PLAYER_PHONEBOOK()
	
	SWITCH GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
		CASE CHAR_MICHAEL
			RETURN MICHAEL_BOOK
		BREAK
		CASE CHAR_TREVOR
			RETURN TREVOR_BOOK
		BREAK
		CASE CHAR_FRANKLIN	
			RETURN FRANKLIN_BOOK
		BREAK
	ENDSWITCH
	
	RETURN ALL_OWNERS_BOOKS
ENDFUNC

FUNC BOOL IS_BOOTYCALL_IN_PHONEBOOK(enumCharacterList CharacterToAdd)
	enumPhonebookPresence WhichPhonebook = GET_CURRENT_PLAYER_PHONEBOOK()
	RETURN GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CharacterToAdd, ENUM_TO_INT(WhichPhonebook)) = LISTED 
ENDFUNC

PROC HANDLE_STRIPPER_SEXT_FOR_BOOTY_CALL()
	IF IS_FREEMODE()
		EXIT //stripper does not contact player in freemode
	ENDIF
	
	BOOTY_CALL_CONTACT_ENUM BootyCallEnum
	SWITCH g_bootyCallData.bootyCallState
		CASE BOOTY_CALL_INACTIVE
			IF (NOT g_bootyCallData.bPreventingBootyCall
			AND CAN_MAKE_BOOTY_CALL(FALSE))

				IF (GET_GAME_TIMER() >= g_bootyCallData.iNextCallPlayerTime)
					
					g_bootyCallData.iStripperCalling = PICK_BOOTY_CALL_TO_BE_CALLING()

					BootyCallEnum = INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling )
					
					IF g_bootyCallData.iStripperCalling >= 0
					AND CAN_TAKE_HOME_STRIPPER()
					AND IS_BOOTYCALL_IN_PHONEBOOK(GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(BootyCallEnum))
					AND IS_STRIPPER_AVAILABLE_AT_THIS_TIME_OF_DAY(BootyCallEnum)
					AND GET_LIKE_FOR_STRIPPER_ID(BootyCallEnum) >= GET_STRIPPER_COME_HOME_THRESHOLD(BootyCallEnum) //make sure stripper actually likes the player
						
						CC_TextPart ePartStripper
						CC_TextPart ePartCount
						ePartStripper = GET_TEXT_PARTID_FOR_STRIPPER(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling))
						ePartCount = GET_TEXT_PARTID_FOR_SEXT_COUNT(0)	//Force this to TPART_SEXT_NEED
						
						CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEXT] Queueing sext to be sent ", GET_COMM_ID_DEBUG_STRING(TEXT_SEXT), " ", GET_TEXT_PART_DEBUG_STRING(ePartStripper), " ", GET_TEXT_PART_DEBUG_STRING(ePartCount), ".")

						REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(
							TEXT_SEXT,
							ePartStripper,
							ePartCount,
							CT_FRIEND,
							GET_CURRENT_PLAYER_PED_BIT(),
							GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling)),
							0)
							
						INCREMENT_NUM_SEXTS_SENT_BY_STRIPPER(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling))
					ENDIF
					
					IF NOT IS_STRIPPER_AVAILABLE_AT_THIS_TIME_OF_DAY(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling ))
						SET_NEXT_CALL_PLAYER_TIME(1000 *60*5) //wrong time of day to call player, try again in 5 real life minutes
					ELSE
						SET_NEXT_CALL_PLAYER_TIME()
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE BOOTY_CALL_STRIPPER_CALLING
		BREAK
	ENDSWITCH
ENDPROC

PROC HANDLE_STRIPPER_TEXT_FOR_BEING_HUNG_UP_ON()
	IF IS_FREEMODE()
		EXIT //stripper does not contact player in freemode
	ENDIF
	
	CC_TextPart ePartStripper
	ePartStripper = GET_TEXT_PARTID_FOR_STRIPPER(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling))

	CDEBUG1LN(DEBUG_BOOTY_CALL,"[SEXT] Queueing sext to be sent ", GET_COMM_ID_DEBUG_STRING(TEXT_SEXT), " ", GET_TEXT_PART_DEBUG_STRING(ePartStripper), " ", GET_TEXT_PART_DEBUG_STRING(TPART_SEXT_RUDE), ".")
	
	REGISTER_COMPOSITE_TEXT_MESSAGE_FROM_CHARACTER_TO_PLAYER(
		TEXT_SEXT,
		ePartStripper,
		TPART_SEXT_RUDE,
		CT_FRIEND,
		GET_CURRENT_PLAYER_PED_BIT(),
		GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling)),
		0)
							
ENDPROC
FUNC TEXT_LABEL_63 GENERATE_PLAYER_BOOTY_CALL_INTRO()

	TEXT_LABEL_63 sPlayerCallLabel
	
	sPlayerCallLabel = "BC_PLYRQ_"
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		sPlayerCallLabel += "M"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		sPlayerCallLabel += "T"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		sPlayerCallLabel += "F"
	ENDIF

	RETURN sPlayerCallLabel
ENDFUNC

// test for player calling, check availability and activation
PROC HANDLE_PLAYER_CALL_STRIPPER()

	BOOTY_CALL_CONTACT_ENUM BootyCallEnum

	SWITCH g_bootyCallData.bootyCallState
		CASE BOOTY_CALL_INACTIVE
			IF NOT g_bootyCallData.bPreventingBootyCall
				INT i
								
				REPEAT COUNT_OF(BOOTY_CALL_CONTACT_ENUM) i 
					BootyCallEnum = INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i )
					
					// only allow this if current booty call state is inactive
					IF IS_CALLING_CONTACT(GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(BootyCallEnum))
					AND ((g_bootyCallData.iAddNumberTime[i] + 2*60*1000 ) < GET_GAME_TIMER() // 2 minutes from when you get the number she will answer
					#IF IS_DEBUG_BUILD
					OR g_bDebugForceBootyCall
					OR g_bDebugForceBootyCallDecline
					#ENDIF )
					
						//Reset if she likes you enough
						IF DOES_STRIPPER_LIKE_PLAYER_ENOUGH_TO_COME_HOME(BootyCallEnum)
							//JDDO # 2 - Set it so after she likes you again it calls 
							SET_STRIPPER_CALLED_AFTER_ATTACKED(BootyCallEnum , FALSE )
							SET_STRIPPER_ATTACKED_ON_BOOTY_CALL(BootyCallEnum, FALSE)
							
							CDEBUG1LN(DEBUG_BOOTY_CALL,"Clearing bits for ", GET_BOOTY_CALL_NAME_AS_STRING(BootyCallEnum) ," likes player again with: ", GET_LIKE_FOR_STRIPPER_ID(BootyCallEnum) )
						ELSE
							CDEBUG1LN(DEBUG_BOOTY_CALL,GET_BOOTY_CALL_NAME_AS_STRING(BootyCallEnum) ,"only likes the player  ", GET_LIKE_FOR_STRIPPER_ID(BootyCallEnum) , " and does not want to go home with  player again." )
						ENDIF
						
						//Ignores you if you called after attacking her
						IF WAS_STRIPPER_CALLED_AFTER_ATTACKED(BootyCallEnum)
							CDEBUG1LN(DEBUG_BOOTY_CALL,"Player calling ", GET_BOOTY_CALL_NAME_AS_STRING(BootyCallEnum) , " is ignoring*****" )
							// set state to player calling
							g_bootyCallData.bStripperWillAccept = FALSE
						ELSE
						
							ADD_PED_FOR_DIALOGUE(bootyCallConversation, 3, NULL, GET_BOOTY_CALL_NAME_AS_STRING(BootyCallEnum))		
														
							TEXT_LABEL_63 sCallLabel
							TEXT_LABEL_63 sPlayerCallLabel
							
							sPlayerCallLabel = GENERATE_PLAYER_BOOTY_CALL_INTRO()
							CDEBUG1LN(DEBUG_BOOTY_CALL,"[CALL LINE] :", sPlayerCallLabel )
							
							IF WAS_STRIPPER_ATTACKED_ON_BOOTY_CALL(BootyCallEnum)
							
								SET_STRIPPER_CALLED_AFTER_ATTACKED(BootyCallEnum)
								
								g_bootyCallData.bStripperWillAccept = FALSE
								sCallLabel = "BC_HATE_"	
							
							#IF IS_DEBUG_BUILD
							ELIF g_bDebugForceBootyCallDecline
								g_bootyCallData.bStripperWillAccept = FALSE
								sCallLabel = "BC_STRN_"							
								//g_bootyCallData.iBootyCallID = REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CHAR_BLANK_ENTRY,GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i)),3,"BCAUD", "BC_STRN",BOOTY_CALL_QUEUE_TIME)
								CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Fell into Line 330 ")
						
							#ENDIF
						
							ELIF GET_GAME_TIMER() >= g_bootyCallData.iNextAvailableTime[i]
							AND CAN_TAKE_HOME_STRIPPER() AND IS_STRIPPER_AVAILABLE_AT_THIS_TIME_OF_DAY(BootyCallEnum)
							AND (GET_LIKE_FOR_STRIPPER_ID(BootyCallEnum) >= GET_STRIPPER_COME_HOME_THRESHOLD(BootyCallEnum)) //make sure stripper actually likes the player
							#IF IS_DEBUG_BUILD
							OR g_bDebugForceBootyCall
							#ENDIF
								
								g_bootyCallData.bStripperWillAccept = TRUE
								sCallLabel = "BC_STRY_"							
								//g_bootyCallData.iBootyCallID = REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CHAR_BLANK_ENTRY,GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i)),3,"BCAUD", "BC_STRY",BOOTY_CALL_QUEUE_TIME)
									
							
						
						
							
							ELSE
								g_bootyCallData.bStripperWillAccept = FALSE
								sCallLabel = "BC_STRN_"							
								//g_bootyCallData.iBootyCallID = REGISTER_CHAT_CALL_FROM_PLAYER_TO_CHARACTER(CHAR_BLANK_ENTRY,GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i)),3,"BCAUD", "BC_STRN",BOOTY_CALL_QUEUE_TIME)
								CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - Fell into Line 330 ")
						
							ENDIF
																		
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								ADD_PED_FOR_DIALOGUE(bootyCallConversation, 0, PLAYER_PED_ID(), "TREVOR")
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								ADD_PED_FOR_DIALOGUE(bootyCallConversation, 0, PLAYER_PED_ID(), "MICHAEL")
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								ADD_PED_FOR_DIALOGUE(bootyCallConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
							ENDIF

							sCallLabel += i
							
							CDEBUG1LN(DEBUG_BOOTY_CALL,"Call Label = ",sCallLabel )
							PLAYER_CALL_CHAR_CELLPHONE_MULTIPART_WITH_2_LINES(bootyCallConversation,GET_BOOTY_CALL_CONTACT_CONTACT_ENUM(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, i)),"BCAUD",sPlayerCallLabel,sPlayerCallLabel,sCallLabel,sCallLabel,CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT)
						ENDIF
						
						// set state to player calling
						g_bootyCallData.iStripperCalling = i
						g_bootyCallData.bootyCallState = BOOTY_CALL_PLAYER_CALLING
						CDEBUG1LN(DEBUG_BOOTY_CALL,"Booty Call - BOOTY_CALL_PLAYER_CALLING ")
					ENDIF
				ENDREPEAT
			ENDIF
		BREAK
		
		CASE BOOTY_CALL_PLAYER_CALLING
			IF NOT IS_CALLING_ANY_CONTACT()
			//IF GET_LAST_COMPLETED_CALL() = g_bootyCallData.iBootyCallID
				IF g_bootyCallData.bStripperWillAccept
					// when phone goes down, launch a stripperhome.sc with no stripper ped
					IF HAS_CELLPHONE_CALL_FINISHED()
					AND NOT WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
						START_BOOTY_CALL(INT_TO_ENUM(BOOTY_CALL_CONTACT_ENUM, g_bootyCallData.iStripperCalling))
						
					ENDIF
					
					IF WAS_LAST_PHONECALL_HUNG_UP_BY_JOYPAD() //WAS_LAST_CELLPHONE_CALL_INTERRUPTED()
						HANDLE_STRIPPER_TEXT_FOR_BEING_HUNG_UP_ON()
						RESET_BOOTY_CALL_TO_INACTIVE(FALSE)
					ENDIF
				ELSE
					// if they didn't accept, reset to inactive
					RESET_BOOTY_CALL_TO_INACTIVE(FALSE)
				ENDIF
			
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// set bPreventing - leave it on until the player can do a new booty call
PROC HANDLE_ALLOW_BOOTY_CALL()
	IF NOT g_bootyCallData.bPreventingBootyCall
		IF NOT CAN_START_NEW_BOOTY_CALL()
			RESET_BOOTY_CALL_TO_INACTIVE(FALSE)
			g_bootyCallData.bPreventingBootyCall = TRUE
		ENDIF
	ELSE
		IF CAN_START_NEW_BOOTY_CALL()
			//Switching back to allowing bootycalls, don't allow a sext right away
			IF g_bootyCallData.iNextCallPlayerTime < (GET_GAME_TIMER() + 2*60*1000)
				SET_NEXT_CALL_PLAYER_TIME(INITIAL_NEXT_CALL_PLAYER_TIME_MIN)
			ENDIF
			g_bootyCallData.bPreventingBootyCall = FALSE
		ENDIF
	ENDIF
ENDPROC


// ===========================================================================================================
//		Cleanup
// ===========================================================================================================

// PURPOSE: Ensures that the script gets a chance to cleanup under specific circumstances (ie: moving from SP to MP)
// 
PROC Script_Cleanup()
	PRINTLN("bootycallhandler.sc - Script_Cleanup()")
	IF candidateID != NO_CANDIDATE_ID
		Mission_Over(candidateID)
	ENDIF
	
	//If we are mid call we need to handle the cleanup of the call now. 
	//Not when this script resumes. #1739622
	IF g_bootyCallData.bootyCallState = BOOTY_CALL_PLAYER_CALLING
		HANDLE_STRIPPER_TEXT_FOR_BEING_HUNG_UP_ON()
		RESET_BOOTY_CALL_TO_INACTIVE(FALSE)
	ENDIF
	
	// CURRENTLY NOTHING TO CLEANUP
	TERMINATE_THIS_THREAD()
	
ENDPROC


// main script
SCRIPT
	#IF IS_DEBUG_BUILD
		DEBUG_PRINT_BOOTY_CALL_HANDLER("Launching booty call handler")
	#ENDIF
	
	// KEITH 21/12/12: This script now terminates when the game is moving between SP and MP and back again, so need different cleanup checks.
	//					Also had to remove the NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() call - it clashes with FORCE_CLEANUP_FLAG_SP_TO_MP leading to an assert.
	BOOL isInMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()
	
//	IF isInMultiplayer
//		Script_Cleanup()
//	ENDIF

	// This script needs to cleanup only when the game moves from SP to MP
	IF NOT (isInMultiplayer)
		// KEITH 21/12/12: Terminate this script when moving from SP to MP
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_MAGDEMO)
			#IF IS_DEBUG_BUILD
				PRINTSTRING("...BootyCallHandler.sc has been forced to cleanup (SP to MP)")
				PRINTNL()
			#ENDIF
			
			Script_Cleanup()
		ENDIF
	ENDIF
	
	// create widgets
	#IF IS_DEBUG_BUILD
		CREATE_STRIP_CLUB_WIDGETS(stripClubWidgets)
	#ENDIF
	
	// initialise time
	SET_NEXT_CALL_PLAYER_TIME()

    WHILE TRUE
       	WAIT(0)
		IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
		
			CPRINTLN(DEBUG_BOOTY_CALL, "Booty Call Handler cleaning up as we have gone on mission.")
			Script_Cleanup()
			
		ENDIF
		
		// KEITH 21/12/12: Terminate this script when moving from MP to SP
		IF (isInMultiplayer)
			IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
				#IF IS_DEBUG_BUILD
					PRINTSTRING("...BootyCallHandler.sc has been forced to cleanup (MP to SP)")
					PRINTNL()
				#ENDIF
				
				Script_Cleanup()
			ENDIF
		ENDIF
		
		// set can make call
		HANDLE_ALLOW_BOOTY_CALL()
		
		HANDLE_STRIPPER_SEXT_FOR_BOOTY_CALL()
		// handle player calling girl
		HANDLE_PLAYER_CALL_STRIPPER()
		
		// debug only: activate/deactivate as booty call with widget
		#IF IS_DEBUG_BUILD
			MONITOR_BOOTY_CALL_DEBUG_ACTIVATION()
		#ENDIF
	ENDWHILE
	
	// get rid of widgets
	#IF IS_DEBUG_BUILD
        DELETE_STRIP_CLUB_WIDGETS(stripClubWidgets)
	#ENDIF
	TERMINATE_THIS_THREAD()
ENDSCRIPT
