USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_game.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ambientBlimp.sc
//
//		AUTHOR			:	David Roberts
//
//		DESCRIPTION		:	Preorder bonus script which gives the player the ability to
//                          call a blimp to be delivered after they have unlocked it by
//                          landing on the world blimp.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// Constants
CONST_INT	NUM_VEHICLES_TO_SCAN 4
CONST_FLOAT BLIMP_HEIGHT 300.0

// Enums
ENUM AMBIENT_BLIMP_STATE
	ABS_DETECT_UNLOCK,
	ABS_DISPLAY_HELP
ENDENUM
AMBIENT_BLIMP_STATE eBlimpState = ABS_DETECT_UNLOCK

// ------------------------------------------------------------------------------------------
//		Cleanup
// ------------------------------------------------------------------------------------------
/// PURPOSE:
///    Terminates the blimp script
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_AMBIENT, "ambientBlimp : Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

// ------------------------------------------------------------------------------------------
//		Functions
// ------------------------------------------------------------------------------------------
/// PURPOSE:
///    Does the Blimp contact already exist in the player's phonebook?
FUNC BOOL HAS_PLAYER_ALREADY_UNLOCKED_BLIMP()

	IF GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 0) = LISTED
	AND GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 1) = LISTED
	AND GLOBAL_CHARACTER_SHEET_GET_PHONEBOOK_STATE(CHAR_BLIMP, 2) = LISTED
		IF IS_PREORDER_GAME()
		OR IS_COLLECTORS_EDITION_GAME()
		OR IS_SPECIAL_EDITION_GAME()
		OR IS_JAPANESE_SPECIAL_EDITION_GAME()
		OR IS_LAST_GEN_PLAYER()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Works out whether the player is near to the blimp
FUNC BOOL IS_BLIMP_NEAR_TO_PLAYER()
	
	VEHICLE_INDEX tempVehicleArray[NUM_VEHICLES_TO_SCAN]
	INT 		  i, iTemp
			
	iTemp = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), tempVehicleArray)
	
	IF iTemp > 0
		REPEAT iTemp i 
			IF IS_VEHICLE_DRIVEABLE(tempVehicleArray[i])
				IF GET_ENTITY_MODEL(tempVehicleArray[i]) = BLIMP
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	RETURN FALSE
ENDFUNC

// ------------------------------------------------------------------------------------------
//		States
// ------------------------------------------------------------------------------------------
/// PURPOSE:
///    Detects that the player landed and is onfoot on the blimp
FUNC BOOL HAS_PLAYER_LANDED_ON_BLIMP()
	
	// Blimp DLC active!
	IF IS_PREORDER_GAME()
	OR IS_COLLECTORS_EDITION_GAME()
	OR IS_SPECIAL_EDITION_GAME()
	OR IS_JAPANESE_SPECIAL_EDITION_GAME()
	OR IS_LAST_GEN_PLAYER()
	
		// Check for player landing on the blimp and getting out on foot
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1) // See B*1568380 Just unlock from start of the game
				CPRINTLN(DEBUG_AMBIENT, "ambientBlimp : Armenian1 now completed so unlocking blimp contact")
				// Add blimp contacts
				ADD_CONTACT_TO_PHONEBOOK(CHAR_BLIMP, ALL_OWNERS_BOOKS, FALSE) // See B*1568380 Don't display signifier
				MAKE_AUTOSAVE_REQUEST()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Displays one time help message after unlocking the blimp contact
FUNC BOOL HAS_BLIMP_HELP_DISPLAYED()
	
	SWITCH GET_FLOW_HELP_MESSAGE_STATUS("BLIMP_UNLOCK")
		CASE FHS_EXPIRED
			ADD_HELP_TO_FLOW_QUEUE("BLIMP_UNLOCK", FHP_MEDIUM, 0, 1000)
		BREAK
		CASE FHS_DISPLAYED
			// Autosave and terminate script
			SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_BLIMP_UNLOCK)
			MAKE_AUTOSAVE_REQUEST()
			RETURN TRUE
		BREAK
		CASE FHS_QUEUED
			// just wait
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// ------------------------------------------------------------------------------------------
//		Main Loop
// ------------------------------------------------------------------------------------------
SCRIPT

	CPRINTLN(DEBUG_AMBIENT, "ambientBlimp : Launched")
	
	// Cleanup callbacks
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CPRINTLN(DEBUG_AMBIENT, "ambientBlimp : Force Cleanup")
		Script_Cleanup()
	ENDIF
	
	// Detect whether the player has unlocked the blimp already - needed if the script
	// is relaunched before the help text has had a chance to display
	IF HAS_PLAYER_ALREADY_UNLOCKED_BLIMP()
		eBlimpState = ABS_DISPLAY_HELP
	ENDIF

	// Main update
	WHILE (TRUE)
		
		SWITCH eBlimpState
		
			// Landing on blimp
			CASE ABS_DETECT_UNLOCK	
				IF HAS_PLAYER_LANDED_ON_BLIMP()
					eBlimpState = ABS_DISPLAY_HELP
				ENDIF	
			BREAK
			
			// Unlock text
			CASE ABS_DISPLAY_HELP
				IF HAS_BLIMP_HELP_DISPLAYED()
					Script_Cleanup()
				ENDIF
			BREAK
		ENDSWITCH
		
		WAIT(1000)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
