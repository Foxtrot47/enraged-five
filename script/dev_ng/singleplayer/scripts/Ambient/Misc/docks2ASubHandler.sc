//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ryan Pendant					Date: 05/30/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							Docks 2A Sub Handler								│
//│																				│
//│		Manages sub hanging from the rope at the end of docks heist 2A.			│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals.sch"
USING "rage_builtins.sch"
USING "brains.sch"


CONST_INT	BIT_SUB_OWNED			0
CONST_INT	BIT_PLAYER_LEFT_AREA	1

VEHICLE_INDEX				vehSub
ENTITY_INDEX				ropeHook

INT iState = 0


PROC Cleanup_Handler()
	CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Handler cleaning up.")
	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehSub)
	SET_ENTITY_AS_NO_LONGER_NEEDED(ropeHook)
	
	CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Handler terminated.")
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT(StructDocksSubHandover sSubHandover)
	CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Sub handler started.")

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Handler was forced to clean up.")
		Cleanup_Handler()
	ENDIF
	
	vehSub = sSubHandover.subHandover
	ropeHook = sSubHandover.hookHandover
	
	WHILE NOT IS_BIT_SET(iState, BIT_PLAYER_LEFT_AREA)

		//Wait for global vehicle sub to exist and grab ownership of it.
		IF NOT IS_BIT_SET(iState, BIT_SUB_OWNED)
			IF DOES_ENTITY_EXIST(vehSub)
				IF IS_VEHICLE_DRIVEABLE(vehSub)
					//Take ownership of sub.
					SET_ENTITY_AS_MISSION_ENTITY(vehSub, FALSE, TRUE)
					CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Handler took ownership of global Sub vehicle index.")
					
					FREEZE_ENTITY_POSITION( vehSub, TRUE )
					SET_ENTITY_PROOFS(vehSub, TRUE, TRUE, TRUE, TRUE, TRUE)
					
					IF DOES_ENTITY_EXIST( ropeHook )
						SET_ENTITY_AS_MISSION_ENTITY(ropeHook, FALSE, TRUE)
						CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> Handler took ownership of global rope hook entity index.")
						FREEZE_ENTITY_POSITION( ropeHook, TRUE )
						SET_ENTITY_PROOFS(ropeHook, TRUE, TRUE, TRUE, TRUE, TRUE)
					ENDIF
					
					SET_BIT(iState, BIT_SUB_OWNED)
				ENDIF
			ENDIF
			
		//Sub owned. Wait for cleanup conditions.
		ELIF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehSub)
				FLOAT fDistanceSquaredFromSub = VDIST2( GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehSub))
				IF fDistanceSquaredFromSub > 90000		//300 meters, clean up if player is looking or not
					CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> The player has left the area.")
					SET_BIT(iState, BIT_PLAYER_LEFT_AREA)
				ELIF fDistanceSquaredFromSub > 40000	//200 meters, clean up if player isn't looking
				AND NOT IS_ENTITY_ON_SCREEN(vehSub)
					CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> The player has left the area.")
					SET_BIT(iState, BIT_PLAYER_LEFT_AREA)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "<SUB-HANDLER> The sub is destroyed.")
				SET_BIT(iState, BIT_PLAYER_LEFT_AREA)
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE

	Cleanup_Handler()
ENDSCRIPT
