//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 28/04/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								Michael 4 Lead-Out 								│
//│																				│
//│		Manages fading in the game and playing a lead-out sequence after 		│
//│		mission Michael4 has just passed.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_camera.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "vehicle_gen_public.sch"
USING "mission_stat_public.sch"
USING "comms_control_public.sch"

CONST_INT	STAGE_WAIT_ENDSCREEN_START	0
CONST_INT	STAGE_WARP_MICHAEL			1
CONST_INT	STAGE_START_CUTSCENE		2
CONST_INT	STAGE_DO_CUTSCENE			3
CONST_INT	STAGE_CLEAN_CUTSCENE		4
CONST_INT	STAGE_FINISHED				5

BOOL bClearedMichaelDamage = FALSE

INT iStage = STAGE_WAIT_ENDSCREEN_START

VEHICLE_INDEX vehMichael
PED_INDEX pedMichael

TEXT_LABEL_31 tlRoomType
INTERIOR_INSTANCE_INDEX savehouseInterior

PROC Cleanup_Script()
	CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Leadout handler terminated.")
	REMOVE_CUTSCENE()
	g_bPauseCommsQueues = FALSE
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Leadout handler started.")
	
	CLEAR_AUTOSAVE_REQUESTS()

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Leadout handler was forced to cleanup.")
		Cleanup_Script()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Cleaning up due to gameflow launch.")
			Cleanup_Script()
		ENDIF
	#ENDIF
	
	WHILE iStage != STAGE_FINISHED
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SWITCH iStage
				CASE STAGE_WAIT_ENDSCREEN_START
					IF IS_RESULT_SCREEN_DISPLAYING()
						g_bEndScreenSuppressFadeIn = TRUE
						iStage = STAGE_WARP_MICHAEL
					ENDIF
					CLEAR_AUTOSAVE_REQUESTS()
				BREAK
			
				CASE STAGE_WARP_MICHAEL
					IF NOT IS_RESULT_SCREEN_DISPLAYING()
						CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Warping Michael to beach front.")
						g_bPauseCommsQueues = TRUE
						DO_PLAYER_WARP_WITH_LOAD_AND_PAUSE(<<-1613.8690, -1054.9580, 12.0722>>)
						CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Warp finished. Loading cutscene and waiting for end screen.")
						REQUEST_CUTSCENE("SOL_5_MCS_2_P5")
						REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
						SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_POLOSHIRT_JEANS_1, FALSE)
						WAIT(500)
						iStage = STAGE_START_CUTSCENE
					ENDIF
					CLEAR_AUTOSAVE_REQUESTS()
				BREAK

				CASE STAGE_START_CUTSCENE
					IF HAS_THIS_CUTSCENE_LOADED("SOL_5_MCS_2_P5")
					AND HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL, VEHICLE_TYPE_CAR)
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()) 
						CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Cutscene loaded. Starting cutscene.")
						SET_CLOCK_TIME(7,5,30)
						SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
    					CLEAR_WEATHER_TYPE_PERSIST()
						CLEAR_AREA(<<-1590.0551, -1037.0665, 12.0186>>, 4.0, TRUE)
						CREATE_PLAYER_VEHICLE(vehMichael, CHAR_MICHAEL, <<-1590.0551, -1037.0665, 12.0186>>, 24.5062, TRUE, VEHICLE_TYPE_CAR)
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehMichael, <<-1590.0551, -1037.0665, 12.0186>>, 24.5062)
						CLEAR_PED_WETNESS(PLAYER_PED_ID())
						RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
						START_CUTSCENE()
						iStage = STAGE_DO_CUTSCENE
					ENDIF
				BREAK
				CASE STAGE_DO_CUTSCENE
					IF IS_CUTSCENE_PLAYING()
						IF NOT bClearedMichaelDamage
							pedMichael = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Michael", PLAYER_ZERO))
							IF DOES_ENTITY_EXIST(pedMichael)
								IF NOT IS_ENTITY_DEAD(pedMichael)
									CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Clearing damage on Michael.")
									CLEAR_PED_BLOOD_DAMAGE(pedMichael)
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_TORSO, "ALL")
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_HEAD, "ALL")
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_LEFT_LEG, "ALL")
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_RIGHT_LEG, "ALL")
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_LEFT_ARM, "ALL")
									CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedMichael, PDZ_RIGHT_ARM, "ALL")
									bClearedMichaelDamage = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_SCREEN_FADED_IN()
							IF NOT IS_SCREEN_FADING_IN()
								CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Starting screen fade in.")
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
								g_bEndScreenSuppressFadeIn = FALSE
							ENDIF
						ELSE
							iStage = STAGE_CLEAN_CUTSCENE
						ENDIF
					ENDIF					
				BREAK
				CASE STAGE_CLEAN_CUTSCENE
					IF NOT IS_CUTSCENE_PLAYING()
					
						//B* - 1978122 Force blip interior update
						SET_STATIC_BLIP_ACTIVE_STATE(g_sSavehouses[SAVEHOUSE_MICHAEL_BH].eBlip, TRUE)	
						GET_SAVEGAME_ROOM_TYPE(SAVEHOUSE_MICHAEL_BH, tlRoomType)
						savehouseInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(g_sSavehouses[SAVEHOUSE_MICHAEL_BH].vSpawnCoords, tlRoomType)
						IF g_sShopSettings.playerInterior = savehouseInterior
							g_sShopSettings.playerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
						ENDIF
						IF savehouseInterior != NULL
							UNPIN_INTERIOR(savehouseInterior)
						ENDIF
						
						CPRINTLN(DEBUG_FLOW, "<MIC4-LEADOUT> Cutscene ended.")
						g_sAutosaveData.bFlushAutosaves = FALSE
						MAKE_AUTOSAVE_REQUEST()
						g_bPauseCommsQueues = FALSE
						ADD_GLOBAL_COMMUNICATION_DELAY(CC_GLOBAL_DELAY_POST_MISSION)
						RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, FALSE)
						iStage = STAGE_FINISHED
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		WAIT(0)
	ENDWHILE
	
	Cleanup_Script()
ENDSCRIPT
