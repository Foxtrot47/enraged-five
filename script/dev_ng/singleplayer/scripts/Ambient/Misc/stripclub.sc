//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   stripclub.sc                                                //
//      AUTHOR     		:	Evan Lawson
//      DESCRIPTION     :   Strip Club		                                            //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"

USING "commands_object.sch"
USING "script_drawing.sch"
USING "lineActivation.sch"
USING "drunk_public.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "script_blips.sch"
USING "clubs_public.sch"
USING "minigame_UIInputs.sch"
USING "friendActivity_public.sch"
USING "cutscene_public.sch"
USING "rgeneral_include.sch"
USING "building_control_public.sch"
USING "flow_processing_game.sch"
USING "transition_common.sch"
USING "freemode_header.sch"
//USING "context_control_public.sch"

CONST_INT MP_STRIPCLUB 0
CONST_INT USE_STRIPCLUB_DEBUG 1
//USING "stripclub_public.sch"
//USING "stripclub.sch"
//USING "stripclub_common.sch"
//USING "stripclub_anim.sch"
//USING "stripclub_ai.sch"
//USING "stripclub_task.sch"
//USING "stripclub_helpers.sch"
//USING "stripclub_stage.sch"
//USING "stripclub_strippers.sch"
//USING "stripclub_workers.sch"
//USING "stripclub_cutscenes.sch"
//USING "stripclub_hotspots.sch"
//USING "stripclub_lapdance.sch"
//USING "stripclub_patrons.sch"
//USING "stripclub_friend.sch"
USING "stripclub_setup.sch"
USING "load_queue_public.sch"

LoadQueueLarge sLoadQueue

// main script
SCRIPT
	INT i
	CPRINTLN(DEBUG_SCLUB, "Start strip club script")
	
	CLEAR_BITMASK_AS_ENUM( g_StripclubGlobals.iStripclubFlags, GSF_IN_CLUB )
	
	SET_LOAD_QUEUE_LARGE_FRAME_DELAY(sLoadQueue, 5)
	
    IF GET_ALLOCATED_STACK_SIZE() != FRIEND_STACK_SIZE
		CPRINTLN(DEBUG_SCLUB, "Stripclub started with an incorrect stack size ", GET_ALLOCATED_STACK_SIZE())
		TERMINATE_THIS_THREAD()
	ENDIF
	
    IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_MAGDEMO | FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_SCLUB, "Force cleanup of strip club")
		IF stripClubStage = STAGE_DANCE
			HANDLE_TELEPORT_OUTSIDE_STRIPCLUB_DURING_FORCE_CLEANUP(sLoadQueue)
		ENDIF
		
		CLEANUP_STRIP_CLUB(sLoadQueue)
    ENDIF
	
	IF g_bMagDemoActive
		PRINTLN("Mag Demo is Active. Quit Stripclub Script")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF NOT STRIPCLUB_IS_PLAYER_ON_MISSION() AND g_bDisableStripClubs
		//There is no reason why the club should be dsiabled if they arn't on a missionm someone fogot to set it too false
		CPRINTLN(DEBUG_SCLUB, "Disable stripclub was set when it shuold not have been")
		g_bDisableStripClubs = FALSE
	ENDIF
	
	IF DOES_TREVOR_OWN_CLUB()
		ADJUST_CLUB_REPUTATION(100)
	ENDIF
	
//	SCRIPT_PLAYSTATS_MISSION_STARTED(GET_MINIGAME_STAT_ID(MINIGAME_STRIPCLUB))
	
	IF (g_savedGlobals.sFlow.isGameflowActive)
		IF NOT (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB)))
			PRINTLN("Flowflag not active! Lock doors!")
			STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0)
			STRIPCLUB_CLOSE_BACK_DOOR(TRUE, 0)
			
			WHILE NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<114.64, -1290.34, 29.68>>) < (100.0*100.0)
			AND NOT (GET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_MINIGAME_ACTIVE, ENUM_TO_INT(MINIGAME_STRIPCLUB)))
				IF IS_STRIPCLUB_MP()
					CLEANUP_STRIP_CLUB(sLoadQueue)
				ENDIF
				
				WAIT(0) //wait here so stripclub doesn't relaunch
			ENDWHILE
			
			CLEANUP_LOAD_QUEUE_LARGE(sLoadQueue)
			TERMINATE_THIS_THREAD()
		ELIF NOT g_bDisableStripClubs
			PRINTLN("Flow Flag is set, Unlock doors!")
			STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, 0)
			OPEN_STRIPCLUB_BACK_DOOR_IF_PLAYER_OWNS_CLUB()
		ELSE
			PRINTLN("Club started disabled")
		ENDIF
	ELSE
		PRINTLN("Stripclub - GTA isn't in game flow?")
	ENDIF
	
	//the bouncer out in front is handled by a different script
	OUTSIDE_BOUNCER_INFO BouncerInfo
	REQUEST_SCRIPT("sclub_front_bouncer")
	
	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(GET_PLAYER_INDEX())

	BOOL bWasDisabled = g_bDisableStripClubs
	SET_STRIPCLUB_GLOBAL_THROWN_OUT(FALSE)
	
	IF IS_SWTICH_INSIDE_CLUB_AS_TREVOR()
		SET_BITMASK_AS_ENUM(iStripClubBits_Cutscenes, STRIP_CLUB_SWITCHED_INSIDE_TO_TREVOR)
	ENDIF
	
    WHILE TRUE
        WAIT(0)
		
		// Replay Camera Movement Blocking (Strip Club)
		IF stripclubInside <> SC_INSIDE_MANAGE
			IF IS_REPLAY_RECORDING()
				IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)	//Fix for bugs 2209274, 2198600, 2195133, 2194867
        		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), <<115.93143, -1290.48279, 27.94359>>) < (70.0*70.0)
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2209617
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		STRIPCLUB_MANAGE_END_CUTSCENE_TO_FIRST_PERSON_CAMERA_EFFECT()
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		IF GET_ALLOCATED_STACK_SIZE() != FRIEND_STACK_SIZE
			PRINTLN("Stack size of stripclub script ", GET_ALLOCATED_STACK_SIZE())
			PRINTLN("Stack size is no longer ", FRIEND_STACK_SIZE, "WTF")
		ENDIF
		
//		CPRINTLN(DEBUG_SCLUB, "Lapdance mission ID ", candidateIDLapdance)
//		
//		IF candidateIDLapdance != NO_CANDIDATE_ID
//			CPRINTLN(DEBUG_SCLUB, " Canidadate mission is running in club")
//		ENDIF

		UPDATE_STRIPPER_DISTANCE_TO_PLAYER()
		
		IF HAS_SCRIPT_LOADED("sclub_front_bouncer")
		AND (NOT g_bDisableStripClubs AND NOT g_bTriggerSceneActive OR IS_STRIPCLUB_GLOBAL_FORCE_FRONT_BOUNCER_BE_CREATED())
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("sclub_front_bouncer")) = 0
				BouncerInfo.bIsMp = FALSE
				START_NEW_SCRIPT_WITH_ARGS("sclub_front_bouncer", BouncerInfo, SIZE_OF(OUTSIDE_BOUNCER_INFO), DEFAULT_STACK_SIZE)
			ENDIF
		ENDIF
		
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		OR IS_SKYSWOOP_MOVING() OR IS_TRANSITION_ACTIVE()
			CLEAR_ALL_STRIP_CLUB_HELP()
			CLEAR_ALL_STRIP_CLUB_PRINT()
		ENDIF
		
		IF bWasDisabled != g_bDisableStripClubs
			IF g_bDisableStripClubs
				CPRINTLN(DEBUG_SCLUB, "Stripclub became disbaled while the player was near or in it")
			ELSE
				CPRINTLN(DEBUG_SCLUB, "Stripclub was disabled, but reenabled while the player was near or in it")
				STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, 0)
				OPEN_STRIPCLUB_BACK_DOOR_IF_PLAYER_OWNS_CLUB()
			ENDIF
			bWasDisabled = g_bDisableStripClubs
		ENDIF
		
        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<114.64, -1290.34, 29.68>>) < (STRIP_CLUB_INIT_DISTANCE*STRIP_CLUB_INIT_DISTANCE)
		AND NOT IS_TRANSITION_ACTIVE()
			
			
			
            IF NOT g_bDisableStripClubs // AND NOT g_bTriggerSceneActive
				#IF IS_DEBUG_BUILD
					DO_DEBUG_SKIPS()
                #ENDIF
				
				// test is in club and can approach
				IF (stripClubStage = STAGE_DANCE)  // bit hacky but avoids random issue where player someone was determined not in the club
					SET_IN_CLUB_FLAG(TRUE)
				ELIF (stripClubStage = STAGE_GO_TO_VIP_ROOM) //same
				AND (gotoStage > STRIPCLUB_GOTO_ROOM_TRIGGER_CUTSCENE)
					SET_IN_CLUB_FLAG(TRUE)
				ELSE
					SET_IN_CLUB_FLAG(IS_PLAYER_IN_CLUB())
				ENDIF
				
				IF SHOULD_STRIPCLUB_DISABLE()
					IF (NOT IS_IN_CLUB_FLAG_SET()
					OR (NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()) AND stripClubStage != STAGE_DANCE))
					AND NOT IS_PLAYER_ON_BOTTOM_FLOOR_OF_CLUB()
						CPRINTLN(DEBUG_SCLUB, "Locking stripclub doors when on mission")
						STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0)
						STRIPCLUB_CLOSE_BACK_DOOR(TRUE, 0)
						g_bDisableStripClubs = TRUE
					ELIF  IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_PLAYER_IS_IN_INTERIOR)
						CPRINTLN(DEBUG_SCLUB, "Player is inside, Unlock doors on mission")
						STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, 0)
						OPEN_STRIPCLUB_BACK_DOOR_IF_PLAYER_OWNS_CLUB()
					ENDIF
					SET_HIGH_FUNCTIONALITY(FALSE)
				ELSE
					SET_STRIPPER_APPROACH_ENABLED(TRUE) //incase someone forgot to set it back
					SET_HIGH_FUNCTIONALITY(TRUE)
				ENDIF
				
				// check hostility
				IF stripClubStage != STAGE_CLUB_HOSTILE
					BS_DelayedAggro=0
			        IF SHOULD_CLUB_GO_HOSTILE()
						SET_BITMASK_ENUM_AS_ENUM(iStripClubBits_Workers, STRIP_CLUB_BIT_WORKER_BOUNCER_AGGRO)
						IF IS_PED_IN_STRIPCLUB_INTERIOR(PLAYER_PED_ID())
							SEND_CLUB_HOSTILE(IS_HOSTILE_FROM_MELEE())
						ELSE
			            	
						ENDIF
			        ENDIF
				ELSE
					//Delayed hostile behaviour due to streaming
					IF BS_DelayedAggro > 0
						REPEAT MAX_NUMBER_CLUB_STAFF i
							IF IS_BIT_SET(BS_DelayedAggro,i)
								IF SET_CLUB_STAFF_PED_HOSTILE(i, IS_HOSTILE_FROM_MELEE())
									CLEAR_BIT(BS_DelayedAggro,i)
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF IS_HOSTILE_FROM_SHOOTING()
						REPEAT MAX_NUMBER_CLUB_STAFF i
							IF NOT IS_STRIP_CLUB_PED_USING_RANGED_WEAPON(i)
								SET_CLUB_STAFF_PED_HOSTILE(i, false)
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
				
				IF (IS_IN_CLUB_FLAG_SET() OR IS_PLAYER_UNDER_CANOPY())
					IF NOT ALLOW_PLAYER_TO_RUN_IN_CLUB(sLoadQueue)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
					ENDIF
				ENDIF
				
				IF IS_PED_IN_STRIPCLUB_INTERIOR(PLAYER_PED_ID())
					STRIPCLUB_SUPPRESS_PLAYERS_WEAPON()
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitationTriggers, TRUE)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableAgitation, TRUE)
					SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableTalk, TRUE)
					SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STRIPCLUB, FALSE)
				ELSE
					IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
					AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
						SET_STATIC_BLIP_ACTIVE_STATE(STATIC_BLIP_MINIGAME_STRIPCLUB, TRUE)
					ENDIF
				ENDIF
				
				SET_CAN_APPROACH_FLAG(CAN_APPROACH_STRIPPER())
				
				IF stripClubStage > STAGE_INIT_CLUB
				AND stripClubStage != STAGE_THROWOUT
					// handle load inside assets
					HANDLE_INSIDE_ASSETS(sLoadQueue, FALSE, stripClubStage != STAGE_CLUB_HOSTILE)
				ENDIF
				
				//Handle Friend Activity
				IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
					MANAGE_FRIEND_IN_STRIPCLUB()
				ENDIF
				
                // run appropriate stage
                SWITCH stripClubStage
					CASE STAGE_INIT_CLUB
						IF INIT_STRIP_CLUB()
							BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_STRIP)	//#1514495 stockmarket
							stripClubStage = STAGE_WANDER_CLUB
						ENDIF
					BREAK
					
					CASE STAGE_WANDER_CLUB
						STRIPCLUB_CLOSE_FRONT_DOOR(FALSE, 0)
						OPEN_STRIPCLUB_BACK_DOOR_IF_PLAYER_OWNS_CLUB()
						DO_STAGE_WANDER_CLUB()
					BREAK
					
					CASE STAGE_GO_TO_VIP_ROOM
						DO_STAGE_GO_TO_ROOM()
					BREAK
					
					CASE STAGE_DANCE
						DO_STAGE_DANCE()
					BREAK
					
                    CASE STAGE_CLUB_HOSTILE
                        DO_STAGE_CLUB_HOSTILE()
                    BREAK
					
					CASE STAGE_THROWOUT
						DO_STAGE_THROWOUT()
					BREAK
					
					CASE STAGE_GO_HOME_WITH_STRIPPER
					BREAK
                ENDSWITCH
				
				CLEAR_BITMASK_AS_ENUM(iStripClubBits_General2, STRIP_CLUB_GENERAL2_SET_SYNC_CAMERA_THIS_FRAME)
			ELIF g_bDisableStripClubs //OR g_bTriggerSceneActive
			AND NOT Is_Player_Timetable_Scene_In_Progress() 
				SET_HOSTILE_FROM_MELEE(FALSE)
				SET_HOSTILE_FROM_SHOOTING(FALSE)
				SET_STRIPCLUB_GLOBAL_HOSTILE(FALSE)
				
				IF stripClubStage > STAGE_INIT_CLUB
					stripClubStage = STAGE_WANDER_CLUB
				ENDIF
				
				IF IS_PLAYER_ON_BOTTOM_FLOOR_OF_CLUB()
				AND NOT IS_SCREEN_FADED_OUT() //only renable if club is actually visible
					CPRINTLN(DEBUG_SCLUB, "Player is inside disabled club? Renable")
					g_bDisableStripClubs = FALSE
				ELIF NOT SHOULD_STRIPCLUB_DISABLE()
					CPRINTLN(DEBUG_SCLUB, "Player not on mission and club is disabled? Enabling club.")
					g_bDisableStripClubs = FALSE
				ELSE
					//PRINTLN("Stripclub is disabled")
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						STRIPCLUB_CLOSE_FRONT_DOOR(TRUE, 0)
						STRIPCLUB_CLOSE_BACK_DOOR(TRUE, 0)
					ENDIF
					
					IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_INSIDE_NEEDS_CLEANUP)
						CPRINTLN(DEBUG_SCLUB, "Cleanup inside")
						CLEANUP_STRIPCLUB_INSIDE(sLoadQueue, TRUE)
						CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_INSIDE_NEEDS_CLEANUP)
					ENDIF
				ENDIF
            ENDIF
        ELSE
			IF NOT IS_TRANSITION_ACTIVE()
		        // cleanup if too far away
				PRINTLN("Player is too far away or dead, cleaning up script")
		        CLEANUP_STRIP_CLUB(sLoadQueue)
			ENDIF
        ENDIF
    ENDWHILE
ENDSCRIPT
