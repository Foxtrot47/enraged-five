// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:		photographyWildlife.sc
//		DATE			:		24/07/2014
//		AUTHOR			:		Colin Considine
//		DESCRIPTION		:		Franklin takes pictures of the wildlife in Los Santos with 
//								his camera phone for a competition.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


//===============================================================================================================================================================================
//		Includes
//===============================================================================================================================================================================
USING "rage_builtins.sch"
USING "globals.sch"
USING "RC_Threat_Public.sch"

USING "photographyWildlife.sch"



//*************************************
//		ENUMS | VARIABLES - START
//*************************************


//================================================================================================================================================
//		Enums
//================================================================================================================================================

// SUCCESSFUL PHOTOGRAPH CHECK STATES
ENUM PHOTOGRAPH_PROCESSING_STAGE_FLAG
	PPF_WAIT_FOR_PHOTOGRAPH,
	PPF_VALID_PHOTOGRAPH_CHECK,
	PPF_VALID_PHOTOGRAPH_CHECK_2_TEST,
	PPF_TRACKING,
	PPF_PED_MODEL_CHECK,
	PPF_SEND_PHOTOGRAPH,
	PPF_HANDLE_UPDATES,
	PPF_CLEAN_UP,
	PPF_WAIT_FOR_CAMERA_MODE
ENDENUM

PHOTOGRAPH_PROCESSING_STAGE_FLAG e_PhotographProcessStage = PPF_WAIT_FOR_PHOTOGRAPH


// SEND VALID PHOTOGRAPH STATES
ENUM SEND_PHOTOGRAPH_STAGE_FLAG
	SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN,
	SPF_HANDLE_CONTACT_SCREEN
ENDENUM

SEND_PHOTOGRAPH_STAGE_FLAG e_SendPhotographState = SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN


// SCALEFORM STATES
ENUM SCALEFORM_STAGE_FLAG
	SSF_LOAD_SCALEFORM,
	SSF_DISPLAY_SCALEFORM,
	SSF_ANIMATE_SCALEFORM,
	SSF_CLEANUP_SCALEFORM
ENDENUM

SCALEFORM_STAGE_FLAG e_ScaleformStage = SSF_LOAD_SCALEFORM


// ACTIVITY FEED ANIMAL ENUM
ACTIVITY_FEED_ANIMAL e_AnimalActivityFeed = AFA_NONE


//================================================================================================================================================
//		Constants
//================================================================================================================================================
CONST_INT NUM_MAX_ANIMALS			20
CONST_INT NUM_MAX_EMAILS			20
CONST_INT NUM_MAX_EMAIL_SUBSTRINGS	10
CONST_INT EMAIL_DELAY_TIME			5

CONST_FLOAT MIN_SCREEN_WIDTH_VALUE 	0.05
CONST_FLOAT MAX_SCREEN_WIDTH_VALUE 	0.95
CONST_FLOAT MIN_SCREEN_HEIGHT_VALUE 0.05
CONST_FLOAT MAX_SCREEN_HEIGHT_VALUE 0.95


//================================================================================================================================================
//		Structs
//================================================================================================================================================
STRUCT EMAIL_HELPER
	EMAIL_MESSAGE_ENUMS e_MailID
ENDSTRUCT

EMAIL_HELPER e_MailHelper[NUM_MAX_EMAILS + 1]


STRUCT MISSION_PED_STRUCT
	PED_INDEX	ped
	BLIP_INDEX	blip
	MODEL_NAMES	model
	INT			i_Event				=	0
	BOOL 		b_Remove 			=	FALSE
	BOOL		b_CheckFinished		=	FALSE
ENDSTRUCT

MISSION_PED_STRUCT	a_Peds[NUM_MAX_ANIMALS]


//================================================================================================================================================
//		Timers
//================================================================================================================================================
structTimer t_PutPhoneAwayTimer
structTimer t_SendEmailDelayTimer


//================================================================================================================================================
//		Scaleform
//================================================================================================================================================
SCALEFORM_INDEX sf_Movie


//================================================================================================================================================
//		Peds
//================================================================================================================================================
PED_INDEX e_FocusAnimal


//================================================================================================================================================
//		Vehicles
//================================================================================================================================================


//================================================================================================================================================
//		Objects
//================================================================================================================================================


//================================================================================================================================================
//		Blips
//================================================================================================================================================


//================================================================================================================================================
//		Ropes
//================================================================================================================================================


//================================================================================================================================================
//		Sequence ID
//================================================================================================================================================


//================================================================================================================================================
//		Cameras
//================================================================================================================================================


//================================================================================================================================================
//		PTFX
//================================================================================================================================================


//================================================================================================================================================
//		Entities
//================================================================================================================================================


//================================================================================================================================================
//		Models
//================================================================================================================================================
MODEL_NAMES m_TargetAnimalModel


//================================================================================================================================================
//		Strings
//================================================================================================================================================


//================================================================================================================================================
//		Bools
//================================================================================================================================================
BOOL b_DisplayScaleform
BOOL b_InvalidModel 		=	FALSE
BOOL b_PhoneForcedAway 		=	FALSE
BOOL b_SendDynamicEmail = FALSE


//================================================================================================================================================
//		Vectors
//================================================================================================================================================
VECTOR VECTOR_ZERO			=	<<0, 0, 0>>
VECTOR v_ModelMin			=	VECTOR_ZERO
VECTOR v_ModelMax			=	VECTOR_ZERO


//================================================================================================================================================
//		Ints
//================================================================================================================================================
INT i_EventState
INT i_PrintedCompletion
INT i_TrackingFrameWait		=	3
INT i_SendDynamicEmailState = 0
INT i_NumFinishedEmailSubstring = 0


//================================================================================================================================================
//		Floats
//================================================================================================================================================


//================================================================================================================================================
//		Misc. Variables
//================================================================================================================================================
PED_BONETAG bt_ScreenPosTestBoneTag		=	BONETAG_ROOT
PED_BONETAG bt_LosTestBoneTag1			=	BONETAG_ROOT
PED_BONETAG bt_LosTestBoneTag2			=	BONETAG_SPINE3

PED_BONETAG bt_AltScreenPosTestBoneTag	=	BONETAG_ROOT
PED_BONETAG bt_AltLosTestBoneTag1		=	BONETAG_ROOT
PED_BONETAG bt_AltLosTestBoneTag2		=	BONETAG_SPINE1


//================================================================================================================================================
//		Debug Vars
//================================================================================================================================================
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID wg_WidgetGroupID
#ENDIF


//*************************************
//		ENUMS | VARIABLES - END
//*************************************





//===============================================================================================================================================================================
//		Functions/Procedures
//===============================================================================================================================================================================

/// PURPOSE:
///    Cleans up all script variables
PROC CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 1 - Clean Up Started")
	
	//	ENUMS
	e_SendPhotographState = SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN
	e_AnimalActivityFeed = AFA_NONE
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 2 - Enum Variables Cleaned Up")
	
	
	//	CONSTS


	//	STRUCTS


	//	PEDS
	e_FocusAnimal = NULL
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 3 - Ped Variables Cleaned Up")


	//	VEHICLES
	
	
	//	OBJECTS
	
	
	//	BLIPS
	
	
	//	ROPES
	
	
	//	SEQUENCE ID
	
	
	//	CAMERAS
	
	
	//	PTFX
	
	
	//	ENTITIES
	
	
	//	MODELS
	m_TargetAnimalModel = DUMMY_MODEL_FOR_SCRIPT
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 4 - Model Variables Cleaned Up")


	//	STRINGS


	//	BOOLS
	b_InvalidModel = FALSE
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 5 - Bool Variables Cleaned Up")
	

	//	VECTORS
	v_ModelMin = VECTOR_ZERO
	v_ModelMax = VECTOR_ZERO
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 6 - Vector Variables Cleaned Up")


	//	INTS


	//	FLOATS
	//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 7 - Float Variables Cleaned Up")
	
	
	
	
	//	TURN OFF CELLPHONE PICTURE SENDING
	ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE) 
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 7 - Disabling Picture Message Sending")
	
	
	//	CLEAR CONTACT CURRENT STORED PICTURE
	CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_LS_TOURIST_BOARD)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 8 - Clearing Los Santos Tourist Board Stored Picture Message")
	
	
	// REMOVE HELP TEXT 1
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_1")
		CLEAR_THIS_PRINT("PW_HELP_1")
	ENDIF
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 9 - Clearing Help Text 1")
	
	
	// REMOVE HELP TEXT 2
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_2")
		CLEAR_THIS_PRINT("PW_HELP_2")
	ENDIF
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 10 - Clearing Help Text 2")
	
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES() - CHECK 11 - Clean Up Finished")
	
ENDPROC


/// PURPOSE:
///    Prints text representations of all the save data bit values
PROC PRINT_WILDLIFE_PHOTOGRAPHY_ANIMAL_COLLECTION_DATA()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: PRINT_WILDLIFE_PHOTOGRAPHY_ANIMAL_COLLECTION_DATA() - CHECK 1 - Print All Save Data Started")
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	// PRINT TOTAL NUMBER OF COLLECTED PHOTOGRAPHS
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal Photographs Collected: ", g_bCompletedAnimalPhotosCount)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	INT i_Temp
	REPEAT 32 i_Temp
		SWITCH i_Temp
			CASE BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED	// SCRIPT LAUNCHER CONTROLLER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Collection [Not Finished]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Collection [### Finished ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ALL_PHOTOGRAPHS_COLLECTED	// ALL PHOTOGRAPHS
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - All Wildlife Photographs [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - All Wildlife Photographs [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_START_EMAIL_SENT	// INITIAL EMAIL SENT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Start Email [Not Sent]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Start Email [### Sent ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_END_EMAIL_SENT	// FINAL EMAIL SENT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - End Email [Not Sent]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - End Email [### Sent ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_01	// BOAR
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": BOAR [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": BOAR [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_02	// CAT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CAT [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CAT [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_03 // CHICKENHAWK
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CHICKENHAWK [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CHICKENHAWK [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_04 // CORMORANT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CORMORANT [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CORMORANT [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_05 // COW
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": COW [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": COW [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_06 // COYOTE
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": COYOTE [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": COYOTE [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_07 // CROW
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CROW [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": CROW [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_08 // DEER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": DEER [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": DEER [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_09 // DOLPHIN
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": DOLPHIN [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": DOLPHIN [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_10 // FISH
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": FISH [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": FISH [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_11 // HEN
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HEN [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HEN [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_12 // HUMPBACK
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HUMPBACK [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HUMPBACK [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_13 // HUSKY
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HUSKY [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": HUSKY [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_14 // KILLERWHALE
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": KILLERWHALE [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": KILLERWHALE [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_15 // MTLION
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": MTLION [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": MTLION [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_16 // PIG
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PIG [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PIG [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_17 // PIGEON
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PIGEON [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PIGEON [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_18 // POODLE
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": POODLE [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": POODLE [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_19 // PUG
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PUG [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": PUG [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_20 // RABBIT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RABBIT [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RABBIT [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_20 // RAT
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RAT [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RAT [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_21 // RETRIEVER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RETRIEVER [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": RETRIEVER [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_22 // ROTTWEILER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": ROTTWEILER [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": ROTTWEILER [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_23 // SEAGULL
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SEAGULL [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SEAGULL [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_24 // SHARKHAMMER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHARKHAMMER [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHARKHAMMER [### Collected ###]")
				ENDIF
			BREAK
			
			
			CASE BIT_SET_ANIMAL_25 // SHARKTIGER
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHARKTIGER [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHARKTIGER [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_26 // SHEPHERD
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHEPHERD [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": SHEPHERD [### Collected ###]")
				ENDIF
			BREAK
			
			
			/*
			CASE BIT_SET_ANIMAL_27 // STINGRAY
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": STINGRAY [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": STINGRAY [### Collected ###]")
				ENDIF
			BREAK
			*/
			
			
			CASE BIT_SET_ANIMAL_28 // WESTY
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_Temp)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": WESTY [Not Collected]")
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: Saved Data - Animal ", (i_Temp-3), ": WESTY [### Collected ###]")
				ENDIF
			BREAK
			
			
			DEFAULT
				//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: PRINT_WILDLIFE_PHOTOGRAPHY_ANIMAL_COLLECTION_DATA() - CHECK 2 - In Default Case")
			BREAK
		ENDSWITCH
		
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	ENDREPEAT
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: PRINT_WILDLIFE_PHOTOGRAPHY_ANIMAL_COLLECTION_DATA() - CHECK 3 - Print All Save Data Finished")
ENDPROC



/// PURPOSE:
///    Sets the animal bit set for the animal model collected & the animal for the activity feed update
/// PARAMS:
///    m_CheckAnimalModel - MODEL_NAMES: Name of model to be checked
PROC SET_ANIMAL_MODEL_AS_COLLECTED(MODEL_NAMES m_CheckAnimalModel)
	SWITCH m_CheckAnimalModel
		CASE A_C_BOAR
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 1 - Setting BOAR Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_BOAR
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 2 - BOAR Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_CAT_01
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 3 - Setting CAT Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_CAT
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 4 - CAT Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_CHICKENHAWK
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 5 - Setting CHICKENHAWK Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_CHICKEN_HAWK
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 6 - CHICKENHAWK Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_CORMORANT
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 7 - Setting CORMORANT Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_CORMORANT
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 8 - CORMORANT Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_COW
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 9 - Setting COW Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_COW
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 10 - COW Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_COYOTE
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 11 - Setting COYOTE Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_COYOTE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 12 - COYOTE Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_CROW
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 13 - Setting CROW Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_CROW
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 14 - CROW Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_DEER
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 15 - Setting DEER Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_DEER
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 16 - DEER Model Already Collected")
			ENDIF
		BREAK
		
		/*
		CASE A_C_DOLPHIN
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_09)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 17 - Setting DOLPHIN Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_09)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 18 - DOLPHIN Model Already Collected")E
			ENDIF
		BREAK
		
		
		CASE A_C_FISH
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_10)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 19 - Setting FISH Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_10)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 20 - FISH Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_HEN
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 21 - Setting HEN Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_HEN
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 22 - HEN Model Already Collected")
			ENDIF
		BREAK
		
		/*
		CASE A_C_HUMPBACK
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_12)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 23 - Setting HUMPBACK Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_12)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 24 - HUMPBACK Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_HUSKY
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 25 - Setting HUSKY Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_HUSKY
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 26 - HUSKY Model Already Collected")

			ENDIF
		BREAK
		
		/*
		CASE A_C_KILLERWHALE
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_14)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 27 - Setting KILLERWHALE Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_14)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 28 - KILLERWHALE Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_MTLION
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 29 - Setting MTLION Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_MOUNTAIN_LION
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 30 - MTLION Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_PIG
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 31 - Setting PIG Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_PIG
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 32 - PIG Model Already Collected")
			ENDIF
		BREAK
		
		/*
		CASE A_C_PIGEON
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_17)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 33 - Setting PIGEON Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_17)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_PIGEON
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 34 - PIGEON Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_POODLE
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 35 - Setting POODLE Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_POODLE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 36 - POODLE Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_PUG
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 37 - Setting PUG Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_PUG
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 38 - PUG Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_RABBIT_01
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 39 - Setting RABBIT Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_RABBIT
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 40 - RABBIT Model Already Collected")
			ENDIF
		BREAK
		
		
		/*
		CASE A_C_RAT
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 39 - Setting RAT Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 40 - RAT Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_RETRIEVER
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 41 - Setting RETRIEVER Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_RETRIEVER
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 42 - RETRIEVER Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_ROTTWEILER
		CASE A_C_CHOP
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 43 - Setting ROTTWEILER Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_ROTTWEILER
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 44 - ROTTWEILER Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_SEAGULL
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 45 - Setting SEAGULL Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_SEAGULL
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 46 - SEAGULL Model Already Collected")
			ENDIF
		BREAK
		
		/*
		CASE A_C_SHARKHAMMER
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_24)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 47 - Setting SHARKHAMMER Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_24)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 48 - SHARKHAMMER Model Already Collected")
			ENDIF
		BREAK
		
		
		CASE A_C_SHARKTIGER
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_25)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 49 - Setting SHARKTIGER Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_25)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 50 - SHARKTIGER Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_SHEPHERD
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 51 - Setting SHEPHERD Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_BORDER_COLLIE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 52 - SHEPHERD Model Already Collected")
			ENDIF
		BREAK
		
		/*
		CASE A_C_STINGRAY
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_27)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 53 - Setting STINGRAY Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_27)
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 54 - STINGRAY Model Already Collected")
			ENDIF
		BREAK
		*/
		
		CASE A_C_WESTY
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 55 - Setting WESTY Model as Collected")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28)
				
				// ACTIVITY FEED
				e_AnimalActivityFeed = AFA_WEST_HIGHLAND_TERRIER
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 56 - WESTY Model Already Collected")
			ENDIF
		BREAK
		
		
		DEFAULT
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_MODEL_AS_COLLECTED() - CHECK 57 - Invalid Animal Model")
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Get the integer number of animal photographs not collected
/// RETURNS:
///    INT: The number of uncollected animal photographs
FUNC INT GET_ANIMAL_PHOTOS_LEFT_COUNT()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_ANIMAL_PHOTOS_LEFT_COUNT() - CHECK 1 - Returning: Max Animals - Number of Completed Animals Photographs: ", NUM_MAX_ANIMALS - GET_NUMBER_OF_COMPLETED_ANIMAL_PHOTOS())
	
	RETURN NUM_MAX_ANIMALS - GET_NUMBER_OF_COMPLETED_ANIMAL_PHOTOS()
ENDFUNC



/// PURPOSE:
///    Decides the text label to be added to the updated animal list email substrings based on the string ID passed and the animals collection bits
/// PARAMS:
///    i_SubStringID - ID number used to determine which pair of animals label is being checked - i.e. 0 = BOAR/BORDER COLLIE
/// RETURNS:
///    STRING: Returns the appropriate substring label for the updated animal list email
FUNC STRING GET_EMAIL_TEXT_FOR_SUBSTRING_INDEX(INT i_SubStringID)
	SWITCH i_SubStringID
		CASE 0	// SUBSTRING 01		BOAR/BORDER COLLIE(SHEPHERD)
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26))
				RETURN "PW_STRING_1_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26))
				RETURN "PW_STRING_1_2"

			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26))
				RETURN "PW_STRING_1_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26))
				RETURN "PW_STRING_1_4"

			ENDIF
		BREAK
		
		CASE 1	// SUBSTRING 02		CAT/CHICKEN-HAWK
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03))
				RETURN "PW_STRING_2_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03))
				RETURN "PW_STRING_2_2"

			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03))
				RETURN "PW_STRING_2_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03))
				RETURN "PW_STRING_2_4"

			ENDIF
		BREAK
		
		CASE 2	// SUBSTRING 03		CORMORANT/COW
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05))
				RETURN "PW_STRING_3_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05))
				RETURN "PW_STRING_3_2"

			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05))
				RETURN "PW_STRING_3_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05))
				RETURN "PW_STRING_3_4"

			ENDIF
		BREAK
		
		CASE 3	// SUBSTRING 04		COYOTE/CROW
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07))
				RETURN "PW_STRING_4_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07))
				RETURN "PW_STRING_4_2"

			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07))
				RETURN "PW_STRING_4_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07))
				RETURN "PW_STRING_4_4"

			ENDIF
		BREAK
		
		CASE 4	// SUBSTRING 05		DEER/HEN
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11))
				RETURN "PW_STRING_5_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11))
				RETURN "PW_STRING_5_2"

			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11))
				RETURN "PW_STRING_5_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11))
				RETURN "PW_STRING_5_4"

			ENDIF
		BREAK
		
		CASE 5	// SUBSTRING 06		HUSKY/MTLION
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15))
				RETURN "PW_STRING_6_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15))
				RETURN "PW_STRING_6_2"
				
			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15))
				RETURN "PW_STRING_6_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15))
				RETURN "PW_STRING_6_4"

			ENDIF
		BREAK
		
		CASE 6	// SUBSTRING 07		PIG/POODLE
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18))
				RETURN "PW_STRING_7_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18))
				RETURN "PW_STRING_7_2"
				
			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18))
				RETURN "PW_STRING_7_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18))
				RETURN "PW_STRING_7_4"

			ENDIF
		BREAK
		
		CASE 7	// SUBSTRING 08		PUG/RABBIT
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20))
				RETURN "PW_STRING_8_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20))
				RETURN "PW_STRING_8_2"
				
			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20))
				RETURN "PW_STRING_8_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20))
				RETURN "PW_STRING_8_4"

			ENDIF
		BREAK
		
		CASE 8	// SUBSTRING 09		RETRIEVER/ROTTWEILER
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22))
				RETURN "PW_STRING_9_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22))
				RETURN "PW_STRING_9_2"
				
			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22))
				RETURN "PW_STRING_9_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22))
				RETURN "PW_STRING_9_4"

			ENDIF
		BREAK
		
		CASE 9	// SUBSTRING 10		SEAGULL/WESTY
			IF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28))
				RETURN "PW_STRING_10_1"
				
			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
			AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28))
				RETURN "PW_STRING_10_2"
				
			ELIF (NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28))
				RETURN "PW_STRING_10_3"

			ELIF (IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
			AND IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28))
				RETURN "PW_STRING_10_4"

			ENDIF
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Wildlife Photography: GET_EMAIL_TEXT_FOR_SUBSTRING_INDEX() - No substring text selected. Bug ColinC")
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_EMAIL_TEXT_FOR_SUBSTRING_INDEX() - CHECK 1 - No Substring Selected for Substring Index: ", i_SubStringID)
	
	RETURN "PW_STRING_NONE"
	
ENDFUNC



/// PURPOSE:
///    Handles the construction and sending of the dynamic emails after a delay
PROC CONSTRUCT_AND_SEND_DYNAMIC_EMAIL()
	IF b_SendDynamicEmail
		IF GET_TIMER_IN_SECONDS_SAFE(t_SendEmailDelayTimer) > EMAIL_DELAY_TIME
		AND GET_ANIMAL_PHOTOS_LEFT_COUNT() >= 1
		AND NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ALL_PHOTOGRAPHS_COLLECTED)
			SWITCH i_SendDynamicEmailState
				CASE 0
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 1 - Fire Email Number: ", (NUM_MAX_ANIMALS - GET_ANIMAL_PHOTOS_LEFT_COUNT()), " into Dynamic Thread")
					
					// PRIME THE EMAIL WE NEED TO CONSTRUCT DYNAMICALLY
					FIRE_EMAIL_INTO_DYNAMIC_THREAD(DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY, e_MailHelper[NUM_MAX_ANIMALS - GET_ANIMAL_PHOTOS_LEFT_COUNT()].e_MailID, TRUE)
					
					i_NumFinishedEmailSubstring = 0
					
					i_SendDynamicEmailState++
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 2 - Moving to Add Content for Dynamic Thread Substring")
				BREAK
				
				
				CASE 1
					// IF CURRENT SUBSTRING IS LESS THAN MAX SUBSTRING
					IF i_NumFinishedEmailSubstring < NUM_MAX_EMAIL_SUBSTRINGS
						// ADD NEW SUBSTRING CONTENT INTO ~a~ TAG IN EMAIL BODY
						ADD_CONTENT_FOR_DYNAMIC_THREAD_SUBSTRING(DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY, GET_EMAIL_TEXT_FOR_SUBSTRING_INDEX(i_NumFinishedEmailSubstring))
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 3 - Added Substring: ", i_NumFinishedEmailSubstring, " to Dynamic Email Body String")
						
						i_NumFinishedEmailSubstring++
					
					ELSE
						i_SendDynamicEmailState++
					ENDIF
				BREAK
				
				
				CASE 2
					// PUSH THE DYNAMIC EMAIL TO THE FEED
					PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD(DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 4 - Pushed Feedification of Dynamic Thread")
					
					i_SendDynamicEmailState++
				BREAK
				
				
				CASE 3
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 5 - Finished Sending Dynamic Email")
					
					// RESET AND PAUSE THE DELAY TIMER FOR THE NEXT EMAIL
					RESTART_TIMER_AT(t_SendEmailDelayTimer, 0.0)
					PAUSE_TIMER(t_SendEmailDelayTimer)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 6 - Reset Dynamic Email Delay Timer at: 0.0")
					
					b_SendDynamicEmail = FALSE
					i_SendDynamicEmailState = 0
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 7 - Reset Send Dynamic Email Variables")
					
					// TRIGGER AUTOSAVE
					MAKE_AUTOSAVE_REQUEST()
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CONSTRUCT_AND_SEND_DYNAMIC_EMAIL() - CHECK 8 - Request Autosave")
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC



//================================================================================================================================================
//		DEBUG FUNCTIONS/PROCEDURES - START
//================================================================================================================================================
#IF IS_DEBUG_BUILD

	//===================== WIDGET VARIABLES =====================
	
	// MAIN WIDGET VARIABLES
	VECTOR v_DebugMouseClickCoords						= 	<<0.0, 0.0, 0.0>>
	BOOL b_DebugToggleDrawDebugLinesAndSpheres			=	FALSE
	BOOL b_DebugDrawDebugLinesAndSpheres				=	FALSE
	BOOL b_DebugToggleSetProfilingOfThisScript			=	FALSE
	BOOL b_DebugSetProfilingOfThisScript				=	FALSE
	BOOL b_DebugAllWildlifePhotographsCollected			= 	FALSE
	BOOL b_DebugPrintAllWildlifePhotographData			= 	FALSE
	BOOL b_DebugPrintCompletedAnimalPhotosCount			=	FALSE
	BOOL b_DebugClearAllEmails							=	FALSE
	BOOL b_DebugClearAllTextMessages					=	FALSE
	BOOL b_DebugForceCellphoneAway 						=	FALSE
	BOOL b_DebugClearArea		 						=	FALSE
	BOOL b_DebugDrawDebuguSpheresAtBonetagCoords		=	FALSE
	
	
	// SET ANIMAL COLLECTED WIDGET VARIABLES
	TEXT_WIDGET_ID tw_DebugSelectedAnimalToBeCollected
	MODEL_NAMES m_DebugAnimalModelToBeCollected			= 	A_C_BOAR
	TEXT_LABEL str_DebugAnimalToBeCollectedName			= 	"Boar"
	INT i_DebugAnimalToBeCollected						= 	0
	INT i_DebugAnimalToBeCollectedPrevious				= 	0
	BOOL b_DebugSetAnimalCollected						= 	FALSE
	
	
	// SET BIT WIDGET VARIABLES
	TEXT_WIDGET_ID tw_DebugSelectedBitToSet
	STRING str_DebugBitToSetName						= 	"Script Launcher Controller"
	INT i_DebugBitToSet									= 	0
	INT i_DebugBitToSetPrevious							= 	0
	BOOL b_DebugSetBit									= 	FALSE
	BOOL b_DebugSetBitPrevious							= 	FALSE
	
	
	// BLIP NEARBY ANIMALS WIDGET VARIABLES
	CONST_INT NUM_MAX_ANIMAL_ARRAY 							20
	PED_INDEX a_DebugAnimals[NUM_MAX_ANIMAL_ARRAY]
	BLIP_INDEX a_DebugBlips[NUM_MAX_ANIMAL_ARRAY]
	TEXT_WIDGET_ID tw_DebugSelectedAnimalToBlip
	MODEL_NAMES m_DebugAnimalModelToBlip				= 	A_C_BOAR
	TEXT_LABEL str_DebugAnimalToBlipName				= 	"Boar"
	INT i_DebugBlipColour								= 	10
	INT i_DebugAnimalToBlip								= 	0
	INT i_DebugAnimalToBlipPrevious						= 	0
	BOOL b_DebugSetAnimalBlip							= 	FALSE
	BOOL b_DebugRemoveAllBlips							= 	FALSE
	
	
	// CREATE ANIMAL WIDGET VARIABLES
	PED_INDEX e_DebugAnimal
	MODEL_NAMES m_DebugAnimal							= 	A_C_BOAR
	INT i_DebugAnimalModel								= 	0
	INT i_DebugPreviousAnimalModel						= 	0
	BOOL b_DebugUpdateModelAnimal						= 	FALSE
	INT i_DebugUpdateAnimalModelState					= 	0
	FLOAT f_DebugHeadingAnimal							= 	0.0
	BOOL b_DebugFreezeAnimal							= 	FALSE
	BOOL b_DebugBlockingAnimal							= 	FALSE
	BOOL b_DebugCreateAnimal							= 	FALSE
	BOOL b_DebugDeleteAnimal							= 	FALSE
	BOOL b_DebugCreateMultipleAnimals					= 	FALSE
	
	
	// BONE TAG SELECT WIDGET VARIABLES
	INT i_DebugScreenPosTestBoneTag						= 	1		//	BONETAG_ROOT
	INT i_DebugLosTestBoneTag1							= 	1		//	BONETAG_ROOT
	INT i_DebugLosTestBoneTag2							= 	8		//	BONETAG_SPINE3
	BOOL b_DebugSetTestBoneTags							= 	FALSE
	
	INT i_DebugAltScreenPosTestBoneTag					= 	1 		//	BONETAG_ROOT
	INT i_DebugAltLosTestBoneTag1						= 	1		//	BONETAG_ROOT
	INT i_DebugAltLosTestBoneTag2						= 	6		//	BONETAG_SPINE1
	BOOL b_DebugSetAltTestBoneTags						= 	FALSE
	
	
	// SUBMARINE WIDGET VARIABLES
	VEHICLE_INDEX e_DebugSubmarine
	MODEL_NAMES m_DebugSubmarineModel					= 	SUBMERSIBLE2
	VECTOR v_DebugSubmarineWarpPlayerPos				= 	<<3864.31, 4463.54,  1.73>>
	VECTOR v_DebugSubmarineSpawnPos						= 	<<3870.75, 4464.67, -0.78>>
	FLOAT f_DebugSubmarineWarpPlayerHeading				= 	272.5
	FLOAT f_DebugSubmarineSpawnHeading					= 	0.0
	BOOL b_DebugSubmarineWarpPlayer						= 	FALSE
	BOOL b_DebugSpawnSubmarine							= 	FALSE
	BOOL b_DebugRemoveSubmarine							= 	FALSE
	
	
	
	/// PURPOSE:
	///    Creates scripted debug widgets
	PROC CREATE_DEBUG_WIDGETS()
		wg_WidgetGroupID = START_WIDGET_GROUP("Photography Wildlife")
			// SCRIPT DEBUG WIDGET
			START_WIDGET_GROUP("Script Debug Widget")
				ADD_WIDGET_BOOL("Draw Debug Line and Spheres", b_DebugToggleDrawDebugLinesAndSpheres)
				ADD_WIDGET_BOOL("Run Profiling of this Script", b_DebugToggleSetProfilingOfThisScript)
				ADD_WIDGET_BOOL("Set All Wildlife Photographs Collected", b_DebugAllWildlifePhotographsCollected)
				ADD_WIDGET_BOOL("Print All Wildlife Photographs Data", b_DebugPrintAllWildlifePhotographData)
				ADD_WIDGET_BOOL("Print Wildlife Photographs Collected Count", b_DebugPrintCompletedAnimalPhotosCount)
				ADD_WIDGET_BOOL("Delete All Emails", b_DebugClearAllEmails)
				ADD_WIDGET_BOOL("Delete All Text Messages", b_DebugClearAllTextMessages)
				ADD_WIDGET_BOOL("Test Scaleform Screen", b_DisplayScaleform)
				ADD_WIDGET_BOOL("Test Force Cellphone Away", b_DebugForceCellphoneAway)
				ADD_WIDGET_BOOL("Test Clear Area", b_DebugClearArea)
				ADD_WIDGET_BOOL("Test Draw Debug Spheres at Bone Coords", b_DebugDrawDebuguSpheresAtBonetagCoords)
				ADD_WIDGET_INT_SLIDER("Tracking Frames to Wait", i_TrackingFrameWait, 0, 100, 1)
			STOP_WIDGET_GROUP()
			
			
			// SET ANIMAL COLLECTED WIDGET
			START_WIDGET_GROUP("Set Animal Collected Widget")
				tw_DebugSelectedAnimalToBeCollected = ADD_TEXT_WIDGET("Selected Animal")
				SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBeCollected, str_DebugAnimalToBeCollectedName)
				ADD_WIDGET_INT_SLIDER("Animal Index", i_DebugAnimalToBeCollected, 0, 19, 1)
				ADD_WIDGET_BOOL("Set Animal Collected", b_DebugSetAnimalCollected)
			STOP_WIDGET_GROUP()
			
			
			// SET BIT WIDGET
			START_WIDGET_GROUP("Set Bit Widget")
				tw_DebugSelectedBitToSet = ADD_TEXT_WIDGET("Selected Bit Name")
				SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedBitToSet, str_DebugBitToSetName)
				ADD_WIDGET_INT_SLIDER("Bit Index", i_DebugBitToSet, 0, 31, 1)
				ADD_WIDGET_BOOL("Set Animal Bit", b_DebugSetBit)
			STOP_WIDGET_GROUP()
			
			
			// BLIP NEARBY ANIMALS WIDGET
			START_WIDGET_GROUP("Blip Nearby Animals Widget")
				ADD_WIDGET_INT_SLIDER("Blip Colour", i_DebugBlipColour, 0, 100, 1)
				ADD_WIDGET_INT_SLIDER("Animal Index", i_DebugAnimalToBlip, 0, 19, 1)
				tw_DebugSelectedAnimalToBlip = ADD_TEXT_WIDGET("Selected Animal")
				SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				ADD_WIDGET_BOOL("Blip Nearby Animals", b_DebugSetAnimalBlip)
				ADD_WIDGET_BOOL("Remove All Blips", b_DebugRemoveAllBlips)
			STOP_WIDGET_GROUP()
			
			
			// CREATE ANIMAL WIDGET
			START_WIDGET_GROUP("Create Animal Widget")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("Boar")
					ADD_TO_WIDGET_COMBO("Border Collie")
					ADD_TO_WIDGET_COMBO("Cat")
					ADD_TO_WIDGET_COMBO("Chicken-Hawk")
					ADD_TO_WIDGET_COMBO("Chimp")
					ADD_TO_WIDGET_COMBO("Chop")
					ADD_TO_WIDGET_COMBO("Cormorant")
					ADD_TO_WIDGET_COMBO("Cow")
					ADD_TO_WIDGET_COMBO("Coyote")
					ADD_TO_WIDGET_COMBO("Crow")
					ADD_TO_WIDGET_COMBO("Deer")
					ADD_TO_WIDGET_COMBO("Dolphin")
					ADD_TO_WIDGET_COMBO("Fish")
					ADD_TO_WIDGET_COMBO("Hen")
					ADD_TO_WIDGET_COMBO("Humpback")
					ADD_TO_WIDGET_COMBO("Husky")
					ADD_TO_WIDGET_COMBO("Killer Whale")
					ADD_TO_WIDGET_COMBO("Mountain Lion")
					ADD_TO_WIDGET_COMBO("Pig")
					ADD_TO_WIDGET_COMBO("Pigeon")
					ADD_TO_WIDGET_COMBO("Poodle")
					ADD_TO_WIDGET_COMBO("Pug")
					ADD_TO_WIDGET_COMBO("Rabbit")
					ADD_TO_WIDGET_COMBO("Rat")
					ADD_TO_WIDGET_COMBO("Retriever")
					ADD_TO_WIDGET_COMBO("Rhesus")
					ADD_TO_WIDGET_COMBO("Rottweiler")
					ADD_TO_WIDGET_COMBO("Seagull")
					ADD_TO_WIDGET_COMBO("Shark Hammerhead")
					ADD_TO_WIDGET_COMBO("Shark Tiger")
					ADD_TO_WIDGET_COMBO("Stingray")
					ADD_TO_WIDGET_COMBO("West Highland Terrier")
				STOP_WIDGET_COMBO("Animal Model", i_DebugAnimalModel)
				
				ADD_WIDGET_BOOL("Update Model", b_DebugUpdateModelAnimal)
				ADD_WIDGET_BOOL("Create Multiple Animals", b_DebugCreateMultipleAnimals)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_VECTOR_SLIDER("Animal Spawn Coords", v_DebugMouseClickCoords, -10000.0, 10000.0, 0.1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_FLOAT_SLIDER("Animal Heading Value", f_DebugHeadingAnimal, -360.0, 360.0, 0.1)
				ADD_WIDGET_BOOL("Freeze Animal Position", b_DebugFreezeAnimal)
				ADD_WIDGET_BOOL("Block Animal Non-Temporary Events", b_DebugBlockingAnimal)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Create Animal", b_DebugCreateAnimal)
				ADD_WIDGET_BOOL("Delete Animal", b_DebugDeleteAnimal)
			STOP_WIDGET_GROUP()
			
			
			// BONE TAG SELECT WIDGET
			START_WIDGET_GROUP("Bone Tag Select Widget")
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("Screen Position Test Bone Tag", i_DebugScreenPosTestBoneTag)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("LOS Test Bone Tag 1", i_DebugLosTestBoneTag1)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("LOS Test Bone Tag 2", i_DebugLosTestBoneTag2)
				
				ADD_WIDGET_BOOL("Set Selected BoneTags", b_DebugSetTestBoneTags)
				
				// ALTERNATE BONETAGS
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("Alternate Screen Position Test Bone Tag", i_DebugAltScreenPosTestBoneTag)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("ALternate LOS Test Bone Tag 1", i_DebugAltLosTestBoneTag1)
				
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("BONETAG_NULL")
					ADD_TO_WIDGET_COMBO("BONETAG_ROOT")
					ADD_TO_WIDGET_COMBO("BONETAG_HEAD")
					ADD_TO_WIDGET_COMBO("BONETAG_NECK")
					ADD_TO_WIDGET_COMBO("BONETAG_PELVIS")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE1")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE2")
					ADD_TO_WIDGET_COMBO("BONETAG_SPINE3")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CLAVICLE")
					ADD_TO_WIDGET_COMBO("BONETAG_R_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_UPPERARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOREARM")
					ADD_TO_WIDGET_COMBO("BONETAG_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_L_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_PH_R_HAND")
					ADD_TO_WIDGET_COMBO("BONETAG_R_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_L_THIGH")
					ADD_TO_WIDGET_COMBO("BONETAG_R_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_L_CALF")
					ADD_TO_WIDGET_COMBO("BONETAG_R_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_L_FOOT")
					ADD_TO_WIDGET_COMBO("BONETAG_R_TOE")
					ADD_TO_WIDGET_COMBO("BONETAG_L_TOE")
				STOP_WIDGET_COMBO("Alternate LOS Test Bone Tag 2", i_DebugAltLosTestBoneTag2)
				
				ADD_WIDGET_BOOL("Set Alternate Selected BoneTags", b_DebugSetAltTestBoneTags)
			STOP_WIDGET_GROUP()
			
			
			// SUBMARINE WIDGET
			START_WIDGET_GROUP("Submarine Widget")
				ADD_WIDGET_BOOL("Warp Player to Sub Location", b_DebugSubmarineWarpPlayer)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_VECTOR_SLIDER("Sub Spawn Coords", v_DebugSubmarineSpawnPos, -10000.0, 10000.0, 0.1)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_FLOAT_SLIDER("Sub Heading Value", f_DebugSubmarineSpawnHeading, -360.0, 360.0, 0.1)
				ADD_WIDGET_BOOL("Spawn Sub", b_DebugSpawnSubmarine)
				ADD_WIDGET_BOOL("Remove Sub", b_DebugRemoveSubmarine)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates script debug widget
	PROC SCRIPT_DEBUG_WIDGET_UPDATE()
		// GET WORLD COORDS FROM MOUSE CLICK
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
			v_DebugMouseClickCoords = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			
		ELIF IS_MOUSE_BUTTON_JUST_RELEASED(MB_LEFT_BTN)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 1 - Mouse Clicked: ", GET_STRING_FROM_VECTOR(v_DebugMouseClickCoords), " - Distance from Player: ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_DebugMouseClickCoords))
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 2 - Updating Screen Click Coords")
		ENDIF
		
		
		// DRAW DEBUG LINES AND SPHERES
		IF b_DebugToggleDrawDebugLinesAndSpheres
			IF NOT b_DebugDrawDebugLinesAndSpheres
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 3 - Set Debug Lines and Spheres Drawing Active")
				
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				b_DebugDrawDebugLinesAndSpheres = TRUE
			ENDIF
		ELSE
			
			IF b_DebugDrawDebugLinesAndSpheres
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 4 - Deactivate Debug Lines and Spheres Drawing")
				
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				b_DebugDrawDebugLinesAndSpheres = FALSE
			ENDIF
		ENDIF
		
		
		// SCRIPT PROFILING
		IF b_DebugToggleSetProfilingOfThisScript
			IF NOT b_DebugSetProfilingOfThisScript
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 5 - Set Profiling of this Script Active")
				
				SET_PROFILING_OF_THIS_SCRIPT(TRUE)
				b_DebugSetProfilingOfThisScript = TRUE
			ENDIF
		ELSE
			
			IF b_DebugSetProfilingOfThisScript
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 6 - Deactivate Profiling of this Script")
				
				SET_PROFILING_OF_THIS_SCRIPT(FALSE)
				b_DebugSetProfilingOfThisScript = FALSE
			ENDIF
		ENDIF
		
		
		// SET COLLECTED ALL WILDLIFE PHOTOS
		IF b_DebugAllWildlifePhotographsCollected
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 7 - Set All Photographs Collected")
			SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ALL_PHOTOGRAPHS_COLLECTED)
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 8 - Set All Animal Photograph Bits As Collected")
			INT i_Counter
			REPEAT 32 i_Counter
				IF i_Counter > 3
					SET_BIT(g_savedGlobals.sAmbient.iWildlifePhotographsFlags, i_Counter)
				ENDIF
			ENDREPEAT
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 9 - Display Scaleform")
			b_DisplayScaleform = TRUE
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 10 - Starting Email Delay Timer")
			START_TIMER_NOW_SAFE(t_SendEmailDelayTimer)
			
			b_DebugAllWildlifePhotographsCollected = FALSE
		ENDIF
		
		
		// PRINT PHOTOGRAPHS COLLECTED NUMBER & ALL BIT SET DATA
		IF b_DebugPrintAllWildlifePhotographData
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 11 - Print Saved Data")
			PRINT_WILDLIFE_PHOTOGRAPHY_ANIMAL_COLLECTION_DATA()
			
			b_DebugPrintAllWildlifePhotographData = FALSE
		ENDIF
		
		
		// PRINT TOTAL NUMBER OF COLLECTED PHOTOGRAPHS
		IF b_DebugPrintCompletedAnimalPhotosCount
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 12 - Saved Data - Animal Photographs Collected: ", g_bCompletedAnimalPhotosCount)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
			
			b_DebugPrintCompletedAnimalPhotosCount = FALSE
		ENDIF
		
		
		// CLEAR ALL CELLPHONE TEXT MESSAGES BY LABEL
		IF b_DebugClearAllTextMessages
			b_DebugClearAllTextMessages = FALSE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 13 - Deleting All Text Messages")
			CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_TEXT_DATA()
		ENDIF
		
		
		// CLEAR ALL CELLPHONE EMAILS
		IF b_DebugClearAllEmails
			b_DebugClearAllEmails = FALSE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 14 - Deleting All Emails")
			CLEAR_ALL_WILDLIFE_PHOTOGRAPHY_EMAIL_DATA()
		ENDIF
		
		
		// FORCE CELLPHONE AWAY
		IF b_DebugForceCellphoneAway
			g_B_ForcedAway = TRUE
			HANG_UP_AND_PUT_AWAY_PHONE()
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 15 - Test Force Phone Away")
			
			b_DebugForceCellphoneAway = FALSE
		ENDIF
		
		
		// CLEAR AREA AROUND PLAYER
		IF b_DebugClearArea
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT_DEBUG_WIDGET_UPDATE() - CHECK 16 - Clear Area")
				CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, TRUE)
			ENDIF
			
			b_DebugClearArea = FALSE
		ENDIF
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates the Set Animal Collected widget
	PROC SET_ANIMAL_COLLECTED_WIDGET_UPDATE()
		IF i_DebugAnimalToBeCollected != i_DebugAnimalToBeCollectedPrevious
			SWITCH i_DebugAnimalToBeCollected
				CASE 0
					m_DebugAnimalModelToBeCollected = A_C_BOAR
					str_DebugAnimalToBeCollectedName = "Boar"
				BREAK
						
				CASE 1
					m_DebugAnimalModelToBeCollected = A_C_SHEPHERD
					str_DebugAnimalToBeCollectedName = "Border Collie"
				BREAK
				
				CASE 2
					m_DebugAnimalModelToBeCollected = A_C_CAT_01
					str_DebugAnimalToBeCollectedName = "Cat"
				BREAK
				
				CASE 3
					m_DebugAnimalModelToBeCollected = A_C_CHICKENHAWK
					str_DebugAnimalToBeCollectedName = "Chicken-hawk"
				BREAK
				
				CASE 4
					m_DebugAnimalModelToBeCollected = A_C_CORMORANT
					str_DebugAnimalToBeCollectedName = "Cormorant"
				BREAK
				
				CASE 5
					m_DebugAnimalModelToBeCollected = A_C_COW
					str_DebugAnimalToBeCollectedName = "Cow"
				BREAK
				
				CASE 6
					m_DebugAnimalModelToBeCollected = A_C_COYOTE
					str_DebugAnimalToBeCollectedName = "Coyote"
				BREAK
				
				CASE 7
					m_DebugAnimalModelToBeCollected = A_C_CROW
					str_DebugAnimalToBeCollectedName = "Crow"
				BREAK
				
				CASE 8
					m_DebugAnimalModelToBeCollected = A_C_DEER
					str_DebugAnimalToBeCollectedName = "Deer"
				BREAK
				
				CASE 9
					m_DebugAnimalModelToBeCollected = A_C_HEN
					str_DebugAnimalToBeCollectedName = "Hen"
				BREAK
				
				CASE 10
					m_DebugAnimalModelToBeCollected = A_C_HUSKY
					str_DebugAnimalToBeCollectedName = "Husky"
				BREAK
				
				CASE 11
					m_DebugAnimalModelToBeCollected = A_C_MTLION
					str_DebugAnimalToBeCollectedName = "Mountain Lion"
				BREAK
				
				CASE 12
					m_DebugAnimalModelToBeCollected = A_C_PIG
					str_DebugAnimalToBeCollectedName = "Pig"
				BREAK
				
				/*
				CASE 13
					m_DebugAnimalModelToBeCollected = A_C_PIGEON
					str_DebugAnimalToBeCollectedName = "Pigeon"
				BREAK
				*/
				
				CASE 13
					m_DebugAnimalModelToBeCollected = A_C_POODLE
					str_DebugAnimalToBeCollectedName = "Poodle"
				BREAK
				
				CASE 14
					m_DebugAnimalModelToBeCollected = A_C_PUG
					str_DebugAnimalToBeCollectedName = "Pug"
				BREAK
				
				CASE 15
					m_DebugAnimalModelToBeCollected = A_C_RABBIT_01
					str_DebugAnimalToBeCollectedName = "Rabbit"
				BREAK
				
				CASE 16
					m_DebugAnimalModelToBeCollected = A_C_RETRIEVER
					str_DebugAnimalToBeCollectedName = "Retriever"
				BREAK
				
				CASE 17
					m_DebugAnimalModelToBeCollected = A_C_ROTTWEILER
					str_DebugAnimalToBeCollectedName = "Rottweiler"
				BREAK
				
				CASE 18
					m_DebugAnimalModelToBeCollected = A_C_SEAGULL
					str_DebugAnimalToBeCollectedName = "Seagull"
				BREAK
				
				CASE 19
					m_DebugAnimalModelToBeCollected = A_C_WESTY
					str_DebugAnimalToBeCollectedName = "Westy"
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
			// UPDATE TEXT BOX NAME
			SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBeCollected, str_DebugAnimalToBeCollectedName)
					
			i_DebugAnimalToBeCollectedPrevious = i_DebugAnimalToBeCollected
		ENDIF
		
		
		IF b_DebugSetAnimalCollected
			SET_ANIMAL_MODEL_AS_COLLECTED(m_DebugAnimalModelToBeCollected)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_COLLECTED_WIDGET_UPDATE() - CHECK 1 - Setting Animal Collected: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimalModelToBeCollected))
			
			// SEND UPDATED ANIMAL LIST EMAIL
			IF GET_ANIMAL_PHOTOS_LEFT_COUNT() >= 1 // DON'T SEND UPDATE LIST EMAIL FOR LAST ANIMAL
				START_TIMER_NOW_SAFE(t_SendEmailDelayTimer)
				b_SendDynamicEmail = TRUE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_COLLECTED_WIDGET_UPDATE() - CHECK 2 - Sending Updated Photography List Email")
			ENDIF
			
			b_DisplayScaleform = TRUE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_ANIMAL_COLLECTED_WIDGET_UPDATE() - CHECK 3 - Set b_DisplayScaleform:  ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_DisplayScaleform))
			
			b_DebugSetAnimalCollected = FALSE
		ENDIF
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates the Set Wildlife Photography Bit widget
	PROC SET_BIT_WIDGET_UPDATE()
		// BIT SELECTOR
		IF i_DebugBitToSet != i_DebugBitToSetPrevious
			SWITCH i_DebugBitToSet
				CASE 0
					str_DebugBitToSetName = "Script Launcher Controller"
				BREAK
				
				CASE 1
					str_DebugBitToSetName = "Collected All Photographs"
				BREAK
				
				CASE 2
					str_DebugBitToSetName = "Start Email"
				BREAK
				
				CASE 3
					str_DebugBitToSetName = "End Email"
				BREAK
				
				CASE 4
					str_DebugBitToSetName = "Boar"
				BREAK
				
				CASE 5
					str_DebugBitToSetName = "Cat"
				BREAK
				
				CASE 6
					str_DebugBitToSetName = "Chicken-hawk"
				BREAK
				
				CASE 7
					str_DebugBitToSetName = "Cormorant"
				BREAK
				
				CASE 8
					str_DebugBitToSetName = "Cow"
				BREAK
				
				CASE 9
					str_DebugBitToSetName = "Coyote"
				BREAK
				
				CASE 10
					str_DebugBitToSetName = "Crow"
				BREAK
				
				CASE 11
					str_DebugBitToSetName = "Deer"
				BREAK
				
				CASE 12
					str_DebugBitToSetName = "Dolphin"
				BREAK
				
				CASE 13
					str_DebugBitToSetName = "Fish"
				BREAK
				
				CASE 14
					str_DebugBitToSetName = "Hen"
				BREAK
				
				CASE 15
					str_DebugBitToSetName = "Humpback"
				BREAK
				
				CASE 16
					str_DebugBitToSetName = "Husky"
				BREAK
				
				CASE 17
					str_DebugBitToSetName = "Killer Whale"
				BREAK
				
				CASE 18
					str_DebugBitToSetName = "Mountain Lion"
				BREAK
				
				CASE 19
					str_DebugBitToSetName = "Pig"
				BREAK
				
				CASE 20
					str_DebugBitToSetName = "Pigeon"
				BREAK
				
				CASE 21
					str_DebugBitToSetName = "Poodle"
				BREAK
				
				CASE 22
					str_DebugBitToSetName = "Pug"
				BREAK
				
				CASE 23
					str_DebugBitToSetName = "Rabbit"
				BREAK
				
				CASE 24
					str_DebugBitToSetName = "Retriever"
				BREAK
				
				CASE 25
					str_DebugBitToSetName = "Rottweiler"
				BREAK
				
				CASE 26
					str_DebugBitToSetName = "Seagull"
				BREAK
				
				CASE 27
					str_DebugBitToSetName = "Shark Hammer"
				BREAK
				
				CASE 28
					str_DebugBitToSetName = "Shark Tiger"
				BREAK
						
				CASE 29
					str_DebugBitToSetName = "Shepherd/Border Collie"
				BREAK
				
				CASE 30
					str_DebugBitToSetName = "Stingray"
				BREAK
				
				CASE 31
					str_DebugBitToSetName = "Westy"
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
			
			// UPDATE ANIMAL NAME IN TEXT BOX
			SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedBitToSet, str_DebugBitToSetName)
			
			
			// SET BOOL TICKED OR UNTICKED
			IF IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_DebugBitToSet)
				b_DebugSetBit = TRUE
				b_DebugSetBitPrevious = TRUE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_BIT_WIDGET_UPDATE() - CHECK 1 - Bit Index: ", i_DebugBitToSet, " - ", str_DebugBitToSetName, ": Bit is Set")
			ELSE
				b_DebugSetBit = FALSE
				b_DebugSetBitPrevious = FALSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_BIT_WIDGET_UPDATE() - CHECK 2 - Bit Index: ", i_DebugBitToSet, " - ", str_DebugBitToSetName, ": Bit is Not Set")
			ENDIF
			
			i_DebugBitToSetPrevious = i_DebugBitToSet
		ENDIF
		
		// UPDATE BIT FROM RAG TOGGLE
		IF NOT b_DebugSetBit = b_DebugSetBitPrevious
			IF b_DebugSetBit
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_BIT_WIDGET_UPDATE() - CHECK 3 - Set: ", str_DebugBitToSetName, " Bit")
				SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_DebugBitToSet)
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SET_BIT_WIDGET_UPDATE() - CHECK 4 - Clear: ", str_DebugBitToSetName, " Bit")
				CLEAR_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, i_DebugBitToSet)
			ENDIF
			
			b_DebugSetBitPrevious = b_DebugSetBit
		ENDIF
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Creates & updates blips for nearby animals - widget
	PROC BLIP_NEARBY_ANIMALS_WIDGET_UPDATE()
		// ANIMAL SELECTOR
		IF i_DebugAnimalToBlip != i_DebugAnimalToBlipPrevious
			SWITCH i_DebugAnimalToBlip
				CASE 0
					m_DebugAnimalModelToBlip = A_C_BOAR
					str_DebugAnimalToBlipName = "Boar"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 1
					m_DebugAnimalModelToBlip = A_C_SHEPHERD
					str_DebugAnimalToBlipName = "Border Collie"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 2
					m_DebugAnimalModelToBlip = A_C_CAT_01
					str_DebugAnimalToBlipName = "Cat"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 3
					m_DebugAnimalModelToBlip = A_C_CHICKENHAWK
					str_DebugAnimalToBlipName = "Chicken-hawk"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 4
					m_DebugAnimalModelToBlip = A_C_CORMORANT
					str_DebugAnimalToBlipName = "Cormorant"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 5
					m_DebugAnimalModelToBlip = A_C_COW
					str_DebugAnimalToBlipName = "Cow"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 6
					m_DebugAnimalModelToBlip = A_C_COYOTE
					str_DebugAnimalToBlipName = "Coyote"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 7
					m_DebugAnimalModelToBlip = A_C_CROW
					str_DebugAnimalToBlipName = "Crow"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 8
					m_DebugAnimalModelToBlip = A_C_DEER
					str_DebugAnimalToBlipName = "Deer"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 9
					m_DebugAnimalModelToBlip = A_C_HEN
					str_DebugAnimalToBlipName = "Hen"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 10
					m_DebugAnimalModelToBlip = A_C_HUSKY
					str_DebugAnimalToBlipName = "Husky"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 11
					m_DebugAnimalModelToBlip = A_C_MTLION
					str_DebugAnimalToBlipName = "Mountain Lion"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 12
					m_DebugAnimalModelToBlip = A_C_PIG
					str_DebugAnimalToBlipName = "Pig"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				/*
				CASE 13
					m_DebugAnimalModelToBlip = A_C_PIGEON
					str_DebugAnimalToBlipName = "Pigeon"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				*/
				
				CASE 13
					m_DebugAnimalModelToBlip = A_C_POODLE
					str_DebugAnimalToBlipName = "Poodle"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 14
					m_DebugAnimalModelToBlip = A_C_PUG
					str_DebugAnimalToBlipName = "Pug"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 15
					m_DebugAnimalModelToBlip = A_C_RABBIT_01
					str_DebugAnimalToBlipName = "Rabbit"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 16
					m_DebugAnimalModelToBlip = A_C_RETRIEVER
					str_DebugAnimalToBlipName = "Retriever"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 17
					m_DebugAnimalModelToBlip = A_C_ROTTWEILER
					str_DebugAnimalToBlipName = "Rottweiler"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 18
					m_DebugAnimalModelToBlip = A_C_SEAGULL
					str_DebugAnimalToBlipName = "Seagull"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				CASE 19
					m_DebugAnimalModelToBlip = A_C_WESTY
					str_DebugAnimalToBlipName = "Westy"
					SET_CONTENTS_OF_TEXT_WIDGET(tw_DebugSelectedAnimalToBlip, str_DebugAnimalToBlipName)
				BREAK
				
				DEFAULT
				BREAK
			ENDSWITCH
			
			i_DebugAnimalToBlipPrevious = i_DebugAnimalToBlip
		ENDIF
		
		
		// BLIP NEARBY ANIMALS
		IF b_DebugSetAnimalBlip
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BLIP_NEARBY_ANIMALS_WIDGET_UPDATE() - CHECK 1 - Getting All Nearby: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimalModelToBlip))
			GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), a_DebugAnimals, PEDTYPE_ARMY | PEDTYPE_BUM | PEDTYPE_CIVFEMALE | PEDTYPE_CIVMALE | PEDTYPE_COP | PEDTYPE_CRIMINAL | PEDTYPE_DEALER
					 | PEDTYPE_FIRE | PEDTYPE_GANG_CHINESE_JAPANESE | PEDTYPE_GANG_PUERTO_RICAN | PEDTYPE_GANG1 | PEDTYPE_GANG10 | PEDTYPE_GANG2 | PEDTYPE_GANG3 | PEDTYPE_GANG4
					  | PEDTYPE_GANG5 | PEDTYPE_GANG6 | PEDTYPE_GANG7 | PEDTYPE_GANG8 | PEDTYPE_GANG9 | PEDTYPE_INVALID | PEDTYPE_MEDIC | PEDTYPE_PLAYER_NETWORK
					   | PEDTYPE_PLAYER_UNUSED | PEDTYPE_PLAYER1 | PEDTYPE_PLAYER2 | PEDTYPE_PROSTITUTE | PEDTYPE_SPECIAL | PEDTYPE_SWAT)
			
			INT i_Counter
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BLIP_NEARBY_ANIMALS_WIDGET_UPDATE() - CHECK 2 - Removing Old Blips")
			FOR i_Counter = 0 TO (NUM_MAX_ANIMAL_ARRAY - 1)
				IF DOES_BLIP_EXIST(a_DebugBlips[i_Counter])
					REMOVE_BLIP(a_DebugBlips[i_Counter])
				ENDIF
			ENDFOR
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BLIP_NEARBY_ANIMALS_WIDGET_UPDATE() - CHECK 3 - Blipping All Nearby: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimalModelToBlip))
			FOR i_Counter = 0 TO (NUM_MAX_ANIMAL_ARRAY - 1)
				IF IS_ENTITY_ALIVE(a_DebugAnimals[i_Counter])
					IF GET_ENTITY_MODEL(a_DebugAnimals[i_Counter]) = m_DebugAnimalModelToBlip
						a_DebugBlips[i_Counter] = CREATE_BLIP_FOR_PED(a_DebugAnimals[i_Counter])
						SET_BLIP_COLOUR(a_DebugBlips[i_Counter], i_DebugBlipColour)
					ENDIF
				ENDIF
			ENDFOR

			b_DebugSetAnimalBlip = FALSE
		ENDIF
		
		
		// REMOVE ALL ANIMAL BLIPS
		IF b_DebugRemoveAllBlips
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BLIP_NEARBY_ANIMALS_WIDGET_UPDATE() - CHECK 4 - Removing All Blips")
			
			INT i_Counter
			
			FOR i_Counter = 0 TO (NUM_MAX_ANIMAL_ARRAY - 1)
				IF DOES_BLIP_EXIST(a_DebugBlips[i_Counter])
					REMOVE_BLIP(a_DebugBlips[i_Counter])
				ENDIF
			ENDFOR

			b_DebugRemoveAllBlips = FALSE
		ENDIF
		
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates Create Animal widget
	PROC CREATE_ANIMAL_WIDGET_UPDATE()
		// UPDATE MODEL
		IF b_DebugUpdateModelAnimal
		OR i_DebugPreviousAnimalModel != i_DebugAnimalModel
			SWITCH i_DebugUpdateAnimalModelState
				CASE 0
					SWITCH i_DebugAnimalModel
						CASE 0
							m_DebugAnimal = A_C_BOAR
						BREAK
						
						CASE 1
							m_DebugAnimal = A_C_SHEPHERD
						BREAK
						
						CASE 2
							m_DebugAnimal = A_C_CAT_01
						BREAK
						
						CASE 3
							m_DebugAnimal = A_C_CHICKENHAWK
						BREAK
						
						CASE 4
							m_DebugAnimal = A_C_CHIMP
						BREAK
						
						CASE 5
							m_DebugAnimal = A_C_CHOP
						BREAK
						
						CASE 6
							m_DebugAnimal = A_C_CORMORANT
						BREAK
						
						CASE 7
							m_DebugAnimal = A_C_COW
						BREAK
						
						CASE 8
							m_DebugAnimal = A_C_COYOTE
						BREAK
						
						CASE 9
							m_DebugAnimal = A_C_CROW
						BREAK
						
						CASE 10
							m_DebugAnimal = A_C_DEER
						BREAK
						
						CASE 11
							m_DebugAnimal = A_C_DOLPHIN
						BREAK
						
						CASE 12
							m_DebugAnimal = A_C_FISH
						BREAK
						
						CASE 13
							m_DebugAnimal = A_C_HEN
						BREAK
						
						CASE 14
							m_DebugAnimal = A_C_HUMPBACK
						BREAK
						
						CASE 15
							m_DebugAnimal = A_C_HUSKY
						BREAK
						
						CASE 16
							m_DebugAnimal = A_C_KILLERWHALE
						BREAK
						
						CASE 17
							m_DebugAnimal = A_C_MTLION
						BREAK
						
						CASE 18
							m_DebugAnimal = A_C_PIG
						BREAK
						
						CASE 19
							m_DebugAnimal = A_C_PIGEON
						BREAK
						
						CASE 20
							m_DebugAnimal = A_C_POODLE
						BREAK
						
						CASE 21
							m_DebugAnimal = A_C_PUG
						BREAK
						
						CASE 22
							m_DebugAnimal = A_C_RABBIT_01
						BREAK
						
						CASE 23
							m_DebugAnimal = A_C_RAT
						BREAK
						
						CASE 24
							m_DebugAnimal = A_C_RETRIEVER
						BREAK
						
						CASE 25
							m_DebugAnimal = A_C_RHESUS
						BREAK
						
						CASE 26
							m_DebugAnimal = A_C_ROTTWEILER
						BREAK
						
						CASE 27
							m_DebugAnimal = A_C_SEAGULL
						BREAK
						
						CASE 28
							m_DebugAnimal = A_C_SHARKHAMMER
						BREAK
						
						CASE 29
							m_DebugAnimal = A_C_SHARKTIGER
						BREAK
						
						CASE 30
							m_DebugAnimal = A_C_STINGRAY
						BREAK
						
						CASE 31
							m_DebugAnimal = A_C_WESTY
						BREAK
						
					ENDSWITCH
						
					i_DebugUpdateAnimalModelState++
					
				BREAK
				
				CASE 1		
					REQUEST_MODEL(m_DebugAnimal)
					WHILE NOT HAS_MODEL_LOADED(m_DebugAnimal)
						WAIT(0)
					ENDWHILE
					
					IF IS_ENTITY_ALIVE(e_DebugAnimal)
						DELETE_PED(e_DebugAnimal)
					ENDIF
					
					i_DebugUpdateAnimalModelState++
				BREAK
				
				CASE 2
					IF IS_VECTOR_ZERO(v_DebugMouseClickCoords)
						v_DebugMouseClickCoords = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
					ENDIF
					
					IF NOT b_DebugCreateMultipleAnimals
						e_DebugAnimal = CREATE_PED(PEDTYPE_ANIMAL, m_DebugAnimal, v_DebugMouseClickCoords, f_DebugHeadingAnimal)
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CREATE_ANIMAL_WIDGET_UPDATE() - CHECK 1 - Set Animal Model: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimal))
						
						IF IS_ENTITY_ALIVE(e_DebugAnimal)
						AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
							f_DebugHeadingAnimal = GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(e_DebugAnimal), GET_ENTITY_COORDS(PLAYER_PED_ID()))// - GET_ENTITY_HEADING(a_Vehicles[i_Counter].veh)
							SET_ENTITY_HEADING(e_DebugAnimal, f_DebugHeadingAnimal)
							
							FREEZE_ENTITY_POSITION(e_DebugAnimal, b_DebugFreezeAnimal)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(e_DebugAnimal, b_DebugBlockingAnimal)
						ENDIF
					ELSE
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CREATE_ANIMAL_WIDGET_UPDATE() - CHECK 2 - Create Multi Animal Model: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimal))
						CREATE_PED(PEDTYPE_ANIMAL, m_DebugAnimal, v_DebugMouseClickCoords, f_DebugHeadingAnimal)
						
					ENDIF
					
					b_DebugUpdateModelAnimal = FALSE
					
					i_DebugPreviousAnimalModel = i_DebugAnimalModel
					
					i_DebugUpdateAnimalModelState = 0
				BREAK
			ENDSWITCH
		ENDIF
		
	
		// CREATE ANIMAL
		IF b_DebugCreateAnimal
			IF NOT DOES_ENTITY_EXIST(e_DebugAnimal)
				REQUEST_MODEL(m_DebugAnimal)
				WHILE NOT HAS_MODEL_LOADED(m_DebugAnimal)
					WAIT(0)
				ENDWHILE
				
				IF IS_VECTOR_ZERO(v_DebugMouseClickCoords)
					v_DebugMouseClickCoords = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				ENDIF
				
				e_DebugAnimal = CREATE_PED(PEDTYPE_ANIMAL, m_DebugAnimal, v_DebugMouseClickCoords, f_DebugHeadingAnimal)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CREATE_ANIMAL_WIDGET_UPDATE() - CHECK 2 - Created Animal Model: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimal))
				
				FREEZE_ENTITY_POSITION(e_DebugAnimal, TRUE)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(e_DebugAnimal, TRUE)
			ENDIF
			
			b_DebugCreateAnimal = FALSE
		ENDIF
		
		
		// DELETE ANIMAL
		IF b_DebugDeleteAnimal
			IF DOES_ENTITY_EXIST(e_DebugAnimal)
				DELETE_PED(e_DebugAnimal)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CREATE_ANIMAL_WIDGET_UPDATE() - CHECK 3 - Deleted Animal Model: ", GET_MODEL_NAME_FOR_DEBUG(m_DebugAnimal))
			ENDIF
			
			b_DebugDeleteAnimal = FALSE
		ENDIF
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates the Bonetag Selection widget
	PROC BONETAG_SELECT_WIDGET_UPDATE()
		IF b_DebugSetTestBoneTags
			// SET SCREEN POSITION TEST BONE TAG
			SWITCH i_DebugScreenPosTestBoneTag
				CASE 0
					bt_ScreenPosTestBoneTag = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_ScreenPosTestBoneTag = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_ScreenPosTestBoneTag = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_ScreenPosTestBoneTag = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_ScreenPosTestBoneTag = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_ScreenPosTestBoneTag = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_ScreenPosTestBoneTag = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_ScreenPosTestBoneTag = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_ScreenPosTestBoneTag = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_ScreenPosTestBoneTag = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_ScreenPosTestBoneTag = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_ScreenPosTestBoneTag = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_ScreenPosTestBoneTag = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_ScreenPosTestBoneTag = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_ScreenPosTestBoneTag = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_ScreenPosTestBoneTag = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_ScreenPosTestBoneTag = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_ScreenPosTestBoneTag = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_ScreenPosTestBoneTag = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_ScreenPosTestBoneTag = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_ScreenPosTestBoneTag = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_ScreenPosTestBoneTag = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_ScreenPosTestBoneTag = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_ScreenPosTestBoneTag = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_ScreenPosTestBoneTag = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_ScreenPosTestBoneTag = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_ScreenPosTestBoneTag = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
		
		
			// SET LINE OF SIGHT TEST BONE TAG 1
			SWITCH i_DebugLosTestBoneTag1
				CASE 0
					bt_LosTestBoneTag1 = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_LosTestBoneTag1 = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_LosTestBoneTag1 = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_LosTestBoneTag1 = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_LosTestBoneTag1 = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_LosTestBoneTag1 = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_LosTestBoneTag1 = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_LosTestBoneTag1 = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_LosTestBoneTag1 = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_LosTestBoneTag1 = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_LosTestBoneTag1 = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_LosTestBoneTag1 = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_LosTestBoneTag1 = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_LosTestBoneTag1 = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_LosTestBoneTag1 = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_LosTestBoneTag1 = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_LosTestBoneTag1 = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_LosTestBoneTag1 = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_LosTestBoneTag1 = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_LosTestBoneTag1 = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_LosTestBoneTag1 = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_LosTestBoneTag1 = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_LosTestBoneTag1 = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_LosTestBoneTag1 = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_LosTestBoneTag1 = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_LosTestBoneTag1 = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_LosTestBoneTag1 = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
		
		
			// SET LINE OF SIGHT TEST BONE TAG 2
			SWITCH i_DebugLosTestBoneTag2
				CASE 0
					bt_LosTestBoneTag2 = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_LosTestBoneTag2 = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_LosTestBoneTag2 = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_LosTestBoneTag2 = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_LosTestBoneTag2 = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_LosTestBoneTag2 = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_LosTestBoneTag2 = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_LosTestBoneTag2 = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_LosTestBoneTag2 = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_LosTestBoneTag2 = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_LosTestBoneTag2 = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_LosTestBoneTag2 = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_LosTestBoneTag2 = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_LosTestBoneTag2 = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_LosTestBoneTag2 = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_LosTestBoneTag2 = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_LosTestBoneTag2 = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_LosTestBoneTag2 = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_LosTestBoneTag2 = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_LosTestBoneTag2 = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_LosTestBoneTag2 = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_LosTestBoneTag2 = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_LosTestBoneTag2 = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_LosTestBoneTag2 = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_LosTestBoneTag2 = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_LosTestBoneTag2 = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_LosTestBoneTag2 = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 1 - Set Screen Position Test Bone Tag: ", GET_BONETAG_NAME_FOR_DEBUG(bt_ScreenPosTestBoneTag))
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 2 - Set Line of Sight Test Bone Tag 1: ", GET_BONETAG_NAME_FOR_DEBUG(bt_LosTestBoneTag1))
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 3 - Set Line of Sight Test Bone Tag 2: ", GET_BONETAG_NAME_FOR_DEBUG(bt_LosTestBoneTag2))
			
			b_DebugSetTestBoneTags = FALSE
		ENDIF
		
		
		IF b_DebugSetAltTestBoneTags
			
			
			// SET ALTERNATE SCREEN POSITION TEST BONE TAG
			SWITCH i_DebugAltScreenPosTestBoneTag
				CASE 0
					bt_AltScreenPosTestBoneTag = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_AltScreenPosTestBoneTag = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_AltScreenPosTestBoneTag = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_AltScreenPosTestBoneTag = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_AltScreenPosTestBoneTag = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_AltScreenPosTestBoneTag = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_AltScreenPosTestBoneTag = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_AltScreenPosTestBoneTag = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_AltScreenPosTestBoneTag = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_AltScreenPosTestBoneTag = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_AltScreenPosTestBoneTag = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_AltScreenPosTestBoneTag = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_AltScreenPosTestBoneTag = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_AltScreenPosTestBoneTag = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_AltScreenPosTestBoneTag = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_AltScreenPosTestBoneTag = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_AltScreenPosTestBoneTag = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_AltScreenPosTestBoneTag = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_AltScreenPosTestBoneTag = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_AltScreenPosTestBoneTag = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_AltScreenPosTestBoneTag = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_AltScreenPosTestBoneTag = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_AltScreenPosTestBoneTag = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_AltScreenPosTestBoneTag = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_AltScreenPosTestBoneTag = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_AltScreenPosTestBoneTag = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_AltScreenPosTestBoneTag = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
		
		
			// SET ALTERNATE LINE OF SIGHT TEST BONE TAG 1
			SWITCH i_DebugAltLosTestBoneTag1
				CASE 0
					bt_AltLosTestBoneTag1 = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_AltLosTestBoneTag1 = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_AltLosTestBoneTag1 = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_AltLosTestBoneTag1 = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_AltLosTestBoneTag1 = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_AltLosTestBoneTag1 = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_AltLosTestBoneTag1 = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_AltLosTestBoneTag1 = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_AltLosTestBoneTag1 = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_AltLosTestBoneTag1 = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_AltLosTestBoneTag1 = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_AltLosTestBoneTag1 = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_AltLosTestBoneTag1 = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_AltLosTestBoneTag1 = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_AltLosTestBoneTag1 = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_AltLosTestBoneTag1 = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_AltLosTestBoneTag1 = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_AltLosTestBoneTag1 = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_AltLosTestBoneTag1 = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_AltLosTestBoneTag1 = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_AltLosTestBoneTag1 = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_AltLosTestBoneTag1 = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_AltLosTestBoneTag1 = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_AltLosTestBoneTag1 = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_AltLosTestBoneTag1 = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_AltLosTestBoneTag1 = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_AltLosTestBoneTag1 = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
		
		
			// SET ALTERNATE LINE OF SIGHT TEST BONE TAG 2
			SWITCH i_DebugAltLosTestBoneTag2
				CASE 0
					bt_AltLosTestBoneTag2 = BONETAG_NULL
				BREAK
				
				CASE 1
					bt_AltLosTestBoneTag2 = BONETAG_ROOT
				BREAK
				
				CASE 2
					bt_AltLosTestBoneTag2 = BONETAG_HEAD
				BREAK
				
				CASE 3
					bt_AltLosTestBoneTag2 = BONETAG_NECK
				BREAK
				
				CASE 4
					bt_AltLosTestBoneTag2 = BONETAG_PELVIS
				BREAK
				
				CASE 5
					bt_AltLosTestBoneTag2 = BONETAG_SPINE
				BREAK
				
				CASE 6
					bt_AltLosTestBoneTag2 = BONETAG_SPINE1
				BREAK
				
				CASE 7
					bt_AltLosTestBoneTag2 = BONETAG_SPINE2
				BREAK
				
				CASE 8
					bt_AltLosTestBoneTag2 = BONETAG_SPINE3
				BREAK
				
				CASE 9
					bt_AltLosTestBoneTag2 = BONETAG_R_CLAVICLE
				BREAK
				
				CASE 10
					bt_AltLosTestBoneTag2 = BONETAG_L_CLAVICLE
				BREAK
				
				CASE 11
					bt_AltLosTestBoneTag2 = BONETAG_R_UPPERARM
				BREAK
				
				CASE 12
					bt_AltLosTestBoneTag2 = BONETAG_L_UPPERARM
				BREAK
				
				CASE 13
					bt_AltLosTestBoneTag2 = BONETAG_R_FOREARM
				BREAK
				
				CASE 14
					bt_AltLosTestBoneTag2 = BONETAG_L_FOREARM
				BREAK
				
				CASE 15
					bt_AltLosTestBoneTag2 = BONETAG_R_HAND
				BREAK
				
				CASE 16
					bt_AltLosTestBoneTag2 = BONETAG_L_HAND
				BREAK
				
				CASE 17
					bt_AltLosTestBoneTag2 = BONETAG_PH_L_HAND
				BREAK
				
				CASE 18
					bt_AltLosTestBoneTag2 = BONETAG_PH_R_HAND
				BREAK
				
				CASE 19
					bt_AltLosTestBoneTag2 = BONETAG_R_THIGH
				BREAK
				
				CASE 20
					bt_AltLosTestBoneTag2 = BONETAG_L_THIGH
				BREAK
				
				CASE 21
					bt_AltLosTestBoneTag2 = BONETAG_R_CALF
				BREAK
				
				CASE 22
					bt_AltLosTestBoneTag2 = BONETAG_L_CALF
				BREAK
				
				CASE 23
					bt_AltLosTestBoneTag2 = BONETAG_R_FOOT
				BREAK
				
				CASE 24
					bt_AltLosTestBoneTag2 = BONETAG_L_FOOT
				BREAK
				
				CASE 25
					bt_AltLosTestBoneTag2 = BONETAG_R_TOE
				BREAK
				
				CASE 26
					bt_AltLosTestBoneTag2 = BONETAG_L_TOE
				BREAK
				
			ENDSWITCH
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 4 - Set Alternate Screen Position Test Bone Tag: ", GET_BONETAG_NAME_FOR_DEBUG(bt_AltScreenPosTestBoneTag))
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 5 - Set Alternate Line of Sight Test Bone Tag 1: ", GET_BONETAG_NAME_FOR_DEBUG(bt_AltLosTestBoneTag1))
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: BONETAG_SELECT_WIDGET_UPDATE() - CHECK 6 - Set Alternate Line of Sight Test Bone Tag 2: ", GET_BONETAG_NAME_FOR_DEBUG(bt_AltLosTestBoneTag2))
			
			b_DebugSetAltTestBoneTags = FALSE
		ENDIF
		
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates the Submarine widget
	PROC SUBMARINE_WIDGET_UPDATE()
		// WAPR PLAYER TO SUBMARINE SPAWN COORDS
		IF b_DebugSubmarineWarpPlayer
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SUBMARINE_WIDGET_UPDATE() - CHECK 1 - Warping Player to Submarine Spawn Location")
			
			IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), v_DebugSubmarineWarpPlayerPos)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), f_DebugSubmarineWarpPlayerHeading)
			ENDIF
			
			b_DebugSubmarineWarpPlayer = FALSE
		ENDIF
		
		
		// CREATE SUBMARINE
		IF b_DebugSpawnSubmarine
			IF NOT DOES_ENTITY_EXIST(e_DebugSubmarine)
				REQUEST_MODEL(m_DebugSubmarineModel)
				WHILE NOT HAS_MODEL_LOADED(m_DebugSubmarineModel)
					WAIT(0)
				ENDWHILE
				
				e_DebugSubmarine = CREATE_VEHICLE(m_DebugSubmarineModel, v_DebugSubmarineSpawnPos, f_DebugSubmarineSpawnHeading)
				
				CREATE_BLIP_FOR_VEHICLE(e_DebugSubmarine)
				
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SUBMARINE_WIDGET_UPDATE() - CHECK 2 - Created Submarine at Spawn Location")
			ENDIF
			
			b_DebugSpawnSubmarine = FALSE
		ENDIF
		
		
		// DELETE SUBMARINE
		IF b_DebugRemoveSubmarine
			IF NOT DOES_ENTITY_EXIST(e_DebugSubmarine)
				DELETE_VEHICLE(e_DebugSubmarine)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SUBMARINE_WIDGET_UPDATE() - CHECK 3 - Deleted Submarine")
			ENDIF
			
			b_DebugRemoveSubmarine = FALSE
		ENDIF
	ENDPROC
	
	
	
//========================================================================================================================================================//
	
	
	
	/// PURPOSE:
	///    Updates all script created widgets - Call every frame
	PROC UPDATE_WIDGETS()
		SCRIPT_DEBUG_WIDGET_UPDATE()
		SET_ANIMAL_COLLECTED_WIDGET_UPDATE()
		SET_BIT_WIDGET_UPDATE()
		BLIP_NEARBY_ANIMALS_WIDGET_UPDATE()
		CREATE_ANIMAL_WIDGET_UPDATE()
		BONETAG_SELECT_WIDGET_UPDATE()
		SUBMARINE_WIDGET_UPDATE()
	ENDPROC

	
	
	/// PURPOSE:
	///    Updates all debug components - Call every frame
	PROC DEBUG_UPDATE()
		// UPDATE ALL SCRIPTED WIDGETS
		UPDATE_WIDGETS()
	ENDPROC



	/// PURPOSE:
	///    Draws debug spheres on focus ped's bone positions
	PROC DRAW_DEBUG_SPHERE_AT_BONETAG_COORD()
		DRAW_DEBUG_SPHERE(v_ModelMax, 0.1)
		DRAW_DEBUG_SPHERE(v_ModelMin, 0.1)
	ENDPROC
	
	
	
	/// PURPOSE:
	///    Initialises all debug components
	PROC INIT_DEBUG()
		CREATE_DEBUG_WIDGETS()
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INIT_DEBUG() - CHECK 1 - Created Debug Widgets")
	ENDPROC
	
	
	
	/// PURPOSE:
	///    Deletes all script created widgets
	PROC DESTROY_WIDGETS()
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DESTROY_WIDGETS() - CHECK 1 - Deactivated Debug Lines and Spheres Drawing")
		
		IF DOES_WIDGET_GROUP_EXIST(wg_WidgetGroupID)
			DELETE_WIDGET_GROUP(wg_WidgetGroupID)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DESTROY_WIDGETS() - CHECK 2 - Deleted Widget Group")
		ENDIF
	ENDPROC
#ENDIF


//================================================================================================================================================
//		DEBUG FUNCTIONS/PROCEDURES - END
//================================================================================================================================================



/// PURPOSE:
///    Checks if the animal model in the photograph has already been collected
/// PARAMS:
///    m_CheckAnimalModel - MODEL_NAMES: Name of model to be checked
/// RETURNS:
///    BOOL: TRUE if model passed has been collected
FUNC BOOL HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED(MODEL_NAMES m_CheckAnimalModel)
	SWITCH m_CheckAnimalModel
		CASE A_C_BOAR
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 1 - Checking BOAR Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_01)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 2 - BOAR Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 3 - BOAR Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_CAT_01
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 4 - Checking CAT Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_02)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 5 - CAT Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 6 - CAT Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_CHICKENHAWK
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 7 - Checking CHICKENHAWK Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_03)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 8 - CHICKENHAWK Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 9 - CHICKENHAWK Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_CORMORANT
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 10 - Checking CORMORANT Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_04)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 11 - CORMORANT Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 12 - CORMORANT Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_COW
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 13 - Checking COW Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_05)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 14 - COW Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 15 - COW Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_COYOTE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 16 - Checking COYOTE Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_06)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 17 - COYOTE Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 18 - COYOTE Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_CROW
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 19 - Checking CROW Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_07)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 20 - CROW Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 21 - CROW Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_DEER
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 22 - Checking DEER Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_08)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 23 - DEER Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 24 - DEER Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_DOLPHIN
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 25 - Checking DOLPHIN Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_09)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 26 - DOLPHIN Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 27 - DOLPHIN Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		/*
		CASE A_C_FISH
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 28 - Checking FISH Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_10)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 29 - FISH Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 30 - FISH Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_HEN
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 31 - Checking HEN Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_11)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 32 - HEN Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 33 - HEN Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_HUMPBACK
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 34 - Checking HUMPBACK Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_12)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 35 - HUMPBACK Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 36 - HUMPBACK Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_HUSKY
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 37 - Checking HUSKY Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_13)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 38 - HUSKY Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 39 - HUSKY Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_KILLERWHALE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 40 - Checking KILLERWHALE Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_14)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 41 - KILLERWHALE Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 42 - KILLERWHALE Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_MTLION
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 43 - Checking MTLION Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_15)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 44 - MTLION Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 45 - MTLION Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_PIG
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 46 - Checking PIG Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_16)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 47 - PIG Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 48 - PIG Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_PIGEON
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 49 - Checking PIGEON Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_17)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 50 - PIGEON Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 51 - PIGEON Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_POODLE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 52 - Checking POODLE Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_18)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 53 - POODLE Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 54 - POODLE Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_PUG
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 55 - Checking PUG Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_19)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 56 - PUG Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 57 - PUG Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_RABBIT_01
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 58 - Checking RABBIT Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 59 - RABBIT Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 60 - RABBIT Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_RAT
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 58 - Checking RAT Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_20)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 59 - RAT Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 60 - RAT Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_RETRIEVER
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 61 - Checking RETRIEVER Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_21)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 62 - RETRIEVER Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 63 - RETRIEVER Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_ROTTWEILER
		CASE A_C_CHOP
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 64 - Checking ROTTWEILER Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_22)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 65 - ROTTWEILER Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 66 - ROTTWEILER Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_SEAGULL
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 67 - Checking SEAGULL Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_23)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 68 - SEAGULL Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 69 - SEAGULL Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_SHARKHAMMER
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 70 - Checking SHARKHAMMER Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_24)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 71 - SHARKHAMMER Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 72 - SHARKHAMMER Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		CASE A_C_SHARKTIGER
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 73 - Checking SHARKTIGER Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_25)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 74 - SHARKTIGER Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 75 - SHARKTIGER Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_SHEPHERD
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 76 - Checking SHEPHERD Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_26)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 77 - SHEPHERD Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 78 - SHEPHERD Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		/*
		CASE A_C_STINGRAY
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 79 - Checking STINGRAY Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_27)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 80 - STINGRAY Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 81 - STINGRAY Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		*/
		
		CASE A_C_WESTY
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 82 - Checking WESTY Model")
			
			IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ANIMAL_28)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 83 - WESTY Model NOT Collected")
				
				RETURN FALSE
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 84 - WESTY Model Already Collected")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		
		DEFAULT
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED() - CHECK 85 - Invalid Animal Model")
			b_InvalidModel = TRUE
		BREAK
	ENDSWITCH
	
	
	RETURN TRUE
	
ENDFUNC



/// PURPOSE:
///    Checks if the animal model is a bird
/// PARAMS:
///    m_CheckAnimalModel - MODEL_NAMES: Name of model to be checked
/// RETURNS:
///    BOOL: TRUE if model passed is a bird model
FUNC BOOL IS_MODEL_A_BIRD(MODEL_NAMES m_CheckAnimalModel)
	SWITCH m_CheckAnimalModel
		CASE A_C_CHICKENHAWK
		CASE A_C_CORMORANT
		CASE A_C_CROW
		CASE A_C_HEN
		CASE A_C_PIGEON
		CASE A_C_SEAGULL
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: IS_MODEL_A_BIRD() - CHECK 1 - Model is a Bird")
			
			RETURN TRUE
		BREAK
		
		DEFAULT
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: IS_MODEL_A_BIRD() - CHECK 2 - Model is Not a Bird")
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Gets the max distance the player can be from an animal to be able to take a valid photograph
/// PARAMS:
///    m_CheckAnimalModel - MODEL_NAMES: Name of model to be checked
/// RETURNS:
///    INT: Returns max success distance based on model passed
FUNC INT GET_MAX_SUCCESS_DISTANCE_FOR_MODEL(MODEL_NAMES m_CheckAnimalModel)
	INT i_ReturnDist
	
	SWITCH m_CheckAnimalModel
		CASE A_C_BOAR
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 1 - BOAR Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_CAT_01
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 2 - CAT Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_CHICKENHAWK
			i_ReturnDist = 60
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 3 - CHICKENHAWK Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_CORMORANT
			i_ReturnDist = 60
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 4 - CORMORANT Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_COW
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 5 - COW Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_COYOTE
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 6 - COYOTE Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_CROW
			i_ReturnDist = 60
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 7 - CROW Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_DEER
			i_ReturnDist = 40 //30 		B* 1979308 - CC
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 8 - DEER Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_DOLPHIN
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 9 - DOLPHIN Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_FISH
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 10 - FISH Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_HEN
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 11 - HEN Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_HUMPBACK
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 12 - HUMPBACK Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_HUSKY
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 13 - HUSKY Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_KILLERWHALE
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 14 - KILLERWHALE Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_MTLION
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 15 - MTLION Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_PIG
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 16 - PIG Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_PIGEON
			i_ReturnDist = 40
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 17 - PIGEON Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_POODLE
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 18 - POODLE Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_PUG
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 19 - PUG Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_RABBIT_01
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 20 - RABBIT Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_RAT
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 20 - RAT Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_RETRIEVER
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 21 - RETRIEVER Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_ROTTWEILER
		CASE A_C_CHOP
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 22 - ROTTWEILER Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_SEAGULL
			i_ReturnDist = 60
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 23 - SEAGULL Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_SHARKHAMMER
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 24 - SHARKHAMMER Max Distance: ", i_ReturnDist)
		BREAK
		
		
		CASE A_C_SHARKTIGER
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 25 - SHARKTIGER Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_SHEPHERD
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 26 - SHEPHERD Max Distance: ", i_ReturnDist)
		BREAK
		
		/*
		CASE A_C_STINGRAY
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 27 - STINGRAY Max Distance: ", i_ReturnDist)
		BREAK
		*/
		
		CASE A_C_WESTY
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 28 - WESTY Max Distance: ", i_ReturnDist)
		BREAK
		
		
		DEFAULT
			i_ReturnDist = 30
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_MAX_SUCCESS_DISTANCE_FOR_MODEL() - CHECK 29 - Unknow Animal Model: Setting Default Max Distance: ", i_ReturnDist)
		BREAK
	ENDSWITCH
	
	
	RETURN i_ReturnDist
ENDFUNC



/// PURPOSE:
///    Returns all nearby animal peds with option to blip them all - {NOT IN USE}
/// PARAMS:
///    b_Blip - BOOL: Pass TRUE to blip the neaerby peds
PROC GET_NEARBY_PEDS(BOOL b_Blip = FALSE)
	PED_INDEX a_PedArray[NUM_MAX_ANIMALS]

	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_NEARBY_PEDS() - CHECK 1 - Getting All Nearby Peds")
	GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), a_PedArray, PEDTYPE_ARMY | PEDTYPE_BUM | PEDTYPE_CIVFEMALE | PEDTYPE_CIVMALE | PEDTYPE_COP | PEDTYPE_CRIMINAL | PEDTYPE_DEALER
			 | PEDTYPE_FIRE | PEDTYPE_GANG_CHINESE_JAPANESE | PEDTYPE_GANG_PUERTO_RICAN | PEDTYPE_GANG1 | PEDTYPE_GANG10 | PEDTYPE_GANG2 | PEDTYPE_GANG3 | PEDTYPE_GANG4
			  | PEDTYPE_GANG5 | PEDTYPE_GANG6 | PEDTYPE_GANG7 | PEDTYPE_GANG8 | PEDTYPE_GANG9 | PEDTYPE_INVALID | PEDTYPE_MEDIC | PEDTYPE_PLAYER_NETWORK
			   | PEDTYPE_PLAYER_UNUSED | PEDTYPE_PLAYER1 | PEDTYPE_PLAYER2 | PEDTYPE_PROSTITUTE | PEDTYPE_SPECIAL | PEDTYPE_SWAT)
	
	
	INT i_Counter
	FOR i_Counter = 0 TO (NUM_MAX_ANIMALS - 1)
		a_Peds[i_Counter].ped = a_PedArray[i_Counter]
		a_Peds[i_Counter].model = GET_ENTITY_MODEL(a_PedArray[i_Counter])
	ENDFOR
	
	IF b_Blip
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_NEARBY_PEDS() - CHECK 2 - Removing Old Blips")
		FOR i_Counter = 0 TO (NUM_MAX_ANIMALS - 1)
			IF DOES_BLIP_EXIST(a_Peds[i_Counter].blip)
				REMOVE_BLIP(a_Peds[i_Counter].blip)
			ENDIF
		ENDFOR
		
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_NEARBY_PEDS() - CHECK 3 - Blipping All Nearby Peds")
		FOR i_Counter = 0 TO (NUM_MAX_ANIMALS - 1)
			IF IS_ENTITY_ALIVE(a_Peds[i_Counter].ped)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: GET_NEARBY_PEDS() - CHECK 4 - Ped at index ", i_Counter, " has model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
				
				a_Peds[i_Counter].blip = CREATE_BLIP_FOR_PED(a_Peds[i_Counter].ped)
				SET_BLIP_COLOUR(a_Peds[i_Counter].blip, 10)
			ENDIF
		ENDFOR
	ENDIF
ENDPROC



/// PURPOSE:
///    Removes all nearby peds stored that are not visible using ped tracking - {NOT IN USE}
PROC CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS()
	INT i_Counter
	FOR i_Counter = 0 TO (NUM_MAX_ANIMALS - 1)
		IF DOES_ENTITY_EXIST(a_Peds[i_Counter].ped)
		AND !a_Peds[i_Counter].b_CheckFinished
			IF IS_ENTITY_ALIVE(a_Peds[i_Counter].ped)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 1 - Focus Ped Index: ", i_Counter, " has Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model), " - State Index: ", a_Peds[i_Counter].i_Event)
				
				SWITCH a_Peds[i_Counter].i_Event
					CASE 0
						IF IS_ENTITY_ON_SCREEN(a_Peds[i_Counter].ped)
							IF NOT IS_ENTITY_OCCLUDED(a_Peds[i_Counter].ped)
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 2 - Distance Between Player and Focus Ped: ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Peds[i_Counter].ped))
								
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Peds[i_Counter].ped) < GET_MAX_SUCCESS_DISTANCE_FOR_MODEL(GET_ENTITY_MODEL(a_Peds[i_Counter].ped))
									//IF CHECK_ENTITY_FULLY_ONSCREEN(i_Counter)
										CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 9 - Request Visibility Tracking for Ped at Index: ", i_Counter, " - Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
										
										
										REQUEST_PED_VISIBILITY_TRACKING(a_Peds[i_Counter].ped)
										a_Peds[i_Counter].i_Event++
									//ELSE
										CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 5 - Ped at Index: ", i_Counter, " Model Not Fully Onscreen - Removing Ped Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
										CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
										CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
										
										a_Peds[i_Counter].ped = NULL
										a_Peds[i_Counter].b_CheckFinished = TRUE
									//ENDIF
								ELSE
									CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 6 - Ped at Index: ", i_Counter, " Too Far from Player - Removing Ped Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
									CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
									CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
									
									a_Peds[i_Counter].ped = NULL
									a_Peds[i_Counter].b_CheckFinished = TRUE
								ENDIF
							ELSE
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 7 - Ped at Index: ", i_Counter, " Is Occluded - Removing Ped Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
								
								a_Peds[i_Counter].ped = NULL
								a_Peds[i_Counter].b_CheckFinished = TRUE
							ENDIF
						ELSE
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 8 - Ped at Index: ", i_Counter, " Not On Screen - Removing Ped Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
							
							a_Peds[i_Counter].ped = NULL
							a_Peds[i_Counter].b_CheckFinished = TRUE
						ENDIF
					BREAK
					
					
					CASE 1
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 14 - Waiting for Visibility Tracking for Ped at Index: ", i_Counter, " - Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
					
						IF IS_PED_TRACKED(a_Peds[i_Counter].ped)
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 10 - Ped being Tracked for Ped at Index: ", i_Counter, " - Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 12 - ", GET_TRACKED_PED_PIXELCOUNT(a_Peds[i_Counter].ped)," Pixel Count for Ped Index: ", i_Counter, " has Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
							
							INT i_FrameCounter
							i_FrameCounter = 0
							WHILE (i_FrameCounter < i_TrackingFrameWait)// AND GET_TRACKED_PED_PIXELCOUNT(a_Peds[i_Counter].ped) = -1)
								i_FrameCounter++
								WAIT(0)
							ENDWHILE
							
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 10 - Frames: ", i_FrameCounter)
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 12 - ", GET_TRACKED_PED_PIXELCOUNT(a_Peds[i_Counter].ped)," Pixel Count for Ped Index: ", i_Counter, " has Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
							
							
							// LOS TEST
							IF IS_TRACKED_PED_VISIBLE(a_Peds[i_Counter].ped) // FAILING
								a_Peds[i_Counter].i_Event++
								
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 11 - Accepting Ped at Index: ", i_Counter)
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 12 - ", GET_TRACKED_PED_PIXELCOUNT(a_Peds[i_Counter].ped), " Pixel Count for Ped Index: ", i_Counter, " has Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
							ELSE
								CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 13 - Ped at Index: ", i_Counter, " Tracking Ped Not Visible - Removing Ped Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
								
								a_Peds[i_Counter].ped = NULL
								a_Peds[i_Counter].b_CheckFinished = TRUE
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						// CHECK PIXELS
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 15 - Pixel Check for Ped at Index: ", i_Counter, " - Model: ", GET_MODEL_NAME_FOR_DEBUG(a_Peds[i_Counter].model))
						
						a_Peds[i_Counter].b_CheckFinished = TRUE
					BREAK
				ENDSWITCH
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 16 - Ped is Not Alive - Remove Ped at Index: ", i_Counter, " - Removing Ped")
				a_Peds[i_Counter].ped = NULL
				a_Peds[i_Counter].b_CheckFinished = TRUE
			ENDIF
		
		ELIF NOT DOES_ENTITY_EXIST(a_Peds[i_Counter].ped)
		AND !a_Peds[i_Counter].b_CheckFinished
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_NEARBY_PEDS_NOT_IN_FOCUS() - CHECK 17 - Ped Does Not Exist - Remove Ped at Index: ", i_Counter, " - Removing Ped ")
			a_Peds[i_Counter].ped = NULL
			a_Peds[i_Counter].b_CheckFinished = TRUE
		ENDIF
	ENDFOR
ENDPROC



/// PURPOSE:
///    Checks that the focus ped's max and min bone positions are within the screen boundary offsets
/// PARAMS:
///    e_Ped - PED_INDEX: Ped to be checked
///    maxBone - PED_BONETAG: Pass the Head bonetag unless other is needed
/// RETURNS:
///    BOOL: TRUE if passed ped is onscreen and within the buffer regions
FUNC BOOL CHECK_ENTITY_FULLY_ONSCREEN(PED_INDEX e_Ped, PED_BONETAG maxBone)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 1 - Check Entity is Fully on Screen Started")
	
	FLOAT f_Min_X = 0.0, f_Min_Y = 0.0, f_Max_X = 0.0, f_Max_Y = 0.0

	v_ModelMax = GET_WORLD_POSITION_OF_ENTITY_BONE(e_Ped, GET_PED_BONE_INDEX(e_Ped, maxBone))
	
	
	// SPECIAL CASE - HAS NO SPINE3 BONETAG - USUALLY SMALL ANIMAL MODELS
	IF (m_TargetAnimalModel = A_C_RABBIT_01
	OR m_TargetAnimalModel = A_C_RAT)
		v_ModelMin = GET_WORLD_POSITION_OF_ENTITY_BONE(e_Ped, GET_PED_BONE_INDEX(e_Ped, BONETAG_SPINE1))
	ELSE
		// CARRY OUT NORMAL CHECKS
		v_ModelMin = GET_WORLD_POSITION_OF_ENTITY_BONE(e_Ped, GET_PED_BONE_INDEX(e_Ped, BONETAG_SPINE3))
	ENDIF
	
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 2 - v_ModelMin: ", v_ModelMin)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 3 - v_ModelMax: ", v_ModelMax)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	
	GET_SCREEN_COORD_FROM_WORLD_COORD(v_ModelMin, f_Min_X, f_Min_Y)
	GET_SCREEN_COORD_FROM_WORLD_COORD(v_ModelMax, f_Max_X, f_Max_Y)
	
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 4 - f_Min_X: ", f_Min_X)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 5 - f_Max_X: ", f_Max_X)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 6 - f_Min_Y: ", f_Min_Y)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 7 - f_Max_Y: ", f_Max_Y)
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	
	// CHEK IF BONE POSITIONS ARE WITHIN THE DEFINE BORDERS
	IF ((f_Min_X >= MIN_SCREEN_WIDTH_VALUE 	AND f_Min_X <= MAX_SCREEN_WIDTH_VALUE)
	AND (f_Max_X >= MIN_SCREEN_WIDTH_VALUE 	AND f_Max_X <= MAX_SCREEN_WIDTH_VALUE)
	AND (f_Min_Y >= MIN_SCREEN_HEIGHT_VALUE AND f_Min_Y <= MAX_SCREEN_HEIGHT_VALUE)
	AND (f_Max_Y >= MIN_SCREEN_HEIGHT_VALUE AND f_Max_Y <= MAX_SCREEN_HEIGHT_VALUE))
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 8 - Entity is Fully on Screen")
		
		RETURN TRUE
	ENDIF
	
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ENTITY_FULLY_ONSCREEN() - CHECK 9 - Entity is Not Fully on Screen")
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Finds the focus ped in a photograph and checks that the focus ped is valid
/// PARAMS:
///    e_Model - MODEL_NAMES: A reference to a MODEL_NAMES variable to store the focus ped model 
///    f_MaxDistance - FLOAT: Pass 60.0 as this is the farthest that a valid photo of any animal can be taken - See GET_MAX_SUCCESS_DISTANCE_FOR_MODEL for max animal distances
///    bt_ScreenPositionTestBoneTag - Pass BONETAG_ROOT as this parameter
///    f_MaxScreenWidthRatioAroundCentreForTestBone - FLOAT: Pass 0.8 as this parameter
///    f_MaxScreenHeightRatioAroundCentreForTestBone - FLOAT: Pass 0.7 as this parameter
///    f_MinRelativeHeadingScore - FLOAT: Pass 0.25 as this parameter
///    f_MaxScreenCentreScoreBoost - FLOAT: Pass 8.0 as this parameter
///    f_MaxScreenRatioAroundCentreForScoreBoost - FLOAT: Pass 0.333 as this parameter
///    bt_LineOfSightTestBoneTag1 - PED_BONETAG: Pass BONETAG_ROOT as this parameter
///    bt_LineOfSightTestBoneTag2 - PED_BONETAG: Pass BONETAG_SPINE3 for all animals except when animal is small ie rabbits or rats - use BONETAG_SPINE1
/// RETURNS:
///    PED_INDEX: Returns a valid focus ped or NULL if no valid focus ped is found
FUNC PED_INDEX VALID_PHOTO_CHECKS(MODEL_NAMES &e_Model, FLOAT f_MaxDistance = 30.0, PED_BONETAG bt_ScreenPositionTestBoneTag = BONETAG_ROOT, FLOAT f_MaxScreenWidthRatioAroundCentreForTestBone = 0.8, FLOAT f_MaxScreenHeightRatioAroundCentreForTestBone = 0.7, FLOAT f_MinRelativeHeadingScore = 0.25, FLOAT f_MaxScreenCentreScoreBoost = 8.0, FLOAT f_MaxScreenRatioAroundCentreForScoreBoost = 0.333, PED_BONETAG bt_LineOfSightTestBoneTag1 = BONETAG_ROOT, PED_BONETAG bt_LineOfSightTestBoneTag2 = BONETAG_SPINE3)
	PED_INDEX e_Ped

	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 1 - Valid Photo Checks Started - Getting Focus Ped")
	
	// GET ENTITY IN QUESTION
	e_Ped = GET_FOCUS_PED_ON_SCREEN(f_MaxDistance, bt_ScreenPositionTestBoneTag, f_MaxScreenWidthRatioAroundCentreForTestBone, f_MaxScreenHeightRatioAroundCentreForTestBone, f_MinRelativeHeadingScore, f_MaxScreenCentreScoreBoost, f_MaxScreenRatioAroundCentreForScoreBoost, bt_LineOfSightTestBoneTag1, bt_LineOfSightTestBoneTag2)
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 2 - bt_ScreenPosTestBoneTag:",  GET_BONETAG_NAME_FOR_DEBUG(bt_ScreenPositionTestBoneTag))
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 3 - bt_LosTestBoneTag1:",  GET_BONETAG_NAME_FOR_DEBUG(bt_LineOfSightTestBoneTag1))
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 4 - bt_LosTestBoneTag2:",  GET_BONETAG_NAME_FOR_DEBUG(bt_LineOfSightTestBoneTag2))
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
	
	
	IF DOES_ENTITY_EXIST(e_Ped)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 5 - Entity Exists")
	ENDIF
	
	IF e_Ped != NULL
	AND IS_ENTITY_ALIVE(e_Ped)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 6 - Focus Ped Ascertained")

		// CHECK IS PEDTYPE_ANIMAL
		IF GET_PED_TYPE(e_Ped) = PEDTYPE_ANIMAL
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 7 - Focus Ped is an Animal Ped_Type")
		
			// IS ENTITY OCLUDED
			IF NOT IS_ENTITY_OCCLUDED(e_Ped)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 8 - Focus Ped is Not Occluded")
			
				// CHECK MODEL
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 9 - Getting Focus Ped Model")
				e_Model = GET_ENTITY_MODEL(e_Ped)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 10 - Focus Ped Model Ascertained - Model: ", GET_MODEL_NAME_FOR_DEBUG(e_Model))
					
				IF CHECK_ENTITY_FULLY_ONSCREEN(e_Ped, BONETAG_HEAD)
							
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 9 - Focus Ped Model Dimensions Are On Screen")
					
					FLOAT f_AnimalDistanceFromPlayer
					f_AnimalDistanceFromPlayer =  GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), e_Ped, TRUE)
					
					
					//////////////////
					///    	  test
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(PLAYER_PED_ID(), e_Ped)
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 11 - CLEAR LINE OF SIGHT")
					ELSE
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 12 - NO LINE OF SIGHT")
					ENDIF
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "")
					
					
					// CHECK IF MODEL IS A BIRD OR LAND ANIMAL
					IF IS_MODEL_A_BIRD(e_Model)
						// IS A BIRD
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 13 - Focus Ped is a Bird")
						IF f_AnimalDistanceFromPlayer <= GET_MAX_SUCCESS_DISTANCE_FOR_MODEL(e_Model)
							// IS IN RANGE
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 14 - Focus Ped Bird is in Range")
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 15 - Valid Photo Checks Finished")
						
						ELSE
							// NOT IN RANGE
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 16 - Focus Ped Bird is Not in Range")
							
							// RESET PED
							e_Ped = NULL
						ENDIF
					ELSE
						// IS AN ANIMAL
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 17 - Focus Ped is an Animal")
						IF f_AnimalDistanceFromPlayer <= GET_MAX_SUCCESS_DISTANCE_FOR_MODEL(e_Model)
							// IS IN RANGE
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 18 - Focus Ped Animal is in Range")
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 19 - Valid Photo Checks Finished")
							
						ELSE
							// NOT IN RANGE
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 20 - Focus Ped Animal is Not in Range")
							
							// RESET PED
							e_Ped = NULL
						ENDIF
					ENDIF
				ELSE
					// MODEL DIMENSIONS NOT ON SCREEN
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 21 - Focus Ped Model Dimensions Not On Screen")
					
					// RESET PED
					e_Ped = NULL
				ENDIF
				
			ELSE
				// NOT ON SCREEN
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 21 - Focus Ped Occluded - Not On Screen")
				
				// RESET PED
				e_Ped = NULL
			ENDIF
			
		ELSE
			// NOT AN ANIMAL PED_TYPE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 22 - Focus Ped is Not an Animal Ped_Type")
			
			// RESET PED
			e_Ped = NULL
		ENDIF
		
	ELSE
		// NO PED IN SHOT
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: VALID_PHOTO_CHECKS() - CHECK 23 - No Focus Ped in Photograph")
		
		// RESET PED
		e_Ped = NULL
	ENDIF

	RETURN e_Ped
ENDFUNC



/// PURPOSE:
///    Handles checking if player has taken a photograph of an animal, validating the photography, sending the photograph and cleaning up after these processes
PROC CHECK_FOR_ANIMAL_PHOTO()
	SWITCH e_PhotographProcessStage
		// CHECK FOR PHOTOGRAGH
		CASE PPF_WAIT_FOR_PHOTOGRAPH
			// HAS A PHOTO BEEN TAKEN
			IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 1 - A Photograph has been Taken")
				
				// NOT A SELFIE AND NO FILTERS OR BORDERS ACTIVE
				IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
				AND g_i_CurrentlySelected_SnapMaticFilter = 0
				AND g_i_CurrentlySelected_SnapMaticBorder = 0
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 2 - Photograph doesn't have a Filter/Border and is not a Selfie")
					
					e_PhotographProcessStage = PPF_VALID_PHOTOGRAPH_CHECK
					
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 3 - Photograph has a Filter/Border or is a Selfie")
				ENDIF
			ENDIF
		BREAK
		
		
		// INITIAL VALID PHOTOGRAPH CHECKS
		CASE PPF_VALID_PHOTOGRAPH_CHECK
			// GET ENTITY IN QUESTION
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 4 - Getting Focus Ped")
			e_FocusAnimal = VALID_PHOTO_CHECKS(m_TargetAnimalModel, 60, bt_ScreenPosTestBoneTag, 0.8, 0.7, 0.25, 8, 0.333, bt_LosTestBoneTag1, bt_LosTestBoneTag2)
			
			IF e_FocusAnimal = NULL
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 5 - Focus Ped is NULL - Trying Alternate Get Focus Ped Function")
				
				// ALTERNATE GET FOCUS PED GRAB - DIFFERENT BONETAGS
				e_FocusAnimal = VALID_PHOTO_CHECKS(m_TargetAnimalModel, 60, bt_AltScreenPosTestBoneTag, 0.8, 0.7, 0.25, 8, 0.333, bt_AltLosTestBoneTag1, bt_AltLosTestBoneTag2)
		
				IF e_FocusAnimal = NULL
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 6 - Focus Ped is NULL: Alternate Get Focus Ped Function Failed to Find a Focus Ped")
					
					// RESET VARIABLES
					e_PhotographProcessStage = PPF_CLEAN_UP
					
				ELSE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 7 - Alternative Focus Ped is Valid")
					e_PhotographProcessStage = PPF_PED_MODEL_CHECK
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 8 - Focus Ped is Valid")
				e_PhotographProcessStage = PPF_PED_MODEL_CHECK
			ENDIF
		BREAK
		
		
		// CHECK ANIMAL MODEL COLLECTED
		CASE PPF_PED_MODEL_CHECK
			IF NOT HAS_ANIMAL_MODEL_PHOTOGRAPH_ALREADY_BEEN_COLLECTED(m_TargetAnimalModel)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 9 - Focus Ped Model Not Already Collected")
			
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 10 - Focus Ped Accepted")
				
				// ALLOW PLAYER TO USE SEND FUNCTION IN CELLPHONE
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE) 
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 11 - Enabling Picture Message Sending")
				
				e_PhotographProcessStage = PPF_SEND_PHOTOGRAPH
				
			ELSE
				IF NOT b_InvalidModel
					// ALREADY HAVE MODEL
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 12 - Aleady Collected Focus Ped Model")
					
				ELSE
					// MODEL NOT ON LIST
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 13 - Focus Ped Model is Not on List")
					b_InvalidModel = FALSE
				ENDIF
				
				// RESET VARIABLES
				e_PhotographProcessStage = PPF_CLEAN_UP
			ENDIF
		BREAK
		
		
		// VALID PHOTOGRAPH ACCEPTED
		CASE PPF_SEND_PHOTOGRAPH
			SWITCH e_SendPhotographState
				// HANDLE SEND PHOTOGRAPH OPTION SCREEN
				CASE SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN
					// PRINT HELP 1
					PRINT_HELP("PW_HELP_1")
					
					IF IS_CONTACTS_LIST_ON_SCREEN()
						IF IS_PHONE_ONSCREEN()
							CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 14 - Forcing Selection of Contact to Los Santos Tourist Board Only")
							
		                    FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_LS_TOURIST_BOARD)
							
							// REMOVE HELP 1
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_1")
								CLEAR_THIS_PRINT("PW_HELP_1")
							ENDIF
						
							e_SendPhotographState = SPF_HANDLE_CONTACT_SCREEN
						ENDIF
		     		ENDIF
					
					// CHECK FOR PLAYER CANCEL
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_SELECT)		//	PAD:	CROSS
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_OPTION)		//	PAD:	TRIANGLE
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 15 - Player Cancelled on Send Photograph Option Screen")
						
						ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
						
						CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_LS_TOURIST_BOARD)
						
						// REMOVE HELP 1
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_1")
							CLEAR_THIS_PRINT("PW_HELP_1")
						ENDIF
						
						e_PhotographProcessStage = PPF_CLEAN_UP
					ENDIF
				BREAK
				
				
				// HANDLE SELECT CONTACT SCREEN
				CASE SPF_HANDLE_CONTACT_SCREEN
					// PRINT HELP 2
					PRINT_HELP("PW_HELP_2")
					
					// CHECK IF PHOTOGRAPH SENT TO RIGHT PERSON
					IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_LS_TOURIST_BOARD)
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 16 - Los Santos Tourist Board Has Received Photograph")
						
						e_SendPhotographState = SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN
						
						ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
						
						CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_LS_TOURIST_BOARD)
						
						// REMOVE HELP 2
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_2")
							CLEAR_THIS_PRINT("PW_HELP_2")
						ENDIF
						
						e_PhotographProcessStage = PPF_HANDLE_UPDATES
					ENDIF
					
					// CHECK FOR PLAYER CANCEL
					IF  IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)		//	PAD:	CIRCLE
						CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 17 - Player Cancelled on Select Contact Screen")
						
						e_SendPhotographState = SPF_HANDLE_SEND_PHOTOGRAPH_SCREEN
						ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
						
						CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_LS_TOURIST_BOARD)
						
						// REMOVE HELP 2
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PW_HELP_2")
							CLEAR_THIS_PRINT("PW_HELP_2")
						ENDIF
						
						e_PhotographProcessStage = PPF_CLEAN_UP
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		
		// PHOTOGRAPH SENT
		CASE PPF_HANDLE_UPDATES
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 18 - Focus Ped Photograph Received by Contact")
			
			// SET ANIMAL MODEL BIT SET & ANIMAL ACTIVITY FEED
			SET_ANIMAL_MODEL_AS_COLLECTED(m_TargetAnimalModel)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 19 - Set Animal Model as Collected")
			
			
			// UPDATE ACTIVITY FEED
			REQUEST_SYSTEM_ACTIVITY_TYPE_TAKEN_ANIMAL_PHOTOGRAPHS(e_AnimalActivityFeed)
			
			
			// UPDATE FRONT END STAT
			STAT_SET_INT(NUM_HIDDEN_PACKAGES_7, GET_NUMBER_OF_COMPLETED_ANIMAL_PHOTOS())
			
			
			// DRAW SCALEFORM
			b_DisplayScaleform = TRUE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 20 - Set b_DisplayScaleform: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_DisplayScaleform))
			
			
			// START PUT PHONE AWAY TIMER
			START_TIMER_NOW_SAFE(t_PutPhoneAwayTimer)
			
			
			// START SEND EMAIL DELAY TIMER
			START_TIMER_NOW_SAFE(t_SendEmailDelayTimer)
			b_SendDynamicEmail = TRUE
			
			
			e_PhotographProcessStage = PPF_CLEAN_UP
		BREAK
		
		
		// CLEAN UP
		CASE PPF_CLEAN_UP
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 21 - Cleaning Up Wildlife Photogrphy Variables")
			
			// RESET ALL VARIABLES
			CLEANUP_WILDLIFE_PHOTOGRAPH_VARIABLES()
			
			e_PhotographProcessStage = PPF_WAIT_FOR_CAMERA_MODE
		BREAK
		
		
		// WAIT FOR BACK IN CAMERA MODE
		CASE PPF_WAIT_FOR_CAMERA_MODE
			IF NOT HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_ANIMAL_PHOTO() - CHECK 22 - Set e_PhotographProcessStage: PPF_WAIT_FOR_PHOTOGRAPH")
				
				e_PhotographProcessStage = PPF_WAIT_FOR_PHOTOGRAPH
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Checks how many animal photographs have been successfully submitted by player
/// RETURNS:
///    INT: Returns integer number of completed animal photographs
FUNC INT CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION() - CHECK 1 - Getting Number of Complete Animal Photographs")
	
	INT i_CompleteAnimalPhotos = GET_NUMBER_OF_COMPLETED_ANIMAL_PHOTOS()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION() - CHECK 2 - Photographs Collected: ", i_CompleteAnimalPhotos, " of ", NUMBER_OF_ANIMALS)
	
	
	IF (i_CompleteAnimalPhotos = NUMBER_OF_ANIMALS)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION() - CHECK 3 - All Animal Photos Collected")
		SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ALL_PHOTOGRAPHS_COLLECTED)
	
		//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION() - [ACHIEVEMENT]: ACH29 - Close Shave : ", i_CompleteAnimalPhotos, " of ", NUMBER_OF_ANIMALS)
		//AWARD_ACHIEVEMENT(ACH29) // close shave
	ENDIF
	
	RETURN i_CompleteAnimalPhotos
ENDFUNC

   

/// PURPOSE:
///    Draws the Wildlife Photography scaleform screen
PROC DRAW_WILDLIFE_PHOTOGRAPHY_SCALEFORM()
	IF NOT IS_RESULT_SCREEN_DISPLAYING()
		IF HAS_SCALEFORM_MOVIE_LOADED(sf_Movie)
			//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DRAW_WILDLIFE_PHOTOGRAPHY_SCALEFORM() - CHECK 1 -  Draw Scaleform")
			
			DRAW_SCALEFORM_MOVIE(sf_Movie,0.5, 0.5, 1.0, 1.0, 100, 100, 100, 255)
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Cleans up the Wildlife Photography scaleform screen
PROC CLEANUP_SCALEFORM()
	IF HAS_SCALEFORM_MOVIE_LOADED(sf_Movie)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CLEANUP_SCALEFORM() - CHECK 1 - Clean Up Scaleform")
		
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sf_Movie)
	ENDIF
ENDPROC



/// PURPOSE:
///    Handles the display and cleanup of the Wildlife Photography sclaeform screen
PROC DISPLAY_SCALEFORM()
	SWITCH e_ScaleformStage
		CASE SSF_LOAD_SCALEFORM
			IF b_DisplayScaleform
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 1 - b_DisplayScaleform = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_DisplayScaleform))
				
				IF NOT IS_RESULT_SCREEN_DISPLAYING()
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 2 - IS_RESULT_SCREEN_DISPLAYING = ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(IS_RESULT_SCREEN_DISPLAYING()))
					
					sf_Movie = REQUEST_SCALEFORM_MOVIE("MIDSIZED_MESSAGE")
					WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(sf_Movie))
					WAIT(0)
					ENDWHILE
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 3 - Loaded Scaleform")
					
					BEGIN_SCALEFORM_MOVIE_METHOD(sf_Movie, "SHOW_BRIDGES_KNIVES_PROGRESS")
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 4 - Begin Scaleform Movie Method")
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PW_TITLE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NUMBER_OF_ANIMALS)
					
					//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER_AND_STRING
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PW_PASS")
					
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("PW_CHALLENGE")	
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION())
					
					END_SCALEFORM_MOVIE_METHOD()
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 5 - End Scaleform Movie Method")
					
					i_PrintedCompletion = CHECK_ANIMAL_PHOTOGRAPHS_COMPLETION()
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 6 - Check Animal Photographs Completion: ", i_PrintedCompletion)
					
					g_bCompletedAnimalPhotosCount = i_PrintedCompletion
					
					SETTIMERB(0)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 7 - Set Timer B")
					
					PLAY_SOUND_FRONTEND( -1,"UNDER_THE_BRIDGE", "HUD_AWARDS")
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 8 - Play Sound Frontend")
				ENDIF
				
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 9 - e_ScaleformStage = SSF_DISPLAY_SCALEFORM")
				e_ScaleformStage = SSF_DISPLAY_SCALEFORM
			ENDIF
		BREAK
		
		
		CASE SSF_DISPLAY_SCALEFORM
			IF TIMERB() > DEFAULT_GOD_TEXT_TIME
			OR IS_SCREEN_FADED_OUT()
			OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_RESULT_SCREEN_DISPLAYING()
			OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR IS_REPLAY_BEING_PROCESSED()
			OR IS_MINIGAME_SPLASH_SHOWING() 
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sf_Movie, "SHARD_ANIM_OUT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(HUD_COLOUR_WHITE)) 
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.33)
				END_SCALEFORM_MOVIE_METHOD()
				SETTIMERB(0)
				
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 10 - e_ScaleformStage = SSF_ANIMATE_SCALEFORM")
				e_ScaleformStage = SSF_ANIMATE_SCALEFORM
				
			ELSE
				DRAW_WILDLIFE_PHOTOGRAPHY_SCALEFORM()
				//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 11 - Draw Wildlife Photography Scaleform")
				
				IF g_bCompletedAnimalPhotosCount > i_PrintedCompletion
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 12 - e_ScaleformStage = SSF_CLEANUP_SCALEFORM")
					e_ScaleformStage = SSF_CLEANUP_SCALEFORM
				ENDIF
			ENDIF
		BREAK
		
		
		CASE SSF_ANIMATE_SCALEFORM
			IF TIMERB() > 500
			OR IS_SCREEN_FADED_OUT()
			OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_RESULT_SCREEN_DISPLAYING()
			OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			OR IS_REPLAY_BEING_PROCESSED()
			OR IS_MINIGAME_SPLASH_SHOWING() 
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 13 - e_ScaleformStage = SSF_CLEANUP_SCALEFORM")
				e_ScaleformStage = SSF_CLEANUP_SCALEFORM
				
			ELSE
				DRAW_WILDLIFE_PHOTOGRAPHY_SCALEFORM()
				//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 14 - Draw Wildlife Photography Scaleform")
				
				IF g_bCompletedAnimalPhotosCount > i_PrintedCompletion
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 15 - e_ScaleformStage = SSF_CLEANUP_SCALEFORM")
					e_ScaleformStage = SSF_CLEANUP_SCALEFORM
				ENDIF
			ENDIF
		BREAK
		
		
		CASE SSF_CLEANUP_SCALEFORM
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 16 - Cleanup Scaleform")
			CLEANUP_SCALEFORM()
			
			b_DisplayScaleform = FALSE
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 17 - Set b_DisplayScaleform: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_DisplayScaleform))
			
			e_ScaleformStage = SSF_LOAD_SCALEFORM
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DISPLAY_SCALEFORM() - CHECK 18 - e_ScaleformStage = SSF_LOAD_SCALEFORM")
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Checks if player's cellphone has been removed by another script - Force cleanup
PROC CHECK_FOR_FORCED_SCRIPT_CLEANUP()
	// CHECK FOR CLEAN UP SCRIPT VARIABLES FOLLOWING PHONE FORCED AWAY
	IF HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
	AND NOT b_PhoneForcedAway
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_FORCED_SCRIPT_CLEANUP() - CHECK 1 - Phone Has Been Forced Away")
		
		// SET FORCED AWAY BOOLEAN OTHERWISE WE GET ENDLESS CLEANUP
		b_PhoneForcedAway = TRUE
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_FORCED_SCRIPT_CLEANUP() - CHECK 2 - Set b_PhoneForcedAway: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_PhoneForcedAway))
		
		e_PhotographProcessStage = PPF_CLEAN_UP
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_FORCED_SCRIPT_CLEANUP() - CHECK 3 - Calling Cleaned Up")
	ENDIF
	
	
	// RESET FORCED AWAY BOOLEAN
	IF NOT HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
	AND b_PhoneForcedAway
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_FORCED_SCRIPT_CLEANUP() - CHECK 4 - Phone Forced Away Reset")
		
		b_PhoneForcedAway = FALSE
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_FOR_FORCED_SCRIPT_CLEANUP() - CHECK 5 - Set b_PhoneForcedAway: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_PhoneForcedAway))
	ENDIF
ENDPROC



/// PURPOSE:
///    Checks if player's cellphone should be removed from screen after scaleform screen - Timer based
PROC CHECK_REMOVE_PHONE_TIMER()
	// CHECK REMOVE CELLPHONE AFTER CALLING SCALEFORM SCREEN
	IF GET_TIMER_IN_SECONDS_SAFE(t_PutPhoneAwayTimer) > 3
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_PHONE_TIMER() - CHECK 1 - Removed Phone Timer at: ", GET_TIMER_IN_SECONDS_SAFE(t_PutPhoneAwayTimer))
		
		
		// PUT THE CELLPHONE AWAY
		HANG_UP_AND_PUT_AWAY_PHONE()
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_PHONE_TIMER() - CHECK 2 - Hung Up and Put Away Phone")
		
		
		// RESET TIMER FOR NEXT TIME
		RESTART_TIMER_AT(t_PutPhoneAwayTimer, 0.0)
		PAUSE_TIMER(t_PutPhoneAwayTimer)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_PHONE_TIMER() - CHECK 3 - Reset Remove Phone Timer at: 0.0")
	ENDIF
	
	
	// CHECK CANCEL TIMER
	IF GET_TIMER_IN_SECONDS_SAFE(t_PutPhoneAwayTimer) <= 3
	AND IS_PHONE_ONSCREEN()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		// IF PLAYER MANUALLY BACKS OUT BEFORE TIMER OR IF PHONE FORCED AWAY
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)
		OR (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
		OR HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
		AND NOT b_PhoneForcedAway
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_PHONE_TIMER() - CHECK 5 - Cancelled Phone Timer at: ", GET_TIMER_IN_SECONDS_SAFE(t_PutPhoneAwayTimer))
			
			
			// RESET TIMER FOR NEXT TIME
			RESTART_TIMER_AT(t_PutPhoneAwayTimer, 0.0)
			PAUSE_TIMER(t_PutPhoneAwayTimer)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: CHECK_REMOVE_PHONE_TIMER() - CHECK 6 - Reset Remove Phone Timer at: 0.0")
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE:
///    Main Wildlife Photography control function - Handles all updating of validating photos, sending emails, timers & scaleform
PROC DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPHS()
	SWITCH i_EventState
		CASE 0
			//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 1 - Check For Photographs")
			
			// CHECK IF THE PALYER HAS TAKEN A PHOTOGRAPH OF AN ANIMAL
			CHECK_FOR_ANIMAL_PHOTO()
			
			
			// CHECK CREATE AND SEND EMAIL AFTER DELAY TIMER
			CONSTRUCT_AND_SEND_DYNAMIC_EMAIL()
			
			
			// CHECK IF SHOULD REMOVE CELLPHONE
			CHECK_REMOVE_PHONE_TIMER()
			
			
			// CHECK CELLPHONE HAS BEEN FORCE REMOVED
			CHECK_FOR_FORCED_SCRIPT_CLEANUP()
			
			
			// PRINT THE SCALEFORM SCREEN
			DISPLAY_SCALEFORM()
			
			
		#IF IS_DEBUG_BUILD
			// DRAW DEBUG SPHERES ONE FOCUS PED BONE COORDS
			IF b_DebugDrawDebuguSpheresAtBonetagCoords
				DRAW_DEBUG_SPHERE_AT_BONETAG_COORD()
			ENDIF
		#ENDIF
			
			
			// WAIT FOR ALL WILDLIFE PHOTOS TO BE COLLECTED
			IF IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_ALL_PHOTOGRAPHS_COLLECTED)
			AND NOT b_DisplayScaleform
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 2 - All Photographs Have Been Collected")
				
				i_EventState++
			ENDIF
		BREAK
		
		
		CASE 1
			IF GET_TIMER_IN_SECONDS_SAFE(t_SendEmailDelayTimer) > EMAIL_DELAY_TIME
				// SEND FINAL TEXT ON COMPLETION
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 3 - Check Send Final Email")
				
				IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_END_EMAIL_SENT)
					// SEND FINAL EMAIL
					FIRE_EMAIL_INTO_DYNAMIC_THREAD(DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY, e_MailHelper[NUM_MAX_ANIMALS].e_MailID, TRUE)
					
					
					// PUSH EMAIL TO FEED
					PUSH_FEEDIFICATION_OF_DYNAMIC_THREAD(DYNAMIC_THREAD_WILDLIFE_PHOTOGRAPHY)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 4 - Sending Final Email")
				
				
					// SET FINAL EMAIL SENT BIT
					SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_END_EMAIL_SENT)
					
					
					// ACTIVATE SUBMARINE VEHICLE GEN
					SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WILDPHOTO_SUB, TRUE)
					CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 5 - Setting Submarine Vehicle Gen Active")
					
					
					i_EventState++
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 2
			// CHECK IF THE VEHICLE GENERATOR IS ACTIVE BEFORE SAVING
			IF IS_VEHICLE_GEN_AVAILABLE(VEHGEN_WILDPHOTO_SUB)
				CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 6 - Submersible2 Vehicle Gen is Available")
				i_EventState++
			ENDIF
		BREAK
		
		
		CASE 3
			// SET SCRIPT LAUNCHER CONTROLLER BIT - STOP SCRIPT EVER LAUNCHING AGAIN
			SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 7 - Setting Collect Wildlife Photography Finished")
	
			
			// TRIGGER FINAL AUTOSAVE REQUEST
			MAKE_AUTOSAVE_REQUEST()
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPH() - CHECK 8 - Request Autosave")
		BREAK
	ENDSWITCH
ENDPROC



/// PURPOSE:
///    Checks if conflicting Paparazzo Strangers and Freaks missions are running - B*1965719
/// RETURNS:
///    BOOL: TRUE if Paparazzo3A or Paparazzo3B is currently running
FUNC BOOL IS_CONFLICTING_PAPARAZZO_RC_RUNNING()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Paparazzo3A")) > 0
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: IS_CONFLICTING_PAPARAZZO_RC_RUNNING() - CHECK 1 - Pap3A Running")
		
		RETURN TRUE
	ENDIF
	
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Paparazzo3B")) > 0
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: IS_CONFLICTING_PAPARAZZO_RC_RUNNING() - CHECK 2 - Pap3B Running")
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Initialises all script variables on script startup
PROC INITIALISE_VARIABLES()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 1 - Initialise Variables Started")
	
	// SET INITIAL EMAIL SENT BIT
	IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_START_EMAIL_SENT)
		SET_BIT(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_START_EMAIL_SENT)
	ENDIF
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 2 - Set Bit For Start Email has Sent: BIT_SET_START_EMAIL_SENT")
	
	
	// INITIALISE EVENT TRACKER
	i_EventState = 0
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 3 - Set i_EventState: ", i_EventState)
	
	
	// INITIALISE PHONE FORCED AWAY TRACKER
	b_PhoneForcedAway = HAS_CELLPHONE_JUST_BEEN_FORCED_AWAY()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 4 - Set b_PhoneForcedAway: ", GET_BOOL_DISPLAY_STRING_FROM_BOOL(b_PhoneForcedAway))
	
	
	// INITIALISE EMAIL ENUM NAMES
	e_MailHelper[0].e_MailID	= WILDLIFE_INITIAL
	e_MailHelper[1].e_MailID	= WILDLIFE_01
	e_MailHelper[2].e_MailID	= WILDLIFE_02
	e_MailHelper[3].e_MailID	= WILDLIFE_03
	e_MailHelper[4].e_MailID	= WILDLIFE_04
	e_MailHelper[5].e_MailID	= WILDLIFE_05
	e_MailHelper[6].e_MailID	= WILDLIFE_06
	e_MailHelper[7].e_MailID	= WILDLIFE_07
	e_MailHelper[8].e_MailID	= WILDLIFE_08
	e_MailHelper[9].e_MailID	= WILDLIFE_09
	e_MailHelper[10].e_MailID	= WILDLIFE_10
	e_MailHelper[11].e_MailID	= WILDLIFE_11
	e_MailHelper[12].e_MailID	= WILDLIFE_12
	e_MailHelper[13].e_MailID	= WILDLIFE_13
	e_MailHelper[14].e_MailID	= WILDLIFE_14
	e_MailHelper[15].e_MailID	= WILDLIFE_15
	e_MailHelper[16].e_MailID	= WILDLIFE_16
	e_MailHelper[17].e_MailID	= WILDLIFE_17
	e_MailHelper[18].e_MailID	= WILDLIFE_18
	e_MailHelper[19].e_MailID	= WILDLIFE_19
	e_MailHelper[20].e_MailID	= WILDLIFE_FINAL
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 5 - Initialised Emails Array")
	
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: INITIALISE_VARIABLES() - CHECK 6 - Initialise Variables Finished")
ENDPROC



/// PURPOSE:
///    Main script loop
SCRIPT
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 1 - Start Script")
	
	//======================= FORCE CLEANUP CHECK ====================//
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_MAGDEMO | FORCE_CLEANUP_FLAG_DIRECTOR)
		CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 2 - Force Cleanup Occurred")
		TERMINATE_THIS_THREAD()	
	ENDIF
	
	
	//======================= INITIALISE ====================//
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 3 - Initialise Variables Started")
	INITIALISE_VARIABLES()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 4 - Initialise Variables Finished")
	
	
#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 5 - Initialise Debug Started")
	INIT_DEBUG()
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 6 - Initialise Debug Finished")
#ENDIF
	
	
	//======================= MAIN LOOP ====================//
	CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 7 - Entering MAIN Loop")
	WHILE TRUE
		WAIT(0)
		//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 8 - MAIN Loop Active")
		
		
		// MAKE SURE PLAYER IS A LASTGEN PLAYER - OTHERWISE TERMINATE SCRIPT
		IF NOT IS_LAST_GEN_PLAYER()
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 9 - Terminating Script. Player isn't LastGen")
			
			TERMINATE_THIS_THREAD()
		ENDIF
		
		IF IS_REPEAT_PLAY_ACTIVE()
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 10 - Terminating Script. Repeat Play Active")
			//SCRIPT_ASSERT("Wildlife Photography: SCRIPT LOOP - CHECK 10 - Terminating Script. Repeat Play Active")
			
			TERMINATE_THIS_THREAD()
		ENDIF
		
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 11 - Terminating Script. IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)")		
			TERMINATE_THIS_THREAD()
		ENDIF
		
		//======================= MAIN SCRIPT FUNCTIONALITY METHOD ====================//
		IF NOT IS_BIT_SET(g_SavedGlobals.sAmbient.iWildlifePhotographsFlags, BIT_SET_COLLECT_WILDLIFE_PHOTOGRAPHS_FINISHED)
		AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		AND NOT IS_CONFLICTING_PAPARAZZO_RC_RUNNING()
			DO_COLLECTIBLES_WILDLIFE_PHOTOGRAPHS()
			
		ELSE
			//======================= TERMINATION ====================//
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 11 - Terminating Script Started")
			
		#IF IS_DEBUG_BUILD
			DESTROY_WIDGETS()
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 12 - Destroyed Debug Widgets")
		#ENDIF
			
			CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 13 - Terminating Script Finished")
			TERMINATE_THIS_THREAD()
		ENDIF
		
		
		//======================= DEBUG UPDATES ====================//
	#IF IS_DEBUG_BUILD
		//CPRINTLN(DEBUG_COLLECTIBLES_PHOTOGRAPHY_WILDLIFE, "Wildlife Photography: SCRIPT LOOP - CHECK 14 - Debug Update")
		DEBUG_UPDATE()
	#ENDIF	
	
	ENDWHILE
	
 ENDSCRIPT
