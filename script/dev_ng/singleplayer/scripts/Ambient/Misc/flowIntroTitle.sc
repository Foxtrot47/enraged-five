//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 15/07/13			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								Flow Intro Title								│
//│																				│
//│		Displays the GTA V title logo during the transition between the			│
//│		Prologue and Armenian1 missions.										│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "globals.sch"
USING "rage_builtins.sch"
USING "commands_hud.sch"
USING "commands_misc.sch"
USING "commands_cutscene.sch"
USING "commands_camera.sch"
USING "commands_recording.sch"
USING "savegame_public.sch"
USING "script_misc.sch"


CONST_INT		INTRO_TITLE_DISPLAY_TIME			8000	//Milliseconds
CONST_FLOAT		INTRO_TITLE_FADE_TIME				2.00	//Seconds


SCALEFORM_INDEX sfTitle

INT iTitleTimer
BOOL bFadingOut = FALSE


//╒═════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ Base  ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════╛

PROC CLEANUP_SCRIPT()
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Cleaning up.")
	
	//Must be set as we clean up as it blocks Armenian1 and this
	//script might not get to run again.
	SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
	
	IF sfTitle != NULL
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfTitle)
	ENDIF
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_NO_LOADING_SCREEN(FALSE)
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	TERMINATE_THIS_THREAD()
ENDPROC


PROC UPDATE_SCREEN_STATE()
	HIDE_LOADING_ON_FADE_THIS_FRAME()
	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
	REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
ENDPROC


//╒═════════════════════════════════════════════════════════════════════╕
//╞═════════════════════╡ Scaleform Interface  ╞════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════╛

PROC SCALEFORM_SETUP_SINGLE_LINE(STRING strName, FLOAT fFadeDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "SETUP_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_DISPLAY_SINGLE_LINE(STRING strName, STRING strSingleLine, STRING strFont, STRING strHudColour, BOOL bUseLiteralString = TRUE)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "ADD_TEXT_TO_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strSingleLine)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strFont)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strHudColour)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bUseLiteralString)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_SINGLE_LINE(STRING strName)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "SHOW_SINGLE_LINE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_SINGLE_LINE(STRING strName)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "HIDE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_REMOVE_ALL_SINGLE_LINES()
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "REMOVE_ALL")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_SHOW_LOGO(STRING strName, FLOAT fFadeInDuration, FLOAT fFadeOutDuration, FLOAT fLogoFadeInDuration, FLOAT fLogoFadeOutDuration, 
						 FLOAT fLogoFadeInDelay, FLOAT fLogoFadeOutDelay, FLOAT fLogoScaleDuration)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "SHOW_LOGO")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fFadeOutDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeInDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeOutDuration)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeInDelay)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoFadeOutDelay)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLogoScaleDuration)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SCALEFORM_HIDE_LOGO(STRING strName)
	BEGIN_SCALEFORM_MOVIE_METHOD(sfTitle, "HIDE_LOGO")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strName)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


//╒═════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ Main ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════╛

SCRIPT
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> GTAV title script started.")

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> GTAV title script forced to clean up.")
		CLEANUP_SCRIPT()
	ENDIF
	
	SET_GAME_PAUSES_FOR_STREAMING(FALSE)
	
	CLEAR_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
	UPDATE_SCREEN_STATE()
	
	#IF IS_DEBUG_BUILD
		IF g_flowUnsaved.bUpdatingGameflow
			CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Flow launch in progress. Skipping title screen.")
			SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
			CLEANUP_SCRIPT()
		ENDIF
	#ENDIF
	
	SET_NO_LOADING_SCREEN(TRUE)
	
	//Ensure screen is faded out.
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	//Load scaleform.
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Starting scaleform loading...")
	sfTitle = REQUEST_SCALEFORM_MOVIE("OPENING_CREDITS")
	WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(sfTitle)
		UPDATE_SCREEN_STATE()
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Scaleform loaded.")
	
	WHILE NOT IS_SCREEN_FADED_OUT()
		UPDATE_SCREEN_STATE()
		WAIT(0)
	ENDWHILE
	
	//Setup title scaleform.
	SCALEFORM_SHOW_LOGO("TITLE", 0.0, 1.0, 1.5, 1.0, 0.0, 1.5, 15.0)
	
	//Main loop. Draw title credit and time the end.
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Fading title in.")
	iTitleTimer = GET_GAME_TIMER() + INTRO_TITLE_DISPLAY_TIME + ROUND(INTRO_TITLE_FADE_TIME * 1000)
	
	// Waits for the Arm1 intro cutscene to start before script terminates. Ensures we 
	// block the loading spinner until the next cutscene to keep the title sequence clean.
	WHILE NOT IS_CUTSCENE_PLAYING() 
		UPDATE_SCREEN_STATE()

		IF NOT IS_BIT_SET(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfTitle, 255, 255, 255, 255)
		
			IF NOT bFadingOut
				// Start the title fading out.
				IF GET_GAME_TIMER() > iTitleTimer
					CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Fading title out.")
					SCALEFORM_HIDE_LOGO("TITLE")
					bFadingOut = TRUE
					iTitleTimer = GET_GAME_TIMER() + ROUND(INTRO_TITLE_FADE_TIME * 1000)
				ENDIF
			ELSE
				// Fade out ended. Flag that the title is finished.
				IF GET_GAME_TIMER() > iTitleTimer
					CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Flagging that the title has finished.")
					SET_BIT(g_savedGlobals.sFlowCustom.spInitBitset, SP_INIT_TITLE_SEQUENCE_DISPLAYED)
					
					CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> HAS_GAME_INSTALLED_THIS_SESSION() = ",HAS_GAME_INSTALLED_THIS_SESSION())
					
#IF FEATURE_GEN9_STANDALONE
					CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Running intro title autosave for standalone.")
					SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE,TRUE)
					MAKE_AUTOSAVE_REQUEST()
#ENDIF				
#IF NOT FEATURE_GEN9_STANDALONE
					IF HAS_GAME_INSTALLED_THIS_SESSION()
					OR IS_PC_VERSION()
					#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("SimulateInstallComplete") #ENDIF
						CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Running intro title autosave.")
						SET_AUTOSAVE_IGNORES_ON_MISSION_FLAG(TRUE,TRUE)
						MAKE_AUTOSAVE_REQUEST()
					ELSE
						CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Clearing autosaves after intro title.")
						CLEAR_AUTOSAVE_REQUESTS()
					ENDIF
#ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_FLOW, "<GTAV-TITLE> Ending due to cutsecne start.")

	CLEANUP_SCRIPT()
ENDSCRIPT
