USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_streaming.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "script_drawing.sch"
USING "lineActivation.sch"
USING "drunk_public.sch"
USING "dialogue_public.sch"
USING "player_ped_public.sch"
USING "script_blips.sch"
USING "clubs_public.sch"
USING "minigame_UIInputs.sch"
USING "friendActivity_public.sch"
USING "cutscene_public.sch"
USING "rgeneral_include.sch"
USING "building_control_public.sch"
USING "flow_processing_game.sch"
USING "transition_common.sch"
USING "freemode_header.sch"
//USING "context_control_public.sch"

CONST_INT MP_STRIPCLUB 0
CONST_INT USE_STRIPCLUB_DEBUG 0
//USING "stripclub_public.sch"
//USING "stripclub.sch"
//USING "stripclub_common.sch"
//USING "stripclub_anim.sch"
//USING "stripclub_ai.sch"
//USING "stripclub_task.sch"
//USING "stripclub_helpers.sch"
//USING "stripclub_stage.sch"
//USING "stripclub_cutscenes.sch"
//USING "stripclub_hotspots.sch"
//USING "stripclub_workers.sch"
//USING "stripclub_lapdance.sch"
//USING "stripclub_friend.sch"
USING "stripclub_setup.sch"

INT iDrinkWaitTime
INT iDrinkScene = -1
BOOL bControlsBartender = TRUE
BOOL bIsMp = FALSE
BOOL bIncDrink = FALSE
MP_PROP_OFFSET_STRUCT whiskeyBarLocateA
MP_PROP_OFFSET_STRUCT whiskeyBarLocateB

STRUCT DRINKING_BROADCAST_DATA
	BOOL bUsingBartender
	BOOL bBartenderSceneStarted	
ENDSTRUCT

DRINKING_BROADCAST_DATA playerDrinkingBD[MP_STRIPCLUB_MAX_PLAYERS]

SCRIPT_TIMER stYachtBarStaffIdleSpeech
SCRIPT_TIMER stYachtBarStaffFirghtenedPreventSpeech
SCRIPT_TIMER stYachtBarStaffFirghtenedSpeech
BOOL bHasBumpedBarStaffSpeech
BOOL bBartenderSaidHi					= FALSE
BOOL bBartenderSaidBye					= FALSE
BOOL bHasFirghtenedBarStaffSpeech		= FALSE
BOOL bDelaySpeechAfterBarStaffShocked 	= FALSE
BOOL bDelaySyncSceneEnd 				= FALSE

CONST_INT ciPedsNearBartenderSize 8

PED_INDEX aNearbyPeds[ciPedsNearBartenderSize]
INT iASYNCSuicideCheck					= 0
INT iNumPedsNearBartender				= 0
SCRIPT_TIMER timerDelayedSyncScene

//SPEECH for yacht bartender
FUNC BOOL DO_BARSTAFF_ALIVE_CHECK(DRINKING_DATA drinkData, PED_INDEX &ped)
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
		ped = drinkData.pedBartender
		IF DOES_ENTITY_EXIST(ped)
			PRINTLN("DO_BARSTAFF_ALIVE_CHECK returning TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTLN("DO_BARSTAFF_ALIVE_CHECK returning FALSE")
	RETURN FALSE
ENDFUNC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_ENTRY(DRINKING_DATA drinkData)
	
	PED_INDEX pedBarStaff
	IF bBartenderSaidHi
	OR NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)	
		EXIT
	ENDIF

	BOOL IsPlayerFemale = IS_PLAYER_FEMALE()

	STRING Context, Params

	IF GET_RANDOM_INT_IN_RANGE(0, 10) > 6
		Context = "GENERIC_HI"
	ELSE
		IF IsPlayerFemale
			Context = "GENERIC_HI_FEMALE"
		ELSE
			Context = "GENERIC_HI_MALE"
		ENDIF
	ENDIF
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_ENTRY - my bar tender played ")
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
	
	bBartenderSaidHi 	= TRUE
	bBartenderSaidBye	= FALSE

ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_GREET(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
		EXIT
	ENDIF
	
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_GREET - played ")
	STRING Context, Params
	Context = "BARTENDER_GREET"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_IDLE(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF
	
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_IDLE - played ")
	STRING Context, Params
	Context = "BARSTAFF_IDLE"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BYE(DRINKING_DATA drinkData)
	
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
		EXIT
	ENDIF
	
	STRING Context, Params
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) = GET_YACHT_PLAYER_IS_ON(PLAYER_ID())
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BYE - my BARSTAFF played ")
		Context = "GENERIC_BYE"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	ELSE
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BYE - not my BARSTAFF played ")
		Context = "GENERIC_BYE"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)
	
	ENDIF
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)

ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_CHAT(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF
	
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_CHAT - my BARSTAFF played ")
	Context = "BARTENDER_CHAT"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_SERVE(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF

	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_SERVE - my captain played ")
	Context = "BARTENDER_SERVE"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_DRUNK(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF

	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_DRUNK - my captain played ")
	Context = "PLAYER_DRUNK"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_GENERIC_THANKS(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF
	
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_GENERIC_THANKS - my captain played ")
	Context = "GENERIC_THANKS"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_BUMP(DRINKING_DATA drinkData)
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR bDelaySpeechAfterBarStaffShocked
		EXIT
	ENDIF
	
	STRING Context, Params
	IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedBarStaff)
	AND bHasBumpedBarStaffSpeech = FALSE
		PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_BUMP - my captain played ")
		Context = "BUMP"
		Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
		bHasBumpedBarStaffSpeech = TRUE
	ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedBarStaff) >= 2.0
		 bHasBumpedBarStaffSpeech = FALSE
	ENDIF
ENDPROC

PROC PLAY_YACHT_SERVING_DIALOGUE(INT iDrunkLevel, DRINKING_DATA drinkData)
	IF iDrunkLevel = 0
		TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_GREET(drinkData)			
	ELIF iDrunkLevel = 1
		TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARTENDER_SERVE(drinkData)
	ELIF iDrunkLevel >= 2
		TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_DRUNK(drinkData)
	ENDIF
ENDPROC

PROC RUN_YACHT_BARTENDER_IDLE_CHAT(DRINKING_DATA drinkData)
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtBarStaffIdleSpeech, 15000)
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_IDLE(drinkData)
		ELSE
			TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_CHAT(drinkData)
		ENDIF
	ENDIF	
ENDPROC

PROC RUN_HELLO_GBYE_CHECKS_YACHT(DRINKING_DATA drinkData)
	TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_BUMP(drinkData)
	IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())!= GET_HASH_KEY("GTaMloRoom_bar1")
		PRINTLN("stripclub_drinking: Player left room clearing goodbye and hello flags")
		bBartenderSaidHi	= FALSE
		bBartenderSaidBye	= FALSE
	ELSE			
		IF bBartenderSaidHi
		AND NOT bBartenderSaidBye
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), drinkData.pedBartender) > 7.5
				IF GET_RANDOM_INT_IN_RANGE(0, 10) > 3
					TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BYE(drinkData)
				ELSE
					TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_BARSTAFF_GENERIC_THANKS(drinkData)
				ENDIF
				bBartenderSaidBye = TRUE
			ENDIF
		ENDIF			
	ENDIF
ENDPROC

PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH(PED_INDEX pedBarStaff)
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH - my captain played ")
	Context = "GENERIC_FRIGHTENED_HIGH"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED(PED_INDEX pedBarStaff)
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED - my captain played ")
	Context = "GENERIC_FRIGHTENED_MED"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH(PED_INDEX pedBarStaff)
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH - my captain played ")
	Context = "GENERIC_SHOCKED_HIGH"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC
PROC TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED(PED_INDEX pedBarStaff)
	STRING Context, Params
	PRINTLN("[YACHTCONT] TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED - my captain played ")
	Context = "GENERIC_SHOCKED_MED"
	Params = AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE)	
	PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedBarStaff, Context, "WFStewardess", Params, FALSE)
ENDPROC

FUNC BOOL RUN_SUICIDE_CHECKS(PED_INDEX pedBarStaff)
	
	// Access nearby peds every time we restart the staggered loop
	IF iASYNCSuicideCheck = 0
		iNumPedsNearBartender = GET_PED_NEARBY_PEDS(pedBarStaff, aNearbyPeds) 
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "stripclub_drinking RUN_SUICIDE_CHECKS: iNumPedsNearBartender: ", iNumPedsNearBartender)

	IF DOES_ENTITY_EXIST(aNearbyPeds[iASYNCSuicideCheck])
	AND NOT IS_ENTITY_DEAD(aNearbyPeds[iASYNCSuicideCheck])
		// Verify ped is in the bar and playing the kill self anim		
		IF GET_ROOM_KEY_FROM_ENTITY(aNearbyPeds[iASYNCSuicideCheck]) = GET_HASH_KEY("GTaMloRoom_bar1")
		AND IS_ENTITY_PLAYING_ANIM(aNearbyPeds[iASYNCSuicideCheck], "MP_SUICIDE", "PILL")
			// Reset the loop
			iASYNCSuicideCheck = 0
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Increment array index
	iASYNCSuicideCheck++
		
	// Reset array index if we've looped through all nearby peds
	// OR we reach end of array.
	IF iASYNCSuicideCheck >= iNumPedsNearBartender
	OR iASYNCSuicideCheck >= ciPedsNearBartenderSize
		iASYNCSuicideCheck = 0 // Reset loop
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RUN_BTENDER_FRIGHTENED_DIALOGUE(DRINKING_DATA drinkData)	
	PED_INDEX pedBarStaff
	IF NOT DO_BARSTAFF_ALIVE_CHECK(drinkData, pedBarStaff)
	OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != GET_HASH_KEY("GTaMloRoom_bar1")
		EXIT
	ENDIF
	
	IF bDelaySpeechAfterBarStaffShocked
	AND HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtBarStaffFirghtenedPreventSpeech, 10000)
		CDEBUG1LN(DEBUG_AMBIENT, "RUN_BARSTAFF_FRIGHTENED_DIALOGUE: Delay Speech Timer expired")
		bDelaySpeechAfterBarStaffShocked = FALSE
	ENDIF
	
	IF bHasFirghtenedBarStaffSpeech
	AND NOT HAS_NET_TIMER_EXPIRED_ONE_FRAME(stYachtBarStaffFirghtenedSpeech, 15000)
		EXIT
	ENDIF
	
	bHasFirghtenedBarStaffSpeech = FALSE
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedBarStaff), 8.0)
	OR RUN_SUICIDE_CHECKS(pedBarStaff)
		STOP_CURRENT_PLAYING_SPEECH(pedBarStaff)
		INT iRandomNo = GET_RANDOM_INT_IN_RANGE(1, 5)
		SWITCH iRandomNo
			CASE 1	TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_HIGH(pedBarStaff) 	BREAK
			CASE 2	TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_FRIGHTENED_MED(pedBarStaff) 	BREAK
			CASE 3	TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_HIGH(pedBarStaff)		BREAK
			CASE 4	TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_CAPTAIN_GENERIC_SHOCKED_MED(pedBarStaff)		BREAK
		ENDSWITCH
		bHasFirghtenedBarStaffSpeech 		= TRUE
		bDelaySpeechAfterBarStaffShocked 	= TRUE
		REINIT_NET_TIMER(stYachtBarStaffFirghtenedSpeech)
	ENDIF
ENDPROC

PROC PRINT_PLAYERS_USING_BAR()
	IF NOT bIsMp
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
			IF playerDrinkingBD[index].bUsingBartender
				CPRINTLN(DEBUG_SCLUB,"Participant ", index, " is using bar")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_ANY_PLAYER_USING_BAR()
	
	IF NOT bIsMp
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
			IF PARTICIPANT_ID_TO_INT() != index
				IF playerDrinkingBD[index].bUsingBartender
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANOTHER_PLAYER_WANT_TO_USE_BAR()
	IF NOT bIsMp
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
			IF playerDrinkingBD[index].bUsingBartender
				IF index = PARTICIPANT_ID_TO_INT()
					RETURN FALSE //in case two players proposition the bartender at the same time, player with lower particpant ID wins
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ANOTHER_PLAYER_STARTED_THE_BARTEND_SCENE()
	
	IF NOT bIsMp
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	INT index
	
	REPEAT MP_STRIPCLUB_MAX_PLAYERS index
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(index))
			IF PARTICIPANT_ID_TO_INT() != index
				IF playerDrinkingBD[index].bBartenderSceneStarted
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SET_DRINKING_DEBUG_VALUES()
	
	#IF IS_DEBUG_BUILD
	#IF USE_STRIPCLUB_DEBUG
		//fLikeMeterPosX = LIKE_BOX_CENTERX
		//fLikeMeterPosY = LIKE_BOX_TOPY
		//fLikeMeterHeight = LIKE_BOX_HEIGHT
		//fLikeMeterWidth = LIKE_BOX_WIDTH
		iDebugDisplayStriperWanderState = iDebugDisplayStriperWanderState
			stripClubWidgets = START_WIDGET_GROUP("Drinking")
				ADD_WIDGET_STRING("Debug Pos")
				ADD_WIDGET_FLOAT_SLIDER("X",vStripDebugVector.x, -2000, 2000, 0.001 ) //<<121.02, -1281.54, 28.48>>
				ADD_WIDGET_FLOAT_SLIDER("Y",vStripDebugVector.y, -2000, 2000, 0.001 )
				ADD_WIDGET_FLOAT_SLIDER("Z",vStripDebugVector.z, -2000, 2000, 0.001 )
				ADD_WIDGET_FLOAT_SLIDER("Heading",fDebugHeading, -360.0,360.0,0.01 ) 
				ADD_WIDGET_FLOAT_SLIDER("fDebugFloat",fDebugFloat, -360.0,360.0,0.01 )
				ADD_WIDGET_INT_SLIDER("Int", iDebugInt, 0, 5, 1)
			STOP_WIDGET_GROUP()
		
		DEBUG_MESSAGE("Strip Club Values Inited")
	#ENDIF
	#ENDIF
ENDPROC

FUNC BOOL DID_PLAYER_TAKE_DRINK(STRIP_CLUB_BAR_ANIM_ENUM eDrinkAnimEnum)
	
	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp))
		RETURN TRUE
	ENDIF
	
	FLOAT fDrinkTime = 0.7
	
	IF eDrinkAnimEnum = SCBA_DRUNK1
		fDrinkTime = 0.57
	ELIF eDrinkAnimEnum = SCBA_DRUNK2
		fDrinkTime = 0.57
	ELIF eDrinkAnimEnum = SCBA_DRUNK3
		fDrinkTime = 0.7
	ELIF eDrinkAnimEnum = SCBA_DRUNK4
		fDrinkTime = 0.64
	ENDIF
	
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp)) > fDrinkTime
ENDFUNC

FUNC BOOL IS_BAR_SCENE_OVER()
	
	TEXT_LABEL_15 sRoot
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp))
		CPRINTLN(DEBUG_SCLUB, "Drink Scene time ", GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp)))
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp)) > 0.31
		AND GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp)) < 0.32
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF IS_PLAYER_CUT_OFF_FROM_DRINKING(-1)
			AND NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
				 sRoot = "SC_LAS_DRINK"
				 CPRINTLN(DEBUG_SCLUB,"Playing last drink convo ", sRoot)
				
				CREATE_CONVERSATION(stripClubConversation, "SCAUD", sRoot, GET_STRIPCLUB_SPEECH_PRIORITY())
			ENDIF
		ENDIF
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp)) >= 1.0
			CPRINTLN(DEBUG_AMBIENT, "Drink scene finished returning true")
			RETURN TRUE
		ENDIF
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "Drink scene not runing returning true")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL REMOVE_BARMAN(DRINKING_DATA &drinkData)
	IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(drinkData.niPedBartender)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(drinkData.niPedBartender)
				DELETE_NET_ID(drinkData.niPedBartender)
				RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS()- 1)
				CDEBUG1LN(DEBUG_SCLUB, "REMOVE_BARMAN: Deleted Barman")
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SCLUB, "REMOVE_BARMAN: Tried to remove barman but player didn't have control of entity")
				NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(drinkData.niPedBartender)
				RETURN FALSE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SCLUB, "REMOVE_BARMAN: Tried to remove barman but entity didn't exist")
		ENDIF
	ENDIF
	CDEBUG1LN(DEBUG_SCLUB, "REMOVE_BARMAN: returning true")
	RETURN TRUE
ENDFUNC

PROC CLEANUP_STRIPCLUB_DRINKING(DRINKING_DATA &drinkData)
	CPRINTLN(DEBUG_SCLUB,"CLEANUP_STRIPCLUB_DRINKING")
	DEBUG_PRINTCALLSTACK()
	
	IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_DRINKING)
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(GET_BAR_ANIM_ENUM_FROM_INDEX(0)))
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(GET_BAR_ANIM_ENUM_FROM_INDEX(1)))
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(GET_BAR_ANIM_ENUM_FROM_INDEX(2)))
		REMOVE_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(GET_BAR_ANIM_ENUM_FROM_INDEX(3)))
		CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_DRINKING)	
	ENDIF
	
	IF DOES_CAM_EXIST(stripCamAnim)
		DESTROY_CAM(stripCamAnim)
	ENDIF
	
	IF DOES_ENTITY_EXIST(drinkData.objGlass)
		IF IS_ENTITY_ATTACHED(drinkData.objGlass)
			DETACH_ENTITY(drinkData.objGlass)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(drinkData.objBottle)
		IF IS_ENTITY_ATTACHED(drinkData.objBottle)
			DETACH_ENTITY(drinkData.objBottle)
		ENDIF
	ENDIF
	
	STRIPCLUB_END_SYNC_SCENE(iDrinkScene, bIsMp)
	g_bUsingBarmaidActivity = FALSE
	ENABLE_INTERACTION_MENU()
	SET_STRIPCLUB_USING_BAR(FALSE)
	barStage = BAR_NOT_ACTIVE
	CLEAR_ALL_STRIP_CLUB_HELP()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC GET_CONTROL_OF_BARTENDER(DRINKING_DATA &drinkData)
	bControlsBartender = TRUE
	IF bIsMp
		IF NOT IS_PED_INJURED(drinkData.pedBartender)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(drinkData.pedBartender)
				CPRINTLN(DEBUG_SCLUB,"Get control of entity")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(drinkData.pedBartender)
				bControlsBartender = FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(drinkData.objGlass)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(drinkData.objGlass)
				CPRINTLN(DEBUG_SCLUB,"Get control of entity")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(drinkData.objGlass)
				bControlsBartender = FALSE
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(drinkData.objBottle)
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(drinkData.objBottle)
				CPRINTLN(DEBUG_SCLUB,"Get control of entity")
				NETWORK_REQUEST_CONTROL_OF_ENTITY(drinkData.objBottle)
				bControlsBartender = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_DRINKING_FACE_ANIMS(STRIP_CLUB_BAR_ANIM_ENUM eDrinkAnimEnum, TEXT_LABEL_31 &txtBartenderFace, TEXT_LABEL_31 &txtPlayerFace)
	txtBartenderFace = ""
	txtPlayerFace = ""
	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		txtPlayerFace = "TREVOR_FACIAL"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		txtPlayerFace = "MICHAEL_FACIAL"
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		txtPlayerFace = "FRANKLIN_FACIAL"
	ENDIF
	
	IF eDrinkAnimEnum = SCBA_DRUNK1
		txtBartenderFace = "one_BARTENDER_facial"	
	ELIF eDrinkAnimEnum = SCBA_DRUNK2
		txtBartenderFace = "two_BARTENDER_facial"	
	ELIF eDrinkAnimEnum = SCBA_DRUNK3
		txtBartenderFace = "three_BARTENDER_facial"
	ELIF eDrinkAnimEnum = SCBA_DRUNK4
		txtBartenderFace = "four_BARTENDER_facial"
	ENDIF

ENDPROC

FUNC INT GET_DRUNK_LINE(INT iDrunkness)
	RETURN iDrunkness
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_BAR()
	IF bIsMp
		IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			RETURN FALSE
		ENDIF
		
		IF GET_GLOBAL_HAS_LOCAL_PLAYER_OPTED_IN_TO_BE_BEAST()
			RETURN FALSE
		ENDIF
		
		IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN TRUE
ENDFUNC

SCRIPT(DRINKING_DATA drinkData)
	
	barStage = BAR_NOT_ACTIVE
	bIsMp = drinkData.iIsMP != 0
	
	TEXT_LABEL_31 txtBartenderFace
	TEXT_LABEL_31 txtPlayerFace
	
	FLOAT iUseBartenderDistance = (STRIP_CLUB_BAR_APPROACH_DISTANCE-0.25)
	
	IF bIsMp
		//In mp use the tunables for price
		DANCE_COST = g_sMPTunables.istripbar_dance_price_modifier
		SC_DRINK_COST = g_sMPTunables.istripbar_shots_at_bar_price_modifier 
		IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			SC_DRINK_COST = 0
			CPRINTLN(DEBUG_SCLUB,"Setting cost of drink to zero because player is in yacht")
		ENDIF
		
		iUseBartenderDistance = (STRIP_CLUB_BAR_APPROACH_DISTANCE-0.5)
		
		IF drinkData.iYachtInstance > -1
			
			GET_POSITION_AS_OFFSET_FOR_YACHT(drinkData.iYachtInstance, MP_PROP_ELEMENT_WHISKY_BAR_LOCATE_A, whiskeyBarLocateA)
			GET_POSITION_AS_OFFSET_FOR_YACHT(drinkData.iYachtInstance, MP_PROP_ELEMENT_WHISKY_BAR_LOCATE_B, whiskeyBarLocateB)
			CPRINTLN(DEBUG_SCLUB,"whiskeyBarLocateA.vloc = ", whiskeyBarLocateA.vLoc, ", whiskeyBarLocateB.vLoc = ", whiskeyBarLocateB.vLoc)
		ELSE
			CPRINTLN(DEBUG_SCLUB,"LOCAL_PLAYER_PRIVATE_YACHT_ID = -1, not getting offset for locate")
		ENDIF
	
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_PLAYER_KILLED_OR_ARRESTED | FORCE_CLEANUP_FLAG_MAGDEMO)
			CPRINTLN(DEBUG_SCLUB, "Force cleanup strip club drinking")
			barStage = BAR_CLEANUP
	    ENDIF
		// This marks the script as a net script, and handles any instancing setup. 
		IF drinkData.bInsideYacht = FALSE
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MP_STRIPCLUB_MAX_PLAYERS, FALSE)
		ELSE
			NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(MP_STRIPCLUB_MAX_PLAYERS, FALSE, drinkData.iYachtInstance)
		ENDIF
		
		// This makes sure the net script is active, waits untull it is.
		HANDLE_NET_SCRIPT_INITIALISATION()
		RESERVE_NETWORK_MISSION_PEDS(1)
		RESERVE_NETWORK_MISSION_OBJECTS(2)
		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerDrinkingBD, SIZE_OF(playerDrinkingBD))
	ELSE
	
		IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_MAGDEMO)
			CPRINTLN(DEBUG_SCLUB, "Force cleanup strip club drinking")
			barStage = BAR_CLEANUP
	    ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(drinkData.pedBartender)
		IF NOT IS_ENTITY_A_MISSION_ENTITY(drinkData.pedBartender)
			CPRINTLN(DEBUG_SCLUB,"Gain control of bartener")
			SET_ENTITY_AS_MISSION_ENTITY(drinkData.pedBartender)
		ENDIF
		
		ADD_PED_FOR_DIALOGUE(stripClubConversation, ENUM_TO_INT(SPEAKER_BARMAID), drinkData.pedBartender, "SClubBarmaid")
	ENDIF
	
	SET_STIRP_CLUB_ADD_PED_FOR_DIALOGUE()
	CPRINTLN(DEBUG_SCLUB,"Start driniking script")
	SET_DRINKING_DEBUG_VALUES()
	STRIP_CLUB_BAR_ANIM_ENUM eDrinkAnimEnum
	//get animation name based on how many drinks are in you system, max out at 3
	MP_PROP_OFFSET_STRUCT yachtWhiskeyBartender	
	
	MP_PROP_OFFSET_STRUCT whiskeyBarScene
	VECTOR tempPosition 
	
	WHILE TRUE
	
	
//		CPRINTLN(DEBUG_SCLUB,"Alcohol ", GET_STRIPCLUB_AMOUNT_OF_ALCOHOL())
		CDEBUG2LN(DEBUG_SCLUB,VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()) , <<127.1629, -1283.5865, 29.2786>>))
		IF NATIVE_TO_INT(PLAYER_ID()) > -1
			IF NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
				IF IS_PLAYER_CUT_OFF_FROM_DRINKING(0)
				AND barStage = BAR_NOT_ACTIVE
					CPRINTLN(DEBUG_SCLUB,"player is too drunk, go to BAR_TOO_DRUNK")
					barStage = BAR_TOO_DRUNK
					//drank to much, end script
					//CLEANUP_STRIPCLUB_DRINKING()
				ENDIF
			ELSE
				
				GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_PLAYER_IS_ON(PLAYER_ID()), MP_PROP_ELEMENT_WHISKY_BARTENDER, yachtWhiskeyBartender)
			ENDIF
		ENDIF
		
		MONITOR_STRIPCLUB_CLOTHS_CHANGE()
		
		IF IS_STRIPCLUB_GLOBAL_HOSTILE_FLAG_SET()
		OR IS_ENTITY_DEAD(PLAYER_PED_ID()) OR IS_ENTITY_DEAD(drinkData.pedBartender)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp))
				SET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp), 1.0)
				IF NOT IS_PED_INJURED(drinkData.pedBartender)
					CLEAR_PED_TASKS_IMMEDIATELY(drinkData.pedBartender)
				ENDIF
			ENDIF
			
			IF barStage = BAR_NOT_ACTIVE
				CPRINTLN(DEBUG_SCLUB,"CLEANUP_STRIPCLUB_DRINKING: ped or bartender dead")
				barStage = BAR_CLEANUP
			ENDIF
		ENDIF
		
		IF bIsMP
			IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
				CPRINTLN(DEBUG_SCLUB, "Drinking script should terminate")
				IF IS_SKYCAM_PAST_FIRST_CUT()
					barStage = BAR_CLEANUP
				ENDIF
				
				CLEANUP_STRIPCLUB_DRINKING(drinkData)
				
			ENDIF
		ENDIF
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		
		IF barStage = BAR_INIT OR barStage = BAR_GETTING_DRINK
			IF STRIP_CLUB_IS_PLAYER_TREVOR() AND DOES_TREVOR_OWN_CLUB()
			OR IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
				// If trevor owns the club, he should not get cut off. He will just loop without hitting the
				// "I'm cutting you off" anim from the bartender
				eDrinkAnimEnum = GET_BAR_ANIM_ENUM_FOR_TREVOR(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL())
				CPRINTLN(DEBUG_SCLUB, "SCRIPT: Owner Trevor is using drink anim: ", ENUM_TO_INT(eDrinkAnimEnum))
			ELSE
				eDrinkAnimEnum = GET_BAR_ANIM_ENUM_FROM_INDEX(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL())
				CPRINTLN(DEBUG_SCLUB, "SCRIPT: Non-Owner is using drink anim: ", ENUM_TO_INT(eDrinkAnimEnum))
			ENDIF
		ENDIF
		
		IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
			RUN_HELLO_GBYE_CHECKS_YACHT(drinkData)
			RUN_BTENDER_FRIGHTENED_DIALOGUE(drinkData)
		ENDIF
		
		//PRINT_PLAYERS_USING_BAR()
		CDEBUG2LN(DEBUG_SCLUB,"barStage: ", barStage)
		SWITCH barStage
		
			CASE BAR_NOT_ACTIVE
			
				BOOL bJustCleanedUpSyncScene
				bJustCleanedUpSyncScene = FALSE
			
				IF bDelaySyncSceneEnd
				AND IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
					IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(timerDelayedSyncScene, 450)
						PRINTLN("STRIPCLUB_DRINKING: Ending sync scene delayed")
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						STRIPCLUB_END_SYNC_SCENE(iDrinkScene, bIsMp)
						bDelaySyncSceneEnd = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "BAR_NOT_ACTIVE: g_bUsingBarmaidActivity = FALSE")
						g_bUsingBarmaidActivity = FALSE
						bJustCleanedUpSyncScene = TRUE
					ENDIF
				ENDIF
				
				IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					IF NOT IS_PED_INJURED(drinkData.pedBartender)
						IF NOT IS_PED_HEADTRACKING_PED(drinkData.pedBartender, PLAYER_PED_ID())
							IF GET_DISTANCE_BETWEEN_ENTITIES(drinkData.pedBartender, PLAYER_PED_ID()) < 8.0
								TASK_LOOK_AT_ENTITY(drinkData.pedBartender, PLAYER_PED_ID(), -1)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "bartender looking at players")
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(drinkData.pedBartender, PLAYER_PED_ID()) >= 8.0
								TASK_CLEAR_LOOK_AT(drinkData.pedBartender)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_CAM_EXIST(stripCamAnim)
				AND NOT bDelaySyncSceneEnd
					stripCamAnim = CREATE_CAMERA(CAMTYPE_ANIMATED)
				ENDIF
				
				
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_STRIP_CLUB_BAR_POSITION()) < STRIP_CLUB_BAR_APPROACH_DISTANCE
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT bDelaySyncSceneEnd
				OR (IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
				AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) < 10 AND NOT g_bMPPlayerPassingOutDrunk)
					IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_STRIP_CLUB_BAR_POSITION()) < STRIP_CLUB_BAR_APPROACH_DISTANCE
					AND NOT IS_PHONE_ONSCREEN() 
					OR IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					AND (GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), yachtWhiskeyBartender.vLoc) < 3 AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("GTaMloRoom_bar1"))
						IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_DRINKING)
							CPRINTLN(DEBUG_SCLUB,"request drinking animation")
							REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_DRUNK1))
							REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_DRUNK2))
							REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_DRUNK3))
							REQUEST_ANIM_DICT(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_DRUNK4))
							IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
								REQUEST_ANIM_DICT("anim@mini@yacht@bar@drink@idle_a")
							ENDIF
							SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_BIT_ANIM_DRINKING)
						ENDIF
						
						IF HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum))
						AND REQUEST_SCRIPT_AUDIO_BANK("SAFEHOUSE_TREVOR_DRINK_WHISKEY")
						AND IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
						AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
						AND NOT IS_ENTITY_DEAD(drinkData.pedBartender)
						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						AND NOT IS_PED_WEARING_A_HELMET(PLAYER_PED_ID())
						AND NOT g_MultiplayerSettings.g_bSuicide
						AND CAN_PLAYER_USE_BAR()
							SET_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_YACHT_AUDIO_BANK_LOADED)
							
							IF NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							OR IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							AND HAS_ANIM_DICT_LOADED("anim@mini@yacht@bar@drink@IDLE_A")
								IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_STRIP_CLUB_BAR_POSITION()) < iUseBartenderDistance
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), whiskeyBarLocateA.vLoc, whiskeyBarLocateB.vLoc, 2 )
									IF NOT IS_ANY_PLAYER_USING_BAR()
										
										IF DOES_ENTITY_EXIST(drinkData.pedBartender)
										AND IS_ENTITY_VISIBLE(drinkData.pedBartender)
										AND IS_ENTITY_ON_SCREEN(drinkData.pedBartender)
											//the range for getting a drink is a little smaller then the range to start the script
											
											IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
												TRIGGER_YACHT_BARSTAFF_SPEECH_PLAYER_ENTRY(drinkData)
												RUN_YACHT_BARTENDER_IDLE_CHAT(drinkData)
											ENDIF
											
											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											AND DOES_ENTITY_EXIST(drinkData.pedBartender)
											AND IS_ENTITY_VISIBLE(drinkData.pedBartender)
											
												CPRINTLN(DEBUG_SCLUB,"No help is being displayed")
												IF GET_STRIP_CLUB_PLAYER_CASH(TRUE, TRUE, drinkData.iIsMP) >= SC_DRINK_COST
													PRINT_STRIPCLUB_DRINK_HELP(TRUE, drinkData.iIsMP)
												ELSE
													IF drinkData.iIsMP = 0
														PRINT_STRIPCLUB_HELP("SCLUB_NO_DRINK", TRUE, drinkData.iIsMP)
													ELSE
														//check for wallet
														IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(SC_DRINK_COST, TRUE, TRUE, drinkData.iIsMP) //Use this function twice to check bank and wallet separately, avoiding SCRIPT_MAX_INT32 overflow.
														OR CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(SC_DRINK_COST, TRUE, FALSE, drinkData.iIsMP)
															PRINT_STRIPCLUB_DRINK_HELP(TRUE, drinkData.iIsMP)
														ELSE
															PRINT_STRIPCLUB_HELP("SCLUB_NO_DRINK_MP", TRUE, drinkData.iIsMP)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											
											IF (STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
											AND IS_ANY_STRIPCLUB_DRINK_HELP_DISPLAYED()
											AND NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty))
											OR (IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
											AND STRIPCLUB_IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
											AND IS_ANY_STRIPCLUB_DRINK_HELP_DISPLAYED()
											AND NOT g_bPlayerInProcessOfExitingInterior
											AND g_bCanPlayerUseSafehouseActivity
											AND NOT bDelaySyncSceneEnd AND NOT bJustCleanedUpSyncScene)
												IF CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(SC_DRINK_COST, TRUE, TRUE, drinkData.iIsMP) //Use this function twice to check bank and wallet separately, avoiding SCRIPT_MAX_INT32 overflow.
												OR CAN_PLAYER_AFFORD_STRIP_CLUB_ITEM(SC_DRINK_COST, TRUE, FALSE, drinkData.iIsMP)
													CPRINTLN(DEBUG_SCLUB,"Pressed button to solicit barman")
													IF bIsMp
														playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bUsingBartender = TRUE
														CDEBUG1LN(DEBUG_SAFEHOUSE, "STRIPCLUB_IS_CONTROL_JUST_PRESSED: g_bUsingBarmaidActivity = TRUE")
														g_bUsingBarmaidActivity = TRUE
													ENDIF
													
													IF NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
														IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()) , <<127.1629, -1283.5865, 29.2786>>) < 0.001
														OR (NOT bIsMP AND NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA())
															//play sync scene right away															
															CPRINTLN(DEBUG_SCLUB,"play sync scene right away")
														ELSE
															CPRINTLN(DEBUG_SCLUB,"TASK_GO_STRAIGHT_TO_COORD")
															TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), <<127.1629, -1283.5865, 29.2786>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 300.0)
														ENDIF
													ELSE
														
														GET_POSITION_AS_OFFSET_FOR_YACHT(drinkData.iYachtInstance, MP_PROP_ELEMENT_WHISKY_BAR_SCENE, whiskeyBarScene)
														
														tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR(eDrinkAnimEnum, TRUE), whiskeyBarScene.vLoc, whiskeyBarScene.vRot)
														
														VECTOR tempRotation 
														tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR(eDrinkAnimEnum, TRUE), whiskeyBarScene.vLoc, whiskeyBarScene.vRot)
														FLOAT fTempRotation 
														fTempRotation = tempRotation.Z
														
														CPRINTLN(DEBUG_SCLUB,"Yacht goto Coords = ", tempRotation, ", rotation = ", fTempRotation)

														TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), tempPosition, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, fTempRotation)
														
		//												tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, TRUE), << -1569.238, -4087.600, 9.764 >>, << 0.000, 0.000, -43.920 >>)
		//												tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, TRUE), << -1569.238, -4087.600, 9.764 >>, << 0.000, 0.000, -43.920 >>)
		//												CPRINTLN(DEBUG_SCLUB,"Yacht glass Coords = ", tempRotation, ", rotation = ", fTempRotation)
		//												
		//												
		//												tempPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, FALSE), << -1569.238, -4087.600, 9.764 >>, << 0.000, 0.000, -43.920 >>)
		//												tempRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, FALSE), << -1569.238, -4087.600, 9.764 >>, << 0.000, 0.000, -43.920 >>)
		//												CPRINTLN(DEBUG_SCLUB,"Yacht bottle Coords = ", tempRotation, ", rotation = ", fTempRotation)
														
														STRIPCLUB_TASK_OBJECT_SYNC_SCENE(drinkData.objGlass,  iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, TRUE),  REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
														STRIPCLUB_TASK_OBJECT_SYNC_SCENE(drinkData.objBottle, iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, FALSE), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
														
														IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
															PLAY_YACHT_SERVING_DIALOGUE(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL(), drinkData)
															REINIT_NET_TIMER(stYachtBarStaffIdleSpeech)
														ENDIF
													ENDIF
													iDrinkWaitTime = GET_GAME_TIMER() + 250 //wait a little bit so we can sync all the people who propositioned the bartender at the same time
													SET_STRIPCLUB_USING_BAR(TRUE)
													CLEAR_ALL_STRIP_CLUB_HELP()
													barStage = BAR_INIT
												ELIF bIsMp
													STRIP_CLUB_BUY_CASH_ALERT(drinkData.iIsMP)
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINT_STRIPCLUB_HELP("SCLUB_BUSY_DRINK", FALSE, drinkData.iIsMP)
									ENDIF
								ELSE //away from the bar
									CDEBUG2LN(DEBUG_SCLUB,"Away from bar")
									CLEAR_STRIPCLUB_DRINKING_HELP()
									IF bIsMp
										IF playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bUsingBartender
											playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bUsingBartender = FALSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "away from the bar: g_bUsingBarmaidActivity = FALSE")
											g_bUsingBarmaidActivity = FALSE
										ENDIF
									ENDIF	
								ENDIF
								
							ELSE
							ENDIF
						ELSE
							CLEAR_STRIPCLUB_DRINKING_HELP()
							
							CPRINTLN(DEBUG_SCLUB,"Don't allow bar drink")
							IF NOT HAS_ANIM_DICT_LOADED(GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum))
								CPRINTLN(DEBUG_SCLUB,"Anims havn't loaded yet")
							ENDIF
							IF NOT REQUEST_SCRIPT_AUDIO_BANK("SAFEHOUSE_TREVOR_DRINK_WHISKEY")
								CPRINTLN(DEBUG_SCLUB,"Sound havn't loaded yet")
							ENDIF
							IF NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
								CPRINTLN(DEBUG_SCLUB,"Player not free for ambient task")
							ENDIF
							IF IS_ANY_INTERACTION_ANIM_PLAYING()
								CPRINTLN(DEBUG_SCLUB,"Interaction anim playing")
							ENDIF
							IF IS_ENTITY_DEAD(drinkData.pedBartender)
								CPRINTLN(DEBUG_SCLUB,"Bartender dead")
							ENDIF
							IF IS_CUSTOM_MENU_ON_SCREEN()
								CPRINTLN(DEBUG_SCLUB,"Custome menu on screen")
							ENDIF
							IF IS_PED_WEARING_A_HELMET(PLAYER_PED_ID())
								CPRINTLN(DEBUG_SCLUB,"Player Wearing helmet")
							ENDIF
							IF NOT CAN_PLAYER_USE_BAR()
								CPRINTLN(DEBUG_SCLUB,"Player cant use bar")
							ENDIF
							
						ENDIF
					ELSE
						IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
							CLEAR_STRIPCLUB_DRINKING_HELP()
						ENDIF
						IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_YACHT_AUDIO_BANK_LOADED)
							CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_YACHT_AUDIO_BANK_LOADED)
							RELEASE_NAMED_SCRIPT_AUDIO_BANK("SAFEHOUSE_TREVOR_DRINK_WHISKEY")
						ENDIF
						CDEBUG2LN(DEBUG_SAFEHOUSE, "yachtWhiskeyBartender.vLoc: ", yachtWhiskeyBartender.vLoc)
						CDEBUG2LN(DEBUG_SAFEHOUSE, "distance from bartender: ", GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(), yachtWhiskeyBartender.vLoc))
					ENDIF
				ELSE
					IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 10
						CDEBUG1LN(DEBUG_SAFEHOUSE, "Player alcohol hit count: ", Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()))
					ENDIF 
					//Not in range clear script
					CLEAR_STRIPCLUB_DRINKING_HELP()
					barStage = BAR_CLEANUP
				ENDIF
			BREAK
			
			CASE BAR_INIT
				
				GET_CONTROL_OF_BARTENDER(drinkData)
				DISABLE_SELECTOR_THIS_FRAME()
				DISABLE_INTERACTION_MENU()
								
				IF DOES_ANOTHER_PLAYER_WANT_TO_USE_BAR()
				OR HAS_ANOTHER_PLAYER_STARTED_THE_BARTEND_SCENE()
				AND bIsMp
					//oh shit another player propositioned the bartender and he has priority over me. I better quit out
					playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bUsingBartender = FALSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "BAR_INIT: g_bUsingBarmaidActivity = FALSE")
					g_bUsingBarmaidActivity = FALSE
					SET_STRIPCLUB_USING_BAR(FALSE)
					PRINT_STRIPCLUB_HELP("SCLUB_BUSY_DRINK", FALSE, drinkData.iIsMP)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					barStage = BAR_NOT_ACTIVE
				ENDIF
				
				IF bControlsBartender
				AND CAN_PLAYER_START_CUTSCENE() AND iDrinkWaitTime < GET_GAME_TIMER()
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK 
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
				
					//Pay for your drink
					STRIP_CLUB_SPEND_CASH(SC_DRINK_COST, TRUE, DRINKING_AT_THE_BAR, FALSE, drinkData.iIsMP)
					
//					STRIP_CLUB_SPEND_CASH(GET_STRIP_CLUB_PLAYER_CASH(TRUE, drinkData.iIsMP), TRUE, DRINKING_AT_THE_BAR, drinkData.iIsMP)
//					STRIPCLUB_GIVE_CASH(50, drinkData.iIsMP)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
					DISABLE_KILL_YOURSELF_OPTION()
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					
					IF bIsMp
						NETWORK_SET_LOOK_AT_TALKERS(FALSE)
					ENDIF
					
					// position and animate peds
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND NOT IS_PED_INJURED(drinkData.pedBartender)
						//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						IF bIsMp
							REMOVE_STRIPCLUB_NIGHTVISION(TRUE)
							REMOVE_STRIPCLUB_PLAYER_MASK(bIsMp)
						ENDIF
						
						HANG_UP_AND_PUT_AWAY_PHONE(FALSE)
						
						GET_DRINKING_FACE_ANIMS(eDrinkAnimEnum, txtBartenderFace, txtPlayerFace)
						
						//REALLY_SLOW_BLEND_IN
						IF NOT IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							STRIPCLUB_CREATE_SYNC_SCENE(iDrinkScene, <<130.19, -1283.79, 30.09>>, <<0,0,30.0>>, FALSE, bIsMp, FALSE)
						ELSE
							GET_POSITION_AS_OFFSET_FOR_YACHT(drinkData.iYachtInstance, MP_PROP_ELEMENT_WHISKY_BAR_SCENE, whiskeyBarScene)
								
							STRIPCLUB_CREATE_SYNC_SCENE(iDrinkScene, whiskeyBarScene.vLoc, whiskeyBarScene.vRot, TRUE, bIsMp, FALSE)
						ENDIF
						
						FLOAT fPlayerSyncSceneBlendOut
						fPlayerSyncSceneBlendOut = -1
						
							IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
								fPlayerSyncSceneBlendOut = REALLY_SLOW_BLEND_OUT
							ENDIF
												
						STRIPCLUB_TASK_SYNC_SCENE(PLAYER_PED_ID(),            iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR(eDrinkAnimEnum, TRUE),          WALK_BLEND_IN , fPlayerSyncSceneBlendOut, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
						STRIPCLUB_TASK_SYNC_SCENE(drinkData.pedBartender,     iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR(eDrinkAnimEnum, FALSE),         REALLY_SLOW_BLEND_IN, -1, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
						STRIPCLUB_TASK_OBJECT_SYNC_SCENE(drinkData.objGlass,  iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, TRUE),  REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
						STRIPCLUB_TASK_OBJECT_SYNC_SCENE(drinkData.objBottle, iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_ANIM_BAR_OBJECTS(eDrinkAnimEnum, FALSE), REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, bIsMp, FALSE)
						IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							STRIPCLUB_SET_SYNC_SCENE_CAM(iDrinkScene, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), GET_STRIP_CLUB_BAR_CAM_ANIM(eDrinkAnimEnum), bIsMp, FALSE )
						ELSE
							IF DOES_CAM_EXIST(stripCamAnim)
								DESTROY_CAM(stripCamAnim)
							ENDIF							
						ENDIF
						STRIPCLUB_START_SYNC_SCENE(iDrinkScene, bIsMp)
						
						IF NOT IS_STRING_NULL_OR_EMPTY(txtBartenderFace)
							TASK_PLAY_ANIM(drinkData.pedBartender, GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), txtBartenderFace, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
						ENDIF
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
						AND NOT IS_STRING_NULL_OR_EMPTY(txtPlayerFace)
							TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(eDrinkAnimEnum), txtPlayerFace, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
						ENDIF
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						IF NOT bIsMP 
							IF NOT IS_PLAYER_IN_FIRST_PERSON_CAMERA()
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
							ENDIF
						ELSE
							playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bBartenderSceneStarted = TRUE
						ENDIF
						
						DISPLAY_RADAR(FALSE)
						
						IF NOT bIsMp
							IF GET_DRUNK_LINE(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL()) <= 0
								PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BUY", SPEECH_PARAMS_FORCE_NORMAL)
							ELIF GET_DRUNK_LINE(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL()) = 1
								PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BUY_REPEAT", SPEECH_PARAMS_FORCE_NORMAL)
							ELIF GET_DRUNK_LINE(GET_STRIPCLUB_AMOUNT_OF_ALCOHOL()) = 2
								PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BUY_DRUNK", SPEECH_PARAMS_FORCE_NORMAL)
							ELSE
								PLAY_PED_AMBIENT_SPEECH(PLAYER_PED_ID(), "GENERIC_BUY_REPEAT_DRUNK", SPEECH_PARAMS_FORCE_NORMAL)
							ENDIF
						ENDIF
						
						SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_CONTROL_REMOVED)
					ENDIF
					
					bIncDrink = FALSE
					barStage = BAR_GETTING_DRINK
				ENDIF
			BREAK
			
			CASE BAR_GETTING_DRINK
				GET_CONTROL_OF_BARTENDER(drinkData)
				CLEAR_STRIPCLUB_DRINKING_HELP()
				
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_SELECTOR_THIS_FRAME()
				
				IF NOT bIncDrink AND DID_PLAYER_TAKE_DRINK(eDrinkAnimEnum)
					CPRINTLN(DEBUG_SCLUB, "INC DRINK")
					INC_STRIPCLUB_DRINKS()
					bIncDrink = TRUE
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_ORDER_DRINK)
				ENDIF
				
				IF IS_BAR_SCENE_OVER() AND bControlsBartender
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(5.2223)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-6.1803)
					
					IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ELSE
						RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, FALSE)
					ENDIF
					
					SETTIMERA(0)
					
					IF NOT IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
						STRIPCLUB_END_SYNC_SCENE(iDrinkScene, bIsMp)
					ELSE
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						PRINTLN("STRIPCLUB_DRINKING: Setting bDelaySyncSceneEnd TRUE")
						bDelaySyncSceneEnd = TRUE
					ENDIF
					
					IF DOES_ENTITY_EXIST(drinkData.pedBartender) AND NOT IS_ENTITY_DEAD(drinkData.pedBartender)
						SET_BARTENDER_IDLE_ANIM(drinkData.pedBartender)
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(drinkData.pedBartender, TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(drinkData.objGlass) AND NOT IS_ENTITY_DEAD(drinkData.objGlass)
						PLAY_ENTITY_ANIM(drinkData.objGlass, "idle_a_shot_glass", GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_IDLE), INSTANT_BLEND_IN, FALSE, TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(drinkData.objBottle) AND NOT IS_ENTITY_DEAD(drinkData.objBottle)
						PLAY_ENTITY_ANIM(drinkData.objBottle, "idle_a_whiskey", GET_STRIP_CLUB_ANIM_DICT_BAR_DRINK(SCBA_IDLE), INSTANT_BLEND_IN, FALSE, TRUE)
					ENDIF
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
					
					IF bIsMp
						SET_STRIPCLUB_PLAYER_MASK(bIsMp)
						SET_STRIPCLUB_NIGHTVISION(TRUE)
					ENDIF
					
					barStage = BAR_USED
				ELSE
					IF IS_PLAYER_SWITCH_IN_PROGRESS()
						CPRINTLN(DEBUG_SCLUB,"Player went to sky cam while getting a drink")
						SET_SYNCHRONIZED_SCENE_PHASE(GET_SRTIPCLUB_LOCAL_SCENE_ID(iDrinkScene, bIsMp), 1.0)
					ENDIF
				ENDIF
			BREAK
			
			CASE BAR_USED
				
				CPRINTLN(DEBUG_SCLUB,"Bar Used. ", TIMERA())
				
				IF  TIMERA() > DEFAULT_INTERP_TO_FROM_GAME OR bIsMP OR IS_PLAYER_IN_FIRST_PERSON_CAMERA()
					SET_STRIPCLUB_USING_BAR(FALSE)
					DISPLAY_RADAR(TRUE)
					ENABLE_SELECTOR()
					ENABLE_KILL_YOURSELF_OPTION()
					ENABLE_INTERACTION_MENU()
					IF IS_BITMASK_AS_ENUM_SET(iStripClubBits_Streaming, STRIP_CLUB_YACHT_AUDIO_BANK_LOADED)
						CLEAR_BITMASK_AS_ENUM(iStripClubBits_Streaming, STRIP_CLUB_YACHT_AUDIO_BANK_LOADED)
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("SAFEHOUSE_TREVOR_DRINK_WHISKEY")
					ENDIF
					IF bIsMp
						playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bUsingBartender = FALSE
						playerDrinkingBD[PARTICIPANT_ID_TO_INT()].bBartenderSceneStarted = FALSE
						NETWORK_SET_LOOK_AT_TALKERS(TRUE)
					ENDIF
					IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK)
						BawsaqIncrementDrunkModifier_PUBCLUBVISIT()
						SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK)
					ENDIF					
					IF NOT IS_BITMASK_AS_ENUM_SET(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK_W_FRIEND)
						IF DOES_ENTITY_EXIST(FRIEND_A_PED_ID())
							BawsaqIncrementDrunkModifier_FRNDTOPUB()
							SET_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_HAD_DRINK_W_FRIEND)
						ENDIF
					ENDIF
					
					CLEAR_BITMASK_AS_ENUM(iStripClubBits_General, STRIP_CLUB_GENERAL_PLAYER_CONTROL_REMOVED)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF bDelaySyncSceneEnd
					AND IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
						RESET_NET_TIMER(timerDelayedSyncScene)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					ENDIF
					barStage = BAR_NOT_ACTIVE
				ENDIF
			BREAK
			
			CASE BAR_TOO_DRUNK
				IF GET_ENTITY_DISTANCE_FROM_LOCATION(PLAYER_PED_ID(),GET_STRIP_CLUB_BAR_POSITION()) < STRIP_CLUB_BAR_APPROACH_DISTANCE
					IF NOT IS_PHONE_ONSCREEN()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_STRIPCLUB_SOLICIT_BUTTON_JUST_PRESSED()
								CPRINTLN(DEBUG_SCLUB,"Pressed button to solicit barman WHILE TOO DRUNK")
								CREATE_CONVERSATION(stripClubConversation, "SCAUD", "SC_NO_DRINK", GET_STRIPCLUB_SPEECH_PRIORITY())
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CLEAR_STRIPCLUB_DRINKING_HELP()
					barStage = BAR_CLEANUP
				ENDIF
			BREAK
			
			CASE BAR_CLEANUP
				IF REMOVE_BARMAN(drinkData)
					CLEANUP_STRIPCLUB_DRINKING(drinkData)
				ELSE
					CPRINTLN(DEBUG_SCLUB,"CLEANUP_STAGE: waiting to remove barmaid")
				ENDIF	
			BREAK
			
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
	
ENDSCRIPT
