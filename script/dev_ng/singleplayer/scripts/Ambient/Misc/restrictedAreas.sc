

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_player.sch"
USING "commands_script.sch"

USING "area_checks.sch"
USING "script_player.sch"
USING "flow_public_core.sch"
USING "replay_public.sch"

USING "vehicle_gen_private.sch"
USING "properties_public.sch"

// Enums
ENUM restrictedStageFlag
	waitingForInteraction = 0,
	debugHoldingCase,
	onReplayScreen
ENDENUM
restrictedStageFlag restrictedStage = waitingForInteraction

// Constants
CONST_INT 		 NUMBER_OF_AREAS 8
CONST_INT 		 GOLF_COURSE     0
CONST_INT 		 AIRPORT_AIRSIDE 1
CONST_INT 		 MILITARY_BASE   2
CONST_INT 		 PRISON          3
CONST_INT 		 BIOPLANT        4
CONST_INT 		 MILITARY_DOCKS  5
CONST_INT 		 MOVIE_STUDIO    6
CONST_INT 		 DOWNTOWN_POLICE 7
CONST_INT 		 PRISON_DELAY    8000

CONST_FLOAT		 SQUARE_DISTANCE 1000000.0 // 1000m^2

// Variables
AREA_CHECK_AREAS acArea
BOOL             bWithinActiveDistance[NUMBER_OF_AREAS]
BOOL             bIsSuppressed[NUMBER_OF_AREAS]
BOOL 			 bAllowedInArea, bWarnedByAirTrafficControl, bRestrictedWantedLevelActive
BOOL 			 bActiveWantedArea[NUMBER_OF_AREAS]
INT				 iAreaCounter 
INT				 iActiveAreas

INT 			 currentArea

INT 			 restrictedWantedLevel = 3
TEXT_LABEL_23 	 suppressingScript
INT 			 iTimeInArea[NUMBER_OF_AREAS]

structPedsForConversation dialogueStruct

// Debug
#IF IS_DEBUG_BUILD
	BOOL	bDisableWanted = FALSE
	BOOL	bDrawLocate
	BOOL	bButtonToggle
	BOOL	bOutput
	VECTOR vMin
	VECTOR vMax
#ENDIF

PROC SETUP_DEBUG()

	#IF IS_DEBUG_BUILD
	 	START_WIDGET_GROUP("Restricted Areas")			
			ADD_WIDGET_BOOL("Disable Restricted Area Wanted Level", bDisableWanted)
			
			ADD_WIDGET_FLOAT_SLIDER("vMin.x", vMin.x, -7000, 7000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vMin.y", vMin.y, -7000, 7000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vMin.z", vMin.z, -150, 150, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("vMax.x", vMax.x, -7000, 7000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vMax.y", vMax.y, -7000, 7000, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("vMax.z", vMax.z, -150, 150, 0.1)
			
			ADD_WIDGET_BOOL("bDrawLocate", bDrawLocate)
			ADD_WIDGET_BOOL("bOutput", bOutput)
			ADD_WIDGET_BOOL("Enable debug spew", bRADebugSpam)
		STOP_WIDGET_GROUP()
	#ENDIF
ENDPROC 

PROC RUN_DEBUG()
	
	#IF IS_DEBUG_BUILD

		// Toggle for wanted level
		IF bDisableWanted
			IF restrictedStage <> debugHoldingCase
				restrictedStage = debugHoldingCase
			ENDIF
		ELSE
			IF restrictedStage = debugHoldingCase
				restrictedStage = waitingForInteraction
			ENDIF
		ENDIF
		
		IF bDrawLocate
			IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vMin, vMax, TRUE)
		ENDIF
		
		IF bOutput
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_VECTOR_TO_DEBUG_FILE(vMin)
			SAVE_STRING_TO_DEBUG_FILE(", ")
			SAVE_VECTOR_TO_DEBUG_FILE(vMax)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			bOutput = FALSE
		ENDIF
		
		IF NOT bButtonToggle
			IF IS_MOUSE_BUTTON_JUST_RELEASED(MB_LEFT_BTN)
				vMin = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				bButtonToggle = TRUE
			ENDIF
		ELSE
			IF IS_MOUSE_BUTTON_JUST_RELEASED(MB_LEFT_BTN)
				vMax = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				vMax.z += 3
				bButtonToggle = FALSE
			ENDIF
		ENDIF
	#ENDIF
ENDPROC 

/// PURPOSE:
///    Has the current player character bought the golf course?
/// RETURNS:
///    TRUE if owned
FUNC BOOL IS_GOLF_COURSE_OWNED_BY_CURRENT_PLAYER()
	
	IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_GOLF_CLUB) = GET_CURRENT_PLAYER_PED_ENUM()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Is the player on the golf course in a vehicle that is not allowed?
/// RETURNS:
///    TRUE if the player is in anything other than a golf cart (CADDY/CADDY2)
FUNC BOOL CHECK_RESTRICTED_VEHICLES_FOR_GOLF_COURSE()
		
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(vehIndex)
			
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(vehIndex))
			OR GET_ENTITY_MODEL(vehIndex) = CADDY 
			OR GET_ENTITY_MODEL(vehIndex) = CADDY2
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Does the current player have a hangar space at the airport?
/// RETURNS:
///    TRUE if current player owns a hangar
FUNC BOOL IS_AIRPORT_OWNED_BY_CURRENT_PLAYER()

	SWITCH GET_CURRENT_PLAYER_PED_ENUM()
		
		CASE CHAR_MICHAEL
			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WEB_HANGAR_MICHAEL, VEHGEN_S_FLAG_ACQUIRED)
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_AIRPORT_OWNED_BY_CURRENT_PLAYER(), VEHGEN_WEB_HANGAR_MICHAEL") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CHAR_FRANKLIN
			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WEB_HANGAR_FRANKLIN, VEHGEN_S_FLAG_ACQUIRED)
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_AIRPORT_OWNED_BY_CURRENT_PLAYER(), VEHGEN_WEB_HANGAR_FRANKLIN") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
//		CASE CHAR_TREVOR
//			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WEB_HANGAR_TREVOR, VEHGEN_S_FLAG_ACQUIRED)
//				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_AIRPORT_OWNED_BY_CURRENT_PLAYER(), VEHGEN_WEB_HANGAR_TREVOR") ENDIF #ENDIF
//				RETURN TRUE
//			ENDIF
//		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_STUDIO_OPEN_FOR_CURRENT_PLAYER()
	
	SWITCH GET_CURRENT_PLAYER_PED_ENUM()

		CASE CHAR_MICHAEL
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN)
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - TRUE for MICHAEL") ENDIF #ENDIF
				RETURN TRUE
				#IF IS_DEBUG_BUILD ELSE IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - FALSE for MICHAEL") ENDIF #ENDIF
			ENDIF
			// B*1405773 - Don't get a wanted level if Solomon1 is available, even if they don't go to security gates
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("ambient_Solomon")) > 0
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - ambient_solomon is running - Michael is allowed in studio") ENDIF #ENDIF
				RETURN TRUE
			ENDIF
		BREAK

		CASE CHAR_FRANKLIN
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN_FRANKLIN)
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - TRUE for FRANKLIN") ENDIF #ENDIF
				RETURN TRUE
				#IF IS_DEBUG_BUILD ELSE IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - FALSE for FRANKLIN") ENDIF #ENDIF
			ENDIF
		BREAK
		
		CASE CHAR_TREVOR
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MOVIE_STUDIO_OPEN)
				#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - TRUE for TREVOR") ENDIF #ENDIF
				RETURN TRUE
				#IF IS_DEBUG_BUILD ELSE IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), "> - IS_STUDIO_OPEN_FOR_CURRENT_PLAYER(), FLOWFLAG_MOVIE_STUDIO_OPEN - FALSE for TREVOR") ENDIF #ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC APPLY_WANTED_LEVEL()
	
	SWITCH currentArea
	
		CASE GOLF_COURSE
			restrictedWantedLevel = 2
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the GOLF_COURSE wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE AIRPORT_AIRSIDE
			IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			AND IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<200,200,200>>, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)-<<200,200,200>>, TRUE)
				restrictedWantedLevel = 5
			ELSE
				restrictedWantedLevel = 3
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the AIRPORT_AIRSIDE wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE MILITARY_BASE
			IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			AND IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<200,200,200>>, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)-<<200,200,200>>, TRUE)
				restrictedWantedLevel = 5
			ELSE
				restrictedWantedLevel = 4
			ENDIF
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the MILITARY_BASE wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE PRISON
			restrictedWantedLevel = 4
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the PRISON wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE BIOPLANT
			restrictedWantedLevel = 4
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the BIOPLANT wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE MILITARY_DOCKS
			restrictedWantedLevel = 4 //#1313927 // #1025597
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the MILITARY_DOCKS wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK
		
		CASE MOVIE_STUDIO
			restrictedWantedLevel = 2
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the MOVIE_STUDIO wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK

		CASE DOWNTOWN_POLICE
			restrictedWantedLevel = 3
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
				CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Setting the DOWNTOWN_POLICE wanted level to ", restrictedWantedLevel)
			ENDIF
		BREAK

	ENDSWITCH

	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < restrictedWantedLevel
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), restrictedWantedLevel)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	
	// Flag for repeat checks
	bActiveWantedArea[ENUM_TO_INT(currentArea)] = TRUE
	bRestrictedWantedLevelActive = TRUE
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO)
		CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - FORCE CLEANUP")
		TERMINATE_THIS_THREAD()	
	ENDIF

	SETUP_DEBUG()
	
	ADD_PED_FOR_DIALOGUE(dialogueStruct, 8, NULL, "TANNOY")

	WHILE TRUE
		
		WAIT(0)

		RUN_DEBUG() 

		IF IS_PLAYER_PLAYING(PLAYER_ID())
		
//			IF IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-1773.943970,3287.334229,30>>, <<-2029.776489,2845.083252,250>>, 250.000000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2725.889404,3291.098633,30>>, <<-2009.181519,2879.835205,250>>, 180.000000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2442.026123,3326.698730,30>>, <<-2033.927856,3089.048828,250>>, 200.000000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-1917.165405,3374.208984,30>>, <<-2016.790894,3195.058105,250>>, 86.250000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2192.752930,3373.277832,30>>, <<-2191.544434,3150.416504,250>>, 140.500000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2077.663330,3344.514160,30>>, <<-2191.544434,3150.416504,250>>, 140.500000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2861.755371,3352.660645,30>>, <<-2715.870850,3269.915527,250>>, 90.000000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-2005.574463,3364.532715,30>>, <<-1977.568848,3330.888184,250>>, 100.000000)
//			OR IS_POINT_IN_ANGLED_AREA(<<0,0,0>>, <<-1682.234985,3004.285156,30>>, <<-1942.746948,2947.441162,250>>, 248.750000)
//
//			ENDIF
			
			SWITCH restrictedStage
		
				CASE waitingForInteraction
				
					IF IS_REPLAY_BEING_PROCESSED()
						CPRINTLN(DEBUG_REPLAY, "Stopping restricted area checks as a replay is being processed")
						restrictedStage = onReplayScreen
					ELSE
					
						// If restricted wanted level is active, this check needs to run every frame to prevent wanted level being cleared and reapplied
						IF bRestrictedWantedLevelActive
							iAreaCounter = 0
							iActiveAreas = 0
							REPEAT NUMBER_OF_AREAS iAreaCounter
								IF bActiveWantedArea[iAreaCounter]
									IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), INT_TO_ENUM(AREA_CHECK_AREAS, (iAreaCounter+ENUM_TO_INT(AC_GOLF_COURSE))))
										#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "RESTRICTED AREAS: SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME in area ", iAreaCounter+ENUM_TO_INT(AC_GOLF_COURSE)) ENDIF #ENDIF
										SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID()) // Prevent constant lose/regain on wanted level
										iActiveAreas++
									ELSE
										#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "RESTRICTED AREAS: Stopping calling SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME in area ", iAreaCounter+ENUM_TO_INT(AC_GOLF_COURSE)) ENDIF #ENDIF
										bActiveWantedArea[iAreaCounter] = FALSE
									ENDIF
								ENDIF
							ENDREPEAT
							IF iActiveAreas = 0
								#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "RESTRICTED AREAS: Number of wanted active areas is now zero, resetting wanted level general check bool") ENDIF #ENDIF
								bRestrictedWantedLevelActive = FALSE
							ENDIF
						ENDIF
						
						// Check each area only once per second.
						IF TIMERA() > (1000/NUMBER_OF_AREAS)
						
							// Get current area
							acArea = INT_TO_ENUM(AREA_CHECK_AREAS, (currentArea+ENUM_TO_INT(AC_GOLF_COURSE)))
							// Wait until player is within 1000m before starting checks.
							IF IS_AREACHECK_IN_ACTIVE_RANGE(acArea, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
								IF NOT bWithinActiveDistance[currentArea]
									CPRINTLN(DEBUG_AMBIENT, "restrictedArea - Within activation range. Current area is ", currentArea)
									bWithinActiveDistance[currentArea] = TRUE
								ENDIF
								
								// Current area is suppressed by another script
								IF IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(acArea)
									IF NOT bIsSuppressed[currentArea]
										CPRINTLN(DEBUG_AMBIENT, "restrictedArea - IF IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(acArea). acArea is ", ENUM_TO_INT(acArea))
										bIsSuppressed[currentArea] = TRUE
									ENDIF
								
									suppressingScript = WANTED_LEVEL_SUPPRESSING_SCRIPT(acArea)
									IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(suppressingScript)) = 0
										IF NOT g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)]
											#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_AMBIENT, "IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(", suppressingScript, ") is no longer running.")
											#ENDIF
											RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(acArea, g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)])
										ENDIF
									ENDIF
								ELSE
									// Player isn't currently marked as active in this area ("Active" means "allowed to be here due to another script causing the global to get set")
									IF NOT g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)]
										
										// Flight specific checks
										IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
											// Check to see whether player is allowed to be in this restricted area
											bAllowedInArea = FALSE

											SWITCH acArea
												CASE AC_GOLF_COURSE
													IF IS_GOLF_COURSE_OWNED_BY_CURRENT_PLAYER() 
													OR CHECK_RESTRICTED_VEHICLES_FOR_GOLF_COURSE()
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_AIRPORT_AIRSIDE
													IF IS_AIRPORT_OWNED_BY_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_MOVIE_STUDIO
													IF IS_STUDIO_OPEN_FOR_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
											ENDSWITCH
											
											IF NOT bAllowedInArea
												IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), acArea)
													#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "Player in restricted area for ", GET_GAME_TIMER() - iTimeInArea[currentArea], " (Z_P2A)") ENDIF #ENDIF
													IF GET_GAME_TIMER() - iTimeInArea[currentArea] > PRISON_DELAY
														CPRINTLN(DEBUG_AMBIENT, "restrictedAreas - IS_COORD_IN_SPECIFIED_AREA() - Allowed time exceeded")
														APPLY_WANTED_LEVEL()
													ELIF IS_PROJECTILE_IN_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<200,200,200>>, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)-<<200,200,200>>, TRUE)
														CPRINTLN(DEBUG_AMBIENT, "restrictedAreas - IS_COORD_IN_SPECIFIED_AREA() - Projectile in area")
														APPLY_WANTED_LEVEL()
													ELSE
														IF acArea != AC_PRISON
															IF NOT bWarnedByAirTrafficControl
																IF CREATE_CONVERSATION(dialogueStruct, "FH1AUD", "FH1_TANNOY", CONV_PRIORITY_HIGH)
																	bWarnedByAirTrafficControl = TRUE
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "Resetting restricted area timer (Z_P2B)") ENDIF #ENDIF
													iTimeInArea[currentArea] = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ELSE
											// Check to see whether player is allowed to be in this restricted area
											bAllowedInArea = FALSE

											SWITCH acArea
												CASE AC_GOLF_COURSE
													IF IS_GOLF_COURSE_OWNED_BY_CURRENT_PLAYER() 
													OR CHECK_RESTRICTED_VEHICLES_FOR_GOLF_COURSE()
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_AIRPORT_AIRSIDE
													IF IS_AIRPORT_OWNED_BY_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_MOVIE_STUDIO
													IF IS_STUDIO_OPEN_FOR_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
											ENDSWITCH

											IF NOT bAllowedInArea
												IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), acArea)
													#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "Player in restricted area for ", GET_GAME_TIMER() - iTimeInArea[currentArea], " (Z_P3A)") ENDIF #ENDIF
													IF NOT (acArea = AC_PRISON) OR (GET_GAME_TIMER() - iTimeInArea[currentArea] > PRISON_DELAY)
														CPRINTLN(DEBUG_AMBIENT, "restrictedArea - IS_COORD_IN_SPECIFIED_AREA()")
														APPLY_WANTED_LEVEL()	
													ENDIF
												ELSE
													#IF IS_DEBUG_BUILD IF bRADebugSpam CPRINTLN(DEBUG_AMBIENT, "Resetting restricted area timer (Z_P3B)") ENDIF #ENDIF
													iTimeInArea[currentArea] = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF
									ELSE
										//IF IS_PED_SHOOTING(PLAYER_PED_ID())
											// Check to see whether player is allowed to be in this restricted area
											bAllowedInArea = FALSE

											SWITCH acArea
												CASE AC_GOLF_COURSE
													IF IS_GOLF_COURSE_OWNED_BY_CURRENT_PLAYER() 
													OR CHECK_RESTRICTED_VEHICLES_FOR_GOLF_COURSE()
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_AIRPORT_AIRSIDE
													IF IS_AIRPORT_OWNED_BY_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
												
												CASE AC_MOVIE_STUDIO
													IF IS_STUDIO_OPEN_FOR_CURRENT_PLAYER() 
														bAllowedInArea = TRUE
													ENDIF
												BREAK
											ENDSWITCH

											IF NOT bAllowedInArea
												IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), acArea)
													//CPRINTLN(DEBUG_AMBIENT, "restrictedArea - IS_COORD_IN_SPECIFIED_AREA()")
													APPLY_WANTED_LEVEL()										
												ENDIF
											ENDIF
										//ENDIF
									ENDIF
								ENDIF
							ELSE
								// Player has left the area
								IF bWithinActiveDistance[currentArea]
									IF IS_RESTRICTED_AREA_WANTED_LEVEL_SUPPRESSED(acArea)
										suppressingScript = WANTED_LEVEL_SUPPRESSING_SCRIPT(acArea)
										IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(suppressingScript)) = 0
											#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_AMBIENT, "IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(", suppressingScript, ") is no longer running. Out of range.")
											#ENDIF
											RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(acArea)
											bWithinActiveDistance[currentArea] = FALSE
										ENDIF
									ELSE
										IF g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)]
											CPRINTLN(DEBUG_AMBIENT, "restrictedArea - Out of activation range for area ", ENUM_TO_INT(acArea))
											g_bPlayerIsActiveInRestrictedArea[ENUM_TO_INT(acArea)] = FALSE
											bWithinActiveDistance[currentArea] = FALSE
											bIsSuppressed[currentArea] = FALSE
											bWarnedByAirTrafficControl = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							// Update next area to check
							currentArea++
							IF currentArea = NUMBER_OF_AREAS
								currentArea = 0
							ENDIF
		
							// Reset timer
							SETTIMERA(0)
							
						ENDIF
					ENDIF
				BREAK
				
				CASE debugHoldingCase
					// Debug BOOL will jump into here to get out of the wanted level checks
				BREAK
				
				CASE onReplayScreen
					
					// Don't do the checks if we are on the replay screen
					IF NOT IS_REPLAY_BEING_PROCESSED()
						CPRINTLN(DEBUG_REPLAY, "Resuming restricted area checks, replay processing finished.")
						restrictedStage = waitingForInteraction // replay finished processing, continue checks
					ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDWHILE
ENDSCRIPT
