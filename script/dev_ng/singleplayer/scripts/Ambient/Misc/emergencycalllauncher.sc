

//Compile out Title Update changes to header functions.
//Must be before includes.

CONST_INT   USE_TU_CHANGES  1


//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //                                             
//      AUTHOR          :                                                                //
//      DESCRIPTION     :   Choose and emergency service                                //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "script_player.sch"
USING "commands_pad.sch"
USING "commands_path.sch"
USING "cellphone_public.sch"
USING "dialogue_public.sch"
USING "emergency_call.sch"
USING "flow_public_GAME.sch"

// Variables ----------------------------------------------//

ENUM EMERGENCY_MAIN_ENUM
    WAIT_FOR_CALL = 0,
    CLEANUP
ENDENUM
EMERGENCY_MAIN_ENUM emergency_main_stages = WAIT_FOR_CALL

ENUM callStageFlag
    callphoneup,
    callwaitforscript
ENDENUM
callStageFlag callStage = callphoneup

structPedsForConversation emergencyPedStruct
TEXT_LABEL_23 suppressingScript
BOOL bDisableCalls
enumConversationPriority callPriority

// Functions ----------------------------------------------//
 
PROC take_call()



    
    SWITCH callStage
            
        CASE callphoneup
        
            IF IS_CALLING_CONTACT (CHAR_CALL911)
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_BUY
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_SELL
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_BIKER_CONTRABAND_DEFEND
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GUNRUNNING_BUY
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GUNRUNNING_SELL
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) != FMMC_TYPE_GUNRUNNING_DEFEND
			
                IF g_bEmergencyCallPriorityIsHigh
                    callPriority = CONV_PRIORITY_CELLPHONE
                ELSE
                    callPriority = CONV_PRIORITY_NON_CRITICAL_CALL
                ENDIF
            
                IF NOT g_bForceEmergencyCallAnswerphone

                    #if USE_TU_CHANGES
                    IF IS_CURRENT_MISSION_DLC() //Added by Steve T to make emergency calls DLC compliant. 11.02.14

                        IF PLAYER_CALL_EMERGENCY_SERVICES_USING_V_CONTENT_IN_DLC (emergencyPedStruct, CHAR_CALL911, "LOCAUD", "LOC_CALLQ", callPriority, "CELL_601", "LOC_CALLA", "LOC_CALLF", "LOC_CALLP") //The calls are already exported and in the XLS sheet.
                            

                            #if IS_DEBUG_BUILD

                                cdPrintstring ("ECLauncher - Using_V_Content_in_DLC")
                                cdPrintnl()

                            #endif
                            

                            IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
                                REQUEST_SCRIPT("emergencycall")
                    
                                WHILE NOT HAS_SCRIPT_LOADED("emergencycall")
                                WAIT(0)
                                ENDWHILE
                                
                                START_NEW_SCRIPT("emergencycall", MINI_STACK_SIZE)
                            ENDIF
                            callStage = callwaitforscript
                            
                        ENDIF

                    ELSE
                    #endif                         
                    
                    
                        #if IS_DEBUG_BUILD

                            cdPrintstring ("ECLauncher - Now using_V_content functionality...")
                            cdPrintnl()

                        #endif

                                                   

                        //
                        IF PLAYER_CALL_EMERGENCY_SERVICES_USING_V_CONTENT_IN_DLC (emergencyPedStruct, CHAR_CALL911, "LOCAUD", "LOC_CALLQ", callPriority, "CELL_601", "LOC_CALLA", "LOC_CALLF", "LOC_CALLP") //The calls are already exported and in the XLS sheet.
                            
                            IF NOT ARE_EMERGENCY_CALLS_SUPPRESSED()
                                REQUEST_SCRIPT("emergencycall")
                    
                                WHILE NOT HAS_SCRIPT_LOADED("emergencycall")
                                WAIT(0)
                                ENDWHILE
                                
                                START_NEW_SCRIPT("emergencycall", MINI_STACK_SIZE)
                            ENDIF
                            callStage = callwaitforscript
                            
                        ENDIF

                    
                    #if USE_TU_CHANGES
                    ENDIF
                    #endif

                    
                ENDIF
        
            ENDIF
            
        BREAK
        
        CASE callwaitforscript
            IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("emergencycall")) = 0   
                IF NOT IS_CALLING_CONTACT (CHAR_CALL911)
                    emergency_main_stages = CLEANUP
                ENDIF
            ENDIF
        BREAK
             
    ENDSWITCH
    
    
ENDPROC       

PROC checkForSuppression()
    IF ARE_EMERGENCY_CALLS_SUPPRESSED()
        suppressingScript = EMERGENCY_CALLS_SUPPRESSING_SCRIPT()
        IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(suppressingScript)) = 0
            RELEASE_SUPPRESSED_EMERGENCY_CALLS()
        ELSE
            g_bForceEmergencyCallAnswerphone = TRUE
        ENDIF
    ELSE
        g_bForceEmergencyCallAnswerphone = FALSE
    ENDIF
ENDPROC
               
PROC emergencyCall_Cleanup()    //Unload assests and reset flags
    IF g_bEmergencyCallPriorityIsHigh
        FORCE_EMERGENCY_CALL_PRIORITY_HIGH(FALSE)
    ENDIF
    emergency_main_stages = WAIT_FOR_CALL
    callStage = callphoneup
    PRINTSTRING ("emergencycalllauncher.sc - Cleanup")
    PRINTNL()
ENDPROC

PROC SETUP_DEBUG()
    #if IS_DEBUG_BUILD
    START_WIDGET_GROUP("Emergency Call Launcher")           
        ADD_WIDGET_BOOL("Enable or Disable Emergency Call Toggle", bDisableCalls)
    STOP_WIDGET_GROUP()
    #ENDIF
ENDPROC 

PROC RUN_DEBUG()
    IF bDisableCalls
        IF ARE_EMERGENCY_CALLS_SUPPRESSED()
            RELEASE_SUPPRESSED_EMERGENCY_CALLS()
        ELSE
            SUPPRESS_EMERGENCY_CALLS()
        ENDIF
        bDisableCalls = FALSE
    ENDIF
ENDPROC


             
// Mission Script -----------------------------------------//


SCRIPT 

//Copied from bootycallhandler.sc by DavidG 01/02/2013
// KEITH 21/12/12: This script now terminates when the game is moving between SP and MP and back again, so need different cleanup checks.
//                  Also had to remove the NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME() call - it clashes with FORCE_CLEANUP_FLAG_SP_TO_MP leading to an assert.
BOOL isInMultiplayer = NETWORK_IS_GAME_IN_PROGRESS()

// This script needs to cleanup only when the game moves from SP to MP
IF NOT (isInMultiplayer)
    // KEITH 21/12/12: Terminate this script when moving from SP to MP
    IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_MAGDEMO)
        #IF IS_DEBUG_BUILD
            PRINTSTRING("...EmergencyCallHandler.sc has been forced to cleanup (SP to MP)")
            PRINTNL()
        #ENDIF
        
        TERMINATE_THIS_THREAD() 
    ENDIF
ELSE
    // Ensure this script persists during network game
    NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
ENDIF


ADD_PED_FOR_DIALOGUE(emergencyPedStruct, 1, NULL, "EMSDispatch")
SETUP_DEBUG()
// Mission Loop -------------------------------------------//
WHILE TRUE
    WAIT(0)






    
    //Copied from bootycallhandler.sc by DavidG 01/02/2013
    // KEITH 21/12/12: Terminate this script when moving from MP to SP
    IF (isInMultiplayer)
        IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
            #IF IS_DEBUG_BUILD
                PRINTSTRING("...EmergencyCallHandler.sc has been forced to cleanup (MP to SP)")
                PRINTNL()
            #ENDIF
            
            TERMINATE_THIS_THREAD()
        ENDIF
    ENDIF
    
    RUN_DEBUG()
    IF IS_PLAYER_PLAYING(PLAYER_ID())

        SWITCH emergency_main_stages
        
            CASE WAIT_FOR_CALL
                take_call()
                
                checkForSuppression()
            BREAK
            
            CASE CLEANUP
                emergencyCall_Cleanup()
            BREAK
                        
        ENDSWITCH
    
    ENDIF
    

    /*
    IF IS_CURRENT_MISSION_DLC()

        DISPLAY_TEXT_WITH_LITERAL_STRING (0.10, 0.14, "STRING", "ECL - Is CurrMiss_DLC") 

    ELSE

        DISPLAY_TEXT_WITH_LITERAL_STRING (0.10, 0.14, "STRING", "ECL - Not CurrMiss_DLC") 
    
    ENDIF
    */



ENDWHILE
ENDSCRIPT   



