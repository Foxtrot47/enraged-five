// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	cablecar.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		: 	Cable Car Handler Script
//
// *****************************************************************************************
// *****************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//----------------------
//	INCLUDES
//----------------------
USING "rgeneral_include.sch"
USING "rc_helper_functions.sch"
USING "RC_asset_public.sch"
USING "RC_Setup_public.sch"
USING "RC_Camera_Public.sch"
USING "net_fps_cam.sch"
USING "friendactivity_public.sch"
 
//----------------------
//	ENUM
//----------------------
ENUM eCableCarState
	CABLECAR_NULLSTATE,
	CABLECAR_WAITING,
	CABLECAR_DEPARTING, 	// close the door
	CABLECAR_SPEEDUP,
	CABLECAR_MOVING,
	CABLECAR_SLOWDOWN,
	CABLECAR_ARRIVING,		// open the doors
	CABLECAR_ARRIVED,
	CABLECAR_PAUSESTART,
	CABLECAR_PAUSED,
	CABLECAR_PAUSEEND,
	CABLECAR_DEAD,
	CABLECAR_REGEN
ENDENUM

ENUM eCableCarDirection
	CABLECAR_MOVEUP,
	CABLECAR_MOVEDOWN
ENDENUM

ENUM eCableCarDestination
	CABLECAR_LOWDOCK,
	CABLECAR_HIGHDOCK
ENDENUM 

ENUM eCableCarHaloMode
	APPROACH_MODE,
	CANTAFFORD_MODE,
	DENIED_MODE,	// for doing certain checks
	TRIGGER_MODE,	// pressing right with enough
	LAUNCH_MODE,
	CLOSED_MODE,
	DAMAGED_MODE,	// if the car is too broken to go in
	WANTED_MODE
ENDENUM

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_CABLEWIRE_POINTS 		15
CONST_INT MAX_CABLEWIRES 			2
CONST_INT COST_CABLECAR				10

CONST_INT CABLE_WIRE_LEFT			0
CONST_INT CABLE_WIRE_RIGHT			1

CONST_INT CABLE_DOCK_LOW			0
CONST_INT CABLE_DOCK_HIGH			1

CONST_FLOAT CABLE_CAR_SLOW_TIME		10.0
CONST_FLOAT CABLE_CAR_WIRE_OFFSET	-0.2
CONST_FLOAT WIRE_SHIFT_INTERPOLATE	0.0

CONST_FLOAT CABLE_CAR_RESTART_MIN_X       -800.0            // if player x is smaller than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MAX_X       640.0       // if player x is smaller than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MIN_Y       5300.0            // if player y is bigger than this - restart
CONST_FLOAT CABLE_CAR_RESTART_MAX_Y       5800.0            // if player y is bigger than this - restart

CONST_FLOAT CABLE_CAR_SHUTDOWN_MIN_X      -900.0            // if player x is smaller than this - suspsend
CONST_FLOAT CABLE_CAR_SHUTDOWN_MAX_X      740.0       // if player x is bigger than this - suspsend
CONST_FLOAT CABLE_CAR_SHUTDOWN_MIN_Y      5200.0            // if player y is smaller than this - suspsend
CONST_FLOAT CABLE_CAR_SHUTDOWN_MAX_Y      5900.0            // if player y is bigger than this - suspsend

CONST_INT CABLECAR_ASSET_SLOT		0
CONST_INT CABLECARDOORL_ASSET_SLOT	1
CONST_INT CABLECARDOORR_ASSET_SLOT	2
CONST_INT CABLECARSND_ASSET_SLOT	3
CONST_INT CABLECARANIM_ASSET_SLOT	4

CONST_FLOAT CABLE_CAR_DOOR_OPEN_LIMIT 0.88

STREAMVOL_ID streamVolCam
BOOL bNewCam = TRUE

//----------------------
//	STRUCTS
//----------------------
STRUCT CABLECAR_WIRE
	VECTOR vPoints[MAX_CABLEWIRE_POINTS]
	INT iPointsUsed
	
	#IF IS_DEBUG_BUILD
		INT iDebugPoint
		FLOAT fDebugInterpolate
		VECTOR vWireDebugPoint
		BOOL bWarpCarToDebugPoint
	#ENDIF
	
	BOOL bShowWire = FALSE
	FLOAT fRopeLength
ENDSTRUCT

STRUCT CABLE_CAR
	eCableCarState carState
	eCableCarHaloMode haloState
	
	OBJECT_INDEX cablecarID
	OBJECT_INDEX lDoor[2]	// doors on the left side not the two left sided doors (confusing...)
	OBJECT_INDEX rDoor[2]
	
	BOOL bIsMoving = FALSE
	BOOL bHelpShown = FALSE
	BOOL bIsPlayerIn = FALSE
	
	VECTOR vPosition			// position along the wire - vehicle has offset added to it
	INT iWireIndex = 0
	FLOAT fSpeed = 0.0
	FLOAT fSpdSgn = 1.0
	
	INT iWireSegment = 0
	FLOAT fTValue = 0.0
	FLOAT fDistTravelled = 0.0
	
	VEHICLE_INDEX storedVehicles[2]
	INT iStoredVehicleCount = 0
	INT iDepartTime = 0
	INT iDiedTime = 0
	INT iJourneyTimer = 0
	INT iRunningSound = -1
	INT iRainSound = -1
	
	FLOAT fDistToNextPoint
	FLOAT fDoorValue = 0.0
	
	BOOL bIsPaused = FALSE
	BOOL bIsPlayerOnTop = FALSE
	BOOL bIsPlayerRiding = FALSE
	
	#IF IS_DEBUG_BUILD
		INT iHaloStateDebug = 0
		INT iCarStateDebug = 0
		BOOL bDebugPause = FALSE
	#ENDIF
ENDSTRUCT

//----------------------
//	VARIABLES
//----------------------
VECTOR vLowDockPosition = <<-740.3, 5594.5, 41.2>>
VECTOR vHighDockPosition = <<446.8, 5571.1, 780.7>>
	
VECTOR vCableCarPlayerAttachOffset = <<0, 2.48, -5.3>>
VECTOR vCableCarLeftExitOffset = <<1.3, 0, -5.3>>
VECTOR vCableCarRightExitOffset = <<-1.3, 0, -5.3>>

FLOAT fTimeStepFactor = 1.0
FLOAT fCableCarMaxSpeed = 10.0
FLOAT fCableCarAcceleration = 0.65
INT iCableCarRegenTime = 120000
INT iCableCarDockWaitTime = 35000

CABLECAR_WIRE cableWire[MAX_CABLEWIRES]		// this assumes there are only 2 cable car wires in the entire game
MODEL_NAMES cableCarModel = P_CABLECAR_S
MODEL_NAMES cableCarDoorLModel = P_CABLECAR_S_DOOR_L
MODEL_NAMES cableCarDoorRModel = P_CABLECAR_S_DOOR_R

FLOAT fCamShiftHack = -15.0
FIRST_PERSON_CAM_STRUCT fpsCam
BOOL bFPSMode = FALSE
BOOL bSkipped = FALSE
BOOL bSimulateMapSkip = TRUE

ASSET_REQUESTER asRequest

CABLE_CAR cableCarLeft
CABLE_CAR cableCarRight
CAMERA_STRUCT externalCamera
CAMERA_INDEX cameraID
INT iCameraPointIndex = 0

FLOAT fImranFactor = 3.0
BOOL bSRLOK = FALSE
FLOAT fSrlTime = 0.0
INT iRevealCounter = 0

enumActivityLocation activityLoc = ALOC_suspendFriends
		
#IF IS_DEBUG_BUILD
	INT iActiveCamera = 0
	FLOAT fDebugWireVertexSize = 0.0625
	FLOAT fDebugWireVertexHeight = 10.0
	BOOL bExitScript = FALSE 
	BOOL bWarpToLowDock = FALSE 
	BOOL bWarpToHighDock = FALSE
	BOOL bBlipCars = FALSE
	BLIP_INDEX blipID[2]
	BOOL bCreateAtHighDock[2]
	BOOL bCreateAtLowDock[2]
	
	INT iExternalCameraTest
	INT iTestInterptime
	BOOL bExternalCamOn
	BOOL bExternalCamOff
	BOOL bInterpolateBackwards = FALSE
	BOOL bExternalCamAlternate
	BOOL bIsCamStatic
	
	BOOL bDoorSlideTest
	FLOAT fDoorSlideValue = 0
	BOOL bShowLoadSceneArea = TRUE
	
#ENDIF

BOOL bIsOnMission = FALSE
BOOL bNoCarMsgShown = FALSE


INT iLoadSceneTimeOut = 0
INT iCurrentLoadSceneIndex = -1
VECTOR vSceneLoadPosition
VECTOR vSceneLoadDirection
FLOAT fSceneLoadFarClip
BOOL bLoadSceneOK = FALSE
BOOL bUseSRLS = FALSE
BOOL bUseFocusChange = FALSE

//INT iUpdateWaitMsgTimer = 0

//----------------------
//	MISC FUNCTIONS
//----------------------

/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC LOAD_CABLE_CAR_ASSETS()
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Loading Assets")
	CLEAR_ASSET_REQUESTER(asRequest)
	ADD_ASSET_REQUEST_FOR_MODEL(asRequest, CABLECAR_ASSET_SLOT, cableCarModel)
	ADD_ASSET_REQUEST_FOR_MODEL(asRequest, CABLECARDOORL_ASSET_SLOT, cableCarDoorLModel)
	ADD_ASSET_REQUEST_FOR_MODEL(asRequest, CABLECARDOORR_ASSET_SLOT, cableCarDoorRModel)
	ADD_ASSET_REQUEST_FROM_STRING(asRequest, CABLECARSND_ASSET_SLOT, ASSET_AUDIOBANK, "CABLE_CAR", "CABLE_CAR_SOUNDS")
	ADD_ASSET_REQUEST_FROM_STRING(asRequest, CABLECARANIM_ASSET_SLOT, ASSET_ANIMDICT, "P_CableCar_S")
	
	WHILE NOT HAVE_ASSET_REQUESTS_LOADED(asRequest)
		WAIT(0)
	ENDWHILE
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Loaded Assets")
ENDPROC

PROC UNLOAD_CABLE_CAR_MODEL_ASSETS()
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Unloading Model Assets")
	_UNLOAD_ASSET_FROM_REQUESTER(asRequest, CABLECAR_ASSET_SLOT)
	_UNLOAD_ASSET_FROM_REQUESTER(asRequest, CABLECARDOORL_ASSET_SLOT)
	_UNLOAD_ASSET_FROM_REQUESTER(asRequest, CABLECARDOORR_ASSET_SLOT)
ENDPROC

//----------------------
//	CAMERA FUNCTIONS
//----------------------

/// PURPOSE:
/// 	Enable First Person Camera
PROC ENABLE_CABLE_CAR_FIRST_PERSON(eCableCarDirection dir = CABLECAR_MOVEDOWN)
	VECTOR v = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0, 0, 0>>)
	
	fpsCam.iLookXLimit = 60
	fpsCam.iLookYLimit = 20
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	
	IF (dir = CABLECAR_MOVEDOWN)
		INIT_FIRST_PERSON_CAMERA(fpsCam, v, GET_ENTITY_ROTATION(PLAYER_PED_ID()) + <<fCamShiftHack, 0, 0>>, 50.0, fpsCam.iLookXLimit, fpsCam.iLookYLimit)
	ELSE
		INIT_FIRST_PERSON_CAMERA(fpsCam, v, GET_ENTITY_ROTATION(PLAYER_PED_ID()), 50.0, fpsCam.iLookXLimit, fpsCam.iLookYLimit)
	ENDIF
	
	ATTACH_CAM_TO_ENTITY(fpsCam.theCam, PLAYER_PED_ID(), <<0, 0.2, 0.5>>)	
//	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)	// SamH: Removing as requested in b*1776373. Problem is fixed in code now.
	
	bNewCam = TRUE
	bFPSMode = TRUE
ENDPROC

/// PURPOSE:
/// 	Disable First Person Camera
PROC DISABLE_CABLE_CAR_FIRST_PERSON()
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
//	CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)		// SamH: Removing as requested in b*1776373. Problem is fixed in code now.
	ENABLE_CAMERA_STRUCT(externalCamera, cameraID)
	bNewCam = TRUE
	bFPSMode = FALSE
ENDPROC
	

PROC SET_EXTERNAL_CAMERA_STRUCT(CAMERA_STRUCT &cst, INT ind, BOOL bUseAlternateShot = FALSE)
	SWITCH (ind)
		CASE 0
			IF (bUseAlternateShot = FALSE)
				SETUP_CAMERA_STRUCT(cst, <<-741.9, 5602.2, 42.3>>, <<-2.2, 0.0, -130.5>>)
				SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-741.9, 5602.2, 42.3>>, <<16.9, 0.0, -116.8>>, 4500)
				SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
			ELSE
				/*
				SETUP_CAMERA_STRUCT(cst, <<-746.1391, 5567.7998, 53.6983>>, <<-12.1941, 0.0000, -44.5173>>)
				SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
				*/
				
				SETUP_CAMERA_STRUCT(cst, <<-733.0331, 5557.0503, 54.3150>>, <<-13.2766, 0.0000, 26.2995>>)
				SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-733.0331, 5557.0503, 54.3050>>, <<3.6561, 0.0000, -49.3377>>, 15000)
				SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
			ENDIF
		BREAK
		CASE 1
			SETUP_CAMERA_STRUCT(cst, <<-674.4, 5644.0, 40.1>>, <<7.5, 0.0, 148.3>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-674.4, 5644.0, 40.1>>, <<7.5, 0.0, -133.2>>, 12000)
			SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
		BREAK
		CASE 2
			IF (bUseAlternateShot = FALSE)
				SETUP_CAMERA_STRUCT(cst, <<-575.3, 5584.3, 81.1>>, <<-25.8, 0.0, 53.1>>)
				SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-575.4, 5590.0, 81.1>>, <<-25.8, -0.0, 53.1>>, 6000)
				SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
			ELSE
				SETUP_CAMERA_STRUCT(cst,  <<-575.3, 5584.3, 81.1>>, <<-25.8, 0.0, 53.1>>)
				SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
			ENDIF
		BREAK		
		
		
		CASE 3
			IF (bUseAlternateShot = FALSE)
				SETUP_CAMERA_STRUCT(cst, <<-536.0, 5524.6, 65.4>>, <<3.7, 0.0, 25.1>>)
				SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
			ELSE
				SETUP_CAMERA_STRUCT(cst, <<-627.3716, 5543.3877, 51.0491>>, <<9.2403, -0.0000, -30.7826>>)
				// <<-607.9493, 5532.4712, 52.6296>>, <<19.1993, -0.0000, -26.6786>>) //<<-536.5602, 5524.1216, 65.3995>>, <<3.8445, 0.0000, 40.0685>>)
				
				// backup <<-628.0014, 5534.6714, 52.2195>>, <<11.8051, -0.0000, -55.3615>>
				SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
			ENDIF
		BREAK		
		CASE 4
			SETUP_CAMERA_STRUCT(cst, <<-580.2, 5589.0, 77.7>>, <<26.3, 0.0, -53.0>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
		BREAK	
		
		
		CASE 5
			SETUP_CAMERA_STRUCT(cst, <<-476.7, 5650.8, 59.9>>, <<6.8, 0.0, 117.7>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
		BREAK	
		CASE 6
			SETUP_CAMERA_STRUCT(cst, <<-476.7, 5650.8, 59.9>>, <<29.8, 0.0, -170.8>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
		BREAK		
		CASE 7
			SETUP_CAMERA_STRUCT(cst, <<-428.1, 5383.4, 102.3>>, <<-4.1, 0.0, -18.6>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 45.0)
		BREAK		
		

		CASE 8
			//SETUP_CAMERA_STRUCT(cst, <<-458.7833, 5337.7959, 91.2493>>, <<7.9000, 0.0000, 4.6000>>)
			SETUP_CAMERA_STRUCT(cst, <<-475.9374, 5375.9893, 94.2374>>, <<3.4297, 0.0000, 10.0545>>)

			SETUP_CAMERA_STRUCT_FOV(cst, 30.0)
		BREAK
		CASE 9
			SETUP_CAMERA_STRUCT(cst, <<-433.4, 5643.9, 121.2>>, <<26.5, 0.0, -120.0>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 44.0)
		BREAK
		CASE 10
			SETUP_CAMERA_STRUCT(cst, <<-508.9219, 5674.6777, 74.7000>>, <<27.0000, -0.0000, -121.7744>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 44.0)
		BREAK		
		
		
		
		CASE 11
			SETUP_CAMERA_STRUCT(cst, <<-281.3, 5596.8, 226.0>>, <<31.9, 0.0, -120.4>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-282.5, 5584.3, 226.0>>, <<33.5, 0.0, -105.0>>, 6000)
			SETUP_CAMERA_STRUCT_FOV(cst, 44.0)
		BREAK
		CASE 12
			SETUP_CAMERA_STRUCT(cst, <<-56.5844, 5560.8516, 311.4859>>, <<-15.2417, -0.0000, 108.7200>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK
		CASE 13
			SETUP_CAMERA_STRUCT(cst, <<-88.8881, 5539.6621, 280.1666>>, <<49.9000, 0.0000, -19.4625>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK	
	
	
	
		CASE 14
			SETUP_CAMERA_STRUCT(cst, <<-6.2591, 5609.3184, 402.8656>>, <<-0.9222, 0.0000, 153.9538>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-5.9107, 5609.6133, 401.1573>>, <<-11.4635, 0.0000, 130.2651>>, 10000)
			SETUP_CAMERA_STRUCT_FOV(cst, 55.0)
		BREAK
		CASE 15
			SETUP_CAMERA_STRUCT(cst, <<0.3, 5584.0, 422.6>>, <<26.5, 0.0, -112.1>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK
		CASE 16
			SETUP_CAMERA_STRUCT(cst, <<-69.8260, 5595.7646, 390.4564>>, <<25.1669, 0.0000, -110.1190>>) //<<0.8, 5591.6, 422.6>>, <<26.5, 0.0, -112.1>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<-68.5961, 5595.1831, 386.8474>>, <<20.6269, 0.0000, -115.1379>>, 10000) // <0.8, 5574.0, 422.6>>, <<26.5, 0.0, -112.1>>, 10000)
			SETUP_CAMERA_STRUCT_FOV(cst, 40.43)
		BREAK
	
	
	
	
		CASE 17
			SETUP_CAMERA_STRUCT(cst, <<250.3515, 5575.8872, 617.4193>>, <<-43.3782, 0.0000, 90.4725>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<247.1179, 5575.8872, 614.3633>>, <<-40.0590, 0.0000, 90.4725>>, 10000)
			SETUP_CAMERA_STRUCT_FOV(cst, 45.0)
		BREAK
		CASE 18
			SETUP_CAMERA_STRUCT(cst, <<234.6, 5588.8, 596.5>>, <<33.1, 0.0, -126.3>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK
		CASE 19
			SETUP_CAMERA_STRUCT(cst, <<215.1628, 5595.8301, 615.1234>>, <<22.9775, -0.0000, -111.6452>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<221.7997, 5593.1108, 597.7890>>, <<17.1025, -0.0000, -121.5107>>, 16000)
			SETUP_CAMERA_STRUCT_FOV(cst, 45.0)
		BREAK
	
	
	
		CASE 20
			SETUP_CAMERA_STRUCT(cst, <<351.2, 5522.4, 695.0>>, <<37.9, 0.0, -72.5>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK		
		CASE 21
			SETUP_CAMERA_STRUCT(cst, <<373.0, 5467.3, 719.7>>, <<18.8, 0.0, -45.0>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 50.0)
		BREAK	
		CASE 22
			SETUP_CAMERA_STRUCT(cst, <<351.9, 5504.4, 709.1>>, <<28.0, 0.0, -61.2>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 29.0)
		BREAK	
		CASE 23
			SETUP_CAMERA_STRUCT(cst, <<462.0, 5599.4, 805.6>>, <<-41.0, 0.0, 120.2>>)
			SETUP_CAMERA_STRUCT_FOV(cst, 40.0)
		BREAK	
		CASE 24
			
			SETUP_CAMERA_STRUCT(cst, <<462.1715, 5634.8574, 803.8868>>, <<-25.3865, -0.0000, 137.5704>>)
			SETUP_CAMERA_STRUCT_END_SHOT(cst, <<470.8885, 5624.8687, 803.6818>>, <<-27.3184, 0.0000, 124.5155>>, 8000)
			SETUP_CAMERA_STRUCT_FOV(cst, 40.0)
		BREAK	
	ENDSWITCH
ENDPROC

PROC SET_EXTERNAL_CAMERA(CAMERA_STRUCT &cst, INT ind, BOOL bUseAlternateShot = FALSE)
	SET_EXTERNAL_CAMERA_STRUCT(cst, ind, bUseAlternateShot)
	#IF IS_DEBUG_BUILD
	iActiveCamera = ind
	#ENDIF
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC NEW_LOAD_SCENE_FOR_CAMERA_STRUCT(CAMERA_STRUCT &cst, INT ind)
	EXIT
	
	VECTOR v
	IF (iCurrentLoadSceneIndex <> ind)
		NEW_LOAD_SCENE_STOP()
		v = CONVERT_ROTATION_TO_DIRECTION_VECTOR(cst.vRotation[0])
		IF NEW_LOAD_SCENE_START_SPHERE(cst.vPosition[0] + (v * 140.0), 141.0, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)                
			CPRINTLN(DEBUG_AMBIENT, "[CABLE CAR]:NEW SCENE LOAD FOR:", cst.vPosition[0], " CAM INDEX:", ind)
			iLoadSceneTimeOut = GET_GAME_TIMER() + 10000
			iCurrentLoadSceneIndex = ind
			vSceneLoadPosition = cst.vPosition[0]
			vSceneLoadDirection = v
			fSceneLoadFarClip = 100.0
			bLoadSceneOK = FALSE
		ENDIF
	ENDIF
ENDPROC


PROC UPDATE_NEW_LOAD_SCENE_FOR_CABLE_CAR_CAMERA()
	EXIT
	
	IF (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()) 
		CPRINTLN(DEBUG_AMBIENT, "[CABLE_CAR]: NEW SCENE LOADED OK")
		NEW_LOAD_SCENE_STOP()
		bLoadSceneOK = TRUE
		iLoadSceneTimeOut = 0
		EXIT
	ENDIF
	
	IF (iLoadSceneTimeOut > 0) AND (GET_GAME_TIMER() > iLoadSceneTimeOut)
		CPRINTLN(DEBUG_AMBIENT, "[CABLE_CAR]: LOAD SCENE TIMED OUT")
		iLoadSceneTimeOut = 0
		EXIT
	ENDIF
ENDPROC

PROC CREATE_STREAMVOL_FOR_NEXT_CAMERA(VECTOR vCamPos, VECTOR vCamRot, FLOAT fFarClip = 200.0)
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		EXIT
	ENDIF
	
	IF (bUseSRLS = TRUE)
		EXIT 
	ENDIF
	
	IF bNewCam
		IF STREAMVOL_IS_VALID(streamVolCam)
			STREAMVOL_DELETE(streamVolCam)
		ENDIF
		
		CPRINTLN(DEBUG_AMBIENT, "CREATE_STREAMVOL_FOR_NEXT_CAMERA: Creating frustum")
		streamVolCam = STREAMVOL_CREATE_FRUSTUM(vCamPos, CONVERT_ROTATION_TO_DIRECTION_VECTOR(vCamRot), fFarClip, FLAG_MAPDATA, STREAMVOL_LOD_FLAG_ALL)
		bNewCam = FALSE
	ELSE
		IF STREAMVOL_IS_VALID(streamVolCam)
			IF IS_STREAMVOL_ACTIVE()
				IF STREAMVOL_HAS_LOADED(streamVolCam)
					CPRINTLN(DEBUG_AMBIENT, "CREATE_STREAMVOL_FOR_NEXT_CAMERA: Successful load")
					STREAMVOL_DELETE(streamVolCam)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

/*
PROC CREATE_STREAMVOL_FOR_CAMERA_STRUCT(CAMERA_STRUCT &cst)
	IF (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()) OR (GET_GAME_R
	
	IF STREAMVOL_IS_VALID(cameraStreamVol)
		CPRINTLN(DEBUG_AMBIENT, "DELETED STREAMING VOL")
		STREAMVOL_DELETE(cameraStreamVol)
	ENDIF
	
	cameraStreamVol = STREAMVOL_CREATE_SPHERE(cst.vPosition[0], 30.0, FLAG_MAPDATA, STREAMVOL_LOD_FLAG_HIGH)
	
	//STREAMVOL_CREATE_FRUSTUM(cst.vPosition[0], CONVERT_ROTATION_TO_DIRECTION_VECTOR(cst.vRotation[0]), 200.0, FLAG_MAPDATA)
	IF STREAMVOL_IS_VALID(cameraStreamVol) 
		CPRINTLN(DEBUG_AMBIENT, "CREATED STREAMING VOL AT ", cst.vPosition[0])
		STREAMVOL_DELETE(cameraStreamVol)
	ENDIF
ENDPROC
*/

//----------------------
//	WIRE FUNCTIONS
//----------------------

/// PURPOSE:
///    Adds a point to the spline list
/// PARAMS:
///    x - xcoord
///    y - ycoord
///    z - zcoord
PROC ADD_CABLE_CAR_WIRE_POINT(CABLECAR_WIRE &wire, FLOAT x, FLOAT y, FLOAT z)
	IF (wire.iPointsUsed >= COUNT_OF(wire.vPoints))
		SCRIPT_ASSERT("INCREASE ARRAY SIZE OF CABLECAR_WIRE::vPoints")
		EXIT
	ENDIF
	
	wire.vPoints[wire.iPointsUsed].x = x
	wire.vPoints[wire.iPointsUsed].y = y 
	wire.vPoints[wire.iPointsUsed].z = z 
	wire.iPointsUsed ++
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC CALCULATE_CABLE_CAR_WIRE_LENGTH(CABLECAR_WIRE &wire)
	INT i
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CALCULATE_CABLE_CAR_WIRE_LENGTH()")
	wire.fRopeLength = 0
	
	REPEAT wire.iPointsUsed - 2 i
		wire.fRopeLength += GET_DISTANCE_BETWEEN_COORDS(wire.vPoints[i], wire.vPoints[i + 1])
	ENDREPEAT
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CALCULATE_CABLE_CAR_WIRE_LENGTH() - Wire Length:", wire.fRopeLength)
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC SETUP_CABLE_CAR_WIRE_LEFT(CABLECAR_WIRE &wire)
	wire.iPointsUsed = 0
	
	ADD_CABLE_CAR_WIRE_POINT(wire, -740.911, 5599.341,  47.25)
	ADD_CABLE_CAR_WIRE_POINT(wire, -739.557, 5599.346,  46.997)
	ADD_CABLE_CAR_WIRE_POINT(wire, -581.009, 5596.517,  77.379)
	ADD_CABLE_CAR_WIRE_POINT(wire, -575.717, 5596.388,  79.220)
	ADD_CABLE_CAR_WIRE_POINT(wire, -273.805, 5590.844, 240.795)
	ADD_CABLE_CAR_WIRE_POINT(wire, -268.707, 5590.744, 243.395)
	ADD_CABLE_CAR_WIRE_POINT(wire,    6.896, 5585.668, 423.614)
	ADD_CABLE_CAR_WIRE_POINT(wire,   11.774, 5585.591, 426.711)
	ADD_CABLE_CAR_WIRE_POINT(wire,  236.820, 5581.445, 599.642)
	ADD_CABLE_CAR_WIRE_POINT(wire, 241.365, 5581.369, 603.183)
	ADD_CABLE_CAR_WIRE_POINT(wire,  412.855, 5578.216, 774.401)
	ADD_CABLE_CAR_WIRE_POINT(wire, 417.541, 5578.124, 777.688)
	ADD_CABLE_CAR_WIRE_POINT(wire,  444.930, 5577.589, 786.535)
	ADD_CABLE_CAR_WIRE_POINT(wire,  446.288, 5577.590, 786.750)
		
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:SETUP_CABLE_CAR_WIRE_LEFT() - Cable Car Left Wire Setup")
	CALCULATE_CABLE_CAR_WIRE_LENGTH(wire)
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED(CABLECAR_WIRE &wire)
	wire.iPointsUsed = 0 
	
	ADD_CABLE_CAR_WIRE_POINT(wire,  446.291, 5566.377, 786.750)
	ADD_CABLE_CAR_WIRE_POINT(wire, 444.937, 5566.383, 786.551)
	ADD_CABLE_CAR_WIRE_POINT(wire,  417.371, 5567.001, 777.708)
	ADD_CABLE_CAR_WIRE_POINT(wire,  412.661, 5567.085, 774.439)
	ADD_CABLE_CAR_WIRE_POINT(wire,  241.310, 5570.594, 603.137)
	ADD_CABLE_CAR_WIRE_POINT(wire, 236.821, 5570.663, 599.561)
	ADD_CABLE_CAR_WIRE_POINT(wire, 11.350, 5575.298, 426.629)
	ADD_CABLE_CAR_WIRE_POINT(wire, 6.575, 5575.391, 423.570)
	ADD_CABLE_CAR_WIRE_POINT(wire, -268.965, 5580.996, 243.386)
	ADD_CABLE_CAR_WIRE_POINT(wire, -273.993, 5581.124, 240.808)
	ADD_CABLE_CAR_WIRE_POINT(wire, -575.898, 5587.286,  79.251)
	ADD_CABLE_CAR_WIRE_POINT(wire, -581.321, 5587.400,  77.348)
	ADD_CABLE_CAR_WIRE_POINT(wire, -739.646, 5590.614,  47.006)
	ADD_CABLE_CAR_WIRE_POINT(wire, -740.970, 5590.617,  47.306)
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED() - Cable Car Right Wire Flipped Setup")
	CALCULATE_CABLE_CAR_WIRE_LENGTH(wire)
ENDPROC

/// PURPOSE:
///    Draw Wire with debug
/// PARAMS:
///    wire - wire reference
PROC DRAW_DEBUG_CABLE_CAR_WIRE(CABLECAR_WIRE &wire, INT r = 0, INT g = 0, INT b = 255, INT a = 255) 
	INT i 
	
	IF (wire.bShowWire = FALSE)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF (wire.iDebugPoint > (wire.iPointsUsed - 1))
		wire.iDebugPoint = (wire.iPointsUsed - 1)
	ENDIF
	#ENDIF
	
	REPEAT wire.iPointsUsed i
		IF (i > 0)
			IF ((i % 2) = 0)
				DRAW_DEBUG_LINE(wire.vPoints[i], wire.vPoints[i - 1], r, g, b, a)
			ELSE
				DRAW_DEBUG_LINE(wire.vPoints[i], wire.vPoints[i - 1], r, 255, b, a)
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF (wire.iDebugPoint <= (wire.iPointsUsed - 2))
			wire.vWireDebugPoint = INTERPOLATE_VECTOR(wire.vPoints[wire.iDebugPoint], wire.vPoints[wire.iDebugPoint + 1], wire.fDebugInterpolate, TRUE)
		ELSE
			wire.vWireDebugPoint = wire.vPoints[wire.iDebugPoint]
		ENDIF
		
		DRAW_DEBUG_LINE(wire.vWireDebugPoint + <<0, 0, fDebugWireVertexHeight>>, wire.vWireDebugPoint - <<0, 0, fDebugWireVertexHeight>>, 0, 255, 0, 255)
		DRAW_DEBUG_SPHERE(wire.vWireDebugPoint, fDebugWireVertexSize)
		DRAW_DEBUG_CIRCLE(wire.vWireDebugPoint, 1.0)
	#ENDIF
	
ENDPROC

/// PURPOSE:
///    intepolate across wire and gives us a vector
/// PARAMS:
///    wire - wire
///    seg - segment number 
///    t -  value across segment from 0 to 1
///    out - return vector
/// RETURNS:
///    true if this works
FUNC VECTOR GET_WIRE_POSITION(CABLECAR_WIRE &wire, INT seg, FLOAT &t)
	IF (seg < 0)
		t = 0.0 
		RETURN wire.vPoints[0]
	ENDIF
	
	IF (seg >= wire.iPointsUsed - 1)
		t = 0.0
		RETURN wire.vPoints[wire.iPointsUsed - 1]
	ENDIF
	
	// clamp t
	IF (t < 0.0) 
		t = 0.0 
	ENDIF
	
	IF (t > 1.0) 
		t = 1.0 
	ENDIF
	
	RETURN INTERPOLATE_VECTOR(wire.vPoints[seg], wire.vPoints[seg + 1], t, TRUE)
ENDFUNC

//----------------------
//	CAR FUNCTIONS
//----------------------

/// PURPOSE:
///    Zeros out variables
/// PARAMS:
///    car - 
PROC ZERO_CABLE_CAR_STRUCT(CABLE_CAR &car)
	car.fDistTravelled = 0
	car.bIsMoving = FALSE
	car.fSpeed = 0.0
	car.bHelpShown = FALSE
	car.carState = CABLECAR_NULLSTATE
	car.bIsPaused = FALSE
ENDPROC
	
FUNC BOOL IS_IT_RAINING_OUTSIDE()
	IF IS_NEXT_WEATHER_TYPE("RAIN")
	OR IS_NEXT_WEATHER_TYPE("THUNDER")
	OR IS_PREV_WEATHER_TYPE("RAIN")
	OR IS_PREV_WEATHER_TYPE("THUNDER")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_INTERNAL_RAIN_SOUND_ENABLED(CABLE_CAR &car, BOOL bOk = TRUE)
	SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRainSound)
	IF (bOk)
		#IF IS_NEXTGEN_BUILD
			IF IS_IT_RAINING_OUTSIDE()	
				car.iRainSound = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(car.iRainSound, "Interior_Rain", PLAYER_PED_ID(), "CABLE_CAR_SOUNDS")
			ENDIF
		#ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Rain Sound is:", bOk)
ENDPROC

/// PURPOSE:
///    Check if ped is in cable car
/// PARAMS:
///    ped - ped index
///    car - cable car index
/// RETURNS:
///    cable car
FUNC BOOL IS_ENTITY_IN_CABLE_CAR(ENTITY_INDEX ent, CABLE_CAR &car, FLOAT ex = 0.0, BOOL drawArea = FALSE)	
	ANGLED_AREA area
	
	IF NOT IS_ENTITY_OK(car.cablecarID)
		RETURN FALSE
	ENDIF
	
	SET_ANGLED_AREA(area,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, <<0, 2.750, vCableCarPlayerAttachOffset.z>>) + <<0, 0, 1>>,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, <<0, -2.750, vCableCarPlayerAttachOffset.z>>) - <<0, 0, 1>>,
		3.0)
	
	IF (drawArea)
		DEBUG_DRAW_ANGLED_AREA_EX(area)
	ENDIF
	
	IF (ex > 0.0)
		ANGLED_AREA areaex = EXPAND_ANGLED_AREA(area, ex)
		RETURN IS_ENTITY_IN_ANGLED_AREA(ent, areaex.vPosition[0], areaex.vPosition[1], areaex.fWidth)
	ENDIF
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(ent, area.vPosition[0], area.vPosition[1], area.fWidth)
ENDFUNC

/// PURPOSE:
///    Check if ped is in cable car
/// PARAMS:
///    ped - ped index
///    car - cable car index
/// RETURNS:
///    cable car
FUNC BOOL IS_ENTITY_ON_TOP_OF_CABLE_CAR(ENTITY_INDEX ent, CABLE_CAR &car, FLOAT ex = 0.0, BOOL drawArea = FALSE)	
	ANGLED_AREA area
	
	IF NOT IS_ENTITY_OK(car.cablecarID)
		RETURN FALSE
	ENDIF
	
	SET_ANGLED_AREA(area,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, <<0, 2.750, vCableCarPlayerAttachOffset.z + 2.63>>) + <<0, 0, 1>>,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, <<0, -2.750, vCableCarPlayerAttachOffset.z + 2.63>>) - <<0, 0, 1>>,
		3.0)
	
	IF (drawArea)
		DEBUG_DRAW_ANGLED_AREA_EX(area)
	ENDIF
	
	IF (ex > 0.0)
		ANGLED_AREA areaex = EXPAND_ANGLED_AREA(area, ex)
		RETURN IS_ENTITY_IN_ANGLED_AREA(ent, areaex.vPosition[0], areaex.vPosition[1], areaex.fWidth)
	ENDIF
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(ent, area.vPosition[0], area.vPosition[1], area.fWidth)
ENDFUNC
/// PURPOSE:
///    Returns the direction enum for the cable car
/// PARAMS:
///    car - cable car reference
FUNC eCableCarDirection GET_CABLE_CAR_DIRECTION_ENUM(CABLE_CAR &car)
	IF (car.iWireIndex = CABLE_WIRE_LEFT)
		IF (car.fSpdSgn = 1.0)
			RETURN CABLECAR_MOVEUP
		ELSE
			RETURN CABLECAR_MOVEDOWN
		ENDIF
	ENDIF
	
	IF (car.fSpdSgn = -1.0)
		RETURN CABLECAR_MOVEUP
	ELSE
		RETURN CABLECAR_MOVEDOWN
	ENDIF
ENDFUNC

/// PURPOSE:
///    Updates a cable car moving forward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
PROC UPDATE_CABLE_CAR_WIRE_ANIM(CABLE_CAR &car)
	TEXT_LABEL lbl = "C" 
	
	lbl += ((car.iWireIndex) + 1)
	
	IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP)
		SWITCH (car.iWireSegment)
			CASE 0
				lbl += "_up_9"
			BREAK
			CASE 1
				lbl += "_up_1"
			BREAK
			CASE 3
				lbl += "_up_3"
			BREAK
			CASE 5
				lbl += "_up_4"
			BREAK
			CASE 7
				lbl += "_up_5"
			BREAK
			CASE 9
				lbl += "_up_6"
			BREAK
			CASE 11
				lbl += "_up_8"
			BREAK
			CASE 12
				lbl += "_up_9"
			BREAK
			DEFAULT
				EXIT
		ENDSWITCH
	ELSE
		SWITCH (car.iWireSegment)
			CASE 0
				lbl += "_down_1"
			BREAK
			CASE 1
				lbl += "_down_2"
			BREAK
			CASE 3
				lbl += "_down_3"
			BREAK
			CASE 5
				lbl += "_down_4"
			BREAK
			CASE 7
				lbl += "_down_5"
			BREAK
			CASE 9
				lbl += "_down_6"
			BREAK
			CASE 11
				lbl += "_down_8"
			BREAK
			CASE 12
				lbl += "_down_9"
			BREAK
			DEFAULT
				EXIT
		ENDSWITCH
	ENDIF
	
	PLAY_ENTITY_ANIM(car.cableCarID, lbl, "p_cablecar_s", NORMAL_BLEND_IN, FALSE, TRUE)
ENDPROC

PROC UPDATE_CABLE_CAR_CAMERA_MOVE_UP(CABLE_CAR &car)
	BOOL camSwitch = FALSE
	
	SWITCH (iCameraPointIndex)
		CASE 0
			SETUP_CAMERA_STRUCT(externalCamera, <<-733.0331, 5557.0503, 54.3150>>, <<-13.2766, 0.0000, 26.2995>>)
			SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-733.0331, 5557.0503, 54.3050>>, <<3.6561, 0.0000, -49.3377>>, 20000)
			SETUP_CAMERA_STRUCT_FOV(externalCamera, 50.0)
		
			iCameraPointIndex ++
			camSwitch = TRUE
		BREAK
		CASE 1
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-674.4, 5644.0, 40.1>>, <<7.5, 0.0, 148.3>>)
			IF (car.fDistTravelled > 45.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-674.4, 5644.0, 40.1>>, <<7.5, 0.0, 148.3>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-674.4, 5644.0, 40.1>>, <<7.5, 0.0, -133.2>>, 11000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 55.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 2
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-539.4792, 5522.9756, 65.4000>>, <<4.1853, -0.0000, 23.2142>>)
			IF (car.fDistTravelled > 136.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-539.4792, 5522.9756, 65.4000>>, <<4.1853, -0.0000, 23.2142>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-536.2819, 5524.2173, 65.4000>>, <<9.7636, -0.0000, 14.8368>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 55.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 3
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-428.1000, 5383.3999, 102.3000>>, <<-6.4381, 0.0000, 12.5398>>)
			IF (car.fDistTravelled > 220.3)
				SETUP_CAMERA_STRUCT(externalCamera, <<-428.1000, 5383.3999, 102.3000>>, <<-6.4381, 0.0000, 12.5398>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-428.1000, 5383.3999, 102.3000>>, <<1.7164, 0.0000, -8.3420>>, 12000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 4
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-443.8493, 5570.7671, 143.7865>>, <<24.0363, -0.0000, -84.8520>>)
			IF (car.fDistTravelled > 337.3)
				SETUP_CAMERA_STRUCT(externalCamera, <<-443.8493, 5570.7671, 143.7865>>, <<24.0363, -0.0000, -84.8520>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-446.9265, 5570.4902, 150.7141>>, <<24.0363, -0.0000, -84.8520>>, 12000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 42.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 5
			iCameraPointIndex ++
		BREAK
		CASE 6
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-237.0857, 5571.8896, 262.2075>>, <<-38.2954, 0.0082, 79.4291>>)
			IF (car.fDistTravelled > 440.5)
				SETUP_CAMERA_STRUCT(externalCamera, <<-237.0857, 5571.8896, 262.2075>>, <<-38.2954, 0.0082, 79.4291>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-234.3823, 5571.3843, 258.7245>>, <<-23.0862, 0.0082, 79.4291>>, 13500)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 44.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 7
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-318.7657, 5556.8384, 239.9462>>, <<18.0440, 0.0339, -85.4425>>)
			IF (car.fDistTravelled > 545.5)
				SETUP_CAMERA_STRUCT(externalCamera, <<-318.7657, 5556.8384, 239.9462>>, <<18.0440, 0.0339, -85.4425>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-311.4510, 5557.4204, 242.3366>>, <<20.0948, 0.0339, -85.4425>>, 20000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 50.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK	
		CASE 8
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<46.513, 5534.116, 378.619>>, <<-12.2512, 0.004, 70.544>>)
			IF (car.fDistTravelled > 663.5)
				SETUP_CAMERA_STRUCT(externalCamera, <<46.513, 5534.116, 378.619>>, <<-12.2512, 0.004, 70.544>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<46.513, 5534.116, 378.619>>, <<8.2022, 0.004, 56.5520>>, 20000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 9
			iCameraPointIndex ++
		BREAK
		CASE 10
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<229.948, 5536.094, 522.865>>, <<-14.7644, -0.1333, 67.2156>>)
			IF (car.fDistTravelled > 855.5)
				//SETUP_CAMERA_STRUCT(externalCamera, <<230.9404, 5535.6777, 523.1490>>, <<-14.7644, -0.1333, 67.2156>>)
				
				SETUP_CAMERA_STRUCT(externalCamera, <<229.948, 5536.094, 522.865>>, <<-14.7644, -0.1333, 67.2156>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<227.183, 5534.888, 523.006>>, <<-2.897, -0.133, 52.652>>, 21000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 50.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 11
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<235.3752, 5515.3662, 564.9175>>, <<18.2202, 0.0000, 8.5423>>)
			IF (car.fDistTravelled > 1060.36)

				SETUP_CAMERA_STRUCT(externalCamera, <<235.3752, 5515.3662, 564.9175>>, <<18.2202, 0.0000, 8.5423>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<234.0372, 5524.2749, 567.8823>>, <<18.2202, 0.0000, 8.5423>>, 12000)

				SETUP_CAMERA_STRUCT_FOV(externalCamera, 50.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 12
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<262.3502, 5562.6812, 624.7158>>, <<33.0263, -0.0000, -82.3579>>)
			IF (car.fDistTravelled > 1185.503540)
				SETUP_CAMERA_STRUCT(externalCamera, <<262.3502, 5562.6812, 624.7158>>, <<33.0263, -0.0000, -82.3579>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<268.5094, 5563.5073, 628.7556>>, <<33.0263, -0.0000, -82.3579>>, 12000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 50.0)
				//SET_EXTERNAL_CAMERA(externalCamera, 10)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 13
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<358.3939, 5507.8916, 702.6762>>, <<12.1268, -0.0966, -6.9744>>)
			IF (car.fDistTravelled > 1285.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<358.3939, 5507.8916, 702.6762>>, <<12.1268, -0.0966, -6.9744>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<351.1451, 5510.4966, 702.6633>>, <<28.5814, -0.0966, -30.1375>>, 12000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)

				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 14

			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<419.673, 5542.148, 781.969>>, <<2.839, 0.0000, -38.893>>)
			IF (car.fDistTravelled > 1380.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<419.673, 5542.148, 781.969>>, <<2.839, 0.0000, -38.893>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<426.675, 5545.488, 782.306>>, <<2.839, 0.0000, -30.850>>, 12000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.0)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 15
			iCameraPointIndex ++
		BREAK

//		CASE 16
//			iCameraPointIndex ++
//			
//			/*
//			IF (car.fDistTravelled > 840.0)
//				SET_EXTERNAL_CAMERA(externalCamera, 15)
//				iCameraPointIndex ++
//				camSwitch = TRUE
//			ENDIF
//			*/
//		BREAK
//		CASE 17
//			iCameraPointIndex ++
////			IF (car.fDistTravelled > 840.0)
////				SET_EXTERNAL_CAMERA(externalCamera, 16)
////				SETUP_CAMERA_STRUCT_INTERPOLATE_TIME(externalCamera, 14000)
////				iCameraPointIndex ++
////				camSwitch = TRUE
////			ENDIF
//		BREAK
//
//		BREAK
//		CASE 18
//			iCameraPointIndex ++
////			IF (car.fDistTravelled > 1130.0)
////				SET_EXTERNAL_CAMERA(externalCamera, 18)
////				iCameraPointIndex ++
////				camSwitch = TRUE
////			ENDIF
//		BREAK
//		CASE 19
//			iCameraPointIndex ++
////			IF (car.fDistTravelled > 1200.0)
////				SET_EXTERNAL_CAMERA(externalCamera, 19)
////				iCameraPointIndex ++
////				camSwitch = TRUE
////			ENDIF
//		BREAK
//		CASE 20
//			iCameraPointIndex ++
////			IF (car.fDistTravelled > 1325.0)
////				SET_EXTERNAL_CAMERA(externalCamera, 20)
////				iCameraPointIndex ++
////				camSwitch = TRUE
////			ENDIF
//		BREAK
//		CASE 21
//			iCameraPointIndex ++
//			/*
//			IF (car.fDistTravelled > 1346.0)
//				SET_EXTERNAL_CAMERA(externalCamera, 21)
//				iCameraPointIndex ++
//				camSwitch = TRUE
//			ENDIF
//			*/
//		BREAK
//		CASE 22
//			iCameraPointIndex ++
//			/*
//			IF (car.fDistTravelled > 1372.0)
//				SET_EXTERNAL_CAMERA(externalCamera, 22)
//				
//				camSwitch = TRUE
//			ENDIF
//			*/
//		BREAK
//		CASE 23
//			iCameraPointIndex ++
//			/*
//			IF (car.fDistTravelled > 1388.0)
//				SET_EXTERNAL_CAMERA(externalCamera, 23)
//				iCameraPointIndex ++
//				camSwitch = TRUE
//			ENDIF
//			*/
//		BREAK
//		CASE 24
//			iCameraPointIndex ++
////			IF (car.fDistTravelled > 1380.0)
////				SET_EXTERNAL_CAMERA(externalCamera, 24)
////				iCameraPointIndex ++
////				camSwitch = TRUE
////			ENDIF
//		BREAK
	ENDSWITCH
	
	IF (camSwitch = TRUE) AND (bFPSMode = FALSE)
		CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_CAMERA_MOVE_UP - Switching to case: ", iCameraPointIndex)
		ENABLE_CAMERA_STRUCT(externalCamera, cameraID, default, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		bNewCam = TRUE
	ENDIF
ENDPROC

PROC UPDATE_CABLE_CAR_CAMERA_MOVE_DOWN(CABLE_CAR &car)
	BOOL camSwitch = FALSE
	CAMERA_STRUCT streamCam
	
	SWITCH (iCameraPointIndex)
		CASE 0
			SETUP_CAMERA_STRUCT(externalCamera, <<461.3928, 5634.5933, 803.8433>>, <<-21.1986, 0.0407, 152.8518>>)
			SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<465.1065, 5631.4941, 803.8177>>, <<-26.0362, 0.0546, 133.1987>>, 23000)
			SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.0)
			
			iCameraPointIndex ++
			camSwitch = TRUE
		BREAK
		CASE 1
			iCameraPointIndex ++
		BREAK
		CASE 2
		CASE 3
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<215.1628, 5595.8301, 615.1234>>, <<22.9775, -0.0000, -111.6452>>)
			IF (car.fDistTravelled > 112.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<215.1628, 5595.8301, 615.1234>>, <<22.9775, -0.0000, -111.6452>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<221.7997, 5593.1108, 597.7890>>, <<17.1025, -0.0000, -121.5107>>, 18000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK	
		CASE 4
			iCameraPointIndex ++
		BREAK
		CASE 5
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<251.0161, 5575.7451, 617.5678>>, <<-56.0667, 0.0000, 89.7301>>)

			IF (car.fDistTravelled > 288.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<251.0161, 5575.7451, 617.5678>>, <<-56.0667, 0.0000, 89.7301>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<248.8245, 5575.7476, 619.0521>>, <<-54.6050, 0.0000, 89.7301>>, 11000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
				
				NEW_LOAD_SCENE_FOR_CAMERA_STRUCT(streamCam, 16)	
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 6
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-69.8260, 5595.7646, 390.4564>>, <<25.1669, 0.0000, -110.1190>>)
			IF (car.fDistTravelled > 400.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-69.8260, 5595.7646, 390.4564>>, <<25.1669, 0.0000, -110.1190>>) //<<0.8, 5591.6, 422.6>>, <<26.5, 0.0, -112.1>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-68.5961, 5595.1831, 386.8474>>, <<20.6269, 0.0000, -115.1379>>, 18000) // <0.8, 5574.0, 422.6>>, <<26.5, 0.0, -112.1>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.43)
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 7
		CASE 8
		CASE 9
		CASE 10
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-6.2591, 5609.3184, 402.8656>>, <<-0.9222, 0.0000, 153.9538>>)
			IF (car.fDistTravelled > 575.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-6.2591, 5609.3184, 402.8656>>, <<-0.9222, 0.0000, 153.9538>>) //<<0.8, 5591.6, 422.6>>, <<26.5, 0.0, -112.1>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-5.9107, 5609.6133, 401.1573>>, <<-11.4635, 0.0000, 130.2651>>, 10000) // <0.8, 5574.0, 422.6>>, <<26.5, 0.0, -112.1>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 55.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 11
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-215.7561, 5606.1870, 282.0778>>, <<22.8233, -0.0000, -118.7368>>)
			IF (car.fDistTravelled > 660.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-215.7561, 5606.1870, 282.0778>>, <<22.8233, -0.0000, -118.7368>>) //<<0.8, 5591.6, 422.6>>, <<26.5, 0.0, -112.1>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-199.8146, 5615.9712, 284.7165>>, <<22.8233, -0.0000, -138.4323>>, 10000) // <0.8, 5574.0, 422.6>>, <<26.5, 0.0, -112.1>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.026)
			
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK		
		CASE 12
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-61.8849, 5558.9736, 338.9037>>, <<-15.2417, 0.0000, 104.1686>>)
			IF (car.fDistTravelled > 740.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-61.8849, 5558.9736, 338.9037>>, <<-15.2417, 0.0000, 104.1686>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-58.0053, 5559.9546, 323.4490>>, <<-10.6159, -0.0000, 104.1686>>, 10000) // <0.8, 5574.0, 422.6>>, <<26.5, 0.0, -112.1>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 47.9808)

				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 13
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-291.6088, 5570.3823, 227.2185>>, <<26.4681, 0.0000, -73.2798>>)
			IF (car.fDistTravelled > 840.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-291.6088, 5570.3823, 227.2185>>, <<26.4681, 0.0000, -73.2798>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-292.7007, 5575.8618, 227.2185>>, <<24.4170, -0.0000, -69.0123>>, 10000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.05)

				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 14
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-509.6508, 5683.8433, 89.9184>>, <<21.3193, 0.0000, -128.1549>>)
			IF (car.fDistTravelled > 920.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-509.6508, 5683.8433, 89.9184>>, <<21.3193, 0.0000, -128.1549>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-511.7916, 5681.1201, 89.9184>>, <<21.3193, 0.0000, -128.1549>>, 18000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 40.0)
				
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 15
			iCameraPointIndex ++
		BREAK
		CASE 16
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-475.9374, 5375.9893, 94.2374>>, <<6.8777, -0.0000, 6.4845>>)	
			IF (car.fDistTravelled > 1090.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-475.9374, 5375.9893, 94.2374>>, <<6.8777, -0.0000, 6.4845>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-475.9374, 5375.9893, 94.2374>>, <<-2.0745, -0.0000, 21.1399>>, 16000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 30.0)

				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 17
			iCameraPointIndex ++
		BREAK
		CASE 18
			iCameraPointIndex ++
		BREAK
		CASE 19
			iCameraPointIndex ++
		BREAK
		CASE 20 
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-674.5237, 5574.5649, 41.1581>>, <<15.6335, 0.0000, -77.0593>>)
			IF (car.fDistTravelled > 1245.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-674.5237, 5574.5649, 41.1581>>, <<15.6335, 0.0000, -77.0593>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-675.0132, 5576.0742, 41.1581>>, <<24.5521, 0.0000, -57.7690>>, 13000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
			
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
		CASE 21
			iCameraPointIndex ++
		BREAK
		CASE 22	
			CREATE_STREAMVOL_FOR_NEXT_CAMERA(<<-743.0775, 5570.7764, 47.8665>>, <<-3.7547, 0.2510, -46.5015>>)
			IF (car.fDistTravelled > 1355.0)
				SETUP_CAMERA_STRUCT(externalCamera, <<-743.0775, 5570.7764, 47.8665>>, <<-3.7547, 0.2510, -46.5015>>)
				SETUP_CAMERA_STRUCT_END_SHOT(externalCamera, <<-749.0400, 5577.4209, 47.9055>>, <<-9.6095, 0.2510, -48.8215>>, 20000)
				SETUP_CAMERA_STRUCT_FOV(externalCamera, 45.0)
			
				iCameraPointIndex ++
				camSwitch = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF (camSwitch = TRUE) AND (bFPSMode = FALSE)
		CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_CAMERA_MOVE_Down - Switching to case: ", iCameraPointIndex)
		ENABLE_CAMERA_STRUCT(externalCamera, cameraID, default, default, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
		bNewCam = TRUE
	ENDIF
ENDPROC

PROC UPDATE_CABLE_CAR_CAMERA(CABLE_CAR &car)
	IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP) 
		UPDATE_CABLE_CAR_CAMERA_MOVE_UP(car)
	ELSE
		UPDATE_CABLE_CAR_CAMERA_MOVE_DOWN(car)
	ENDIF
	
	UPDATE_NEW_LOAD_SCENE_FOR_CABLE_CAR_CAMERA()
	/*
	IF STREAMVOL_IS_VALID(cameraStreamVol)
		IF STREAMVOL_HAS_LOADED(cameraStreamVol)
			STREAMVOL_DELETE(cameraStreamVol)
		ENDIF
	ENDIF
	*/
ENDPROC

/// PURPOSE:
///    Attach entity to cable car - attaches object to the car relative to where it is 
/// PARAMS:
///    ent - entity
///    car - car
PROC ATTACH_ENTITY_TO_CABLE_CAR(ENTITY_INDEX ent, CABLE_CAR &car)
	VECTOR v
	VECTOR cmin, cmax
	VECTOR entmin, entmax
	FLOAT entlength, entwidth
	
	IF IS_ENTITY_ATTACHED(ent)
		DETACH_ENTITY(ent)
	ENDIF
	
	v = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(car.cablecarID, GET_ENTITY_COORDS(ent))

	// attach vehicle and make sure it's inside
	GET_MODEL_DIMENSIONS(CABLECAR, cmin, cmax)
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(ent), entmin, entmax)
	entlength = ABSF(entmax.y - entmin.y)
	entwidth = ABSF(entmax.x - entmin.x)
	
	v.x = CLAMP(v.x, cmin.x + (entwidth * 0.75), cmax.x - (entwidth * 0.75))
	v.y = CLAMP(v.y, cmin.y + (entlength * 0.5), cmax.y - (entlength * 0.5))
	v.y *= car.fSpdSgn
	
	SET_ENTITY_HEADING(ent, GET_ENTITY_HEADING(car.cablecarID))
	
	FREEZE_ENTITY_POSITION(ent, TRUE)
	ATTACH_ENTITY_TO_ENTITY(ent, car.cablecarID, 0, v, <<0, 0, 0>>)
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Attaching Entity ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ent)), " to cable car at ", v, " Model Dimensions [", entmin, "] [", entmax, "]")
ENDPROC



FUNC BOOL IS_VEHICLE_ALLOWED_TO_BE_ON_CABLE_CAR(VEHICLE_INDEX ind)
	MODEL_NAMES mdl = GET_ENTITY_MODEL(ind)
	IF (mdl = CABLECAR)
		RETURN FALSE
	ENDIF
	RETURN IS_THIS_MODEL_A_BIKE(mdl) OR IS_THIS_MODEL_A_BICYCLE(mdl)
ENDFUNC

PROC ATTACH_FRIEND_A_TO_CABLE_CAR(CABLE_CAR &car)
	IF NOT IS_ENTITY_OK(FRIEND_A_PED_ID()) OR NOT IS_ENTITY_OK(car.cablecarID)
		EXIT
	ENDIF
	
	VECTOR v = vCableCarPlayerAttachOffset
	v.x = -1.0
	
	SET_ENTITY_COORDS(FRIEND_A_PED_ID(), GET_ENTITY_COORDS(car.cablecarID))
	IF IS_ENTITY_ATTACHED(FRIEND_A_PED_ID())
		DETACH_ENTITY(FRIEND_A_PED_ID())
	ENDIF
	
	v.y *= car.fSpdSgn
	IF (car.fSpdSgn = -1.0)
		ATTACH_ENTITY_TO_ENTITY(FRIEND_A_PED_ID(), car.cablecarID, 0, v, <<0, 0, 180.0>>)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(FRIEND_A_PED_ID(), car.cablecarID, 0, v, <<0, 0, 0>>)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:ATTACH_FRIEND_A_TO_CABLE_CAR() - Friend A Attached")
ENDPROC

PROC ATTACH_FRIEND_B_TO_CABLE_CAR(CABLE_CAR &car)
	IF NOT IS_ENTITY_OK(FRIEND_B_PED_ID()) OR NOT IS_ENTITY_OK(car.cablecarID)
		EXIT
	ENDIF
	
	VECTOR v = vCableCarPlayerAttachOffset
	v.x = 1.0
	
	SET_ENTITY_COORDS(FRIEND_B_PED_ID(), GET_ENTITY_COORDS(car.cablecarID))
	IF IS_ENTITY_ATTACHED(FRIEND_B_PED_ID())
		DETACH_ENTITY(FRIEND_B_PED_ID())
	ENDIF
	
	v.y *= car.fSpdSgn
	IF (car.fSpdSgn = -1.0)
		ATTACH_ENTITY_TO_ENTITY(FRIEND_B_PED_ID(), car.cablecarID, 0, v, <<0, 0, 180.0>>)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(FRIEND_B_PED_ID(), car.cablecarID, 0, v, <<0, 0, 0>>)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:ATTACH_FRIEND_B_TO_CABLE_CAR() - Friend B Attached")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_PLAYER_TO_CABLE_CAR(CABLE_CAR &car)
	IF NOT IS_ENTITY_OK(PLAYER_PED_ID()) OR NOT IS_ENTITY_OK(car.cablecarID)
		EXIT
	ENDIF
	
	VECTOR v = vCableCarPlayerAttachOffset
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(car.cablecarID))
	IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		DETACH_ENTITY(PLAYER_PED_ID())
	ENDIF
	
	v.y *= car.fSpdSgn
	IF (car.fSpdSgn = -1.0)
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), car.cablecarID, 0, v, <<0, 0, 180.0>>)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), car.cablecarID, 0, v, <<0, 0, 0>>)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:ATTACH_PED_TO_CABLE_CAR() - Player Ped Attached")
	
	ATTACH_FRIEND_A_TO_CABLE_CAR(car)
	ATTACH_FRIEND_B_TO_CABLE_CAR(car)
ENDPROC

PROC ATTACH_VEHICLE_TO_CABLE_CAR(ENTITY_INDEX ent, CABLE_CAR &car, INT iIndex)

	VECTOR v
	IF IS_ENTITY_ATTACHED(ent)
		DETACH_ENTITY(ent)
	ENDIF
	
	// 0.463465
	
	IF (iIndex = 0)
		v = << 0.6, -1.21756, -5.854 >>
	ELSE
		v = << -0.6, -1.21756, -5.85388 >>
	ENDIF
	
	v.y *= car.fSpdSgn
	IF (car.fSpdSgn = -1.0)
		ATTACH_ENTITY_TO_ENTITY(ent, car.cablecarID, 0, v, <<0, 0, 180.0>>)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(ent, car.cablecarID, 0, v, <<0, 0, 0>>)
	ENDIF
	
	SET_ENTITY_HEADING(ent, GET_ENTITY_HEADING(car.cablecarID))
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Attaching Vehicle ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(ent)), " to cable car at ", v)
ENDPROC


/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_FRIEND_A_CABLE_CAR_ARRIVAL(CABLE_CAR &car)
	VECTOR v = vCableCarLeftExitOffset
	
	IF IS_ENTITY_OK(FRIEND_A_PED_ID()) AND IS_ENTITY_OK(car.cablecarID)
		IF IS_ENTITY_ATTACHED(FRIEND_A_PED_ID())
			DETACH_ENTITY(FRIEND_A_PED_ID())
		ENDIF
		
		v.x += 0.2
		v.y += 0.5
		ATTACH_ENTITY_TO_ENTITY(FRIEND_A_PED_ID(), car.cablecarID, 0, v, <<0, 0, -90.0>>)
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_FRIEND_B_CABLE_CAR_ARRIVAL(CABLE_CAR &car)
	VECTOR v = vCableCarLeftExitOffset

	IF IS_ENTITY_OK(FRIEND_B_PED_ID()) AND IS_ENTITY_OK(car.cablecarID)
		IF IS_ENTITY_ATTACHED(FRIEND_B_PED_ID())
			DETACH_ENTITY(FRIEND_B_PED_ID())
		ENDIF
		
		v.x -= 0.4
		v.y -= 0.5
		ATTACH_ENTITY_TO_ENTITY(FRIEND_B_PED_ID(), car.cablecarID, 0, v, <<0, 0, -90.0>>)
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_PLAYER_TO_CABLE_CAR_ARRIVAL(CABLE_CAR &car)
	VECTOR v = vCableCarLeftExitOffset
	
	IF IS_ENTITY_OK(PLAYER_PED_ID()) AND IS_ENTITY_OK(car.cablecarID)
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		
		ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), car.cablecarID, 0, v, <<0, 0, -90.0>>)
	ENDIF
	
	ATTACH_FRIEND_A_CABLE_CAR_ARRIVAL(car)
	ATTACH_FRIEND_B_CABLE_CAR_ARRIVAL(car)
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC DETACH_PED_FROM_CABLE_CAR(PED_INDEX ped, CABLE_CAR &car)
	IS_ENTITY_OK(car.cablecarID) 
	IS_ENTITY_OK(ped) 
		
	IF DOES_ENTITY_EXIST(car.cablecarID) AND DOES_ENTITY_EXIST(ped)
		IF IS_ENTITY_ATTACHED_TO_ENTITY(ped, car.cablecarID)
			DETACH_ENTITY(ped)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Detaches and releases vehicles attached to cable car
/// PARAMS:
///    car - car
PROC DETACH_STORED_VEHICLES_IN_CABLE_CAR(CABLE_CAR &car)
	INT i
	
	CPRINTLN(DEBUG_AMBIENT, "Cable Car:", car.iWireIndex, " Ready to Release ", car.iStoredVehicleCount, " vehicles")
	
	REPEAT car.iStoredVehicleCount i
		IF DOES_ENTITY_EXIST(car.storedVehicles[i])
			IS_ENTITY_OK(car.storedVehicles[i])
			IF IS_ENTITY_ATTACHED(car.storedVehicles[i])
				DETACH_ENTITY(car.storedVehicles[i], FALSE, FALSE)
			ENDIF
			
			FREEZE_ENTITY_POSITION(car.storedVehicles[i], FALSE)
			SET_VEHICLE_ON_GROUND_PROPERLY(car.storedVehicles[i])
			CPRINTLN(DEBUG_AMBIENT, "Cable Car:", car.iWireIndex, " has detached a vehicles")
			SAFE_RELEASE_VEHICLE(car.storedVehicles[i])
			car.storedVehicles[i] = NULL
		ENDIF
	ENDREPEAT
	
	CPRINTLN(DEBUG_AMBIENT, "Cable Car:", car.iWireIndex, " has released ", car.iStoredVehicleCount, " vehicles")
	car.iStoredVehicleCount = 0
ENDPROC

/// PURPOSE:
///    Attach and store vehicles
/// PARAMS:
///    car - 
PROC ATTACH_AND_STORE_VEHICLES_IN_CABLE_CAR(CABLE_CAR &car)
	INT i = 0
	INT cnt = 0
	VEHICLE_INDEX array[10]
	
	IS_ENTITY_OK(PLAYER_PED_ID())
	cnt = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), array)
	IF (cnt = 0)
		EXIT
	ENDIF
	
	DETACH_STORED_VEHICLES_IN_CABLE_CAR(car)
	
	REPEAT cnt i
		IF (car.iStoredVehicleCount < COUNT_OF(car.storedVehicles))
			IF IS_ENTITY_OK(array[i]) AND IS_VEHICLE_ALLOWED_TO_BE_ON_CABLE_CAR(array[i])
				IF IS_ENTITY_IN_CABLE_CAR(array[i], car)
					car.storedVehicles[car.iStoredVehicleCount] = array[i]
					IF NOT IS_ENTITY_A_MISSION_ENTITY(car.storedVehicles[car.iStoredVehicleCount])
						SET_ENTITY_AS_MISSION_ENTITY(car.storedVehicles[car.iStoredVehicleCount])
					ENDIF
		
					ATTACH_VEHICLE_TO_CABLE_CAR(car.storedVehicles[car.iStoredVehicleCount], car, car.iStoredVehicleCount)
					//ATTACH_ENTITY_TO_CABLE_CAR(car.storedVehicles[car.iStoredVehicleCount], car)
					car.iStoredVehicleCount ++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
		
	CPRINTLN(DEBUG_AMBIENT, "Cable Car:", car.iWireIndex, " has stored ", car.iStoredVehicleCount, " vehicles")
ENDPROC
	
/// PURPOSE:
///    cHECKs if player is trying to put stupid stuff in cable car
/// PARAMS:
///    car - 
FUNC BOOL IS_A_WRONG_VEHICLE_IN_CABLE_CAR(CABLE_CAR &car)
	INT i = 0
	INT cnt = 0
	VEHICLE_INDEX array[10]
	
	IS_ENTITY_OK(PLAYER_PED_ID())
	cnt = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), array)
	IF (cnt = 0)
		RETURN FALSE
	ENDIF
	
	REPEAT cnt i
		IF IS_ENTITY_OK(array[i]) AND NOT IS_VEHICLE_ALLOWED_TO_BE_ON_CABLE_CAR(array[i])
			IF IS_ENTITY_IN_CABLE_CAR(array[i], car)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_RIDING_ANY_CABLE_CAR()
	RETURN (cableCarLeft.bIsPlayerIn AND cableCarLeft.bIsPlayerRiding) OR (cableCarRight.bIsPlayerIn AND cableCarRight.bIsPlayerRiding)
ENDFUNC

PROC SHARED_CLEANUP_CABLE_CAR(CABLE_CAR &car)

	IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_UP_SCENE")
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:PLAYER_END_CABLE_CAR() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_UP_SCENE")
		STOP_AUDIO_SCENE("CABLE_CAR_RIDE_UP_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_DOWN_SCENE")
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:PLAYER_END_CABLE_CAR() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_DOWN_SCENE")
		STOP_AUDIO_SCENE("CABLE_CAR_RIDE_DOWN_SCENE")
	ENDIF
	
	SET_INTERNAL_RAIN_SOUND_ENABLED(car, FALSE)
	SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRunningSound)
	DETACH_PED_FROM_CABLE_CAR(PLAYER_PED_ID(), car)
	DETACH_STORED_VEHICLES_IN_CABLE_CAR(car)
	car.bIsPlayerOnTop = FALSE
ENDPROC 


/// PURPOSE:
///    Cleans up a cable car
/// PARAMS:
///    car - car reference
PROC CLEANUP_CABLE_CAR(CABLE_CAR &car, BOOL bForceDelete = FALSE)
	SHARED_CLEANUP_CABLE_CAR(car)
	
	IF DOES_ENTITY_EXIST(car.cablecarID)
		IF IS_SCREEN_FADED_OUT() or bForceDelete
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CLEANUP_CABLE_CAR - Cable Car:", car.iWireIndex, " Deleted")
			SAFE_DELETE_OBJECT(car.lDoor[0])
			SAFE_DELETE_OBJECT(car.lDoor[1])
			SAFE_DELETE_OBJECT(car.rDoor[0])
			SAFE_DELETE_OBJECT(car.rDoor[1])
			SAFE_DELETE_OBJECT(car.cablecarID)
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CLEANUP_CABLE_CAR() - Cable Car:", car.iWireIndex, " Released")
			SAFE_RELEASE_OBJECT(car.lDoor[0])
			SAFE_RELEASE_OBJECT(car.lDoor[1])
			SAFE_RELEASE_OBJECT(car.rDoor[0])
			SAFE_RELEASE_OBJECT(car.rDoor[1])
			SAFE_RELEASE_OBJECT(car.cablecarID)
		ENDIF
	ENDIF
	
ENDPROC 

/// PURPOSE:
///    Cleans up a cable car
/// PARAMS:
///    car - car reference
PROC DELETE_CABLE_CAR(CABLE_CAR &car)
	SHARED_CLEANUP_CABLE_CAR(car)
	SAFE_DELETE_OBJECT(car.lDoor[0])
	SAFE_DELETE_OBJECT(car.lDoor[1])
	SAFE_DELETE_OBJECT(car.rDoor[0])
	SAFE_DELETE_OBJECT(car.rDoor[1])
	SAFE_DELETE_OBJECT(car.cablecarID)
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CLEANUP_CABLE_CAR() - Cable Car:", car.iWireIndex, " Deleted")
ENDPROC 

/// PURPOSE:
///    Skips the cable car to position
/// PARAMS:
///    car - 
///    wire - 
///    seg - 
///    t - 
PROC SKIP_CABLE_CAR_TO_WIRE_POSITION(CABLE_CAR &car, INT seg = 0, FLOAT t = 0.0)
	car.vPosition = GET_WIRE_POSITION(cableWire[car.iWireIndex], seg, t)
	IF IS_ENTITY_OK(car.cablecarID)
		SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
	ENDIF
	
	car.iWireSegment = seg
	car.fTValue = t
ENDPROC



PROC SET_CABLE_CAR_DOOR_OFFSET(CABLE_CAR &car, FLOAT x)
	INT i
	
	IF IS_ENTITY_OK(car.cablecarID)
		REPEAT COUNT_OF(car.lDoor) i
			IF IS_ENTITY_OK(car.lDoor[i])
				IF IS_ENTITY_ATTACHED(car.lDoor[i])
					DETACH_ENTITY(car.lDoor[i])
				ENDIF
				
				IF (i = 0)
					ATTACH_ENTITY_TO_ENTITY(car.lDoor[i], car.cablecarID, 0, <<0, x, 0>>, <<0, 0, 0>>, FALSE, FALSE, TRUE)
				ELSE
					ATTACH_ENTITY_TO_ENTITY(car.lDoor[i], car.cablecarID, 0, <<0, -x, 0>>, <<0, 0, 0>>, FALSE, FALSE, TRUE)
				ENDIF
			ENDIF
		
			IF IS_ENTITY_OK(car.rDoor[i])
				IF IS_ENTITY_ATTACHED(car.rDoor[i])
					DETACH_ENTITY(car.rDoor[i])
				ENDIF
				
				IF (i = 0)
					ATTACH_ENTITY_TO_ENTITY(car.rDoor[i], car.cablecarID, 0, <<0, -x, 0>>, <<0, 0, 180>>, FALSE, FALSE, TRUE)
				ELSE
					ATTACH_ENTITY_TO_ENTITY(car.rDoor[i], car.cablecarID, 0, <<0, x, 0>>, <<0, 0, 180>>, FALSE, FALSE, TRUE)
				ENDIF				
			ENDIF
		ENDREPEAT	
	ENDIF
ENDPROC

PROC FORCE_BREAK_CABLE_CAR_DOORS(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.cablecarID)
		car.fDoorValue = CABLE_CAR_DOOR_OPEN_LIMIT
		SET_CABLE_CAR_DOOR_OFFSET(car, CABLE_CAR_DOOR_OPEN_LIMIT)
	ENDIF
ENDPROC

PROC REPAIR_CABLE_CAR_DOORS(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.cablecarID)
		car.fDoorValue = 0.0
		SET_CABLE_CAR_DOOR_OFFSET(car, 0.0)
	ENDIF
ENDPROC

PROC START_CABLE_CAR_CUTSCENE(CABLE_CAR &car)
	INT iTimer = 0
	STRING srl = NULL
	BOOL bExitLoop = FALSE
	
	IF bUseSRLS = TRUE
		IF GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP
			srl = "cablecar_up"
		ELSE
			srl = "cablecar_down"
		ENDIF
	ENDIF
	
	bSRLOK = FALSE
	g_bIsOnCableCar = TRUE
	RC_START_CUTSCENE_MODE(<<0, 0, 0>>)
	CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 2.0)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(srl)
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car ", car.iWireIndex, " Prefetch SRL:", srl)
		PREFETCH_SRL(srl)
		
		iTimer = GET_GAME_TIMER() + 6000
		WHILE (bExitLoop = FALSE)
			IF IS_SRL_LOADED()
				bSRLOK = TRUE
				bExitLoop = TRUE
			ENDIF
			
			car.fDoorValue -= TIMESTEP()
			IF (car.fDoorValue <= 0.0)
				car.fDoorValue = 0.0
			ENDIF
			
			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
			
			IF (GET_GAME_TIMER() > iTimer)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car ", car.iWireIndex, " Prefetch SRL:", srl , " Failed Abort")
				bExitLoop = TRUE
				bSRLOK = FALSE
			ENDIF
			WAIT(0)
		ENDWHILE
		
		IF bSRLOK = TRUE
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car ", car.iWireIndex, " SRL:", srl, " Loaded - Load Time:", GET_GAME_TIMER() - iTimer)
			fSrlTime = 0.0
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			BEGIN_SRL()
		ELSE
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car ", car.iWireIndex, " Prefetch SRL:", srl , " End SRL")
			END_SRL()
		ENDIF
	ENDIF
	
	
ENDPROC

PROC END_CABLE_CAR_CUTSCENE(CABLE_CAR &car)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	
	SHUTDOWN_CAMERA(cameraID)
	IF (car.bIsPlayerIn = TRUE)
		DETACH_PED_FROM_CABLE_CAR(PLAYER_PED_ID(), car)
		DETACH_PED_FROM_CABLE_CAR(FRIEND_A_PED_ID(), car)
		DETACH_PED_FROM_CABLE_CAR(FRIEND_B_PED_ID(), car)
	ENDIF
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	RC_END_CUTSCENE_MODE()
	
	IF (car.bIsPlayerIn = TRUE)
		g_bIsOnCableCar = FALSE
	ENDIF
	
	IF bSRLOK = TRUE
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car ", car.iWireIndex, " SRL Ended")
		bSRLOK = FALSE
		END_SRL()
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a cable car
/// PARAMS:
///    car - car reference
///    wire - index of wire to be on
///    seg - wire segment to start on - 0 if going down to up
///    f - how far along the wire segment to be placed
PROC CREATE_CABLE_CAR(CABLE_CAR &car, INT wire, INT seg = 0, FLOAT t = 0.0, FLOAT fSpdSgn = 1.0)
	FLOAT heading
	
	ZERO_CABLE_CAR_STRUCT(car)
	
	car.iWireIndex = wire
	car.iWireSegment = seg
	car.vPosition = GET_WIRE_POSITION(cableWire[wire], seg, t)
	CLEAR_AREA_OF_OBJECTS(car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>, 2.0)
	car.cablecarID = CREATE_OBJECT(cableCarModel, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
	car.fTValue = t
	car.fSpdSgn = fSpdSgn
	car.iDepartTime = GET_GAME_TIMER() + iCableCarDockWaitTime
	car.bIsPaused = FALSE
	
	FREEZE_ENTITY_POSITION(car.cablecarID, TRUE)
	
	SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
	
	heading = GET_HEADING_FROM_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
	SET_ENTITY_HEADING(car.cablecarID, heading)
	
	car.lDoor[0] = CREATE_OBJECT(cableCarDoorLModel, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>) SET_ENTITY_HEADING(car.lDoor[0], heading)
	car.lDoor[1] = CREATE_OBJECT(cableCarDoorRModel, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>) SET_ENTITY_HEADING(car.lDoor[1], heading)
	
	ATTACH_ENTITY_TO_ENTITY(car.lDoor[0], car.cablecarID, 0, <<0, 0, 0>>, <<0, 0, 0>>, FALSE, FALSE, TRUE)
	ATTACH_ENTITY_TO_ENTITY(car.lDoor[1], car.cablecarID, 0, <<0, 0, 0>>, <<0, 0, 0>>, FALSE, FALSE, TRUE)
	
	car.rDoor[0] = CREATE_OBJECT(cableCarDoorLModel, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>) 
	car.rDoor[1] = CREATE_OBJECT(cableCarDoorRModel, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>) 

	ATTACH_ENTITY_TO_ENTITY(car.rDoor[0], car.cablecarID, 0, <<0, 0, 0>>, <<0, 0, 180>>, FALSE, FALSE, TRUE)
	ATTACH_ENTITY_TO_ENTITY(car.rDoor[1], car.cablecarID, 0, <<0, 0, 0>>, <<0, 0, 180>>, FALSE, FALSE, TRUE)
	
	UPDATE_CABLE_CAR_WIRE_ANIM(car)
	FORCE_BREAK_CABLE_CAR_DOORS(car)
	
	IF IS_ENTITY_OK(car.cablecarID)
		PLAY_SOUND_FROM_ENTITY(-1, "Arrive_Station", car.cablecarID, "CABLE_CAR_SOUNDS")
	ENDIF
	
	//SET_ENTITY_PROOFS(car.carID, TRUE, FALSE, FALSE, FALSE, TRUE)
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CREATE_CABLE_CAR() - Cable Car Created - Wire:", wire, " Segment:", seg, " T:", t)
ENDPROC

FUNC CABLE_CAR GET_OPPOSITE_CABLE_CAR(CABLE_CAR &car)
	IF (car.cableCarID = cableCarLeft.cablecarID)
		RETURN cableCarRight
	ELSE
		RETURN cableCarLeft
	ENDIF
ENDFUNC

PROC SKIP_CABLE_CAR_TO_END(CABLE_CAR &car)
	eCableCarDirection dir = GET_CABLE_CAR_DIRECTION_ENUM(car)
	
	IF (car.iWireIndex = CABLE_WIRE_LEFT)
		IF (dir = CABLECAR_MOVEDOWN)
			SKIP_CABLE_CAR_TO_WIRE_POSITION(car, 0, 0)		
		ELSE
			SKIP_CABLE_CAR_TO_WIRE_POSITION(car, cableWire[car.iWireIndex].iPointsUsed - 1, 0)
		ENDIF
	ELSE
		IF (dir = CABLECAR_MOVEUP)
			SKIP_CABLE_CAR_TO_WIRE_POSITION(car, 0, 0)				
		ELSE
			SKIP_CABLE_CAR_TO_WIRE_POSITION(car, cableWire[car.iWireIndex].iPointsUsed - 1, 0)
		ENDIF		
	ENDIF
ENDPROC

PROC SIMULATE_MAP_FILL(CABLECAR_WIRE &wire)
	INT cp
	FLOAT t
	VECTOR pos
	
	IF bSimulateMapSkip = FALSE
		EXIT
	ENDIF
	
	REPEAT wire.iPointsUsed - 2 cp
		t = 0
		WHILE t < 1.0
			pos = INTERPOLATE_VECTOR(wire.vPoints[cp], wire.vPoints[cp + 1], t, TRUE)
			CPRINTLN(DEBUG_AMBIENT, "SIMULATE MAP FILL AT:", pos, " CP:", cp, " T:", t)
			SET_MINIMAP_FOW_REVEAL_COORDINATE(pos)
			t += 0.125
		ENDWHILE
		WAIT (0)
	ENDREPEAT
	
	SET_MINIMAP_FOW_REVEAL_COORDINATE(wire.vPoints[wire.iPointsUsed - 1])
	bSimulateMapSkip = FALSE
ENDPROC

PROC HANDLE_CABLE_CAR_FAKE_SKIP(CABLE_CAR &car)
	SKIP_CABLE_CAR_TO_END(car)
	FORCE_BREAK_CABLE_CAR_DOORS(car)
	car.bIsMoving = FALSE
	car.carState = CABLECAR_ARRIVED
	car.fDistTravelled = 0.0
ENDPROC

/// PURPOSE:
///    Handles cable car arriving
/// PARAMS:
///    car - 
///    skip - 
PROC CABLE_CAR_ARRIVED(CABLE_CAR &car, BOOL skip = FALSE)
	BOOL bFriendWait = FALSE
	
	
	SET_INTERNAL_RAIN_SOUND_ENABLED(car, FALSE)
	SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRunningSound)
	
	IF (skip) AND ((car.bIsPlayerIn = TRUE) OR (car.bIsPlayerRiding = TRUE))
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED - Cable Car:", car.iWireIndex, " Skipped Re-Spotting Cable Car")
		bFPSMode = FALSE
		

		IF STREAMVOL_IS_VALID(streamVolCam)
			STREAMVOL_DELETE(streamVolCam)
		ENDIF
		

		SKIP_CABLE_CAR_TO_END(car)
		
		IF (skip)
			IF (car.iWireIndex <> cableCarLeft.iWireIndex)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Left car to end to re-sync")
				HANDLE_CABLE_CAR_FAKE_SKIP(cableCarLeft)
			ENDIF
			
			IF (car.iWireIndex <> cableCarRight.iWireIndex)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Right car to end to re-sync")
				HANDLE_CABLE_CAR_FAKE_SKIP(cableCarRight)
			ENDIF
		ENDIF
		
		CLEAR_FOCUS()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_SELECTOR_THIS_FRAME()
		WAIT(0)

		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(car.cablecarID), 50, FLAG_MAPDATA | FLAG_COLLISIONS_MOVER, DEFAULT, TRUE, TRUE)
		
		DETACH_PED_FROM_CABLE_CAR(PLAYER_PED_ID(), car)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(car.cablecarID) + <<6, 0, 0>>)
		
		IF IS_ENTITY_OK(car.cablecarID)
			PLAY_SOUND_FROM_ENTITY(-1, "Arrive_Station", car.cablecarID, "CABLE_CAR_SOUNDS")
			PLAY_SOUND_FROM_ENTITY(-1, "DOOR_OPEN", car.cablecarID, "CABLE_CAR_SOUNDS")
		ENDIF
	
		ATTACH_PLAYER_TO_CABLE_CAR_ARRIVAL(car)
		
		bSkipped = FALSE
	ENDIF
	
	IF (car.bIsPlayerIn = TRUE) OR (car.bIsPlayerRiding = TRUE)
		IF (skip)
			FORCE_BREAK_CABLE_CAR_DOORS(car)
		ENDIF
		
		CLEAR_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_FROZEN_ON_CABLE_CAR_UP))
		CLEAR_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_FROZEN_ON_CABLE_CAR_DOWN))
				
		IF (car.iStoredVehicleCount = 0) 
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
		ENDIF
	ENDIF
	
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		IF IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car)
		
			IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_UP_SCENE")
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_UP_SCENE")
				STOP_AUDIO_SCENE("CABLE_CAR_RIDE_UP_SCENE")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_DOWN_SCENE")
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_DOWN_SCENE")
				STOP_AUDIO_SCENE("CABLE_CAR_RIDE_DOWN_SCENE")
			ENDIF
		
			END_CABLE_CAR_CUTSCENE(car)
		ENDIF
	ENDIF
	
	IF (skip)
		FORCE_BREAK_CABLE_CAR_DOORS(car)
		
		/* B*2206536 - WAIT(125) gave a 1/8th of a second for phone calls to slip through
		// Instead We run a loop which blocks the phone and selector every frame until the wait is over
		INT iDelayTimer = (GET_GAME_TIMER() + 15)
		WHILE (GET_GAME_TIMER() < iDelayTimer)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SELECTOR_THIS_FRAME()
			WAIT(0)
		ENDWHILE
		*/
	ENDIF
	
	car.bIsMoving = FALSE
	car.carState = CABLECAR_ARRIVED
	car.fDistTravelled = 0.0
	
	IF (car.bIsPlayerIn = TRUE) OR (car.bIsPlayerRiding = TRUE)
		DO_SCREEN_FADE_IN(500)
		
		IF (skip)
			FORCE_BREAK_CABLE_CAR_DOORS(car)
		ENDIF
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		SHUTDOWN_CAMERA(cameraID)
		CHARGE_CURRENT_PLAYER_PED_ACCOUNT_BALANCE(COST_CABLECAR)
		CHARGE_PLAYER_PED_ACCOUNT_BALANCE(FRIEND_A_PED_ID(), COST_CABLECAR)
		CHARGE_PLAYER_PED_ACCOUNT_BALANCE(FRIEND_B_PED_ID(), COST_CABLECAR)
		
		car.bIsPlayerIn = FALSE
		REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_CABLE)
		CLEAR_FOCUS()
	
		IF IS_ENTITY_OK(FRIEND_A_PED_ID())
			TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(FRIEND_A_PED_ID(), PLAYER_PED_ID(), <<-1, 2, 0>>, PEDMOVE_WALK)
			bFriendWait = TRUE
		ENDIF			
		
		IF IS_ENTITY_OK(FRIEND_B_PED_ID())
			TASK_GO_STRAIGHT_TO_COORD_RELATIVE_TO_ENTITY(FRIEND_B_PED_ID(), PLAYER_PED_ID(), <<1, 2, 0>>, PEDMOVE_WALK)
			bFriendWait = TRUE
		ENDIF	
		
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Cable Car:", car.iWireIndex, " Delay For Friend Walkout")
			IF (bFriendWait)
				WAIT(1000)
			ENDIF
			
			finishActivity(activityLoc, AR_playerWon)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CABLE_CAR_ARRIVED() - Cable Car:", car.iWireIndex, " Finish Friend Cable Car")
		ENDIF
	ENDIF
	
	IF (skip)
		SIMULATE_MAP_FILL(cableWire[car.iWireIndex])
	ENDIF
	
	car.bIsPlayerRiding = FALSE
	DETACH_STORED_VEHICLES_IN_CABLE_CAR(car)
ENDPROC

/// PURPOSE:
///    Actually moves the cable car 
/// PARAMS:
///    car - 
PROC UPDATE_CABLE_CAR_POSITION(CABLE_CAR &car)


	IF (cableCarLeft.bIsPlayerIn) OR (cableCarRight.bIsPlayerIn)
		SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
		EXIT
	ENDIF
	
	IF IS_SPHERE_VISIBLE(car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET / 2.0>>, 15.0) 
		SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
		EXIT
	ENDIF
	
	IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(car.cablecarID) + <<0, 0, CABLE_CAR_WIRE_OFFSET / 2.0>>, 15.0)
		SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
		EXIT
	ENDIF	
ENDPROC		

/// PURPOSE:
///    Updates a cable car moving forward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR_FORWARD(CABLE_CAR &car)
	FLOAT delta
	
	IF (ENUM_TO_INT(car.carState) >= ENUM_TO_INT(CABLECAR_PAUSESTART))
		RETURN FALSE
	ENDIF
	
	IF (!car.bIsMoving)
		RETURN TRUE
	ENDIF
	
	IF ((car.iWireSegment + 1) >= cableWire[car.iWireIndex].iPointsUsed)
		RETURN TRUE
	ENDIF
	
	VECTOR oldpos = car.vPosition
	//FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
	
	// zero divide defense
	IF (car.fDistToNextPoint != 0.0)
		delta = (car.fSpdSgn * car.fSpeed) / car.fDistToNextPoint
	ELSE
		delta = 0.0
	ENDIF
	
	IF (car.bIsPlayerOnTop)
		delta = 0.0
	ENDIF
	
	// only bother to update if we have told car to move
	IF (delta != 0.0) 
		FLOAT tShift = (delta * TIMESTEP() * fTimeStepFactor) 	
		
		IF NOT IS_PLAYER_RIDING_ANY_CABLE_CAR() 
			IF NOT IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), car.vPosition, 50.0)
				IF NOT IS_ENTITY_ON_SCREEN(car.cablecarID)
					tShift *= fImranFactor
				ENDIF
			ENDIF
		ENDIF
		car.fTValue += tShift		
		
		IF (car.fTValue >= 1.0)
			car.fTValue = 0.0
	
			IF (car.bIsPlayerIn = TRUE)
				VECTOR vpos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_FORWARD() - REVEAL MAP AT:", vpos)
				SET_MINIMAP_FOW_REVEAL_COORDINATE(vpos)
			ENDIF
			
			car.iWireSegment ++
			IF (car.iWireSegment >= (cableWire[car.iWireIndex].iPointsUsed - 1))
				car.iWireSegment = (cableWire[car.iWireIndex].iPointsUsed - 1)
				car.fSpeed = 0.0
				car.bIsMoving = FALSE
				car.vPosition = cableWire[car.iWireIndex].vPoints[car.iWireSegment]
				SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_FORWARD() - Cable Car:", car.iWireIndex, " Finished")
				RETURN TRUE
			ENDIF	
	
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_FORWARD() - Cable Car:", car.iWireIndex, " At Point:", car.iWireSegment)
			UPDATE_CABLE_CAR_WIRE_ANIM(car)
			car.fDistToNextPoint = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
		ENDIF
				
		// check no bounds are broken
		IF (car.iWireSegment < (cableWire[car.iWireIndex].iPointsUsed - 1))
			car.vPosition = GET_WIRE_POSITION(cableWire[car.iWireIndex], car.iWireSegment, car.fTValue)
			UPDATE_CABLE_CAR_POSITION(car)
		ENDIF
		
		car.fDistTravelled += GET_DISTANCE_BETWEEN_COORDS(car.vPosition, oldpos)
		//CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_FORWARD() Distance travelled:", car.fDistTravelled)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates a cable car moving backward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR_BACKWARD(CABLE_CAR &car)
	FLOAT delta
	
	IF (ENUM_TO_INT(car.carState) >= ENUM_TO_INT(CABLECAR_PAUSESTART))
		RETURN FALSE
	ENDIF
	
	IF (!car.bIsMoving)
		RETURN TRUE
	ENDIF
	
	
	VECTOR oldpos = car.vPosition
	//FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
	
	// zero divide defense
	IF (car.fDistToNextPoint != 0.0)
		delta = (car.fSpdSgn * car.fSpeed) / car.fDistToNextPoint
	ELSE
		delta = 0.0
	ENDIF
	
	IF (car.bIsPlayerOnTop)
		delta = 0.0
	ENDIF
	
	// only bother to update if we have told car to move
	IF (delta != 0.0) 
		FLOAT tShift = (delta * TIMESTEP() * fTimeStepFactor) 	
		
		IF NOT IS_PLAYER_RIDING_ANY_CABLE_CAR() 
			IF NOT IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), car.vPosition, 50.0)
				IF NOT IS_ENTITY_ON_SCREEN(car.cablecarID)
					tShift *= fImranFactor
				ENDIF
			ENDIF
		ENDIF
		car.fTValue += tShift
		
		IF (car.fTValue < 0.0)
			car.fTValue = 1.0
		
			IF (car.bIsPlayerIn = TRUE)
				VECTOR vpos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_BACKWARD() - REVEAL MAP AT:", vpos)
				SET_MINIMAP_FOW_REVEAL_COORDINATE(vpos)
			ENDIF
			
			car.iWireSegment --
			IF (car.iWireSegment < 0)
				car.iWireSegment = 0
				car.fTValue = 0.0
				car.fSpeed = 0.0
				car.bIsMoving = FALSE
				car.vPosition = cableWire[car.iWireIndex].vPoints[car.iWireSegment]
				SET_ENTITY_COORDS(car.cablecarID, car.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_BACKWARD() - Cable Car:", car.iWireIndex, " Finished")
				RETURN TRUE
			ENDIF	
			
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_BACKWARD() - Cable Car:", car.iWireIndex, " At Point:", car.iWireSegment)
			UPDATE_CABLE_CAR_WIRE_ANIM(car)
			car.fDistToNextPoint = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
		ENDIF
		
		IF (car.iWireSegment < (cableWire[car.iWireIndex].iPointsUsed - 1))
			car.vPosition = INTERPOLATE_VECTOR(cableWire[car.iWireIndex].vPoints[car.iWireSegment], 
											   cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1], car.fTValue)
			UPDATE_CABLE_CAR_POSITION(car)
		ENDIF
		
		car.fDistTravelled += GET_DISTANCE_BETWEEN_COORDS(car.vPosition, oldpos)
		//CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_BACKWARD() Distance travelled:", car.fDistTravelled)
			
	ENDIF

	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Disables attack, weapon select and certain movement controls when about to trigger a cutscene
PROC DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
ENDPROC

FUNC BOOL UPDATE_CABLE_CAR_PLAYER_HALO(CABLE_CAR &car)
	BOOL bInCableCar = IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car)
	
	IF bInCableCar
		car.iDepartTime = GET_GAME_TIMER() + iCableCarDockWaitTime
		
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
		DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
	ENDIF
	
	SWITCH (car.haloState)
		CASE APPROACH_MODE
			IF bInCableCar
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					car.haloState = WANTED_MODE
				ELIF IS_A_WRONG_VEHICLE_IN_CABLE_CAR(car)
					car.haloState = DENIED_MODE
				ELSE				
					car.haloState = TRIGGER_MODE
				ENDIF
				
				IF (car.bHelpShown = TRUE)	
					CHECK_AND_CLEAR_HELP()
				ENDIF
				car.bHelpShown = FALSE
			ENDIF
		BREAK
		CASE CANTAFFORD_MODE
			IF NOT (car.bHelpShown)	
				PRINT_HELP_FOREVER("PLAY_CABLE_NO_CASH")			
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player Can't Afford It")
				car.bHelpShown = TRUE
			ENDIF
			
			IF (NOT bInCableCar) OR (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() >= COST_CABLECAR)
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				RETURN FALSE
			ENDIF			
		BREAK
		CASE DENIED_MODE
			IF NOT (car.bHelpShown)	
				PRINT_HELP_FOREVER("CAR_IN_CABLECAR")	
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " You can't put a car in a cable car")
				car.bHelpShown = TRUE
			ENDIF
			
			IF (NOT bInCableCar) OR NOT IS_A_WRONG_VEHICLE_IN_CABLE_CAR(car)
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				RETURN FALSE
			ENDIF	
		BREAK
		CASE WANTED_MODE
			IF NOT (car.bHelpShown)	
				PRINT_HELP_FOREVER("CABLE_WANTED")	
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " No Cable Car For You")
				car.bHelpShown = TRUE
			ENDIF
			
			IF (NOT bInCableCar) OR (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				RETURN FALSE
			ENDIF
		BREAK
		CASE TRIGGER_MODE
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
			DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
			DISABLE_SELECTOR_THIS_FRAME()
			
			IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				car.bHelpShown = FALSE
				RETURN FALSE
			ENDIF
			
			IF (NOT bInCableCar)
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				car.bHelpShown = FALSE
				RETURN FALSE
			ENDIF
			
			IF IS_SELECTOR_ONSCREEN(FALSE)
			OR IS_PLAYER_SWITCH_IN_PROGRESS()
				CHECK_AND_CLEAR_HELP()
				car.haloState = APPROACH_MODE
				RETURN FALSE
			ENDIF
			
			IF (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() < COST_CABLECAR)
				CHECK_AND_CLEAR_HELP()
				car.haloState = CANTAFFORD_MODE
				car.bHelpShown = FALSE
				RETURN FALSE
			ENDIF
					
			IF NOT (car.bHelpShown)	AND NOT IS_PHONE_ONSCREEN()
				PRINT_HELP_FOREVER("PLAY_CABLE")	
				car.bHelpShown = FALSE
			ENDIF		
			
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT) AND NOT IS_PHONE_ONSCREEN()
				car.haloState = LAUNCH_MODE
			ENDIF
		BREAK  
		CASE LAUNCH_MODE
			DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
			DISABLE_SELECTOR_THIS_FRAME()
			
			car.haloState = APPROACH_MODE
			car.bHelpShown = FALSE
			car.bIsPlayerRiding = TRUE
			
			
			IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP)
				SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_FROZEN_ON_CABLE_CAR_UP))
			ELSE
				SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_FROZEN_ON_CABLE_CAR_DOWN))
			ENDIF
			
			CLEAR_HELP()
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_CABLE_CAR_PLAYER_ENTRY(CABLE_CAR &car)
	IF (car.bIsMoving)
		EXIT
	ENDIF
	
	//If in director mode, disable starting the cablecar
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player is in Director mode, disabling cablecar usage")
		EXIT
	ENDIF
	
	BOOL ok 
	CABLE_CAR opp 
	
	IF IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car, 1.05)
		car.iDepartTime = GET_GAME_TIMER() + iCableCarDockWaitTime
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CONTEXT)
		DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
		
		IF (car.bHelpShown = TRUE) AND IS_PHONE_ONSCREEN()
			CLEAR_HELP()
			car.bHelpShown = FALSE
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player Ride Help Cleared - Phone is on screen")
		ENDIF
		
		ok = UPDATE_CABLE_CAR_PLAYER_HALO(car)
		IF (ok = TRUE)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Is Ready To Depart With Player")
			SHUTDOWN_CAMERA(cameraID)
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Waiting for Player Off Vehicle")
				ok = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				WHILE (OK = TRUE)
					IF NOT IS_ENTITY_OK(PLAYER_PED_ID())
						ok = FALSE
					ELSE
						ok = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					ENDIF					
					WAIT(0)
					// frames where control blocking won't be active
					DISABLE_CONTROL_ACTIONS_FOR_CABLE_CAR()
					DISABLE_SELECTOR_THIS_FRAME()
				ENDWHILE
			ENDIF

			car.bIsPlayerIn = TRUE
			iCameraPointIndex = 0
			car.carState = CABLECAR_DEPARTING
			
			iCurrentLoadSceneIndex = -1
			iLoadSceneTimeOut = GET_GAME_TIMER()
			
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
				startActivity(activityLoc)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Starting Friend Cable Car")
			ENDIF
			
			UPDATE_CABLE_CAR_CAMERA(car)
			ATTACH_AND_STORE_VEHICLES_IN_CABLE_CAR(car)	
			ATTACH_PLAYER_TO_CABLE_CAR(car)
			
			IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Moving Up")
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Moving Down")
			ENDIF
			
			PLAY_SOUND_FROM_ENTITY(-1, "DOOR_CLOSE", car.cablecarID, "CABLE_CAR_SOUNDS")
			START_CABLE_CAR_CUTSCENE(car)
		
			CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0)
			STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0)
			
			
			IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP)
				IF NOT IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_UP_SCENE")
					CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Start Audio Scene: CABLE_CAR_RIDE_UP_SCENE")
					START_AUDIO_SCENE("CABLE_CAR_RIDE_UP_SCENE")
				ENDIF
			ELIF NOT IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_DOWN_SCENE")
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Start Audio Scene: CABLE_CAR_RIDE_DOWN_SCENE")
				START_AUDIO_SCENE("CABLE_CAR_RIDE_DOWN_SCENE")
			ENDIF	
			
			//REPAIR_CABLE_CAR_DOORS(car)
			CLEAR_HELP()
			EXIT
		ENDIF
	ELIF (car.bHelpShown = TRUE)
		CLEAR_HELP()
		car.bHelpShown = FALSE
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Player Ride Help Cleared")
	ENDIF
	
	// or make the cable car go by itself
	IF (GET_GAME_TIMER() > car.iDepartTime) 
		IF IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, vCableCarLeftExitOffset), 1.0, FALSE, TRUE, TRUE, FALSE, FALSE)
			EXIT
		ENDIF

		IF IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.cablecarID, vCableCarRightExitOffset), 1.0, FALSE, TRUE, TRUE, FALSE, FALSE)
			EXIT
		ENDIF

		// don't let a cable car go until the one with the player in is at least 200 meters away
		IF IS_PLAYER_RIDING_ANY_CABLE_CAR() 
			IF IS_ENTITY_IN_RANGE_COORDS_2D(PLAYER_PED_ID(), car.vPosition, 200.0)
				EXIT
			ENDIF
		ELSE
			opp = GET_OPPOSITE_CABLE_CAR(car)
			IF IS_ENTITY_OK(opp.cablecarID) AND ((opp.fDistTravelled > 0.0) OR (opp.bIsMoving))
				IF IS_ENTITY_IN_RANGE_COORDS_2D(car.cablecarID, opp.vPosition, 200.0)
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		ATTACH_AND_STORE_VEHICLES_IN_CABLE_CAR(car)	
		car.carState = CABLECAR_DEPARTING
		IF (car.bHelpShown = TRUE)
			CLEAR_HELP()
			car.bHelpShown = FALSE
		ENDIF
		car.bIsPlayerIn = FALSE
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR_PLAYER_ENTRY() - Cable Car:", car.iWireIndex, " Car is going by itself")
		PLAY_SOUND_FROM_ENTITY(-1, "DOOR_CLOSE", car.cablecarID, "CABLE_CAR_SOUNDS")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Handles controls while in cable car - skipping and camera switch
/// PARAMS:
///    car - 
PROC UPDATE_CABLE_CAR_PLAYER_CONTROLS(CABLE_CAR &car)
	INT iEnum = ENUM_TO_INT(car.carState)
	
	IF (iEnum < ENUM_TO_INT(CABLECAR_SPEEDUP)) OR (iEnum > ENUM_TO_INT(CABLECAR_SLOWDOWN))
		EXIT 
	ENDIF
	
	// camera switching
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	// block char switching
	DISABLE_SELECTOR_THIS_FRAME()
	
	IF (iEnum < ENUM_TO_INT(CABLECAR_SLOWDOWN))
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT) AND (car.bIsPaused = FALSE)	
			IF NOT bFPSMode
				SHUTDOWN_CAMERA(cameraID)
				ENABLE_CABLE_CAR_FIRST_PERSON(GET_CABLE_CAR_DIRECTION_ENUM(car))
				SET_INTERNAL_RAIN_SOUND_ENABLED(car, TRUE)
				SET_FOCUS_ENTITY(car.cableCarID)
			ELSE
				SET_INTERNAL_RAIN_SOUND_ENABLED(car, FALSE)
				DISABLE_CABLE_CAR_FIRST_PERSON()
				ENABLE_CAMERA_STRUCT(externalCamera, cameraID)
			ENDIF
		ENDIF
	ENDIF
	
	IF (bFPSMode) AND NOT IS_IT_RAINING_OUTSIDE()
		IF (car.iRainSound <> -1)
			SET_INTERNAL_RAIN_SOUND_ENABLED(car, FALSE)
		ENDIF
	ENDIF
	
	// skipping
	IF (bSkipped = FALSE)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
		AND (car.bIsPaused = FALSE)	
			bSkipped = TRUE
			car.bIsPlayerRiding = FALSE
			SAFE_FADE_SCREEN_OUT_TO_BLACK(500, FALSE)
		ENDIF
	ELIF IS_SCREEN_FADED_OUT()
		CABLE_CAR_ARRIVED(car, TRUE)
	ELIF NOT IS_SCREEN_FADING_OUT() //B*-2183175
		//If screen faded for other reason, and cablecar is continuing- undo skip.
		bSkipped = FALSE
		car.bIsPlayerRiding = TRUE
	ENDIF	
ENDPROC



PROC UPDATE_CABLE_CAR(CABLE_CAR &car)

	VECTOR v
	IF NOT DOES_ENTITY_EXIST(car.cablecarID)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		car.iHaloStateDebug = ENUM_TO_INT(car.haloState)
		car.iCarStateDebug = ENUM_TO_INT(car.carState)
		
		IF (car.bDebugPause) AND (car.bIsMoving)
			car.bIsPaused = TRUE
			car.bDebugPause = FALSE
		ENDIF
		
		IF (cableWire[0].bShowWire OR cableWire[1].bShowWire)
			IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car, 0, TRUE)
		ENDIF
			
	#ENDIF
	
	
	
 	car.iJourneyTimer = GET_GAME_TIMER() - car.iDepartTime
	IF (car.iJourneyTimer < 0)
		car.iJourneyTimer = 0
	ENDIF
	 
	// make vehicle drop when dead
	IF IS_ENTITY_DEAD(car.cablecarID) AND (car.carState < CABLECAR_DEAD)
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " is dead...")
		SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRunningSound)
		car.carState = CABLECAR_DEAD
	ENDIF
	
	IF bSRLOK = TRUE
		fSrlTime += GET_FRAME_TIME()
		SET_SRL_TIME(fSrlTime)
	ENDIF
	
	car.bIsPlayerOnTop = IS_ENTITY_ON_TOP_OF_CABLE_CAR(PLAYER_PED_ID(), car, 0, FALSE)
	IF (car.bIsPlayerOnTop) 
		IF (car.iRunningSound <> -1)
			SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRunningSound)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Player is on top - stopping running sound")
		ENDIF
		
		IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Player is on top - give wanted rating")
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
	ENDIF
	
	//DRAW_DEBUG_LINE(GET_ENTITY_COORDS(car.cablecarID) + <<0, 0, fDebugWireVertexHeight>>, GET_ENTITY_COORDS(car.cablecarID) - <<0, 0, fDebugWireVertexHeight>>, 0, 255, 0, 255)
	
	SWITCH (car.carState)
		CASE CABLECAR_NULLSTATE
		CASE CABLECAR_WAITING
		
			
			IF (car.fDoorValue < CABLE_CAR_DOOR_OPEN_LIMIT)
				car.fDoorValue += TIMESTEP()
				IF (car.fDoorValue > CABLE_CAR_DOOR_OPEN_LIMIT)
					car.fDoorValue = CABLE_CAR_DOOR_OPEN_LIMIT
				ENDIF
			ENDIF
			
			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
			UPDATE_CABLE_CAR_PLAYER_ENTRY(car)
		BREAK
		CASE CABLECAR_DEPARTING
			//CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Accelerating")
			SET_ENTITY_HAS_GRAVITY(car.cablecarID, FALSE)	
			
			car.fDoorValue -= TIMESTEP()
			IF (car.fDoorValue <= 0.0)
				car.fDoorValue = 0.0
			
				car.carState = CABLECAR_SPEEDUP
				car.fDistTravelled = 0.0
				car.bIsMoving = TRUE
				car.fSpeed = 0.0
				car.iDepartTime = GET_GAME_TIMER()
				car.iJourneyTimer = 0
				PLAY_SOUND_FROM_ENTITY(-1, "Leave_Station", car.cablecarID, "CABLE_CAR_SOUNDS")
				
				car.iRunningSound = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(car.iRunningSound, "Running", car.cablecarID, "CABLE_CAR_SOUNDS")
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " has started moving play running sound")
				car.fDistToNextPoint = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
				
				IF (car.bIsPlayerIn = TRUE)
					PRINT_HELP("FGND_SWTCHCAM")
				ENDIF
			ENDIF
			
			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
		BREAK
		CASE CABLECAR_SPEEDUP	
			IF (car.bIsPlayerIn = TRUE)
				UPDATE_CABLE_CAR_CAMERA(car)
			ENDIF
			
			IF NOT(car.bIsPlayerOnTop)
				car.fSpeed += (fCableCarAcceleration * TIMESTEP() * fTimeStepFactor)
				IF (car.fSpeed >= fCableCarMaxSpeed)
					car.fSpeed = fCableCarMaxSpeed
					car.carState = CABLECAR_MOVING
					CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Acceleration Complete")
					CPRINTLN(DEBUG_AMBIENT, "                                Departure Time:", car.iDepartTime, " Dist Travelled:", car.fDistTravelled)
					
					car.fDoorValue = 0.0
					SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
				ENDIF
			ENDIF
		BREAK	
		CASE CABLECAR_MOVING
			IF (car.bIsPlayerIn = TRUE)
				UPDATE_CABLE_CAR_CAMERA(car)
			ENDIF
			
			IF NOT(car.bIsPlayerOnTop) AND (car.bIsMoving)
				IF (car.iRunningSound = -1)
					car.iRunningSound = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(car.iRunningSound, "Running", car.cablecarID, "CABLE_CAR_SOUNDS")
					CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " has started moving again - play running sound")
				ENDIF				
			ENDIF
			
			IF (!car.bIsPaused)
				car.fSpeed += (fCableCarAcceleration * TIMESTEP() * fTimeStepFactor)
				IF (car.fSpeed >= fCableCarMaxSpeed)
					car.fSpeed = fCableCarMaxSpeed
				ENDIF
			ENDIF
			
			IF ((cableWire[car.iWireIndex].fRopeLength - car.fDistTravelled) < 75.0)
				car.carState = CABLECAR_SLOWDOWN
				
				IF (car.bIsPlayerIn = TRUE)
					IF (bFPSMode = TRUE)
						DISABLE_CABLE_CAR_FIRST_PERSON()
						
						IF (GET_CABLE_CAR_DIRECTION_ENUM(car) = CABLECAR_MOVEUP) 
							ENABLE_CAMERA_STRUCT(externalCamera, cameraID)
						ELSE
							ENABLE_CAMERA_STRUCT_REVERSE_INTERPOLATE(externalCamera, cameraID)
						ENDIF 
					ENDIF 
					ATTACH_PLAYER_TO_CABLE_CAR_ARRIVAL(car)
				ENDIF
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Slowing Down")
			ENDIF
				
			IF NOT (car.bIsMoving) OR (car.fDistTravelled >= cableWire[car.iWireIndex].fRopeLength)
				car.carState = CABLECAR_ARRIVING
			ENDIF
			
			IF (car.bIsPaused) AND (car.bIsPlayerIn = TRUE)
				car.fSpeed -= (fCableCarAcceleration * TIMESTEP() * fTimeStepFactor * 2.0)
				IF (car.fSpeed <= 0.0) 
					car.fSpeed = 0.0
					car.carState = CABLECAR_PAUSESTART
					PLAY_SOUND_FROM_ENTITY(-1, "DOOR_OPEN", car.cablecarID, "CABLE_CAR_SOUNDS")
					CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Pause")
				ENDIF
			ENDIF
		BREAK		
		CASE CABLECAR_PAUSESTART
			car.fDoorValue += TIMESTEP()
			
			IF (car.fDoorValue > CABLE_CAR_DOOR_OPEN_LIMIT)
				car.fDoorValue = CABLE_CAR_DOOR_OPEN_LIMIT	
				car.bIsPaused = TRUE
				car.carState = CABLECAR_PAUSED
				END_CABLE_CAR_CUTSCENE(car)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Doors Open")
			ENDIF

			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
		BREAK
		CASE CABLECAR_PAUSED
			IF IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
					RC_START_CUTSCENE_MODE(<<0, 0, 0>>)
					car.carState = CABLECAR_PAUSEEND
					PLAY_SOUND_FROM_ENTITY(-1, "DOOR_CLOSE", car.cablecarID, "CABLE_CAR_SOUNDS")
				ENDIF
			ELIF NOT IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car, 1.1)
				car.carState = CABLECAR_PAUSEEND
				PLAY_SOUND_FROM_ENTITY(-1, "DOOR_CLOSE", car.cablecarID, "CABLE_CAR_SOUNDS")
				car.bIsPlayerIn = FALSE
			ENDIF
		BREAK
		CASE CABLECAR_PAUSEEND
			car.fDoorValue -= TIMESTEP()
			IF (car.fDoorValue <= 0.0)
				car.fDoorValue = 0.0
				car.carState = CABLECAR_MOVING
				
				IF (car.bIsPlayerIn = TRUE)
					ATTACH_PLAYER_TO_CABLE_CAR(car)
					ENABLE_CABLE_CAR_FIRST_PERSON(GET_CABLE_CAR_DIRECTION_ENUM(car))
				ENDIF 
				car.bIsPaused = FALSE
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Moving Again")
			ENDIF
			
			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
		BREAK	
		CASE CABLECAR_SLOWDOWN		
			IF (car.bIsPlayerIn = TRUE)
				UPDATE_CABLE_CAR_CAMERA(car)
			ENDIF
			
			IF NOT (car.bIsPlayerOnTop)
				car.fSpeed -= (fCableCarAcceleration * TIMESTEP() * fTimeStepFactor)
				IF (car.fSpeed <= 1.0) 
					car.fSpeed = 1.0
				ENDIF
				
				IF NOT (car.bIsMoving)
					car.carState = CABLECAR_ARRIVING
					car.fSpeed = 0.0
					CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Slowing Complete")
					
					PLAY_SOUND_FROM_ENTITY(-1, "Arrive_Station", car.cablecarID, "CABLE_CAR_SOUNDS")
					PLAY_SOUND_FROM_ENTITY(-1, "DOOR_OPEN", car.cablecarID, "CABLE_CAR_SOUNDS")
					SAFE_STOP_AND_RELEASE_SOUND_ID(car.iRunningSound)
				ENDIF	
			ENDIF
		BREAK		
		CASE CABLECAR_ARRIVING
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Arrived")

			car.fDoorValue += TIMESTEP()
			IF (car.fDoorValue > CABLE_CAR_DOOR_OPEN_LIMIT)
				car.fDoorValue = CABLE_CAR_DOOR_OPEN_LIMIT
				CABLE_CAR_ARRIVED(car)
			ENDIF
			
			SET_CABLE_CAR_DOOR_OFFSET(car, car.fDoorValue)
		BREAK	
		CASE CABLECAR_ARRIVED
			IF NOT IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car, 1.125)
				car.carState = CABLECAR_WAITING
				car.iDepartTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(iCableCarDockWaitTime - 8000, iCableCarDockWaitTime + 8000)
				car.fSpdSgn *= -1.0
			ENDIF
		BREAK	
		CASE CABLECAR_DEAD
			// we do this on purpose just incase things player wasn't flagged for some reason
			IF IS_ENTITY_IN_CABLE_CAR(PLAYER_PED_ID(), car)
				car.bIsPlayerIn = TRUE
			ENDIF
			
			IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), car.cablecarID)
				DETACH_ENTITY(PLAYER_PED_ID()) 
			ENDIF
			DETACH_STORED_VEHICLES_IN_CABLE_CAR(car)
			SET_ENTITY_HAS_GRAVITY(car.cablecarID, TRUE)
			FREEZE_ENTITY_POSITION(car.cablecarID, FALSE)
			car.iDiedTime = GET_GAME_TIMER() 
			//ADD_EXPLOSION(GET_ENTITY_COORDS(car.cablecarID) + <<0, 0, CABLE_CAR_WIRE_OFFSET>>, EXP_TAG_CAR)
			CLEAR_HELP()
			
			IF (car.bIsPlayerIn = TRUE)
				END_CABLE_CAR_CUTSCENE(car)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Emergency abort player is still in it")
				CLEAR_HELP()
				
				CLEANUP_CABLE_CAR(car)
				EXIT
				//PRINT_HELP("U_BROKE_CABLECAR")
			ELSE
				IF STREAMVOL_IS_VALID(streamVolCam)
					STREAMVOL_DELETE(streamVolCam)
				ENDIF
			ENDIF
		
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Entering Regen State")
			car.carState = CABLECAR_REGEN
		EXIT
		CASE CABLECAR_REGEN
			// wait till sphere isn't visible for 10 meters
			v = GET_ENTITY_COORDS(car.cablecarID, FALSE)
			IF NOT IS_SPHERE_VISIBLE(v, 10.0)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Is Clear To Regenerate")
				CLEANUP_CABLE_CAR(car)
				//CLEAR_AREA_OF_VEHICLES(v, 1.0)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	IF (car.bIsPlayerIn)
		UPDATE_CABLE_CAR_PLAYER_CONTROLS(car)
	ENDIF
	
	// update movement
	IF (car.bIsMoving)
		IF (car.fSpdSgn >= 0.0)
	 		UPDATE_CABLE_CAR_FORWARD(car)
		ELSE
			UPDATE_CABLE_CAR_BACKWARD(car)
		ENDIF
	ENDIF
	
ENDPROC

PROC UPDATE_CABLE_CAR_REGENERATION(CABLE_CAR &car)
	VECTOR v
	FLOAT t = 0.0

	IF IS_ENTITY_ALIVE(car.cablecarID) 
		EXIT
	ENDIF
	
	IF (car.carState <= CABLECAR_DEAD)
		EXIT
	ENDIF
	
	IF NOT IS_SCREEN_FADED_OUT()
	
		IF (GET_GAME_TIMER() < (car.iDiedTime + iCableCarRegenTime))
			EXIT
		ENDIF
	
		V = GET_WIRE_POSITION(cableWire[car.iWireIndex], 0, t)
		v += <<0, 0, CABLE_CAR_WIRE_OFFSET>>
		IF IS_SPHERE_VISIBLE(v, 30.0)
			EXIT
		ENDIF
	ENDIF
	
	LOAD_CABLE_CAR_ASSETS()
	CREATE_CABLE_CAR(car, car.iWireIndex, 0, t, 1)	// low dock
	_UNLOAD_ASSET_FROM_REQUESTER(asRequest, CABLECAR_ASSET_SLOT)
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car:", car.iWireIndex, " Regenerated")
ENDPROC

PROC UPDATE_WAIT_MESSAGE()
	INT iCheck
	
	#IF IS_DEBUG_BUILD
	IF (cableWire[0].bShowWire OR cableWire[1].bShowWire)
		DRAW_DEBUG_AREA(<<446.258179,5582.625488,780.189209>>, <<446.300507,5560.007813,786.939209>>, 12.250000)
		DRAW_DEBUG_AREA(<<-742.347656,5587.018555,40.665493>>, <<-742.146118,5602.772461,49.847099>>, 12.250000)
	ENDIF
	#ENDIF
	
	IF ((cableCarLeft.bIsPlayerIn = TRUE) OR (cableCarRight.bIsPlayerIn = TRUE))
		EXIT
	ENDIF
	
	IF bNoCarMsgShown = TRUE
		EXIT
	ENDIF
	
	// high dock check
	iCheck = 0
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<446.258179,5582.625488,780.189209>>, <<446.300507,5560.007813,786.939209>>, 12.250000)
		IF (cableCarLeft.bIsMoving) 
			iCheck ++
		ENDIF
		
		IF (cableCarRight.bIsMoving)
			iCheck ++
		ENDIF
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-742.347656,5587.018555,40.665493>>, <<-742.146118,5602.772461,49.847099>>, 12.250000)
		IF (cableCarLeft.bIsMoving) 
			iCheck ++
		ENDIF
		
		IF (cableCarRight.bIsMoving)
			iCheck ++
		ENDIF
	ENDIF
	
	IF (iCheck = 2)
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR] - Pahahaha - you have to wait like everyone else")
		PRINT_HELP("WAIT_CABLE")
		bNoCarMsgShown = TRUE
	ENDIF
ENDPROC

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_WidgetGroup
#ENDIF

#IF IS_DEBUG_BUILD

/// PURPOSE:
/// 	Initializes Wire Debug Widgets
PROC SETUP_CABLECAR_WIRE_DEBUG_WIDGETS(STRING str, CABLECAR_WIRE &wire)
	START_WIDGET_GROUP(str)	
		ADD_WIDGET_BOOL("Show Wire", wire.bShowWire)
		ADD_WIDGET_INT_SLIDER("Show Point", wire.iDebugPoint, 0, COUNT_OF(wire.vPoints) - 1, 1)
		ADD_WIDGET_FLOAT_SLIDER("Show IValue", wire.fDebugInterpolate, 0, 1, 0.000625)
		ADD_WIDGET_BOOL("Warp Car To Debug", wire.bWarpCarToDebugPoint)
		ADD_WIDGET_INT_READ_ONLY("Points Used", wire.iPointsUsed)
		ADD_WIDGET_FLOAT_READ_ONLY("Wire Length", wire.fRopeLength)
		ADD_WIDGET_VECTOR_SLIDER("Wire Start", wire.vPoints[0], -8000.0, 8000.0, 0.05)
		ADD_WIDGET_VECTOR_SLIDER("Wire End", wire.vPoints[wire.iPointsUsed - 1], -8000.0, 8000.0, 0.05)
		ADD_WIDGET_READ_ONLY_VECTOR("Wire Debug Pos", wire.vWireDebugPoint)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_CABLECAR_DEBUG_WIDGETS(STRING str, CABLE_CAR &car)
	START_WIDGET_GROUP(str)	
		ADD_WIDGET_BOOL("Is Moving", car.bIsMoving)
		ADD_WIDGET_INT_READ_ONLY("Journey Timer", car.iJourneyTimer)
		ADD_WIDGET_FLOAT_READ_ONLY("Dist Travelled", car.fDistTravelled)
		ADD_WIDGET_FLOAT_SLIDER("Spd Sgn", car.fSpdSgn, -1, 1, 1)
		ADD_WIDGET_BOOL("Debug Pause", car.bDebugPause)
		START_WIDGET_GROUP("Read Only")	
			ADD_WIDGET_BOOL("Help Shown", car.bHelpShown)
			ADD_WIDGET_INT_READ_ONLY("Halo Debug", car.iHaloStateDebug)
			ADD_WIDGET_INT_READ_ONLY("State Debug", car.iCarStateDebug)
			ADD_WIDGET_INT_READ_ONLY("Depart Time", car.iDepartTime)
			ADD_WIDGET_INT_READ_ONLY("Current Index", car.iWireSegment)
			ADD_WIDGET_FLOAT_READ_ONLY("Interpolate Value", car.fTValue)
			ADD_WIDGET_READ_ONLY_VECTOR("Position", car.vPosition)	
			ADD_WIDGET_FLOAT_READ_ONLY("Speed", car.fSpeed)
			ADD_WIDGET_BOOL("Paused", car.bIsPaused)
			ADD_WIDGET_BOOL("PlayerOnTop", car.bIsPlayerOnTop)
			ADD_WIDGET_BOOL("PlayerIn", car.bIsPlayerIn)
			ADD_WIDGET_BOOL("PlayerRiding", car.bIsPlayerRiding)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	SET_PROFILING_OF_THIS_SCRIPT(FALSE)
	
	m_WidgetGroup = START_WIDGET_GROUP("Cable Car System")	
		START_WIDGET_GROUP("Configuration")	
			ADD_WIDGET_BOOL("Quit Script", bExitScript)
			ADD_WIDGET_BOOL("Warp To Low Dock", bWarpToLowDock)
			ADD_WIDGET_BOOL("Warp To High Dock", bWarpToHighDock)
			ADD_WIDGET_BOOL("No Car Msg Shown", bNoCarMsgShown)
			ADD_WIDGET_FLOAT_SLIDER("Time Step Factor", fTimeStepFactor, 0, 100, 0.25)
			ADD_WIDGET_FLOAT_SLIDER("Imran Factor", fImranFactor, 1.0, 5.0, 0.25)
			ADD_WIDGET_FLOAT_SLIDER("Camera Hack", fCamShiftHack, -180.0, 180.0, 0.25)
			ADD_WIDGET_BOOL("Use SRLS", bUseSRLS)
			ADD_WIDGET_BOOL("Use Focus Point", bUseFocusChange)
		STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Camera Debug")	
			ADD_WIDGET_INT_SLIDER("FPS X Look Limit", FPSCam.iLookXLimit, 0, 180, 1)
			ADD_WIDGET_INT_SLIDER("FPS Y Look Limit", FPSCam.iLookYLimit, 0, 180, 1)
			ADD_WIDGET_BOOL("External Cam On", bExternalCamOn)
			ADD_WIDGET_BOOL("External Cam Off", bExternalCamOff)
			ADD_WIDGET_BOOL("R Interpolate", bInterpolateBackwards)
			ADD_WIDGET_BOOL("Alt Mode", bExternalCamAlternate)
			ADD_WIDGET_INT_SLIDER("Camera Index", iExternalCameraTest, 0, 24, 1)
			ADD_WIDGET_INT_SLIDER("Cam Interptime", iTestInterptime, 0, 15000, 1000)
			ADD_WIDGET_INT_READ_ONLY("Camera Index", iCameraPointIndex)
			ADD_WIDGET_INT_READ_ONLY("ActiveCamera", iActiveCamera)
			ADD_WIDGET_BOOL("Is Cam Static", bIsCamStatic)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Car Debug")	
			ADD_WIDGET_BOOL("Blip Cars", bBlipCars)
			SETUP_CABLECAR_DEBUG_WIDGETS("Left Car", cableCarLeft)
			SETUP_CABLECAR_DEBUG_WIDGETS("Right Car", cableCarRight)
			ADD_WIDGET_FLOAT_SLIDER("Max Speed", fCableCarMaxSpeed, 0, 100, 0.25)
			ADD_WIDGET_FLOAT_SLIDER("Acceleration", fCableCarAcceleration, 0, 20, 0.005)
			ADD_WIDGET_INT_SLIDER("Car Regen Time", iCableCarRegenTime, 250, 600000, 1000)
			ADD_WIDGET_INT_SLIDER("Car Wait Time", iCableCarDockWaitTime, 250, 600000, 1000)
			ADD_WIDGET_BOOL("Create Left Low Car", bCreateAtLowDock[0])
			ADD_WIDGET_BOOL("Create Right Low Car", bCreateAtLowDock[1])
			ADD_WIDGET_BOOL("Create Left High Car", bCreateAtHighDock[0])
			ADD_WIDGET_BOOL("Create Right High Car", bCreateAtHighDock[1])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Wire Debug")	
			SETUP_CABLECAR_WIRE_DEBUG_WIDGETS("Left Wire", cableWire[CABLE_WIRE_LEFT])
			SETUP_CABLECAR_WIRE_DEBUG_WIDGETS("Right Wire", cableWire[CABLE_WIRE_RIGHT])
			ADD_WIDGET_FLOAT_SLIDER("Vert Size", fDebugWireVertexSize, 0, 1, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("Vert Height", fDebugWireVertexHeight, 0, 30, 0.25)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Door Debug")
			ADD_WIDGET_BOOL("Test Doors", bDoorSlideTest)
			ADD_WIDGET_FLOAT_SLIDER("Door Slide", fDoorSlideValue, -5, 5, 0.05)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()

	IF (bWarpToLowDock) 
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		
		
		RC_END_CUTSCENE_MODE()
		CLEAR_FIRST_PERSON_CAMERA(fpsCam)
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vLowDockPosition)
		bWarpToLowDock = FALSE 
	ENDIF
	
	IF (bWarpToHighDock) 
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		
		RC_END_CUTSCENE_MODE()
		CLEAR_FIRST_PERSON_CAMERA(fpsCam)
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vHighDockPosition)
		bWarpToHighDock = FALSE 
	ENDIF
	
	IF (bCreateAtLowDock[0])
		DELETE_CABLE_CAR(cableCarLeft)
		CREATE_CABLE_CAR(cableCarLeft, CABLE_WIRE_LEFT, 0)
		bCreateAtLowDock[0] = FALSE
		bBlipCars = TRUE
	ENDIF

	IF (bCreateAtHighDock[0])
		DELETE_CABLE_CAR(cableCarLeft)
		CREATE_CABLE_CAR(cableCarLeft, CABLE_WIRE_LEFT, cableWire[CABLE_WIRE_LEFT].iPointsUsed - 1, 0, -1)
		bCreateAtHighDock[0] = FALSE
		bBlipCars = TRUE
	ENDIF

	IF (bCreateAtLowDock[1])
		DELETE_CABLE_CAR(cableCarRight)
		CREATE_CABLE_CAR(cableCarRight, CABLE_WIRE_RIGHT, 0)
		bCreateAtLowDock[1] = FALSE
		bBlipCars = TRUE
	ENDIF

	IF (bCreateAtHighDock[1])
		DELETE_CABLE_CAR(cableCarRight)
		CREATE_CABLE_CAR(cableCarRight, CABLE_WIRE_RIGHT, cableWire[CABLE_WIRE_RIGHT].iPointsUsed - 2, 1.0, -1)
		bCreateAtHighDock[1] = FALSE
		bBlipCars = TRUE
	ENDIF
	
	IF (bBlipCars)
		SAFE_REMOVE_BLIP(blipID[0])
		SAFE_REMOVE_BLIP(blipID[1])
		IF IS_ENTITY_OK(cableCarLeft.cablecarID)
			blipID[0] = CREATE_BLIP_FOR_OBJECT(cableCarLeft.cablecarID)
		ENDIF
		
		IF IS_ENTITY_OK(cableCarRight.cablecarID)
			blipID[1] = CREATE_BLIP_FOR_OBJECT(cableCarRight.cablecarID)
		ENDIF
		bBlipCars = FALSE
	ENDIF
	
	IF (bExternalCamOn)
		bIsCamStatic = FALSE
		SET_EXTERNAL_CAMERA(externalCamera, iExternalCameraTest, bExternalCamAlternate)
		IF (externalCamera.iInterpolateTime = 0)
			iTestInterptime = 0
			bIsCamStatic = TRUE
		ENDIF
		externalCamera.iInterpolateTime = iTestInterptime
		
		IF (bIsCamStatic = FALSE) AND (bInterpolateBackwards)
			ENABLE_CAMERA_STRUCT_REVERSE_INTERPOLATE(externalCamera, cameraID)
		ELSE
			ENABLE_CAMERA_STRUCT(externalCamera, cameraID)
		ENDIF
		
		bExternalCamOn = FALSE
	ENDIF
	
	IF (bExternalCamOff)
		SHUTDOWN_CAMERA(cameraID)
		bExternalCamOff = FALSE
	ENDIF
	
	IF (bDoorSlideTest)
		SET_CABLE_CAR_DOOR_OFFSET(cableCarLeft, fDoorSlideValue)
		SET_CABLE_CAR_DOOR_OFFSET(cableCarRight, fDoorSlideValue)
	ENDIF
	
	IF (cableWire[CABLE_WIRE_LEFT].bWarpCarToDebugPoint)
		cableCarLeft.iWireSegment = cableWire[CABLE_WIRE_LEFT].iDebugPoint
		cableCarLeft.fTValue = cableWire[CABLE_WIRE_LEFT].fDebugInterpolate
		cableCarLeft.vPosition = GET_WIRE_POSITION(cableWire[CABLE_WIRE_LEFT], cableCarLeft.iWireSegment, cableCarLeft.fTValue)
		cableCarLeft.bIsMoving = FALSE
		IF IS_ENTITY_OK(cableCarLeft.cablecarID)
			SET_ENTITY_COORDS(cableCarLeft.cablecarID, cableCarLeft.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
		ENDIF
		
		cableWire[CABLE_WIRE_LEFT].bWarpCarToDebugPoint = FALSE
	ENDIF
	
	IF (cableWire[CABLE_WIRE_RIGHT].bWarpCarToDebugPoint)
		cableCarRight.iWireSegment = cableWire[CABLE_WIRE_RIGHT].iDebugPoint
		cableCarRight.fTValue = cableWire[CABLE_WIRE_RIGHT].fDebugInterpolate
		cableCarRight.vPosition = GET_WIRE_POSITION(cableWire[CABLE_WIRE_RIGHT], cableCarRight.iWireSegment, cableCarRight.fTValue)
		cableCarRight.bIsMoving = FALSE
		IF IS_ENTITY_OK(cableCarRight.cablecarID)
			SET_ENTITY_COORDS(cableCarRight.cablecarID, cableCarRight.vPosition + <<0, 0, CABLE_CAR_WIRE_OFFSET>>)
		ENDIF
		
		cableWire[CABLE_WIRE_RIGHT].bWarpCarToDebugPoint = FALSE
	ENDIF
ENDPROC	

PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC	
#ENDIF
 
/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC SCRIPT_CLEANUP(BOOL bDelete = FALSE)
	VECTOR v
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup")
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup")
	
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
		SAFE_REMOVE_BLIP(blipID[0])
		SAFE_REMOVE_BLIP(blipID[1])
	#ENDIF
	
	IF IS_PLAYER_RIDING_ANY_CABLE_CAR()
		CLEAR_FOCUS()
	ENDIF
	
	IF bSRLOK = TRUE
		CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car System SRL Ended")
		bSRLOK = FALSE
		END_SRL()
	ENDIF
	
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_COUNTRYSIDE_CHILEAD_CABLE_CAR_LINE", FALSE, TRUE)
	RC_END_CUTSCENE_MODE()	// for some reason going to into multiplayer but not freezes controls
	g_bIsOnCableCar = FALSE
	
	SHUTDOWN_CAMERA(cameraID)
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	IS_ENTITY_OK(PLAYER_PED_ID()) 
	
	IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_SP_TO_MP	
		IF IS_PLAYER_RIDING_ANY_CABLE_CAR()
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup For MP - Emergency Teleport")
			DETACH_PED_FROM_CABLE_CAR(PLAYER_PED_ID(), cableCarLeft)
			DETACH_PED_FROM_CABLE_CAR(PLAYER_PED_ID(), cableCarRight)
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vLowDockPosition) < 200)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vLowDockPosition)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup For MP - Player warped to Low Dock")			
			ELIF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vHighDockPosition) < 200)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vHighDockPosition)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup For MP - Player warped to High Dock")
			ELSE
				v = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0, 0, 5.0>>
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Cable Car - Script Cleanup For MP - Not sure what to do here put the player on the floor?")
				SET_ENTITY_COORDS(PLAYER_PED_ID(), v)
				v.z = INVALID_WORLD_Z
				SET_ENTITY_COORDS(PLAYER_PED_ID(), v)
			ENDIF
		ENDIF
		bDelete = TRUE
	ENDIF
	
	IF (bDelete)
		DELETE_CABLE_CAR(cableCarLeft)
		DELETE_CABLE_CAR(cableCarRight)
	ELSE
		CLEANUP_CABLE_CAR(cableCarLeft)
		CLEANUP_CABLE_CAR(cableCarRight)
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	UNLOAD_REQUESTED_ASSETS(asRequest)	
	
	// Fix for 2018293: Clearing a streaming volume at an arbitrary moment in a background script is 
	// considered dangerous as it could clash with new load scenes in other scripts. Moved stream vol 
	// delete to be our last cleanup step and then we hang this thread until we are sure new load scenes 
	// are cleared. Note: this should only ever happen behind a black screen and all other cleanup has
	// already completed. -Ben R
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
		CDEBUG1LN(DEBUG_AMBIENT, "[CABLECAR]: Waiting for NEW_LOAD_SCENE to end before deleting steaming volume.")
		WAIT(0)
	ENDWHILE
	
	IF STREAMVOL_IS_VALID(streamVolCam)
		STREAMVOL_DELETE(streamVolCam)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Terminating cable car script.")
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT		
	VECTOR v
	VECTOR vLastFinalCamPos = GET_FINAL_RENDERED_CAM_COORD()
	
	// Register script to launch after reload
    IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH (HASH("cablecar")) > 1
    	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:CableCar is attempting to launch with an instance already active.")
    	TERMINATE_THIS_THREAD()
    ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Initializing")

	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_MAGDEMO | FORCE_CLEANUP_FLAG_REPEAT_PLAY | FORCE_CLEANUP_FLAG_DIRECTOR))
		RC_END_CUTSCENE_MODE()
		g_bIsOnCableCar = FALSE
		SCRIPT_CLEANUP()
	ENDIF
	
	// wait till player is within area
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Player is within cable car area - Start Script Proper")

	IS_ENTITY_OK(PLAYER_PED_ID())
	LOAD_CABLE_CAR_ASSETS()
		
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Setting Up Cables")
	SETUP_CABLE_CAR_WIRE_LEFT(cableWire[CABLE_WIRE_LEFT])
	SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED(cableWire[CABLE_WIRE_RIGHT])
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_COUNTRYSIDE_CHILEAD_CABLE_CAR_LINE", TRUE, TRUE)
	
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Creating Cable Cars")
	CREATE_CABLE_CAR(cableCarLeft, CABLE_WIRE_LEFT)	
	CREATE_CABLE_CAR(cableCarRight, CABLE_WIRE_RIGHT) 	
	UNLOAD_CABLE_CAR_MODEL_ASSETS()
	
	cableCarLeft.iDepartTime = GET_GAME_TIMER() + iCableCarDockWaitTime
	cableCarRight.iDepartTime = GET_GAME_TIMER() + iCableCarDockWaitTime

	DUMMY_REFERENCE_BOOL(bLoadSceneOK)
	DUMMY_REFERENCE_FLOAT(fSceneLoadFarClip)
	IS_VECTOR_ZERO(vSceneLoadDirection)
	IS_VECTOR_ZERO(vSceneLoadPosition)
	
	#IF IS_DEBUG_BUILD
		DUMMY_REFERENCE_BOOL(bshowLoadSceneArea)
	#ENDIF
	
	WHILE (TRUE)
		IF (g_bForceNoCableCar = TRUE)
			REPAIR_CABLE_CAR_DOORS(cableCarLeft)
			REPAIR_CABLE_CAR_DOORS(cableCarRight)
		ENDIF
			
		IF (NOT IS_ENTITY_OK(PLAYER_PED_ID())) OR (g_bForceNoCableCar = TRUE)
			RC_END_CUTSCENE_MODE()
			g_bIsOnCableCar = FALSE
			SHUTDOWN_CAMERA(cameraID)
			CLEAR_FIRST_PERSON_CAMERA(fpsCam)
//			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)			// SamH: Removing as requested in b*1776373. Problem is fixed in code now.
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (bExitScript)
				RC_END_CUTSCENE_MODE()
//				CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)		// SamH: Removing as requested in b*1776373. Problem is fixed in code now.
				SCRIPT_CLEANUP(TRUE)
			ENDIF	
		#ENDIF
		
		/*
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		*/
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_CABLE_CAR_WIRE(cableWire[CABLE_WIRE_LEFT], 255, 0, 0, 255)
			DRAW_DEBUG_CABLE_CAR_WIRE(cableWire[CABLE_WIRE_RIGHT], 0, 0, 255, 255)
			
			/*
			IF (bShowLoadSceneArea)
				IF (bLoadSceneOK = FALSE)
					DRAW_DEBUG_SPHERE(vSceneLoadPosition, fSceneLoadFarClip, 255, 0, 0, 128)		
				ELSE
					DRAW_DEBUG_SPHERE(vSceneLoadPosition, fSceneLoadFarClip, 0, 255, 0, 128)
				ENDIF		
				
				DRAW_DEBUG_SPHERE(vSceneLoadPosition, 0.0625, 255, 0, 0, 128)	
				DRAW_DEBUG_LINE_WITH_TWO_COLOURS(vSceneLoadPosition, vSceneLoadPosition + (vSceneLoadDirection) * 10.0, 255, 0, 0, 255)
			ENDIF
			*/
		#ENDIF
		
		v = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF (v.x <= CABLE_CAR_SHUTDOWN_MIN_X) OR (v.x >= CABLE_CAR_SHUTDOWN_MAX_X) OR (v.y <= CABLE_CAR_SHUTDOWN_MIN_Y) OR (v.y >= CABLE_CAR_SHUTDOWN_MAX_Y)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]: Player going out of Range - Terminate the script")
			SCRIPT_CLEANUP()
		ENDIF
		
		UPDATE_CABLE_CAR(cableCarLeft)
		UPDATE_CABLE_CAR(cableCarRight)
		UPDATE_CABLE_CAR_REGENERATION(cableCarLeft)
		UPDATE_CABLE_CAR_REGENERATION(cableCarRight)
		
		IF bFPSMode = TRUE
			UPDATE_FIRST_PERSON_CAMERA(fpsCam, FALSE)
		ELSE
			UPDATE_WAIT_MESSAGE()
		ENDIF

		IF IS_ENTITY_OK(PLAYER_PED_ID())
			IF IS_SCREEN_FADED_IN() AND (bSkipped = FALSE)
				IF IS_PLAYER_RIDING_ANY_CABLE_CAR()
					v = GET_FINAL_RENDERED_CAM_COORD()
					
					IF bUseFocusChange
						SET_FOCUS_POS_AND_VEL(v, GET_FINAL_RENDERED_CAM_COORD() - vLastFinalCamPos)
						vLastFinalCamPos = v
					ENDIF
					
					IF GET_GAME_TIMER() > iRevealCounter
						v = GET_ENTITY_COORDS(PLAYER_PED_ID())
						SET_MINIMAP_FOW_REVEAL_COORDINATE(v)
						iRevealCounter = GET_GAME_TIMER() + 5000
						CPRINTLN(DEBUG_AMBIENT, "[CABLE CAR]: REVEAL MAP AT:", v)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		IF (bIsOnMission = FALSE) AND GET_MISSION_FLAG() AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("bailbond3")) = 0)
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:On Mission - Seal up the Cable Car")
			REPAIR_CABLE_CAR_DOORS(cableCarLeft)
			REPAIR_CABLE_CAR_DOORS(cableCarRight)
			
			SAFE_STOP_AND_RELEASE_SOUND_ID(cableCarLeft.iRunningSound)
			SAFE_STOP_AND_RELEASE_SOUND_ID(cableCarRight.iRunningSound)
			
			/*
			IF STREAMVOL_IS_VALID(cameraStreamVol)
				CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:On Mission - Delete Streamvol")
				STREAMVOL_DELETE(cameraStreamVol)
			ENDIF
			*/
			bIsOnMission = TRUE
		ENDIF
		
		IF (bIsOnMission = TRUE) AND NOT GET_MISSION_FLAG()
			CPRINTLN(DEBUG_AMBIENT, "[CABLECAR]:On Mission - Reopen up the Cable Car")
			FORCE_BREAK_CABLE_CAR_DOORS(cableCarLeft)
			FORCE_BREAK_CABLE_CAR_DOORS(cableCarRight)
			bIsOnMission = FALSE
		ENDIF
			
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT


