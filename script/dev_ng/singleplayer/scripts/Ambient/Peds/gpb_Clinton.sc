// gpb_Clinton.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.

USING "GenericBrain/Generic_Brain_Header.sch"

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_MILITARYBUM
	txtPedMissionID = "PBCL"
	txtPedVoiceID = "CLINTON"
	txtBaseIdleDict = "Special_Ped@Clinton"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	talkWithRandom = TRUE
	
	max_conversation_offset = 15
	max_random_offset = 5
	
	
	sInteractLabel = "PBCL_INTERACT"
	greet_michael = "PBCL_CONV_M1"
	greet_trevor = "PBCL_CONV_T1"
	greet_franklin = "PBCL_CONV_F1"
	idle_dic = "special_ped@Clinton@base"
	into_idle_dic = "special_ped@clinton@intro"
	
	thisPed = CLINTON
	
	starting_offset = 0
	
	max_conversation_split_offsets[0] = 6
	max_conversation_split_offsets[1] = 5
	max_conversation_split_offsets[2] = 5
	max_conversation_split_offsets[3] = 6
	max_conversation_split_offsets[4] = 4
	max_conversation_split_offsets[5] = 5
	max_conversation_split_offsets[6] = 3
	max_conversation_split_offsets[7] = 4
	max_conversation_split_offsets[8] = 5
	max_conversation_split_offsets[9] = 4
	max_conversation_split_offsets[10] = 3
	max_conversation_split_offsets[11] = 3
	max_conversation_split_offsets[12] = 3
	max_conversation_split_offsets[13] = 6
	max_conversation_split_offsets[14] = 3
	
	
	max_random_split_offsets[0] = 2
	max_random_split_offsets[1] = 2
	max_random_split_offsets[2] = 2
	max_random_split_offsets[3] = 2
	max_random_split_offsets[4] = 2
	
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@Clinton"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "We_Were_On_A_Ridge"
		BREAK
		CASE 2
			txtAnimToPlay = "Theyll_Kill_Ya"
		BREAK
		CASE 3
			txtAnimToPlay = "When_Your_In_The"
		BREAK
		CASE 4
			txtAnimToPlay = "What_Would_You_Do"
		BREAK
		CASE 5
			txtAnimToPlay = "War_Is_Beauty"
		BREAK
		CASE 6
			txtAnimToPlay = "War_Crimes"
		BREAK
		CASE 7
			txtAnimToPlay = "Pow_Pow_Pow"
		BREAK
		CASE 8
			txtAnimToPlay = "War_Is_Hell"
		BREAK
		CASE 9
			txtAnimToPlay = "Let_Me_Tell_All"
		BREAK
		CASE 10
			txtAnimToPlay = "This_Country_Used_To"
		BREAK
		CASE 11
			txtAnimToPlay = "When_Your_Deep_In"
		BREAK
		CASE 12
			txtAnimToPlay = "I_Kill_For_You"
		BREAK
		CASE 13
			txtAnimToPlay = "Charlies_Coming_Back"
		BREAK
		CASE 14
			txtAnimToPlay = "You_Know_What_They"
		BREAK
		CASE 15
			txtAnimToPlay = "At_Least_You_Didnt"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	SWITCH (random_reply)
		CASE 0
			txt_label = "Im_In_Hell"
		BREAK
		CASE 1
			txt_label = "Living_In_This_Plastic"
		BREAK
		CASE 2
			txt_label = "Im_Having_Myself"
		BREAK
		CASE 3
			txt_label = "Im_Having_Just"
		BREAK
		CASE 4
			txt_label = "You_Dont_Give_A"
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	return GET_CONVERSATION_SECTION_COUNT() //max_random_split_offsets[random_reply]
ENDFUNC


USING "GenericBrain/Generic_Brain.sch"


