

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//////////////////////////////////////////////////////////
// 				PREACHER
//		   		Kevin Wong
//
//			1

//
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///    
///    
///    
///    
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING"script_PLAYER.sch"

USING "rage_builtins.sch"





///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM PREACHERStageFlag
	PREACHERInit,
	PREACHERWaitForPlayer,
	
	PREACHERCleanup
ENDENUM



ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

BOOL bAssetsLoaded = FALSE
VECTOR v_spawnpoint
VECTOR voffset_temp
INT random_int
PED_INDEX PREACHER

BOOL flag_preacher_playing_anim_short = FALSE
BOOL flag_preacher_playing_anim_long = FALSE

SEQUENCE_INDEX  a_sequence_temp
///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
PROC missionCleanup()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()

ENDPROC


PROC MISSION_FAILED()

ENDPROC

PROC create_preacher()


	//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<50.0, 50.0, 50.0>>, FALSE)
		PRINTSTRING("about to Created preacher \n")

		REQUEST_MODEL(S_M_M_StrPreach_01) // PREACHER
		REQUEST_ANIM_DICT("amb@PREACHER")
		
		WHILE NOT HAS_MODEL_LOADED(S_M_M_StrPreach_01)
		OR NOT HAS_ANIM_DICT_LOADED("amb@PREACHER") 
			WAIT(0)
		ENDWHILE

		
	
		
		voffset_temp.z = v_spawnpoint.z - 1.0
		PREACHER = CREATE_PED(PEDTYPE_DEALER,S_M_M_StrPreach_01, <<v_spawnpoint.x, v_spawnpoint.y,voffset_temp.z>>, 0.0)
		PRINTSTRING("Created preacher \n")
		//PREACHER = CREATE_PED(PEDTYPE_DEALER,S_M_M_StrPreach_01, v_spawnpoint, 0.0)
		

		IF NOT IS_ENTITY_DEAD(PREACHER)
		
			#IF IS_DEBUG_BUILD
				BLIP_INDEX debug_ped_blip
				debug_ped_blip = ADD_BLIP_FOR_ENTITY(PREACHER)
				SET_BLIP_SCALE(debug_ped_blip, 0.6 )
				SET_BLIP_AS_FRIENDLY(debug_ped_blip, TRUE)
			#endif
			//SET_ENTITY_HEADING(PREACHER, voffset_temp)

			OPEN_SEQUENCE_TASK(a_sequence_temp)
				TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
				TASK_PLAY_ANIM(NULL, "amb@PREACHER", "PREACH", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(a_sequence_temp)
			TASK_PERFORM_SEQUENCE(PREACHER, a_sequence_temp)
			CLEAR_SEQUENCE_TASK(a_sequence_temp)
		ENDIF
		

		bAssetsLoaded = TRUE
//	ENDIF
	
ENDPROC

PROC make_PREACHER_call_over_player()
	IF TIMERB() > 7000
		IF NOT IS_ENTITY_DEAD(PREACHER)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), PREACHER, <<10.0, 10.0, 10.0>>, FALSE)
			AND flag_preacher_playing_anim_long = FALSE
					SETTIMERB(0)
					voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
					OPEN_SEQUENCE_TASK(a_sequence_temp)
						TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
						TASK_PLAY_ANIM(NULL, "amb@PREACHER", "PREACH", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(a_sequence_temp)
					IF NOT IS_ENTITY_DEAD(PREACHER)
						TASK_PERFORM_SEQUENCE(PREACHER, a_sequence_temp)
					ENDIF
				//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PREACHER PERFORMING 1", 4000, 1) //
					CLEAR_SEQUENCE_TASK(a_sequence_temp)
					flag_preacher_playing_anim_long = TRUE
			ENDIF
		
		
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), PREACHER, <<3.0, 3.0, 3.0>>, FALSE)
			AND flag_preacher_playing_anim_short = FALSE
					voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
					SETTIMERB(0)
					OPEN_SEQUENCE_TASK(a_sequence_temp)
						TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
						TASK_PLAY_ANIM(NULL, "amb@PREACHER", "PREACH", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
						
					CLOSE_SEQUENCE_TASK(a_sequence_temp)
					
						TASK_PERFORM_SEQUENCE(PREACHER, a_sequence_temp)
					ENDIF
					CLEAR_SEQUENCE_TASK(a_sequence_temp)
				//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PREACHER PERFORMING 2", 4000, 1) //
					flag_preacher_playing_anim_short = TRUE
			ENDIF
		ENDIF
	
	/*
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<10.0, 10.0, 10.0>>, FALSE)
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<3.0, 3.0, 3.0>>, FALSE)
				SETTIMERB(0)
				voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
					TASK_PLAY_ANIM(NULL, "amb@PREACHER", "PREACH", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				IF NOT IS_ENTITY_DEAD(PREACHER)
					TASK_PERFORM_SEQUENCE(PREACHER, a_sequence_temp)
				ENDIF
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PREACHER PERFORMING 1", 4000, 1) //
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
		ENDIF
	
	
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<3.0, 3.0, 3.0>>, FALSE)
				voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
				SETTIMERB(0)
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
					TASK_PLAY_ANIM(NULL, "amb@PREACHER", "PREACH", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				IF NOT IS_ENTITY_DEAD(PREACHER)
					TASK_PERFORM_SEQUENCE(PREACHER, a_sequence_temp)
				ENDIF
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
				PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PREACHER PERFORMING 2", 4000, 1) //
		ENDIF*/
	//ENDIF
	
ENDPROC





///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
	SCRIPT(coords_struct in_coords)
//	SCRIPT(PED_INDEX passed_ped)	
		WAIT(0)
		
	//PREACHER = passed_ped
		
		
	v_spawnpoint = in_coords.vec_coord[0]
	PRINTVECTOR(v_spawnpoint)
	PRINTNL()

	PRINTSTRING("preacher")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	/*

	IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
		IF NOT HAVE_ENOUGH_DAYS_PASSED_SINCE_LAST_RUN(g_ipreacherDateStamp)
			PRINTSTRING("preacher not enough days have passed \n")
			missionCleanup()
		ENDIF
	ENDIF*/

	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS| FORCE_CLEANUP_FLAG_RANDOM_EVENTS)		
		PRINTSTRING("preacher death arrest \n")
		missionCleanup()
	ENDIF 

	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			
			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
				SWITCH ambStage
					CASE ambCanRun
						IF bAssetsLoaded = TRUE
							ambStage = (ambRunning)
						ELSE
							create_preacher()
						ENDIF
					BREAK
					CASE ambRunning
						//DO STUFF
						IF random_int = 0
							make_PREACHER_call_over_player()
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				PRINTSTRING("Main mission is running \n")
				missionCleanup()
			ENDIF
		ELSE
			PRINTSTRING("preacher not in activation range \n")
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
///    //////////////////////////////////////////////////////////
///    							DEBUG
///    //////////////////////////////////////////////////////////

