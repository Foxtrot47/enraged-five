// gpb_BillBinder.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the player when he leaves.

// Bill is the Maniac right-Wing financial guru
USING "GenericBrain/Generic_Brain_Header.sch"

/// PURPOSE:
///    Any one-time setup the hero needs. In this case, adding people to the conversation, setting up a model, etc.
PROC GENERIC_BRAIN_CUSTOM_INIT()
	// Set the ped's model.
	ePedModel = U_M_O_FinGuru_01
	
	// Set the ped's baseline mission ID
	txtPedMissionID = "PBBB"
	
	// Set the ped's speaking voice ID
	txtPedVoiceID = "BILLBINDER"
	
	txtBaseIdleDict = "Special_Ped@bill"
	txtBaseIdleAnim = "Base"
	
	
	ePedModel = U_M_O_FinGuru_01
	txtPedMissionID = "PBBB"
	txtPedVoiceID = "BILLBINDER"
	txtBaseIdleDict = "Special_Ped@bill"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBBB_INTERACT"
	greet_michael = "PBBB_CONV_GM"
	greet_trevor = "PBBB_CONV_GT"
	greet_franklin = "PBBB_CONV_GF"
	idle_dic = "special_ped@bill@base"
	into_idle_dic = "special_ped@bill@intro"
	
	thisPed = BILL
	
	max_conversation_split_offsets[0] = 7
	max_conversation_split_offsets[1] = 5
	max_conversation_split_offsets[2] = 5
	max_conversation_split_offsets[3] = 7
	max_conversation_split_offsets[4] = 7
	max_conversation_split_offsets[5] = 4
	max_conversation_split_offsets[6] = 6
	max_conversation_split_offsets[7] = 6
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_TREV_CON_BOUGHT)
		conversation_number[CHAR_TREVOR] = 2
	ENDIF
	
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	
	txtAnimRoot = "Special_Ped@bill"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "Bill_ig_1_A_01_IHearWhatYourSayin"
		BREAK
		CASE 2
			txtAnimToPlay = "Bill_ig_1_A_03_OhIKnowWhatYouAreThinking"
		BREAK
		CASE 3
			txtAnimToPlay = "Bill_ig_1_A_04_TheWholePlaceIsAGoddamPonzieScheme"
		BREAK
		CASE 4
			txtAnimToPlay = "Bill_ig_1_B_01_ImOfferingIronClad"
		BREAK
		CASE 5
			txtAnimToPlay = "Bill_ig_1_B_02_IKnowWhatYouAreThinking"
		BREAK
		CASE 6
			txtAnimToPlay = "Bill_ig_1_C_01_DontBeAFool"
		BREAK
		CASE 7
			txtAnimToPlay = "Bill_ig_1_C_02_YesItsADogEatDog"
		BREAK
		CASE 8
			txtAnimToPlay = "Bill_ig_1_C_03_ILikeSharingInformation"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		IF conversation_number[CHAR_TREVOR] = 1
			txt_label = "Bill_ig_4_03_convo_TheEliteAreOutToCrush"
		ELSE
			txt_label = "BILL_IG_5_02_CONVO_GoodToSeeYouAgain"
		ENDIF
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "Bill_ig_2_CONVO_michael_YouSeemMellow"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "bill_ig_3_convo_franklin_nothingisupman"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		IF conversation_number[CHAR_TREVOR] = 1
			return 14
		ELSE
			return 5
		ENDIF
	ELIF (ePlayer = CHAR_MICHAEL)
		return 4
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 5
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
