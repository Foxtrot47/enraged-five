// gpb_Baygor.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

// Baygor is the Espilonist Member
USING "GenericBrain/Generic_Brain_Header.sch"

/// PURPOSE:
///    Any one-time setup the hero needs. In this case, adding people to the conversation, setting up a model, etc.
PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_ZOMBIE_01
	txtPedMissionID = "PBZB"
	txtPedVoiceID = "ZOMBIE"
	txtBaseIdleDict = "Special_Ped@zombie"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBZB_INTERACT"
	greet_michael = "PBZB_CONV_GM"
	greet_trevor = "PBZB_CONV_GT"
	greet_franklin = "PBZB_CONV_GF"
	idle_dic = "special_ped@zombie@base"
	into_idle_dic = "special_ped@zombie@intro"
	
	thisPed = ZOMBIE
	
	max_conversation_split_offsets[0] = 9
	max_conversation_split_offsets[1] = 8
	max_conversation_split_offsets[2] = 7
	max_conversation_split_offsets[3] = 14
	max_conversation_split_offsets[4] = 8
	max_conversation_split_offsets[5] = 7
	max_conversation_split_offsets[6] = 10
	max_conversation_split_offsets[7] = 7
	
ENDPROC

OBJECT_INDEX phone
BOOL phoneCreated = FALSE
PROC GENERIC_BRAIN_ON_CREATE()
	PRINTSTRING("[SP] SET_PED_CAN_EVASIVE_DIVE FALSE") PRINTNL()
	SET_PED_CAN_EVASIVE_DIVE(pedSpecial, FALSE)
	SET_PED_CONFIG_FLAG(pedSpecial, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(pedSpecial, PCF_ActivateRagdollFromMinorPlayerContact, TRUE)
	
ENDPROC

PROC GENERIC_BRAIN_ON_PLAY_MONOLOGUE(INT i)
	UNUSED_PARAMeTER(i)
	/*IF i = 3
		IF NOT phoneCreated
			phone = CREATE_OBJECT(PROP_NPC_PHONE, GET_ENTITY_COORDS(pedSpecial))
			ATTACH_ENTITY_TO_ENTITY(phone, pedSpecial, GET_PED_BONE_INDEX(pedSpecial, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
			phoneCreated = TRUE
		ENDIF
	ENDIF*/
ENDPROC

PROC GENERIC_BRAIN_ON_END_MONOLOGUE(INT i)
	UNUSED_PARAMeTER(i)
	/*IF i = 3
		IF phoneCreated
			DETACH_ENTITY(phone)
			DELETE_OBJECT(phone)
			phoneCreated = FALSE
		ENDIF
	ENDIF*/
ENDPROC

FUNC BOOL CAN_PED_LOOK_AT_PLAYER()
	IF conversation_offset[thisPed] = 3
		return FALSE
	ENDIF
	return TRUE
ENDFUNC

FUNC BOOL TALKING_ON_PHONE()
	IF (conversation_offset[thisPed] = 3)
	AND (conversation_split_offset >= 4 AND conversation_split_offset <= 12)
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

PROC GENERIC_BRAIN_ON_PLAYING_MONOLOGUE(INT i, INT split_i, FLOAT time)
	UNUSED_PARAMeTER(i)
	IF i = 3
		IF split_i = 3 AND NOT phoneCreated AND time >= 0.773 
			phone = CREATE_OBJECT(PROP_NPC_PHONE, GET_ENTITY_COORDS(pedSpecial))
			ATTACH_ENTITY_TO_ENTITY(phone, pedSpecial, GET_PED_BONE_INDEX(pedSpecial, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
			phoneCreated = TRUE
		ENDIF
		IF split_i = 12 AND phoneCreated AND time >= 0.116
			DETACH_ENTITY(phone)
			DELETE_OBJECT(phone)
			phoneCreated = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PED_FLEE_WHEN_BUMPED()
	return TALKING_ON_PHONE()
ENDFUNC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	IF phoneCreated
		IF TALKING_ON_PHONE()
			DETACH_ENTITY(phone)
			phoneCreated = FALSE
		ELSE
			DELETE_OBJECT(phone)
			phoneCreated = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_TALK_TO_PED()
	//Don't allow talking when on phone.
	return NOT TALKING_ON_PHONE()
ENDFUNC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	
	txtAnimRoot = "Special_Ped@zombie"
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "IAmUndead"
		BREAK
		CASE 2
			txtAnimToPlay = "DoYouWantAPhotoWithMe"
		BREAK
		CASE 3
			txtAnimToPlay = "Careful"
		BREAK
		CASE 4
			txtAnimToPlay = "IamTheUndead"
		BREAK
		CASE 5
			txtAnimToPlay = "BrainsItsBrains"
		BREAK
		CASE 6
			txtAnimToPlay = "IAmThevinewoodZombie"
		BREAK
		CASE 7
			txtAnimToPlay = "RRRItsBrains"
		BREAK
		CASE 8
			txtAnimToPlay = "WhoSaysCosplayIsA"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "CONVO_TREVOR_MYBROTHER"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "CONVO_MICHAEL_OHHOWYOUDOING"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "CONVO_FRANKLIN_HEYWASSUP"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 19
	ELIF (ePlayer = CHAR_MICHAEL)
		return 8
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 7
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
