// gpb_Mime.sc
CONST_INT 	PED_BRAIN_PED_HAS_ABUSE_DIALOGUE 	1
CONST_INT 	PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1	
CONST_INT	PED_BRAIN_PED_LOOKS_AT_PEOPLE		0

USING "GenericBrain/Generic_Brain_Header.sch"

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = S_M_Y_MIME
	txtPedMissionID = "PBMM"
	txtPedVoiceID = "MIME"
	txtBaseIdleDict = "Special_Ped@mime"
	txtBaseIdleAnim = "Base"
	talkWithAnims = TRUE
	noConversation = TRUE
	max_conversation_offset = 8
	sInteractLabel = ""
	greet_michael = ""
	greet_trevor = ""
	greet_franklin = ""
	idle_dic = "special_ped@mime@base"
	into_idle_dic = "special_ped@mime@intro"
	canInteract = FALSE
	
	thisPed = MIME

	max_conversation_split_offsets[0] = 1
	max_conversation_split_offsets[1] = 1
	max_conversation_split_offsets[2] = 1
	max_conversation_split_offsets[3] = 1
	max_conversation_split_offsets[4] = 1
	max_conversation_split_offsets[5] = 1
	max_conversation_split_offsets[6] = 1
	max_conversation_split_offsets[7] = 1
	
ENDPROC

PROC PED_BUMPED()
	PRINTSTRING("PAul - PED_BUMPED") PRINTNL()
	ADD_PED_FOR_DIALOGUE(convoPed, 3, pedSpecial, txtPedVoiceID)	
	CREATE_CONVERSATION(convoPed, txt_block, "PBMM_ABUSE", CONV_PRIORITY_AMBIENT_MEDIUM, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN)
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@mime"
	
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "02_IG_1_R"
		BREAK
		CASE 2
			txtAnimToPlay = "02_IG_1_CLI"
		BREAK
		CASE 3
			txtAnimToPlay = "03_IG_1_OPEN_WIND"
		BREAK
		CASE 4
			txtAnimToPlay = "09_IG_1_BALO"
		BREAK
		CASE 5
			txtAnimToPlay = "10_IG_1_WA"
		BREAK
		CASE 6
			txtAnimToPlay = "12_IG_1_BEATEN_"
		BREAK
		CASE 7
			txtAnimToPlay = "11_IG_1_RUN_AW"
		BREAK
		CASE 8
			txtAnimToPlay = "08_IG_1_WALL_BA"
		BREAK
		
	ENDSWITCH
ENDPROC

USING "GenericBrain/Generic_Brain.sch"


