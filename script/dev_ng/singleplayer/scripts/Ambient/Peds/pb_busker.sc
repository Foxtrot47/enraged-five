

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//////////////////////////////////////////////////////////
// 				busker
//		   		Kevin Wong
//
//			1

//
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///    
///    
///    
///    
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING"script_PLAYER.sch"

USING "rage_builtins.sch"





///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM buskerStageFlag
	buskerInit,
	buskerWaitForPlayer,
	
	buskerCleanup
ENDENUM



ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

BOOL bAssetsLoaded = FALSE
VECTOR v_spawnpoint
VECTOR voffset_temp
INT random_int
PED_INDEX busker
PED_INDEX drug_buyer

SEQUENCE_INDEX  a_sequence_temp
///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
PROC missionCleanup()
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()

ENDPROC


PROC MISSION_FAILED()

ENDPROC

PROC create_assets()

	random_int = GET_RANDOM_INT_IN_RANGE(0,2)

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<50.0, 50.0, 50.0>>, FALSE)

		REQUEST_MODEL(A_F_M_BevHills_02) //drug buyer
		REQUEST_MODEL(G_M_Y_MexGoon_02) // busker
		
		WHILE NOT HAS_MODEL_LOADED(A_F_M_BevHills_02)
		OR NOT HAS_MODEL_LOADED(G_M_Y_MexGoon_02)
			WAIT(0)
		ENDWHILE

		REQUEST_ANIM_DICT("amb@BUSKER")
		WHILE NOT  HAS_ANIM_DICT_LOADED("amb@BUSKER") 
			WAIT(0)
		ENDWHILE
		
		
		voffset_temp.z = v_spawnpoint.z - 1.0
		busker = CREATE_PED(PEDTYPE_DEALER,G_M_Y_MexGoon_02, <<v_spawnpoint.x, v_spawnpoint.y,voffset_temp.z>>, 0.0)
		
	//	busker = CREATE_PED(PEDTYPE_DEALER,G_M_Y_MexGoon_02, v_spawnpoint, 0.0)
		

		IF NOT IS_ENTITY_DEAD(busker)
			//SET_ENTITY_HEADING(busker, voffset_temp)

			OPEN_SEQUENCE_TASK(a_sequence_temp)
				TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
			//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
			//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
			//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_03", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, "amb@BUSKER", "SAX_loop_A", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(a_sequence_temp)
			TASK_PERFORM_SEQUENCE(busker, a_sequence_temp)
			CLEAR_SEQUENCE_TASK(a_sequence_temp)
		ENDIF
		
		IF random_int = 1
			voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(busker, <<3.8,0.8,0.0>>)
			drug_buyer = CREATE_PED(PEDTYPE_CIVFEMALE,A_F_M_BevHills_02, voffset_temp, 0.0)
			IF NOT IS_ENTITY_DEAD(drug_buyer)
			//	SET_ENTITY_HEADING(drug_buyer, v_spawnpoint)
				TASK_TURN_PED_TO_FACE_COORD( drug_buyer,  v_spawnpoint ) 
				
			ENDIF
		ENDIF
		bAssetsLoaded = TRUE
	ENDIF
	
ENDPROC

PROC interact_with_busker()
	IF TIMERB() > 7000
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<10.0, 10.0, 10.0>>, FALSE)
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<3.0, 3.0, 3.0>>, FALSE)
				SETTIMERB(0)
				voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
					TASK_PLAY_ANIM(NULL, "amb@BUSKER", "SAX_loop_B", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
			//		TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_03", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				IF NOT IS_ENTITY_DEAD(busker)
					TASK_PERFORM_SEQUENCE(busker, a_sequence_temp)
				ENDIF
			//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "BUSKER PERFORMING 1", 4000, 1) //
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
		ENDIF
	
	
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<3.0, 3.0, 3.0>>, FALSE)
				voffset_temp = GET_ENTITY_COORDS(PLAYER_PED_ID() )
				SETTIMERB(0)
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
					TASK_PLAY_ANIM(NULL, "amb@BUSKER", "SAX_loop_A", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
			//		TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_03", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				//	TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				IF NOT IS_ENTITY_DEAD(busker)
					TASK_PERFORM_SEQUENCE(busker, a_sequence_temp)
				ENDIF
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "BUSKER PERFORMING 2", 4000, 1) //
		ENDIF
	ENDIF
	
ENDPROC





///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

	v_spawnpoint = in_coords.vec_coord[0]
	PRINTVECTOR(v_spawnpoint)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("Drugdealer")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	missionCleanup()
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS| FORCE_CLEANUP_FLAG_RANDOM_EVENTS)			
		
	ENDIF
	
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
				SWITCH ambStage
					CASE ambCanRun
						IF bAssetsLoaded = TRUE
							ambStage = (ambRunning)
						ELSE
							create_assets()
						ENDIF
					BREAK
					CASE ambRunning
						//DO STUFF
						IF random_int = 0
							interact_with_busker()
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				missionCleanup()
			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
///    //////////////////////////////////////////////////////////
///    							DEBUG
///    //////////////////////////////////////////////////////////

