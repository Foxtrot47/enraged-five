//////////////////////////////////////////////////////////
// 				prostitute
// Original Author:	Kevin Wong -> Evan Lawson
// Scripter: John Diaz
//

//USING "pb_prostitute_pimp.sch" // ---- comment this in and remove the next 2 if IS_PIMP_AVAILABLE	BECOMES 1
CONST_INT ALLOW_PROSTITUTE_DEBUG 0

USING "pb_prostitute_ai.sch"
USING "load_queue_public.sch"

LoadQueueLarge sLoadQueue

/////////////////////////////////////
// *** MISSION START PROCEDURE *** //
/////////////////////////////////////

PROC START_UP_CHECKS()
	
	//is_ambinet_script_interacting_with_player
	//is_this_script_interacting
	SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)
	IF DOES_ENTITY_EXIST(prostitute_ped)
		IF NOT IS_ENTITY_DEAD(prostitute_ped)
			SET_PROSTITUTE_MISSION_IN_PROGRESS(TRUE)
		ELSE
			PROST_PB_DEBUG_MESSAGE("Prostitue: not continuing, prostitue is dead")
		ENDIF
	ELSE
		PROST_PB_DEBUG_MESSAGE("Prostitue: not continuing, prostitue doesn't exist")
	ENDIF
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
	OR IS_ENTITY_DEAD(PLAYER_ped_ID())		
		SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)		
		PROST_PB_DEBUG_MESSAGE("Prostitue: not continuing, player isn't player or is dead")
	ENDIF
	
ENDPROC

FUNC BOOL INITIALISE_THE_SCRIPT()
	SWITCH ePBInitState
		CASE PROSTITUTE_INIT_REQUEST
			
			IF NOT IS_THIS_MP_PROSTITUTION()
				ADD_STREAMED_ANIM(pbStreamedAnims, GET_ANIM_DICT_FOR_PROSTITUTE_TYPE(eProstituteType))
//			ADD_STREAMED_ANIM(pbStreamedAnims, GET_ANIM_DICT_FOR_PROSTITUTE_TYPE(PRO_TYPE_CRACKHEAD))
//			ADD_STREAMED_ANIM(pbStreamedAnims, GET_ANIM_DICT_FOR_PROSTITUTE_TYPE(PRO_TYPE_COKEHEAD))
//			ADD_STREAMED_ANIM(pbStreamedAnims, GET_ANIM_DICT_FOR_PROSTITUTE_TYPE(PRO_TYPE_VANILLA))
//			ADD_STREAMED_ANIM(pbStreamedAnims, GET_ANIM_DICT_FOR_PROSTITUTE_TYPE(PRO_TYPE_FRENCH))
			
				REQUEST_ALL_ANIMS(pbStreamedAnims, sLoadQueue)			
				PROST_PB_DEBUG_MESSAGE("Moving to PROSTITUTE_INIT_STREAMING")
			ENDIF
			IF IS_THIS_MP_PROSTITUTION()
				RESERVE_NETWORK_MISSION_PEDS(1)
				RESERVE_NETWORK_MISSION_VEHICLES(1)
				NETWORK_REQUEST_CONTROL_OF_ENTITY( prostitute_ped )
			ENDIF
			ePBInitState = PROSTITUTE_INIT_STREAMING		
		BREAK
		
		CASE PROSTITUTE_INIT_STREAMING
			IF NOT IS_THIS_MP_PROSTITUTION()
				IF ARE_ANIMS_STREAMED(sLoadQueue)
					SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bAnimsAllStreamed)
					PROST_PB_DEBUG_MESSAGE("Moving to PROSTITUTE_INIT_INIT")
					ePBInitState = PROSTITUTE_INIT_INIT	
				ENDIF
			ELIF IS_THIS_MP_PROSTITUTION()
				SET_BITMASK_AS_ENUM(iProScriptBits, PRO_BIT_bAnimsAllStreamed)
				PROST_PB_DEBUG_MESSAGE("Moving to PROSTITUTE_INIT_INIT")
				ePBInitState = PROSTITUTE_INIT_INIT
			ENDIF	
		BREAK
		
		CASE PROSTITUTE_INIT_INIT
			//LOAD_ADDITIONAL_TEXT ("PUZZLE", MINIGAME_TEXT_SLOT)
			chosen_sexual_activity = SEX_TYPE_CHOSEN_BLOWJOB //SEX_TYPE_CHOSEN_NONE
			
			chosen_sexual_activity_int = ENUM_TO_INT (chosen_sexual_activity)//4
			CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_sex_anims_have_been_started)
			CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_player_has_made_a_choice)
			customer_type = CUSTOMER_NO_CUSTOMER
			customer_ped = NULL
			customer_VEHICLE = NULL
			
	#IF IS_PIMP_AVAILABLE//-----------------------------------------
			pimp_ped = NULL
	#ENDIF					
	
			PROST_PB_DEBUG_MESSAGE("Moving to PROSTITUTE_INIT_COMPLETE")
			ePBInitState = PROSTITUTE_INIT_COMPLETE		
		BREAK		
		
		CASE PROSTITUTE_INIT_COMPLETE
			
			IF NOT IS_PED_INJURED(prostitute_ped)
				
				IF NOT IS_THIS_MP_PROSTITUTION()
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(prostitute_ped, FALSE)
					SET_PED_CAN_EVASIVE_DIVE(prostitute_ped,FALSE)
					//SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(prostitute_ped)
					
					ADD_PED_FOR_DIALOGUE(cultresConversation, CONST_iDialogueProstituteSlot, prostitute_ped, "Prostitutes")
					CDEBUG1LN(DEBUG_PROSTITUTE, "PROSTITUTE_INIT_COMPLETE in SP")
					
					RETURN TRUE
				ELSE
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY( prostitute_ped )
						NETWORK_REQUEST_CONTROL_OF_ENTITY( prostitute_ped )
					
					ELSE
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(prostitute_ped, FALSE)
						ADD_PED_FOR_DIALOGUE(cultresConversation, CONST_iDialogueProstituteSlot, prostitute_ped, "Prostitutes")
						SET_PED_CAN_EVASIVE_DIVE(prostitute_ped,FALSE)
						SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(prostitute_ped)
						CDEBUG1LN(DEBUG_PROSTITUTE, "PROSTITUTE_INIT_COMPLETE in MP")
						
						RETURN TRUE
		
					ENDIF
				ENDIF
					
			ENDIF
			
			
		BREAK		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC HANDLE_CLOSEST_PROSTITUTE()
	
	//Reset Flag = PRF_DisablePedEnteredMyVehicleEvents
	IF g_bPlayerHasActiveProstitute
	
		//Prevents here from freaking out because I haven't set relationship groups
		IF IS_VEHICLE_DRIVEABLE(iPlayersVehicle)
			IF IS_PED_IN_VEHICLE(prostitute_ped,iPlayersVehicle)
				SET_DONT_FREAK_OUT_ON_EXIT_FLAG()
			ENDIF
		ENDIF	
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("PLYR HAS ACTIVE PRO", <<0.6,0.04,0.0>>)
		#ENDIF
		EXIT //if you already have a prostitute don't worry about it
	ENDIF
	
	//If player is wanted exit out
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D("PLYR IS WANTED", <<0.6,0.04,0.0>>)
		#ENDIF
		EXIT //
	ENDIF
	
	IF IS_ENTITY_DEAD( prostitute_ped )
	OR NOT DOES_ENTITY_EXIST( prostitute_ped )
		IF g_threadIDOfClosestProstitute <> GET_ID_OF_THIS_THREAD()
			AND IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bIsClosestProstitute)
			CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsClosestProstitute)
		ENDIF
		
		EXIT
	ENDIF
	
	IF (VDIST2(GET_ENTITY_COORDS(prostitute_ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < g_fDistToClosestProstitute
	OR g_fDistToClosestProstitute = 0)
	AND customer_type != CUSTOMER_OTHER_IN_VEHICLE
	
		g_fDistToClosestProstitute = VDIST2(GET_ENTITY_COORDS(prostitute_ped), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		g_threadIDOfClosestProstitute = GET_ID_OF_THIS_THREAD()
		
		SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsClosestProstitute)	
	ELSE
		IF g_threadIDOfClosestProstitute = GET_ID_OF_THIS_THREAD()
			g_fDistToClosestProstitute = VDIST2(GET_ENTITY_COORDS(prostitute_ped), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ELSE
			CLEAR_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bIsClosestProstitute)
		ENDIF
	ENDIF
	
ENDPROC

/////////////////////////////
// *** DEBUG FUNCTIONS *** //
/////////////////////////////
///    This is where we do the first debug screen printing.
PROC PROSTITUTE_DEBUG_STUFF()

	#IF IS_DEBUG_BUILD	
	#IF ALLOW_PROSTITUTE_DEBUG
		SHOW_WHAT_STATE_THE_PRO_IS_IN()
	
		//Print use context debug to screen
		IF g_bPlayerHasActiveProstitute
			TEXT_LABEL_31 sDebugContext 
			sDebugContext = "Pro Context: "
			sDebugContext += prostituteContextID
			DRAW_DEBUG_TEXT_2D(sDebugContext,<<0.8, 0.2,0.0>>)
		ENDIF
		
		IF HAS_SCRIPT_LOADED("pb_prostitute")
			TEXT_LABEL_31 sDebugScriptCount
			sDebugScriptCount = "# Pros Running: "
			sDebugScriptCount += GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("pb_prostitute"))
			DRAW_DEBUG_TEXT_2D(sDebugScriptCount,<<0.8, 0.22,0.0>>)
		
		ENDIF
		
		//Dist to Closest Prostitute
		TEXT_LABEL_31 sDebugClosest 
		sDebugClosest = "Closest in m: "
		sDebugClosest += ROUND(g_fDistToClosestProstitute)
		DRAW_DEBUG_TEXT_2D(sDebugClosest,<<0.8, 0.24,0.0>>)
		
		TEXT_LABEL_31 sDebugClosestID 
		sDebugClosestID = "ID# Closest: "
		sDebugClosestID += NATIVE_TO_INT(g_threadIDOfClosestProstitute)
		DRAW_DEBUG_TEXT_2D(sDebugClosestID,<<0.8, 0.26,0.0>>)
		
			
		//Piggy Back off this variable in the booty call scripts
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF NOT IS_BITMASK_AS_ENUM_SET(iProScriptBits, PRO_BIT_bDebugInfo)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				bDbgBlipPeds = TRUE
				debug_text = TRUE
				SET_BITMASK_AS_ENUM(iProScriptBits, PRO_BIT_bDebugInfo)
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				bDbgBlipPeds = FALSE
				debug_text = FALSE
				CLEAR_BITMASK_AS_ENUM(iProScriptBits, PRO_BIT_bDebugInfo)
				
				IF DOES_BLIP_EXIST(blipNearby)
					REMOVE_BLIP(blipNearby)
				ENDIF
				
				IF DOES_BLIP_EXIST(prostitute_blip)
					REMOVE_BLIP(prostitute_blip)
				ENDIF
				
				IF DOES_BLIP_EXIST(customer_blip)
					REMOVE_BLIP(customer_blip)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
			SET_PROSTITUTE_STATE(prostitute_WAITING_TO_END)
			CDEBUG1LN(DEBUG_PROSTITUTE, "[DEBUG] F KEY PRESSED!!!!!!!!!!!!!!!!!")
		ENDIF
		IF debug_text	
				
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
				WHILE IS_KEYBOARD_KEY_PRESSED(KEY_S)
					WAIT(0)
				ENDWHILE
				// go to mission passed
				SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)
			ENDIF
			
			IF IS_KEYBOARD_KEY_PRESSED(KEY_B)
				WHILE IS_KEYBOARD_KEY_PRESSED(KEY_B)
					WAIT(0)
				ENDWHILE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_ped_ID())
					iPlayersVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_ped_ID() )
					
					MAKE_VEHICLE_BOUNCE(iPlayersVehicle)
				ENDIF
			ENDIF
			
			
		ENDIF

	#ENDIF
	#endif
	
	/*Functions used to travel world and mark quiet locations
		
		SET_BIGMAP_ACTIVE(TRUE)
		SET_BIGMAP_FULLSCREEN(TRUE)
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
			VECTOR pos = GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)
			pos.z = 0
			blip = ADD_BLIP_FOR_RADIUS(pos, 100.0)
			SET_BLIP_COLOUR(blip, BLIP_COLOUR_RED)
		ENDIF
	
	
	//*/
	
//	IF IS_KEYBOARD_KEY_PRESSED(KEY_1)
//		WHILE IS_KEYBOARD_KEY_PRESSED(KEY_1)
//			WAIT(0)
//		ENDWHILE
//		
//		IF NOT IS_ped_INJURED(prostitute_ped)
//			IF prostitute_told_to_play_loop
//				// if doing the sex anim				
//						
//				IF GET_SCRIPT_TASK_STATUS (prostitute_ped, SCRIPT_TASK_PLAY_ANIM, current_task_status) = PERFORMING_TASK
//					IF IS_ENTITY_PLAYING_ANIM(prostitute_ped, anim_dict, sex_f_anim_loop[chosen_sexual_activity])
//						// if anim time is near a certain predetermined time (when contact)
//						FLOAT sex_anim_time
//						GET_ENTITY_ANIM_CURRENT_TIME(prostitute_ped, anim_dict, sex_f_anim_loop[chosen_sexual_activity], sex_anim_time)
//			
//						OPEN_DEBUG_FILE ()
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_STRING_TO_DEBUG_FILE("// sex_anim_time = ") 
//						SAVE_FLOAT_TO_DEBUG_FILE (sex_anim_time)
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_STRING_TO_DEBUG_FILE("IF sex_anim_time > ") 
//						SAVE_FLOAT_TO_DEBUG_FILE (sex_anim_time - 0.005)
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_STRING_TO_DEBUG_FILE("AND sex_anim_time < ") 
//						SAVE_FLOAT_TO_DEBUG_FILE (sex_anim_time + 0.005)
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_STRING_TO_DEBUG_FILE("RETURN TRUE") 
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						SAVE_STRING_TO_DEBUG_FILE("ENDIF")
//						SAVE_NEWLINE_TO_DEBUG_FILE()
//						CLOSE_DEBUG_FILE()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	

ENDPROC

PROC CLIENT_MAINTAIN_RESERVATIONS()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		CDEBUG2LN( DEBUG_NET_AMBIENT, "Prostitute: CLIENT_MAINTAIN_RESERVATIONS - Exiting: NOT NETWORK_IS_GAME_IN_PROGRESS")
		EXIT
	ENDIF
	IF NOT NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
		CDEBUG2LN( DEBUG_NET_AMBIENT, "Prostitute: CLIENT_MAINTAIN_RESERVATIONS - Exiting: NOT NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT")
		EXIT
	ENDIF
	
	IF GET_NUM_RESERVED_MISSION_PEDS() <> 1
		IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(1, FALSE, TRUE)
			IF GET_NUM_CREATED_MISSION_PEDS() <= 1
				RESERVE_NETWORK_MISSION_PEDS(1)
				CDEBUG2LN( DEBUG_NET_AMBIENT, "Prostitute: RESERVE_NETWORK_MISSION_PEDS(", 1, ")" )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

////////////////////// *** SCRIPT *** //
//////////////////// 
SCRIPT (PED_INDEX passed_ped)
//	IF IS_THIS_MP_PROSTITUTION()
//		NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()
//	ENDIF
	
	IF g_bMagDemoActive
		PRINTLN("Mag Demo is Active. Quit Prostitute Script")
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// B* 1596098 - High script memory. Requested to stop script from running during jobs
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			PRINTLN("A Multiplayer Job is currently active. Quit Prostitute Script")
			TERMINATE_THIS_THREAD()
		ENDIF
		IF IS_PLAYER_IN_VEHICLE_WITH_ANOTHER_PLAYER(PLAYER_ID(), TRUE)
			PRINTLN("[PB_PROSTITUTE] Another player is in the vehicle with us. Quit Prostitute Script")
			TERMINATE_THIS_THREAD()
		ENDIF
	ENDIF
	
	//Peyote B* - 2067198
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		TERMINATE_THIS_THREAD()
	ENDIF
	
	IF NOT IS_THIS_MP_PROSTITUTION()
	and not NETWORK_IS_GAME_IN_PROGRESS()
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP| DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_MAGDEMO)
			CPRINTLN(DEBUG_PROSTITUTE, "FORCE_CLEANUP CALLED")
			CLEANUP_PLAYER_PROSTITUTE_INTERACTION()
			TASK_PROSTITUTE_TO_LEAVE_VEHICLE(TRUE)
			CLEANUP_PROSTITUTE_PB(sLoadQueue)
		ENDIF	
	ENDIF
	
	WAIT(0)

	PRINTSTRING("\n Starting Prostitute! \n")

	CDEBUG1LN(DEBUG_PROSTITUTE,"prostitute_ped 1 = ", NATIVE_TO_INT(prostitute_ped) )

	prostitute_ped = passed_ped
	CDEBUG1LN(DEBUG_PROSTITUTE,"prostitute_ped 2 = ", NATIVE_TO_INT(prostitute_ped) )

	IF NETWORK_IS_GAME_IN_PROGRESS()
		SET_THIS_AS_NETWORK_MP_PROSTITUTION_SESSION()
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(2, FALSE, NATIVE_TO_INT(PLAYER_ID()))
				
		HANDLE_NET_SCRIPT_INITIALISATION()
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	ENDIF
	
	START_UP_CHECKS()
	
	CLEAR_NETWORK_PRO_FLAG(NPF_MP_SHOULD_TERMINATE)
		
	WHILE bBrainIsRunning
		BOOL bOwned = TRUE
		
		UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
		
		IF IS_THIS_MP_PROSTITUTION()
			IF IS_NETWORK_PRO_FLAG_SET( NPF_CHANGED_CLOTHES )
				IF NOT IS_ENTITY_DEAD( PLAYER_PED_ID() )
					IF HAS_PED_HEAD_BLEND_FINISHED( PLAYER_PED_ID() )
						AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED( PLAYER_PED_ID() )
						CLEAR_NETWORK_PRO_FLAG( NPF_CHANGED_CLOTHES )
						FINALIZE_HEAD_BLEND( PLAYER_PED_ID() )
					ENDIF		
				ENDIF
			ENDIF
			
			IF NOT IS_NETWORK_PRO_FLAG_SET( NPF_CHANGED_CLOTHES )
				AND IS_NETWORK_PRO_FLAG_SET( NPF_TERMINATE_THREAD )
				TERMINATE_THIS_THREAD()
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(prostitute_ped)
				bOwned = FALSE
			ELSE
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(prostitute_ped)
					bOwned = FALSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(prostitute_ped)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_THIS_MP_PROSTITUTION()
		OR NOT IS_NETWORK_PRO_FLAG_SET( NPF_TERMINATE_THREAD )
			CLIENT_MAINTAIN_RESERVATIONS()
			
			IF IS_THIS_MP_PROSTITUTION()
				PROCESS_PROSTITUTE_EVENTS()
				IF IS_NETWORK_PRO_FLAG_SET(NPF_MP_SHOULD_TERMINATE)
					bBrainIsRunning = FALSE
				ENDIF
			ENDIF
			
			IF SHOULD_PROSTITUTE_RUN()
				IF bOwned
				
					IF (ePBState > PROS_PB_INIT)
						MANAGE_PLAYER_ATTACKING_PROSTITUTE(sLoadQueue)
					ENDIF
					
					HANDLE_CLOSEST_PROSTITUTE()
					
					MANAGE_PROSTITUTE_HEAD_TRACKING()

					// main mission loop goes here
					SWITCH ePBState				
						// waiting to start the mission
						CASE PROS_PB_INIT
							//Assign Prostitute Type
							IF eProstituteType = PRO_TYPE_INVALID
								IF IS_PED_ACTIVE_IN_SCENARIO(prostitute_ped)
									eProstituteType = GET_PROSTITUTE_TYPE()
								ELIF IS_THIS_SPECIAL_MICHAEL_SWITCH_HOOKER()
									eProstituteType = PRO_TYPE_SPECIAL
								ENDIF
							ELSE
							
								IF INITIALISE_THE_SCRIPT()		
									IF IS_PROSTITUTE_STANDING_STILL()
										CDEBUG1LN(DEBUG_PROSTITUTE,"[SCENARIO ] STARTED " )
										IF IS_THIS_MP_PROSTITUTION()
											IF NETWORK_HAS_CONTROL_OF_ENTITY(prostitute_ped)
												TASK_START_SCENARIO_IN_PLACE(prostitute_ped,"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS" )
											ELSE
												NETWORK_REQUEST_CONTROL_OF_ENTITY(prostitute_ped)
											ENDIF
										ELSE
											TASK_START_SCENARIO_IN_PLACE(prostitute_ped,"WORLD_HUMAN_PROSTITUTE_HIGH_CLASS" )
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_PROSTITUTE,"[SCENARIO ] NOT STANDING STILL " )
									ENDIF
							
									ePBState = PROS_PB_CUSTOMER_DETERMINE_TYPE
									PROST_PB_DEBUG_MESSAGE("Prostitue: moving to PROS_PB_CUSTOMER_DETERMINE_TYPE")
								ENDIF
							ENDIF
						BREAK
									
						CASE PROS_PB_CUSTOMER_DETERMINE_TYPE
							IF IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_bProstituteHasWitnessedAShockingEvent)
								MANAGE_PROSTITUTE_BEING_ATTACKED()
							ENDIF
							
							IF IS_CUSTOMER_A_PLAYER()
								ePBState = PROS_PB_CUSTOMER_PLAYER
								PROST_PB_DEBUG_MESSAGE("Prostitue: moving to PROS_PB_CUSTOMER_PLAYER")
							ENDIF
												
							IF IS_CUSTOMER_OTHER()
								ePBState = PROS_PB_CUSTOMER_OTHER
								SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bInteractedWithAI)
								g_bIsProstituteAICustomerActive = TRUE
								PROST_PB_DEBUG_MESSAGE("Prostitue: moving to PROS_PB_CUSTOMER_OTHER")
							ENDIF
							
						#IF IS_PIMP_AVAILABLE//-------------------------------------------------------
							IF IS_CUSTOMER_A_PIMP()
								SET_BITMASK_AS_ENUM(iPimpFlags, PIMP_WAS_VISITED)
								g_bIsProstitutePimpActive = TRUE
								SET_BITMASK_AS_ENUM(iProScriptBits,PRO_BIT_bInteractedWithPimp)
								ePBState = PROS_PB_CUSTOMER_PIMP
								PROST_PB_DEBUG_MESSAGE("Prostitue: moving to PROS_PB_CUSTOMER_PIMP")
							ENDIF
							
						#ENDIF
					
						BREAK
						
						CASE PROS_PB_CUSTOMER_PLAYER
					
							PLAYER_IN_VEHICLE_SECTION(sLoadQueue)		
						BREAK
						
						CASE PROS_PB_CUSTOMER_OTHER
						
							OTHER_IN_VEHICLE_SECTION()
						BREAK
						
					#IF IS_PIMP_AVAILABLE//-------------------------------------------------------
						
						CASE PROS_PB_CUSTOMER_PIMP
							PROSTITUTE_PIMP_ARGUEMENT()
						BREAK
						
					#ENDIF
						
						DEFAULT
						BREAK
					ENDSWITCH
				ENDIF
			ELSE			
				// clean up the mission
				SET_PROSTITUTE_MISSION_IN_PROGRESS(FALSE)
			ENDIF
			
			// debug		
			PROSTITUTE_DEBUG_STUFF()
		ENDIF
		
		WAIT(0)			
	ENDWHILE
	
	IF IS_BITMASK_AS_ENUM_SET(iProScriptBits,PRO_BIT_IS_THIS_SCRIPT_INTERACTING)
		#IF IS_DEBUG_BUILD						
			REMOVE_PROSTITUTE_BLIP()										
			PRINTLN("RESETTING INTERFACE FLAG TO FALSE - PROSTITUTE SCRIPT TERMINATING")
		#endif		
	ENDIF
	
	PROST_PB_DEBUG_MESSAGE(" Something forced this exit!!!!!!~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
	CLEANUP_PLAYER_PROSTITUTE_INTERACTION()
	CLEANUP_PROSTITUTE_PB(sLoadQueue)
	
	PRINTLN("pb_prostitute has shut down")
ENDSCRIPT

