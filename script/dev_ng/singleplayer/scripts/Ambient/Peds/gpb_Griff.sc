// gpb_Baygor.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

// Baygor is the Espilonist Member
USING "GenericBrain/Generic_Brain_Header.sch"

OBJECT_INDEX sign

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_M_GRIFF_01
	txtPedMissionID = "PBGR"
	txtPedVoiceID = "GRIFF"
	txtBaseIdleDict = "Special_Ped@griff"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBGR_INTERACT"
	greet_michael = "PBGR_CONV_GM"
	greet_trevor = "PBGR_CONV_GT"
	greet_franklin = "PBGR_CONV_GF"
	idle_dic = "special_ped@griff@base"
	into_idle_dic = "special_ped@griff@intro"
	
	thisPed = GRIFF

	max_conversation_split_offsets[0] = 8
	max_conversation_split_offsets[1] = 7
	max_conversation_split_offsets[2] = 8
	max_conversation_split_offsets[3] = 10
	max_conversation_split_offsets[4] = 10
	max_conversation_split_offsets[5] = 8
	max_conversation_split_offsets[6] = 11
	max_conversation_split_offsets[7] = 11
	
ENDPROC

PROC GENERIC_BRAIN_ON_CREATE()
	SET_PED_COMPONENT_VARIATION(pedSpecial, PED_COMP_BERD, 0, 0)
	SET_PED_COMPONENT_VARIATION(pedSpecial, PED_COMP_JBIB, 0, 0)
	SET_PED_PROP_INDEX(pedSpecial, ANCHOR_EYES, 0, 0)
	SET_PED_PROP_INDEX(pedSpecial, ANCHOR_HEAD, 0, 0)
	
	//Create our sign.
	sign = CREATE_OBJECT(PROP_CS_PROTEST_SIGN_01, GET_ENTITY_COORDS(pedSpecial))
	ATTACH_ENTITY_TO_ENTITY(sign, pedSpecial, GET_PED_BONE_INDEX(pedSpecial, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
ENDPROC

PROC GENERIC_BRAIN_ON_DESTROY()
	PRINTSTRING("[SP] GRIFF GENERIC_BRAIN_ON_DESTROY") PRINTNL()
	IF DOES_ENTITY_EXIST(sign)
		DELETE_OBJECT(sign)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	PRINTSTRING("[SP] GRIFF GENERIC_BRAIN_ON_RUNAWAY") PRINTNL()
	IF DOES_ENTITY_EXIST(sign)
		DETACH_ENTITY(sign)
		SET_OBJECT_AS_NO_LONGER_NEEDED(sign)
	ENDIF
ENDPROC

FUNC BOOL PED_FLEE_WHEN_BUMPED()
	return TRUE
ENDFUNC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@griff"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "IAmNotARacist"
		BREAK
		CASE 2
			txtAnimToPlay = "America_is_not"
		BREAK
		CASE 3
			txtAnimToPlay = "America_is_for_the_americans"
		BREAK
		CASE 4
			txtAnimToPlay = "this_is_the_land"
		BREAK
		CASE 5
			txtAnimToPlay = "this_is_the_land_opportunity"
		BREAK
		CASE 6
			txtAnimToPlay = "keep_america_clean"
		BREAK
		CASE 7
			txtAnimToPlay = "e_pluribus_unum"
		BREAK
		CASE 8
			txtAnimToPlay = "let_me_be_clear"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "convo_trevor_whatareyoudoing"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "convo_michael_heyheymanthankgod"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "convo_franklin_heywhatsuphomie"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 7
	ELIF (ePlayer = CHAR_MICHAEL)
		return 5
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 9
ENDFUNC


USING "GenericBrain/Generic_Brain.sch"
