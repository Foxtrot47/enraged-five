// gpb_PamelaDrake.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

// Pamela Drake is the Female older movie star.
USING "GenericBrain/Generic_Brain_Header.sch"

PED_INDEX insult_ped
BOOL set_off = FALSE
BOOL done = FALSE
BOOL saidStuff = FALSE
PROC GENERIC_BRAIN_CUSTOM_INIT()

	
	ePedModel = U_F_O_MOVIESTAR
	txtPedMissionID = "PBPD"
	txtPedVoiceID = "PAMELADRAKE"
	txtBaseIdleDict = "Special_Ped@pamela"
	txtBaseIdleAnim = "Base"
	talkWithAnims = TRUE
	max_conversation_offset = 14
	sInteractLabel = "PBPD_INTERACT"
	greet_michael = "PBPD_CONV_GM"
	greet_trevor = "PBPD_CONV_GT"
	greet_franklin = "PBPD_CONV_GF"
	idle_dic = "special_ped@pamela@base"
	into_idle_dic = "special_ped@pamela@intro"
	
	thisPed = PAMELA
	
	max_conversation_split_offsets[0] = 8
	max_conversation_split_offsets[1] = 8
	max_conversation_split_offsets[2] = 8
	max_conversation_split_offsets[3] = 12
	max_conversation_split_offsets[4] = 9
	max_conversation_split_offsets[5] = 8
	max_conversation_split_offsets[6] = 8
	max_conversation_split_offsets[7] = 9
	max_conversation_split_offsets[8] = 9
	max_conversation_split_offsets[9] = 8
	max_conversation_split_offsets[10] = 5
	max_conversation_split_offsets[11] = 7
	max_conversation_split_offsets[12] = 7
	max_conversation_split_offsets[13] = 6
	
ENDPROC

PROC GENERIC_BRAIN_ON_CREATE()
	SET_PED_PROP_INDEX(pedSpecial, ANCHOR_EYES, 0, 0)
	IF NOT g_bMagDemoActive OR done 
		EXIT
	ENDIF
	REQUEST_MODEL(A_M_M_BUSINESS_01)
	WHILE NOT HAS_MODEL_LOADED(A_M_M_BUSINESS_01)	//TEMP!!
			WAIT(0)
	ENDWHILE
	
	insult_ped = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01, <<396.8498, 143.3195, 101.4659>>, 360.0)
	IF DOES_ENTITY_EXIST(insult_ped)
	AND NOT IS_ENTITY_DEAD(insult_ped)
	AND NOT IS_ENTITY_DEAD(pedSpecial)
		SET_IK_TARGET(insult_ped, IK_PART_HEAD, pedSpecial, 0, <<0,0,0>>, ITF_DEFAULT)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		PRINTSTRING("[SP] Adding the ped Pamela to the audio mix group.") PRINTNL()
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedSpecial, "MAG_2_PAM_PED_GROUP")
	ENDIF
	
ENDPROC

PROC GENERIC_BRAIN_PROCESS()
	IF NOT g_bMagDemoActive OR done 
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(insult_ped)
	AND NOT IS_ENTITY_DEAD(insult_ped)
	AND NOT IS_ENTITY_DEAD(pedSpecial)
		
		
		IF NOT set_off
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedSpecial), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 15.0
	//			TASK_GO_STRAIGHT_TO_COORD(insult_ped, <<377.4565, 154.6708, 102.0838>>, 1.2, DEFAULT_TIME_NEVER_WARP)
				TASK_FLUSH_ROUTE()				
				TASK_EXTEND_ROUTE(<<379.8408, 152.3401, 102.0718>>)
				TASK_EXTEND_ROUTE(<<380.4872, 158.6530, 102.1292>>)
				TASK_EXTEND_ROUTE(<<388.7568, 179.6577, 102.1317>>)
				
				TASK_FOLLOW_POINT_ROUTE(insult_ped, PEDMOVE_WALK, TICKET_SINGLE)
				
				set_off = TRUE
			ENDIF
		ELSE
			SET_IK_TARGET(insult_ped, IK_PART_HEAD, pedSpecial, 0, <<0,0,0>>, ITF_DEFAULT)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(insult_ped), <<377.4565, 154.6708, 102.0838>>) > 25.0
				SET_PED_AS_NO_LONGER_NEEDED(insult_ped)
				done = TRUE
			ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(insult_ped), GET_ENTITY_COORDS(pedSpecial)) < 3.0 AND saidStuff = FALSE
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(insult_ped, "PBPD_ATAA", "BUSINESSMAN", "SPEECH_PARAMS_FORCE_NORMAL")
				saidStuff = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		PRINTSTRING("[SP] Removing the ped Pamela from the audio mix group.") PRINTNL()
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(pedSpecial)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_DESTROY()
	IF NOT g_bMagDemoActive
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(insult_ped)	
		SET_PED_AS_NO_LONGER_NEEDED(insult_ped)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	
	txtAnimRoot = "Special_Ped@pamela"
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "pamela_ig_1_p1_letmetellyoumydarlings"
		BREAK
		CASE 2
			txtAnimToPlay = "pamela_ig_1_p2_thingsweredifferent"
		BREAK
		CASE 3
			txtAnimToPlay = "pamela_ig_1_p3_perhapsyouhaveseentheplay"
		BREAK
		CASE 4
			txtAnimToPlay = "pamela_ig_1_p4_whathappenstothelonelyones"
		BREAK
		CASE 5
			txtAnimToPlay = "IwasThatslut"
		BREAK
		CASE 6
			txtAnimToPlay = "pamela_ig_1_p6_intheolddays"
		BREAK
		CASE 7
			txtAnimToPlay = "pamela_ig_1_p7_ImJustAWiltedFlower"
		BREAK
		CASE 8
			txtAnimToPlay = "pamela_ig_1_p8_WonderfulTimes"
		BREAK
		CASE 9
			txtAnimToPlay = "vinewood_is_cruel"
		BREAK
		CASE 10
			txtAnimToPlay = "you_dont_like_movies_you_dont_like_stars"
		BREAK
		CASE 11
			txtAnimToPlay = "this_town_is_run_by_god_damn_accountants"
		BREAK
		CASE 12
			txtAnimToPlay = "pamela_ig_1_p12_NoWonderThisPlaceIs"
		BREAK
		CASE 13
			txtAnimToPlay = "pamela_ig_1_p13_TheWorldIsFor"
		BREAK
		CASE 14
			txtAnimToPlay = "pamela_ig_1_p14_YourHereToSeeStars"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "pamela_convo_trevor_im_trying_to_get_noticed"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "pamela_ig_2_imterribledarling"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "pamela_ig_5_inthebackdoor"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 3
	ELIF (ePlayer = CHAR_MICHAEL)
		return 22
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 4
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
