PROC GET_TAG_OFFSET(INT offset, TEXT_LABEL_15& txt)
	txt = "SKIP_"
	txt += offset //ITOS(offset)
ENDPROC

FUNC FLOAT GET_NEXT_SKIP_POINT(ENTITY_INDEX ped, STRING anim_dic, STRING anim)
	FLOAT current_time = GET_ENTITY_ANIM_CURRENT_TIME(ped, anim_dic, anim) 
	INT offset = 0
	FLOAT start_time
	FLOAT end_time
	TEXT_LABEL_15 tag_string
	GET_TAG_OFFSET(offset+1, tag_string)
	PRINTLN("[SP] Looking for ")
	PRINTSTRING(tag_string)
	PRINTNL()
	WHILE(FIND_ANIM_EVENT_PHASE(anim_dic, anim, tag_string, start_time, end_time))
		PRINTLN("[SP] Skipping found ")
		PRINTSTRING(tag_string)
		PRINTNL()
		PRINTLN("[SP] start and end ")
		PRINTFLOAT(start_time)
		PRINTNL()
		PRINTFLOAT(end_time)
		PRINTNL()
		IF (start_time > current_time)
			PRINTLN("[SP] Found next point.")
			return start_time
		ENDIF
		offset++
		GET_TAG_OFFSET(offset+1, tag_string)
	ENDWHILE
	return 1.0 //End of the anim.
ENDFUNC
