
PROC INT_TO_ALPHAS(INT i, TEXT_LABEL_15 &tl15_alpha)
	SWITCH (i)
		CASE 0
			tl15_alpha = "A"
		BREAK
		CASE 1
			tl15_alpha = "B"
		BREAK
		CASE 2
			tl15_alpha = "C"
		BREAK
		CASE 3
			tl15_alpha = "D"
		BREAK
		CASE 4
			tl15_alpha = "E"
		BREAK
		CASE 5
			tl15_alpha = "F"
		BREAK
		CASE 6
			tl15_alpha = "G"
		BREAK
		CASE 7
			tl15_alpha = "H"
		BREAK
		CASE 8
			tl15_alpha = "I"
		BREAK
		CASE 9
			tl15_alpha = "J"
		BREAK
		CASE 10
			tl15_alpha = "K"
		BREAK
		CASE 11
			tl15_alpha = "L"
		BREAK
		CASE 12
			tl15_alpha = "M"
		BREAK
		CASE 13
			tl15_alpha = "N"
		BREAK
		CASE 14
			tl15_alpha = "O"
		BREAK
		CASE 15
			tl15_alpha = "P"
		BREAK
		CASE 16
			tl15_alpha = "Q"
		BREAK
		CASE 17
			tl15_alpha = "R"
		BREAK
		CASE 18
			tl15_alpha = "S"
		BREAK
		CASE 19
			tl15_alpha = "T"
		BREAK
		CASE 20
			tl15_alpha = "U"
		BREAK
		CASE 21
			tl15_alpha = "V"
		BREAK
		CASE 22
			tl15_alpha = "W"
		BREAK
		CASE 23
			tl15_alpha = "X"
		BREAK
		CASE 24
			tl15_alpha = "Y"
		BREAK
		CASE 25
			tl15_alpha = "Z"
		BREAK
		
	ENDSWITCH
ENDPROC
