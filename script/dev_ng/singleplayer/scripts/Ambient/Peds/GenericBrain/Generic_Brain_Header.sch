// Generic_Brain_Header.sch
USING "globals.sch"
USING "cellphone_public.sch"
USING "director_mode_public.sch"

USING "dialogue_public.sch"
USING "context_control_public.sch"

CONST_INT		PED_BRAIN_AIM_THRESHOLD				1		// How long the player needs to at aim/target a special ped before they flee
CONST_INT 		PED_BRAIN_NORMAL_BUMP_THRESHOLD		4
CONST_INT		PED_BRAIN_STD_DELAY					0
CONST_INT		PED_BRAIN_SPEECH_DELAY 				10000	// How long should a ped wait before random speech. (in ms)
CONST_INT		PED_BRAIN_SHORT_SPEECH_DELAY		6000	// How long should a short speech delay be. (in ms)
CONST_FLOAT		PED_BRAIN_INTERACT_RANGE			2.5		// How close does the player need to be before interacting with a ped.
CONST_FLOAT		PED_BRAIN_GOODBYE_RANGE				5.0		// How close does the player need to get before the ped will say goodbye when he leaves.
CONST_FLOAT		PED_BRAIN_MAX_TALK_RANGE			12.0	// How close the player needs to be to hear this guy talking.


/// PURPOSE:
///    Create this and set it to 1 in your ped brain script if your ped will be speaking random dialogue on his own.
/// NEED:
///    PROC GENERIC_BRAIN_CUSTOM_INIT_RANDOM()
///    GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(iConvoNum, sAnimRoot, sAnimToPlay)
#IF NOT DEFINED(PED_BRAIN_PED_HAS_RANDOM_DIALOGUE)
	CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 0
#ENDIF
#IF PED_BRAIN_PED_HAS_RANDOM_DIALOGUE
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Create this and set it to 1 in your ped brain script if the player will be able to press a button to interact with the ped.
#IF NOT DEFINED(PED_BRAIN_PED_HAS_INTERACTION)
	CONST_INT PED_BRAIN_PED_HAS_INTERACTION 0
#ENDIF
#IF PED_BRAIN_PED_HAS_INTERACTION
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/// PURPOSE:
///    Create this and set it to 1 in your ped brain script if you want the ped to react to repeated bumps
#IF NOT DEFINED(PED_BRAIN_PED_HAS_ABUSE_DIALOGUE)
	CONST_INT PED_BRAIN_PED_HAS_ABUSE_DIALOGUE 0
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Create this and set it to 1 in your ped brain script if the ped wills ay goodbye to the player when he leaves.
#IF NOT DEFINED(PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE)
	CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE 0
#ENDIF
#IF PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE
	
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Create this and set it to 1 if your ped has greet dialogue.
#IF NOT DEFINED(PED_BRAIN_PED_HAS_GREET_DIALOGUE)
	CONST_INT PED_BRAIN_PED_HAS_GREET_DIALOGUE 0
#ENDIF
#IF PED_BRAIN_PED_HAS_GREET_DIALOGUE
	BOOL bPlayedGreet = FALSE
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/// PURPOSE:
///    Create this and set it to 0 if you don't want your ped to look at people.
#IF NOT DEFINED(PED_BRAIN_PED_LOOKS_AT_PEOPLE)
	CONST_INT PED_BRAIN_PED_LOOKS_AT_PEOPLE 1
#ENDIF
#IF PED_BRAIN_PED_LOOKS_AT_PEOPLE
	
	
#ENDIF
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MODEL_NAMES					ePedModel					// The model of the ped that we're spawning.
structPedsForConversation 	convoPed					// Ped conversation struct.

TEXT_LABEL_63				txtBaseIdleDict	= ""		// The ped's base idle. Will not override if not set.
TEXT_LABEL_63				txtBaseIdleAnim = ""

TEXT_LABEL_7				txtPedMissionID				// The Mission ID for this ped in D*. If your ped doesn't have dialogue, ignore.
TEXT_LABEL_15				txtPedVoiceID				// The voice ID for this ped in D*. If your ped doesn't have dialogue, ignore.


PED_INDEX					pedSpecial
BOOL						bPedCreated
VECTOR 						vInitialPos
INT							iBlockObject = -1


INT 						talking_offset = 0
INT 						starting_offset = 1

BLIP_INDEX					pedBlip

BOOL isIdleAnimsrequested = FALSE

//TODO - a lot of these states can be taken out now. 
ENUM PEDS_STATE
	SET_IDLING,
	WAIT_FOR_ANIM_TO_BE_LOADED,
	WAITING_TO_KILL_MONOLOGUE_LOADING_ANIM,
	START_IDLING,
	WAITING_TO_KILL_CONVERSATION_LOADING_ANIM,
	START_MOVE_BACK_TO_INITIAL_POSITION,
	WAIT_MOVE_BACK_TO_INITIAL_POSITION,
	PRE_PLAY_MONOLOGUE,
	PLAY_MONOLOGUE,
	MOVE_TO_NEXT_MONOLOGUE,
	START_LOADING_CONVERSATION,
	CHECK_FOR_LOADED_CONVERSATION,
	START_CONVERSATION,
	NEWSTATE,
	WAIT_TO_START_CONVERSATION,
	WAIT_FOR_CONVERSATION_SECTION_OVER,
	WAIT_FOR_CONVERSATION_TO_END,
	TALKING_MONOLOGUE,
	WAITING_TO_KILL_CONVERSATION,
	WAITING_FOR_IDLE_BEFORE_CONVERSATION,
	WAITING_FOR_MONOLOGUE_BEFORE_CONVERSATION,
	WAITING_TO_KILL_MONOLOGUE,
	WAITING_TO_KILL_CONVERSATION_FROM_MONOLOGUE,
	PLAYING_CONVERSATION,
	WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING,
	WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING
ENDENUM

PEDS_STATE ped_state

specialPedList thisPed

INT max_conversation_offset
INT max_random_offset
CONST_INT max_conversation_split_offset 20 //Guess, the maximum split anims.
CONST_INT max_random_split_offset 20 //Guess, the maximum split anims.
INT random_reply
INT max_conversation_split_offsets[max_conversation_split_offset]
INT max_random_split_offsets[max_random_split_offset]
INT conversation_split_offset
INT talk_context

INT conversation_number[3]

STRING	sInteractLabel	
TEXT_LABEL_15 txt_block
TEXT_LABEL_15 txt_root

TEXT_LABEL_15 greet_michael
TEXT_LABEL_15 greet_trevor
TEXT_LABEL_15 greet_franklin
TEXT_LABEL_63 idle_dic
TEXT_LABEL_63 into_idle_dic
TEXT_LABEL_63 current_monologue_line
TEXT_LABEL_63 current_monologue_dic
TEXT_LABEL_63 requested_dic
//FLOAT next_skip_point
TEXT_LABEL_63 talking_anim
TEXT_LABEL_63 current_anim

BOOL talkWithAnims = FALSE
BOOL noConversation = FALSE
BOOL talkWithRandom = FALSE
BOOL canInteract = TRUE
BOOL monologueStarted = FALSE
BOOL immortal = FALSE
//TODO TAKE OUT NOT USED
////////////////////////////////////////////////////////
INT iNumRandomConvos 
BOOL bPlayAsConvo 
INT iRandomLimit 
INT iDelayAmt
INT iNumPedInteractResponses
////////////////////////////////////////////////////////

BOOL createBlip = TRUE

FUNC INT GET_CONVERSATION_SECTION_COUNT()
	return max_random_split_offsets[random_reply]
ENDFUNC 
