// generic_brain_support.sch
USING "globals.sch"
USING "director_mode_public.sch"

FUNC DirectorUnlockSpecial GET_DM_SPECIAL_UNLOCK_FROM_SPECIAL_PEDS( SPECIAL_PEDS eSpecialPed )
	INT iIndex 
	REPEAT MAX_DU_SPECIAL iIndex
		DirectorUnlockSpecial eUnlockPed = INT_TO_ENUM( DirectorUnlockSpecial, iIndex )
		IF GET_SPECIAL_PEDS_FROM_DIRECTOR_UNLOCK_SPECIAL( eUnlockPed, TRUE ) = eSpecialPed
			RETURN eUnlockPed
		ENDIF
	ENDREPEAT
	RETURN DU_SPECIAL_INVALID
ENDFUNC

/// PURPOSE:
///    Sets a special ped as dead. It will never respawn.
PROC SET_SPECIALPED_AS_DEAD(SPECIAL_PEDS eDeadPed)
	SET_BITMASK_ENUM_AS_ENUM(g_savedGlobals.sSpecialPedData.ePedsKilled, eDeadPed)
	
	DirectorUnlockSpecial eSpecialChar = GET_DM_SPECIAL_UNLOCK_FROM_SPECIAL_PEDS( eDeadPed )
	IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( eSpecialChar )
		SET_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED( GET_DM_SPECIAL_UNLOCK_FROM_SPECIAL_PEDS( eDeadPed ) )
	ENDIF
ENDPROC


/// PURPOSE:
///    Checks to see if a special ped is dead.
FUNC BOOL IS_SPECIALPED_DEAD(SPECIAL_PEDS ePed)
	RETURN IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sSpecialPedData.ePedsKilled, ePed)
ENDFUNC

/// PURPOSE:
///    Given a ped brain script name, returns the SPECIAL_PEDS enum.
///    Not the cheapest function to call...
FUNC SPECIAL_PEDS GET_SPECIAL_PED_FROM_SCRIPT_NAME(STRING sScriptName)
	// Make sure we're given a valid script....
	IF (IS_STRING_NULL_OR_EMPTY(sScriptName))
		RETURN SP_NONE
	ENDIF
	
	IF ARE_STRINGS_EQUAL(sScriptName, "gpb_AndyMoon")
		RETURN SP_ANDYMOON
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Baygor")
		RETURN SP_BAYGOR
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_BillBinder")
		RETURN SP_BILLBINDER
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Clinton")
		RETURN SP_CLINTON
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Griff")
		RETURN SP_GRIFF
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Jane")
		RETURN SP_JANE
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Jerome")
		RETURN SP_JEROME
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Jesse")
		RETURN SP_JESSE
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Mani")
		RETURN SP_MANI
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Mime")
		RETURN SP_MIME
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_PamelaDrake")
		RETURN SP_PAMELADRAKE
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Superhero")
		RETURN SP_SUPERHERO
	ELIF ARE_STRINGS_EQUAL(sScriptName, "gpb_Zombie")
		RETURN SP_ZOMBIE
	ENDIF
	
	RETURN SP_NONE
ENDFUNC
