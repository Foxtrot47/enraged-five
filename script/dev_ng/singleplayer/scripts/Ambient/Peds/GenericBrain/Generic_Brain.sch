USING "commands_player.sch"
USING "commands_script.sch"
USING "dialogue_public.sch"
USING "brains.sch"
USING "ambience_run_checks.sch"
USING "globals.sch"
USING "context_control_public.sch"

USING "Generic_Brain_Skip.sch"

USING "generic_brain_support.sch"
USING "RC_Threat_public.sch"
USING "generic_brain_int_to_alpha.sch"

USING "Flow_Mission_Data_Public.sch"

#IF NOT DEFINED(GENERIC_BRAIN_CUSTOM_INIT)
PROC GENERIC_BRAIN_CUSTOM_INIT()

ENDPROC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_CUSTOM_REQUEST_ASSETS)
PROC GENERIC_BRAIN_CUSTOM_REQUEST_ASSETS()
	
ENDPROC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_ARE_CUSTOMS_ASSETS_LOADED)
FUNC BOOL GENERIC_BRAIN_ARE_CUSTOMS_ASSETS_LOADED()
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(IS_SCRIPT_ALLOWED_TO_RUN)
FUNC BOOL IS_SCRIPT_ALLOWED_TO_RUN()
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_ON_CREATE)
PROC GENERIC_BRAIN_ON_CREATE()
	
ENDPROC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_PROCESS)
PROC GENERIC_BRAIN_PROCESS()
	
ENDPROC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_ON_DESTROY)
PROC GENERIC_BRAIN_ON_DESTROY()
	
ENDPROC
#ENDIF

#IF NOT DEFINED(GENERIC_BRAIN_ON_RUNAWAY)
PROC GENERIC_BRAIN_ON_RUNAWAY()
	
ENDPROC
#ENDIF

#IF NOT DEFINED(GET_NUMBER_OF_CONVERSATION_SECTIONS)
FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	return 0
ENDFUNC
#ENDIF

#IF NOT DEFINED(GENERIC_GET_INTERACT_ANIM)
PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	txt_label=""
ENDPROC
#ENDIF

#IF NOT DEFINED (PED_FLEE_WHEN_BUMPED)
FUNC BOOL PED_FLEE_WHEN_BUMPED()
	return FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED (CAN_TALK_TO_PED)
FUNC BOOL CAN_TALK_TO_PED()
	return TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED (CONVERSATION_OVER)
PROC CONVERSATION_OVER()
ENDPROC
#ENDIF

#IF NOT DEFINED (GENERIC_BRAIN_ON_PLAY_MONOLOGUE)
PROC GENERIC_BRAIN_ON_PLAY_MONOLOGUE(INT i)
	UNUSED_PARAMeTER(i)
ENDPROC
#ENDIF

#IF NOT DEFINED (GENERIC_BRAIN_ON_PLAYING_MONOLOGUE)
PROC GENERIC_BRAIN_ON_PLAYING_MONOLOGUE(INT i, INT split_i, FLOAT time)
	UNUSED_PARAMeTER(i)
	UNUSED_PARAMeTER(split_i)
	UNUSED_PARAMeTER(time)
ENDPROC
#ENDIF

#IF NOT DEFINED (GENERIC_BRAIN_ON_END_MONOLOGUE)
PROC GENERIC_BRAIN_ON_END_MONOLOGUE(INT i)
	UNUSED_PARAMETER(i)
ENDPROC
#ENDIF

#IF NOT DEFINED (CAN_PED_LOOK_AT_PLAYER)
FUNC BOOL CAN_PED_LOOK_AT_PLAYER()
	return TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED (PED_BUMPED)
PROC PED_BUMPED()
	
ENDPROC
#ENDIF

FUNC BOOL IS_ANIM_PLAYING_ON_PED()
	IF ((GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK)
	AND (GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_PLAY_ANIM) <> WAITING_TO_START_TASK))
		return FALSE
	ENDIF
	return TRUE
ENDFUNC

PROC GET_CONVERSATION_NAME(TEXT_LABEL_15 &name)
	//See which character the player is and return the correct name. 
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		name = "TREVOR"
	ELIF (ePlayer = CHAR_MICHAEL)
		name = "MICHAEL"
	ELSE 
		name = "FRANKLIN"
	ENDIF
ENDPROC

PROC GET_GREET_LINE(TEXT_LABEL_15 &line)
	//See which character the player is and return the correct name. 
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		line = greet_trevor
	ELIF (ePlayer = CHAR_MICHAEL)
		line = greet_michael
	ELSE 
		line = greet_franklin
	ENDIF
ENDPROC

PROC GET_CONVERSATION_LINE(TEXT_LABEL_15 &line)
	line = txtPedMissionID
	line += "_CONV_"
	
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		line += "T"
	ELIF (ePlayer = CHAR_MICHAEL)
		line += "M"
	ELSE 
		line += "F"
	ENDIF
	line += conversation_number[GET_CURRENT_PLAYER_PED_ENUM()] //Generally, this will be one. This is so we can have different conversations depending on context. This refers to the number in the convo , i.e. CONV_M1
ENDPROC 

PROC GET_RANDOM_CONV_LINE(TEXT_LABEL_15 &line)
	line = txtPedMissionID
	line += "_CONV_"
	line += random_reply+1 //Convos in dialog star start at 1
ENDPROC

PROC GET_MONOLOGUE_DIC_LINE(TEXT_LABEL_15 &line)
	line = "MONOLOGUE_"
	line += (conversation_offset[thisPed]+1)
ENDPROC 

PROC GET_MONOLOGUE_DIC_SPLIT_LINE(TEXT_LABEL_15 &line)
	INT_TO_ALPHAS(conversation_split_offset, line) 
ENDPROC

PROC GET_NAME_DIC_LINE(TEXT_LABEL_15 &line)
	TEXT_LABEL_15 name
	GET_CONVERSATION_NAME(name)
	line = name
	line += "_"
	line += conversation_number[GET_CURRENT_PLAYER_PED_ENUM()] //Generally, this will be one. This is so we can have different conversations depending on context. This refers to the number in the anim dic, i.e. MICHAEL_1@
ENDPROC 

PROC GET_DIC_NAME(TEXT_LABEL_63& dic_name)
	TEXT_LABEL_15 mono_line
	
	dic_name = txtBaseIdleDict
	dic_name += "@"
	GET_MONOLOGUE_DIC_LINE(mono_line)
	dic_name += mono_line
ENDPROC

PROC GET_DIC_SPLIT_NAME(TEXT_LABEL_63& dic_name)
	TEXT_LABEL_15 mono_line
	
	dic_name = txtBaseIdleDict
	dic_name += "@"
	GET_MONOLOGUE_DIC_LINE(mono_line)
	dic_name += mono_line
	dic_name += "@"
	dic_name += mono_line
	GET_MONOLOGUE_DIC_SPLIT_LINE(mono_line)
	dic_name += mono_line
ENDPROC

PROC GET_TALKING_DIC_NAME(TEXT_LABEL_63& dic_name)
	TEXT_LABEL_15 mono_line
	
	dic_name = txtBaseIdleDict
	dic_name += "@"
	GET_NAME_DIC_LINE(mono_line)
	dic_name += mono_line
	dic_name += "@"
	dic_name += mono_line
ENDPROC

PROC GET_INTERACT_ANIM_WITH_OFFSET(TEXT_LABEL_63& anim_name)
	TEXT_LABEL_63 interact_anim
	GENERIC_GET_INTERACT_ANIM(interact_anim)
	anim_name = interact_anim
	anim_name += "_"
	anim_name += talking_offset
ENDPROC

PROC GET_RANDOM_TALKING_NAME(TEXT_LABEL_63& dic_name)
	INT new_random_reply = random_reply+1 //Offsets start at 1 in the animation sets. So we need to add 1. 
	dic_name = txtBaseIdleDict
	dic_name += "@"
	dic_name += "CONVO_"
	dic_name += new_random_reply
	dic_name += "@"
	dic_name += "CONVO_"
	dic_name += new_random_reply
ENDPROC

PROC REQUEST_DIC()
	TEXT_LABEL_63 dic
	GET_DIC_NAME(dic)
	CPRINTLN(DEBUG_AMBIENT, "[SP] REQUEST_DIC Requesting anim:")
	PRINTSTRING(dic)
	PRINTNL()
	REQUEST_ANIM_DICT(dic)
	requested_dic = dic
ENDPROC

PROC REQUEST_SPLIT_DIC()
	TEXT_LABEL_63 dic
	GET_DIC_SPLIT_NAME(dic)
	CPRINTLN(DEBUG_AMBIENT, "[SP] REQUEST_SPLIT_DIC Requesting anim dic:")
	PRINTSTRING(dic)
	PRINTNL()
	REQUEST_ANIM_DICT(dic)
	requested_dic = dic
ENDPROC

/*PROC REQUEST_TALKING_DIC()
	TEXT_LABEL_63 dic
	GET_TALKING_DIC_NAME(dic)
	CPRINTLN(DEBUG_AMBIENT, "[SP] Requesting anim:")
	PRINTSTRING(dic)
	PRINTNL()
	REQUEST_ANIM_DICT(dic)
	requested_dic = dic
ENDPROC*/

PROC REMOVE_DIC()
	PRINTSTRING("[SP] REMOVE_DIC removing requested_dic")
	PRINTNL()
	PRINTSTRING(requested_dic)
	PRINTNL()
					
	REMOVE_ANIM_DICT(requested_dic)
	PRINTSTRING("[SP] /REMOVE_DIC removing requested_dic")
	PRINTNL()
	
ENDPROC

FUNC BOOL IS_DIC_LOADED()
	//CPRINTLN(DEBUG_AMBIENT, "[SP] Have we loaded: ", requested_dic)
	BOOL rt = HAS_ANIM_DICT_LOADED(requested_dic)
	
	/*iF (rt = TRUE)
		PRINTLN("[SP] LOADED anim dict:")
		PRINTNL()
	ELSE
		PRINTLN("[SP] NOT LOADED anim dict:")
		PRINTNL()
	ENDIF*/
	
	//PRINTLN(requested_dic)
	//PRINTNL()
	
	RETURN rt
ENDFUNC

PROC SET_PED_IDLING(BOOL blend = FALSE, BOOL force = TRUE)
	CPRINTLN(DEBUG_AMBIENT, "[SP] SET_PED_IDLING")
	
	IF blend = TRUE
		CPRINTLN(DEBUG_AMBIENT, "[SP] Blend")
		TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
	ELSE
		TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, INSTANT_BLEND_IN, INSTANT_BLEND_IN, -1, AF_DEFAULT)
	ENDIF
	current_monologue_line = txtBaseIdleAnim //We are now playing the base
	PRINTSTRING("[SP]FORCE_PED_AI_AND_ANIMATION_UPDATE 2") PRINTNL()
	IF force
		IF NOT IS_PED_INJURED(pedSpecial)
		AND NOT IS_PED_DEAD_OR_DYING(pedSpecial)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
		ENDIF
	ENDIF
ENDPROC
TEXT_LABEL_15 saved_root
FUNC BOOL PRELOAD_MONOLOGUE()
	TEXT_LABEL_15 convo_txt_root = txt_root
	
	convo_txt_root += (conversation_offset[thisPed]+1)
	
	
	
	//Preload the next line...
	ADD_PED_FOR_DIALOGUE(convoPed, 3, pedSpecial, txtPedVoiceID)	
	
	BOOL created 
	IF noConversation
		created = TRUE
	ELSE
		created =  CREATE_CONVERSATION(convoPed, txt_block, convo_txt_root, CONV_PRIORITY_AMBIENT_MEDIUM, DISPLAY_SUBTITLES)
	ENDIF
	
	//Last conversation line is....
	saved_root = convo_txt_root
	PRINTSTRING("[SP] saved root is ")
	PRINTSTRING(saved_root)
	PRINTNL()
	
	RETURN created
ENDFUNC

PROC SET_PED_MONALOGUE()
	TEXT_LABEL_31 anim_root
	TEXT_LABEL_63 anim_to_play
	
	GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(conversation_offset[thisPed]+1, anim_root, anim_to_play)
	anim_to_play += "_"
	anim_to_play += conversation_split_offset
	
	//Save our anim to play as we may need it later.
	current_monologue_line = anim_to_play
	current_monologue_dic = requested_dic
	
	DUMMY_REFERENCE_STRING(current_monologue_line)
	CPRINTLN(DEBUG_AMBIENT, "[SP] TASK_PLAY_ANIM: ", current_monologue_line)
	TASK_PLAY_ANIM(pedSpecial, requested_dic, anim_to_play, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
	PRINTLN("[SP] /TASK_PLAY_ANIM")
	PRINTSTRING("[SP]FORCE_PED_AI_AND_ANIMATION_UPDATE 3") PRINTNL()
	FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
	
	GENERIC_BRAIN_ON_PLAY_MONOLOGUE(conversation_offset[thisPed])
ENDPROC

PROC SET_PED_BLEND_INTO_IDLE()
	TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
	PRINTSTRING("[Sp] SET_PED_BLEND_INTO_IDLE ") PRINTSTRING(txtPedVoiceID) PRINTNL()
	IF NOT g_bMagDemoActive OR GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedSpecial) < 25.0
		PRINTSTRING("[SP]FORCE_PED_AI_AND_ANIMATION_UPDATE 4") PRINTNL()
		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
	ELSE
		PRINTSTRING("[SP] NOT FORCE_PED_AI_AND_ANIMATION_UPDATE") PRINTNL()
	ENDIF
ENDPROC

PROC SET_PED_FLEE()
	GENERIC_BRAIN_ON_RUNAWAY()
	IF IS_ENTITY_ALIVE(pedSpecial)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSpecial, FALSE)
		TASK_SMART_FLEE_PED(pedSpecial, PLAYER_PED_ID(), 100.0, -1)
		SET_PED_KEEP_TASK(pedSpecial, TRUE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	ENDIF
ENDPROC

PROC SET_PED_WANDER()
	GENERIC_BRAIN_ON_RUNAWAY()
	IF IS_ENTITY_ALIVE(pedSpecial)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSpecial, FALSE)
		TASK_WANDER_STANDARD(pedSpecial)
		SET_PED_KEEP_TASK(pedSpecial, TRUE)
	ENDIF
ENDPROC

PROC GENERATE_RANDOM_CONVERSATION()
	random_reply = GET_RANDOM_INT_IN_RANGE(0, max_random_offset)
	PRINTSTRING("[SP] Random converstaion ")
	PRINTINT(random_reply)
	PRINTNL()
ENDPROC

PROC ON_CREATE()
	GENERIC_BRAIN_ON_CREATE()
	//SET_AUDIO_FLAG("ScriptedSpeechDuckingDisabled", TRUE)
	//iBlockObject = ADD_NAVMESH_BLOCKING_OBJECT(vInitialPos, <<2.000, 2.000, 2.000>>, 0)
ENDPROC

PROC ON_DESTROY()
	GENERIC_BRAIN_ON_DESTROY()
ENDPROC

PROC ADD_BLOCKING_OBJECT()
	IF iBlockObject = -1
		PRINTSTRING("[SP] ADD_NAVMESH_BLOCKING_OBJECT ")
		iBlockObject = ADD_NAVMESH_BLOCKING_OBJECT(vInitialPos, <<2.000, 2.000, 2.000>>, 0, FALSE, BLOCKING_OBJECT_WANDERPATH)
		PRINTINT(iBlockObject) PRINTNL()
	ENDIF
ENDPROC

PROC REMOVE_BLOCKING_OBJECT()
	IF iBlockObject != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iBlockObject)
			REMOVE_NAVMESH_BLOCKING_OBJECT(iBlockObject)
		ENDIF
		iBlockObject = -1
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_SP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF createBlip = TRUE
		IF NOT DOES_BLIP_EXIST(blipIndex)
			IF DOES_ENTITY_EXIST(pedIndex)
				IF NOT IS_PED_INJURED(pedIndex)
					blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
					IF bEnemy = FALSE
						SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
					ENDIF
					SET_BLIP_COLOUR(blipIndex, BLIP_COLOUR_BLUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_SP_BLIP(BLIP_INDEX &blipIndex)
	IF createBlip = TRUE
		IF DOES_BLIP_EXIST(blipIndex)
			REMOVE_BLIP(blipIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP(BOOL delete_spec_ped = FALSE)
	ON_DESTROY()
	IF bPedCreated
		//Only kill the conversation if the ped is the one speaking
		TEXT_LABEL_15 current_speech = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		PRINTSTRING("[SP] cleaning up...")
		PRINTSTRING(current_speech)
		PRINTNL()
		IF ARE_STRINGS_EQUAL(current_speech, saved_root)
			//Killing conversation, we are are still talking.
			PRINTSTRING("[SP] KILL_ANY_CONVERSATION")
			KILL_ANY_CONVERSATION()
		ELSE
			PRINTSTRING("[SP] NOT CALLING KILL_ANY_CONVERSATION")
		ENDIF
		PRINTNL()
		
		
		IF delete_spec_ped = TRUE	
			DELETE_PED(pedSpecial)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedSpecial)
		ENDIF
		bPedCreated = FALSE
	ENDIF
	
	//Get rid of our takling anim.
	IF NOT IS_STRING_NULL_OR_EMPTY(talking_anim)
		REMOVE_ANIM_DICT(talking_anim)
	ENDIF
	
	IF (isIdleAnimsrequested = TRUE)
		IF NOT IS_STRING_NULL_OR_EMPTY(idle_dic) //Check we have set it up. 
			IF HAS_ANIM_DICT_LOADED(idle_dic)
				REMOVE_ANIM_DICT(idle_dic)
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(into_idle_dic) //Check we have set it up. 
			IF HAS_ANIM_DICT_LOADED(into_idle_dic)
				REMOVE_ANIM_DICT(into_idle_dic)
			ENDIF
		ENDIF
	ENDIF
	
	IF (talk_context <> NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(talk_context)
	ENDIF
	
	//Remove the blip if there.
	IF DOES_BLIP_EXIST(pedBlip)
		SAFE_REMOVE_BLIP(pedBlip)
	ENDIF
	
	REMOVE_BLOCKING_OBJECT()
	
	//Set the players hurt soudns back on in case we are cleaning up mid conversation.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(), false)
	ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

FUNC BOOL IS_PED_ABOUT_TO_LEAVE()
	IF ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING
	OR ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING
		return TRUE
	ENDIF
	return FALSE
ENDFUNC

INT new_conversation_offset = 0
BOOL apply_new_conversation_offset = FALSE
BOOL show_debug_text = FALSE
	
#IF IS_DEBUG_BUILD
PROC SETUP_DEBUG_WIDGETS()
	START_WIDGET_GROUP("Special Peds")
		ADD_WIDGET_INT_SLIDER("New conversation offset", new_conversation_offset, 0, max_conversation_offset-1, 1)
		ADD_WIDGET_BOOL("Apply", apply_new_conversation_offset)
		ADD_WIDGET_BOOL("Show Extra Debug", show_debug_text)
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

BOOL controlledByAnim = FALSE

PROC MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
	IF NOT IS_ANIM_PLAYING_ON_PED()
		PRINTSTRING("Paul - SET_PED_IDLING MAKE_SURE_PED_DOESNT_STOP_ANIMATING") PRINTNL()
		SET_PED_IDLING(TRUE)
		//TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, INSTANT_BLEND_IN, INSTANT_BLEND_IN, -1, AF_DEFAULT)
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
	ENDIF
ENDPROC

SCRIPT(coords_struct inCoords)
	CPRINTLN(DEBUG_AMBIENT, "[SP] Starting script: ", GET_THIS_SCRIPT_NAME())
	PRINTSTRING("[SP] conversation_offset[thisPed]") PRINTINT(conversation_offset[thisPed]) PRINTNL()
	
	///...blocking object...
	vInitialPos = inCoords.vec_coord[0]
	ADD_BLOCKING_OBJECT()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU)
		CPRINTLN(DEBUG_AMBIENT, "[SP] Force cleanup has occurred.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF IS_SPECIALPED_DEAD(GET_SPECIAL_PED_FROM_SCRIPT_NAME(GET_THIS_SCRIPT_NAME()))
		CPRINTLN(DEBUG_AMBIENT, "[SP] Ped is dead, terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NOT IS_SCRIPT_ALLOWED_TO_RUN()
		CPRINTLN(DEBUG_AMBIENT, "[SP] Script isn't allowed to run, terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		SCRIPT_CLEANUP()
	ENDIF
	
	IF (IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE())
		CPRINTLN(DEBUG_AMBIENT, "[SP] Mission already running, terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	IF (IS_MISSION_LEADIN_ACTIVE())
		CPRINTLN(DEBUG_AMBIENT, "[SP] Lead in active, terminating script.")
		
		SCRIPT_CLEANUP()
	ENDIF
	
	//Don't allow ped brains to launch if the player is moving very fast.
	//The streaming engine will be under too much stress.
	IF VMAG2(GET_ENTITY_VELOCITY(PLAYER_PED_ID())) > AMBIENT_LOAD_PLAYER_SPEED_CUTOFF
	AND NOT IS_HIGH_MEMORY_PC()
		CPRINTLN(DEBUG_AMBIENT, "[SP] Ped brain can't run as the player is moving too fast.")
		SCRIPT_CLEANUP()
	ENDIF
	
	//Only allow ped brains to load assets once every 10 seconds.
	IF GET_GAME_TIMER() < (g_iPedBrainLastLoaded + PED_BRAIN_LOAD_COOLDOWN)
	AND NOT IS_HIGH_MEMORY_PC()
		CPRINTLN(DEBUG_AMBIENT, "[SP] Ped brain can't run as a ped brain has loaded too recently.")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Save our creation position.
	
	talking_anim = ""
	
	//General Init
	conversation_number[CHAR_MICHAEL] = 1
	conversation_number[CHAR_FRANKLIN] = 1
	conversation_number[CHAR_TREVOR] = 1
	
	//Call our custom setup, this sets up all variables needed.
	GENERIC_BRAIN_CUSTOM_INIT() 
	
	//Request our assets...
	REQUEST_MODEL(ePedModel)
	WHILE NOT HAS_MODEL_LOADED(ePedModel)
		WAIT(0)
	ENDWHILE
	
	IF NOT talkWithAnims
		CPRINTLN(DEBUG_AMBIENT, "[SP] Ped not setup with tagged animation/audio, terminating script.")
		SCRIPT_CLEANUP()
	ENDIF
	
	REQUEST_ANIM_DICT(idle_dic)
	REQUEST_ANIM_DICT(into_idle_dic)
	isIdleAnimsrequested = TRUE
	
	WHILE NOT HAS_ANIM_DICT_LOADED(idle_dic) OR NOT HAS_ANIM_DICT_LOADED(into_idle_dic) OR NOT HAS_MODEL_LOADED(ePedModel)	
		WAIT(0)
	ENDWHILE
	
	g_iPedBrainLastLoaded = GET_GAME_TIMER()
	CPRINTLN(DEBUG_AMBIENT, "[SP] Updated ped brain last loaded timer to ", g_iPedBrainLastLoaded, ".")
	
	//Create and setup our ped.	
	CLEAR_AREA_OF_PEDS(vInitialPos, 0.5)
	pedSpecial = CREATE_PED(PEDTYPE_SPECIAL, ePedModel, vInitialPos, inCoords.headings[0], FALSE)
	
	ON_CREATE()
	
	SET_MODEL_AS_NO_LONGER_NEEDED(ePedModel)
	bPedCreated = TRUE
	IF DOES_ENTITY_EXIST(pedSpecial)	
	AND IS_ENTITY_ALIVE(pedSpecial)
		SET_PED_COMBAT_ATTRIBUTES(pedSpecial, CA_ALWAYS_FLEE, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSpecial, TRUE)
	ENDIF
		
	conversation_split_offset = 0
	
	////////////////////////////////////////////////////////////
	//TODO TAKE THIS OUT!
	iNumRandomConvos = iNumRandomConvos
	bPlayAsConvo = bPlayAsConvo
	iRandomLimit = iRandomLimit
	iDelayAmt = iDelayAmt
	iNumPedInteractResponses = iNumPedInteractResponses
	////////////////////////////////////////////////////////////
	
	txt_block = txtPedMissionID
	txt_block += "AUD"
	txt_root = txtPedMissionID
	txt_root += "_RAND_"
	
	ped_state = SET_IDLING
	//conversation_offset = 0
	talk_context = NEW_CONTEXT_INTENTION
	
	BOOL do_monologue = TRUE
	
	//TEXT_LABEL_63 interact_anim = ""
	TEXT_LABEL_15 player_name
	TEXT_LABEL_15 greet_line
	TEXT_LABEL_15 conversation_line
	TEXT_LABEL_63 interact_anim_with_offset
	
	BOOL switched_to_idle
	BOOL conversation_played = FALSE
	TEXT_LABEL_15 tl15_alpha
	
	BOOL conversationLoaded
	BOOL convoStillRunning
	BOOL animPlayingOnPed
	BOOL conversationAlreadyOngoing
	bool carry_on_talking
	FLOAT talk_dist
	INT nudgeTimer = 0
	
	BOOL startConvo
	
	max_random_offset = max_random_offset
	max_random_split_offsets[0] = max_random_split_offsets[0]
	random_reply = random_reply
	
	new_conversation_offset = new_conversation_offset
	apply_new_conversation_offset = apply_new_conversation_offset
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	
	PRINTSTRING("[SP] PRE SET_PED_IDLING") PRINTNL()
	SET_PED_IDLING(FALSE, FALSE)
	PRINTSTRING("[SP] POST SET_PED_IDLING") PRINTNL()
	
	//TODO - rename the enum states, as they are crap names. 
	
	show_debug_text = FALSE
	WHILE do_monologue = TRUE

		IF NOT IS_ENTITY_ALIVE(pedSpecial)
			CPRINTLN(DEBUG_AMBIENT, "[SP] Ped has died, terminating script.")
			SET_SPECIALPED_AS_DEAD(GET_SPECIAL_PED_FROM_SCRIPT_NAME(GET_THIS_SCRIPT_NAME()))
			SCRIPT_CLEANUP()
		ENDIF
		
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			//We are out of range, delete the ped.
			CPRINTLN(DEBUG_AMBIENT, "[SP] Out of range, terminating script.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		FLOAT dista = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedSpecial) 
		
		IF ((dista < 10.0) AND CAN_PED_LOOK_AT_PLAYER())
			//Look at the player.
			SET_IK_TARGET(pedSpecial, IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
		ENDIF
		
		IF (dista < 8.0)
			//player look at the ped. Stagger the distance so they don't look at the same tie. 
			SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, pedSpecial, ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
		ENDIF

		IF (dista < 15.0)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_TALK)
		ENDIF

		//Draw the state of the controlled by anim flag.
		TEXT_LABEL_63 conv_output
		TEXT_LABEL_63 talking_output
			
			
		IF show_debug_text
			IF controlledByAnim
				DRAW_DEBUG_TEXT_2D("controlledByAnim", <<0.02, 0.5, 0>>)
			ELSE
				DRAW_DEBUG_TEXT_2D("NOT controlledByAnim", <<0.02, 0.5, 0>>)
			ENDIF
			
			IF iBlockObject = -1
				DRAW_DEBUG_TEXT_2D("iBlockObject OFF", <<0.02, 0.6, 0>>)
			ELSE
				DRAW_DEBUG_TEXT_2D("iBlockObject ON", <<0.02, 0.6, 0>>)
			ENDIF
			
			IF monologueStarted
				DRAW_DEBUG_TEXT_2D("MONOLOGUE", <<0.02, 0.65, 0>>)
			ELSE
				DRAW_DEBUG_TEXT_2D("IDLE", <<0.02, 0.65, 0>>)
			ENDIF
			
			conv_output = "conversation_offset "
			conv_output+=conversation_offset[thisPed]
			DRAW_DEBUG_TEXT_2D(conv_output, <<0.02, 0.8, 0>>)
			conv_output = "max_conversation_offset "
			conv_output+=max_conversation_offset
			DRAW_DEBUG_TEXT_2D(conv_output, <<0.02, 0.81, 0>>)
			
			conv_output = "conversation_split_offset "
			conv_output+=conversation_split_offset
			DRAW_DEBUG_TEXT_2D(conv_output, <<0.02, 0.82, 0>>)
			conv_output = "max_conversation_split_offsets[conversation_offset] "
			conv_output+=max_conversation_split_offsets[conversation_offset[thisPed]]
			DRAW_DEBUG_TEXT_2D(conv_output, <<0.02, 0.83, 0>>)
		ENDIF
		//Do we need to clean up?
		VECTOR pedCoords = GET_ENTITY_COORDS(pedSpecial)
		//PRINTSTRING("Paul speed") PRINTFLOAT(GET_ENTITY_SPEED(PLAYER_PED_ID())) PRINTNL()

		IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(pedSpecial)
		AND NOT (ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING)
		AND NOT (ped_state = START_MOVE_BACK_TO_INITIAL_POSITION)
		AND NOT (ped_state = WAIT_MOVE_BACK_TO_INITIAL_POSITION)
			PED_BUMPED()
			//We have been nudged too far away.
			PRINTSTRING("PAUL - WE ARE touching") PRINTNL()
			IF PED_FLEE_WHEN_BUMPED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
				ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING
			ELSE
				REMOVE_BLOCKING_OBJECT()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//SET_PED_IDLING(TRUE, TRUE)
				ped_state = START_MOVE_BACK_TO_INITIAL_POSITION
				nudgeTimer = GET_GAME_TIMER()
			ENDIF
			PLAY_PED_AMBIENT_SPEECH_NATIVE(pedSpecial, "GENERIC_CURSE_MED", "SPEECH_PARAMS_FORCE")

		ENDIF

		//Wait a few frames for the tests to see if we want to quit. Nasty things happen with anims if we try and do too many things on the same frame, mainly forcing animations to play. 
		//This is more prominent when debugging to a mission, as lots of things tend to kick off at the same time. 
		IF (IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE() OR GET_MISSION_FLAG() OR IS_MISSION_LEADIN_ACTIVE() OR NOT IS_SCRIPT_ALLOWED_TO_RUN())
			IF NOT (IS_PED_ABOUT_TO_LEAVE())
			AND (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("director_mode"))=0)	//B* 2220119: Only leave if not exiting Director mode
				IF IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
					PRINTSTRING("[SP] IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()") PRINTNL()
				ENDIF
				IF GET_MISSION_FLAG()
					PRINTSTRING("[SP] GET_MISSION_FLAG()") PRINTNL()
				ENDIF
				
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
				ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING
			ENDIF
		ENDIF
		IF IS_PLAYER_PLAYING(PLAYER_ID())//No idea why we need this, hopefully it won't hide any other problems. 
			IF (((IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSpecial) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSpecial)) AND dista < 25.0) 
			AND (GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED) 
			AND IS_PED_IN_PED_VIEW_CONE(pedSpecial, PLAYER_PED_ID()))
				IF NOT (IS_PED_ABOUT_TO_LEAVE())
					//The player is aiming at the ped with a weapon, make them run off.
					CPRINTLN(DEBUG_AMBIENT, "[SP] PLAYER AIMING AT ME")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
					ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING
				ENDIF
			ENDIF
		ENDIF
		IF (IS_PED_INJURED(pedSpecial) OR IS_BULLET_IN_AREA(pedCoords, 50) OR IS_BULLET_IN_AREA(pedCoords, 50, FALSE) OR IS_PROJECTILE_IN_AREA(pedCoords, <<20, 20, 20>>)) OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, pedCoords, 50.0) OR GET_IS_PETROL_DECAL_IN_RANGE(pedCoords, 1)
 
			IF NOT (IS_PED_ABOUT_TO_LEAVE())
				//Set to flee with the above situations as well. 
				CPRINTLN(DEBUG_AMBIENT, "[SP] SET TO FLEE")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
				ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING
			ENDIF
		ENDIF
		
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSpecial, PLAYER_PED_ID())
			//Set to flee with the above situations as well. 
			CPRINTLN(DEBUG_AMBIENT, "[SP] PED ATTACKED")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
			ped_state = WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING
		ENDIF
		
		//Add the blip
		IF (GET_DISTANCE_BETWEEN_ENTITIES(pedSpecial, PLAYER_PED_ID()) < 25.0)
			IF NOT DOES_BLIP_EXIST(pedBlip)
				SAFE_ADD_BLIP_SP_PED(pedBlip, pedSpecial, FALSE)
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(pedBlip)
				SAFE_REMOVE_SP_BLIP(pedBlip)
			ENDIF
		ENDIF
		
		//FLOAt ddd = GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), pedSpecial)
		//PRINTSTRING("DISTANC") PRINTFLOAT(ddd) PRINTNL()
		
		// If a special ped is none interactable, unlock associated Director Mode character 
		// when the player is within 4m of them.
		#IF FEATURE_SP_DLC_DIRECTOR_MODE
			IF NOT canInteract
				IF NOT IS_PED_INJURED(pedSpecial) AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSpecial)) < 16.0 // 4m^2
						INT iScriptHash = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
						DirectorUnlockSpecial duSpecialUnlock = PRIVATE_Get_Director_Unlock_From_Special_Ped_Script_Hash(iScriptHash)
						IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK( duSpecialUnlock )
							IF NOT IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED( duSpecialUnlock )
								CPRINTLN(DEBUG_DIRECTOR, "Unlocking none interactive special ped based on proximity.")
								UNLOCK_DIRECTOR_SPECIAL_CHAR_FROM_SCRIPT_HASH(iScriptHash)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		
		SWITCH (ped_state)
			CASE SET_IDLING
				DRAW_DEBUG_TEXT_2D("SET_IDLING", <<0.02, 0.1, 0>>)
				
				//Request the dictionary for the first animation in out split.
				conversation_split_offset = 0
				REQUEST_SPLIT_DIC()
				
				//Go to next state, where we wait for the anim to be loaded.
				ped_state = WAIT_FOR_ANIM_TO_BE_LOADED
				
				//Just in case we are here when the anim comes to an end, start idling again. 
				IF NOT IS_ANIM_PLAYING_ON_PED()
					DRAW_DEBUG_TEXT_2D("SET_IDLING SET_PED_IDLING", <<0.02, 0.11, 0>>)
					SET_PED_IDLING()
				ENDIF
			BREAK
			
			CASE WAIT_FOR_ANIM_TO_BE_LOADED
				DRAW_DEBUG_TEXT_2D("WAIT_FOR_ANIM_TO_BE_LOADED", <<0.02, 0.15, 0>>)
				
				conversationAlreadyOngoing = IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF show_debug_text
					IF conversationAlreadyOngoing
						DRAW_DEBUG_TEXT_2D("conversationAlreadyOngoing", <<0.8, 0.1, 0>>)
					ELSE
						DRAW_DEBUG_TEXT_2D("NOT conversationAlreadyOngoing", <<0.8, 0.1, 0>>)
					ENDIF
				ENDIF
				//Have we done loading and nothing else is going on?
				IF IS_DIC_LOADED() AND NOT conversationAlreadyOngoing AND NOT IS_PLAYER_IN_ANY_SHOP()
					//Yes, so go to the next state. 
					ped_state = START_IDLING
				ENDIF
				
				//Again, just in case we hit the end of the anim here, start idling again.
				IF NOT IS_ANIM_PLAYING_ON_PED()
					PRINTSTRING("[SP] - END OF ANIM HIT!") PRINTNL()
					SET_PED_IDLING()
				ENDIF
			BREAK
			
			CASE START_MOVE_BACK_TO_INITIAL_POSITION
				DRAW_DEBUG_TEXT_2D("START_MOVE_BACK_TO_INITIAL_POSITION", <<0.02, 0.2, 0>>)	
				
				//PRINTSTRING("[SP] START_MOVE_BACK_TO_INITIAL_POSITION!") PRINTNL()
				IF ((GET_GAME_TIMER() - nudgeTimer) > 1) //Wait a few seconds before trying to walk back. 
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedSpecial, vInitialPos, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_ACCURATE_WALKRUN_START, inCoords.headings[0])
					ped_state = WAIT_MOVE_BACK_TO_INITIAL_POSITION
				ENDIF
				//IF NOT IS_ANIM_PLAYING_ON_PED()
				//	//We have been nudged, so if we finish the current anim, blend into idle.
				//	PRINTSTRING("Paul - START_MOVE_BACK_TO_INITIAL_POSITION blending into idle.") PRINTNL()
				//	SET_PED_IDLING(TRUE)
				//ENDIF
			BREAK
			
			CASE WAIT_MOVE_BACK_TO_INITIAL_POSITION
				DRAW_DEBUG_TEXT_2D("WAIT_MOVE_BACK_TO_INITIAL_POSITION", <<0.02, 0.25, 0>>)
				
				PRINTSTRING("[SP] WAIT_MOVE_BACK_TO_INITIAL_POSITION!") PRINTNL()
				IF ((GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
				AND (GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK))	
					DRAW_DEBUG_TEXT_2D("WAIT_MOVE_BACK_TO_INITIAL_POSITION TASK_PLAY_ANIM", <<0.02, 0.26, 0>>)
					TASK_PLAY_ANIM(pedSpecial, into_idle_dic, "idle_intro", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_DEFAULT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
					ADD_BLOCKING_OBJECT()
					ped_state = SET_IDLING
				ENDIF	
			BREAK
			
			CASE START_IDLING
				DRAW_DEBUG_TEXT_2D("START_IDLING", <<0.02, 0.1, 0>>)
				
				IF g_bMagDemoActive
					talk_dist = 25.0
				ELSE
					talk_dist = 25.0
				ENDIF
				
				//Load the conversation.
				IF GET_DISTANCE_BETWEEN_ENTITIES(pedSpecial, PLAYER_PED_ID()) < talk_dist
					
					IF PRELOAD_MONOLOGUE() = TRUE 
						ped_state = PLAY_MONOLOGUE
						monologueStarted = FALSE
						
						//monologueStarted: This flag is used so GENERIC_BRAIN_ON_PLAYING_MONOLOGUE does not fire when we are running the idles.
						//We don't want GENERIC_BRAIN_ON_PLAYING_MONOLOGUE to run unless we are running monologues. 
					ENDIF
				ENDIF
				
				//Again, just in case we hit the end of the anim here, start idling again.
				IF NOT IS_ANIM_PLAYING_ON_PED() 
					PRINTSTRING("[SP] - END OF ANIM HIT 2!") PRINTNL()
					SET_PED_IDLING()
				ENDIF
			BREAK
			
			CASE PRE_PLAY_MONOLOGUE
				DRAW_DEBUG_TEXT_2D("PRE_PLAY_MONOLOGUE", <<0.02, 0.1, 0>>)
				
				//Wait until we have loaded the next dictionary in. 
				
				IF IS_DIC_LOADED()
					//Yes, so go to the next state. 
					ped_state = PLAY_MONOLOGUE
				ENDIF
				//Don't do anything else ... if we don't load the next in time, there is not much we can do....
				IF NOT IS_ANIM_PLAYING_ON_PED()
					PRINTSTRING("[SP] - PRE_PLAY_MONOLOGUE NO ANIM PLAYING!") PRINTNL()
				ENDIF
			BREAK
			
			CASE PLAY_MONOLOGUE
				DRAW_DEBUG_TEXT_2D("PLAY_MONOLOGUE", <<0.02, 0.1, 0>>)
				max_conversation_offset = max_conversation_offset
				
				
				
				
				//Have we done playing the  anim?
				//Do we want to quit?
				IF IS_PLAYER_IN_ANY_SHOP()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_PED_IDLING()
					ped_state = SET_IDLING
				//SCRIPT_ASSERT("PAUL")
				ELIF NOT IS_ANIM_PLAYING_ON_PED() 
					
					//Play the anim.
					SET_PED_MONALOGUE()
					monologueStarted = TRUE
					//Remove the requested one.
					REMOVE_DIC()
					
					//Request the next one.
					conversation_split_offset++
					
					carry_on_talking = TRUE
					IF dista > 15.0 //and g_bMagDemoActive
						carry_on_talking = false
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_PED_IDLING(TRUE, FALSE)
					endif
					
					IF conversation_split_offset < max_conversation_split_offsets[conversation_offset[thisPed]] AND carry_on_talking
						REQUEST_SPLIT_DIC()
						ped_state = PRE_PLAY_MONOLOGUE
					ELSE
						//Move to the next conversation
						ped_state = MOVE_TO_NEXT_MONOLOGUE
					ENDIF
				ELSE
					//We are playing a monologue.
					//PRINTSTRING("[SP] IS_STRING_NULL_OR_EMPTY") PRINTNL()
					//PRINTSTRING(current_monologue_line) PRINTNL()
					//PRINTSTRING(txtBaseIdleAnim) PRINTNL()
					//IF NOT IS_STRING_NULL_OR_EMPTY(current_monologue_dic) AND NOT IS_STRING_NULL_OR_EMPTY(current_monologue_line) AND NOT ARE_STRINGS_EQUAL(current_monologue_line, txtBaseIdleAnim)
					IF monologueStarted = TRUE
						//PRINTSTRING("[SP] CurrentTime ") PRINTSTRING(current_monologue_dic) PRINTSTRING(" ") PRINTSTRING(current_monologue_line) PRINTNL()
						IF IS_ENTITY_PLAYING_ANIM(pedSpecial, current_monologue_dic, current_monologue_line)
							GENERIC_BRAIN_ON_PLAYING_MONOLOGUE(conversation_offset[thisPed], conversation_split_offset, GET_ENTITY_ANIM_CURRENT_TIME(pedSpecial, current_monologue_dic, current_monologue_line))
						ELSE
							PRINTSTRING("[Sp] Warning - Not playing the anim that we thought we were!") PRINTNL()
						ENDIF
					ENDIF
					//ENDIF
				ENDIF
			BREAK
			
			CASE MOVE_TO_NEXT_MONOLOGUE
				DRAW_DEBUG_TEXT_2D("MOVE_TO_NEXT_MONOLOGUE", <<0.02, 0.1, 0>>)
				
				//Are we not playing an anim, and also make sure there are no other conversations going on. 
				//We don't want to change the controlled flag while any other convo is going on. 
				//Note, this will rpoabbly make the ped do nothing until it can carry on.
				//As soon as the CONTROLLED_BY_ANIM flasg is set in the anim itself, this can be taken out. 
				convoStillRunning =  IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				animPlayingOnPed = IS_ANIM_PLAYING_ON_PED()
				
				GENERIC_BRAIN_ON_END_MONOLOGUE(conversation_offset[thisPed])
				
				IF (NOT animPlayingOnPed) AND ((NOT convoStillRunning) OR noConversation) //OK, are we not playing an anim on the ped, AND there is no conversation currently running OR we don't play conversations so we don't need to wait for conversations to end andyway. Simples.
					PRINTSTRING("IS_ANIM_PLAYING_ON_PED")
					PRINTNL()
					
					PRINTSTRING("+SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM")
					PRINTNL()
					PRINTSTRING("+NOT SET_CONVERSATION_AUDIO_CONTROLLED_BY_ANIM")
					PRINTNL()
					
					IF apply_new_conversation_offset //This is for the debug widget.
						conversation_offset[thisPed] = new_conversation_offset
					ELSE
						conversation_offset[thisPed]++
					ENDIF
					PRINTINT(conversation_offset[thisPed])
					PRINTNL()
					IF NOT (conversation_offset[thisPed] < max_conversation_offset)
						conversation_offset[thisPed] = 0
					ENDIF
					SET_PED_IDLING()
					ped_state = SET_IDLING
				ELIF NOT animPlayingOnPed AND convoStillRunning
					//Anotehr conversation is running, wait until we are safe to proceed.
					DRAW_DEBUG_TEXT_2D("CONVERSATION STILL RUNNING", <<0.02, 0.3, 0>>)
					//If we are not running an animm play the idle again. Otherwise we will glitch back to the deault pose. 
					IF NOT IS_ANIM_PLAYING_ON_PED()
						SET_PED_IDLING()
					ENDIF
				ENDIF
			BREAK
			
			CASE NEWSTATE
				DRAW_DEBUG_TEXT_2D("NEWSTATE", <<0.02, 0.1, 0>>)
				
				GET_CONVERSATION_NAME(player_name)
				ADD_PED_FOR_DIALOGUE(convoPed, 0, PLAYER_PED_ID(), player_name)	
				ADD_PED_FOR_DIALOGUE(convoPed, 3, pedSpecial, txtPedVoiceID)	
				
				GET_GREET_LINE(greet_line)
				IF CREATE_CONVERSATION(convoPed, txt_block, greet_line, CONV_PRIORITY_AMBIENT_MEDIUM, DISPLAY_SUBTITLES)
					ped_state = WAIT_TO_START_CONVERSATION
				ENDIF
				
				MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
			BREAK
			
			CASE WAIT_TO_START_CONVERSATION
				DRAW_DEBUG_TEXT_2D("WAIT_TO_START_CONVERSATION", <<0.02, 0.1, 0>>)
				
				switched_to_idle = switched_to_idle
				/*IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())
					//Stop our current conversation...
				
					//we need to diff between the idle and the monologue
					IF NOT IS_ANIM_PLAYING_ON_PED() OR switched_to_idle = TRUE
						PRINTLN("SWITCHING TO IDLE")
						SET_PED_IDLING(TRUE)
						PRINTLN("/SWITCHING TO IDLE")
						//REMOVE_PED_FOR_DIALOGUE(convoPed, 0)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ped_state = START_CONVERSATION
					ENDIF
					
				ENDIF*/
				//should we do something different depending on the mode?
				//monologe, wait until anim done
				//idle, just go!
				
				//so, if monologue, wait until anim over, so he carries on talking,
				//but if idle, just check for 
				startConvo = FALSE
				IF monologueStarted = FALSE
					//We are in idle, just go. 
					MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
					
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() 
						
					startConvo = TRUE
				ELSE
					//IF NOT IS_AMBIENT_SPEECH_PLAYING(PLAYER_PED_ID())// AND NOT IS_ANIM_PLAYING_ON_PED() 
					
					//	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() //this is ok, as we have checked above for ambient speech playing.
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	//Wait for the players greet.
						startConvo = TRUE
					ENDIF
					MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
				ENDIF
				
				IF startConvo
				
					//Start the next anim...
					PRINTLN("NOT IS_ANIM_PLAYING_ON_PED()")
					//SET_PED_IDLING(TRUE)
					//switched_to_idle = TRUE
					
					
					//if we are in monologue, play the next anim so we are doing something while loading.
					//else play the idle
					
					talking_offset = starting_offset
					
					
					//Set the convo paramters.
					//Are we a normal conversation?
					//Or random?
					IF NOT talkWithRandom
						//Normal, speak based on what character interacted with you.
						GET_CONVERSATION_LINE(conversation_line)
						GET_CONVERSATION_NAME(player_name)
					ELSE
						//Random line, doesn't matter which character said anything. 
						GENERATE_RANDOM_CONVERSATION()
						GET_RANDOM_CONV_LINE(conversation_line)
					ENDIF
					
					//Add the characters.
					ADD_PED_FOR_DIALOGUE(convoPed, 0, PLAYER_PED_ID(), player_name)	
					ADD_PED_FOR_DIALOGUE(convoPed, 3, pedSpecial, txtPedVoiceID)	
					
					//SETTIMERA(0)  //temp
					//Skip to the next monologue, as we don't want to repeat the last one. 
					//This has been tested, but I am still a bit dubious with the loading, there may be situations where this won't work.
					//Checking in for testing though.
					conversation_offset[thisPed]++
					IF NOT (conversation_offset[thisPed] < max_conversation_offset)
						conversation_offset[thisPed] = 0
					ENDIF
					
					ped_state = START_LOADING_CONVERSATION
				ENDIF
			BREAK
			
			CASE START_LOADING_CONVERSATION
				DRAW_DEBUG_TEXT_2D("START_LOADING_CONVERSATION", <<0.02, 0.1, 0>>)
				
				//Start loading the first of the conversation anims...
				IF NOT talkWithRandom
					//Normal anim....based on character.
					GET_TALKING_DIC_NAME(talking_anim)
					INT_TO_ALPHAS(talking_offset, tl15_alpha)
					talking_anim += tl15_alpha
				ELSE
					//Random talk, not based off character. 
					GET_RANDOM_TALKING_NAME(talking_anim)
					INT_TO_ALPHAS(talking_offset, tl15_alpha)
					talking_anim += tl15_alpha
				ENDIF
				
				PRINTLN("[SP] START_LOADING_CONVERSATION")
				PRINTSTRING(talking_anim)
				PRINTNL()
				REQUEST_ANIM_DICT(talking_anim)
				
				PRINTLN("[SP] /REQUEST_ANIM_DICT")
				PRINTSTRING(talking_anim)
				PRINTNL()
				conversationLoaded = FALSE
				
			
				IF HAS_ANIM_DICT_LOADED(talking_anim) //AND TIMERA() > 16000
					PRINTLN("[SP] HAS_ANIM_DICT_LOADED") PRINTNL()
					PRINTSTRING("[SP] conversation_line ") PRINTSTRING(conversation_line) PRINTNL()
					IF noConversation
						conversationLoaded = TRUE
					ELIF CREATE_CONVERSATION(convoPed, txt_block, conversation_line, CONV_PRIORITY_AMBIENT_MEDIUM, DISPLAY_SUBTITLES)
						conversationLoaded = TRUE
						PRINTLN("[SP] CREATE_CONVERSATION SUCCESS!") PRINTNL()
					ELSE
						PRINTSTRING("[SP] no conversation loaded!") PRINTNL()
					ENDIF
					
					IF conversationLoaded = TRUE
						ped_state = CHECK_FOR_LOADED_CONVERSATION
					ENDIF
				ELSE
					PRINTLN("[SP] HAS_ANIM_DICT_LOADED FAIL!") PRINTNL()
				ENDIF
				MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
					
			BREAK
			
			CASE CHECK_FOR_LOADED_CONVERSATION
				DRAW_DEBUG_TEXT_2D("CHECK_FOR_LOADED_CONVERSATION", <<0.02, 0.1, 0>>)
			
				IF IS_SCRIPTED_CONVERSATION_LOADED()
					PRINTLN("[SP] IS_SCRIPTED_CONVERSATION_LOADED SUCCCESS!") PRINTNL()
					GET_INTERACT_ANIM_WITH_OFFSET(interact_anim_with_offset)
					PRINTLN("[SP] TASK_PLAY_ANIM")
					PRINTSTRING(talking_anim)
					PRINTNL()
					PRINTSTRING(interact_anim_with_offset)
					PRINTNL()
					
					//SCRIPT_ASSERT("TEST")
					//This is the first conversation line. 
					TASK_PLAY_ANIM(pedSpecial, talking_anim, interact_anim_with_offset, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_DEFAULT)
					//TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY|AF_LOOPING)

					PRINTSTRING("[SP] switching to START_CONVERSATION") PRINTNL()
					ped_state = START_CONVERSATION
				ELSE
					PRINTLN("[SP] IS_SCRIPTED_CONVERSATION_LOADED FAIL!") PRINTNL()
					MAKE_SURE_PED_DOESNT_STOP_ANIMATING()
				ENDIF
			BREAK
			
			CASE START_CONVERSATION
				DRAW_DEBUG_TEXT_2D("START_CONVERSATION", <<0.02, 0.1, 0>>)
				PRINTSTRING("[SP] START_CONVERSATION")
				PRINTNL()
				
				//Preload the next anim...
				talking_offset++
				current_anim = talking_anim
				
				IF ((NOT (talking_offset < GET_NUMBER_OF_CONVERSATION_SECTIONS())) OR (dista > 10.0) )
					//TODO - I am convinced that the conversations don't work like they should - look into getting the conv playing split up.
					//Once this works, we should not have to kill the conversations. 
					IF dista > 10.0 AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_ANY_CONVERSATION()
					ENDIF
					ped_state = WAIT_FOR_CONVERSATION_TO_END
					//Wait for the conversation to end, but don't pre load the next set (there isn't any more at this point).
				ELSE
					//Are we too far away?
					//current_anim = talking_anim
				
					IF NOT talkWithRandom
						//Normal anim....based on character.
						GET_TALKING_DIC_NAME(talking_anim)
						INT_TO_ALPHAS(talking_offset, tl15_alpha)
						talking_anim += tl15_alpha
					ELSE
						//Random talk, not based off character. 
						GET_RANDOM_TALKING_NAME(talking_anim)
						INT_TO_ALPHAS(talking_offset, tl15_alpha)
						talking_anim += tl15_alpha
					ENDIF
				
					REQUEST_ANIM_DICT(talking_anim)
					ped_state = WAIT_FOR_CONVERSATION_SECTION_OVER
				ENDIF
			BREAK
			
			CASE WAIT_FOR_CONVERSATION_SECTION_OVER
				DRAW_DEBUG_TEXT_2D("WAIT_FOR_CONVERSATION_SECTION_OVER", <<0.02, 0.1, 0>>)
				
				talking_output = "talking offset:"
				talking_output+= talking_offset
				DRAW_DEBUG_TEXT_2D(talking_output, <<0.02, 0.9, 0>>)
				talking_output=GET_NUMBER_OF_CONVERSATION_SECTIONS()
				DRAW_DEBUG_TEXT_2D(talking_output, <<0.03, 0.9, 0>>)
				
				IF HAS_ANIM_DICT_LOADED(talking_anim) 
					DRAW_DEBUG_TEXT_2D("HAS_ANIM_DICT_LOADED", <<0.02, 0.2, 0>>)
					IF NOT IS_ANIM_PLAYING_ON_PED()	
						DRAW_DEBUG_TEXT_2D("NOT IS_ANIM_PLAYING_ON_PED", <<0.02, 0.3, 0>>)
						
						//Remove the current anim. TODO test this properly. 
						PRINTSTRING("[SP] removing current_anim")
						PRINTNL()
						PRINTSTRING(current_anim)
						PRINTNL()
						REMOVE_ANIM_DICT(current_anim)
						PRINTSTRING("[SP] /removing current_anim")
						
						IF NOT talkWithRandom
							//Normal anim....based on character.
							GET_TALKING_DIC_NAME(talking_anim)
							INT_TO_ALPHAS(talking_offset, tl15_alpha)
							talking_anim += tl15_alpha
						ELSE
							//Random talk, not based off character. 
							GET_RANDOM_TALKING_NAME(talking_anim)
							INT_TO_ALPHAS(talking_offset, tl15_alpha)
							talking_anim += tl15_alpha
						ENDIF
						
						GET_INTERACT_ANIM_WITH_OFFSET(interact_anim_with_offset)
						//GENERIC_GET_INTERACT_ANIM(interact_anim)
						//interact_anim_with_offset = interact_anim
						//interact_anim_with_offset += "_"
						//interact_anim_with_offset += talking_offset
						
						TASK_PLAY_ANIM(pedSpecial, talking_anim, interact_anim_with_offset, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_DEFAULT)
						//TASK_PLAY_ANIM(pedSpecial, idle_dic, txtBaseIdleAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_SECONDARY | AF_UPPERBODY)

						PRINTSTRING("[SP]FORCE_PED_AI_AND_ANIMATION_UPDATE 1") PRINTNL()
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSpecial)
						
						

						ped_state = START_CONVERSATION
					ENDIF
				ELSE
					IF NOT IS_ANIM_PLAYING_ON_PED()	
						//We have not loaded the next anim, but the current one has come to an end. 
						PRINTSTRING("[SP] We have not loaded the next anim, but the current one has come to an end.") 
						PRINTSTRING("Next talking offset ") PRINTINT(talking_offset) PRINTNL()
						PRINTNL()
					ENDIF
				ENDIF
			BREAK
			
			CASE WAIT_FOR_CONVERSATION_TO_END
				DRAW_DEBUG_TEXT_2D("WAIT_FOR_CONVERSATION_TO_END", <<0.02, 0.1, 0>>)
			
			
				IF NOT IS_ANIM_PLAYING_ON_PED()	
					REMOVE_PED_FOR_DIALOGUE(convoPed, 0)
					
					//Set back the players hurt sounds.
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(), false)
					ENDIF
					
					//Remove our dictionary...
					PRINTSTRING("[SP] WAIT_FOR_CONVERSATION_TO_END removing current_anim")
					PRINTNL()
					PRINTSTRING(current_anim)
					PRINTNL()
					REMOVE_ANIM_DICT(current_anim)
					PRINTSTRING("[SP] /WAIT_FOR_CONVERSATION_TO_END removing current_anim")
					
					CONVERSATION_OVER()
					
					SET_PED_IDLING()
					ped_state = SET_IDLING
				ENDIF
			BREAK
			
			CASE WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING
				DRAW_DEBUG_TEXT_2D("WAITING_TO_BLEND_INTO_IDLE_BEFORE_FLEEING", <<0.02, 0.1, 0>>)
				WAIT(1000) //Wait until we blend proper.
				//Wait until we have blended into the base pose until fleeing. 
			//	IF ((GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_PLAY_ANIM) <> PERFORMING_TASK)
			//	AND (GET_SCRIPT_TASK_STATUS(pedSpecial, SCRIPT_TASK_PLAY_ANIM) <> WAITING_TO_START_TASK))
					SET_PED_FLEE()
					do_monologue = FALSE
			//	ENDIF
			BREAK
			
			CASE WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING
				DRAW_DEBUG_TEXT_2D("WAITING_TO_BLEND_INTO_IDLE_BEFORE_WANDERING", <<0.02, 0.1, 0>>)
				//WAIT(750) //Wait until we blend proper.
				SET_PED_WANDER()
				do_monologue = FALSE
			BREAK
		ENDSWITCH
		
		#if USE_CLF_DLC
			BOOL contextUp = (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controllerCLF")) > 0)
			IF NOT contextUp
				PRINTSTRING("Paul - NOT contextUp") PRINTNL()
			ENDIF
		#endif
		#if USE_NRM_DLC
			BOOL contextUp = (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controllerNRM")) > 0)
			IF NOT contextUp
				PRINTSTRING("Paul - NOT contextUp") PRINTNL()
			ENDIF
		#endif
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			BOOL contextUp = (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("context_controller")) > 0)
			IF NOT contextUp
				PRINTSTRING("Paul - NOT contextUp") PRINTNL()
			ENDIF
		#endif
		#endif
		
		//The player has started talking. 
		IF ((ped_state = SET_IDLING OR ped_state = WAIT_FOR_ANIM_TO_BE_LOADED OR ped_state = START_IDLING OR ped_state = PLAY_MONOLOGUE OR ped_state = MOVE_TO_NEXT_MONOLOGUE OR ped_state = PRE_PLAY_MONOLOGUE) AND (GET_DISTANCE_BETWEEN_ENTITIES(pedSpecial, PLAYER_PED_ID()) < 5.0) AND (talkWithAnims = TRUE) AND NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)) AND (conversation_played = FALSE) AND (canInteract = TRUE) AND(NOT g_bMagDemoActive) AND CAN_TALK_TO_PED()) AND contextUp
			IF (talk_context = NEW_CONTEXT_INTENTION)
				REGISTER_CONTEXT_INTENTION(talk_context, CP_HIGH_PRIORITY, sInteractLabel)
			ENDIF

			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)

			IF HAS_CONTEXT_BUTTON_TRIGGERED(talk_context)
				
				//Set the state to load the conversation.
				GET_CONVERSATION_NAME(player_name)
				GET_GREET_LINE(greet_line)
				
				//Turn off players hurt sounds.
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					DISABLE_PED_PAIN_AUDIO(PLAYER_PED_ID(), true)
				ENDIF
			
				
				//Kill the conversation...
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//Play the players speech
				
				//created =  CREATE_CONVERSATION(convoPed, txt_block, convo_txt_root, CONV_PRIORITY_AMBIENT_MEDIUM, DISPLAY_SUBTITLES)
				//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PLAYER_PED_ID(), greet_line, player_name, "SPEECH_PARAMS_FORCE_FRONTEND")
				//Set the ped to go to the idle straight away.
				SET_PED_IDLING(TRUE)
				
				conversation_played = TRUE //TODO PUT BACK
				
				//ped_state = WAIT_TO_START_CONVERSATION
				ped_state = NEWSTATE
				switched_to_idle = FALSE
				
				//Unlock any Director Mode characters associated with this ped brain.
				#IF FEATURE_SP_DLC_DIRECTOR_MODE
					DirectorUnlockSpecial eSpecialUnlock = PRIVATE_Get_Director_Unlock_From_Special_Ped_Script_Hash(GET_HASH_KEY(GET_THIS_SCRIPT_NAME()))
					IF IS_VALID_DM_SPECIAL_CHARA_UNLOCK(eSpecialUnlock)
						IF NOT IS_DIRECTOR_SPECIAL_CHARACTER_UNLOCKED(eSpecialUnlock)
							CPRINTLN(DEBUG_DIRECTOR, "Unlocking interactive special ped based on button press.")
							UNLOCK_DIRECTOR_SPECIAL_CHAR_FROM_SCRIPT_HASH(GET_HASH_KEY(GET_THIS_SCRIPT_NAME()))
						ENDIF
					ENDIF
				#ENDIF
				
				RELEASE_CONTEXT_INTENTION(talk_context)
			ENDIF
		ELSE
			IF (talk_context <> NEW_CONTEXT_INTENTION)
				RELEASE_CONTEXT_INTENTION(talk_context)
			ENDIF
		ENDIF
		
		
		GENERIC_BRAIN_PROCESS()
		//Debug kill the script
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
				CPRINTLN(DEBUG_AMBIENT, "[SP] QUITTING DEBUG")
				WAIT(30)
				KILL_ANY_CONVERSATION()
				STOP_SCRIPTED_CONVERSATION(FALSE)
				RELEASE_CONTEXT_INTENTION(talk_context)
				TERMINATE_THIS_THREAD()
			ENDIF
		#ENDIF
		WAIT(0)
	ENDWHILE
	
	IF (talk_context <> NEW_CONTEXT_INTENTION)
		RELEASE_CONTEXT_INTENTION(talk_context)
	ENDIF
	
	//Remove the blip if there.
	IF DOES_BLIP_EXIST(pedBlip)
		SAFE_REMOVE_SP_BLIP(pedBlip)
	ENDIF
	BOOL pedHurt = FALSE
	
	
	WHILE TRUE
		DRAW_DEBUG_TEXT_2D("do_monologue = FALSE", <<0.02, 0.1, 0>>)
			
			
		//We have broken away, so wait until the ped can be cleaned up.
		IF show_debug_text
			DRAW_DEBUG_TEXT_2D("WAITING TO CLEAN UP", <<0.02, 0.1, 0>>)
		ENDIF
		IF NOT IS_ENTITY_ALIVE(pedSpecial)
			//Ped is dead, set flag and finish the script.
			CPRINTLN(DEBUG_AMBIENT, "[SP] Ped has died, terminating script.")
			IF NOT immortal
				SET_SPECIALPED_AS_DEAD(GET_SPECIAL_PED_FROM_SCRIPT_NAME(GET_THIS_SCRIPT_NAME()))
			ENDIF
			SCRIPT_CLEANUP()
		ENDIF
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSpecial, PLAYER_PED_ID()) AND NOT pedHurt
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSpecial, FALSE)
			CLEAR_PED_TASKS(pedSpecial)
			TASK_SMART_FLEE_PED(pedSpecial, PLAYER_PED_ID(), 100.0, -1)
			SET_PED_KEEP_TASK(pedSpecial, TRUE)
			pedHurt = TRUE
		ENDIF
		
		IF ((IS_ENTITY_OCCLUDED(pedSpecial)) AND (GET_DISTANCE_BETWEEN_ENTITIES(pedSpecial, PLAYER_PED_ID()) > 50.0) AND NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE())
			//If we cannot be seen, get rid.
			CPRINTLN(DEBUG_AMBIENT, "[SP] Ped is out of range and offscreen, terminating script.")
			SCRIPT_CLEANUP(TRUE)
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
