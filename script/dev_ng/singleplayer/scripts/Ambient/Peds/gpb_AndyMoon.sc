// gpb_AndyMoon.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the player when he leaves.

// Andy Moon is the apocalyptic Hippy
USING "GenericBrain/Generic_Brain_Header.sch"

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_Hippie_01
	txtPedMissionID = "PBAM"
	txtPedVoiceID = "ANDYMOON"
	txtBaseIdleDict = "Special_Ped@andy_moon"
	txtBaseIdleAnim = "Base"
	
	talkWithAnims = !g_bMagDemoActive

	max_conversation_offset = 15
	sInteractLabel = "PBAM_INTERACT" //swap out
	greet_michael = "PBAM_CONV_GM"
	greet_trevor = "PBAM_CONV_GT"
	greet_franklin = "PBAM_CONV_GF"
	idle_dic = "special_ped@andy_moon@base"
	into_idle_dic = "special_ped@andy_moon@intro"
	
	thisPed = ANDYMOON
	
	max_conversation_split_offsets[0] = 6
	max_conversation_split_offsets[1] = 7
	max_conversation_split_offsets[2] = 6
	max_conversation_split_offsets[3] = 6
	max_conversation_split_offsets[4] = 5
	max_conversation_split_offsets[5] = 8
	max_conversation_split_offsets[6] = 7
	max_conversation_split_offsets[7] = 6
	max_conversation_split_offsets[8] = 4
	max_conversation_split_offsets[9] = 6
	max_conversation_split_offsets[10] = 6  
	max_conversation_split_offsets[11] = 6
	max_conversation_split_offsets[12] = 6
	max_conversation_split_offsets[13] = 5
	max_conversation_split_offsets[14] = 5
	
	IF g_savedGlobals.sRandomEventData.iNumberOfDeliveredPeds  = 0
		conversation_number[CHAR_TREVOR] = 3
		andyToldYouToSeeNakedOldPeople = TRUE
	ELSE
		IF andyToldYouToSeeNakedOldPeople = TRUE
			conversation_number[CHAR_TREVOR] = 2
		ELSE
			conversation_number[CHAR_TREVOR] = 1
		ENDIF
	ENDIF

ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@andy_moon"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "andy_ig_1_p1_andhisnameisdeath"
		BREAK
		CASE 2
			txtAnimToPlay = "andy_ig_1_p2_cantyouseeitman"
		BREAK
		CASE 3
			txtAnimToPlay = "ANDY_IG_1_P3_MeditateMeditateWellIMeditate"
		BREAK
		CASE 4
			txtAnimToPlay = "ANDY_IG_1_P4_GonnaDrownInFire"
		BREAK
		CASE 5
			txtAnimToPlay = "ANDY_IG_1_P5_YouLoveTheEarth"
		BREAK
		CASE 6
			txtAnimToPlay = "ANDY_IG_1_P6_ThisComplacency"
		BREAK
		CASE 7
			txtAnimToPlay = "ANDY_IG_1_P7_TheyAreComing"
		BREAK
		CASE 8
			txtAnimToPlay = "ANDY_IG_1_P8_ThisIsTheDream"
		BREAK
		CASE 9
			txtAnimToPlay = "ANDY_IG_1_P9_drink_your_own_mothers_bood"
		BREAK
		CASE 10
			txtAnimToPlay = "ANDY_IG_1_P10_MadeThemAngryPrayToYour401k"
		BREAK
		CASE 11
			txtAnimToPlay = "ANDY_IG_1_P11_DontWorryWhenTheyRoastYou"
		BREAK
		CASE 12
			txtAnimToPlay = "ANDY_IG_1_P12_WeAreGoingToBeHarvested"
		BREAK
		CASE 13
			txtAnimToPlay = "ANDY_IG_1_P13_DidYouThinkThereWouldntBeAPriceToPay"
		BREAK
		CASE 14
			txtAnimToPlay = "ANDY_IG_1_P14_YouveGotTwoChoices"
		BREAK
		CASE 15
			txtAnimToPlay = "ANDY_IG_1_P15_TheresNothingLeft"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		IF conversation_number[CHAR_TREVOR] = 1
			txt_label = "andy_ig_4_imspeakingthetruth"
		ELIF conversation_number[CHAR_TREVOR] = 2
			txt_label = "andy_ig_5_didyouseethem"
		ELSE
			txt_label = "andy_ig_3_gotothehills"
		ENDIF
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "andy_ig_2_whenwillistrike"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "andy_ig_2_b_theenergyisupbrother"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		IF conversation_number[CHAR_TREVOR] = 1
			return 10
		ELIF conversation_number[CHAR_TREVOR] = 2 
			return 6
		ELSE
			return 14
		ENDIF
	ELIF (ePlayer = CHAR_MICHAEL)
		return 4
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 4
ENDFUNC



USING "GenericBrain/Generic_Brain.sch"
