USING "GenericBrain/Generic_Brain_Header.sch"
USING "randomchar_public.sch"

FUNC BOOL IS_TONYA_1_ACTIVE_AND_NOT_COMPLETED()
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_1].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_1].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL IS_TONYA_AVAILABLE_AS_SPECIAL_PED()
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_3].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_3].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		CPRINTLN(DEBUG_AMBIENT, "IS_TONYA_AVAILABLE_AS_SPECIAL_PED - TONYA 3 ACTIVE AND NOT COMPLETED - FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_4].rcFlags, ENUM_TO_INT(RC_FLAG_ACTIVATED))
	AND NOT IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_4].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		CPRINTLN(DEBUG_AMBIENT, "IS_TONYA_AVAILABLE_AS_SPECIAL_PED - TONYA 4 ACTIVE AND NOT COMPLETED - FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_5].rcFlags, ENUM_TO_INT(RC_FLAG_COMPLETED))
		CPRINTLN(DEBUG_AMBIENT, "IS_TONYA_AVAILABLE_AS_SPECIAL_PED - TONYA 5 COMPLETED - FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_1].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
	OR IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_2].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
	OR IS_BIT_SET(g_savedGlobals.sRandomChars.savedRC[RC_TONYA_5].rcFlags, ENUM_TO_INT(RC_FLAG_READY_TO_PLAY))
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "IS_TONYA_AVAILABLE_AS_SPECIAL_PED - FALSE")
	RETURN FALSE
ENDFUNC


PROC GENERIC_BRAIN_CUSTOM_INIT()
	
	ePedModel = IG_TONYA
	txtPedMissionID = "TOW"
	txtPedVoiceID = "TONYA"
	txtBaseIdleDict = "special_ped@tonya"
	txtBaseIdleAnim = "Base"
	talkWithAnims = TRUE
	max_conversation_offset = 6
	
	IF IS_TONYA_1_ACTIVE_AND_NOT_COMPLETED()
		sInteractLabel = ""
	ELSE
		sInteractLabel = "PBTS_INTERACT"
	ENDIF

	greet_michael = "TONYA_MIC"
	greet_trevor = "TONYA_TRE"
	greet_franklin = ""
	idle_dic = "special_ped@tonya@base"
	into_idle_dic = "special_ped@tonya@intro"
	immortal = TRUE 
	
	thisPed = TONYA
	
	//canInteract = FALSE
	createBlip = FALSE
	
	max_conversation_split_offsets[0] = 4
	max_conversation_split_offsets[1] = 3
	max_conversation_split_offsets[2] = 3
	max_conversation_split_offsets[3] = 3
	max_conversation_split_offsets[4] = 3
	max_conversation_split_offsets[5] = 4
	
ENDPROC

FUNC BOOL IS_SCRIPT_ALLOWED_TO_RUN()
      IF (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
	  OR NOT IS_TONYA_AVAILABLE_AS_SPECIAL_PED()
            RETURN FALSE
      ENDIF
      RETURN TRUE
ENDFUNC


PROC GENERIC_BRAIN_ON_CREATE()
	
ENDPROC 

PROC GENERIC_BRAIN_ON_DESTROY()
	
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
    UNUSED_PARAMETER(txtAnimToPlay)
    txtAnimRoot = "Special_Ped@Tonya"
      
      SWITCH (iConvoNum)
            CASE 1
                  txtAnimToPlay = "CMON_LET_OLE_TONYA"
            BREAK
            CASE 2
                  txtAnimToPlay = "COME_OVER_HERE"
            BREAK
            CASE 3
                  txtAnimToPlay = "HEY_HEY"
            BREAK
            CASE 4
                  txtAnimToPlay = "HEY_CRACKA"
            BREAK
            CASE 5
                  txtAnimToPlay = "WHAT_CHU_DOIN"
            BREAK
			CASE 6
                  txtAnimToPlay = "ANYBODY_WANNA"
            BREAK

      ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
      enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
      IF (ePlayer = CHAR_TREVOR)
            txt_label = "CONVO_TREVOR_WHAT_ARE_YOU_ON"
      ELIF (ePlayer = CHAR_MICHAEL)
            txt_label = "CONVO_MICHAEL_LET_ME_ASK_YOU_SOMTHING"
	  ELSE
	  		PRINTLN("PLAYING AS FRANKLIN")
      ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
      enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
      IF (ePlayer = CHAR_TREVOR)
            return 9
      ELIF (ePlayer = CHAR_MICHAEL)
            return 6
      ENDIF
      //ELSE (ePlayer = CHAR_FRANKLIN)
      return -1 //this should not get called?
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
