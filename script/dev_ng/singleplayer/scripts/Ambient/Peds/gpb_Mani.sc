// gpb_Baygor.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

// Baygor is the Espilonist Member
USING "GenericBrain/Generic_Brain_Header.sch"

/// PURPOSE:
///    Any one-time setup the hero needs. In this case, adding people to the conversation, setting up a model, etc.
PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_MANI
	txtPedMissionID = "PBMN"
	txtPedVoiceID = "MANI"
	txtBaseIdleDict = "Special_Ped@mani"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBMN_INTERACT"
	greet_michael = "PBMN_CONV_GM"
	greet_trevor = "PBMN_CONV_GT"
	greet_franklin = "PBMN_CONV_GF"
	idle_dic = "special_ped@mani@base"
	into_idle_dic = "special_ped@mani@intro"
	
	thisPed = MANI
	
	max_conversation_split_offsets[0] = 6
	max_conversation_split_offsets[1] = 6
	max_conversation_split_offsets[2] = 6
	max_conversation_split_offsets[3] = 6
	max_conversation_split_offsets[4] = 7
	max_conversation_split_offsets[5] = 5
	max_conversation_split_offsets[6] = 6
	max_conversation_split_offsets[7] = 7
	
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	
	txtAnimRoot = "Special_Ped@mani"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "NoTengoUnPlan"
		BREAK
		CASE 2
			txtAnimToPlay = "AyeProblem"
		BREAK
		CASE 3
			txtAnimToPlay = "SoyUnMexicanoHumilde"
		BREAK
		CASE 4
			txtAnimToPlay = "SoloSoyUnActorMexicano"
		BREAK
		CASE 5
			txtAnimToPlay = "YaceAquiEstoyNomas"
		BREAK
		CASE 6
			txtAnimToPlay = "NosHablosMisHermanos"
		BREAK
		CASE 7
			txtAnimToPlay = "SoyUnActor"
		BREAK
		CASE 8
			txtAnimToPlay = "MiranmeMiranme"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "CONVO_Trevor_WhaHellHolaAmigo"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "CONVO_Michael_HeyBroKeepFighting"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "CONVO_Franklin_HeyWhatsCrackinMan"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 10
	ELIF (ePlayer = CHAR_MICHAEL)
		return 3
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 5
ENDFUNC


USING "GenericBrain/Generic_Brain.sch"
