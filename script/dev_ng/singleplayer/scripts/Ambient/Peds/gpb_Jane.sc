// gpb_Jane.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the player when he leaves.

// Jane is the Children of the Mountain member.
USING "GenericBrain/Generic_Brain_Header.sch"

STRING sClipSetOverride
BOOL overrideLoaded = FALSE

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_F_Y_COMJane
	txtPedMissionID = "PBJA"
	txtPedVoiceID = "JANE"
	txtBaseIdleDict = "Special_Ped@jane"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 7
	sInteractLabel = "PBJA_INTERACT"
	greet_michael = "PBJA_CONV_GM"
	greet_trevor = "PBJA_CONV_GT"
	greet_franklin = "PBJA_CONV_GF"
	idle_dic = "special_ped@jane@base"
	into_idle_dic = "special_ped@jane@intro"
	sClipSetOverride = "move_f@hurry@b"
	thisPed = JANE
	
	max_conversation_split_offsets[0] = 5
	max_conversation_split_offsets[1] = 4
	max_conversation_split_offsets[2] = 5
	max_conversation_split_offsets[3] = 5
	max_conversation_split_offsets[4] = 7
	max_conversation_split_offsets[5] = 6
	max_conversation_split_offsets[6] = 6
	
ENDPROC

FUNC BOOL IS_SCRIPT_ALLOWED_TO_RUN()
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GENERIC_BRAIN_ON_CREATE()
	REQUEST_ANIM_DICT(sClipSetOverride)
	WHILE NOT HAS_ANIM_DICT_LOADED(sClipSetOverride)
		WAIT(0)
	ENDWHILE
	overrideLoaded = TRUE
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		SET_PED_PROP_INDEX(pedSpecial, ANCHOR_EYES, 0, 0)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_DESTROY()
	IF overrideLoaded = TRUE
		IF HAS_ANIM_DICT_LOADED(sClipSetOverride)
			REMOVE_ANIM_DICT(sClipSetOverride)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PED_FLEE_WHEN_BUMPED()
	return TRUE
ENDFUNC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		SET_PED_MOVEMENT_CLIPSET(pedSpecial, sClipSetOverride)
	ENDIF 
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@jane"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "WereNotACult"
		BREAK
		CASE 2
			txtAnimToPlay = "YesIKnow"
		BREAK
		CASE 3
			txtAnimToPlay = "ItsTimeToUtilise"
		BREAK
		CASE 4
			txtAnimToPlay = "DontBelieveWhatOthers"
		BREAK
		CASE 5
			txtAnimToPlay = "BrotherAdrianHasShown"
		BREAK
		CASE 6
			txtAnimToPlay = "IHearYou"
		BREAK
		CASE 7
			txtAnimToPlay = "ItsTimeTo"
		BREAK
		
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "ImAnActualized"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "IAmWellonTheWay"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "EverythingIsUp"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 6
	ELIF (ePlayer = CHAR_MICHAEL)
		return 5
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 10
ENDFUNC


USING "GenericBrain/Generic_Brain.sch"
