// gpb_Baygor.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

// Baygor is the Espilonist Member
USING "GenericBrain/Generic_Brain_Header.sch"
USING "flow_help_public.sch"

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_Baygor
	txtPedMissionID = "PBBY"
	txtPedVoiceID = "BAYGOR"
	txtBaseIdleDict = "Special_Ped@baygor"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBBY_INTERACT" //swap out
	greet_michael = "PBBY_CONV_GM"
	greet_trevor = "PBBY_CONV_GT"
	greet_franklin = "PBBY_CONV_GF"
	idle_dic = "special_ped@baygor@base"
	into_idle_dic = "special_ped@baygor@intro"
	
	thisPed = BAYGOR
	
	max_conversation_split_offsets[0] = 8
	max_conversation_split_offsets[1] = 9
	max_conversation_split_offsets[2] = 10
	max_conversation_split_offsets[3] = 7
	max_conversation_split_offsets[4] = 8
	max_conversation_split_offsets[5] = 12
	max_conversation_split_offsets[6] = 9
	max_conversation_split_offsets[7] = 10
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_DONATED_5000) //Has Michael donated 5000? If so, say something else.
		conversation_number[CHAR_MICHAEL] = 2
	ENDIF
	
ENDPROC
OBJECT_INDEX pamphlet

PROC GENERIC_BRAIN_ON_CREATE()
	pamphlet = CREATE_OBJECT(PROP_CS_PAMPHLET_01, GET_ENTITY_COORDS(pedSpecial))
	ATTACH_ENTITY_TO_ENTITY(pamphlet, pedSpecial, GET_PED_BONE_INDEX(pedSpecial, BONETAG_PH_R_HAND), << 0.0, 0.0, 0.0 >>, << 0.0, 0.0, 0.0 >>)
	SET_PED_PROP_INDEX(pedSpecial, ANCHOR_EYES, 0, 0)
ENDPROC

PROC GENERIC_BRAIN_ON_DESTROY()
	IF DOES_ENTITY_EXIST(pamphlet)
		DELETE_OBJECT(pamphlet)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	IF DOES_ENTITY_EXIST(pamphlet)
		DETACH_ENTITY(pamphlet)
	ENDIF
ENDPROC

FUNC BOOL PED_FLEE_WHEN_BUMPED()
	return TRUE
ENDFUNC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	
	txtAnimRoot = "Special_Ped@Baygor"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "Ask_You_A_Question"
		BREAK
		CASE 2
			txtAnimToPlay = "You_Can_Ignore_Me"
		BREAK
		CASE 3
			txtAnimToPlay = "Trees_Can_Talk"
		BREAK
		CASE 4
			txtAnimToPlay = "We_Are_All_Related"
		BREAK
		CASE 5
			txtAnimToPlay = "Do_You_Want_To_Be_Happy"
		BREAK
		CASE 6
			txtAnimToPlay = "Salvation_Comes_At_A_Price"
		BREAK
		CASE 7
			txtAnimToPlay = "Do_You_Want_Happiness"
		BREAK
		CASE 8
			txtAnimToPlay = "Im_An_Epsilonist"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "How_You_Doin"
	ELIF (ePlayer = CHAR_MICHAEL)
		IF conversation_number[CHAR_MICHAEL] = 2
			txt_label = "Hey_How_You_Doing2" //We have become a cult member, say something different. 
		ELSE
			txt_label = "Hey_How_You_Doing"
		ENDIF
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "Sup_Homie_Hows_Everything"
	ENDIF
ENDPROC

PROC CONVERSATION_OVER()
	IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_EPSILON_QUESTIONNAIRE_DONE) AND NOT HAS_ONE_TIME_HELP_DISPLAYED(FHM_EPSILON_WEBSITE)
		PRINT_HELP("PBBY_ENDCHLP")
		SET_ONE_TIME_HELP_MESSAGE_DISPLAYED(FHM_EPSILON_WEBSITE)
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 5
	ELIF (ePlayer = CHAR_MICHAEL)
		IF conversation_number[CHAR_MICHAEL] = 2
			return 4
		ENDIF
		return 13
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 6
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
