USING "GenericBrain/Generic_Brain_Header.sch"

PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_M_Jesus_01
	txtPedMissionID = "PBJS"
	txtPedVoiceID = "JESSE"
	txtBaseIdleDict = "Special_Ped@jessie"
	txtBaseIdleAnim = "Base"
	talkWithAnims = TRUE
	max_conversation_offset = 15
	sInteractLabel = "PBJS_INTERACT"
	greet_michael = "PBJS_CONV_GM"
	greet_trevor = "PBJS_CONV_GT"
	greet_franklin = "PBJS_CONV_GF"
	idle_dic = "special_ped@jessie@base"
	into_idle_dic = "special_ped@jessie@intro"
	
	thisPed = JESSE
	
	max_conversation_split_offsets[0] = 8
	max_conversation_split_offsets[1] = 6
	max_conversation_split_offsets[2] = 6
	max_conversation_split_offsets[3] = 8
	max_conversation_split_offsets[4] = 6
	max_conversation_split_offsets[5] = 6
	max_conversation_split_offsets[6] = 5
	max_conversation_split_offsets[7] = 7
	max_conversation_split_offsets[8] = 5
	max_conversation_split_offsets[9] = 6
	max_conversation_split_offsets[10] = 6
	max_conversation_split_offsets[11] = 3
	max_conversation_split_offsets[12] = 5
	max_conversation_split_offsets[13] = 4
	max_conversation_split_offsets[14] = 6
	
ENDPROC

PROC GENERIC_BRAIN_ON_CREATE()
	
ENDPROC 

PROC GENERIC_BRAIN_ON_DESTROY()
	
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@jessie"
	
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "jessie_ig_1_p1_heydudes"
		BREAK
		CASE 2
			txtAnimToPlay = "jessie_ig_1_p2_gottahavefaith"
		BREAK
		CASE 3
			txtAnimToPlay = "jessie_ig_1_p3_lifeiscoolbutsoisdeath"
		BREAK
		CASE 4
			txtAnimToPlay = "jessie_ig_1_p4_sentheretoclearthingsup"
		BREAK
		CASE 5
			txtAnimToPlay = "jessie_ig_1_p5_heressomthinginteresting"
		BREAK
		CASE 6
			txtAnimToPlay = "jessie_ig_1_p6_haveyoueverwondered"
		BREAK
		CASE 7
			txtAnimToPlay = "jessie_ig_1_p7_importanttomakethingsclear"
		BREAK
		CASE 8
			txtAnimToPlay = "jessie_ig_1_p8_thingsareallreallycool"
		BREAK
		CASE 9
			txtAnimToPlay = "jessie_ig_1_p9_whothehellwalks"
		BREAK
		CASE 10
			txtAnimToPlay = "jessie_ig_1_p10_theresthiskidiknow"
		BREAK
		CASE 11
			txtAnimToPlay = "jessie_ig_1_p11_canyouimagine"
		BREAK
		CASE 12
			txtAnimToPlay = "jessie_ig_1_p12_likeiusedtobe"
		BREAK
		CASE 13
			txtAnimToPlay = "jessie_ig_1_p13_likeispoketomydad"
		BREAK
		CASE 14
			txtAnimToPlay = "if_you_want_to_get_baked"
		BREAK
		CASE 15
			txtAnimToPlay = "i_love_chill_people"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		txt_label = "dadwhatthefuck"
	ELIF (ePlayer = CHAR_MICHAEL)
		txt_label = "jessie_ig_2_yeahyeahyeah"
	ELIF (ePlayer = CHAR_FRANKLIN)
		txt_label = "its_all_good_my_dads_really_happy_with_me"
	ENDIF
ENDPROC

FUNC INT GET_NUMBER_OF_CONVERSATION_SECTIONS()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF (ePlayer = CHAR_TREVOR)
		return 11
	ELIF (ePlayer = CHAR_MICHAEL)
		return 3
	ENDIF
	//ELSE (ePlayer = CHAR_FRANKLIN)
	return 16
ENDFUNC

USING "GenericBrain/Generic_Brain.sch"
