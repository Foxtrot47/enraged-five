// gpb_Superhero.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.
CONST_INT PED_BRAIN_PED_HAS_INTERACTION 		1			// There will be a prompt to talk to this ped.
CONST_INT PED_BRAIN_PED_HAS_GOODBYE_DIALOGUE	1			// Ped will say goodbye to the palyer when he leaves.

USING "GenericBrain/Generic_Brain_Header.sch"

/// PURPOSE:
///    Any one-time setup the hero needs. In this case, adding people to the conversation, setting up a model, etc.
PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = U_M_Y_IMPORAGE
	txtPedMissionID = "PBSH"
	txtPedVoiceID = "IMPORAGE"
	txtBaseIdleDict = "Special_Ped@Impotent_Rage"
	txtBaseIdleAnim = "Base"
	talkWithAnims = TRUE
	talkWithRandom = TRUE
	
	max_conversation_offset = 15
	max_random_offset = 6
	
	
	sInteractLabel = "PBSH_INTERACT"
	greet_michael = "PBSH_CONV_M1"
	greet_trevor = "PBSH_CONV_T1"
	greet_franklin = "PBSH_CONV_F1"
	idle_dic = "special_ped@Impotent_Rage@base"
	into_idle_dic = "special_ped@Impotent_Rage@intro"
	
	thisPed = SUPERHERO
	
	max_conversation_split_offsets[0] = 4
	max_conversation_split_offsets[1] = 3
	max_conversation_split_offsets[2] = 4
	max_conversation_split_offsets[3] = 3
	max_conversation_split_offsets[4] = 3
	max_conversation_split_offsets[5] = 3
	max_conversation_split_offsets[6] = 3
	max_conversation_split_offsets[7] = 4
	max_conversation_split_offsets[8] = 5
	max_conversation_split_offsets[9] = 3
	max_conversation_split_offsets[10] = 4
	max_conversation_split_offsets[11] = 4
	max_conversation_split_offsets[12] = 3
	max_conversation_split_offsets[13] = 3
	max_conversation_split_offsets[14] = 4
	
	
	max_random_split_offsets[0] = 1
	max_random_split_offsets[1] = 1
	max_random_split_offsets[2] = 1
	max_random_split_offsets[3] = 1
	max_random_split_offsets[4] = 1
	max_random_split_offsets[5] = 1
	
	starting_offset = 0
	
ENDPROC

PROC GENERIC_BRAIN_ON_CREATE()
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		PRINTSTRING("[SP] Addingt the ped Impotent to the audio mix group.") PRINTNL()
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedSpecial, "MAG_2_RAGE_PED_GROUP")
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_ON_RUNAWAY()
	IF NOT IS_ENTITY_DEAD(pedSpecial)
		PRINTSTRING("[SP] Removing the ped Impotent from the audio mix group.") PRINTNL()
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(pedSpecial)
	ENDIF
ENDPROC

PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@Impotent_Rage"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "Grr_Im_Impotent_Rage"
		BREAK
		CASE 2
			txtAnimToPlay = "Im_Really_Pretty_Mad"
		BREAK
		CASE 3
			txtAnimToPlay = "Im_Completely_Crazy_Here"
		BREAK
		CASE 4
			txtAnimToPlay = "This_Is_Just_Insanity"
		BREAK
		CASE 5
			txtAnimToPlay = "Im_So_Damn_Mad"
		BREAK
		CASE 6
			txtAnimToPlay = "Dont_Get_Me_Excited"
		BREAK
		CASE 7
			txtAnimToPlay = "Ill_Find_The_Doughnut"
		BREAK
		CASE 8
			txtAnimToPlay = "America_Im_The_Hero"
		BREAK
		CASE 9
			txtAnimToPlay = "I_Am_The_Ultimate"
		BREAK
		CASE 10
			txtAnimToPlay = "I_Will_Defeat_The"
		BREAK
		CASE 11
			txtAnimToPlay = "Really_Very_Unhappy"
		BREAK
		CASE 12
			txtAnimToPlay = "You_Cant_Stop_My"
		BREAK
		CASE 13
			txtAnimToPlay = "I_Am_White_Male"
		BREAK
		CASE 14
			txtAnimToPlay = "Youll_Not_Stop_me"
		BREAK
		CASE 15
			txtAnimToPlay = "Newest_Superhero"
		BREAK
	ENDSWITCH
ENDPROC

PROC GENERIC_GET_INTERACT_ANIM(TEXT_LABEL_63& txt_label)
	SWITCH (random_reply)
		CASE 0
			txt_label = "Im_An_Actor"
		BREAK
		CASE 1
			txt_label = "I_Wanna_Do_Hamlet"
		BREAK
		CASE 2
			txt_label = "Having_About_as_Much"
		BREAK
		CASE 3
			txt_label = "Just_Woundering_When"
		BREAK
		CASE 4
			txt_label = "Great_I_Wanted_To"
		BREAK
		CASE 5
			txt_label = "A_Tourist_Just_Asked"
		BREAK
	ENDSWITCH
	
ENDPROC


USING "GenericBrain/Generic_Brain.sch"
