

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//////////////////////////////////////////////////////////
// 				Homeless person
//		   		Kevin Wong
//
//
//				1
//			
//
//
//
//				2
//////////////////////////////////////////////////////////
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING"script_PLAYER.sch"
USING "rage_builtins.sch"
USING "LineActivation.sch"
USING "commands_brains.sch"

///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM homelessStageFlag
	homelessInit,
	homelessWaitForPlayer,
	homelessCleanup
ENDENUM

//homelessStageFlag = homelessInit


ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

INT random_int 
BOOL bAssetsLoaded = FALSE
BOOL fmake_player_gives_money = FALSE
VECTOR v_spawnpoint
VECTOR voffset_temp
REL_GROUP_HASH homelessGroup
PED_INDEX homeless_ped
PED_INDEX pedestrian

BOOL fmake_homeless_person_ask_passerby_for_money = FALSE

SEQUENCE_INDEX a_sequence_temp
///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
PROC missionCleanup()
	PRINTSTRING("homeless ended")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	missionCleanup()
ENDPROC


PROC MISSION_FAILED()
	PRINTSTRING("MIssion failed")
	missionCleanup()

ENDPROC

PROC create_homeless()
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<50.0, 50.0, 50.0>>, FALSE)

		REQUEST_MODEL(A_M_O_Tramp_01) //whore 
		REQUEST_MODEL(G_M_Y_StrPunk_01) // punter
		REQUEST_ANIM_DICT("amb@drug_dealer")
		
		WHILE NOT HAS_MODEL_LOADED(A_M_O_Tramp_01)
		OR NOT HAS_MODEL_LOADED(G_M_Y_StrPunk_01)
		OR NOT   HAS_ANIM_DICT_LOADED("amb@drug_dealer") 
			WAIT(0)
		ENDWHILE
		ADD_RELATIONSHIP_GROUP("homeless", homelessGroup)
		
	//	voffset_temp = GET_OFFSET_FROM__IN_WORLD_COORDS(homeless_ped, <<0.0,0.0,-1.0>>)
		voffset_temp.z = v_spawnpoint.z - 1.0
		homeless_ped = CREATE_PED(PEDTYPE_DEALER,A_M_O_Tramp_01, <<v_spawnpoint.x, v_spawnpoint.y,voffset_temp.z>>, 0.0)
		SET_PED_RELATIONSHIP_GROUP_HASH(homeless_ped, homelessGroup)


		
		IF random_int = 1
		
			voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(homeless_ped, <<0.8,0.8,-1.0>>)
			pedestrian = CREATE_PED(PEDTYPE_CIVFEMALE,G_M_Y_StrPunk_01, voffset_temp, 0.0)
			ADD_RELATIONSHIP_GROUP("homeless", homelessGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedestrian, homelessGroup)
			IF NOT IS_ENTITY_DEAD(homeless_ped)
				//SET_ENTITY_HEADING(homeless_ped, voffset_temp)
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp ) 
					//TASK_PLAY_ANIM_ADVANCED (NULL, "amb@pim_prover_1", "m_argue01", v_spawnpoint, << -1.519, 0.000, -0.000 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
					TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				TASK_PERFORM_SEQUENCE(homeless_ped, a_sequence_temp)
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
			ENDIF
			
		
			IF NOT IS_ENTITY_DEAD(pedestrian)
				OPEN_SEQUENCE_TASK(a_sequence_temp)
					TASK_TURN_PED_TO_FACE_COORD( NULL,  v_spawnpoint ) 
					TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_02", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				//	TASK_PLAY_ANIM()
			//		TASK_PLAY_ANIM_ADVANCED (NULL, "amb@pim_prover_1", "f_argue01", voffset_temp , << -1.519, 0.000, -0.000 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
				CLOSE_SEQUENCE_TASK(a_sequence_temp)
				TASK_PERFORM_SEQUENCE(pedestrian, a_sequence_temp)
				CLEAR_SEQUENCE_TASK(a_sequence_temp)
			ENDIF
		ENDIF
		bAssetsLoaded = TRUE
	ENDIF
	
ENDPROC


PROC make_homeless_person_ask_passerby_for_money()
	IF TIMERB() > 10000
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_spawnpoint, <<10.0, 10.0, 10.0>>, FALSE)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					// fmake_homeless_person_ask_passerby_for_money = TRUE
					SETTIMERB(0)
					IF NOT IS_ENTITY_DEAD(homeless_ped)
					
						IF random_int = 0
				//			SET_PED_RELATIONSHIP(homeless_ped,ACQUAINTANCE_TYPE_PED_HATE,RELGROUP_GANG2)
							IF fmake_player_gives_money = FALSE
								//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Spare some change?.", 4000, 1) //
							ELSE
								//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thanks again kind sir", 4000, 1)
							ENDIF
							OPEN_SEQUENCE_TASK(a_sequence_temp)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_PLAY_ANIM(NULL, "amb@drug_dealer", "beckon_03", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
							CLOSE_SEQUENCE_TASK(a_sequence_temp)
							TASK_PERFORM_SEQUENCE(homeless_ped, a_sequence_temp)
							CLEAR_SEQUENCE_TASK(a_sequence_temp)


						ELSE
							IF NOT IS_ENTITY_DEAD(pedestrian)
							//	TASK_SMART_FLEE_PED(homeless_ped, pedestrian, 50.0, 30000, TRUE) 
						//		SET_PED_RELATIONSHIP(pedestrian,ACQUAINTANCE_TYPE_PED_HATE,RELGROUP_GANG1)
						//		TASK_TURN_PED_TO_FACE_ENTITY(pedestrian, homeless_ped)
								
								TASK_TURN_PED_TO_FACE_ENTITY(homeless_ped, pedestrian )
								
								
								IF fmake_player_gives_money = FALSE
									//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Come on, give me some change. I got 5 kids to feed.", 4000, 1) //
								ELSE
									//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thanks again kind sir", 4000, 1)
								ENDIF
								TASK_WANDER_STANDARD (pedestrian)
								random_int = 0
								//SET_ENTITY_AS_NO_LONGER_NEEDED(pedestrian)
							ENDIF
						ENDIF
					ENDIF
			ENDIF
	ENDIF
ENDPROC




///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

/*	SCRIPT (PED_INDEX passed_ped)	
		WAIT(0)
		beggar_ped = passed_ped*/

	v_spawnpoint = in_coords.vec_coord[0]
	PRINTVECTOR(v_spawnpoint)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("homeless")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	
	random_int = GET_RANDOM_INT_IN_RANGE(0,2)
	missionCleanup()
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS| FORCE_CLEANUP_FLAG_RANDOM_EVENTS)			
		missionCleanup()
	ENDIF 


	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
				SWITCH ambStage
					CASE ambCanRun
						IF bAssetsLoaded = TRUE
							ambStage = (ambRunning)
						ELSE
							create_homeless()
						ENDIF
					BREAK
					CASE ambRunning
						//DO STUFF
						
						IF NOT IS_ENTITY_DEAD(homeless_ped)
							IF fmake_player_gives_money = FALSE
								IF IS_PLAYER_ACTIVATING_PED(homeless_ped)
									//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE,  SPC_LEAVE_CAMERA_CONTROL_ON)
									//EUpdate = EUpdate_RunEvent2  
									fmake_player_gives_money = TRUE
									//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "PLAYER GIVES MONEY.", 4000, 1) //
									
								ENDIF
							ENDIF
						ENDIF
							
							
						IF fmake_homeless_person_ask_passerby_for_money = FALSE
							make_homeless_person_ask_passerby_for_money()
						ENDIF
						
						

						
					BREAK
				ENDSWITCH
			ELSE
				missionCleanup()
			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
