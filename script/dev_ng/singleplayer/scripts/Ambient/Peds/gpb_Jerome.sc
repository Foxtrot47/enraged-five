// gpb_Jerome.sc
CONST_INT PED_BRAIN_PED_HAS_RANDOM_DIALOGUE 	1			// Ped will be speaking random dialogue.

// Jerome is the cousin of the preacher "from #4". #4 might mean Jane.
USING "GenericBrain/Generic_Brain_Header.sch"

/// PURPOSE:
///    Any one-time setup the hero needs. In this case, adding people to the conversation, setting up a model, etc.
PROC GENERIC_BRAIN_CUSTOM_INIT()
	ePedModel = S_M_M_StrPreach_01
	txtPedMissionID = "PBJE"
	txtPedVoiceID = "JEROME"
	txtBaseIdleDict = "Special_Ped@jerome"
	txtBaseIdleAnim = "Base"
	talkWithAnims = !g_bMagDemoActive
	max_conversation_offset = 8
	sInteractLabel = "PBJS_INTERACT"
	greet_michael = ""
	greet_trevor = ""
	greet_franklin = ""
	idle_dic = "special_ped@jerome@base"
	into_idle_dic = "special_ped@jerome@intro"
	canInteract = FALSE
	
	thisPed = JEROME
	
	max_conversation_split_offsets[0] = 9
	max_conversation_split_offsets[1] = 9
	max_conversation_split_offsets[2] = 6
	max_conversation_split_offsets[3] = 9
	max_conversation_split_offsets[4] = 6
	max_conversation_split_offsets[5] = 8
	max_conversation_split_offsets[6] = 8
	max_conversation_split_offsets[7] = 8
ENDPROC

PROC GENERIC_BRAIN_ON_CREATE()
	SET_PED_PROP_INDEX(pedSpecial, ANCHOR_EYES, 0, 0)
ENDPROC	


PROC GENERIC_BRAIN_GET_RANDOM_ANIM_NAME(INT iConvoNum, TEXT_LABEL_31& txtAnimRoot, TEXT_LABEL_63& txtAnimToPlay)
	UNUSED_PARAMETER(txtAnimRoot)
	UNUSED_PARAMETER(txtAnimToPlay)
	txtAnimRoot = "Special_Ped@jerome"
	 
	// Anim to play is based off the convo played.
	SWITCH (iConvoNum)
		CASE 1
			txtAnimToPlay = "MyBrotherTaughtMe"
		BREAK
		CASE 2
			txtAnimToPlay = "IAmYourTrueSalvation"
		BREAK
		CASE 3
			txtAnimToPlay = "WhatIsParadise"
		BREAK
		CASE 4
			txtAnimToPlay = "ListenToMe"
		BREAK
		CASE 5
			txtAnimToPlay = "MyBrotherTold"
		BREAK
		CASE 6
			txtAnimToPlay = "YouThinkYourHappy"
		BREAK
		CASE 7
			txtAnimToPlay = "ListenListen"
		BREAK
		CASE 8
			txtAnimToPlay = "IgnoreMeAtYourPeril"
		BREAK
	ENDSWITCH
ENDPROC

USING "GenericBrain/Generic_Brain.sch"
