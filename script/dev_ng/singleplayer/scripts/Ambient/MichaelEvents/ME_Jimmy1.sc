
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "RC_Helper_Functions.sch"
USING "rgeneral_include.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF
USING "replay_public.sch"
USING "RC_Threat_public.sch"
USING "family_public.sch"
USING "commands_misc.sch"
USING "taxi_functions.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ME_Jimmy1.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Jimmy had arranged to go on a date with someone he met online.
//                          As a result, he finds himself trapped in the trunk of a car
//                          owned by an overweight geek who's driving to a LARP.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// ===========================================================================================================
//		Variables / Constants etc
// ===========================================================================================================

ENUM MISSION_STATE
	MS_INIT,
	MS_DRIVE_TO_LOCATION,
	MS_RESCUE_JIMMY,
	MS_DRIVE_HOME,
	MS_RETURN_TO_JIMMY,
	MS_LOSE_COPS,
	MS_AT_HOME,
	MS_AT_HOTEL,
	MS_FAIL_DELAY
ENDENUM

ENUM MISSION_SUBSTATE
	MSS_PREPARE,
	MSS_ACTIVE_ONE,
	MSS_ACTIVE_TWO,
	MSS_ACTIVE_THREE
ENDENUM

ENUM MISSION_FAILED_REASON
	MFR_NONE,
	MFR_JIMMY_HURT,
	MFR_JIMMY_DEAD,
	MFR_JIMMY_KIDNAPPED,
	MFR_JIMMY_LEFT,
	MFR_KIDNAPPER_DIED
ENDENUM

ENUM JIMMYS_ANIMS
	JA_READY,
	JA_NULL,
	JA_ENTER_BACK,
	JA_BACK,
	JA_ENTER_LEFT,
	JA_LEFT,
	JA_LEFT_PLAYING,
	JA_EXIT_LEFT,
	JA_ENTER_RIGHT,
	JA_RIGHT,
	JA_RIGHT_PLAYING,
	JA_EXIT_RIGHT,
	JA_EXITED_SIDE,
	JA_WAIT,
	JA_EXIT_BACK,
	JA_ENTER_BACKSEAT,
	JA_PUT_JIMMY_IN_SEAT,
	JA_EXIT_VEHICLE
ENDENUM

ENUM DRIVE_CONV_STATE
	DCS_NULL,
	DCS_READY,
	DCS_PLAYING,
	DCS_FINISHED
ENDENUM

ENUM PLAYER_REL_POS
	PRP_LEFT,
	PRP_BEHIND,
	PRP_RIGHT,
	PRP_NULL
ENDENUM

CONST_INT MICHAEL_ID 0
CONST_INT JIMMY_ID 3
CONST_INT JIMMY_PHONE_ID 4
CONST_INT KIDNAPPER_ID 5
CONST_INT PASSENGER_ID 6
structPedsForConversation pedConvStruct

CONST_INT KIDNAPPERS_STUCK_TIME 5000

STRING ANIM_DICT_JIMMY = "missmej1ig_1"
INT iJimmyAnims
JIMMYS_ANIMS eJimmysAnimStage = JA_READY
STRING ANIM_ATTACH_POINT = "seat_pside_r"//"chassis_dummy"
RAGDOLL_BLOCKING_FLAGS rbfSyncedScene = RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_IMPACT_OBJECT
BOOL bExitBack = FALSE

DRIVE_CONV_STATE eDriveConvState = DCS_NULL

//CONST_INT NUM_DRIVE_CONVS 5
CONST_INT iDriveConvTimer 10000
//STRING sDriveConvs[NUM_DRIVE_CONVS]
INT iCurrentDriveConv = -1
INT iPreviousDriveConv = -1
TEXT_LABEL_23 sSceneConvLine

/*VECTOR vOnFootEndPosMichael = <<-823.5155, 181.1892, 70.6611>>
FLOAT fOnFootEndRotMichael = 129.8824
VECTOR vOnFootEndPosJimmy = <<-824.33, 180.29, 71.55>>
FLOAT fOnFootEndRotJimmy = -31.51*/
VECTOR vJimmyFinalDest = <<-814.317505,179.017578,71.159195>>
VECTOR vJimmyFinalDestRear = <<-806.181396,184.289764,71.347694>>
VECTOR vKitchen = <<-801.9036, 182.9307, 71.6055>>
VECTOR vHotel = <<-1368.278687,355.901825,63.081089>>//<<-1320.7941, 380.0252, 68.0737>>
//CAMERA_INDEX ciHomeCam
INT iCutTimer
BOOL bSkipped

/*CONST_INT NUM_START_LINES 5
CONST_INT iStartLinesTimer 10000
STRING sStartLines[NUM_START_LINES]
INT iCurrentStartLine = 3//-1	// Currently Michael only says one line when going to find Jimmy, if the other lines are wanted again, these values must change back to -1
INT iPreviousStartLine = 3//-1
*/

CONST_FLOAT CHASE_SHOUT_DIST 50.0
CONST_INT NUM_CHASE_LINES 8//5
CONST_INT iChaseLinesTimer 5000
STRING sChaseLines[NUM_CHASE_LINES]
INT iCurrentChaseLine = -1
INT iPreviousChaseLine = -1
INT iJimmyChaseTimer = -1
BOOL bHasPlayerTouchedKidnapper = FALSE
BOOL bJimmyDadLinePlayed = FALSE
BOOL bPlayNoKillLine = TRUE
BOOL bTriggerSecondPhoneCall = TRUE
BOOL bStartFirstRecording = TRUE

/*CONST_INT NUM_TIED_LINES 5
STRING sTiedLines[NUM_TIED_LINES]
INT iCurrentTiedLine = 0*/
CONST_INT TIED_LINE_TIME 15000
INT iTiedLineTimer = 0

INT iConversationTimer
INT iPreChaseLinesTimer
INT iLookBehindTimer

BOOL bTurnOnVisibleDamage = FALSE
INT iVisibleDamageTimer = -1

CONST_INT DRIVING_LINES_TIMEOUT 9000

CONST_INT NUM_CRASH_LINES 6
INT iCrashLineTimer
STRING sCrashLines[NUM_CRASH_LINES]
INT iCurrentCrashLine = 0

CONST_INT NUM_CRIME_LINES 3
INT iCrimeLineTimer
STRING sCrimeLines[NUM_CRIME_LINES]
STRING sCrimeLinesNoKill[NUM_CRIME_LINES]
INT iCurrentCrimeLine = 0

CONST_INT NUM_STILL_LINES 3
INT iStillLineTimer
INT iStillTime
CONST_INT iStillTimeout 60
STRING sStillLines[NUM_STILL_LINES]
INT iCurrentStillLine = 0

CONST_INT NUM_AIM_LINES 4
INT iAimLineTimer
STRING sAimLines[NUM_AIM_LINES]
INT iCurrentAimLine = 0

CONST_INT SHOOTING_TIMEOUT 10000
INT iShootingLineTimer = 0

CONST_INT NUM_FOOT_CONVS 12//3
INT iFootConvTimer
STRING sFootConvs[NUM_FOOT_CONVS]
INT iCurrentFootConv = 0
CONST_FLOAT fFootConvDist 30.0

VEHICLE_INDEX viPlayersVehicle
CONST_INT iCheckVehicle 1000
INT iCheckVehicleTimer = 0

BOOL bAllowKidnapperDeadConv = TRUE
BOOL bKillLineOneReady = TRUE
//BOOL bKillLineTwoReady = TRUE

VECTOR vMissionLocation = <<-2271.74, 1070.36, 198.20>>
VECTOR vHomeLocation = <<-825.503540,179.026611,70.369011>>//<<-822.573730,182.467636,70.849342>>//<<-829.0000, 171.0000, 70.0000>>
VECTOR vHotelLocation = <<-1378.4454, 367.8120, 63.0445>>//<<-1317.6356, 391.5271, 68.6091>>

CONST_FLOAT fFailDistanceFirst 800.0
CONST_FLOAT fFailDistanceSecond 200.0
FLOAT fFailDistance = fFailDistanceFirst
CONST_FLOAT fVanWarnDistanceFirst 100.0
CONST_FLOAT fVanWarnDistanceSecond 500.0
FLOAT fVanWarnDistance = fVanWarnDistanceFirst
CONST_FLOAT fVanReturnDistance 50.0
CONST_FLOAT fWarnDistance 15.0
CONST_FLOAT fReturnDistance 5.0
CONST_FLOAT fSpawnSetupDist 1400.0//600.0//850.0//1000.0//600.0//400.0//200.0

MISSION_STATE missionState = MS_INIT
MISSION_SUBSTATE missionSubstate = MSS_PREPARE

MISSION_FAILED_REASON missionFailedReason = MFR_NONE

BLIP_INDEX biObjective

PED_INDEX piJimmy
REL_GROUP_HASH rghPlayer

CONST_INT NUM_KIDNAPPERS 3
PED_INDEX piKidnapper[NUM_KIDNAPPERS]
VEHICLE_SEAT vsKidnapperSeat[NUM_KIDNAPPERS]
MODEL_NAMES mnKidnapper = A_M_M_GENFAT_01

VEHICLE_INDEX viKidnapCar
MODEL_NAMES mnKidnapCar = LANDSTALKER
VECTOR vKidnapCarPosA = <<-2271.74, 1070.36, 198.20>>
FLOAT fKidnapCarHeadingA = 135.06
VECTOR vAreaAPos1 = <<-2223.215820,1009.274719,163.511749>>
VECTOR vAreaAPos2 = <<-2226.588135,1147.933105,270.956543>> // THIS ONE ADJUSTS MAX HEIGHT
VECTOR vAreaAPos1Alt = <<-2269.846924,891.106750,192.302780>>
VECTOR vAreaAPos2Alt = <<-2161.118896,1225.726929,500.543518>>
VECTOR vAreaBPos1 = <<-2402.328857,1057.228638,189.975388>>
VECTOR vAreaBPos2 = <<-2410.820068,1005.260376,200.859146>>
VECTOR vAreaBPos1Alt = <<-2328.858887,892.164307,196.736588>>
VECTOR vAreaBPos2Alt = <<-2333.411621,1221.692627,495.647705>>
FLOAT fAreaWidth = 90.0
FLOAT fAreaWidthAlt = 200.0

BOOL bShowHomeObjective = TRUE
BOOL bShowReturnObjective = TRUE
BOOL bGoToJimmy = FALSE
BOOL bShowReturnToVanMessage = TRUE

BOOL bDoneFailDialogue

// variables for dealing with the player bursting the car tyres
BOOL bTyresBurst = FALSE
INT iTyresBurstTimer = 0
CONST_INT iTyresTime 1000

// Clear this area if the player arrives home on foot
VECTOR vCutsceneAreaPos1 = <<-826.696777,178.020920,69.187263>>
VECTOR vCutsceneAreaPos2 = <<-817.752869,185.950363,72.353897>> 
FLOAT fCutsceneAreaWidth = 7.0
VECTOR vCarMovePos = <<-826.00, 157.66, 69.06>>
FLOAT fCarMoveHead = 273.51

CONST_INT CP_MISSION_START 0
CONST_INT CP_AT_KIDNAPPER 1
CONST_INT CP_DRIVE_HOME 2
CONST_INT CP_MISSION_PASSED 3

// variables for dealing with the number of times the player has rammed the kidnappers
INT iVanRammedCount = 0
BOOL bVanRammedLastFrame = FALSE
FLOAT fPlayerVsVanClosingSpeedLastFrame = 0
INT iRamTimer = 0

// Timer to check if conversations have failed to trigger
INT iConvFailedTimer
//CONST_INT CONV_FAILED_TIME 5000

MODEL_NAMES mnRestartCar = TAILGATER

// To allow certain conversations to be interrupted by objectives
RC_CONV_RESTORE_STATE stateRestoreConversation
TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[3]
#ENDIF

// ===========================================================================================================
//		Termination
// ===========================================================================================================
/// PURPOSE:
///    Releases or deletes everything created by the mission
/// PARAMS:
///    deleteAll - True if everything should be deleted, false if things should be released
PROC RemovePedsAndVehicles(BOOL deleteAll = FALSE, BOOL bFailed = FALSE)
	IF deleteAll
		SAFE_DELETE_PED(piJimmy)
	ELSE
		IF IS_PED_UNINJURED(piJimmy) AND IS_PED_IN_GROUP(piJimmy)
			REMOVE_PED_FROM_GROUP(piJimmy)
		ENDIF
		IF bFailed
			SAFE_RELEASE_PED(piJimmy)
		ELSE
			IF IS_PED_UNINJURED(piJimmy) AND IS_ENTITY_AT_COORD(piJimmy, vHomeLocation, <<50,50,50>>)
				RELEASE_PED_TO_FAMILY_SCENE(piJimmy)
			ELSE
				SAFE_RELEASE_PED(piJimmy)
			ENDIF
		ENDIF
	ENDIF
	
	INT i = 0
	IF deleteAll
		REPEAT NUM_KIDNAPPERS i
			SAFE_DELETE_PED(piKidnapper[i])
		ENDREPEAT
	ELSE
		REPEAT NUM_KIDNAPPERS i
			SAFE_RELEASE_PED(piKidnapper[i])
		ENDREPEAT
	ENDIF
	
	SAFE_REMOVE_BLIP(biObjective)
	
	STOP_AUDIO_SCENES()
	
	IF IS_VEHICLE_OK(viKidnapCar)
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(viKidnapCar)
	ENDIF
	IF deleteAll
		SAFE_DELETE_VEHICLE(viKidnapCar)
	ELSE
		SAFE_RELEASE_VEHICLE(viKidnapCar)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up and ends the mission
PROC Mission_Cleanup(BOOL bFailed = FALSE)

	CPRINTLN(DEBUG_MISSION,"...ME_Jimmy1 Cleanup")
	
	RemovePedsAndVehicles(FALSE, bFailed)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	CLEAR_PRINTS()
	KILL_FACE_TO_FACE_CONVERSATION()
	REMOVE_ANIM_DICT(ANIM_DICT_JIMMY)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vHotelLocation-<<10,10,10>>), (vHotelLocation+<<10,10,10>>),TRUE)
	DISABLE_TAXI_HAILING(FALSE)

	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Passes the mission
PROC Mission_Passed()

	CPRINTLN(DEBUG_MISSION,"...ME_Jimmy1 Passed")

	//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

// ===========================================================================================================
//		Helper Functions
// ===========================================================================================================
/// PURPOSE:
///    Checks if it's OK to play non-essential dialogue without it overriding god text
/// RETURNS:
///    TRUE if there's no god text or if the player isn't using subtitles
FUNC BOOL IsOkToPlayDialogue()
	IF NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Suppresses the players vehicle model
PROC SUPPRESS_PLAYERS_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayerCar)
			MODEL_NAMES mnPlayerCar = GET_ENTITY_MODEL(viPlayerCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnPlayerCar, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays the player's dialogue while driving to meet Jimmy
/*PROC PlayStartLines()
	IF iCurrentStartLine < NUM_START_LINES AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEJ1_02")	// Trash the kidnappers' ~r~car.
	AND NOT IS_REPLAY_IN_PROGRESS()
		IF iPreviousStartLine = iCurrentStartLine
			iCurrentStartLine++
			iConversationTimer = GET_GAME_TIMER() + iStartLinesTimer
		ELSE
			IF GET_GAME_TIMER() > iConversationTimer
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_MICHAEL", sStartLines[iCurrentStartLine], CONV_PRIORITY_MEDIUM)
					iPreviousStartLine = iCurrentStartLine
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC*/

/// PURPOSE:
///    Plays the player's dialogue while chasing the kidnapper
PROC PlayChaseLines()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piKidnapper[0]) AND IS_VEHICLE_OK(viKidnapCar) //AND IsOkToPlayDialogue()
		IF iCurrentChaseLine = -1 AND iPreviousChaseLine = -1
			IF NOT bHasPlayerTouchedKidnapper
				bHasPlayerTouchedKidnapper = IS_ENTITY_TOUCHING_ENTITY(viKidnapCar, PLAYER_PED_ID())
			ENDIF
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viKidnapCar, PLAYER_PED_ID()) OR bHasPlayerTouchedKidnapper
			//OR (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piKidnapper[0]) < CHASE_SHOUT_DIST AND NOT IS_ENTITY_OCCLUDED(viKidnapCar))
				//HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				KILL_PHONE_CONVERSATION()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(pedConvStruct, CHAR_JIMMY, "MEJ1AUD", "MEJ1_DAD", CONV_PRIORITY_MEDIUM)
				/*BOOL bConvPlayed = FALSE
				IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piKidnapper[0]) > CHASE_SHOUT_DIST) OR bHasPlayerTouchedKidnapper
					bConvPlayed = PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_RAM", "MEJ1_RAM_3", CONV_PRIORITY_MEDIUM)
				ELSE
					bConvPlayed = CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_SEES",  CONV_PRIORITY_MEDIUM)
				ENDIF
				IF bConvPlayed*/
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_RAM", "MEJ1_RAM_3", CONV_PRIORITY_MEDIUM)
					bTriggerSecondPhoneCall = TRUE
					iCurrentChaseLine++
					iConversationTimer = GET_GAME_TIMER()	// want the next conversation to start immediately
					// want the kidnapper to speed up the first time you damage him
					TASK_VEHICLE_MISSION_PED_TARGET(piKidnapper[0], viKidnapCar, PLAYER_PED_ID(), MISSION_FLEE, 25.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 0.0, REPLAY_IMPORTANCE_HIGH)	// Record The player first attacking the kidnappers
					//SET_VEHICLE_FORWARD_SPEED(viKidnapCar, 25.0)
					//SET_DRIVE_TASK_CRUISE_SPEED(piKidnapper[0], 50.0)
					// Make Jimmy turn around in the boot
					eJimmysAnimStage = JA_ENTER_BACK
					fFailDistance = fFailDistanceSecond
					fVanWarnDistance = fVanWarnDistanceSecond
				ENDIF
			ELIF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piKidnapper[0]) < CHASE_SHOUT_DIST AND NOT IS_ENTITY_OCCLUDED(viKidnapCar))
				IF GET_GAME_TIMER() > iPreChaseLinesTimer
					BOOL bConvDone = FALSE
					IF IS_MESSAGE_BEING_DISPLAYED()
						bConvDone = CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_SEES",  CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					ELSE
						bConvDone = CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_SEES",  CONV_PRIORITY_MEDIUM)
					ENDIF
					
					IF bStartFirstRecording
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 0.0, REPLAY_IMPORTANCE_HIGH)	// Record the player attacking the kidnappers for the first time
						bStartFirstRecording = FALSE
					ENDIF
					
					IF bConvDone
						iPreChaseLinesTimer = GET_GAME_TIMER() + 10000
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bTriggerSecondPhoneCall
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(pedConvStruct, CHAR_JIMMY, "MEJ1AUD", "MEJ1_DAD", CONV_PRIORITY_MEDIUM)
						bTriggerSecondPhoneCall = FALSE
					ENDIF
				ENDIF
			ELIF iCurrentChaseLine < NUM_CHASE_LINES AND (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piKidnapper[0]) < CHASE_SHOUT_DIST)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEJ1_02")	// Trash the kidnapper's ~r~van.
				IF !bJimmyDadLinePlayed
				AND CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_DSPOT", CONV_PRIORITY_MEDIUM)
					CPRINTLN(DEBUG_MISSION, "PlayChaseLines: Triggering the new Dad! Dad! line")
					iConversationTimer = GET_GAME_TIMER() + iChaseLinesTimer
					bJimmyDadLinePlayed = TRUE
					iJimmyChaseTimer = GET_GAME_TIMER() + 10000
				ELIF iPreviousChaseLine = iCurrentChaseLine
					iCurrentChaseLine++
					iConversationTimer = GET_GAME_TIMER() + iChaseLinesTimer
				ELSE
					IF GET_GAME_TIMER() > iConversationTimer
						//IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", sChaseLines[iCurrentChaseLine], CONV_PRIORITY_MEDIUM)
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_MRAM", sChaseLines[iCurrentChaseLine], CONV_PRIORITY_MEDIUM)
							iPreviousChaseLine = iCurrentChaseLine
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PlayJimmyChaseLines()
	IF iJimmyChaseTimer > 0 AND GET_GAME_TIMER() > iJimmyChaseTimer
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
			IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) < CHASE_SHOUT_DIST
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEJ1_02")	// Trash the kidnapper's ~r~van.
				IF bPlayNoKillLine
					IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_DONT", CONV_PRIORITY_MEDIUM)
						iJimmyChaseTimer = GET_GAME_TIMER() + 10000
						bPlayNoKillLine = FALSE
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_CALL", CONV_PRIORITY_MEDIUM)
						iJimmyChaseTimer = GET_GAME_TIMER() + 10000
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue where Jimmy shouts about being tied up in the back of the car
PROC PlayJimmyTiedLines()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
		//IF iCurrentTiedLine < NUM_TIED_LINES
			IF GET_GAME_TIMER() > iTiedLineTimer
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
					//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_TIED", sTiedLines[iCurrentTiedLine], CONV_PRIORITY_MEDIUM)
					IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HERE", CONV_PRIORITY_MEDIUM)
						//iCurrentTiedLine++
						iTiedLineTimer = GET_GAME_TIMER() + TIED_LINE_TIME
					ENDIF
				ENDIF
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads the models used by the mission
PROC LoadMissionModels()
	REQUEST_LOAD_MODEL(mnKidnapper)
	REQUEST_LOAD_MODEL(GET_NPC_PED_MODEL(CHAR_JIMMY))
	REQUEST_LOAD_MODEL(mnKidnapCar)
ENDPROC

FUNC PLAYER_REL_POS DetectPlayersRelativeSide()
	IF IS_VEHICLE_OK(viKidnapCar) AND IS_ENTITY_IN_RANGE_ENTITY(viKidnapCar, PLAYER_PED_ID(), 10.0)
		VECTOR vPlayersRelativePos
		vPlayersRelativePos = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viKidnapCar, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		
		IF vPlayersRelativePos.y < -1
			RETURN PRP_BEHIND
		ELIF vPlayersRelativePos.x < -1
			RETURN PRP_RIGHT
		ELIF vPlayersRelativePos.x > 1
			RETURN PRP_LEFT
		ELSE
			RETURN PRP_BEHIND
		ENDIF
	ENDIF
	
	RETURN PRP_NULL
ENDFUNC

PROC ControlJimmysAnims()
	IF IS_PED_UNINJURED(piJimmy) AND IS_VEHICLE_OK(viKidnapCar)
		SWITCH eJimmysAnimStage
			CASE JA_READY
				IF bExitBack OR NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR)
					eJimmysAnimStage = JA_ENTER_BACKSEAT
				ENDIF
			BREAK
			CASE JA_ENTER_BACK
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_back", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				bTurnOnVisibleDamage = FALSE
				iVisibleDamageTimer = -1
				eJimmysAnimStage = JA_EXITED_SIDE
			BREAK
			CASE JA_BACK
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF bExitBack OR NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR)
						eJimmysAnimStage = JA_EXIT_BACK
					ELSE
						SWITCH DetectPlayersRelativeSide()
							CASE PRP_LEFT
								eJimmysAnimStage = JA_ENTER_LEFT
							BREAK
							CASE PRP_RIGHT
								eJimmysAnimStage = JA_ENTER_RIGHT
							BREAK
						ENDSWITCH
					ENDIF
				/*ELSE
					iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,TRUE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
					IF IS_VEHICLE_OK(viKidnapCar)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
					ENDIF
					TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "back", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)*/
					//eJimmysAnimStage = JA_NULL
				ENDIF
			BREAK
			
			CASE JA_ENTER_LEFT
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				eJimmysAnimStage = JA_LEFT
			BREAK
			CASE JA_LEFT
				//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.9
						iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,TRUE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
						TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
						eJimmysAnimStage = JA_LEFT_PLAYING
					ENDIF
				ENDIF
			BREAK
			CASE JA_LEFT_PLAYING
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF bExitBack OR NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR)
						eJimmysAnimStage = JA_EXIT_LEFT
					ELSE
						SWITCH DetectPlayersRelativeSide()
							CASE PRP_BEHIND
							CASE PRP_RIGHT
								eJimmysAnimStage = JA_EXIT_LEFT
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			BREAK
			CASE JA_EXIT_LEFT
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "exit_left", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				IF bExitBack OR (IS_VEHICLE_OK(viKidnapCar) AND NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR))
					eJimmysAnimStage = JA_WAIT
				ELSE
					eJimmysAnimStage = JA_EXITED_SIDE
				ENDIF
			BREAK
			
			CASE JA_ENTER_RIGHT
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_right", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				eJimmysAnimStage = JA_RIGHT
			BREAK
			CASE JA_RIGHT
				//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.9
						iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,TRUE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
						TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "right", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
						eJimmysAnimStage = JA_RIGHT_PLAYING
					ENDIF
				ENDIF
			BREAK
			CASE JA_RIGHT_PLAYING
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF bExitBack OR NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR)
						eJimmysAnimStage = JA_EXIT_RIGHT
					ELSE
						SWITCH DetectPlayersRelativeSide()
							CASE PRP_BEHIND
							CASE PRP_LEFT
								eJimmysAnimStage = JA_EXIT_RIGHT
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			BREAK
			CASE JA_EXIT_RIGHT
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "exit_right", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				IF bExitBack OR NOT IS_VEHICLE_WINDOW_INTACT(viKidnapCar, SC_WINDSCREEN_REAR)
					eJimmysAnimStage = JA_WAIT
				ELSE
					eJimmysAnimStage = JA_EXITED_SIDE
				ENDIF
			BREAK
			
			CASE JA_EXITED_SIDE
				//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.9
						IF iVisibleDamageTimer < 0
							bTurnOnVisibleDamage = TRUE
							iVisibleDamageTimer = GET_GAME_TIMER() + 1000
						ENDIF
						iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,TRUE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
						TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "back", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
						eJimmysAnimStage = JA_BACK
					ENDIF
				ENDIF
			BREAK
			
			CASE JA_WAIT
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.7
						eJimmysAnimStage = JA_EXIT_BACK
					ENDIF
				ENDIF
			BREAK
			
			CASE JA_EXIT_BACK
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "exit_back", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				eJimmysAnimStage = JA_ENTER_BACKSEAT
			BREAK
			CASE JA_ENTER_BACKSEAT
//				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
//					iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
//					SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
//					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
//					IF IS_VEHICLE_OK(viKidnapCar)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, "chassis_dummy"))
//					ENDIF
//					TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_backseat", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
//					eJimmysAnimStage = JA_PUT_JIMMY_IN_SEAT
//				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.95
						CPRINTLN(DEBUG_MISSION, "ControlJimmysAnims: JA_ENTER_BACKSEAT: Updating anim on phase ", GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims))
						iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						SET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims, 0.0)
						SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
						IF IS_VEHICLE_OK(viKidnapCar)
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
						ENDIF
						TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_backseat", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
						eJimmysAnimStage = JA_PUT_JIMMY_IN_SEAT
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "ControlJimmysAnims: JA_ENTER_BACKSEAT: Updating anim on scene completed")
					iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,FALSE)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,TRUE)
					IF IS_VEHICLE_OK(viKidnapCar)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
					ENDIF
					TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "enter_backseat", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
					eJimmysAnimStage = JA_PUT_JIMMY_IN_SEAT
				ENDIF
			BREAK
			CASE JA_PUT_JIMMY_IN_SEAT
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iJimmyAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims) >= 0.85
						CPRINTLN(DEBUG_MISSION, "ControlJimmysAnims: JA_PUT_JIMMY_IN_SEAT: Warped Jimmy into seat at phase ", GET_SYNCHRONIZED_SCENE_PHASE(iJimmyAnims))
						//SET_ENTITY_COLLISION(piJimmy, TRUE)
						//TASK_WARP_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_BACK_RIGHT)
						STOP_SYNCHRONIZED_ENTITY_ANIM(piJimmy, SLOW_BLEND_OUT, TRUE)
						SAFE_SET_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_BACK_RIGHT)
						eJimmysAnimStage = JA_EXIT_VEHICLE
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "ControlJimmysAnims: JA_PUT_JIMMY_IN_SEAT: Warped Jimmy into seat on scene completed")
					SET_ENTITY_COLLISION(piJimmy, TRUE)
					//TASK_WARP_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_BACK_RIGHT)
					SAFE_SET_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_BACK_RIGHT)
					eJimmysAnimStage = JA_EXIT_VEHICLE
				ENDIF
			BREAK
			CASE JA_EXIT_VEHICLE
				IF IS_PED_IN_VEHICLE(piJimmy, viKidnapCar) AND bExitBack
					// Only get out if player hasn't got in - B*1225919
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viKidnapCar)
						TASK_LEAVE_ANY_VEHICLE(piJimmy, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 0.0, REPLAY_IMPORTANCE_HIGH)	// Record Jimmy leaving the car
					ENDIF
					eJimmysAnimStage = JA_NULL
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes a ped play animations to make them look behind in the car
/// PARAMS:
///    PedIndex - the ped who will look behind
///    iTime - how long to look behind for
///    Animflags - Any anim flags that are needed
PROC TASK_PLAY_LOOK_BEHIND_ANIM(PED_INDEX PedIndex, INT iTime = -1, ANIMATION_FLAGS Animflags = AF_DEFAULT)

	IF DOES_ENTITY_EXIST(PedIndex)
		IF NOT IS_ENTITY_DEAD(PedIndex)
	
			STRING 			sEnterAnim
			STRING 			sLoopAnim
			STRING 			sExitAnim
			SEQUENCE_INDEX 	SequenceIndex
			VEHICLE_SEAT	eSeat
			
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PedIndex)
				VEHICLE_INDEX viPed = GET_VEHICLE_PED_IS_IN(PedIndex)
				IF IS_VEHICLE_OK(viPed)
					eSeat = GET_PED_VEHICLE_SEAT(PedIndex, viPed)
					SWITCH eSeat
						CASE VS_DRIVER
						CASE VS_BACK_LEFT
							sEnterAnim 	= "incar_lookbehind_enter_driver"
							sLoopAnim	= "incar_lookbehind_idle_driver"
							sExitAnim	= "incar_lookbehind_exit_driver"
						BREAK
						CASE VS_FRONT_RIGHT
						CASE VS_BACK_RIGHT
							sEnterAnim 	= "incar_lookbehind_enter_passenger"
							sLoopAnim	= "incar_lookbehind_idle_passenger"
							sExitAnim	= "incar_lookbehind_exit_passenger"
						BREAK
					ENDSWITCH
					
					CLEAR_SEQUENCE_TASK(SequenceIndex)
					OPEN_SEQUENCE_TASK(SequenceIndex)
						TASK_PLAY_ANIM(NULL, "missfam3", sEnterAnim, 	SLOW_BLEND_IN, 		NORMAL_BLEND_OUT, 	-1, 	AF_UPPERBODY | Animflags) 
						TASK_PLAY_ANIM(NULL, "missfam3", sLoopAnim, 	NORMAL_BLEND_IN, 	NORMAL_BLEND_OUT, 	iTime, 	AF_UPPERBODY | Animflags | AF_LOOPING) 
						TASK_PLAY_ANIM(NULL, "missfam3", sExitAnim, 	NORMAL_BLEND_IN, 	SLOW_BLEND_OUT, 	-1, 	AF_UPPERBODY | Animflags)				
					CLOSE_SEQUENCE_TASK(SequenceIndex)
					TASK_PERFORM_SEQUENCE(PedIndex, SequenceIndex)
					CLEAR_SEQUENCE_TASK(SequenceIndex)
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC MAKE_KIDNAPPER_LOOK_AT_PLAYER()
	IF HAS_ANIM_DICT_LOADED("missfam3")
		IF GET_GAME_TIMER() > iLookBehindTimer
			VECTOR vBehindKidnappers = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viKidnapCar, <<0,-20,0>>)
			IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vBehindKidnappers, 20.0)
				INT iPed = GET_RANDOM_INT_IN_RANGE(1, COUNT_OF(piKidnapper))
				INT iLookBehindDelay = GET_RANDOM_INT_IN_RANGE(1, 6)
				iLookBehindDelay*=1000
				
				TASK_PLAY_LOOK_BEHIND_ANIM(piKidnapper[iPed], iLookBehindDelay)
				
				iLookBehindTimer = GET_GAME_TIMER() + iLookBehindDelay
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Local replacement for CREATE_NPC_PED functions so you can specify a specific outfit
/// RETURNS:
///    TRUE when spawned
FUNC BOOL SPAWN_JIMMY(PED_INDEX &JimmyPed, VECTOR vCoords, FLOAT fHeading = 0.0, BOOL bCleanupModel = TRUE)

	// Load the required model
	MODEL_NAMES model = GET_NPC_PED_MODEL(CHAR_JIMMY)
	REQUEST_MODEL(model)

	IF HAS_MODEL_LOADED(model)
		// Delete ped in case of weirdness- should be OK though
		SAFE_DELETE_PED(JimmyPed)

		JimmyPed = CREATE_PED(PEDTYPE_MISSION, model, vCoords, fHeading, FALSE, FALSE)
		
		// Set up outfit
		SET_PED_COMPONENT_VARIATION(JimmyPed, PED_COMP_TORSO, 6, 0)
		SET_PED_COMPONENT_VARIATION(JimmyPed, PED_COMP_LEG, 0, 0)
		SET_PED_COMPONENT_VARIATION(JimmyPed, PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(JimmyPed, PED_COMP_BERD, 3, 0)	// No accessories
	//	StoreAsGlobalFriend(TraceyPed, CHAR_JIMMY) // The CREATE_NPC_PED func does this but it's not necessary AFAIK
            
		IF bCleanupModel
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
		ENDIF
            
		RETURN TRUE
    ENDIF

    RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Creates Jimmy, the kidnapper and the kidnapper's van
PROC SpawnMissionSetup()
	IF IS_VEHICLE_OK(viKidnapCar)
		INT i = 0
		REPEAT NUM_KIDNAPPERS i
			IF NOT IS_PED_UNINJURED(piKidnapper[i])
				IF HAS_MODEL_LOADED(mnKidnapper)
					piKidnapper[i] = CREATE_PED_INSIDE_VEHICLE(viKidnapCar, PEDTYPE_CIVMALE, mnKidnapper, vsKidnapperSeat[i])
					SET_PED_COMPONENT_VARIATION(piKidnapper[i], INT_TO_ENUM(PED_COMPONENT,0), i, i, 0) //(head)
					SET_PED_COMPONENT_VARIATION(piKidnapper[i], INT_TO_ENUM(PED_COMPONENT,2), i, i, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(piKidnapper[i], INT_TO_ENUM(PED_COMPONENT,3), i, i, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(piKidnapper[i], INT_TO_ENUM(PED_COMPONENT,4), i, i, 0) //(lowr)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piKidnapper[i], TRUE)
					SET_ENTITY_VISIBLE(piKidnapper[i], FALSE)
					SET_PED_FLEE_ATTRIBUTES(piKidnapper[i], FA_USE_VEHICLE, FALSE)
					SET_PED_FLEE_ATTRIBUTES(piKidnapper[i], FA_DISABLE_HANDS_UP, FALSE)
					SET_PED_FLEE_ATTRIBUTES(piKidnapper[i], FA_FORCE_EXIT_VEHICLE, TRUE)
					SET_PED_COMBAT_ATTRIBUTES(piKidnapper[i], CA_ALWAYS_FLEE, TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(piKidnapper[i], TRUE)
					IF i = 0
						ADD_PED_FOR_DIALOGUE(pedConvStruct, KIDNAPPER_ID, piKidnapper[i], "MEJimmyKidnapper", TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_UNINJURED(piJimmy)
			IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_JIMMY))
			AND HAS_ANIM_DICT_LOADED(ANIM_DICT_JIMMY) // B*1260858
				//CREATE_NPC_PED_INSIDE_VEHICLE(piJimmy, CHAR_JIMMY, viKidnapCar, VS_BACK_LEFT)
				SPAWN_JIMMY(piJimmy, vKidnapCarPosA, fKidnapCarHeadingA)
				//CREATE_NPC_PED_ON_FOOT(piJimmy, CHAR_JIMMY, vKidnapCarPosA, fKidnapCarHeadingA)
				SET_ENTITY_VISIBLE(piJimmy, FALSE)
				SET_PED_CAN_BE_TARGETTED(piJimmy, FALSE)
				SET_PED_CONFIG_FLAG(piJimmy, PCF_GetOutUndriveableVehicle, FALSE)
				SET_PED_CONFIG_FLAG(piJimmy, PCF_GetOutBurningVehicle, FALSE)
				SET_PED_CONFIG_FLAG(piJimmy, PCF_OpenDoorArmIK, TRUE)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(piJimmy, TRUE)
				SET_PED_PATH_CAN_DROP_FROM_HEIGHT(piJimmy, TRUE)
				
				// Added for B* 1308548 - Jimmy uses the female skeleton which results in his helmet being offset, disabling helmet use was the best solution
				SET_PED_HELMET(piJimmy, FALSE)
				
				SET_ENTITY_PROOFS(piJimmy, TRUE, FALSE, FALSE, TRUE, TRUE)
				SET_PED_CAN_RAGDOLL(piJimmy, FALSE)
				SET_PED_RESET_FLAG(piJimmy, PRF_DisablePotentialBlastReactions, TRUE)
				SET_PED_RESET_FLAG(piJimmy, PRF_DisableVehicleDamageReactions, TRUE)
				SET_PED_RESET_FLAG(piJimmy, PRF_CannotBeTargetedByAI, TRUE)
				
				//SET_PED_CONFIG_FLAG(piJimmy, PCF_WaitForDirectEntryPointToBeFreeWhenExiting, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piJimmy, TRUE)
				SET_PED_DIES_IN_WATER(piJimmy, TRUE)
				SET_PED_DIES_IN_SINKING_VEHICLE(piJimmy, TRUE)
				ADD_PED_FOR_DIALOGUE(pedConvStruct, JIMMY_ID, piJimmy, "JIMMY", TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rghPlayer)
				SET_PED_RELATIONSHIP_GROUP_HASH(piJimmy, rghPlayer)
				SET_ENTITY_COLLISION(piJimmy, FALSE)
				iJimmyAnims = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
				SET_SYNCHRONIZED_SCENE_LOOPED(iJimmyAnims,TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iJimmyAnims,FALSE)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iJimmyAnims, viKidnapCar, GET_ENTITY_BONE_INDEX_BY_NAME(viKidnapCar, ANIM_ATTACH_POINT))
				TASK_SYNCHRONIZED_SCENE(piJimmy, iJimmyAnims, ANIM_DICT_JIMMY, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, rbfSyncedScene)
				eJimmysAnimStage = JA_READY
			ENDIF
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(mnKidnapCar)
			viKidnapCar = CREATE_VEHICLE(mnKidnapCar, vKidnapCarPosA, fKidnapCarHeadingA)
			//SET_VEHICLE_COLOUR_COMBINATION(viKidnapCar, 7)
			SET_VEHICLE_COLOURS(viKidnapCar, 22, 22)
			SET_VEHICLE_EXTRA_COLOURS(viKidnapCar, 0, 8)
			SET_ENTITY_VISIBLE(viKidnapCar, FALSE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(viKidnapCar, TRUE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnKidnapCar, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnKidnapCar)
			//SET_VEHICLE_WINDOW_TINT(viKidnapCar, 0)
			//SET_VEHICLE_MOD_KIT(viKidnapCar, 0)
			SET_ENTITY_LOAD_COLLISION_FLAG(viKidnapCar, TRUE)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(viKidnapCar, FALSE)
			bTurnOnVisibleDamage = FALSE
			iVisibleDamageTimer = -1
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(viKidnapCar, "M_E_JIMMY_ENEMY_VEHICLE_GROUP")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Teleports the kidnapper's van to an appropriate place based on where the player is and starts the chase if the player is in a trigger zone
/// RETURNS:
///    TRUE if the player is in one of the two trigger zones
FUNC BOOL StartCarChase()
	IF IS_VEHICLE_OK(viKidnapCar) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piKidnapper[0]) AND IS_PED_UNINJURED(piJimmy)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaAPos1, vAreaAPos2, fAreaWidth) OR  IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaAPos1Alt, vAreaAPos2Alt, fAreaWidthAlt)
			SET_ENTITY_COORDS(viKidnapCar, vKidnapCarPosA)
			SET_ENTITY_HEADING(viKidnapCar, fKidnapCarHeadingA)
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaBPos1, vAreaBPos2, fAreaWidth) OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vAreaBPos1Alt, vAreaBPos2Alt, fAreaWidthAlt)
			SET_ENTITY_COORDS(viKidnapCar, vKidnapCarPosA)
			SET_ENTITY_HEADING(viKidnapCar, fKidnapCarHeadingA)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Have the tyres on the enemy car been burst?
/// RETURNS:
///    TRUE if more than two tyres have been burst
FUNC BOOL EnemyTyresBurst()
	IF bTyresBurst
		IF GET_GAME_TIMER() > iTyresBurstTimer
			RETURN TRUE
		ENDIF
	ELSE
		INT numTyres = 0
		IF IS_VEHICLE_TYRE_BURST(viKidnapCar, SC_WHEEL_CAR_FRONT_LEFT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viKidnapCar, SC_WHEEL_CAR_FRONT_RIGHT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viKidnapCar, SC_WHEEL_CAR_REAR_LEFT)
			numTyres++
		ENDIF
		IF IS_VEHICLE_TYRE_BURST(viKidnapCar, SC_WHEEL_CAR_REAR_RIGHT)
			numTyres++
		ENDIF
		
		IF numTyres > 2
			bTyresBurst = TRUE
			iTyresBurstTimer = GET_GAME_TIMER() + iTyresTime
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the fail reason, and sets the mission state to be fail delay
/// PARAMS:
///    eFailReason - the reason the player failed the mission
PROC SetFailReason(MISSION_FAILED_REASON eFailReason)
	missionFailedReason = eFailReason
	missionState = MS_FAIL_DELAY
	missionSubstate = MSS_PREPARE
ENDPROC

/// PURPOSE:
///    Fails the mission if the player is too far from Jimmy
PROC TooFarFromJimmy()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
		IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) > fFailDistance)
			IF missionState = MS_RESCUE_JIMMY AND missionSubstate = MSS_ACTIVE_ONE
				SetFailReason(MFR_JIMMY_KIDNAPPED)
			ELSE
				SetFailReason(MFR_JIMMY_LEFT) 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the upside down check from the kidnapper's van and starts the foot conversation timer
PROC CommonCollectionStuff()
	iFootConvTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
	missionSubstate = MSS_PREPARE
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
		missionState = MS_DRIVE_HOME
	ELSE
		missionState = MS_LOSE_COPS
	ENDIF
ENDPROC

/// PURPOSE:
///    Deals with the player rescuing Jimmy, either by releasing him or by taking the van
/// PARAMS:
///    bProperRescue - TRUE if the player has released Jimmy by walking to the back of the van, FALSE if they've taken the van without first releasing him
PROC PlayerCollectsJimmy()//BOOL bProperRescue = TRUE)
	//IF bProperRescue
		// Conversation is optional so not used with an IF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_RESCUED", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
			IF IS_MESSAGE_BEING_DISPLAYED()
				PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_WHERE", "MEJ1_WHERE_2", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
			ELSE
				PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_WHERE", "MEJ1_WHERE_2", CONV_PRIORITY_MEDIUM)
			ENDIF
		ENDIF
		CommonCollectionStuff()
	/*ELSE
		// Conversation is optional so not used with an IF
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_VAN", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
		ENDIF
		CommonCollectionStuff()
	ENDIF*/
ENDPROC

/// PURPOSE:
///    Fails the mission if the player has injured or killed Jimmy, makes him respond appropriately if the player threatens him
PROC CheckJimmy()
	IF missionState = MS_FAIL_DELAY
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(piJimmy) AND IS_PED_INJURED(piJimmy)
		SetFailReason(MFR_JIMMY_DEAD) 

	ELIF IS_PED_UNINJURED(piJimmy) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(piJimmy), 2.0)
		AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_WATER_HYDRANT, GET_ENTITY_COORDS(piJimmy), 2.0)
			//EXPLODE_PED_HEAD(piJimmy)
			SET_PED_CAN_RAGDOLL(piJimmy, TRUE)
			SET_PED_TO_RAGDOLL(piJimmy, 500, 1000, TASK_RELAX, FALSE, FALSE)
			SET_ENTITY_HEALTH(piJimmy, 99)
			SET_ENTITY_COLLISION(piJimmy, TRUE)	// B* 1915369 - Jimmy fell through the road at this point
			SetFailReason(MFR_JIMMY_DEAD)
		ELIF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piJimmy, PLAYER_PED_ID())
			IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(piJimmy, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(piJimmy)
				REMOVE_PED_FROM_GROUP(piJimmy)
				SEQUENCE_INDEX siJimmy
				OPEN_SEQUENCE_TASK(siJimmy)
					IF IS_PED_IN_ANY_VEHICLE(piJimmy)
						TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT | ECF_DONT_CLOSE_DOOR)
					ENDIF
					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
				CLOSE_SEQUENCE_TASK(siJimmy)
				TASK_PERFORM_SEQUENCE(piJimmy, siJimmy)
				CLEAR_SEQUENCE_TASK(siJimmy)
				SetFailReason(MFR_JIMMY_HURT)
			ENDIF
		ELSE
			IF IS_ENTITY_ALIVE(viKidnapCar) AND (IS_PED_IN_VEHICLE(piJimmy, viKidnapCar) OR IS_ENTITY_PLAYING_ANIM(piJimmy, ANIM_DICT_JIMMY, "back", ANIM_SYNCED_SCENE)
			 OR IS_ENTITY_PLAYING_ANIM(piJimmy, ANIM_DICT_JIMMY, "base", ANIM_SYNCED_SCENE) OR IS_ENTITY_PLAYING_ANIM(piJimmy, ANIM_DICT_JIMMY, "enter_back", ANIM_SYNCED_SCENE)
			 OR IS_ENTITY_PLAYING_ANIM(piJimmy, ANIM_DICT_JIMMY, "enter_backseat", ANIM_SYNCED_SCENE) OR IS_ENTITY_PLAYING_ANIM(piJimmy, ANIM_DICT_JIMMY, "exit_back", ANIM_SYNCED_SCENE))
				IF IS_ENTITY_ON_FIRE(viKidnapCar)
					SetFailReason(MFR_JIMMY_HURT)
				ENDIF
			ENDIF
			//IF IS_PLAYER_VISIBLY_TARGETTING_PED(piJimmy) 
			IF IS_PLAYER_FREE_AIMING(PLAYER_ID()) AND IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piJimmy, 50.0) AND CAN_PED_SEE_PLAYER(piJimmy)
			AND CAN_PED_SEE_PED(PLAYER_PED_ID(), piJimmy, 90.0)
			//AND (IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(piJimmy), 10.0))// OR IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(piJimmy), -5.0))
				IF iCurrentAimLine < NUM_AIM_LINES AND GET_GAME_TIMER() > iAimLineTimer
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_INJURE", sAimLines[iCurrentAimLine], CONV_PRIORITY_MEDIUM)
							iCurrentAimLine++
							iAimLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
						ENDIF
					ENDIF
				ENDIF
				IF (GET_SCRIPT_TASK_STATUS(piJimmy, SCRIPT_TASK_HANDS_UP) <> PERFORMING_TASK)
					TASK_HANDS_UP(piJimmy, -1)
					
				ENDIF
			ELIF (GET_SCRIPT_TASK_STATUS(piJimmy, SCRIPT_TASK_HANDS_UP) = PERFORMING_TASK)
				CLEAR_PED_TASKS(piJimmy)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player has killed a kidnapper, fails if they have
PROC CheckKidnappers()
	IF missionState = MS_FAIL_DELAY
		EXIT
	ENDIF
	
	INT i_index = 0
	REPEAT NUM_KIDNAPPERS i_index
		IF DOES_ENTITY_EXIST(piKidnapper[i_index])
			IF IS_PED_INJURED(piKidnapper[i_index])
				SetFailReason(MFR_KIDNAPPER_DIED)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/*
INT iHandDropTimer = 0

IF IS_PLAYER_VISIBLY_TARGETTING_PED(piJimmy) 
	IF (GET_SCRIPT_TASK_STATUS(piJimmy, SCRIPT_TASK_HANDS_UP) <> PERFORMING_TASK)
		TASK_HANDS_UP(piJimmy, -1)
	ENDIF
	
	iHandDropTimer = GET_GAME_TIMER() + 2000
ELIF (GET_GAME_TIMER() > iHandDropTimer)
	IF (GET_SCRIPT_TASK_STATUS(piJimmy, SCRIPT_TASK_HANDS_UP) = PERFORMING_TASK)
		CLEAR_PED_TASKS(piJimmy)
	ENDIF
ENDIF
*/

//VECTOR D
//angle = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vNormalisedVec)

/// PURPOSE:
///    Checks if Michael and Jimmy are in the same vehicle
/// RETURNS:
///    True if they are in the same vehicle, false otherwise
FUNC BOOL MichaelAndJimmyInSameVehicle()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piJimmy, viTemp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Michael and Jimmy are on foot
/// RETURNS:
///    True if they are both on foot, false otherwise
FUNC BOOL MichaelAndJimmyOnFoot()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR IS_PED_IN_ANY_VEHICLE(piJimmy)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Controls Michael and Jimmy's conversations while driving home
PROC ManageDriveConversations()
	IF eDriveConvState = DCS_NULL
		IF /*iCurrentDriveConv = -1 /*< NUM_DRIVE_CONVS AND*/ iPreviousDriveConv = -1 AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
		AND MichaelAndJimmyInSameVehicle() //OR MichaelAndJimmyOnFoot())
			IF iPreviousDriveConv = iCurrentDriveConv
				iCurrentDriveConv++
				iConversationTimer = GET_GAME_TIMER() + iDriveConvTimer
			ELSE
				IF GET_GAME_TIMER() > iConversationTimer
					//IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", sDriveConvs[iCurrentDriveConv], CONV_PRIORITY_MEDIUM)
					IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_DRIVE1", CONV_PRIORITY_MEDIUM)
						iPreviousDriveConv = iCurrentDriveConv
						eDriveConvState = DCS_PLAYING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF eDriveConvState = DCS_READY
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue() AND MichaelAndJimmyInSameVehicle() AND GET_GAME_TIMER() > iConversationTimer
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(pedConvStruct, "MEJ1AUD", "MEJ1_DRIVE1", sSceneConvLine, CONV_PRIORITY_MEDIUM)
				eDriveConvState = DCS_PLAYING
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pauses the drive conversation ready to restart it
PROC PauseDriveConversation()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// AND eDriveConvState = DCS_PLAYING
		TEXT_LABEL_23 tlConv
		tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		STRING sConv
		sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
		IF ARE_STRINGS_EQUAL(sConv, "MEJ1_DRIVE1")
			CPRINTLN(DEBUG_MISSION,"Drive conversation playing, ready to pause")
			sSceneConvLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
			CPRINTLN(DEBUG_MISSION,"Current conversation label is: ", sSceneConvLine)
			IF ARE_STRINGS_EQUAL(sSceneConvLine, "") OR ARE_STRINGS_EQUAL(sSceneConvLine, "NULL")
				eDriveConvState = DCS_FINISHED
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iConversationTimer = GET_GAME_TIMER() + iDriveConvTimer
				eDriveConvState = DCS_READY
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tests if the player has hurt or killed a specific ped
/// PARAMS:
///    testPed - the ped to test
/// RETURNS:
///    TRUE if the ped has hurt or killed this ped
FUNC BOOL PedHurtByPlayer(PED_INDEX testPed)
	IF IS_PED_INJURED(testPed) OR (IS_PED_UNINJURED(testPed) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(testPed, PLAYER_PED_ID())) //OR IS_PLAYER_SHOOTING_NEAR_PED(testPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Jimmy speaks to Michael if the player kills the kidnapper
PROC ManageKillingKidnapper()
	IF bAllowKidnapperDeadConv AND DOES_ENTITY_EXIST(piKidnapper[0]) AND DOES_ENTITY_EXIST(piKidnapper[1]) AND DOES_ENTITY_EXIST(piKidnapper[2]) AND IS_PED_UNINJURED(piJimmy)
	AND NOT IS_MESSAGE_BEING_DISPLAYED()
		/*IF PedHurtByPlayer(piKidnapper[0]) OR PedHurtByPlayer(piKidnapper[1]) OR PedHurtByPlayer(piKidnapper[2])
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF (HAS_ENTITY_CLEAR_LOS_TO_ENTITY(piJimmy, piKidnapper[0]) AND IS_PED_INJURED(piKidnapper[0]))
				OR (HAS_ENTITY_CLEAR_LOS_TO_ENTITY(piJimmy, piKidnapper[1]) AND IS_PED_INJURED(piKidnapper[1]))
				OR (HAS_ENTITY_CLEAR_LOS_TO_ENTITY(piJimmy, piKidnapper[2]) AND IS_PED_INJURED(piKidnapper[2]))
					IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_KILL", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						bAllowKidnapperDeadConv = FALSE
					ENDIF
				ELSE
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_KILL", "MEJ1_KILL_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						bAllowKidnapperDeadConv = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF*/
		/*IF IS_PED_INJURED(piKidnapper[0])
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_KIDNAP", CONV_PRIORITY_MEDIUM)
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				bAllowKidnapperDeadConv = FALSE
			ENDIF
		ELIF IS_PED_INJURED(piKidnapper[1]) OR IS_PED_INJURED(piKidnapper[2])
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HENCH", CONV_PRIORITY_MEDIUM)
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				bAllowKidnapperDeadConv = FALSE
			ENDIF
		ELSE*/
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_THANK", CONV_PRIORITY_MEDIUM)
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
				bAllowKidnapperDeadConv = FALSE
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Plays dialogue if the player kills the kidnapper or a henchman
/*PROC PlayKillLines()
	IF bKillLineOneReady
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
			IF PedHurtByPlayer(piKidnapper[0]) OR PedHurtByPlayer(piKidnapper[1]) OR PedHurtByPlayer(piKidnapper[2])
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_KILL", "MEJ1_KILL_1", CONV_PRIORITY_MEDIUM)
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					bKillLineOneReady = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELIF bKillLineTwoReady
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_MESSAGE_BEING_DISPLAYED()
			IF PedHurtByPlayer(piKidnapper[0]) OR PedHurtByPlayer(piKidnapper[1]) OR PedHurtByPlayer(piKidnapper[2])
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_KILL", "MEJ1_KILL_2", CONV_PRIORITY_MEDIUM)
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					bKillLineTwoReady = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC*/

/// PURPOSE:
///    Controls Jimmy responding to the player crashing the car
PROC ManageCrashLines()
	IF iCurrentCrashLine < NUM_CRASH_LINES AND GET_GAME_TIMER() > iCrashLineTimer /*AND IsOkToPlayDialogue()*/ AND MichaelAndJimmyInSameVehicle()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) /*AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()*/ AND NOT IS_PED_STOPPED(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viTemp)
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(viTemp)
					CPRINTLN(DEBUG_MISSION, "Player collided with something")
					//PauseDriveConversation()
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					BOOL bConvDone = FALSE
					IF IsOkToPlayDialogue()
						bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_CRASH", sCrashLines[iCurrentCrashLine], CONV_PRIORITY_MEDIUM)
					ELSE
						bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_CRASH", sCrashLines[iCurrentCrashLine], CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
					IF bConvDone
						iCurrentCrashLine++
						iCrashLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls Jimmy responding to the player committing crime
PROC ManageCrimeLines()
	IF iCurrentCrimeLine < NUM_CRIME_LINES AND GET_GAME_TIMER() > iCrimeLineTimer //AND IsOkToPlayDialogue()
	AND (MichaelAndJimmyInSameVehicle() OR MichaelAndJimmyOnFoot())
		IF /*IS_PED_SHOOTING(PLAYER_PED_ID()) OR*/ HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			CPRINTLN(DEBUG_MISSION, "Player hurt someone")
			//PauseDriveConversation()
			IF bKillLineOneReady
			// Player hasn't killed any of the kidnappers
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					BOOL bConvDone = FALSE
					IF IsOkToPlayDialogue()
						bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_NOKILL", sCrimeLinesNoKill[iCurrentCrimeLine], CONV_PRIORITY_MEDIUM)
					ELSE
						bConvDone = PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_NOKILL", sCrimeLinesNoKill[iCurrentCrimeLine], CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					ENDIF
					IF bConvDone
						iCurrentCrimeLine++
						iCrimeLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					ENDIF
				//ENDIF
			ELSE
			// Player has killed at least one of the kidnappers
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HITKILL", sCrimeLines[iCurrentCrimeLine], CONV_PRIORITY_MEDIUM)
						iCurrentCrimeLine++
						iCrimeLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
						CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls Jimmy responding to the player staying stationary
PROC ManageStationaryLines()
	IF iCurrentStillLine < NUM_STILL_LINES AND GET_GAME_TIMER() > iStillLineTimer
	AND (MichaelAndJimmyInSameVehicle() OR MichaelAndJimmyOnFoot())
		IF IS_PED_STOPPED(PLAYER_PED_ID())
			// check stopped timer, do line if too long
			iStillTime++
			IF iStillTime > iStillTimeout
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_STILL", sStillLines[iCurrentStillLine], CONV_PRIORITY_MEDIUM)
						iCurrentStillLine++
						iStillTime = 0
						iStillLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iStillTime = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manages conversations between Michael and the kidnapper if the player decides to chase after him
PROC ManageFootChaseLines()
	IF iCurrentFootConv < NUM_FOOT_CONVS AND GET_GAME_TIMER() > iFootConvTimer AND IS_PED_UNINJURED(piKidnapper[0])
		IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piKidnapper[0]) < fFootConvDist
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
				//IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", sFootConvs[iCurrentFootConv], CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_FLEE", sFootConvs[iCurrentFootConv], CONV_PRIORITY_MEDIUM)
					iCurrentFootConv++
					iFootConvTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC ManageShootingLines()
	IF GET_GAME_TIMER() > iShootingLineTimer
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue() AND IS_PED_SHOOTING(PLAYER_PED_ID())
			IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_SHOOT", CONV_PRIORITY_MEDIUM)
				iShootingLineTimer = GET_GAME_TIMER() + SHOOTING_TIMEOUT
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the player is close to the kidnappers in a cop car with the siren on
/// RETURNS:
///    TRUE if the ped is close, in a cop car and has the siren on
FUNC BOOL PlayerCloseInCopCar()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viKidnapCar)
		VEHICLE_INDEX viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayer)
			MODEL_NAMES mnPlayer = GET_ENTITY_MODEL(viPlayer)
			IF IS_MODEL_POLICE_VEHICLE(mnPlayer) OR mnPlayer = POLICE4 OR mnPlayer = POLICEOLD1 OR mnPlayer = POLICEOLD2 OR mnPlayer = FBI
				IF IS_VEHICLE_SIREN_ON(viPlayer) AND IS_ENTITY_IN_RANGE_ENTITY(viPlayer, viKidnapCar, 15.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the player's current car and adds it to the upside down check
PROC GetCurrentPlayersCar()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND GET_GAME_TIMER() > iCheckVehicleTimer
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX viNewVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF viNewVehicle <> viPlayersVehicle
				IF IS_VEHICLE_OK(viPlayersVehicle)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayersVehicle)
				ENDIF
				viPlayersVehicle = viNewVehicle
			ENDIF
		ENDIF
		iCheckVehicleTimer = GET_GAME_TIMER() + iCheckVehicle
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes Jimmy leave a submerged vehicle
PROC JimmyLeavesDrowningCar()
	IF IS_PED_UNINJURED(piJimmy)
		IF IS_PED_IN_ANY_VEHICLE(piJimmy)
			VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_IN(piJimmy)
			IF DOES_ENTITY_EXIST(viVehicle)
				IF NOT IS_VEHICLE_DRIVEABLE(viVehicle) AND IS_ENTITY_IN_WATER(viVehicle)
					TASK_LEAVE_ANY_VEHICLE(piJimmy, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_JUMP_OUT | ECF_DONT_CLOSE_DOOR)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pick a sensible spawnpoint near to where the CP coords got set
/// PARAMS:
///    vCoords - Current checkpoint spawn vector, will be reset to one of the presets
///    fHeading - Current spawn heading, will be reset to one of the presets
PROC GET_NEAREST_PRESET_SPAWN_TO_CP(VECTOR &vCoords, FLOAT &fHeading)
      
      // Set up possible spawn coords/heading
      VECTOR vSpawnCoords[5]
      FLOAT fSpawnHead[5]
      vSpawnCoords[0] = << -2539.17, 1866.73, 166.10 >>
      fSpawnHead[0] = 216.03
      vSpawnCoords[1] = << -2022.33, 826.23, 162.32 >>
      fSpawnHead[1] = 172.12
      vSpawnCoords[2] = << -1038.30, 253.00, 64.21 >>
      fSpawnHead[2] = 270.02
      vSpawnCoords[3] = << -3140.89, 1024.06, 18.33 >>
      fSpawnHead[3] = 152.08
      vSpawnCoords[4] = << -2641.30, -110.03, 18.70 >>
      fSpawnHead[4] = 221.35
      
      // Index for best match found so far
      INT iBestMatch = 0
      INT iCount = 1
      WHILE iCount < 5
            // Compare distances for current 'best' and next in list (actually comparing the squares but amounts to the same thing)
            IF VDIST2(vSpawnCoords[iCount], vCoords) < VDIST2(vSpawnCoords[iBestMatch], vCoords)
                  iBestMatch = iCount
            ENDIF
            iCount++
      ENDWHILE
      
      // Set the passed in variables to be the best match found
      vCoords = vSpawnCoords[iBestMatch]
      fHeading = fSpawnHead[iBestMatch]
      
ENDPROC

// ===========================================================================================================
//		Debug functions
// ===========================================================================================================
/// PURPOSE:
///    Restarts the mission from the beginning
PROC RestartMission()
	RC_START_Z_SKIP()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		RemovePedsAndVehicles(TRUE)
		LoadMissionModels()
		VEHICLE_INDEX viTemp
		CREATE_VEHICLE_FOR_REPLAY(viTemp, << -2086.9126, 951.6347, 183.7526 >>, 75.8, TRUE, TRUE, FALSE, TRUE, TRUE, mnRestartCar)
		IF IS_VEHICLE_OK(viTemp)
			SET_VEHICLE_ENGINE_ON(viTemp, TRUE, TRUE)
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SpawnMissionSetup()
		WHILE NOT IS_PED_UNINJURED(piJimmy) AND NOT IS_PED_UNINJURED(piKidnapper[0]) AND NOT IS_PED_UNINJURED(piKidnapper[1]) AND NOT IS_PED_UNINJURED(piKidnapper[2])
			SpawnMissionSetup()
			WAIT(0)
		ENDWHILE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		
		IF IS_VEHICLE_OK(viTemp)
			SET_VEHICLE_FORWARD_SPEED(viTemp, 25.0)
		ENDIF
	ENDIF
	RC_END_Z_SKIP()
ENDPROC

/// PURPOSE:
///    Makes Jimmy walk off if the player debug passes the mission
PROC DebugPassDealWithJimmy()
	IF IS_PED_UNINJURED(piJimmy)
		IF IS_VEHICLE_OK(viKidnapCar)
			IF IS_PED_IN_VEHICLE(piJimmy, viKidnapCar) AND IS_VEHICLE_SEAT_FREE(viKidnapCar, VS_FRONT_RIGHT)
				SAFE_DELETE_PED(piJimmy)
				EXIT
			ENDIF
		ENDIF
		
		REMOVE_PED_FROM_GROUP(piJimmy)
		SEQUENCE_INDEX siJimmy
		OPEN_SEQUENCE_TASK(siJimmy)
			IF IS_PED_IN_ANY_VEHICLE(piJimmy)
				TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
			ENDIF
			TASK_WANDER_STANDARD(NULL)
		CLOSE_SEQUENCE_TASK(siJimmy)
		TASK_PERFORM_SEQUENCE(piJimmy, siJimmy)
		CLEAR_SEQUENCE_TASK(siJimmy)
		SET_PED_KEEP_TASK(piJimmy, TRUE)
		SAFE_RELEASE_PED(piJimmy)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the shop scene models have loaded
/// PARAMS:
///    iTimer - timer to continue the mission if models have failed to load after one second
/// RETURNS:
///    TRUE when all models have loaded or one second has passed
FUNC BOOL AreSceneModelsLoaded(INT & iTimer)
	IF iTimer < 0
		iTimer = GET_GAME_TIMER() + 1000
	ELIF GET_GAME_TIMER() > iTimer
		CPRINTLN(DEBUG_MISSION, "TIMEOUT -- AreSceneModelsLoaded")
		RETURN TRUE
	ENDIF
	IF NOT HAS_MODEL_LOADED(mnKidnapCar)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(mnKidnapper)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_JIMMY))
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the shop scene entities have been created
/// PARAMS:
///    iTimer - timer to continue the mission if models have failed to load after one second
/// RETURNS:
///    TRUE when they've all been created or if one second has passed
FUNC BOOL DoSceneEntitiesExist(INT & iTimer)
	IF iTimer < 0
		iTimer = GET_GAME_TIMER() + 1000
	ELIF GET_GAME_TIMER() > iTimer
		CPRINTLN(DEBUG_MISSION, "TIMEOUT -- DoSceneEntitiesExist")
		RETURN TRUE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(viKidnapCar)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piKidnapper[0])
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piKidnapper[1])
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piKidnapper[2])
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piJimmy)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Spawns the mission peds and vehicles if the player debug skips
PROC DebugSpawnMissionSetup()
	INT iTimer = -1
	LoadMissionModels()
	WHILE NOT AreSceneModelsLoaded(iTimer)
		WAIT(0)
	ENDWHILE
	
	iTimer = -1
	SpawnMissionSetup()
	WHILE NOT DoSceneEntitiesExist(iTimer)
		SpawnMissionSetup()
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Skips to the Drive Home stage, used when restarting and Z skipping
/// PARAMS:
///    bDebugSkip - is this a debug skip?
PROC SKIP_TO_DRIVE_HOME(BOOL bDebugSkip = FALSE)
	RemovePedsAndVehicles(TRUE)
	DebugSpawnMissionSetup()
	SET_ENTITY_VISIBLE(viKidnapCar, TRUE)
	SET_ENTITY_VISIBLE(piJimmy, TRUE)
	INT i = 0
	REPEAT NUM_KIDNAPPERS i
		SAFE_DELETE_PED(piKidnapper[i])
	ENDREPEAT
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
		SET_ENTITY_COLLISION(piJimmy, TRUE)
		IF bDebugSkip
			IF IS_VEHICLE_OK(viKidnapCar) AND IS_VEHICLE_SEAT_FREE(viKidnapCar)	AND IS_VEHICLE_SEAT_FREE(viKidnapCar, VS_FRONT_RIGHT)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viKidnapCar)
				SET_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_FRONT_RIGHT)
				SET_VEHICLE_ENGINE_ON(viKidnapCar, TRUE, TRUE)
			ENDIF
		ELSE
			VEHICLE_INDEX viTemp
			VECTOR vPos = GET_REPLAY_CHECKPOINT_PLAYER_POSITION()
			FLOAT fHeading
			GET_NEAREST_PRESET_SPAWN_TO_CP(vPos, fHeading)
			CREATE_VEHICLE_FOR_REPLAY(viTemp, vPos, fHeading, TRUE, FALSE, TRUE, TRUE, TRUE)
			IF IS_VEHICLE_OK(viTemp) AND IS_VEHICLE_SEAT_FREE(viTemp, VS_FRONT_RIGHT) AND IS_PED_UNINJURED(piJimmy)
				SET_PED_INTO_VEHICLE(piJimmy, viTemp, VS_FRONT_RIGHT)
				SET_VEHICLE_ENGINE_ON(viTemp, TRUE, TRUE)
				SAFE_RELEASE_VEHICLE(viKidnapCar)
			ENDIF
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	bShowHomeObjective = TRUE
	bShowReturnObjective = TRUE
	bAllowKidnapperDeadConv = TRUE
	iCurrentDriveConv = -1
	iPreviousDriveConv = -1
	iCurrentCrashLine = 0
	iCurrentCrimeLine = 0
	iCurrentStillLine = 0
	iCrashLineTimer = 0
	iCrimeLineTimer = 0
	iStillLineTimer = 0
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
	missionSubstate = MSS_PREPARE
	missionState = MS_DRIVE_HOME
ENDPROC

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Skips to the next stage of the mission
	PROC J_SKIP()
		RC_START_Z_SKIP()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		// used to deal with failing for taking too long. Commented out for now in case this is needed again
		//iFailTimer = GET_CLOCK_HOURS()
		SWITCH missionState
			CASE MS_DRIVE_TO_LOCATION
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viTemp
						REQUEST_MODEL(mnRestartCar)
						WHILE NOT HAS_MODEL_LOADED(mnRestartCar)
							WAIT(0)
						ENDWHILE
						viTemp = CREATE_VEHICLE(mnRestartCar, << -2192.9592, 1044.9886, 191.9691 >>, 42.5)
						WHILE NOT DOES_ENTITY_EXIST(viTemp)
							WAIT(0)
						ENDWHILE
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viTemp)
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(mnRestartCar)
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -2192.9592, 1044.9886, 191.9691 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 42.5)
					ENDIF
					DebugSpawnMissionSetup()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					bAllowKidnapperDeadConv = TRUE
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_KIDNAPPER, "Rescue Jimmy")
					missionSubstate = MSS_PREPARE
					missionState = MS_RESCUE_JIMMY
				ENDIF
			BREAK
			
			CASE MS_RESCUE_JIMMY
				INT i
				i = 0
				REPEAT NUM_KIDNAPPERS i
					SAFE_DELETE_PED(piKidnapper[i])
				ENDREPEAT
				WHILE DOES_ENTITY_EXIST(piKidnapper[0]) OR DOES_ENTITY_EXIST(piKidnapper[1]) OR DOES_ENTITY_EXIST(piKidnapper[2])
					WAIT(0)
				ENDWHILE
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy) AND IS_VEHICLE_OK(viKidnapCar) AND IS_VEHICLE_SEAT_FREE(viKidnapCar)
				AND IS_VEHICLE_SEAT_FREE(viKidnapCar, VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viKidnapCar)
					SET_PED_INTO_VEHICLE(piJimmy, viKidnapCar, VS_FRONT_RIGHT)
					SET_ENTITY_COLLISION(piJimmy, TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
				bShowHomeObjective = TRUE
				bAllowKidnapperDeadConv = TRUE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
				missionSubstate = MSS_PREPARE
				missionState = MS_DRIVE_HOME
				iCurrentDriveConv = -1
				iPreviousDriveConv = -1
				iCurrentCrashLine = 0
				iCurrentCrimeLine = 0
				iCurrentStillLine = 0
				iCrashLineTimer = 0
				iCrimeLineTimer = 0
				iStillLineTimer = 0
			BREAK
			
			CASE MS_DRIVE_HOME
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
					IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX viTemp
							viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piJimmy, viTemp)
								SET_ENTITY_COORDS(viTemp, vHotelLocation)
								SET_ENTITY_HEADING(viTemp, 300.0)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vHotelLocation)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
								SET_ENTITY_COORDS(piJimmy, vHotelLocation+<<1,1,0>>)
								SET_ENTITY_HEADING(piJimmy, 300.0)
							ENDIF
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHotelLocation)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
							SET_ENTITY_COORDS(piJimmy, vHotelLocation+<<1,1,0>>)
							SET_ENTITY_HEADING(piJimmy, 300.0)
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX viTemp
							viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piJimmy, viTemp)
								SET_ENTITY_COORDS(viTemp, vHomeLocation)
								SET_ENTITY_HEADING(viTemp, 300.0)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vHomeLocation)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
								SET_ENTITY_COORDS(piJimmy, vHomeLocation+<<1,1,0>>)
								SET_ENTITY_HEADING(piJimmy, 300.0)
							ENDIF
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHomeLocation)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
							SET_ENTITY_COORDS(piJimmy, vHomeLocation+<<1,1,0>>)
							SET_ENTITY_HEADING(piJimmy, 300.0)
						ENDIF
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		RC_END_Z_SKIP()
	ENDPROC
	
	/// PURPOSE:
	///    Skips to the previous stage of the mission
	PROC P_SKIP()
		RC_START_Z_SKIP()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		SWITCH missionState
			CASE MS_RESCUE_JIMMY
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viTemp
						REQUEST_MODEL(mnRestartCar)
						WHILE NOT HAS_MODEL_LOADED(mnRestartCar)
							WAIT(0)
						ENDWHILE
						viTemp = CREATE_VEHICLE(mnRestartCar, << -2086.9126, 951.6347, 183.7526 >>, 75.8)
						WHILE NOT DOES_ENTITY_EXIST(viTemp)
							WAIT(0)
						ENDWHILE
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viTemp)
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(mnRestartCar)
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -2086.9126, 951.6347, 183.7526 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 75.8)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					RemovePedsAndVehicles(TRUE)
					bAllowKidnapperDeadConv = TRUE
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_LOCATION
				ENDIF
			BREAK
			
			CASE MS_DRIVE_HOME
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX viTemp
					REQUEST_MODEL(mnRestartCar)
					WHILE NOT HAS_MODEL_LOADED(mnRestartCar)
						WAIT(0)
					ENDWHILE
					viTemp = CREATE_VEHICLE(mnRestartCar, << -2192.9592, 1044.9886, 191.9691 >>, 42.5)
					WHILE NOT DOES_ENTITY_EXIST(viTemp)
						WAIT(0)
					ENDWHILE
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viTemp)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(mnRestartCar)
				ELSE
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -2192.9592, 1044.9886, 191.9691 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 42.5)
				ENDIF
				RemovePedsAndVehicles(TRUE)
				DebugSpawnMissionSetup()
				bAllowKidnapperDeadConv = TRUE
				iCurrentDriveConv = -1
				iPreviousDriveConv = -1
				iCurrentCrashLine = 0
				iCurrentCrimeLine = 0
				iCurrentStillLine = 0
				iCrashLineTimer = 0
				iCrimeLineTimer = 0
				iStillLineTimer = 0
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_KIDNAPPER, "Rescue Jimmy")
				missionSubstate = MSS_PREPARE
				missionState = MS_RESCUE_JIMMY
			BREAK
		ENDSWITCH
		RC_END_Z_SKIP()
	ENDPROC
	
	/// PURPOSE:
	///    Skips to a player selected stage of the mission
	PROC Z_SKIP()
		INT debugJumpStage = -1
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, debugJumpStage)
			RC_START_Z_SKIP()
			IF debugJumpStage = 0
				// Drive to location
				RestartMission()
				missionSubstate = MSS_PREPARE
				missionState = MS_DRIVE_TO_LOCATION
			ELIF debugJumpStage = 1
				// Rescue Jimmy
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viTemp
						REQUEST_MODEL(mnRestartCar)
						WHILE NOT HAS_MODEL_LOADED(mnRestartCar)
							WAIT(0)
						ENDWHILE
						viTemp = CREATE_VEHICLE(mnRestartCar, << -2192.9592, 1044.9886, 191.9691 >>, 42.5)
						WHILE NOT DOES_ENTITY_EXIST(viTemp)
							WAIT(0)
						ENDWHILE
						IF IS_PED_UNINJURED(PLAYER_PED_ID())
							TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viTemp)
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED(mnRestartCar)
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -2192.9592, 1044.9886, 191.9691 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 42.5)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					bAllowKidnapperDeadConv = TRUE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					RemovePedsAndVehicles(TRUE)
					DebugSpawnMissionSetup()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_KIDNAPPER, "Rescue Jimmy")
					missionSubstate = MSS_PREPARE
					missionState = MS_RESCUE_JIMMY
				ENDIF
			ELIF debugJumpStage = 2
				// Drive home
				SKIP_TO_DRIVE_HOME(TRUE)
			ENDIF
			RC_END_Z_SKIP()
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Checks for the player pressing a skip stage debug key
	PROC CheckDebugKeys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			J_SKIP()
		ENDIF
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			P_SKIP()
		ENDIF
		Z_SKIP()
	ENDPROC
#ENDIF

// ===========================================================================================================
//		Mission state functions
// ===========================================================================================================
/// PURPOSE:
///    Initilises the mission
PROC INIT()
	REQUEST_ADDITIONAL_TEXT("MEJ1", MISSION_TEXT_SLOT)
	
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		CPRINTLN(DEBUG_MISSION,"MS_INIT")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(pedConvStruct, MICHAEL_ID, PLAYER_PED_ID(), "MICHAEL", TRUE)
		ENDIF
		ADD_PED_FOR_DIALOGUE(pedConvStruct, JIMMY_PHONE_ID, NULL, "JIMMY")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			rghPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		ENDIF
		
		DISABLE_TAXI_HAILING(TRUE)
		
		// Set the instance priority
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
		
		REQUEST_ANIM_DICT(ANIM_DICT_JIMMY)
		
		#IF IS_DEBUG_BUILD
			sSkipMenu[0].sTxtLabel = "Drive to Jimmy"
			sSkipMenu[1].sTxtLabel = "Rescue Jimmy"
			sSkipMenu[2].sTxtLabel = "Drive home"
		#ENDIF
		
		/*sDriveConvs[0] = "MEJ1_DRIVE1"
		sDriveConvs[1] = "MEJ1_DRIVE2"
		sDriveConvs[2] = "MEJ1_DRIVE3"
		sDriveConvs[3] = "MEJ1_DRIVE4"
		sDriveConvs[4] = "MEJ1_DRIVE5"*/
		
		/*sStartLines[0] = "MEJ1_MICHAEL_1"
		sStartLines[1] = "MEJ1_MICHAEL_2"
		sStartLines[2] = "MEJ1_MICHAEL_3"
		sStartLines[3] = "MEJ1_MICHAEL_4"
		sStartLines[4] = "MEJ1_MICHAEL_5"*/
		
		/*sChaseLines[0] = "MEJ1_CHASE2"
		sChaseLines[1] = "MEJ1_CHASE3"
		sChaseLines[2] = "MEJ1_CHASE4"
		sChaseLines[3] = "MEJ1_CHASE5"
		sChaseLines[4] = "MEJ1_CHASE6"*/
		sChaseLines[0] = "MEJ1_MRAM_1"
		sChaseLines[1] = "MEJ1_MRAM_2"
		sChaseLines[2] = "MEJ1_MRAM_3"
		sChaseLines[3] = "MEJ1_MRAM_4"
		sChaseLines[4] = "MEJ1_MRAM_5"
		sChaseLines[2] = "MEJ1_MRAM_6"
		sChaseLines[3] = "MEJ1_MRAM_7"
		sChaseLines[4] = "MEJ1_MRAM_8"
		
		/*sTiedLines[0] = "MEJ1_TIED_1"
		sTiedLines[1] = "MEJ1_TIED_2"
		sTiedLines[2] = "MEJ1_TIED_3"
		sTiedLines[3] = "MEJ1_TIED_4"
		sTiedLines[4] = "MEJ1_TIED_5"*/
		
		sCrashLines[0] = "MEJ1_CRASH_1"
		sCrashLines[1] = "MEJ1_CRASH_2"
		sCrashLines[2] = "MEJ1_CRASH_3"
		sCrashLines[3] = "MEJ1_CRASH_4"
		sCrashLines[4] = "MEJ1_CRASH_5"
		sCrashLines[5] = "MEJ1_CRASH_6"
		
		sCrimeLines[0] = "MEJ1_HITKILL_1"
		sCrimeLines[1] = "MEJ1_HITKILL_2"
		sCrimeLines[2] = "MEJ1_HITKILL_3"
		sCrimeLinesNoKill[0] = "MEJ1_NOKILL_1"
		sCrimeLinesNoKill[1] = "MEJ1_NOKILL_2"
		sCrimeLinesNoKill[2] = "MEJ1_NOKILL_3"
		
		sStillLines[0] = "MEJ1_STILL_1"
		sStillLines[1] = "MEJ1_STILL_2"
		sStillLines[2] = "MEJ1_STILL_3"
		
		sAimLines[0] = "MEJ1_INJURE_1"
		sAimLines[1] = "MEJ1_INJURE_2"
		sAimLines[2] = "MEJ1_INJURE_3"
		sAimLines[3] = "MEJ1_INJURE_4"
		
		/*sFootConvs[0] = "MEJ1_FOOT1"
		sFootConvs[1] = "MEJ1_FOOT2"
		sFootConvs[2] = "MEJ1_FOOT3"*/
		sFootConvs[0] = "MEJ1_FLEE_1"
		sFootConvs[1] = "MEJ1_FLEE_2"
		sFootConvs[2] = "MEJ1_FLEE_3"
		sFootConvs[3] = "MEJ1_FLEE_4"
		sFootConvs[4] = "MEJ1_FLEE_5"
		sFootConvs[5] = "MEJ1_FLEE_6"
		sFootConvs[6] = "MEJ1_FLEE_7"
		sFootConvs[7] = "MEJ1_FLEE_8"
		sFootConvs[8] = "MEJ1_FLEE_9"
		sFootConvs[9] = "MEJ1_FLEE_10"
		sFootConvs[10] = "MEJ1_FLEE_11"
		sFootConvs[11] = "MEJ1_FLEE_12"
		
		vsKidnapperSeat[0] = VS_DRIVER
		vsKidnapperSeat[1] = VS_FRONT_RIGHT
		vsKidnapperSeat[2] = VS_BACK_LEFT
		
		IF IS_REPEAT_PLAY_ACTIVE() AND IS_PED_UNINJURED(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
			CREATE_VEHICLE_FOR_REPLAY(viTemp, vPos, fHead, TRUE, FALSE, TRUE, TRUE, TRUE)
		ENDIF

		IF IS_REPLAY_IN_PROGRESS()
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				iReplayStage++ // player is skipping this stage
			ENDIF
			
			SWITCH iReplayStage
				CASE CP_MISSION_START
					// don't do anything to the player if they failed before even reaching the mission
					VEHICLE_INDEX viTemp
					CREATE_VEHICLE_FOR_PHONECALL_TRIGGER_REPLAY(viTemp, TRUE, TRUE, TRUE, TRUE, mnRestartCar)
					IF IS_ENTITY_IN_RANGE_COORDS(viTemp, <<-811.343750,187.436691,71.478607>>, 6.0)
						SET_ENTITY_COORDS(viTemp, <<-822.404419,182.607849,71.378090>>) 
						SET_ENTITY_HEADING(viTemp, 135.654083)
					ENDIF
					SAFE_RELEASE_VEHICLE(viTemp)
					SUPPRESS_PLAYERS_VEHICLE()
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
		
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_LOCATION
				BREAK
				
				CASE CP_AT_KIDNAPPER
					// Give the player a car and respot them near the start of the chase
					RestartMission()
					SUPPRESS_PLAYERS_VEHICLE()
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_LOCATION
				BREAK
				
				CASE CP_DRIVE_HOME
					RC_START_Z_SKIP()
					SKIP_TO_DRIVE_HOME()
					SUPPRESS_PLAYERS_VEHICLE()
					WAIT(500)
					RC_END_Z_SKIP()
				BREAK
				
				CASE CP_MISSION_PASSED
					WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					RC_END_Z_SKIP()
					Mission_Passed()
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
				BREAK
			ENDSWITCH
		ELSE
			CPRINTLN(DEBUG_MISSION, "Not on a replay")
			SUPPRESS_PLAYERS_VEHICLE()
			
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
		
			missionSubstate = MSS_PREPARE
			missionState = MS_DRIVE_TO_LOCATION
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Player drives to the start of the car chsae
PROC DRIVE_TO_LOCATION()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEJ1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering drive to location state")
			SAFE_REMOVE_BLIP(biObjective)
			biObjective = CREATE_COORD_BLIP(vMissionLocation, BLIPPRIORITY_MED, FALSE)
			SET_BLIP_COLOUR(biObjective, BLIP_COLOUR_BLUE)
			SET_BLIP_NAME_FROM_TEXT_FILE(biObjective, "MEJ1_JIM")
			SET_BLIP_ROUTE(biObjective, TRUE)
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(biObjective, << -2086.9126, 951.6347, 183.7526 >>, 75.8)
			PRINT_NOW("MEJ1_01", DEFAULT_GOD_TEXT_TIME, 1)	// Save ~b~Jimmy.
			CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
			/*iCurrentStartLine = 3//-1
			iPreviousStartLine = 3//-1
			*/
			LoadMissionModels()
			IF IS_REPLAY_IN_PROGRESS()
				missionSubstate = MSS_ACTIVE_THREE
			ELSE
				missionSubstate = MSS_ACTIVE_ONE
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_ONE
			//PlayStartLines()
			// spawn mission set up when close enough
			IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vMissionLocation) < fSpawnSetupDist)
				missionSubstate = MSS_ACTIVE_TWO
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_TWO
			// start car chase and change state when close enough
			SpawnMissionSetup()
			IF StartCarChase()
				IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_RAM2", CONV_PRIORITY_MEDIUM)
				OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					missionSubstate = MSS_PREPARE
					missionState = MS_RESCUE_JIMMY
				ENDIF
			ENDIF
			
			// If Jimmy and the kidnapper have been created, play the phone conversation
			IF IS_PED_UNINJURED(piJimmy) AND IS_PED_UNINJURED(piKidnapper[0])
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vMissionLocation, 150)
				OR CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(pedConvStruct, CHAR_JIMMY, "MEJ1AUD", "MEJ1_CLOSE", CONV_PRIORITY_MEDIUM)
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_THREE
			// start car chase and change state when close enough
			SpawnMissionSetup()
			IF StartCarChase()
				IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_RAM2", CONV_PRIORITY_MEDIUM)
				OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					missionSubstate = MSS_PREPARE
					missionState = MS_RESCUE_JIMMY
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_GO_TO_JIMMY")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			START_AUDIO_SCENE("M_E_JIMMY_GO_TO_JIMMY")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player chases the kidnapper and smashes up his van before rescuing Jimmy
PROC RESCUE_JIMMY()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEJ1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			CPRINTLN(DEBUG_MISSION,"Entering Resue Jimmy stage")
			SAFE_REMOVE_BLIP(biObjective)
			IF IS_VEHICLE_OK(viKidnapCar) AND IS_PED_UNINJURED(piKidnapper[0]) AND IS_PED_UNINJURED(piKidnapper[1]) AND IS_PED_UNINJURED(piKidnapper[2]) AND IS_PED_UNINJURED(piJimmy)
				SET_ENTITY_VISIBLE(viKidnapCar, TRUE)
				SET_VEHICLE_HAS_STRONG_AXLES(viKidnapCar, TRUE)
				INT i
				i = 0
				REPEAT NUM_KIDNAPPERS i
					SET_ENTITY_VISIBLE(piKidnapper[i], TRUE)
				ENDREPEAT
				SET_ENTITY_VISIBLE(piJimmy, TRUE)
				SET_PED_DIES_INSTANTLY_IN_WATER(piJimmy, TRUE)
				SET_VEHICLE_FORWARD_SPEED(viKidnapCar, 12)//23.0)
				//TASK_VEHICLE_MISSION_PED_TARGET(piKidnapper[0], viKidnapCar, PLAYER_PED_ID(), MISSION_CRUISE, 12, DRIVINGMODE_AVOIDCARS_RECKLESS, 100.0,1.0)
				TASK_VEHICLE_DRIVE_WANDER(piKidnapper[0], viKidnapCar, 12, DRIVINGMODE_AVOIDCARS_RECKLESS)
				REQUEST_VEHICLE_ASSET(mnKidnapCar)
				biObjective = CREATE_VEHICLE_BLIP(viKidnapCar, FALSE)
				PRINT_NOW("MEJ1_02", DEFAULT_GOD_TEXT_TIME, 1)	// Trash the kidnappers' ~r~car.
				CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
				PRINT_HELP("MEJ1_WARN")
				// Give the player car strong axles
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX vehPTmp
					vehPTmp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					SET_ENTITY_AS_MISSION_ENTITY(vehPTmp)
					IF IS_ENTITY_ALIVE(vehPTmp)
						SET_VEHICLE_HAS_STRONG_AXLES(vehPTmp, TRUE)
					ENDIF
					SAFE_RELEASE_VEHICLE(vehPTmp)
				ENDIF
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_GO_TO_JIMMY")
				STOP_AUDIO_SCENE("M_E_JIMMY_GO_TO_JIMMY")
			ENDIF
			START_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
			iCurrentChaseLine = -1
			iPreviousChaseLine = -1
			iJimmyChaseTimer = -1
			//iCurrentTiedLine = 0
			iTiedLineTimer = 0
			bTyresBurst = FALSE
			bGoToJimmy = FALSE
			bKillLineOneReady = TRUE
			//bKillLineTwoReady = TRUE
			bHasPlayerTouchedKidnapper = FALSE
			bJimmyDadLinePlayed = FALSE
			bPlayNoKillLine = TRUE
			bExitBack = FALSE
			bStartFirstRecording = TRUE
			INIT_HAS_PLAYER_RAMMED_ENEMY_ENOUGH(bVanRammedLastFrame, iRamTimer, iVanRammedCount, fPlayerVsVanClosingSpeedLastFrame)
			fFailDistance = fFailDistanceFirst
			fVanWarnDistance = fVanWarnDistanceFirst
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			REQUEST_ANIM_DICT("missfam3")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_KIDNAPPER, "Rescue Jimmy")
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			MAKE_KIDNAPPER_LOOK_AT_PLAYER()
			PlayChaseLines()
			PlayJimmyChaseLines()
			UPDATE_CHASE_BLIP(biObjective, piJimmy, fFailDistance)
			IF IS_VEHICLE_OK(viKidnapCar) AND DOES_ENTITY_EXIST(piKidnapper[0])
				IF bTurnOnVisibleDamage AND GET_GAME_TIMER() > iVisibleDamageTimer
					SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(viKidnapCar, TRUE)
					bTurnOnVisibleDamage = FALSE
				ENDIF
				CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, viKidnapCar)
				
				IF (GET_ENTITY_HEALTH(viKidnapCar) < 850) OR EnemyTyresBurst()
				OR PlayerCloseInCopCar()
				OR HAS_PLAYER_RAMMED_ENEMY_ENOUGH(viKidnapCar, bVanRammedLastFrame, iRamTimer, iVanRammedCount, fPlayerVsVanClosingSpeedLastFrame)
				OR (GET_VEHICLE_ENGINE_HEALTH(viKidnapCar) < 50)
				OR (GET_VEHICLE_PETROL_TANK_HEALTH(viKidnapCar) < 50)
				//OR IS_PED_INJURED(piKidnapper[0])
				OR (IS_PED_UNINJURED(piKidnapper[0]) AND NOT IS_PED_IN_VEHICLE(piKidnapper[0], viKidnapCar))
				OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_HUNG_UP, KIDNAPPERS_STUCK_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_JAMMED, KIDNAPPERS_STUCK_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_ON_ROOF, KIDNAPPERS_STUCK_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_ON_SIDE, KIDNAPPERS_STUCK_TIME)
					IF IS_MOBILE_PHONE_CALL_ONGOING()
						KILL_PHONE_CONVERSATION()
					ENDIF
					IF IS_PED_UNINJURED(piKidnapper[0])
						TASK_VEHICLE_TEMP_ACTION(piKidnapper[0], viKidnapCar, TEMPACT_SWERVELEFT_STOP, 1000)
					ENDIF
					BRING_VEHICLE_TO_HALT(viKidnapCar, 20.0, 1)
					REMOVE_ANIM_DICT("missfam3")
					missionSubstate = MSS_ACTIVE_TWO
				ENDIF
				IF IS_ENTITY_ON_FIRE(viKidnapCar)
					// if the car's on fire, fail the mission because Jimmy's still in it
					SET_PED_CAN_RAGDOLL(piJimmy, TRUE)
					SET_PED_TO_RAGDOLL(piJimmy, 500, 1000, TASK_RELAX, FALSE, FALSE)
					SET_ENTITY_HEALTH(piJimmy, 99)
					SetFailReason(MFR_JIMMY_DEAD)
				ENDIF
			ELIF DOES_ENTITY_EXIST(viKidnapCar)
				REMOVE_ANIM_DICT("missfam3")
				//missionSubstate = MSS_ACTIVE_TWO
				IF IS_ENTITY_ON_FIRE(viKidnapCar)
					// if the car's on fire, fail the mission because Jimmy's still in it
					SET_PED_CAN_RAGDOLL(piJimmy, TRUE)
					SET_PED_TO_RAGDOLL(piJimmy, 500, 1000, TASK_RELAX, FALSE, FALSE)
					SET_ENTITY_HEALTH(piJimmy, 99)
					SetFailReason(MFR_JIMMY_DEAD)
				ENDIF
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_TWO
			UPDATE_CHASE_BLIP(biObjective, piJimmy, fFailDistance)
			IF IS_VEHICLE_OK(viKidnapCar)
				IF IS_VEHICLE_STOPPED(viKidnapCar)
					INT i
					i = 0
					SEQUENCE_INDEX siFlee
					REPEAT NUM_KIDNAPPERS i
						IF IS_PED_UNINJURED(piKidnapper[i])
							OPEN_SEQUENCE_TASK(siFlee)
								TASK_PAUSE(NULL, (i*1000))
								IF IS_PED_IN_VEHICLE(piKidnapper[i], viKidnapCar)
									TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED | ECF_DONT_CLOSE_DOOR)
								ENDIF
								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 500, -1)
							CLOSE_SEQUENCE_TASK(siFlee)
							TASK_PERFORM_SEQUENCE(piKidnapper[i], siFlee)
							CLEAR_SEQUENCE_TASK(siFlee)
							
							// non-essential conversation so not in an IF statement
							IF i = 0 AND NOT PlayerCloseInCopCar()
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_NERD", "MEJ1_NERD_1", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
							ELIF ((i = 1 AND NOT IS_PED_UNINJURED(piKidnapper[0]))
							OR (i = 2 AND NOT IS_PED_UNINJURED(piKidnapper[0]) AND NOT IS_PED_UNINJURED(piKidnapper[1]))) AND NOT PlayerCloseInCopCar()
								ADD_PED_FOR_DIALOGUE(pedConvStruct, PASSENGER_ID, piKidnapper[i], "MEJimmyKidnapper2", TRUE)
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_NERD", "MEJ1_NERD_2", CONV_PRIORITY_MEDIUM)
							ELIF (i = 1 AND IS_PED_UNINJURED(piKidnapper[0]))
							OR (i = 2 AND (IS_PED_UNINJURED(piKidnapper[0]) OR IS_PED_UNINJURED(piKidnapper[1])))
								MAKE_PED_SCREAM(piKidnapper[i])
							ENDIF
						ENDIF
					ENDREPEAT
					SAFE_REMOVE_BLIP(biObjective)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(viKidnapCar, FALSE)
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					iCurrentFootConv = 0
					iFootConvTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					IF IS_PED_UNINJURED(piJimmy) AND
					(IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_HUNG_UP, KIDNAPPERS_STUCK_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_JAMMED, KIDNAPPERS_STUCK_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_ON_ROOF, KIDNAPPERS_STUCK_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(viKidnapCar, VEH_STUCK_ON_SIDE, KIDNAPPERS_STUCK_TIME))
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) > (fReturnDistance + 1.0)
							biObjective = CREATE_PED_BLIP(piJimmy, TRUE, TRUE)
							PRINT_NOW("MEJ1_07", DEFAULT_GOD_TEXT_TIME, 1)	// Go to ~b~Jimmy.
							CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
							IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_STOP_KIDNAPPERS")
								STOP_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
							ENDIF
							bGoToJimmy = TRUE
							/*SEQUENCE_INDEX siLeave
							OPEN_SEQUENCE_TASK(siLeave)
								IF IS_PED_IN_VEHICLE(piJimmy, viKidnapCar)
									TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_RESUME_IF_INTERRUPTED | ECF_DONT_CLOSE_DOOR)
								ENDIF
								TASK_COWER(NULL)
							CLOSE_SEQUENCE_TASK(siLeave)
							TASK_PERFORM_SEQUENCE(piJimmy, siLeave)
							CLEAR_SEQUENCE_TASK(siLeave)*/
							//eJimmysAnimStage = JA_EXIT_BACK
							bExitBack = TRUE
							iFootConvTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
							missionSubstate = MSS_ACTIVE_THREE
						ELSE
							//TASK_LEAVE_ANY_VEHICLE(piJimmy)
							//eJimmysAnimStage = JA_EXIT_BACK
							bExitBack = TRUE
							IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_STOP_KIDNAPPERS")
								STOP_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
							ENDIF
							PlayerCollectsJimmy()
						ENDIF
					ELSE
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viKidnapCar)
							//eJimmysAnimStage = JA_EXIT_BACK
							bExitBack = TRUE
							IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_STOP_KIDNAPPERS")
								STOP_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
							ENDIF
							PlayerCollectsJimmy()
						ELSE
							biObjective = CREATE_VEHICLE_BLIP(viKidnapCar)
							PRINT_NOW("MEJ1_03", DEFAULT_GOD_TEXT_TIME, 1)	// Go to the ~b~van.
							CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
							IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_STOP_KIDNAPPERS")
								STOP_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
							ENDIF
							iFootConvTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
							missionSubstate = MSS_ACTIVE_THREE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_THREE
			//ManageKillingKidnapper()
			ManageFootChaseLines()
			PlayJimmyTiedLines()
			IF IS_VEHICLE_OK(viKidnapCar) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
				IF bShowReturnToVanMessage AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) > fVanWarnDistance
					PRINT_NOW("MEJ1_08", DEFAULT_GOD_TEXT_TIME, 1)	// Return to the ~b~van.
					CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
					bShowReturnToVanMessage = FALSE
				ENDIF
				//IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF bGoToJimmy
						IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) < fReturnDistance
							CLEAR_PED_TASKS_IMMEDIATELY(piJimmy)
							PlayerCollectsJimmy()
						ENDIF
					ELIF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), viKidnapCar) < (fReturnDistance - 1)
						//TASK_LEAVE_ANY_VEHICLE(piJimmy)
						//eJimmysAnimStage = JA_EXIT_BACK
						bExitBack = TRUE
						PlayerCollectsJimmy()
					ENDIF
				/*ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viKidnapCar) AND IS_PED_IN_VEHICLE(piJimmy, viKidnapCar)
					PlayerCollectsJimmy()
				ENDIF*/
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_FOCUS_CAM")
		IF IS_GAMEPLAY_HINT_ACTIVE()
			START_AUDIO_SCENE("M_E_JIMMY_FOCUS_CAM")
		ENDIF
	ELSE
		IF NOT IS_GAMEPLAY_HINT_ACTIVE()
			STOP_AUDIO_SCENE("M_E_JIMMY_FOCUS_CAM")
		ENDIF
	ENDIF
	
	IF IS_PED_UNINJURED(piJimmy)
		SET_PED_RESET_FLAG(piJimmy, PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_RESET_FLAG(piJimmy, PRF_DisableVehicleDamageReactions, TRUE)
		SET_PED_RESET_FLAG(piJimmy, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF
	
	ControlJimmysAnims()
	//PlayKillLines()
	TooFarFromJimmy()
ENDPROC

PROC CheckAtDestination()
	VECTOR vDest
	IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
		vDest = vHotelLocation
	ELSE
		vDest = vHomeLocation
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
	OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDest) < LOCATE_SIZE_MISSION_TRIGGER)//5.0)
		BOOL bIsOnGround
		bIsOnGround = FALSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viTemp)
				IF IS_VEHICLE_ON_ALL_WHEELS(viTemp)
					bIsOnGround = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
				bIsOnGround = TRUE
			ENDIF
		ENDIF
		IF bIsOnGround
			missionSubstate = MSS_PREPARE
			IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
				missionState = MS_AT_HOTEL
			ELSE
				missionState = MS_AT_HOME
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player drives home with Jimmy
PROC DRIVE_HOME()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEJ1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering Drive home stage")
			SAFE_REMOVE_BLIP(biObjective)
			IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
				biObjective = CREATE_COORD_BLIP(vHotelLocation)
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA((vHotelLocation-<<10,10,10>>), (vHotelLocation+<<10,10,10>>),FALSE)
				CLEAR_AREA_OF_VEHICLES(vHotelLocation, 10)
			ELSE
				biObjective = CREATE_COORD_BLIP(vHomeLocation)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_STOP_KIDNAPPERS")
				STOP_AUDIO_SCENE("M_E_JIMMY_STOP_KIDNAPPERS")
			ENDIF
			IF bShowHomeObjective
				PRINT_NOW("MEJ1_04", DEFAULT_GOD_TEXT_TIME, 1)	// Go ~y~home.
				CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
				START_AUDIO_SCENE("M_E_JIMMY_DRIVE_HOME")
				bShowHomeObjective = FALSE
				bShowReturnObjective = TRUE
				iCurrentAimLine = 0
				eDriveConvState = DCS_NULL
			ENDIF
			IF IS_PED_UNINJURED(piJimmy) AND NOT IS_PED_GROUP_MEMBER(piJimmy, PLAYER_GROUP_ID())
				SET_PED_AS_GROUP_MEMBER(piJimmy, PLAYER_GROUP_ID())
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(piJimmy, VS_FRONT_RIGHT)
				SET_PED_NEVER_LEAVES_GROUP(piJimmy, TRUE)
				SET_PED_DIES_INSTANTLY_IN_WATER(piJimmy, FALSE)
				SET_ENTITY_PROOFS(piJimmy, TRUE, FALSE, FALSE, FALSE, TRUE)
				SET_PED_CAN_RAGDOLL(piJimmy, TRUE)
			ENDIF
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
				ManageKillingKidnapper()
				ManageDriveConversations()
				ManageCrimeLines()
				ManageCrashLines()
				ManageStationaryLines()
				ManageFootChaseLines()
				ManageShootingLines()
				GetCurrentPlayersCar()
				JimmyLeavesDrowningCar()
				CheckAtDestination()
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) > fWarnDistance
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					missionSubstate = MSS_PREPARE
					missionState = MS_RETURN_TO_JIMMY
				ENDIF
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					missionSubstate = MSS_PREPARE
					missionState = MS_LOSE_COPS
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	ControlJimmysAnims()
	TooFarFromJimmy()
ENDPROC

/// PURPOSE:
///    The player returns to Jimmy after having gone too far from him
PROC RETURN_TO_JIMMY()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEJ1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering Return to Jimmy stage")
			SAFE_REMOVE_BLIP(biObjective)
			biObjective = CREATE_PED_BLIP(piJimmy, TRUE, TRUE)
			IF bShowReturnObjective
				PRINT_NOW("MEJ1_05", DEFAULT_GOD_TEXT_TIME, 1)	// Return to ~b~Jimmy.
				CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
				bShowReturnObjective = FALSE
			ENDIF
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			JimmyLeavesDrowningCar()
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) < fReturnDistance
					CLEAR_THIS_PRINT("MEJ1_05")	// Return to ~b~Jimmy.
					missionSubstate = MSS_PREPARE
					IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
						missionState = MS_DRIVE_HOME
					ELSE
						missionState = MS_LOSE_COPS
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	TooFarFromJimmy()
ENDPROC

/// PURPOSE:
///    The player must lose their wanted level to complete the mission
PROC LOSE_COPS()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEJ1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering Lose cops stage")
			SAFE_REMOVE_BLIP(biObjective)
			PRINT_NOW("MEJ1_06", DEFAULT_GOD_TEXT_TIME, 1)	// Lose the cops.
			CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
			IF IS_PED_UNINJURED(piJimmy) AND NOT IS_PED_GROUP_MEMBER(piJimmy, PLAYER_GROUP_ID())
				SET_PED_AS_GROUP_MEMBER(piJimmy, PLAYER_GROUP_ID())
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(piJimmy, VS_FRONT_RIGHT)
				SET_PED_NEVER_LEAVES_GROUP(piJimmy, TRUE)
				SET_PED_DIES_INSTANTLY_IN_WATER(piJimmy, FALSE)
			ENDIF
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
				ManageCrashLines()
				ManageStationaryLines()
				JimmyLeavesDrowningCar()
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_HOME
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piJimmy) > fWarnDistance
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					missionSubstate = MSS_PREPARE
					missionState = MS_RETURN_TO_JIMMY
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	ControlJimmysAnims()
	
	IF IS_PED_UNINJURED(piJimmy)
		SET_PED_RESET_FLAG(piJimmy, PRF_CannotBeTargetedByAI, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    The player arrives home with Jimmy
PROC AT_HOME()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
		SET_PED_RESET_FLAG(piJimmy, PRF_SearchForClosestDoor, TRUE )
		VEHICLE_INDEX viTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SWITCH missionSubstate
			CASE MSS_PREPARE
				CPRINTLN(DEBUG_MISSION,"Entering At home stage")
				KILL_FACE_TO_FACE_CONVERSATION()
				KILL_PHONE_CONVERSATION()
				IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_DRIVE_HOME")
					STOP_AUDIO_SCENE("M_E_JIMMY_DRIVE_HOME")
				ENDIF
				SAFE_REMOVE_BLIP(biObjective)
                RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, FALSE)
                /*ciHomeCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -840.7, 185.3, 78.4 >>, << -14.0, 0.0, -110.4 >>, 48.4)
                SET_CAM_ACTIVE(ciHomeCam, TRUE)
                RENDER_SCRIPT_CAMS(TRUE, FALSE)*/
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ELSE
					/*SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vOnFootEndPosMichael)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fOnFootEndRotMichael)
					CLEAR_PED_TASKS_IMMEDIATELY(piJimmy)
					SET_ENTITY_COORDS_GROUNDED(piJimmy, vOnFootEndPosJimmy)
					SET_ENTITY_HEADING(piJimmy, fOnFootEndRotJimmy)*/
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vCutsceneAreaPos1, vCutsceneAreaPos2, fCutsceneAreaWidth, vCarMovePos, fCarMoveHead, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piJimmy)
					//TASK_TURN_PED_TO_FACE_ENTITY(piJimmy, PLAYER_PED_ID())
					SEQUENCE_INDEX siJimmy
					OPEN_SEQUENCE_TASK(siJimmy)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_RUN)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 10000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vKitchen, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(siJimmy)
					TASK_PERFORM_SEQUENCE(piJimmy, siJimmy)
					CLEAR_SEQUENCE_TASK(siJimmy)
				ENDIF
				IF IS_PED_UNINJURED(piJimmy) AND IS_PED_IN_GROUP(piJimmy)
					REMOVE_PED_FROM_GROUP(piJimmy)
				ENDIF
				iConvFailedTimer = -1
				bSkipped = FALSE
				missionSubstate = MSS_ACTIVE_ONE
			BREAK
			
			CASE MSS_ACTIVE_ONE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piJimmy, 5.0)
						IF IS_VEHICLE_OK(viKidnapCar) AND IS_PED_UNINJURED(piJimmy) AND IS_PED_IN_VEHICLE(piJimmy, viKidnapCar) AND IS_VEHICLE_SEAT_FREE(viKidnapCar, VS_FRONT_RIGHT)
							IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HOME2", CONV_PRIORITY_MEDIUM)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								missionSubstate = MSS_ACTIVE_TWO
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HOME", CONV_PRIORITY_MEDIUM)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								missionSubstate = MSS_ACTIVE_TWO
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					bSkipped = TRUE
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_TWO
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
					/*IF NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_PAUSE) AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
					AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)*/
						IF IS_VEHICLE_STOPPED(viTemp)
							//TASK_LEAVE_ANY_VEHICLE(piJimmy)
							SEQUENCE_INDEX siLeaveVehicle
							OPEN_SEQUENCE_TASK(siLeaveVehicle)
								TASK_PAUSE(NULL, 6500)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vKitchen, PEDMOVE_WALK)
							CLOSE_SEQUENCE_TASK(siLeaveVehicle)
							TASK_PERFORM_SEQUENCE(piJimmy, siLeaveVehicle)
							CLEAR_SEQUENCE_TASK(siLeaveVehicle)
							iCutTimer = GET_GAME_TIMER() + 25000
							missionSubstate = MSS_ACTIVE_THREE
						ENDIF
					//ENDIF
				ELSE
					iCutTimer = GET_GAME_TIMER() + 25000
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					bSkipped = TRUE
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_THREE
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					bSkipped = TRUE
				ENDIF
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND (IS_ENTITY_IN_RANGE_COORDS_2D(piJimmy, vJimmyFinalDest, 2.0) OR IS_ENTITY_IN_RANGE_COORDS_2D(piJimmy, vJimmyFinalDestRear, 2.0))) // Normal completion
				OR bSkipped // Player has skipped
				OR (GET_GAME_TIMER() > iCutTimer) // Emergency timeout
					#IF IS_DEBUG_BUILD
						IF GET_GAME_TIMER() > iCutTimer
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up from timeout")
						ELIF bSkipped
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up from skip")
						ELSE
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up normally")
						ENDIF
					#ENDIF
					IF bSkipped
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						IF IS_PED_UNINJURED(piJimmy)
							CLEAR_PED_TASKS(piJimmy)
							SET_ENTITY_COORDS(piJimmy, vKitchen)
							SET_ENTITY_HEADING(piJimmy, 90)
						ENDIF
						WAIT(1000)
					ENDIF
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
	                //RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
	                RC_END_CUTSCENE_MODE()
	                //SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	                //DESTROY_CAM(ciHomeCam)
					Mission_Passed()
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF
ENDPROC

PROC AT_HOTEL()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piJimmy)
		SET_PED_RESET_FLAG(piJimmy, PRF_SearchForClosestDoor, TRUE )
		VEHICLE_INDEX viTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SWITCH missionSubstate
			CASE MSS_PREPARE
				CPRINTLN(DEBUG_MISSION,"Entering At hotel stage")
				KILL_FACE_TO_FACE_CONVERSATION()
				KILL_PHONE_CONVERSATION()
				IF IS_AUDIO_SCENE_ACTIVE("M_E_JIMMY_DRIVE_HOME")
					STOP_AUDIO_SCENE("M_E_JIMMY_DRIVE_HOME")
				ENDIF
				SAFE_REMOVE_BLIP(biObjective)
                RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, FALSE, TRUE, TRUE, FALSE)
                /*ciHomeCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -840.7, 185.3, 78.4 >>, << -14.0, 0.0, -110.4 >>, 48.4)
                SET_CAM_ACTIVE(ciHomeCam, TRUE)
                RENDER_SCRIPT_CAMS(TRUE, FALSE)*/
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), piJimmy, -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ELSE
					/*SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vOnFootEndPosMichael)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fOnFootEndRotMichael)
					CLEAR_PED_TASKS_IMMEDIATELY(piJimmy)
					SET_ENTITY_COORDS_GROUNDED(piJimmy, vOnFootEndPosJimmy)
					SET_ENTITY_HEADING(piJimmy, fOnFootEndRotJimmy)*/
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piJimmy)
					//TASK_TURN_PED_TO_FACE_ENTITY(piJimmy, PLAYER_PED_ID())
					SEQUENCE_INDEX siJimmy
					OPEN_SEQUENCE_TASK(siJimmy)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_RUN)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 10000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHotel, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(siJimmy)
					TASK_PERFORM_SEQUENCE(piJimmy, siJimmy)
					CLEAR_SEQUENCE_TASK(siJimmy)
				ENDIF
				IF IS_PED_UNINJURED(piJimmy) AND IS_PED_IN_GROUP(piJimmy)
					REMOVE_PED_FROM_GROUP(piJimmy)
				ENDIF
				iConvFailedTimer = -1
				bSkipped = FALSE
				
				IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()))
				OR (IS_PED_IN_ANY_VEHICLE(piJimmy, TRUE) AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(piJimmy))
				OR IS_PED_RAGDOLL(PLAYER_PED_ID())
				OR IS_PED_RAGDOLL(piJimmy)
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					
					WAIT(1000)
					
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), <<-1374.11, 363.81, 64.25>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 117.46)
					IF IS_PED_UNINJURED(piJimmy)
						SET_ENTITY_COORDS_NO_OFFSET(piJimmy, <<-1375.82, 362.30, 64.17>>)
						SET_ENTITY_HEADING(piJimmy, -49.27)
					ENDIF
					
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piJimmy)
					SEQUENCE_INDEX siJimmy
					OPEN_SEQUENCE_TASK(siJimmy)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 10000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHotel, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(siJimmy)
					TASK_PERFORM_SEQUENCE(piJimmy, siJimmy)
					CLEAR_SEQUENCE_TASK(siJimmy)
					
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
				ENDIF
				missionSubstate = MSS_ACTIVE_ONE
			BREAK
			
			CASE MSS_ACTIVE_ONE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					//IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piJimmy, 5.0)
						IF IS_VEHICLE_OK(viKidnapCar) AND IS_PED_UNINJURED(piJimmy) AND IS_PED_IN_VEHICLE(piJimmy, viKidnapCar) AND IS_VEHICLE_SEAT_FREE(viKidnapCar, VS_FRONT_RIGHT)
							IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HOME2", CONV_PRIORITY_MEDIUM)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								missionSubstate = MSS_ACTIVE_TWO
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_HOME", CONV_PRIORITY_MEDIUM)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								missionSubstate = MSS_ACTIVE_TWO
							ENDIF
						ENDIF
					//ENDIF
				ENDIF
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					bSkipped = TRUE
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_TWO
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
					/*IF NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_PAUSE) AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
					AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) AND NOT IsPedPerformingTask(piJimmy, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)*/
						IF IS_VEHICLE_STOPPED(viTemp)
							//TASK_LEAVE_ANY_VEHICLE(piJimmy)
							SEQUENCE_INDEX siLeaveVehicle
							OPEN_SEQUENCE_TASK(siLeaveVehicle)
								TASK_PAUSE(NULL, 6500)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHotel, PEDMOVE_WALK)
							CLOSE_SEQUENCE_TASK(siLeaveVehicle)
							TASK_PERFORM_SEQUENCE(piJimmy, siLeaveVehicle)
							CLEAR_SEQUENCE_TASK(siLeaveVehicle)
							iCutTimer = GET_GAME_TIMER() + 25000
							missionSubstate = MSS_ACTIVE_THREE
						ENDIF
					//ENDIF
				ELSE
					iCutTimer = GET_GAME_TIMER() + 25000
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					bSkipped = TRUE
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					missionSubstate = MSS_ACTIVE_THREE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_THREE
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					SAFE_FADE_SCREEN_OUT_TO_BLACK()
					bSkipped = TRUE
				ENDIF
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND (IS_ENTITY_IN_RANGE_COORDS_2D(piJimmy, vHotel, 4.0))) // Normal completion
				OR bSkipped // Player has skipped
				OR (GET_GAME_TIMER() > iCutTimer) // Emergency timeout
					#IF IS_DEBUG_BUILD
						IF GET_GAME_TIMER() > iCutTimer
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up from timeout")
						ELIF bSkipped
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up from skip")
						ELSE
							CPRINTLN(DEBUG_MISSION, "AT_HOME: ACTIVE_THREE cleaning up normally")
						ENDIF
					#ENDIF
					IF bSkipped
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						WAIT(1000)
					ENDIF
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					ENDIF
	                //RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SAFE_DELETE_PED(piJimmy)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
	                RC_END_CUTSCENE_MODE()
	                //SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	                //DESTROY_CAM(ciHomeCam)
					Mission_Passed()
				ENDIF
			BREAK

		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades out and fails the mission with a delay if needed
PROC FAIL_DELAY()
	SWITCH missionSubstate
		CASE MSS_PREPARE
			
			CPRINTLN(DEBUG_MISSION,"Entering Fail delay stage")
			CLEAR_PRINTS()
			SAFE_REMOVE_BLIP(biObjective)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
			// set if we have fail dialogue to play
			bDoneFailDialogue = TRUE
			IF missionFailedReason = MFR_JIMMY_HURT
				bDoneFailDialogue = FALSE
				iConvFailedTimer = -1
			ENDIF
			
			STRING sFailReason
	
			SWITCH missionFailedReason
				CASE MFR_NONE
					
				BREAK
				
				CASE MFR_JIMMY_HURT
					sFailReason = "MEJ1_FAIL4"	// ~r~You injured Jimmy.
				BREAK
				
				CASE MFR_JIMMY_DEAD
					sFailReason = "MEJ1_FAIL1"	// ~r~Jimmy died.
				BREAK
				
				CASE MFR_JIMMY_KIDNAPPED
					sFailReason = "MEJ1_FAIL2"	// ~r~The kidnapper escaped with Jimmy.
				BREAK
				
				CASE MFR_JIMMY_LEFT
					sFailReason = "MEJ1_FAIL3"	// ~r~You abandoned Jimmy.
				BREAK
				
				CASE MFR_KIDNAPPER_DIED
					sFailReason = "MEJ1_FAIL5"	// ~s~A kidnapper died.
				BREAK
			ENDSWITCH
			
			IF missionFailedReason = MFR_NONE
				Mission_Flow_Mission_Failed()
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)
			ENDIF

			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// This must only take 1 frame and terminate the thread
			
				RemovePedsAndVehicles(TRUE)
				Mission_Cleanup(TRUE)
				
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
				IF bDoneFailDialogue = FALSE
					IF IS_PED_UNINJURED(piJimmy)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							/*IF iConvFailedTimer = -1
								iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
							ENDIF*/
							IF CREATE_CONVERSATION(pedConvStruct, "MEJ1AUD", "MEJ1_INJURE", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								bDoneFailDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT

	CPRINTLN(DEBUG_MISSION,"...ME_Jimmy1 Launched")
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		CPRINTLN(DEBUG_MISSION,"...ME_Jimmy1 Force Cleanup")
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup(TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Uncomment to display polys
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	WHILE (TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_PO")
		// Uncomment to display polys
		//DRAW_DEBUG_POLY_ANGLED_AREA(vAreaAPos1, vAreaAPos2, fAreaWidth, 255, 0, 0, 255, TRUE, FALSE)
		//DRAW_DEBUG_POLY_ANGLED_AREA(vAreaAPos1Alt, vAreaAPos2Alt, fAreaWidthAlt, 0, 255, 255, 255, TRUE, FALSE)
		//DRAW_DEBUG_POLY_ANGLED_AREA(vAreaBPos1, vAreaBPos2, fAreaWidth, 0, 255, 0, 255, TRUE, FALSE)
		//DRAW_DEBUG_POLY_ANGLED_AREA(vAreaBPos1Alt, vAreaBPos2Alt, fAreaWidthAlt, 0, 0, 255, 255, TRUE, FALSE)
		SWITCH missionState
			CASE MS_INIT
				INIT()
			BREAK
			
			CASE MS_DRIVE_TO_LOCATION
				DRIVE_TO_LOCATION()
			BREAK
			
			CASE MS_RESCUE_JIMMY
				RESCUE_JIMMY()
			BREAK
			
			CASE MS_DRIVE_HOME
				DRIVE_HOME()
			BREAK
			
			CASE MS_RETURN_TO_JIMMY
				RETURN_TO_JIMMY()
			BREAK
			
			CASE MS_LOSE_COPS
				LOSE_COPS()
			BREAK
			
			CASE MS_AT_HOME
				AT_HOME()
			BREAK
			
			CASE MS_AT_HOTEL
				AT_HOTEL()
			BREAK
			
			CASE MS_FAIL_DELAY
				FAIL_DELAY()
			BREAK
		ENDSWITCH
		
		CheckJimmy()
		CheckKidnappers()
		
		#IF IS_DEBUG_BUILD
			IF missionState <> MS_FAIL_DELAY
				// Check for Pass
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					CLEAR_PRINTS()
					DebugPassDealWithJimmy()
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					Mission_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					SetFailReason(MFR_NONE)
				ENDIF
				
				CheckDebugKeys()
			ENDIF
		#ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
