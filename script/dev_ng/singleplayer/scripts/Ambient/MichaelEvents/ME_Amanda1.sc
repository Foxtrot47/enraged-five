
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "RC_Helper_Functions.sch"
USING "rgeneral_include.sch"
USING "taxi_functions.sch"
#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
#ENDIF
USING "replay_public.sch"
USING "RC_Threat_public.sch"
USING "family_public.sch"
USING "shared_hud_displays.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	ME_Amanda1.sc
//		AUTHOR			:	Joe Binks
//		DESCRIPTION		:	Amanda has been arrested for shoplifting and placed in the back
//                          of a police car. Michael must steal the cop car and escape 
//                          before the cops drive away.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

// ===========================================================================================================
//		Variables / Constants etc
// ===========================================================================================================

ENUM MISSION_STATE
	MS_INIT,
	MS_DRIVE_TO_SHOP,
	MS_RESCUE_AMANDA,
	MS_LOSE_COPS,
	MS_DRIVE_HOME,
	MS_RETURN_TO_AMANDA,
	MS_AT_HOME,
	MS_AT_HOTEL,
	MS_FAIL_DELAY
ENDENUM

ENUM MISSION_SUBSTATE
	MSS_PREPARE,
	MSS_ACTIVE_ONE,
	MSS_ACTIVE_TWO,
	MSS_ACTIVE_THREE,
	MSS_ACTIVE_FOUR
ENDENUM

ENUM MISSION_FAILED_REASON
	MFR_NONE,
	MFR_AMANDA_HURT,
	MFR_AMANDA_DEAD,
	MFR_AMANDA_ARRESTED,
	MFR_AMANDA_LEFT,
	MFR_TRASHED_CAR
ENDENUM

ENUM GRIEF_CONV_STATE
	GCS_READY,
	GCS_PLAYING,
	GCS_FINISH
ENDENUM

ENUM ARREST_SCENE_STAGE
	ASS_STANDING,
	ASS_ARREST,
	ASS_READY,
	ASS_POST_ARREST,
	ASS_BREAKOUT,
	ASS_END
ENDENUM

ENUM COP_CHAT_STAGE
	CCS_NOT_STARTED,
	CCS_INIT,
	CCS_WAIT_FOR_REINIT,
	CCS_PLAYING_NO_SUBS,
	CCS_PLAYING_SUBS,
	CCS_DONE
ENDENUM

CONST_INT MICHAEL_ID 0
CONST_INT AMANDA_ID 4//3
CONST_INT AMANDA_PHONE_ID 4
CONST_INT SHOPKEEPER_ID 5
CONST_INT COP_ID 6
structPedsForConversation pedConvStruct
//STRING sPlayerGestureGroup = "PlayerGestureGroup"

INT iArrestScene
ARREST_SCENE_STAGE eArrestSceneStage = ASS_STANDING
VECTOR vArrestScenePos = <<-836.87, -153.92, 34.77>>//<<-836.8745, -154.0169, 34.671>> 
VECTOR vArrestSceneRot = <<-1, -2, 114>>//<<0,0,112.8727>>
STRING sCopStand = "stand_loop_cop"
STRING sShopStand = "stand_loop_owner"
STRING sAmandaStand = "stand_loop_ama"
STRING sCopCarStand = "stand_loop_car"
STRING sCopArrest = "arrest_cop"
STRING sShopArrest = "arrest_owner"
STRING sAmandaArrest = "arrest_ama"
STRING sCopCarArrest = "arrest_car"
STRING sCopPstArrest = "pst_arrest_loop_cop"
STRING sShopPstArrest = "pst_arrest_loop_owner"
CONST_FLOAT fStartArrestAnimDist 90.0 // Doubled if in vehicle, but lines are not subtitled until you are within this distance
COP_CHAT_STAGE copChatState
TEXT_LABEL_23 tCopCharConvLabel

/*VECTOR vOnFootEndPosMichael = <<-823.5155, 181.1892, 70.6611>>
FLOAT fOnFootEndRotMichael = 129.8824
VECTOR vOnFootEndPosAmanda = <<-824.33, 180.29, 71.55>>
FLOAT fOnFootEndRotAmanda = -31.51*/
/*FLOAT fOnFootEndCamHeading = 129.9509
FLOAT fOnFootEndCamPitch = -22.8990*/
VECTOR vAmandaFinalDest = <<-813.367920,179.495605,71.159195>>//<<-814.317505,179.017578,71.159195>>//<<-807.178955,180.306046,71.153076>>
VECTOR vAmandaFinalDestRear = <<-806.181396,184.289764,71.347694>>
VECTOR vKitchen = <<-801.9036, 182.9307, 71.6055>>
VECTOR vHotel = <<-1368.278687,355.901825,63.081089>>//<<-1320.7941, 380.0252, 68.0737>>

CONST_INT NUM_DRIVE_CONVS 4
CONST_INT iDriveConvTimer 2000
STRING sDriveConvs[NUM_DRIVE_CONVS]
INT iCurrentDriveConv = -1
INT iPreviousDriveConv = -1

CONST_INT NUM_START_LINES 1//5
CONST_INT iStartLinesTimer 10000
STRING sStartLines//[NUM_START_LINES]
INT iCurrentStartLine = -1
INT iPreviousStartLine = -1

INT iConversationTimer

CAMERA_INDEX ciHomeCam

CONST_INT DRIVING_LINES_TIMEOUT 9000//3000

INT iGriefingTimer = 0
CONST_INT GRIEFING_TIME 7000
TEXT_LABEL_23 sSceneConvLine//[NUM_SCENE_LINES]
GRIEF_CONV_STATE eGriefState = GCS_READY

CONST_INT NUM_CRASH_LINES 4
INT iCrashLineTimer
STRING sCrashLines[NUM_CRASH_LINES]
INT iCurrentCrashLine = 0

CONST_INT NUM_CRIME_LINES 4
//INT iCrimeLineTimer
//STRING sCrimeLines[NUM_CRIME_LINES]
//INT iCurrentCrimeLine = 0

CONST_INT NUM_STILL_LINES 3
INT iStillLineTimer
INT iStillTime
CONST_INT iStillTimeout 180//60
STRING sStillLines[NUM_STILL_LINES]
INT iCurrentStillLine = 0

INT iAbandonLineTimer

CONST_INT NUM_SIREN_LINES 3
INT iSirenLineTimer
STRING sSirenLines[NUM_SIREN_LINES]
INT iCurrentSirenLine = 0
BOOL bIsSirenOn = FALSE
BOOL bWasSirenOn = FALSE

CONST_INT NUM_LOSECOP_LINES 9
INT iLoseCopsTimer
STRING sLoseCopLines[NUM_LOSECOP_LINES]
INT iCurrentLoseCopLine = 0
CONST_INT NUM_LOSECOP_REPLY 4
STRING sLoseCopReply[NUM_LOSECOP_LINES]
INT iSelectedReply = 0
INT iPreviousReply = 0

CONST_INT NUM_NOT_HOME_LINES 3
INT iNotHomeTimer
STRING sNotHomeLines[NUM_NOT_HOME_LINES]
INT iCurrentNotHomeLine = 0

INT iCopCarDamageTimer = 0

CONST_FLOAT fSceneConvRadius 20.0
//BOOL bAllowSceneConv = TRUE

CONST_FLOAT fAmandaConvRadius 5.0
//BOOL bAllowAmandaConv = TRUE
CONST_INT NUM_AMANDA_LINES  5
INT iAmandaLineTimer
STRING sAmandaLines[NUM_AMANDA_LINES]
INT iCurrentAmandaLine = 0

STRING sMissionAnimDict = "rcmme_amanda1"
STRING sAmandaMoveDict = "move_characters@amanda@bag"

VECTOR vDisableNavmeshMin = <<-839.543579,-161.144562,36.821663>>
VECTOR vDisableNavmeshMax = <<-837.645996,-155.749100,37.943207>>

VECTOR vShopLocation = << -839.0088, -161.5691, 36.8037 >>
VECTOR vHomeLocation = <<-825.503540,179.026611,70.369011>>//<<-822.573730,182.467636,70.849342>>//<<-829.0000, 171.0000, 70.0000>>
VECTOR vHotelLocation = <<-1378.4454, 367.8120, 63.0445>>//<<-1317.6356, 391.5271, 68.6091>>
CONST_FLOAT fSpawnSetupDist 200.0
CONST_FLOAT fBlipAmandaDist 30.0
CONST_FLOAT fFailDistance 200.0
CONST_FLOAT fWarnDistance 15.0
CONST_FLOAT fReturnDistance 5.0
INT iDriveToShopTime 
INT iFailTimer = 0
INT iCopDrivingTimer = 0
INT iMissionTimer = 0
//INT iStartDay = -1
CONST_INT iCopChaseTime 10000
CONST_INT MIN_DRIVE_TO_SHOP_TIME 30000
BOOL bPlayBeeps = TRUE
INT iBeepTimer
BOOL bStartedBeepTimer = FALSE

CONST_INT AT_SHOP_FAIL_TIME 25000	// The amount of time it takes for the cop/shopkeeper conversation to play

INT iCopGunTimer

MISSION_STATE missionState = MS_INIT
MISSION_SUBSTATE missionSubstate = MSS_PREPARE

MISSION_FAILED_REASON missionFailedReason = MFR_NONE

BLIP_INDEX biObjective

PED_INDEX piAmanda
REL_GROUP_HASH rghPlayer

VEHICLE_INDEX viCopCar
MODEL_NAMES mnCopCar = POLICE
VECTOR vCopCarPos = <<-840.28, -153.57, 37.41>>//<<-840.94, -153.84, 37.39>>
FLOAT fCopCarHeading = 298.55//299.88

PED_INDEX piCop
MODEL_NAMES mnCop = S_M_Y_COP_01
VECTOR vCopPos = <<-839.05, -158.35, 37.88>>
FLOAT fCopHeading = -163.87

//VECTOR vAmandaPos = <<-838.05, -157.35, 37.88>>

/*OBJECT_INDEX oiNotebook
MODEL_NAMES mnNotebook = PROP_NOTEPAD_01
VECTOR vNotebookOffset = <<0.1476, -0.0042, -0.0350>>
VECTOR vNotebookRotate = <<373.8389, -141.4298, -12.1299>>*/

PED_INDEX piShopkeeper
MODEL_NAMES mnShopkeeper = A_M_Y_BUSINESS_01
VECTOR vShopkeeperPos = <<-838.82, -159.50, 37.86>>
FLOAT fShopkeeperHeading = 11.64

VEHICLE_INDEX viPlayersVehicle
CONST_INT iCheckVehicle 1000
INT iCheckVehicleTimer = 0
CONST_INT NUM_ROOF_LINES 3
INT iCurrentRoofLine = -1
INT iPreviousRoofLine = -1
STRING sRoofLines[NUM_ROOF_LINES]
CONST_INT iRoofTime 5000
INT iRoofConvTimer
INT iInCarConvTimer

BOOL bShowHomeObjective = TRUE
BOOL bShowReturnObjective = TRUE
BOOL bStartCopToCar = TRUE
//BOOL bAllowLooking = TRUE
BOOL bAllowInCarConv = TRUE
BOOL bIsCarFrozen = TRUE
//BOOL bArrivedInVehicle = TRUE
BOOL bForceAmandaIntoGroup = FALSE
BOOL bPausedConversation = FALSE

BOOL bHandcuffSoundReady = FALSE
BOOL bLoadHandcuffSound = FALSE
INT iHandcuffSound
INT iHandCuffTimer

BOOL bShowingAmandasBag = TRUE

// variables for disabling the road the police car is on
VECTOR vRoadOffOne = <<-784.169678,-121.780930,35.823196>>
VECTOR vRoadOffTwo = <<-847.763611,-155.086975,37.793705>> 
FLOAT fRoadOffWidth = 6.0

// Timer to check if conversations have failed to trigger
INT iConvFailedTimer
INT iCopConvFailedTimer
INT iWalkToHouseFailed
//CONST_INT CONV_FAILED_TIME 5000

BOOL bNotBumpedYet = TRUE
INT iBumpTimer = -1

BOOL bDisplaySeatObjective = TRUE

// Clear this area if the player arrives home on foot
VECTOR vCutsceneAreaPos1 = <<-826.696777,178.020920,69.187263>>
VECTOR vCutsceneAreaPos2 = <<-817.752869,185.950363,72.353897>> 
FLOAT fCutsceneAreaWidth = 7.0
VECTOR vCarMovePos = <<-826.00, 157.66, 69.06>>
FLOAT fCarMoveHead = 273.51

// house and garage areas
/*VECTOR vGaragePos1 = <<-807.051025,188.957352,72.835045>>
VECTOR vGaragePos2 = <<-825.467041,182.292862,69.701797>> 
FLOAT fGarageWidth = 6.0
VECTOR vHousePos1 = <<-816.768799,171.898209,69.564308>>
VECTOR vHousePos2 = <<-793.081055,180.968567,81.835052>> 
FLOAT fHouseWidth = 20.0*/

/*VECTOR vOutsideGatePos1 = <<-856.688721,159.215210,63.143036>>
VECTOR vOutsideGatePos2 = <<-844.199646,159.172287,67.778503>> 
FLOAT fOutsideGateWidth = 8.0*/

CONST_INT CP_MISSION_START 0
CONST_INT CP_AT_SHOP 1
//CONST_INT CP_DRIVE_HOME 2
CONST_INT CP_MISSION_PASSED 2//3

#IF IS_DEBUG_BUILD
	MissionStageMenuTextStruct sSkipMenu[4]
#ENDIF

RC_CONV_RESTORE_STATE stateRestoreConversation
TEXT_LABEL_23 tSavedConversationRoot
TEXT_LABEL_23 tSavedConversationLabel

// ===========================================================================================================
//		Termination
// ===========================================================================================================
/// PURPOSE:
///    Deletes or releases all peds and vehicles created by the mission
/// PARAMS:
///    deleteAll - Should the peds and vehicles be deleted? TRUE if so
PROC RemovePedsAndVehicles(BOOL deleteAll = FALSE, BOOL bFailed = FALSE)
	IF deleteAll
		SAFE_DELETE_PED(piAmanda)
	ELSE
		IF IS_PED_UNINJURED(piAmanda) AND IS_PED_IN_GROUP(piAmanda)
			REMOVE_PED_FROM_GROUP(piAmanda)
		ENDIF
		IF bFailed
			SAFE_RELEASE_PED(piAmanda)
		ELSE
			IF IS_PED_UNINJURED(piAmanda) AND IS_ENTITY_AT_COORD(piAmanda, vHomeLocation, <<50,50,50>>)
				RELEASE_PED_TO_FAMILY_SCENE(piAmanda)
			ELSE
				SAFE_RELEASE_PED(piAmanda)
			ENDIF
		ENDIF
	ENDIF
	
	SAFE_REMOVE_BLIP(biObjective)
	
	IF deleteAll
		SAFE_DELETE_VEHICLE(viCopCar)
	ELSE
		IF IS_VEHICLE_OK(viCopCar)
			//SET_VEHICLE_CAN_SAVE_IN_GARAGE(viCopCar, TRUE)
		ENDIF
		SAFE_RELEASE_VEHICLE(viCopCar)
	ENDIF
	
	IF deleteAll
		SAFE_DELETE_VEHICLE(viPlayersVehicle)
	ELSE
		IF IS_VEHICLE_OK(viPlayersVehicle)
			//SET_VEHICLE_CAN_SAVE_IN_GARAGE(viPlayersVehicle, TRUE)
		ENDIF
		SAFE_RELEASE_VEHICLE(viPlayersVehicle)
	ENDIF
	
	//SAFE_DELETE_OBJECT(oiNotebook)
	
	IF DOES_ENTITY_EXIST(piCop)
		REMOVE_PED_FOR_DIALOGUE(pedConvStruct, COP_ID)
		BOOL bKeepTask = TRUE
		IF NOT IS_PED_INJURED(piCop)
			IF IS_PED_IN_COMBAT(piCop) AND NOT (GET_PEDS_CURRENT_WEAPON(piCop) = WEAPONTYPE_PISTOL)
				CLEAR_PED_TASKS(piCop)
				TASK_WANDER_STANDARD(piCop)
			/*ELIF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopAnim)
				bKeepTask = FALSE*/
			ENDIF
		ENDIF
		IF deleteAll
			SAFE_DELETE_PED(piCop)
		ELSE
			SAFE_RELEASE_PED(piCop, bKeepTask)
		ENDIF
	ENDIF
	
	IF deleteAll
		SAFE_DELETE_PED(piShopkeeper)
	ELSE
		SAFE_RELEASE_PED(piShopkeeper, FALSE)
	ENDIF
	
	STOP_AUDIO_SCENES()
	
	DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
	SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
	CLEAR_PED_NON_CREATION_AREA()
    REMOVE_SCENARIO_BLOCKING_AREAS()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Cleans up everything used by the mission
PROC Mission_Cleanup(BOOL bFailed = FALSE)

	CPRINTLN(DEBUG_MISSION,"...ME_Amanda1 Cleanup")
	RemovePedsAndVehicles(FALSE, bFailed)
	RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth)
	CLEAR_PRINTS()
	KILL_FACE_TO_FACE_CONVERSATION()
	SET_AUDIO_FLAG("WantedMusicOnMission", FALSE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------
/// PURPOSE:
///    Passes the mission
PROC Mission_Passed()
	
	CPRINTLN(DEBUG_MISSION,"...ME_Amanda1 Passed")

	//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(TRUE)
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

// ===========================================================================================================
//		Helper functions
// ===========================================================================================================
/// PURPOSE:
///    Plays dialogue of Michael talking to himself while driving to get Amanda
PROC PlayStartLines()
	IF iCurrentStartLine < NUM_START_LINES AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEA1_01")	// Go to ~y~Didier Sachs.
	AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShopLocation) > (fBlipAmandaDist*3.0))
		IF iPreviousStartLine = iCurrentStartLine
			iCurrentStartLine++
			iConversationTimer = GET_GAME_TIMER() + iStartLinesTimer
		ELSE
			IF GET_GAME_TIMER() > iConversationTimer
				//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_MICHAEL", sStartLines[iCurrentStartLine], CONV_PRIORITY_MEDIUM)
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_MICHAEL", sStartLines, CONV_PRIORITY_MEDIUM)
					iPreviousStartLine = iCurrentStartLine
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if it's ok to display subtitles for a conversation
/// RETURNS:
///    The appropriate subtitles state, based on whether or not an objective is being displayed
FUNC enumSubtitlesState CheckDisplaySubtitles()

	IF IS_MESSAGE_BEING_DISPLAYED()
		RETURN DO_NOT_DISPLAY_SUBTITLES
	ELSE
		RETURN DISPLAY_SUBTITLES
	ENDIF
ENDFUNC

/// PURPOSE:
///    Loads all the models and animations used in the shop scene
PROC LoadSceneModels()
	REQUEST_MODEL(mnCopCar)
	REQUEST_MODEL(mnCop)
	REQUEST_LOAD_MODEL(GET_NPC_PED_MODEL(CHAR_AMANDA))
	//REQUEST_MODEL(mnNotebook)
	REQUEST_MODEL(mnShopkeeper)
	REQUEST_ANIM_DICT(sMissionAnimDict)
	REQUEST_ANIM_DICT(sAmandaMoveDict)
ENDPROC

/// PURPOSE:
///    Suppresses the players vehicle model
PROC SUPPRESS_PLAYERS_VEHICLE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayerCar)
			MODEL_NAMES mnPlayerCar = GET_ENTITY_MODEL(viPlayerCar)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(mnPlayerCar, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the cop, shopkeeper, cop car and Amanda
PROC SpawnShopScene()
	IF NOT DOES_ENTITY_EXIST(viCopCar)
		IF HAS_MODEL_LOADED(mnCopCar)
			viCopCar = CREATE_VEHICLE(mnCopCar, vCopCarPos, fCopCarHeading)
			SET_VEHICLE_ENGINE_ON(viCopCar, TRUE, TRUE)
			FREEZE_ENTITY_POSITION(viCopCar, TRUE)
			SET_VEHICLE_CAN_SAVE_IN_GARAGE(viCopCar, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viCopCar, FALSE)
			SET_VEHICLE_EXTRA(viCopCar, VEHICLE_SETUP_FLAG_EXTRA_2, TRUE)
			bIsCarFrozen = TRUE
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCar)
			SET_ROADS_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth, FALSE, FALSE)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(piAmanda)
		IF IS_VEHICLE_OK(viCopCar) AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_AMANDA))
			CREATE_NPC_PED_INSIDE_VEHICLE(piAmanda, CHAR_AMANDA, viCopCar, VS_BACK_RIGHT)
			//CREATE_NPC_PED_ON_FOOT(piAmanda, CHAR_AMANDA, vAmandaPos)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_HAIR, 4, 0)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_TORSO, 5, 0)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_LEG, 5, 0)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_SPECIAL, 1, 0)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_BERD, 0, 0)
			SET_PED_PRELOAD_VARIATION_DATA(piAmanda, PED_COMP_DECL, 1, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_HAIR, 4, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_TORSO, 5, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_LEG, 5, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_DECL, 1, 0)
			SET_PED_PROP_INDEX(piAmanda, ANCHOR_EYES, 0)
			SET_PED_CAN_BE_TARGETTED(piAmanda, FALSE)
			SET_PED_CONFIG_FLAG(piAmanda, PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(piAmanda, PCF_GetOutBurningVehicle, FALSE)
			SET_PED_CONFIG_FLAG(piAmanda, PCF_OpenDoorArmIK, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piAmanda, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), rghPlayer)
			SET_PED_RELATIONSHIP_GROUP_HASH(piAmanda, rghPlayer)
			//SET_PED_DIES_INSTANTLY_IN_WATER(piAmanda, TRUE)
			SET_PED_DIES_IN_WATER(piAmanda, TRUE)
			SET_PED_DIES_IN_SINKING_VEHICLE(piAmanda, TRUE)
			SET_PED_CAN_PLAY_GESTURE_ANIMS(piAmanda, TRUE)
			eArrestSceneStage = ASS_STANDING
		ENDIF
	ENDIF
	
	//IF NOT DOES_ENTITY_EXIST(oiNotebook)
	IF NOT DOES_ENTITY_EXIST(piCop)
		IF /*HAS_MODEL_LOADED(mnNotebook) AND*/ HAS_MODEL_LOADED(mnCop)
			//oiNotebook = CREATE_OBJECT_NO_OFFSET(mnNotebook, vCopPos)
			//IF NOT DOES_ENTITY_EXIST(piCop)
				piCop = CREATE_PED(PEDTYPE_COP, mnCop, vCopPos, fCopHeading)
				ADD_PED_FOR_DIALOGUE(pedConvStruct, COP_ID, piCop, "MEAmandaCop", TRUE)
				SET_PED_CAN_SWITCH_WEAPON(piCop, FALSE)
				SET_PED_PROP_INDEX(piCop, ANCHOR_EYES, 0)
				SET_PED_ACCURACY(piCop, 25)
				SET_ENTITY_COORDS_NO_OFFSET(piCop, vCopPos)
				SET_ENTITY_HEALTH(piCop, 110)
				/*IF HAS_ANIM_DICT_LOADED(sMissionAnimDict)
					TASK_PLAY_ANIM(piCop, sMissionAnimDict, sCopAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
				ENDIF*/
				//ATTACH_ENTITY_TO_ENTITY(oiNotebook, piCop, GET_PED_BONE_INDEX( piCop, BONETAG_R_HAND ), vNotebookOffset, vNotebookRotate, TRUE, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnCop)
				//SET_MODEL_AS_NO_LONGER_NEEDED(mnNotebook)
			//ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(piShopkeeper)
		IF HAS_MODEL_LOADED(mnShopkeeper) //AND HAS_ANIM_DICT_LOADED(sMissionAnimDict)
			piShopkeeper = CREATE_PED(PEDTYPE_CIVMALE, mnShopkeeper, vShopkeeperPos, fShopkeeperHeading)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piShopkeeper, TRUE)
			SET_ENTITY_COORDS_NO_OFFSET(piShopkeeper, vShopkeeperPos)
			//TASK_PLAY_ANIM(piShopkeeper, sMissionAnimDict, sShopAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
			ADD_PED_FOR_DIALOGUE(pedConvStruct, SHOPKEEPER_ID, piShopkeeper, "MEAmandaShop", TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnShopkeeper)
			
			// only need to do this stuff once so placing it here to ensure that happens
			CLEAR_AREA_OF_PEDS(vCopPos, 50.0)
			DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
			SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
			SET_PED_NON_CREATION_AREA(vDisableNavmeshMin, vDisableNavmeshMax)
		    ADD_SCENARIO_BLOCKING_AREA(vDisableNavmeshMin, vDisableNavmeshMax)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers the conversation between the shopkeeper and cop when the player is close enough
PROC PlayCopShopkeeperConv()

	SWITCH copChatState
	
		CASE CCS_NOT_STARTED
			// Waiting to start
		BREAK
	
		CASE CCS_INIT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				/*FLOAT fTriggerDist
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					fTriggerDist = fStartArrestAnimDist
				ELSE
					fTriggerDist = (fStartArrestAnimDist*2)
				ENDIF*/
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, fStartArrestAnimDist)
						ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
						IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_SCENE", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
						OR CONVERSATION_TIMED_OUT(iCopConvFailedTimer, 2000)
							CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_INIT: Started synched scene conversation with subtitles.")
							eArrestSceneStage = ASS_ARREST
							//bAllowSceneConv = FALSE
							IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								copChatState = CCS_DONE
							ELSE
								copChatState = CCS_PLAYING_SUBS
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, (fStartArrestAnimDist*2))
						ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
						IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_SCENE", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						OR CONVERSATION_TIMED_OUT(iCopConvFailedTimer, 2000)
							CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_INIT: Started synched scene conversation without subtitles.")
							eArrestSceneStage = ASS_ARREST
							//bAllowSceneConv = FALSE
							/*IF GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
								copChatState = CCS_DONE
							ELSE*/
								copChatState = CCS_PLAYING_NO_SUBS
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		CASE CCS_WAIT_FOR_REINIT
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, 90)
				AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEA1_02") AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEA1_03") // Get in the ~b~cop car. / Lose the cops.
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(pedConvStruct, "MEA1AUD", "MEA1_SCENE", tCopCharConvLabel, CONV_PRIORITY_MEDIUM)
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_WAIT_FOR_REINIT: Resumed synched scene conversation with subtitles")
						//bAllowSceneConv = FALSE
						copChatState = CCS_PLAYING_SUBS
					ENDIF
				ELSE
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(pedConvStruct, "MEA1AUD", "MEA1_SCENE", tCopCharConvLabel, CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_WAIT_FOR_REINIT: Resumed synched scene conversation without subtitles")
						//bAllowSceneConv = FALSE
						copChatState = CCS_PLAYING_NO_SUBS
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE CCS_PLAYING_NO_SUBS
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				// See if we need to reinit
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, 90)
					CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_NO_SUBS: Player in range, stopping conversation")
					tCopCharConvLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					IF ARE_STRINGS_EQUAL(tCopCharConvLabel, "")
					OR ARE_STRINGS_EQUAL(tCopCharConvLabel, "NULL")
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_NO_SUBS: Player in range but conversation already on last line, going to CCS_DONE")
						copChatState = CCS_DONE
					ELSE
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_NO_SUBS: Stopping and storing synched scene conversation")
						KILL_ANY_CONVERSATION()
						copChatState = CCS_WAIT_FOR_REINIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		CASE CCS_PLAYING_SUBS
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				// See if we need to reinit
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, 91)
					CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_SUBS: Player out of range, stopping conversation")
					tCopCharConvLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					IF ARE_STRINGS_EQUAL(tCopCharConvLabel, "")
					OR ARE_STRINGS_EQUAL(tCopCharConvLabel, "NULL")
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_SUBS: Player out of range but conversation already on last line, going to CCS_DONE")
						copChatState = CCS_DONE
					ELSE
						CPRINTLN(DEBUG_MISSION, "PlayCopShopkeeperConv: CCS_PLAYING_SUBS: Stopping and storing synched scene conversation")
						KILL_ANY_CONVERSATION()
						copChatState = CCS_WAIT_FOR_REINIT
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		CASE CCS_DONE
			// Finished
		BREAK
	
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Hides Amanda's bag when she gets in a car, shows it again when she gets out
PROC AmandasBagManager()
	IF IS_PED_UNINJURED(piAmanda)
		IF bShowingAmandasBag
			IF IS_PED_IN_ANY_VEHICLE(piAmanda)
				SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_SPECIAL, 0, 0)
				bShowingAmandasBag = FALSE
			ENDIF
		ELSE
			IF NOT IS_PED_IN_ANY_VEHICLE(piAmanda, TRUE)
				SET_PED_COMPONENT_VARIATION(piAmanda, PED_COMP_SPECIAL, 1, 0)
				bShowingAmandasBag = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    State machine for controlling the arrest scene synced animation
PROC ControlArrestScene()
	IF IS_PED_UNINJURED(piCop) AND IS_PED_UNINJURED(piShopkeeper) AND IS_PED_UNINJURED(piAmanda) AND IS_VEHICLE_OK(viCopCar) AND HAS_ANIM_DICT_LOADED(sMissionAnimDict)
		#IF IS_DEBUG_BUILD
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), viCopCar) AND IS_SYNCHRONIZED_SCENE_RUNNING(iArrestScene)
				CPRINTLN(DEBUG_MISSION, "ControlArrestScene: Player contacted cop car at phase ", GET_SYNCHRONIZED_SCENE_PHASE(iArrestScene))
			ENDIF
		#ENDIF
		SWITCH eArrestSceneStage
			CASE ASS_STANDING
				PlayCopShopkeeperConv()
				iArrestScene = CREATE_SYNCHRONIZED_SCENE(vArrestScenePos, vArrestSceneRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(iArrestScene,TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iArrestScene,FALSE)
				
				bShowingAmandasBag = TRUE
				
				TASK_SYNCHRONIZED_SCENE(piCop, iArrestScene, sMissionAnimDict, sCopStand, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(piShopkeeper, iArrestScene, sMissionAnimDict, sShopStand, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(piAmanda, iArrestScene, sMissionAnimDict, sAmandaStand, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(viCopCar, iArrestScene, sCopCarStand, sMissionAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
				
				// Init conversation tracking variables
				//copChatState = CCS_NOT_STARTED
				iCopConvFailedTimer = -1
				copChatState = CCS_INIT
				
				//eArrestSceneStage = ASS_ARREST
				eArrestSceneStage = ASS_READY
			BREAK
			CASE ASS_READY
				PlayCopShopkeeperConv()
			BREAK
			CASE ASS_ARREST
				PlayCopShopkeeperConv()
				/*FLOAT fTriggerDist
				IF IS_PED_ON_FOOT(PLAYER_PED_ID())
					fTriggerDist = fStartArrestAnimDist
				ELSE
					fTriggerDist = (fStartArrestAnimDist*2)
				ENDIF
				
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vShopLocation, fTriggerDist)*/
					// Init the conversation
					/*IF copChatState = CCS_NOT_STARTED
						copChatState = CCS_INIT
					ENDIF*/
					
					//IF NOT bAllowSceneConv
						iArrestScene = CREATE_SYNCHRONIZED_SCENE(vArrestScenePos, vArrestSceneRot)
						SET_SYNCHRONIZED_SCENE_LOOPED(iArrestScene,FALSE)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iArrestScene,FALSE)
						
						TASK_SYNCHRONIZED_SCENE(piCop, iArrestScene, sMissionAnimDict, sCopArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						TASK_SYNCHRONIZED_SCENE(piShopkeeper, iArrestScene, sMissionAnimDict, sShopArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
						TASK_SYNCHRONIZED_SCENE(piAmanda, iArrestScene, sMissionAnimDict, sAmandaArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(viCopCar, iArrestScene, sCopCarArrest, sMissionAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
						SET_SYNCHRONIZED_SCENE_PHASE(iArrestScene, 0.622)
						//SET_SYNCHRONIZED_SCENE_PHASE(iArrestScene, 0.5)
						
						eArrestSceneStage = ASS_POST_ARREST
					//ENDIF
				//ENDIF
			BREAK
			CASE ASS_POST_ARREST
				PlayCopShopkeeperConv()
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iArrestScene)
					CPRINTLN(DEBUG_MISSION, "ControlArrestScene: Recreating synched scene without Amanda and car...")
					iArrestScene = CREATE_SYNCHRONIZED_SCENE(vArrestScenePos, vArrestSceneRot)
					SET_SYNCHRONIZED_SCENE_LOOPED(iArrestScene,TRUE)
					
					TASK_SYNCHRONIZED_SCENE(piCop, iArrestScene, sMissionAnimDict, sCopPstArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					TASK_SYNCHRONIZED_SCENE(piShopkeeper, iArrestScene, sMissionAnimDict, sShopPstArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					//TASK_SYNCHRONIZED_SCENE(piAmanda, iArrestScene, sMissionAnimDict, sAmandaPstArrest, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					IF IS_VEHICLE_SEAT_FREE(viCopCar, VS_BACK_RIGHT)
						//TASK_WARP_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_BACK_RIGHT)
						SET_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_BACK_RIGHT)
						SET_VEHICLE_DOOR_SHUT(viCopCar, SC_DOOR_REAR_RIGHT)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viCopCar, TRUE)
						//SET_ENTITY_COLLISION(piAmanda, TRUE)
						TASK_LOOK_AT_ENTITY(piAmanda, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						CPRINTLN(DEBUG_MISSION,"Amanda has just been given the look at entity task for the player")
					ENDIF
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar, TRUE)
						PRINT_NOW("MEA1_02", DEFAULT_GOD_TEXT_TIME, 1)	// Get in the ~b~cop car.
					ENDIF
					CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
					//PLAY_SYNCHRONIZED_ENTITY_ANIM(viCopCar, iArrestScene, sCopCarPstArrest, sMissionAnimDict, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					
					eArrestSceneStage = ASS_END
				ELSE
					IF GET_SYNCHRONIZED_SCENE_PHASE(iArrestScene) >= 0.9
						IF IS_ENTITY_PLAYING_ANIM(piAmanda, sMissionAnimDict, sAmandaArrest, ANIM_SYNCED_SCENE)
						//	IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viCopCar, 2.0)
								CPRINTLN(DEBUG_MISSION, "ControlArrestScene: Pulling Amanda and car out of synched scene...")
								STOP_SYNCHRONIZED_ENTITY_ANIM(piAmanda, INSTANT_BLEND_OUT, FALSE)
								//STOP_SYNCHRONIZED_ENTITY_ANIM(viCopCar, SLOW_BLEND_OUT, TRUE)
								IF IS_VEHICLE_SEAT_FREE(viCopCar, VS_BACK_RIGHT)
									//TASK_WARP_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_BACK_RIGHT)
									SET_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_BACK_RIGHT)
									SET_VEHICLE_DOOR_SHUT(viCopCar, SC_DOOR_REAR_RIGHT)
									SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viCopCar, TRUE)
									//SET_ENTITY_COLLISION(piAmanda, TRUE)
									TASK_LOOK_AT_ENTITY(piAmanda, PLAYER_PED_ID(), -1, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
									CPRINTLN(DEBUG_MISSION,"Amanda has just been given the look at entity task for the player")
								ENDIF
						//	ENDIF
						ENDIF
					ELIF GET_SYNCHRONIZED_SCENE_PHASE(iArrestScene) >= 0.85
						IF IS_ENTITY_PLAYING_ANIM(viCopCar, sMissionAnimDict, sCopCarArrest, ANIM_SYNCED_SCENE)
							SET_ENTITY_COLLISION(piAmanda, FALSE)
							STOP_SYNCHRONIZED_ENTITY_ANIM(viCopCar, SLOW_BLEND_OUT, TRUE)
							SET_VEHICLE_DOOR_SHUT(viCopCar, SC_DOOR_REAR_RIGHT)
						ENDIF
					/*ELIF GET_SYNCHRONIZED_SCENE_PHASE(iArrestScene) >= 0.622
						IF copChatState = CCS_NOT_STARTED
							copChatState = CCS_INIT
						ENDIF*/
					ENDIF
				ENDIF
			BREAK
			CASE ASS_BREAKOUT
				// Shopkeeper and Cop leave the anim when the player steals the cop car
				/*iArrestScene = CREATE_SYNCHRONIZED_SCENE(vArrestScenePos, vArrestSceneRot)
				SET_SYNCHRONIZED_SCENE_LOOPED(iArrestScene,FALSE)
				
				//TASK_SYNCHRONIZED_SCENE(piCop, iArrestScene, sMissionAnimDict, sCopBreak, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(piShopkeeper, iArrestScene, sMissionAnimDict, sShopBreak, INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)*/
				
				IF IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopArrest, ANIM_SYNCED_SCENE)
				OR IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopPstArrest, ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, NORMAL_BLEND_OUT, TRUE)
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopArrest, ANIM_SYNCED_SCENE)
				OR IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopPstArrest, ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piCop, NORMAL_BLEND_OUT, TRUE)
				ENDIF
				
				IF IS_PED_UNINJURED(piCop)
					TASK_TURN_PED_TO_FACE_ENTITY(piCop, PLAYER_PED_ID(), -1)
				ENDIF
					/*IF NOT IsPedPerformingTask(piCop, SCRIPT_TASK_ARREST_PED)
						SEQUENCE_INDEX siCopStart
						OPEN_SEQUENCE_TASK(siCopStart)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
							TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						CLOSE_SEQUENCE_TASK(siCopStart)
						TASK_PERFORM_SEQUENCE(piCop, siCopStart)
						CLEAR_SEQUENCE_TASK(siCopStart)
					ENDIF
				ENDIF
				IF IS_PED_UNINJURED(piShopkeeper) AND NOT IS_PED_FLEEING(piShopkeeper)
					TASK_SMART_FLEE_PED(piShopkeeper, PLAYER_PED_ID(), 500, -1)
				ENDIF*/
				
				eArrestSceneStage = ASS_END
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC


/// PURPOSE:
///    Updates the fail reason, and sets the mission state to be fail delay
/// PARAMS:
///    eFailReason - the reason the player failed the mission
PROC SetFailReason(MISSION_FAILED_REASON eFailReason)
	missionFailedReason = eFailReason
	missionState = MS_FAIL_DELAY
	missionSubstate = MSS_PREPARE
ENDPROC

/// PURPOSE:
///    Checks if Amanda has been killed or injured
PROC CheckAmanda()
	IF missionState = MS_FAIL_DELAY
		EXIT
	ENDIF
	IF DOES_ENTITY_EXIST(piAmanda) AND IS_PED_INJURED(piAmanda)
		SetFailReason(MFR_AMANDA_DEAD)

	ELIF IS_PED_UNINJURED(piAmanda) AND IS_PED_UNINJURED(PLAYER_PED_ID())
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piAmanda, PLAYER_PED_ID())
			IF HAS_PED_BEEN_DAMAGED_BY_WEAPON(piAmanda, WEAPONTYPE_INVALID, GENERALWEAPON_TYPE_ANYWEAPON) OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(piAmanda)
				SetFailReason(MFR_AMANDA_HURT) 
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Used to fail the mission if the player takes too long, commented out in case this is needed again
PROC CopGetsInCar()
	IF bStartCopToCar
		DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
		SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
		IF IS_PED_UNINJURED(piCop) AND IS_VEHICLE_OK(viCopCar) AND IS_VEHICLE_SEAT_FREE(viCopCar)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
			IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_LEAVE", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
				SEQUENCE_INDEX siDriveOff
				OPEN_SEQUENCE_TASK(siDriveOff)
					TASK_ENTER_VEHICLE(NULL, viCopCar, DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_WALK)
					TASK_VEHICLE_DRIVE_WANDER(NULL, viCopCar, 50, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
				CLOSE_SEQUENCE_TASK(siDriveOff)
				TASK_PERFORM_SEQUENCE(piCop, siDriveOff)
				CLEAR_SEQUENCE_TASK(siDriveOff)
				FREEZE_ENTITY_POSITION(viCopCar, FALSE)
				eArrestSceneStage = ASS_END
				copChatState = CCS_DONE
				bStartCopToCar = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CopInCarFail()
	IF IS_PED_UNINJURED(piCop) AND IS_VEHICLE_OK(viCopCar)
		IF IS_PED_IN_VEHICLE(piCop, viCopCar)
			//SET_VEHICLE_DOORS_LOCKED(viCopCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			FREEZE_ENTITY_POSITION(viCopCar, FALSE)
			IF IsPedPerformingTask(piCop, SCRIPT_TASK_VEHICLE_DRIVE_WANDER)
				IF GET_GAME_TIMER() > iCopDrivingTimer
					SetFailReason(MFR_AMANDA_ARRESTED)
				ENDIF
			ELSE
				TASK_VEHICLE_DRIVE_WANDER(piCop, viCopCar, 30.0, DRIVINGMODE_AVOIDCARS)
				iCopDrivingTimer = GET_GAME_TIMER() + 2000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the current time and converts it to seconds
/// RETURNS:
///    The current time in seconds
FUNC INT GetTimeInSeconds()
	/*INT iHours = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
	IF iStartDay = -1
		iStartDay = GET_TIMEOFDAY_DAY(GET_CURRENT_TIMEOFDAY())
	ELSE
		IF iStartDay <> GET_TIMEOFDAY_DAY(GET_CURRENT_TIMEOFDAY())
			iHours += 24
		ENDIF
	ENDIF
	// convert the hours value to minutes
	iHours *= 60
	iHours *= 1000
	
	INT iMinutes = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
	// add the converted hours value
	//iMinutes += iHours
	// convert the minutes value to seconds
	iMinutes *= 1000//60
	
	INT iSeconds = iHours + iMinutes//GET_TIMEOFDAY_SECOND(GET_CURRENT_TIMEOFDAY())
	// add the converted minutes value
	//iSeconds += iMinutes
	
	RETURN iSeconds*/
	RETURN GET_GAME_TIMER()
ENDFUNC

/// PURPOSE:
///    Fails the mission if the player takes too long to reach Amanda
PROC FailIfTooLate()
	IF GetTimeInSeconds() > iFailTimer - 1000
			IF IS_PED_UNINJURED(piCop)
				/*IF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopAnim)
					STOP_ANIM_TASK(piCop, sMissionAnimDict, sCopAnim)
				ENDIF*/
				CopGetsInCar()
				CopInCarFail()
			ELIF missionState = MS_DRIVE_TO_SHOP
				SetFailReason(MFR_AMANDA_ARRESTED)
			ENDIF
			IF missionState <> MS_RESCUE_AMANDA AND bPlayBeeps
				PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
				bPlayBeeps = FALSE
			ENDIF
	ELIF missionState <> MS_RESCUE_AMANDA //AND IS_MISSION_TITLE_FINISHED() //NOT (missionState = MS_RESCUE_AMANDA AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
		iMissionTimer = iFailTimer - GetTimeInSeconds()
		INT iTimeNow = GetTimeInSeconds()
		//DRAW_GENERIC_METER(iMissionTimer, iDriveToShopTime, "TIMER_TIME")
		HIDE_STREET_AND_CAR_NAMES_THIS_FRAME()
		IF iMissionTimer > 11000
			DRAW_GENERIC_TIMER(iMissionTimer, "MEA1_TIME")
		ELIF iMissionTimer > 6000
			DRAW_GENERIC_TIMER(iMissionTimer, "MEA1_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_PURE_WHITE)
			IF NOT bStartedBeepTimer
				CPRINTLN(DEBUG_MISSION, "BEEP! Once a second")
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				iBeepTimer = GetTimeInSeconds()
				bStartedBeepTimer = TRUE
			ELIF iTimeNow-iBeepTimer > 1000
				CPRINTLN(DEBUG_MISSION, "BEEP! Once a second")
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				iBeepTimer = GetTimeInSeconds()
			ENDIF
		ELSE
			DRAW_GENERIC_TIMER(iMissionTimer, "MEA1_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, -1, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED, HUDFLASHING_NONE, 0, FALSE, HUD_COLOUR_PURE_WHITE)
			IF iTimeNow-iBeepTimer > 500
				CPRINTLN(DEBUG_MISSION, "BEEP! Twice a second")
				PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
				iBeepTimer = GetTimeInSeconds()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Fails the mission if the player trashes the cop car before resuing amanda
PROC TrashedCopCarFail()
	IF IS_PED_UNINJURED(piAmanda) AND NOT IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
		IF IS_VEHICLE_OK(viCopCar)
			//CPRINTLN(DEBUG_MISSION,"Cop car is OK, ready for on roof check")
			IF IS_VEHICLE_STUCK_TIMER_UP(viCopCar, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(viCopCar, VEH_STUCK_JAMMED, JAMMED_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(viCopCar, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(viCopCar, VEH_STUCK_ON_SIDE, SIDE_TIME)
				CPRINTLN(DEBUG_MISSION,"Cop car is on roof")
				SetFailReason(MFR_TRASHED_CAR)
			ENDIF
		ELSE
			IF missionState = MS_RESCUE_AMANDA
				SetFailReason(MFR_TRASHED_CAR)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the cop attack and the shopkeeper flee if the player threatens them
PROC PlayerThreatensPeds()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND eArrestSceneStage <> ASS_END
		BOOL bPlayerOnlyBumped = FALSE
		IF (IS_PED_UNINJURED(piCop) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), piCop)) OR (IS_PED_UNINJURED(piShopkeeper) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), piShopkeeper))
		AND NOT (HAS_PLAYER_THREATENED_PED(piCop) OR HAS_PLAYER_THREATENED_PED(piShopkeeper) OR IS_PED_SHOOTING(PLAYER_PED_ID())) AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF IS_PED_RAGDOLL(piCop) OR IS_PED_RAGDOLL(piShopkeeper)
				bPlayerOnlyBumped = TRUE
			ELSE
				IF bNotBumpedYet
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_GRIEF", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
						TASK_LOOK_AT_ENTITY(piCop, PLAYER_PED_ID(), 2000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						bNotBumpedYet = FALSE
						iBumpTimer = -1
					ENDIF
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CONVERSATION_TIMED_OUT(iBumpTimer, 2000)
						bPlayerOnlyBumped = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (DOES_ENTITY_EXIST(piCop) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piCop, PLAYER_PED_ID()))
		OR (DOES_ENTITY_EXIST(piCop) AND IS_PED_INJURED(piCop))
		OR IS_PLAYER_SHOOTING_NEAR_PED(piCop) OR IS_PLAYER_VISIBLY_TARGETTING_PED(piCop) OR IS_PLAYER_VISIBLY_TARGETTING_PED(piShopkeeper)
		OR (DOES_ENTITY_EXIST(piShopkeeper) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piShopkeeper, PLAYER_PED_ID()))
		OR (DOES_ENTITY_EXIST(piShopkeeper) AND IS_PED_INJURED(piShopkeeper))
		OR IS_PED_SHOOTING(PLAYER_PED_ID()) OR bPlayerOnlyBumped
		OR IS_ANYONE_SHOOTING_NEAR_PED(piShopkeeper) OR IS_PED_SHOOTING(PLAYER_PED_ID())
			// player has aimed at or shot near the cop or shopkeeper
			DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
			SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
			// Give the player an appropriate wanted level, make the cop attack and the shopkeeper flee
			IF bPlayerOnlyBumped
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
					DISTANT_COP_CAR_SIRENS(TRUE)
				ENDIF
			ELSE
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
					DISTANT_COP_CAR_SIRENS(TRUE)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(piCop) AND NOT IS_ENTITY_ON_FIRE(piCop) AND NOT IS_PED_GETTING_UP(piCop) AND NOT IS_PED_PRONE(piCop) AND NOT IS_PED_RAGDOLL(piCop)
				/*IF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopAnim)
					STOP_ANIM_TASK(piCop, sMissionAnimDict, sCopAnim)
				ENDIF*/
				IF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopArrest, ANIM_SYNCED_SCENE)
				OR IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopPstArrest, ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piCop, NORMAL_BLEND_OUT, TRUE)
				ENDIF
				IF NOT IS_PED_IN_COMBAT(piCop) AND NOT IsPedPerformingTask(piCop, SCRIPT_TASK_ARREST_PED)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_PED_CAN_SWITCH_WEAPON(piCop, TRUE)
					GIVE_WEAPON_TO_PED(piCop, WEAPONTYPE_PISTOL, -1)
					IF bPlayerOnlyBumped
						TASK_ARREST_PED(piCop, PLAYER_PED_ID())
					ELSE
						//TASK_COMBAT_PED(piCop, PLAYER_PED_ID())
						TASK_ARREST_PED(piCop, PLAYER_PED_ID())
					ENDIF
				ENDIF
			/*ELIF IS_PED_UNINJURED(piShopkeeper)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					// non-essential dialogue so not in an IF statement
					PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_ATTACK", "MEA1_ATTACK_2", CONV_PRIORITY_MEDIUM)
				ENDIF*/
			ENDIF
			IF IS_PED_UNINJURED(piShopkeeper) AND NOT IS_PED_FLEEING(piShopkeeper)
				IF IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopArrest, ANIM_SYNCED_SCENE)
				OR IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopPstArrest, ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, NORMAL_BLEND_OUT, TRUE)
				ENDIF
				TASK_SMART_FLEE_PED(piShopkeeper, PLAYER_PED_ID(), 500, -1)
			ENDIF
			/*IF DOES_ENTITY_EXIST(oiNotebook)
				DETACH_ENTITY(oiNotebook)
			ENDIF*/
			
			eArrestSceneStage = ASS_END
			copChatState = CCS_DONE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda shouting at Michael when the player is close enough
PROC PlayNearAmandaConv()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
		IF iCurrentAmandaLine < NUM_AMANDA_LINES AND GET_GAME_TIMER() > iAmandaLineTimer AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), piAmanda) < fAmandaConvRadius
			//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda, "MEA1_AJAA", "AMANDA", SPEECH_PARAMS_FORCE_FRONTEND)
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda, sAmandaLines[iCurrentAmandaLine], "AMANDA", SPEECH_PARAMS_FORCE_FRONTEND)
			iCurrentAmandaLine++
			iAmandaLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
			//IF bAllowLooking
			/*IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_LOOK_AT_ENTITY)
				TASK_LOOK_AT_ENTITY(piAmanda, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
				//bAllowLooking = FALSE
			ENDIF*/
		ENDIF
		IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_LOOK_AT_ENTITY) AND IS_PED_UNINJURED(piCop) AND IS_PED_IN_ANY_VEHICLE(piAmanda)
			TASK_LOOK_AT_ENTITY(piAmanda, piCop, -1, SLF_USE_TORSO | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks for the player either getting in the car or grabbing it with a tow truck
PROC PlayerInCar()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viCopCar) AND IS_PED_UNINJURED(piAmanda)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar) OR IS_PLAYER_TOWING_VEHICLE(viCopCar)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			//ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
			SAFE_REMOVE_BLIP(biObjective)
			//SET_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_FRONT_RIGHT)
			FREEZE_ENTITY_POSITION(viCopCar, FALSE)
			bIsCarFrozen = FALSE
			iConvFailedTimer = -1
			CPRINTLN(DEBUG_MISSION,"Entering rescue amanda state: Active 3")
			REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
			//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda,"MEA1_BFAA","AMANDA",SPEECH_PARAMS_FORCE_NORMAL)
			missionSubstate = MSS_ACTIVE_TWO
		ELIF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viCopCar, 6.5)
			IF bIsCarFrozen
				FREEZE_ENTITY_POSITION(viCopCar, FALSE)
				bIsCarFrozen = FALSE
			ENDIF
		ELSE
			IF NOT bIsCarFrozen
				FREEZE_ENTITY_POSITION(viCopCar, TRUE)
				bIsCarFrozen = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CheckForFreeSeats()
	IF bDisplaySeatObjective AND NOT IS_MESSAGE_BEING_DISPLAYED() AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
		VEHICLE_INDEX viCheck
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			viCheck = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viCheck) AND NOT IS_PED_IN_VEHICLE(piAmanda, viCheck, TRUE) AND NOT DOES_VEHICLE_HAVE_FREE_SEAT(viCheck)
				PRINT_NOW("MEA1_07", DEFAULT_GOD_TEXT_TIME, 1)	// Get a different vehicle.
				CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
				bDisplaySeatObjective = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Releases the shopkeeper when he's far from the player
PROC ReleaseShopkeeper()
	IF IS_PED_UNINJURED(piShopkeeper) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piShopkeeper) > 100
		SAFE_RELEASE_PED(piShopkeeper, FALSE)
	ENDIF
	IF IS_PED_UNINJURED(piCop) AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piCop) > 100
		SAFE_RELEASE_PED(piCop)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the player a wanted level and makes the cop attack
PROC ActivateCops()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
			CPRINTLN(DEBUG_MISSION,"Trying to set player's wanted level, first attempt")
		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
			CPRINTLN(DEBUG_MISSION,"Trying to set player's wanted level")
		ELSE
			CPRINTLN(DEBUG_MISSION,"Player is wanted, activating cops")
			DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
			SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
			DISTANT_COP_CAR_SIRENS(TRUE)
			IF IS_PED_UNINJURED(piAmanda)
				TASK_CLEAR_LOOK_AT(piAmanda)
			ENDIF
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(vRoadOffOne, vRoadOffTwo, fRoadOffWidth)
			
			bLoadHandcuffSound = TRUE
			bAllowInCarConv = TRUE
			iConvFailedTimer = -1
			
			/*IF DOES_ENTITY_EXIST(oiNotebook)
				DETACH_ENTITY(oiNotebook)
			ENDIF*/
			IF IS_PED_UNINJURED(piCop) AND NOT IS_ENTITY_ON_FIRE(piCop) AND NOT IS_PED_GETTING_UP(piCop) AND NOT IS_PED_PRONE(piCop) AND NOT IS_PED_RAGDOLL(piCop)
				IF NOT IsPedPerformingTask(piCop, SCRIPT_TASK_ARREST_PED)
					IF IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopArrest, ANIM_SYNCED_SCENE)
					OR IS_ENTITY_PLAYING_ANIM(piCop, sMissionAnimDict, sCopPstArrest, ANIM_SYNCED_SCENE)
						STOP_SYNCHRONIZED_ENTITY_ANIM(piCop, NORMAL_BLEND_OUT, TRUE)
					ENDIF
					SEQUENCE_INDEX siCopStart
					OPEN_SEQUENCE_TASK(siCopStart)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
						//TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
						TASK_ARREST_PED(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(siCopStart)
					TASK_PERFORM_SEQUENCE(piCop, siCopStart)
					CLEAR_SEQUENCE_TASK(siCopStart)
					//TASK_TURN_PED_TO_FACE_ENTITY(piCop, PLAYER_PED_ID(), 1500)
				ENDIF
			ENDIF
			IF IS_PED_UNINJURED(piShopkeeper) AND NOT IS_PED_FLEEING(piShopkeeper)
				IF IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopArrest, ANIM_SYNCED_SCENE)
				OR IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopPstArrest, ANIM_SYNCED_SCENE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, NORMAL_BLEND_OUT, TRUE)
				ENDIF
				TASK_SMART_FLEE_PED(piShopkeeper, PLAYER_PED_ID(), 500, -1)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_ENTER_COP_CAR")
				STOP_AUDIO_SCENE("M_E_AMANDA_ENTER_COP_CAR")
			ENDIF
			START_AUDIO_SCENE("M_E_AMANDA_ESCAPE_COPS")
			
			PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_MIC_AMANDA_01", 0.0)
			
			//eArrestSceneStage = ASS_BREAKOUT
			eArrestSceneStage = ASS_END
			copChatState = CCS_DONE
			missionSubstate = MSS_PREPARE
			missionState = MS_LOSE_COPS
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers the cop if the player alerts him for a reason other than stealing the cop car or directly threatening the cop or shopkeeper
/// PARAMS:
///    bCopAttacks - Should the cop attack? If this is false, the cop will attempt to arrest the player
PROC MinorCopAlert(BOOL bCopAttacks = TRUE)
	IF IS_PED_UNINJURED(piCop) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_ON_FIRE(piCop) AND NOT IS_PED_GETTING_UP(piCop) AND NOT IS_PED_PRONE(piCop) AND NOT IS_PED_RAGDOLL(piCop)
		/*IF DOES_ENTITY_EXIST(oiNotebook)
			DETACH_ENTITY(oiNotebook)
		ENDIF*/
		IF NOT (GET_PEDS_CURRENT_WEAPON(piCop) = WEAPONTYPE_PISTOL)
			SET_PED_CAN_SWITCH_WEAPON(piCop, TRUE)
			GIVE_WEAPON_TO_PED(piCop, WEAPONTYPE_PISTOL, -1)
		ENDIF
		IF bCopAttacks 
			IF NOT IS_PED_IN_COMBAT(piCop)
				//TASK_COMBAT_PED(piCop, PLAYER_PED_ID())
				TASK_ARREST_PED(piCop, PLAYER_PED_ID())
			ENDIF
		ELSE
			IF NOT IsPedPerformingTask(piCop, SCRIPT_TASK_ARREST_PED)
				TASK_ARREST_PED(piCop, PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	IF IS_PED_UNINJURED(piShopkeeper) AND NOT IS_PED_FLEEING(piShopkeeper) AND NOT IS_PED_IN_COMBAT(piShopkeeper)
		IF IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopArrest, ANIM_SYNCED_SCENE)
		OR IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopPstArrest, ANIM_SYNCED_SCENE)
			STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, NORMAL_BLEND_OUT, TRUE)
		ENDIF
		TASK_SMART_FLEE_PED(piShopkeeper, PLAYER_PED_ID(), 500, -1)
	ENDIF
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	eArrestSceneStage = ASS_END
	copChatState = CCS_DONE
	//bAllowSceneConv = FALSE
	DISABLE_NAVMESH_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, FALSE)
	SET_PED_PATHS_IN_AREA(vDisableNavmeshMin, vDisableNavmeshMax, TRUE)
ENDPROC

/// PURPOSE:
///    Makes the shopkeeper flee if he's threatened
PROC MakeShopkeeperFlee()
	IF IS_PED_UNINJURED(piShopkeeper) AND NOT IsPedPerformingTask(piShopkeeper, SCRIPT_TASK_SMART_FLEE_PED)
		IF IS_ANYONE_SHOOTING_NEAR_PED(piShopkeeper) OR IS_PED_SHOOTING(PLAYER_PED_ID()) OR (IS_PED_UNINJURED(piCop) AND IS_PED_SHOOTING(piCop))
			IF IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopArrest, ANIM_SYNCED_SCENE)
			OR IS_ENTITY_PLAYING_ANIM(piShopkeeper, sMissionAnimDict, sShopPstArrest, ANIM_SYNCED_SCENE)
				STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, NORMAL_BLEND_OUT, TRUE)
			ENDIF
			TASK_SMART_FLEE_PED(piShopkeeper, PLAYER_PED_ID(), 500, -1)
			MinorCopAlert()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives the cop a pistol
PROC GiveWeaponToCop()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piCop) AND NOT IS_ENTITY_ON_FIRE(piCop) AND NOT IS_PED_GETTING_UP(piCop) AND NOT IS_PED_PRONE(piCop) AND NOT IS_PED_RAGDOLL(piCop)
		IF GET_GAME_TIMER() > iCopGunTimer
			IF NOT (GET_PEDS_CURRENT_WEAPON(piCop) = WEAPONTYPE_PISTOL)
				SET_PED_CAN_SWITCH_WEAPON(piCop, TRUE)
				GIVE_WEAPON_TO_PED(piCop, WEAPONTYPE_PISTOL, -1)
			ENDIF
			IF NOT IS_PED_IN_COMBAT(piCop)
				CLEAR_PED_TASKS(piCop)
				//TASK_COMBAT_PED(piCop, PLAYER_PED_ID())
				TASK_ARREST_PED(piCop, PLAYER_PED_ID())
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds Amanda to the player's group, sets her so that she can't be targetted, even after the mission finishes
PROC AddAmandaToPlayerGroup()
	SET_PED_AS_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
	SET_PED_GROUP_MEMBER_PASSENGER_INDEX(piAmanda, VS_FRONT_RIGHT)
	SET_PED_NEVER_LEAVES_GROUP(piAmanda, TRUE)
	SET_PED_CAN_BE_TARGETTED(piAmanda, FALSE)
	//SET_PED_DIES_INSTANTLY_IN_WATER(piAmanda, FALSE)
	SET_PED_DIES_IN_WATER(piAmanda, FALSE)
	SET_PED_DIES_IN_SINKING_VEHICLE(piAmanda, FALSE)
	DISTANT_COP_CAR_SIRENS(FALSE)
ENDPROC

PROC ForceAmandaIntoGroupIfInterruptedSyncScene()
	IF bForceAmandaIntoGroup
		IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
			bForceAmandaIntoGroup = FALSE
		ELSE
			AddAmandaToPlayerGroup()
		ENDIF
	ENDIF
ENDPROC

PROC PlayerInterruptsSyncedScene()
	VEHICLE_INDEX viPlayer = GET_PLAYERS_LAST_VEHICLE()
	IF IS_PED_INJURED(piCop) OR IS_PED_INJURED(piShopkeeper) OR HAS_PLAYER_THREATENED_PED(piCop) OR HAS_PLAYER_THREATENED_PED(piShopkeeper)
	//OR (IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viCopCar) AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), viCopCar))
	OR (IS_VEHICLE_OK(viPlayer) AND IS_VEHICLE_OK(viCopCar) AND IS_ENTITY_TOUCHING_ENTITY(viPlayer, viCopCar))
	OR (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShopLocation) < fBlipAmandaDist)
		IF NOT IS_PED_IN_VEHICLE(piAmanda, viCopCar)
			IF NOT IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				eArrestSceneStage = ASS_END
				copChatState = CCS_DONE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iArrestScene)
					FLOAT fPhase = GET_SYNCHRONIZED_SCENE_PHASE(iArrestScene)
					STOP_SYNCHRONIZED_ENTITY_ANIM(piAmanda, INSTANT_BLEND_OUT, TRUE)
					STOP_SYNCHRONIZED_ENTITY_ANIM(viCopCar, INSTANT_BLEND_OUT, TRUE)
					IF fPhase > 0.75
						STOP_SYNCHRONIZED_ENTITY_ANIM(piAmanda, INSTANT_BLEND_OUT, FALSE)
						SAFE_SET_PED_INTO_VEHICLE(piAmanda, viCopCar, VS_BACK_RIGHT)
					ELSE
						STOP_SYNCHRONIZED_ENTITY_ANIM(piAmanda, INSTANT_BLEND_OUT, TRUE)
						SET_ENTITY_COLLISION(piAmanda, TRUE)
					ENDIF
					IF IS_PED_UNINJURED(piCop)
						STOP_SYNCHRONIZED_ENTITY_ANIM(piCop, INSTANT_BLEND_OUT, TRUE)
					ENDIF
					IF IS_PED_UNINJURED(piShopkeeper)
						STOP_SYNCHRONIZED_ENTITY_ANIM(piShopkeeper, INSTANT_BLEND_OUT, TRUE)
					ENDIF
					//SET_SYNCHRONIZED_SCENE_PHASE(iArrestScene, 1.0)
				ENDIF
				FREEZE_ENTITY_POSITION(piAmanda, FALSE)
				FREEZE_ENTITY_POSITION(viCopCar, FALSE)
				IF IS_VEHICLE_OK(viPlayer) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayer) AND IS_VEHICLE_SEAT_FREE(viPlayer, VS_FRONT_RIGHT)
					IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_ENTER_VEHICLE)
						TASK_ENTER_VEHICLE(piAmanda, viPlayer, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_SPRINT)
					ENDIF
				ELIF GET_DISTANCE_BETWEEN_PEDS(piAmanda, PLAYER_PED_ID()) > 10
					IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_GO_TO_ENTITY)
						TASK_GO_TO_ENTITY(piAmanda, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 7.0)
					ENDIF
				ELSE
					CLEAR_PED_TASKS(piAmanda)
				ENDIF
				AddAmandaToPlayerGroup()
				SAFE_REMOVE_BLIP(biObjective)
				//biObjective = CREATE_PED_BLIP(piAmanda, TRUE, TRUE)
				bForceAmandaIntoGroup = TRUE
				/*missionSubstate = MSS_ACTIVE_ONE
				missionState = MS_RETURN_TO_AMANDA*/
			ENDIF
			ActivateCops()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if Amanda has been freed, adds her to the player's group if necessary
/// RETURNS:
///    TRUE if Amanda has been freed
FUNC BOOL AmandaReleased()
	IF IS_PED_UNINJURED(piAmanda)
	CPRINTLN(DEBUG_MISSION,"Checking Amanda released")
		IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
			CPRINTLN(DEBUG_MISSION,"Amanda is in the player's group, returning true")
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				RETURN TRUE
			ELSE // Need to create one of the "Lost cops" conversations here
				RETURN TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MISSION,"Amanda is not in the player's group, adding her and doing other stuff")
			IF IS_PLAYER_TOWING_VEHICLE(viCopCar)
				RETURN TRUE
			ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar)
				AND IS_PED_IN_VEHICLE(piAmanda, viCopCar)
					
					if CREATE_MULTIPART_CONVERSATION_WITH_4_LINES(pedConvStruct, "MEA1AUD", "MEA1_LOSTCOP", "MEA1_LOSTCOP_1", "MEA1_LOSTCOP", "MEA1_LOSTCOP_2", "MEA1_LOSTCOP", "MEA1_LOSTCOP_3", "MEA1_LOSTCOP", "MEA1_LOSTCOP_4", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
						AddAmandaToPlayerGroup()
						SEQUENCE_INDEX siLook
						OPEN_SEQUENCE_TASK(siLook)
							TASK_LOOK_AT_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCopCar, <<2, 2, 0>>), 2000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							TASK_PAUSE(NULL, 1000)
							TASK_LOOK_AT_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCopCar, <<-2, 2, 0>>), 2000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							TASK_PAUSE(NULL, 1000)
							TASK_LOOK_AT_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCopCar, <<2, 2, 0>>), 2000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							TASK_PAUSE(NULL, 1000)
							TASK_LOOK_AT_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viCopCar, <<-2, 2, 0>>), 2000, SLF_FAST_TURN_RATE | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						CLOSE_SEQUENCE_TASK(siLook)
						TASK_PERFORM_SEQUENCE(piAmanda, siLook)
						CLEAR_SEQUENCE_TASK(siLook)
						iHandCuffTimer = GET_GAME_TIMER() + 9000
						bHandcuffSoundReady = TRUE
						RETURN TRUE
					ENDIF
				ELSE
					AddAmandaToPlayerGroup()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    The player has returned to Amanda after leaving her, put the mission back to the appropriate stage
PROC BackWithAmanda()
	CLEAR_THIS_PRINT("MEA1_05")	// Return to ~b~Amanda.
	missionSubstate = MSS_PREPARE
	CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID()) // Clear now - ped damage events can occur before you start checking for them B*1178888
	IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
		missionState = MS_DRIVE_HOME
	ELSE
		missionState = MS_LOSE_COPS
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if it's OK to play non-essential dialogue without it overriding god text
/// RETURNS:
///    TRUE if there's no god text or if the player isn't using subtitles
FUNC BOOL IsOkToPlayDialogue()
	IF NOT IS_MESSAGE_BEING_DISPLAYED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Michael and Amanda are in the same vehicle
/// RETURNS:
///    True if they are in the same vehicle, false otherwise
FUNC BOOL MichaelAndAmandaInSameVehicle()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piAmanda, viTemp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if Michael and Amanda are on foot
/// RETURNS:
///    True if they are both on foot, false otherwise
FUNC BOOL MichaelAndAmandaOnFoot()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) OR IS_PED_IN_ANY_VEHICLE(piAmanda)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC ManageGriefingLines()
	IF IS_VEHICLE_OK(viCopCar) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piCop) AND IS_PED_UNINJURED(piShopkeeper)
		SWITCH eGriefState
			CASE GCS_READY
				//CPRINTLN(DEBUG_MISSION,"eGriefState = GCS_READY")
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					TEXT_LABEL_23 tlConv
					tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
					STRING sConv
					sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
					IF ARE_STRINGS_EQUAL(sConv, "MEA1_SCENE")
						/*INT iCurrentLine
						iCurrentLine = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()*/
						sSceneConvLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						//CPRINTLN(DEBUG_MISSION,"Current iCurrentLine: ", sSceneConvLine)
						IF (IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viCopCar, 5.0) OR IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), piCop, 5.0))
						// Don't interrupt the first three lines or they'll be out of sync with Amanda getting in the car
						AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_1") AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_2") AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_3")
						AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_4") AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_5") AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "MEA1_SCENE_6")
						AND CAN_PED_SEE_PLAYER(piCop)
							IF GET_GAME_TIMER() > iGriefingTimer
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								/*IF iCurrentLine > 0
									iSceneConvLine += iCurrentLine
								ENDIF*/
								eGriefState = GCS_PLAYING
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE GCS_PLAYING
				//CPRINTLN(DEBUG_MISSION,"eGriefState = GCS_PLAYING")
				FLOAT fCopDist
				fCopDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), piCop)
				FLOAT fCarDist
				fCarDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), viCopCar)
				BOOL bConvPlayed
				bConvPlayed = FALSE
				ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				IF fCopDist < fCarDist
					bConvPlayed = CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_GRIEF", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
				ELSE
					bConvPlayed = CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_MESS", CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
				ENDIF
				IF bConvPlayed
					TASK_LOOK_AT_ENTITY(piCop, PLAYER_PED_ID(), 2000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					eGriefState = GCS_FINISH
				ENDIF
			BREAK
			
			CASE GCS_FINISH
				//CPRINTLN(DEBUG_MISSION,"eGriefState = GCS_FINISH")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//IF iSceneConvLine > -1 AND iSceneConvLine < NUM_SCENE_LINES
					IF NOT ARE_STRINGS_EQUAL(sSceneConvLine, "") AND NOT ARE_STRINGS_EQUAL(sSceneConvLine, "NULL")
						ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(pedConvStruct, "MEA1AUD", "MEA1_SCENE", sSceneConvLine, CONV_PRIORITY_MEDIUM, CheckDisplaySubtitles())
							iGriefingTimer = GET_GAME_TIMER() + GRIEFING_TIME
							//bAllowSceneConv = FALSE
							eGriefState = GCS_READY
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	//CPRINTLN(DEBUG_MISSION,"Current iSceneConvLine: ", iSceneConvLine)
ENDPROC

PROC ManagePlayerDamagesCopCarLines()
	IF GET_GAME_TIMER() > iCopCarDamageTimer
		//IF DOES_ENTITY_EXIST(viCopCar) AND IS_PED_UNINJURED(piAmanda) //AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda) AND IS_PED_IN_ANY_VEHICLE(piAmanda) AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) < 10
			VEHICLE_INDEX viAmanda = GET_VEHICLE_PED_IS_IN(piAmanda)
			IF IS_VEHICLE_OK(viAmanda) AND NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), viAmanda)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viAmanda, PLAYER_PED_ID())
					REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda, "GENERIC_FRIGHTENED_HIGH", "AMANDA_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					CPRINTLN(DEBUG_MISSION,"Player damaged the cop car, Amanda should be speaking")
					iCopCarDamageTimer = GET_GAME_TIMER() + 5000
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(viCopCar)
				ENDIF
			ELIF IS_ENTITY_ON_FIRE(viAmanda)
				REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda, "GENERIC_FRIGHTENED_HIGH", "AMANDA_NORMAL", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
				CPRINTLN(DEBUG_MISSION,"Cop car is on fire, Amanda should be speaking")
				iCopCarDamageTimer = GET_GAME_TIMER() + 5000
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TriggerHandcuffSound()
	IF bHandcuffSoundReady
		CPRINTLN(DEBUG_MISSION,"Allowed to play handcuff sound")
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CPRINTLN(DEBUG_MISSION,"Conversation playing: ")
			IF GET_GAME_TIMER() > iHandCuffTimer
				CPRINTLN(DEBUG_MISSION,"Starting handcuff noise")
				PLAY_SOUND_FROM_ENTITY(iHandcuffSound, "MICHAEL_EVENT_AMANDA_REMOVE_HANDCUFFS_MASTER", piAmanda)
				bHandcuffSoundReady = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers conversations between the player and Amanda
PROC ManageDriveConversations()
	IF iCurrentDriveConv < NUM_DRIVE_CONVS AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
	AND (MichaelAndAmandaInSameVehicle() OR MichaelAndAmandaOnFoot())
	AND IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
		IF iPreviousDriveConv = iCurrentDriveConv
			iCurrentDriveConv++
			iConversationTimer = GET_GAME_TIMER() + iDriveConvTimer
		ELSE
			IF GET_GAME_TIMER() > iConversationTimer
				ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", sDriveConvs[iCurrentDriveConv], CONV_PRIORITY_MEDIUM)
					iPreviousDriveConv = iCurrentDriveConv
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda's responses to the player crashing their car
PROC ManageCrashLines()
	IF iCurrentCrashLine < NUM_CRASH_LINES AND GET_GAME_TIMER() > iCrashLineTimer AND IsOkToPlayDialogue() AND MichaelAndAmandaInSameVehicle()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND NOT IS_PED_STOPPED(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viTemp)
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(viTemp)
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF (iCurrentCrashLine = 0 AND IS_VEHICLE_OK(viCopCar) AND IS_PED_IN_VEHICLE(piAmanda, viCopCar) AND GET_SEAT_PED_IS_IN(piAmanda) = VS_BACK_RIGHT) // Prevent the "Hello. No seatbelt!" line playing if not in the back of the original cop car - B*1491664
						OR ((iCurrentCrashLine = 1 OR iCurrentCrashLine = 2) AND IS_PED_IN_ANY_VEHICLE(piAmanda) AND (GET_SEAT_PED_IS_IN(piAmanda) = VS_BACK_RIGHT OR GET_SEAT_PED_IS_IN(piAmanda) = VS_BACK_LEFT))
						OR (iCurrentCrashLine = 3 AND IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())) // Prevent the "You cannot wreck this cop car. You cannot." line playing if not in a cop car - B*1231582
							ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
							IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_CRASH", sCrashLines[iCurrentCrashLine], CONV_PRIORITY_MEDIUM)
								iCurrentCrashLine++
								iCrashLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
							ENDIF
						ELSE
							iCurrentCrashLine++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda's responses to the player committing crimes
//PROC ManageCrimeLines()
//	IF iCurrentCrimeLine < NUM_CRIME_LINES AND GET_GAME_TIMER() > iCrimeLineTimer AND IsOkToPlayDialogue()
//	AND (MichaelAndAmandaInSameVehicle() OR MichaelAndAmandaOnFoot())
//		IF IS_PED_SHOOTING(PLAYER_PED_ID()) OR HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//			//	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_CRIME", sCrimeLines[iCurrentCrimeLine], CONV_PRIORITY_MEDIUM)
//					CPRINTLN(DEBUG_MISSION, "ManageCrimeLines: Triggered crime line", iCurrentCrimeLine)
//					iCurrentCrimeLine++
//					iCrimeLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
//					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
//			//	ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

/// PURPOSE:
///    Triggers Amanda's responses to the player not moving
PROC ManageStationaryLines()
	IF iCurrentStillLine < NUM_STILL_LINES AND GET_GAME_TIMER() > iStillLineTimer AND IsOkToPlayDialogue()
	AND MichaelAndAmandaInSameVehicle() //OR MichaelAndAmandaOnFoot())
		IF IS_PED_STOPPED(PLAYER_PED_ID())
			// check stopped timer, do line if too long
			iStillTime++
			IF iStillTime > iStillTimeout //AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vOutsideGatePos1, vOutsideGatePos2, fOutsideGateWidth)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT IS_THIS_PRINT_BEING_DISPLAYED("MEA1_04")	// Go ~y~home.
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF iCurrentStillLine = 2 AND NOT MichaelAndAmandaInSameVehicle()
						iCurrentStillLine++
					ELIF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_STILL", sStillLines[iCurrentStillLine], CONV_PRIORITY_MEDIUM)
						iCurrentStillLine++
						iStillTime = 0
						iStillLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iStillTime = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Amanda whines if Michael gets out of the cop car while she's still cuffed - B*1077704
PROC ManageAbandonLines()
	// If Amanda is still in cop car and the home objective hasn't been shown, Amanda is still cuffed
	IF IS_ENTITY_ALIVE(viCopCar) AND IS_PED_UNINJURED(piAmanda) AND IS_PED_IN_VEHICLE(piAmanda, viCopCar) //bShowHomeObjective = FALSE
	AND GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) < 10
		IF GET_GAME_TIMER() > iAbandonLineTimer
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				IF IS_THIS_PRINT_BEING_DISPLAYED("MEA1_03") // Lose the cops
				OR IS_THIS_PRINT_BEING_DISPLAYED("MEA1_05") // Return to Amanda
				OR IS_THIS_PRINT_BEING_DISPLAYED("MEA1_06") // Get back in the cop car
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HELP", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
						CPRINTLN(DEBUG_MISSION, "Playing Amanda abandon complaint")
						iAbandonLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HELP", CONV_PRIORITY_MEDIUM)
						CPRINTLN(DEBUG_MISSION, "Playing Amanda abandon complaint")
						iAbandonLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda's responses to the player turning on the cop siren
PROC ManageSirenLines()
	IF iCurrentSirenLine < NUM_SIREN_LINES AND GET_GAME_TIMER() > iSirenLineTimer AND IsOkToPlayDialogue() AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND MichaelAndAmandaInSameVehicle()
		VEHICLE_INDEX viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayer) AND IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(viPlayer))
			bIsSirenOn = IS_VEHICLE_SIREN_ON(viPlayer)
			IF bIsSirenOn
				// Player has just turned on the siren, play dialogue
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND bWasSirenOn = FALSE
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_SIREN", sSirenLines[iCurrentSirenLine], CONV_PRIORITY_MEDIUM)
						iCurrentSirenLine++
						bWasSirenOn = bIsSirenOn
						iSirenLineTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
					ENDIF
				ENDIF
			ELSE
				bWasSirenOn = bIsSirenOn
			ENDIF
		ENDIF
	ELIF bWasSirenOn AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayer) AND IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(viPlayer))
			bWasSirenOn = IS_VEHICLE_SIREN_ON(viPlayer)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda telling the player to lose the cops
PROC ManageLoseCopLines()
	IF iCurrentLoseCopLine < NUM_LOSECOP_LINES AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
	AND MichaelAndAmandaInSameVehicle() //OR MichaelAndAmandaOnFoot())
	//AND IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
	AND NOT IS_CHAR_ALMOST_STOPPED(PLAYER_PED_ID())
		IF GET_GAME_TIMER() > iLoseCopsTimer
			ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
			iSelectedReply = GET_RANDOM_INT_IN_RANGE(0, NUM_LOSECOP_REPLY)
			IF iSelectedReply = iPreviousReply
				iSelectedReply++
				IF iSelectedReply >= NUM_LOSECOP_REPLY
					iSelectedReply = 0
				ENDIF
			ENDIF
			//IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_LOSEGEN", sLoseCopLines[iCurrentLoseCopLine], CONV_PRIORITY_MEDIUM)
			IF CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(pedConvStruct, "MEA1AUD", "MEA1_LOSEGEN", sLoseCopLines[iCurrentLoseCopLine], "MEA1_GOTTHIS", sLoseCopReply[iSelectedReply], CONV_PRIORITY_MEDIUM)
				iLoseCopsTimer = GET_GAME_TIMER() + DRIVING_LINES_TIMEOUT
				iCurrentLoseCopLine++
				iPreviousReply = iSelectedReply
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda complaining that the player hasn't gone home
PROC ManageNotAtHomeLines()
	IF iCurrentNotHomeLine < NUM_NOT_HOME_LINES AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND IsOkToPlayDialogue()
	AND MichaelAndAmandaInSameVehicle() //OR MichaelAndAmandaOnFoot())
	AND IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
	//AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF GET_GAME_TIMER() > iNotHomeTimer
			ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
			IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_NOHOME", sNotHomeLines[iCurrentNotHomeLine], CONV_PRIORITY_MEDIUM)
				iNotHomeTimer = GET_GAME_TIMER() + (iStartLinesTimer*2)
				iCurrentNotHomeLine++
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the player's current car and adds it to the upside down check
PROC GetCurrentPlayersCar()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND GET_GAME_TIMER() > iCheckVehicleTimer
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX viNewVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF viNewVehicle <> viPlayersVehicle
				IF viNewVehicle = viCopCar
					viPlayersVehicle = viNewVehicle
				ELSE
					IF IS_VEHICLE_OK(viPlayersVehicle)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(viPlayersVehicle)
					ENDIF
					viPlayersVehicle = viNewVehicle
				ENDIF
			ENDIF
		ENDIF
		iCheckVehicleTimer = GET_GAME_TIMER() + iCheckVehicle
	ENDIF
ENDPROC

/// PURPOSE:
///    Triggers Amanda's responses to the player getting their car stuck on its roof
PROC PlayUpsideDownLine()
	IF iCurrentRoofLine < NUM_ROOF_LINES AND IS_VEHICLE_OK(viPlayersVehicle) AND IsOkToPlayDialogue()
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viPlayersVehicle) AND IS_PED_IN_VEHICLE(piAmanda, viPlayersVehicle)
			IF IS_VEHICLE_STUCK_TIMER_UP(viPlayersVehicle, VEH_STUCK_HUNG_UP, iRoofTime)
			OR IS_VEHICLE_STUCK_TIMER_UP(viPlayersVehicle, VEH_STUCK_JAMMED, iRoofTime)
			OR IS_VEHICLE_STUCK_TIMER_UP(viPlayersVehicle, VEH_STUCK_ON_ROOF, iRoofTime)
			OR IS_VEHICLE_STUCK_TIMER_UP(viPlayersVehicle, VEH_STUCK_ON_SIDE, iRoofTime)
				IF iPreviousRoofLine = iCurrentRoofLine
					iCurrentRoofLine++
					iRoofConvTimer = GET_GAME_TIMER() + iRoofTime
				ELSE
					IF GET_GAME_TIMER() > iRoofConvTimer AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						// play upside down dialogue line here
						ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_ROOF", sRoofLines[iCurrentRoofLine], CONV_PRIORITY_MEDIUM)
							iPreviousRoofLine = iCurrentRoofLine
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pick a sensible spawnpoint near to where the CP coords got set
/// PARAMS:
///    vCoords - Current checkpoint spawn vector, will be reset to one of the presets
///    fHeading - Current spawn heading, will be reset to one of the presets
PROC GET_NEAREST_PRESET_SPAWN_TO_CP(VECTOR &vCoords, FLOAT &fHeading)
      
      // Set up possible spawn coords/heading
      VECTOR vSpawnCoords[4]
      FLOAT fSpawnHead[4]
      vSpawnCoords[0] = << -713.50, -174.48, 36.50 >>
      fSpawnHead[0] = 28.96
      vSpawnCoords[1] = << -645.00, -47.36, 40.22 >>
      fSpawnHead[1] = 98.09
      vSpawnCoords[2] = << -531.74, -270.68, 34.83 >>
      fSpawnHead[2] = 111.76
      vSpawnCoords[3] = << -183.39, -58.87, 51.45 >>
      fSpawnHead[3] = 71.13
      
      // Index for best match found so far
      INT iBestMatch = 0
      INT iCount = 1
      WHILE iCount < 4
            // Compare distances for current 'best' and next in list (actually comparing the squares but amounts to the same thing)
            IF VDIST2(vSpawnCoords[iCount], vCoords) < VDIST2(vSpawnCoords[iBestMatch], vCoords)
                  iBestMatch = iCount
            ENDIF
            iCount++
      ENDWHILE
      
      // Set the passed in variables to be the best match found
      vCoords = vSpawnCoords[iBestMatch]
      fHeading = fSpawnHead[iBestMatch]
      
ENDPROC

/// PURPOSE:
///    Checks if the shop scene models have loaded
/// PARAMS:
///    iTimer - timer to continue the mission if models have failed to load after one second
/// RETURNS:
///    TRUE when all models have loaded or one second has passed
FUNC BOOL AreSceneModelsLoaded(INT & iTimer)
	IF iTimer < 0
		iTimer = GET_GAME_TIMER() + 2000
	ELIF GET_GAME_TIMER() > iTimer
		RETURN TRUE
	ENDIF
	IF NOT HAS_MODEL_LOADED(mnCopCar)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(mnCop)
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_AMANDA))
		RETURN FALSE
	ENDIF
	IF NOT HAS_MODEL_LOADED(mnShopkeeper)
		RETURN FALSE
	ENDIF
	/*IF NOT HAS_MODEL_LOADED(mnNotebook)
		RETURN FALSE
	ENDIF*/
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the shop scene entities have been created
/// PARAMS:
///    iTimer - timer to continue the mission if models have failed to load after one second
/// RETURNS:
///    TRUE when they've all been created or if one second has passed
FUNC BOOL DoSceneEntitiesExist(INT & iTimer)
	IF iTimer < 0
		iTimer = GET_GAME_TIMER() + 2000
	ELIF GET_GAME_TIMER() > iTimer
		RETURN TRUE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(viCopCar)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piCop)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piAmanda)
		RETURN FALSE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(piShopkeeper)
		RETURN FALSE
	ENDIF
	/*IF NOT DOES_ENTITY_EXIST(oiNotebook)
		RETURN FALSE
	ENDIF*/
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Loads the models and spawns the shop scene while faded out
PROC DebugSpawnShopScene()
	INT iTimer = -1
	LoadSceneModels()
	WHILE NOT AreSceneModelsLoaded(iTimer)
		WAIT(0)
	ENDWHILE
	
	iTimer = -1
	SpawnShopScene()
	WHILE NOT DoSceneEntitiesExist(iTimer)
		SpawnShopScene()
		WAIT(0)
	ENDWHILE
ENDPROC

/// PURPOSE:
///    Figures out how long the player has to reach Amanda, based on how far away they are
PROC SetMissionTime()
	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vShopLocation)
	iDriveToShopTime = (ROUND(fDist) * 180)
	CPRINTLN(DEBUG_MISSION, "Time to destination ", iDriveToShopTime)
	IF iDriveToShopTime < MIN_DRIVE_TO_SHOP_TIME
		iDriveToShopTime = MIN_DRIVE_TO_SHOP_TIME
	ENDIF
	iFailTimer = GetTimeInSeconds() + iDriveToShopTime
ENDPROC

/// PURPOSE:
///    Resets the amount of time the player has to reach Amanda when they arrive at the shop
PROC SetMissionTimeAtShop()
	FLOAT fDifference = TO_FLOAT(iFailTimer) - TO_FLOAT(GetTimeInSeconds())
	FLOAT fAdjustment = TO_FLOAT(AT_SHOP_FAIL_TIME) / fDifference
	fAdjustment *= TO_FLOAT(iDriveToShopTime)
	iFailTimer = AT_SHOP_FAIL_TIME + GetTimeInSeconds()
	iDriveToShopTime = ROUND(fAdjustment)
ENDPROC

/// PURPOSE:
///    Skips to the Rescue Amanda stage, used when restarting and Z skipping
PROC SKIP_TO_RESCUE_AMANDA()
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-832.1946, -178.3130, 36.5807>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 41.9602)
		VEHICLE_INDEX viPlayer = GET_PLAYERS_LAST_VEHICLE()
		IF IS_VEHICLE_OK(viPlayer)
			SET_ENTITY_COORDS(viPlayer, <<-836.72, -179.78, 37.10>>)
			SET_ENTITY_HEADING(viPlayer, 32.61)
		ELSE
			VEHICLE_INDEX viTemp
			CREATE_VEHICLE_FOR_REPLAY(viTemp, <<-836.72, -179.78, 37.10>>, 32.61, FALSE, TRUE, TRUE, TRUE, TRUE, TAILGATER)
		ENDIF
		CLEAR_AREA_OF_VEHICLES(<<-836.72, -179.78, 37.10>>, 100)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		DISTANT_COP_CAR_SIRENS(FALSE)
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		RemovePedsAndVehicles(TRUE)
		DebugSpawnShopScene()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
		missionSubstate = MSS_PREPARE
		missionState = MS_RESCUE_AMANDA
	ENDIF
ENDPROC

/// PURPOSE:
///    Skips to the Drive Home stage, used when restarting and Z skipping
/// PARAMS:
///    bDebugSkip - is this a debug skip?
PROC SKIP_TO_DRIVE_HOME(BOOL bDebugSkip = FALSE)
	RemovePedsAndVehicles(TRUE)
	DebugSpawnShopScene()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
		IF bDebugSkip
			IF IS_VEHICLE_OK(viCopCar) AND IS_VEHICLE_SEAT_FREE(viCopCar)
				TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viCopCar)
				SET_ENTITY_COORDS(viCopCar, << -761.0187, -151.4704, 36.2233 >>)
				SET_ENTITY_HEADING(viCopCar, 209.2)
				FREEZE_ENTITY_POSITION(viCopCar, FALSE)
			ENDIF
		ELSE
			VEHICLE_INDEX viTemp
			VECTOR vPos = GET_REPLAY_CHECKPOINT_PLAYER_POSITION()
			FLOAT fHeading
			GET_NEAREST_PRESET_SPAWN_TO_CP(vPos, fHeading)
			CREATE_VEHICLE_FOR_REPLAY(viTemp, vPos, fHeading, TRUE, FALSE, TRUE, TRUE, TRUE, mnCopCar)
			IF IS_VEHICLE_OK(viTemp) AND IS_VEHICLE_SEAT_FREE(viTemp, VS_BACK_RIGHT) AND IS_PED_UNINJURED(piAmanda)
				SET_VEHICLE_EXTRA(viTemp, VEHICLE_SETUP_FLAG_EXTRA_1, TRUE)
				SET_PED_INTO_VEHICLE(piAmanda, viTemp, VS_BACK_RIGHT)
				SAFE_RELEASE_VEHICLE(viCopCar)
			ENDIF
		ENDIF
		ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		DISTANT_COP_CAR_SIRENS(FALSE)
	ENDIF
	
	IF IS_PED_UNINJURED(piCop)
		TASK_WANDER_STANDARD(piCop)
		SAFE_RELEASE_PED(piCop)
	ENDIF
	IF IS_PED_UNINJURED(piShopkeeper)
		TASK_WANDER_STANDARD(piShopkeeper)
		SAFE_RELEASE_PED(piShopkeeper)
	ENDIF
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	bShowHomeObjective = TRUE
	bShowReturnObjective = TRUE
	iCurrentDriveConv = -1
	iPreviousDriveConv = -1
	iCurrentRoofLine = -1
	iPreviousRoofLine = -1
	iCurrentCrashLine = 0
//	iCurrentCrimeLine = 0
	iCurrentStillLine = 0
	iCurrentSirenLine = 0
	bIsSirenOn = FALSE
	bWasSirenOn = FALSE
	iCrashLineTimer = 0
//	iCrimeLineTimer = 0
	iStillLineTimer = 0
	iSirenLineTimer = 0
	//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
	missionSubstate = MSS_PREPARE
	missionState = MS_DRIVE_HOME
ENDPROC

// ===========================================================================================================
//		Debug functions
// ===========================================================================================================
#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Skips to the next stage of the mission
	PROC J_SKIP()
		INT iLoopTimer
		//iStartDay = -1
		SetMissionTime()
	
		RC_START_Z_SKIP()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		SWITCH missionState
			CASE MS_DRIVE_TO_SHOP
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -840.1384, -182.7698, 36.5921 >>)
					VEHICLE_INDEX tempVehicle
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						tempVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
					IF IS_VEHICLE_OK(tempVehicle)
						SET_ENTITY_HEADING(tempVehicle, 18.0)
					ELSE
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 18.0)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					DebugSpawnShopScene()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
				ENDIF
			BREAK
			
			CASE MS_RESCUE_AMANDA
				CPRINTLN(DEBUG_MISSION,"Doing jskip from rescue amanda state")
				
				// force wanted level to change so skip doesn't fail
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
						CPRINTLN(DEBUG_MISSION,"J skip is setting the player's wanted level")
						
						iLoopTimer = GET_GAME_TIMER()
						WHILE GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
							WAIT(0)
							CPRINTLN(DEBUG_MISSION,"Waiting for player to become wanted")
							IF IS_PED_UNINJURED(PLAYER_PED_ID())
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
								ENDIF
							ENDIF
							IF GET_GAME_TIMER() > iLoopTimer + 5000
								CPRINTLN(DEBUG_MISSION,"J skip failed to set wanted level")
								EXIT
							ENDIF
						ENDWHILE
					ENDIF
				ENDIF
			
				IF IS_PED_UNINJURED(piAmanda)
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				ENDIF
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viCopCar) 
					IF IS_VEHICLE_SEAT_FREE(viCopCar)
						TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viCopCar)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					FREEZE_ENTITY_POSITION(viCopCar, FALSE)
					//bAllowSceneConv = FALSE
					//bAllowLooking = FALSE
					ActivateCops()
				ENDIF
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
			BREAK
			
			CASE MS_LOSE_COPS
				CPRINTLN(DEBUG_MISSION,"Doing jskip from lose cops state")
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				DISTANT_COP_CAR_SIRENS(FALSE)
				IF IS_PED_UNINJURED(piCop)
					CLEAR_PED_TASKS(piCop)
					TASK_WANDER_STANDARD(piCop)
				ENDIF
				IF IS_PED_UNINJURED(piShopkeeper)
					TASK_WANDER_STANDARD(piShopkeeper)
				ENDIF
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -761.0187, -151.4704, 36.2233 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 209.2)
					ELSE
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << -761.0187, -151.4704, 36.2233 >>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 209.2)
						SET_ENTITY_COORDS(piAmanda, << -760.0187, -150.4704, 36.2233 >>)
						SET_ENTITY_HEADING(piAmanda, 209.2)
					ENDIF
				ENDIF
				bShowHomeObjective = TRUE
				bShowReturnObjective = TRUE
				iCurrentDriveConv = -1
				iPreviousDriveConv = -1
				iCurrentRoofLine = -1
				iPreviousRoofLine = -1
				iCurrentCrashLine = 0
//				iCurrentCrimeLine = 0
				iCurrentStillLine = 0
				iCurrentSirenLine = 0
				bIsSirenOn = FALSE
				bWasSirenOn = FALSE
				iCrashLineTimer = 0
//				iCrimeLineTimer = 0
				iStillLineTimer = 0
				iSirenLineTimer = 0
				//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
			BREAK
			
			CASE MS_DRIVE_HOME
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
					IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX viTemp
							viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piAmanda, viTemp)
								SET_ENTITY_COORDS(viTemp, vHotelLocation)
								SET_ENTITY_HEADING(viTemp, 300.0)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vHotelLocation)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
								SET_ENTITY_COORDS(piAmanda, vHotelLocation+<<1,1,0>>)
								SET_ENTITY_HEADING(piAmanda, 300.0)
							ENDIF
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHotelLocation)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
							SET_ENTITY_COORDS(piAmanda, vHotelLocation+<<1,1,0>>)
							SET_ENTITY_HEADING(piAmanda, 300.0)
						ENDIF
					ELSE
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							VEHICLE_INDEX viTemp
							viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							IF IS_VEHICLE_OK(viTemp) AND IS_PED_IN_VEHICLE(piAmanda, viTemp)
								SET_ENTITY_COORDS(viTemp, vHomeLocation)
								SET_ENTITY_HEADING(viTemp, 300.0)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(), vHomeLocation)
								SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
								SET_ENTITY_COORDS(piAmanda, vHomeLocation+<<1,1,0>>)
								SET_ENTITY_HEADING(piAmanda, 300.0)
							ENDIF
						ELSE
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHomeLocation)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 300.0)
							SET_ENTITY_COORDS(piAmanda, vHomeLocation+<<1,1,0>>)
							SET_ENTITY_HEADING(piAmanda, 300.0)
						ENDIF
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		RC_END_Z_SKIP()
	ENDPROC
	
	/// PURPOSE:
	///    Skips to the previous stage of the mission
	PROC P_SKIP()
		RC_START_Z_SKIP()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		//iStartDay = -1
		SetMissionTime()
		SWITCH missionState
			CASE MS_RESCUE_AMANDA
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -788.8835, -252.5904, 36.1073 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 35.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					RemovePedsAndVehicles(TRUE)
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_SHOP
				ENDIF
			BREAK
			
			CASE MS_LOSE_COPS
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -840.1384, -182.7698, 36.5921 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 18.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					DISTANT_COP_CAR_SIRENS(FALSE)
					RemovePedsAndVehicles(TRUE)
					DebugSpawnShopScene()
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
					missionSubstate = MSS_PREPARE
					missionState = MS_RESCUE_AMANDA
				ENDIF
			BREAK
			
			CASE MS_DRIVE_HOME
				IF IS_VEHICLE_OK(viCopCar) AND IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), viCopCar)
					SET_ENTITY_COORDS(viCopCar, vCopCarPos)
					SET_ENTITY_HEADING(viCopCar, fCopCarHeading)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					bShowHomeObjective = TRUE
					bShowReturnObjective = TRUE
					//bAllowSceneConv = FALSE
					//bAllowLooking = FALSE
					iCurrentDriveConv = -1
					iPreviousDriveConv = -1
					iCurrentRoofLine = -1
					iPreviousRoofLine = -1
					iCurrentCrashLine = 0
//					iCurrentCrimeLine = 0
					iCurrentStillLine = 0
					iCurrentSirenLine = 0
					bIsSirenOn = FALSE
					bWasSirenOn = FALSE
					iCrashLineTimer = 0
//					iCrimeLineTimer = 0
					iStillLineTimer = 0
					iSirenLineTimer = 0
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
					missionSubstate = MSS_ACTIVE_ONE
					missionState = MS_RESCUE_AMANDA
				ENDIF
			BREAK
		ENDSWITCH
		RC_END_Z_SKIP()
	ENDPROC
	
	/// PURPOSE:
	///    Skips to a player selected stage of the mission
	PROC Z_SKIP()
		INT debugJumpStage = -1
		IF LAUNCH_MISSION_STAGE_MENU(sSkipMenu, debugJumpStage)
			//iStartDay = -1
			RC_START_Z_SKIP()
			SetMissionTime()
			IF debugJumpStage = 0
				// Drive to shop
				IF IS_PED_UNINJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -788.8835, -252.5904, 36.1073 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 35.0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					DISTANT_COP_CAR_SIRENS(FALSE)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					RemovePedsAndVehicles(TRUE)
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_SHOP
				ENDIF
			ELIF debugJumpStage = 1
				// Rescue Amanda
				SKIP_TO_RESCUE_AMANDA()
			ELIF debugJumpStage = 2
				// Lose cops
				RemovePedsAndVehicles(TRUE)
				DebugSpawnShopScene()
				IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viCopCar) AND IS_VEHICLE_SEAT_FREE(viCopCar)
					TASK_WARP_PED_INTO_VEHICLE(PLAYER_PED_ID(), viCopCar)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				ENDIF
				IF IS_PED_UNINJURED(piAmanda)
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//bAllowSceneConv = FALSE
				//bAllowLooking = FALSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
				missionSubstate = MSS_ACTIVE_ONE
				missionState = MS_RESCUE_AMANDA
			ELIF debugJumpStage = 3
				// Drive home
				SKIP_TO_DRIVE_HOME(TRUE)
			ENDIF
			RC_END_Z_SKIP()
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Checks for the player pressing a mission skip debug key
	PROC CheckDebugKeys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			J_SKIP()
		ENDIF
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			P_SKIP()
		ENDIF
		Z_SKIP()
	ENDPROC

	/// PURPOSE:
	///    Makes Amanda walk off if the player debug passes the mission
	PROC DebugPassDealWithAmanda()
		IF IS_PED_UNINJURED(piAmanda)
			IF IS_VEHICLE_OK(viCopCar)
				IF IS_PED_IN_VEHICLE(piAmanda, viCopCar) AND IS_VEHICLE_SEAT_FREE(viCopCar, VS_FRONT_RIGHT)
					SAFE_DELETE_PED(piAmanda)
					EXIT
				ENDIF
			ENDIF
			
			REMOVE_PED_FROM_GROUP(piAmanda)
			SEQUENCE_INDEX siAmanda
			OPEN_SEQUENCE_TASK(siAmanda)
				IF IS_PED_IN_ANY_VEHICLE(piAmanda)
					TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				ENDIF
				TASK_WANDER_STANDARD(NULL)
			CLOSE_SEQUENCE_TASK(siAmanda)
			TASK_PERFORM_SEQUENCE(piAmanda, siAmanda)
			CLEAR_SEQUENCE_TASK(siAmanda)
			SET_PED_KEEP_TASK(piAmanda, TRUE)
			SAFE_RELEASE_PED(piAmanda)
		ENDIF
	ENDPROC
#ENDIF

// ===========================================================================================================
//		Script States
// ===========================================================================================================
/// PURPOSE:
///    Initialises the mission
PROC INIT()
	REQUEST_ADDITIONAL_TEXT("MEA1", MISSION_TEXT_SLOT)
	
	IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		CPRINTLN(DEBUG_MISSION,"MS_INIT")
		
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(pedConvStruct, MICHAEL_ID, PLAYER_PED_ID(), "MICHAEL", TRUE)
			//SET_PED_GESTURE_GROUP(PLAYER_PED_ID(), sPlayerGestureGroup)
			SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(), TRUE)
		ENDIF
		ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_PHONE_ID, NULL, "AMANDA")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			rghPlayer = GET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID())
		ENDIF
		
		SET_AUDIO_FLAG("WantedMusicOnMission", TRUE)
		
		sDriveConvs[0] = "MEA1_DRIVE1"
		sDriveConvs[1] = "MEA1_DRIVE2"
		sDriveConvs[2] = "MEA1_DRIVE3"
		sDriveConvs[3] = "MEA1_CHAT"
		
		sStartLines = "MEA1_MICHAEL_1"
		/*sStartLines[0] = "MEA1_MICHAEL_1"
		sStartLines[1] = "MEA1_MICHAEL_2"
		sStartLines[2] = "MEA1_MICHAEL_3"
		sStartLines[3] = "MEA1_MICHAEL_4"
		sStartLines[4] = "MEA1_MICHAEL_5"*/
		
		sCrashLines[0] = "MEA1_CRASH_1"
		sCrashLines[1] = "MEA1_CRASH_2"
		sCrashLines[2] = "MEA1_CRASH_3"
		
//		sCrimeLines[0] = "MEA1_CRIME_1"
//		sCrimeLines[1] = "MEA1_CRIME_2"
//		sCrimeLines[2] = "MEA1_CRIME_3"
		
		sStillLines[0] = "MEA1_STILL_1"
		sStillLines[1] = "MEA1_STILL_2"
		sStillLines[2] = "MEA1_STILL_3"
		
		sSirenLines[0] = "MEA1_SIREN_1"
		sSirenLines[1] = "MEA1_SIREN_2"
		sSirenLines[2] = "MEA1_SIREN_3"
		
		sRoofLines[0] = "MEA1_ROOF_1"
		sRoofLines[1] = "MEA1_ROOF_2"
		sRoofLines[2] = "MEA1_ROOF_3"
		
		sLoseCopLines[0] = "MEA1_LOSEGEN_1"
		sLoseCopLines[1] = "MEA1_LOSEGEN_2"
		sLoseCopLines[2] = "MEA1_LOSEGEN_3"
		sLoseCopLines[3] = "MEA1_LOSEGEN_4"
		sLoseCopLines[4] = "MEA1_LOSEGEN_5"
		sLoseCopLines[5] = "MEA1_LOSEGEN_6"
		sLoseCopLines[6] = "MEA1_LOSEGEN_7"
		sLoseCopLines[7] = "MEA1_LOSEGEN_8"
		sLoseCopLines[8] = "MEA1_LOSEGEN_9"
		sLoseCopReply[0] = "MEA1_GOTTHIS_1"
		sLoseCopReply[1] = "MEA1_GOTTHIS_2"
		sLoseCopReply[2] = "MEA1_GOTTHIS_3"
		sLoseCopReply[3] = "MEA1_GOTTHIS_4"
		
		sNotHomeLines[0] = "MEA1_NOHOME_1"
		sNotHomeLines[1] = "MEA1_NOHOME_2"
		sNotHomeLines[2] = "MEA1_NOHOME_3"
		
		sAmandaLines[0] = "MEA1_BEAA_01"
		sAmandaLines[1] = "MEA1_BEAA_02"
		sAmandaLines[2] = "MEA1_BEAA_03"
		sAmandaLines[3] = "MEA1_BEAA_04"
		sAmandaLines[4] = "MEA1_BEAA_05"
		
		#IF IS_DEBUG_BUILD
			sSkipMenu[0].sTxtLabel = "Drive to shop"
			sSkipMenu[1].sTxtLabel = "Rescue Amanda"
			sSkipMenu[2].sTxtLabel = "Lose cops"
			sSkipMenu[3].sTxtLabel = "Drive home"
		#ENDIF
		
		SetMissionTime()
		
		IF IS_REPEAT_PLAY_ACTIVE() AND IS_PED_UNINJURED(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			FLOAT fHead = GET_ENTITY_HEADING(PLAYER_PED_ID())
			CREATE_VEHICLE_FOR_REPLAY(viTemp, vPos, fHead, TRUE, FALSE, TRUE, TRUE, TRUE)
			CLEAR_AREA_OF_VEHICLES(vPos, 100)
		ENDIF
		
		IF IS_REPLAY_IN_PROGRESS()
			INT iReplayStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				iReplayStage++ // player is skipping this stage
			ENDIF
			
			SWITCH iReplayStage
				CASE CP_MISSION_START
					// don't do anything to the player if they failed before even reaching the mission
					VEHICLE_INDEX viTemp
					CREATE_VEHICLE_FOR_PHONECALL_TRIGGER_REPLAY(viTemp, TRUE, TRUE, TRUE, TRUE, TAILGATER)
					IF IS_ENTITY_IN_RANGE_COORDS(viTemp, <<-811.343750,187.436691,71.478607>>, 6.0)
						SET_ENTITY_COORDS(viTemp, <<-822.404419,182.607849,71.378090>>) 
						SET_ENTITY_HEADING(viTemp, 135.654083)
					ENDIF
					SAFE_RELEASE_VEHICLE(viTemp)
					SUPPRESS_PLAYERS_VEHICLE()
					missionSubstate = MSS_PREPARE
					missionState = MS_DRIVE_TO_SHOP
					CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
				BREAK
				
				CASE CP_AT_SHOP
					IF IS_PED_UNINJURED(PLAYER_PED_ID())
						RC_START_Z_SKIP()
						SKIP_TO_RESCUE_AMANDA()
						SUPPRESS_PLAYERS_VEHICLE()
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						WAIT(500)
						RC_END_Z_SKIP()
					ENDIF
				BREAK
				
				/*CASE CP_DRIVE_HOME
					RC_START_Z_SKIP()
					SKIP_TO_DRIVE_HOME()
					SUPPRESS_PLAYERS_VEHICLE()
					WAIT(500)
					RC_END_Z_SKIP()
				BREAK*/
				
				CASE CP_MISSION_PASSED
					WAIT_FOR_WORLD_TO_LOAD(GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					RC_END_Z_SKIP()
					Mission_Passed()
				BREAK
				
				DEFAULT
					SCRIPT_ASSERT("Replay in progress: Unknown checkpoint selected")
				BREAK
			ENDSWITCH
		ELSE
			SAFE_FADE_SCREEN_IN_FROM_BLACK()
			SUPPRESS_PLAYERS_VEHICLE()
			missionSubstate = MSS_PREPARE
			missionState = MS_DRIVE_TO_SHOP
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Pauses the cop/shopkeeper conversation if the player drives too far away
PROC HIDE_SUBTITLES_IF_FAR_AWAY()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vArrestScenePos, 100)
		IF bPausedConversation
			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			bPausedConversation = FALSE
		ENDIF
	ELSE
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT bPausedConversation
				TEXT_LABEL_23 tlConv
				tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				STRING sConv
				sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
				IF ARE_STRINGS_EQUAL(sConv, "MEA1_SCENE")
					PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					bPausedConversation = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player drives to the shop where Amanda has been arrested
PROC DRIVE_TO_SHOP()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEA1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering drive to shop state")
			SAFE_REMOVE_BLIP(biObjective)
			//biObjective = CREATE_COORD_BLIP(vShopLocation, BLIPPRIORITY_HIGH)
			biObjective = CREATE_COORD_BLIP(vCopCarPos, BLIPPRIORITY_HIGH, FALSE)
			SET_BLIP_NAME_FROM_TEXT_FILE(biObjective, "MEA1_AMA")
			SET_BLIP_COLOUR(biObjective, BLIP_COLOUR_BLUE)
			SET_BLIP_ROUTE(biObjective, TRUE)
			SET_TAXI_DROPOFF_LOCATION_FOR_BLIP(biObjective, <<-838.47, -177.11, 36.92>>, 31.44)
			
			LoadSceneModels()
			
			iConversationTimer = GET_GAME_TIMER() + iStartLinesTimer
			iCurrentStartLine = -1
			iPreviousStartLine = -1
			
			bForceAmandaIntoGroup = FALSE
			bPausedConversation = FALSE
			
			PRINT_NOW("MEA1_01", DEFAULT_GOD_TEXT_TIME, 1)	// Go to ~y~Didier Sachs.
			//PRINT_HELP("MEA1_HELP")
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			IF IS_PED_UNINJURED(PLAYER_PED_ID())
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShopLocation) < fSpawnSetupDist)
					missionSubstate = MSS_ACTIVE_TWO
				ENDIF
			ENDIF
			FailIfTooLate()
		BREAK
		
		CASE MSS_ACTIVE_TWO
			SpawnShopScene()
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda) AND DOES_ENTITY_EXIST(piShopkeeper) AND DOES_ENTITY_EXIST(piCop) AND IS_VEHICLE_OK(viCopCar)
				IF /*IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vShopLocation, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
				OR*/ (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vShopLocation) < fBlipAmandaDist)
					//IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), viCopCar)
						iConvFailedTimer = -1
						missionSubstate = MSS_PREPARE
						missionState = MS_RESCUE_AMANDA
					//ENDIF
				ENDIF
				PlayerInterruptsSyncedScene()
			ENDIF
			ManagePlayerDamagesCopCarLines()
			FailIfTooLate()
			PlayerThreatensPeds()
		BREAK
	ENDSWITCH
	
	IF NOT IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_GO_TO_AMANDA")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			START_AUDIO_SCENE("M_E_AMANDA_GO_TO_AMANDA")
		ENDIF
	ENDIF
	PlayStartLines()
	ControlArrestScene()
	HIDE_SUBTITLES_IF_FAR_AWAY()
ENDPROC

PROC TRIGGER_AMANDA_ESCAPE_CONV()
	IF bAllowInCarConv AND GET_GAME_TIMER() > iInCarConvTimer AND MichaelAndAmandaInSameVehicle()
		REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda,"MEA1_BFAA","AMANDA",SPEECH_PARAMS_FORCE_NORMAL)
		bAllowInCarConv = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    The player arrives at the shop and rescues Amanda
PROC RESCUE_AMANDA()
	IF IS_PED_UNINJURED(piCop) AND IS_PED_UNINJURED(piShopkeeper)
		HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEA1AUD", tSavedConversationRoot, tSavedConversationLabel)
	ENDIF
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering rescue amanda state")
			//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				/*IF iConvFailedTimer = -1
					iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
				ENDIF*/
				/*IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_ARRIVE", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
				OR CONVERSATION_TIMED_OUT(iConvFailedTimer)*/
					SAFE_REMOVE_BLIP(biObjective)
					IF IS_VEHICLE_OK(viCopCar)
						biObjective = CREATE_VEHICLE_BLIP(viCopCar, TRUE)
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_GO_TO_AMANDA")
						STOP_AUDIO_SCENE("M_E_AMANDA_GO_TO_AMANDA")
					ENDIF
					START_AUDIO_SCENE("M_E_AMANDA_ENTER_COP_CAR")
					//bAllowSceneConv = TRUE
					//bAllowAmandaConv = TRUE
					//bAllowLooking = TRUE
					bAllowInCarConv = TRUE
					bNotBumpedYet = TRUE
					bDisplaySeatObjective = TRUE
					//iSceneConvLine = 0
					eGriefState = GCS_READY
					/*PRINT_NOW("MEA1_02", DEFAULT_GOD_TEXT_TIME, 1)	// Get in the ~b~cop car.
					CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)*/
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_AT_SHOP, "Rescue Amanda")
					SetMissionTimeAtShop()
					iGriefingTimer = GET_GAME_TIMER() + GRIEFING_TIME
					VEHICLE_INDEX viPlayer
					viPlayer = GET_PLAYERS_LAST_VEHICLE()
					IF IS_VEHICLE_OK(viPlayer) AND GET_ENTITY_MODEL(viPlayer) <> mnCopCar
						SET_MISSION_LAST_VEHICLE_AS_VEHICLE_GEN(<<-823, 181, 71>>, 71.0, TRUE, CHAR_MICHAEL)	// Vehicle will be cleared up when player is 310m away from it
					ENDIF
					missionSubstate = MSS_ACTIVE_ONE
					CPRINTLN(DEBUG_MISSION,"Entering rescue amanda state: Active 1")
					
					//B* - 1980359
					SET_STATIC_BLIP_ACTIVE_STATE(g_sSavehouses[SAVEHOUSE_MICHAEL_BH].eBlip, TRUE)	
					
				//ENDIF
			//ENDIF
			FailIfTooLate()
		BREAK
		
		CASE MSS_ACTIVE_ONE
			ControlArrestScene()
			PlayerInCar()
			PlayerThreatensPeds()
			ManageGriefingLines()
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_VEHICLE_OK(viCopCar) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(viCopCar, PLAYER_PED_ID()) AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(viCopCar, FALSE)
				MinorCopAlert()
			ENDIF
			FailIfTooLate()
		BREAK
		
		CASE MSS_ACTIVE_TWO
			IF IS_VEHICLE_OK(viCopCar)
				/*IF bAllowInCarConv //AND GET_GAME_TIMER() > iInCarConvTimer
					REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda,"MEA1_BFAA","AMANDA",SPEECH_PARAMS_FORCE_NORMAL)
					bAllowInCarConv = FALSE
				ENDIF*/
				/*IF bAllowInCarConv
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_INCAR", CONV_PRIORITY_MEDIUM)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
						IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_ENTER_COP_CAR")
							STOP_AUDIO_SCENE("M_E_AMANDA_ENTER_COP_CAR")
						ENDIF
						START_AUDIO_SCENE("M_E_AMANDA_ESCAPE_COPS")
						iConvFailedTimer = -1
						bAllowInCarConv = FALSE
					ENDIF
				ELSE*/
					//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR NOT IS_VEHICLE_STOPPED(viCopCar)
						IF IS_PED_UNINJURED(piCop)
							/*IF iConvFailedTimer = -1
								iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
							ENDIF*/
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							
							IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_COP", CONV_PRIORITY_MEDIUM)
							OR CONVERSATION_TIMED_OUT(iConvFailedTimer, 2000)
							//OR GET_GAME_TIMER() > iConvFailedTimer
								ActivateCops()
								//TASK_TURN_PED_TO_FACE_ENTITY(piCop, PLAYER_PED_ID(), -1)
								iInCarConvTimer = GET_GAME_TIMER() + 1000
								eArrestSceneStage = ASS_BREAKOUT
								copChatState = CCS_DONE
							ENDIF
						ELSE
							ActivateCops()
						ENDIF
					//ENDIF
				//ENDIF
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_THREE
			TRIGGER_AMANDA_ESCAPE_CONV()
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				ActivateCops()
			ENDIF
		BREAK
	ENDSWITCH
	
	PlayerInterruptsSyncedScene()
	ManagePlayerDamagesCopCarLines()
	//PlayCopShopkeeperConv()
	//ControlArrestScene()
	//PlayNearAmandaConv()
	TrashedCopCarFail()
	MakeShopkeeperFlee()
	HIDE_SUBTITLES_IF_FAR_AWAY()
ENDPROC

/// PURPOSE:
///    The player has to escape from the cops
PROC LOSE_COPS()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEA1AUD", tSavedConversationRoot, tSavedConversationLabel)
	IF IS_PED_UNINJURED(piAmanda)
		SET_PED_CAN_PLAY_GESTURE_ANIMS(piAmanda, TRUE)
	ENDIF
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering lose cops state")
			SAFE_REMOVE_BLIP(biObjective)
			PRINT_NOW("MEA1_03", DEFAULT_GOD_TEXT_TIME, 1)	// Lose the cops.
			CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
			missionSubstate = MSS_ACTIVE_ONE
			iCopGunTimer = GET_GAME_TIMER() + iCopChaseTime
			iConvFailedTimer = -1
			
			//SET_PED_DESIRED_MOVE_BLEND_RATIO(piAmanda, PEDMOVE_SPRINT)
			
			//iInCarConvTimer = GET_GAME_TIMER() + 500
		BREAK
		
		CASE MSS_ACTIVE_ONE
			/*IF IS_PED_UNINJURED(piAmanda)
				SET_PED_MIN_MOVE_BLEND_RATIO(piAmanda, PEDMOVE_RUN)
			ENDIF*/
			TRIGGER_AMANDA_ESCAPE_CONV()
			IF bLoadHandcuffSound
				IF REQUEST_MISSION_AUDIO_BANK("MICHAEL_EVENT_AMANDA_1")
					iHandcuffSound = GET_SOUND_ID()
					bLoadHandcuffSound = FALSE
				ENDIF
			ENDIF
			MakeShopkeeperFlee()
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda) //AND IS_VEHICLE_OK(viCopCar)
				ManageLoseCopLines()
				ManageCrashLines()
				//ManageStationaryLines()
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					IF AmandaReleased()
						//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
						missionSubstate = MSS_PREPARE
						/*IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGaragePos1, vGaragePos2, fGarageWidth)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vHousePos1, vHousePos2, fHouseWidth)
							missionState = MS_AT_HOME
						ELSE*/
							missionState = MS_DRIVE_HOME
						//ENDIF
					ENDIF
				ENDIF
				IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
					IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) > (fWarnDistance*2)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						missionSubstate = MSS_PREPARE
						missionState = MS_RETURN_TO_AMANDA
					ENDIF
				ELSE
					IF IS_VEHICLE_OK(viCopCar) AND IS_PED_SITTING_IN_VEHICLE(piAmanda, viCopCar) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar) AND NOT IS_PLAYER_TOWING_VEHICLE(viCopCar)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						missionSubstate = MSS_PREPARE
						missionState = MS_RETURN_TO_AMANDA
					ENDIF
				ENDIF
			ENDIF
			GiveWeaponToCop()
			//IF bAllowInCarConv
				/*IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND MichaelAndAmandaInSameVehicle()
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_INCAR", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
						bAllowInCarConv = FALSE
					ENDIF
				ENDIF*/
				/*IF GET_GAME_TIMER() > iInCarConvTimer
					REMOVE_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(piAmanda,"MEA1_BFAA","AMANDA",SPEECH_PARAMS_FORCE_NORMAL)
					bAllowInCarConv = FALSE
				ENDIF*/
			//ENDIF
		BREAK
	ENDSWITCH
	ReleaseShopkeeper()
	TrashedCopCarFail()
	CheckForFreeSeats()
	ManagePlayerDamagesCopCarLines()
	// Prevent player losing wanted level if at Michael's house B*1224925
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -806.21, 165.83, 68.98 >>, << 52.00, 36.00, 16.25 >>)
	OR IS_ENTITY_AT_COORD(piAmanda, << -806.21, 165.83, 68.98 >>, << 52.00, 36.00, 16.25 >>)
		SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
		//SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(prop_lrggate_02_ld, << -844.05, 155.97, 66.03 >>, FALSE, 1.0)
	ENDIF
ENDPROC

PROC CheckAtDestination()
	VECTOR vDest
	IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
		vDest = vHotelLocation
	ELSE
		vDest = vHomeLocation
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<1,1,LOCATE_SIZE_HEIGHT>>, TRUE)
	OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDest) < LOCATE_SIZE_MISSION_TRIGGER)//5.0)
		BOOL bIsOnGround
		bIsOnGround = FALSE
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX viTemp
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viTemp)
				IF IS_VEHICLE_ON_ALL_WHEELS(viTemp)
					bIsOnGround = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
				bIsOnGround = TRUE
			ENDIF
		ENDIF
		IF bIsOnGround
			missionSubstate = MSS_PREPARE
			IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
				missionState = MS_AT_HOTEL
			ELSE
				missionState = MS_AT_HOME
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    The player drives home with Amanda
PROC DRIVE_HOME()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEA1AUD", tSavedConversationRoot, tSavedConversationLabel)
	IF IS_PED_UNINJURED(piAmanda)
		SET_PED_CAN_PLAY_GESTURE_ANIMS(piAmanda, TRUE)
	ENDIF
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering drive home state")
			SAFE_REMOVE_BLIP(biObjective)
			IF IS_PED_UNINJURED(piCop)
				CLEAR_PED_TASKS(piCop)
				TASK_WANDER_STANDARD(piCop)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_ESCAPE_COPS")
				STOP_AUDIO_SCENE("M_E_AMANDA_ESCAPE_COPS")
				START_AUDIO_SCENE("M_E_AMANDA_DRIVE_HOME")
			ENDIF
			RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
			IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
				bShowHomeObjective = FALSE
				bShowReturnObjective = TRUE
			ENDIF
			IF AmandaReleased()
				/*biObjective = CREATE_COORD_BLIP(vHomeLocation)
				SET_BLIP_DISPLAY(biObjective, DISPLAY_NOTHING)*/
				//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(CP_DRIVE_HOME, "Drive Home", TRUE)
				IF HAS_ANIM_DICT_LOADED(sAmandaMoveDict)
					SET_PED_MOVEMENT_CLIPSET(piAmanda, sAmandaMoveDict)
				ENDIF
				iNotHomeTimer = GET_GAME_TIMER() + (iStartLinesTimer*2)
				//iConversationTimer = GET_GAME_TIMER() + 200
				PRINT_NOW("MEA1_04", DEFAULT_GOD_TEXT_TIME, 1)	// Go ~y~home.
				IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
					biObjective = CREATE_COORD_BLIP(vHotelLocation)
				ELSE
					biObjective = CREATE_COORD_BLIP(vHomeLocation)
				ENDIF
				missionSubstate = MSS_ACTIVE_ONE
			ENDIF
		BREAK
		
		CASE MSS_ACTIVE_ONE
			//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				/*SET_BLIP_DISPLAY(biObjective, DISPLAY_BLIP)
				SET_BLIP_COLOUR(biObjective, BLIP_COLOUR_YELLOW)*/
				IF bShowHomeObjective
					IF GET_GAME_TIMER() > iConversationTimer	// Make sure the lost cops conversation has had time to start
						/*PRINT_NOW("MEA1_04", DEFAULT_GOD_TEXT_TIME, 1)	// Go ~y~home.
						CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)*/
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar)
							AND IS_PED_IN_VEHICLE(piAmanda, viCopCar)
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(pedConvStruct, "MEA1AUD", "MEA1_LOSTCOP", "MEA1_LOSTCOP_5", CONV_PRIORITY_MEDIUM)
									bShowHomeObjective = FALSE
									bShowReturnObjective = TRUE
								ENDIF
							ELSE
								bShowHomeObjective = FALSE
								bShowReturnObjective = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iCheckVehicleTimer = GET_GAME_TIMER() + iCheckVehicle
					/*IF GetMichaelScheduleStage() = MSS_M4_WithoutFamily
						biObjective = CREATE_COORD_BLIP(vHotelLocation)
					ELSE
						biObjective = CREATE_COORD_BLIP(vHomeLocation)
					ENDIF*/
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID()) // Clear now - ped damage events can occur before you start checking for them B*1178888
					missionSubstate = MSS_ACTIVE_TWO
				ENDIF
			//ENDIF
		BREAK
		
		CASE MSS_ACTIVE_TWO
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
				TriggerHandcuffSound()
				ManageDriveConversations()
				ManageCrashLines()
//				ManageCrimeLines()
				ManageStationaryLines()
				ManageSirenLines()
				ManageNotAtHomeLines()
				GetCurrentPlayersCar()
				PlayUpsideDownLine()
				IF NOT IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
					IF IS_VEHICLE_OK(viCopCar)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar)
							AmandaReleased()
						ENDIF
					ENDIF
				ENDIF
				CheckAtDestination()
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
					missionSubstate = MSS_PREPARE
					missionState = MS_LOSE_COPS
					CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID()) // Clear now - ped damage events can occur before you start checking for them B*1178888
				ENDIF
				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) > fWarnDistance
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					missionSubstate = MSS_PREPARE
					missionState = MS_RETURN_TO_AMANDA
				/*ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX viPlayer
					viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF NOT IS_PED_IN_VEHICLE(piAmanda, viPlayer, TRUE) AND NOT ARE_ANY_VEHICLE_SEATS_FREE(viPlayer)
					//IF NOT IS_PED_IN_VEHICLE(piAmanda, viPlayer, TRUE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						missionSubstate = MSS_PREPARE
						missionState = MS_RETURN_TO_AMANDA
					ENDIF*/
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	CheckForFreeSeats()
	ManagePlayerDamagesCopCarLines()
	ReleaseShopkeeper()
ENDPROC

/// PURPOSE:
///    The player has gone too far away from Amanda and must return to her
PROC RETURN_TO_AMANDA()
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	HANDLE_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT(stateRestoreConversation, pedConvStruct, "MEA1AUD", tSavedConversationRoot, tSavedConversationLabel)
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CPRINTLN(DEBUG_MISSION,"Entering return to amanda state")
			SAFE_REMOVE_BLIP(biObjective)
			IF IS_PED_UNINJURED(piAmanda)
				IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
					biObjective = CREATE_PED_BLIP(piAmanda, TRUE, TRUE)
					IF bShowReturnObjective
						PRINT_NOW("MEA1_05", DEFAULT_GOD_TEXT_TIME, 1)	// Return to ~b~Amanda.
						CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
						bShowReturnObjective = FALSE
					ENDIF
				ELSE
					biObjective = CREATE_VEHICLE_BLIP(viCopCar)
					IF bShowReturnObjective
						PRINT_NOW("MEA1_06", DEFAULT_GOD_TEXT_TIME, 1)	// Get back in the ~b~cop car.
						CHECK_CONVERSATION_AND_OBJECTIVE_TEXT_CONFLICT_NOW(stateRestoreConversation)
						bShowReturnObjective = FALSE
					ENDIF
				ENDIF
			ENDIF
			iAbandonLineTimer = GET_GAME_TIMER() + 1000
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
			IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
				ForceAmandaIntoGroupIfInterruptedSyncScene()
				IF IS_PED_GROUP_MEMBER(piAmanda, PLAYER_GROUP_ID())
					/*IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viPlayer
						viPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_PED_IN_VEHICLE(piAmanda, viPlayer)
							BackWithAmanda()
						ENDIF
					EL*/IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) < fReturnDistance
						BackWithAmanda()
					ENDIF
				ELSE
					IF (IS_VEHICLE_OK(viCopCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar) AND IS_PED_IN_VEHICLE(piAmanda, viCopCar))
					OR (NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viCopCar) AND NOT IS_PED_IN_VEHICLE(piAmanda, viCopCar))
						BackWithAmanda()
					ENDIF
				ENDIF
				ManageAbandonLines()
				
				IF (GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), piAmanda) > fFailDistance)
					SetFailReason(MFR_AMANDA_LEFT)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	ManagePlayerDamagesCopCarLines()
ENDPROC

/// PURPOSE:
///    The player arrives at Michael's house with Amanda
PROC AT_HOME()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
		SET_PED_RESET_FLAG(piAmanda, PRF_SearchForClosestDoor, TRUE )
		VEHICLE_INDEX viTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SWITCH missionSubstate
			CASE MSS_PREPARE
				CPRINTLN(DEBUG_MISSION,"Entering at home state")
				KILL_FACE_TO_FACE_CONVERSATION()
				KILL_PHONE_CONVERSATION()
				IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_DRIVE_HOME")
					STOP_AUDIO_SCENE("M_E_AMANDA_DRIVE_HOME")
				ENDIF
				VEHICLE_INDEX viBang
				viBang = GET_PLAYERS_LAST_VEHICLE()
				IF IS_ENTITY_ALIVE(viBang)
				AND IS_ENTITY_ON_FIRE(viBang)
					CPRINTLN(DEBUG_MISSION,"Entering at home state: Got player's last vehicle, removing fire")
					IF GET_ENTITY_HEALTH(viBang) < 1
						SET_ENTITY_HEALTH(viBang, 1)
					ENDIF
					IF GET_VEHICLE_ENGINE_HEALTH(viBang) < 1
						SET_VEHICLE_ENGINE_HEALTH(viBang, 1)
					ENDIF
					IF GET_VEHICLE_PETROL_TANK_HEALTH(viBang) < 1
						SET_VEHICLE_PETROL_TANK_HEALTH(viBang, 1)
					ENDIF
					STOP_ENTITY_FIRE(viBang)
					STOP_FIRE_IN_RANGE(GET_ENTITY_COORDS(viBang), 2.5)
					REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(viBang), 2.5)
					if not IS_ENTITY_A_MISSION_ENTITY(viBang)
						SET_ENTITY_AS_MISSION_ENTITY(viBang)
					ENDIF
					SET_DISABLE_VEHICLE_ENGINE_FIRES(viBang, TRUE)
					SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(viBang, TRUE)
					SAFE_RELEASE_VEHICLE(viBang)
				ENDIF
				SAFE_REMOVE_BLIP(biObjective)
				iConvFailedTimer = -1
				iWalkToHouseFailed = -1
                RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE)
                //iCutTimer = GET_GAME_TIMER() + 5000
                ciHomeCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -840.7, 185.3, 78.4 >>, << -14.0, 0.0, -110.4 >>, 48.4)
                SET_CAM_ACTIVE(ciHomeCam, TRUE)
                RENDER_SCRIPT_CAMS(TRUE, FALSE)
				IF IS_VEHICLE_OK(viTemp)
					IF IS_VEHICLE_DOOR_DAMAGED(viTemp, SC_DOOR_FRONT_RIGHT)
						SET_PED_CONFIG_FLAG(piAmanda, PCF_PedIgnoresAnimInterruptEvents, TRUE)
					ENDIF
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
					MODEL_NAMES mnTemp
					mnTemp = GET_ENTITY_MODEL(viTemp)
					IF IS_THIS_MODEL_A_CAR(mnTemp) AND IS_MODEL_POLICE_VEHICLE(mnTemp)
						missionSubstate = MSS_ACTIVE_ONE
					ELSE
						missionSubstate = MSS_ACTIVE_TWO
					ENDIF
					//bArrivedInVehicle = TRUE
				ELSE
					//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					/*SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vOnFootEndPosMichael)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fOnFootEndRotMichael)
					CLEAR_PED_TASKS(piAmanda)
					SET_ENTITY_COORDS_GROUNDED(piAmanda, vOnFootEndPosAmanda)
					SET_ENTITY_HEADING(piAmanda, fOnFootEndRotAmanda)*/
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(fOnFootEndCamHeading)
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(fOnFootEndCamPitch)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(vCutsceneAreaPos1, vCutsceneAreaPos2, fCutsceneAreaWidth, vCarMovePos, fCarMoveHead, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					CLEAR_PED_TASKS_IMMEDIATELY(piAmanda)
					SET_ENTITY_COORDS_GROUNDED(piAmanda, GET_ENTITY_COORDS(PLAYER_PED_ID())+<<2,2,0>>)
					SET_ENTITY_HEADING(piAmanda, -155.84)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), -43.04)
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piAmanda)
					//TASK_TURN_PED_TO_FACE_ENTITY(piAmanda, PLAYER_PED_ID())
					SEQUENCE_INDEX siAmanda
					OPEN_SEQUENCE_TASK(siAmanda)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_RUN)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vKitchen, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(siAmanda)
					TASK_PERFORM_SEQUENCE(piAmanda, siAmanda)
					CLEAR_SEQUENCE_TASK(siAmanda)
					STOP_FIRE_IN_RANGE(vCarMovePos, 2.5)
					REMOVE_PARTICLE_FX_IN_RANGE(vCarMovePos, 2.5)
					missionSubstate = MSS_ACTIVE_TWO
					//bArrivedInVehicle = FALSE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_ONE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HOME", CONV_PRIORITY_MEDIUM)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
					//OR GET_GAME_TIMER() > iConvFailedTimer
						missionSubstate = MSS_ACTIVE_THREE
					ENDIF
				//ENDIF
			BREAK
			
			CASE MSS_ACTIVE_TWO
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HOME2", CONV_PRIORITY_MEDIUM)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
					//OR GET_GAME_TIMER() > iConvFailedTimer
						missionSubstate = MSS_ACTIVE_THREE
					ENDIF
				//ENDIF
			BREAK
			
			CASE MSS_ACTIVE_THREE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(piAmanda) AND IS_VEHICLE_OK(viTemp)
					IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_PERFORM_SEQUENCE)
						IF IS_VEHICLE_STOPPED(viTemp)
							//TASK_LEAVE_ANY_VEHICLE(piAmanda)
							SEQUENCE_INDEX siLeave
							OPEN_SEQUENCE_TASK(siLeave)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vKitchen, PEDMOVE_WALK)
							CLOSE_SEQUENCE_TASK(siLeave)
							TASK_PERFORM_SEQUENCE(piAmanda, siLeave)
							CLEAR_SEQUENCE_TASK(siLeave)
						ENDIF
					ENDIF
				ELSE
					/*IF bArrivedInVehicle
						IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(piAmanda, vAmandaFinalDest, PEDMOVE_WALK)
						ENDIF
					ENDIF*/
					
					IF IS_ENTITY_IN_RANGE_COORDS_2D(piAmanda, vAmandaFinalDest, 1.5)//3.0)
					OR IS_ENTITY_IN_RANGE_COORDS_2D(piAmanda, vAmandaFinalDestRear, 1.5)//3.0)
					OR CONVERSATION_TIMED_OUT(iWalkToHouseFailed, 10000)
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							iWalkToHouseFailed = -1
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							missionSubstate = MSS_ACTIVE_FOUR
						ELSE
							CLEAR_PED_TASKS(PLAYER_PED_ID())
			                RENDER_SCRIPT_CAMS(FALSE, FALSE)
			                RC_END_CUTSCENE_MODE()
			                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			                DESTROY_CAM(ciHomeCam)
							Mission_Passed()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_FOUR
				IF CONVERSATION_TIMED_OUT(iWalkToHouseFailed, 300)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
	                RENDER_SCRIPT_CAMS(FALSE, FALSE)
	                RC_END_CUTSCENE_MODE()
	                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	                DESTROY_CAM(ciHomeCam)
					Mission_Passed()
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC AT_HOTEL()
	IF IS_PED_UNINJURED(PLAYER_PED_ID()) AND IS_PED_UNINJURED(piAmanda)
		SET_PED_RESET_FLAG(piAmanda, PRF_SearchForClosestDoor, TRUE )
		VEHICLE_INDEX viTemp
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			viTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		SWITCH missionSubstate
			CASE MSS_PREPARE
				CPRINTLN(DEBUG_MISSION,"Entering at home state")
				KILL_FACE_TO_FACE_CONVERSATION()
				KILL_PHONE_CONVERSATION()
				IF IS_AUDIO_SCENE_ACTIVE("M_E_AMANDA_DRIVE_HOME")
					STOP_AUDIO_SCENE("M_E_AMANDA_DRIVE_HOME")
				ENDIF
				SAFE_REMOVE_BLIP(biObjective)
				iConvFailedTimer = -1
				iWalkToHouseFailed = -1
                RC_START_CUTSCENE_MODE(<<0.0,0.0,0.0>>, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, FALSE)
                //iCutTimer = GET_GAME_TIMER() + 5000
                ciHomeCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-1392.2539, 367.9411, 75.0046>>, <<-44.2648, -0.0000, -119.8730>>, 48.4)
                SET_CAM_ACTIVE(ciHomeCam, TRUE)
                RENDER_SCRIPT_CAMS(TRUE, FALSE)
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
					MODEL_NAMES mnTemp
					mnTemp = GET_ENTITY_MODEL(viTemp)
					IF IS_THIS_MODEL_A_CAR(mnTemp) AND IS_MODEL_POLICE_VEHICLE(mnTemp)
						missionSubstate = MSS_ACTIVE_ONE
					ELSE
						missionSubstate = MSS_ACTIVE_TWO
					ENDIF
					//bArrivedInVehicle = TRUE
				ELSE
					//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					/*SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vOnFootEndPosMichael)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), fOnFootEndRotMichael)
					CLEAR_PED_TASKS(piAmanda)
					SET_ENTITY_COORDS_GROUNDED(piAmanda, vOnFootEndPosAmanda)
					SET_ENTITY_HEADING(piAmanda, fOnFootEndRotAmanda)*/
					//SET_GAMEPLAY_CAM_RELATIVE_HEADING(fOnFootEndCamHeading)
					//SET_GAMEPLAY_CAM_RELATIVE_PITCH(fOnFootEndCamPitch)
					TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), piAmanda)
					//TASK_TURN_PED_TO_FACE_ENTITY(piAmanda, PLAYER_PED_ID())
					SEQUENCE_INDEX siAmanda
					OPEN_SEQUENCE_TASK(siAmanda)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVE_RUN)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 3000)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHotel, PEDMOVE_WALK)
					CLOSE_SEQUENCE_TASK(siAmanda)
					TASK_PERFORM_SEQUENCE(piAmanda, siAmanda)
					CLEAR_SEQUENCE_TASK(siAmanda)
					missionSubstate = MSS_ACTIVE_TWO
					//bArrivedInVehicle = FALSE
				ENDIF
			BREAK
			
			CASE MSS_ACTIVE_ONE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HOME", CONV_PRIORITY_MEDIUM)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
					//OR GET_GAME_TIMER() > iConvFailedTimer
						missionSubstate = MSS_ACTIVE_THREE
					ENDIF
				//ENDIF
			BREAK
			
			CASE MSS_ACTIVE_TWO
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					/*IF iConvFailedTimer = -1
						iConvFailedTimer = GET_GAME_TIMER() + CONV_FAILED_TIME
					ENDIF*/
					ADD_PED_FOR_DIALOGUE(pedConvStruct, AMANDA_ID, piAmanda, "AMANDA", TRUE)
					IF CREATE_CONVERSATION(pedConvStruct, "MEA1AUD", "MEA1_HOME2", CONV_PRIORITY_MEDIUM)
					OR CONVERSATION_TIMED_OUT(iConvFailedTimer)
					//OR GET_GAME_TIMER() > iConvFailedTimer
						missionSubstate = MSS_ACTIVE_THREE
					ENDIF
				//ENDIF
			BREAK
			
			CASE MSS_ACTIVE_THREE
				IF IS_VEHICLE_OK(viTemp)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(viTemp)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(piAmanda) AND IS_VEHICLE_OK(viTemp)
					IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
						IF IS_VEHICLE_STOPPED(viTemp)
							//TASK_LEAVE_ANY_VEHICLE(piAmanda)
							SEQUENCE_INDEX siLeave
							OPEN_SEQUENCE_TASK(siLeave)
								TASK_LEAVE_ANY_VEHICLE(NULL)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 2000)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vHotel, PEDMOVE_WALK)
							CLOSE_SEQUENCE_TASK(siLeave)
							TASK_PERFORM_SEQUENCE(piAmanda, siLeave)
							CLEAR_SEQUENCE_TASK(siLeave)
						ENDIF
					ENDIF
				ELSE
					/*IF bArrivedInVehicle
						IF NOT IsPedPerformingTask(piAmanda, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(piAmanda, vAmandaFinalDest, PEDMOVE_WALK)
						ENDIF
					ENDIF*/
					
					IF /*NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND*/ IS_ENTITY_IN_RANGE_COORDS_2D(piAmanda, vHotel, 1.5)//3.0)
					OR CONVERSATION_TIMED_OUT(iWalkToHouseFailed, 10000)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
		                RENDER_SCRIPT_CAMS(FALSE, FALSE)
		                RC_END_CUTSCENE_MODE()
		                SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		                DESTROY_CAM(ciHomeCam)
						Mission_Passed()
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Fades out and fails the mission with a delay if needed
PROC FAIL_DELAY()
	SWITCH missionSubstate
		CASE MSS_PREPARE
			CLEAR_PRINTS()
			SAFE_REMOVE_BLIP(biObjective)
			
			IF DOES_ENTITY_EXIST(piAmanda)
				IF IS_PED_INJURED(piAmanda)
					missionFailedReason = MFR_AMANDA_DEAD
				ELSE
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(piAmanda, PLAYER_PED_ID())
						missionFailedReason = MFR_AMANDA_HURT
					ENDIF
				ENDIF
			ENDIF
			
			STRING sFailReason
	
			SWITCH missionFailedReason
				CASE MFR_NONE
					// no fail reason
				BREAK
				
				CASE MFR_AMANDA_HURT
					sFailReason = "MEA1_FAIL4"	// You injured Amanda.
				BREAK
				
				CASE MFR_AMANDA_DEAD
					sFailReason = "MEA1_FAIL1"	// Amanda died.
				BREAK
				
				CASE MFR_AMANDA_ARRESTED
					sFailReason = "MEA1_FAIL2"	// Amanda was arrested.
				BREAK
				
				CASE MFR_AMANDA_LEFT
					sFailReason = "MEA1_FAIL3"	// You abandoned Amanda.
				BREAK
				
				CASE MFR_TRASHED_CAR
					sFailReason = "MEA1_FAIL5"	// ~r~You trashed the cop car.
				BREAK
			ENDSWITCH
		

			IF missionFailedReason = MFR_NONE
				Mission_Flow_Mission_Failed()
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)
			ENDIF
			
			missionSubstate = MSS_ACTIVE_ONE
		BREAK
		
		CASE MSS_ACTIVE_ONE
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
			
				// must only take 1 frame and terminate the thread 
				CPRINTLN(DEBUG_MISSION,"...ME_Amanda1 Failed")
				RemovePedsAndVehicles(TRUE)
				Mission_Cleanup(TRUE) 
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================
SCRIPT
	CPRINTLN(DEBUG_MISSION,"ME Amanda 1 launched")
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		CPRINTLN(DEBUG_MISSION,"...ME_Amanda1 Force Cleanup")
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup(TRUE)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	WHILE (TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TGH")
		IF IS_PED_UNINJURED(piAmanda)
			SET_PED_RESET_FLAG(piAmanda, PRF_CannotBeTargetedByAI, TRUE)
		ENDIF
		AmandasBagManager()
		SWITCH missionState
			CASE MS_INIT
				INIT()
			BREAK
			CASE MS_DRIVE_TO_SHOP
				DRIVE_TO_SHOP()
			BREAK
			
			CASE MS_RESCUE_AMANDA
				RESCUE_AMANDA()
			BREAK
			
			CASE MS_LOSE_COPS
				LOSE_COPS()
			BREAK
			
			CASE MS_DRIVE_HOME
				DRIVE_HOME()
			BREAK
			
			CASE MS_RETURN_TO_AMANDA
				RETURN_TO_AMANDA()
			BREAK
			
			CASE MS_AT_HOME
				AT_HOME()
			BREAK
			
			CASE MS_AT_HOTEL
				AT_HOTEL()
			BREAK
			
			CASE MS_FAIL_DELAY
				FAIL_DELAY()
			BREAK
		ENDSWITCH

		CheckAmanda()
		
		#IF IS_DEBUG_BUILD
			IF missionState <> MS_FAIL_DELAY
				// Check for Pass
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_PRINTS()
					DebugPassDealWithAmanda()
					Mission_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					IF missionState = MS_AT_HOME
						RC_END_CUTSCENE_MODE()
					ENDIF
					SetFailReason(MFR_NONE)
				ENDIF
				
				CheckDebugKeys()
			ENDIF
		#ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
