// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Random Event - Duel
//		DATE			: 	24/07/14
//		AUTHOR			:	Harry Turner
//		DESCRIPTION		:	'Duel' random event. Player steals a car and is then chased.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//================================================================================================================================================
//		INCLUDES
//================================================================================================================================================
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch"
USING "ambient_common.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "cheat_controller_public.sch"
USING "clearMissionArea.sch"
USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch"
USING "distributed_los.sch"
USING "flow_help_public.sch"
USING "flow_public_core_override.sch"      
USING "random_events_public.sch"
USING "rc_helper_functions.sch"
USING "script_blips.sch"
USING "script_buttons.sch"
USING "script_ped.sch"
USING "selector_public.sch"
USING "select_mission_stage.sch"   
USING "social_public.sch"

//================================================================================================================================================
//		ENUMS
//================================================================================================================================================
ENUM MISSION_STAGE_FLAGS
	MSF_STEAL_CAR,
	MSF_MISSION_PASSED	// Passed
ENDENUM

ENUM MISSION_STAGE_SECTIONS
	MSS_SETUP,			// Stage is being setup
	MSS_ACTIVE,			// Stage is running
	MSS_CLEANUP		// Stage is being cleaned up
ENDENUM

ENUM MISSION_BIT_FLAGS
	MBF_SEQUENCE_OPEN,		// Used for sequence tasks
	MBF_CAR_STOLEN,
	MBF_CAR_CHASE,
	MBF_LEFT_SIDE,
	MBF_TRUCKS_AVOIDED,
	MBF_DO_HORN,
	MBF_WARP_TRUCKS,	// url:bugstar:2006142 - Have the trucks warp behind you if you outrun them
	MBF_WARP_SUVS,
	MBF_TRUCKS_TRIGGERED,
	MBF_SPAWN_TRUCKS,
	MBF_TRUCK_1_SPAWNED,
	MBF_TRUCK_2_SPAWNED,
	MBF_TRUCK_PASSED_WARP,	// Prevent truck from being warped backwards
	MBF_MISSION_SETUP
ENDENUM

ENUM MISSION_PED_FLAGS
	MPF_TRUCKER_1,
	MPF_TRUCKER_2,
	MPF_HILLBILLY_1,
	MPF_HILLBILLY_2,
	MPF_HILLBILLY_3,
	MPF_HILLBILLY_4,
	MPF_HILLBILLY_5,
	MPF_HILLBILLY_6
ENDENUM

ENUM MISSION_PED_BIT_FLAGS
	MPBF_VEH_BLIP,			// Peds blip has been set to vehicle size
	MPBF_BLOCK_RAGDOLL,
	MPBF_DONT_CLEANUP,
	MPBF_WARPED
ENDENUM

ENUM MISSION_VEHICLE_FLAGS
	MVF_DUEL,
	MVF_TRAILER_1,
	MVF_TRAILER_2,
	MVF_TRUCK_1,
	MVF_TRUCK_2,
	MVF_SUV_1,
	MVF_SUV_2,
	MVF_SUV_3
ENDENUM

//================================================================================================================================================
//		STRUCTS
//================================================================================================================================================
STRUCT MISSION_PED_STRUCT
	PED_INDEX			ped
	BLIP_INDEX			blip
	INT					iBitFlags
	INT					iEvent
	INT					iTimer
ENDSTRUCT

STRUCT MISSION_VEHICLE_STRUCT
	VEHICLE_INDEX	veh
	BLIP_INDEX		blip
	INT				iTimer
	INT				iEvent
ENDSTRUCT

//================================================================================================================================================
//		MISSION VARS
//================================================================================================================================================
MISSION_STAGE_FLAGS 	e_MissionStage 		= MSF_STEAL_CAR		// Current stage
MISSION_STAGE_SECTIONS	e_StageSection		= MSS_SETUP			// Current section of the stage we're in

//================================================================================================================================================
//		PEDS
//================================================================================================================================================
MISSION_PED_STRUCT	a_Peds[COUNT_OF(MISSION_PED_FLAGS)]

//================================================================================================================================================
//		VEHICLES
//================================================================================================================================================
MISSION_VEHICLE_STRUCT	a_Vehicles[COUNT_OF(MISSION_VEHICLE_FLAGS)]

//================================================================================================================================================
//		OBJECTS
//================================================================================================================================================


//================================================================================================================================================
//		BLIPS
//================================================================================================================================================


//================================================================================================================================================
//		STRINGS
//================================================================================================================================================


//===============================================
//			ANIMATIONS
//===============================================


//===============================================
//			VEHICLE RECORDINGS
//===============================================


//===============================================
//			WAYPOINT RECORDINGS
//===============================================


//================================================================================================================================================
//		BOOLS
//================================================================================================================================================


//================================================================================================================================================
//		VECTORS
//================================================================================================================================================
VECTOR	V_ZERO		=	<<0,0,0>>
VECTOR	v_Input
VECTOR	v_CarStart	=	<<1064.8711, 2670.8572, 38.5511>>

VECTOR	v_Truck1Spawn	=	<<926.3422, 2708.9260, 39.5394>>
VECTOR	v_Truck2Spawn	=	<<1140.030, 2698.027, 37.1568>>

VECTOR	v_SecondTruckWarp	=	 V_ZERO

//================================================================================================================================================
//		CAR CHASE VARIABLES
//================================================================================================================================================
VECTOR 			v_LastPlayerPos
FLOAT			f_LastPlayerHeading
VECTOR 			v_NextCarWarpPos
FLOAT			f_NextCarWarpHeading
INT				i_GlobalWarpTimer
INT				i_Car1WarpTimer
INT				i_Car2WarpTimer
INT				i_Car3WarpTimer
INT				i_Truck1WarpTimer
INT				i_Truck2WarpTimer

//================================================================================================================================================
//		INTEGERS
//================================================================================================================================================
INT			i_CurrentEvent		=	0
INT 		i_MissionBitFlags	=	0
INT			i_HornTime			=	0
INT			i_WarpTruckTimer	=	0	// When expired, trucks will no longer be warped
INT			i_WarpSUVTimer		=	0	// When expired, SUVs will no longer be warped
INT			i_TruckTriggerTimer	=	0
INT			i_RamTimer			=	0
INT			i_HiddenTimerA		=	0
INT			i_HiddenTimerB		=	0

//================================================================================================================================================
//		FLOATS
//================================================================================================================================================
FLOAT		f_LastVehSpeed		=	0


//================================================================================================================================================
//		MODELS
//================================================================================================================================================
MODEL_NAMES	e_ModelDuel			=	INT_TO_ENUM(MODEL_NAMES, HASH("DUKES2"))
MODEL_NAMES	e_ModelTruck		=	PHANTOM
MODEL_NAMES	e_ModelTrailer		=	TRAILERS2
MODEL_NAMES	e_ModelSUV			=	RANCHERXL
MODEL_NAMES	e_ModelHillBilly1	=	A_M_M_HILLBILLY_01
MODEL_NAMES	e_ModelHillbilly2	=	A_M_M_HILLBILLY_02

//================================================================================================================================================
//		MISC. VARIABLES
//================================================================================================================================================
REL_GROUP_HASH				relGroupEnemies
//structPedsForConversation	s_Convo
SEQUENCE_INDEX				seqMain
//MISSION_VEHICLE_FLAGS		e_TruckSpawn	=	MVF_TRUCK_1
MISSION_VEHICLE_FLAGS		e_HornTruck		= 	MVF_TRUCK_1

//================================================================================================================================================
//		DEBUG VARIABLES
//================================================================================================================================================
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			widGroup
#ENDIF

//================================================================================================================================================
//		FUNCTIONS/PROCEDURES
//================================================================================================================================================

//===============================================
//	Reset mission variables
//===============================================
PROC RESET_VARIABLES()
	e_MissionStage = MSF_STEAL_CAR
	e_StageSection = MSS_SETUP
	i_CurrentEvent = 0
	i_MissionBitFlags = 0
ENDPROC

//===============================================
//	Remove all mission blips
//===============================================
PROC REMOVE_ALL_BLIPS()
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		SAFE_REMOVE_BLIP(a_Peds[i].blip)
	ENDREPEAT

	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		SAFE_REMOVE_BLIP(a_Vehicles[i].blip)
	ENDREPEAT
ENDPROC

//===============================================
//	Remove all anim dicts
//===============================================
PROC REMOVE_ALL_ANIM_DICTS()

ENDPROC

//===============================================
//	Remove all waypoint recordings
//===============================================
PROC REMOVE_ALL_WAYPOINTS()

ENDPROC

//===============================================
//	Remove all vehicle recordings
//===============================================
PROC REMOVE_ALL_VEH_RECS()

ENDPROC

//===============================================
//	Set mission bit flag
//===============================================
PROC SET_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag )
	SET_BIT( i_MissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Clear mission bit flag
//===============================================
PROC CLEAR_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag )
	CLEAR_BIT( i_MissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Checks if a mission bit flag has been set
//===============================================
FUNC BOOL IS_MISSION_BIT_FLAG_SET( MISSION_BIT_FLAGS eFlag )
	RETURN IS_BIT_SET( i_MissionBitFlags, ENUM_TO_INT( eFlag ) )
ENDFUNC

//===============================================
//	Custom conversation func
//===============================================
//FUNC BOOL START_CONVERSATION( STRING strRoot )
//	RETURN CREATE_CONVERSATION( s_Convo, "TEMPAU", strRoot, CONV_PRIORITY_HIGH )
//ENDFUNC

//===============================================
//	Cleanup a mission ped
//===============================================
PROC CLEANUP_PED( MISSION_PED_FLAGS ePed, BOOL bForce )
	IF DOES_ENTITY_EXIST( a_Peds[ePed].ped )
		SAFE_REMOVE_BLIP( a_Peds[ePed].blip )
		
		IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		AND IS_ENTITY_ATTACHED( a_Peds[ePed].ped )
		AND NOT IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped, TRUE )
		AND NOT IS_PED_SITTING_IN_ANY_VEHICLE( a_Peds[ePed].ped )
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE( a_Peds[ePed].ped )
			DETACH_ENTITY( a_Peds[ePed].ped )
		ENDIF
		
		IF bForce
			DELETE_PED( a_Peds[ePed].ped )
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED( a_Peds[ePed].ped )
		ENDIF
		
		a_Peds[ePed].iBitFlags = 0
	ENDIF
ENDPROC

//===============================================
//	Cleanup all mission peds
//===============================================
PROC CLEANUP_PEDS( BOOL bForce )
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		CLEANUP_PED( INT_TO_ENUM( MISSION_PED_FLAGS, i ), bForce )
	ENDREPEAT
ENDPROC

//===============================================
//	Check if it's safe to cleanup a specific vehicle
//===============================================
FUNC BOOL IS_SAFE_TO_CLEANUP_VEHICLE( VEHICLE_INDEX& vehIndex )
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

//===============================================
//	Cleanup mission vehicle
//===============================================
PROC CLEANUP_VEHICLE( MISSION_VEHICLE_FLAGS eVeh, BOOL bForce )
	IF DOES_ENTITY_EXIST( a_Vehicles[eVeh].veh )
		SAFE_REMOVE_BLIP( a_Vehicles[eVeh].blip )
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE( a_Vehicles[eVeh].veh )
			STOP_PLAYBACK_RECORDED_VEHICLE( a_Vehicles[eVeh].veh )
		ENDIF
		
		IF IS_SAFE_TO_CLEANUP_VEHICLE( a_Vehicles[eVeh].veh )
			IF bForce
				DELETE_VEHICLE( a_Vehicles[eVeh].veh )
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED( a_Vehicles[eVeh].veh )
			ENDIF
		ENDIF
		a_Vehicles[eVeh].veh = NULL
		a_Vehicles[eVeh].iTimer = 0
		a_Vehicles[eVeh].iEvent = 0
	ENDIF
ENDPROC

//===============================================
//	Cleanup all mission vehicles
//===============================================
PROC CLEANUP_VEHICLES( BOOL bForce )
	INT i
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		CLEANUP_VEHICLE( INT_TO_ENUM( MISSION_VEHICLE_FLAGS, i ), bForce )
	ENDREPEAT
ENDPROC

//===============================================
//	Custom function for opening a sequence
//===============================================
PROC SAFE_OPEN_SEQUENCE()
	IF NOT IS_MISSION_BIT_FLAG_SET(MBF_SEQUENCE_OPEN)
		CLEAR_SEQUENCE_TASK( seqMain )
		OPEN_SEQUENCE_TASK( seqMain )
		SET_MISSION_BIT_FLAG(MBF_SEQUENCE_OPEN)
	ENDIF
ENDPROC

//===============================================
//	Custom function for using a sequence
//===============================================
PROC SAFE_PERFORM_SEQUENCE_TASK( PED_INDEX ped )
	IF IS_MISSION_BIT_FLAG_SET(MBF_SEQUENCE_OPEN)
		CLOSE_SEQUENCE_TASK( seqMain )
		CLEAR_MISSION_BIT_FLAG(MBF_SEQUENCE_OPEN)
	ENDIF
	TASK_PERFORM_SEQUENCE( ped, seqMain )
	CLEAR_SEQUENCE_TASK( seqMain )
ENDPROC

//===============================================
//	Set ped bit flag
//===============================================
PROC SET_PED_BIT_FLAG( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	SET_BIT( a_Peds[ePed].iBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Clear ped bit flag
//===============================================
PROC CLEAR_PED_BIT_FLAG( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	CLEAR_BIT( a_Peds[ePed].iBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Checks if a ped bit flag has been set
//===============================================
FUNC BOOL IS_PED_BIT_FLAG_SET( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	RETURN IS_BIT_SET( a_Peds[ePed].iBitFlags, ENUM_TO_INT( eFlag ) )
ENDFUNC

//===============================================
//	Show blip if driver dead.
//===============================================
FUNC BOOL SHOULD_HIDE_BLIP_IN_VEHICLE( MISSION_PED_FLAGS ePed )
	IF ePed = MPF_HILLBILLY_2
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_1].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_HILLBILLY_4
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_3].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_HILLBILLY_6
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_5].ped )
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

//===============================================
//	Manages blip size of peds. Removes blips
//	when they die.
//===============================================
PROC UPDATE_ALL_BLIPS()
		INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		IF DOES_ENTITY_EXIST(a_Peds[i].ped)
		AND DOES_BLIP_EXIST(a_Peds[i].blip)	
			IF IS_PED_IN_ANY_VEHICLE(a_Peds[i].ped)
				IF NOT IS_PED_BIT_FLAG_SET( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
					SET_BLIP_SCALE( a_Peds[i].blip, BLIP_SIZE_VEHICLE )
					SET_PED_BIT_FLAG( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				ENDIF
				
				IF GET_SEAT_PED_IS_IN( a_Peds[i].ped ) != VS_DRIVER
					IF SHOULD_HIDE_BLIP_IN_VEHICLE( INT_TO_ENUM(MISSION_PED_FLAGS, i) )
						IF GET_BLIP_ALPHA( a_Peds[i].blip ) > 0
							SET_BLIP_ALPHA( a_Peds[i].blip, 0 )
							SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, TRUE )
						ENDIF
					ELSE
						IF GET_BLIP_ALPHA( a_Peds[i].blip ) = 0
							SET_BLIP_ALPHA( a_Peds[i].blip, 255 )
							SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, FALSE )
						ENDIF
					ENDIF
				ENDIF

			ELIF IS_PED_BIT_FLAG_SET( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				SET_BLIP_SCALE( a_Peds[i].blip, BLIP_SIZE_PED )
				CLEAR_PED_BIT_FLAG( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				
				IF GET_BLIP_ALPHA( a_Peds[i].blip ) = 0
					SET_BLIP_ALPHA( a_Peds[i].blip, 255 )
					SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, FALSE )
				ENDIF
			ENDIF
			
			// Remove blips for any injured peds automatically.
			IF IS_PED_INJURED(a_Peds[i].ped)
				SAFE_REMOVE_BLIP(a_Peds[i].blip)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		IF DOES_ENTITY_EXIST(a_Vehicles[i].veh)
		AND DOES_BLIP_EXIST(a_Vehicles[i].blip)	
			// Remove blips for any destroyed cars
			IF NOT IS_VEHICLE_DRIVEABLE(a_Vehicles[i].veh)
				SAFE_REMOVE_BLIP(a_Vehicles[i].blip)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//===============================================
//	Configure specific vehicle colours
//===============================================
PROC SET_MISSION_VEHICLE_COLOURS( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		SWITCH eVeh
			CASE MVF_SUV_1
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 48, 48 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 8, 156 )
			BREAK
			CASE MVF_SUV_2
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 58, 58 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 8, 156 )
			BREAK
			CASE MVF_SUV_3
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 94, 94 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 8, 156 )
			BREAK
			CASE MVF_TRUCK_1
			CASE MVF_TRUCK_2
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 0, 0 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 61, 156 )
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

//===============================================
//	Configure specific vehicle mods
//===============================================
PROC SET_MISSION_VEHICLE_MODS( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		SWITCH eVeh
			CASE MVF_SUV_1
			CASE MVF_SUV_2
			CASE MVF_SUV_3
			CASE MVF_TRUCK_1
			CASE MVF_TRUCK_2
				SET_VEHICLE_DIRT_LEVEL( a_Vehicles[eVeh].veh, 15 )
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

//===============================================
//	Configure specific ped variations
//===============================================
PROC SET_MISSION_PED_VARIATIONS( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			CASE MPF_TRUCKER_1		// 	A_M_M_HILLBILLY_01
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			BREAK
			CASE MPF_TRUCKER_2		//	A_M_M_HILLBILLY_01
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_1	//	A_M_M_HILLBILLY_02
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_2	//	A_M_M_HILLBILLY_01
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_3	//	A_M_M_HILLBILLY_01	
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 2, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_4	// 	A_M_M_HILLBILLY_02
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_5	//	A_M_M_HILLBILLY_02
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 2, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 2, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			BREAK
			CASE MPF_HILLBILLY_6	//	A_M_M_HILLBILLY_02
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//===============================================
//	Setup mission peds for dialogue
//===============================================
PROC ADD_MISSION_PED_FOR_DIALOGUE( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			CASE MPF_HILLBILLY_1
				
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//===============================================
//	Creates a mission ped
//===============================================
FUNC BOOL SPAWN_MISSION_PED( MISSION_PED_FLAGS ePed, BOOL bBlip, VECTOR vPosOverride, FLOAT fHeadingOverride = -1.0 )
	
	IF NOT DOES_ENTITY_EXIST( a_Peds[ePed].ped )
	
		// Values used to create & configure the peds
		VECTOR vPos
		FLOAT fHeading
		MODEL_NAMES eModel
		WEAPON_TYPE	eWeapon = WEAPONTYPE_UNARMED
		VEHICLE_INDEX startVeh = NULL
		VEHICLE_SEAT eSeat = VS_DRIVER
		ENTITY_INDEX attachEntity
		VECTOR vAttachPosOffset = V_ZERO
		VECTOR vAttachRotOffset = V_ZERO
		WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID
		COMBAT_ABILITY_LEVEL eCombatLevel = CAL_AVERAGE
		INT iHealth = 0
		INT iArmour = 0
		INT iAccuracy = 10
		FLOAT fMinBlanks = 0
		FLOAT fMaxBlanks = 0
		FLOAT fSeeingRange = 60.0
		FLOAT fPeripheralSeeingRange = 5.0
        FLOAT fCentreAngle = 120.0
		FLOAT fMinAngle = -90.0 
		FLOAT fMaxAngle = 90.0
		REL_GROUP_HASH relGroup = relGroupEnemies
		BOOL bCanListen = TRUE
		BOOL bGenerateSound = TRUE
		BOOL bBlockEvents = FALSE
		BOOL bKeepTask = TRUE
		BOOL bDiesWhenInjured = TRUE
		BOOL bInfluenceWantedLevel = TRUE
		BOOL bAutoEquipWeapon = FALSE
		BOOL bScanDeadBodies = TRUE
		BOOL bLoadCollision = FALSE
		BOOL bLeaveVehicle = TRUE
		BOOL bVisible = TRUE
		BOOL bReact = FALSE
		BOOL bNoOffset = FALSE
		BOOL bCollision = TRUE
		BOOL bInvincible = FALSE
		BOOL bDoDriveby = TRUE
		BOOL bUseVehicle = TRUE
		BOOL bVehicleAttack = FALSE
		BOOL bSufferCritical = TRUE
		BOOL bTargetable = TRUE
		BOOL bRunFromExplosions = TRUE
		BOOL bFallOutVehicle = FALSE
		BOOL bBlindFire = FALSE
		BOOL bDisablePinDownOthers = FALSE
		BOOL bDisablePinnedDown = FALSE
		BOOL bMoveBeforeCover = FALSE
		BOOL bKinematicWhenStill = FALSE
		BOOL bDisableHurt = TRUE
		BOOL bLoadCover = FALSE
		BOOL bCantJackPlayer = FALSE
		BOOL bStopSpeaking = FALSE
		BOOL bDropsWeapon = TRUE
		BOOL bCleanupOnDeath = TRUE
		BOOL bOverrideMaxHealth = FALSE
		BOOL bAlwaysFlee = FALSE
		BOOL bDisablePainAudio = FALSE
		BOOL bStealthMode = FALSE
		BOOL bCompletelyBlockCollision = FALSE
		BOOL bFreeze = FALSE
		BOOL bNotAvoided = FALSE
		BOOL bAggresive = TRUE
		BOOL bAutoShuffleToDriver = TRUE
		BOOL bPreventJackReact = FALSE
		STRING strScenario
		#IF IS_DEBUG_BUILD TEXT_LABEL strDebugName	#ENDIF
		
		// Configure each ped appropriately
		SWITCH ePed
			CASE MPF_TRUCKER_1
				#IF IS_DEBUG_BUILD	strDebugName = "TRUCKER 1"	#ENDIF
				eModel = e_ModelHillBilly1
				startVeh = a_Vehicles[MVF_TRUCK_1].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
				//bBlockEvents = TRUE
				bAutoEquipWeapon = TRUE
				bPreventJackReact = TRUE
			BREAK
			
			CASE MPF_TRUCKER_2
				#IF IS_DEBUG_BUILD	strDebugName = "TRUCKER 2"	#ENDIF
				eModel = e_ModelHillBilly1
				startVeh = a_Vehicles[MVF_TRUCK_2].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
				//bBlockEvents = TRUE
				bAutoEquipWeapon = TRUE
				bPreventJackReact = TRUE
			BREAK
		
			CASE MPF_HILLBILLY_1
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 1"	#ENDIF
				eModel = e_ModelHillbilly2
				startVeh = a_Vehicles[MVF_SUV_1].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			CASE MPF_HILLBILLY_2
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 2"	#ENDIF
				eModel = e_ModelHillBilly1
				startVeh = a_Vehicles[MVF_SUV_1].veh
				eSeat = VS_FRONT_RIGHT
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			CASE MPF_HILLBILLY_3
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 3"	#ENDIF
				eModel = e_ModelHillBilly1
				startVeh = a_Vehicles[MVF_SUV_2].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			CASE MPF_HILLBILLY_4
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 4"	#ENDIF
				eModel = e_ModelHillbilly2
				startVeh = a_Vehicles[MVF_SUV_2].veh
				eSeat = VS_FRONT_RIGHT
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			CASE MPF_HILLBILLY_5
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 5"	#ENDIF
				eModel = e_ModelHillbilly2
				startVeh = a_Vehicles[MVF_SUV_3].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			CASE MPF_HILLBILLY_6
				#IF IS_DEBUG_BUILD	strDebugName = "HILLBILLY 6"	#ENDIF
				eModel = e_ModelHillbilly2
				startVeh = a_Vehicles[MVF_SUV_3].veh
				eSeat = VS_FRONT_RIGHT
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bInfluenceWantedLevel = FALSE
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("SPAWN_MISSION_PED() - Invalid ped flag.")
			BREAK
		ENDSWITCH
		
		// Exit function is the model hasn't already been loaded
		IF NOT HAS_MODEL_LOADED( eModel )
			RETURN FALSE
			PRINTLN("SPAWN_MISSION_PED() - Model not loaded.")
		ENDIF
		
		// Default position and header can be overridden
		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
			IF NOT DOES_ENTITY_EXIST( attachEntity )
				vPos = vPosOverride
			ELSE
				vAttachPosOffset = vPosOverride
			ENDIF
		ENDIF
		
		IF fHeadingOverride != -1.0
			fHeading = fHeadingOverride
		ENDIF
		
		// Create ped, setup blip and type
		IF IS_ENTITY_ALIVE( startVeh )
			a_Peds[ePed].ped = CREATE_PED_INSIDE_VEHICLE( startVeh, PEDTYPE_MISSION, eModel, eSeat )
		ELSE
			a_Peds[ePed].ped = CREATE_PED( PEDTYPE_MISSION, eModel, vPos, fHeading )
			SET_ENTITY_COLLISION( a_Peds[ePed].ped, bCollision )
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION( a_Peds[ePed].ped, !bCompletelyBlockCollision )
		ENDIF
		
		IF bNoOffset
			SET_ENTITY_COORDS_NO_OFFSET( a_Peds[ePed].ped, vPos )
		ENDIF
		
		IF bBlip
			IF relGroup = relGroupEnemies
				a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
			ELSE
				a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, FALSE )
			ENDIF
		ENDIF
		
		SET_PED_DROPS_WEAPONS_WHEN_DEAD( a_Peds[ePed].ped, bDropsWeapon )
		
		// Flags
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_ListensToSoundEvents, bCanListen )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_GeneratesSoundEvents, bGenerateSound )
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, bBlockEvents )
		SET_PED_KEEP_TASK(a_Peds[ePed].ped, bKeepTask )
		SET_PED_DIES_WHEN_INJURED(a_Peds[ePed].ped, bDiesWhenInjured )
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_DisableHurt, bDisableHurt)
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_DontInfluenceWantedLevel, !bInfluenceWantedLevel )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_WILL_SCAN_FOR_DEAD_PEDS, bScanDeadBodies )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_LEAVE_VEHICLES, bLeaveVehicle )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_NotAllowedToJackAnyPlayers, bCantJackPlayer )
		SET_ENTITY_LOAD_COLLISION_FLAG( a_Peds[ePed].ped, bLoadCollision )
		SET_PED_ARMOUR( a_Peds[ePed].ped, iArmour )
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_PLAY_REACTION_ANIMS, bReact )
		SET_ENTITY_VISIBLE( a_Peds[ePed].ped, bVisible )
		SET_ENTITY_INVINCIBLE( a_Peds[ePed].ped, bInvincible )
		GIVE_WEAPON_TO_PED( a_Peds[ePed].ped, eWeapon, INFINITE_AMMO, bAutoEquipWeapon, bAutoEquipWeapon )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_DO_DRIVEBYS, bDoDriveby )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, bUseVehicle )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE_ATTACK, bVehicleAttack )
		SET_PED_COMBAT_ATTRIBUTES(  a_Peds[ePed].ped, CA_AGGRESSIVE, bAggresive )
		SET_PED_SUFFERS_CRITICAL_HITS( a_Peds[ePed].ped, bSufferCritical )
		SET_PED_CAN_BE_TARGETTED( a_Peds[ePed].ped, bTargetable )
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_RunFromFiresAndExplosions, bRunFromExplosions)  
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_FallsOutOfVehicleWhenKilled, bFallOutVehicle)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_BLIND_FIRE_IN_COVER, bBlindFire)                      
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_DISABLE_PIN_DOWN_OTHERS, bDisablePinDownOthers)                  
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_DISABLE_PINNED_DOWN, bDisablePinnedDown)                        
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, bMoveBeforeCover)                                    
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_UseKinematicModeWhenStationary, bKinematicWhenStill)   
		SET_PED_TO_LOAD_COVER(a_Peds[ePed].ped, bLoadCover)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_ALWAYS_FLEE, bAlwaysFlee )
		DISABLE_PED_PAIN_AUDIO(a_Peds[ePed].ped, bDisablePainAudio)
		STOP_PED_SPEAKING( a_Peds[ePed].ped, bStopSpeaking )
		SET_PED_VISUAL_FIELD_PROPERTIES( a_Peds[ePed].ped, fSeeingRange, fPeripheralSeeingRange, fCentreAngle, fMinAngle, fMaxAngle )
		FREEZE_ENTITY_POSITION( a_Peds[ePed].ped, bFreeze )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_Avoidance_Ignored_by_All, bNotAvoided )
		SET_PED_IS_AVOIDED_BY_OTHERS( a_Peds[ePed].ped, !bNotAvoided )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_AllowAutoShuffleToDriversSeat, bAutoShuffleToDriver )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_PreventPedFromReactingToBeingJacked, bPreventJackReact )

		IF NOT bCleanupOnDeath
			SET_PED_BIT_FLAG( ePed, MPBF_DONT_CLEANUP )
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strScenario)
			TASK_START_SCENARIO_IN_PLACE( a_Peds[ePed].ped, strScenario, -1 )
		ENDIF
		
		IF iHealth > 0
			IF iHealth > GET_ENTITY_MAX_HEALTH( a_Peds[ePed].ped )
			OR bOverrideMaxHealth
				SET_ENTITY_MAX_HEALTH( a_Peds[ePed].ped, iHealth )
			ENDIF
			SET_ENTITY_HEALTH( a_Peds[ePed].ped, iHealth )
		ENDIF
		
		IF iAccuracy > 0
			SET_PED_ACCURACY( a_Peds[ePed].ped, iAccuracy )
		ENDIF
		
		IF fMinBlanks > 0
		OR fMaxBlanks > 0
			SET_PED_CHANCE_OF_FIRING_BLANKS( a_Peds[ePed].ped, fMinBlanks, fMaxBlanks )
		ENDIF
		
		// Relationship group
		SET_PED_RELATIONSHIP_GROUP_HASH( a_Peds[ePed].ped, relGroup )
		
		#IF IS_DEBUG_BUILD 
			SET_PED_NAME_DEBUG( a_Peds[ePed].ped, strDebugName )
		#ENDIF
		
		// Attachment
		IF DOES_ENTITY_EXIST( attachEntity )
			ATTACH_ENTITY_TO_ENTITY( a_Peds[ePed].ped, attachEntity, -1, vAttachPosOffset, vAttachRotOffset, TRUE, TRUE, TRUE )
			SET_PED_CAN_RAGDOLL( a_Peds[ePed].ped, FALSE )
			SET_PED_BIT_FLAG( ePed, MPBF_BLOCK_RAGDOLL )
		ENDIF
		
		// Configure variations
		SET_MISSION_PED_VARIATIONS( ePed )
		
		ADD_MISSION_PED_FOR_DIALOGUE( ePed )
		
		IF eWeaponComponent != WEAPONCOMPONENT_INVALID
			GIVE_WEAPON_COMPONENT_TO_PED( a_Peds[ePed].ped, eWeapon, eWeaponComponent )
			SET_CURRENT_PED_WEAPON( a_Peds[ePed].ped, eWeapon, TRUE )
		ENDIF
		
		SET_PED_COMBAT_ABILITY( a_Peds[ePed].ped, eCombatLevel )
		
		IF bStealthMode
			SET_PED_STEALTH_MOVEMENT( a_Peds[ePed].ped, TRUE, "DEFAULT_ACTION" )
		ENDIF
		
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

//===============================================
//	Checks if area contains a mission vehicle
//===============================================
FUNC BOOL IS_MISSION_VEHICLE_IN_AREA( VECTOR vPos, FLOAT fSize = 5.0 )
	BOOL bOccupied
	bOccupied = FALSE
	
	INT i
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		IF DOES_ENTITY_EXIST(a_Vehicles[i].veh)
		AND IS_ENTITY_AT_COORD(a_Vehicles[i].veh, vPos, <<fSize,fSize,fSize>>)
			bOccupied = TRUE
		ENDIF
	ENDREPEAT
	
	RETURN bOccupied
ENDFUNC

//===============================================
//	Updates a ped
//===============================================
PROC UPDATE_PED( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			CASE MPF_TRUCKER_1
			CASE MPF_TRUCKER_2
				MISSION_VEHICLE_FLAGS eTruck
				MISSION_VEHICLE_FLAGS eOtherTruck
				
				IF ePed = MPF_TRUCKER_1
					eTruck = MVF_TRUCK_1
					eOtherTruck = MVF_TRUCK_2
				ELSE
					eTruck = MVF_TRUCK_2
					eOtherTruck = MVF_TRUCK_1
				ENDIF
			
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						a_Peds[ePed].iTimer = GET_GAME_TIMER()
						
						IF GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) <= 15
							SET_IK_TARGET( a_Peds[ePed].ped, IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), V_ZERO, ITF_DEFAULT )
						ENDIF

					BREAK
					CASE 1
						IF IS_ENTITY_ALIVE( a_Vehicles[eTruck].veh )
							// Stop cars spawning on it's route
							SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS( a_Vehicles[eTruck].veh, TRUE )
							VECTOR vOffset
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
							AND NOT IS_ENTITY_TOUCHING_ENTITY( a_Vehicles[eTruck].veh, a_Vehicles[MVF_DUEL].veh )

								// Attempt to warp truck
								IF GET_GAME_TIMER() >= i_RamTimer
								AND NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_PASSED_WARP )
								AND NOT IS_ENTITY_AT_COORD( a_Vehicles[eTruck].veh, v_SecondTruckWarp, <<7,7,7>> )
								AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_SecondTruckWarp) < GET_DISTANCE_BETWEEN_ENTITIES( a_Vehicles[eTruck].veh, PLAYER_PED_ID() )
								AND NOT IS_SPHERE_VISIBLE( v_SecondTruckWarp, 7.0 )
								AND NOT IS_SPHERE_VISIBLE( GET_ENTITY_COORDS(a_Vehicles[eTruck].veh), 7.0 )
								AND NOT IS_MISSION_VEHICLE_IN_AREA( v_SecondTruckWarp, 8 )
									FLOAT fHeading
									fHeading = GET_HEADING_BETWEEN_VECTORS_2D(v_SecondTruckWarp, GET_ENTITY_COORDS(PLAYER_PED_ID()))
									CLEAR_AREA_OF_VEHICLES( v_SecondTruckWarp, 8.0 )
									CLEAR_AREA_OF_VEHICLES( GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_SecondTruckWarp, fHeading, <<0,-8,0>>), 8.0 )
									SET_ENTITY_COORDS( a_Vehicles[eTruck].veh, v_SecondTruckWarp )
									SET_VEHICLE_ON_GROUND_PROPERLY( a_Vehicles[eTruck].veh )
									SET_ENTITY_HEADING( a_Vehicles[eTruck].veh, fHeading )
									SET_VEHICLE_ENGINE_ON( a_Vehicles[eTruck].veh, TRUE, TRUE )
									SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eTruck].veh, 30 )
									
									SET_MISSION_BIT_FLAG(MBF_DO_HORN)
									e_HornTruck = eTruck
									i_HornTime = GET_GAME_TIMER() + 5000
								ENDIF
								
								IF NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_PASSED_WARP )
								AND IS_ENTITY_AT_COORD( a_Vehicles[eTruck].veh, v_SecondTruckWarp, <<7,7,7>> ) 	
									SET_MISSION_BIT_FLAG( MBF_TRUCK_PASSED_WARP )
								ENDIF
								
								// Fudge heading
								IF GET_GAME_TIMER() >= i_RamTimer
								AND IS_ENTITY_OCCLUDED(a_Vehicles[eTruck].veh )
								AND IS_ENTITY_ALIVE(a_Vehicles[eOtherTruck].veh) 
								AND GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[eTruck].veh, a_Vehicles[eOtherTruck].veh) >= 20
									vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( a_Vehicles[eTruck].veh, GET_ENTITY_COORDS(PLAYER_PED_ID()) )
									
									IF vOffset.X < -3.5
									OR vOffset.X > 3.5
										FLOAT fSpeed
										fSpeed = GET_ENTITY_SPEED( a_Vehicles[eTruck].veh )
										fSpeed = CLAMP( fSpeed, 20, 50 )
										SET_ENTITY_HEADING( a_Vehicles[eTruck].veh, GET_HEADING_BETWEEN_VECTORS_2D( GET_ENTITY_COORDS(a_Vehicles[eTruck].veh), GET_ENTITY_COORDS(PLAYER_PED_ID()) ) )
										SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eTruck].veh, fSpeed )
									ENDIF
								ELSE
//									vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( a_Vehicles[eTruck].veh, GET_ENTITY_COORDS( PLAYER_PED_ID() ) )
//									
//									IF vOffset.X < -1.5
//										TASK_VEHICLE_TEMP_ACTION( a_Peds[ePed].ped, a_Vehicles[eTruck].veh, TEMPACT_SWERVELEFT, 500 )
//									ELIF vOffset.X > 1.5
//										TASK_VEHICLE_TEMP_ACTION( a_Peds[ePed].ped, a_Vehicles[eTruck].veh, TEMPACT_SWERVERIGHT, 500 )
//									ENDIF
								ENDIF
								
								// Abort when near player or the other truck
								IF IS_PED_BIT_FLAG_SET( ePed, MPBF_WARPED )
								OR GET_DISTANCE_BETWEEN_ENTITIES(a_Peds[ePed].ped, PLAYER_PED_ID()) <= 15
								OR (IS_ENTITY_ALIVE(a_Vehicles[eOtherTruck].veh) AND GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[eTruck].veh, a_Vehicles[eOtherTruck].veh) <= 15)
									IF GET_DISTANCE_BETWEEN_ENTITIES(a_Peds[ePed].ped, PLAYER_PED_ID()) <= 15
										IF NOT DOES_BLIP_EXIST( a_Peds[ePed].blip )
											a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
											PLAY_PED_AMBIENT_SPEECH( a_Peds[ePed].ped, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED | SPEECH_PARAMS_MEGAPHONE )
										ENDIF
									ENDIF
									a_Peds[ePed].iEvent++
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST( a_Peds[ePed].blip )
									a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
									PLAY_PED_AMBIENT_SPEECH( a_Peds[ePed].ped, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED | SPEECH_PARAMS_MEGAPHONE )
								ENDIF
								a_Peds[ePed].iEvent++
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_ENTITY_ALIVE( a_Vehicles[eTruck].veh )
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
							TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 3
						IF NOT DOES_BLIP_EXIST( a_Peds[ePed].blip )
							IF GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 85
								a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
							ENDIF
						ENDIF
					
						IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
						OR IS_MISSION_BIT_FLAG_SET( MBF_TRUCKS_TRIGGERED )
							IF NOT IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
							AND GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) >= 250
								CLEANUP_PED(ePed, FALSE)
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) >= 250
								CLEANUP_PED(ePed, FALSE)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		
			CASE MPF_HILLBILLY_1
			CASE MPF_HILLBILLY_2
			CASE MPF_HILLBILLY_3
			CASE MPF_HILLBILLY_4
			CASE MPF_HILLBILLY_5
			CASE MPF_HILLBILLY_6
				IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_SUVS )
					IF NOT IS_PED_IN_COMBAT( a_Peds[ePed].ped, PLAYER_PED_ID() )
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_CHASE )
						TASK_COMBAT_PED(a_Peds[ePed].ped, PLAYER_PED_ID())
						PRINTLN("RE_Duel: Hillbilly ", (ENUM_TO_INT(ePed) -1), " isn't doing anything. Re-tasking.")
					ENDIF
				ENDIF
				
				IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_SUVS )
					IF NOT IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
					AND GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) >= 250
						CLEANUP_PED(ePed, FALSE)
					ENDIF
				ELSE
					IF IS_MISSION_BIT_FLAG_SET( MBF_CAR_CHASE )
					AND GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) >= 250
						CLEANUP_PED(ePed, FALSE)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF IS_PED_BIT_FLAG_SET( ePed, MPBF_BLOCK_RAGDOLL )
			SET_PED_CAN_RAGDOLL( a_Peds[ePed].ped, TRUE )
			DETACH_ENTITY( a_Peds[ePed].ped, TRUE, FALSE )
			CLEAR_PED_BIT_FLAG( ePed, MPBF_BLOCK_RAGDOLL )
		ENDIF
		
		IF NOT IS_PED_BIT_FLAG_SET( ePed, MPBF_DONT_CLEANUP )
			IF IS_ENTITY_VISIBLE( a_Peds[ePed].ped )
				CLEANUP_PED( ePed, FALSE )
			ELSE
				CLEANUP_PED( ePed, TRUE )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Checks if vehicle has a driver
//===============================================
FUNC BOOL DOES_VEHICLE_HAVE_A_DRIVER( VEHICLE_INDEX veh )

	IF IS_ENTITY_ALIVE( veh )
	AND NOT IS_VEHICLE_SEAT_FREE( veh )
		PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT( veh )
		
		IF IS_ENTITY_ALIVE( pedDriver )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//===============================================
//	Should this car be warped
//===============================================
FUNC BOOL SHOULD_CAR_BE_WARPED( MISSION_VEHICLE_FLAGS eVeh )
	MISSION_VEHICLE_FLAGS eStart
	MISSION_VEHICLE_FLAGS eEnd
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
		eStart = MVF_TRUCK_1
	ELSE
		eStart = MVF_SUV_1
	ENDIF
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_SUVS )
		eEnd = MVF_SUV_3
	ELSE
		IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
			eEnd = MVF_TRUCK_2
		ELSE
			eEnd = MVF_SUV_1
		ENDIF
	ENDIF
	
	INT i
	
	INT iLongest
	iLongest = -1
	MISSION_VEHICLE_FLAGS eLongest
	
	FLOAT fFurthest
	fFurthest = -1.0
	MISSION_VEHICLE_FLAGS eFurthest

	FOR i = ENUM_TO_INT(eStart) TO ENUM_TO_INT(eEnd)
		IF IS_ENTITY_ALIVE(a_Vehicles[i].veh)
			IF ((iLongest = -1.0) OR ((GET_GAME_TIMER() - a_Vehicles[i].iTimer) > iLongest))
				iLongest = GET_GAME_TIMER() - a_Vehicles[i].iTimer
				eLongest = INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)
			ENDIF
			
			IF((fFurthest = -1) OR (GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[i].veh, PLAYER_PED_ID()) > fFurthest))
				fFurthest = GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[i].veh, PLAYER_PED_ID())
				eFurthest = INT_TO_ENUM(MISSION_VEHICLE_FLAGS, i)
			ENDIF
		ENDIF
	ENDFOR

	IF (eVeh = eLongest) OR (eVeh = eFurthest)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

//===============================================
//	Makes sure if a car is in front of player, it stays in front. Same for behind
//===============================================
FUNC BOOL IS_CAR_BEING_WARPED_SAME_DIRECTION( VEHICLE_INDEX vehWarper, VEHICLE_INDEX vehTarget, VECTOR vWarpPos )
	VECTOR vCurrentOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( vehTarget, GET_ENTITY_COORDS(vehWarper) )
	VECTOR vNewOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( vehTarget, vWarpPos )
	
	IF ((vCurrentOffset.Y > 0) AND (vNewOffset.Y > 0))
	OR ((vCurrentOffset.Y < 0) AND (vNewOffset.Y < 0))
		RETURN TRUE
	ENDIF
	
	// Trucks are exempt for this rule if not blipped
	IF vehWarper = a_Vehicles[MVF_TRUCK_1].veh
	AND NOT DOES_BLIP_EXIST( a_Peds[MPF_TRUCKER_1].blip )
		RETURN TRUE
	ENDIF
	
	IF vehWarper = a_Vehicles[MVF_TRUCK_2].veh
	AND NOT DOES_BLIP_EXIST( a_Peds[MPF_TRUCKER_2].blip )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Makes sure car isn't being warped too far
//===============================================
FUNC BOOL IS_CAR_BEING_WARPED_CLOSE_ENOUGH( VEHICLE_INDEX vehWarper, VECTOR vWarpPos, FLOAT fDistance = 1500.0 )
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( vehWarper, vWarpPos ) <= fDistance
		RETURN TRUE
	ENDIF
	
	// Trucks are exempt for this rule if not blipped
	IF vehWarper = a_Vehicles[MVF_TRUCK_1].veh
	AND NOT DOES_BLIP_EXIST( a_Peds[MPF_TRUCKER_1].blip )
		RETURN TRUE
	ENDIF
	
	IF vehWarper = a_Vehicles[MVF_TRUCK_2].veh
	AND NOT DOES_BLIP_EXIST( a_Peds[MPF_TRUCKER_2].blip )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Warps car near player
//===============================================
PROC WARP_CHASE_CAR( MISSION_VEHICLE_FLAGS eVeh, VEHICLE_INDEX playerVeh, INT& iCarWarpTimer )
	IF IS_VEHICLE_DRIVEABLE(a_Vehicles[eVeh].veh)
		IF DOES_ENTITY_EXIST(playerVeh)
		AND a_Vehicles[eVeh].veh != playerVeh
		AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(playerVeh))
		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(playerVeh))
		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(playerVeh))
		AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(playerVeh))
		AND DOES_VEHICLE_HAVE_A_DRIVER(a_Vehicles[eVeh].veh)
		
			FLOAT fWarpDistFromPlayer
			FLOAT fVehDistFromPlayer
			INT iHiddenTime
			
			IF eVeh = MVF_TRUCK_1
			OR eVeh = MVF_TRUCK_2
				fWarpDistFromPlayer = 50
				fVehDistFromPlayer = 35
				iHiddenTime = 1500
			ELSE
				fWarpDistFromPlayer = 75
				fVehDistFromPlayer = 50
				iHiddenTime = 3000
			ENDIF
			
			// Once player is  away from their last noted position, update the variables
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( playerVeh, v_LastPlayerPos) >= fWarpDistFromPlayer
			AND IS_VEHICLE_ON_ALL_WHEELS(playerVeh)
				// Use last noted position as potenital warp location
				v_NextCarWarpPos 		= v_LastPlayerPos
				f_NextCarWarpHeading 	= f_LastPlayerHeading
				
				// Take note of players new position for future checks
				v_LastPlayerPos = GET_ENTITY_COORDS( playerVeh )
				f_LastPlayerHeading = GET_ENTITY_HEADING( playerVeh )
			ENDIF
			
			// If car isn't occlued (it's visible), then update timer
			IF NOT IS_ENTITY_OCCLUDED(a_Vehicles[eVeh].veh)
				iCarWarpTimer = GET_GAME_TIMER()
			// Otherwise, car is hidden:
			ELSE
				IF GET_GAME_TIMER() - iCarWarpTimer > iHiddenTime // Car must be hidden for 3 seconds before it is warped
				AND GET_GAME_TIMER() - i_GlobalWarpTimer > 1500 // A car warp can only occur 1.5 seconds after the last one
				AND (GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[eVeh].veh, playerVeh)) > (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(playerVeh, v_NextCarWarpPos)) // Only warp if it's closer
				AND SHOULD_CAR_BE_WARPED(eVeh)
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( playerVeh, v_NextCarWarpPos) >= fWarpDistFromPlayer	// Make sure player is away from warp pos
				AND GET_DISTANCE_BETWEEN_ENTITIES( a_Vehicles[eVeh].veh, playerVeh ) >= fVehDistFromPlayer	// Make sure car being warped is away from player
				AND IS_CAR_BEING_WARPED_CLOSE_ENOUGH( a_Vehicles[eVeh].veh, v_NextCarWarpPos ) 	// url:bugstar:2018155
				AND IS_CAR_BEING_WARPED_SAME_DIRECTION( a_Vehicles[eVeh].veh, playerVeh, v_NextCarWarpPos ) /// url:bugstar:2018155
				AND NOT IS_VECTOR_ZERO(v_NextCarWarpPos)					// Don't warp to origin!
				AND NOT IS_SPHERE_VISIBLE(v_NextCarWarpPos, 4.0)			// Check warp pos isn't visible
				AND NOT IS_MISSION_VEHICLE_IN_AREA(v_NextCarWarpPos)
					CLEAR_AREA_OF_PEDS(v_NextCarWarpPos, 1.5)
			        CLEAR_AREA_OF_VEHICLES(v_NextCarWarpPos, 5.0)
					
					IF eVeh = MVF_TRUCK_1
					OR eVeh = MVF_TRUCK_2
						DETACH_VEHICLE_FROM_TRAILER( a_Vehicles[eVeh].veh )
						
						IF eVeh = MVF_TRUCK_1
							SET_PED_BIT_FLAG( MPF_TRUCKER_1, MPBF_WARPED )
						ELSE
							SET_PED_BIT_FLAG( MPF_TRUCKER_2, MPBF_WARPED )
						ENDIF
					ENDIF
					
					SET_ENTITY_COORDS( a_Vehicles[eVeh].veh, v_NextCarWarpPos, TRUE )
					SET_VEHICLE_ON_GROUND_PROPERLY( a_Vehicles[eVeh].veh )
					SET_ENTITY_HEADING( a_Vehicles[eVeh].veh, f_NextCarWarpHeading )
		            
					SET_VEHICLE_ENGINE_ON( a_Vehicles[eVeh].veh, TRUE, TRUE )
		            SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eVeh].veh, GET_ENTITY_SPEED(playerVeh) * 1.2 )
		            
		            iCarWarpTimer = GET_GAME_TIMER()
					a_Vehicles[eVeh].iTimer = GET_GAME_TIMER()
					i_GlobalWarpTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ELSE
			iCarWarpTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Is the player in the target vehicle
//===============================================
FUNC BOOL IS_PLAYER_IN_DUEL_CAR()
	RETURN ( IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh ) AND IS_PED_IN_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh ) )
ENDFUNC

//===============================================
//	Updates a vehicle
//===============================================
PROC UPDATE_VEHICLE( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		SWITCH eVeh
			CASE MVF_SUV_1
				IF ( NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_1].ped ) AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_2].ped ) )
				OR NOT IS_VEHICLE_DRIVEABLE( a_Vehicles[eVeh].veh )
					CLEANUP_VEHICLE( eVeh, FALSE )
				ENDIF
			BREAK
			CASE MVF_SUV_2
				IF ( NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_3].ped ) AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_4].ped ) )
				OR NOT IS_VEHICLE_DRIVEABLE( a_Vehicles[eVeh].veh )
					CLEANUP_VEHICLE( eVeh, FALSE )
				ENDIF
			BREAK
			CASE MVF_SUV_3
				IF ( NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_5].ped ) AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_6].ped ) )
				OR NOT IS_VEHICLE_DRIVEABLE( a_Vehicles[eVeh].veh )
					CLEANUP_VEHICLE( eVeh, FALSE )
				ENDIF
			BREAK
			CASE MVF_TRUCK_1
				IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_1].ped )
				OR NOT IS_VEHICLE_DRIVEABLE( a_Vehicles[eVeh].veh )
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Vehicles[eVeh].veh) >= 50
						CLEANUP_VEHICLE( eVeh, FALSE )
					ENDIF
				ENDIF
				
				IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
					WARP_CHASE_CAR( eVeh, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), i_Truck1WarpTimer )
				ENDIF
			BREAK
			CASE MVF_TRUCK_2
				IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_2].ped )
				OR NOT IS_VEHICLE_DRIVEABLE( a_Vehicles[eVeh].veh )
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Vehicles[eVeh].veh) >= 50
						CLEANUP_VEHICLE( eVeh, FALSE )
					ENDIF
				ENDIF
				
				IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
					WARP_CHASE_CAR( eVeh, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), i_Truck2WarpTimer )
				ENDIF
			BREAK
			CASE MVF_TRAILER_1
				IF NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_TRUCK_1].veh )
				OR GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[MVF_TRUCK_1].veh, a_Vehicles[eVeh].veh) >= 150
					CLEANUP_VEHICLE( eVeh, FALSE )
				ENDIF
			BREAK
			CASE MVF_TRAILER_2
				IF NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_TRUCK_2].veh )
				OR GET_DISTANCE_BETWEEN_ENTITIES(a_Vehicles[MVF_TRUCK_2].veh, a_Vehicles[eVeh].veh) >= 150
					CLEANUP_VEHICLE( eVeh, FALSE )
				ENDIF
			BREAK
			CASE MVF_DUEL
				SWITCH a_Vehicles[eVeh].iEvent
					CASE 0
						IF IS_PLAYER_IN_DUEL_CAR()
							SET_MISSION_BIT_FLAG( MBF_CAR_STOLEN )
							SAFE_REMOVE_BLIP( a_Vehicles[eVeh].blip )
							a_Vehicles[eVeh].iEvent++
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		IF IS_ENTITY_VISIBLE( a_Vehicles[eVeh].veh )
			CLEANUP_VEHICLE( eVeh, FALSE )
		ELSE
			CLEANUP_VEHICLE( eVeh, TRUE )
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Updates all mission peds
//===============================================
PROC UPDATE_ALL_PEDS()
	INT i
	REPEAT COUNT_OF( MISSION_PED_FLAGS ) i
		IF DOES_ENTITY_EXIST( a_Peds[i].ped )
			UPDATE_PED( INT_TO_ENUM( MISSION_PED_FLAGS, i ) )
		ENDIF
	ENDREPEAT
ENDPROC

//===============================================
//	Updates all mission vehs
//===============================================
PROC UPDATE_ALL_VEHICLES()
	INT i
	REPEAT COUNT_OF( MISSION_VEHICLE_FLAGS ) i
		IF DOES_ENTITY_EXIST( a_Vehicles[i].veh )
			UPDATE_VEHICLE( INT_TO_ENUM( MISSION_VEHICLE_FLAGS, i ) )
		ENDIF
	ENDREPEAT
ENDPROC

////===============================================
////	Updates all mission trains
////===============================================
//PROC UPDATE_ALL_TRAINS()
//	INT i
//	REPEAT COUNT_OF( MISSION_TRAIN_FLAGS ) i
//		IF DOES_ENTITY_EXIST( a_Trains[i].train )
//			UPDATE_TRAIN( INT_TO_ENUM( MISSION_TRAIN_FLAGS, i ) )
//		ENDIF
//	ENDREPEAT
//ENDPROC

//===============================================
//	Load scene with wait
//===============================================
PROC LOAD_SCENE_WITH_WAIT( VECTOR vPos, FLOAT fRadius = 20.0 )
	NEW_LOAD_SCENE_START_SPHERE( vPos, fRadius )
		
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
		WAIT(0)
	ENDWHILE

	NEW_LOAD_SCENE_STOP()
ENDPROC

//===============================================
//	Creates a mission vehicle
//===============================================
FUNC BOOL SPAWN_MISSION_VEHICLE( MISSION_VEHICLE_FLAGS eVeh, VECTOR vPosOverride, FLOAT fHeadingOverride = -1.0, BOOL bBlip = FALSE)
	
	IF NOT DOES_ENTITY_EXIST( a_Vehicles[eVeh].veh )
	
		// Values used to create & configure the vehicles
		VECTOR vPos
		FLOAT fHeading
		MODEL_NAMES eModel
		LOCK_STATE eLock = VEHICLELOCK_NONE
		BOOL bCollision = TRUE
		BOOL bVisible = TRUE
		BOOL bStrong = FALSE
		BOOL bFreeze = FALSE
		BOOL bEngineOn = FALSE
		BOOL bHeliBlades = FALSE
		BOOL bEnemy = TRUE
		BOOL bInvincible = FALSE
		BOOL bLoadCollision = FALSE
		BOOL bTargetable = FALSE
		BOOL bAutoDamageEngine = FALSE
		BOOL bDisableTowing = FALSE
		BOOL bAutoAttach = TRUE
		BOOL bOnlyPlayerDamage = FALSE
		BOOL bCanTyresPop = TRUE
		BOOL bNeedsHotwiring = FALSE
		BOOL bCompletelyDisableCollision = FALSE
		FLOAT fSpeed
		FLOAT fPlaneTurb = -1
		FLOAT fHeliTurb = -1
		ENTITY_INDEX attachEntity
		VECTOR vAttachPosOffset
		VECTOR vAttachRotOffset
		STRING strNumPlate
		#IF IS_DEBUG_BUILD STRING strName	#ENDIF

		// Configure each vehicle appropriately 
		SWITCH eVeh
			CASE MVF_DUEL
				#IF IS_DEBUG_BUILD	strName = "DUEL"	#ENDIF
				vPos = v_CarStart
				fHeading = 89.7263
				eModel = e_ModelDuel
				bEnemy = FALSE
				bDisableTowing = TRUE
				bAutoAttach = FALSE
				bNeedsHotwiring = TRUE
				bStrong = TRUE
				bCanTyresPop = FALSE
				eLock = VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED
			BREAK
			
			CASE MVF_TRUCK_1
				#IF IS_DEBUG_BUILD	strName = "TRUCK 1"	#ENDIF
				vPos = v_Truck1Spawn
				fHeading = 175.3714
				eModel = e_ModelTruck
				bEngineOn = TRUE
				bDisableTowing = TRUE
				bAutoAttach = FALSE
				strNumPlate = "96NWO218"
				eLock = VEHICLELOCK_LOCKED
			BREAK
			
			CASE MVF_TRUCK_2
				#IF IS_DEBUG_BUILD	strName = "TRUCK 2"	#ENDIF
				vPos = v_Truck2Spawn
				fHeading = 171.000
				eModel = e_ModelTruck
				bEngineOn = TRUE
				bDisableTowing = TRUE
				bAutoAttach = FALSE
				strNumPlate = "01DTS039"
				eLock = VEHICLELOCK_LOCKED
			BREAK
			
			CASE MVF_TRAILER_1
				#IF IS_DEBUG_BUILD	strName = "TRAILER 1"	#ENDIF
				vPos = <<930.46, 2719.65, 42.41>>
				fHeading = 180.53
				eModel = e_ModelTrailer
			BREAK
			
			CASE MVF_TRAILER_2
				#IF IS_DEBUG_BUILD	strName = "TRAILER 2"	#ENDIF
				vPos = <<1141.17, 2711.97, 40.04>>
				fHeading = 176.67
				eModel = e_ModelTrailer
			BREAK
			
			CASE MVF_SUV_1
				#IF IS_DEBUG_BUILD	strName = "SUV 1"	#ENDIF
				vPos = <<514.5844, -649.9937, 23.7512>>
				fHeading = 182.0097
				eModel = e_ModelSUV
				bFreeze = TRUE
				bEngineOn = TRUE
				bCanTyresPop = FALSE
				strNumPlate = "18NJM316"
			BREAK
			
			CASE MVF_SUV_2
				#IF IS_DEBUG_BUILD	strName = "SUV 2"	#ENDIF
				vPos = <<507.3959, -653.6174, 23.7512>>
				fHeading = 177.8055
				eModel = e_ModelSUV
				bFreeze = TRUE
				bEngineOn = TRUE
				bCanTyresPop = FALSE
				strNumPlate = "28HDT291"
			BREAK
			
			CASE MVF_SUV_3
				#IF IS_DEBUG_BUILD	strName = "SUV 3"	#ENDIF
				vPos = <<1200.5469, -1553.6067, 38.4019>>
				fHeading = 0.0001
				eModel = e_ModelSUV
				bFreeze = TRUE
				bEngineOn = TRUE
				bCanTyresPop = FALSE
				strNumPlate = "23DJT162"
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("SPAWN_MISSION_VEHICLE() - Invalid vehicle flag.")
			BREAK
		ENDSWITCH
		
		// Exit function is the model hasn't already been loaded
		IF NOT HAS_MODEL_LOADED( eModel )
			RETURN FALSE
			PRINTLN("SPAWN_MISSION_VEHICLE() - Model not loaded.")
		ENDIF
		
		// Default position and rotation can be overridden
		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
			vPos = vPosOverride
		ENDIF
		
		IF fHeadingOverride != -1.0
			fHeading = fHeadingOverride
		ENDIF
		
		// Create vehicle
		a_Vehicles[eVeh].veh = CREATE_VEHICLE( eModel, vPos, fHeading )
		SET_MISSION_VEHICLE_COLOURS( eVeh )
		SET_MISSION_VEHICLE_MODS( eVeh )
		SET_ENTITY_VISIBLE( a_Vehicles[eVeh].veh, bVisible )
		IF DOES_ENTITY_EXIST( attachEntity )
			ATTACH_ENTITY_TO_ENTITY( a_Vehicles[eVeh].veh, attachEntity, -1, vAttachPosOffset, vAttachRotOffset )
		ELSE
			SET_ENTITY_COLLISION( a_Vehicles[eVeh].veh, bCollision )
		ENDIF
		SET_VEHICLE_STRONG( a_Vehicles[eVeh].veh, bStrong )
		FREEZE_ENTITY_POSITION( a_Vehicles[eVeh].veh, bFreeze )
		SET_VEHICLE_ENGINE_ON( a_Vehicles[eVeh].veh, bEngineOn, TRUE )
		SET_ENTITY_INVINCIBLE( a_Vehicles[eVeh].veh, bInvincible )
		SET_ENTITY_LOAD_COLLISION_FLAG( a_Vehicles[eVeh].veh, bLoadCollision )
		SET_VEHICLE_CAN_BE_TARGETTED( a_Vehicles[eVeh].veh, bTargetable )
		SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET( a_Vehicles[eVeh].veh, bAutoDamageEngine )
		SET_VEHICLE_AUTOMATICALLY_ATTACHES( a_Vehicles[eVeh].veh, bAutoAttach )
		SET_VEHICLE_DISABLE_TOWING( a_Vehicles[eVeh].veh, bDisableTowing )
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER( a_Vehicles[eVeh].veh, bOnlyPlayerDamage )
		SET_VEHICLE_TYRES_CAN_BURST( a_Vehicles[eVeh].veh, bCanTyresPop )
		SET_VEHICLE_NEEDS_TO_BE_HOTWIRED( a_Vehicles[eVeh].veh, bNeedsHotwiring )
		SET_ENTITY_COMPLETELY_DISABLE_COLLISION( a_Vehicles[eVeh].veh, !bCompletelyDisableCollision )
		#IF IS_DEBUG_BUILD SET_VEHICLE_NAME_DEBUG(a_Vehicles[eVeh].veh, strName) #ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY( strNumPlate )
			SET_VEHICLE_NUMBER_PLATE_TEXT( a_Vehicles[eVeh].veh, strNumPlate )
		ENDIF
		
		IF fSpeed > 0
			SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eVeh].veh, fSpeed )
		ENDIF
		
		IF bHeliBlades
			SET_HELI_BLADES_FULL_SPEED( a_Vehicles[eVeh].veh )
		ENDIF
		
		IF fPlaneTurb >= 0
			SET_PLANE_TURBULENCE_MULTIPLIER( a_Vehicles[eVeh].veh, fPlaneTurb )
		ENDIF
		
		IF fHeliTurb >= 0
			SET_HELI_TURBULENCE_SCALAR( a_Vehicles[eVeh].veh, fHeliTurb )
		ENDIF
		
		IF eLock != VEHICLELOCK_NONE
			SET_VEHICLE_DOORS_LOCKED( a_Vehicles[eVeh].veh, eLock )
		ENDIF
		
		IF bBlip
			a_Vehicles[eVeh].blip = CREATE_BLIP_FOR_VEHICLE( a_Vehicles[eVeh].veh, bEnemy )
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

////===============================================
////	Creates a mission train
////===============================================
//FUNC BOOL SPAWN_MISSION_TRAIN( MISSION_TRAIN_FLAGS eTrain, VECTOR vPosOverride, BOOL bBlip = FALSE)
//	
//	IF NOT DOES_ENTITY_EXIST( a_Trains[eTrain].train )
//	
//		// Values used to create & configure the trains
//		VECTOR vPos
//		FLOAT fSpeed
//		STRING strTrain
//
//		// Configure each vehicle appropriately 
//		SWITCH eTrain
//			CASE MTF_EXAMPLE
//				vPos = V_ZERO
//				strTrain = ""
//				fSpeed = 0
//			BREAK
//
//			DEFAULT
//				SCRIPT_ASSERT("SPAWN_MISSION_TRAIN() - Invalid train flag.")
//			BREAK
//		ENDSWITCH
//		
//		// Default position and rotation can be overridden
//		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
//			vPos = vPosOverride
//		ENDIF
//		
//		// Create train
//		a_Trains[eTrain].train = CREATE_MISSION_TRAIN_BY_NAME( GET_HASH_KEY(strTrain), vPos, TRUE )
//		
//		SET_MISSION_TRAIN_SPEED( eTrain, fSpeed )
//		
//		IF bBlip
//			a_Trains[eTrain].blip = CREATE_BLIP_FOR_VEHICLE( a_Trains[eTrain].train, TRUE )
//		ENDIF
//	ENDIF
//		
//	RETURN TRUE
//ENDFUNC

//===============================================
//	Fade out and wait
//===============================================
PROC DO_FADE_OUT_WITH_WAIT( BOOL bHideHUDRadar = FALSE )
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_OUT()		
			WAIT(0)
			IF bHideHUDRadar
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

//===============================================
//	Fade in and wait
//===============================================
PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_IN()		
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

////===============================================
////	Request all assets required for a specific stage
////===============================================
//PROC REQUEST_STAGE_ASSETS( MISSION_STAGE_FLAGS eStage, BOOL bLoadAll = TRUE )
//	bLoadAll = bLoadAll
//	SWITCH eStage
//		CASE MSF_STEAL_CAR
//			
//		BREAK
//		
//		CASE MSF_MISSION_PASSED
//		
//		BREAK
//	ENDSWITCH
//ENDPROC
//
////===============================================
////	Check if all stage assets have been loaded
////===============================================
//FUNC BOOL HAVE_STAGE_ASSETS_LOADED( MISSION_STAGE_FLAGS eStage )
//	SWITCH eStage
//		CASE MSF_INTRO
//			RETURN TRUE
//		BREAK
//		
//		CASE MSF_MISSION_PASSED
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC
//
////===============================================
////	Spawn all assets required for a specific stage
////===============================================
//PROC SPAWN_STAGE_ASSETS( MISSION_STAGE_FLAGS eStage )
//	SWITCH eStage
//		CASE MSF_INTRO
//			
//		BREAK
//
//		CASE MSF_MISSION_PASSED
//		
//		BREAK
//	ENDSWITCH
//ENDPROC
//
////===============================================
////	Load stage assets with wait. Used during
//// 	checkpoints & debug skips
////===============================================
//PROC CREATE_STAGE_ASSETS_WITH_WAIT( MISSION_STAGE_FLAGS eStage )
//	// Load assets
//	REQUEST_STAGE_ASSETS( eStage )
//		
//	// Wait until all have loaded
//	WHILE NOT HAVE_STAGE_ASSETS_LOADED( eStage )
//		WAIT(0)
//	ENDWHILE
//
//	// Spawn assets required
//	SPAWN_STAGE_ASSETS( eStage )
//ENDPROC

//===============================================
//	Remove mission text
//===============================================
PROC REMOVE_MISSION_TEXT( BOOL bClearSpeech = TRUE, BOOL bClearGodText = TRUE, BOOL bClearHelpText = TRUE, BOOL bKeepSubs = FALSE )
	IF bClearSpeech
		KILL_ANY_CONVERSATION()
	ENDIF
	
	IF bClearGodText
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		OR (bClearSpeech AND NOT bKeepSubs)
			CLEAR_PRINTS()
		ENDIF
		CLEAR_REMINDER_MESSAGE()
	ENDIF
	
	IF bClearHelpText
		CLEAR_HELP()
	ENDIF
ENDPROC

//===============================================
//	Suppress vehicle models
//===============================================
PROC SUPPRESS_MISSION_MODELS( BOOL bSuppress )
	SET_VEHICLE_MODEL_IS_SUPPRESSED( e_ModelSUV, bSuppress )
	SET_VEHICLE_MODEL_IS_SUPPRESSED( e_ModelTruck, bSuppress )
ENDPROC

//===============================================
//	Toggle emergency services
//===============================================
PROC BLOCK_ALL_EMERGENCY_SERVICES( BOOL bBlock )
	IF bBlock
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		SET_CREATE_RANDOM_COPS(FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	ELSE
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_CREATE_RANDOM_COPS(TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	ENDIF
ENDPROC

//===============================================
//	Go to the next stage
//===============================================
PROC ADVANCE_STAGE( MISSION_STAGE_FLAGS eNextStage )
	i_CurrentEvent = 0
	e_StageSection = MSS_SETUP
	e_MissionStage = eNextStage
ENDPROC

//===============================================
//	Disable the mod shops
//===============================================
PROC TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(BOOL bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, bToggleFlag)
ENDPROC

//===============================================
//	Initialise mission
//===============================================
PROC SETUP_MISSION()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

	//REMOVE_MISSION_TEXT()
	SUPPRESS_MISSION_MODELS(TRUE)
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
//	// Load our text
//	REQUEST_ADDITIONAL_TEXT("CIAJET", DLC_TEXT_SLOT0)
	
	//ADD_MISSION_SCENARIO_BLOCKING_AREA( SBA_EXAMPLE )
	
	//BLOCK_ALL_EMERGENCY_SERVICES( TRUE )
	
	//SET_IGNORE_NO_GPS_FLAG( TRUE )
	
	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	//SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	// Relationship groups
	ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, relGroupEnemies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupEnemies, relGroupEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, relGroupEnemies, RELGROUPHASH_PLAYER)


	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
	
	// Conversation
//	ADD_PED_FOR_DIALOGUE( s_Convo, 1, PLAYER_PED_ID(), "TREVOR" )
	
	TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(TRUE)
	
	// Prevent random trains
//	DELETE_ALL_TRAINS()
//	SET_RANDOM_TRAINS(FALSE)
ENDPROC

//===============================================
//	Move ped out of vehicle
//===============================================
PROC SET_PLAYER_OUT_OF_ANY_VEHICLE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_ENTITY_COORDS( PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0,0,1>> )
	ENDIF
ENDPROC

//===============================================
//	Mission Cleanup
//===============================================
PROC MISSION_CLEANUP( BOOL bTerminate, BOOL bForce )

//	IF NOT HAS_CUTSCENE_FINISHED()
//		STOP_CUTSCENE()
//	ENDIF
	
	//SET_PLAYER_OUT_OF_ANY_VEHICLE()

	//REMOVE_CUTSCENE()

	RESET_VARIABLES()
	REMOVE_ALL_BLIPS()
	
	SUPPRESS_MISSION_MODELS( FALSE )
	REMOVE_MISSION_TEXT()
	
	BLOCK_ALL_EMERGENCY_SERVICES( FALSE )
	
	CLEAR_TIMECYCLE_MODIFIER()
	DISPLAY_RADAR( TRUE )
	
	SET_IGNORE_NO_GPS_FLAG( FALSE )
	
	REMOVE_ALL_ANIM_DICTS()
	REMOVE_ALL_WAYPOINTS()
	REMOVE_ALL_VEH_RECS()
	
	IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
		SET_VEHICLE_TYRES_CAN_BURST( a_Vehicles[MVF_DUEL].veh, TRUE )
	ENDIF
	
	CLEANUP_PEDS( bForce )
	CLEANUP_VEHICLES( bForce )
//	CLEANUP_TRAINS( bForce )
	
	REMOVE_RELATIONSHIP_GROUP( relGroupEnemies )
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_ANY_CONVERSATION()
	
//	REMOVE_PED_FOR_DIALOGUE( s_Convo, 1 )
//	REMOVE_PED_FOR_DIALOGUE( s_Convo, 3 )
//	REMOVE_PED_FOR_DIALOGUE( s_Convo, 4 )
//	REMOVE_PED_FOR_DIALOGUE( s_Convo, 5 )
	
	TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(FALSE)
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS( FALSE, FALSE )
	
	SET_RANDOM_TRAINS(TRUE)
	
	IF bTerminate
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widGroup)
				DELETE_WIDGET_GROUP(widGroup)
			ENDIF
		#ENDIF
	
		RANDOM_EVENT_OVER()
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//===============================================
//	Mission Passed
//===============================================
PROC MISSION_PASSED()

	// Activate the reward vehicle gens.
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RE_DUEL_CITY, TRUE)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RE_DUEL_COUNTRY, TRUE)
	
	// Track car for impound - url:bugstar:2005915
	TRACK_VEHICLE_FOR_IMPOUND(a_Vehicles[MVF_DUEL].veh)

	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	MISSION_CLEANUP(TRUE, FALSE)
ENDPROC

//===============================================
//	Mission Failed
//===============================================
PROC MISSION_FAILED()
	// Lock doors of the Dukes
	IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
		SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_DUEL].veh, VEHICLELOCK_LOCKED )
	ENDIF
	MISSION_CLEANUP(TRUE, FALSE)
ENDPROC

//===============================================
//	Warp the player to a specific point, optionally reset the camera
//===============================================
PROC WARP_PLAYER( VECTOR vPos, FLOAT fHeading, BOOL bSetCamera = TRUE, BOOL bRemoveWeapon = TRUE )
	SET_ENTITY_COORDS( PLAYER_PED_ID(), vPos )
	SET_ENTITY_HEADING( PLAYER_PED_ID(), fHeading )
	IF bSetCamera
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	IF bRemoveWeapon
		SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED )
	ENDIF
ENDPROC

//===============================================
//	Setup car to chase player
//===============================================
PROC SETUP_CHASE_CAR( VEHICLE_INDEX veh, VECTOR vNewPos )
	IF IS_ENTITY_ALIVE( veh )
		FREEZE_ENTITY_POSITION( veh, FALSE )
		
		CLEAR_AREA_OF_PEDS(vNewPos, 1.5)
		CLEAR_AREA_OF_VEHICLES(vNewPos, 3.0)
		SET_ENTITY_COORDS( veh, vNewPos )
		SET_VEHICLE_ON_GROUND_PROPERLY( veh )

		SET_ENTITY_HEADING( veh, GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) )
		
		IF IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT(veh) )
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				TASK_VEHICLE_CHASE( GET_PED_IN_VEHICLE_SEAT(veh), PLAYER_PED_ID() )
			ELSE
				TASK_VEHICLE_MISSION_PED_TARGET( GET_PED_IN_VEHICLE_SEAT(veh), veh, PLAYER_PED_ID(), MISSION_RAM, 100, DRIVINGMODE_AVOIDCARS, -1, -1 )
			ENDIF
		ENDIF
		
		SET_VEHICLE_FORWARD_SPEED( veh, GET_ENTITY_SPEED(PLAYER_PED_ID()) * 1.2 )
	ENDIF
ENDPROC

//===============================================
//	Checks if car is only chase car left
//===============================================
FUNC BOOL IS_ONLY_CHASE_CAR_ALIVE( VEHICLE_INDEX veh )
	
	IF veh = a_Vehicles[MVF_SUV_1].veh
		IF NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_2].veh )
		AND NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_3].veh )
			RETURN TRUE
		ENDIF
	ELIF veh = a_Vehicles[MVF_SUV_2].veh
		IF NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_1].veh )
		AND NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_3].veh )
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_1].veh )
		AND NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_2].veh )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Find a suitable position for the car to spawn
//===============================================
//FUNC BOOL GET_TRUCK_SPAWN_POSITION( VECTOR& vSpawn )
//	BOOL bFound
//	INT i
//	INT iDensity
//	INT iProperties
//	VECTOR vOffset
//	VECTOR vTruckOffset
//	BOOL bSetLeft
//	
//	IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
//		FOR i = 0 TO 1
//			SWITCH i
//				CASE 0
//					vOffset = <<-45,0,0>>
//				BREAK
//				CASE 1
//					vOffset = <<45,0,0>>
//				BREAK
//			ENDSWITCH
//			
//			bSetLeft = FALSE
//			
//			IF GET_CLOSEST_VEHICLE_NODE( GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(a_Vehicles[MVF_DUEL].veh), 356.9734, vOffset), vSpawn )
//				GET_VEHICLE_NODE_PROPERTIES( vSpawn, iDensity, iProperties )
//				
//				// Don't allow nodes that block big vehicles
//				IF ((iProperties & ENUM_TO_INT(VNP_NO_BIG_VEHICLES)) = 0)
//					IF IS_POINT_IN_ANGLED_AREA( vSpawn, <<1043.455688,2683.424316,36.494774>>, <<1044.053711,2695.760742,48.264427>>, 250 )
//						SET_ENTITY_COORDS_NO_OFFSET( a_Vehicles[MVF_DUMMY].veh, GET_ENTITY_COORDS( PLAYER_PED_ID() ) )
//						SET_ENTITY_HEADING( a_Vehicles[MVF_DUMMY].veh, 356.9734 )
//						vTruckOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( a_Vehicles[MVF_DUMMY].veh, vSpawn )
//						IF vTruckOffset.X < 0
//							vSpawn = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( vSpawn, 86.185921, <<-3,0,0>> )
//						ELSE
//							vSpawn = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( vSpawn, 86.185921, <<3,0,0>> )
//							bSetLeft = TRUE
//						ENDIF
//						
//						IF NOT IS_SPHERE_VISIBLE( vSpawn, 6.0 )
//							bFound = TRUE
//							IF bSetLeft
//								SET_MISSION_BIT_FLAG( MBF_LEFT_SIDE )
//							ENDIF
//							i = 4	// Exit the loop
//						ENDIF
//					ELSE
//						SET_MISSION_BIT_FLAG( MBF_TRUCKS_AVOIDED )
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDFOR
//	ENDIF
//	
//	RETURN bFound
//ENDFUNC

//===============================================
//	Stops SUVs from being spawned on side roads when player isn't
//===============================================
FUNC BOOL IS_PLAYER_NEARBY_NODE_ON()
	VECTOR vNode
	IF GET_CLOSEST_VEHICLE_NODE( GET_ENTITY_COORDS(PLAYER_PED_ID()), vNode )
		INT iProperties
		INT iDensity
		IF GET_VEHICLE_NODE_PROPERTIES( vNode, iDensity, iProperties )
			RETURN ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) = 0)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Stops SUVs from being spawned off road when player isn't
//===============================================
FUNC BOOL IS_PLAYER_OFF_ROAD()
	VECTOR vNode
	IF GET_CLOSEST_VEHICLE_NODE( GET_ENTITY_COORDS(PLAYER_PED_ID()), vNode )
		INT iProperties
		INT iDensity
		IF GET_VEHICLE_NODE_PROPERTIES( vNode, iDensity, iProperties )
			RETURN ((iProperties & ENUM_TO_INT(VNP_OFF_ROAD)) = 1)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Stops SUVs from being spawned off road when player isn't
//===============================================
FUNC BOOL IS_PLAYER_GOING_TO_BE_AT( VECTOR vLoc, FLOAT fSeconds )
	FLOAT fSpeed = GET_ENTITY_SPEED(PLAYER_PED_ID())
	FLOAT fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vLoc)
	
	VECTOR vCurrent = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vNext = vCurrent + GET_ENTITY_VELOCITY(PLAYER_PED_ID())
	
	IF (fDistance / fSpeed) <= fSeconds
	AND GET_DISTANCE_BETWEEN_COORDS(vNext, vLoc) < GET_DISTANCE_BETWEEN_COORDS(vCurrent, vLoc)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//===============================================
//	Starts the truck trap sequence
//===============================================
PROC TRIGGER_TRUCK_SEQUENCE( MISSION_VEHICLE_FLAGS eVeh, MISSION_PED_FLAGS ePed, MISSION_VEHICLE_FLAGS eOtherTruck, MISSION_PED_FLAGS eOtherTrucker )
	SET_MISSION_BIT_FLAG( MBF_TRUCKS_TRIGGERED )

	SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eVeh].veh, 5 )
	a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
	
	SAFE_OPEN_SEQUENCE()
		TASK_VEHICLE_DRIVE_TO_COORD(NULL, a_Vehicles[eVeh].veh, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[eVeh].veh, <<0,20,0>>), 30, DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 4.0, -1 )
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
		TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
	SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
	
	// Shout an insult when driving out
	PLAY_PED_AMBIENT_SPEECH( a_Peds[ePed].ped, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED | SPEECH_PARAMS_MEGAPHONE )
	
	a_Peds[ePed].iEvent = 3
	
	// Warp the other truck behind
	IF IS_ENTITY_ALIVE( a_Vehicles[eOtherTruck].veh )
		IF eVeh = MVF_TRUCK_1
			v_SecondTruckWarp = <<1031.986, 2693.441, 38.6861>>
		ELSE
			v_SecondTruckWarp = <<1027.001, 2686.890, 37.8987>>
		ENDIF
		
		i_RamTimer = GET_GAME_TIMER() + 1000
		
		IF IS_ENTITY_ALIVE(a_Peds[eOtherTrucker].ped)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[eOtherTrucker].ped, TRUE )
			TASK_VEHICLE_MISSION( a_Peds[eOtherTrucker].ped, a_Vehicles[eOtherTruck].veh, a_Vehicles[MVF_DUEL].veh, MISSION_RAM, 30, DRIVINGMODE_PLOUGHTHROUGH, -1, -1 )
		ENDIF
		
		a_Peds[eOtherTrucker].iEvent++
	ENDIF
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemies, RELGROUPHASH_PLAYER)
ENDPROC

//===============================================
//	Check if player has rammed into a vehicle
//===============================================
FUNC BOOL HAS_PLAYER_RAMMED_VEHICLE(VEHICLE_INDEX veh)
	VEHICLE_INDEX playerVeh = GET_PLAYERS_LAST_VEHICLE()

	IF IS_ENTITY_ALIVE(veh)
	AND IS_ENTITY_ALIVE(playerVeh)
	AND f_LastVehSpeed != 0
	AND f_LastVehSpeed >= 9
		FLOAT fCurrentSpeed = GET_ENTITY_SPEED(playerVeh)
		
		IF IS_ENTITY_TOUCHING_ENTITY(veh, playerVeh)
		AND ( fCurrentSpeed <= (f_LastVehSpeed * 0.5) )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Check if either truck has been messed with
//===============================================
FUNC BOOL HAVE_TRUCKS_BEEN_MESSED_WITH()
	// Avoided
	IF (IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),a_Peds[MPF_TRUCKER_1].ped) >= 200)
	AND (IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),a_Peds[MPF_TRUCKER_2].ped) >= 200)
		RETURN TRUE
	ENDIF

	// Killed
	IF ((IS_MISSION_BIT_FLAG_SET(MBF_TRUCK_1_SPAWNED) AND NOT IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped)))
	OR ((IS_MISSION_BIT_FLAG_SET(MBF_TRUCK_2_SPAWNED) AND NOT IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped)))
		RETURN TRUE
	ENDIF
	
	// Attacked
	IF (IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped) AND (IS_PED_IN_COMBAT(a_Peds[MPF_TRUCKER_1].ped, PLAYER_PED_ID()) OR NOT IS_PED_IN_ANY_VEHICLE(a_Peds[MPF_TRUCKER_1].ped)))
	OR (IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped) AND (IS_PED_IN_COMBAT(a_Peds[MPF_TRUCKER_2].ped, PLAYER_PED_ID()) OR NOT IS_PED_IN_ANY_VEHICLE(a_Peds[MPF_TRUCKER_2].ped)))
		RETURN TRUE
	ENDIF
	
	// Damage either trailer
	IF (IS_ENTITY_ALIVE(a_Vehicles[MVF_TRAILER_1].veh) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(a_Vehicles[MVF_TRAILER_1].veh, PLAYER_PED_ID()))
	OR (IS_ENTITY_ALIVE(a_Vehicles[MVF_TRAILER_2].veh) AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(a_Vehicles[MVF_TRAILER_2].veh, PLAYER_PED_ID()))
		RETURN TRUE
	ENDIF
	
	// Rammed into truck
	IF HAS_PLAYER_RAMMED_VEHICLE(a_Vehicles[MVF_TRUCK_1].veh)
	OR HAS_PLAYER_RAMMED_VEHICLE(a_Vehicles[MVF_TRUCK_2].veh)
		RETURN TRUE
	ENDIF
	
	// Update last speed variable
	VEHICLE_INDEX playerVeh = GET_PLAYERS_LAST_VEHICLE()
	IF IS_ENTITY_ALIVE(playerVeh)
		f_LastVehSpeed = GET_ENTITY_SPEED(playerVeh)
	ELSE
		f_LastVehSpeed = 0.0
	ENDIF
	
	// Check if player is trying to steal truck
	IF IS_PED_TRYING_TO_ENTER_A_LOCKED_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehTarget
		INT iVeh = GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID())
		vehTarget = INT_TO_NATIVE(VEHICLE_INDEX,iVeh)
		IF (vehTarget = a_Vehicles[MVF_TRUCK_1].veh AND IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped))
		OR (vehTarget = a_Vehicles[MVF_TRUCK_2].veh AND IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//================================================================================================================================================
//		DEBUG FUNCTIONS/PROCEDURES
//================================================================================================================================================
#IF IS_DEBUG_BUILD	
	//===============================================
	//	Create widgets
	//===============================================
	PROC CREATE_DEBUG_WIDGETS()
		widGroup = START_WIDGET_GROUP("RE - Duel")
			ADD_WIDGET_INT_READ_ONLY( "Current Event:", i_CurrentEvent )
		STOP_WIDGET_GROUP()
	ENDPROC
	
	//===============================================
	//	Debug update called every frame
	//===============================================
	PROC UPDATE_DEBUG()	
		// Debug pass/fail
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()
		ENDIF

		IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD5 )
		AND IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
		AND NOT IS_PED_SITTING_IN_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh )
			SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh )
			SET_VEHICLE_ENGINE_ON( a_Vehicles[MVF_DUEL].veh, TRUE, TRUE )
		ENDIF
	ENDPROC
	
	//===============================================
	//	Initialise debug components
	//===============================================
	PROC SETUP_DEBUG()
		CREATE_DEBUG_WIDGETS()
	ENDPROC
#ENDIF

//===============================================
//	Steal car stage
//===============================================
PROC DO_STAGE_STEAL_CAR()
	SWITCH e_StageSection
		CASE MSS_SETUP
			// Mission setup
			SETUP_MISSION()
			
			// Debug setup
			#IF IS_DEBUG_BUILD
				SETUP_DEBUG()
			#ENDIF
			
			SET_MISSION_BIT_FLAG(MBF_MISSION_SETUP)
		
			REQUEST_MODEL( e_ModelDuel )
		
			// Progress to active state
			e_StageSection = MSS_ACTIVE
		BREAK
		
		CASE MSS_ACTIVE
			SWITCH i_CurrentEvent
				CASE 0
					IF HAS_MODEL_LOADED( e_ModelDuel )
						SPAWN_MISSION_VEHICLE( MVF_DUEL, V_ZERO, -1, TRUE )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelDuel )
						
						REQUEST_MODEL( e_ModelTruck )
						REQUEST_MODEL( e_ModelHillBilly1 )
						REQUEST_MODEL( e_ModelTrailer )
						
						SET_RANDOM_EVENT_ACTIVE()
						
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 1
					IF HAS_MODEL_LOADED( e_ModelTruck )
					AND HAS_MODEL_LOADED( e_ModelHillBilly1 )
					AND HAS_MODEL_LOADED( e_ModelTrailer )
						SET_MISSION_BIT_FLAG( MBF_SPAWN_TRUCKS )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 2
					VECTOR vOffset
					
					IF IS_ENTITY_ALIVE( a_Vehicles[MVF_TRUCK_1].veh )
					AND IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_1].ped )
					AND IS_PLAYER_IN_DUEL_CAR()
						vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( a_Vehicles[MVF_TRUCK_1].veh, GET_ENTITY_COORDS(PLAYER_PED_ID()) )
						
						IF vOffset.X <= 0
						AND IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<953.505188,2665.587158,38.612366>>, <<955.094116,2716.872070,50.670456>>, 71.000000)
							IF IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_1].veh, <<0,10,0>>), 1.25 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_1].veh, <<0,12.5,0>>), 1.5 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_1].veh, <<0,15,0>>), 1.75 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_1].veh, <<0,17.5,0>>), 2 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_1].veh, <<0,20,0>>), 2.25 )
								TRIGGER_TRUCK_SEQUENCE( MVF_TRUCK_1, MPF_TRUCKER_1, MVF_TRUCK_2, MPF_TRUCKER_2 )
								i_CurrentEvent = 4
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCKS_TRIGGERED )
					AND IS_ENTITY_ALIVE( a_Vehicles[MVF_TRUCK_2].veh )
					AND IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_2].ped )
					AND IS_PLAYER_IN_DUEL_CAR()
						vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS( a_Vehicles[MVF_TRUCK_2].veh, GET_ENTITY_COORDS(PLAYER_PED_ID()) )
						
						IF vOffset.X >= 0
						AND IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<1110.712036,2695.670410,36.779930>>, <<1109.166016,2665.530762,48.503548>>, 71.000000)
							IF IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_2].veh, <<0,10,0>>), 1.5 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_2].veh, <<0,12.5,0>>), 1.75 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_2].veh, <<0,15,0>>), 2.0 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_2].veh, <<0,17.5,0>>), 2.25 )
							OR IS_PLAYER_GOING_TO_BE_AT( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_TRUCK_2].veh, <<0,20,0>>), 2.5 )
								TRIGGER_TRUCK_SEQUENCE( MVF_TRUCK_2, MPF_TRUCKER_2, MVF_TRUCK_1, MPF_TRUCKER_1 )
								i_CurrentEvent = 4
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCKS_TRIGGERED )
					AND HAVE_TRUCKS_BEEN_MESSED_WITH()
						SET_MISSION_BIT_FLAG( MBF_WARP_TRUCKS )
						IF IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[MPF_TRUCKER_1].ped, FALSE )
							TASK_COMBAT_PED( a_Peds[MPF_TRUCKER_1].ped, PLAYER_PED_ID() )
							a_Peds[MPF_TRUCKER_1].iEvent = 3
						ENDIF
						
						IF IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[MPF_TRUCKER_2].ped, FALSE )
							TASK_COMBAT_PED( a_Peds[MPF_TRUCKER_2].ped, PLAYER_PED_ID() )
							a_Peds[MPF_TRUCKER_2].iEvent = 3
						ENDIF
						
						IF IS_ENTITY_ALIVE(a_Vehicles[MVF_TRUCK_1].veh)
							SET_VEHICLE_DOORS_LOCKED(a_Vehicles[MVF_TRUCK_1].veh, VEHICLELOCK_UNLOCKED)
						ENDIF
						IF IS_ENTITY_ALIVE(a_Vehicles[MVF_TRUCK_2].veh)
							SET_VEHICLE_DOORS_LOCKED(a_Vehicles[MVF_TRUCK_2].veh, VEHICLELOCK_UNLOCKED)
						ENDIF
						
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 3
					IF ( (NOT IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_1].ped)) OR (DOES_BLIP_EXIST(a_Peds[MPF_TRUCKER_1].blip)) )
					AND ( (NOT IS_ENTITY_ALIVE(a_Peds[MPF_TRUCKER_2].ped)) OR (DOES_BLIP_EXIST(a_Peds[MPF_TRUCKER_2].blip)) )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 4
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemies, RELGROUPHASH_PLAYER)
					IF IS_ENTITY_ALIVE(a_Vehicles[MVF_TRUCK_1].veh)
						SET_VEHICLE_DOORS_LOCKED(a_Vehicles[MVF_TRUCK_1].veh, VEHICLELOCK_UNLOCKED)
					ENDIF
					IF IS_ENTITY_ALIVE(a_Vehicles[MVF_TRUCK_2].veh)
						SET_VEHICLE_DOORS_LOCKED(a_Vehicles[MVF_TRUCK_2].veh, VEHICLELOCK_UNLOCKED)
					ENDIF
					REQUEST_MODEL( e_ModelSUV )
					SETTIMERA(0)
					i_CurrentEvent++
				BREAK
				CASE 5
					IF HAS_MODEL_LOADED( e_ModelSUV )
						REQUEST_MODEL( e_ModelHillBilly1 )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 6
					IF HAS_MODEL_LOADED( e_ModelHillBilly1 )
						REQUEST_MODEL( e_ModelHillbilly2 )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 7
					// Create enemies once loaded
					IF HAS_MODEL_LOADED( e_ModelSUV )
					AND HAS_MODEL_LOADED( e_ModelHillbilly1 )
					AND HAS_MODEL_LOADED( e_ModelHillbilly2 )
						SPAWN_MISSION_VEHICLE( MVF_SUV_1, V_ZERO )
						SPAWN_MISSION_VEHICLE( MVF_SUV_2, V_ZERO )
						SPAWN_MISSION_VEHICLE( MVF_SUV_3, V_ZERO )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelSUV )
						
						SPAWN_MISSION_PED( MPF_HILLBILLY_1, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_HILLBILLY_2, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_HILLBILLY_3, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_HILLBILLY_4, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_HILLBILLY_5, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_HILLBILLY_6, FALSE, V_ZERO )
						IF IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_1_SPAWNED )
						AND IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_2_SPAWNED )
							SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelHillBilly1 )
						ENDIF
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelHillbilly2 )
						
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 8
					IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_1].ped )
					AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_2].ped )
						IF TIMERA() < 16000
							SETTIMERA(16000)
						ENDIF
					ENDIF
				
					IF TIMERA() > 20000
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_CarStart) >= 150
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 9
					IF IS_PLAYER_IN_DUEL_CAR()
					OR ( IS_ENTITY_ALIVE(a_Vehicles[MVF_DUEL].veh) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh) <= 35 )
						// Warp the cars
						VECTOR vPlayer
						VECTOR vCarWarp1
						VECTOR vCarWarp2
						VECTOR vCarWarp3
						INT iRandomNode
						FLOAT fDesiredHeading
						vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
						iRandomNode = GET_RANDOM_INT_IN_RANGE(6, 10)
						
						IF GET_NTH_CLOSEST_VEHICLE_NODE( vPlayer, iRandomNode, vCarWarp1, DEFAULT, 5.0 )

							INT iDensity
							INT iProperties
							
							GET_VEHICLE_NODE_PROPERTIES( vCarWarp1, iDensity, iProperties )
						
							// Don't allow nodes that block big vehicles (we're spawning three cars here!)
							IF ((iProperties & ENUM_TO_INT(VNP_NO_BIG_VEHICLES)) = 0)
							AND ( (NOT IS_PLAYER_NEARBY_NODE_ON()) OR ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) = 0) )
							AND ( IS_PLAYER_OFF_ROAD() OR ((iProperties & ENUM_TO_INT(VNP_OFF_ROAD)) = 0) )
							AND NOT IS_MISSION_VEHICLE_IN_AREA(vCarWarp1)
								fDesiredHeading = GET_HEADING_BETWEEN_VECTORS_2D( vCarWarp1, GET_ENTITY_COORDS(PLAYER_PED_ID()) )
								vCarWarp2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( vCarWarp1, fDesiredHeading, <<6, -6, 0>> )
								vCarWarp3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS( vCarWarp1, fDesiredHeading, <<-6, -6, 0>> )
								
								IF NOT IS_SPHERE_VISIBLE( vCarWarp1, 7.0 )
								AND NOT IS_SPHERE_VISIBLE( vCarWarp2, 7.0 )
								AND NOT IS_SPHERE_VISIBLE( vCarWarp3, 7.0 )
								AND NOT IS_MISSION_VEHICLE_IN_AREA(vCarWarp2)
								AND NOT IS_MISSION_VEHICLE_IN_AREA(vCarWarp3)
									SETUP_CHASE_CAR( a_Vehicles[MVF_SUV_1].veh, vCarWarp1 )
									SETUP_CHASE_CAR( a_Vehicles[MVF_SUV_2].veh, vCarWarp2 )
									SETUP_CHASE_CAR( a_Vehicles[MVF_SUV_3].veh, vCarWarp3 )
									i_GlobalWarpTimer = GET_GAME_TIMER()
									
									INT i
									FOR i = ENUM_TO_INT(MPF_HILLBILLY_1) TO ENUM_TO_INT(MPF_HILLBILLY_6)
										IF IS_ENTITY_ALIVE(a_Peds[i].ped)
											a_Peds[i].blip = CREATE_BLIP_FOR_PED( a_Peds[i].ped, TRUE )
											
											IF IS_PED_IN_ANY_VEHICLE( a_Peds[i].ped )
												IF GET_SEAT_PED_IS_IN(a_Peds[i].ped) = VS_DRIVER
													SET_BLIP_SCALE( a_Peds[i].blip, BLIP_SIZE_VEHICLE )
												ELSE
													// Hide blip if they're not the driver
													SET_BLIP_ALPHA( a_Peds[i].blip, 0 )
													SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, TRUE )
												ENDIF
											ENDIF
										ENDIF
									ENDFOR
									
									SET_MISSION_BIT_FLAG( MBF_CAR_CHASE )
									
									SET_MISSION_BIT_FLAG( MBF_WARP_SUVS )
									i_WarpSUVTimer = GET_GAME_TIMER() + 60000
									
									CLEAR_MISSION_BIT_FLAG( MBF_WARP_TRUCKS )
									
									i_CurrentEvent++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 10
					IF IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_1].ped )
					OR IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_2].ped )
					OR IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_3].ped )
					OR IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_4].ped )
					OR IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_5].ped )
					OR IS_ENTITY_ALIVE( a_Peds[MPF_HILLBILLY_6].ped )
						IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_SUVS )
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_1].veh )
								WARP_CHASE_CAR( MVF_SUV_1, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), i_Car1WarpTimer )
							ENDIF
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_2].veh )
								WARP_CHASE_CAR( MVF_SUV_2, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), i_Car2WarpTimer )
							ENDIF
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_SUV_3].veh )
								WARP_CHASE_CAR( MVF_SUV_3, GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), i_Car3WarpTimer )
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_1].ped )
						AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_TRUCKER_2].ped )
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
								e_StageSection = MSS_CLEANUP
							ELSE
								MISSION_FAILED()
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
		BREAK
		
		CASE MSS_CLEANUP
			// Move to next stage
			ADVANCE_STAGE( MSF_MISSION_PASSED )
		BREAK
	ENDSWITCH
ENDPROC

//===============================================
//	Mission passed stage
//===============================================
PROC DO_STAGE_MISSION_PASSED()
	SWITCH e_StageSection
		CASE MSS_SETUP
			// Progress to active state
			e_StageSection = MSS_ACTIVE
		BREAK
		
		CASE MSS_ACTIVE
			SWITCH i_CurrentEvent
				CASE 0
					e_StageSection = MSS_CLEANUP
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MSS_CLEANUP
			// Passed
			MISSION_PASSED()
		BREAK
	ENDSWITCH
ENDPROC

//===============================================
//	Make sure peds are dead or not blipped before failing
//===============================================
FUNC BOOL ARE_ALL_ENEMIES_DEAD_OR_NOT_BLIPPED()
	BOOL bReturn
	bReturn = TRUE
	
	INT i
	
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		IF IS_ENTITY_ALIVE(a_Peds[i].ped)
		AND DOES_BLIP_EXIST(a_Peds[i].blip)
			bReturn = FALSE
			i = COUNT_OF(MISSION_PED_FLAGS)
		ENDIF
	ENDREPEAT
	
	RETURN bReturn
ENDFUNC

//===============================================
//	Check for fail
//===============================================
PROC FAIL_CHECKS()
	// Abandoning the car
	IF (IS_MISSION_BIT_FLAG_SET( MBF_CAR_STOLEN ) OR i_CurrentEvent > 2)
	AND ARE_ALL_ENEMIES_DEAD_OR_NOT_BLIPPED()
		IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
		AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh ) >= 250
			MISSION_FAILED()
		ENDIF
	ENDIF
	
	// Destroying the car before the chase
	IF ARE_ALL_ENEMIES_DEAD_OR_NOT_BLIPPED()
		IF DOES_ENTITY_EXIST( a_Vehicles[MVF_DUEL].veh )
		AND NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
			MISSION_FAILED()
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Main update function called every frame
//===============================================
PROC UPDATE_MAIN()
	IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
			MISSION_CLEANUP( TRUE, FALSE )
		ENDIF
	ENDIF
	
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_DUEL")
	
	// Check for fail
	FAIL_CHECKS()

	// Update peds, vehicles, objects & blips
	UPDATE_ALL_PEDS()
	UPDATE_ALL_VEHICLES()
	UPDATE_ALL_BLIPS()
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_DO_HORN )
	AND IS_ENTITY_ALIVE(a_Vehicles[e_HornTruck].veh)
	AND GET_GAME_TIMER() < i_HornTime
		SET_HORN_PERMANENTLY_ON( a_Vehicles[e_HornTruck].veh )
	ENDIF
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_TRUCKS )
		IF i_WarpTruckTimer = 0
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), v_CarStart ) >= 150
				i_WarpTruckTimer = GET_GAME_TIMER() + 30000
			ENDIF
		ELSE
			IF GET_GAME_TIMER() >= i_WarpTruckTimer
				CLEAR_MISSION_BIT_FLAG( MBF_WARP_TRUCKS )
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_WARP_SUVS )
		IF GET_GAME_TIMER() >= i_WarpSUVTimer
			CLEAR_MISSION_BIT_FLAG( MBF_WARP_SUVS )
		ENDIF
	ENDIF
	
	IF IS_MISSION_BIT_FLAG_SET( MBF_SPAWN_TRUCKS )
	AND IS_ENTITY_ALIVE(a_Vehicles[MVF_DUEL].veh) 
	AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), a_Vehicles[MVF_DUEL].veh) <= 35
		
		IF NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_1_SPAWNED )
			IF NOT IS_SPHERE_VISIBLE( v_Truck1Spawn, 7.0 )
				IF GET_GAME_TIMER() - i_HiddenTimerA >= 1000
				AND NOT IS_MISSION_VEHICLE_IN_AREA( v_Truck1Spawn, 7.0 )
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), v_Truck1Spawn ) >= 50
					CLEAR_AREA( v_Truck1Spawn, 7.0, TRUE )
					SPAWN_MISSION_VEHICLE( MVF_TRUCK_1, V_ZERO )
					SPAWN_MISSION_VEHICLE( MVF_TRAILER_1, V_ZERO )
					ATTACH_VEHICLE_TO_TRAILER( a_Vehicles[MVF_TRUCK_1].veh, a_Vehicles[MVF_TRAILER_1].veh )
					SPAWN_MISSION_PED( MPF_TRUCKER_1, FALSE, V_ZERO )
					SET_MISSION_BIT_FLAG( MBF_TRUCK_1_SPAWNED )
					
					IF i_CurrentEvent > 2
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[MPF_TRUCKER_1].ped, FALSE )
						TASK_COMBAT_PED( a_Peds[MPF_TRUCKER_1].ped, PLAYER_PED_ID() )
						a_Peds[MPF_TRUCKER_1].iEvent = 3
					ENDIF
				ENDIF
			ELSE
				i_HiddenTimerA = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF NOT IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_2_SPAWNED )
			IF NOT IS_SPHERE_VISIBLE( v_Truck2Spawn, 7.0 )
				IF GET_GAME_TIMER() - i_HiddenTimerB >= 1000
				AND NOT IS_MISSION_VEHICLE_IN_AREA( v_Truck2Spawn, 7.0 )
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), v_Truck2Spawn ) >= 50
					CLEAR_AREA( v_Truck2Spawn, 7.0, TRUE )
					SPAWN_MISSION_VEHICLE( MVF_TRUCK_2, V_ZERO )
					SPAWN_MISSION_VEHICLE( MVF_TRAILER_2, V_ZERO )
					ATTACH_VEHICLE_TO_TRAILER( a_Vehicles[MVF_TRUCK_2].veh, a_Vehicles[MVF_TRAILER_2].veh )
					SPAWN_MISSION_PED( MPF_TRUCKER_2, FALSE, V_ZERO )
					SET_MISSION_BIT_FLAG( MBF_TRUCK_2_SPAWNED )
					
					IF i_CurrentEvent > 2
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[MPF_TRUCKER_2].ped, FALSE )
						TASK_COMBAT_PED( a_Peds[MPF_TRUCKER_2].ped, PLAYER_PED_ID() )
						a_Peds[MPF_TRUCKER_2].iEvent = 3
					ENDIF
				ENDIF
			ELSE
				i_HiddenTimerB = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		IF IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_1_SPAWNED )
		AND IS_MISSION_BIT_FLAG_SET( MBF_TRUCK_2_SPAWNED )
			CLEAR_MISSION_BIT_FLAG( MBF_SPAWN_TRUCKS )
			SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelTruck )
			SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelTrailer )
			SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelHillBilly1 )
		ENDIF
	ENDIF

	IF IS_MISSION_BIT_FLAG_SET( MBF_TRUCKS_TRIGGERED )
		IF i_TruckTriggerTimer = 0
			i_TruckTriggerTimer = GET_GAME_TIMER() + 3500
		ELSE
			IF GET_GAME_TIMER() >= i_TruckTriggerTimer
				CLEAR_MISSION_BIT_FLAG( MBF_TRUCKS_TRIGGERED )
				SET_MISSION_BIT_FLAG( MBF_WARP_TRUCKS )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//================================================================================================================================================
//		MAIN SCRIPT LOOP
//================================================================================================================================================
SCRIPT(coords_struct in_coords)
	v_Input = in_coords.vec_coord[0]

	// Handle force cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		// Lock doors of the Dukes
		IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
			SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_DUEL].veh, VEHICLELOCK_LOCKED )
		ENDIF
		MISSION_CLEANUP(TRUE,FALSE)
	ENDIF
	
	IF CAN_RANDOM_EVENT_LAUNCH(v_Input, RE_DUEL)
		LAUNCH_RANDOM_EVENT(RE_DUEL)
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
    WHILE TRUE
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR IS_MISSION_BIT_FLAG_SET( MBF_CAR_STOLEN )
		OR i_CurrentEvent > 2
			// Updates required regardless of stage
			IF IS_MISSION_BIT_FLAG_SET( MBF_MISSION_SETUP )
				UPDATE_MAIN()
			ENDIF

			// Update flow
			SWITCH e_MissionStage
				CASE MSF_STEAL_CAR
					DO_STAGE_STEAL_CAR()
				BREAK
				CASE MSF_MISSION_PASSED
					DO_STAGE_MISSION_PASSED()
				BREAK
			ENDSWITCH
		ELSE
			// Lock doors of the Dukes
			IF IS_ENTITY_ALIVE( a_Vehicles[MVF_DUEL].veh )
				SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_DUEL].veh, VEHICLELOCK_LOCKED )
			ENDIF
			MISSION_CLEANUP( TRUE, FALSE )
		ENDIF

		// Debug updates
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG()
		#ENDIF
		
		WAIT(0)
    ENDWHILE
ENDSCRIPT
