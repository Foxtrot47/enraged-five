///
///    100% UFO EASTER EGG
///    
///    STEVE R LDS.


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_clock.sch"
USING "commands_streaming.sch"
USING "building_control_public.sch"

#IF IS_DEBUG_BUILD
	BOOL bUFODebugSpam = FALSE
#ENDIF

ENUM eUFO_STATE

	UFO_STATE_WAITING_TO_START = 0,
	UFO_STATE_SPAWNING_UFO,
	UFO_STATE_MONITORING_UFO

ENDENUM

eUFO_STATE eUFOState = UFO_STATE_WAITING_TO_START


PROC missionCleanup()

	SET_BUILDING_STATE(BUILDINGNAME_IPL_UFO_EYE, BUILDINGSTATE_NORMAL, TRUE)
	
	IF IS_AMBIENT_ZONE_ENABLED("AZ_SPECIAL_UFO_03")
		SET_AMBIENT_ZONE_STATE("AZ_SPECIAL_UFO_03",FALSE,TRUE)
	ENDIF

	CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " script cleaning up.")
	TERMINATE_THIS_THREAD()
	
ENDPROC


/// PURPOSE:
///    Checks if the weather is right for the UFO spawn
/// RETURNS:
///    
FUNC BOOL IS_UFO_WEATHER_CORRECT()

	IF IS_NEXT_WEATHER_TYPE("RAIN")
	OR IS_NEXT_WEATHER_TYPE("THUNDER")
	OR IS_PREV_WEATHER_TYPE("RAIN")
	OR IS_PREV_WEATHER_TYPE("THUNDER")
		#IF IS_DEBUG_BUILD
			IF bUFODebugSpam
				CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " weather is correct")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bUFODebugSpam
			CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " weather isn't correct")
		ENDIF
	#ENDIF
	
	RETURN FALSE

ENDFUNC




SCRIPT (coords_struct in_coords)

#IF IS_DEBUG_BUILD
	PRINTNL()
	PRINTLN("100% UFO")
	PRINTVECTOR(in_coords.vec_coord[0])
	PRINTNL()
#ENDIF

#IF IS_FINAL_BUILD
UNUSED_PARAMETER(in_coords)
#ENDIF

	IF g_savedGlobals.sCompletionPercentageData.b_g_OneHundredPercentReached = FALSE
		CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - 100% completion hasn't been reached - terminating script")
		missionCleanup()
	ENDIF

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
	//AND NOT bReplay
		 missionCleanup()
	ENDIF

	WHILE TRUE
		WAIT(0)
		
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " is not within world point brain activation range.")
			missionCleanup()
		ENDIF
		
		SWITCH eUFOState
		
			CASE UFO_STATE_WAITING_TO_START
			
				#IF IS_DEBUG_BUILD
					IF bUFODebugSpam
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " is waiting for rainy/thundery weather at 3AM")
					ENDIF
				#ENDIF
			
				// Only activate UFO at 3AM in Rainy weather
				IF GET_CLOCK_HOURS() = 3
				AND IS_UFO_WEATHER_CORRECT()
					eUFOState = UFO_STATE_SPAWNING_UFO
				ENDIF
				
			BREAK
			
			
			CASE UFO_STATE_SPAWNING_UFO
			
				#IF IS_DEBUG_BUILD
					IF bUFODebugSpam
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Waiting for ufo to spawn")
					ENDIF
				#ENDIF
				
				SET_BUILDING_STATE(BUILDINGNAME_IPL_UFO_EYE, BUILDINGSTATE_DESTROYED)
				
				eUFOState = UFO_STATE_MONITORING_UFO
	
				IF NOT IS_AMBIENT_ZONE_ENABLED("AZ_SPECIAL_UFO_03")
					SET_AMBIENT_ZONE_STATE("AZ_SPECIAL_UFO_03",TRUE,TRUE)
				ENDIF
				
			BREAK
			
			CASE UFO_STATE_MONITORING_UFO
			
				#IF IS_DEBUG_BUILD
					IF bUFODebugSpam
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Monitoring UFO spawn conditions")
					ENDIF
				#ENDIF
				
				IF GET_CLOCK_HOURS() != 3
				OR NOT IS_UFO_WEATHER_CORRECT()
					missionCleanup()
				ENDIF
				
			BREAK
			
			ENDSWITCH
		
	ENDWHILE

ENDSCRIPT
