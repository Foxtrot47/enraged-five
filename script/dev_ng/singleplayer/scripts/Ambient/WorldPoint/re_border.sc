//World Point template

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_path.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "script_blips.sch"
USING "script_PLAYER.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "AggroSupport.sch"
USING "finance_control_public.sch"
USING "context_control_public.sch"
USING "commands_debug.sch"
USING "flow_public_core_override.sch"
USING "random_events_public.sch"

USING "script_maths.sch"
USING "commands_cutscene.sch"
USING "Ambient_Common.sch"

//Steve T added this.
USING "dialogue_public.sch"
USING "cutscene_public.sch"
USING "blip_control_public.sch"
USING "completionpercentage_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
    ambCanRun,
    ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

VECTOR vInput
//	FLOAT fInput
VECTOR vAmbWorldPoint1 = << 928.0903, 1733.12, 165.1067 >>
VECTOR vAmbWorldPoint2 = << 2374.21, 316.6781, 180.4001 >>
VECTOR vAmbWorldPoint3 = << -804.8174, 4051.37, 159.6384 >>

CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
CONST_INT iWorldPoint3	3
CONST_INT LINES_TO_PLAY 3
INT iSelectedVariation

CONST_INT TOTAL_ENEMIES 3

BOOL bDoFullCleanUp
BOOL bSpeakAttackLine
BOOL bSpokeFirstLine
BOOL bIdentifiedAsAlien
BOOL bPlayerAttackedPatrolMember = FALSE
BOOL bPlayerDialogue
BOOL bPatrolInAnyVehicleIdentified[TOTAL_ENEMIES]
BOOL bPatrolOnFootIdentified[TOTAL_ENEMIES]
BOOL bPatrolInAnyVehicle[TOTAL_ENEMIES]
BOOL bPatrolOnFoot[TOTAL_ENEMIES]

INT iLineCounter
INT iAliveCounter
PED_INDEX pedPatrol[TOTAL_ENEMIES]
VEHICLE_INDEX vehBike[TOTAL_ENEMIES]
//AI_BLIP_STRUCT pedPatrolBlips[TOTAL_ENEMIES]
BLIP_INDEX blipPatrol[TOTAL_ENEMIES]

//SEQUENCE_INDEX seq

REL_GROUP_HASH relGroupBaddies

STRING playerVoice
structPedsForConversation dialogueStruct


// Functions ----------------------------------------------//

FUNC BOOL LOADED_AND_CREATED_BORDER_PATROL()
	VECTOR vGen
	FLOAT fGen
	INT index
	INT iRandomVehicle
	MODEL_NAMES modelBike = SANCHEZ
	MODEL_NAMES modelQuad = BLAZER
    MODEL_NAMES modelBorderPatrol = A_M_M_HILLBILLY_02
    REQUEST_MODEL(modelBorderPatrol)
	REQUEST_MODEL(modelBike)
	REQUEST_MODEL(modelQuad)
    IF HAS_MODEL_LOADED(modelBorderPatrol)
	AND HAS_MODEL_LOADED(modelBike)
	AND HAS_MODEL_LOADED(modelQuad)
		IF IS_VEHICLE_DRIVEABLE(vehBike[0])
		AND IS_VEHICLE_DRIVEABLE(vehBike[1])
		AND IS_VEHICLE_DRIVEABLE(vehBike[2])
		
			RETURN TRUE
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				GET_NTH_CLOSEST_VEHICLE_NODE_FAVOUR_DIRECTION(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0, -25, 0 >>), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 1, vGen, fGen)
				IF NOT IS_SPHERE_VISIBLE(vGen, 5)
					
		//			LOAD_ALL_PATH_NODES(TRUE)
					ADD_RELATIONSHIP_GROUP("re_border badGuys", relGroupBaddies)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relGroupBaddies, RELGROUPHASH_PLAYER)
					
					REPEAT TOTAL_ENEMIES index
						vGen.x += TO_FLOAT(index)
						vGen.y += TO_FLOAT(index)
						iRandomVehicle = GET_RANDOM_INT_IN_RANGE(0, 5)
						IF iRandomVehicle > 1
							vehBike[index] = CREATE_VEHICLE(modelBike, vGen, fGen)
						ELSE
							vehBike[index] = CREATE_VEHICLE(modelQuad, vGen, fGen)
						ENDIF
						pedPatrol[index] = CREATE_PED_INSIDE_VEHICLE(vehBike[index], PEDTYPE_CRIMINAL, modelBorderPatrol)
	//					MISSION_ESCORT_LEFT,            // 10
	//				    MISSION_ESCORT_RIGHT,           // 11
	//				    MISSION_ESCORT_REAR,            // 12
//						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//							TASK_VEHICLE_MISSION(pedPatrol[index], vehBike[index], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), V INT_TO_ENUM(VEHICLE_MISSION, i+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 60, DRIVINGMODE_AVOIDCARS, 3, 10)
//							TASK_VEHICLE_CHASE(pedPatrol[index], PLAYER_PED_ID())
//							SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_USE_VEHICLE_ATTACK, FALSE)
//							bPatrolInAnyVehicle[index] = TRUE
//						ELSE
//							TASK_VEHICLE_MISSION_PED_TARGET(pedPatrol[index], vehBike[index], PLAYER_PED_ID(), INT_TO_ENUM(VEHICLE_MISSION, i+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 60, DRIVINGMODE_AVOIDCARS, 5, 10)
//							SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_USE_VEHICLE_ATTACK, TRUE)
//						ENDIF
						TASK_LOOK_AT_ENTITY(pedPatrol[index], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
						SET_PED_KEEP_TASK(pedPatrol[index], TRUE)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPatrol[index], TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedPatrol[index], relGroupBaddies)
						
						GIVE_WEAPON_TO_PED(pedPatrol[index], WEAPONTYPE_SAWNOFFSHOTGUN, INFINITE_AMMO, TRUE)
						SET_VEHICLE_FORWARD_SPEED(vehBike[index], GET_ENTITY_SPEED(PLAYER_PED_ID()))
						
						SET_PED_COMBAT_MOVEMENT(pedPatrol[index], CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_CAN_CHARGE, TRUE)
						
						SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_DO_DRIVEBYS, TRUE)
//						SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_LEAVE_VEHICLES, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_USE_VEHICLE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_USE_VEHICLE_ATTACK, TRUE)
						SET_PED_ACCURACY(pedPatrol[index], 5)
						SET_PED_FIRING_PATTERN(pedPatrol[index], FIRING_PATTERN_SINGLE_SHOT)
						SET_PED_SHOOT_RATE(pedPatrol[index], GET_RANDOM_INT_IN_RANGE(50, 100))
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedPatrol[index], KNOCKOFFVEHICLE_NEVER)
						SET_PED_CONFIG_FLAG(pedPatrol[index], PCF_DontInfluenceWantedLevel, TRUE)
						SET_PED_HELMET(pedPatrol[index], FALSE)
						SET_PED_MONEY(pedPatrol[index], 233)
						
						SET_AMBIENT_VOICE_NAME(pedPatrol[index], "A_M_M_HILLBILLY_02_WHITE_MINI_04")
						STOP_PED_SPEAKING(pedPatrol[index], TRUE)
					ENDREPEAT
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[0], 0)//GET_RANDOM_INT_IN_RANGE(0, 3))
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[1], 1)//GET_RANDOM_INT_IN_RANGE(4, 7))
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)//GET_RANDOM_INT_IN_RANGE(8, 10))
				ENDIF
			ENDIF
	        
	        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
	            CASE CHAR_MICHAEL  //0
	                playerVoice = "MICHAEL"
	            BREAK
	            
	            CASE CHAR_FRANKLIN //1
	                playerVoice = "FRANKLIN"
	            BREAK
	            
	            CASE CHAR_TREVOR   //2
	                playerVoice = "TREVOR"
	            BREAK
	        ENDSWITCH
	        SET_WANTED_LEVEL_MULTIPLIER(0)
	        ADD_PED_FOR_DIALOGUE(dialogueStruct, 0, PLAYER_PED_ID(), playerVoice)
	        ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedPatrol[0], "BorderPatrol")
			//REMOVE_PED_FOR_DIALOGUE()
	        
	    ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_ANY_INAPPROPRIATE_VEHICLE()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), AMBULANCE)
				OR IS_PED_IN_MODEL(PLAYER_PED_ID(), FIRETRUK)
				OR IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
	//			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), ANNIHILATOR)
	//			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), BUZZARD)
	//			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), LAZER)
				OR IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
				OR g_bPlayerIsInTaxi = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC updateBadGuyBlips()
	INT index
	REPEAT TOTAL_ENEMIES index
		IF NOT IS_PED_INJURED(pedPatrol[index])
			IF bSpokeFirstLine
			OR bSpeakAttackLine
				IF NOT DOES_BLIP_EXIST(blipPatrol[index])
					blipPatrol[index] = CREATE_BLIP_FOR_PED(pedPatrol[index], TRUE)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipPatrol[index])
				REMOVE_BLIP(blipPatrol[index])
			ENDIF
		ENDIF
	ENDREPEAT
//	UPDATE_AI_PED_BLIP(pedPatrol[0], pedPatrolBlips[0])
//	UPDATE_AI_PED_BLIP(pedPatrol[1], pedPatrolBlips[1])
//	UPDATE_AI_PED_BLIP(pedPatrol[2], pedPatrolBlips[2])
ENDPROC

PROC updateBadGuyActions()
	INT index
	REPEAT TOTAL_ENEMIES index
		IF NOT IS_PED_INJURED(pedPatrol[index])
//		AND IS_VEHICLE_DRIVEABLE(vehBike[index])
			//1988807 - Put into combat as soon as the player attacks them.
			IF NOT bPlayerAttackedPatrolMember
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPatrol[index], PLAYER_PED_ID())
				OR IS_PED_SHOOTING(PLAYER_PED_ID())
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedPatrol[index])
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedPatrol[index])
					bSpeakAttackLine = TRUE
					bPlayerAttackedPatrolMember = TRUE
					bIdentifiedAsAlien = TRUE
					iLineCounter = LINES_TO_PLAY
				ENDIF
			ENDIF

//			IF GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
//			OR GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_MISSION) != WAITING_TO_START_TASK
			IF bIdentifiedAsAlien
			OR bPlayerAttackedPatrolMember
				IF IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT bPatrolInAnyVehicleIdentified[index]
//						IF GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_CHASE) != PERFORMING_TASK
//						AND GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_CHASE) != WAITING_TO_START_TASK
//							CLEAR_PED_TASKS(pedPatrol[index])
							IF NOT IS_ENTITY_DEAD(vehBike[index])
								TASK_VEHICLE_MISSION(pedPatrol[index], vehBike[index], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), INT_TO_ENUM(VEHICLE_MISSION, index+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 30, DRIVINGMODE_AVOIDCARS, 10, 5)
							ENDIF
//							TASK_VEHICLE_CHASE(pedPatrol[index], PLAYER_PED_ID())
							IF NOT bPlayerAttackedPatrolMember
								WAIT(GET_RANDOM_INT_IN_RANGE(400, 800))
							ELSE
								WAIT(0)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedPatrol[index])
								IF IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
									TASK_DRIVE_BY(pedPatrol[index], PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 1000, 60, TRUE)
	//								SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(pedPatrol[index], VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE, TRUE)
									SET_PED_KEEP_TASK(pedPatrol[index], TRUE)
								ENDIF
							ENDIF
							bPatrolOnFootIdentified[index] = FALSE
							bPatrolInAnyVehicleIdentified[index] = TRUE
//						ENDIF
					ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT bPatrolOnFootIdentified[index]
						IF NOT IS_PED_INJURED(pedPatrol[index])
						AND NOT IS_ENTITY_DEAD(vehBike[index])
							TASK_VEHICLE_MISSION_PED_TARGET(pedPatrol[index], vehBike[index], PLAYER_PED_ID(), INT_TO_ENUM(VEHICLE_MISSION, index+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 30, DRIVINGMODE_AVOIDCARS, 10, 5)
							
							IF NOT bPlayerAttackedPatrolMember
								WAIT(GET_RANDOM_INT_IN_RANGE(400, 800))
							ELSE
								WAIT(0)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedPatrol[index])
								IF IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
									TASK_DRIVE_BY(pedPatrol[index], PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 1000, 60, TRUE)
	//								SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(pedPatrol[index], VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE, TRUE)
									SET_PED_KEEP_TASK(pedPatrol[index], TRUE)
								ENDIF
							ENDIF
							bPatrolInAnyVehicleIdentified[index] = FALSE
							bPatrolOnFootIdentified[index] = TRUE
						ENDIF
					ENDIF
				ELIF NOT IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
//				AND (bPatrolInAnyVehicleIdentified[index] OR bPatrolOnFootIdentified[index])
//					CLEAR_PED_TASKS(pedPatrol[index])
					TASK_COMBAT_PED(pedPatrol[index], PLAYER_PED_ID())
					SET_PED_KEEP_TASK(pedPatrol[index], TRUE)
//					bPatrolInAnyVehicleIdentified[index] = FALSE
//					bPatrolOnFootIdentified[index] = FALSE
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT bPatrolInAnyVehicle[index]
						IF GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_CHASE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(pedPatrol[index], SCRIPT_TASK_VEHICLE_CHASE) != WAITING_TO_START_TASK
							CLEAR_PED_TASKS(pedPatrol[index])
							IF NOT IS_ENTITY_DEAD(vehBike[index])
								TASK_VEHICLE_MISSION(pedPatrol[index], vehBike[index], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), INT_TO_ENUM(VEHICLE_MISSION, index+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 30, DRIVINGMODE_AVOIDCARS, 10, 5)
								TASK_LOOK_AT_ENTITY(pedPatrol[index], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
							ENDIF
//							TASK_VEHICLE_CHASE(pedPatrol[index], PLAYER_PED_ID())
//							WAIT(1)
//							IF NOT IS_PED_INJURED(pedPatrol[index])
//								SET_TASK_VEHICLE_CHASE_BEHAVIOR_FLAG(pedPatrol[index], VEHICLE_CHASE_CANT_MAKE_AGGRESSIVE_MOVE, TRUE)
//							ENDIF
							bPatrolOnFoot[index] = FALSE
							bPatrolInAnyVehicle[index] = TRUE
						ENDIF
					ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT bPatrolOnFoot[index]
						IF NOT IS_PED_INJURED(pedPatrol[index])
						AND NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(vehBike[index])
							CLEAR_PED_TASKS(pedPatrol[index])
							TASK_VEHICLE_MISSION_PED_TARGET(pedPatrol[index], vehBike[index], PLAYER_PED_ID(), INT_TO_ENUM(VEHICLE_MISSION, index+ENUM_TO_INT(MISSION_ESCORT_LEFT)), 30, DRIVINGMODE_AVOIDCARS, 10, 5)
							TASK_LOOK_AT_ENTITY(pedPatrol[index], PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
							bPatrolInAnyVehicle[index] = FALSE
							bPatrolOnFoot[index] = TRUE
						ENDIF
					ENDIF
				ELIF NOT IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
//				AND (bPatrolInAnyVehicle[index] OR bPatrolOnFoot[index])
//					CLEAR_PED_TASKS(pedPatrol[index])
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedPatrol[index], PLAYER_PED_ID(), << 1, 0, 0 >>, PEDMOVEBLENDRATIO_RUN)
//					bPatrolInAnyVehicle[index] = FALSE
//					bPatrolOnFoot[index] = FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedPatrol[index])
				IF IS_ENTITY_IN_WATER(pedPatrol[index])
					IF IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
						TASK_LEAVE_ANY_VEHICLE(pedPatrol[index])
					ENDIF
					IF NOT IS_PED_IN_COMBAT(pedPatrol[index])
						TASK_COMBAT_PED(pedPatrol[index], PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(pedPatrol[index])
				IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 13
//					PRINTLN("@@@@@@@@@@ GET_ENTITY_SPEED(PLAYER_PED_ID()) > 13 @@@@@@@@@@")
					IF IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_VEHICLE_PED_IS_IN(pedPatrol[index]))
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedPatrol[index], KNOCKOFFVEHICLE_HARD)
//						PRINTLN("@@@@@@@@@@ KNOCKOFFVEHICLE_HARD @@@@@@@@@@")
					ELSE
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedPatrol[index], KNOCKOFFVEHICLE_NEVER)
	//					PRINTLN("@@@@@@@@@@ KNOCKOFFVEHICLE_NEVER @@@@@@@@@@")
					ENDIF
				ELSE
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedPatrol[index], KNOCKOFFVEHICLE_NEVER)
	//				PRINTLN("@@@@@@@@@@ KNOCKOFFVEHICLE_NEVER @@@@@@@@@@")
				ENDIF
			ENDIF
			
			IF IS_PLAYER_IN_ANY_INAPPROPRIATE_VEHICLE()
				TASK_SMART_FLEE_PED(pedPatrol[index], PLAYER_PED_ID(), 1000, -1)
			ENDIF
		ELSE
			//1988820 - If any of the peds were killed then flag them to combat.
			IF DOES_ENTITY_EXIST(pedPatrol[index])
				bPlayerAttackedPatrolMember = TRUE
				bIdentifiedAsAlien = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL IS_ANY_PATROLLER_IN_RANGE()
	INT index
	iAliveCounter = 0
	REPEAT TOTAL_ENEMIES index
		IF NOT IS_PED_INJURED(pedPatrol[index])
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedPatrol[index], FALSE)) < 250*250
				RETURN TRUE
			ELSE
				iAliveCounter++
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), " - IS_ANY_PATROLLER_IN_RANGE() has returned FALSE, this will run MISSION_FAILED() if all are alive, MISSION_PASSED() if at least one is dead.")
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_PATROLLER_IN_SPEAKING_RANGE()
	INT index
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		REPEAT TOTAL_ENEMIES index
			IF NOT IS_PED_INJURED(pedPatrol[index])
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedPatrol[index], FALSE)) < 20*20
				
					IF NOT bSpeakAttackLine
						IF iLineCounter = 2
							IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2)
								bSpeakAttackLine = TRUE
								iLineCounter = LINES_TO_PLAY
							ELIF (IS_PED_ON_FOOT(PLAYER_PED_ID()) AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1)
								bSpeakAttackLine = TRUE
								iLineCounter = LINES_TO_PLAY
							ENDIF
						ENDIF
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedPatrol[index], PLAYER_PED_ID())
						OR IS_PED_SHOOTING(PLAYER_PED_ID())
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedPatrol[index])
						OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedPatrol[index])
							bSpeakAttackLine = TRUE
							bPlayerAttackedPatrolMember = TRUE
							bIdentifiedAsAlien = TRUE
							iLineCounter = LINES_TO_PLAY
						ENDIF
					ENDIF
					
					IF NOT bSpeakAttackLine
					OR iLineCounter < LINES_TO_PLAY
						IF TIMERA() > 5000 //12000
							IF NOT IS_PED_IN_WRITHE(pedPatrol[index])
								REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)	
								ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedPatrol[index], "BorderPatrol")
								IF iLineCounter = 0
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										IF GET_RANDOM_BOOL()
											CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_INS1", CONV_PRIORITY_AMBIENT_HIGH)
										ELSE
											CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_INS2", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
										iLineCounter++
									ELSE
										CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_INS2", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
								ELSE
									IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_STOP", CONV_PRIORITY_AMBIENT_HIGH)
										iLineCounter++
									ELSE
										CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_PULL", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
								ENDIF
								bSpokeFirstLine = TRUE
								iLineCounter++
								IF iLineCounter >= LINES_TO_PLAY
									bSpeakAttackLine = TRUE
								ENDIF
								SETTIMERA(0)
							ENDIF
						ENDIF
					ELIF NOT bIdentifiedAsAlien	
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_PED_IN_WRITHE(pedPatrol[index])
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupBaddies, RELGROUPHASH_PLAYER)
							SET_PED_COMBAT_ATTRIBUTES(pedPatrol[index], CA_DO_DRIVEBYS, TRUE)
							REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)	
							ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedPatrol[index], "BorderPatrol")
//							CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_ATTK", CONV_PRIORITY_AMBIENT_HIGH)
							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
								CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_ATTM", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
								CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_ATTF", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
								CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_ATTT", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							bIdentifiedAsAlien = TRUE
							bSpeakAttackLine = TRUE
//							SET_ALL_TO_ATTACK()
							SETTIMERA(0)
						ENDIF
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT bPlayerDialogue
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_REACTM", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_REACTF", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_REACTT", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
								bPlayerDialogue = TRUE
							ENDIF
							IF TIMERA() > 5000 //15000
							AND NOT IS_PED_IN_WRITHE(pedPatrol[index])
								REMOVE_PED_FOR_DIALOGUE(dialogueStruct, 1)	
								ADD_PED_FOR_DIALOGUE(dialogueStruct, 1, pedPatrol[index], "BorderPatrol")
//								CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_CONT", CONV_PRIORITY_AMBIENT_HIGH)
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_CONM", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_CONF", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
									CREATE_CONVERSATION(dialogueStruct, "REBORAU", "REBOR_COTT", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
//								SET_ALL_TO_ATTACK()
								SETTIMERA(0)
							ENDIF
						ENDIF
					ENDIF
				
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PATROLLERS_DEAD()
	INT index
	BOOL bReturn = TRUE
	REPEAT TOTAL_ENEMIES index
		IF NOT IS_PED_INJURED(pedPatrol[index])
			bReturn = FALSE
		ENDIF
	ENDREPEAT
	RETURN bReturn
ENDFUNC

PROC missionCleanup()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@\n")
	
	IF bDoFullCleanUp
//		REMOVE_RELATIONSHIP_GROUP(relGroupBaddies)
		SET_WANTED_LEVEL_MULTIPLIER(1)
		INT index
		REPEAT TOTAL_ENEMIES index
			IF NOT IS_PED_INJURED(pedPatrol[index])
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_SMART_FLEE_PED(pedPatrol[index], PLAYER_PED_ID(), 1000, -1)
				SET_PED_KEEP_TASK(pedPatrol[index], TRUE)
			ENDIF
		ENDREPEAT
//		RELEASE_PATH_NODES()
//		LOAD_ALL_PATH_NODES(FALSE)
	ENDIF
	
	IF GET_RANDOM_EVENT_FLAG()
		IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			SET_WANTED_LEVEL_MULTIPLIER(1)
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@\n")
	//REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_WAND1)
	LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	RANDOM_EVENT_PASSED(RE_BORDERPATROL, iSelectedVariation)
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n@@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@\n")
	missionCleanup()
ENDPROC

FUNC VECTOR getWorldPoint()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint1)
		iSelectedVariation = iWorldPoint1
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint2) 
			iSelectedVariation = iWorldPoint2
		ENDIF
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vAmbWorldPoint3) 
			iSelectedVariation = iWorldPoint3
		ENDIF
		
		IF iSelectedVariation = iWorldPoint1
			RETURN vAmbWorldPoint1
		ELIF iSelectedVariation = iWorldPoint2
			RETURN vAmbWorldPoint2
		ELIF iSelectedVariation = iWorldPoint3
			RETURN vAmbWorldPoint3
		ENDIF

	ENDIF
	
	RETURN << 0, 0, 0 >>
	
ENDFUNC

PROC RUN_DEBUG()
#IF IS_DEBUG_BUILD 
	// Event Passed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		MISSION_PASSED()
	ENDIF
	// Event Failed
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
		MISSION_FAILED()
	ENDIF
#ENDIF
ENDPROC

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)
vInput = in_coords.vec_coord[0]
//	fInput = in_coords.headings[0]

// work out which variation is being launched
getWorldPoint()

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	missionCleanup()
ENDIF

// Small section to fool the compiler
	PRINTNL()
	PRINTSTRING("re_border launched at ")
	PRINTVECTOR(vInput)
	PRINTSTRING(" with heading ")
	PRINTFLOAT(in_coords.headings[0])
	PRINTNL()

//----

IF IS_PLAYER_IN_ANY_INAPPROPRIATE_VEHICLE()
	TERMINATE_THIS_THREAD()
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_BORDERPATROL, iSelectedVariation)
	LAUNCH_RANDOM_EVENT(RE_BORDERPATROL)
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	RUN_DEBUG()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		VECTOR vPlayerPosition
		vPlayerPosition = GET_PLAYER_COORDS(PLAYER_ID())
		REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vPlayerPosition.x -2000, vPlayerPosition.y -2000, vPlayerPosition.x +2000, vPlayerPosition.y +2000)
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	ENDIF
	IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_BP")
			
			SWITCH ambStage
				CASE ambCanRun
//					IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
//						missionCleanup()
//					ENDIF
					IF LOADED_AND_CREATED_BORDER_PATROL()
						SETTIMERA(15000)
						bDoFullCleanUp = TRUE
						ambStage = (ambRunning)
					ENDIF
				BREAK
				CASE ambRunning
					IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
						SET_RANDOM_EVENT_ACTIVE()
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			missionCleanup()
		ENDIF
	ELSE
		updateBadGuyBlips()
//		IF bIdentifiedAsAlien
			updateBadGuyActions()
//		ENDIF
		IF IS_ANY_PATROLLER_IN_RANGE()
			IS_ANY_PATROLLER_IN_SPEAKING_RANGE()
			IF ARE_ALL_PATROLLERS_DEAD()
				MISSION_PASSED()
			ENDIF
		ELSE
			IF iAliveCounter < TOTAL_ENEMIES
				MISSION_PASSED()
			ELSE
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
ENDWHILE

ENDSCRIPT
