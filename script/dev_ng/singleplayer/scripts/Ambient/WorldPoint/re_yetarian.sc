
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "completionpercentage_public.sch"
//USING "ambience_run_checks.sch"
USING "ambient_common.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "random_events_public.sch"
USING "rc_helper_functions.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	re_yetarian.sc
//		AUTHOR			:	Rob Taylor	
//		DESCRIPTION		:	Yetarian Beat where Franklin returns to the car showroom
//                          after Armenian 3 is completed
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
// Enums
ENUM EVENT_STAGE
	STAGE_SCENE_SETUP,
	STAGE_APPROACH_SHOWROOM,
	STAGE_PLAYER_DETECTED,
	STAGE_GOONS_ATTACKING
ENDENUM

ENUM EVENT_SUBSTAGE
	SUBSTAGE_SETUP,
	SUBSTAGE_RUN,
	SUBSTAGE_CLEANUP
ENDENUM

ENUM GOON_STATE
	GS_NULL = 0,
	GS_CREATE,
	GS_LEAVE_SHOWROOM,
	GS_ATTACK,
	GS_MONITOR_GOON_DEATHS,
	GS_CLEANUP,
	GS_DEAD
ENDENUM

// Constants
CONST_INT		MAX_GOONS 			2
//CONST_INT		DETECT_RANGE 		20
CONST_INT		MAX_GOON_SPAWNS 	3
CONST_INT 		MAX_CHASE_DISTANCE 	10

// STRUCTS
STRUCT GOON_PED
	
	MODEL_NAMES		mModel 
	PED_INDEX 		mPed
	AI_BLIP_STRUCT	mBlip
	VECTOR 			vSpawn
	FLOAT 			fHeading
	VECTOR 			vGoTo
	WEAPON_TYPE 	mWeapon
	GOON_STATE 		mState
	BOOL			bCreated
		
ENDSTRUCT

GOON_PED mGoon[MAX_GOONS]

// Variables
EVENT_STAGE		eventStage = STAGE_SCENE_SETUP
EVENT_SUBSTAGE	eventSubStage = SUBSTAGE_SETUP

PED_INDEX		mYetarian
PED_INDEX		mYetarianFront
PED_INDEX		mYetarianBack
BLIP_INDEX		mYetarianBlip
OBJECT_INDEX	oiPhone
OBJECT_INDEX	oiPhone1
OBJECT_INDEX	oiPhone2

INT				iConvDelayTimer = -1
INT				iConvDelayAmount = -1

VECTOR			vYetarianSpawnFront
FLOAT			fYetarianHeadFront
VECTOR			vYetarianSpawnBack
FLOAT			fYetarianHeadBack

BOOL			bMusicTriggered = FALSE

BOOL			bOpenDoor		= FALSE
BOOL			bIsExternal		= FALSE
BOOL			bUsedSideDoor	= FALSE
BOOL			bIsPanic		= FALSE
BOOL 			bIsFleeing 		= FALSE
BOOL 			bCanAttack 		= FALSE
BOOL 			bDoGoonLine		= FALSE
BOOL 			bSaidGoonLine	= FALSE
BOOL			bUpgradedGoons[2]
BOOL			bSimeonTasked	= FALSE
BOOL			bSimeonSpotted 	= FALSE
BOOL			bSimeonDead		= FALSE
MODEL_NAMES		mPhoneModel		= p_amb_phone_01

VEHICLE_INDEX 	viGreyJeep
VEHICLE_INDEX 	viBlueObey
VEHICLE_INDEX 	viYellowBJ

VECTOR			vCopCheck1	= <<-35.6070, -1110.0458, 26.4364>> 
VECTOR			vCopCheck2  = <<-126.1154, -1129.3466, 23.4329>>

VECTOR			vGoonScenarioPos[2]
FLOAT			fGoonScenarioHead[2]

//FLOAT			fDoorRatio = 0.0

SCENARIO_BLOCKING_INDEX iScenarioBlocker
INT 			iNavBlocker1 = -1
INT 			iNavBlocker2 = -1
INT 			iNavBlocker3 = -1

INT 			iNavBlockerGate = -1

SEQUENCE_INDEX	seq

REL_GROUP_HASH mGoonGroup
REL_GROUP_HASH mPlayerGroup
REL_GROUP_HASH mYetarianGroup

STRING sSubtitleID	= "REYE_AU"
structPedsForConversation mYetaConv

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Cleanup()
	
	CPRINTLN(DEBUG_AMBIENT, "Cleanup: re_yetarian") 
	
	// TODO - Tidy this up and make sure it works in the same manner
	//        as other random events - check usage of setting stats...	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//		IF NOT (bSimeonDead)
//			CPRINTLN(DEBUG_AMBIENT, "re_yetarian: Not complete!")
//			
//			IF bSimeonSpotted
//				CPRINTLN(DEBUG_AMBIENT, "re_yetarian: Resetting timer!") 
//				Mission_Over(g_iRECandidateID)
//			ELSE
//				CPRINTLN(DEBUG_AMBIENT, "re_yetarian: Not resetting timer!")
//				g_iLastSuccessfulAmbientLaunchTime-=999999
//				Mission_Over(g_iRECandidateID)
//			ENDIF
//		ELSE
//			CPRINTLN(DEBUG_AMBIENT, "re_yetarian: Passed!")
//			Mission_Over(g_iRECandidateID)
//			// We've completed Yetarian - so from now on, let's set the cleanup state on the showroom, assuming it's not on that already
//			SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_WINDOWS, BUILDINGSTATE_CLEANUP, TRUE) 	// Board up the windows
//			SET_BUILDING_STATE(BUILDINGNAME_IPL_CAR_SHOWROOM_LOD_BOARD, BUILDINGSTATE_CLEANUP, TRUE) 	// Board up the windows
//			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_BJXL_CRASH_POST_ARM3, FALSE)
//			SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE1, BUILDINGSTATE_CLEANUP, TRUE) 	// Adding a blocking object for the rubble
//			SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE2, BUILDINGSTATE_CLEANUP, TRUE) 	// Adding a blocking object for the rubble
//			SET_BUILDING_STATE(BUILDINGNAME_ES_CAR_SHOWROOOM_RUBBLE3, BUILDINGSTATE_CLEANUP, TRUE) 	// Adding a blocking object for the rubble
//		ENDIF
		Mission_Over(g_iRECandidateID)
	ELSE
		CPRINTLN(DEBUG_AMBIENT, "re_yetarian: Not even initialised...")
		Mission_Over(g_iRECandidateID)
	ENDIF	
	
	//PD - Command added to set random as finished, will update flow and debug screens
	RANDOM_EVENT_OVER()
	
	REMOVE_RELATIONSHIP_GROUP(mGoonGroup)
	REMOVE_RELATIONSHIP_GROUP(mPlayerGroup)
	REMOVE_RELATIONSHIP_GROUP(mYetarianGroup)
	
	IF bMusicTriggered = FALSE
		CANCEL_MUSIC_EVENT("RE9_SPOTTED")
	ENDIF
	
	// Remove Simeon
	IF DOES_ENTITY_EXIST(mYetarian)
		SET_PED_AS_NO_LONGER_NEEDED(mYetarian)
		
		IF DOES_BLIP_EXIST(mYetarianBlip)
			REMOVE_BLIP(mYetarianBlip)
		ENDIF
		//DELETE_PED(mYetarian)
	ENDIF

	INT i = 0
	// Remove goons
	REPEAT MAX_GOONS i
		mGoon[i].mState = GS_CLEANUP
	ENDREPEAT
	
	REMOVE_SCENARIO_BLOCKING_AREA(iScenarioBlocker)
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	IF iNavBlocker1 != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker1)
	ENDIF
	IF iNavBlocker2 != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker2)
	ENDIF
	IF iNavBlocker3 != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker3)
	ENDIF
	IF iNavBlockerGate != -1
		REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockerGate)
	ENDIF
	
	IF DOES_SCENARIO_GROUP_EXIST("DEALERSHIP") 
		IF NOT IS_SCENARIO_GROUP_ENABLED("DEALERSHIP") 
			SET_SCENARIO_GROUP_ENABLED("DEALERSHIP",TRUE)
			CPRINTLN(DEBUG_AMBIENT, "Enabled Dealership scenarios")
		ENDIF
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-51.0518, -1104.6848, 25.2975>>, <<-29.9812, -1094.0787, 27.5686>>, TRUE)
			
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Passed()
	CPRINTLN(DEBUG_AMBIENT, "Passed: re_yetarian")
	SAFE_RELEASE_PED(mYetarian)
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		CPRINTLN(DEBUG_AMBIENT, "Played scripted scanner report for Simeon")
		PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_SIMEON_01", 0.0)
	ENDIF
//	SET_DOOR_STATE(DOORNAME_DEALERSHIP_FRONT_L, DOORSTATE_LOCKED)
//	SET_DOOR_STATE(DOORNAME_DEALERSHIP_FRONT_R, DOORSTATE_LOCKED)
//	SET_DOOR_STATE(DOORNAME_DEALERSHIP_SIDE_L, DOORSTATE_LOCKED)
//	SET_DOOR_STATE(DOORNAME_DEALERSHIP_SIDE_R, DOORSTATE_LOCKED)
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	Mission_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------
PROC Mission_Failed()
	CPRINTLN(DEBUG_AMBIENT, "Failed: re_yetarian") 
	Mission_Cleanup()
ENDPROC

// ===========================================================================================================
//		Functions
// ===========================================================================================================

#IF IS_DEBUG_BUILD
	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
	
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			INT i = 0
			REPEAT MAX_GOONS i
				IF IS_ENTITY_ALIVE(mGoon[i].mPed)
					CLEAR_PED_TASKS(mGoon[i].mPed)
					SET_PED_AS_NO_LONGER_NEEDED(mGoon[i].mPed)
				ENDIF
			ENDREPEAT
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF
	ENDPROC
#ENDIF	

/// PURPOSE:
///    Initialises variables
PROC InitRandomEvent()
	CPRINTLN(DEBUG_AMBIENT, "Init: re_yetarian") 
	
	// Simeon
//	vYetarianSpawn		= <<-45.15, -1098.91, 25.42>> // By red jeep thing
//	fYetarianHead 		= 184.5161
	
	vYetarianSpawnFront		= <<-45.97, -1095.58, 25.42>> // By red jeep thing
	fYetarianHeadFront 		= 105.42
	vYetarianSpawnBack		= <<-33.39, -1101.79, 25.42>> // By red jeep thing
	fYetarianHeadBack 		= 184.5161
	
	// Goons
	mGoon[0].vSpawn		= <<-29.66, -1103.44, 26.42>> 	// Inside office, near back
	mGoon[0].fHeading	= 58.7988
	mGoon[0].vGoTo		= <<-35.19, -1101.72, 25.42>>		// Outside Simeon's office
	mGoon[0].mModel		= G_M_M_ArmGoon_01
	mGoon[0].mWeapon	= WEAPONTYPE_PISTOL
	vGoonScenarioPos[0]	= <<-30.35, -1104.29, 25.89>>
	fGoonScenarioHead[0]= 177.22
	
	mGoon[1].vSpawn		= <<-36.8032, -1090.6185, 25.4223>>		// Inside, behind side door
	mGoon[1].fHeading	= 257.6675
	mGoon[1].vGoTo		= <<-34.66, -1097.23, 25.42>>		// Outside side door	
	mGoon[1].mModel		= G_M_Y_ArmGoon_02	
	mGoon[1].mWeapon	= WEAPONTYPE_PISTOL
	vGoonScenarioPos[1]	= <<-29.11, -1104.99, 25.89>> // Not being used anymore...
	fGoonScenarioHead[1]= 136.57 // Or this...
		
	ADD_RELATIONSHIP_GROUP("GoonGroup", mGoonGroup)
	ADD_RELATIONSHIP_GROUP("PlayerGroup", mPlayerGroup)
	ADD_RELATIONSHIP_GROUP("YetarianGroup", mYetarianGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, mYetarianGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, mYetarianGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, mGoonGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, mGoonGroup)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-51.0518, -1104.6848, 25.2975>>, <<-29.9812, -1094.0787, 27.5686>>, FALSE)
	
	iScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA(<<-79.95, -1126.33, 20.65>>, <<-3.50, -1081.60, 30.67>>)
	ADD_COVER_BLOCKING_AREA(<<-34.2476, -1107.2834, 24.9621>>, <<-24.7924, -1102.5566, 29.8287>>, TRUE, TRUE, TRUE)
	
	// These are now handled by the building controller as IPLs
//	IF iNavBlocker1 = -1
//		iNavBlocker1 = ADD_NAVMESH_BLOCKING_OBJECT(<<-49.21, -1090.28, 25.42>>, <<2.5,3,3>>, 0)
//	ENDIF
//	IF iNavBlocker2 = -1
//		iNavBlocker2 = ADD_NAVMESH_BLOCKING_OBJECT(<<-49.07, -1093.25, 25.42>>, <<4,2,3>>, 0)
//	ENDIF
//	IF iNavBlocker3 = -1
//		iNavBlocker3 = ADD_NAVMESH_BLOCKING_OBJECT(<<-52.62, -1096.79, 25.42>>, <<1.5,4.5,2>>, -45)
//	ENDIF

	IF iNavBlockerGate = -1
		iNavBlockerGate = ADD_NAVMESH_BLOCKING_OBJECT(<<-29.30, -1086.35, 25.57>>, <<5.5,3,2>>, -10)
	ENDIF
	
	SET_DOOR_STATE(DOORNAME_DEALERSHIP_FRONT_L, DOORSTATE_UNLOCKED)
	SET_DOOR_STATE(DOORNAME_DEALERSHIP_FRONT_R, DOORSTATE_UNLOCKED)
	SET_DOOR_STATE(DOORNAME_DEALERSHIP_SIDE_L, DOORSTATE_UNLOCKED)
	SET_DOOR_STATE(DOORNAME_DEALERSHIP_SIDE_R, DOORSTATE_UNLOCKED)
	
	// Request models, do it here so we're only requesting them once.
	//REQUEST_WEAPON_ASSET(mYetarianWeapon)
	REQUEST_MODEL(mGoon[0].mModel)
	REQUEST_MODEL(mGoon[1].mModel)
	REQUEST_MODEL(mPhoneModel)
	REQUEST_MODEL(BjXL)
	REQUEST_MODEL(tailgater)
	REQUEST_MODEL(baller2)
	REQUEST_WEAPON_ASSET(mGoon[0].mWeapon)
	REQUEST_WEAPON_ASSET(mGoon[1].mWeapon)
	REQUEST_ANIM_DICT("cellphone@")
	REQUEST_ANIM_DICT("cellphone@str")
	REQUEST_WAYPOINT_RECORDING("reyetarian_simeonoffice")
	
	PREPARE_MUSIC_EVENT("RE9_SPOTTED")
	
ENDPROC

/// PURPOSE:
///    Checks if the interior of the car showroom has loaded in, so we can 
///    spawn a couple of goon inside to come out and attack the player
/// RETURNS:
///    TRUE if the interior has loaded.
FUNC BOOL HAS_SHOWROOM_INTERIOR_LOADED()

	INTERIOR_INSTANCE_INDEX mInterior = GET_INTERIOR_AT_COORDS(mGoon[0].vSpawn) // Will only work if we spawn this guy inside.
	IF NATIVE_TO_INT(mInterior) != 0
		PIN_INTERIOR_IN_MEMORY(mInterior)
		
		IF IS_INTERIOR_READY(mInterior)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Make Simeon 'put the phone away' (delete it when his hand is hidden)
PROC CHECK_TO_PUT_SIMEONS_PHONE_AWAY()

	IF IS_ENTITY_ALIVE(mYetarian)
		IF IS_ENTITY_PLAYING_ANIM(mYetarian, "cellphone@", "cellphone_call_out")
			IF GET_ENTITY_ANIM_CURRENT_TIME(mYetarian, "cellphone@", "cellphone_call_out") >= 0.240
				IF DOES_ENTITY_EXIST(oiPhone)
					CPRINTLN(DEBUG_AMBIENT, "Simeon's phone deleted!")
					DELETE_OBJECT(oiPhone)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC HANDLE_DOOR()
	IF bOpenDoor = FALSE
		IF IS_ENTITY_ALIVE(mYetarian)
			IF IS_ENTITY_AT_COORD(mYetarian, <<-31.58, -1102.86, 25.42>>, <<0.8,0.8,4>>, FALSE, FALSE)
				CPRINTLN(DEBUG_AMBIENT, "Door opening")
				bOpenDoor = TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE))
			FLOAT fSpeed = 0.05
			FLOAT fTolerance = 0.01
			FLOAT fOpenRatioIn = -1.0
			FLOAT fOpenRatio

			fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE))

			IF fOpenRatio <= fOpenRatioIn + -fTolerance
			OR fOpenRatio >= fOpenRatioIn + fTolerance
				IF fOpenRatio > fOpenRatioIn
					fOpenRatio -= fSpeed
					IF fOpenRatio < fOpenRatio
						fOpenRatio = fOpenRatioIn
					ENDIF
				ELIF fOpenRatio < fOpenRatioIn
					fOpenRatio += fSpeed
					IF fOpenRatio > fOpenRatioIn
						fOpenRatio = fOpenRatioIn
					ENDIF
				ENDIF
				CPRINTLN(DEBUG_AMBIENT, "Door ratio after fiddling: ", fOpenRatio)
			ENDIF
			
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE), fOpenRatio, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE), DOORSTATE_LOCKED, FALSE, TRUE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Check if the player is currently equipped with melee weapon or is unarmed
/// RETURNS:
///    TRUE if a melee weapon or unarmed is equipped
FUNC BOOL IS_PLAYER_USING_MELEE()
	
	IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED 
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT //CHECK FOR MOBILE PHONE AS THIS WILL TRIGGER
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_KNIFE
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_NIGHTSTICK
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_HAMMER
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_BAT
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_CROWBAR
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_GOLFCLUB
	AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_DLC_BOTTLE
		// Player is armed with something, but we don't want to return true when we're in a vehicle because the weapon isn't visible and it doesn't make sense
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		// If the player's shooting in a vehicle, then the weapon is visible and we should return false
		ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if all the requested models have been loaded in
/// RETURNS:
///    True if all requested models have loaded.
FUNC BOOL HAVE_MODELS_LOADED()
		
	IF HAS_MODEL_LOADED(mGoon[0].mModel)
	AND HAS_MODEL_LOADED(mGoon[1].mModel)
	AND HAS_WEAPON_ASSET_LOADED(mGoon[0].mWeapon)
	AND HAS_WEAPON_ASSET_LOADED(mGoon[1].mWeapon)
	AND HAS_SHOWROOM_INTERIOR_LOADED()
	AND HAS_ANIM_DICT_LOADED("cellphone@")
	AND HAS_ANIM_DICT_LOADED("cellphone@str")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("reyetarian_simeonoffice")
	AND HAS_MODEL_LOADED(mPhoneModel)
	AND HAS_MODEL_LOADED(BJXL)
	AND HAS_MODEL_LOADED(TAILGATER)
	AND HAS_MODEL_LOADED(baller2)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if the chasing Yetarian has moved far enough away from his spawn
/// RETURNS:
///    TRUE if Yetarian is far enough away from his original position.
FUNC BOOL YETARIAN_FINISHED_CHASING()
	
	IF IS_ENTITY_ALIVE(mYetarian)
		// Check if the player has entered the showroom through the car park doors - Simeon check for being far from his spawn is smaller if so
		IF NOT bUsedSideDoor
			//IF GET_DISTANCE_BETWEEN_COORDS(vYetarianSpawn, GET_ENTITY_COORDS(mYetarian, FALSE)) >= 10
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) <= 7
			OR GET_SCRIPT_TASK_STATUS(mYetarian, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				IF NOT bIsFleeing
//					IF GET_DISTANCE_BETWEEN_COORDS(vYetarianSpawn, GET_ENTITY_COORDS(mYetarian, FALSE)) >= 4
//						CPRINTLN(DEBUG_AMBIENT, "Simeon >= 4m away from spawn...")
//					ENDIF
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) <= 7
						CPRINTLN(DEBUG_AMBIENT, "Simeon <= 7m away from player")
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(mYetarian, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
						CPRINTLN(DEBUG_AMBIENT, "Simeon finished task")
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			//IF GET_DISTANCE_BETWEEN_COORDS(vYetarianSpawn, GET_ENTITY_COORDS(mYetarian, FALSE)) >= 3.2
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) <= 4.5
			OR GET_SCRIPT_TASK_STATUS(mYetarian, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				IF NOT bIsFleeing
//					IF GET_DISTANCE_BETWEEN_COORDS(vYetarianSpawn, GET_ENTITY_COORDS(mYetarian, FALSE)) >= 3.2
//						CPRINTLN(DEBUG_AMBIENT, "Simeon >= 3.2m away from spawn...")
//					ENDIF
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) <= 4.5
						CPRINTLN(DEBUG_AMBIENT, "Simeon <= 4.5m away from player")
					ENDIF
					IF GET_SCRIPT_TASK_STATUS(mYetarian, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
						CPRINTLN(DEBUG_AMBIENT, "Simeon finished task")
					ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC CHOOSE_A_SIMEON(BOOL bRearSimeon = TRUE)

	IF bRearSimeon
		IF IS_ENTITY_ALIVE(mYetarianBack)
			SET_ENTITY_VISIBLE(mYetarianBack, TRUE)
			FREEZE_ENTITY_POSITION(mYetarianBack, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(mYetarianBack, TRUE)
			mYetarian = mYetarianBack
			oiPhone = oiPhone2
			SAFE_DELETE_PED(mYetarianFront)
			SAFE_DELETE_OBJECT(oiPhone1)
		ENDIF
	ELSE
		IF IS_ENTITY_ALIVE(mYetarianFront)
			SET_ENTITY_VISIBLE(mYetarianFront, TRUE)
			FREEZE_ENTITY_POSITION(mYetarianFront, FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(mYetarianFront, TRUE)
			mYetarian = mYetarianFront
			oiPhone = oiPhone1
			SAFE_DELETE_PED(mYetarianBack)
			SAFE_DELETE_OBJECT(oiPhone2)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///		Checks if Yetarian can detect Franklin/Michael close by, if there's no cops in the area, or if Franklin/Michael has attacked or shot at him.
/// RETURNS:
///    True if Yetarian has detected Franklin/Michael.
FUNC BOOL IS_PLAYER_CLOSE()

//	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mYetarian, PLAYER_PED_ID())
//	OR IS_BULLET_IN_AREA(vYetarianSpawn, 4.0, TRUE)
//		CPRINTLN(DEBUG_AMBIENT, "Player Close: Attacking Yetarian")
//		bIsPanic = TRUE
//		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//			SET_RANDOM_EVENT_ACTIVE()
//		ENDIF
//		RETURN TRUE
//	ENDIF
	
//	IF bIsExternal = FALSE
//		IF GET_NUMBER_OF_FIRES_IN_RANGE(<<-44.953209,-1095.387939,25.422319>> , 15.5) >=1
//		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-58.884151,-1091.699829,24.672319>>, <<-31.732553,-1101.449097,30.422319>>, 31.0)
//		OR IS_BULLET_IN_ANGLED_AREA(<<-58.348457,-1101.258911,24.683113>>, <<-34.863289,-1109.844971,30.451334>>, 1.25) // Long car park side window
//		OR IS_BULLET_IN_ANGLED_AREA(<<-61.425377,-1096.529053,24.686569>>, <<-56.039036,-1081.619629,30.866621>>, 1.25) // Street facing short window
//		OR IS_BULLET_IN_ANGLED_AREA(<<-36.620426,-1114.434937,24.731697>>, <<-34.885033,-1109.809326,30.442011>>, 1.25) // Office window by door
//			CPRINTLN(DEBUG_AMBIENT, "Player Close: React to outside gunfire/explosions")
//			bIsExternal	= TRUE
//			bIsPanic = TRUE
//			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//				CHOOSE_A_SIMEON(TRUE)
//				SET_RANDOM_EVENT_ACTIVE()
//			ENDIF
//			RETURN TRUE
//		ENDIF
//	ELSE
//		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//			SET_RANDOM_EVENT_ACTIVE()
//		ENDIF
//		RETURN TRUE
//	ENDIF
	
	IF NOT IS_COP_PED_IN_AREA_3D(vCopCheck1, vCopCheck2)
	AND NOT (IS_POSITION_OCCUPIED(<<-61.013214,-1093.392944,25.752972>>, 2.0, FALSE, TRUE, FALSE, FALSE, FALSE)
		AND IS_POSITION_OCCUPIED(<<-38.596832,-1109.284058,25.687418>> , 2.0, FALSE, TRUE, FALSE, FALSE, FALSE))
	
		// Player is nearby - may want to change this to LOS check
		//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mYetarian)) < DETECT_RANGE
//		INTERIOR_INSTANCE_INDEX mInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_ID())
//		
//		IF mInteror = GET_INTERIOR_AT_COORDS(vYetarianSpawn)
//			CPRINTLN(DEBUG_AMBIENT, "Player Close: Inside Showroom")
//			RETURN TRUE
//		ENDIF
		
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("rm_showroom")
			CPRINTLN(DEBUG_AMBIENT, "Player Close: Spotted by Yetarian inside car showroom")
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-41.190151,-1104.137207,24.422319>>, <<-33.794300,-1107.183472,28.672319>>, 8.50)
					bUsedSideDoor = TRUE
					CPRINTLN(DEBUG_AMBIENT, "Player Close: Player used the side door")
					CHOOSE_A_SIMEON(FALSE)
				ELSE
					bUsedSideDoor = FALSE
					CPRINTLN(DEBUG_AMBIENT, "Player Close: Player used the front door")
					CHOOSE_A_SIMEON(TRUE)
				ENDIF
				SET_RANDOM_EVENT_ACTIVE()
			ENDIF
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_COP_PED_IN_AREA_3D(vCopCheck1, vCopCheck2)
		OR (IS_POSITION_OCCUPIED(<<-61.013214,-1093.392944,25.752972>>, 2.0, FALSE, TRUE, FALSE, FALSE, FALSE) AND IS_POSITION_OCCUPIED(<<-38.596832,-1109.284058,25.687418>> , 2.0, FALSE, TRUE, FALSE, FALSE, FALSE))
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("rm_showroom")
				CPRINTLN(DEBUG_AMBIENT, "Player Close: Inside showroom with both exits blocked or cops in area - cleanup")
				SAFE_RELEASE_PED(mGoon[0].mPed)
				SAFE_RELEASE_PED(mGoon[1].mPed)
				SAFE_DELETE_PED(mYetarianBack)
				SAFE_DELETE_PED(mYetarianFront)
				Mission_Cleanup()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MONITOR_GOON_STATES(INT i)

	SWITCH mGoon[i].mState 
	
		CASE GS_NULL
			//CPRINTLN(DEBUG_AMBIENT, "Goon state = NULL")
		BREAK
		
		CASE GS_CREATE
			IF NOT mGoon[i].bCreated
				IF NOT IS_ENTITY_ALIVE(mGoon[i].mPed)
					mGoon[i].mPed = CREATE_PED(PEDTYPE_CRIMINAL, mGoon[i].mModel, mGoon[i].vSpawn, mGoon[i].fHeading)
					IF IS_ENTITY_ALIVE(mGoon[i].mPed)
						CPRINTLN(DEBUG_AMBIENT, "Goon created!")
						// Goon 0 (the one sat in the office) has some special commands to make him get up and combat properly
						IF i = 0
							TASK_START_SCENARIO_AT_POSITION(mGoon[i].mPed, "PROP_HUMAN_SEAT_CHAIR", vGoonScenarioPos[i], fGoonScenarioHead[i])
							SET_PED_SHOULD_PLAY_NORMAL_SCENARIO_EXIT(mGoon[i].mPed)
							SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(mGoon[i].mPed, TRUE)
							SET_PED_SPHERE_DEFENSIVE_AREA(mGoon[i].mPed, <<-37.347847,-1102.022217,25.422319>> , 3.0)
							SET_PED_COMBAT_ATTRIBUTES(mGoon[i].mPed, CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
						ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mGoon[i].mPed, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(mGoon[i].mPed, mGoonGroup)
						SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(mGoon[i].mPed, FALSE, mGoonGroup)
						//SET_PED_COMBAT_ATTRIBUTES(mGoon[i].mPed, CA_USE_PROXIMITY_FIRING_RATE, ALLOW_PROXIMITY_FIRE_RATE)
						SET_PED_COMBAT_ATTRIBUTES(mGoon[i].mPed, CA_REQUIRES_LOS_TO_SHOOT, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(mGoon[i].mPed, CA_USE_VEHICLE, FALSE)
						SET_PED_COMBAT_ATTRIBUTES(mGoon[i].mPed, CA_CAN_USE_FRUSTRATED_ADVANCE, TRUE)
						
						SET_PED_CHANCE_OF_FIRING_BLANKS(mGoon[i].mPed, 0.7, 0.3)
						bUpgradedGoons[i] = FALSE
						// Arm them if Michael
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							GIVE_WEAPON_TO_PED(mGoon[i].mPed, mGoon[i].mWeapon, INFINITE_AMMO, TRUE)
						ENDIF
						
						// Release model
						SET_MODEL_AS_NO_LONGER_NEEDED(mGoon[i].mModel)
						mGoon[i].bCreated = TRUE
						
						mGoon[i].mState = GS_LEAVE_SHOWROOM
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE GS_LEAVE_SHOWROOM
			
			IF IS_ENTITY_ALIVE(mGoon[i].mPed)
				IF bCanAttack
					UPDATE_AI_PED_BLIP(mGoon[i].mPed, mGoon[i].mBlip)
					CPRINTLN(DEBUG_AMBIENT, "Goon tasked with leaving showroom")
					CLEAR_PED_TASKS(mGoon[i].mPed)
					SET_PED_USING_ACTION_MODE(mGoon[i].mPed, TRUE, -1, "DEFAULT_ACTION")
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_EXTEND_PITCH_LIMIT|SLF_EXTEND_YAW_LIMIT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, mGoon[i].vGoTo, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 3.0)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(mGoon[i].mPed, seq)
					CLEAR_SEQUENCE_TASK(seq)
					mGoon[i].mState = GS_ATTACK
				ELSE
					//CPRINTLN(DEBUG_AMBIENT, "bCanAttack = FALSE")
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mGoon[i].mPed, FALSE)) >= 250
					CPRINTLN(DEBUG_AMBIENT, "Too far away - Goon cleaned from Monitor Goon Deaths - GS_LEAVE_SHOWROOM panic check")
					mGoon[i].mState = GS_CLEANUP
				ENDIF
			ELSE 
				CPRINTLN(DEBUG_AMBIENT, "Goon cleaned from Leave Showroom")
				mGoon[i].mState = GS_CLEANUP
			ENDIF
		BREAK
		
		CASE GS_ATTACK
			
			IF IS_ENTITY_ALIVE(mGoon[i].mPed)
				
				UPDATE_AI_PED_BLIP(mGoon[i].mPed, mGoon[i].mBlip)
				
				IF GET_SCRIPT_TASK_STATUS(mGoon[i].mPed, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mGoon[i].mPed, FALSE)) <= 5
					
					CPRINTLN(DEBUG_AMBIENT, "Goon told to fight player")
					TASK_LOOK_AT_ENTITY(mGoon[i].mPed, PLAYER_PED_ID(), -1)
					TASK_COMBAT_PED(mGoon[i].mPed, PLAYER_PED_ID())					
					SET_PED_ALERTNESS(mGoon[i].mPed, AS_MUST_GO_TO_COMBAT)
					SET_PED_KEEP_TASK(mGoon[i].mPed, TRUE)
					mGoon[i].mState = GS_MONITOR_GOON_DEATHS
				ENDIF
				
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mGoon[i].mPed, FALSE)) >= 250
					CPRINTLN(DEBUG_AMBIENT, "Too far away - Goon cleaned from Monitor Goon Deaths - GS_ATTACK panic check")
					mGoon[i].mState = GS_CLEANUP
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "Goon cleaned from Goons Attack")
				mGoon[i].mState = GS_CLEANUP
			ENDIF
						
		BREAK
		
		CASE GS_MONITOR_GOON_DEATHS
		
			UPDATE_AI_PED_BLIP(mGoon[i].mPed, mGoon[i].mBlip)
		
			IF NOT IS_ENTITY_ALIVE(mGoon[i].mPed)
				CPRINTLN(DEBUG_AMBIENT, "Goon cleaned from Monitor Goon Deaths")
				mGoon[i].mState = GS_CLEANUP
			ELSE
				IF bUpgradedGoons[i] = FALSE
					IF NOT IS_PLAYER_USING_MELEE()
					OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						GIVE_WEAPON_TO_PED(mGoon[i].mPed, mGoon[i].mWeapon, INFINITE_AMMO, FALSE, TRUE)
						//CLEAR_PED_TASKS(mGoon[i].mPed)
						bUpgradedGoons[i] = TRUE
						CPRINTLN(DEBUG_AMBIENT, "Franklin using non-melee weapon/player is Michael, upgrading goons...")
					ENDIF
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(mGoon[i].mPed, SCRIPT_TASK_COMBAT) = FINISHED_TASK
					CPRINTLN(DEBUG_AMBIENT, "Retasked goon to combat player...")
					TASK_COMBAT_PED(mGoon[i].mPed, PLAYER_PED_ID())
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mGoon[i].mPed, FALSE)) >= 75
					CPRINTLN(DEBUG_AMBIENT, "Too far away - Goon cleaned from Monitor Goon Deaths")
					mGoon[i].mState = GS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE GS_CLEANUP
			
			CPRINTLN(DEBUG_AMBIENT, "Goon cleaned up")
			
			CLEANUP_AI_PED_BLIP(mGoon[i].mBlip)
	
			IF DOES_ENTITY_EXIST(mGoon[i].mPed)
				SET_PED_AS_NO_LONGER_NEEDED(mGoon[i].mPed)
			ENDIF
			
			mGoon[i].mState = GS_DEAD
		BREAK
		
		CASE GS_DEAD
			//CPRINTLN(DEBUG_AMBIENT, "Goon state = DEAD")
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Puts the goons into the their create state.
PROC CREATE_GOONS()

	CPRINTLN(DEBUG_AMBIENT, "Create Goons Called...")
	
	INT i
	i = 0
	REPEAT MAX_GOONS i 
		mGoon[i].mState = GS_CREATE
	ENDREPEAT
				
ENDPROC

PROC DO_GOON_LINE()
	IF bDoGoonLine = FALSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF IS_ENTITY_ALIVE(mGoon[0].mPed)
				ADD_PED_FOR_DIALOGUE(mYetaConv, 5, mGoon[0].mPed, "YETARIAN_GOON")
			ELIF IS_ENTITY_ALIVE(mGoon[1].mPed)
				ADD_PED_FOR_DIALOGUE(mYetaConv, 5, mGoon[1].mPed, "YETARIAN_GOON")
			ELSE
				// Both peds are dead, don't try and do the line
				bDoGoonLine = TRUE
				bSaidGoonLine = TRUE
			ENDIF
			IF bDoGoonLine = FALSE // Recheck in case both peds were dead
				// If Michael, do a multipart convo with Michael's line tacked on the end
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETA_MIKE", CONV_PRIORITY_MEDIUM)
						bDoGoonLine = TRUE
					ENDIF
				ELSE
				// If Franklin, just play the goon convo
					IF CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETA_FRAN", CONV_PRIORITY_MEDIUM)
						bDoGoonLine = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF bSaidGoonLine = FALSE
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF IS_ENTITY_ALIVE(mGoon[0].mPed)
				PLAY_PED_AMBIENT_SPEECH(mGoon[0].mPed, "FIGHT", SPEECH_PARAMS_SHOUTED)
				bSaidGoonLine = TRUE
			ELIF IS_ENTITY_ALIVE(mGoon[1].mPed)
				PLAY_PED_AMBIENT_SPEECH(mGoon[1].mPed, "FIGHT", SPEECH_PARAMS_SHOUTED)
				bSaidGoonLine = TRUE
			ELSE
				// Both peds are dead, don't try and do the line
				bSaidGoonLine = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_SIMEON_LINE()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(mYetaConv, 1, PLAYER_PED_ID(), "FRANKLIN")
	ELSE
		ADD_PED_FOR_DIALOGUE(mYetaConv, 0, PLAYER_PED_ID(), "MICHAEL")
	ENDIF
	
	// Add Simeon for conversation.
	ADD_PED_FOR_DIALOGUE(mYetaConv, 4, mYetarian, "SIMEON")

	// if bIsPanic has been set from IS_PLAYER_CLOSE(), we play Simeon's panicky dialogue instead of the chara-specific line
	IF bIsPanic
		CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETA_SNIP", CONV_PRIORITY_MEDIUM)
	ELIF bIsExternal
		CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETA_EXT", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
	ELSE
		// If the player has walked in holding a gun, we play the panicky dialogue instead there too
		IF IS_PLAYER_USING_MELEE()
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETAM", CONV_PRIORITY_MEDIUM)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETAF", CONV_PRIORITY_MEDIUM)
			ENDIF
		ELSE
			CREATE_CONVERSATION(mYetaConv, sSubtitleID, "RE_YETA_SNIP", CONV_PRIORITY_MEDIUM)
		ENDIF
	ENDIF

ENDPROC

/// ================================
///    MISSION STATES
/// ================================
///    
/// PURPOSE:
///    Loads all required assets and creates peds
PROC ES_Scene_Setup()

	SWITCH eventSubStage

		CASE SUBSTAGE_SETUP
			IF HAVE_MODELS_LOADED()
				CPRINTLN(DEBUG_AMBIENT, "Stage: ES_Scene_Setup")
				eventSubStage = SUBSTAGE_RUN
			ELSE
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
					Mission_Cleanup()
				ENDIF
			ENDIF
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-51.89, -1097.35, 23.42>>, <<-45.71, -1092.10, 27.42>>, FALSE)
			// Remove the yellow tailgater if present
			CLEAR_ANGLED_AREA_OF_VEHICLES(<<-53.575745,-1095.356445,24.172319>>, <<-46.525509,-1094.587158,27.422319>>, 3.75)
		BREAK
		
		CASE SUBSTAGE_RUN
			// Create Simeon
			IF NOT IS_ENTITY_ALIVE(mYetarianFront)
			OR NOT IS_ENTITY_ALIVE(mYetarianBack)
				CREATE_NPC_PED_ON_FOOT(mYetarianFront, CHAR_SIMEON, vYetarianSpawnFront, fYetarianHeadFront)
				IF IS_ENTITY_ALIVE(mYetarianFront)
					CPRINTLN(DEBUG_AMBIENT, "YETARIAN PED CREATED")
					SET_ENTITY_VISIBLE(mYetarianFront, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED(mYetarianFront, FALSE)
					SET_PED_CAN_BE_TARGETTED(mYetarianFront, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mYetarianFront, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(mYetarianFront, mYetarianGroup)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mYetarianFront, TRUE)
					GIVE_WEAPON_TO_PED(mYetarianFront, WEAPONTYPE_PISTOL, 999, FALSE, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(mYetarianFront, FALSE, mGoonGroup)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, mGoonGroup, RELGROUPHASH_PLAYER)
					SET_ENTITY_LOAD_COLLISION_FLAG(mYetarianFront, TRUE)
					SET_PED_CONFIG_FLAG(mYetarianFront, PCF_OpenDoorArmIK, TRUE )
					oiPhone1 = CREATE_OBJECT(mPhoneModel, GET_ENTITY_COORDS(mYetarianFront))
					ATTACH_ENTITY_TO_ENTITY(oiPhone1, mYetarianFront, GET_PED_BONE_INDEX(mYetarianFront, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE) //<<0.06,0,-0.005>>, <<110,190,-40>>, TRUE)
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_b")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_f")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_no_a")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_d")
						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(mYetarianFront, seq)
					CLEAR_SEQUENCE_TASK(seq)
					FREEZE_ENTITY_POSITION(mYetarianFront, TRUE)
				ENDIF
				
				CREATE_NPC_PED_ON_FOOT(mYetarianBack, CHAR_SIMEON, vYetarianSpawnBack, fYetarianHeadBack)
				IF IS_ENTITY_ALIVE(mYetarianBack)
					SET_ENTITY_VISIBLE(mYetarianBack, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED(mYetarianBack, FALSE)
					SET_PED_CAN_BE_TARGETTED(mYetarianBack, FALSE)
					CPRINTLN(DEBUG_AMBIENT, "YETARIAN PED CREATED")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mYetarianBack, TRUE)
					SET_PED_RELATIONSHIP_GROUP_HASH(mYetarianBack, mYetarianGroup)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mYetarianBack, TRUE)
					GIVE_WEAPON_TO_PED(mYetarianBack, WEAPONTYPE_PISTOL, 999, FALSE, FALSE)
					SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(mYetarianBack, FALSE, mGoonGroup)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, mGoonGroup, RELGROUPHASH_PLAYER)
					SET_ENTITY_LOAD_COLLISION_FLAG(mYetarianBack, TRUE)
					SET_PED_CONFIG_FLAG(mYetarianBack, PCF_OpenDoorArmIK, TRUE )
					oiPhone2 = CREATE_OBJECT(mPhoneModel, GET_ENTITY_COORDS(mYetarianBack))
					ATTACH_ENTITY_TO_ENTITY(oiPhone2, mYetarianBack, GET_PED_BONE_INDEX(mYetarianBack, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>, TRUE) //<<0.06,0,-0.005>>, <<110,190,-40>>, TRUE)
					OPEN_SEQUENCE_TASK(seq)
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_b")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_f")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_no_a")
						TASK_PLAY_ANIM(NULL, "cellphone@str", "cellphone_call_listen_d")
						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(mYetarianBack, seq)
					CLEAR_SEQUENCE_TASK(seq)
					FREEZE_ENTITY_POSITION(mYetarianFront, TRUE)
				ENDIF
			ELSE
				CREATE_GOONS()
				
				viGreyJeep = CREATE_VEHICLE(baller2, <<-46.272202,-1097.466187,25.42>>, 112.536331)
				SET_VEHICLE_COLOUR_COMBINATION(viGreyJeep, 7)
				SET_VEHICLE_WINDOW_TINT(viGreyJeep, 3)
				SET_VEHICLE_DOORS_LOCKED(viGreyJeep, VEHICLELOCK_LOCKED)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viGreyJeep, FALSE)
				
				viBlueObey = CREATE_VEHICLE(tailgater, <<-41.073540,-1099.566895,25.42>>, 138.429230)
				SET_VEHICLE_COLOUR_COMBINATION(viBlueObey, 12)
				SET_VEHICLE_DOORS_LOCKED(viBlueObey, VEHICLELOCK_LOCKED)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viBlueObey, FALSE)
				
				viYellowBJ = CREATE_VEHICLE(BjXL, <<-36.744564,-1101.582642,25.42>>, 155.157349)
				SET_VEHICLE_COLOUR_COMBINATION(viYellowBJ, 4)
				SET_VEHICLE_WINDOW_TINT(viYellowBJ, 3)
				SET_VEHICLE_DOORS_LOCKED(viYellowBJ, VEHICLELOCK_LOCKED)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viYellowBJ, FALSE)
				
				// Next stage
				eventSubStage = SUBSTAGE_CLEANUP				
			ENDIF
		BREAK
		
		CASE SUBSTAGE_CLEANUP
		
			// Setup stage vars
			eventStage    = STAGE_APPROACH_SHOWROOM
			eventSubStage = SUBSTAGE_SETUP
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Blip random event and wait for player to approach the car showroom
PROC ES_Approach_Showroom()
	
	SWITCH eventSubStage

		CASE SUBSTAGE_SETUP
			
			CPRINTLN(DEBUG_AMBIENT, "Stage: ES_Approach_Showroom")
			
			// Next stage
			eventSubStage = SUBSTAGE_RUN
		BREAK
		
		CASE SUBSTAGE_RUN
		
			IF HAS_SHOWROOM_INTERIOR_LOADED()
				// Try to remove the yellow tailgater if present
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE))
					CPRINTLN(DEBUG_AMBIENT, "Door isn't registered, adding now...")
					ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE), V_ILEV_FIB_DOOR1, <<-31.72, -1101.85, 26.57>>)
				ELSE
					IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE)) < 1.0
						DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE), 1.0, FALSE)
						DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_ARM2_SIMEON_OFFICE), DOORSTATE_LOCKED, FALSE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			CHECK_TO_PUT_SIMEONS_PHONE_AWAY()
		
			// Check player's distance to Simeon
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF IS_ENTITY_ALIVE(mYetarianFront)
				or IS_ENTITY_ALIVE(mYetarianBack)
					IF IS_PLAYER_CLOSE()
						IF bSimeonTasked = FALSE
							IF bIsExternal // If the player's scared Simeon off by shooting the exterior or something, make him run away without stopping
								CLEAR_PED_TASKS(mYetarian)
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_EXTEND_PITCH_LIMIT|SLF_EXTEND_YAW_LIMIT)
									TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_call_out")
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(mYetarian, seq)
								CLEAR_SEQUENCE_TASK(seq)
								CPRINTLN(DEBUG_AMBIENT, "Done Simeon panic tasks...")
							ELSE
								CLEAR_PED_TASKS(mYetarian)
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_EXTEND_PITCH_LIMIT|SLF_EXTEND_YAW_LIMIT)
									TASK_PLAY_ANIM(NULL, "cellphone@", "cellphone_call_out")
									//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(mYetarian, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							bSimeonSpotted = TRUE // The player has let Simeon see them and started the event, reset the timer if the event's not completed
							bSimeonTasked = TRUE
						ENDIF
						
//						IF NOT DOES_BLIP_EXIST(mYetarianBlip)
//							mYetarianBlip = CREATE_AMBIENT_BLIP_FOR_PED(mYetarian, TRUE)
//						ENDIF
						
						// If the player is in this area (walked through the car park doors), delay the convo by 1 second - otherwise, 4 seconds
						IF NOT bUsedSideDoor
						AND NOT bIsExternal
							IF iConvDelayTimer = -1
								iConvDelayTimer = GET_GAME_TIMER()
							ENDIF
							IF iConvDelayAmount = -1
								iConvDelayAmount = 4000
							ENDIF
						ELSE
							IF iConvDelayTimer = -1
								iConvDelayTimer = GET_GAME_TIMER()
							ENDIF
							IF iConvDelayAmount = -1
								iConvDelayAmount = 1000
							ENDIF
						ENDIF
						
						IF iConvDelayTimer != -1
						AND iConvDelayAmount != 1
							IF (GET_GAME_TIMER() - iConvDelayTimer) >= iConvDelayAmount
								DO_SIMEON_LINE()
								TRIGGER_MUSIC_EVENT("RE9_SPOTTED")
								bMusicTriggered = TRUE
								CPRINTLN(DEBUG_AMBIENT, "Triggered music/dialogue...")
								eventSubStage = SUBSTAGE_CLEANUP
							ELSE
								CPRINTLN(DEBUG_AMBIENT, "Waiting to say Simeon's dialogue...")
							ENDIF
						ENDIF					
					ENDIF
						
				ELSE
					IF DOES_BLIP_EXIST(mYetarianBlip)
						REMOVE_BLIP(mYetarianBlip)
					ENDIF
					IF DOES_ENTITY_EXIST(mYetarian)
						IF IS_ENTITY_DEAD(mYetarian)
							IF NOT bSimeonDead
								bSimeonDead = TRUE
								CPRINTLN(DEBUG_AMBIENT, "Simeon dead?")
							ENDIF
						ENDIF
					ENDIF
					bCanAttack = TRUE
					IF mGoon[0].mState >= GS_CLEANUP
					AND mGoon[1].mState >= GS_CLEANUP
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-69.623955,-1087.369507,24.598333>>, <<-11.278607,-1108.279785,34.172050>>, 47.5)
							Mission_Passed()
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "Waiting for player to leave dealership area before passing")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SUBSTAGE_CLEANUP
			
			CPRINTLN(DEBUG_AMBIENT, "ES_Approach_Showroom() Cleanup")
			// Setup stage vars
			eventStage    = STAGE_PLAYER_DETECTED
			eventSubStage = SUBSTAGE_SETUP
		BREAK

	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Simeon reacts to Franklin getting within range
PROC ES_Player_Detected()
	
	SWITCH eventSubStage

		CASE SUBSTAGE_SETUP
			
			CPRINTLN(DEBUG_AMBIENT, "Stage: ES_Franklin_Detected")
			IF HAS_SHOWROOM_INTERIOR_LOADED()		
				// Next stage
				eventSubStage = SUBSTAGE_RUN
			ENDIF
			
			CHECK_TO_PUT_SIMEONS_PHONE_AWAY()
			
		BREAK
		
		CASE SUBSTAGE_RUN
			
			CHECK_TO_PUT_SIMEONS_PHONE_AWAY()
			//UPDATE_CHASE_BLIP(mYetarianBlip, mYetarian, 125, 0.8)
			//HANDLE_DOOR()

//			IF IS_DOOR_REGISTERED_WITH_SYSTEM(iYetarianOfficeDoor)
//				IF DOOR_SYSTEM_GET_OPEN_RATIO(iYetarianOfficeDoor) > -1.0
//					fDoorRatio = DOOR_SYSTEM_GET_OPEN_RATIO(iYetarianOfficeDoor) - 0.1			
//					DOOR_SYSTEM_SET_OPEN_RATIO(iYetarianOfficeDoor, fDoorRatio, false, false)
//					DOOR_SYSTEM_SET_DOOR_STATE(iYetarianOfficeDoor, DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA, FALSE, TRUE)
//					CPRINTLN(DEBUG_AMBIENT, "Opening office door... ratio: ", fDoorRatio)
//				ENDIF
//			ENDIF
			
			IF IS_ENTITY_ALIVE(mYetarian)
			
				IF YETARIAN_FINISHED_CHASING()		
					
					IF NOT bIsFleeing
						CPRINTLN(DEBUG_AMBIENT, "Yetarian has finished chasing")
						bCanAttack = TRUE
						bOpenDoor = TRUE
						CLEAR_PED_TASKS(mYetarian)
						
						IF DOES_ENTITY_EXIST(oiPhone)
							CPRINTLN(DEBUG_AMBIENT, "Panic delete Simeon's phone!")
							DELETE_OBJECT(oiPhone)
						ENDIF
						
						//SET_PED_COMBAT_ATTRIBUTES(mYetarian, CA_USE_COVER, TRUE)
						
						// Check if the player has entered the showroom through the car park doors - Simeon will have to flee differently if this is the case
						IF NOT bUsedSideDoor
							// If we're panicing, make Simeon not stop but instantly flee instead
							IF NOT bIsPanic
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
									TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1, FALSE)
								CLOSE_SEQUENCE_TASK(seq)
							ELSE
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1, FALSE)
								CLOSE_SEQUENCE_TASK(seq)
							ENDIF
							TASK_PERFORM_SEQUENCE(mYetarian, seq)
							CLEAR_SEQUENCE_TASK(seq)
							bIsFleeing = TRUE
						ELSE
							IF NOT bIsPanic
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
									IF GET_DISTANCE_BETWEEN_ENTITIES(mYetarian, PLAYER_PED_ID()) <= 4
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 1500)
									ENDIF
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-33.32, -1098.04, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-44.31, -1094.10, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-54.01, -1094.59, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1, TRUE)
								CLOSE_SEQUENCE_TASK(seq)
							ELSE
								OPEN_SEQUENCE_TASK(seq)
									TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-33.32, -1098.04, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-44.31, -1094.10, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-54.01, -1094.59, 25.42>>, PEDMOVEBLENDRATIO_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 150, -1, TRUE)
								CLOSE_SEQUENCE_TASK(seq)
							ENDIF
							TASK_PERFORM_SEQUENCE(mYetarian, seq)
							CLEAR_SEQUENCE_TASK(seq)
							bIsFleeing = TRUE
						ENDIF
					ELSE
						DO_GOON_LINE()
						SET_PED_MAX_MOVE_BLEND_RATIO(mYetarian, PEDMOVE_RUN)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) >= 100
						AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(mYetarian, FALSE), 3)
							CPRINTLN(DEBUG_AMBIENT, "Simeon has got too far away, delete him")
							DELETE_PED(mYetarian)
						ENDIF
						IF mGoon[0].mState >= GS_CLEANUP
						AND mGoon[1].mState >= GS_CLEANUP
							Mission_Passed()
						ENDIF
					ENDIF
				ELSE
					DO_GOON_LINE()
					SET_PED_MAX_MOVE_BLEND_RATIO(mYetarian, PEDMOVE_RUN)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(mYetarian, FALSE)) >= 100
					AND NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(mYetarian, FALSE), 3)
						CPRINTLN(DEBUG_AMBIENT, "Simeon has got too far away, delete him")
						DELETE_PED(mYetarian)
					ENDIF
					IF mGoon[0].mState >= GS_CLEANUP
					AND mGoon[1].mState >= GS_CLEANUP
						IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-69.623955,-1087.369507,24.598333>>, <<-11.278607,-1108.279785,34.172050>>, 47.5)
							Mission_Passed()
						ELSE
							CPRINTLN(DEBUG_AMBIENT, "Waiting for player to leave dealership area before passing")
						ENDIF
					ENDIF
					IF IS_ENTITY_ALIVE(mYetarian)
						SET_PED_RESET_FLAG(mYetarian, PRF_SearchForClosestDoor, TRUE )
					ENDIF
				ENDIF
				
			ELSE
				IF DOES_BLIP_EXIST(mYetarianBlip)
					REMOVE_BLIP(mYetarianBlip)
				ENDIF
				IF DOES_ENTITY_EXIST(mYetarian)
					IF IS_ENTITY_DEAD(mYetarian)
						IF NOT bSimeonDead
							bSimeonDead = TRUE
						ENDIF
					ENDIF
				ENDIF
				bCanAttack = TRUE
				IF mGoon[0].mState >= GS_CLEANUP
				AND mGoon[1].mState >= GS_CLEANUP
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-69.623955,-1087.369507,24.598333>>, <<-11.278607,-1108.279785,34.172050>>, 47.5)
						Mission_Passed()
					ELSE
						CPRINTLN(DEBUG_AMBIENT, "Waiting for player to leave dealership area before passing")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "Waiting for goons to die...")
				ENDIF
			ENDIF
														
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT (coords_struct in_coords)

	CPRINTLN(DEBUG_AMBIENT, "Launched: re_yetarian")	
	DUMMY_REFERENCE_VECTOR(in_coords.vec_coord[0])
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		CPRINTLN(DEBUG_AMBIENT, "Force cleanup: re_yetarian")
		Mission_Failed()
	ENDIF
	
	// Added run checks command here all the run checks for this script are handled in the headers "random_events_public.sch" and "random_event_private.sch"
	// Any bugs you receive for the RE not launching or changes to the requirements to run give me a shout - Paul D
	IF CAN_RANDOM_EVENT_LAUNCH(in_coords.vec_coord[0], RE_SIMEONYETARIAN, DEFAULT)
		LAUNCH_RANDOM_EVENT(RE_SIMEONYETARIAN)
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// All systems go!
	CPRINTLN(DEBUG_AMBIENT, "Activated: re_yetarian")
	
	// Initialisation
	InitRandomEvent()
	
	// Main loop
	WHILE (TRUE)
	
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
				Mission_Cleanup()
			ENDIF
		ENDIF
		
		IF DOES_SCENARIO_GROUP_EXIST("DEALERSHIP") 
			IF IS_SCENARIO_GROUP_ENABLED("DEALERSHIP") 
				SET_SCENARIO_GROUP_ENABLED("DEALERSHIP",FALSE)
				CPRINTLN(DEBUG_AMBIENT, "Disabled Dealership scenarios")
			ENDIF
		ENDIF
		
		FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 20.0
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vYetarianSpawnFront) < (fReplayBlockRange*fReplayBlockRange)	//30.0f replay recording range
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2238920
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vYetarianSpawnBack) < (fReplayBlockRange*fReplayBlockRange)	//30.0f replay recording range
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2238920
		ENDIF
		
		// Are we still within range of this event?
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			
			INT i = 0
			REPEAT MAX_GOONS i 
				MONITOR_GOON_STATES(i)
			ENDREPEAT
			
			SWITCH eventStage
				
				// Load models and create peds
				CASE STAGE_SCENE_SETUP
					ES_Scene_Setup()
				BREAK
				
				// Wait for player to approach showroom
				CASE STAGE_APPROACH_SHOWROOM
					ES_Approach_Showroom()
				BREAK
				
				// Simeon spots Franklin/Michael
				CASE STAGE_PLAYER_DETECTED
					ES_Player_Detected()
				BREAK
											
			ENDSWITCH
		ELSE
			IF bSimeonSpotted // If we're out of brain range but Simeon has seen you (i.e. the event is still in progress), keep running things 
				INT i = 0
				REPEAT MAX_GOONS i 
					MONITOR_GOON_STATES(i)
				ENDREPEAT
				
				SWITCH eventStage
					
					// Load models and create peds
					CASE STAGE_SCENE_SETUP
						ES_Scene_Setup()
					BREAK
					
					// Wait for player to approach showroom
					CASE STAGE_APPROACH_SHOWROOM
						ES_Approach_Showroom()
					BREAK
					
					// Simeon spots Franklin/Michael
					CASE STAGE_PLAYER_DETECTED
						ES_Player_Detected()
					BREAK
												
				ENDSWITCH
				IF IS_ENTITY_ALIVE(mYetarian)
					IF GET_DISTANCE_BETWEEN_ENTITIES(mYetarian, PLAYER_PED_ID()) > 120
						CPRINTLN(DEBUG_AMBIENT, "Cleanup: Player went out of range and Simeon is too far away")
						Mission_Cleanup()
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "Cleanup: Player went out of range and event has not begun")
				Mission_Cleanup()
			ENDIF
		ENDIF
		
		// Debug skips
		#IF IS_DEBUG_BUILD
			DEBUG_Check_Debug_Keys()
		#ENDIF
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
