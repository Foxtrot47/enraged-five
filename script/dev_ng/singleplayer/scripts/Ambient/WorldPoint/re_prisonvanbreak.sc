//////////////////////////////////////////////////////////
// 				Country side robbery
//		   		Kevin Wong
//
//			1

//
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///    there
///    
///    
///    
///    
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING"script_PLAYER.sch"
USING "commands_event.sch"
USING "rage_builtins.sch"
USING"commands_path.sch"
USING "Ambient_Speech.sch"
USING "Ambient_Common.sch"
USING "script_blips.sch"
USING "completionpercentage_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"
// 1936 3711 32


///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////
ENUM prisonvanbreakStageFlag
	prisonvanbreakInit,
	prisonvanbreakWaitForPlayer,
	
	prisonvanbreakCleanup
ENDENUM

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun


BOOL bAssetsLoaded, bDoFullCleanUp

BOOL has_player_attacked_cops
BOOL has_player_attacked_robbers
BOOL robbers_bliped_friendly_and_untargeted
BOOL robbers_bliped_enemy_and_targeted
BOOL cops_bliped_enemy_and_targeted

structPedsForConversation cultresConversation
VECTOR vInput
//VECTOR voffset_temp
//INT random_int
CONST_INT MAX_NUM_COPS 3

PED_INDEX ped_prisoner, ped_prisoner1, ped_ambulance1, ped_ambulance2
PED_INDEX ped_cops[MAX_NUM_COPS]
PED_INDEX ped_injured_police
PED_INDEX ped_players_free_aim_target
VEHICLE_INDEX police_cars[3], vehicle_getaway, vehicle_ambulance

//VEHICLE_INDEX veh_police_heli
//PED_INDEX ped_heli_cop0, ped_heli_cop1 


//INCIDENT_INDEX ambulance_index
//INCIDENT_INDEX helicopter_incident
INT free_aim_counter = 0
INT free_aim_counter_robber1 = 0
INT free_aim_counter_robber0 = 0
BOOL is_player_free_aiming_at_crooks1
BOOL is_player_free_aiming_at_crooks2
INT iNumberOfTimesTyreShot[16]
BOOL bBriefcaseCreated
BOOL bBriefcaseCreated1
BOOL bWarnedAboutBriefcases
BOOL bToldToJoinOrLeave
BOOL make_crooks_enter_car
INT crooks_get_away_variation
BOOL cops_investigate_crooks_are_all_dead

REL_GROUP_HASH prisonerGroup, copsGroup
//REL_GROUP_HASH ambulance_group
SEQUENCE_INDEX seq_temp
ENTITY_INDEX free_aim_target_entity
//VECTOR coords_questionmark_blip
VECTOR temp_coords //, temp_coords2
BOOL bLeavingActivationArea
//BOOL change_blip_to_blue_short_range = TRUE
BOOL remove_blip_1
BOOL remove_blip_0
BOOL prision1bliphasbeencreated
BOOL prision2bliphasbeencreated
BOOL remove_cop0_voice
BOOL bInitialDialoguePlayed
//BOOL flag_remove_victim_blip
BOOL flag_player_given_money_by_thieves
BOOL flag_make_crooks_attack_player_during_getaway
INT dialogue_when_player_arrives = 0
//VECTOR offset_vector
BOOL have_crooks_been_given_escape_tasks
BOOL have_crooks_been_given_flee_on_foot_tasks
//INT counter_player_aiming_at_cops = 0
BOOL is_player_aiming_at_a_cop
BOOL cops_warn_player_not_to_aim_at_them
//BOOL cops_put_guns_away
BOOL bPrisonerJacking
BOOL bCollectedBriefcase0, bCollectedBriefcase1

// NOTE: HAd to add this flag. You were calling SET_ROADS_BACK_TO_ORIGINAL when cleaning up this script due to
// another mission running. However, you'd do this even if you never disabled the roads, which messed the roads up for the mission 
// (in this case, Triathlon). Should be a pretty transparent fix.

VEHICLE_INDEX crooks_temp_car
PICKUP_INDEX piBriefcasePickup[2]
BLIP_INDEX blip_prisoner0, blip_prisoner1, blip_cops[MAX_NUM_COPS], blip_case[2]//, blip_person_getting_players_attention


///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
PROC missionCleanup()
	PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@@@@@@\n")
	
	IF bDoFullCleanUp
		//SET_ROADS_BACK_TO_ORIGINAL(<<1917.13,3678.7,27.21>>, <<2020.0,37.97,72.8>>) 
		//REMOVE_BLIP(blip_prisoner1)
		PRINTSTRING("\n country side rob has cleaned up \n")
		REMOVE_BLIP(blip_prisoner0)
		REMOVE_BLIP(blip_prisoner1)
		
		SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE) 
		SET_WANTED_LEVEL_MULTIPLIER(1) 
		RESET_DISPATCH_IDEAL_SPAWN_DISTANCE()
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		
//		SET_ROADS_BACK_TO_ORIGINAL(<< 218.13, 2549.7, -27.21 >>, << 577.0, 2888.97, 50.8 >>)
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<< 418.299561, 2650.596680, 42.847225 >>, << 297.898132, 2617.968262, 53.641197 >>, 49.6875)
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 328.0555, 2612.6938, 43.4932 >> - << 10, 10, 10 >>, << 328.0555, 2612.6938, 43.4932 >> + << 10, 10, 10 >>, TRUE)
		
		INT index
		REPEAT COUNT_OF(ped_cops) index
			IF NOT IS_PED_INJURED(ped_cops[index])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_cops[index], FALSE)
				IF IS_PED_INJURED(PLAYER_PED_ID())
	//				TASK_SEEK_COVER_FROM_POS(ped_cops[index], << 324.4726, 2634.0288, 43.5553 >>, 10000)
	//				TASK_COMBAT_HATED_TARGETS_IN_AREA(ped_cops[index], << 324.4726, 2634.0288, 43.5553 >>, 10000)
	//				TASK_STAND_STILL(ped_cops[index], 10000)
	//				TASK_STAND_GUARD
					TASK_STAY_IN_COVER(ped_cops[index])
					SET_PED_KEEP_TASK(ped_cops[index], TRUE)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner)
				SET_PED_CONFIG_FLAG(ped_prisoner, PCF_CanSayFollowedByPlayerAudio, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, FALSE)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle_getaway)
					IF IS_PED_IN_VEHICLE(ped_prisoner, vehicle_getaway)
						SET_DISABLE_PRETEND_OCCUPANTS(vehicle_getaway, TRUE)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner1)
				SET_PED_CONFIG_FLAG(ped_prisoner1, PCF_CanSayFollowedByPlayerAudio, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, FALSE)
				IF NOT IS_VEHICLE_DRIVEABLE(vehicle_getaway)
					IF IS_PED_IN_VEHICLE(ped_prisoner1, vehicle_getaway)
						SET_DISABLE_PRETEND_OCCUPANTS(vehicle_getaway, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ MISSION PASSED @@@@@@@@@@@@@@@@@@\n")
	//CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	IF NOT has_player_attacked_cops
	
	ELSE
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Lose your wanted level", 4000, 1) //
	ENDIF
	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		WAIT(0)
	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@@@@ MISSION FAILED @@@@@@@@@@@@@@@@@@\n")
	missionCleanup()
ENDPROC

PROC maintain_debug()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		//	is_mission_in_progress = FALSE
			MISSION_PASSED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		//	is_mission_in_progress = FALSE
			MISSION_FAILED()
		ENDIF
	#ENDIF
ENDPROC

PROC create_assets()
	
//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 50, 50, 50 >>, FALSE)
		
		REQUEST_MODEL(S_M_Y_Ranger_01) //drug buyer
		REQUEST_MODEL(A_M_Y_GenStreet_01) // prisonvanbreak
		REQUEST_MODEL(S_M_M_Paramedic_01)
		REQUEST_MODEL(SHERIFF)
		REQUEST_MODEL(PHOENIX)
		IF HAS_MODEL_LOADED(S_M_Y_Ranger_01)
		AND HAS_MODEL_LOADED(A_M_Y_GenStreet_01)
		AND HAS_MODEL_LOADED(S_M_M_Paramedic_01)
		AND HAS_MODEL_LOADED(SHERIFF)
		AND HAS_MODEL_LOADED(PHOENIX)
			
			//CREATE_INCIDENT(DT_POLICE_HELICOPTER, <<307.61,2590.97,71.88>>, 2, 30.0, helicopter_incident) 
			
			SET_VEHICLE_MODEL_IS_SUPPRESSED(PHOENIX, TRUE)
			SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), TRUE) 
			SET_WANTED_LEVEL_MULTIPLIER(0) 
			
//			SET_ROADS_IN_AREA(<< 218.13, 2549.7, -27.21 >>, << 577.0, 2888.97, 50.8 >>, FALSE)
			ADD_SCENARIO_BLOCKING_AREA(<< 335.22, 2630.83, 44.49 >> - << 50, 50, 20 >>, << 335.22, 2630.83, 44.49 >> + << 50, 50, 20 >>)
			SET_ROADS_IN_ANGLED_AREA(<< 418.299561, 2650.596680, 42.847225 >>, << 297.898132, 2617.968262, 53.641197 >>, 49.6875, FALSE, FALSE)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 328.0555, 2612.6938, 43.4932 >> - << 10, 10, 10 >>, << 328.0555, 2612.6938, 43.4932 >> + << 10, 10, 10 >>, FALSE)
			CLEAR_AREA_OF_VEHICLES(<< 328.0555, 2612.6938, 43.4932 >>, 5)
	//		CLEAR_AREA(<< 335.22, 2630.83, 44.49 >>, 100, TRUE)
			
	//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2041.0, 2778.0, 50.0>>,<<100.0,100.0,100.0>>, FALSE) //casino
	//			ped_prisoner1 = CREATE_PED(PEDTYPE_DEALER,A_M_Y_GenStreet_01, <<2044.27,2784.69,49.3>>, 0.0)
	//			ped_prisoner = CREATE_PED(PEDTYPE_DEALER,A_M_Y_GenStreet_01, <<2043.76,2783.6,49.1>>, 0.0)
	//			ped_injured_police = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<2042.08,2777.4,49.77>>, 0.0)
	//			//create cops
	//			
	//			ped_cops[0] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, << 2067.7258, 2794.8870, 49.2054 >> ,144.9793)
	//			ped_cops[1] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, << 2057.2383, 2770.4780, 49.3553 >> , 356.5678)
	//			ped_cops[2] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, << 2053.6780, 2768.4287, 49.3553 >>,1.4061 )
	//			ped_cops[3] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, << 2033.1162, 2763.0991, 49.2054 >> , 283.7395)
	//			police_cars[0] = CREATE_VEHICLE(POLICE,<< 2055.1328, 2769.8386, 49.3553 >> ,73.6297   )
	//			police_cars[1] = CREATE_VEHICLE(POLICE, << 2025.6630, 2761.1541, 49.2054 >> , 65.5308 )
	//			police_cars[2] = CREATE_VEHICLE(POLICE,<< 2064.5740, 2794.9443, 49.2054 >>,80.1532 )
	//			vehicle_getaway = CREATE_VEHICLE(PHOENIX,<< 2045.3262, 2782.4358, 49.2054 >>, 303.7491 )
	//			
	//			
	//		ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1955.27,3769.69,31.3>>,<<100.0,100.0,100.0>>, FALSE) //country market
	//			ped_prisoner1 = CREATE_PED(PEDTYPE_DEALER,A_M_Y_GenStreet_01, <<1955.27,3769.69,31.3>>, 0.0)
	//			ped_prisoner = CREATE_PED(PEDTYPE_DEALER,A_M_Y_GenStreet_01, <<1951.46,3766.3,31.1>>, 0.0)
	//			ped_injured_police = CREATE_PED(PEDTYPE_CIVFEMALE,S_M_Y_Ranger_01, <<1955.08,3775.4,32.77>>, 0.0)
	//			//create cops
	//			ped_cops[0] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<1960.34,3789.07,31.35>>, 0.0)
	//			ped_cops[1] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<1956.4,3791.96,31.37>>, 0.0)
	//			ped_cops[2] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<1934.95,3777.66,31.24>>, 0.0)
	//			ped_cops[3] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<1937.08,3773.4,31.77>>, 0.0)
	//			
	//			police_cars[0] = CREATE_VEHICLE(POLICE,<< 1958.2401, 3789.0149, 31.3334 >>, 29.9436 )
	//			police_cars[1] = CREATE_VEHICLE(POLICE, << 1937.6355, 3776.5942, 31.1807 >>, 241.9436 )
	//			police_cars[2] = CREATE_VEHICLE(POLICE,<< 1939.3207, 3771.0210, 31.1569 >>, 12.9436 )
	//			vehicle_getaway = CREATE_VEHICLE(PHOENIX,<< 1952.1356, 3769.3367, 31.1145  >>, 345.9599 )
	//			
	//			ELIF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<324.42,2627.84,44.5>>,<<100.0,100.0,100.0>>, FALSE) //country gas store
				
				vehicle_getaway = CREATE_VEHICLE(PHOENIX, << 326.78, 2626.56, 43.5081 >>, 45.21)
				police_cars[0] = CREATE_VEHICLE(SHERIFF, << 314.1008, 2641.6704, 43.5055 >>, 258.9436)
				police_cars[1] = CREATE_VEHICLE(SHERIFF, << 330.7097, 2646.9126, 43.6850 >>, 120.9436)
				police_cars[2] = CREATE_VEHICLE(SHERIFF, << 321.4496, 2649.3877, 43.5706 >>, 105.9436)
				
				SET_SIREN_WITH_NO_DRIVER(police_cars[2], TRUE)
				SET_SIREN_WITH_NO_DRIVER(police_cars[1], TRUE)
				SET_SIREN_WITH_NO_DRIVER(police_cars[0], TRUE)
				
				SET_VEHICLE_SIREN(police_cars[2], TRUE)
				SET_VEHICLE_SIREN(police_cars[1], TRUE)
				SET_VEHICLE_SIREN(police_cars[0], TRUE)
				SET_VEHICLE_HAS_MUTED_SIRENS(police_cars[0], TRUE)
				SET_VEHICLE_HAS_MUTED_SIRENS(police_cars[2], TRUE)
				SET_VEHICLE_HAS_MUTED_SIRENS(police_cars[1], TRUE)
				
				SET_VEHICLE_PROVIDES_COVER(vehicle_getaway, TRUE)
				SET_VEHICLE_PROVIDES_COVER(police_cars[0], TRUE)
				SET_VEHICLE_PROVIDES_COVER(police_cars[1], TRUE)
				SET_VEHICLE_PROVIDES_COVER(police_cars[2], TRUE)
				
				//veh_police_heli = CREATE_VEHICLE(polmav,<< 326.78, 2626.56, 43.5081  >>, 45.21 )
				
				
				ped_prisoner = CREATE_PED(PEDTYPE_DEALER, A_M_Y_GenStreet_01, << 321.31, 2615.66, 43.4963 >>, 346)
				ped_prisoner1 = CREATE_PED(PEDTYPE_DEALER, A_M_Y_GenStreet_01, << 326.25, 2624.51, 43.4846 >>, 341)
				ped_injured_police = CREATE_PED(PEDTYPE_COP, S_M_Y_Ranger_01, << 325.3731, 2631.1597, 43.5248 >>, 87)
				#IF IS_DEBUG_BUILD
					SET_PED_NAME_DEBUG(ped_prisoner, "ped_prisoner")
					SET_PED_NAME_DEBUG(ped_prisoner1, "ped_prisoner1")
				#ENDIF
				
				//create cops
				ped_cops[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_Ranger_01, << 310.6203, 2642.8538, 43.5173 >>, 226)
				ped_cops[1] = CREATE_PED(PEDTYPE_COP, S_M_Y_Ranger_01, << 319.5652, 2650.0669, 43.5411 >>, 189)
				ped_cops[2] = CREATE_PED(PEDTYPE_COP, S_M_Y_Ranger_01, << 324.0396, 2650.9636, 43.5983 >>, 214)
				SET_PED_NAME_DEBUG(ped_cops[0], "ped_cops[0]")
				SET_PED_NAME_DEBUG(ped_cops[1], "ped_cops[1]")
				SET_PED_NAME_DEBUG(ped_cops[2], "ped_cops[2]")
				//ped_cops[3] = CREATE_PED(PEDTYPE_CIVMALE,S_M_Y_Ranger_01, <<333.7188, 2647.7195, 43.7110>>, 145.0)
				SET_PED_USING_ACTION_MODE(ped_cops[0], TRUE)
				SET_PED_USING_ACTION_MODE(ped_cops[1], TRUE)
				SET_PED_USING_ACTION_MODE(ped_cops[2], TRUE)
				
				SET_PED_SPHERE_DEFENSIVE_AREA(ped_prisoner1, << 326.25, 2624.51, 43.4846 >>, 3.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(ped_prisoner, << 321.31, 2615.66, 43.4963 >>, 3.5)
				
				SET_PED_SPHERE_DEFENSIVE_AREA(ped_cops[0], << 310.6203, 2642.8538, 43.5173 >>, 3.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(ped_cops[1], << 319.5652, 2650.0669, 43.5411 >>, 3.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(ped_cops[2], << 324.0396, 2650.9636, 43.5983 >>, 3.5)
				
				TASK_STAY_IN_COVER(ped_cops[0])
				TASK_STAY_IN_COVER(ped_cops[1])
				TASK_STAY_IN_COVER(ped_cops[2])
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[0], CA_USE_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[1], CA_USE_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[2], CA_USE_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[0], CA_USE_VEHICLE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[1], CA_USE_VEHICLE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_cops[2], CA_USE_VEHICLE, TRUE)
				SET_PED_ACCURACY(ped_cops[0], 13)
				SET_PED_ACCURACY(ped_cops[1], 13)
				SET_PED_ACCURACY(ped_cops[2], 13)
				
				SET_PED_COMBAT_ATTRIBUTES(ped_prisoner1, CA_USE_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_prisoner, CA_USE_COVER, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_prisoner1, CA_USE_VEHICLE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_prisoner, CA_USE_VEHICLE, TRUE)
				
				//SET_PED_MONEY()
			//ENDIF
			
			SET_VEHICLE_DOOR_OPEN(police_cars[0], SC_DOOR_FRONT_LEFT)
			SET_VEHICLE_DOOR_OPEN(police_cars[0], SC_DOOR_FRONT_RIGHT)
			SET_VEHICLE_DOOR_OPEN(police_cars[1], SC_DOOR_FRONT_RIGHT)
			SET_VEHICLE_DOOR_OPEN(police_cars[1], SC_DOOR_FRONT_LEFT)
			SET_VEHICLE_DOOR_OPEN(police_cars[2], SC_DOOR_FRONT_RIGHT)
			SET_VEHICLE_DOOR_OPEN(police_cars[2], SC_DOOR_FRONT_LEFT)
			//police_cars[3] = CREATE_VEHICLE(POLICE,<< 1975.5317, 3727.8120, 31.3164 >>, 51.9436 )
	//		coords_questionmark_blip = GET_ENTITY_COORDS(ped_cops[0])
			
			//SET_VEHICLE_DOOR_OPEN(vehicle_getaway, SC_DOOR_REAR_LEFT) 
			//SET_VEHICLE_DOOR_OPEN(vehicle_getaway, SC_DOOR_REAR_RIGHT) 
			SET_VEHICLE_DOOR_OPEN(vehicle_getaway, SC_DOOR_FRONT_RIGHT) 
			SET_VEHICLE_DOOR_OPEN(vehicle_getaway, SC_DOOR_FRONT_LEFT)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehicle_getaway, TRUE)
		//	SET_ENTITY_QUATERNION(vehicle_getaway, -77.57, -88.2955, -43.1590, 0.0)
			//FREEZE_ENTITY_POSITION(vehicle_getaway, true)
			
			//voffset_temp.x = vInput.x - 0.7
		//	Voffset_temp.y = vInput.y - 0.5
		//	voffset_temp.z = vInput.z -0.5
			ADD_RELATIONSHIP_GROUP("robbers", prisonerGroup)
			ADD_RELATIONSHIP_GROUP("cops", copsGroup)
			//ADD_RELATIONSHIP_GROUP("ambulance", ambulance_group) 
			
			SET_ENTITY_HEALTH(ped_injured_police, 0)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(ped_prisoner1, prisonerGroup)
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner1, FALSE, copsGroup)
			SET_ENTITY_HEALTH(ped_prisoner1, 300)
			GIVE_WEAPON_TO_PED(ped_prisoner1, WEAPONTYPE_PISTOL, 200, TRUE)
			SET_PED_SEEING_RANGE(ped_prisoner1, 100) 
			SET_PED_HEARING_RANGE(ped_prisoner1, 100) 
			TASK_GUARD_CURRENT_POSITION(ped_prisoner1,  35, 35, TRUE)
			SET_PED_ACCURACY(ped_prisoner1, 13)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(ped_prisoner, prisonerGroup)
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner, FALSE, copsGroup)
			SET_ENTITY_HEALTH(ped_prisoner, 300)
			GIVE_WEAPON_TO_PED(ped_prisoner, WEAPONTYPE_PISTOL, 200, TRUE)
			SET_PED_SEEING_RANGE(ped_prisoner, 100)
			SET_PED_HEARING_RANGE(ped_prisoner, 100)
			TASK_GUARD_CURRENT_POSITION(ped_prisoner,  35, 35, TRUE)
			SET_PED_ACCURACY(ped_prisoner, 13)
			
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_cops[0], FALSE, prisonerGroup)
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_cops[1], FALSE, prisonerGroup)
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_cops[2], FALSE, prisonerGroup)
			
			GIVE_WEAPON_TO_PED(ped_cops[0], WEAPONTYPE_PUMPSHOTGUN, 200, TRUE)
			GIVE_WEAPON_TO_PED(ped_cops[1], WEAPONTYPE_PISTOL, 200, TRUE)
			GIVE_WEAPON_TO_PED(ped_cops[2], WEAPONTYPE_PUMPSHOTGUN, 200, TRUE)
		//	GIVE_WEAPON_TO_PED(ped_cops[3], WEAPONTYPE_PISTOL, 200, TRUE)
			
			SET_PED_SEEING_RANGE(ped_cops[0], 100)
			SET_PED_HEARING_RANGE(ped_cops[0], 100)
			SET_PED_SEEING_RANGE(ped_cops[1], 100)
			SET_PED_HEARING_RANGE(ped_cops[1], 100)
			SET_PED_SEEING_RANGE(ped_cops[2], 100)
			SET_PED_HEARING_RANGE(ped_cops[2], 100)
			//SET_PED_SEEING_RANGE(ped_cops[3], 100)
			//SET_PED_HEARING_RANGE(ped_cops[3], 100)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(ped_cops[0], copsGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(ped_cops[1], copsGroup)
			SET_PED_RELATIONSHIP_GROUP_HASH(ped_cops[2], copsGroup)
			//SET_PED_RELATIONSHIP_GROUP_HASH(ped_cops[3], copsGroup)
			
//			blip_person_getting_players_attention = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(ped_prisoner)
	//		REMOVE_BLIP(blip_person_getting_players_attention)
	//		INT index
	//		REPEAT COUNT_OF(ped_cops) index
	//			IF NOT IS_PED_INJURED(ped_cops[index])
	//				IF NOT DOES_BLIP_EXIST(blip_cops[index])
	//					blip_cops[index] = CREATE_BLIP_FOR_PED(ped_cops[index])
	//				ENDIF
	//			ENDIF
	//		ENDREPEAT
	//		IF NOT IS_ENTITY_DEAD(ped_prisoner)
	//		OR NOT IS_PED_INJURED(ped_prisoner)
	//		AND NOT prision1bliphasbeencreated
	////			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner1, TRUE, copsGroup)
	//			blip_prisoner0 = CREATE_BLIP_FOR_PED(ped_prisoner)
	//			remove_blip_0 = FALSE
	//			prision1bliphasbeencreated = TRUE
	//		ENDIF
	//		IF NOT IS_ENTITY_DEAD(ped_prisoner1)
	//		OR NOT IS_PED_INJURED(ped_prisoner1)
	//		AND NOT prision2bliphasbeencreated
	////			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner1, TRUE, copsGroup)
	//			blip_prisoner1 = CREATE_BLIP_FOR_PED(ped_prisoner1)
	//			remove_blip_1 = FALSE
	//			prision2bliphasbeencreated = TRUE
	//		ENDIF
			
	//		SET_BLIP_SPRITE(blip_person_getting_players_attention, RADAR_TRACE_RANDOM_CHARACTER)
			//SET_BLIP_AS_FRIENDLY(blip_person_getting_players_attention, TRUE)
			//SET_BLIP_SCALE(blip_person_getting_players_attention, re_blip_scale)
			
			
			//TASK_TURN_PED_TO_FACE_COORD( ped_cops[0], voffset_temp)
			//TASK_TURN_PED_TO_FACE_COORD( ped_cops[1], voffset_temp)
			//TASK_TURN_PED_TO_FACE_COORD( ped_cops[2], voffset_temp)
			//TASK_TURN_PED_TO_FACE_COORD( ped_cops[3], voffset_temp)
			//TASK_PLAY_ANIM (ped_cops[2], "amb@heart_attack", "f_attack", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
	//		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
	//		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
			ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copsGroup, RELGROUPHASH_COP)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, copsGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copsGroup, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, prisonerGroup, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, prisonerGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, prisonerGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, copsGroup)
			
			TASK_SEEK_COVER_FROM_PED(ped_cops[0], ped_prisoner, 3000)
			TASK_SEEK_COVER_FROM_PED(ped_cops[1], ped_prisoner, 3000)
			TASK_SEEK_COVER_FROM_PED(ped_cops[2], ped_prisoner, 3000)
			//TASK_SEEK_COVER_FROM_PED(ped_cops[3],ped_prisoner,3000)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "MICHAEL")
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
			ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner, "RECSBRobber1")
	//		ADD_PED_FOR_DIALOGUE(cultresConversation, 6, ped_prisoner1, "RECSBRobber2")
			SET_AMBIENT_VOICE_NAME(ped_cops[0], "S_M_Y_RANGER_01_WHITE_FULL_01")
			SET_AMBIENT_VOICE_NAME(ped_prisoner, "G_M_Y_ArmGoon_02_White_Armenian_MINI_01")
			SET_AMBIENT_VOICE_NAME(ped_prisoner, "G_M_Y_ArmGoon_02_White_Armenian_MINI_01")
//			STOP_PED_SPEAKING(ped_cops[0], TRUE)
//			STOP_PED_SPEAKING(ped_cops[1], TRUE)
//			STOP_PED_SPEAKING(ped_cops[2], TRUE)
			bAssetsLoaded = TRUE
		ENDIF
//	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_AN_ATTACK_VEHICLE()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), LAZER)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), BUZZARD)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC make_crooks_give_cash_to_player()
	//IF crooks_get_away_variation = 1 //only ped 1 is alive
		
		IF NOT IS_ENTITY_DEAD(ped_prisoner)
			temp_coords = GET_ENTITY_COORDS(ped_prisoner)
		ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
			temp_coords = GET_ENTITY_COORDS(ped_prisoner1)
		ENDIF
		
		temp_coords.z -=0.11
		
		
			IF NOT IS_ENTITY_DEAD(ped_prisoner)
			//	IF IS_ENTITY_AT_ENTITY(ped_prisoner, PLAYER_PED_ID(), <<5.0,5.0,5.0>>, FALSE)
//				IF IS_PED_IN_VEHICLE(ped_prisoner, vehicle_getaway)
							//create a reward for player
					SETTIMERA(0)
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thanks for helping us out. You get a small cut.", 4000, 1) //
					
//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT (0)
					
					IF NOT has_player_attacked_robbers
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF NOT IS_ENTITY_DEAD(ped_prisoner)
							// "You earned your cut. That cash on the ground is yours. There'll be more heat coming. We need get the hell out of here."
							CREATE_CONVERSATION_FROM_SPECIFIC_LINE(cultresConversation, "recsbau", "recsb_thanxa", "recsb_thanxa_3", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
							// "You earned your cut. That cash on the ground is yours. There'll be more heat coming. We need get the hell out of here."
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
							CREATE_CONVERSATION_FROM_SPECIFIC_LINE(cultresConversation, "recsbau", "recsb_thanxa", "recsb_thanxa_3", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						CREATE_MONEY_PICKUPS(GET_SAFE_PICKUP_COORDS(temp_coords), 1000, 5)
						flag_player_given_money_by_thieves = TRUE
//					ENDIF
					
					
					//CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_DEFAULT_DEBUG, 1000)
					//piBriefcasePickup[0] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE,temp_coords, << -104.0050, 104.6958, -180.0000>>, 0, 1000)
					
					
				//	SET_ENTITY_COLLISION(piBriefcasePickup, TRUE)
				//	IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
					
				//	ENDIF
					
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(ped_prisoner1)
			AND NOT IS_ENTITY_DEAD(vehicle_getaway)
			//	IF IS_ENTITY_AT_ENTITY(ped_prisoner1, PLAYER_PED_ID(), <<5.0,5.0,5.0>>, FALSE)
							//create a reward for player
				IF IS_PED_IN_VEHICLE(ped_prisoner1, vehicle_getaway)
					SETTIMERA(0)
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thanks for helping us out. You get a small cut.", 4000, 1) //
					
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					//WAIT (0)
					IF NOT IS_ENTITY_DEAD(ped_prisoner)
						//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_prisoner, "recsbau", "recsb_thanxa")
					ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
						//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_prisoner1, "recsbau", "recsb_thanxb")
					ENDIF
					
					//CREATE_MONEY_PICKUPS(GET_SAFE_PICKUP_COORDS(temp_coords), 1000, 5)
					//piBriefcasePickup[0] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE,temp_coords,<< -104.0050, 104.6958, -180.0000>>, 0, 1000)
					flag_player_given_money_by_thieves = TRUE
					
				ENDIF
			ENDIF
		
//	ENDIF

ENDPROC

PROC check_if_cops_are_all_dead()

//	IF NOT make_crooks_enter_car
		IF IS_ENTITY_DEAD(ped_cops[0])
		AND IS_ENTITY_DEAD(ped_cops[1])
		AND IS_ENTITY_DEAD(ped_cops[2])
		//AND IS_ENTITY_DEAD(ped_cops[3])
			REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop
			
			PRINTSTRING("\n all cops are deed \n")
			bLeavingActivationArea = TRUE
			make_crooks_enter_car = TRUE
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, prisonerGroup, RELGROUPHASH_PLAYER)
//			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, prisonerGroup, copsGroup)
			
			IF NOT has_player_attacked_robbers
			AND has_player_attacked_cops
		//	IF DOES_BLIP_EXIST(blip_prisoner0)
			//	IF NOT IS_ENTITY_DEAD(ped_prisoner)
					REMOVE_BLIP(blip_prisoner0)
				
				//ENDIF
		//	ENDIF
			
		//	IF DOES_BLIP_EXIST(blip_prisoner1)
			
				//IF NOT IS_ENTITY_DEAD(ped_prisoner1)
					REMOVE_BLIP(blip_prisoner1)
			//	ENDIF
			
			//ENDIF
			ENDIF
			
		//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Thats all the pigs smoked. Lets get out of here.", 4000, 1) //
		/*	IF NOT IS_ENTITY_DEAD(ped_prisoner)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT (0)
					IF NOT IS_ENTITY_DEAD(ped_prisoner)
						PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_prisoner, "recsbau", "recsb_thanxb")
					ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
						PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_prisoner1, "recsbau", "recsb_thanxb")
					ENDIF
					
			ENDIF*/
			IF NOT has_player_attacked_robbers
				IF NOT IS_ENTITY_DEAD(ped_prisoner)
					// "I think that's all of them. What a fucking mess."
					PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "recsbau", "recsb_thanxa", "recsb_thanxa_1", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
					// "I think that's all of them. What a fucking mess."
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
					PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "recsbau", "recsb_thanxa", "recsb_thanxa_1", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehicle_getaway)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle_getaway)
				IF NOT IS_PED_INJURED(ped_prisoner)
				AND NOT IS_PED_HURT(ped_prisoner)//IS_PED_IN_WRITHE
					SET_PED_CURRENT_WEAPON_VISIBLE(ped_prisoner, TRUE)
					//SET_CURRENT_PED_WEAPON(ped_prisoner, WEAPONTYPE_UNARMED,TRUE)
					crooks_get_away_variation = 1 //only ped 1 is alive
					CLEAR_PED_TASKS(ped_prisoner)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, TRUE)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped_prisoner)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle_getaway)
					OPEN_SEQUENCE_TASK(seq_temp)
						//TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1, 3.0)
						//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
						//TASK_PLAY_ANIM (NULL, "amb@BASEJUMP", "PARACHUTE_GIVE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
						TASK_ENTER_VEHICLE(NULL, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_RESUME_IF_INTERRUPTED|ECF_JACK_ANYONE)
						//TASK_SMART_FLEE_COORD(NULL,vInput,200.0,20000,FALSE )
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_prisoner, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					//SET_PED_KEEP_TASK(ped_prisoner, TRUE)
					
					IF NOT IS_PED_INJURED(ped_prisoner1)
					AND NOT IS_PED_HURT(ped_prisoner1)//IS_PED_IN_WRITHE
						CLEAR_PED_TASKS(ped_prisoner1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, TRUE)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped_prisoner1)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle_getaway)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, TRUE)
						TASK_ENTER_VEHICLE(ped_prisoner1, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
						crooks_get_away_variation = 3 //both peds are alive
					ENDIF
					
				ELSE
					IF NOT IS_PED_INJURED(ped_prisoner1)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, TRUE)
						SET_PED_CURRENT_WEAPON_VISIBLE(ped_prisoner1, TRUE)
						//SET_CURRENT_PED_WEAPON(ped_prisoner1, WEAPONTYPE_UNARMED,TRUE)
						CLEAR_PED_TASKS(ped_prisoner1)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, TRUE)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(ped_prisoner1)
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehicle_getaway)
						OPEN_SEQUENCE_TASK(seq_temp)
							//TASK_GO_TO_ENTITY(NULL,PLAYER_PED_ID(),-1, 3.0)
							//TASK_TURN_PED_TO_FACE_ENTITY( NULL,  PLAYER_PED_ID() ) 
							//TASK_PLAY_ANIM (NULL, "amb@BASEJUMP", "PARACHUTE_GIVE", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
							TASK_ENTER_VEHICLE(NULL, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN) 
						//	TASK_SMART_FLEE_COORD(NULL,vInput,200.0,20000,FALSE )
						CLOSE_SEQUENCE_TASK(seq_temp)
						TASK_PERFORM_SEQUENCE(ped_prisoner1, seq_temp)
						CLEAR_SEQUENCE_TASK(seq_temp)
						//SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
						
						
					//	TASK_ENTER_VEHICLE(ped_prisoner1,vehicle_getaway,DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN) 
						crooks_get_away_variation = 2 //only ped 2 is alive
					ENDIF
				ENDIF
				
			ELSE
				crooks_get_away_variation = 4
			ENDIF
			
			
			//create a reward for player
		//	IF NOT IS_ENTITY_DEAD(ped_prisoner)
		//		temp_coords = GET_ENTITY_COORDS(ped_prisoner)
		//	ELIF NOT IS_ENTITY_DEAD(ped_prisoner1)
		//		temp_coords = GET_ENTITY_COORDS(ped_prisoner1)
		//	ENDIF
			
		//	temp_coords.z +=0.2.
		//	CREATE_MONEY_PICKUPS(GET_SAFE_PICKUP_COORDS(temp_coords), 2500, 5)
			
		ENDIF
//	ENDIF
ENDPROC

PROC CHECK_IF_CROOKS_ARE_IN_CAR()
	IF NOT have_crooks_been_given_escape_tasks
	
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<< 418.299561, 2650.596680, 42.847225 >>, << 297.898132, 2617.968262, 53.641197 >>, 49.6875)
		
		IF bPrisonerJacking
			IF NOT IS_PED_INJURED(ped_prisoner)
				IF IS_PED_JACKING(ped_prisoner)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bPrisonerJacking = TRUE
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner1)
				IF IS_PED_JACKING(ped_prisoner1)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5)
					ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bPrisonerJacking = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF crooks_get_away_variation = 1 //only ped 1 is alive
			IF NOT IS_PED_INJURED(ped_prisoner)
			AND NOT IS_ENTITY_DEAD(vehicle_getaway)
				IF IS_PED_IN_VEHICLE(ped_prisoner, vehicle_getaway)
				
				//	TASK_SMART_FLEE_COORD(ped_prisoner,vInput, 200.0, 20000, FALSE)
			//		TASK_VEHICLE_MISSION(ped_prisoner, vehicle_getaway, NULL, MISSION_GOTO, 20, DRIVINGMODE_PLOUGHTHROUGH, 5, 5)
				//	TASK_VEHICLE_DRIVE_WANDER(ped_prisoner, vehicle_getaway, 15, DRIVINGMODE_PLOUGHTHROUGH)
			//		TASK_VEHICLE_MISSION(ped_prisoner, vehicle_getaway, NULL, MISSION_GOTO, 20, DRIVINGMODE_PLOUGHTHROUGH, 5, 5)
					//TASK_GO_TO_COORD_ANY_MEANS(ped_prisoner, <<247.4,2834.6,43.16>>, 0.0, vehicle_getaway)
					//TASK_VEHICLE_MISSION_PED_TARGET(ped_prisoner, vehicle_getaway,PLAYER_PED_ID(),MISSION_FLEE, 15000, DRIVINGMODE_PLOUGHTHROUGH,1000.0, 10.0)
					SET_PED_FLEE_ATTRIBUTES(ped_prisoner, FA_USE_VEHICLE, TRUE)
					OPEN_SEQUENCE_TASK(seq_temp)
//						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehicle_getaway, << 282.4, 3403.6, 37.16 >>, 100, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH,5.0, 10.0)
//						TASK_VEHICLE_DRIVE_WANDER(NULL, vehicle_getaway, 100, DRIVINGMODE_PLOUGHTHROUGH)
//						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehicle_getaway, PLAYER_PED_ID(), MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS, 1000, 10)
					
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_prisoner, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
					//TASK_VEHICLE_MISSION_PED_TARGET(ped_prisoner, vehicle_getaway,PLAYER_PED_ID(),MISSION_FLEE, 15, DRIVINGMODE_PLOUGHTHROUGH,10.0, 10.0)
					SET_PED_KEEP_TASK(ped_prisoner, TRUE)
					//MISSION_PASSED()
					have_crooks_been_given_escape_tasks = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
		IF crooks_get_away_variation = 2 //only ped 2 is alive
			IF NOT IS_PED_INJURED(ped_prisoner1)
			AND NOT IS_ENTITY_DEAD(vehicle_getaway)
				IF IS_PED_IN_VEHICLE(ped_prisoner1, vehicle_getaway)
					//TASK_SMART_FLEE_COORD(ped_prisoner1,vInput,200.0,20000,FALSE )
					//TASK_VEHICLE_DRIVE_WANDER(ped_prisoner1, vehicle_getaway, 15.0, DRIVINGMODE_PLOUGHTHROUGH)
					//TASK_VEHICLE_MISSION_PED_TARGET(ped_prisoner1, vehicle_getaway,PLAYER_PED_ID(),MISSION_FLEE, 20, DRIVINGMODE_PLOUGHTHROUGH,2000.0, 10.0)
					SET_PED_FLEE_ATTRIBUTES(ped_prisoner1, FA_USE_VEHICLE, TRUE)
					OPEN_SEQUENCE_TASK(seq_temp)
//						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehicle_getaway, << 282.4, 3403.6, 37.16 >>, 20, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH,5.0, 10.0)
//						TASK_GO_TO_COORD_ANY_MEANS(NULL, <<247.4,2834.6,43.16>>,0.0,vehicle_getaway)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehicle_getaway, PLAYER_PED_ID(), MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS, 1000, 10)
//						TASK_VEHICLE_DRIVE_WANDER(NULL, vehicle_getaway, 100, DRIVINGMODE_PLOUGHTHROUGH)
//						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_prisoner1, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
					SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
					//MISSION_PASSED()
					have_crooks_been_given_escape_tasks = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF crooks_get_away_variation = 3 //both peds are alive
			IF NOT IS_PED_INJURED(ped_prisoner1)
			AND NOT IS_PED_INJURED(ped_prisoner)
			AND NOT IS_ENTITY_DEAD(vehicle_getaway)
				IF IS_PED_IN_VEHICLE(ped_prisoner, vehicle_getaway)
				AND IS_PED_IN_VEHICLE(ped_prisoner1, vehicle_getaway)
					//TASK_SMART_FLEE_COORD(ped_prisoner,vInput,200.0,20000,FALSE )
					//TASK_VEHICLE_DRIVE_WANDER(ped_prisoner, vehicle_getaway, 15.0, DRIVINGMODE_PLOUGHTHROUGH)
					//TASK_VEHICLE_MISSION_PED_TARGET(ped_prisoner, vehicle_getaway,PLAYER_PED_ID(),MISSION_FLEE, 15, DRIVINGMODE_PLOUGHTHROUGH,1000.0, 10.0)
					SET_PED_FLEE_ATTRIBUTES(ped_prisoner, FA_USE_VEHICLE, TRUE)
					SET_PED_FLEE_ATTRIBUTES(ped_prisoner1, FA_USE_VEHICLE, TRUE)
					OPEN_SEQUENCE_TASK(seq_temp)
//						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehicle_getaway, << 282.4, 3403.6, 37.16 >>, 15, DRIVINGSTYLE_NORMAL, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH,5.0, 10.0)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehicle_getaway, PLAYER_PED_ID(), MISSION_FLEE, 100, DRIVINGMODE_AVOIDCARS, 1000, 10)
//						TASK_VEHICLE_DRIVE_WANDER(NULL, vehicle_getaway, 100, DRIVINGMODE_PLOUGHTHROUGH)
//						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_prisoner, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
//					TASK_SMART_FLEE_PED(ped_prisoner1, PLAYER_PED_ID(), 200, -1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, FALSE)
					
					SET_PED_KEEP_TASK(ped_prisoner, TRUE)
//					SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
					//MISSION_PASSED()
					have_crooks_been_given_escape_tasks = TRUE
				ENDIF
			ENDIF
			
			
			// if ped 0 dies trying to get in, change state to crooks_get_away_variation = 2 and make him get inside car
			
			IF IS_PED_INJURED(ped_prisoner)
			AND NOT IS_PED_INJURED(ped_prisoner1)
				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
			
				TASK_ENTER_VEHICLE(ped_prisoner1, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JACK_ANYONE)
				crooks_get_away_variation = 2
			ENDIF
			
			IF IS_PED_INJURED(ped_prisoner1)
			AND NOT IS_PED_INJURED(ped_prisoner)
						
				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
			//	IF DOES_VEHICLE_HAVE_FREE_SEAT(vehicle_getaway)
			//	DRIVER
				TASK_ENTER_VEHICLE(ped_prisoner, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JACK_ANYONE)
				crooks_get_away_variation = 1
			ENDIF
		ENDIF
		
		IF crooks_get_away_variation = 4 //both peds are alive but if car is destroyed
			IF NOT IS_PED_INJURED(ped_prisoner)
				CLEAR_PED_TASKS(ped_prisoner)
				TASK_SMART_FLEE_COORD(ped_prisoner, vInput, 1000, -1, FALSE)
				SET_PED_KEEP_TASK(ped_prisoner, TRUE) 
			ENDIF
			
			IF NOT IS_PED_INJURED(ped_prisoner1)
				CLEAR_PED_TASKS(ped_prisoner1)
				TASK_SMART_FLEE_COORD(ped_prisoner1, vInput, 1000, -1, FALSE)
				SET_PED_KEEP_TASK(ped_prisoner1, TRUE) 
			ENDIF
			
		//	MISSION_PASSED()
			have_crooks_been_given_escape_tasks = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC make_crooks_flee_if_doors_are_blocked()
	IF NOT IS_PED_INJURED(ped_prisoner)
	AND NOT IS_ENTITY_DEAD(vehicle_getaway)
		IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped_prisoner, vehicle_getaway, VS_DRIVER)
		AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped_prisoner, vehicle_getaway, VS_FRONT_RIGHT)
//			CLEAR_PED_TASKS(ped_prisoner)
			TASK_SMART_FLEE_COORD(ped_prisoner, vInput, 1000, -1, FALSE)
			SET_PED_KEEP_TASK(ped_prisoner, TRUE)
			have_crooks_been_given_flee_on_foot_tasks = TRUE
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(ped_prisoner1)
	AND NOT IS_ENTITY_DEAD(vehicle_getaway)
		IF NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped_prisoner1, vehicle_getaway, VS_DRIVER)
		AND NOT IS_ENTRY_POINT_FOR_SEAT_CLEAR(ped_prisoner1, vehicle_getaway, VS_FRONT_RIGHT)
//			CLEAR_PED_TASKS(ped_prisoner1)
			TASK_SMART_FLEE_COORD(ped_prisoner1, vInput, 1000, -1, FALSE)
			SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
			have_crooks_been_given_flee_on_foot_tasks = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC MAKE_CROOKS_HATE_PLAYER_IF_HE_ATTACKS_AFTER_ESCAPE()

	IF NOT flag_make_crooks_attack_player_during_getaway
		
		IF NOT IS_PED_INJURED(ped_prisoner)
			IF IS_PED_IN_ANY_VEHICLE(ped_prisoner)
				crooks_temp_car = GET_VEHICLE_PED_IS_IN(ped_prisoner)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crooks_temp_car, PLAYER_PED_ID())
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
				OR IS_PED_BEING_JACKED(ped_prisoner)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), crooks_temp_car)
				OR HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), crooks_temp_car, WEAPONTYPE_STICKYBOMB)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, FALSE)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
					PRINTSTRING("\n Crooks attack player now for damaging car \n ")
					IF IS_PED_IN_ANY_VEHICLE(ped_prisoner)
						GIVE_WEAPON_TO_PED(ped_prisoner, WEAPONTYPE_MICROSMG, 200, TRUE)
						SET_CURRENT_PED_WEAPON(ped_prisoner, WEAPONTYPE_MICROSMG, TRUE)
						TASK_DRIVE_BY(ped_prisoner, PLAYER_PED_ID(), NULL, <<0.0, 0.0, 0.0>>, 200, 50)
					ENDIF
					
					IF NOT IS_PED_INJURED(ped_prisoner1)
						IF IS_PED_IN_ANY_VEHICLE(ped_prisoner1)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, FALSE)
							GIVE_WEAPON_TO_PED(ped_prisoner1, WEAPONTYPE_MICROSMG,200, TRUE)
							SET_CURRENT_PED_WEAPON(ped_prisoner1, WEAPONTYPE_MICROSMG, TRUE)
							
							TASK_DRIVE_BY(ped_prisoner1, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
						ENDIF
					ENDIF
					has_player_attacked_robbers = TRUE
					flag_make_crooks_attack_player_during_getaway = TRUE
				ENDIF
			ENDIF
			
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner, PLAYER_PED_ID())
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
				PRINTSTRING("\n Crooks attack player now for damaging prisoner 0 \n ")
				IF IS_PED_IN_ANY_VEHICLE(ped_prisoner)
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(crooks_temp_car)
					IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, FALSE)
						GIVE_WEAPON_TO_PED(ped_prisoner, WEAPONTYPE_MICROSMG, 200, TRUE)
						SET_CURRENT_PED_WEAPON(ped_prisoner, WEAPONTYPE_MICROSMG, TRUE)
						TASK_DRIVE_BY(ped_prisoner, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
						has_player_attacked_robbers = TRUE
						flag_make_crooks_attack_player_during_getaway = TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(ped_prisoner1)
					IF IS_PED_IN_ANY_VEHICLE(ped_prisoner1)
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(crooks_temp_car)
						IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, FALSE)
							GIVE_WEAPON_TO_PED(ped_prisoner1, WEAPONTYPE_MICROSMG, 200, TRUE)
							SET_CURRENT_PED_WEAPON(ped_prisoner1, WEAPONTYPE_MICROSMG, TRUE)
							TASK_DRIVE_BY(ped_prisoner1, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
							has_player_attacked_robbers = TRUE
							flag_make_crooks_attack_player_during_getaway = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(ped_prisoner1)
			IF IS_PED_IN_ANY_VEHICLE(ped_prisoner1)
				crooks_temp_car = GET_VEHICLE_PED_IS_IN(ped_prisoner1)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(crooks_temp_car, PLAYER_PED_ID())
				OR IS_PED_BEING_JACKED(ped_prisoner1)
				OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), crooks_temp_car)
				OR HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), crooks_temp_car, WEAPONTYPE_STICKYBOMB)
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
					PRINTSTRING("\n Crooks attack player now for damaging prisoner car \n ")
					IF NOT IS_PED_INJURED(ped_prisoner)
						IF IS_PED_IN_ANY_VEHICLE(ped_prisoner)
						AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(crooks_temp_car)
							IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner, FALSE)
								GIVE_WEAPON_TO_PED(ped_prisoner, WEAPONTYPE_MICROSMG, 200, TRUE)
								SET_CURRENT_PED_WEAPON(ped_prisoner, WEAPONTYPE_MICROSMG, TRUE)
								TASK_DRIVE_BY(ped_prisoner, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
								has_player_attacked_robbers = TRUE
								flag_make_crooks_attack_player_during_getaway = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_IN_ANY_VEHICLE(ped_prisoner1)
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(crooks_temp_car)
						IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_prisoner1, FALSE)
							GIVE_WEAPON_TO_PED(ped_prisoner1, WEAPONTYPE_MICROSMG,200, TRUE)
							SET_CURRENT_PED_WEAPON(ped_prisoner1, WEAPONTYPE_MICROSMG, TRUE)
							TASK_DRIVE_BY(ped_prisoner1, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
							has_player_attacked_robbers = TRUE
							flag_make_crooks_attack_player_during_getaway = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner1, PLAYER_PED_ID())
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
				PRINTSTRING("\n Crooks attack player now for damaging prisoner 1 \n ")
				IF NOT IS_PED_INJURED(ped_prisoner)
					IF IS_PED_IN_ANY_VEHICLE(ped_prisoner)
					AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_ENTITY_DEAD(crooks_temp_car)
						IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
							GIVE_WEAPON_TO_PED(ped_prisoner, WEAPONTYPE_MICROSMG, 200, TRUE)
							SET_CURRENT_PED_WEAPON(ped_prisoner, WEAPONTYPE_MICROSMG, TRUE)
							TASK_DRIVE_BY(ped_prisoner, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
							has_player_attacked_robbers = TRUE
							flag_make_crooks_attack_player_during_getaway = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(ped_prisoner1)
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(crooks_temp_car)
					IF IS_ENTITY_AT_ENTITY(crooks_temp_car, PLAYER_PED_ID(), << 10, 10, 10 >>, FALSE)
						GIVE_WEAPON_TO_PED(ped_prisoner1, WEAPONTYPE_MICROSMG, 200, TRUE)
						SET_CURRENT_PED_WEAPON(ped_prisoner1, WEAPONTYPE_MICROSMG, TRUE)
						TASK_DRIVE_BY(ped_prisoner1, PLAYER_PED_ID(), NULL, << 0, 0, 0 >>, 200, 50)
						has_player_attacked_robbers = TRUE
						flag_make_crooks_attack_player_during_getaway = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	
	ENDIF
ENDPROC

PROC make_cop_tell_player_to_leave(PED_INDEX ped)

	IF NOT IS_PED_INJURED(ped)
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	AND TIMERA() > 12000
		ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped, "RECSBCop1")
		IF IS_ENTITY_AT_ENTITY(ped, PLAYER_PED_ID(), << 20, 20, 20 >>, FALSE)
			CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_sscene", CONV_PRIORITY_AMBIENT_HIGH)
			SETTIMERA(0)
		ENDIF
	ENDIF
	
ENDPROC

//PROC make_cop_put_away_weapon(PED_INDEX ped)
//
//	IF NOT IS_PED_INJURED(ped)
//		IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
//			
//			cops_put_guns_away = TRUE
//			IF NOT IS_PED_INJURED(ped_cops[0])
//				SET_PED_CURRENT_WEAPON_VISIBLE(ped_cops[0], FALSE)
//			ENDIF
//			IF NOT IS_PED_INJURED(ped_cops[1])
//				SET_PED_CURRENT_WEAPON_VISIBLE(ped_cops[1], FALSE)
//			ENDIF
//			IF NOT IS_PED_INJURED(ped_cops[2])
//				SET_PED_CURRENT_WEAPON_VISIBLE(ped_cops[2], FALSE)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDPROC

PROC DO_DYNAMIC_TYRE_POP(VEHICLE_INDEX vehTarget, INT &iCount, SC_WHEEL_LIST sWheel, FLOAT fTargetRadius)

VECTOR vDamageArea
VECTOR vDamageDirection
SC_WINDOW_LIST windowMatch

	IF NOT IS_ENTITY_DEAD(vehTarget)
		IF iCount < 5
			SWITCH sWheel
				CASE SC_WHEEL_CAR_FRONT_LEFT
					vDamageArea = <<-1, 1, 0.5>>
					vDamageDirection = <<-0.5, 0.75, 0.05>>
					windowMatch = SC_WINDOW_FRONT_LEFT
				BREAK
				CASE SC_WHEEL_CAR_FRONT_RIGHT
					vDamageArea = <<1, 1, 0.5>>
					vDamageDirection = <<0.5, 0.75, 0.05>>
					windowMatch = SC_WINDOW_FRONT_RIGHT
				BREAK
				CASE SC_WHEEL_CAR_REAR_LEFT
					vDamageArea = <<-1, -1, 0.5>>
					vDamageDirection = <<-0.5, -0.75, 0.05>>
					windowMatch = SC_WINDOW_REAR_LEFT
				BREAK
				CASE SC_WHEEL_CAR_REAR_RIGHT
					vDamageArea = <<1, -1, 0.5>>
					vDamageDirection = <<0.5, -0.75, 0.05>>
					windowMatch = SC_WINDOW_REAR_RIGHT
				BREAK
			ENDSWITCH
			
			IF HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTarget, vDamageArea), fTargetRadius)
			OR HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTarget, vDamageArea), fTargetRadius, FALSE)
				SWITCH iCount
					CASE 0
						SET_VEHICLE_TYRE_BURST(vehTarget, sWheel)
						iCount++
					BREAK
					CASE 1
						SMASH_VEHICLE_WINDOW(vehTarget, windowMatch)
						iCount++
					BREAK
					CASE 2
						iCount++
					BREAK
					CASE 3
						iCount++
					BREAK
					CASE 4
						SET_VEHICLE_DAMAGE(vehTarget, vDamageDirection, 150.0, 50.0, TRUE)
						iCount++
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC check_if_player_is_helping_cops()

	IF NOT IS_ENTITY_DEAD(ped_prisoner)
	
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner, PLAYER_PED_ID())
			has_player_attacked_robbers = TRUE
			SETTIMERA(0)
			//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's a civilian helping us take out these crooks!", 4000, 1) //
			PRINTSTRING("\n@@@@@@@@@@@@@@@ ATTACKED PRISONERS @@@@@@@@@@@@@\n")
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
			IF NOT IS_PED_INJURED(ped_prisoner)
				TASK_COMBAT_PED(ped_prisoner, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner, TRUE)
				REGISTER_TARGET(ped_prisoner, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner1)
				TASK_COMBAT_PED(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
				REGISTER_TARGET(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner1, TRUE)
			ENDIF
			
			IF NOT has_player_attacked_cops
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_PED_INJURED(ped_cops[0])
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[1])
					SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[2])
					SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
		ENDIF
		
		
		IF NOT IS_PED_INJURED(ped_prisoner)
			IF NOT IS_PLAYER_IN_AN_ATTACK_VEHICLE()
			AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_prisoner)
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_prisoner)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_prisoner, << 20, 20, 20 >>, FALSE, TRUE)
					AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner, PLAYER_PED_ID())
						free_aim_counter_robber0 ++
						
						IF NOT is_player_free_aiming_at_crooks1
						AND NOT IS_THIS_CONVERSATION_PLAYING("recsb_theaim")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							IF NOT IS_PED_INJURED(ped_prisoner)
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_theaim", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF NOT IS_PED_INJURED(ped_prisoner1)
								REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
								ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_theaim", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							is_player_free_aiming_at_crooks1 = TRUE
						ENDIF
						
						IF free_aim_counter_robber0 > 45
							has_player_attacked_robbers = TRUE
							SETTIMERA(0)
							//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's a civilian helping us take out these crooks!", 4000, 1) //
							//	PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_prisoner, "recsbau", "recsb_theaim")
							IF NOT has_player_attacked_cops
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								WAIT(0)
								IF NOT IS_PED_INJURED(ped_cops[0])
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[1])
									SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[2])
									SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					IF is_player_free_aiming_at_crooks1
					OR is_player_free_aiming_at_crooks2
						free_aim_counter_robber0 = 45
					ELSE
						free_aim_counter_robber0 = 0
					ENDIF
//					is_player_free_aiming_at_crooks1 = FALSE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	
	IF NOT IS_ENTITY_DEAD(ped_prisoner1)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner1, PLAYER_PED_ID())
			has_player_attacked_robbers = TRUE
			SETTIMERA(0)
			//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's a civilian helping us take out these crooks!", 4000, 1) //
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
			IF NOT IS_PED_INJURED(ped_prisoner1)
				TASK_COMBAT_PED(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
				REGISTER_TARGET(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner1, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner)
				TASK_COMBAT_PED(ped_prisoner, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner, TRUE)
				REGISTER_TARGET(ped_prisoner, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner, TRUE)
			ENDIF
			
			IF NOT has_player_attacked_cops
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT IS_PED_INJURED(ped_cops[0])
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[1])
					SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[2])
					SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
			
		ENDIF
		
		IF NOT IS_PED_INJURED(ped_prisoner1)
			IF NOT IS_PLAYER_IN_AN_ATTACK_VEHICLE()
			AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_prisoner1)
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_prisoner1)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_prisoner1, << 20, 20, 20 >>, FALSE, TRUE)
					AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner1, PLAYER_PED_ID())
					
						IF NOT is_player_free_aiming_at_crooks2
						AND NOT IS_PED_INJURED(ped_prisoner)
						AND NOT IS_THIS_CONVERSATION_PLAYING("recsb_theaim")
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
							IF NOT IS_PED_INJURED(ped_prisoner)
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_theaim", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF NOT IS_PED_INJURED(ped_prisoner1)
								REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
								ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_theaim", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							is_player_free_aiming_at_crooks2 = TRUE
						ENDIF
						
						free_aim_counter_robber1++
						IF  free_aim_counter_robber1 > 45
							has_player_attacked_robbers = TRUE
							SETTIMERA(0)
							//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's a civilian helping us take out these crooks!", 4000, 1) //
							IF NOT has_player_attacked_cops
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_cops[0], "recsbau", "recsb_copsth")
								WAIT(0)
								IF NOT IS_PED_INJURED(ped_cops[0])
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[1])
									SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[2])
									SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF is_player_free_aiming_at_crooks1
					OR is_player_free_aiming_at_crooks2
						free_aim_counter_robber0 = 45
					ELSE
						free_aim_counter_robber0 = 0
					ENDIF
//					is_player_free_aiming_at_crooks2 = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	// check if they are being targetted
	
	IF NOT bCollectedBriefcase0
	AND bBriefcaseCreated
		IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
			IF NOT IS_PED_INJURED(ped_prisoner)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner, PLAYER_PED_ID())
				AND IS_ENTITY_AT_ENTITY(ped_prisoner, PLAYER_PED_ID(), << 20, 20, 20 >>)
					has_player_attacked_robbers = TRUE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bCollectedBriefcase0 = TRUE
				ENDIF
			ELIF NOT IS_PED_INJURED(ped_prisoner1)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner1, PLAYER_PED_ID())
				AND IS_ENTITY_AT_ENTITY(ped_prisoner1, PLAYER_PED_ID(), << 20, 20, 20 >>)
					has_player_attacked_robbers = TRUE
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5)
					ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bCollectedBriefcase0 = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT bCollectedBriefcase1
	AND bBriefcaseCreated1
		IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
			IF NOT IS_PED_INJURED(ped_prisoner)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner, PLAYER_PED_ID())
				AND IS_ENTITY_AT_ENTITY(ped_prisoner, PLAYER_PED_ID(), << 20, 20, 20 >>)
					has_player_attacked_robbers = TRUE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bCollectedBriefcase1 = TRUE
				ENDIF
			ELIF NOT IS_PED_INJURED(ped_prisoner1)
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_prisoner1, PLAYER_PED_ID())
				AND IS_ENTITY_AT_ENTITY(ped_prisoner1, PLAYER_PED_ID(), << 20, 20, 20 >>)
					has_player_attacked_robbers = TRUE
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5)
					ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_jack", CONV_PRIORITY_AMBIENT_MEDIUM)
					bCollectedBriefcase1 = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(vehicle_getaway)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle_getaway, PLAYER_PED_ID())
//		AND IS_ENTITY_DEAD(crooks_temp_car)
		OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle_getaway)
		OR HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), vehicle_getaway, WEAPONTYPE_STICKYBOMB)
			has_player_attacked_robbers = TRUE
			SETTIMERA(0)
			//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's a civilian helping us take out these crooks!", 4000, 1) //
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
			IF NOT IS_PED_INJURED(ped_prisoner)
				TASK_COMBAT_PED(ped_prisoner, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner, TRUE)
				REGISTER_TARGET(ped_prisoner, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner1)
				TASK_COMBAT_PED(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
				REGISTER_TARGET(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner1, TRUE)
			ENDIF
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF NOT IS_ENTITY_DEAD(vehicle_getaway)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle_getaway)
					IF NOT IS_PED_INJURED(ped_prisoner)
						// "You're dead, asshole!"
						PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "recsbau", "recsb_deadcr", "recsb_deadcr_1", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_ENTER_VEHICLE(ped_prisoner, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JUST_PULL_PED_OUT|ECF_JACK_ANYONE)
					ELIF NOT IS_PED_INJURED(ped_prisoner1)
						// "You're dead, asshole!"
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
						PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "recsbau", "recsb_deadcr", "recsb_deadcr_1", CONV_PRIORITY_AMBIENT_HIGH)
						TASK_ENTER_VEHICLE(ped_prisoner1, vehicle_getaway, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_JUST_PULL_PED_OUT|ECF_JACK_ANYONE)
					ENDIF
				ELSE
					IF NOT has_player_attacked_cops
						IF NOT IS_PED_INJURED(ped_cops[0])
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[1])
							SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[2])
							SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsth", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC

PROC check_if_player_is_close_bystanders()

	IF NOT IS_PED_INJURED(ped_cops[0])
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(ped_prisoner)
		
		/*
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),ped_cops[0], <<30.5,30.5,30.5>>,FALSE )
		OR  IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),ped_prisoner, <<30.5,30.5,30.5>>,FALSE )
			//offset_vector = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(prisonvanbreak, <<-10, -30.0, 0>>) //Ped placement
			REMOVE_BLIP(blip_person_getting_players_attention)
			IF prision1bliphasbeencreated = FALSE
				IF  NOT IS_ENTITY_DEAD(ped_prisoner)
				OR NOT IS_PED_INJURED(ped_prisoner)
					blip_prisoner0 = ADD_BLIP_FOR_ENTITY(ped_prisoner)
				
					SET_BLIP_SCALE(blip_prisoner0, re_blip_scale)
					remove_blip_0 = FALSE
					prision1bliphasbeencreated = TRUE
				ENDIF
			ENDIF
			
			IF prision2bliphasbeencreated = FALSE
				IF  NOT IS_ENTITY_DEAD(ped_prisoner1)
				OR NOT IS_PED_INJURED(ped_prisoner1)
					blip_prisoner1 = ADD_BLIP_FOR_ENTITY(ped_prisoner1)
					SET_BLIP_SCALE(blip_prisoner1, re_blip_scale)
					remove_blip_1 = FALSE
					prision1bliphasbeencreated = TRUE
				ENDIF
			ENDIF
			
			flag_player_talks_to_bystanders = TRUE
		ENDIF
		*/
		/*
		IF change_blip_to_blue_short_range
		AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),ped_cops[0], <<40.5,40.5,40.5>>,FALSE )
		AND DOES_BLIP_EXIST(blip_person_getting_players_attention)
			change_blip_to_blue_short_range = FALSE
			REMOVE_BLIP(blip_person_getting_players_attention)
			blip_person_getting_players_attention = ADD_BLIP_FOR_ENTITY(ped_cops[0])
			SET_BLIP_AS_FRIENDLY(blip_person_getting_players_attention, TRUE)
			SET_BLIP_SCALE(blip_person_getting_players_attention, re_blip_scale)
			flag_player_talks_to_bystanders = TRUE
			
		ENDIF*/
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << 220.6075, 2615.1452, 35.8465 >>, << 440.8339, 2684.2297, 72.6324 >>, 183.1250)
	AND IS_SPHERE_VISIBLE(vInput, 1)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 325.6556, 2647.0210, 43.6289 >>, << 30, 30, 30 >>)
		//offset_vector = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(prisonvanbreak, <<-10, -30.0, 0>>) //Ped placement
//		REMOVE_BLIP(blip_person_getting_players_attention)
		
		INT index
		REPEAT COUNT_OF(ped_cops) index
			IF NOT IS_PED_INJURED(ped_cops[index])
				SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_cops[index], TRUE, prisonerGroup)
				IF NOT DOES_BLIP_EXIST(blip_cops[index])
					blip_cops[index] = CREATE_BLIP_FOR_PED(ped_cops[index])
					SHOW_HEIGHT_ON_BLIP(blip_cops[index], FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_ENTITY_DEAD(ped_prisoner)
		OR NOT IS_PED_INJURED(ped_prisoner)
		AND NOT prision1bliphasbeencreated
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner, TRUE, copsGroup)
			blip_prisoner0 = CREATE_BLIP_FOR_PED(ped_prisoner)
			SHOW_HEIGHT_ON_BLIP(blip_prisoner0, FALSE)
			remove_blip_0 = FALSE
			prision1bliphasbeencreated = TRUE
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(ped_prisoner1)
		OR NOT IS_PED_INJURED(ped_prisoner1)
		AND NOT prision2bliphasbeencreated
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(ped_prisoner1, TRUE, copsGroup)
			blip_prisoner1 = CREATE_BLIP_FOR_PED(ped_prisoner1)
			SHOW_HEIGHT_ON_BLIP(blip_prisoner1, FALSE)
			remove_blip_1 = FALSE
			prision2bliphasbeencreated = TRUE
		ENDIF
		SETTIMERA(0)
		
//		IF NOT has_player_attacked_cops
//		AND NOT cops_investigate_crooks_are_all_dead
//			//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, PED_COPS[0], "recsbau", "recsb_copswa")
//			IF NOT IS_PED_INJURED(ped_cops[0])
//				ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
//				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF NOT IS_PED_INJURED(ped_cops[1])
//				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
//				ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
//				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
//			ELIF NOT IS_PED_INJURED(ped_cops[2])
//				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
//				ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
//				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
//			ENDIF
//		ENDIF
		
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Get down civilian! Some crooks have robbed a convienet store! They are armed and dangerous", 4000, 1) //
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			SET_RANDOM_EVENT_ACTIVE()
		ENDIF
	ENDIF
	
ENDPROC

PROC can_player_is_close_to_ped_and_play_dialogue(PED_INDEX& ped, STRING linetoplay)
	IF NOT IS_PED_INJURED(ped)
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped, << 4, 4, 4 >>)
			IF NOT has_player_attacked_cops
				ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped, "RECSBCop1")
				CREATE_CONVERSATION(cultresConversation, "recsbau", linetoplay, CONV_PRIORITY_AMBIENT_HIGH)
//				SET_PED_STEALTH_MOVEMENT(ped, TRUE, "default_action")
				TASK_LOOK_AT_ENTITY(ped, PLAYER_PED_ID(), 10000, SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT, SLF_LOOKAT_VERY_HIGH)
//				TASK_TURN_PED_TO_FACE_ENTITY(ped, PLAYER_PED_ID(), 8000)
			ENDIF
			SETTIMERA(0)
			dialogue_when_player_arrives++
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_CASES_BEEN_PICKED_UP_OR_NEVER_CREATED()
	
	IF bBriefcaseCreated
	OR bBriefcaseCreated1
		
		IF bBriefcaseCreated
		AND bBriefcaseCreated1
			IF NOT DOES_BLIP_EXIST(blip_case[0])
			AND NOT DOES_BLIP_EXIST(blip_case[1])
				RETURN TRUE
			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//				IF DOES_BLIP_EXIST(blip_case[0])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[0]) @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[0])
//				ENDIF
//			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//				IF DOES_BLIP_EXIST(blip_case[1])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[1]) @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[1])
//				ENDIF
//			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//			AND HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//				PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[0]) AND REMOVE_BLIP(blip_case[1]) - RETURN TRUE @@@@@@@@@@@@@@@@@ \n")
//				RETURN TRUE
//			ENDIF
		ENDIF
		
		IF bBriefcaseCreated
		AND NOT bBriefcaseCreated1
			IF NOT DOES_BLIP_EXIST(blip_case[0])
				RETURN TRUE
			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//				IF DOES_BLIP_EXIST(blip_case[0])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[0]) - RETURN TRUE @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[0])
//					RETURN TRUE
//				ENDIF
//			ENDIF
		ENDIF
		
		IF bBriefcaseCreated1
		AND NOT bBriefcaseCreated
			IF NOT DOES_BLIP_EXIST(blip_case[1])
				RETURN TRUE
			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//				IF DOES_BLIP_EXIST(blip_case[1])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[1]) - RETURN TRUE @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[1])
//					RETURN TRUE
//				ENDIF
//			ENDIF
		ENDIF
		
//		IF DOES_PICKUP_EXIST(piBriefcasePickup[0])
//		AND DOES_PICKUP_EXIST(piBriefcasePickup[1])
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//			AND NOT HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//				IF DOES_BLIP_EXIST(blip_case[0])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[0]) @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[0])
//				ENDIF
//			ENDIF
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//			AND NOT HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//				IF DOES_BLIP_EXIST(blip_case[1])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[1]) @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[1])
//				ENDIF
//			ENDIF
//		ENDIF
//		IF DOES_PICKUP_EXIST(piBriefcasePickup[0])
//		AND NOT DOES_PICKUP_EXIST(piBriefcasePickup[1])
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
//				IF DOES_BLIP_EXIST(blip_case[0])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[0]) - RETUNR TRUE @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[0])
//				ENDIF
//				
//				RETURN TRUE
//			ENDIF
//		ENDIF
//		IF DOES_PICKUP_EXIST(piBriefcasePickup[1])
//		AND NOT DOES_PICKUP_EXIST(piBriefcasePickup[0])
//			IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
//				IF DOES_BLIP_EXIST(blip_case[1])
//					PRINTSTRING("\n @@@@@@@@@@@@@@@@@ REMOVE_BLIP(blip_case[1]) - RETUNR TRUE @@@@@@@@@@@@@@@@@ \n")
//					REMOVE_BLIP(blip_case[1])
//				ENDIF
//				
//				RETURN TRUE
//			ENDIF
//		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC end_mission_checks()

	IF NOT remove_cop0_voice
	
		IF IS_ENTITY_DEAD(ped_cops[0])
		OR IS_PED_INJURED(ped_cops[0])
			REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
			
			remove_cop0_voice = TRUE
		ENDIF
		
	ENDIF
	
//	IF make_crooks_enter_car
	
		IF NOT bBriefcaseCreated
			IF IS_ENTITY_DEAD(ped_prisoner)
			OR IS_PED_INJURED(ped_prisoner)
			OR IS_PED_HURT(ped_prisoner)
			//OR IS_PED_IN_WRITHE(ped_prisoner)
				REQUEST_MODEL(Prop_Security_case_01)
				IF HAS_MODEL_LOADED(Prop_Security_case_01)
					temp_coords = GET_DEAD_PED_PICKUP_COORDS(ped_prisoner)
					temp_coords.z +=0.3
					piBriefcasePickup[0] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, temp_coords + << 0, 0, -0.49 >>, << -90, 0, 0 >>, 0, 5000, EULER_YXZ, TRUE, Prop_Security_case_01)
	//				CREATE_AMBIENT_PICKUP(PICKUP_MONEY_CASE, temp_coords, 0, 3000)
					IF NOT DOES_BLIP_EXIST(blip_case[0])
						blip_case[0] = CREATE_BLIP_FOR_PICKUP(piBriefcasePickup[0])
					ENDIF
					bBriefcaseCreated = TRUE
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bBriefcaseCreated1
			IF IS_ENTITY_DEAD(ped_prisoner1)
			OR IS_PED_INJURED(ped_prisoner1)
			OR IS_PED_HURT(ped_prisoner1)
			//OR IS_PED_IN_WRITHE(ped_prisoner1)#
				REQUEST_MODEL(Prop_Security_case_01)
				IF HAS_MODEL_LOADED(Prop_Security_case_01)
					temp_coords = GET_DEAD_PED_PICKUP_COORDS(ped_prisoner1)
					temp_coords.z +=0.3
					piBriefcasePickup[1] = CREATE_PICKUP_ROTATE(PICKUP_MONEY_CASE, temp_coords + << 0, 0, -0.49 >>, << -90, 0, 0 >>, 0, 5000, EULER_YXZ, TRUE, Prop_Security_case_01)
	//				CREATE_AMBIENT_PICKUP(PICKUP_MONEY_CASE,temp_coords, 0, 3000)
					IF NOT DOES_BLIP_EXIST(blip_case[1])
						blip_case[1] = CREATE_BLIP_FOR_PICKUP(piBriefcasePickup[1])
					ENDIF
					bBriefcaseCreated1 = TRUE
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
				ENDIF
			ENDIF
		ENDIF
		
//	ENDIF
	
	IF NOT have_crooks_been_given_escape_tasks
	
		IF NOT remove_blip_0
			IF DOES_ENTITY_EXIST(ped_prisoner)
				IF IS_ENTITY_DEAD(ped_prisoner)
				OR IS_PED_INJURED(ped_prisoner)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner, PLAYER_PED_ID())
						REMOVE_BLIP(blip_prisoner0)
						
						remove_blip_0 = TRUE
						has_player_attacked_robbers = TRUE
						PRINTSTRING("\n Removed blip 1 \n")
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
						IF NOT IS_PED_INJURED(ped_prisoner1)
							TASK_COMBAT_PED(ped_prisoner1, PLAYER_PED_ID())
							SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
							REGISTER_TARGET(ped_prisoner1, PLAYER_PED_ID())
							SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner1, TRUE)
	//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//						WAIT(0)
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
		//					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_deadcr", CONV_PRIORITY_AMBIENT_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT remove_blip_1
			IF DOES_ENTITY_EXIST(ped_prisoner1)
				IF IS_ENTITY_DEAD(ped_prisoner1)
				OR IS_PED_INJURED(ped_prisoner1)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_prisoner1, PLAYER_PED_ID())
						REMOVE_BLIP(blip_prisoner1)
						
						remove_blip_1 = TRUE
						has_player_attacked_robbers = TRUE
						PRINTSTRING("\n Removed blip 2 \n")
						
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
						IF NOT IS_PED_INJURED(ped_prisoner)
							TASK_COMBAT_PED(ped_prisoner, PLAYER_PED_ID())
							SET_PED_KEEP_TASK(ped_prisoner, TRUE)
							REGISTER_TARGET(ped_prisoner, PLAYER_PED_ID())
							SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner, TRUE)
	//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//						WAIT(0)
//							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
		//					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
//							ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner, "RECSBRobber1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_deadcr", CONV_PRIORITY_AMBIENT_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_CASES_BEEN_PICKED_UP_OR_NEVER_CREATED()
		OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 310.6203, 2642.8538, 43.5173 >>, << 150, 150, 180 >>)
			IF IS_PED_INJURED(ped_prisoner)
			AND IS_PED_INJURED(ped_prisoner1)
//				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF HAS_CASES_BEEN_PICKED_UP_OR_NEVER_CREATED()
				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 50, 50, 50 >>)
					MISSION_PASSED()
				ENDIF
				IF has_player_attacked_robbers
				AND NOT has_player_attacked_cops
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 310.6203, 2642.8538, 43.5173 >>, << 150, 150, 180 >>)
						SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_COPS)
						MISSION_PASSED()
					ENDIF
				ENDIF
			ELIF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 310.6203, 2642.8538, 43.5173 >>, << 200, 200, 180 >>)
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
	
	IF have_crooks_been_given_escape_tasks
		IF HAS_CASES_BEEN_PICKED_UP_OR_NEVER_CREATED()
			// if players kills both robbers as they get away, player gets ALL the cash
			IF IS_ENTITY_DEAD(ped_prisoner)
			AND IS_ENTITY_DEAD(ped_prisoner1)
//			AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				//PRINTSTRING("\n Creating money pickups\n")
//				temp_coords = GET_DEAD_PED_PICKUP_COORDS(ped_prisoner)
				//temp_coords.z -=0.11
				//CREATE_MONEY_PICKUPS(GET_SAFE_PICKUP_COORDS(temp_coords), 3000, 5)
				
				//CREATE_AMBIENT_PICKUP(PICKUP_MONEY_CASE,GET_SAFE_PICKUP_COORDS(temp_coords), 0, 3000)
				
				
//				temp_coords = GET_DEAD_PED_PICKUP_COORDS(ped_prisoner1)
			//	temp_coords.z -=0.11
				//CREATE_MONEY_PICKUPS(GET_SAFE_PICKUP_COORDS(temp_coords), 3000, 5)
				//CREATE_AMBIENT_PICKUP(PICKUP_MONEY_CASE,GET_SAFE_PICKUP_COORDS(temp_coords), 0, 3000)
				
			//	REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
				//REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
				
				//CREATE_MONEY_PICKUPS(temp_coords, 5000, 5)
				MISSION_PASSED()
				//bBriefcaseCreated = TRUE
			ENDIF
			
			// if crooks have driven away
			IF have_crooks_been_given_escape_tasks
				IF DOES_ENTITY_EXIST(ped_prisoner)
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_prisoner, << 150, 150, 150 >>, FALSE, FALSE)
//					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						PRINTSTRING("\n crooks got away0\n ")
						IF NOT has_player_attacked_robbers
							SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_CRIMINALS)
						ENDIF
						MISSION_PASSED()
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(ped_prisoner1)
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_prisoner1, << 150, 150, 150 >>, FALSE, FALSE)
//					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						PRINTSTRING("\n crooks got away1\n ")
						IF NOT has_player_attacked_robbers
							SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_HELP_CRIMINALS)
						ENDIF
						MISSION_PASSED()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		/*
		IF bBriefcaseCreated
		
			IF DOES_PICKUP_EXIST(piBriefcasePickup[2])
				temp_coords2 = GET_PICKUP_COORDS(piBriefcasePickup[2])
				
				
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), temp_coords2, <<50.0,50.0,50.0>>, FALSE, TRUE)
					PRINTSTRING("\n player is far away from loot\n ")
					MISSION_PASSED()
				ENDIF
				
				IF HAS_PICKUP_BEEN_COLLECTED (piBriefcasePickup[2])
					PRINTSTRING("\n player collected loot\n ")
					//MISSION_PASSED()
				ENDIF
				
			ENDIF
			
			IF DOES_PICKUP_EXIST(piBriefcasePickup[1])
				temp_coords = GET_PICKUP_COORDS(piBriefcasePickup[1])
				
				
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), temp_coords, <<50.0,50.0,50.0>>, FALSE, TRUE)
					PRINTSTRING("\n player is far away from loot\n ")
					MISSION_PASSED()
				ENDIF
				
				IF HAS_PICKUP_BEEN_COLLECTED (piBriefcasePickup[1])
					PRINTSTRING("\n player collected loot\n ")
					//MISSION_PASSED()
				ENDIF
				
			ENDIF
			CREATE_AMBIENT_PICKUP
			
			IF HAS_PICKUP_BEEN_COLLECTED (piBriefcasePickup[2])
			AND HAS_PICKUP_BEEN_COLLECTED (piBriefcasePickup[1])
				PRINTSTRING("\n player collected loot\n ")
				MISSION_PASSED()
			ENDIF
			PRINTSTRING("\n waiting for player to collect loot\n ")
		
		ENDIF
		
		*/
	ELSE
	
	
		IF NOT cops_investigate_crooks_are_all_dead
		AND NOT has_player_attacked_cops
			SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<< 418.299561, 2650.596680, 42.847225 >>, << 297.898132, 2617.968262, 53.641197 >>, 49.6875)
//			bLeavingActivationArea = TRUE
			REQUEST_MODEL(AMBULANCE)
			REQUEST_ANIM_DICT("random@countrysiderobbery")
			IF IS_ENTITY_DEAD(ped_prisoner)
			AND IS_ENTITY_DEAD(ped_prisoner1)
			AND HAS_MODEL_LOADED(AMBULANCE)
			AND HAS_ANIM_DICT_LOADED("random@countrysiderobbery")
			
				REMOVE_BLIP(blip_prisoner0)
				REMOVE_BLIP(blip_prisoner1)
				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 6) //removing prisoner 0
				
				INT index
				REPEAT COUNT_OF(ped_cops) index
					IF DOES_BLIP_EXIST(blip_cops[index])
						REMOVE_BLIP(blip_cops[index])
					ENDIF
				ENDREPEAT
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copsGroup, RELGROUPHASH_PLAYER)
//				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copsGroup, prisonerGroup)
				//IF NOT IS_ENTITY_DEAD(vehicle_getaway)
				
//				BLOCK_DECISION_MAKER_EVENT(DECISION_MAKER_EMPTY, EVENT_REACTION_INVESTIGATE_DEAD_PED)
//				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
				//CREATE_INCIDENT(DT_AMBULANCE_DEPARTMENT, <<322.61,2625.97,43.88>>, 2, 30.0, ambulance_index) 
				
//				VEHICLE_INDEX vehicle_ambulance
				//IF NOT ON_SCREEN
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 267.94, 2700.48, 43.6 >>, << 15, 15, 15 >>)
					vehicle_ambulance = CREATE_VEHICLE(AMBULANCE, << 267.94, 2700.48, 43.1 >>, 180.0)
				ELSE
					vehicle_ambulance = CREATE_VEHICLE(AMBULANCE, << 185.94, 2603.48 ,46.9 >>, 0.0)
				ENDIF
				
				SET_VEHICLE_SIREN(vehicle_ambulance, TRUE)
				
				ped_ambulance1 = CREATE_PED_INSIDE_VEHICLE(vehicle_ambulance, PEDTYPE_MEDIC, S_M_M_Paramedic_01, VS_DRIVER)
				ped_ambulance2 = CREATE_PED_INSIDE_VEHICLE(vehicle_ambulance, PEDTYPE_MEDIC, S_M_M_Paramedic_01, VS_FRONT_RIGHT)
				SET_PED_COMBAT_ATTRIBUTES(ped_ambulance1, CA_ALWAYS_FLEE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(ped_ambulance2, CA_ALWAYS_FLEE, TRUE)
				SET_PED_CONFIG_FLAG(ped_ambulance1, PCF_UseKinematicModeWhenStationary, TRUE)
				SET_PED_CONFIG_FLAG(ped_ambulance2, PCF_UseKinematicModeWhenStationary, TRUE)
				
				//SET_PED_AS_GROUP_LEADER(ped_ambulance1,ambulance_group )
				//SET_PED_AS_GROUP_MEMBER(ped_ambulance2,ambulance_group )
				
//				temp_coords = GET_DEAD_PED_PICKUP_COORDS(ped_prisoner)
				
				IF DOES_ENTITY_EXIST(ped_prisoner)
					OPEN_SEQUENCE_TASK(seq_temp)
						//TASK_GO_TO_COORD_ANY_MEANS(NULL,<<305.5,2643.23,45.14>>, PEDMOVE_SPRINT, vehicle_ambulance )
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehicle_ambulance, << 307.1379, 2640.0417, 43.4639 >>, 18, DRIVINGSTYLE_NORMAL, AMBULANCE, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, 5, 5)
//						TASK_LEAVE_VEHICLE(NULL, vehicle_ambulance, ECF_RESUME_IF_INTERRUPTED)
						TASK_GO_TO_ENTITY(NULL, ped_prisoner, DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK, 10)
						
						//TASK_START_SCENARIO_AT_POSITION(NULL, "MEDIC_TENDTODEAD",temp_coords, 0.0 )
						// Changed as per # 300347 - Seemed to work fine to me! - RP (12/1/2011)
						TASK_LOOK_AT_ENTITY(NULL, ped_prisoner, -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, ped_prisoner)
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//						TASK_DUCK(NULL, 20000)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_TIME_OF_DEATH")
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_TEND_TO_DEAD")
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_KNEEL")
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_ambulance1, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
					SET_PED_KEEP_TASK(ped_ambulance1, TRUE)
				ENDIF
				IF DOES_ENTITY_EXIST(ped_prisoner1)
					OPEN_SEQUENCE_TASK(seq_temp)
						TASK_PAUSE(NULL, 15000)
//						TASK_GO_TO_COORD_ANY_MEANS(NULL, << 294.5, 2636.23, 45.14 >> , PEDMOVE_SPRINT, vehicle_ambulance)
						//SET_DRIVE_TASK_CRUISE_SPEED(NULL, 30.0)
						//SET_DRIVE_TASK_DRIVING_STYLE(NULL, 786598 )
//						TASK_LEAVE_VEHICLE(NULL, vehicle_ambulance, ECF_RESUME_IF_INTERRUPTED)
						
						TASK_GO_TO_ENTITY(NULL, ped_prisoner1, DEFAULT_TIME_NEVER_WARP, 2, PEDMOVEBLENDRATIO_WALK, 10)
						TASK_LOOK_AT_ENTITY(NULL, ped_prisoner1, -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, ped_prisoner1)
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//						TASK_LOOK_AT_COORD(NULL, temp_coords, 200000, SLF_DEFAULT)
//						TASK_DUCK(NULL, -1)
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_KNEEL")
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_TEND_TO_DEAD")
//						TASK_START_SCENARIO_IN_PLACE(NULL, "CODE_HUMAN_MEDIC_TIME_OF_DEATH")
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_ambulance2, seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
					SET_PED_KEEP_TASK(ped_ambulance2, TRUE)
				ENDIF
				//DISPA
				//CREATE_EMERGENCY_SERVICES_CAR_THEN_WALK (VehicleModelIndex, DestinationX, DestinationY, DestinationZ)
				//DISPAT
				
				IF NOT IS_PED_INJURED(ped_cops[0])
				AND DOES_ENTITY_EXIST(vehicle_getaway)
					SET_PED_CONFIG_FLAG(ped_cops[0], PCF_UseKinematicModeWhenStationary, TRUE)
					//TASK_GOTO_VEHICLE(ped_cops[0], vehicle_getaway, -1, 6.0)
//					IF NOT IS_ENTITY_DEAD(police_cars[0])
//						SET_PED_COMBAT_ATTRIBUTES(ped_cops[0], CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
//						TASK_SET_DECISION_MAKER(ped_cops[0], DECISION_MAKER_EMPTY)
//						CLEAR_PED_TASKS(ped_cops[0])
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_cops[0], FALSE)
//						SET_PED_COMBAT_ATTRIBUTES(ped_cops[0], CA_WILL_SCAN_FOR_DEAD_PEDS, TRUE)
//						SET_CURRENT_PED_WEAPON(ped_cops[0], WEAPONTYPE_UNARMED, TRUE)
						OPEN_SEQUENCE_TASK(seq_temp)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(vehicle_getaway, FALSE) + << 0, 5, 0 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							TASK_SWAP_WEAPON(NULL, FALSE)
							TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							TASK_PLAY_ANIM (NULL, "random@countrysiderobbery", "idle_d", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seq_temp)
						TASK_PERFORM_SEQUENCE(ped_cops[0], seq_temp)
						CLEAR_SEQUENCE_TASK(seq_temp)
						
						/*
						OPEN_SEQUENCE_TASK(seq_temp)
							TASK_ENTER_VEHICLE(NULL,police_cars[0] ,DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN)
							TASK_SMART_FLEE_COORD(NULL,vInput,200.0,20000,FALSE)
						CLOSE_SEQUENCE_TASK(seq_temp)
						TASK_PERFORM_SEQUENCE(ped_cops[0], seq_temp)
						CLEAR_SEQUENCE_TASK(seq_temp)
						*/
						SET_PED_KEEP_TASK(ped_cops[0], TRUE)
//					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(ped_cops[1])
				AND DOES_ENTITY_EXIST(ped_injured_police)
					SET_PED_CONFIG_FLAG(ped_cops[1], PCF_UseKinematicModeWhenStationary, TRUE)
//					SET_PED_COMBAT_ATTRIBUTES(ped_cops[1], CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
//					CLEAR_PED_TASKS(ped_cops[1])
//					TASK_SET_DECISION_MAKER(ped_cops[1], DECISION_MAKER_EMPTY)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_cops[1], FALSE)
					SET_CURRENT_PED_WEAPON(ped_cops[1], WEAPONTYPE_UNARMED, TRUE)
					OPEN_SEQUENCE_TASK(seq_temp)
//						TASK_ENTER_VEHICLE(NULL, police_cars[2], DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_RUN) 
//						TASK_VEHICLE_DRIVE_WANDER(NULL, police_cars[2], 10, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(ped_injured_police, FALSE) + << 0, 1, 0 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
						TASK_LOOK_AT_COORD(NULL, GET_ENTITY_COORDS(ped_injured_police, FALSE), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT)
						TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(ped_injured_police, FALSE))
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "enter", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "base", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
					CLOSE_SEQUENCE_TASK(seq_temp)
					TASK_PERFORM_SEQUENCE(ped_cops[1], seq_temp)
					CLEAR_SEQUENCE_TASK(seq_temp)
					
					SET_PED_KEEP_TASK(ped_cops[1], TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(ped_cops[2])
				AND DOES_ENTITY_EXIST(ped_prisoner1)
					SET_PED_CONFIG_FLAG(ped_cops[2], PCF_UseKinematicModeWhenStationary, TRUE)
//					SET_CURRENT_PED_WEAPON(ped_cops[2], WEAPONTYPE_UNARMED, TRUE)
					IF DOES_ENTITY_EXIST(ped_prisoner1)
						OPEN_SEQUENCE_TASK(seq_temp)
//							TASK_GO_TO_ENTITY(NULL, ped_prisoner1, DEFAULT_TIME_NEVER_WARP, 6, PEDMOVE_WALK)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(ped_prisoner1, FALSE) + << 0, 1, 0 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//							TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_COP_IDLES", 0, TRUE)
							TASK_SWAP_WEAPON(NULL, FALSE)
							TASK_PLAY_ANIM(NULL, "random@countrysiderobbery", "idle_d", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
							TASK_PLAY_ANIM (NULL, "random@countrysiderobbery", "idle_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seq_temp)
						TASK_PERFORM_SEQUENCE(ped_cops[2], seq_temp)
						CLEAR_SEQUENCE_TASK(seq_temp)
					ENDIF
					SET_PED_KEEP_TASK(ped_cops[2], TRUE) 
//					SET_PED_COMBAT_ATTRIBUTES(ped_cops[2], CA_WILL_SCAN_FOR_DEAD_PEDS, TRUE)
//					TASK_SET_DECISION_MAKER(ped_cops[2], DECISION_MAKER_EMPTY)
				//	CLEAR_PED_TASKS_IMMEDIATELY(ped_cops[2])
				//	TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_cops[2], FALSE)
				//	IF NOT IS_ENTITY_DEAD(police_cars[1])
					//	OPEN_SEQUENCE_TASK(seq_temp)
					//		TASK_ENTER_VEHICLE(NULL,police_cars[1] ,DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN) 
					//		TASK_SMART_FLEE_COORD(NULL,vInput,200.0,20000,FALSE )
					//	CLOSE_SEQUENCE_TASK(seq_temp)
					//	TASK_PERFORM_SEQUENCE(ped_cops[2], seq_temp)
					//	CLEAR_SEQUENCE_TASK(seq_temp)
					
						//SET_PED_KEEP_TASK(ped_cops[2], TRUE)
					//ENDIF
					
				ENDIF
				/*
				IF NOT IS_ENTITY_DEAD(ped_cops[3])
				
				AND NOT IS_ENTITY_DEAD(vehicle_getaway)
				
					TASK_GOTO_ENTITY_AIMING(ped_cops[3], vehicle_getaway, -1, 6.0)
					SET_PED_COMBAT_ATTRIBUTES(ped_cops[3], CA_WILL_SCAN_FOR_DEAD_PEDS, TRUE)
				//	CLEAR_PED_TASKS_IMMEDIATELY(ped_cops[3])
					//TASK_SET_DECISION_MAKER(ped_cops[3], DECISION_MAKER_EMPTY)
					//TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped_cops[3], TRUE)
					//TASK_ENTER_VEHICLE(ped_cops[3],police_cars[1] ,DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER, PEDMOVE_RUN) 
					SET_PED_KEEP_TASK(ped_cops[3], TRUE) 
					
				ENDIF
				//ENDIF
				*/
				
//				IF NOT bWarnedAboutBriefcases
//					IF DOES_PICKUP_EXIST(piBriefcasePickup[0])
//						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_PICKUP_COORDS(piBriefcasePickup[0]), << 20, 20, 20 >>)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PED_INJURED(ped_cops[0])
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[1])
									SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[2])
									SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
//								bWarnedAboutBriefcases = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//					IF DOES_PICKUP_EXIST(piBriefcasePickup[1])
//						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_PICKUP_COORDS(piBriefcasePickup[1]), << 20, 20, 20 >>)
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							WAIT(0)
//							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_PED_INJURED(ped_cops[0])
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[1])
									SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ELIF NOT IS_PED_INJURED(ped_cops[2])
									SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
									REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
									ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
									CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_ntouch", CONV_PRIORITY_AMBIENT_HIGH)
								ENDIF
//								bWarnedAboutBriefcases = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				//MISSION_PASSED()
				SETTIMERB(0)
				cops_investigate_crooks_are_all_dead = TRUE
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF make_crooks_enter_car
	AND NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		MISSION_FAILED()
	ENDIF
	
	IF IS_ENTITY_DEAD(ped_prisoner)
	AND IS_ENTITY_DEAD(ped_prisoner1)
	AND IS_ENTITY_DEAD(ped_cops[0])
	AND IS_ENTITY_DEAD(ped_cops[1])
	AND IS_ENTITY_DEAD(ped_cops[2])
	//AND NOT bBriefcaseCreated
	AND (HAS_CASES_BEEN_PICKED_UP_OR_NEVER_CREATED() OR NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 310.6203, 2642.8538, 43.5173 >>, << 200, 200, 180 >>))
		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
		SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
		MISSION_PASSED()
	ENDIF
	
ENDPROC


PROC has_player_attacked_the_cops()
	
	IF NOT is_player_aiming_at_a_cop
		
		GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), free_aim_target_entity)
		
		
		//ped_players_free_aim_target = NULL
		//IF DOES_ENTITY_EXIST(free_aim_target_entity)
			IF IS_ENTITY_A_PED(free_aim_target_entity)
				//WAIT(0)
				ped_players_free_aim_target = GET_PED_INDEX_FROM_ENTITY_INDEX(free_aim_target_entity)
				//WAIT(0)
			ENDIF
	//	ENDIF
		
		IF ped_players_free_aim_target = ped_cops[0]
		OR ped_players_free_aim_target = ped_cops[1]
		OR ped_players_free_aim_target = ped_cops[2]
			
			IF NOT IS_PED_INJURED(ped_players_free_aim_target)
				IF NOT IS_PLAYER_IN_AN_ATTACK_VEHICLE()
				AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
					IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ped_players_free_aim_target)
					OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ped_players_free_aim_target)
						IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_players_free_aim_target, << 20, 20, 20 >>)
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_players_free_aim_target, PLAYER_PED_ID())
							OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), ped_players_free_aim_target, << 8, 8, 8 >>) // Sometimes the LOS doesn't seem to work when they're in cover.
								IF NOT cops_warn_player_not_to_aim_at_them
									
									//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Don't point that weapon at us, civilian!", 4000, 1) //
									IF NOT IS_THIS_CONVERSATION_PLAYING("recsb_coppon")
										IF NOT IS_PED_INJURED(ped_cops[0])
											ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_coppon", CONV_PRIORITY_AMBIENT_HIGH)
											cops_warn_player_not_to_aim_at_them = TRUE
										ELIF NOT IS_PED_INJURED(ped_cops[1])
											SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
											REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
											ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_coppon", CONV_PRIORITY_AMBIENT_HIGH)
											cops_warn_player_not_to_aim_at_them = TRUE
										ELIF NOT IS_PED_INJURED(ped_cops[2])		
											SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
											REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
											ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											WAIT(0)
											CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_coppon", CONV_PRIORITY_AMBIENT_HIGH)
											cops_warn_player_not_to_aim_at_them = TRUE
										ENDIF
									ENDIF
									
									
								ENDIF
								
								free_aim_counter++
								PRINTSTRING("\n Player is aiming at cops\n ")
								IF free_aim_counter > 40
									is_player_aiming_at_a_cop = TRUE
									has_player_attacked_cops = TRUE
									IF NOT IS_ENTITY_DEAD(free_aim_target_entity)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
										INT index
										REPEAT COUNT_OF(ped_cops) index
											IF NOT IS_PED_INJURED(ped_cops[index])
												TASK_COMBAT_PED(ped_cops[index], PLAYER_PED_ID())
												SET_PED_KEEP_TASK(ped_cops[index], TRUE)
												REGISTER_TARGET(ped_cops[index], PLAYER_PED_ID())
												SET_PED_HIGHLY_PERCEPTIVE(ped_cops[index], TRUE)
											ENDIF
										ENDREPEAT
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF cops_warn_player_not_to_aim_at_them
							free_aim_counter = 40
						ELSE
							free_aim_counter = 0
						ENDIF
						//ped_players_free_aim_target = NULL
//						cops_warn_player_not_to_aim_at_them = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT index
	REPEAT COUNT_OF(ped_cops) index
		IF DOES_ENTITY_EXIST(ped_cops[index])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_cops[index], PLAYER_PED_ID())
				has_player_attacked_cops = TRUE
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_cops[index], GET_PLAYERS_LAST_VEHICLE())
					has_player_attacked_cops = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
//	IF NOT IS_ENTITY_DEAD(ped_cops[3])
	//	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_cops[3], PLAYER_PED_ID())
	//		has_player_attacked_cops = TRUE
	//	ENDIF
//	ENDIF
	
	IF is_player_aiming_at_a_cop
	
	ENDIF
	
	IF cops_investigate_crooks_are_all_dead
		
		IF TIMERB() > 6000
			IF TIMERB() > 6000
			AND TIMERB() < 7000
				STOP_FIRE_IN_RANGE(<< 330.1744, 2629.2375, 43.5056 >>, 30)
			ENDIF
			
			IF NOT bWarnedAboutBriefcases
				IF has_player_attacked_robbers
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "We got them! Thanks to you civilian!", 4000, 1) //
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ should play line @@@@@@@@@@@@@@@@@@@\n")
	//					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//		PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_cops[0], "recsbau", "recsb_crooks")
//						WAIT (0)
						IF NOT IS_PED_INJURED(ped_cops[0])
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooks", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[1])
							SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooks", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[2])
							SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooks", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						bWarnedAboutBriefcases = TRUE
					ENDIF
				ELSE
					PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ not playing recsb_crooks @@@@@@@@@@@@@@@@@@@\n")
					IF NOT make_crooks_enter_car
						//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Just keep walking civilian. Nothing to see here.", 4000, 1) //
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//	PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_cops[0], "recsbau", "recsb_crooka")
//							WAIT (0)
							IF NOT IS_PED_INJURED(ped_cops[0])
								ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooka", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF NOT IS_PED_INJURED(ped_cops[1])
								SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
								REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
								ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooka", CONV_PRIORITY_AMBIENT_HIGH)
							ELIF NOT IS_PED_INJURED(ped_cops[2])
								SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
								REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
								ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
								CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_crooka", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
							bWarnedAboutBriefcases = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), << 326.6079, 2626.2900, 43.5005 >> - << 50, 50, 50 >>, << 326.6079, 2626.2900, 43.5005 >> + << 50, 50, 50 >>, FALSE)
				has_player_attacked_cops = TRUE
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehicle_getaway)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle_getaway)
					has_player_attacked_cops = TRUE
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					IF NOT IS_PED_INJURED(ped_cops[0])
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF NOT IS_PED_INJURED(ped_cops[1])
						SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF NOT IS_PED_INJURED(ped_cops[2])
						SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
//		IF DOES_ENTITY_EXIST(ped_injured_police)
//			IF IS_PED_RAGDOLL(ped_injured_police)
//				has_player_attacked_cops = TRUE
//			ENDIF
//		ENDIF
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(ped_injured_police, FALSE), << 1, 1, 1 >>)
//			has_player_attacked_cops = TRUE
//		ENDIF
		
		REPEAT COUNT_OF(ped_cops) index
			IF NOT IS_PED_INJURED(ped_cops[index])
				IF HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[0])
				OR HAS_PICKUP_BEEN_COLLECTED(piBriefcasePickup[1])
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(ped_cops[index], PLAYER_PED_ID())
					AND IS_ENTITY_AT_ENTITY(ped_cops[index], PLAYER_PED_ID(), << 20, 20, 20 >>)
						has_player_attacked_cops = TRUE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF NOT IS_PED_INJURED(ped_cops[0])
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealb", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[1])
							SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealb", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[2])
							SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealb", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), ped_cops[index])
					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, copsGroup, RELGROUPHASH_PLAYER)
					has_player_attacked_cops = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT IS_PED_INJURED(ped_cops[1])
		AND NOT IS_ENTITY_DEAD(police_cars[2])
			IF NOT IS_ENTITY_AT_ENTITY(ped_cops[1], PLAYER_PED_ID(), << 100, 100, 100 >>)
				SET_PED_AS_NO_LONGER_NEEDED(ped_cops[1])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(police_cars[2])
			ENDIF
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(police_cars) index
		IF NOT IS_ENTITY_DEAD(police_cars[index])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(police_cars[index], PLAYER_PED_ID())
			OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), police_cars[index])
			OR HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(PLAYER_PED_ID(), police_cars[index], WEAPONTYPE_STICKYBOMB)
				has_player_attacked_cops = TRUE
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(police_cars[index], GET_PLAYERS_LAST_VEHICLE())
					has_player_attacked_cops = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_ENTITY_DEAD(vehicle_ambulance)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehicle_ambulance, PLAYER_PED_ID())
		OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle_ambulance)
			has_player_attacked_cops = TRUE
		ENDIF
	ENDIF
	
	// If the player is towing vehicles
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK)
		OR IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK2)
			VEHICLE_INDEX tempTowVehicle
			tempTowVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF cops_investigate_crooks_are_all_dead
				IF NOT IS_ENTITY_DEAD(vehicle_getaway)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempTowVehicle, vehicle_getaway)
						has_player_attacked_cops = TRUE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
						IF NOT IS_PED_INJURED(ped_cops[0])
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[1])
							SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
						ELIF NOT IS_PED_INJURED(ped_cops[2])
							SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
							REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
							ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
							CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_stealc", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			REPEAT COUNT_OF(police_cars) index
				IF NOT IS_ENTITY_DEAD(police_cars[index])
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempTowVehicle, police_cars[index])
						has_player_attacked_cops = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			IF NOT IS_ENTITY_DEAD(vehicle_ambulance)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(tempTowVehicle, vehicle_ambulance)
					has_player_attacked_cops = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
//	IF IS_PLAYER_WANTED_LEVEL_GREATER( PLAYER_ID(), 1 ) 
//		has_player_attacked_cops = TRUE
//	ENDIF
	
	
	IF has_player_attacked_cops
	OR is_player_aiming_at_a_cop
		SETTIMERA(0)
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "That guys in on the robbery too!", 4000, 1) //
	//	SET_FAKE_WANTED_LEVEL(3)
		SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
		SET_DISPATCH_IDEAL_SPAWN_DISTANCE(350)
		has_player_attacked_cops = TRUE
	//	PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_cops[0], "recsbau", "recsb_copsho")
			IF NOT IS_ENTITY_DEAD(vehicle_getaway)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicle_getaway)
			
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				WAIT(0)
				IF NOT cops_investigate_crooks_are_all_dead
					IF IS_PED_INJURED(ped_prisoner)
					AND NOT IS_PED_INJURED(ped_prisoner1)
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
					ENDIF
					IF NOT IS_PED_INJURED(ped_cops[0])
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsho", CONV_PRIORITY_AMBIENT_HIGH)
						
					ELIF NOT IS_PED_INJURED(ped_cops[1])
						SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsho", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF NOT IS_PED_INJURED(ped_cops[2])
						SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copsho", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF	
					
				ELSE
					IF NOT IS_PED_INJURED(ped_cops[0])
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_sscen3", CONV_PRIORITY_AMBIENT_HIGH)
						
					ELIF NOT IS_PED_INJURED(ped_cops[1])
						SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_sscen3", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF NOT IS_PED_INJURED(ped_cops[2])
						SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_sscen3", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
			ENDIF
			
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "He's another robber! Call for more back up!", 4000, 1) //
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
	//	SET_MAX_WANTED_LEVEL( INT NewMaxLevel)
		IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
			//offset_vector = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(prisonvanbreak, <<-10, -30.0, 0>>) //Ped placement
//			REMOVE_BLIP(blip_person_getting_players_attention)
			
//			IF NOT prision1bliphasbeencreated
//				IF NOT IS_ENTITY_DEAD(ped_prisoner)
//				OR NOT IS_PED_INJURED(ped_prisoner)
//					blip_prisoner0 = CREATE_BLIP_FOR_PED(ped_prisoner)
//					remove_blip_0 = FALSE
//					prision1bliphasbeencreated = true
//				ENDIF
//			ENDIF
//			
//			IF NOT prision2bliphasbeencreated
//				IF NOT IS_ENTITY_DEAD(ped_prisoner1)
//				OR NOT IS_PED_INJURED(ped_prisoner1)
//					blip_prisoner1 = CREATE_BLIP_FOR_PED(ped_prisoner1)
//					remove_blip_1 = FALSE
//					prision2bliphasbeencreated = TRUE
//				ENDIF
//			ENDIF
			SETTIMERA(0)
			//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "There's so many of these bastards robbing this store!", 4000, 1) //
			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				SET_RANDOM_EVENT_ACTIVE()
			ENDIF
		ENDIF
		
		IF cops_investigate_crooks_are_all_dead
			REPEAT COUNT_OF(ped_cops) index
				IF NOT IS_PED_INJURED(ped_cops[index])
					TASK_COMBAT_PED(ped_cops[index], PLAYER_PED_ID())
					SET_PED_KEEP_TASK(ped_cops[index], TRUE)
					REGISTER_TARGET(ped_cops[index], PLAYER_PED_ID())
					SET_PED_CURRENT_WEAPON_VISIBLE(ped_cops[index], TRUE)
					SET_PED_HIGHLY_PERCEPTIVE(ped_cops[index], TRUE)
					SET_PED_COMBAT_MOVEMENT(ped_cops[index], CM_WILLADVANCE)
					SET_PED_COMBAT_ATTRIBUTES(ped_cops[index], CA_CAN_CHARGE, TRUE)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(ped_ambulance1)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_ambulance1, PLAYER_PED_ID())
			SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
			has_player_attacked_cops = TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(ped_ambulance2)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(ped_ambulance2, PLAYER_PED_ID())
			SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
			has_player_attacked_cops = TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
	//SET_PLAYER_WANTED_LEVEL( PLAYER_ID(), 0)
		
		has_player_attacked_cops = TRUE
	//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Get him as well!", 4000, 1)
		SET_POLICE_IGNORE_PLAYER(PLAYER_ID(), FALSE)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
	ENDIF
ENDPROC

PROC proc_dialogue_end_player_gets_close()

	IF NOT bInitialDialoguePlayed
		IF NOT has_player_attacked_cops
		AND NOT cops_investigate_crooks_are_all_dead
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 325.6556, 2647.0210, 43.6289 >>, << 30, 30, 30 >>)
				INT index
				REPEAT COUNT_OF(ped_cops) index
					IF DOES_BLIP_EXIST(blip_cops[index])
						SHOW_HEIGHT_ON_BLIP(blip_cops[index], TRUE)
					ENDIF
				ENDREPEAT
				IF DOES_BLIP_EXIST(blip_prisoner0)
					SHOW_HEIGHT_ON_BLIP(blip_prisoner0, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blip_prisoner1)
					SHOW_HEIGHT_ON_BLIP(blip_prisoner1, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(ped_cops[0])
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[1])
					SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[2])	
					SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_copswa", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
				bInitialDialoguePlayed = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
	AND NOT make_crooks_enter_car
	AND NOT cops_investigate_crooks_are_all_dead
	
		IF dialogue_when_player_arrives = 0
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			SETTIMERA(0)
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_comm", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_comf", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_comt", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			dialogue_when_player_arrives++
			SETTIMERA(0)
		ENDIF
		
		IF dialogue_when_player_arrives = 1
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		
				SETTIMERA(0)
				//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Drop your weapons! We have the convenient store surrounded!", 4000, 1) //
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation, ped_cops[0], "recsbau", "recsb_banteb")
				//WAIT(0)
				IF NOT IS_PED_INJURED(ped_cops[0])
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[0], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_banteb", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[1])
					SET_AMBIENT_VOICE_NAME(ped_cops[1], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[1], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_banteb", CONV_PRIORITY_AMBIENT_HIGH)
				ELIF NOT IS_PED_INJURED(ped_cops[2])
					SET_AMBIENT_VOICE_NAME(ped_cops[2], "S_M_Y_RANGER_01_WHITE_FULL_01")
					REMOVE_PED_FOR_DIALOGUE(cultresConversation, 4) //removing cop 0
					ADD_PED_FOR_DIALOGUE(cultresConversation, 4, ped_cops[2], "RECSBCop1")
					CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_banteb", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF	
				dialogue_when_player_arrives++
			SETTIMERA(0)
			
		ENDIF
		
		IF dialogue_when_player_arrives = 2
		AND TIMERA()> 2000
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
		//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Shit! We're surrounded by cops!", 4000, 1) //
		
			//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			//WAIT (0)
			IF NOT IS_PED_INJURED(ped_prisoner)
				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_bantea", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF NOT IS_PED_INJURED(ped_prisoner1)
				REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
				ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
				CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_bantea", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			
			SETTIMERA(0)
			dialogue_when_player_arrives++
		ENDIF
		
		IF dialogue_when_player_arrives = 3
		AND TIMERA()> 2000
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT has_player_attacked_robbers
				IF NOT bToldToJoinOrLeave
					IF NOT IS_PED_INJURED(ped_prisoner)
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_croclo", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF NOT IS_PED_INJURED(ped_prisoner1)
						REMOVE_PED_FOR_DIALOGUE(cultresConversation, 5) //removing prisoner 0
						ADD_PED_FOR_DIALOGUE(cultresConversation, 5, ped_prisoner1, "RECSBRobber1")
						CREATE_CONVERSATION(cultresConversation, "recsbau", "recsb_croclo", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
					bToldToJoinOrLeave = TRUE
				ENDIF
			ENDIF
			IF NOT has_player_attacked_cops
				can_player_is_close_to_ped_and_play_dialogue(ped_cops[0], "recsb_copclo")
				can_player_is_close_to_ped_and_play_dialogue(ped_cops[1], "recsb_copclo")
				can_player_is_close_to_ped_and_play_dialogue(ped_cops[2], "recsb_copclo")
			ENDIF
		ENDIF
		
		IF dialogue_when_player_arrives = 4
		AND TIMERA()> 5000
		
		//	PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Firing at police officers will get you the chair. You do know that?", 4000, 1) //
			
			SETTIMERA(0)
			dialogue_when_player_arrives++
		ENDIF
		
	ENDIF
ENDPROC

PROC blips_and_targeting_and_sirens()

	INT index
	REPEAT COUNT_OF(ped_cops) index
		IF IS_PED_INJURED(ped_cops[index])
			IF DOES_BLIP_EXIST(blip_cops[index])
				REMOVE_BLIP(blip_cops[index])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_PED_INJURED(ped_prisoner)
		IF DOES_BLIP_EXIST(blip_prisoner0)
			REMOVE_BLIP(blip_prisoner0)
		ENDIF
	ENDIF
	IF IS_PED_INJURED(ped_prisoner1)
		IF DOES_BLIP_EXIST(blip_prisoner1)
			REMOVE_BLIP(blip_prisoner1)
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(police_cars) index
		IF NOT IS_ENTITY_DEAD(police_cars[index])
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), police_cars[index])
				SET_VEHICLE_HAS_MUTED_SIRENS(police_cars[index], FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF has_player_attacked_robbers
		
		IF NOT robbers_bliped_enemy_and_targeted
			PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ robbers_bliped_enemy_and_targeted @@@@@@@@@@@@@@@@@@@\n")
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, prisonerGroup, RELGROUPHASH_PLAYER)
			IF NOT IS_PED_INJURED(ped_prisoner)
				IF DOES_BLIP_EXIST(blip_prisoner0)
					REMOVE_BLIP(blip_prisoner0)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blip_prisoner0)
					blip_prisoner0 = CREATE_BLIP_FOR_PED(ped_prisoner, TRUE)
					SET_PED_CAN_BE_TARGETTED(ped_prisoner, TRUE)
				ENDIF
				TASK_COMBAT_PED(ped_prisoner, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner, TRUE)
				REGISTER_TARGET(ped_prisoner, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(ped_prisoner1)
				IF DOES_BLIP_EXIST(blip_prisoner1)
					REMOVE_BLIP(blip_prisoner1)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blip_prisoner1)
					blip_prisoner1 = CREATE_BLIP_FOR_PED(ped_prisoner1, TRUE)
					SET_PED_CAN_BE_TARGETTED(ped_prisoner1, TRUE)
				ENDIF
				TASK_COMBAT_PED(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_KEEP_TASK(ped_prisoner1, TRUE)
				REGISTER_TARGET(ped_prisoner1, PLAYER_PED_ID())
				SET_PED_HIGHLY_PERCEPTIVE(ped_prisoner1, TRUE)
			ENDIF
			robbers_bliped_enemy_and_targeted = TRUE
		ENDIF
		
		IF has_player_attacked_cops
			IF NOT cops_bliped_enemy_and_targeted
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
				REPEAT COUNT_OF(ped_cops) index
					IF NOT IS_PED_INJURED(ped_cops[index])
						SET_PED_CAN_BE_TARGETTED(ped_cops[index], TRUE)
						IF DOES_BLIP_EXIST(blip_cops[index])
							REMOVE_BLIP(blip_cops[index])
						ENDIF
//						IF NOT DOES_BLIP_EXIST(blip_cops[index])
//							blip_cops[index] = CREATE_BLIP_FOR_PED(ped_cops[index], TRUE)
//						ENDIF
						TASK_COMBAT_PED(ped_cops[index], PLAYER_PED_ID())
						SET_PED_KEEP_TASK(ped_cops[index], TRUE)
						REGISTER_TARGET(ped_cops[index], PLAYER_PED_ID())
						SET_PED_HIGHLY_PERCEPTIVE(ped_cops[index], TRUE)
					ENDIF
				ENDREPEAT
				cops_bliped_enemy_and_targeted = TRUE
			ENDIF
		ELSE
		
			REPEAT COUNT_OF(ped_cops) index
				IF NOT IS_PED_INJURED(ped_cops[index])
					SET_PED_CAN_BE_TARGETTED(ped_cops[index], FALSE)
				ENDIF
			ENDREPEAT
		ENDIF
		
	ELSE
	
		IF has_player_attacked_cops
		
			IF NOT robbers_bliped_friendly_and_untargeted
				IF NOT IS_PED_INJURED(ped_prisoner)
//					IF DOES_BLIP_EXIST(blip_prisoner0)
//						REMOVE_BLIP(blip_prisoner0)
//						blip_prisoner0 = CREATE_BLIP_FOR_PED(ped_prisoner)
						SET_PED_CAN_BE_TARGETTED(ped_prisoner, FALSE)
//					ENDIF
				ENDIF
				IF NOT IS_PED_INJURED(ped_prisoner1)
//					IF DOES_BLIP_EXIST(blip_prisoner1)
//						REMOVE_BLIP(blip_prisoner1)
//						blip_prisoner1 = CREATE_BLIP_FOR_PED(ped_prisoner1)
						SET_PED_CAN_BE_TARGETTED(ped_prisoner1, FALSE)
//					ENDIF
				ENDIF
				robbers_bliped_friendly_and_untargeted = TRUE
			ENDIF
			
			IF NOT cops_bliped_enemy_and_targeted
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copsGroup, RELGROUPHASH_PLAYER)
				REPEAT COUNT_OF(ped_cops) index
					IF NOT IS_PED_INJURED(ped_cops[index])
						SET_PED_CAN_BE_TARGETTED(ped_cops[index], TRUE)
						REMOVE_COP_BLIP_FROM_PED(ped_cops[index])
//						SET_PED_HAS_AI_BLIP(ped_cops[index], FALSE)
						IF DOES_BLIP_EXIST(blip_cops[index])
							REMOVE_BLIP(blip_cops[index])
						ENDIF
						IF NOT DOES_BLIP_EXIST(blip_cops[index])
							blip_cops[index] = CREATE_BLIP_FOR_PED(ped_cops[index], TRUE)
						ENDIF
						TASK_COMBAT_PED(ped_cops[index], PLAYER_PED_ID())
						SET_PED_KEEP_TASK(ped_cops[index], TRUE)
						REGISTER_TARGET(ped_cops[index], PLAYER_PED_ID())
						SET_PED_HIGHLY_PERCEPTIVE(ped_cops[index], TRUE)
					ENDIF
				ENDREPEAT
				cops_bliped_enemy_and_targeted = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC


///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTVECTOR(vInput)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("prisonvanbreak")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS| FORCE_CLEANUP_FLAG_RANDOM_EVENTS)	
		missionCleanup()
	ENDIF
	
	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput)
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bLeavingActivationArea
//			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_CR")
				SWITCH ambStage
				
					CASE ambCanRun
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 100, 100, 100 >>)
							PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ TOO CLOSE TO EVENT @@@@@@@@@@@@@@@@@@@\n")
							missionCleanup()
						ENDIF
						IF bAssetsLoaded
							ambStage = (ambRunning)
						ELSE
							create_assets()
							bDoFullCleanUp = TRUE
						ENDIF
					BREAK
					
					CASE ambRunning
						//DO STUFF
						SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
						
						blips_and_targeting_and_sirens()
						proc_dialogue_end_player_gets_close()
						
						IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							check_if_player_is_close_bystanders()
						ENDIF
						
						IF NOT has_player_attacked_cops
							has_player_attacked_the_cops()
						ENDIF
						
						IF NOT has_player_attacked_robbers
						AND NOT have_crooks_been_given_escape_tasks
							check_if_player_is_helping_cops()
						ENDIF
						
						IF cops_investigate_crooks_are_all_dead
						AND NOT has_player_attacked_cops
							// make cops say random line if player is in crime scene
							make_cop_tell_player_to_leave(ped_cops[0])
							make_cop_tell_player_to_leave(ped_cops[1])
							make_cop_tell_player_to_leave(ped_cops[2])
							
//							IF NOT cops_put_guns_away
//								make_cop_put_away_weapon(ped_cops[0])
//								make_cop_put_away_weapon(ped_cops[1])
//								make_cop_put_away_weapon(ped_cops[2])
//							ENDIF
						ENDIF
						
						IF NOT cops_investigate_crooks_are_all_dead
						AND NOT have_crooks_been_given_escape_tasks
						AND IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
							DO_DYNAMIC_TYRE_POP(police_cars[0], iNumberOfTimesTyreShot[0], SC_WHEEL_CAR_FRONT_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[0], iNumberOfTimesTyreShot[1], SC_WHEEL_CAR_REAR_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[0], iNumberOfTimesTyreShot[2], SC_WHEEL_CAR_FRONT_LEFT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[0], iNumberOfTimesTyreShot[3], SC_WHEEL_CAR_REAR_LEFT, 0.7)
							
							DO_DYNAMIC_TYRE_POP(police_cars[1], iNumberOfTimesTyreShot[4], SC_WHEEL_CAR_FRONT_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[1], iNumberOfTimesTyreShot[5], SC_WHEEL_CAR_REAR_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[1], iNumberOfTimesTyreShot[6], SC_WHEEL_CAR_FRONT_LEFT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[1], iNumberOfTimesTyreShot[7], SC_WHEEL_CAR_REAR_LEFT, 0.7)
							
							DO_DYNAMIC_TYRE_POP(police_cars[2], iNumberOfTimesTyreShot[8], SC_WHEEL_CAR_FRONT_RIGHT,0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[2], iNumberOfTimesTyreShot[9], SC_WHEEL_CAR_REAR_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[2], iNumberOfTimesTyreShot[10], SC_WHEEL_CAR_FRONT_LEFT, 0.7)
							DO_DYNAMIC_TYRE_POP(police_cars[2], iNumberOfTimesTyreShot[11], SC_WHEEL_CAR_REAR_LEFT, 0.7)
							
							DO_DYNAMIC_TYRE_POP(vehicle_getaway, iNumberOfTimesTyreShot[12], SC_WHEEL_CAR_FRONT_RIGHT,0.7)
							DO_DYNAMIC_TYRE_POP(vehicle_getaway, iNumberOfTimesTyreShot[13], SC_WHEEL_CAR_REAR_RIGHT, 0.7)
							DO_DYNAMIC_TYRE_POP(vehicle_getaway, iNumberOfTimesTyreShot[14], SC_WHEEL_CAR_FRONT_LEFT, 0.7)
							DO_DYNAMIC_TYRE_POP(vehicle_getaway, iNumberOfTimesTyreShot[15], SC_WHEEL_CAR_REAR_LEFT, 0.7)
						ENDIF
						
						IF NOT make_crooks_enter_car
							check_if_cops_are_all_dead()
						ELSE
							IF NOT have_crooks_been_given_escape_tasks
								// check if crooks are in car and make em drive off
								CHECK_IF_CROOKS_ARE_IN_CAR()
							ELSE
								IF NOT flag_player_given_money_by_thieves
								AND has_player_attacked_cops
									make_crooks_give_cash_to_player()
								ENDIF
							ENDIF
							IF NOT have_crooks_been_given_flee_on_foot_tasks
								make_crooks_flee_if_doors_are_blocked()
							ENDIF
							MAKE_CROOKS_HATE_PLAYER_IF_HE_ATTACKS_AFTER_ESCAPE()
						ENDIF
						
						end_mission_checks()
						maintain_debug()
					BREAK
					
				ENDSWITCH
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE
	
	
ENDSCRIPT
///    //////////////////////////////////////////////////////////
///    							DEBUG
///    //////////////////////////////////////////////////////////

