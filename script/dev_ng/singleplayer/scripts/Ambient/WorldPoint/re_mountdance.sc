//
//												Random event - Mountain Dancer							
//																		Vicki

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING"commands_path.sch"
USING "AggroSupport.sch"
USING "context_control_public.sch"
USING "ambient_speech.sch"
USING "Ambient_Common.sch"
USING "script_player.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "commands_player.sch"
USING "random_events_public.sch"
USING "script_ped.sch"
USING "RC_Threat_public.sch"
MODEL_NAMES modelDancer = U_M_O_TAPHILLBILLY
VECTOR vInput
FLOAT fInput
PED_INDEX dancer
OBJECT_INDEX stereo
BOOL wanderRun = FALSE
STRING animDic = "SPECIAL_PED@MOUNTAIN_DANCER@MONOLOGUE_1@MONOLOGUE_1A"
ENUM STAGES
	SETUP,
	PRE_SCENE,
	PREPARE_SCENE,
	RUN,
	IN_RAGDOLL_WAITING_TO_NOT_BE,
	WAIT_TO_CLEANUP,
	WAIT_TO_REACH_DEST_BEFORE_CLEANUP,
	WAIT_TO_LEAVE_AREA_FOR_CLEANUP
ENDENUM
STAGES mountainStage = SETUP
BLIP_INDEX pedBlip

PROC SAFE_ADD_BLIP_SP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bEnemy = TRUE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_PED_INJURED(pedIndex)
				blipIndex = CREATE_BLIP_FOR_PED(pedIndex, bEnemy)
				IF bEnemy = FALSE
					SET_BLIP_PRIORITY(blipIndex, BLIPPRIORITY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_SP_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC missionCleanup(BOOL deletePed = TRUE)
	IF DOES_ENTITY_EXIST(stereo)
		DELETE_OBJECT(stereo)
	ENDIF
	
	IF DOES_BLIP_EXIST(pedBlip)
		SAFE_REMOVE_SP_BLIP(pedBlip)
	ENDIF
				
	IF DOES_ENTITY_EXIST(dancer)
		IF deletePed
			DELETE_PED(dancer)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(dancer)
		ENDIF
	ENDIF
	
	PRINTSTRING("[MD] CLEANUP!") PRINTNL()
				
	TERMINATE_THIS_THREAD()
ENDPROC

PROC missionPassed()
ENDPROC

PROC RUN_DEBUG()
#IF IS_DEBUG_BUILD 
    // Event Passed
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
        missionPassed()
    ENDIF
    // Jump to end cut
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)

    ENDIF
    // Event Failed
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
        missionCleanup()
    ENDIF
#ENDIF
ENDPROC

FUNC BOOL loadAssets()
	REQUEST_MODEL(modelDancer)
	REQUEST_ANIM_DICT(animDic)
	IF HAS_MODEL_LOADED(modelDancer)
	AND HAS_ANIM_DICT_LOADED(animDic)
		return TRUE
	ENDIF
	return FALSE
ENDFUNC
INT syncedScene = -1
//SEQUENCE_INDEX danceSeq

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)
	vInput = in_coords.vec_coord[0]
	fInput = in_coords.headings[0]
	VECTOR dancerCoords
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		missionCleanup(FALSE)
	ENDIF
	
	IF IS_BITMASK_ENUM_AS_ENUM_SET(g_savedGlobals.sSpecialPedData.ePedsKilled, SP_DANCER)
		missionCleanup(FALSE)
	ENDIF
	
	WHILE TRUE
		WAIT(0)
		
		IF DOES_ENTITY_EXIST(dancer)
			IF IS_ENTITY_DEAD(dancer) AND NOT (mountainStage = WAIT_TO_LEAVE_AREA_FOR_CLEANUP)
				PRINTSTRING("[MD] Dancer down!") PRINTNL()
				SET_BITMASK_ENUM_AS_ENUM(g_savedGlobals.sSpecialPedData.ePedsKilled, SP_DANCER)
				mountainStage = WAIT_TO_LEAVE_AREA_FOR_CLEANUP
			ENDIF
		ENDIF
			
		SWITCH mountainStage
			CASE SETUP
				PRINTSTRING("[MD] PREPARE_SCENE") PRINTNL()
				IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE() AND NOT SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS() AND NOT SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					IF loadAssets()
						dancer = CREATE_PED(PEDTYPE_MISSION, modelDancer, vInput, fInput)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dancer, TRUE)
						stereo = CREATE_OBJECT(Prop_Tapeplayer_01, <<704.7615, 4179.5737, 39.7093>>)
						SET_ENTITY_HEADING(stereo, 233.8288+180.0) //180 degrees out apparently.
						SET_PED_PROP_INDEX(dancer, ANCHOR_HEAD, 0, 0)
						mountainStage = PRE_SCENE
					ENDIF
				ELSE
					missionCleanup(FALSE)
				ENDIF
			BREAK
			
			CASE PRE_SCENE
				syncedScene = CREATE_SYNCHRONIZED_SCENE(vInput+<<0, 0, 1>>, <<0, 0, fInput>>)
				TASK_SYNCHRONIZED_SCENE(dancer, syncedScene, animDic, "MTN_DNC_if_you_want_to_get_to_heaven", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS/*|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION*/)
				//SET_SYNCHRONIZED_SCENE_LOOPED(syncedScene, TRUE)
				INIT_SYNCH_SCENE_AUDIO_WITH_ENTITY("MNT_DNC", dancer)
				mountainStage = PREPARE_SCENE
			BREAK
			
			CASE PREPARE_SCENE
				PRINTSTRING("[MD] PREPARE_SCENE") PRINTNL()
			
				IF PREPARE_SYNCHRONIZED_AUDIO_EVENT_FOR_SCENE(syncedScene, "MNT_DNC") = TRUE
					PRINTSTRING("[MD] PREPARE_SYNCHRONIZED_AUDIO_EVENT_FOR_SCENE") PRINTNL()
					//TASK_SYNCHRONIZED_SCENE(dancer, syncedScene, animDic, "MTN_DNC_if_you_want_to_get_to_heaven", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_AUDIO_EVENT(syncedScene)
					mountainStage = RUN
				ELSE
					PRINTSTRING("[MD] PREPARE_SYNCHRONIZED_AUDIO_EVENT_FOR_SCENE FAIL!") PRINTNL()
				ENDIF
			BREAK
			
			CASE RUN
				dancerCoords = GET_ENTITY_COORDS(dancer)

				SET_PED_LOD_MULTIPLIER(dancer, 3.0)

				IF GET_DISTANCE_BETWEEN_PEDS(PLAYER_PED_ID(), dancer) < 10.0
					SET_IK_TARGET(PLAYER_PED_ID(), IK_PART_HEAD, dancer, ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
					SET_IK_TARGET(dancer, IK_PART_HEAD, PLAYER_PED_ID(), ENUM_TO_INT(BONETAG_HEAD), <<0,0,0>>, ITF_DEFAULT)
				ENDIF
				
				IF (GET_DISTANCE_BETWEEN_ENTITIES(dancer, PLAYER_PED_ID()) < 25.0)
					IF NOT DOES_BLIP_EXIST(pedBlip)
						SAFE_ADD_BLIP_SP_PED(pedBlip, dancer, FALSE)
						SET_BLIP_COLOUR(pedBlip, BLIP_COLOUR_BLUE)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(pedBlip)
						SAFE_REMOVE_SP_BLIP(pedBlip)
					ENDIF
				ENDIF
		
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScene) > 0.9999999
					PRINTSTRING("[MD] RUN REPEAT") PRINTNL()
					//TASK_SYNCHRONIZED_SCENE(dancer, syncedScene, animDic, "MTN_DNC_if_you_want_to_get_to_heaven", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_ACTIVATE_RAGDOLL_ON_COLLISION)
					//SET_SYNCHRONIZED_SCENE_PHASE(syncedScene, 0.0)
					mountainStage = PRE_SCENE
				ENDIF
					
				IF (IS_PED_RAGDOLL(dancer))
					PRINTSTRING("RAGDOLL") PRINTNL()
					//Wander off
					STOP_SYNCHRONIZED_AUDIO_EVENT(syncedScene)
					mountainStage = IN_RAGDOLL_WAITING_TO_NOT_BE
				ENDIF
				
				IF ((IS_PED_INJURED(dancer) OR IS_BULLET_IN_AREA(dancerCoords, 50) OR IS_BULLET_IN_AREA(dancerCoords, 50, FALSE) OR IS_PROJECTILE_IN_AREA(dancerCoords, <<20, 20, 20>>)) OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, dancerCoords, 50.0)) 
				OR ((IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), dancer) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), dancer)) AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), dancerCoords) < 25.0) AND (IS_PED_IN_PED_VIEW_CONE(dancer, PLAYER_PED_ID())) )
					PRINTSTRING("RUNNING OFF") PRINTNL()
					
					//Run off
					STOP_SYNCHRONIZED_AUDIO_EVENT(syncedScene)
				
					mountainStage = IN_RAGDOLL_WAITING_TO_NOT_BE
					wanderRun = TRUE
				ENDIF
			BREAK
			
			CASE IN_RAGDOLL_WAITING_TO_NOT_BE
				IF DOES_BLIP_EXIST(pedBlip)
					SAFE_REMOVE_SP_BLIP(pedBlip)
				ENDIF
	
				IF (NOT IS_PED_RAGDOLL(dancer))
					
					IF wanderRun = TRUE
						TASK_FOLLOW_NAV_MESH_TO_COORD(dancer, <<723.1315, 4171.9565, 39.7091>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_ACCURATE_WALKRUN_START)
						//TASK_SMART_FLEE_PED(dancer, PLAYER_PED_ID(), 100.0, -1)
					ELSE
						TASK_FOLLOW_NAV_MESH_TO_COORD(dancer, <<723.1315, 4171.9565, 39.7091>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 1.0, ENAV_ACCURATE_WALKRUN_START)
						//TASK_WANDER_STANDARD(dancer)
					ENDIF
					SET_PED_KEEP_TASK(dancer, TRUE)
					mountainStage = WAIT_TO_REACH_DEST_BEFORE_CLEANUP
				ENDIF
			BREAK
			
			CASE WAIT_TO_REACH_DEST_BEFORE_CLEANUP
				IF ((GET_SCRIPT_TASK_STATUS(dancer, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK)
				AND (GET_SCRIPT_TASK_STATUS(dancer, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK))	
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dancer, FALSE)
					IF wanderRun = TRUE
						TASK_SMART_FLEE_PED(dancer, PLAYER_PED_ID(), 100.0, -1)
					ELSE
						TASK_WANDER_STANDARD(dancer)
					ENDIF
					SET_PED_KEEP_TASK(dancer, TRUE)
					mountainStage = WAIT_TO_CLEANUP
				ENDIF
			BREAK 
			
			CASE WAIT_TO_CLEANUP
				IF ((IS_ENTITY_OCCLUDED(dancer)) AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(dancer), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50.0) AND NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE())
					missionCleanup(TRUE)
				ENDIF
			BREAK
			
			CASE WAIT_TO_LEAVE_AREA_FOR_CLEANUP
				IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
					missionCleanup(FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDWHILE
ENDSCRIPT
