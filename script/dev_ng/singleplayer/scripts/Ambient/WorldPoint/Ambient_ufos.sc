// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Ambient U.F.Os 
//		AUTHOR			:	Ian Gander / Aaron Gandaa
//		DESCRIPTION		: 	Ambient UFO Handler Script
//
// *****************************************************************************************
// *****************************************************************************************


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


//----------------------
//	INCLUDES
//----------------------
USING "rgeneral_include.sch"
USING "rc_helper_functions.sch"
USING "script_launch_control.sch"

//----------------------
//	ENUMS
//----------------------
ENUM eUFOSTAGE
	UFO_WAITTILLSOUNDRANGE,
	UFO_WAITTILLRANGE,
	UFO_INRANGE,
	UFO_TIMER,
	UFO_DOWNFORCE
ENDENUM

//----------------------
//	STRUCT
//----------------------

//----------------------
//	VARIABLES
//----------------------
INT iInRangeTimer = -1
INT iCountDown = 0

INT iClosestUFO = -1
eUFOSTAGE UFOstage	
VECTOR vAmbUFOPosition[2]
BOOL bDistCleanup = TRUE
FLOAT fDownForce = 10
FLOAT fAffectRadius = 90
FLOAT fPedAffectRadius = 35
FLOAT fPedAwayForce = 60
FLOAT fAmbSoundRadius = 275.0

INT iBeamStartTime = 1500

INT iBeamSound = -1
INT iBeamSoundTimer 

INT iBeamSoundPlayTime = 3000
//INT iTaserSoundPlayTime = 708
INT iStunSoundPlayTime = 708
INT iCrackleSoundPlayTime = 377

//INT iTaserSoundDelay = 3000
INT iStunSoundDelay = 1000
INT iCrackleSoundDelay = 2093

INT iCrackleSoundTimer 
INT iStunSoundTimer 

INT iCrackleSoundID = -1
INT iStunSoundID = -1

BOOL bPlayStunSound = TRUE
BOOL bPlayCrackleSound = TRUE

#IF IS_DEBUG_BUILD
	BOOL bKillScript = FALSE
	WIDGET_GROUP_ID m_WidgetGroup
	BLIP_INDEX bBlip[2]
	BOOL bShowDebug
#ENDIF

//----------------------
//	DEBUG FUNCTIONS
//----------------------

#IF IS_DEBUG_BUILD
/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	SET_PROFILING_OF_THIS_SCRIPT(FALSE)
	
	m_WidgetGroup = START_WIDGET_GROUP("Ambient UFO")
		ADD_WIDGET_BOOL("Kill Script", bKillScript)
		ADD_WIDGET_BOOL("Dist Cleanup", bDistCleanup)
		ADD_WIDGET_BOOL("Show Debug", bShowDebug)
	
		ADD_WIDGET_READ_ONLY_VECTOR("UFO Pos 1", vAmbUFOPosition[0])
		ADD_WIDGET_READ_ONLY_VECTOR("UFO Pos 2", vAmbUFOPosition[1])
		ADD_WIDGET_INT_READ_ONLY("Closest UFO", iClosestUFO)
		ADD_WIDGET_INT_READ_ONLY("Range Time", iInRangeTimer)
		ADD_WIDGET_INT_READ_ONLY("Countdown", iCountDown)
		
		ADD_WIDGET_INT_SLIDER("Beam Shoot Start Time", iBeamStartTime, 250, 10000, 5)
		
		START_WIDGET_GROUP("Forces and Radii")
			ADD_WIDGET_FLOAT_SLIDER("Ambient Sound Radius", fAmbSoundRadius, 0, AMBIENT_UFO_SCRIPT_TRIGGER_RANGE, 5)
			ADD_WIDGET_FLOAT_SLIDER("Effect Radius", fAffectRadius, 0, 200, 5)
			ADD_WIDGET_FLOAT_SLIDER("Ped Effect Radius", fPedAffectRadius, 0, 200, 5)
			ADD_WIDGET_FLOAT_SLIDER("Down Force", fDownForce, 0, 1000, 5)
			ADD_WIDGET_FLOAT_SLIDER("Ped Away Force", fPedAwayForce, 0, 1000, 5)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Sound")
			ADD_WIDGET_BOOL("Play Stun Sound", bPlayStunSound)
			ADD_WIDGET_BOOL("Play Crackle Sound", bPlayCrackleSound)
			
			START_WIDGET_GROUP("Play Time")
				ADD_WIDGET_INT_SLIDER("Beam Sound", iBeamSoundPlayTime, 250, 10000, 5)
				//ADD_WIDGET_INT_SLIDER("Tazer Sound", iTaserSoundPlayTime, 250, 10000, 5)
				ADD_WIDGET_INT_SLIDER("Stun Sound", iStunSoundPlayTime, 250, 10000, 5)
				ADD_WIDGET_INT_SLIDER("Crackle Sound", iCrackleSoundPlayTime, 250, 10000, 5)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Delay")
				//ADD_WIDGET_INT_SLIDER("Tazer Sound", iTaserSoundDelay, 250, 10000, 5)
				ADD_WIDGET_INT_SLIDER("Stun Sound", iStunSoundDelay, 250, 10000, 5)
				ADD_WIDGET_INT_SLIDER("Crackle Sound", iCrackleSoundDelay, 250, 10000, 5)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
	
	SAFE_REMOVE_BLIP(bBlip[0])
	SAFE_REMOVE_BLIP(bBlip[1])
ENDPROC	
#ENDIF

//----------------------
//	FUNCTIONS
//----------------------
PROC RESTORE_VEHICLE_POWER()
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX viPlayerVeh  = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_OK(viPlayerVeh)
				SET_VEHICLE_ENGINE_ON(viPlayerVeh, TRUE, FALSE) // Reactivate the engine
				CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Player Craft Reactivated")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CUT_VEHICLE_POWER()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayerVeh)
			SET_VEHICLE_ENGINE_ON(viPlayerVeh, FALSE, FALSE)
			APPLY_FORCE_TO_ENTITY(viPlayerVeh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0, -fDownForce>>, <<0,1,0>>, 0, FALSE, TRUE, true)
		ENDIF
	ENDIF
ENDPROC

PROC PUSH_PLAYER_AWAY()
	IF iClosestUFO = -1
		EXIT
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[iClosestUFO], fPedAffectRadius)
		EXIT
	ENDIF
	
	VECTOR vForce = NORMALISE_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID()) - vAmbUFOPosition[iClosestUFO]) * fPedAwayForce
	APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_EXTERNAL_FORCE, vForce, <<0,0,0>>, 0, FALSE, TRUE, true)
ENDPROC

PROC UPDATE_STUN_SOUND()
	IF (bPlayStunSound)
		IF GET_GAME_TIMER() > iStunSoundTimer
			IF (iStunSoundID <> -1)
				SAFE_STOP_AND_RELEASE_SOUND_ID(iStunSoundID)
				iStunSoundTimer = GET_GAME_TIMER() + iStunSoundDelay
			ELSE
				iStunSoundID = GET_SOUND_ID()
				PLAY_SOUND(iStunSoundID, "spl_stun_npc_master")
				iStunSoundTimer = GET_GAME_TIMER() + iStunSoundPlayTime	// + GET_RANDOM_INT_IN_RANGE(-iStunSoundPlayTime / 2, iStunSoundPlayTime / 2)							
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_CRACKLE_SOUND()
	IF (bPlayCrackleSound)
		IF GET_GAME_TIMER() > iCrackleSoundTimer
			IF (iCrackleSoundID <> -1)
				SAFE_STOP_AND_RELEASE_SOUND_ID(iCrackleSoundID)
				iCrackleSoundTimer = GET_GAME_TIMER() + iCrackleSoundDelay
			ELSE
				iCrackleSoundID = GET_SOUND_ID()
				PLAY_SOUND(iCrackleSoundID, "ent_amb_elec_crackle")
				iCrackleSoundTimer = GET_GAME_TIMER() + iCrackleSoundPlayTime // + GET_RANDOM_INT_IN_RANGE(-iCrackleSoundPlayTime / 2, iCrackleSoundPlayTime / 2)								
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	IF IS_IPL_ACTIVE("ufo")
		REMOVE_IPL("ufo")
	ENDIF
	
	SAFE_STOP_AND_RELEASE_SOUND_ID(iStunSoundID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(iCrackleSoundID)
	SAFE_STOP_AND_RELEASE_SOUND_ID(iBeamSound)
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_01",FALSE,TRUE)
	SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_02",FALSE,TRUE)

	RESTORE_VEHICLE_POWER()
	CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - script cleaning up.")
	TERMINATE_THIS_THREAD()
ENDPROC


//----------------------
//	SCRIPT
//----------------------
SCRIPT
	INT i
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP)	
		SCRIPT_CLEANUP()
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Waiting For Screen To Fade In")
	WHILE IS_SCREEN_FADED_OUT()
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Requesting IPL")
	IF NOT IS_IPL_ACTIVE("ufo")
		REQUEST_IPL("ufo")
	ENDIF
	
	vAmbUFOPosition[0] = vAmbUFOPosition0
 	vAmbUFOPosition[1] = vAmbUFOPosition1
	
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID())
	
		IF (g_bForceNoAmbientUFO = TRUE)
			CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Someone set g_bForceNoAmbientUFO to TRUE")
			SCRIPT_CLEANUP()
		ENDIF			
		
		#IF IS_DEBUG_BUILD
			IF (bKillScript)
				SCRIPT_CLEANUP()
			ENDIF
		
			IF bShowDebug
				i = 0
					
				REPEAT COUNT_OF(vAmbUFOPosition) i
					DRAW_DEBUG_CIRCLE(vAmbUFOPosition[i], fAffectRadius)
					DRAW_DEBUG_CIRCLE(vAmbUFOPosition[i], fPedAffectRadius)
					DRAW_DEBUG_CIRCLE(vAmbUFOPosition[i], fAmbSoundRadius)
					DRAW_DEBUG_LINE(vAmbUFOPosition[i], <<vAmbUFOPosition[i].x, vAmbUFOPosition[i].y, -1000>>)
					
					IF NOT DOES_BLIP_EXIST(bBlip[i])
						bBlip[i] = CREATE_COORD_BLIP(vAmbUFOPosition[i])
					ENDIF
				ENDREPEAT
			ENDIF
		#ENDIF
		
		IF (bDistCleanup)
			IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[0], AMBIENT_UFO_SCRIPT_TRIGGER_RANGE + 50.0)
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[1], AMBIENT_UFO_SCRIPT_TRIGGER_RANGE + 50.0)
					CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Player out of range of UFOs")
					SCRIPT_CLEANUP()
				ENDIF
			ENDIF
			
			IF (UFOstage != UFO_WAITTILLSOUNDRANGE)
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[0], fAmbSoundRadius + 50.0)
					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[1], fAmbSoundRadius + 50.0)
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Player out of sound range of UFOs")
						SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_01",FALSE,TRUE)
						SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_02",FALSE,TRUE)
						UFOstage = UFO_WAITTILLSOUNDRANGE
						iClosestUFO = -1
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		PUSH_PLAYER_AWAY()
		
		
		SWITCH UFOstage	
			CASE UFO_WAITTILLSOUNDRANGE
				i = 0
				REPEAT COUNT_OF(vAmbUFOPosition) i
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[i], fAmbSoundRadius)
						iClosestUFO = i
						UFOstage = UFO_WAITTILLRANGE
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Player is within Sound Range of Closest UFO:", iClosestUFO)
						SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_01",TRUE,TRUE)
						SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_02",TRUE,TRUE)
					ENDIF
				ENDREPEAT
			BREAK
			CASE UFO_WAITTILLRANGE
				i = 0
				REPEAT COUNT_OF(vAmbUFOPosition) i
					IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[i], fAffectRadius)
						iClosestUFO = i
						UFOstage = UFO_INRANGE
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Closest UFO:", iClosestUFO)
					ENDIF
				ENDREPEAT
			BREAK
			CASE UFO_INRANGE
				IF (iClosestUFO = -1)
					UFOstage = UFO_WAITTILLRANGE
				ELSE
					CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Range Timer Started For UFO:", iClosestUFO)
					iInRangeTimer = GET_GAME_TIMER()
					iCountDown = 0
					UFOstage = UFO_TIMER
				ENDIF
			BREAK
			CASE UFO_TIMER
				IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[iClosestUFO], fAffectRadius)
					iCountDown = (GET_GAME_TIMER() - iInRangeTimer)
					IF (iCountDown) >= iBeamStartTime
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Down Force Started For UFO:", iClosestUFO)
						UFOstage = UFO_DOWNFORCE
						iBeamSound = GET_SOUND_ID()
						iBeamSoundTimer = GET_GAME_TIMER()
						
						IF (iClosestUFO = 0)
							PLAY_SOUND_FROM_COORD(iBeamSound, "SPECIAL_EVIL_UFO_DEATH_RAY", vAmbUFOPosition[iClosestUFO])
						ELSE
							PLAY_SOUND_FROM_COORD(iBeamSound, "SPECIAL_EVIL_UFO_DEATH_RAY_3", vAmbUFOPosition[iClosestUFO])
						ENDIF
						
						UPDATE_STUN_SOUND()
						UPDATE_CRACKLE_SOUND()
						
					ENDIF
				ELSE
					UFOstage = UFO_WAITTILLRANGE
				ENDIF
			BREAK
			CASE UFO_DOWNFORCE
				CUT_VEHICLE_POWER()
				UPDATE_STUN_SOUND()
				UPDATE_CRACKLE_SOUND()
				IF GET_GAME_TIMER() > (iBeamSoundTimer + iBeamSoundPlayTime)
					SAFE_STOP_AND_RELEASE_SOUND_ID(iBeamSound)
				ENDIF
				
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vAmbUFOPosition[iClosestUFO], fAmbSoundRadius)
					RESTORE_VEHICLE_POWER()
					iClosestUFO = -1
					UFOstage = UFO_WAITTILLSOUNDRANGE
					
					SAFE_STOP_AND_RELEASE_SOUND_ID(iStunSoundID)
					SAFE_STOP_AND_RELEASE_SOUND_ID(iCrackleSoundID)
					SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_01",FALSE,TRUE)
					SET_AMBIENT_ZONE_STATE_PERSISTENT("AZ_SPECIAL_UFO_02",FALSE,TRUE)
				ENDIF
			BREAK
		ENDSWITCH	
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT

/*
//////////////////////////////////////////////////////////
// 				Ambient U.F.Os - Ian Gander				//
//		   												//
//////////////////////////////////////////////////////////
   
/////////////////////////////////////////////////////////////
//   					 HEADERS						   //
/////////////////////////////////////////////////////////////
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING "script_PLAYER.sch"
USING "rage_builtins.sch"
USING "LineActivation.sch"
USING "script_ped.sch"
USING "ambient_common.sch"
USING "RC_Helper_Functions.sch"

/////////////////////////////////////////////////////////////
//   					 VARIABLES						   //
/////////////////////////////////////////////////////////////

int 	UFOstage = 0
INT		iTimer = -1

//////////////////////////////////////////////////////////////
//   					 PROCS						  	 	//
//////////////////////////////////////////////////////////////

FUNC BOOL IS_UFO_AVAILABLE()
	CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - starting ambient ufo")
	RETURN TRUE
ENDFUNC

PROC MISSIONCLEANUP()
	IF IS_IPL_ACTIVE("ufo")
		REMOVE_IPL("ufo")
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX viPlayerVeh
		viPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF IS_VEHICLE_OK(viPlayerVeh)
			SET_VEHICLE_ENGINE_ON(viPlayerVeh, TRUE, FALSE) // Reactivate the engine
		ENDIF
	ENDIF
	CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - script cleaning up.")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC REQUEST_ASSETS()

ENDPROC

PROC CREATE_ASSETS()
	
ENDPROC

///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT
	IF not IS_UFO_AVAILABLE()
		CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - ambient ufos not available")
		MISSIONCLEANUP()
	ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_SP_TO_MP)	
		missionCleanup()
	ENDIF

	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()	
			
			SWITCH UFOstage			
				CASE 0
					IF NOT IS_IPL_ACTIVE("ufo")
						REQUEST_IPL("ufo")
						UFOstage++
					ENDIF
				BREAK
				
				CASE 1
					IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<2490.0, 3777.0, 2402.879>>, <<75, 75, 40>>)
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-2366.379, 2962.758, 1448.873>>, <<75, 75, 40>>)
							IF iTimer = -1
								iTimer = GET_GAME_TIMER()
							ELSE
								IF (GET_GAME_TIMER() - iTimer) >= 3000
									CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - too close to UFO!")
									UFOstage++
								ENDIF
							ENDIF
						ELSE
							iTimer = -1
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						VEHICLE_INDEX viPlayerVeh
						viPlayerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_VEHICLE_OK(viPlayerVeh)
							SET_VEHICLE_ENGINE_ON(viPlayerVeh, FALSE, FALSE)
							APPLY_FORCE_TO_ENTITY(viPlayerVeh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,-10>>, <<0,1,0>>, 0, FALSE, TRUE, true)
						ENDIF
					ENDIF
				BREAK
				
			ENDSWITCH
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE

ENDSCRIPT
*/
