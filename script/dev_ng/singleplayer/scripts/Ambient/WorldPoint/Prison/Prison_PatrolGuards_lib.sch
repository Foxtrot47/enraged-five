// Prison_PatrolGuards_lib.sch

CONST_INT MAX_NUMBER_PATROL_GUARDS 3
CONST_INT MAX_NUMBER_STANDING_GUARDS 0
CONST_INT MAX_NUMBER_PATROL_GUARD_FOLLOWS 4

VECTOR vPrisonCenterPosition = << 1683.6819, 2518.8669, 44.5651 >>
BOOL bTooCloseAggro = FALSE


STRUCT PRISON_PATROL_GUARDS
	VECTOR vSpawnPosition[MAX_NUMBER_PATROL_GUARDS]
	FLOAT fSpawnHeading[MAX_NUMBER_PATROL_GUARDS]
	STRING patrolGuardRoute[MAX_NUMBER_PATROL_GUARDS]
	BOOL bStartCombat[MAX_NUMBER_PATROL_GUARDS]
	BOOL bStartCombatFollows[MAX_NUMBER_PATROL_GUARD_FOLLOWS]
	PED_INDEX piPatrolGuard[MAX_NUMBER_PATROL_GUARDS]
	PED_INDEX piPatrolGuardFollows[MAX_NUMBER_PATROL_GUARD_FOLLOWS]
	BLIP_INDEX blipPatrolGuard[MAX_NUMBER_PATROL_GUARDS]
	BLIP_INDEX blipPatrolGuardFollows[MAX_NUMBER_PATROL_GUARD_FOLLOWS]
ENDSTRUCT

//STRUCT PRISON_STANDING_GUARDS
//	VECTOR vSpawnPosition[MAX_NUMBER_STANDING_GUARDS]
//	FLOAT fSpawnHeading[MAX_NUMBER_STANDING_GUARDS]
//	BOOL bStartCombat[MAX_NUMBER_STANDING_GUARDS]
//	PED_INDEX piStandingGuard[MAX_NUMBER_STANDING_GUARDS]
//	BLIP_INDEX blipStandingGuard[MAX_NUMBER_STANDING_GUARDS]
//ENDSTRUCT

PRISON_PATROL_GUARDS guardPatrols
//PRISON_STANDING_GUARDS guardStanding

PROC INIT_PATROL_GUARD_DATA()
//	guardPatrols.vSpawnPosition[0] = << 1667.0487, 2675.9543, 54.3302 >>
	guardPatrols.vSpawnPosition[0] = << 1768.6276, 2538.9705, 44.4054 >>
	guardPatrols.vSpawnPosition[1] = << 1633.6128, 2498.8491, 44.4117 >>
	guardPatrols.vSpawnPosition[2] = << 1622.6099, 2555.6829, 44.4051 >>
//	guardPatrols.vSpawnPosition[4] = << 1619.7992, 2525.2952, 44.4052 >>
//	guardPatrols.vSpawnPosition[5] = << 1587.0146, 2671.5007, 44.4775 >> 
//	guardPatrols.vSpawnPosition[6] = << 1757.5291, 2431.6204, 44.4976 >>

//	guardPatrols.fSpawnHeading[0] = 0
	guardPatrols.fSpawnHeading[0] = 0
	guardPatrols.fSpawnHeading[1] = 0
	guardPatrols.fSpawnHeading[2] = 198.4323
//	guardPatrols.fSpawnHeading[4] = 243.4286
//	guardPatrols.fSpawnHeading[5] = 121.3236
//	guardPatrols.fSpawnHeading[6] = 80.0014
	
//	guardPatrols.patrolGuardRoute[0] = "PatrolGuard01"
	guardPatrols.patrolGuardRoute[0] = "PatrolGuard02"
	guardPatrols.patrolGuardRoute[1] = "PatrolGuard03"
	guardPatrols.patrolGuardRoute[2] = "PatrolGuard04"
//	guardPatrols.patrolGuardRoute[4] = "PatrolGuard05"
//	guardPatrols.patrolGuardRoute[5] = "PatrolGuard06"
//	guardPatrols.patrolGuardRoute[6] = "PatrolGuard07"
ENDPROC	

//PROC INIT_STANDING_GUARD_DATA()
//	guardStanding.vSpawnPosition[0] = << 1847.8397, 2584.4304, 44.6720 >> 
//	guardStanding.vSpawnPosition[1] = << 1831.5615, 2602.7717, 44.9254 >>  
//	guardStanding.vSpawnPosition[2] = << 1794.5889, 2598.7256, 44.6524 >> 
//	guardStanding.vSpawnPosition[3] = << 1781.8113, 2527.4153, 44.4696 >> 
//	guardStanding.vSpawnPosition[4] = << 1581.5870, 2619.7957, 44.5072 >>
//	
//	guardStanding.fSpawnHeading[0] = 354.5476
//	guardStanding.fSpawnHeading[1] = 309.5453
//	guardStanding.fSpawnHeading[2] = 334.7815
//	guardStanding.fSpawnHeading[3] = 13.1531
//	guardStanding.fSpawnHeading[4] = 55.3466
//ENDPROC

PROC CREATE_PATROL_GUARDS()
	INT idx
	
	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		IF NOT DOES_ENTITY_EXIST(guardPatrols.piPatrolGuard[idx])
			guardPatrols.piPatrolGuard[idx] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, guardPatrols.vSpawnPosition[idx], guardPatrols.fSpawnHeading[idx])
			PRINTLN("CREATING PATROL GUARD: ", idx)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_15 sName
				sName = "PATROL GUARD: "
				sName += idx
				SET_PED_NAME_DEBUG(guardPatrols.piPatrolGuard[idx], sName)
			#ENDIF
			WAIT(0)
		ENDIF
	ENDREPEAT
	
	VECTOR vOffSet01 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(guardPatrols.vSpawnPosition[1], guardPatrols.fSpawnHeading[1], <<-2,0,0>>)
	VECTOR vOffSet02 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(guardPatrols.vSpawnPosition[1], guardPatrols.fSpawnHeading[1], <<-1,0,0>>)
	VECTOR vOffSet03 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(guardPatrols.vSpawnPosition[2], guardPatrols.fSpawnHeading[2], <<1,0,0>>)
	VECTOR vOffSet04 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(guardPatrols.vSpawnPosition[2], guardPatrols.fSpawnHeading[2], <<-1,0,0>>)
	
	// Following Patrol Guard 2
	guardPatrols.piPatrolGuardFollows[0] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, vOffSet01, guardPatrols.fSpawnHeading[1])
	#IF IS_DEBUG_BUILD
		SET_PED_NAME_DEBUG(guardPatrols.piPatrolGuardFollows[0], "PATROL FOLLOW 0")
	#ENDIF
	
	guardPatrols.piPatrolGuardFollows[1] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, vOffSet02, guardPatrols.fSpawnHeading[1])
	#IF IS_DEBUG_BUILD
		SET_PED_NAME_DEBUG(guardPatrols.piPatrolGuardFollows[1], "PATROL FOLLOW 1")
	#ENDIF
	
	// Following Patrol Guard 3
	guardPatrols.piPatrolGuardFollows[2] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, vOffSet03, guardPatrols.fSpawnHeading[2])
	#IF IS_DEBUG_BUILD
		SET_PED_NAME_DEBUG(guardPatrols.piPatrolGuardFollows[2], "PATROL FOLLOW 2")
	#ENDIF
	
	guardPatrols.piPatrolGuardFollows[3] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, vOffSet04, guardPatrols.fSpawnHeading[2])
	#IF IS_DEBUG_BUILD
		SET_PED_NAME_DEBUG(guardPatrols.piPatrolGuardFollows[3], "PATROL FOLLOW 3")
	#ENDIF
	
	REPEAT MAX_NUMBER_PATROL_GUARD_FOLLOWS idx
		IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuardFollows[idx])
			GIVE_WEAPON_TO_PED(guardPatrols.piPatrolGuardFollows[idx], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
			SET_ENTITY_LOD_DIST(guardPatrols.piPatrolGuardFollows[idx], 500)
			SET_PED_RELATIONSHIP_GROUP_HASH(guardPatrols.piPatrolGuardFollows[idx], RELGROUPHASH_SECURITY_GUARD)
			SET_PED_STEERS_AROUND_PEDS(guardPatrols.piPatrolGuardFollows[idx], FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(guardPatrols.piPatrolGuardFollows[idx], TRUE)
		ENDIF
	ENDREPEAT
	
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PRISGUARD_01)
ENDPROC

PROC SETUP_PATROL_GUARDS()
	INT idx
	
	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuard[idx]) AND NOT IS_ENTITY_DEAD(guardPatrols.piPatrolGuard[idx])
			GIVE_WEAPON_TO_PED(guardPatrols.piPatrolGuard[idx], WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
			SET_ENTITY_LOD_DIST(guardPatrols.piPatrolGuard[idx], 1000)
			SET_PED_RELATIONSHIP_GROUP_HASH(guardPatrols.piPatrolGuard[idx], RELGROUPHASH_SECURITY_GUARD)
			SET_PED_IS_AVOIDED_BY_OTHERS(guardPatrols.piPatrolGuard[idx], FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(guardPatrols.piPatrolGuard[idx], TRUE)
			
			IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuard[idx])
				TASK_FOLLOW_WAYPOINT_RECORDING(guardPatrols.piPatrolGuard[idx], guardPatrols.patrolGuardRoute[idx])
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

// Make all requests for the patrolguards system here.
PROC PATROLGUARD_REQUESTS()
	INT idx
	
	INIT_PATROL_GUARD_DATA()
	
	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		REQUEST_WAYPOINT_RECORDING(guardPatrols.patrolGuardRoute[idx])
		PRINTLN("REQUESTING PATROL GUARD WAYPOINT RECORDING: ", guardPatrols.patrolGuardRoute[idx])
	ENDREPEAT
	
ENDPROC

// Check to see if all requests have loaded here.
FUNC BOOL HAS_PATROLGUARD_LOADED()
	INT idx
	
	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED(guardPatrols.patrolGuardRoute[idx])
			WAIT(0)
			PRINTLN("WAITING FOR PATROL GUARD WAYPOINT RECORDINGS", idx)
		ENDWHILE
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

// Spawn everything needed for the patrolguard system here.
PROC PATROLGUARD_SPAWN()
	CREATE_PATROL_GUARDS()
	SETUP_PATROL_GUARDS()
ENDPROC

// Update patrolguard system here.
PROC PATROLGUARD_UPDATE()
	INT idx
	VECTOR vPlayerPosition
	
	// To fix Bug # 588434 - for some reason if you start up the prison on a fresh boot, the normal area checks don't seem to work.
	IF NOT guardPatrols.bStartCombat[0]
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
		
		IF NOT bRunWarningCheck
			IF VDIST2(vPlayerPosition, vPrisonCenterPosition) < 5625
				bTooCloseAggro = TRUE
				PRINTLN("bTooCloseAggro = TRUE")
			ENDIF
		ENDIF
	ENDIF
	
	//------------------------------PATROL GUARDS------------------------------
	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuard[idx])
			// Aggro check
			IF NOT guardPatrols.bStartCombat[idx]
				IF DO_AGGRO_CHECK(guardPatrols.piPatrolGuard[idx], NULL, prisonAggroArgs, eAggroReasons, TRUE)
				OR bTooCloseAggro
					TASK_COMBAT_PED(guardPatrols.piPatrolGuard[idx], PLAYER_PED_ID())
					PRINTLN("TASKING PATROL GUARD TO COMBAT: ", idx)
					guardPatrols.bStartCombat[idx] = TRUE
				ENDIF
			ENDIF
			IF NOT guardPatrols.bStartCombat[idx]
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(guardPatrols.piPatrolGuard[idx])
					TASK_FOLLOW_WAYPOINT_RECORDING(guardPatrols.piPatrolGuard[idx], guardPatrols.patrolGuardRoute[idx])
				ENDIF
			ENDIF
		ELSE
			// Remove blips if the they become injured
			IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuard[idx])
				REMOVE_BLIP(guardPatrols.blipPatrolGuard[idx])
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	//------------------------------PATROL GUARD FOLLOW------------------------------
	REPEAT MAX_NUMBER_PATROL_GUARD_FOLLOWS idx
		IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuardFollows[idx])
			IF IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[idx])
				IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuardFollows[idx])
					REMOVE_BLIP(guardPatrols.blipPatrolGuardFollows[idx])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	// Aggro check - Patrol Guards 0 and 1
	IF NOT guardPatrols.bStartCombatFollows[0]
		IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[0])
			IF DO_AGGRO_CHECK(guardPatrols.piPatrolGuardFollows[0], NULL, prisonAggroArgs, eAggroReasons, TRUE)
			OR guardPatrols.bStartCombat[1] = TRUE
			OR bTooCloseAggro
				CLEAR_PED_TASKS(guardPatrols.piPatrolGuardFollows[0])
				TASK_COMBAT_PED(guardPatrols.piPatrolGuardFollows[0], PLAYER_PED_ID())
				PRINTLN("TASKING PATROL GUARD FOLLOW 0 TO COMBAT")
				guardPatrols.bStartCombatFollows[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT guardPatrols.bStartCombatFollows[1]
		IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[1])
			IF DO_AGGRO_CHECK(guardPatrols.piPatrolGuardFollows[1], NULL, prisonAggroArgs, eAggroReasons)
			OR guardPatrols.bStartCombat[1] = TRUE
			OR bTooCloseAggro
				CLEAR_PED_TASKS(guardPatrols.piPatrolGuardFollows[1])
				TASK_COMBAT_PED(guardPatrols.piPatrolGuardFollows[1], PLAYER_PED_ID())
				PRINTLN("TASKING PATROL GUARD FOLLOW 1 TO COMBAT")
				guardPatrols.bStartCombatFollows[1] = TRUE
			ENDIF
		ENDIF
	ENDIF
	// Aggro check - Patrol Guards 2 and 3
	IF NOT guardPatrols.bStartCombatFollows[2]
		IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[2])
			IF DO_AGGRO_CHECK(guardPatrols.piPatrolGuardFollows[2], NULL, prisonAggroArgs, eAggroReasons)
			OR guardPatrols.bStartCombat[2] = TRUE
			OR bTooCloseAggro
				CLEAR_PED_TASKS(guardPatrols.piPatrolGuardFollows[2])
				TASK_COMBAT_PED(guardPatrols.piPatrolGuardFollows[2], PLAYER_PED_ID())
				PRINTLN("TASKING PATROL GUARD FOLLOW 2 TO COMBAT")
				guardPatrols.bStartCombatFollows[2] = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF NOT guardPatrols.bStartCombatFollows[3]
		IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[3])
			IF DO_AGGRO_CHECK(guardPatrols.piPatrolGuardFollows[3], NULL, prisonAggroArgs, eAggroReasons)
			OR guardPatrols.bStartCombat[2] = TRUE
			OR bTooCloseAggro
				CLEAR_PED_TASKS(guardPatrols.piPatrolGuardFollows[3])
				TASK_COMBAT_PED(guardPatrols.piPatrolGuardFollows[3], PLAYER_PED_ID())
				PRINTLN("TASKING PATROL GUARD FOLLOW 3 TO COMBAT")
				guardPatrols.bStartCombatFollows[3] = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Follow - No Aggro
	IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuard[1])
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(guardPatrols.piPatrolGuard[1])
			// Tasking to follow 
			IF NOT guardPatrols.bStartCombatFollows[0]
				IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[0])
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(guardPatrols.piPatrolGuardFollows[0], guardPatrols.piPatrolGuard[1], <<-2,0,0>>, PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
			IF NOT guardPatrols.bStartCombatFollows[1]
				IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[1])
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(guardPatrols.piPatrolGuardFollows[1], guardPatrols.piPatrolGuard[1], <<-1,0,0>>, PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuard[2])
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(guardPatrols.piPatrolGuard[2])
			// Tasking to follow
			IF NOT guardPatrols.bStartCombatFollows[2]
				IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[2])
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(guardPatrols.piPatrolGuardFollows[2], guardPatrols.piPatrolGuard[2], <<1,0,0>>, PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
			IF NOT guardPatrols.bStartCombatFollows[3]
				IF NOT IS_PED_INJURED(guardPatrols.piPatrolGuardFollows[3])
					TASK_FOLLOW_TO_OFFSET_OF_ENTITY(guardPatrols.piPatrolGuardFollows[3], guardPatrols.piPatrolGuard[2], <<-1,0,0>>, PEDMOVEBLENDRATIO_WALK)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Clean everything associated with the patrolguard subsystem.
PROC PATROLGUARD_CLEAN()
	INT idx

	REPEAT MAX_NUMBER_PATROL_GUARDS idx
		IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuard[idx])
			IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuard[idx])
				REMOVE_BLIP(guardPatrols.blipPatrolGuard[idx])
				PRINTLN("REMOVING PATROL GUARD BLIPS", idx)
			ENDIF
			
			IF IS_ENTITY_OCCLUDED(guardPatrols.piPatrolGuard[idx])
				DELETE_PED(guardPatrols.piPatrolGuard[idx])
				PRINTLN("DELETING guardPatrols.piPatrolGuard: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(guardPatrols.piPatrolGuard[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDE - guardPatrols.piPatrolGuard, INDEX: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
//	REPEAT MAX_NUMBER_STANDING_GUARDS idx
//		IF DOES_ENTITY_EXIST(guardStanding.piStandingGuard[idx])
//			IF DOES_BLIP_EXIST(guardStanding.blipStandingGuard[idx])
//				REMOVE_BLIP(guardStanding.blipStandingGuard[idx])
//				PRINTLN("REMOVING STANDING GUARD BLIPS", idx)
//			ENDIF
//			
//			DELETE_PED(guardStanding.piStandingGuard[idx])
//			PRINTLN("DELETING guardStanding.piStandingGuard: ", idx)
//		ENDIF
//	ENDREPEAT
	
	REPEAT MAX_NUMBER_PATROL_GUARD_FOLLOWS idx
		IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuardFollows[idx])
			IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuardFollows[idx])
				REMOVE_BLIP(guardPatrols.blipPatrolGuardFollows[idx])
				PRINTLN("REMOVING PATROL GUARD FOLLOW BLIPS", idx)
			ENDIF
			
			IF IS_ENTITY_OCCLUDED(guardPatrols.piPatrolGuardFollows[idx])
				DELETE_PED(guardPatrols.piPatrolGuardFollows[idx])
				PRINTLN("DELETING guardPatrols.piPatrolGuardFollows: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(guardPatrols.piPatrolGuardFollows[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDED - guardPatrols.piPatrolGuardFollows, INDEX: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

