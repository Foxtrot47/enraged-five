// Prison_TowerGuards_lib.sch
USING "commands_clock.sch"

CONST_INT MAX_NUMBER_TOWERS 10
CONST_INT MAX_NUMBER_TOWER_GUARDS 10

STRUCT PRISON_TOWERS
	VECTOR vSpawnPosition[MAX_NUMBER_TOWER_GUARDS]
	FLOAT fSpawnHeading[MAX_NUMBER_TOWER_GUARDS]
	STRING guardTowerRoute[MAX_NUMBER_TOWER_GUARDS]
	BOOL bStartCombat[MAX_NUMBER_TOWER_GUARDS]
	PED_INDEX piTowerGuard[MAX_NUMBER_TOWER_GUARDS]
	BLIP_INDEX blipTowerGuard[MAX_NUMBER_TOWER_GUARDS]
ENDSTRUCT

PRISON_TOWERS prisonTowerGuards

// HACK: To get spotlights.
VEHICLE_INDEX	spotlightHelis[3]
PED_INDEX		spotlightPilots[3]
BOOL			bTowerlightOn[3]
//BOOL 			bForceRenderTowerLight[3]
FLOAT 			fAffectRot = 0.0
BOOL 			bRotPos = TRUE
BOOL			bInsidePrison = FALSE
INT 			iCounter

VECTOR 			vPatrolPathPoint[MAX_NUMBER_TOWER_GUARDS][4]

PROC INIT_TOWER_GUARD_DATA()

	prisonTowerGuards.vSpawnPosition[0] = <<1827.6901, 2474.1807, 61.7202>> //<< 1820.0801, 2476.6326, 61.6894 >>   
	prisonTowerGuards.vSpawnPosition[1] = <<1764.7294, 2409.1394, 61.7533>> //<< 1763.5819, 2407.1338, 61.7407 >> 
	prisonTowerGuards.vSpawnPosition[2] = <<1658.8293, 2390.8882, 61.7462>> //<< 1658.4121, 2390.6538, 61.7306 >>  
	prisonTowerGuards.vSpawnPosition[3] = <<1537.2800, 2468.3381, 61.7497>> //<< 1537.0304, 2471.1284, 61.7383 >>  
	prisonTowerGuards.vSpawnPosition[4] = <<1530.4930, 2585.1724, 61.7001>> //<< 1530.9949, 2585.4912, 61.7411 >>  
	prisonTowerGuards.vSpawnPosition[5] = <<1566.9205, 2682.5247, 61.7716>> //<< 1566.4689, 2682.9478, 61.7588 >>
	prisonTowerGuards.vSpawnPosition[6] = <<1648.1042, 2761.5281, 61.9103>> //<< 1648.2838, 2761.6677, 61.9248 >>
	prisonTowerGuards.vSpawnPosition[7] = <<1774.5229, 2766.5588, 61.9143>> //<< 1774.0535, 2766.6345, 61.9218 >>
	prisonTowerGuards.vSpawnPosition[8] = << 1852.4752, 2697.6318, 61.9547 >> //<< 1852.5692, 2699.2729, 61.9845 >>  
	prisonTowerGuards.vSpawnPosition[9] = <<1824.2876, 2625.0415, 61.9749>> //<< 1827.7363, 2620.3923, 61.9786 >>  
	
//	prisonTowerGuards.vSpawnPosition[0] = << 1824.7705, 2471.9473, 61.6002 >>
//	prisonTowerGuards.vSpawnPosition[1] = << 1658.4121, 2390.6538, 61.7306 >> 
//	prisonTowerGuards.vSpawnPosition[2] = << 1566.4689, 2682.9478, 61.7588 >>
//	prisonTowerGuards.vSpawnPosition[3] = << 1648.2838, 2761.6677, 61.9248 >>
//	prisonTowerGuards.vSpawnPosition[4] = << 1774.0535, 2766.6345, 61.9218 >>
	
	prisonTowerGuards.fSpawnHeading[0] = 248.9733
	prisonTowerGuards.fSpawnHeading[1] = 45.8793
	prisonTowerGuards.fSpawnHeading[2] = 186.3656
	prisonTowerGuards.fSpawnHeading[3] = 313.5206
	prisonTowerGuards.fSpawnHeading[4] = 95.9574
	prisonTowerGuards.fSpawnHeading[5] = 289.3531
	prisonTowerGuards.fSpawnHeading[6] = 13.7511
	prisonTowerGuards.fSpawnHeading[7] = 0
	prisonTowerGuards.fSpawnHeading[8] = 208.5786
	prisonTowerGuards.fSpawnHeading[9] = 280.9389
	
ENDPROC	

PROC SETUP_PATROL_PATHS()

	// TOWER 1
	vPatrolPathPoint[0][0] = <<1827.6901, 2474.1807, 61.7202>>
	vPatrolPathPoint[0][1] = <<1826.0537, 2478.9336, 61.7157>>
	vPatrolPathPoint[0][2] = <<1820.9086, 2477.5276, 61.7153>>
	vPatrolPathPoint[0][3] = <<1822.5847, 2472.1223, 61.7167>>
	
	OPEN_PATROL_ROUTE("miss_Tower_01")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[0][0], <<1879.9438, 2461.9880, 53.5496>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[0][1], <<1831.6309, 2522.2180, 54.1376>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[0][2], <<1789.0701, 2490.5581, 54.1381>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[0][3], <<1789.8002, 2426.8760, 44.7729>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 2
	vPatrolPathPoint[1][0] = <<1764.7294, 2409.1394, 61.7533>>
	vPatrolPathPoint[1][1] = <<1763.1285, 2413.9756, 61.7328>>
	vPatrolPathPoint[1][2] = <<1758.0787, 2411.9839, 61.7403>>
	vPatrolPathPoint[1][3] = <<1760.2131, 2406.8271, 61.7419>>
	
	OPEN_PATROL_ROUTE("miss_Tower_02")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[1][0], <<1879.9438, 2461.9880, 53.5496>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[1][1], <<1831.6309, 2522.2180, 54.1376>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[1][2], <<1789.0701, 2490.5581, 54.1381>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[1][3], <<1789.8002, 2426.8760, 44.7729>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 3
	vPatrolPathPoint[2][0] = <<1658.8293, 2390.8882, 61.7462>>
	vPatrolPathPoint[2][1] = <<1662.5215, 2394.6921, 61.7532>>
	vPatrolPathPoint[2][2] = <<1658.8541, 2398.0615, 61.7573>>
	vPatrolPathPoint[2][3] = <<1655.1500, 2394.7048, 61.7429>>
	
	OPEN_PATROL_ROUTE("miss_Tower_03")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[2][0], <<1655.9081, 2349.0215, 55.1775>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[2][1], <<1733.8748, 2385.5215, 44.9049>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[2][2], <<1662.2343, 2446.3574, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[2][3], <<1524.6310, 2426.9971, 44.6212>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 4
	vPatrolPathPoint[3][0] = <<1537.2800, 2468.3381, 61.7497>>
	vPatrolPathPoint[3][1] = <<1542.1888, 2465.7556, 61.7247>>
	vPatrolPathPoint[3][2] = <<1543.8986, 2470.9707, 61.7482>>
	vPatrolPathPoint[3][3] = <<1539.1404, 2473.2637, 61.7359>>
	
	OPEN_PATROL_ROUTE("miss_Tower_04")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[3][0], <<1455.0807, 2432.6418, 51.3366>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[3][1], <<1576.1744, 2407.0762, 44.8143>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[3][2], <<1578.1000, 2486.2266, 44.5655>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[3][3], <<1512.8368, 2560.4192, 44.8417>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 5
	vPatrolPathPoint[4][0] = <<1535.0978, 2581.9187, 61.7312>>
	vPatrolPathPoint[4][1] = <<1535.1241, 2581.1006, 61.7002>>
	vPatrolPathPoint[4][2] = <<1538.3250, 2585.7224, 61.7251>>
	vPatrolPathPoint[4][3] = <<1534.6793, 2589.2678, 61.7123>>
	
	OPEN_PATROL_ROUTE("miss_Tower_05")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[4][0], <<1484.1997, 2584.4094, 51.9283>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[4][1], <<1535.1536, 2538.9255, 44.4960>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[4][2], <<1570.6615, 2587.8142, 44.5655>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[4][3], <<1538.2266, 2655.5305, 44.9156>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 6
	vPatrolPathPoint[5][0] = <<1566.9205, 2682.5247, 61.7716>>
	vPatrolPathPoint[5][1] = <<1567.9268, 2677.4634, 61.7741>>
	vPatrolPathPoint[5][2] = <<1572.5742, 2678.1926, 61.7702>>
	vPatrolPathPoint[5][3] = <<1572.3588, 2683.0859, 61.7664>>
	
	OPEN_PATROL_ROUTE("miss_Tower_06")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[5][0], <<1505.5382, 2727.2422, 37.6965>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[5][1], <<1545.9504, 2632.5242, 44.7178>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[5][2], <<1599.3546, 2658.3188, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[5][3], <<1599.0406, 2713.3926, 44.4309>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 7
	vPatrolPathPoint[6][0] = <<1648.1042, 2761.5281, 61.9103>>
	vPatrolPathPoint[6][1] = <<1646.0511, 2756.6714, 61.9091>>
	vPatrolPathPoint[6][2] = <<1651.5334, 2754.6677, 61.9021>>
	vPatrolPathPoint[6][3] = <<1653.1254, 2759.3274, 61.9056>>
	
	OPEN_PATROL_ROUTE("miss_Tower_07")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[6][0], <<1606.6610, 2815.6726, 37.9512>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[6][1], <<1602.8872, 2721.3984, 44.6510>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[6][2], <<1599.3546, 2658.3188, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[6][3], <<1719.2246, 2766.1113, 44.6846>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
	// TOWER 8
	vPatrolPathPoint[7][0] = <<1774.5229, 2766.5588, 61.9143>>
	vPatrolPathPoint[7][1] = <<1769.7677, 2763.8162, 61.9248>>
	vPatrolPathPoint[7][2] = <<1772.4415, 2759.1394, 61.9193>>
	vPatrolPathPoint[7][3] = <<1776.8927, 2762.3562, 61.9258>>
	
	OPEN_PATROL_ROUTE("miss_Tower_08")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[7][0], <<1783.2037, 2811.3752, 44.4414>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[7][1], <<1709.2751, 2764.4321, 44.5747>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[7][2], <<1599.3546, 2658.3188, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[7][3], <<1822.4561, 2749.1892, 44.9326>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
//	// TOWER 9
//	vPatrolPathPoint[8][0] = <<1774.5229, 2766.5588, 61.9143>>
//	vPatrolPathPoint[8][1] = <<1769.7677, 2763.8162, 61.9248>>
//	vPatrolPathPoint[8][2] = <<1772.4415, 2759.1394, 61.9193>>
//	vPatrolPathPoint[8][3] = <<1776.6552, 2762.3008, 61.9340>>
//	
//	OPEN_PATROL_ROUTE("miss_Tower_09")
//        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[8][0], <<1606.6610, 2815.6726, 37.9512>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
//		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[8][1], <<1602.8872, 2721.3984, 44.6510>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
//		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[8][2], <<1599.3546, 2658.3188, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
//		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[8][3], <<1719.2246, 2766.1113, 44.6846>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
//		
//        ADD_PATROL_ROUTE_LINK (0, 1)
//		ADD_PATROL_ROUTE_LINK (1, 2)
//		ADD_PATROL_ROUTE_LINK (2, 3)
//		ADD_PATROL_ROUTE_LINK (3, 0)
//    CLOSE_PATROL_ROUTE ()
//    CREATE_PATROL_ROUTE ()

	// TOWER 10
	vPatrolPathPoint[9][0] = <<1824.2876, 2625.0415, 61.9749>>
	vPatrolPathPoint[9][1] = <<1820.5237, 2621.4897, 61.9951>>
	vPatrolPathPoint[9][2] = <<1820.4141, 2621.5444, 61.9908>>
	vPatrolPathPoint[9][3] = <<1823.4504, 2617.4773, 61.9829>>
	
	OPEN_PATROL_ROUTE("miss_Tower_10")
        ADD_PATROL_ROUTE_NODE ( 0, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[9][0], <<1606.6610, 2815.6726, 37.9512>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 1, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[9][1], <<1602.8872, 2721.3984, 44.6510>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 2, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[9][2], <<1599.3546, 2658.3188, 44.5652>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		ADD_PATROL_ROUTE_NODE ( 3, "WORLD_HUMAN_GUARD_STAND", vPatrolPathPoint[9][3], <<1719.2246, 2766.1113, 44.6846>>, GET_RANDOM_INT_IN_RANGE(5000, 10000))
		
        ADD_PATROL_ROUTE_LINK (0, 1)
		ADD_PATROL_ROUTE_LINK (1, 2)
		ADD_PATROL_ROUTE_LINK (2, 3)
		ADD_PATROL_ROUTE_LINK (3, 0)
    CLOSE_PATROL_ROUTE ()
    CREATE_PATROL_ROUTE ()
	
ENDPROC

PROC CREATE_TOWER_GUARDS()
	INT idx
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		IF NOT DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx])
//			IF NOT IS_SPHERE_VISIBLE(prisonTowerGuards.vSpawnPosition[idx], 0.5)
				prisonTowerGuards.piTowerGuard[idx] = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, prisonTowerGuards.vSpawnPosition[idx], prisonTowerGuards.fSpawnHeading[idx])
				PRINTLN("CREATING TOWER GUARD: ", idx)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PRISGUARD_01)
				WAIT(0)
//			ELSE
//				PRINTLN("SPAWN POSITION IS VISIBLE, NOT SPAWNING TOWER GUARD")
//			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

PROC CREATE_SPOTLIGHT_HELICOPTERS()
	INT idx
	
	// HACK: Create spotlight helis with pilots.
	IF NOT IS_SPHERE_VISIBLE(<< 1823.8451, 2621.2671, 57.0 >>, 0.5)
		spotlightHelis[0] = CREATE_VEHICLE(POLMAV, << 1823.8451, 2621.2671, 57.0 >>, 53.0)
		IF DOES_ENTITY_EXIST(spotlightHelis[0]) AND NOT IS_ENTITY_DEAD(spotlightHelis[0])
			// Pilots
			spotlightPilots[0] = CREATE_PED_INSIDE_VEHICLE(spotlightHelis[0], PEDTYPE_COP, S_M_M_PRISGUARD_01)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightPilots[0], FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(spotlightPilots[0], relGuards)
			// Helicopters
			SET_ENTITY_COLLISION(spotlightHelis[0], FALSE)
			SET_ENTITY_VISIBLE(spotlightHelis[0], FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightHelis[0], FALSE)
			FREEZE_ENTITY_POSITION(spotlightHelis[0], TRUE)
		ENDIF
		WAIT(0)
	ENDIF
	IF NOT IS_SPHERE_VISIBLE(<< 1761.4176, 2410.3784, 61.0 >>, 0.5)
		spotlightHelis[1] = CREATE_VEHICLE(POLMAV, << 1761.4176, 2410.3784, 61.0 >>, 13.0)
		IF DOES_ENTITY_EXIST(spotlightHelis[1]) AND NOT IS_ENTITY_DEAD(spotlightHelis[1])
			// Pilots
			spotlightPilots[1] = CREATE_PED_INSIDE_VEHICLE(spotlightHelis[1], PEDTYPE_COP, S_M_M_PRISGUARD_01)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightPilots[1], FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(spotlightPilots[1], relGuards)
			
			// Helicopters
			SET_ENTITY_COLLISION(spotlightHelis[1], FALSE)
			SET_ENTITY_VISIBLE(spotlightHelis[1], FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightHelis[1], FALSE)
			FREEZE_ENTITY_POSITION(spotlightHelis[1], TRUE)
		ENDIF
		WAIT(0)
	ENDIF
	IF NOT IS_SPHERE_VISIBLE(<< 1534.6355, 2585.1621, 61.0 >>, 0.5)
		spotlightHelis[2] = CREATE_VEHICLE(POLMAV, << 1534.6355, 2585.1621, 61.0 >>, 250.0)
		IF DOES_ENTITY_EXIST(spotlightHelis[2]) AND NOT IS_ENTITY_DEAD(spotlightHelis[2])
			// Pilots
			spotlightPilots[2] = CREATE_PED_INSIDE_VEHICLE(spotlightHelis[2], PEDTYPE_COP, S_M_M_PRISGUARD_01)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightPilots[2], FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(spotlightPilots[2], relGuards)
			
			// Helicopters
			SET_ENTITY_COLLISION(spotlightHelis[2], FALSE)
			SET_ENTITY_VISIBLE(spotlightHelis[2], FALSE)
			SET_ENTITY_CAN_BE_DAMAGED(spotlightHelis[2], FALSE)
			FREEZE_ENTITY_POSITION(spotlightHelis[2], TRUE)
		ENDIF
		WAIT(0)
	ENDIF
	
	REPEAT 3 idx
		IF DOES_ENTITY_EXIST(spotlightPilots[idx]) AND NOT IS_ENTITY_DEAD(spotlightPilots[idx])
		AND NOT IS_ENTITY_DEAD(spotlightHelis[idx])
			IF PRISON_SHOULD_LIGHTS_BE_ON()
				bTowerlightOn[idx] = TRUE
				SET_VEHICLE_SEARCHLIGHT(spotlightHelis[idx], TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SETUP_TOWER_GUARDS()
	INT idx
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		// Set Relationship Info
		IF DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx]) AND NOT IS_ENTITY_DEAD(prisonTowerGuards.piTowerGuard[idx])
			SET_PED_RELATIONSHIP_GROUP_HASH(prisonTowerGuards.piTowerGuard[idx], RELGROUPHASH_SECURITY_GUARD)
			SET_PED_CONFIG_FLAG(prisonTowerGuards.piTowerGuard[idx], PCF_KeepRelationshipGroupAfterCleanUp, TRUE)
			SET_PED_TO_INFORM_RESPECTED_FRIENDS(prisonTowerGuards.piTowerGuard[idx], 300, 10)
			SET_PED_SEEING_RANGE(prisonTowerGuards.piTowerGuard[idx], 1000.0)
			SET_PED_ID_RANGE(prisonTowerGuards.piTowerGuard[idx], 1000.0)
			SET_PED_HEARING_RANGE(prisonTowerGuards.piTowerGuard[idx], 1000.0)
			
			// Set Behaviors
			SET_PED_COMBAT_ABILITY(prisonTowerGuards.piTowerGuard[idx], CAL_PROFESSIONAL)
			SET_PED_COMBAT_ATTRIBUTES(prisonTowerGuards.piTowerGuard[idx], CA_AGGRESSIVE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(prisonTowerGuards.piTowerGuard[idx], CA_USE_COVER, FALSE)
			SET_PED_TARGET_LOSS_RESPONSE(prisonTowerGuards.piTowerGuard[idx], TLR_NEVER_LOSE_TARGET)
			SET_PED_COMBAT_MOVEMENT(prisonTowerGuards.piTowerGuard[idx], CM_DEFENSIVE)
			SET_PED_DESIRED_MOVE_BLEND_RATIO(prisonTowerGuards.piTowerGuard[idx], PEDMOVE_STILL)
			SET_PED_KEEP_TASK(prisonTowerGuards.piTowerGuard[idx], TRUE)
			SET_PED_ACCURACY(prisonTowerGuards.piTowerGuard[idx], 20)
			SET_PED_COMBAT_RANGE(prisonTowerGuards.piTowerGuard[idx],CR_FAR)
			SET_PED_COMBAT_ATTRIBUTES(prisonTowerGuards.piTowerGuard[idx], CA_REQUIRES_LOS_TO_SHOOT, FALSE)
	//		SET_PED_FIRING_PATTERN(prisonTowerGuards.piTowerGuard[idx], FIRING_PATTERN_FULL_AUTO)
			
			GIVE_WEAPON_TO_PED(prisonTowerGuards.piTowerGuard[idx], WEAPONTYPE_SNIPERRIFLE, -1, TRUE)
			SET_ENTITY_LOD_DIST(prisonTowerGuards.piTowerGuard[idx], 1000)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(prisonTowerGuards.piTowerGuard[idx], TRUE)

			SET_PED_SEEING_RANGE(prisonTowerGuards.piTowerGuard[idx], 1000.0)
					
			IF NOT IS_PED_INJURED(prisonTowerGuards.piTowerGuard[idx])
				IF idx = 0
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_01", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 1
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_02", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 2
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_03", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 3
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_04", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 4
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_05", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 5
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_06", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 6
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_07", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 7
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_08", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELIF idx = 9
					TASK_PATROL(prisonTowerGuards.piTowerGuard[idx], "miss_Tower_10", PAS_ALERT)
//					PRINTLN("TASKING GUARD ", idx, " TO PATROL")
				ELSE
					TASK_FOLLOW_WAYPOINT_RECORDING(prisonTowerGuards.piTowerGuard[idx], prisonTowerGuards.guardTowerRoute[idx])
//					PRINTLN("TASKING GUARD ", idx, " TO FOLLOW WAYPOINT RECORDING")
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
ENDPROC

// Make all requests for the tower guards system here.
PROC TOWERGUARD_REQUESTS()
	INT idx
	
	prisonTowerGuards.guardTowerRoute[0] = "TowerGuard01"
	prisonTowerGuards.guardTowerRoute[1] = "TowerGuard02"
	prisonTowerGuards.guardTowerRoute[2] = "TowerGuard03"
	prisonTowerGuards.guardTowerRoute[3] = "TowerGuard04"
	prisonTowerGuards.guardTowerRoute[4] = "TowerGuard05"
	prisonTowerGuards.guardTowerRoute[5] = "TowerGuard06"
	prisonTowerGuards.guardTowerRoute[6] = "TowerGuard07"
	prisonTowerGuards.guardTowerRoute[7] = "TowerGuard08"
	prisonTowerGuards.guardTowerRoute[8] = "TowerGuard09"
	prisonTowerGuards.guardTowerRoute[9] = "TowerGuard10"
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		REQUEST_WAYPOINT_RECORDING(prisonTowerGuards.guardTowerRoute[idx])
		PRINTLN("REQUESTING WAYPOINT RECORDING: ", prisonTowerGuards.guardTowerRoute[idx])
	ENDREPEAT
	
	REQUEST_MODEL(POLMAV)
	PRINTLN("REQUESTING - POLMAV")
	REQUEST_MODEL(S_M_M_PRISGUARD_01)
	PRINTLN("REQUESTING - S_M_M_PRISGUARD_01")
ENDPROC

// Check to see if all requests have loaded here.
FUNC BOOL HAS_TOWERGUARD_LOADED()
	INT idx
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		IF NOT GET_IS_WAYPOINT_RECORDING_LOADED(prisonTowerGuards.guardTowerRoute[idx])
			PRINTLN("WAITING FOR WAYPOINT RECORDING: ", prisonTowerGuards.guardTowerRoute[idx])
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	IF NOT HAS_MODEL_LOADED(POLMAV)
		PRINTLN("WAITING ON - POLMAV")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(S_M_M_PRISGUARD_01)
		PRINTLN("WAITING ON - S_M_M_PRISGUARD_01")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Spawn everything needed for the tower guards system here.
PROC TOWERGUARD_SPAWN()

	INIT_TOWER_GUARD_DATA()
	CREATE_TOWER_GUARDS()
	CREATE_SPOTLIGHT_HELICOPTERS()
	SETUP_TOWER_GUARDS()
	SETUP_PATROL_PATHS()
	
	// HACK: Making some invisible helicopters with no collision for spotlights.
ENDPROC

FUNC BOOL IS_PLAYER_CLOSE_TO_GUARDS(PED_INDEX & pedToCheck)
	VECTOR vPlayerPosition, vGuardPosition
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedToCheck)
		vGuardPosition = GET_ENTITY_COORDS(pedToCheck)
	ENDIF
	
	IF VDIST2(vPlayerPosition, vGuardPosition) < 100
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAKE_TOWER_GUARDS_ATTACK(INT idx)
	IF NOT prisonTowerGuards.bStartCombat[idx]
		TASK_SHOOT_AT_ENTITY(prisonTowerGuards.piTowerGuard[idx], PLAYER_PED_ID(), -1, FIRING_TYPE_DEFAULT)
		SET_PED_SEEING_RANGE(prisonTowerGuards.piTowerGuard[idx], 1000.0)
		SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(prisonTowerGuards.piTowerGuard[idx])
		PRINTLN("TASKING PED TO SHOOT AT ENTITY, INDEX = ", idx)
		prisonTowerGuards.bStartCombat[idx] = TRUE
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("SETTING WANTED LEVEL TO 3")
		ENDIF
	ENDIF
ENDPROC

// Update tower guards system here.
PROC TOWERGUARD_UPDATE()
	INT idx
	
	iCounter++
	
	IF iCounter >= 30
		IF NOT bRunWarningCheck
			IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_PRISON, 100)
				REPEAT MAX_NUMBER_TOWER_GUARDS idx
					IF DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx]) AND NOT IS_PED_INJURED(prisonTowerGuards.piTowerGuard[idx])
						IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) != PPS_INVALID
							IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
							OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
								SET_PED_ACCURACY(prisonTowerGuards.piTowerGuard[idx], 10)
								bInsidePrison = TRUE
//								PRINTLN("SETTING ACCURACY TO 10")
							ENDIF
						ELIF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
							SET_PED_ACCURACY(prisonTowerGuards.piTowerGuard[idx], 10)
							bInsidePrison = TRUE
							PRINTLN("SETTING ACCURACY TO 10 - PLAYER IN FLYING VEHICLE")
						ELSE
							SET_PED_ACCURACY(prisonTowerGuards.piTowerGuard[idx], 100)
							bInsidePrison = TRUE
//							PRINTLN("bInsidePrison = TRUE")
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				bInsidePrison = FALSE
//				PRINTLN("bInsidePrison = FALSE")
			ENDIF
		ENDIF
		iCounter = 0
	ENDIF
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		IF DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx]) AND NOT IS_PED_INJURED(prisonTowerGuards.piTowerGuard[idx])
						
			IF IS_PLAYER_WANTED_LEVEL_GREATER(GET_PLAYER_INDEX(), 0) 
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
			ENDIF
			
//			PRINTLN("RUNNING AGGRO CHECK ON INDEX: ", idx)
			
			IF DO_AGGRO_CHECK(prisonTowerGuards.piTowerGuard[idx], NULL, prisonAggroArgs, eAggroReasons)
			OR IS_BULLET_IN_AREA(prisonTowerGuards.vSpawnPosition[idx], 20.0)
			OR IS_SNIPER_BULLET_IN_AREA((prisonTowerGuards.vSpawnPosition[idx] - <<20,20,20>>), (prisonTowerGuards.vSpawnPosition[idx] + <<20,20,20>>))
			OR bInsidePrison
			OR IS_PLAYER_CLOSE_TO_GUARDS(prisonTowerGuards.piTowerGuard[idx])
			
//				IF DO_AGGRO_CHECK(prisonTowerGuards.piTowerGuard[idx], NULL, prisonAggroArgs, eAggroReasons)
//					PRINTLN("AGGRO CHECK PASSED")
//				ENDIF
//				IF IS_BULLET_IN_AREA(prisonTowerGuards.vSpawnPosition[idx], 20.0)
//					PRINTLN("BULLET IN AREA PASSED")
//				ENDIF
//				IF IS_SNIPER_BULLET_IN_AREA((prisonTowerGuards.vSpawnPosition[idx] - <<20,20,20>>), (prisonTowerGuards.vSpawnPosition[idx] + <<20,20,20>>))
//					PRINTLN("SNIPER BULLET IN AREA PASSED")
//				ENDIF
//				IF bInsidePrison
//					PRINTLN("BOOL CHECK PASSED")
//				ENDIF
//				IF IS_PLAYER_CLOSE_TO_GUARDS(prisonTowerGuards.piTowerGuard[idx])
//					PRINTLN("PROXIMITY CHECK PASSED")
//				ENDIF

				IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
					
					IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
						IF bOkayToAttack
							MAKE_TOWER_GUARDS_ATTACK(idx)
						ENDIF
					ELSE
						MAKE_TOWER_GUARDS_ATTACK(idx)
					ENDIF
				ELSE
//					PRINTLN("WE'RE ON A RANDOM EVENT - DO NOT FIRE")
				ENDIF
			ELSE
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(prisonTowerGuards.piTowerGuard[idx])
						TASK_FOLLOW_WAYPOINT_RECORDING(prisonTowerGuards.piTowerGuard[idx], prisonTowerGuards.guardTowerRoute[idx])
						prisonTowerGuards.bStartCombat[idx] = FALSE
//						PRINTLN("THE ROUTE IS OVER, RETASK TO WALK AROUND TOWERS")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(prisonTowerGuards.blipTowerGuard[idx])
				REMOVE_BLIP(prisonTowerGuards.blipTowerGuard[idx])
			ENDIF
		ENDIF
	ENDREPEAT
	
	// HACK: Move the spotlights.
	REPEAT 3 idx
		IF DOES_ENTITY_EXIST(spotlightPilots[idx]) AND NOT IS_ENTITY_DEAD(spotlightPilots[idx]) 
		AND DOES_ENTITY_EXIST(spotlightHelis[idx]) AND NOT IS_ENTITY_DEAD(spotlightHelis[idx])
			VECTOR vAimAt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(spotlightPilots[idx], <<0, 3.5, -2.0>>)
			TASK_VEHICLE_AIM_AT_COORD(spotlightPilots[idx], vAimAt)	
			SET_VEHICLE_ENGINE_ON(spotlightHelis[idx], FALSE, FALSE)
			

			// Check the light activation.
			IF bTowerlightOn[idx]
				IF NOT PRISON_SHOULD_LIGHTS_BE_ON()
					IF NOT IS_ENTITY_DEAD(spotlightHelis[idx])
						bTowerlightOn[idx] = FALSE
						SET_VEHICLE_SEARCHLIGHT(spotlightHelis[idx], FALSE)
						//SET_ENTITY_ALWAYS_PRERENDER(spotlightHelis[idx], FALSE)
					ENDIF
				ENDIF
			ELSE
				IF PRISON_SHOULD_LIGHTS_BE_ON()
					IF NOT IS_ENTITY_DEAD(spotlightHelis[idx])
						bTowerlightOn[idx] = TRUE
						SET_VEHICLE_SEARCHLIGHT(spotlightHelis[idx], TRUE)
						//SET_ENTITY_ALWAYS_PRERENDER(spotlightHelis[idx], TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			// Hack to enable the spotlight to be on 
			/*IF bTowerlightOn[idx]
				VECTOR vHeliPos
				vHeliPos = GET_ENTITY_COORDS(spotlightHelis[idx])
				IF VDIST2(vPlayerPos, vHeliPos) < 90000 // 300m
					IF NOT bForceRenderTowerLight[idx]
						bForceRenderTowerLight[idx] = TRUE
						SET_ENTITY_ALWAYS_PRERENDER(spotlightHelis[idx], TRUE)
					ENDIF
				ELSE
					IF bForceRenderTowerLight[idx]
						bForceRenderTowerLight[idx] = FALSE
						SET_ENTITY_ALWAYS_PRERENDER(spotlightHelis[idx], FALSE)
					ENDIF
				ENDIF
			ENDIF*/
		ENDIF
	ENDREPEAT
	
	// Make them move
	IF (fAffectRot > 20.0)
		bRotPos = FALSE
	ELIF (fAffectRot < -20.0)
		bRotPos = TRUE
	ENDIF
	IF bRotPos
		fAffectRot += 0.25
	ELSE
		fAffectRot -= 0.25
	ENDIF
	IF DOES_ENTITY_EXIST(spotlightHelis[0])
		//FLOAT fHead = GET_ENTITY_HEADING(spotlightHelis[0])
		SET_ENTITY_HEADING(spotlightHelis[0], 53.0 + fAffectRot)
	ENDIF
	
	IF DOES_ENTITY_EXIST(spotlightHelis[1])
		//fHead = GET_ENTITY_HEADING(spotlightHelis[1])
		SET_ENTITY_HEADING(spotlightHelis[1], 13.0 + fAffectRot)
	ENDIF
	
	IF DOES_ENTITY_EXIST(spotlightHelis[2])
		//fHead = GET_ENTITY_HEADING(spotlightHelis[2])
		SET_ENTITY_HEADING(spotlightHelis[2], 250.0 + fAffectRot)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean everything associated with the towerguard subsystem.
PROC TOWERGUARD_CLEAN()
	INT idx
	
	REPEAT 3 idx
		IF DOES_ENTITY_EXIST(spotlightHelis[idx])
			DELETE_VEHICLE(spotlightHelis[idx])
			PRINTLN("DELETING SPOTLIGHT HELI'S: ", idx)
		ENDIF
		IF DOES_ENTITY_EXIST(spotlightPilots[idx])
			DELETE_PED(spotlightPilots[idx])
			PRINTLN("DELETING SPOTLIGHT HELI PEDS: ", idx)
		ENDIF
	ENDREPEAT
	
	REPEAT MAX_NUMBER_TOWER_GUARDS idx
		IF DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx])
			IF DOES_BLIP_EXIST(prisonTowerGuards.blipTowerGuard[idx])
				REMOVE_BLIP(prisonTowerGuards.blipTowerGuard[idx])
				PRINTLN("REMOVING TOWER GUARD BLIPS")
			ENDIF
			
			IF IS_ENTITY_OCCLUDED(prisonTowerGuards.piTowerGuard[idx])
				DELETE_PED(prisonTowerGuards.piTowerGuard[idx])
				PRINTLN("DELETING prisonTowerGuards.piTowerGuard: ", idx)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(prisonTowerGuards.piTowerGuard[idx])
				PRINTLN("SETTING PED AS NO LONGER NEEDED - prisonTowerGuards.piTowerGuard, INDEX: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

