// Prison_Prisoners_lib.sch
CONST_INT		MAX_NUMBER_PRISONERS		20
CONST_INT		MAX_NUMBER_WORKOUT_PRISONERS 3

PED_INDEX		Prisoners[MAX_NUMBER_PRISONERS]
//PED_INDEX		WorkoutPrisoners[MAX_NUMBER_WORKOUT_PRISONERS]
BLIP_INDEX  	blipPrisoners[MAX_NUMBER_PRISONERS]
//BLIP_INDEX 		blipWorkoutPrisoners[MAX_NUMBER_WORKOUT_PRISONERS]
BOOL			bShouldHavePrisoners
//VECTOR			vScenarioPositions[MAX_NUMBER_WORKOUT_PRISONERS]
//FLOAT 			fScenarioHeadings[MAX_NUMBER_WORKOUT_PRISONERS]

// Make all requests for the prisoners system here.
PROC PRISONER_REQUESTS()
	bShouldHavePrisoners = FALSE
	
	IF NOT PRISON_SHOULD_LIGHTS_BE_ON()
		// It's daytime, give us prisoners.
		bShouldHavePrisoners = TRUE
		
		// Request our prisoner model.
		REQUEST_MODEL(S_M_Y_PRISONER_01)
	ENDIF
ENDPROC

// Check to see if all requests have loaded here.
FUNC BOOL HAS_PRISONER_LOADED()
	IF bShouldHavePrisoners
		// No addition things to check for loading yet.
		IF NOT HAS_MODEL_LOADED(S_M_Y_PRISONER_01)
			PRINTLN("Haven't loaded S_M_Y_PRISONER_01.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

// Spawn everything needed for the prisoners system here.
PROC PRISONER_SPAWN()
	INT idx
	
	// Block existing scenarios... to remove muscle beach guys/gals
	ADD_SCENARIO_BLOCKING_AREA(<< 1645.2937, 2531.1960, 44.4052 >>-<<10,10,10>>, << 1645.2937, 2531.1960, 44.4052 >>+<<10,10,10>>)

	IF bShouldHavePrisoners
		// Grassy Area
		Prisoners[0] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1757.1998, 2542.3005, 44.4054 >>, 270.0322)
		Prisoners[1] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1751.8163, 2547.0293, 44.4054 >>, 43.3036)
		Prisoners[2] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1747.5006, 2545.4263, 44.4054 >>, 116.7187)
		Prisoners[3] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1747.5619, 2540.7253, 44.4054 >>, 168.6898)
		
		// Basketball Court
		Prisoners[4] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1717.9097, 2539.7312, 44.4052 >>, 63.8495)
		Prisoners[5] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1721.8210, 2543.4341, 44.4052 >>, 300.0491)
		Prisoners[6] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1726.9661, 2539.2505, 44.4052 >>, 233.0669)
		Prisoners[7] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1720.9940, 2532.4463, 44.4052 >>, 144.9721)
		Prisoners[8] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1723.3790, 2525.9336, 44.4052 >>, 201.9492)
		Prisoners[9] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1731.1072, 2527.5181, 44.4052 >>, 284.1870)
		
		// Hand Ball Court (south side)
		Prisoners[10] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1714.0020, 2528.9185, 44.4052 >>, 149.1826)
		Prisoners[11] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1710.7429, 2530.8135, 44.4052 >>, 62.7950)
		
		// Hand Ball Court (north side)
		Prisoners[12] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1706.9022, 2528.5571, 44.4052 >>, 135.6930)
		Prisoners[13] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1707.9067, 2520.5383, 44.4052 >>, 294.9346)
		
		// Second Basketball Court 
		Prisoners[14] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1684.8003, 2515.5117, 44.4052 >>, 97.4424)
		Prisoners[15] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1678.2651, 2513.5071, 44.4045 >>, 152.7437)
		Prisoners[16] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1690.1620, 2516.5466, 44.4052 >>, 102.7509)
		Prisoners[17] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1683.1696, 2518.3774, 44.4052 >>, 94.7119)
		Prisoners[18] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1676.9913, 2513.3904, 44.4045 >>, 124.6871)
		Prisoners[19] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, << 1677.8088, 2505.6707, 44.4052 >>, 204.2711)
		
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_PRISONER_01)
		
		// Task them to wander.
		REPEAT MAX_NUMBER_PRISONERS idx
			// Grassy Area - 4 guys (0,1,2,3)
			IF idx < 4
				TASK_WANDER_IN_AREA(Prisoners[idx], << 1751.3304, 2545.5432, 44.4054 >>, 10.0)
			// Basketball court - 6 guys (4,5,6,7,8,9)
			ELIF idx >= 4 AND idx < 10
				TASK_WANDER_IN_AREA(Prisoners[idx], << 1724.3732, 2535.4626, 44.4052 >>, 10.0)
			// Hand ball court - 2 guys (north side) (10, 11)
			ELIF idx >= 10 AND idx < 12
				TASK_WANDER_IN_AREA(Prisoners[idx], << 1709.7864, 2530.3442, 44.4052 >>, 3.0)
			// Hand ball court - 2 guys (south side) (12, 13)
			ELIF idx >= 12 AND idx < 14
				TASK_WANDER_IN_AREA(Prisoners[idx], << 1713.6505, 2520.8035, 44.4052 >>, 3.0)
			// Second Basketball court - 4 guys (14, 15, 16, 17, 18, 19)
			ELIF idx >= 14 AND idx <= 19
				TASK_WANDER_IN_AREA(Prisoners[idx], << 1680.7445, 2513.4409, 44.4051 >>, 8.0)
			ENDIF
			
			IF idx % 2 = 0
				SET_PED_RELATIONSHIP_GROUP_HASH(Prisoners[idx], relPrisoners)
			ENDIF
			
//			blipPrisoners[idx] = ADD_BLIP_FOR_ENTITY(Prisoners[idx])
//			SET_BLIP_SCALE(blipPrisoners[idx], 0.5)
//			SET_BLIP_COLOUR(blipPrisoners[idx], BLIP_COLOUR_RED)
			
			SET_ENTITY_LOD_DIST(Prisoners[idx], 500)
		ENDREPEAT
		
//		// --------------------------------------------WORKOUT SCENARIO PEDS --------------------------------------------
//		// Setup spawn data
//		vScenarioPositions[0] = << 1640.7822, 2523.1777, 44.4052 >>
//		vScenarioPositions[1] = << 1643.8464, 2527.2041, 44.4052 >>
//		vScenarioPositions[2] = << 1642.6403, 2534.4448, 44.4052 >>
//		
//		fScenarioHeadings[0] = 160.2526
//		fScenarioHeadings[1] = 342.1075
//		fScenarioHeadings[2] = 342.1252
//		
//		// Create workout peds
//		WorkoutPrisoners[0] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, vScenarioPositions[0], fScenarioHeadings[0])
//		WorkoutPrisoners[1] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, vScenarioPositions[1], fScenarioHeadings[1])
//		WorkoutPrisoners[2] = CREATE_PED(PEDTYPE_CRIMINAL, S_M_Y_PRISONER_01, vScenarioPositions[2], fScenarioHeadings[2])
//		
//		// Task to use scenarios
//		REPEAT MAX_NUMBER_WORKOUT_PRISONERS idx
//			IF DOES_ENTITY_EXIST(WorkoutPrisoners[idx]) AND NOT IS_PED_INJURED(WorkoutPrisoners[idx])
//				IF DOES_SCENARIO_EXIST_IN_AREA(vScenarioPositions[idx], 3.0, TRUE)
//					TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(WorkoutPrisoners[idx], vScenarioPositions[idx], 3.0)
////					blipWorkoutPrisoners[idx] = ADD_BLIP_FOR_ENTITY(WorkoutPrisoners[idx])
////					SET_BLIP_SCALE(blipWorkoutPrisoners[idx], 0.5)
////					SET_BLIP_COLOUR(blipWorkoutPrisoners[idx], BLIP_COLOUR_RED)
//				ELSE
////					PRINTLN("SCENARIO DOESN'T EXIST", idx)
//				ENDIF
//			ENDIF
//		ENDREPEAT
	ENDIF
ENDPROC

// Update prisoners system here.
PROC PRISONER_UPDATE()
	INT idx
	
	IF bShouldHavePrisoners
		REPEAT MAX_NUMBER_PRISONERS idx
			IF DOES_ENTITY_EXIST(Prisoners[idx]) AND IS_PED_INJURED(Prisoners[idx])
				IF DOES_BLIP_EXIST(blipPrisoners[idx])
					REMOVE_BLIP(blipPrisoners[idx])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
//	IF bShouldHavePrisoners
//		REPEAT MAX_NUMBER_WORKOUT_PRISONERS idx
//			IF DOES_ENTITY_EXIST(WorkoutPrisoners[idx]) AND NOT IS_PED_INJURED(WorkoutPrisoners[idx])
//				IF DOES_SCENARIO_EXIST_IN_AREA(vScenarioPositions[idx], 3.0, TRUE)
//					IF NOT IS_PED_USING_ANY_SCENARIO(WorkoutPrisoners[idx])
//						TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(WorkoutPrisoners[idx], vScenarioPositions[idx], 3.0)
////						PRINTLN("RE-TASKING PED TO USE SCENARIOS", idx)
//					ENDIF
//				ELSE
////					PRINTLN("SCENARIO DOESN'T EXIST", idx)
//				ENDIF
//			ELSE
//				IF DOES_BLIP_EXIST(blipWorkoutPrisoners[idx])
//					REMOVE_BLIP(blipWorkoutPrisoners[idx])
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
ENDPROC

/// PURPOSE:
///    Clean everything associated with the prisoners subsystem.
PROC PRISONER_CLEAN()
	INT idx
	
	REPEAT MAX_NUMBER_PRISONERS idx
		IF DOES_ENTITY_EXIST(Prisoners[idx])
			IF DOES_BLIP_EXIST(blipPrisoners[idx])
				REMOVE_BLIP(blipPrisoners[idx])
			ENDIF
			
			DELETE_PED(Prisoners[idx])
			PRINTLN("DELETING Prisoners: ", idx)
		ENDIF
	ENDREPEAT
	
//	REPEAT MAX_NUMBER_WORKOUT_PRISONERS idx
//		IF DOES_ENTITY_EXIST(WorkoutPrisoners[idx])
//			IF DOES_BLIP_EXIST(blipWorkoutPrisoners[idx])
//				REMOVE_BLIP(blipWorkoutPrisoners[idx])
//			ENDIF
//		ENDIF
//		
//		DELETE_PED(WorkoutPrisoners[idx])
//		PRINTLN("DELETING WorkoutPrisoners: ", idx)
//	ENDREPEAT
ENDPROC

