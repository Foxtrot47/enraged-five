// re_Prison.sc


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "re_Prison.sch"
//USING "z_volumes.sch"

/*********************************** GLOBAL VARIABLES ************************************/
AMBIENT_PRISON_STATE 	eState = ePRISON_INIT
PED_INDEX 				playerPed
VECTOR 					vPlayerPos
INT						iBools

REL_GROUP_HASH relGuards
REL_GROUP_HASH relPrisoners

/********************************** SUBLIBRARY INCLUDES **********************************/
USING "Prison_TowerGuards_lib.sch"
USING "Prison_Heli_lib.sch"
USING "Prison_Prisoners_lib.sch"
USING "Prison_PatrolCar_lib.sch"
USING "Prison_PatrolGuards_lib.sch"

/*****************************************************************************************/

// This script init. Not the sublibrary init.
PROC INIT_SCRIPT()
	iBools = iBools	// Temp.
	
//	INIT_ZVOLUME_WIDGETS()
	
	iBools = 0
	
	INIT_NAVMESH_BLOCKING_AREAS()
	
	SET_AGGRO_FIELDS_TO_NOT_CHECK_AND_DISABLE_NAVMESH()
	
	prisonAggroArgs.iGunShotHearRange = 75
	prisonAggroArgs.iShotIRange = 15
	
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_GATE_PRISON_01, <<1845, 2605, 45>>, TRUE)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_GATE_PRISON_01, << 1819.2744, 2608.5369, 44.6195 >>, TRUE, 0, 50)
	
	ADD_RELATIONSHIP_GROUP("Prison_Guards", relGuards)
	ADD_RELATIONSHIP_GROUP("Prison_Prisoners", relPrisoners)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGuards, relPrisoners)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relPrisoners, relGuards)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGuards, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, relGuards)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, relPrisoners)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relPrisoners, RELGROUPHASH_PLAYER)
ENDPROC

// This script cleanup. Should invoke sublibrary cleanups.
PROC CLEANUP_SCRIPT(BOOL bForceClearArea = FALSE)
	INT idx

	// Add your stuff here.
//	COMMON_CLEANUP()
	HELICOPTER_CLEAN()
	TOWERGUARD_CLEAN()
	PRISONER_CLEAN()
	PATROLCAR_CLEAN()
	PATROLGUARD_CLEAN()
	PRISON_ATTACK_PEDS_AND_VEHICLES_CLEAN()
	
	STOP_ALARM("PRISON_ALARMS", TRUE)
	
	IF bPlayerAggroedScenarioPeds
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, RELGROUPHASH_SECURITY_GUARD)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_SECURITY_GUARD, RELGROUPHASH_PLAYER)
		PRINTLN("RESETING GUARD RELATIONSHIP TO PLAYER")
	ENDIF
	
	REPEAT MAX_NUMBER_GATES idx
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iNavMeshBlockingObjects[idx])
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavMeshBlockingObjects[idx])
			PRINTLN("REMOVING NAVMESH BLOCKING OBJECTS, INDEX: ", idx)
		ENDIF
	ENDREPEAT
	
	//Lock prison doors
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_GATE_PRISON_01, <<1845, 2605, 45>>, TRUE)
	SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_GATE_PRISON_01, << 1819.2744, 2608.5369, 44.6195 >>, TRUE, 0, 50)
//	RELEASE_SCRIPT_AUDIO_BANK()

	//Force clear area before terminating
	IF bForceClearArea
		CLEAR_AREA(<<1695.1, 2595.8, 50>>,1000,TRUE)
	ENDIF

	// Don't touch.
	PRINTLN("*********************** KILLING AMBIENT PRISON ***********************")
	TERMINATE_THIS_THREAD()
ENDPROC

PROC TOGGLE_BLIPS()

	#IF IS_DEBUG_BUILD
		INT idx
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_B, KEYBOARD_MODIFIER_ALT, "") 
			
			// Helicopter
			IF DOES_ENTITY_EXIST(policeHeli)
				IF NOT DOES_BLIP_EXIST(blipHeli)
					blipHeli = ADD_BLIP_FOR_ENTITY(policeHeli)
					SET_BLIP_SCALE(blipHeli, 0.75)
					SET_BLIP_COLOUR(blipHeli, BLIP_COLOUR_RED)
					PRINTLN("ADDING BLIP - blipHeli")
				ELSE
					IF DOES_BLIP_EXIST(blipHeli)
						REMOVE_BLIP(blipHeli)
						PRINTLN("REMOVING blipHeli")
					ENDIF
				ENDIF
			ENDIF
			
			// Patrol cars 
			IF DOES_ENTITY_EXIST(patrolCar)
				IF NOT DOES_BLIP_EXIST(blipPatrolCar)
					blipPatrolCar = ADD_BLIP_FOR_ENTITY(patrolCar)
					SET_BLIP_SCALE(blipPatrolCar, 0.75)
					SET_BLIP_COLOUR(blipPatrolCar, BLIP_COLOUR_RED)
					PRINTLN("ADDING BLIP - blipPatrolCar")	
				ELSE
					IF DOES_BLIP_EXIST(blipPatrolCar)
						REMOVE_BLIP(blipPatrolCar)
						PRINTLN("REMOVING blipPatrolCar")
					ENDIF
				ENDIF
			ENDIF
			
			// Prison patrol guards
			REPEAT MAX_NUMBER_PATROL_GUARDS idx
				IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuard[idx])
					IF NOT DOES_BLIP_EXIST(guardPatrols.blipPatrolGuard[idx])
						guardPatrols.blipPatrolGuard[idx] = ADD_BLIP_FOR_ENTITY(guardPatrols.piPatrolGuard[idx])
						SET_BLIP_SCALE(guardPatrols.blipPatrolGuard[idx], 0.5)
						SET_BLIP_COLOUR(guardPatrols.blipPatrolGuard[idx], BLIP_COLOUR_RED)
					ELSE
						IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuard[idx])
							REMOVE_BLIP(guardPatrols.blipPatrolGuard[idx])
							PRINTLN("REMOVING PATROL GUARD BLIPS", idx)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
//			REPEAT MAX_NUMBER_STANDING_GUARDS idx
//				IF DOES_ENTITY_EXIST(guardStanding.piStandingGuard[idx])
//					IF NOT DOES_BLIP_EXIST(guardStanding.blipStandingGuard[idx])
//						guardStanding.blipStandingGuard[idx] = ADD_BLIP_FOR_ENTITY(guardStanding.piStandingGuard[idx])
//						SET_BLIP_SCALE(guardStanding.blipStandingGuard[idx], 0.5)
//						SET_BLIP_COLOUR(guardStanding.blipStandingGuard[idx], BLIP_COLOUR_RED)
//						PRINTLN("ADDING BLIPS - guardStanding.blipStandingGuard: ", idx)
//					ELSE
//						IF DOES_BLIP_EXIST(guardStanding.blipStandingGuard[idx])
//							REMOVE_BLIP(guardStanding.blipStandingGuard[idx])
//							PRINTLN("REMOVING STANDING GUARD BLIPS", idx)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDREPEAT
			REPEAT MAX_NUMBER_PATROL_GUARD_FOLLOWS idx
				IF DOES_ENTITY_EXIST(guardPatrols.piPatrolGuardFollows[idx])
					IF NOT DOES_BLIP_EXIST(guardPatrols.blipPatrolGuardFollows[idx])
						guardPatrols.blipPatrolGuardFollows[idx] = ADD_BLIP_FOR_ENTITY(guardPatrols.piPatrolGuardFollows[idx])
						SET_BLIP_SCALE(guardPatrols.blipPatrolGuardFollows[idx], 0.5)
						SET_BLIP_COLOUR(guardPatrols.blipPatrolGuardFollows[idx], BLIP_COLOUR_RED)
					ELSE
						IF DOES_BLIP_EXIST(guardPatrols.blipPatrolGuardFollows[idx])
							REMOVE_BLIP(guardPatrols.blipPatrolGuardFollows[idx])
							PRINTLN("REMOVING PATROL GUARD FOLLOW BLIPS", idx)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			// Prisoners
			REPEAT MAX_NUMBER_PRISONERS idx
				IF DOES_ENTITY_EXIST(Prisoners[idx])
					IF NOT DOES_BLIP_EXIST(blipPrisoners[idx])
						blipPrisoners[idx] = ADD_BLIP_FOR_ENTITY(Prisoners[idx])
						SET_BLIP_SCALE(blipPrisoners[idx], 0.5)
						SET_BLIP_COLOUR(blipPrisoners[idx], BLIP_COLOUR_RED)
					ELSE
						IF DOES_BLIP_EXIST(blipPrisoners[idx])
							REMOVE_BLIP(blipPrisoners[idx])
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT

			// Tower guards
			REPEAT MAX_NUMBER_TOWER_GUARDS idx
				IF DOES_ENTITY_EXIST(prisonTowerGuards.piTowerGuard[idx])
					IF NOT DOES_BLIP_EXIST(prisonTowerGuards.blipTowerGuard[idx])
						prisonTowerGuards.blipTowerGuard[idx] = ADD_BLIP_FOR_ENTITY(prisonTowerGuards.piTowerGuard[idx])
						SET_BLIP_SCALE(prisonTowerGuards.blipTowerGuard[idx], 0.5)
						SET_BLIP_COLOUR(prisonTowerGuards.blipTowerGuard[idx], BLIP_COLOUR_RED)
					ELSE
						IF DOES_BLIP_EXIST(prisonTowerGuards.blipTowerGuard[idx])
							REMOVE_BLIP(prisonTowerGuards.blipTowerGuard[idx])
							PRINTLN("REMOVING TOWER GUARD BLIPS")
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
	#ENDIF
ENDPROC

PROC HANDLE_PRISON_ALARMS()
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 2
		OR IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_PRISON, 150)
			IF PREPARE_ALARM("PRISON_ALARMS")
				START_ALARM("PRISON_ALARMS", FALSE)
//				PRINTLN("STARTING PRISON ALARM")
			ENDIF
		ELSE
			STOP_ALARM("PRISON_ALARMS", FALSE)
//			PRINTLN("STOPPING PRISON ALARM - PLAYER IS NO LONGER WANTED")
		ENDIF
	ENDIF
ENDPROC

// Main body.
SCRIPT
	PRINTLN("*********************** scriptlaunch: re_Prison.sc ***********************")
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTLN("********** re_Prison.sc FORCE CLEANUP **********")		
		CLEANUP_SCRIPT()
	ENDIF
		
	// Grab the player once. This is for all systems to share.
	playerPed = PLAYER_PED_ID()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
		PRINTLN("CALLING - CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED")
	ENDIF
	
	WHILE TRUE
		// Check to see if we're quitting due to death. This also prevents all subsystems from having to check this.
		IF IS_ENTITY_DEAD(playerPed)
			PRINTLN("re_Prison: 	Player is dead! Terminating.")
			CLEANUP_SCRIPT()
		ENDIF
		
		CHECK_FOR_PLAYER_ATTACKING_GUARDS()
		
		// Check to see if the player has wandered too far away.
		vPlayerPos = GET_ENTITY_COORDS(playerPed)
		//IF VDIST2(vPlayerPos, << 1692.1469, 2562.3127, 44.4056 >>) > PRISON_SCRIPT_QUIT_DIST
		//	PRINTLN("re_Prison: 	Player has left the area! Terminating.")
		//	CLEANUP_SCRIPT()
		//ENDIF
		
		SWITCH (eState)
			CASE ePRISON_INIT
			
					INIT_SCRIPT()
					PRINTLN("GOING TO STATE - ePRISON_REQUEST")
					eState = ePRISON_REQUEST
			BREAK
			
			CASE ePRISON_REQUEST
				IF NOT g_bDisablePrison
					// Requesting everything.
					MAKE_COMMON_REQUESTS()
					HELICOPTER_REQUESTS()
					TOWERGUARD_REQUESTS()
					PATROLGUARD_REQUESTS()
					
					PRINTLN("GOING TO STATE - ePRISON_STREAM")
					eState = ePRISON_STREAM
				ENDIF
			BREAK
			
			CASE ePRISON_STREAM

				// Make sure everything is finished streaming.
				IF HAVE_COMMON_REQUESTS_LOADED() 
				AND HAS_HELICOPTER_LOADED() 
				AND HAS_TOWERGUARD_LOADED() 
				AND HAS_PRISONER_LOADED()
				AND HAS_PATROLGUARD_LOADED()
					PRINTLN("GOING TO STATE - ePRISON_SPAWN")
					eState = ePRISON_SPAWN
				ENDIF
			BREAK
			
			CASE ePRISON_SPAWN
			
				// Spawn everything we need.
				TOWERGUARD_SPAWN()
				WAIT(0)
				HELICOPTER_SPAWN()
				WAIT(0)
				PATROLGUARD_SPAWN()
				
				// Wipe all memory we held in common.
				CLEAR_COMMON_REQUESTS()
				
				PRINTLN("GOING TO STATE - ePRISON_UPDATE")
				eState = ePRISON_UPDATE
			BREAK
			
			CASE ePRISON_UPDATE
				IF NOT g_bDisablePrison

					#IF IS_DEBUG_BUILD
						TOGGLE_BLIPS()
					#ENDIF
					
					IF g_bTriggerSceneActive
	//					PRINTLN("PRISON: GOING TO CLEANUP - g_bTriggerSceneActive IS TRUE")
						CLEANUP_SCRIPT()
					ENDIF
					
					SET_AGGRO_FIELDS_TO_NOT_CHECK_AND_DISABLE_NAVMESH()
					
					HANDLE_PRISON_ALARMS()

					// General update for all systems.
					TOWERGUARD_UPDATE()
					PATROLGUARD_UPDATE()
					HELICOPTER_UPDATE()
					UPDATE_PRISON_ATTACK()

					CHECK_FOR_PLAYER_NEAR_PERIMETER()
					
					#IF IS_DEBUG_BUILD
						IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Quit")
							CLEANUP_SCRIPT()
						ENDIF
					#ENDIF
				ELIF IS_DIRECTOR_MODE_RUNNING()
					CLEANUP_SCRIPT(TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT
