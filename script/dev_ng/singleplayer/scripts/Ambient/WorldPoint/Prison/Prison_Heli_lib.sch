// Prison_Heli_lib.sch
USING "beam_effect.sch"

VEHICLE_INDEX 	policeHeli
PED_INDEX		heliPilot
BLIP_INDEX		blipHeli
BOOL			bSpotlightOn = FALSE
BOOL 			bForceRender = FALSE
BOOL 			bShouldHaveHeli
INT 			iRand

/// PURPOSE:
///   Make all requests for the patrol Heli system here.
PROC HELICOPTER_REQUESTS()
	bShouldHaveHeli = FALSE
	
	IF PRISON_SHOULD_LIGHTS_BE_ON()
		// It's nighttime, give us a heli
		iRand = (GET_RANDOM_INT_IN_RANGE() % 2)
		PRINTLN("iRand = ", iRand)
		
		IF iRand = 0
			bShouldHaveHeli = TRUE
			PRINTLN("bShouldHaveHeli = TRUE")
			REQUEST_MODEL(POLMAV)
			REQUEST_VEHICLE_RECORDING(101, "PrisonHeli")
		ELSE
			bShouldHaveHeli = FALSE
			PRINTLN("bShouldHaveHeli = FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Check to see if all requests have loaded here.
FUNC BOOL HAS_HELICOPTER_LOADED()
	IF bShouldHaveHeli
		IF NOT HAS_MODEL_LOADED(POLMAV)
			PRINTLN("Haven't loaded patrol Heli.")
			RETURN FALSE
		ENDIF
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "PrisonHeli")
			PRINTLN("Haven't heli recording.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   Spawn everything needed for the patrol Heli system here.
PROC HELICOPTER_SPAWN()
	IF bShouldHaveHeli
		policeHeli = CREATE_VEHICLE(POLMAV, <<10, 10, -30>>, 0.0)
		SET_VEHICLE_ENGINE_ON(policeHeli, TRUE, TRUE)
		SET_HELI_BLADES_FULL_SPEED(policeHeli)
		SET_ENTITY_VELOCITY(policeHeli, <<0,0, 10.0>>)
		SET_ENTITY_LOD_DIST(policeHeli, 500)
		
//		blipHeli = ADD_BLIP_FOR_ENTITY(policeHeli)
//		SET_BLIP_SCALE(blipHeli, 0.75)
//		SET_BLIP_COLOUR(blipHeli, BLIP_COLOUR_RED)
		
		IF PRISON_SHOULD_LIGHTS_BE_ON()
			bSpotlightOn = TRUE
			SET_VEHICLE_SEARCHLIGHT(policeHeli, TRUE)
			PRINTLN("---- TURNING HELI LIGHT ON AT START! ----")
		ELSE
			PRINTLN("---- TURNING HELI LIGHT OFF AT START! ----")
		ENDIF

		heliPilot = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, <<10, 10, -20>>, 0.0)
		SET_PED_INTO_VEHICLE(heliPilot, policeHeli)

		IF NOT IS_ENTITY_DEAD(policeHeli)
			START_PLAYBACK_RECORDED_VEHICLE(policeHeli, 101, "PrisonHeli")
		ENDIF

		SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
	ENDIF
ENDPROC

/// PURPOSE:
///   Update patrol Heli system here.
PROC HELICOPTER_UPDATE()
	IF bShouldHaveHeli
		IF NOT IS_ENTITY_DEAD(policeHeli)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(policeHeli)
				START_PLAYBACK_RECORDED_VEHICLE(policeHeli, 101, "PrisonHeli")
			ENDIF
			IF NOT IS_ENTITY_DEAD(heliPilot)
				VECTOR vAimAt = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(heliPilot, <<0, 4.0, -2.0>>)
				TASK_VEHICLE_AIM_AT_COORD(heliPilot, vAimAt)	
			ENDIF
			
			// Do lights need to be on?
			IF bSpotlightOn
				IF NOT PRISON_SHOULD_LIGHTS_BE_ON()
					bSpotlightOn = FALSE
					SET_VEHICLE_SEARCHLIGHT(policeHeli, FALSE)
					PRINTLN("---- TURNING HELI LIGHT OFF! ----")
				ENDIF
			ELSE
				IF PRISON_SHOULD_LIGHTS_BE_ON()
					bSpotlightOn = TRUE
					SET_VEHICLE_SEARCHLIGHT(policeHeli, TRUE)
					PRINTLN("---- TURNING HELI LIGHT ON! ----")
				ENDIF
			ENDIF
			
			// Hack to enable the spotlight to be on 
			IF bSpotlightOn
				VECTOR vHeliPos
				vHeliPos = GET_ENTITY_COORDS(policeHeli)
				IF VDIST2(vPlayerPos, vHeliPos) < 90000 // 300m
					IF NOT bForceRender
						bForceRender = TRUE
//						SET_ENTITY_ALWAYS_PRERENDER(policeHeli, TRUE)
					ENDIF
				ELSE
//					IF bForceRender
//						bForceRender = FALSE
////						SET_ENTITY_ALWAYS_PRERENDER(policeHeli, FALSE)
//					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipHeli)
				REMOVE_BLIP(blipHeli)
				PRINTLN("REMOVING BLIP - blipHeli BECAUSE IT'S DEAD")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_SWITCH_PLANE_TASKS(VEHICLE_INDEX& vehToCheck, PED_INDEX& pedToCheck)

	IF DOES_ENTITY_EXIST(vehToCheck) AND DOES_ENTITY_EXIST(pedToCheck)
	AND NOT IS_ENTITY_DEAD(vehToCheck) AND NOT IS_ENTITY_DEAD(pedToCheck)
		IF IS_ENTITY_IN_AIR(vehToCheck)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					TASK_PLANE_MISSION(pedToCheck, vehToCheck, NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 30.0, 100, 50)
					PRINTLN("SAFE_SWITCH_PLANE_TASKS BEING CALLED")
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean everything associated with the helicopter subsystem.
PROC HELICOPTER_CLEAN()
	IF DOES_BLIP_EXIST(blipHeli)
		REMOVE_BLIP(blipHeli)
	ENDIF
	IF DOES_ENTITY_EXIST(policeHeli)
		IF IS_ENTITY_OCCLUDED(policeHeli)
			DELETE_VEHICLE(policeHeli)
			PRINTLN("DELETING VEHICLE - policeHeli")
			DELETE_PED(heliPilot)
			PRINTLN("DELETING PED - heliPilot")
		ELSE
			SAFE_SWITCH_PLANE_TASKS(policeHeli, heliPilot)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(policeHeli)
			PRINTLN("SETTING VEHICLE AS NO LONGER NEEDED - policeHeli")
		ENDIF
	ENDIF
ENDPROC

