// Prison_PatrolCar_lib.sch
USING "commands_clock.sch"

VEHICLE_INDEX	patrolCar
PED_INDEX		patrolDriver
BLIP_INDEX 		blipPatrolCar
SEQUENCE_INDEX	iSeq
BOOL 			bLightsOn
BOOL 			bStartCombatCar = FALSE

// Make all requests for the patrolcar system here.
PROC PATROLCAR_REQUESTS()
	REQUEST_MODEL(POLICET)
	REQUEST_VEHICLE_RECORDING(101, "PrisonPatrolCar")
	REQUEST_VEHICLE_RECORDING(102, "PrisonPatrolCar")
ENDPROC

// Check to see if all requests have loaded here.
FUNC BOOL HAS_PATROLCAR_LOADED()
	IF NOT HAS_MODEL_LOADED(POLICET)
		PRINTLN("Haven't loaded POLICET.")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "PrisonPatrolCar")
		PRINTLN("Haven't loaded PatrolCar 101 recording")
		RETURN FALSE
	ENDIF
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "PrisonPatrolCar")
		PRINTLN("Haven't loaded PatrolCar 102 recording")
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

// Spawn everything needed for the patrolcar system here.
PROC PATROLCAR_SPAWN()
	// Spawn the car.
	patrolCar = CREATE_VEHICLE(POLICET, << 1773.1880, 2601.8291, 44.6373 >>, 271.9278)
	SET_VEHICLE_ENGINE_ON(patrolCar, TRUE, TRUE)
	SET_ENTITY_LOD_DIST(patrolCar, 500)
	SET_VEHICLE_DOORS_LOCKED(patrolCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)

	// Turn the lights on at night.
	IF GET_CLOCK_HOURS() > 18 AND GET_CLOCK_HOURS() < 7
		SET_VEHICLE_LIGHTS(patrolCar, FORCE_VEHICLE_LIGHTS_ON)
		bLightsOn = TRUE
	ELSE
		bLightsOn = FALSE
	ENDIF
	
	// Spawn the guy, force him into the car.
	patrolDriver = CREATE_PED(PEDTYPE_COP, S_M_M_PRISGUARD_01, <<10, 10, -20>>, 0.0)
	SET_PED_INTO_VEHICLE(patrolDriver, patrolCar)
	SET_PED_CAN_BE_TARGETTED(patrolDriver, TRUE)
	SET_PED_CONFIG_FLAG(patrolDriver, PCF_WillFlyThroughWindscreen, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(patrolDriver, TRUE)
	GIVE_WEAPON_TO_PED(patrolDriver, WEAPONTYPE_ASSAULTRIFLE, -1, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(patrolDriver, RELGROUPHASH_SECURITY_GUARD)
	
	// Release the model.
	SET_MODEL_AS_NO_LONGER_NEEDED(POLICET)
ENDPROC

// Update patrolcar system here.
PROC PATROLCAR_UPDATE()
	// If we're at the end of our recording, and we're occluded, restart the recording.
	
	IF IS_ENTITY_DEAD(patrolDriver)
		IF DOES_BLIP_EXIST(blipPatrolCar)
			REMOVE_BLIP(blipPatrolCar)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(patrolCar)
		
		IF NOT bStartCombatCar
			IF DO_AGGRO_CHECK(patrolDriver, patrolCar, prisonAggroArgs, eAggroReasons, TRUE)
				
				PRINTLN("AGGRO CHECK 01 HAS RETURNED TRUE!!!!!!!!!")
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(patrolCar)
					STOP_PLAYBACK_RECORDED_VEHICLE(patrolCar)
					PRINTLN("STOPPING RECORDING ON patrolCar")
				ENDIF
				
				OPEN_SEQUENCE_TASK(iSeq)
					TASK_LEAVE_VEHICLE(NULL, patrolCar, ECF_DONT_CLOSE_DOOR)
					TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
				CLOSE_SEQUENCE_TASK(iSeq)
				TASK_PERFORM_SEQUENCE(patrolDriver, iSeq)
				CLEAR_SEQUENCE_TASK(iSeq)
				bStartCombatCar = TRUE
			ENDIF
		ENDIF
		
		IF NOT bStartCombatCar
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(patrolCar)
				IF IS_ENTITY_OCCLUDED(patrolCar)
					START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(patrolCar, 101, "PrisonPatrolCar", ENUM_TO_INT(TURN_ON_ENGINE_INSTANTLY | SWITCH_ON_ANY_VEHICLE_IMPACT), 1000)
				ENDIF
			ENDIF
		ENDIF
		
		// Headlight TOD Check.
		IF GET_CLOCK_HOURS() > 18 AND GET_CLOCK_HOURS() < 7
			IF NOT bLightsOn
				SET_VEHICLE_LIGHTS(patrolCar, FORCE_VEHICLE_LIGHTS_ON)
				bLightsOn = TRUE
			ENDIF
		ELSE
			IF bLightsOn
				SET_VEHICLE_LIGHTS(patrolCar, FORCE_VEHICLE_LIGHTS_OFF)
				bLightsOn = FALSE
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipPatrolCar)
			REMOVE_BLIP(blipPatrolCar)
			PRINTLN("REMOVING blipPatrolCar BECAUSE IT'S DEAD")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean everything associated with the patrolcar subsystem.
PROC PATROLCAR_CLEAN()

	IF DOES_BLIP_EXIST(blipPatrolCar)
		REMOVE_BLIP(blipPatrolCar)
		PRINTLN("REMOVING blipPatrolCar")
	ENDIF
	IF DOES_ENTITY_EXIST(patrolCar)
		DELETE_VEHICLE(patrolCar)
		PRINTLN("DELETING patrolCar")
	ENDIF
ENDPROC

