// 															Random Event: Shop Robbery

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

// Includes -----------------------------------------------//

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_interiors.sch"
USING "commands_camera.sch"
USING "LineActivation.sch"
USING "dialogue_public.sch"
USING "shop_public.sch"
USING "commands_fire.sch"
USING "script_blips.sch"
USING "commands_path.sch"
USING "Ambient_Common.sch"
USING "Ambient_Approach.sch"
USING "shop_public.sch"
USING "script_ped.sch"
USING "cutscene_public.sch"
USING "completionpercentage_public.sch"
USING "building_control_public.sch"
USING "commands_event.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

// Variables ----------------------------------------------//
ENUM ROBBERY_STAGES
	STAGE_CREATE_SCENE,
	STAGE_RUN_ROBBERY,
	STAGE_CLEAN_UP
ENDENUM
ROBBERY_STAGES RobberyStage = STAGE_CREATE_SCENE

ENUM RUN_ROBBERY_STAGES
	ROBBERY_PLAYER_ENTERED,
	ROBBERY_COMMENCES,
	ROBBERY_AFTERMATH,
	//LEFT_AREA,
	//IGNORED_INSTRUCTION,
	//ARMED_AND_SHOOTING,
	//STANDOFF_SITUATION,
	ROBBERS_ATTACKED,
	ROBBERS_ATTACKED_DEAD_CHECK,
	HARMED_ROBBERS,
	RETRIEVE_BAG_FROM_VEH,
	PREVENTED_GETAWAY,
	DRIVING_AWAY,
	STOLEN_STUFF_DROPPED,
	PICKED_UP_STOLEN_STUFF, 
	BACK_IN_SHOP,
	LEFT_THE_SCENE,
	END_SCRIPT_INSIDE_INTERIOR,
	RUN_OUT_OF_TIME
ENDENUM
RUN_ROBBERY_STAGES RunRobberyStage = ROBBERY_PLAYER_ENTERED

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventVariations
	TARGET_NONE,
	HAIRDRESSERS_HOLDUP,
	CLOTHES_SHOP_HOLDUP,
	TARGET_END
ENDENUM
eventVariations thisEvent = TARGET_NONE

VECTOR vInput

BOOL bAssetsLoaded
BOOL bVariablesInitialised

CONST_INT iMaxNoOfRobbers 2
CONST_INT iMaxNoOfWorkers 3
CONST_INT iMaxNoOfShoppers 4

VECTOR vRobberInitialPos[iMaxNoOfRobbers]
FLOAT fRobberInitialHdg[iMaxNoOfRobbers]

VECTOR vWorkerPedsPos[iMaxNoOfWorkers]
FLOAT fWorkerPedsHdg[iMaxNoOfWorkers]

VECTOR vShoppersPedsPos[iMaxNoOfShoppers]
FLOAT fShoppersPedsHdg[iMaxNoOfShoppers]

VECTOR vBagCreationPos
VECTOR vBagCreationRot

VECTOR vFleeCoords

INT iPlacementFlags = 0
VECTOR vDroppedItemPlacement
PICKUP_INDEX pickupStolenBag

INT iNoOfRobbers
INT iNoOfWorkers
INT iNoOfShoppers

PED_INDEX pedRobber[iMaxNoOfRobbers]
PED_INDEX pedWorker[iMaxNoOfWorkers]
PED_INDEX pedShopper[iMaxNoOfShoppers]

//INT i // counter
INT iAimingTimer
//INT iAimingCurrentTimer

INT iWhichWorldPoint
VECTOR vWorldPoint1 = << -824.1719, -187.6540, 36.5707 >> // hairdressers 
VECTOR vWorldPoint2 = << -1202.94, -779.74, 17.33 >>	// clothes store
CONST_INT iWorldPoint1	1
CONST_INT iWorldPoint2	2
INT iSelectedVariation

BOOL bStreamInterior
BOOL bAcknowledgedPlayer
BOOL bCreatedScene
BOOL bRobbersFled
BOOL bBreakingOut
BOOL bRobbersFightingBack

BOOL bReactedToPlayerAttack[2], bReactedToDriverGone[2]

BOOL bRobber0Lost
//BOOL bRobber1Lost = FALSE
BOOL bGetawayVehLost

BOOL bRobber0Gone
BOOL bRobber1Gone
BOOL bDriverGone

SEQUENCE_INDEX seq
INTERIOR_INSTANCE_INDEX INTID_ROBBERY_INTERIOR

BOOL bPhSpeech1
BOOL bPhSpeech2
BOOL bPhSpeech3
BOOL bPhSpeech5

BOOL bPhSpeech7
//BOOL bPhSpeech8 = FALSE
//BOOL bPhSpeech9 = FALSE
//BOOL bPhSpeech10 = FALSE
//BOOL bAimTimerStarted  = FALSE
//BOOL b2ndRobberAimingAtPlayer  = FALSE
BOOL bToldToEmptyRegister

BOOL bHTGoods
BOOL bCreateBagAtVehicle
BOOL bFleeInVeh

BOOL bGetAwayDriverTask

BOOL bPlayerGotUp
BOOL bRobber0NotInSalon
BOOL bRobber1NotInSalon

OBJECT_INDEX objIdBag
OBJECT_INDEX objCash[2]
//WEAPON_TYPE CurrentWeapon

VEHICLE_INDEX vehPlayersStoredVehicle
VECTOR vGetawayCreationPoint

VEHICLE_INDEX vehGetawayVeh
BOOL bGetawayVehCreated
BOOL bPlayerGotDown
BOOL bRobberGoToBag
BOOL bGetawayModelLoaded
BOOL bPedsGivenApreRobberyTasks
BOOL bReadyToGiveBagBack
BOOL bGetToHandoverPosition

BOOL bStartedHandoverSyncScene

PED_INDEX pedDriver
BLIP_INDEX blipRobber[iMaxNoOfRobbers]

MODEL_NAMES modelRobbersBag
INT iStolenMoney

REL_GROUP_HASH robbersRelGroup

BLIP_INDEX blipGetawayVeh
BLIP_INDEX blipBag

BLIP_INDEX blipShopWorker
VECTOR vLocReturnBag
FLOAT fStoreBagCollectDist

VECTOR scenePosition, vSyncRobberyPosition
VECTOR sceneRotation, vSyncRobberyRotation

CAMERA_INDEX CamIDHandingOverBag
STRING ambrobberydict
STRING return_bag_player
STRING return_bag_female
STRING return_bag_bag
STRING return_bag_cam
INT sceneId
FLOAT fSyncPhase

//INTERIOR_INSTANCE_INDEX INTID_PED_INTERIOR

MODEL_NAMES modelShopworkerMale
MODEL_NAMES modelShopworkerFemale
MODEL_NAMES modelCustomerMale
MODEL_NAMES modelCustomerFemale
MODEL_NAMES modelRobber

//VECTOR vInteriorLocCoord

BOOL bDoFullCleanUp

BOOL bSpeedUp
BOOL bCalledCops

CAMERA_INDEX camQuickShot
INT iQuickCamCutStage
INT iCamShotCounter

INT iBlipTimer
INT iFleeTimer
BOOL bCustomersFledStore

structPedsForConversation RobberyDialogueStruct 
// Functions ----------------------------------------------//

FUNC VECTOR getWorldPoint()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		FLOAT fDistanceToClosestWorldPoint
		
		fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint1)
		iWhichWorldPoint = iWorldPoint1
		
		IF GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2) < fDistanceToClosestWorldPoint
			fDistanceToClosestWorldPoint = GET_DISTANCE_BETWEEN_COORDS((GET_ENTITY_COORDS(PLAYER_PED_ID())), vWorldPoint2)
			iWhichWorldPoint = iWorldPoint2
		ENDIF
			
		IF iWhichWorldPoint = iWorldPoint1
			iSelectedVariation = iWorldPoint1
			thisEvent = HAIRDRESSERS_HOLDUP
			RETURN vWorldPoint1
		ELIF iWhichWorldPoint = iWorldPoint2
			iSelectedVariation = iWorldPoint2
			thisEvent = CLOTHES_SHOP_HOLDUP
			RETURN vWorldPoint2
		ENDIF
		
	ENDIF
	
	RETURN << 0, 0, 0 >>
ENDFUNC

PROC loadAssets()

//	IF NOT DOES_BLIP_EXIST(blipShopWorker)
//		blipShopWorker = CREATE_AMBIENT_BLIP_INITIAL_COORD(vLocReturnBag)
//	ENDIF
	REQUEST_MODEL(modelShopworkerFemale)
	REQUEST_MODEL(modelCustomerMale)
	REQUEST_MODEL(modelCustomerFemale)
	REQUEST_MODEL(modelRobber)
	REQUEST_MODEL(prop_anim_cash_pile_01)
	REQUEST_MODEL(Prop_CS_Duffel_01)
	REQUEST_WAYPOINT_RECORDING("re_shoprobbery")
	REQUEST_WAYPOINT_RECORDING("re_shoprobbery2")
//	REQUEST_ANIM_DICT("amb@robbery")
	REQUEST_ANIM_DICT("random@robbery")
	REQUEST_ANIM_DICT("random@shop_robbery")
	PREPARE_MUSIC_EVENT("RE51A_SHOP")
	IF HAS_MODEL_LOADED(modelShopworkerFemale)
	AND HAS_MODEL_LOADED(modelCustomerMale)
	AND HAS_MODEL_LOADED(modelCustomerFemale)
	AND HAS_MODEL_LOADED(modelRobber)
	AND HAS_MODEL_LOADED(prop_anim_cash_pile_01)
	AND HAS_MODEL_LOADED(Prop_CS_Duffel_01)
	AND GET_IS_WAYPOINT_RECORDING_LOADED("re_shoprobbery")
	AND GET_IS_WAYPOINT_RECORDING_LOADED("re_shoprobbery2")
//	AND HAS_ANIM_DICT_LOADED("amb@robbery")
	AND HAS_ANIM_DICT_LOADED("random@robbery")
	AND HAS_ANIM_DICT_LOADED("random@shop_robbery")
	AND PREPARE_MUSIC_EVENT("RE51A_SHOP")
		bAssetsLoaded = TRUE
	ELSE
		REQUEST_MODEL(modelShopworkerFemale)
		REQUEST_MODEL(modelCustomerMale)
		REQUEST_MODEL(modelCustomerFemale)
		REQUEST_MODEL(modelRobber)
		REQUEST_MODEL(prop_anim_cash_pile_01)
		REQUEST_MODEL(Prop_CS_Duffel_01)
//		REQUEST_ANIM_DICT("amb@robbery")
		REQUEST_ANIM_DICT("random@robbery")
		REQUEST_ANIM_DICT("random@shop_robbery")
		PREPARE_MUSIC_EVENT("RE51A_SHOP")
	ENDIF
ENDPROC

PROC initialiseEventVariables()
	
	bReactedToPlayerAttack[0] = FALSE
	bReactedToPlayerAttack[1] = FALSE
		
		IF thisEvent = HAIRDRESSERS_HOLDUP
			modelShopworkerFemale = A_F_Y_BevHills_02 //A_F_Y_Hipster_04
			modelCustomerMale = A_M_Y_BevHills_02 //A_M_Y_Hipster_01
			modelCustomerFemale = A_F_Y_BevHills_02
			
			modelRobber = G_M_Y_Korean_01
			
			iNoOfRobbers = 2
			iNoOfWorkers = 3
			iNoOfShoppers = 3
			
			vRobberInitialPos[0] = << -821.446106, -184.847443, 37.566795 >>
			fRobberInitialHdg[0] = 252.3810
			
			vRobberInitialPos[1] = << -819.4664, -186.8146, 36.5671 >>
			fRobberInitialHdg[1] = -153.956223
			
			vWorkerPedsPos[0] = << -821.9410, -183.3325, 36.5689 >>
			fWorkerPedsHdg[0] = 203.3684
			
			vWorkerPedsPos[1] = << -813.6672, -185.5652, 36.5689 >>
			fWorkerPedsHdg[1] = 90.1761
			
			vWorkerPedsPos[2] = << -816.1049, -183.9966, 36.5689 >>
			fWorkerPedsHdg[2] = 192.7338
			
			vShoppersPedsPos[0] = << -813.008, -184.931, 37.869 >>
			fShoppersPedsHdg[0] = 337.680
			
			vShoppersPedsPos[1] = << -817.5606, -190.8273, 37.3114 >>
			fShoppersPedsHdg[1] = 25.8399
			
			vShoppersPedsPos[2] = << -816.1862, -182.8970, 37.6304 >>
			fShoppersPedsHdg[2] = 30.0000
			
			vBagCreationPos = << -822.2148, -184.0822, 37.7027 >>//, 30.0017
			vBagCreationRot = << -1.8236 , 0.6172 , 75.8024 >>
			
			vFleeCoords = << -808.5918, -180.0378, 36.5689 >>
			
			modelRobbersBag = Prop_CS_Duffel_01
			iStolenMoney = 2000
			
			vLocReturnBag = << -822.8538, -187.7645, 36.5792 >>
			//vInteriorLocCoord = << -818.7802, -184.5899, 36.5680 >>
			
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			modelShopworkermale = A_M_Y_Hipster_01
			modelShopworkerFemale = A_F_Y_Hipster_04
			modelCustomerMale = A_M_Y_Hipster_01
			modelCustomerFemale = A_F_Y_Hipster_04 //A_F_Y_BevHills_02
			
			modelRobber = G_M_Y_StrPunk_02
			
			iNoOfRobbers = 2
			iNoOfWorkers = 2
			iNoOfShoppers = 2
			
			vRobberInitialPos[0] = << -1193.045, -769.304, 16.321 >>
			fRobberInitialHdg[0] = -85.500
			
			vRobberInitialPos[1] = << -1199.0454, -778.8358, 16.3277 >> //<< -1191.7655, -774.6216, 16.3330 >>
			fRobberInitialHdg[1] = 329.1899 //51.2330
			
			vWorkerPedsPos[0] = << -1193.8021, -766.8922, 16.3310 >>
			fWorkerPedsHdg[0] = 221.3152
			
			vWorkerPedsPos[1] = << -1193.8497, -771.4392, 16.3227 >>
			fWorkerPedsHdg[1] = 247.1149
			
			vShoppersPedsPos[0] = << -1184.3588, -767.0463, 16.3468 >>
			fShoppersPedsHdg[0] = 103.3015
			
			vShoppersPedsPos[1] = << -1185.9325, -770.6240, 16.3440 >>
			fShoppersPedsHdg[1] = 43.2150
			
			vBagCreationPos = << -1192.9675, -767.0651, 17.2968 >>
			vBagCreationRot = << 0.0000, 0.0000, -128.5200 >>
			
			vFleeCoords = << -1180.4567, -763.9163, 16.3267 >>
			
			modelRobbersBag = Prop_CS_Duffel_01 //Prop_Med_Bag_01b
			iStolenMoney = 2000
			
			vLocReturnBag =  << -1201.0062, -777.4955, 16.3235 >>
			// vInteriorLocCoord = << -1198.7549, -775.4148, 16.3263 >>
		ENDIF
		
		bVariablesInitialised = TRUE

ENDPROC

FUNC BOOL IS_PLAYER_IN_ROBBERY_INTERIOR()
//	INTID_PED_INTERIOR = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
//	IF INTID_PED_INTERIOR = INTID_ROBBERY_INTERIOR
//	AND INTID_PED_INTERIOR <> NULL
//		RETURN TRUE
//	ENDIF
	IF thisEvent = HAIRDRESSERS_HOLDUP
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -803.459778, -175.813522, 36.283669 >>, << -823.024231, -187.046219, 40.535473>>, 11.9375)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -823.370605, -188.060181, 36.621624 >>, << -822.836060, -187.746613, 39.069138>>, 1.5000)
			RETURN TRUE
		ENDIF
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1204.030640, -779.628967, 16.335648 >>, << -1174.798584, -757.070374, 21.012814>>, 12.1250)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1203.899170, -776.917786, 20.168028 >>, << -1201.289307, -780.469849, 15.615259>>, 3.9375)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1206.544678, -777.894104, 21.203548 >>, << -1204.313354, -778.224487, 16.084339>>, 2.2500)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1202.927612, -782.688293, 21.203688 >>, << -1202.583008, -780.506653, 16.082426>>, 2.2500)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

// Clean Up
PROC missionCleanup()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ missionCleanup() @@@@@@@@@@@@@@@ \n")
    #IF IS_DEBUG_BUILD
        IF can_cleanup_script_for_debug
			INT i
			FOR i = 0 to (iNoOfRobbers - 1)
				IF DOES_ENTITY_EXIST(pedRobber[i])
					DELETE_PED(pedRobber[i])
				ENDIF
				IF DOES_BLIP_EXIST(blipRobber[i])
					REMOVE_BLIP(blipRobber[i])
				ENDIF
			ENDFOR
			FOR  i = 0 to (iNoOfShoppers - 1)
				IF DOES_ENTITY_EXIST(pedShopper[i])
					DELETE_PED(pedShopper[i])
				ENDIF
			ENDFOR
			FOR  i = 0 to (iNoOfWorkers - 1)
				IF DOES_ENTITY_EXIST(pedWorker[i])
					DELETE_PED(pedWorker[i])
				ENDIF
			ENDFOR
			IF DOES_ENTITY_EXIST(objIdBag)
				DELETE_OBJECT(objIdBag)
			ENDIF
		ENDIF
    #ENDIF
	
	IF bDoFullCleanUp
		
		DISABLE_CELLPHONE(FALSE)
		CANCEL_MUSIC_EVENT("RE51A_SHOP")
		
		REMOVE_SCENARIO_BLOCKING_AREAS()
		SET_ROADS_BACK_TO_ORIGINAL(<< -871, -246, 0 >>, << -798, -163, 50 >>)
		REMOVE_WAYPOINT_RECORDING("re_shoprobbery")
		REMOVE_WAYPOINT_RECORDING("re_shoprobbery2")
		
		INT i
		FOR i = 0 to (iNoOfRobbers - 1)
			IF DOES_ENTITY_EXIST(pedRobber[i])
				IF NOT IS_PED_INJURED(pedRobber[i])
				AND NOT IS_PED_IN_ANY_VEHICLE(pedRobber[i])
					TASK_SMART_FLEE_COORD(pedRobber[i], vFleeCoords, 150, -1)
					SET_PED_KEEP_TASK(pedRobber[i], TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(pedRobber[i])
			ENDIF
			IF DOES_BLIP_EXIST(blipRobber[i])
				REMOVE_BLIP(blipRobber[i])
			ENDIF
		ENDFOR
		IF DOES_BLIP_EXIST(blipBag)
			REMOVE_BLIP(blipBag)
		ENDIF
		
		FOR i = 0 to (iNoOfShoppers - 1)
			IF DOES_ENTITY_EXIST(pedShopper[i])
				IF NOT IS_PED_INJURED(pedShopper[i])
					SET_ENTITY_LOAD_COLLISION_FLAG(pedShopper[i], TRUE)
				//	IF GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_ENTITY_COORDS(pedShopper[i]), "V_Hairdresser") = NULL
//						IF IS_PLAYER_IN_ROBBERY_INTERIOR()
//							IF i = 0 OR i = 2
//								IF i = 0
//									scenePositionExit = << -812.370, -185.650, 37.105 >>
//									sceneRotationExit = << 0.000, 0.000, 37.680 >>
//								ELIF i = 2
//									scenePositionExit = << -816.224, -182.897, 37.180 >>
//									sceneRotationExit = << 0.000, 0.000, 30.000 >>
//								ENDIF
//								INT syncId = CREATE_SYNCHRONIZED_SCENE(scenePositionExit, sceneRotationExit)
//								TASK_SYNCHRONIZED_SCENE(pedShopper[i], syncId, "amb@prop_human_seat_chair@female@proper@react_flee", "exit_fwd", FAST_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_BLOCK_MOVER_UPDATE)
//							ELSE
								OPEN_SEQUENCE_TASK(seq)
									IF thisEvent = HAIRDRESSERS_HOLDUP
										TASK_PLAY_ANIM(NULL, "random@robbery", "exit_flee", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION|AF_OVERRIDE_PHYSICS|AF_EXTRACT_INITIAL_OFFSET, 0, FALSE, AIK_DISABLE_LEG_IK)
									ELIF thisEvent = CLOTHES_SHOP_HOLDUP
										CLEAR_PED_TASKS(pedShopper[i])
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1201.4758, -790.2733, 15.5475 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
									ENDIF
									TASK_SMART_FLEE_COORD(NULL, vFleeCoords, 150, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedShopper[i], seq)
								CLEAR_SEQUENCE_TASK(seq)
//							ENDIF
//						ELSE
//							TASK_COWER(pedShopper[i], -1)
//						ENDIF
						SET_PED_KEEP_TASK(pedShopper[i], TRUE)
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], FALSE)
//						SET_PED_AS_NO_LONGER_NEEDED(pedShopper[i])
				//	ENDIF
				ENDIF
			ENDIF
		ENDFOR
		FOR i = 0 to (iNoOfWorkers - 1)
			IF DOES_ENTITY_EXIST(pedWorker[i])
				IF NOT IS_PED_INJURED(pedWorker[i])
					SET_PED_CONFIG_FLAG(pedWorker[i], PCF_CanSayFollowedByPlayerAudio, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(pedWorker[i], TRUE)
//					IF IS_PLAYER_IN_ROBBERY_INTERIOR()
						IF i = 0
							IF NOT bStartedHandoverSyncScene
								CLEAR_PED_TASKS(pedWorker[i])
								TASK_SMART_FLEE_COORD(pedWorker[i], vFleeCoords, 150, -1)
							ELSE
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[i], FALSE)
							ENDIF
						ELSE
							OPEN_SEQUENCE_TASK(seq)
								IF thisEvent = CLOTHES_SHOP_HOLDUP
									CLEAR_PED_TASKS(pedWorker[i])
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1201.4758, -790.2733, 15.5475 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
								ENDIF
								TASK_SMART_FLEE_COORD(NULL, vFleeCoords, 150, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedWorker[i], seq)
							CLEAR_SEQUENCE_TASK(seq)
						ENDIF
//					ELSE
//						TASK_COWER(pedWorker[i], -1)
//					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[i], FALSE)
					SET_PED_CAN_BE_TARGETTED(pedWorker[i], TRUE)
					PRINTLN("@@@@@@@@@@@@@@ SET_PED_CAN_BE_TARGETTED(pedWorker[i], TRUE) @@@@@@@@@@@@@")
//					SET_PED_AS_NO_LONGER_NEEDED(pedWorker[i])
				ENDIF
			ENDIF
		ENDFOR
		IF DOES_ENTITY_EXIST(pedDriver)
			SET_PED_AS_NO_LONGER_NEEDED(pedDriver)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objIdBag)
			IF IS_ENTITY_ATTACHED(objIdBag)
				DETACH_ENTITY(objIdBag)
			ENDIF
			SET_OBJECT_AS_NO_LONGER_NEEDED(objIdBag)
		ENDIF
		IF DOES_ENTITY_EXIST(vehGetawayVeh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGetawayVeh)
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
		ENDIF
		
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		SET_MAX_WANTED_LEVEL(5)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		IF thisEvent = HAIRDRESSERS_HOLDUP
			SET_SHOP_IS_BEING_ROBBED(HAIRDO_SHOP_01_BH, FALSE)
			LOCK_SHOP_DOORS(HAIRDO_SHOP_01_BH, TRUE)
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			SET_SHOP_IS_BEING_ROBBED(CLOTHES_SHOP_M_01_SM, FALSE)
			LOCK_SHOP_DOORS(CLOTHES_SHOP_M_01_SM, TRUE)
		ENDIF
	ENDIF
	
	RANDOM_EVENT_OVER()
	
	// Do none of the above, just what is below, if the script has been set to clean up straight away
	//  v               v               v         v             v            v           v           v            v         v         v
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
	RANDOM_EVENT_PASSED(RE_SHOPROBBERY, iSelectedVariation)
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	missionCleanup()
ENDPROC

PROC interiorStreamingSystem()
	IF NOT bStreamInterior
       INTID_ROBBERY_INTERIOR = GET_INTERIOR_AT_COORDS(vRobberInitialPos[0])
       PIN_INTERIOR_IN_MEMORY(INTID_ROBBERY_INTERIOR)
       bStreamInterior = TRUE
    ENDIF
ENDPROC

PROC createScene()

	SET_CREATE_RANDOM_COPS(FALSE)
	
	ADD_RELATIONSHIP_GROUP("ROBBERS", robbersRelGroup)
	// create robbers
	INT i
	FOR i = 0 to (iNoOfRobbers - 1)
		pedRobber[i] = CREATE_PED(PEDTYPE_MISSION, modelRobber, vRobberInitialPos[i], fRobberInitialHdg[i])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedRobber[i], TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedRobber[i], robbersRelGroup)
		SET_PED_COMBAT_ATTRIBUTES(pedRobber[i], CA_PLAY_REACTION_ANIMS, FALSE)
		SET_PED_COMBAT_ABILITY(pedRobber[i], CAL_POOR)
		SET_PED_ACCURACY(pedRobber[i], 13)
		GIVE_WEAPON_TO_PED(pedRobber[i], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(pedRobber[i], TRUE)
		SET_PED_CONFIG_FLAG(pedRobber[i], PCF_DontInfluenceWantedLevel, TRUE)
		SET_PED_CONFIG_FLAG(pedRobber[i], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		SET_PED_CONFIG_FLAG(pedRobber[i], PCF_DisableHurt, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedRobber[i], FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedRobber[i], FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedRobber[i], FA_PREFER_PAVEMENTS, TRUE)
	ENDFOR
	SET_PED_NAME_DEBUG(pedRobber[0], "pedRobber[0]")
	SET_PED_NAME_DEBUG(pedRobber[1], "pedRobber[1]")
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, robbersRelGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, robbersRelGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, robbersRelGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, robbersRelGroup)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 0, PLAYER_PED_ID(), "FRANKLIN")
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 0, PLAYER_PED_ID(), "TREVOR")
	ENDIF
	
	// create bag
	objIdBag = CREATE_OBJECT(modelRobbersBag, vBagCreationPos)
	
	IF thisEvent = HAIRDRESSERS_HOLDUP
		ADD_SCENARIO_BLOCKING_AREA(vWorldPoint1 - << 20, 20, 20 >>, vWorldPoint1 + << 20, 20, 20 >>)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(BALLER, TRUE)
		
		objCash[0] = CREATE_OBJECT(prop_anim_cash_pile_01, vBagCreationPos)
		objCash[1] = CREATE_OBJECT(prop_anim_cash_pile_01, vBagCreationPos - << 0, 0, 0.1 >>)
		
		SET_AMBIENT_VOICE_NAME(pedRobber[0], "G_M_Y_KorLieut_01_Korean_MINI_01")
		SET_AMBIENT_VOICE_NAME(pedRobber[1], "G_M_Y_Korean_01_Korean_MINI_02")
		
		vSyncRobberyPosition = << -821.565, -186.467, 36.600 >>
		vSyncRobberyRotation = << 0.000, 0.000, -59.960 >>
		sceneId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
//		TASK_SYNCHRONIZED_SCENE(pedRobber[0], sceneId, "random@shop_robbery", "robbery_intro_loop_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(pedRobber[1], sceneId, "random@shop_robbery", "robbery_intro_loop_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
		PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, "robbery_intro_loop_bag", "random@shop_robbery", INSTANT_BLEND_IN)
		
		// create workers
		FOR i = 0 to (iNoOfWorkers - 1)
			pedWorker[i] = CREATE_PED(PEDTYPE_MISSION, modelShopworkerFemale, vWorkerPedsPos[i], fWorkerPedsHdg[i])
			SET_PED_COMBAT_ATTRIBUTES(pedWorker[i], CA_ALWAYS_FLEE, TRUE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedWorker[i], RELGROUPHASH_PLAYER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[i], TRUE)
			SET_PED_CAN_BE_TARGETTED(pedWorker[i], FALSE)
			SET_PED_CONFIG_FLAG(pedWorker[i], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		ENDFOR
		SET_PED_NAME_DEBUG(pedWorker[0], "pedWorker[0]")
		SET_PED_NAME_DEBUG(pedWorker[1], "pedWorker[1]")
		SET_PED_NAME_DEBUG(pedWorker[2], "pedWorker[2]")
		
		// create customers
		// male customer
		pedShopper[0] = CREATE_PED(PEDTYPE_MISSION, modelCustomerMale, vShoppersPedsPos[0], fShoppersPedsHdg[0])
		SET_PED_COMBAT_ATTRIBUTES(pedShopper[0], CA_ALWAYS_FLEE, TRUE)
//		SET_PED_RELATIONSHIP_GROUP_HASH(pedShopper[0], RELGROUPHASH_PLAYER)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[0], TRUE)
		SET_PED_CAN_BE_TARGETTED(pedShopper[0], FALSE)
		SET_PED_CONFIG_FLAG(pedShopper[0], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		//female customers
		FOR i = 1 to 2
			pedShopper[i] = CREATE_PED(PEDTYPE_MISSION, modelCustomerFemale, vShoppersPedsPos[i], fShoppersPedsHdg[i])
			SET_PED_COMBAT_ATTRIBUTES(pedShopper[i], CA_ALWAYS_FLEE, TRUE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedShopper[i], RELGROUPHASH_PLAYER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], TRUE)
			SET_PED_CAN_BE_TARGETTED(pedShopper[i], FALSE)
			SET_PED_CONFIG_FLAG(pedShopper[i], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		ENDFOR
		SET_PED_NAME_DEBUG(pedShopper[0], "pedShopper[0]")
		SET_PED_NAME_DEBUG(pedShopper[1], "pedShopper[1]")
		SET_PED_NAME_DEBUG(pedShopper[2], "pedShopper[2]")
		
		//set up receptionist
		SET_PED_COMPONENT_VARIATION(pedWorker[0], PED_COMP_HEAD, 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedWorker[0], PED_COMP_HAIR, 1, 1, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedWorker[0], PED_COMP_TORSO, 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedWorker[0], PED_COMP_LEG, 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedWorker[0], PED_COMP_HAND, 0, 0, 0) //(hand)
		TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, "random@shop_robbery", "robbery_intro_loop_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
		SET_AMBIENT_VOICE_NAME(pedWorker[0], "A_M_Y_BevHills_01_White_Mini_01")
		ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 5, pedWorker[0], "REROBShopworker")
//		TASK_PLAY_ANIM(pedWorker[0], "amb@robberystand@", "stand_worried_female", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
//		SET_PED_KEEP_TASK(pedWorker[0], TRUE)
		
		//set up hairdresser right side
		SET_PED_COMPONENT_VARIATION(pedWorker[1], PED_COMP_HEAD, 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedWorker[1], PED_COMP_HAIR, 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedWorker[1], PED_COMP_TORSO, 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedWorker[1], PED_COMP_LEG, 1, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedWorker[1], PED_COMP_HAND, 0, 0, 0) //(hand)
		TASK_COWER(pedWorker[1])
//		TASK_PLAY_ANIM(pedWorker[1], "random@robbery", "f_cower_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		SET_PED_KEEP_TASK(pedWorker[1], TRUE)
		
		//set up hairdresser left side
		SET_PED_COMPONENT_VARIATION(pedWorker[2], PED_COMP_HEAD, 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedWorker[2], PED_COMP_HAIR, 1, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedWorker[2], PED_COMP_TORSO, 0, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedWorker[2], PED_COMP_LEG, 1, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedWorker[2], PED_COMP_HAND, 0, 0, 0) //(hand)
		TASK_COWER(pedWorker[2])
//		TASK_PLAY_ANIM(pedWorker[2], "random@robbery", "f_cower_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		SET_PED_KEEP_TASK(pedWorker[2], TRUE)
		
		//set up customer right side (male)
		sceneId = CREATE_SYNCHRONIZED_SCENE(<< -812.370, -185.650, 37.5804 >>, << 0.000, 0.000, 30.000 >>)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
		
		SET_PED_COMPONENT_VARIATION(pedShopper[0], PED_COMP_HEAD, 0, 1, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedShopper[0], PED_COMP_HAIR, 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedShopper[0], PED_COMP_TORSO, 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedShopper[0], PED_COMP_LEG, 0, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedShopper[0], PED_COMP_SPECIAL, 0, 0, 0) //(accs)
//		TASK_START_SCENARIO_AT_POSITION(pedShopper[0], "PROP_HUMAN_SEAT_CHAIR", << -812.370, -185.650, 37.105 >>, 37.680)
		TASK_SYNCHRONIZED_SCENE(pedShopper[0], SceneId, "random@robbery", "cower", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS/*|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE*/)
//		TASK_PLAY_ANIM_ADVANCED(pedShopper[0], "random@robbery", "cower", << -812.370, -185.650, 37.6304 >>,  << 0.000, 0.000, 30.000 >>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
		SET_PED_KEEP_TASK(pedShopper[0], TRUE)
		
		//set up customer left side
		sceneId = CREATE_SYNCHRONIZED_SCENE(<< -817.5606, -190.8273, 37.6114 >>, << -0.000, 0.000, 25.8399 >>)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
		
		SET_PED_COMPONENT_VARIATION(pedShopper[1], PED_COMP_HEAD, 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedShopper[1], PED_COMP_HAIR, 0, 1, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedShopper[1], PED_COMP_TORSO, 1, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedShopper[1], PED_COMP_LEG, 1, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedShopper[1], PED_COMP_SPECIAL, 0, 0, 0) //(accs)
		TASK_SYNCHRONIZED_SCENE(pedShopper[1], SceneId, "random@robbery", "cower", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS/*|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE*/)
//		TASK_PLAY_ANIM_ADVANCED (pedShopper[1], "random@robbery", "cower", << -817.5606, -190.8273, 37.6114 >>, << -0.000, 0.000, 25.8399 >>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
		SET_PED_KEEP_TASK(pedShopper[1], TRUE)
		
		//set up customer sat at front
		sceneId = CREATE_SYNCHRONIZED_SCENE(<< -816.1862, -182.8970, 37.680 >>, << 0.000, 0.000, 30.000 >>)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
		
		SET_PED_COMPONENT_VARIATION(pedShopper[2], PED_COMP_HEAD, 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedShopper[2], PED_COMP_HAIR, 0, 2, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedShopper[2], PED_COMP_TORSO, 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedShopper[2], PED_COMP_LEG, 1, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedShopper[2], PED_COMP_SPECIAL, 0, 0, 0) //(accs)
//		TASK_START_SCENARIO_AT_POSITION(pedShopper[2], "PROP_HUMAN_SEAT_CHAIR", << -816.224, -182.897, 37.180 >>, 30.000)
		TASK_SYNCHRONIZED_SCENE(pedShopper[2], SceneId, "random@robbery", "cower", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS/*|SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE*/)
//		TASK_PLAY_ANIM_ADVANCED (pedShopper[2], "random@robbery", "cower", << -816.1862, -182.8970, 37.680 >>, << 0.000, 0.000, 30.000 >>, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE|AF_USE_KINEMATIC_PHYSICS)
		SET_PED_KEEP_TASK(pedShopper[2], TRUE)
		
		TASK_AIM_GUN_AT_ENTITY(pedRobber[0], pedWorker[0], -1)
//		TASK_PLAY_ANIM(pedRobber[1], "random@robbery", "robbery_main_male", FAST_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		
		SET_ROADS_IN_AREA(<< -871, -246, 0 >>, << -798, -163, 50 >>, FALSE)
		
		ASSISTED_MOVEMENT_REQUEST_ROUTE("Hairdresser1")
		SET_SHOP_IS_BEING_ROBBED(HAIRDO_SHOP_01_BH, TRUE)
		FORCE_SHOP_CLEANUP(HAIRDO_SHOP_01_BH)
		WAIT(500)
		LOCK_SHOP_DOORS(HAIRDO_SHOP_01_BH, FALSE)
		
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
		ADD_SCENARIO_BLOCKING_AREA(vWorldPoint2 - << 20, 20, 20 >>, vWorldPoint2 + << 20, 20, 20 >>)
		
		// create workers
		SET_ENTITY_ROTATION(objIdBag, vBagCreationRot)
		pedWorker[0] = CREATE_PED(PEDTYPE_MISSION, modelShopworkerFemale, vWorkerPedsPos[0], fWorkerPedsHdg[0])
		SET_PED_NAME_DEBUG(pedWorker[0], "pedWorker[0]")
		SET_AMBIENT_VOICE_NAME(pedWorker[0], "A_M_Y_BeachVesp_01_White_Mini_01")
		ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 6, pedWorker[0], "REROBShopworker2")
		pedWorker[1] = CREATE_PED(PEDTYPE_MISSION, modelShopworkerMale, vWorkerPedsPos[1], fWorkerPedsHdg[1])
		SET_PED_NAME_DEBUG(pedWorker[1], "pedWorker[1]")
		FOR i = 0 to (iNoOfWorkers - 1)
			SET_PED_COMBAT_ATTRIBUTES(pedWorker[i], CA_ALWAYS_FLEE, TRUE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedWorker[i], RELGROUPHASH_PLAYER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[i], TRUE)
			SET_PED_CAN_BE_TARGETTED(pedWorker[i], FALSE)
			SET_PED_CONFIG_FLAG(pedWorker[i], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		ENDFOR
		
		// create customers
		pedShopper[0] = CREATE_PED(PEDTYPE_MISSION, modelCustomerMale, vShoppersPedsPos[0], fShoppersPedsHdg[0])
		SET_PED_NAME_DEBUG(pedShopper[0], "pedShopper[0]")
		pedShopper[1] = CREATE_PED(PEDTYPE_MISSION, modelCustomerFemale, vShoppersPedsPos[1], fShoppersPedsHdg[1])
		SET_PED_NAME_DEBUG(pedShopper[1], "pedShopper[1]")
		FOR i = 0 to (iNoOfShoppers - 1)
			SET_PED_COMBAT_ATTRIBUTES(pedShopper[i], CA_ALWAYS_FLEE, TRUE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedShopper[i], RELGROUPHASH_PLAYER)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], TRUE)
			SET_PED_CAN_BE_TARGETTED(pedShopper[i], FALSE)
			SET_PED_CONFIG_FLAG(pedShopper[i], PCF_Avoidance_Ignore_WeirdPedBuffer, TRUE)
		ENDFOR
		
		//set up receptionist
		TASK_PLAY_ANIM(pedWorker[0], "random@shop_robbery", "robbery_intro_loop_f", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		SET_PED_KEEP_TASK(pedWorker[0], TRUE)
		
		//set up worker
		TASK_PLAY_ANIM(pedWorker[1], "random@robbery", "f_cower_02", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		SET_PED_KEEP_TASK(pedWorker[1], TRUE)
		
		//set up customer left side
//		OPEN_SEQUENCE_TASK(seq)
//			TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_base"/*, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 0, AF_NOT_INTERRUPTABLE*/)
//			TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_idle_b"/*, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 0, AF_NOT_INTERRUPTABLE*/)
//			TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_idle_e"/*, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 0, AF_NOT_INTERRUPTABLE*/)
//		SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//		CLOSE_SEQUENCE_TASK(seq)
//		TASK_PERFORM_SEQUENCE(pedShopper[0], seq)
//		CLEAR_SEQUENCE_TASK(seq)
//		TASK_START_SCENARIO_IN_PLACE(pedShopper[0], "CODE_HUMAN_SHOCKING_BIG_REACTION")
		TASK_COWER(pedShopper[0])
		SET_PED_KEEP_TASK(pedShopper[0], TRUE)
		
		//set up customer right side
		TASK_PLAY_ANIM(pedShopper[1], "random@robbery", "f_cower_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,  AF_LOOPING|AF_NOT_INTERRUPTABLE)
		SET_PED_KEEP_TASK(pedShopper[1], TRUE)
		
		vSyncRobberyPosition = << -1192.010, -768.929, 16.358 >>
		vSyncRobberyRotation = << 0.000, 0.000, -59.500 >>
		sceneId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
		SET_SYNCHRONIZED_SCENE_LOOPED(sceneId, TRUE)
		TASK_SYNCHRONIZED_SCENE(pedRobber[0], sceneId, "random@shop_robbery", "robbery_intro_loop_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
		TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, "random@shop_robbery", "robbery_intro_loop_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
//		PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, "robbery_intro_loop_bag", "random@shop_robbery", INSTANT_BLEND_IN)
//		TASK_PLAY_ANIM(pedRobber[0], "random@shop_robbery", "robbery_intro_loop_b", FAST_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
//		TASK_AIM_GUN_AT_ENTITY(pedRobber[0], pedWorker[0], -1)
//		TASK_AIM_GUN_AT_ENTITY(pedRobber[1], pedWorker[1], -1)
		TASK_PLAY_ANIM(pedRobber[1], "random@shop_robbery", "robbery_intro_loop_a", FAST_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
		
		SET_SHOP_IS_BEING_ROBBED(CLOTHES_SHOP_M_01_SM, TRUE)
		FORCE_SHOP_CLEANUP(CLOTHES_SHOP_M_01_SM)
		WAIT(500)
		LOCK_SHOP_DOORS(CLOTHES_SHOP_M_01_SM, FALSE)
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 3, pedRobber[0], "REROBRobber1")
	ADD_PED_FOR_DIALOGUE(RobberyDialogueStruct, 4, pedRobber[1], "REROBRobber2")
	
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BevHills_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_BevHills_02)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_Hipster_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Hipster_04)
	SET_MODEL_AS_NO_LONGER_NEEDED(G_M_Y_Korean_01)
	
ENDPROC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -823.1281, -187.9591, 36.6189 >>, << 12, 12, 12 >>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1201.0172, -777.5527, 16.3398 >>, << 12, 12, 12 >>)
		IF NOT IS_PED_INJURED(pedRobber[0])
		AND NOT IS_PED_INJURED(pedRobber[1])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[0], PLAYER_PED_ID())
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[1], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			IF CAN_PED_SEE_HATED_PED(pedRobber[0], PLAYER_PED_ID())
			OR CAN_PED_SEE_HATED_PED(pedRobber[1], PLAYER_PED_ID())
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber[0])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber[0])
				OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),pedRobber[1])
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber[1])
					IF iAimingTimer = -1
						iAimingTimer = GET_GAME_TIMER()
					ENDIF
				ELSE
					iAimingTimer = -1
				ENDIF
				IF (iAimingTimer != -1 AND GET_GAME_TIMER() > iAimingTimer + 1500)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF IS_BULLET_IN_BOX(vLocReturnBag - << 20, 20, 20 >> , vLocReturnBag + << 20, 20, 20 >>)
//	IF IS_PROJECTILE_IN_AREA(vLocReturnBag - << 20, 20, 20 >>, vLocReturnBag + << 20, 20, 20 >>)
	IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, vLocReturnBag - << 20, 20, 20 >>, vLocReturnBag + << 20, 20, 20 >>)
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_DEAD(pedRobber[0])
	OR IS_ENTITY_DEAD(pedRobber[1])
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_PLAYER_IN_ROBBERY_INTERIOR()
		RETURN TRUE
	ENDIF
	
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND IS_PLAYER_IN_ROBBERY_INTERIOR()
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedRobber[0])
	AND NOT IS_PED_INJURED(pedRobber[1])
		IF IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedRobber[1]) - << 12, 12, 12 >>, GET_ENTITY_COORDS(pedRobber[1]) + << 12, 12, 12 >>, FALSE)
		AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
		OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedRobber[0]), 3)
		OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedRobber[1]), 3)
//			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber[0])
//			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber[0])
//			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),pedRobber[1])
//			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber[1])
				RETURN TRUE
//			ENDIF
		ENDIF
	ENDIF
	
	INT index
	REPEAT COUNT_OF(pedWorker) index
		IF DOES_ENTITY_EXIST(pedWorker[index])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedWorker[index], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(pedShopper) index
		IF DOES_ENTITY_EXIST(pedShopper[index])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedShopper[index], PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_OUT_FRONT()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -823.1281, -187.9591, 36.6189 >>, << 4, 4, 4 >>)
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -1201.0172, -777.5527, 16.3398 >>, << 4, 4, 4 >>)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, 1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_MOVING_LEFT_STICK(INT iLEFT_STICK_THRESHOLD = 64) 
	
	INT ReturnLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) -128 
	INT ReturnLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) -128 
	
	IF ReturnLeftX < iLEFT_STICK_THRESHOLD
	AND ReturnLeftX > -iLEFT_STICK_THRESHOLD
	AND ReturnLeftY < iLEFT_STICK_THRESHOLD
	AND ReturnLeftY > -iLEFT_STICK_THRESHOLD
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_WE_STORED_THE_PLAYERS_VEH()
	VEHICLE_INDEX vehTemp =	GET_PLAYERS_LAST_VEHICLE()
	IF IS_VEHICLE_DRIVEABLE(vehTemp)
		IF IS_ENTITY_AT_COORD(vehTemp, << -829.3693, -191.9869, 36.4616 >>, << 15, 15, 5 >>)
			vehPlayersStoredVehicle = vehTemp
			IF NOT IS_ENTITY_A_MISSION_ENTITY(vehPlayersStoredVehicle)
				SET_ENTITY_AS_MISSION_ENTITY(vehPlayersStoredVehicle)
			ENDIF
			PRINTNL()
			PRINTSTRING("HAVE_WE_STORED_THE_PLAYERS_VEH - RETURN true" )
			PRINTNL()
			RETURN TRUE
		ENDIF
	ENDIF
	PRINTNL()
	PRINTSTRING("HAVE_WE_STORED_THE_PLAYERS_VEH - RETURN FALSE" )
	PRINTNL()
	RETURN FALSE
ENDFUNC

PROC createGetawayVeh()
	MODEL_NAMES modelGetAwayVeh
	modelGetAwayVeh = BALLER
	IF NOT bGetawayModelLoaded
		REQUEST_MODEL(modelGetAwayVeh)
		IF HAS_MODEL_LOADED(modelGetAwayVeh)
			bGetawayModelLoaded = TRUE
		ELSE
			REQUEST_MODEL(modelGetAwayVeh)
		ENDIF
	ELSE
		IF HAS_MODEL_LOADED(modelGetAwayVeh)
			IF HAVE_WE_STORED_THE_PLAYERS_VEH()
				IF IS_ENTITY_IN_ANGLED_AREA(vehPlayersStoredVehicle, <<-828.369202,-194.466949,36.413845>>, <<-824.580139,-201.135345,39.394363>>, 4.312500)
					vGetawayCreationPoint = << -822.1784, -205.8110, 36.3444 >>
				ELSE
					vGetawayCreationPoint = << -826.2828, -197.8547, 36.3995 >>
				ENDIF
			ELSE
				vGetawayCreationPoint = << -826.2828, -197.8547, 36.3995 >>
			ENDIF
			vehGetawayVeh = CREATE_VEHICLE(modelGetAwayVeh, vGetawayCreationPoint, 31.9252)
//			SET_VEHICLE_CAN_BE_TARGETTED(vehGetawayVeh, TRUE)
//			SET_VEHICLE_DOORS_LOCKED(vehGetawayVeh, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehGetawayVeh, TRUE)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehGetawayVeh)
			SET_VEHICLE_ENGINE_ON(vehGetawayVeh, TRUE, TRUE)
			pedDriver =	CREATE_PED_INSIDE_VEHICLE(vehGetawayVeh, PEDTYPE_MISSION, G_M_Y_Korean_01)
			SET_PED_CONFIG_FLAG(pedDriver, PCF_PreventPedFromReactingToBeingJacked, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDriver, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedDriver, robbersRelGroup)
			SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_PLAY_REACTION_ANIMS, FALSE)
			SET_PED_COMBAT_ABILITY(pedDriver, CAL_POOR)
			SET_PED_ACCURACY(pedDriver, 13)
			GIVE_WEAPON_TO_PED(pedDriver, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
			SET_ENTITY_IS_TARGET_PRIORITY(pedDriver, TRUE)
			SET_PED_CONFIG_FLAG(pedDriver, PCF_DontInfluenceWantedLevel, TRUE)
			bGetawayVehCreated = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC moveBlockingVehicle()
	
	IF thisEvent = HAIRDRESSERS_HOLDUP
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_COORDS(PLAYER_PED_ID()), << 8, 8, 8 >>)
			OR IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), << -823.1281, -187.9591, 36.6189 >>, << 8, 8, 8 >>)
//				IF IS_VEHICLE_MODEL_ON_BLACKLIST(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//				ELSE
					SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -831.9709, -192.0823, 36.5008 >>, FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
//				ENDIF
			ENDIF
		ENDIF
		CLEAR_AREA(<< -823.1281, -187.9591, 36.6189 >>, 3, TRUE)
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_COORDS(PLAYER_PED_ID()), << 8, 8, 8 >>)
			OR IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), << -1201.0172, -777.5527, 16.3398 >>, << 8, 8, 8 >>)
				SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -1205.9360, -781.2014, 16.0088 >>, FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
			ENDIF
		ENDIF
		CLEAR_AREA(<< -1201.0172, -777.5527, 16.3398 >>, 8, TRUE)
	ENDIF
	
ENDPROC
//
//PROC addBlipForBag()
//	FOR i = 0 to (iNoOfRobbers - 1)
//		IF DOES_BLIP_EXIST(blipRobber[i])
//			REMOVE_BLIP(blipRobber[i])
//		ENDIF
//	ENDFOR
//	IF DOES_BLIP_EXIST(blipGetawayVeh)
//		REMOVE_BLIP(blipGetawayVeh)
//	ENDIF
//	IF NOT DOES_BLIP_EXIST(blipBag)
//		blipBag = CREATE_AMBIENT_BLIP_FOR_OBJECT(objIdBag)
//	ENDIF
//ENDPROC

PROC removeDeadPedBlips()
	INT i
	FOR i = 0 to (iNoOfRobbers - 1)
		IF DOES_ENTITY_EXIST(pedRobber[i])
			IF IS_PED_INJURED(pedRobber[i])
				IF DOES_BLIP_EXIST(blipRobber[i])
					REMOVE_BLIP(blipRobber[i])
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC removeAllBlips()
	IF DOES_BLIP_EXIST(blipBag)
		REMOVE_BLIP(blipBag)
	ENDIF
	IF DOES_BLIP_EXIST(blipShopWorker)
		REMOVE_BLIP(blipShopWorker)
	ENDIF
	IF DOES_BLIP_EXIST(blipGetawayVeh)
		REMOVE_BLIP(blipGetawayVeh)
	ENDIF
	IF DOES_BLIP_EXIST(blipRobber[0])
		REMOVE_BLIP(blipRobber[0])
	ENDIF
	IF DOES_BLIP_EXIST(blipRobber[1])
		REMOVE_BLIP(blipRobber[1])
	ENDIF
ENDPROC

PROC keepEmCrying()

//	IF thisEvent = HAIRDRESSERS_HOLDUP
		IF NOT IS_PED_INJURED(pedWorker[1])
			IF IS_PED_MALE(pedWorker[1])
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedWorker[1], "WHIMPER", "WAVELOAD_PAIN_MALE", SPEECH_PARAMS_FORCE)
			ELSE
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedWorker[1], "WHIMPER", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
			ENDIF
			PLAY_PAIN(pedWorker[1], AUD_DAMAGE_REASON_WHIMPER)
		ENDIF
//	ENDIF
	IF NOT IS_PED_INJURED(pedShopper[1])
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedShopper[1], "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)
		PLAY_PAIN(pedShopper[1], AUD_DAMAGE_REASON_SCREAM_PANIC)
	ENDIF
	
ENDPROC

PROC makePedsFleeWhenRobbersAreAway()
	BOOL bRobber0OutOfArea = FALSE
	BOOL bRobber1OutOfArea = FALSE
	BOOL bDriverOutOfArea = FALSE
	
	IF thisEvent = HAIRDRESSERS_HOLDUP
	
		IF DOES_ENTITY_EXIST(pedRobber[0])
			IF NOT IS_PED_INJURED(pedRobber[0])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], << -837.930237, -176.010086, 36.087787 >>, << -822.971008, -200.629913, 37.911846 >>, 16)
					bRobber0OutOfArea = TRUE
				ENDIF
			ELSE
				bRobber0OutOfArea = TRUE
			ENDIF
		ELSE
			bRobber0OutOfArea = TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRobber[1])
			IF NOT IS_PED_INJURED(pedRobber[1])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], << -837.930237, -176.010086, 36.087787 >>, << -822.971008, -200.629913, 37.911846 >>, 16)
					bRobber1OutOfArea = TRUE
				ENDIF
			ELSE
				bRobber1OutOfArea = TRUE
			ENDIF
		ELSE
			bRobber1OutOfArea = TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedDriver)
			IF NOT IS_PED_INJURED(pedDriver)
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedDriver, << -837.930237, -176.010086, 36.087787 >>, << -822.971008, -200.629913, 37.911846 >>, 16)
					bDriverOutOfArea = TRUE
				ENDIF
			ELSE
				bDriverOutOfArea = TRUE
			ENDIF
		ELSE
			bDriverOutOfArea = TRUE
		ENDIF
		
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
	
		IF DOES_ENTITY_EXIST(pedRobber[0])
			IF NOT IS_PED_INJURED(pedRobber[0])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], << -1199.9963, -791.9769, 15.3862 >>, << -1211.8534, -775.8312, 16.6341 >>, 16)
					bRobber0OutOfArea = TRUE
					bDriverOutOfArea = TRUE
				ENDIF
			ELSE
				bRobber0OutOfArea = TRUE
				bDriverOutOfArea = TRUE
			ENDIF
		ELSE
			bRobber0OutOfArea = TRUE
			bDriverOutOfArea = TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedRobber[1])
			IF NOT IS_PED_INJURED(pedRobber[1])
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], << -1199.9963, -791.9769, 15.3862 >>, << -1211.8534, -775.8312, 16.6341 >>, 16)
					bRobber1OutOfArea = TRUE
					bDriverOutOfArea = TRUE
				ENDIF
			ELSE
				bRobber1OutOfArea = TRUE
				bDriverOutOfArea = TRUE
			ENDIF
		ELSE
			bRobber1OutOfArea = TRUE
			bDriverOutOfArea = TRUE
		ENDIF
	ENDIF
	
	IF bRobber0OutOfArea
	AND bRobber1OutOfArea
	AND bDriverOutOfArea
	AND bRobber0NotInSalon
	AND bRobber1NotInSalon
		IF NOT bCustomersFledStore
			// customer left side
			IF NOT IS_PED_INJURED(pedShopper[1])
				TASK_SMART_FLEE_PED(pedShopper[1], PLAYER_PED_ID(), 250, -1, TRUE)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[1], FALSE)
				SET_PED_KEEP_TASK(pedShopper[1], TRUE)
			ENDIF
			// customer sat at front
			IF NOT IS_PED_INJURED(pedShopper[2])
				TASK_SMART_FLEE_PED(pedShopper[2], PLAYER_PED_ID(), 250, -1, TRUE)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[2], FALSE)
				SET_PED_KEEP_TASK(pedShopper[2], TRUE)
			ENDIF
			iFleeTimer = (GET_GAME_TIMER() + 1000)
			bCustomersFledStore = TRUE
		ENDIF
		
		IF bCustomersFledStore
			IF iFleeTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedShopper[1])
					SET_PED_AS_NO_LONGER_NEEDED(pedShopper[1])
				ENDIF
				IF NOT IS_PED_INJURED(pedShopper[2])
					SET_PED_AS_NO_LONGER_NEEDED(pedShopper[2])
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC superQuickActionCam()
	SWITCH iQuickCamCutStage
		CASE 0
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			CLEAR_AREA_OF_PEDS(GET_PLAYER_COORDS(PLAYER_ID()), 10)
			camQuickShot = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
			SET_CAM_ACTIVE(camQuickShot, TRUE)
			PLAY_SYNCHRONIZED_CAM_ANIM(camQuickShot, sceneId, "robbery_action_cam", "random@shop_robbery")
			IF thisEvent = HAIRDRESSERS_HOLDUP
				SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME("Hair_room")
//				SET_CAM_PARAMS(camQuickShot, << -818.0676, -185.5752, 38.0525 >>, << 0.9936, 0.0000, 96.5723 >>, 52.8959)
//				SET_CAM_PARAMS(camQuickShot, << -818.0676, -185.5752, 38.0525 >>, << 0.9936, 0.0000, 96.5723 >>, 52.8959, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
//			ELIF thisEvent = CLOTHES_SHOP_HOLDUP
//				SET_CAM_PARAMS(camQuickShot, << -1190.7926, -773.7481, 17.9004 >>, << -1.0658, -0.0000, 109.6293 >>, 34.803738)
//				SET_CAM_PARAMS(camQuickShot, << -1190.7926, -773.7481, 17.9004 >>, << -1.0658, -0.0000, 107.0096 >>, 34.803738, 7000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
			ENDIF
			
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			iCamShotCounter = (GET_GAME_TIMER() + 3500)
			iQuickCamCutStage ++
		BREAK
		CASE 1
			IF iCamShotCounter < GET_GAME_TIMER()
				IF thisEvent = HAIRDRESSERS_HOLDUP
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(115)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-9.1222)
				ELIF thisEvent = CLOTHES_SHOP_HOLDUP
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(44.5236)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-18.1582)
				ENDIF
				NEW_LOAD_SCENE_STOP()
				SET_CAM_ACTIVE(camQuickShot, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				moveBlockingVehicle()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				iQuickCamCutStage ++
			ENDIF
		BREAK
		CASE 2
		
		BREAK
	ENDSWITCH
ENDPROC

PROC freePedsFromScript()
	IF thisEvent = HAIRDRESSERS_HOLDUP
		// receptionist
		IF DOES_ENTITY_EXIST(pedWorker[0])
			IF NOT IS_PED_INJURED(pedWorker[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[0], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(pedWorker[0])
			ENDIF
		ENDIF
		// hairdresser right side
		IF DOES_ENTITY_EXIST(pedWorker[1])
			IF NOT IS_PED_INJURED(pedWorker[1])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[1], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(pedWorker[1])
			ENDIF
		ENDIF
		// customer right side (male)
		IF DOES_ENTITY_EXIST(pedShopper[0])
			IF NOT IS_PED_INJURED(pedShopper[0])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[0], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(pedShopper[0])
			ENDIF
		ENDIF
		// customer left side
		IF DOES_ENTITY_EXIST(pedShopper[1])
			IF NOT IS_PED_INJURED(pedShopper[1])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[1], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(pedShopper[1])
			ENDIF
		ENDIF
		// customer sat at front
		IF DOES_ENTITY_EXIST(pedShopper[2])
			IF NOT IS_PED_INJURED(pedShopper[2])
				SET_ENTITY_COORDS(pedShopper[2], << -820.4240, -181.9059, 36.5687 >>)
				SET_ENTITY_HEADING(pedShopper[2], 167.7909)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[2], FALSE)
				SET_PED_AS_NO_LONGER_NEEDED(pedShopper[2])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC givePedsSomeApreRobberyTasks()

	IF thisEvent = HAIRDRESSERS_HOLDUP
		// receptionist
		IF NOT IS_PED_INJURED(pedWorker[0])
			CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_TKH", CONV_PRIORITY_AMBIENT_HIGH)
//			OPEN_SEQUENCE_TASK(seq)
//				TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_idle_e", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
//				TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 3500)
//				TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_idle_e", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
//				TASK_PLAY_ANIM(NULL, "random@robbery", "shocked_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, 3500)
//				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
//			CLOSE_SEQUENCE_TASK(seq)
//			TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
//			TASK_START_SCENARIO_IN_PLACE(pedWorker[0], "CODE_HUMAN_SHOCKING_BIG_REACTION")
			TASK_COWER(pedWorker[0])
			SET_PED_KEEP_TASK(pedWorker[0], TRUE)
		ENDIF
		
		// hairdresser right side
		IF NOT IS_PED_INJURED(pedWorker[1])
//			TASK_PLAY_ANIM(pedWorker[1], "random@robbery", "shocked_idle_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,  AF_LOOPING)
//			TASK_START_SCENARIO_IN_PLACE(pedWorker[1], "CODE_HUMAN_SHOCKING_BIG_REACTION")
			TASK_COWER(pedWorker[1])
			SET_PED_KEEP_TASK(pedWorker[1], TRUE)
		ENDIF
//		
//		// customer right side (male)
//		IF NOT IS_PED_INJURED(pedShopper[0])
//			TASK_PLAY_ANIM(pedShopper[0], "random@robbery", "drink_bench_idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_LOOPING)
//			SET_PED_KEEP_TASK(pedShopper[0], TRUE)
//		ENDIF
//		
//		// customer left side
//		IF NOT IS_PED_INJURED(pedShopper[i])
//			TASK_PLAY_ANIM(pedShopper[i], "random@robbery" "sit_down_idle_01", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_LOOPING | AF_OVERRIDE_PHYSICS)
//			SET_PED_KEEP_TASK(pedShopper[i], TRUE)
//		ENDIF
//		
//		// customer sat at front
//		IF NOT IS_PED_INJURED(pedShopper[i])
//			TASK_PLAY_ANIM(pedShopper[i], "random@robbery", "sit_chair_idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_LOOPING | AF_OVERRIDE_PHYSICS)
//			SET_PED_KEEP_TASK(pedShopper[i], TRUE)
//		ENDIF
	
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
		INT i
		FOR i = 1 to (iNoOfShoppers - 1)
			CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_TKH", CONV_PRIORITY_AMBIENT_HIGH)
			IF NOT IS_PED_INJURED(pedShopper[i])
				TASK_PLAY_ANIM(pedShopper[i], "random@robbery", "sit_down_idle_01", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1,  AF_LOOPING|AF_OVERRIDE_PHYSICS)
				SET_PED_KEEP_TASK(pedShopper[i], TRUE)
			ENDIF
		ENDFOR
	ENDIF	
	
	WAIT(0)
ENDPROC

//PROC playerGetUp()
//	IF IS_BUTTON_PRESSED(PAD1,  LEFTSHOULDER2)
//	OR IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER2)
//		IF NOT bBreakingOut
//			CLEAR_PED_TASKS(PLAYER_PED_ID())
//			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
//				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon, TRUE)
//			ENDIF
//			SETTIMERB(0)
//			bBreakingOut = TRUE
//		ENDIF
//	ENDIF
//	IF bBreakingOut
//		IF TIMERB() > 500
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			bPlayerGotUp = TRUE
//		ENDIF
//	ENDIF
//ENDPROC

PROC swapBagModels()
	VECTOR vTempObjPos
	VECTOR vTempObjRot
	vTempObjPos = GET_ENTITY_COORDS(objIdBag)
	vTempObjRot = GET_ENTITY_ROTATION(objIdBag)
	
	DELETE_OBJECT(objIdBag)
	objIdBag = CREATE_OBJECT(Prop_CS_Duffel_01, vTempObjPos) //Prop_Med_Bag_01b
	SET_ENTITY_ROTATION(objIdBag, vTempObjRot)
ENDPROC

FUNC BOOL ARE_ALL_ROBBERS_IN_CAR()
	IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
		IF NOT IS_PED_INJURED(pedRobber[0])
			IF IS_PED_IN_VEHICLE(pedRobber[0], vehGetawayVeh)
				IF NOT IS_PED_INJURED(pedRobber[1])
					IF IS_PED_IN_VEHICLE(pedRobber[1], vehGetawayVeh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//FUNC BOOL IS_PLAYER_AIMING_WEAPON()
//	IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
//	OR IS_AIM_CAM_ACTIVE()
//		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
//			IF CurrentWeapon <> WEAPONTYPE_UNARMED
//				IF NOT bAimTimerStarted
//					iAimingTimer = GET_GAME_TIMER()
//					bAimTimerStarted = TRUE
//				ENDIF
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC

//FUNC BOOL HAS_GETAWAY_DRIVER_GOT_AWAY()
//	IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
//	OR IS_ENTITY_DEAD(vehGetawayVeh)
//		IF DOES_ENTITY_EXIST(objIdBag)
//			IF GET_DISTANCE_BETWEEN_ENTITIES(vehGetawayVeh, objIdBag) > 20
//				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGetawayVeh)
//				IF DOES_ENTITY_EXIST(pedDriver)
//					SET_PED_AS_NO_LONGER_NEEDED(pedDriver)
//				ENDIF
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL ARE_WE_READY_TO_CREATE_BAG_PICKUP()
	IF NOT DOES_ENTITY_EXIST(pedRobber[0])
		bRobber0Gone = TRUE
	ELSE
		IF IS_PED_INJURED(pedRobber[0])
			bRobber0Gone = TRUE
		ENDIF
	ENDIF
	IF NOT bCreateBagAtVehicle
		IF NOT DOES_ENTITY_EXIST(pedRobber[1]) 
			bRobber1Gone = TRUE
		ELSE
			IF IS_PED_INJURED(pedRobber[1])
				bRobber1Gone = TRUE
			ENDIF
		ENDIF
	ELSE
		bRobber1Gone = TRUE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedDriver)
		bDriverGone = TRUE
	ELSE
		IF IS_PED_INJURED(pedDriver)
		OR NOT IS_PED_IN_ANY_VEHICLE(pedDriver)
//		OR HAS_GETAWAY_DRIVER_GOT_AWAY()
			bDriverGone = TRUE
		ENDIF
	ENDIF
	
	IF bRobber0Gone
	AND bRobber1Gone
	OR bDriverGone// AND ARE_ALL_ROBBERS_IN_CAR())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC playerEntersRobberyScene()

	IF NOT bAcknowledgedPlayer
	AND CAN_PLAYER_START_CUTSCENE()
		IF DOES_BLIP_EXIST(blipShopWorker)
			REMOVE_BLIP(blipShopWorker)
		ENDIF
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		CLEAR_HELP(TRUE)
		DISPLAY_HUD(FALSE)
		DISPLAY_RADAR(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		
		IF thisEvent = HAIRDRESSERS_HOLDUP
			IF NOT bPlayerGotDown
				sceneId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@shop_robbery", "robbery_action_p", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				IF NOT IS_PED_INJURED(pedRobber[0])
				AND NOT IS_PED_INJURED(pedRobber[1])
				AND NOT IS_PED_INJURED(pedWorker[0])
				AND DOES_ENTITY_EXIST(objIdBag)
					TASK_SYNCHRONIZED_SCENE(pedRobber[0], sceneId, "random@shop_robbery", "robbery_action_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_SYNCHRONIZED_SCENE(pedRobber[1], sceneId, "random@shop_robbery", "robbery_action_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, "random@shop_robbery", "robbery_action_f", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, "robbery_action_bag", "random@shop_robbery", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				ENDIF
				SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.060)
//				IF NOT IS_PED_INJURED(pedRobber[1])
//					CLEAR_PED_TASKS(PLAYER_PED_ID())
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_LOOK_AT_ENTITY(NULL, pedRobber[1], 1500)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -819.7756, -186.1851, 36.5671 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.75)
//						TASK_PLAY_ANIM(NULL, "random@robbery", "idle_2_hands_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME | AF_NOT_INTERRUPTABLE)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//					CLEAR_SEQUENCE_TASK(seq)
					bPlayerGotDown = TRUE
//				ENDIF
			ENDIF
			
	//		IF NOT IS_PED_INJURED(pedRobber[0])
	//			CLEAR_PED_TASKS(pedRobber[0])
	//			OPEN_SEQUENCE_TASK(seq)
	//				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000)
	//				TASK_PLAY_ANIM(NULL, "random@shop_robbery", "robbery_action_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.060)
	//			CLOSE_SEQUENCE_TASK(seq)
	//			TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
	//			CLEAR_SEQUENCE_TASK(seq)
	//		ENDIF
			
			
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			IF NOT IS_PED_INJURED(pedWorker[0])
				CLEAR_PED_TASKS(pedWorker[0])
				TASK_PLAY_ANIM(pedWorker[0], "random@robbery", "robbery_main_female", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1)
			ENDIF
			NEW_LOAD_SCENE_START_SPHERE(<< -1195.0616, -773.2098, 16.3240 >>, 20, NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR)

			sceneId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
	//		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	//		TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "random@shop_robbery", "robbery_action_p", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
			IF NOT IS_PED_INJURED(pedRobber[0])
			AND NOT IS_PED_INJURED(pedRobber[1])
			AND NOT IS_PED_INJURED(pedWorker[0])
			AND DOES_ENTITY_EXIST(objIdBag)
				TASK_SYNCHRONIZED_SCENE(pedRobber[0], sceneId, "random@shop_robbery", "robbery_action_b", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
	//			TASK_SYNCHRONIZED_SCENE(pedRobber[1], sceneId, "random@shop_robbery", "robbery_action_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, "random@shop_robbery", "robbery_action_f", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, "robbery_action_bag", "random@shop_robbery", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
				IF DOES_ENTITY_EXIST(objCash[0])
				AND DOES_ENTITY_EXIST(objCash[1])
					ATTACH_ENTITY_TO_ENTITY(objCash[0], pedWorker[0], GET_PED_BONE_INDEX(pedWorker[0], BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, TRUE, TRUE)
					ATTACH_ENTITY_TO_ENTITY(objCash[1], pedWorker[0], GET_PED_BONE_INDEX(pedWorker[0], BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>, TRUE, TRUE)
				ENDIF
			ENDIF
			SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.050)
			
			vSyncRobberyPosition = << -1199.3690, -776.1991, 16.3235 >>
			vSyncRobberyRotation = << 0.000, 0.000, -60.000 >>
			sceneId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
			IF NOT IS_PED_INJURED(pedRobber[1])
				TASK_SYNCHRONIZED_SCENE(pedRobber[1], sceneId, "random@shop_robbery", "robbery_action_a", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//				CLEAR_PED_TASKS(pedRobber[1])
//				OPEN_SEQUENCE_TASK(seq)
//					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000)
//				//	TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
//					//TASK_PLAY_ANIM(NULL, "random@robbery", "goddamn")
//	//				TASK_AIM_GUN_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
//					TASK_PLAY_ANIM(NULL, "random@shop_robbery", "robbery_action_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.060)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
//				CLEAR_SEQUENCE_TASK(seq)
			ENDIF
			SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.060)
						
	//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
	//		ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1200.4554, -777.6201, 16.3244 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 305.3806)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			OPEN_SEQUENCE_TASK(seq)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1200.2491, -775.2443, 16.3211 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
//				TASK_PLAY_ANIM(NULL, "random@robbery", "idle_2_hands_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME|AF_NOT_INTERRUPTABLE)
				TASK_PLAY_ANIM(NULL, "random@shop_robbery", "robbery_action_p", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_EXPAND_PED_CAPSULE_FROM_SKELETON, 0.060)
				TASK_PLAY_ANIM(NULL, "random@shop_robbery", "kneel_loop_p", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE|AF_LOOPING|AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
			CLEAR_SEQUENCE_TASK(seq)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
		ENDIF
		bAcknowledgedPlayer = TRUE
	ENDIF
	
	//	IF NOT bPhSpeech1
	//		IF NOT IS_PED_INJURED(pedRobber[1])
	//			IF GET_SCRIPT_TASK_STATUS(pedRobber[1], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
	//				IF GET_SEQUENCE_PROGRESS(pedRobber[1]) = 1
	//				AND CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_GD", CONV_PRIORITY_AMBIENT_HIGH)
	//					IF thisEvent = CLOTHES_SHOP_HOLDUP
	//						superQuickActionCam()
	//					ENDIF
	//					bPhSpeech1 = TRUE
	//				ENDIF
	//			ENDIF
	//		ENDIF
	//	ENDIF
	
	IF NOT bGetawayVehCreated
		createGetawayVeh()
	ENDIF
	
	INT i
	FOR i = 0 to (iNoOfRobbers -1)
		IF NOT DOES_BLIP_EXIST(blipRobber[i])
			IF NOT IS_PED_INJURED(pedRobber[i])
				blipRobber[i] = CREATE_BLIP_FOR_PED(pedRobber[i], TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF (thisEvent = HAIRDRESSERS_HOLDUP AND DOES_ENTITY_EXIST(vehGetawayVeh))
	OR thisEvent = CLOTHES_SHOP_HOLDUP
		SETTIMERA(0)
		RunRobberyStage = ROBBERY_COMMENCES
	ENDIF
	// will then go to ROBBERY_COMMENCES
ENDPROC

PROC robberyCommences()
	
//	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//		CLEAR_HELP()
//	ENDIF
	
	IF thisEvent = HAIRDRESSERS_HOLDUP
		IF IS_PED_INJURED(pedRobber[0])
		AND IS_PED_INJURED(pedRobber[1])
			RunRobberyStage = PREVENTED_GETAWAY
		ENDIF
	ELIF thisEvent = CLOTHES_SHOP_HOLDUP
		IF IS_PED_INJURED(pedRobber[0])
		AND IS_PED_INJURED(pedRobber[1])
			RunRobberyStage = PREVENTED_GETAWAY
		ENDIF
	ENDIF
	
	INT index
	REPEAT COUNT_OF(pedRobber) index
	IF IS_PED_INJURED(pedRobber[index])
		IF DOES_BLIP_EXIST(blipRobber[index])
			REMOVE_BLIP(blipRobber[index])
		ENDIF
	ENDIF
	ENDREPEAT
	
	INT i
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
		IF NOT bRobbersFightingBack
			FOR i = 0 to (iNoOfRobbers - 1)
				IF NOT IS_PED_INJURED(pedRobber[i])
					TASK_COMBAT_PED(pedRobber[i], PLAYER_PED_ID())
				ENDIF
			ENDFOR
			bRobbersFightingBack = TRUE
			IF IS_ENTITY_ATTACHED(objIdBag)
				RunRobberyStage = HARMED_ROBBERS
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bPhSpeech1
	AND NOT bRobbersFightingBack
		TRIGGER_MUSIC_EVENT("RE51A_SHOP")
//		IF TIMERA() > 400
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			IF thisEvent = HAIRDRESSERS_HOLDUP
				IF CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_GD2", CONV_PRIORITY_AMBIENT_HIGH)
					bPhSpeech1 = TRUE
				ENDIF
			ELIF thisEvent = CLOTHES_SHOP_HOLDUP
				IF CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_GD", CONV_PRIORITY_AMBIENT_HIGH)
					bPhSpeech1 = TRUE
				ENDIF
			ENDIF
//		ENDIF
	ENDIF
	
	IF bPhSpeech1
	AND NOT bPhSpeech2
	AND NOT bRobbersFightingBack
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_MDS", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_FC", CONV_PRIORITY_AMBIENT_HIGH)
			ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_TP", CONV_PRIORITY_AMBIENT_HIGH)
			ENDIF
			bPhSpeech2 = TRUE
		ENDIF
	ENDIF
	
	
	IF NOT bRobbersFightingBack
//		IF TIMERA() > 1500
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF bPhSpeech2
			AND NOT bPhSpeech3
//				Temporary "Give us all the cash! Put it in the bag."
				IF thisEvent = HAIRDRESSERS_HOLDUP
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CH2", "REROB_CH2_3", CONV_PRIORITY_AMBIENT_HIGH)
						bPhSpeech3 = TRUE
					ENDIF
				ELIF thisEvent = CLOTHES_SHOP_HOLDUP
					IF PLAY_SINGLE_LINE_FROM_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CH", "REROB_CH_3", CONV_PRIORITY_AMBIENT_HIGH)
						bPhSpeech3 = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF thisEvent = HAIRDRESSERS_HOLDUP
//			IF TIMERA() > 7500
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT bPhSpeech5
					IF thisEvent = HAIRDRESSERS_HOLDUP
						CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_LV2", CONV_PRIORITY_AMBIENT_HIGH)
					ELIF thisEvent = CLOTHES_SHOP_HOLDUP
						CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_LV", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
//					IF DOES_ENTITY_EXIST(objIdBag)
//						IF NOT IS_PED_INJURED(pedWorker[0])
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(objIdBag))
//								TASK_PLAY_ANIM(NULL, "random@robbery", "putdown_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//								TASK_PLAY_ANIM(NULL, "random@robbery", "putdown_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//								TASK_PLAY_ANIM(NULL, "random@robbery", "f_cower_01", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
//							CLEAR_SEQUENCE_TASK(seq)
							bPhSpeech5 = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
//			
//			IF TIMERA() > 8000
//				IF DOES_ENTITY_EXIST(objIdBag)
//					IF NOT IS_PED_INJURED(pedRobber[0])
//						IF NOT bRobberGoToBag
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(objIdBag))
//								TASK_GO_TO_ENTITY(NULL, objIdBag, DEFAULT_TIME_NEVER_WARP, 0.75, PEDMOVEBLENDRATIO_WALK)
//								//TASK_PLAY_ANIM(NULL, "random@robbery", "pickup_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
//							CLEAR_SEQUENCE_TASK(seq)
//							bRobberGoToBag = TRUE
//						ELSE
//							IF IS_ENTITY_AT_ENTITY(pedRobber[0], objIdBag, << 1.5, 1.5, 1.5 >>)
//							//	IF IS_ENTITY_PLAYING_ANIM(pedRobber[0], "random@robbery", "pickup_high")
//									IF NOT IS_ENTITY_ATTACHED(objIdBag)
//										swapBagModels()
//										ATTACH_ENTITY_TO_ENTITY(objIdBag, pedRobber[0], GET_PED_BONE_INDEX(pedRobber[0], BONETAG_L_HAND), << 0.380, 0.000, 0.020 >>, << 80.000, -90.000, 0.000 >>, FALSE)
//									ENDIF
////									IF DOES_BLIP_EXIST(blipRobber[1])
////										REMOVE_BLIP(blipRobber[1])
////									ENDIF
//								//ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			IF TIMERA() > 4000
				IF NOT bPhSpeech5
					IF DOES_ENTITY_EXIST(objIdBag)
						IF NOT IS_PED_INJURED(pedWorker[0])
//							OPEN_SEQUENCE_TASK(seq)
//								TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(objIdBag))
//								TASK_GO_STRAIGHT_TO_COORD(NULL, << -1193.3796, -766.6425, 16.3309 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 221.1527)
////								TASK_PLAY_ANIM(NULL, "amb@robbery", "putdown_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
////								TASK_PLAY_ANIM(NULL, "amb@robbery", "putdown_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//								TASK_PLAY_ANIM(NULL, "random@robbery", "f_cower_01", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
//							CLEAR_SEQUENCE_TASK(seq)
							bPhSpeech5 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			IF TIMERA() > 8500
				IF DOES_ENTITY_EXIST(objIdBag)
				AND NOT IS_PED_INJURED(pedRobber[0])
					IF NOT bRobberGoToBag
//						OPEN_SEQUENCE_TASK(seq)
//							TASK_GO_STRAIGHT_TO_COORD(NULL, << -1192.5469, -767.7260, 16.3295 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, 29.1260)
							//TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(objIdBag))
//							TASK_PLAY_ANIM(NULL, "amb@robbery", "pickup_high", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
//						CLOSE_SEQUENCE_TASK(seq)
//						TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
//						CLEAR_SEQUENCE_TASK(seq)
						bRobberGoToBag = TRUE
					ELSE
//						IF IS_ENTITY_AT_ENTITY(pedRobber[0], objIdBag, << 2, 2, 2 >>)
//							IF IS_ENTITY_PLAYING_ANIM(pedRobber[0], "amb@robbery", "pickup_high")
//							IF IS_SYNCHRONIZED_SCENE_RUNNING()
//							IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.453//0.6//25
//								IF NOT IS_ENTITY_ATTACHED(objIdBag)
//									swapBagModels()
//									SET_CURRENT_PED_WEAPON(pedRobber[0], WEAPONTYPE_UNARMED, TRUE)
//									ATTACH_ENTITY_TO_ENTITY(objIdBag, pedRobber[0], GET_PED_BONE_INDEX(pedRobber[0], BONETAG_R_HAND), << 0.300, 0.000, 0.000 >>, << 80.000, -90.000, 0.000 >>, TRUE, TRUE)
////									TASK_PLAY_ANIM(pedRobber[0], "random@robbery", "run", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
////									SET_PED_MOVEMENT_CLIPSET(pedRobber[0], "move_weapon@jerrycan@generic")
//								ENDIF
//							ENDIF
//						ENDIF
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
		
//		IF IS_ENTITY_ATTACHED(objIdBag)
			IF NOT bRobbersFled
				IF thisEvent = HAIRDRESSERS_HOLDUP
					IF TIMERA() > 14100
//						IF DOES_ENTITY_EXIST(objCash[0])
							DELETE_OBJECT(objCash[0])
							DELETE_OBJECT(objCash[1])
//						ENDIF
						//Temporary "Come on, let's get out of here."
						IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
							IF NOT IS_PED_INJURED(pedRobber[0])
								CLEAR_PED_TASKS(pedRobber[0])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_ENTER_VEHICLE(NULL, vehGetawayVeh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							IF NOT IS_PED_INJURED(pedRobber[1])
//								SET_PED_MIN_MOVE_BLEND_RATIO(pedRobber[1], PEDMOVEBLENDRATIO_RUN)
								FORCE_PED_MOTION_STATE(pedRobber[1], MS_ON_FOOT_RUN, TRUE)
//								CLEAR_PED_TASKS(pedRobber[1])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_ENTER_VEHICLE(NULL, vehGetawayVeh, DEFAULT_TIME_NEVER_WARP, VS_BACK_LEFT, PEDMOVE_RUN)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							RunRobberyStage = ROBBERY_AFTERMATH
							bRobbersFled = TRUE
						ELSE
							IF NOT IS_PED_INJURED(pedRobber[0])
								SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[0], TRUE)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRobber[0])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							IF NOT IS_PED_INJURED(pedRobber[1])
								SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[1], TRUE)
								CLEAR_PED_TASKS_IMMEDIATELY(pedRobber[1])
								OPEN_SEQUENCE_TASK(seq)
									TASK_CLEAR_LOOK_AT(NULL)
									TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
							RunRobberyStage = ROBBERY_AFTERMATH
							bRobbersFled = TRUE
						ENDIF
					ENDIF
				
				ELIF thisEvent = CLOTHES_SHOP_HOLDUP
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.453//0.6//25 //TIMERA() > 10500
						IF NOT IS_PED_INJURED(pedWorker[0])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[0], TRUE)
							OPEN_SEQUENCE_TASK(seq)
//								TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(pedWorker[0]) + << 1, 1, 0 >>)
								TASK_PLAY_ANIM(NULL, "random@robbery", "f_distressed_loop",/*"amb@robberystand@", "stand_worried_female",*/ REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedWorker[0], TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(pedRobber[0])
							CLEAR_PED_TASKS(pedRobber[0])
							SET_PED_FLEE_ATTRIBUTES(pedRobber[0], FA_DISABLE_HANDS_UP, TRUE)
							SET_PED_FLEE_ATTRIBUTES(pedRobber[0], FA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedRobber[0], CA_ALWAYS_FLEE, TRUE)
							SET_PED_MAX_MOVE_BLEND_RATIO(pedRobber[0], 1.6)
							SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[0], TRUE)
//							CLEAR_PED_TASKS(pedRobber[0])
							OPEN_SEQUENCE_TASK(seq)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1195.3849, -771.4706, 16.3211 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 1)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1203.8125, -779.2685, 16.3317 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 1)
								TASK_PLAY_ANIM(NULL, "random@robbery", "run", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY|AF_UPPERBODY)
								TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "re_shoprobbery", 0, EWAYPOINT_DEFAULT, 16)
//								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
								TASK_CLEAR_LOOK_AT(NULL)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedRobber[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedRobber[0], TRUE)
							SETTIMERB(0)
						ENDIF
						IF NOT IS_PED_INJURED(pedRobber[1])
							SET_PED_FLEE_ATTRIBUTES(pedRobber[1], FA_DISABLE_HANDS_UP, TRUE)
							SET_PED_FLEE_ATTRIBUTES(pedRobber[1], FA_USE_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(pedRobber[1], CA_ALWAYS_FLEE, TRUE)
							SET_PED_MAX_MOVE_BLEND_RATIO(pedRobber[1], 1.6)
							SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[1], TRUE)
//							CLEAR_PED_TASKS(pedRobber[1])
							OPEN_SEQUENCE_TASK(seq)
								TASK_CLEAR_LOOK_AT(NULL)
//								TASK_STAND_STILL(NULL, 500)
//								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1203.3595, -778.9078, 16.3331 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "re_shoprobbery2", 0, EWAYPOINT_DEFAULT, 17)
//								TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PED_KEEP_TASK(pedRobber[1], TRUE)
						ENDIF
						RunRobberyStage = ROBBERY_AFTERMATH
						bRobbersFled = TRUE
					ENDIF
				ENDIF
				
				SET_WANTED_LEVEL_MULTIPLIER(0)
//				SET_MAX_WANTED_LEVEL(0)
				
//				FOR i = 0 to (iNoOfRobbers - 1)
//					IF NOT IS_PED_INJURED(pedRobber[i])
//						SET_PED_COMBAT_ATTRIBUTES(pedRobber[i], CA_AGGRESSIVE, FALSE)
//						SET_PED_COMBAT_ATTRIBUTES(pedRobber[i], CA_ALWAYS_FLEE | CA_FLEE_WHILST_IN_VEHICLE | CA_USE_VEHICLE, TRUE)
//					ENDIF
//				ENDFOR
				
			ENDIF
//		ENDIF
	ENDIF
	
ENDPROC

//PROC simpleRobberyStandOff()
//	iAimingCurrentTimer = GET_GAME_TIMER()
//
//	IF NOT IS_PED_SHOOTING(PLAYER_PED_ID())
//		IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
//		OR IS_AIM_CAM_ACTIVE()
//			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
//				IF CurrentWeapon <> WEAPONTYPE_UNARMED
//					IF NOT bPhSpeech8
					
//						bPhSpeech8 = TRUE
//					ENDIF
//					
//					IF (iAimingCurrentTimer - iAimingTimer) > 9000
//						IF NOT bPhSpeech9
						
//							bPhSpeech9 = TRUE
//						ENDIF
//					ENDIF
//					
//					IF (iAimingCurrentTimer - iAimingTimer) > 19000
//						IF NOT bPhSpeech10
//							FOR i = 0 TO 1
//								IF NOT IS_PED_INJURED(pedRobber[i])
//									TASK_COMBAT_PED(pedRobber[i], PLAYER_PED_ID())
//								ENDIF
//							ENDFOR
//							bPhSpeech10 = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF NOT b2ndRobberAimingAtPlayer
//			IF NOT IS_PED_INJURED(pedRobber[1])
//				IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedRobber[1])
//				OR  IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedRobber[1])
//					CLEAR_PED_TASKS(pedRobber[1])
//					TASK_AIM_GUN_AT_ENTITY(pedRobber[1], PLAYER_PED_ID(), -1)
//					b2ndRobberAimingAtPlayer = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
//		AND NOT IS_AIM_CAM_ACTIVE()
//			RunRobberyStage = ROBBERY_PLAYER_ENTERED
//		ENDIF
//	ELSE
//		RunRobberyStage = ARMED_AND_SHOOTING
//	ENDIF
//	
//ENDPROC

FUNC BOOL HAVE_ROBBERS_LEFT_INTERIOR()

//	IF NOT IS_ENTITY_DEAD(pedRobber[0])	
//		INTID_PED_INTERIOR = GET_INTERIOR_FROM_ENTITY(pedRobber[0])
//		IF INTID_PED_INTERIOR <> INTID_ROBBERY_INTERIOR
//		AND INTID_PED_INTERIOR = NULL
//			bRobber0NotInSalon = TRUE
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_ENTITY_DEAD(pedRobber[1])
//		INTID_PED_INTERIOR = GET_INTERIOR_FROM_ENTITY(pedRobber[1])
//		IF INTID_PED_INTERIOR <> INTID_ROBBERY_INTERIOR
//		AND INTID_PED_INTERIOR = NULL
//			bRobber1NotInSalon = TRUE
//		ENDIF
//	ENDIF
	
	IF NOT IS_PED_INJURED(pedRobber[0])
	AND NOT IS_PED_INJURED(pedRobber[1])
		IF thisEvent = HAIRDRESSERS_HOLDUP
			IF NOT bRobber0NotInSalon
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], <<-823.506592,-187.346466,40.363548>>, <<-803.637695,-175.949646,36.433704>>, 11.937500)
//					IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
//						TASK_ENTER_VEHICLE(pedRobber[0], vehGetawayVeh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
//					ELSE
//						TASK_SMART_FLEE_PED(pedRobber[0], PLAYER_PED_ID(), 1000, -1)
//					ENDIF
					bRobber0NotInSalon = TRUE
				ENDIF
			ENDIF
			IF NOT bRobber1NotInSalon
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], <<-823.506592,-187.346466,40.363548>>, <<-803.637695,-175.949646,36.433704>>, 11.937500)
//					IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
//						TASK_ENTER_VEHICLE(pedRobber[1], vehGetawayVeh, DEFAULT_TIME_NEVER_WARP, VS_BACK_LEFT, PEDMOVE_RUN)
//					ELSE
//						TASK_SMART_FLEE_PED(pedRobber[1], PLAYER_PED_ID(), 1000, -1)
//					ENDIF
					bRobber1NotInSalon = TRUE
				ENDIF
			ENDIF
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			IF NOT bRobber0NotInSalon
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], <<-1204.030640,-779.628967,16.335648>>, <<-1174.798584,-757.070374,21.012814>>, 12.125000)
//				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], <<-1203.899170,-776.917786,20.168028>>, <<-1201.289307,-780.469849,15.615259>>, 3.937500)
				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], <<-1206.544678,-777.894104,21.203548>>, <<-1204.313354,-778.224487,16.084339>>, 2.250000)
				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[0], <<-1202.927612,-782.688293,21.203688>>, <<-1202.583008,-780.506653,16.082426>>, 2.250000)
//				IF IS_ENTITY_AT_COORD(pedRobber[0], << -1220.6881, -767.4489, 17.3912 >>, << 10, 5, 5 >>)
					TASK_SMART_FLEE_PED(pedRobber[0], PLAYER_PED_ID(), 1000, -1)
					bRobber0NotInSalon = TRUE
				ENDIF
			ENDIF
			IF NOT bRobber1NotInSalon
				IF NOT IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], <<-1204.030640,-779.628967,16.335648>>, <<-1174.798584,-757.070374,21.012814>>, 12.125000)
//				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], <<-1203.899170,-776.917786,20.168028>>, <<-1201.289307,-780.469849,15.615259>>, 3.937500)
				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], <<-1206.544678,-777.894104,21.203548>>, <<-1204.313354,-778.224487,16.084339>>, 2.250000)
				OR IS_ENTITY_IN_ANGLED_AREA(pedRobber[1], <<-1202.927612,-782.688293,21.203688>>, <<-1202.583008,-780.506653,16.082426>>, 2.250000)
//				IF IS_ENTITY_AT_COORD(pedRobber[1], << -1220.6881, -767.4489, 17.3912 >>, << 10, 5, 5 >>)
					TASK_SMART_FLEE_PED(pedRobber[1], PLAYER_PED_ID(), 1000, -1)
					bRobber1NotInSalon = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bRobber0NotInSalon
	AND bRobber1NotInSalon
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PROC playerGoingAfterRobberChecks()
//	IF NOT IS_ENTITY_DEAD(pedRobber[1])
//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[1], PLAYER_PED_ID())
//			IF NOT bShootBackAtPlayer
//				IF NOT IS_PED_INJURED(pedRobber[1])
//					CLEAR_PED_TASKS(pedRobber[1])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, FIRING_TYPE_1_BURST)
//						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//				bShootBackAtPlayer = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF NOT IS_ENTITY_DEAD(pedRobber[0])
//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[0], PLAYER_PED_ID())
//			IF NOT bShootBackAtPlayer
//				IF NOT IS_PED_INJURED(pedRobber[1])
//					CLEAR_PED_TASKS(pedRobber[1])
//					OPEN_SEQUENCE_TASK(seq)
//						TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, FIRING_TYPE_1_BURST)
//						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)	
//					CLOSE_SEQUENCE_TASK(seq)
//					TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
//					CLEAR_SEQUENCE_TASK(seq)
//				ENDIF
//			ENDIF
//			RunRobberyStage = HARMED_ROBBERS
//		ENDIF
//	ELSE
//		IF NOT bShootBackAtPlayer
//			IF NOT IS_PED_INJURED(pedRobber[1])
//				CLEAR_PED_TASKS(pedRobber[1])
//				OPEN_SEQUENCE_TASK(seq)
//					TASK_SHOOT_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000, FIRING_TYPE_1_BURST)
//					TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 1000, -1)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(pedRobber[1], seq)
//				CLEAR_SEQUENCE_TASK(seq)
//			ENDIF
//		ENDIF
//		RunRobberyStage = HARMED_ROBBERS
//	ENDIF
//	
//ENDPROC

FUNC BOOL HAS_PLAYER_DAMAGED_ROBBERS_OR_CAR()

	INT i
	FOR i = 0 to (iNoOfRobbers - 1)
		IF DOES_ENTITY_EXIST(pedRobber[i])
			IF NOT IS_PED_INJURED(pedRobber[i])
			OR IS_ENTITY_DEAD(pedRobber[i])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[i], PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 to (iNoOfRobbers - 1)
		IF DOES_ENTITY_EXIST(pedRobber[i])
			IF NOT IS_PED_INJURED(pedRobber[i])
				IF IS_PED_BEING_JACKED(pedRobber[i])
					IF GET_PEDS_JACKER(pedRobber[i]) = PLAYER_PED_ID()
						SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[i], TRUE)
						TASK_SMART_FLEE_PED(pedRobber[i], PLAYER_PED_ID(), 250, -1)
						SET_PED_KEEP_TASK(pedRobber[i], TRUE)
						WAIT(0)
						SET_PED_AS_NO_LONGER_NEEDED(pedRobber[i])
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(pedDriver)
		IF NOT IS_PED_INJURED(pedDriver)
		OR IS_ENTITY_DEAD(pedDriver)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDriver, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedDriver)
			IF IS_PED_BEING_STUNNED(PLAYER_PED_ID(), WEAPONTYPE_STUNGUN)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedDriver)
		IF NOT IS_PED_INJURED(pedDriver)
			IF IS_PED_BEING_JACKED(pedDriver)
				IF GET_PEDS_JACKER(pedDriver) = PLAYER_PED_ID()
					SET_ENTITY_LOAD_COLLISION_FLAG(pedDriver, TRUE)
					TASK_SMART_FLEE_PED(pedDriver, PLAYER_PED_ID(), 250, -1)
					SET_PED_KEEP_TASK(pedDriver, TRUE)
					WAIT(0)
					SET_PED_AS_NO_LONGER_NEEDED(pedDriver)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehGetawayVeh)
		IF NOT IS_ENTITY_DEAD(vehGetawayVeh)
		OR IS_ENTITY_DEAD(vehGetawayVeh)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehGetawayVeh, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC vehicleChaseReduceHealth()
	IF NOT IS_ENTITY_DEAD(vehGetawayVeh)
		IF GET_ENTITY_HEALTH(vehGetawayVeh) < 800
			SET_ENTITY_HEALTH(vehGetawayVeh, 0)
			SET_VEHICLE_ENGINE_HEALTH(vehGetawayVeh, 10)
		ENDIF
	ENDIF
ENDPROC

PROC loseRobbersCheck()
	IF DOES_ENTITY_EXIST(pedRobber[0])
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedRobber[0], FALSE)) > 200
		AND IS_ENTITY_OCCLUDED(pedRobber[0])
			IF DOES_BLIP_EXIST(blipRobber[0])
				REMOVE_BLIP(blipRobber[0])
			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(pedRobber[0])
			bRobber0Lost = TRUE
		ENDIF
	ELSE
		bRobber0Lost = TRUE
	ENDIF
	IF DOES_ENTITY_EXIST(vehGetawayVeh)
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehGetawayVeh, FALSE)) > 200
		AND IS_ENTITY_OCCLUDED(vehGetawayVeh)
			IF DOES_BLIP_EXIST(blipGetawayVeh)
				REMOVE_BLIP(blipGetawayVeh)
			ENDIF
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehGetawayVeh)
			bGetawayVehLost = TRUE
		ENDIF
	ELSE
		bGetawayVehLost = TRUE
	ENDIF
	
	IF bCreateBagAtVehicle
		IF bGetawayVehLost
			IF IS_PLAYER_IN_ROBBERY_INTERIOR()
				RunRobberyStage = END_SCRIPT_INSIDE_INTERIOR
			ELSE
				WAIT(0)
				RobberyStage = STAGE_CLEAN_UP
			ENDIF
		ENDIF
	ELSE
		IF bRobber0Lost
			WAIT(0)
			IF IS_PLAYER_IN_ROBBERY_INTERIOR()
				RunRobberyStage = END_SCRIPT_INSIDE_INTERIOR
			ELSE
				RobberyStage = STAGE_CLEAN_UP
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC createStolenBagPickup()
	
	IF NOT DOES_PICKUP_EXIST(pickupStolenBag)
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		IF NOT bCreateBagAtVehicle
			IF IS_ENTITY_DEAD(pedRobber[0])
				IF DOES_ENTITY_EXIST(pedRobber[0])
					vDroppedItemPlacement = GET_DEAD_PED_PICKUP_COORDS(pedRobber[0])
				ENDIF
			ELSE
				vDroppedItemPlacement = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(pedRobber[0]))
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehGetawayVeh)
				vDroppedItemPlacement = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(vehGetawayVeh, FALSE))
			ENDIF
		ENDIF
		
		vDroppedItemPlacement = <<vDroppedItemPlacement.x, vDroppedItemPlacement.y, (vDroppedItemPlacement.z+0.25)>>
		pickupStolenBag = CREATE_PICKUP(PICKUP_MONEY_MED_BAG, vDroppedItemPlacement, iPlacementFlags, iStolenMoney, TRUE, Prop_CS_Duffel_01)
		blipBag = CREATE_BLIP_FOR_PICKUP(pickupStolenBag)
		
		IF thisEvent = HAIRDRESSERS_HOLDUP
			INT i
			FOR i = 0 to (iNoOfRobbers - 1)
				IF NOT IS_PED_INJURED(pedRobber[i])
					SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[i], TRUE)
					CLEAR_PED_TASKS(pedRobber[i])
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedRobber[i], seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedRobber[i], TRUE)
				ENDIF
			ENDFOR
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			IF NOT IS_PED_INJURED(pedRobber[0])
				CLEAR_PED_SECONDARY_TASK(pedRobber[0])
				IF NOT IS_PED_IN_WRITHE(pedRobber[0])
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					WAIT(0)
					CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_DRP", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC getRidOfBagObjectFromInjuredPed()
	IF IS_PED_INJURED(pedRobber[0])
		IF DOES_ENTITY_EXIST(objIdBag)
			IF IS_ENTITY_ATTACHED(objIdBag)
				DETACH_ENTITY(objIdBag)
			ENDIF
			DELETE_OBJECT(objIdBag)
			removeAllBlips()
			createStolenBagPickup()
			RunRobberyStage = STOLEN_STUFF_DROPPED
		ENDIF
	ELSE
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[0], PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(objIdBag)
				IF IS_ENTITY_ATTACHED(objIdBag)
					DETACH_ENTITY(objIdBag)
				ENDIF
				DELETE_OBJECT(objIdBag)
				removeAllBlips()
				createStolenBagPickup()
				RunRobberyStage = STOLEN_STUFF_DROPPED
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC bagPickUp()
	IF HAS_PICKUP_BEEN_COLLECTED(pickupStolenBag)
		QUICK_INCREMENT_INT_STAT(RC_WALLETS_RECOVERED, 1)
		IF NOT IS_PED_INJURED(pedWorker[0])
			blipShopWorker = CREATE_BLIP_FOR_PED(pedWorker[0])
		ENDIF
		START_RETURN_ITEM_BLIP_FLASHING(iBlipTimer)
		//===========================
		// for later check: LEFT_THE_SCENE
		fStoreBagCollectDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), getWorldPoint())
		//===========================
		SETTIMERA(0)
		RunRobberyStage = PICKED_UP_STOLEN_STUFF
	ENDIF
	IF DOES_PICKUP_EXIST(pickupStolenBag)
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_PICKUP_COORDS(pickupStolenBag)) > 200
			MISSION_FAILED()
		ENDIF
	ENDIF
ENDPROC

PROC givingBagBackScene()
	IF IS_PLAYER_IN_ROBBERY_INTERIOR()
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//		ENDIF
		IF DOES_BLIP_EXIST(blipShopWorker)
			REMOVE_BLIP(blipShopWorker)
		ENDIF
		IF NOT bGetToHandoverPosition
			IF NOT IS_PED_INJURED(pedWorker[0])
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_HELP(TRUE)
				CLEAR_AREA_OF_PROJECTILES(GET_ENTITY_COORDS(pedWorker[0]), 20)
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				IF thisEvent = HAIRDRESSERS_HOLDUP
//					TASK_GO_TO_ENTITY(PLAYER_PED_ID(), pedWorker[0], DEFAULT_TIME_NEVER_WARP, 0.8, PEDMOVEBLENDRATIO_WALK)
					bReadyToGiveBagBack = TRUE
				ELIF thisEvent = CLOTHES_SHOP_HOLDUP
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1199.5553, -776.6296, 16.3271 >>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 303.3907)
//					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), (GET_ENTITY_COORDS(pedWorker[0])), PEDMOVEBLENDRATIO_WALK)
				ENDIF
				bGetToHandoverPosition = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bReadyToGiveBagBack
		IF NOT IS_PED_INJURED(pedWorker[0])
//			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedWorker[0], << 1.2, 1.2, 5 >>)
//				CLEAR_PED_TASKS(PLAYER_PED_ID())
				bReadyToGiveBagBack = TRUE
//			ENDIF
		ENDIF
	ENDIF
	
	IF bReadyToGiveBagBack
		IF NOT bStartedHandoverSyncScene
			
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			IF DOES_BLIP_EXIST(blipShopWorker)
				REMOVE_BLIP(blipShopWorker)
			ENDIF
			
			scenePosition = GET_ENTITY_COORDS(pedWorker[0], FALSE)
			
//			INT iRandomReturnAnimation
			
			IF thisEvent = HAIRDRESSERS_HOLDUP
			
				scenePosition = << -820.770, -186.514, 36.569 >> //<< -819.645, -186.138, 36.569 >>
				sceneRotation = << 0.000, 0.000, -60.000 >>
				
				fSyncPhase = 0.855//0.95
//				scenePosition = << scenePosition.x, scenePosition.y, scenePosition.z -0.9 >>
//				sceneRotation = << 0.000, 0.000, 240.000 >>
				
//				iRandomReturnAnimation = GET_RANDOM_INT_IN_RANGE(0, 2)
//				IF iRandomReturnAnimation = 0
//					ambrobberydict = "random@robbery"
//					return_bag_player = "return_bag_stand_a"
//					return_bag_female = "return_bag_stand_a_female"
//					return_bag_bag = "return_bag_stand_a_bag"
//					return_bag_cam = "return_bag_stand_a_cam"
//				ELIF iRandomReturnAnimation = 1
					ambrobberydict = "random@robbery"
					return_bag_player = "return_bag_stand_b"
					return_bag_female = "return_bag_stand_b_female"
					return_bag_bag = "return_bag_stand_b_bag"
					return_bag_cam = "return_bag_stand_b_cam"
//				ELSE
//					ambrobberydict = "random@robbery"
//					return_bag_player = "return_bag_stand_c_rt"
//					return_bag_female = "return_bag_stand_c_rt_female"
//					return_bag_bag = "return_bag_stand_c_rt_bag"
//					return_bag_cam = "return_bag_stand_c_rt_cam"
//				ENDIF
				
			ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			
				scenePosition = << -1198.295, -776.533, 16.326 >>//<< -1198.772, -776.858, 16.288 >>
				sceneRotation = << 0.000, 0.000, -60.250 >>
				
				fSyncPhase = 0.785//0.86
//				scenePosition = << scenePosition.x, scenePosition.y -0.5, scenePosition.z -0.9 >>
//				sceneRotation = << 0.0000, 0.0000, -40.0000 >>
				
//				iRandomReturnAnimation = GET_RANDOM_INT_IN_RANGE(1, 2)
//				IF iRandomReturnAnimation = 0
//					ambrobberydict = "random@robbery"
//					return_bag_player = "return_bag_stand_a"
//					return_bag_female = "return_bag_stand_a_female"
//					return_bag_bag = "return_bag_stand_a_bag"
//					return_bag_cam = "return_bag_stand_a_cam"
//				IF iRandomReturnAnimation = 1
//					ambrobberydict = "random@robbery"
//					return_bag_player = "return_bag_stand_b"
//					return_bag_female = "return_bag_stand_b_female"
//					return_bag_bag = "return_bag_stand_b_bag"
//					return_bag_cam = "return_bag_stand_b_cam"
//				ELSE
					ambrobberydict = "random@robbery"
					return_bag_player = "return_bag_stand_c_rt"
					return_bag_female = "return_bag_stand_c_rt_female"
					return_bag_bag = "return_bag_stand_c_rt_bag"
					return_bag_cam = "return_bag_stand_c_rt_cam"
//				ENDIF
//				iRandomReturnAnimation = GET_RANDOM_INT_IN_RANGE(0, 1)
//				IF iRandomReturnAnimation = 0
//					ambrobberydict = "amb@robberywalk@"
//					return_bag_player = "return_bag_walk_a"
//					return_bag_female = "return_bag_walk_a_female"
//					return_bag_bag = "return_bag_walk_a_bag"
//					return_bag_cam = "return_bag_walk_a_cam"
//				ELSE
//					ambrobberydict = "amb@robberywalk@"
//					return_bag_player = "return_bag_walk_b"
//					return_bag_female = "return_bag_walk_b_female"
//					return_bag_bag = "return_bag_walk_b_bag"
//					return_bag_cam = "return_bag_walk_b_cam"
//				ENDIF
			ENDIF
//			return_bag_player = "Return_Handbag_Player"
//			return_bag_female = "Return_Handbag_Ped"
//			return_bag_cam = "Return_Handbag_Cam"
			
			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			CamIDHandingOverBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
			
//			IF thisEvent = CLOTHES_SHOP_HOLDUP
//				IF NOT IS_PED_INJURED(pedWorker[0])
//					IF IS_PED_FACING_PED(pedWorker[0], PLAYER_PED_ID(), 180)
//						SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.2)
//						ambrobberydict = "amb@robberystand@"
//						return_bag_player = "return_bag_stand_c_rt"
//						return_bag_female = "return_bag_stand_c_rt_female"
//						return_bag_bag = "return_bag_stand_c_rt_bag"
//						return_bag_cam = "return_bag_stand_c_rt_cam"
//					ENDIF
//				ENDIF
//			ENDIF
			
			PLAY_SYNCHRONIZED_CAM_ANIM(CamIDHandingOverBag, sceneId, return_bag_cam, ambrobberydict)
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, ambrobberydict, return_bag_player, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)//, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_ARM_IK)
			//THEN GO TO << -1202.2101, -778.8771, 16.3355 >>
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			
			CLEAR_PED_TASKS_IMMEDIATELY(pedWorker[0])
			TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, ambrobberydict, return_bag_female, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedWorker[0])
			
			//recreate bag
			objIdBag = CREATE_OBJECT(Prop_CS_Duffel_01, GET_ENTITY_COORDS(PLAYER_PED_ID()))
			PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, return_bag_bag, ambrobberydict, INSTANT_BLEND_IN)
			
//			IF thisEvent = CLOTHES_SHOP_HOLDUP
//				SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.140)
//			ENDIF
			
			IF NOT IS_PED_INJURED(pedWorker[0])
				moveBlockingVehicle()
				SET_CAM_ACTIVE(CamIDHandingOverBag, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SETTIMERA(0)
				bStartedHandoverSyncScene = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF TIMERA() > 1500
	AND bStartedHandoverSyncScene
	AND NOT bPhSpeech7
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		WAIT(0)
		IF thisEvent = HAIRDRESSERS_HOLDUP
			CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_TKH", CONV_PRIORITY_AMBIENT_HIGH)
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CLDISC", CONV_PRIORITY_AMBIENT_HIGH) //REROB_TKB
		ENDIF
		bPhSpeech7 = TRUE
	ENDIF
	
//	IF TIMERA() > 7000
	IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > fSyncPhase
	AND bStartedHandoverSyncScene
		
		IF NOT IS_PED_INJURED(pedWorker[0])
			SET_PED_MONEY(pedWorker[0], iStolenMoney)
			CLEAR_PED_TASKS(pedWorker[0])
//			CLEAR_PED_TASKS_IMMEDIATELY(pedWorker[0])
//			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedWorker[0])
			OPEN_SEQUENCE_TASK(seq)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vWorkerPedsPos[0], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, fWorkerPedsHdg[0])
				TASK_PLAY_ANIM(NULL, "random@robbery", "stand_worried_female", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING)
//				TASK_PAUSE(NULL, 100000000)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_KEEP_TASK(pedWorker[0], TRUE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objIdBag)
			DELETE_OBJECT(objIdBag)
		ENDIF
		
//		IF thisEvent = HAIRDRESSERS_HOLDUP
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -821.8534, -187.1348, 36.5689 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 111.7935)
//		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -1199.9266, -777.0011, 16.3240 >>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 118.6598)
//		ENDIF
		
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
		SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 650)
//		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_CLEAR_TASKS)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(CamIDHandingOverBag)
		freePedsFromScript()
		
		PRINTSTRING("re_shoprobbery - took bag back to shop")
		PRINTNL()
		
//		WHILE IS_PLAYER_IN_ROBBERY_INTERIOR()
//			WAIT(0)
//		ENDWHILE
		
		QUICK_INCREMENT_INT_STAT(RC_WALLETS_RETURNED, 1)
		IF thisEvent = HAIRDRESSERS_HOLDUP
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iStolenMoney)
			SET_PLAYER_HAS_STOPPED_SHOP_ROBBERY(HAIRDO_SHOP_01_BH, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
		ELIF thisEvent = CLOTHES_SHOP_HOLDUP
			DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, iStolenMoney)
			SET_PLAYER_HAS_STOPPED_SHOP_ROBBERY(CLOTHES_SHOP_M_01_SM, GET_CURRENT_PLAYER_PED_ENUM(), TRUE)
		ENDIF
//		IF thisEvent = CLOTHES_SHOP_HOLDUP
//			SET_PLAYER_HAS_STOPPED_SHOP_ROBBERY()
//		ENDIF
		SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
		MISSION_PASSED()
	ENDIF
ENDPROC

//FUNC BOOL IS_VARIATION_COMPLETE()
//	BOOL bReturn = FALSE
//	
//	SWITCH iCurrentArrest
//		CASE ARREST_1
//			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ARR1)
//				bReturn = TRUE
//			ENDIF
//		BREAK
//		CASE ARREST_2
//			IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_ARR2)
//				bReturn = TRUE
//			ENDIF
//		BREAK
//	ENDSWITCH
//	
//	RETURN bReturn
//ENDFUNC


// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

vInput = in_coords.vec_coord[0]
PRINTSTRING("re_shoprobbery launched ")
PRINTVECTOR(vInput)
PRINTNL()

// work out which variation is being launched
getWorldPoint()

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
	KILL_FACE_TO_FACE_CONVERSATION()
	missionCleanup()
ENDIF

IF thisEvent = HAIRDRESSERS_HOLDUP
	IF NOT CAN_ROBBERY_TAKE_PLACE_IN_SHOP(HAIRDO_SHOP_01_BH)
	OR NOT IS_SHOP_AVAILABLE(HAIRDO_SHOP_01_BH)
		PRINTSTRING("the player hasn't yet visited the hairdressers yet so re_shoprobbery not launched")
		PRINTNL()
		TERMINATE_THIS_THREAD()
	ELSE
		PRINTSTRING("the player has visited the hairdressers already - so re_shoprobbery launched")
		PRINTNL()
	ENDIF
ELIF thisEvent = CLOTHES_SHOP_HOLDUP
	IF NOT CAN_ROBBERY_TAKE_PLACE_IN_SHOP(CLOTHES_SHOP_M_01_SM)
	OR NOT IS_SHOP_AVAILABLE(CLOTHES_SHOP_M_01_SM)
		PRINTSTRING("the player hasn't yet visited the clothes shop yet so re_shoprobbery not launched")
		PRINTNL()
		TERMINATE_THIS_THREAD()
	ELSE
		PRINTSTRING("the player has visited the clothes shop already - so re_shoprobbery launched")
		PRINTNL()
	ENDIF
ENDIF

//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_SHOPROBBERY, iSelectedVariation)
	LAUNCH_RANDOM_EVENT(RE_SHOPROBBERY)
ELSE
	TERMINATE_THIS_THREAD()
ENDIF

// Mission Loop -------------------------------------------//
WHILE TRUE
WAIT(0)
	IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
	OR bPlayerGotUp
//		IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)

			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_SR")
			
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				SWITCH ambStage
					CASE ambCanRun
						IF bAssetsLoaded 
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ELSE
							IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
								missionCleanup()
							ENDIF
							IF NOT bVariablesInitialised
								initialiseEventVariables()
							ENDIF
							IF bVariablesInitialised
								loadAssets()
							ENDIF
						ENDIF
					BREAK
					CASE ambRunning
					
						SWITCH RobberyStage
						
							CASE STAGE_CREATE_SCENE
								IF NOT bCreatedScene
									interiorStreamingSystem()
									IF IS_INTERIOR_READY(INTID_ROBBERY_INTERIOR)
										createScene()
										bCreatedScene = TRUE
									ENDIF
								ELSE
									IF thisEvent = HAIRDRESSERS_HOLDUP
										REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2220348
									ENDIF
									
									keepEmCrying()
									
									UNPIN_INTERIOR(INTID_ROBBERY_INTERIOR)
									IF IS_PLAYER_OUT_FRONT()
										IF NOT bToldToEmptyRegister
											IF thisEvent = HAIRDRESSERS_HOLDUP
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CH2", "REROB_CH2_1", CONV_PRIORITY_AMBIENT_HIGH)
													bToldToEmptyRegister = TRUE
												ENDIF
											ELIF thisEvent = CLOTHES_SHOP_HOLDUP
												IF PLAY_SINGLE_LINE_FROM_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CH", "REROB_CH_1", CONV_PRIORITY_AMBIENT_HIGH)
													bToldToEmptyRegister = TRUE
												ENDIF
											ENDIF
										ENDIF
//										SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//										IF IS_PHONE_ONSCREEN()
//											DISABLE_CELLPHONE(TRUE)
//										ENDIF
//									ELIF NOT IS_PHONE_ONSCREEN()
//										DISABLE_CELLPHONE(FALSE)
									ENDIF
									
									IF IS_PLAYER_INTERFERING_WITH_EVENT()
										IF DOES_BLIP_EXIST(blipShopWorker)
											REMOVE_BLIP(blipShopWorker)
										ENDIF
										CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
										SET_WANTED_LEVEL_MULTIPLIER(0)
//										SET_MAX_WANTED_LEVEL(0)
										INT index
										REPEAT COUNT_OF(pedRobber) index
											IF NOT IS_PED_INJURED(pedRobber[index])
												TASK_COMBAT_PED(pedRobber[index], PLAYER_PED_ID())
												SET_PED_KEEP_TASK(pedRobber[index], TRUE)
												IF NOT DOES_BLIP_EXIST(blipRobber[index])
													blipRobber[index] = CREATE_BLIP_FOR_PED(pedRobber[index], TRUE)
												ENDIF
											ENDIF
										ENDREPEAT
										IF thisEvent = CLOTHES_SHOP_HOLDUP
											REPEAT COUNT_OF(pedWorker) index
												IF NOT IS_PED_INJURED(pedWorker[index])
													TASK_COWER(pedWorker[index])
													SET_PED_KEEP_TASK(pedWorker[index], TRUE)
												ENDIF
											ENDREPEAT
											REPEAT COUNT_OF(pedShopper) index
												IF NOT IS_PED_INJURED(pedShopper[index])
													TASK_COWER(pedShopper[index])
													SET_PED_KEEP_TASK(pedShopper[index], TRUE)
												ENDIF
											ENDREPEAT
										ENDIF
										DELETE_OBJECT(objIdBag)
										bRobbersFightingBack = TRUE
										RobberyStage = STAGE_RUN_ROBBERY
										RunRobberyStage = ROBBERS_ATTACKED
									ENDIF
									
									IF IS_PLAYER_IN_ROBBERY_INTERIOR()
										SET_RANDOM_EVENT_ACTIVE()
										
//										CurrentWeapon = GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID())
										SETTIMERA(0)
										ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_GUN_FIGHT, vLocReturnBag, 15000)
										RobberyStage = STAGE_RUN_ROBBERY
									ENDIF
								ENDIF
							BREAK
							
							CASE STAGE_RUN_ROBBERY
								SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
								keepEmCrying()
								INT index
								REPEAT COUNT_OF(pedWorker) index
									IF DOES_ENTITY_EXIST(pedWorker[index])
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedWorker[index], PLAYER_PED_ID())
											MISSION_FAILED()
										ENDIF
									ENDIF
								ENDREPEAT
								REPEAT COUNT_OF(pedShopper) index
									IF DOES_ENTITY_EXIST(pedShopper[index])
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedShopper[index], PLAYER_PED_ID())
											MISSION_FAILED()
										ENDIF
									ENDIF
								ENDREPEAT
								
								SWITCH RunRobberyStage
								
									CASE ROBBERY_PLAYER_ENTERED
										IF IS_PLAYER_IN_ROBBERY_INTERIOR()
											playerEntersRobberyScene()
//											IF thisEvent = HAIRDRESSERS_HOLDUP
												superQuickActionCam()
//											ENDIF
											// player intervention checks
//											IF IS_PED_SHOOTING(PLAYER_PED_ID())
//												RunRobberyStage = ARMED_AND_SHOOTING
//											ENDIF
//											IF  IS_PLAYER_AIMING_WEAPON()
//												RunRobberyStage = STANDOFF_SITUATION
//											ENDIF
										ENDIF
									BREAK
									
									CASE ROBBERY_COMMENCES
										robberyCommences()
										superQuickActionCam()
									BREAK
									
									CASE ROBBERY_AFTERMATH
										removeDeadPedBlips()
										vehicleChaseReduceHealth()
										loseRobbersCheck()
										IF TIMERB() > 500
											IF DOES_ENTITY_EXIST(objIdBag)
												IF NOT IS_ENTITY_ATTACHED(objIdBag)
													swapBagModels()
													SET_CURRENT_PED_WEAPON(pedRobber[0], WEAPONTYPE_UNARMED, TRUE)
													ATTACH_ENTITY_TO_ENTITY(objIdBag, pedRobber[0], GET_PED_BONE_INDEX(pedRobber[0], BONETAG_R_HAND), << 0.300, 0.000, 0.000 >>, << 80.000, -90.000, 0.000 >>, TRUE, TRUE)
												ENDIF
											ENDIF
										ENDIF
										
										IF IS_PROJECTILE_IN_AREA(vLocReturnBag - << 20, 20, 20 >>, vLocReturnBag + << 20, 20, 20 >>)
											IF NOT IS_PED_INJURED(pedWorker[1])
												TASK_SMART_FLEE_COORD(pedWorker[1], vFleeCoords, 150, -1)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[1], FALSE)
												SET_PED_AS_NO_LONGER_NEEDED(pedWorker[1])
											ENDIF
											IF NOT IS_PED_INJURED(pedWorker[2])
												TASK_SMART_FLEE_COORD(pedWorker[2], vFleeCoords, 150, -1)
												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[2], FALSE)
												SET_PED_AS_NO_LONGER_NEEDED(pedWorker[2])
											ENDIF
											INT i
//											INT syncId
											FOR i = 0 to (iNoOfShoppers - 1)
												IF NOT IS_PED_INJURED(pedShopper[i])
													SET_ENTITY_LOAD_COLLISION_FLAG(pedShopper[i], TRUE)
//													IF i = 0 OR i = 2
//														IF i = 0
//															scenePositionExit = << -812.370, -185.650, 37.105 >>
//															sceneRotationExit = << 0.000, 0.000, 37.680 >>
//														ELIF i = 2
//															scenePositionExit = << -816.224, -182.897, 37.180 >>
//															sceneRotationExit = << 0.000, 0.000, 30.000 >>
//														ENDIF
//														syncId = CREATE_SYNCHRONIZED_SCENE(scenePositionExit, sceneRotationExit)
//														TASK_SYNCHRONIZED_SCENE(pedShopper[i], syncId, "amb@prop_human_seat_chair@female@proper@react_flee", "exit_fwd", FAST_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_BLOCK_MOVER_UPDATE)
//														SET_ENTITY_COORDS(pedShopper[i], GET_ENTITY_COORDS(pedShopper[i]) - << 0, 0, 0.4754 >>)
//														TASK_PLAY_ANIM(pedShopper[i], "amb@prop_human_seat_chair@female@proper@react_flee", "exit_fwd", FAST_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION|AF_OVERRIDE_PHYSICS)
//													ELSE
														OPEN_SEQUENCE_TASK(seq)
															IF thisEvent = HAIRDRESSERS_HOLDUP
																TASK_PLAY_ANIM(NULL, "random@robbery", "exit_flee", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION|AF_OVERRIDE_PHYSICS|AF_EXTRACT_INITIAL_OFFSET, 0, FALSE, AIK_DISABLE_LEG_IK)
															ELIF thisEvent = CLOTHES_SHOP_HOLDUP
																TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -1201.4758, -790.2733, 15.5475 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
															ENDIF
															TASK_SMART_FLEE_COORD(NULL, vFleeCoords, 150, -1)
														CLOSE_SEQUENCE_TASK(seq)
														TASK_PERFORM_SEQUENCE(pedShopper[i], seq)
														CLEAR_SEQUENCE_TASK(seq)
//													ENDIF
													SET_PED_KEEP_TASK(pedShopper[i], TRUE)
													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], FALSE)
													SET_PED_CAN_BE_TARGETTED(pedShopper[i], TRUE)
													SET_PED_AS_NO_LONGER_NEEDED(pedShopper[i])
												ENDIF
											ENDFOR
										ENDIF
//										makePedsFleeWhenRobbersAreAway()
										
										// force player up on to feet once pedRobber[0] is no longer in the interior
										IF HAVE_ROBBERS_LEFT_INTERIOR()
										OR TIMERA() > 20000
											IF NOT bPlayerGotUp
												IF NOT IS_ENTITY_DEAD(pedRobber[0])
													SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
													DISABLE_CELLPHONE(FALSE)
													DISPLAY_HUD(TRUE)
													DISPLAY_RADAR(TRUE)
													SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//													sceneOutId = CREATE_SYNCHRONIZED_SCENE(vSyncRobberyPosition, vSyncRobberyRotation)
													TASK_PLAY_ANIM(PLAYER_PED_ID(), "random@shop_robbery", "kneel_getup_p", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, 2700)
													
													IF thisEvent = HAIRDRESSERS_HOLDUP
														IF DOES_ENTITY_EXIST(objIdBag)
//															DELETE_OBJECT(objIdBag)
															SET_ENTITY_VISIBLE(objIdBag, FALSE)
															SET_ENTITY_COLLISION(objIdBag, FALSE)
														ENDIF
													ELIF thisEvent = CLOTHES_SHOP_HOLDUP
//														IF DOES_BLIP_EXIST(blipRobber[1])
//															REMOVE_BLIP(blipRobber[1])
//														ENDIF
													ENDIF
													
													REPEAT COUNT_OF(pedWorker) index
														IF NOT IS_PED_INJURED(pedWorker[index])
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[index], FALSE)
														ENDIF
													ENDREPEAT
													REPEAT COUNT_OF(pedShopper) index
														IF NOT IS_PED_INJURED(pedShopper[index])
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[index], FALSE)
														ENDIF
													ENDREPEAT
													IF NOT IS_PED_INJURED(pedWorker[0])
														SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[0], TRUE)
														OPEN_SEQUENCE_TASK(seq)
//															TASK_TURN_PED_TO_FACE_COORD(NULL, GET_ENTITY_COORDS(pedWorker[0]) + << 1, 1, 0 >>)
															TASK_PLAY_ANIM(NULL, "random@robbery", "f_distressed_loop",/*"amb@robberystand@", "stand_worried_female",*/ REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
														CLOSE_SEQUENCE_TASK(seq)
														TASK_PERFORM_SEQUENCE(pedWorker[0], seq)
														CLEAR_SEQUENCE_TASK(seq)
														SET_PED_KEEP_TASK(pedWorker[0], TRUE)
													ENDIF
													
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CLAWAY", CONV_PRIORITY_AMBIENT_HIGH)//REROB_GO
													ENDIF
													bPlayerGotUp = TRUE
												ENDIF
											ENDIF
										ENDIF
										
										IF NOT bBreakingOut
											IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "random@shop_robbery", "kneel_getup_p")
												IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@shop_robbery", "kneel_getup_p") > 0.7
												OR (GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "random@shop_robbery", "kneel_getup_p") > 0.6 AND IS_PLAYER_MOVING_LEFT_STICK())
													CLEAR_PED_TASKS(PLAYER_PED_ID())
//													IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), CurrentWeapon)
//														SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentWeapon, TRUE)
//													ENDIF
													bBreakingOut = TRUE
												ENDIF
											ENDIF
										ENDIF
										
										IF thisEvent = HAIRDRESSERS_HOLDUP
											// give peds in salon tasks if the player has left the interior
											IF NOT IS_PLAYER_IN_ROBBERY_INTERIOR()
												IF NOT bPedsGivenApreRobberyTasks
												//	givePedsSomeApreRobberyTasks()
													bPedsGivenApreRobberyTasks = TRUE
												ENDIF
											ENDIF
											
											// if the robber has been injured before making it to the vehicle with the bag
											// delete bag object, create bag pickup
											// then go through to stage to pick it up
											IF NOT bCreateBagAtVehicle
												getRidOfBagObjectFromInjuredPed()
											ENDIF
											
											// remove bag object if the robber has made it in to the vehicle
											IF NOT IS_PED_INJURED(pedRobber[0])
											AND	IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
												IF IS_PED_IN_VEHICLE(pedRobber[0], vehGetawayVeh)
													IF DOES_ENTITY_EXIST(objIdBag)
														DELETE_OBJECT(objIdBag)
														bCreateBagAtVehicle = TRUE
													ENDIF
												ENDIF
											ENDIF
											
											// control robbers making a run for it in the getaway car
											IF ARE_ALL_ROBBERS_IN_CAR()
											AND NOT bFleeInVeh
												IF DOES_BLIP_EXIST(blipRobber[0])
													REMOVE_BLIP(blipRobber[0])
												ENDIF
												IF DOES_BLIP_EXIST(blipRobber[1])
													REMOVE_BLIP(blipRobber[1])
												ENDIF
												IF NOT DOES_BLIP_EXIST(blipGetawayVeh)
													blipGetawayVeh = CREATE_BLIP_FOR_VEHICLE(vehGetawayVeh, TRUE)
												ENDIF
												bCreateBagAtVehicle = TRUE
												IF NOT IS_PED_INJURED(pedDriver)
													SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_ALWAYS_FLEE, TRUE) 
													SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
													SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_USE_VEHICLE, TRUE)
													TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehGetawayVeh, PLAYER_PED_ID(), MISSION_FLEE, 16, DRIVINGMODE_AVOIDCARS, -1, -1)
													SET_PED_KEEP_TASK(pedDriver, TRUE)
													bFleeInVeh = TRUE
												ENDIF
											ENDIF
											IF bFleeInVeh
											AND NOT bSpeedUp
												IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
												AND NOT IS_PED_INJURED(pedDriver)
													IF NOT IS_ENTITY_AT_COORD(vehGetawayVeh, vRobberInitialPos[0], << 20, 20, 10 >>)
														TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehGetawayVeh, PLAYER_PED_ID(), MISSION_FLEE, 19, DRIVINGMODE_AVOIDCARS, -1, -1)
														bSpeedUp = TRUE
													ENDIF
												ENDIF
											ENDIF
											
											IF HAS_PLAYER_DAMAGED_ROBBERS_OR_CAR()
												// driver makes a getaway regardless of who is in the vehicle
												IF NOT IS_PED_INJURED(pedDriver)
													IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
														IF NOT bGetAwayDriverTask
															SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_ALWAYS_FLEE, TRUE)
															SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
															SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_USE_VEHICLE, TRUE)
															TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehGetawayVeh, PLAYER_PED_ID(), MISSION_FLEE, 17, DRIVINGMODE_AVOIDCARS, -1, -1)
															bGetAwayDriverTask = TRUE
														ENDIF
														IF NOT IS_PED_INJURED(pedRobber[0])
															IF IS_PED_IN_VEHICLE(pedRobber[0], vehGetawayVeh)
																IF DOES_BLIP_EXIST(blipRobber[0])
																	REMOVE_BLIP(blipRobber[0])
																ENDIF
																IF DOES_BLIP_EXIST(blipRobber[1])
																	REMOVE_BLIP(blipRobber[1])
																ENDIF
																IF NOT DOES_BLIP_EXIST(blipGetawayVeh)
																	blipGetawayVeh = CREATE_BLIP_FOR_VEHICLE(vehGetawayVeh, TRUE)
																ENDIF
																bCreateBagAtVehicle = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
												// make peds attack player if in a vehicle and attack if not
												INT i
												FOR i = 0 to (iNoOfRobbers - 1)
													IF NOT bReactedToPlayerAttack[i]
														IF NOT IS_PED_INJURED(pedRobber[i])
															IF IS_PED_IN_ANY_VEHICLE(pedRobber[i])
															AND NOT IS_PED_INJURED(pedDriver)
																TASK_DRIVE_BY(pedRobber[i], PLAYER_PED_ID(), NULL, << 0.3, 0.7, 1.9 >>, -1, 15)
																bReactedToPlayerAttack[i] = TRUE
															ELIF NOT DOES_BLIP_EXIST(blipBag)
																TASK_COMBAT_PED(pedRobber[i], PLAYER_PED_ID())
																SET_PED_KEEP_TASK(pedRobber[i], TRUE)
																IF DOES_BLIP_EXIST(blipGetawayVeh)
																	REMOVE_BLIP(blipGetawayVeh)
																ENDIF
																IF NOT DOES_BLIP_EXIST(blipRobber[i])
																	blipRobber[i] = CREATE_BLIP_FOR_PED(pedRobber[i], TRUE)
																ENDIF
																bReactedToPlayerAttack[i] = TRUE
															ENDIF
														ENDIF
													ENDIF
													// make peds flee player if driver is dead in vehicle
													IF NOT bReactedToDriverGone[i]
														IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
															IF NOT IS_PED_INJURED(pedRobber[i])
																IF IS_PED_INJURED(pedDriver)
																OR IS_VEHICLE_SEAT_FREE(vehGetawayVeh, VS_DRIVER)
																OR IS_ENTITY_ON_FIRE(pedRobber[i])
																	SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[i], TRUE)
																	OPEN_SEQUENCE_TASK(seq)
																		TASK_LEAVE_ANY_VEHICLE(NULL)
																		TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
																	CLOSE_SEQUENCE_TASK(seq)
																	TASK_PERFORM_SEQUENCE(pedRobber[i], seq)
																	CLEAR_SEQUENCE_TASK(seq)
																	SET_PED_KEEP_TASK(pedRobber[i], TRUE)
																	IF NOT IS_PED_INJURED(pedDriver)
																		SET_ENTITY_LOAD_COLLISION_FLAG(pedDriver, TRUE)
																		OPEN_SEQUENCE_TASK(seq)
																			TASK_LEAVE_ANY_VEHICLE(NULL)
																			TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
																		CLOSE_SEQUENCE_TASK(seq)
																		TASK_PERFORM_SEQUENCE(pedDriver, seq)
																		CLEAR_SEQUENCE_TASK(seq)
																		SET_PED_KEEP_TASK(pedDriver, TRUE)
																	ENDIF
																	IF NOT DOES_BLIP_EXIST(blipBag)
																		IF DOES_BLIP_EXIST(blipGetawayVeh)
																			REMOVE_BLIP(blipGetawayVeh)
																		ENDIF
																		IF NOT DOES_BLIP_EXIST(blipRobber[i])
																			blipRobber[i] = CREATE_BLIP_FOR_PED(pedRobber[i], TRUE)
																		ENDIF
																	ENDIF
																	bReactedToDriverGone[i] = TRUE
																ENDIF
															ENDIF
														ELSE
															IF NOT IS_PED_INJURED(pedRobber[i])
																SET_ENTITY_LOAD_COLLISION_FLAG(pedRobber[i], TRUE)
																OPEN_SEQUENCE_TASK(seq)
																	TASK_LEAVE_ANY_VEHICLE(NULL)
																	TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedRobber[i], seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedRobber[i], TRUE)
															ENDIF
															IF NOT IS_PED_INJURED(pedDriver)
																SET_ENTITY_LOAD_COLLISION_FLAG(pedDriver, TRUE)
																OPEN_SEQUENCE_TASK(seq)
																	TASK_LEAVE_ANY_VEHICLE(NULL)
																	TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
																CLOSE_SEQUENCE_TASK(seq)
																TASK_PERFORM_SEQUENCE(pedDriver, seq)
																CLEAR_SEQUENCE_TASK(seq)
																SET_PED_KEEP_TASK(pedDriver, TRUE)
															ENDIF
															IF NOT DOES_BLIP_EXIST(blipBag)
																IF DOES_BLIP_EXIST(blipGetawayVeh)
																	REMOVE_BLIP(blipGetawayVeh)
																ENDIF
																IF NOT DOES_BLIP_EXIST(blipRobber[i])
																	blipRobber[i] = CREATE_BLIP_FOR_PED(pedRobber[i], TRUE)
																ENDIF
																bReactedToDriverGone[i] = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDFOR
											ENDIF
											IF bCreateBagAtVehicle
												IF ARE_WE_READY_TO_CREATE_BAG_PICKUP()
													removeAllBlips()
													createStolenBagPickup()
													RunRobberyStage = STOLEN_STUFF_DROPPED
												ENDIF
											ENDIF
											
										ELIF thisEvent = CLOTHES_SHOP_HOLDUP
											IF IS_PED_INJURED(pedRobber[0])
												IF DOES_BLIP_EXIST(blipRobber[0])
													REMOVE_PED_FOR_DIALOGUE(RobberyDialogueStruct, 3)
													REMOVE_BLIP(blipRobber[0])
												ENDIF
											ENDIF
											IF IS_PED_INJURED(pedRobber[1])
												IF DOES_BLIP_EXIST(blipRobber[1])
													REMOVE_PED_FOR_DIALOGUE(RobberyDialogueStruct, 4)
													REMOVE_BLIP(blipRobber[1])
												ENDIF
											ENDIF
											IF IS_PED_INJURED(pedRobber[0])
												IF DOES_ENTITY_EXIST(objIdBag)
													IF IS_ENTITY_ATTACHED(objIdBag)
														DETACH_ENTITY(objIdBag)
													ENDIF
													DELETE_OBJECT(objIdBag)
													removeAllBlips()
													createStolenBagPickup()
													RunRobberyStage = STOLEN_STUFF_DROPPED
												ENDIF
											ELSE
												IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedRobber[0], PLAYER_PED_ID())
													IF DOES_ENTITY_EXIST(objIdBag)
														IF IS_ENTITY_ATTACHED(objIdBag)
															DETACH_ENTITY(objIdBag)
														ENDIF
														DELETE_OBJECT(objIdBag)
														removeAllBlips()
														createStolenBagPickup()
														RunRobberyStage = STOLEN_STUFF_DROPPED
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									BREAK
									
									CASE ROBBERS_ATTACKED
										robberyCommences()
									BREAK
									
									CASE STOLEN_STUFF_DROPPED
										removeDeadPedBlips()
										bagPickUp()
									BREAK
									
									CASE PICKED_UP_STOLEN_STUFF
										PRINT_LIMITED_RANDOM_EVENT_HELP(REHLP_RETURN_ITEM)
										FLASH_RANDOM_EVENT_RETURN_ITEM_BLIP(blipShopWorker, iBlipTimer)
										
										IF thisEvent = HAIRDRESSERS_HOLDUP
											IF NOT bHTGoods
												REQUEST_ANIM_DICT("random@robbery")
												IF NOT HAS_ANIM_DICT_LOADED("random@robbery")
													REQUEST_ANIM_DICT("random@robbery")
												ELSE
													IF NOT IS_PED_INJURED(pedWorker[0])
														SET_ENTITY_COORDS(pedWorker[0], << -818.5553, -185.4815, 36.5689 >>)//<< -818.7359, -185.6700, 36.5689 >>)
														SET_ENTITY_HEADING(pedWorker[0], 47.0582)//35.0262)
														TASK_PLAY_ANIM(pedWorker[0], "random@robbery", "stand_worried_female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
														IF NOT IS_PED_INJURED(pedWorker[1])
															TASK_SMART_FLEE_COORD(pedWorker[1], vFleeCoords, 150, -1)
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[1], FALSE)
															SET_PED_AS_NO_LONGER_NEEDED(pedWorker[1])
														ENDIF
														IF NOT IS_PED_INJURED(pedWorker[2])
															TASK_SMART_FLEE_COORD(pedWorker[2], vFleeCoords, 150, -1)
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[2], FALSE)
															SET_PED_AS_NO_LONGER_NEEDED(pedWorker[2])
														ENDIF
														
														INT i
//														INT syncId
														FOR i = 0 to (iNoOfShoppers - 1)
															IF NOT IS_PED_INJURED(pedShopper[i])
//																IF i = 0 OR i = 2
//																	IF i = 0
//																		scenePositionExit = << -812.370, -185.650, 37.105 >>
//																		sceneRotationExit = << 0.000, 0.000, 37.680 >>
//																	ELIF i = 2
//																		scenePositionExit = << -816.224, -182.897, 37.180 >>
//																		sceneRotationExit = << 0.000, 0.000, 30.000 >>
//																	ENDIF
//																	syncId = CREATE_SYNCHRONIZED_SCENE(scenePositionExit, sceneRotationExit)
//																	TASK_SYNCHRONIZED_SCENE(pedShopper[i], syncId, "amb@prop_human_seat_chair@female@proper@react_flee", "exit_fwd", FAST_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_BLOCK_MOVER_UPDATE)
//																	SET_ENTITY_COORDS(pedShopper[i], GET_ENTITY_COORDS(pedShopper[i]) - << 0, 0, 0.4754 >>)
//																	TASK_PLAY_ANIM(pedShopper[i], "amb@prop_human_seat_chair@female@proper@react_flee", "exit_fwd", FAST_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION|AF_OVERRIDE_PHYSICS)
//																ELSE
																	OPEN_SEQUENCE_TASK(seq)
																		TASK_PLAY_ANIM(NULL, "random@robbery", "exit_flee", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_TURN_OFF_COLLISION|AF_OVERRIDE_PHYSICS|AF_EXTRACT_INITIAL_OFFSET, 0, FALSE, AIK_DISABLE_LEG_IK)
																		TASK_SMART_FLEE_COORD(NULL, vFleeCoords, 150, -1)
																	CLOSE_SEQUENCE_TASK(seq)
																	TASK_PERFORM_SEQUENCE(pedShopper[i], seq)
																	CLEAR_SEQUENCE_TASK(seq)
//																ENDIF
																SET_PED_KEEP_TASK(pedShopper[i], TRUE)
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], FALSE)
																SET_PED_CAN_BE_TARGETTED(pedShopper[i], TRUE)
																SET_PED_AS_NO_LONGER_NEEDED(pedShopper[i])
															ENDIF
														ENDFOR
														bHTGoods = TRUE
													ENDIF
												ENDIF
											ENDIF
											SET_DOOR_STATE(DOORNAME_HAIR_SALON_L, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
											SET_DOOR_STATE(DOORNAME_HAIR_SALON_R, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
											
										ELIF thisEvent = CLOTHES_SHOP_HOLDUP
											IF NOT bHTGoods
												REQUEST_ANIM_DICT("random@robbery")
												PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@ REQUEST_ANIM_DICT(random@robbery) @@@@@@@@@@@@@@@@@@@\n")
//												REQUEST_ANIM_DICT("amb@robberywalk@")
												IF NOT HAS_ANIM_DICT_LOADED("random@robbery")
//												AND NOT HAS_ANIM_DICT_LOADED("amb@robberywalk@")
													REQUEST_ANIM_DICT("random@robbery")
//													REQUEST_ANIM_DICT("amb@robberywalk@")
												ELSE
													IF NOT IS_PED_INJURED(pedWorker[0])
														SET_ENTITY_COORDS(pedWorker[0], << -1197.4552, -776.0289, 16.3254>>)
														SET_ENTITY_HEADING(pedWorker[0], 205.7753)
														TASK_PLAY_ANIM(pedWorker[0], "random@robbery", "stand_worried_female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
//														TASK_PLAY_ANIM(pedWorker[0], "amb@robberywalk@", "walk_worried_female", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
														IF NOT IS_PED_INJURED(pedWorker[1])
															TASK_SMART_FLEE_COORD(pedWorker[1], vFleeCoords, 150, -1)
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[1], FALSE)
															SET_PED_AS_NO_LONGER_NEEDED(pedWorker[1])
														ENDIF
														IF NOT IS_PED_INJURED(pedWorker[2])
															TASK_SMART_FLEE_COORD(pedWorker[2], vFleeCoords, 150, -1)
															SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[2], FALSE)
															SET_PED_AS_NO_LONGER_NEEDED(pedWorker[2])
														ENDIF
														
														INT i
														FOR i = 0 to (iNoOfShoppers - 1)
															IF NOT IS_PED_INJURED(pedShopper[i])
																TASK_SMART_FLEE_COORD(pedShopper[i], vFleeCoords, 150, -1)
																SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedShopper[i], FALSE)
																SET_PED_CAN_BE_TARGETTED(pedShopper[i], TRUE)
																SET_PED_AS_NO_LONGER_NEEDED(pedShopper[i])
															ENDIF
														ENDFOR
														bHTGoods = TRUE
													ENDIF
												ENDIF
											ENDIF
											IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< -1201.43, -776.86, 17.99 >>, 6.0, V_ILEV_CLOTHMIDDOOR)
												SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_CLOTHMIDDOOR, << -1201.43, -776.86, 17.99 >>, FALSE, 0.0)
											ENDIF
										ENDIF
										IF IS_PLAYER_OUT_FRONT()
										ENDIF
										IF IS_PLAYER_IN_ROBBERY_INTERIOR()
										AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
											SETTIMERA(0)
											RunRobberyStage = BACK_IN_SHOP
										ENDIF
										// check for player leaving with bag - i.e. not returning to shop
										IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), getWorldPoint()) > (fStoreBagCollectDist+150)
											RunRobberyStage = LEFT_THE_SCENE
										ENDIF
										IF TIMERA() > 120000
											RunRobberyStage = RUN_OUT_OF_TIME
										ENDIF
										IF IS_PED_INJURED(pedWorker[0])
											SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
											MISSION_PASSED()
										ELSE
											IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedWorker[0], PLAYER_PED_ID())
												SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
												MISSION_PASSED()
											ENDIF
										ENDIF
									BREAK
									
									CASE BACK_IN_SHOP
										givingBagBackScene()
									BREAK
									
									CASE LEFT_THE_SCENE
//										CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 12000,FALSE,TRUE)
										SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
										MISSION_PASSED()
									BREAK
									
									CASE PREVENTED_GETAWAY
//										givePedsSomeApreRobberyTasks()
										MISSION_PASSED()
									BREAK
									
									CASE RUN_OUT_OF_TIME
//										CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, 12000,FALSE,TRUE)
										SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_KEEP_ITEM)
										MISSION_PASSED()
									BREAK
									
									CASE END_SCRIPT_INSIDE_INTERIOR
										PRINTSTRING("/n @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ END_SCRIPT_INSIDE_INTERIOR @@@@@@@@@@@@@@@@@@@@@@@@@@@@@ /n")
										IF NOT bCalledCops
											IF NOT IS_PED_INJURED(pedWorker[0])
												CLEAR_PED_TASKS(pedWorker[0])
												IF thisEvent = HAIRDRESSERS_HOLDUP
													CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_LSTH", CONV_PRIORITY_AMBIENT_HIGH)
												ELIF thisEvent = CLOTHES_SHOP_HOLDUP
													CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_CLLOST", CONV_PRIORITY_AMBIENT_HIGH)//REROB_LSTB
												ENDIF
//												IF CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_LST", CONV_PRIORITY_AMBIENT_HIGH)
					 								TASK_USE_MOBILE_PHONE_TIMED(pedWorker[0], 20000)
													SET_PED_KEEP_TASK(pedWorker[0],TRUE)
													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWorker[0], FALSE)
													bCalledCops = TRUE
//												ENDIF
											ENDIF
										ENDIF
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											RobberyStage = STAGE_CLEAN_UP
										ENDIF
									BREAK
									
									// player can no longer break out of anim - is forced into anim
//									CASE STANDOFF_SITUATION
//										simpleRobberyStandOff()
//									BREAK
//									CASE LEFT_AREA
										// if the player has immediately exited the interior after entering
//										IF NOT IS_PLAYER_IN_ROBBERY_INTERIOR()
//											IF TIMERA() > 7500
//												FOR i = 0 to 1
//													IF NOT IS_ENTITY_DEAD(pedRobber[i])
//														SET_ENTITY_HEALTH(pedRobber[i], 0)
//													ENDIF
//												ENDFOR
//												FOR i = 0 to 2
//													IF NOT IS_ENTITY_DEAD(pedWorker[i])
//														SET_ENTITY_HEALTH(pedWorker[i], 0)
//													ENDIF
//												ENDFOR
//												missionCleanup()
//											ENDIF
//										ELSE
//											RunRobberyStage = IGNORED_INSTRUCTION
//										ENDIF
//									BREAK
									
//									CASE IGNORED_INSTRUCTION
//										IF NOT bPhSpeech4
//											CLEAR_HELP()
//											FOR i = 0 TO 1
//												IF NOT IS_PED_INJURED(pedRobber[i])
//													TASK_COMBAT_PED(pedRobber[i], PLAYER_PED_ID())
//												ENDIF
//											ENDFOR
//											bPhSpeech4 = TRUE
//										ENDIF
//									BREAK
									
//									CASE ARMED_AND_SHOOTING
//										IF NOT bPhSpeech7
//											FOR i = 0 TO 1
//												IF NOT IS_PED_INJURED(pedRobber[i])
//													TASK_COMBAT_PED(pedRobber[i], PLAYER_PED_ID())
//												ENDIF
//											ENDFOR
//											bPhSpeech7 = TRUE
//										ENDIF
//									BREAK
								ENDSWITCH
							BREAK
							
							CASE STAGE_CLEAN_UP
								MISSION_FAILED()
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			ENDIF
//		ELSE
//			missionCleanup()
//		ENDIF
	ELSE
		missionCleanup()
	ENDIF
	
//	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
//		MISSION_FAILED()
//	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
		OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			MISSION_PASSED()
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()
		ENDIF
	#ENDIF
	
ENDWHILE

ENDSCRIPT

//PROC PlayerRetrievingBag()
//
//	IF IS_PLAYER_ACTIVATING_OBJECT(objIdBag, ECOMPASS_SOUTH, 0.4,  FALSE, EACTIVESIDE_FOURCORNERS, EACTIVATIONEASE_EASY)
//		IF NOT bDuckingTimerSet
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			// single angle camera cut
//			CamIDHandingOverBag = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//			ATTACH_CAM_TO_PED_BONE(CamIDHandingOverBag, PLAYER_PED_ID(), BONETAG_HEAD, <<0.4, 0.45, 0.3>>)
//			POINT_CAM_AT_PED_BONE(CamIDHandingOverBag, PLAYER_PED_ID(), BONETAG_HEAD, <<0, 0, 0.2>>)
//			RENDER_SCRIPT_CAMS(TRUE, TRUE, 0)
//			// ----------------------------------
//			
//			IF DOES_ENTITY_EXIST(objIdBag)
//				OPEN_SEQUENCE_TASK(seq)
//					TASK_LOOK_AT_ENTITY(NULL, objIdBag,  2000)
//					TASK_DUCK(NULL, 1000)
//				CLOSE_SEQUENCE_TASK(seq)
//				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
//				CLEAR_SEQUENCE_TASK(Seq)
//				
//				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
//			ENDIF
//			
//			SETTIMERA(0)
//			bDuckingTimerSet = TRUE
//		ENDIF
//	ENDIF
//	
//	IF bDuckingTimerSet 
//		IF TIMERA() > 2000
//			ATTACH_ENTITY_TO_ENTITY(objIdBag, PLAYER_PED_ID(), BONETAG_R_HAND, << 0.400, -0.100, -0.000 >>, << -94.000, -161.000, 69.000 >>, FALSE)
//			SETTIMERA(0)
//			IF DOES_BLIP_EXIST(blipBag)
//				REMOVE_BLIP(blipBag)
//			ENDIF
//			blipShopWorker = ADD_BLIP_FOR_COORD(vLocReturnBag)
//			SET_BLIP_SCALE(blipShopWorker, RE_BLIP_SCALE)
//			
//			//SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_CLEAR_TASKS)
//			
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			SET_CAM_ACTIVE(CamIDingUpBag, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			DESTROY_CAM(CamIDHandingOverBag)
//			
//			TASK_PLAY_ANIM(PLAYER_PED_ID(), "gun@handgun", "holster")
//			RunRobberyStage = PICKED_UP_STOLEN_STUFF
//		ENDIF
//	ENDIF
//ENDPROC

//	IF IS_PLAYER_ACTIVATING_OBJECT(objIdBag, ECOMPASS_SOUTH, 0.5,  FALSE, EACTIVESIDE_FOURCORNERS, EACTIVATIONEASE_EASY)
//	//	bPlayerGettingBag = TRUE
//		IF NOT bDuckingTimerSet
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			SET_ENTITY_DYNAMIC(objIdBag, TRUE)
//			
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//			
//			scenePosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//			scenePosition = <<scenePosition.x, scenePosition.y, scenePosition.z-0.9>>
//			sceneRotation = << 0.000, 0.000, 0.000 >>
//			
//			//===========================
//			// for later check: leave scene
//			fStoreBagCollectDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), getWorldPoint())
//			//===========================
//				
//			STRING pickUpAnim
//			STRING pickUpCam
//			STRING pickUpBagAnim
//			
//			pickUpAnim = "Pick_Up_Briefcase_Player"
//			pickUpCam = "Pick_Up_Briefcase_Cam"
//			pickUpBagAnim = "PICK_UP_HANDBAG_BAG"
//			
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			CamIDHandingOverBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//			PLAY_SYNCHRONIZED_CAM_ANIM(CamIDHandingOverBag, sceneId, pickUpCam, "amb@robbery")
//			
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "amb@robbery", pickUpAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, pickUpBagAnim, "amb@robbery", INSTANT_BLEND_IN)
//			
//			WAIT(0)
//			
//			//ATTACH_ENTITY_TO_ENTITY(objIdBag, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
//			
//			SET_CAM_ACTIVE(CamIDHandingOverBag, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			SETTIMERA(0)
//			bDuckingTimerSet = TRUE
//		ENDIF
//	ENDIF
//	
//	IF bDuckingTimerSet 
//		IF TIMERA() > 3500
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//			SET_CAM_ACTIVE(CamIDHandingOverBag, FALSE)
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			DESTROY_CAM(CamIDHandingOverBag)
//			
//			IF DOES_BLIP_EXIST(blipBag)
//				REMOVE_BLIP(blipBag)
//			ENDIF
//			blipShopWorker = CREATE_BLIP_FOR_COORD(vLocReturnBag, TRUE)
//			
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			TASK_PLAY_ANIM(PLAYER_PED_ID(), "gun@handgun", "holster")
//			RunRobberyStage = PICKED_UP_STOLEN_STUFF
//		ENDIF
//	ENDIF
	
//	IF bReadyToGiveBagBack
//		IF NOT bStartedHandoverSyncScene
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
//			
//			IF DOES_BLIP_EXIST(blipShopWorker)
//				REMOVE_BLIP(blipShopWorker)
//			ENDIF
//			
//			scenePosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
//			scenePosition = <<scenePosition.x, scenePosition.y, scenePosition.z-0.9>>
//			sceneRotation = << 0.000, 0.000, 0.000 >>
//			
//			return_bag_player = "Return_Handbag_Player"
//			return_bag_female = "Return_Handbag_Ped"
//			return_bag_cam = "Return_Handbag_Cam"
//			
////			return_bag_player = "Return_Handbag_Player"
////			return_bag_female = "Return_Handbag_Ped"
////			return_bag_cam = "Return_Handbag_Cam"
//				
//			sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
//			CamIDHandingOverBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//			PLAY_SYNCHRONIZED_CAM_ANIM(CamIDHandingOverBag, sceneId, return_bag_cam, "amb@robbery")
//			
//			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "amb@robbery", return_bag_player, INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//			IF NOT IS_PED_INJURED(pedWorker[0])
//				CLEAR_PED_TASKS_IMMEDIATELY(pedWorker[0])
//				TASK_SYNCHRONIZED_SCENE(pedWorker[0], sceneId, "amb@robbery", return_bag_female, INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedWorker[0])
//			ENDIF
//			
//			//recreate bag
//			objIdBag = CREATE_OBJECT(Prop_CS_Duffel_01, GET_ENTITY_COORDS(PLAYER_PED_ID()))
//			PLAY_SYNCHRONIZED_ENTITY_ANIM(objIdBag, sceneId, "RETURN_HANDBAG_BAG",  "amb@robbery", INSTANT_BLEND_IN)
//			
//			IF NOT IS_PED_INJURED(pedWorker[0])
//				IF NOT bPhSpeech6
//					CREATE_CONVERSATION(RobberyDialogueStruct, "REROBAU", "REROB_TK", CONV_PRIORITY_AMBIENT_HIGH)
//					bPhSpeech6 = TRUE
//				ENDIF
//			ENDIF
//			
//			SET_CAM_ACTIVE(CamIDHandingOverBag, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			SETTIMERA(0)
//			
//			bStartedHandoverSyncScene = TRUE
//		ENDIF
//	ENDIF
//	
//	IF TIMERA() > 4500
//	AND  bStartedHandoverSyncScene
//		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//		
//		IF DOES_ENTITY_EXIST(objIdBag)
//			DELETE_OBJECT(objIdBag)
//		ENDIF
//		
//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, SPC_CLEAR_TASKS)
//		
//		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
//		
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
//		
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		DESTROY_CAM(CamIDHandingBag)
//		givePedsSomeApreRobberyTasks()
//		
//		PRINTSTRING("re_shoprobbery - took bag back to shop")
//		PRINTNL()
//		
//		missionCleanup()
//	ENDIF
	
//									CASE DRIVING_AWAY
//										vehicleChaseReduceHealth()
//										removeDeadPedBlips()
//										loseRobbersCheck()
//										
//										IF thisEvent = HAIRDRESSERS_HOLDUP
//											IF NOT bSpeedUp
//												IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
//												AND NOT IS_PED_INJURED(pedDriver)
//													IF NOT IS_ENTITY_AT_COORD(vehGetawayVeh,vRobberInitialPos[0], <<20, 20, 10>> )
//														TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehGetawayVeh, PLAYER_PED_ID(), MISSION_FLEE, 19, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
//														bSpeedUp = TRUE
//													ENDIF
//												ENDIF
//											ENDIF
//											IF HAS_PLAYER_DAMAGED_ROBBERS_OR_CAR()
//												IF NOT IS_PED_INJURED(pedDriver)
//													IF IS_VEHICLE_DRIVEABLE(vehGetawayVeh)
//														SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_ALWAYS_FLEE, TRUE)
//														SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_FLEE_WHILST_IN_VEHICLE, TRUE)
//														SET_PED_COMBAT_ATTRIBUTES(pedDriver, CA_USE_VEHICLE, TRUE)
//														TASK_VEHICLE_MISSION_PED_TARGET(pedDriver, vehGetawayVeh, PLAYER_PED_ID(), MISSION_FLEE, 16, DRIVINGMODE_PLOUGHTHROUGH, -1, -1)
//													ENDIF
//												ENDIF
//												// make peds attack player if in a vehicle and flee if not
//												FOR i = 0 to (iNoOfRobbers - 1)
//													IF NOT IS_PED_INJURED(pedRobber[i])
//														IF IS_PED_IN_ANY_VEHICLE(pedRobber[i])
//															TASK_DRIVE_BY(pedRobber[i], PLAYER_PED_ID(), NULL, <<0.3, 0.7, 1.9>>, 25, 15)
//														ELSE
//															TASK_SMART_FLEE_PED(pedRobber[i], PLAYER_PED_ID(), 250, -1, TRUE)
//														ENDIF
//													ENDIF
//												ENDFOR
//												RunRobberyStage = HARMED_ROBBERS
//											ENDIF
//										ENDIF
//										
//									BREAK
