

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// ************************************************************************
// SCRIPT:		re_LosSantosIntl.sc
// SCRIPTER: 	David Stinchcomb
// SUMMARY:		The ambient script for the Los Santos International Airport
//				in the SE of the map.
// ************************************************************************
USING "re_LosSantosIntl.sch"
USING "LosSantos_Airplane_lib.sch"
USING "LosSantos_Vehicle_lib.sch"

// **************************** SHARED VARIABLES ****************************
PED_INDEX 				pedPlayer

// *************************** SUBSYSTEM INCLUDES ***************************


// ***************************** CORE VARIABLES *****************************
LOSSANTOS_CORE_STATE 	eCoreState = LCS_INIT
INT 					iWaitTime = 500
// **************************************************************************


/// PURPOSE:
///    Cleans all assets in the airport script. Should invoke individual system cleanups.
PROC CLEANUP_SCRIPT()
	PRINTLN("*********************** Terminating Script: re_LosSantosIntl.sc ***********************")
	
	LOSSANTOS_AIRPLANE_CLEANUP()
	LOSSANTOS_VEHICLE_CLEANUP()
	
	TERMINATE_THIS_THREAD()
ENDPROC


// Main script update.
SCRIPT
	PRINTLN("*********************** scriptLaunch: re_LosSantosIntl.sc ***********************")
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTLN("********** re_LosSantosIntl.sc FORCE CLEANUP **********")		
		CLEANUP_SCRIPT()
	ENDIF
	
	// Grab the player once. This is for all systems to share.
	pedPlayer = PLAYER_PED_ID()
	
	WHILE TRUE
		// Check to see if we're quitting due to death. This also prevents all subsystems from having to check this.
		IF IS_ENTITY_DEAD(pedPlayer)
			PRINTLN("re_ArmyBase: 	Player is dead! Terminating.")
			eCoreState = LCS_CLEAN
		ENDIF

		// State-based update.
		SWITCH (eCoreState)
			CASE LCS_INIT			
				PRINTLN("INSIDE STATE - LCS_INIT")
				
				eCoreState = LCS_REQUEST_COMMON
			BREAK
			
			CASE LCS_REQUEST_COMMON
				PRINTLN("INSIDE STATE - LCS_REQUEST_COMMON")
				
				// Make our common requests.
				LOSSANTOS_MAKE_COMMON_REQUESTS()
				
				eCoreState = LCS_STREAM_COMMON
			BREAK
			
			CASE LCS_STREAM_COMMON
				PRINTLN("INSIDE STATE - LCS_STREAM_COMMON")
			
				// Make sure common requests have streamed.
				IF LOSSANTOS_ARE_COMMON_REQUESTS_LOADED()
					eCoreState = LCS_SPAWN_COMMON
				ENDIF
			BREAK
			
			CASE LCS_SPAWN_COMMON
				PRINTLN("INSIDE STATE - LCS_SPAWN_COMMON")
				
				// Spawn any common items. (shouldn't be many)
				LOSSANTOS_SPAWN_COMMON()
				
				eCoreState = LCS_UPDATE
			BREAK
			
			CASE LCS_UPDATE
//				PRINTLN("INSIDE STATE - LCS_UPDATE")

				IF g_bTriggerSceneActive
//					PRINTLN("AIRPORT: GOING TO CLEANUP - g_bTriggerSceneActive IS TRUE")
					eCoreState = LCS_CLEAN
				ENDIF
				
				#IF IS_DEBUG_BUILD
					TOGGLE_BLIPS()
//					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_ALT, "")
//						g_bTriggerSceneActive = TRUE
//					ENDIF
				#ENDIF
				
				// This is where all subsystems will be updated.
				LOSSANTOS_AIRPLANE_UPDATE()
			BREAK
			
			CASE LCS_CLEAN
				PRINTLN("INSIDE STATE - LCS_CLEAN")
				
				// This is where everything gets wrapped up.
				LOSSANTOS_AIRPLANE_CLEANUP()
				
				// Call this last. It'll terminate script.
				CLEANUP_SCRIPT()
			BREAK
		ENDSWITCH
		
		WAIT(iWaitTime)
	ENDWHILE
ENDSCRIPT
