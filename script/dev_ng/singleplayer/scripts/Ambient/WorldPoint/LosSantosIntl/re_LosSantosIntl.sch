// re_LosSantosIntl.sch
USING "commands_script.sch"
USING "script_player.sch"
USING "script_maths.sch"
USING "commands_streaming.sch"
USING "commands_clock.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_vehicle.sch"
USING "minigames_helpers.sch"
USING "oddjob_aggro.sch"

CONST_FLOAT		LOSSANTOS_QUIT_DIST2			490000.0		// 700m

ENUM LOSSANTOS_CORE_STATE
	LCS_INIT = 0,
	LCS_REQUEST_COMMON,
	LCS_STREAM_COMMON,
	LCS_SPAWN_COMMON,
	LCS_UPDATE,	
	LCS_CLEAN
ENDENUM

ENUM LOSSANTOS_SYSTEM_STATE
	LSS_INIT = 0,
	LSS_REQUEST,
	LSS_STREAM,
	LSS_SPAWN,
	LSS_UPDATE
ENDENUM


/// PURPOSE:
///    Debug function to turn on all blips in the area.
#IF IS_DEBUG_BUILD
PROC TOGGLE_BLIPS()

ENDPROC
#ENDIF

/// PURPOSE:
///    Handles requesting of assets for the army base that are used across multiple systems.
PROC LOSSANTOS_MAKE_COMMON_REQUESTS()

ENDPROC

/// PURPOSE:
///    Checks to see if common requests have finished loading.
/// RETURNS:
///    TRUE when common requests have loaded.
FUNC BOOL LOSSANTOS_ARE_COMMON_REQUESTS_LOADED()
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles any common spawning the army base script needs to do.
PROC LOSSANTOS_SPAWN_COMMON()

ENDPROC
