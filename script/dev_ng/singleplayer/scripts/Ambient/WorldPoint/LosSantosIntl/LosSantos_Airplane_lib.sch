// LosSantos_Airplane_lib.sch
USING "re_LosSantosIntl.sch"

CONST_INT MAX_NUMBER_JETS 4
CONST_FLOAT SPHERE_VISIBILITY_DIST 50.0

VEHICLE_INDEX vehJet[MAX_NUMBER_JETS]
VEHICLE_INDEX vehEastWestJet
PED_INDEX pedJetDriver[MAX_NUMBER_JETS]
PED_INDEX pedEastWestPilot
//BLIP_INDEX blipJet[MAX_NUMBER_JETS]
VECTOR vJetStartingPos[MAX_NUMBER_JETS]
VECTOR vSmallJetStartingPos

BOOL bJetStarted01 = FALSE
BOOL bRunPlanes = TRUE
BOOL bOkayToRunJet2 = FALSE
BOOL bOkayToRunJet4 = FALSE
BOOL bResetLoop = FALSE
BOOL bRecordingsHaveLoaded = FALSE

INT iEastWestFlightStages = 0

MODEL_NAMES mnPilot = S_M_M_PILOT_01

FLOAT fJetPos01, fJetPos02

ENUM JET_PLANE
	JET_PLANE_STATE_01 = 0,
	JET_PLANE_STATE_02, 
	JET_PLANE_STATE_03,
	JET_PLANE_STATE_04,
	JET_PLANE_STATE_05,
	JET_PLANE_STATE_06
ENDENUM

JET_PLANE jetPlaneStages = JET_PLANE_STATE_01

LOSSANTOS_SYSTEM_STATE 	eAirplaneState

PROC JET_HOLD_POSITION(VEHICLE_INDEX& vehJetToHold, PED_INDEX& pedToHold)
	SET_ENTITY_VISIBLE(vehJetToHold, FALSE)
	SET_ENTITY_VISIBLE(pedToHold, FALSE)
	
	SET_ENTITY_COLLISION(vehJetToHold, FALSE)
	FREEZE_ENTITY_POSITION(vehJetToHold, TRUE)
ENDPROC

PROC JET_UNHOLD_POSITION(VEHICLE_INDEX& vehJetToUnHold, PED_INDEX& pedToUnHold)
	SET_ENTITY_VISIBLE(vehJetToUnHold, TRUE)
	SET_ENTITY_VISIBLE(pedToUnHold, TRUE)
	
	SET_ENTITY_COLLISION(vehJetToUnHold, TRUE)
	FREEZE_ENTITY_POSITION(vehJetToUnHold, FALSE)
ENDPROC

PROC RESET_LOOP()
	fJetPos01 = 0
	bJetStarted01 = FALSE
	bResetLoop = TRUE
	bOkayToRunJet2 = FALSE
	PRINTLN("RESTARTING LOOP")
ENDPROC

PROC AIRPORT_PLANE_AUTOPILOT(VEHICLE_INDEX Plane, FLOAT fSpeed, BOOL bRotInterp, FLOAT xRot = 0.0, FLOAT yRot = 0.0)

	// Error checking.
	IF IS_ENTITY_DEAD(Plane)
		DEBUG_MESSAGE("PLANE_AUTOPILOT: Plane not alive, exiting!")
		EXIT
	ENDIF
	
	// If Plane speed is below desired speed change it.
	IF (GET_ENTITY_SPEED(Plane) < fSpeed)
		SET_VEHICLE_FORWARD_SPEED(Plane, fSpeed)
	ENDIF
	
	// If Plane isn't level, rotate it (interp if desired).
	VECTOR vRot = GET_ENTITY_ROTATION(Plane)
	BOOL bPlaneRotated = FALSE
	IF bRotInterp
		FLOAT fRotInterp = GET_FRAME_TIME() * 45.0
		IF (vRot.x < -fRotInterp)
			bPlaneRotated = TRUE
			vRot.x += fRotInterp
		ELIF (vRot.x < xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ELIF (vRot.x > fRotInterp)
			bPlaneRotated = TRUE
			vRot.x -= fRotInterp
		ELIF (vRot.x > xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ENDIF
		IF (vRot.y < -fRotInterp)
			bPlaneRotated = TRUE
			vRot.y += fRotInterp
		ELIF (vRot.y < yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ELIF (vRot.y > fRotInterp)
			bPlaneRotated = TRUE
			vRot.y -= fRotInterp
		ELIF (vRot.y > yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ENDIF
	ELSE
		IF (vRot.x <> xRot)
			bPlaneRotated = TRUE
			vRot.x = xRot
		ENDIF
		IF (vRot.y <> yRot)
			bPlaneRotated = TRUE
			vRot.y = yRot
		ENDIF
	ENDIF
	IF bPlaneRotated
		SET_ENTITY_ROTATION(Plane, vRot)
	ENDIF
	
ENDPROC

PROC HANDLE_EAST_WEST_FLIGHT()
	VECTOR vPlayerPosition
	
	SWITCH iEastWestFlightStages
		CASE 0
			IF NOT DOES_ENTITY_EXIST(vehEastWestJet)
				
				vehEastWestJet = CREATE_VEHICLE(JET, <<-65.3177, 15.4603, 703.1060>>)
				SET_ENTITY_LOD_DIST(vehEastWestJet, 1000)
				SET_VEHICLE_ENGINE_ON(vehEastWestJet, TRUE, TRUE)
				SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehEastWestJet, FALSE)
				
				pedEastWestPilot = CREATE_PED_INSIDE_VEHICLE(vehEastWestJet, PEDTYPE_CIVMALE, mnPilot)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedEastWestPilot, TRUE)
				
				PRINTLN("iEastWestFlightStages = 1")
				iEastWestFlightStages = 1
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------
		CASE 1
			IF DOES_ENTITY_EXIST(vehEastWestJet) AND NOT IS_ENTITY_DEAD(vehEastWestJet)
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "EastWestFlight")
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEastWestJet)
						
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehEastWestJet, 101, "EastWestFlight", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT), 5)
						PRINTLN("STARTING EAST WEST FLIGHT PLAYBACK")
						
						PRINTLN("iEastWestFlightStages = 2")
						iEastWestFlightStages = 2
					ENDIF
				ENDIF
			ENDIF
		BREAK
		//-------------------------------------------------------------------------------------------------------------------------
		CASE 2
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			ENDIF
		
			IF DOES_ENTITY_EXIST(vehEastWestJet) AND NOT IS_ENTITY_DEAD(vehEastWestJet)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehEastWestJet)
					IF IS_ENTITY_OCCLUDED(vehEastWestJet)
					AND NOT IS_SPHERE_VISIBLE(<<-1602.0862, -2674.0386, 12.9444>>, SPHERE_VISIBILITY_DIST)
					AND VDIST2(vPlayerPosition, GET_ENTITY_COORDS(vehEastWestJet)) > 62500 // 250m
				
						PRINTLN("RESETTING LOOP: iEastWestFlightStages = 1")
						iEastWestFlightStages = 1
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC SWITCH_AIRBORNE_JETS_TO_AI(BOOL bCleanup = FALSE)

	IF bRunPlanes
		IF DOES_ENTITY_EXIST(vehJet[0]) AND NOT IS_ENTITY_DEAD(vehJet[0])
			IF IS_ENTITY_IN_AIR(vehJet[0])
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[0])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedJetDriver[0]) AND NOT IS_ENTITY_DEAD(vehJet[0])  
						TASK_PLANE_MISSION(pedJetDriver[0], vehJet[0], NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 90, 100, 50)
//						PRINTLN("TASKING PLANES TO FLEE AIRBORNE 01")
					ENDIF
				ELSE
//					PRINTLN("PLAYBACK IS STILL GOING")
					IF bCleanup
						IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedJetDriver[0]) AND NOT IS_ENTITY_DEAD(vehJet[0])  
							TASK_PLANE_MISSION(pedJetDriver[0], vehJet[0], NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 90, 100, 50)
//							PRINTLN("TASKING PLANES TO FLEE AIRBORNE 01a")
						ENDIF
					ENDIF
				ENDIF
			ELSE
//				PRINTLN("ENTITY IS NOT IN AIR 01")
			ENDIF
		ELSE
//			PRINTLN("ENTITY DOES NOT EXIST OR IS DEAD")
		ENDIF
		IF DOES_ENTITY_EXIST(vehJet[2]) AND NOT IS_ENTITY_DEAD(vehJet[2])
			IF IS_ENTITY_IN_AIR(vehJet[2])
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[2])
					IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedJetDriver[2]) AND NOT IS_ENTITY_DEAD(vehJet[2]) 
						TASK_PLANE_MISSION(pedJetDriver[2], vehJet[2], NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 90, 100, 50)
//						PRINTLN("TASKING PLANES TO FLEE AIRBORNE 02")
					ENDIF
				ELSE
					IF bCleanup
						IF NOT IS_PED_INJURED(PLAYER_PED_ID()) AND NOT IS_ENTITY_DEAD(pedJetDriver[2]) AND NOT IS_ENTITY_DEAD(vehJet[2]) 
							TASK_PLANE_MISSION(pedJetDriver[2], vehJet[2], NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 90, 100, 50)
//							PRINTLN("TASKING PLANES TO FLEE AIRBORNE 02")
						ENDIF
					ENDIF
				ENDIF
			ELSE
//				PRINTLN("ENTITY IS NOT IN AIR 02")
			ENDIF
		ENDIF
	ELSE
//		PRINTLN("NOT RUNNING PLANES")
	ENDIF
ENDPROC

/// PURPOSE:
///    Handle the entire AirControl system.
PROC LOSSANTOS_AIRPLANE_UPDATE()	
	INT idx

	SWITCH (eAirplaneState)
		CASE LSS_INIT
			// Fill in this.
			vJetStartingPos[0] = << -1542.1127, -3023.8025, 23.2538 >>
			vJetStartingPos[1] = <<-3089.8879, -1960.0751, 313.5590>>
			vJetStartingPos[2] = << -1037.6381, -3316.1196, 23.2475 >>
			vJetStartingPos[3] = <<451.1740, -4009.4602, 135.1171>>

			vSmallJetStartingPos = << -1612.1736, -2688.4419, 12.9444 >>
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID()) OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					PRINTLN("NOT RUNNING PLANES DUE TO PLAYER BEING IN A FLYING VEHICLE!!!!!")
					bRunPlanes = FALSE
				ELSE
					PRINTLN("RUNNING PLANES")
					bRunPlanes = TRUE
				ENDIF
			ENDIF
			
			eAirplaneState = LSS_REQUEST
		BREAK
		
		CASE LSS_REQUEST
			// Request assets here.
			REQUEST_MODEL(JET)
			PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - JET")
			REQUEST_MODEL(mnPilot)
			PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - mnPilot")
		
			REQUEST_VEHICLE_RECORDING(101, "AirportJetTakeOff")
			REQUEST_VEHICLE_RECORDING(103, "AirplaneLandingRedux")
			REQUEST_VEHICLE_RECORDING(101, "EastWestFlight")
			REQUEST_VEHICLE_RECORDING(101, "AirportNew")
			REQUEST_VEHICLE_RECORDING(104, "AirplaneLandingRedux")
			eAirplaneState = LSS_STREAM
		BREAK
		
		CASE LSS_STREAM
			// Stream here.
			IF NOT bRecordingsHaveLoaded
				IF HAS_MODEL_LOADED(JET)
				AND HAS_MODEL_LOADED(mnPilot)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "AirportJetTakeOff")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "AirplaneLandingRedux")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "AirportNew")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "AirplaneLandingRedux")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "EastWestFlight")
					PRINTLN("MODELS AND RECORDINGS HAVE BEEN SUCCESSFULLY LOADED")
					bRecordingsHaveLoaded = TRUE
				ELSE
					REQUEST_MODEL(JET)
					PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - JET")
					REQUEST_MODEL(mnPilot)
					PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - mnPilot")
				
					REQUEST_VEHICLE_RECORDING(101, "AirportJetTakeOff")
					REQUEST_VEHICLE_RECORDING(102, "AirportJetTakeOff")
					REQUEST_VEHICLE_RECORDING(101, "AirportNew")
					REQUEST_VEHICLE_RECORDING(102, "AirportNew")
					REQUEST_VEHICLE_RECORDING(101, "EastWestFlight")
					PRINTLN("AMBIENT AIRPORT - WAITING ON JET MODEL AND VEHICLE RECORDINGS")
				ENDIF
			ENDIF
			
			// Once we loaded the models and recordings, wait until the spawn points are not visible to the player.
			IF bRecordingsHaveLoaded
				IF NOT IS_SPHERE_VISIBLE(vJetStartingPos[0], SPHERE_VISIBILITY_DIST)
				AND NOT IS_SPHERE_VISIBLE(vSmallJetStartingPos, SPHERE_VISIBILITY_DIST)
					PRINTLN("GOING TO STATE - LSS_SPAWN")
					eAirplaneState = LSS_SPAWN
				ELSE
					IF IS_SPHERE_VISIBLE(vJetStartingPos[0], SPHERE_VISIBILITY_DIST)
						PRINTLN("WAITING - PLANE SPAWN POINT IS VISIBLE 01")
					ENDIF
					IF IS_SPHERE_VISIBLE(vSmallJetStartingPos, SPHERE_VISIBILITY_DIST)
						PRINTLN("WAITING - PLANE SPAWN POINT IS VISIBLE 02")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE LSS_SPAWN			
			// Spawn assets here.
			vehJet[0] = CREATE_VEHICLE(JET, vJetStartingPos[0])
			SET_ENTITY_HEADING(vehJet[0], 240.3179)
			SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehJet[0], FALSE)
			
//			blipJet[0] = CREATE_BLIP_FOR_ENTITY(vehJet[0])
//			SET_BLIP_COLOUR(blipJet[0], BLIP_COLOUR_BLUE)
			
			vehJet[1] = CREATE_VEHICLE(JET, vJetStartingPos[1])
			SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehJet[1], FALSE)

//			blipJet[1] = CREATE_BLIP_FOR_ENTITY(vehJet[1])
//			SET_BLIP_COLOUR(blipJet[1], BLIP_COLOUR_GREEN)
			
			vehJet[2] = CREATE_VEHICLE(JET, vJetStartingPos[2])
			SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehJet[2], FALSE)

//			blipJet[2] = CREATE_BLIP_FOR_ENTITY(vehJet[2])
//			SET_BLIP_COLOUR(blipJet[2], BLIP_COLOUR_RED)
			
			vehJet[3] = CREATE_VEHICLE(JET, vJetStartingPos[3])
			SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehJet[3], FALSE)

//			blipJet[3] = CREATE_BLIP_FOR_ENTITY(vehJet[3])
//			SET_BLIP_COLOUR(blipJet[3], BLIP_COLOUR_YELLOW)

			REPEAT MAX_NUMBER_JETS idx
				IF DOES_ENTITY_EXIST(vehJet[idx])
					pedJetDriver[idx] = CREATE_PED_INSIDE_VEHICLE(vehJet[idx], PEDTYPE_CIVMALE, mnPilot)
					SET_ENTITY_LOD_DIST(vehJet[idx], 1000)
					SET_VEHICLE_ENGINE_ON(vehJet[idx], TRUE, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJetDriver[idx], TRUE)
				ENDIF
			ENDREPEAT
			
			JET_HOLD_POSITION(vehJet[1], pedJetDriver[1])
			JET_HOLD_POSITION(vehJet[2], pedJetDriver[2])
			JET_HOLD_POSITION(vehJet[3], pedJetDriver[3])
			
			IF bRunPlanes
				IF DOES_ENTITY_EXIST(vehJet[0])
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "AirportJetTakeOff")
						START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehJet[0], 101, "AirportJetTakeOff", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT), 5)
						PRINTLN("STARTING JET 1")
						bJetStarted01 = TRUE
					ELSE
						REQUEST_VEHICLE_RECORDING(101, "AirportJetTakeOff")
						PRINTLN("RECORDING HAS NOT BEEN LOADED - REQUEST AirportJetTakeOff 101")
					ENDIF
				ENDIF
			ENDIF
			
			PRINTLN("GOING TO STATE - LSS_UPDATE")
			eAirplaneState = LSS_UPDATE
		BREAK
		
		CASE LSS_UPDATE
			
			// We need to check if the loop has made it through one loop successfully, then check for Jet 3 to be off-screen.
			IF bResetLoop
				IF DOES_ENTITY_EXIST(vehJet[3]) AND NOT IS_ENTITY_DEAD(vehJet[3])
					IF IS_ENTITY_OCCLUDED(vehJet[3]) 
						IF NOT bOkayToRunJet2
							JET_HOLD_POSITION(vehJet[3], pedJetDriver[3])
							bOkayToRunJet2 = TRUE
							PRINTLN("INSIDE RESET LOOP - HOLDING JET vehJet[3]")
						ENDIF
					ELSE
//						PRINTLN("vehJet[3] IS NOT OCCLUDED")
					ENDIF
				ELSE
//					PRINTLN("vehJet[3] IS DEAD OR DOES NOT EXIST")
				ENDIF
			ENDIF
			
			SWITCH_AIRBORNE_JETS_TO_AI()
			HANDLE_EAST_WEST_FLIGHT()
						
			SWITCH jetPlaneStages
				CASE JET_PLANE_STATE_01
				
//					PRINTLN("INSIDE STATE - JET_PLANE_STATE_01")
				
					// Bool check if the first plane has not started.
					IF NOT bJetStarted01
						// If we're not in a plane, go and ahead and start running planes.
						IF bRunPlanes
							IF DOES_ENTITY_EXIST(vehJet[0]) AND NOT IS_ENTITY_DEAD(vehJet[0])
								IF IS_ENTITY_OCCLUDED(vehJet[0]) AND NOT IS_SPHERE_VISIBLE(vJetStartingPos[0], SPHERE_VISIBILITY_DIST)
									IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "AirportJetTakeOff")
										START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehJet[0], 101, "AirportJetTakeOff", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT), 5)
										PRINTLN("RESTARTING JET 1")
										bJetStarted01 = TRUE
									ELSE
										REQUEST_VEHICLE_RECORDING(101, "AirportJetTakeOff")
										PRINTLN("RECORDING HAS NOT BEEN LOADED - REQUEST AirportJetTakeOff 101")
									ENDIF
								ENDIF
							ELSE
//								PRINTLN("vehJet[0] DOES NOT EXIST OR IS DEAD")
							ENDIF
						ELSE
//							PRINTLN("bRunPlanes = FALSE")
						ENDIF
					ENDIF
					
					// The first jet has started, go ahead and check it's position.
					IF bJetStarted01
						IF DOES_ENTITY_EXIST(vehJet[0]) AND NOT IS_ENTITY_DEAD(vehJet[0])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[0])
								fJetPos01 = GET_POSITION_IN_RECORDING(vehJet[0])
//								PRINTLN("fJetPos01 = ", fJetPos01)
							ENDIF
						ENDIF
						
						// We're far enough along with the first jet, go ahead and move on to the next jet.
						IF fJetPos01 > 1100.0
							PRINTLN("JET 01 IS PAST POSITION 1100, MOVING ON TO JET 02")
							jetPlaneStages = JET_PLANE_STATE_02
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE JET_PLANE_STATE_02
					IF DOES_ENTITY_EXIST(vehJet[1]) AND NOT IS_ENTITY_DEAD(vehJet[1])
						// Are we okay to run planes.
						IF bRunPlanes
							IF IS_ENTITY_OCCLUDED(vehJet[1])
								// If we run the loop once and Jet 4 is off-screen, start again.
								IF bResetLoop
									// We're on the second loop
									IF bOkayToRunJet2
										// We're okay to start the second plane again
										JET_UNHOLD_POSITION(vehJet[1], pedJetDriver[1])
										IF HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "AirplaneLandingRedux")
											START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehJet[1], 103, "AirplaneLandingRedux", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT), 5)
											bOkayToRunJet4 = FALSE
											PRINTLN("STARTING JET 2")
											PRINTLN("GOING TO STATE - JET_PLANE_STATE_03 - 01")
											jetPlaneStages = JET_PLANE_STATE_03
										ELSE
											REQUEST_VEHICLE_RECORDING(103, "AirplaneLandingRedux")
											PRINTLN("RECORDING HAS NOT BEEN LOADED - REQUEST AirplaneLandingRedux 103")
										ENDIF
									ENDIF
								// First time through, go ahead and run Jet 2
								ELSE
									JET_UNHOLD_POSITION(vehJet[1], pedJetDriver[1])
									IF HAS_VEHICLE_RECORDING_BEEN_LOADED(103, "AirplaneLandingRedux")
										START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(vehJet[1], 103, "AirplaneLandingRedux", ENUM_TO_INT(SWITCH_ON_ANY_VEHICLE_IMPACT), 5)
										bOkayToRunJet4 = FALSE
										PRINTLN("STARTING JET 2")
										PRINTLN("GOING TO STATE - JET_PLANE_STATE_03 - 02")
										jetPlaneStages = JET_PLANE_STATE_03
									ELSE
										REQUEST_VEHICLE_RECORDING(103, "AirplaneLandingRedux")
										PRINTLN("AIRPORT:  103, AirplaneLandingRedux NOT LOADED 02")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE JET_PLANE_STATE_03
					IF DOES_ENTITY_EXIST(vehJet[1]) AND NOT IS_ENTITY_DEAD(vehJet[1])
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[1])
							
							PRINTLN("GOING TO STATE - JET_PLANE_STATE_04")
							jetPlaneStages = JET_PLANE_STATE_04
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE JET_PLANE_STATE_04
					// Start up Jet 3, i.e. the second one to take off... first check if it's off-screen, then go.
					IF DOES_ENTITY_EXIST(vehJet[2]) AND NOT IS_ENTITY_DEAD(vehJet[2])
						IF HAS_VEHICLE_RECORDING_BEEN_LOADED(101, "AirportNew")
							IF NOT IS_SPHERE_VISIBLE(vJetStartingPos[2], SPHERE_VISIBILITY_DIST)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[2])
									JET_UNHOLD_POSITION(vehJet[2], pedJetDriver[2])
									START_PLAYBACK_RECORDED_VEHICLE(vehJet[2], 101, "AirportNew")
									PRINTLN("STARTING JET 3")
									PRINTLN("GOING TO STATE - JET_PLANE_STATE_05")
									jetPlaneStages = JET_PLANE_STATE_05
								ELSE
//									PRINTLN("PLAYBACK IS STILL ON GOING FOR vehJet[2]")
								ENDIF
							ELSE
//								PRINTLN("SPHERE IS VISIBLE")
							ENDIF
						ELSE
//							PRINTLN("AIRPORT:  101, AirportNew NOT LOADED")
						ENDIF
					ELSE
//						PRINTLN("vehJet[2] DOESN'T EXIST OR IS DEAD")
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE JET_PLANE_STATE_05
					
					// Grab position of Jet 3, i.e. the second one to take-off.
					IF DOES_ENTITY_EXIST(vehJet[2]) AND NOT IS_ENTITY_DEAD(vehJet[2])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[2])
							fJetPos02 = GET_POSITION_IN_RECORDING(vehJet[2])
//							PRINTLN("fJetPos02 = ", fJetPos02)
						ENDIF
					ENDIF
					
					// We need to wait until the second jet is cleared from the runway before starting the 4th jet, otherwise they'll crash into each other!
					IF DOES_ENTITY_EXIST(vehJet[1]) AND NOT IS_ENTITY_DEAD(vehJet[1])
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[1])
							IF IS_ENTITY_OCCLUDED(vehJet[1])
								JET_HOLD_POSITION(vehJet[1], pedJetDriver[1])
								bOkayToRunJet4 = TRUE
//								PRINTLN("HOLDING JET 2")
							ENDIF
						ENDIF
					ENDIF
					
					// Check if Jet 3, i.e .the second one to take-off is far enough along to start the next jet.
					IF fJetPos02 > 1100.0
						IF DOES_ENTITY_EXIST(vehJet[3]) AND NOT IS_ENTITY_DEAD(vehJet[3])
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(104, "AirplaneLandingRedux")
								IF NOT IS_SPHERE_VISIBLE(vJetStartingPos[3], SPHERE_VISIBILITY_DIST)
									IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[3]) AND bOkayToRunJet4
										JET_UNHOLD_POSITION(vehJet[3], pedJetDriver[3])
										START_PLAYBACK_RECORDED_VEHICLE(vehJet[3], 104, "AirplaneLandingRedux")
										PRINTLN("STARTING JET 4")
										PRINTLN("GOING TO STATE - JET_PLANE_STATE_06")
										jetPlaneStages = JET_PLANE_STATE_06
									ENDIF
								ENDIF
							ELSE
								PRINTLN("AIRPORT:  102, AirportNew NOT LOADED")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				//-----------------------------------------------------------------------------------------------------------------------
				CASE JET_PLANE_STATE_06
				
					// If the last plane has run and finished playback, go ahead and reset the loop.
					IF DOES_ENTITY_EXIST(vehJet[3]) AND NOT IS_ENTITY_DEAD(vehJet[3])
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJet[3])
							RESET_LOOP()
							
							PRINTLN("GOING TO STATE - JET_PLANE_STATE_01")
							jetPlaneStages = JET_PLANE_STATE_01
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC SAFE_SWITCH_PLANE_TASKS(VEHICLE_INDEX& vehToCheck, PED_INDEX& pedToCheck)

	IF DOES_ENTITY_EXIST(vehToCheck) AND DOES_ENTITY_EXIST(pedToCheck)
	AND NOT IS_ENTITY_DEAD(vehToCheck) AND NOT IS_ENTITY_DEAD(pedToCheck)
		IF IS_ENTITY_IN_AIR(vehToCheck)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehToCheck)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					TASK_PLANE_MISSION(pedToCheck, vehToCheck, NULL, NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_FLEE, 50.0, -1, 30.0, 100, 50)
					PRINTLN("SAFE_SWITCH_PLANE_TASKS BEING CALLED")
				ENDIF				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans everything created/used in the Los Santos airplane system.
PROC LOSSANTOS_AIRPLANE_CLEANUP()
	INT idx

	REPEAT MAX_NUMBER_JETS idx
		IF DOES_ENTITY_EXIST(vehJet[idx])
			IF IS_ENTITY_OCCLUDED(vehJet[idx]) OR (NOT IS_ENTITY_DEAD(vehJet[idx]) AND NOT IS_ENTITY_VISIBLE(vehJet[idx]))
				DELETE_VEHICLE(vehJet[idx])
				PRINTLN("DELETING JET: ", idx)
				DELETE_PED(pedJetDriver[idx])
				PRINTLN("DELETING JET PILOT: ", idx)
			ELSE
				IF idx = 0 OR idx = 2
					PRINTLN("CLEANUP GETTING CALLED INDEX: ", idx)
					SWITCH_AIRBORNE_JETS_TO_AI(TRUE)
				ENDIF
				
				SAFE_SWITCH_PLANE_TASKS(vehJet[idx], pedJetDriver[idx])
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehJet[idx])
				PRINTLN("SETTING JET AS NOT LONGER NEEDED: ", idx)
				SET_PED_AS_NO_LONGER_NEEDED(pedJetDriver[idx])
				PRINTLN("SETTING JET PILOT AS NOT LONGER NEEDED: ", idx)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(vehEastWestJet)
		IF IS_ENTITY_OCCLUDED(vehEastWestJet)
			DELETE_VEHICLE(vehEastWestJet)
			PRINTLN("DELETING JET - vehEastWestJet")
			DELETE_PED(pedEastWestPilot)
			PRINTLN("DELETING JET PILOT - pedEastWestPilot")
		ELSE
		
			SAFE_SWITCH_PLANE_TASKS(vehEastWestJet, pedEastWestPilot)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehEastWestJet)
			PRINTLN("SETTING JET AS NOT LONGER NEEDED - vehEastWestJet")
			SET_PED_AS_NO_LONGER_NEEDED(pedEastWestPilot)
			PRINTLN("SETTING JET PILOT AS NOT LONGER NEEDED - pedEastWestPilot")
		ENDIF
	ENDIF
	
	IF bRecordingsHaveLoaded
		REMOVE_VEHICLE_RECORDING(101, "AirportJetTakeOff")
		PRINTLN("REMOVING VEHICLE RECORDING - AirportJetTakeOff 101")
	
		REMOVE_VEHICLE_RECORDING(102, "AirportJetTakeOff")
		PRINTLN("REMOVING VEHICLE RECORDING - AirportJetTakeOff 102")
		
		REMOVE_VEHICLE_RECORDING(101, "AirportNew")
		PRINTLN("REMOVING VEHICLE RECORDING - AirportNew 101")
		
		REMOVE_VEHICLE_RECORDING(102, "AirportNew")
		PRINTLN("REMOVING VEHICLE RECORDING - AirportNew 102")
		
		REMOVE_VEHICLE_RECORDING(103, "AirplaneLandingRedux")
		PRINTLN("REMOVING VEHICLE RECORDING - AirplaneLandingRedux 103")
		
		REMOVE_VEHICLE_RECORDING(104, "AirplaneLandingRedux")
		PRINTLN("REMOVING VEHICLE RECORDING - AirplaneLandingRedux 104")
		
		REMOVE_VEHICLE_RECORDING(101, "EastWestFlight")
		PRINTLN("REMOVING VEHICLE RECORDING - EastWestFlight 101")
	ENDIF
	
ENDPROC
