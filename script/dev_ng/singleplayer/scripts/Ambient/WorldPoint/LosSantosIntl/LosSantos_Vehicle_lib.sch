// LosSantos_Vehicle_lib.sch
USING "re_LosSantosIntl.sch"

VEHICLE_INDEX vehHeli01
PED_INDEX pedHeliDriver01
VECTOR vHeliStartPos
FLOAT fHeliPos
BOOL bHeliRecordingLoaded = FALSE

LOSSANTOS_SYSTEM_STATE 	eVehicleState

/// PURPOSE:
///    Handle the entire AirControl system.
PROC LOSSANTOS_VEHICLE_UPDATE()	
	SWITCH (eVehicleState)
		CASE LSS_INIT
			// Fill in this.
			
			vHeliStartPos = << -1146.8289, -2866.1721, 14.0626 >>
			
			eVehicleState = LSS_REQUEST
		BREAK
		
		CASE LSS_REQUEST
			// Request assets here.
			REQUEST_MODEL(MAVERICK)
			PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - MAVERICK")
			REQUEST_MODEL(A_M_Y_GENSTREET_01)
			PRINTLN("AMBIENT AIRPORT - REQUESTING MODEL - A_M_Y_GENSTREET_01")
			
			REQUEST_VEHICLE_RECORDING(102, "HelicopterTakeOff")
			eVehicleState = LSS_STREAM
		BREAK
		
		CASE LSS_STREAM
			// Stream here.
			IF HAS_MODEL_LOADED(MAVERICK)
			AND HAS_MODEL_LOADED(A_M_Y_GENSTREET_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(102, "HelicopterTakeOff")
				PRINTLN("GOING TO STATE - LSS_SPAWN")
				bHeliRecordingLoaded = TRUE
				eVehicleState = LSS_SPAWN
			ELSE
				PRINTLN("AMBIENT AIRPORT - WAITING ON HELI MODEL AND VEHICLE RECORDINGS")
			ENDIF
		BREAK
		
		CASE LSS_SPAWN			
			// Spawn assets here.
			vehHeli01 = CREATE_VEHICLE(MAVERICK, vHeliStartPos)
			SET_VEHICLE_GENERATES_ENGINE_SHOCKING_EVENTS(vehHeli01, FALSE)
			
			CREATE_PED_INSIDE_VEHICLE(vehHeli01, PEDTYPE_CIVMALE, A_M_Y_GENSTREET_01)
			SET_ENTITY_LOD_DIST(vehHeli01, 600)
			
			IF DOES_ENTITY_EXIST(vehHeli01)
				START_PLAYBACK_RECORDED_VEHICLE(vehHeli01, 102, "HelicopterTakeOff")
			ENDIF
			
			eVehicleState = LSS_UPDATE
		BREAK
		
		CASE LSS_UPDATE
			// General update.
			IF DOES_ENTITY_EXIST(vehHeli01) AND NOT IS_ENTITY_DEAD(vehHeli01)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehHeli01)
					fHeliPos = GET_POSITION_IN_RECORDING(vehHeli01)
//					PRINTLN("fHeliPos = ", fHeliPos)
				ENDIF
				IF fHeliPos > 700.0
					IF DOES_ENTITY_EXIST(vehHeli01) AND NOT IS_ENTITY_DEAD(vehHeli01)
						IF IS_ENTITY_OCCLUDED(vehHeli01)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehHeli01)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehHeli01)
								START_PLAYBACK_RECORDED_VEHICLE(vehHeli01, 102, "HelicopterTakeOff")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Cleans everything created/used in the Los Santos airplane system.
PROC LOSSANTOS_VEHICLE_CLEANUP()
	VECTOR vHeliCoord, vHeliOffset, vGoToCoord
	FLOAT fHeliHeading

	IF DOES_ENTITY_EXIST(vehHeli01)
		IF IS_ENTITY_OCCLUDED(vehHeli01)
			DELETE_VEHICLE(vehHeli01)
			PRINTLN("DELETING HELI")
		ELSE
			IF NOT IS_ENTITY_DEAD(vehHeli01) AND NOT IS_PED_INJURED(pedHeliDriver01)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehHeli01)
				SET_PED_KEEP_TASK(pedHeliDriver01, TRUE)
				
				vHeliCoord = GET_ENTITY_COORDS(vehHeli01)
				fHeliHeading = GET_ENTITY_HEADING(vehHeli01)
				vHeliOffset = <<0,500,50>>
					
				vGoToCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vHeliCoord, fHeliHeading, vHeliOffset)
				TASK_HELI_MISSION(pedHeliDriver01, vehHeli01, NULL, NULL, vGoToCoord, MISSION_GOTO, 50, -1, 0, 100, 50)
			ENDIF
		ENDIF
	ENDIF
	
	IF bHeliRecordingLoaded
		REMOVE_VEHICLE_RECORDING(102, "HelicopterTakeOff")
		PRINTLN("REMOVING VEHICLE RECORDING - HelicopterTakeOff 102")
	ENDIF
	
ENDPROC
