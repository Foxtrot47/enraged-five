///
///    100% BoomBox EASTER EGG
///    
///    STEVE R LDS.

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_player.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_clock.sch"
USING "commands_streaming.sch"

#IF IS_DEBUG_BUILD
	BOOL bBoomBoxDebugSpam = TRUE
#ENDIF

ENUM eBOOMBOX_STATE

	BOOMBOX_STATE_WAITING_TO_START = 0,
	BOOMBOX_STATE_SPAWNING,
	BOOMBOX_STATE_MONITORING,
	BOOMBOX_WAITING_TO_CLEANUP

ENDENUM

eBOOMBOX_STATE eBoomBoxState = BOOMBOX_STATE_WAITING_TO_START


PROC missionCleanup()

	IF IS_IPL_ACTIVE("ID2_21_G_Night")
		REMOVE_IPL("ID2_21_G_Night")
	ENDIF

	CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " script cleaning up.")
	TERMINATE_THIS_THREAD()
	
ENDPROC


/// PURPOSE:
///    Checks if the weather is right for the BoomBox spawn
/// RETURNS:
///    
//FUNC BOOL IS_WEATHER_CORRECT()
//
//	IF IS_NEXT_WEATHER_TYPE("RAIN")
//	OR IS_NEXT_WEATHER_TYPE("THUNDER")
//	OR IS_PREV_WEATHER_TYPE("RAIN")
//	OR IS_PREV_WEATHER_TYPE("THUNDER")
//		#IF IS_DEBUG_BUILD
//			IF bBoomBoxDebugSpam
//				CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " weather is correct")
//			ENDIF
//		#ENDIF
//		RETURN TRUE
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		IF bBoomBoxDebugSpam
//			CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " weather isn't correct")
//		ENDIF
//	#ENDIF
//	
//	RETURN FALSE
//
//ENDFUNC




SCRIPT (coords_struct in_coords)

#IF IS_DEBUG_BUILD
	PRINTNL()
	PRINTLN("WP PartyBoomBox - UPDATED")
	PRINTVECTOR(in_coords.vec_coord[0])
	PRINTNL()
#ENDIF

#IF IS_FINAL_BUILD
UNUSED_PARAMETER(in_coords)
#ENDIF

	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH( HASH("wp_PartyBoomBox")) > 1
		TERMINATE_THIS_THREAD()
	ENDIF

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		 missionCleanup()
	ENDIF

	WHILE TRUE
		WAIT(0)
		
		IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
			CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " is not within world point brain activation range.")
			eBoomBoxState = BOOMBOX_WAITING_TO_CLEANUP
		ENDIF
				
		SWITCH eBoomBoxState
		
			CASE BOOMBOX_STATE_WAITING_TO_START
			
				// Only activate BoomBox at 3AM in Rainy weather
				IF GET_CLOCK_HOURS() >= 22
				OR GET_CLOCK_HOURS() <=4
				//AND IS_WEATHER_CORRECT()
					CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Time correct - spawning")
					eBoomBoxState = BOOMBOX_STATE_SPAWNING
				ELSE
					CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Time incorrect - cleaning up")
					missionCleanup()
				ENDIF
				
			BREAK
			
			
			CASE BOOMBOX_STATE_SPAWNING
			
				#IF IS_DEBUG_BUILD
					IF bBoomBoxDebugSpam
						CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Waiting for BoomBox to spawn")
					ENDIF
				#ENDIF
				
				IF NOT IS_IPL_ACTIVE("ID2_21_G_Night")
					REQUEST_IPL("ID2_21_G_Night")
					CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " Waiting for script brain to deactivate")
					eBoomBoxState = BOOMBOX_STATE_MONITORING
				ENDIF
				
			BREAK
			
			CASE BOOMBOX_STATE_MONITORING
		
				
			BREAK
			
			CASE BOOMBOX_WAITING_TO_CLEANUP
			
				
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE() 
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					missionCleanup()
				ENDIF
				
				CPRINTLN( DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " waiting for load scene or switch scene to finish before cleaning up.")
			
			BREAK
			
			ENDSWITCH
		
	ENDWHILE

ENDSCRIPT
