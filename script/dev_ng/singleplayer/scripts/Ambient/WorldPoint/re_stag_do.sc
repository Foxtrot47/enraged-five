//////////////////////////////////////////////////////////
// 				Stag
//		   		Tor Sigurdson
//
//			1

//
//////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///    
///    
///    
///    
///    
///    //////////////////////////////////////////////////////////
///   							 HEADERS
///    //////////////////////////////////////////////////////////

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
//USING "ambience_run_checks.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_ped.sch"
USING "script_PLAYER.sch"
//USING "SP_Debug_Menu.sch"
USING "rage_builtins.sch"
USING "minigame_private.sch"
USING "script_blips.sch"
USING "commands_camera.sch"
USING "script_races.sch"
USING "Ambient_Speech.sch"
USING "commands_clock.sch"
USING "shared_hud_displays.sch"
USING "ambient_common.sch"
USING "completionpercentage_public.sch"
USING "cutscene_public.sch"
USING "buddy_head_track_public.sch"
USING "random_events_public.sch"
USING "commands_recording.sch"

//Player Position = << -2124.7388, 4366.5615, 45.5280 >>   heading: 327.6184   room: NONE
//Actual Z coords = 46.5338
//Debug Menu Warp = x="-2124.7388" y="4366.5615" z="45.5280" heading="327.6184"
//Camera Position = << 2442.0000, -1660.0000, 14.5000 >>, << 0.0000, 0.0000, 0.0000 >>
//Camera FOV = 45.0000


///    //////////////////////////////////////////////////////////
///    							VARIABLES
///    //////////////////////////////////////////////////////////

ENUM ambStageFlag
	ambCanRun,
	ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM ambDestinationFlag
	ambgogetclothes,
	ambwaitforstag,
	ambpickupbestman,
	ambgotoChapel
ENDENUM
ambDestinationFlag ambdestStage = ambgogetclothes //ambpickupbestman

BOOL bAssetsLoaded
BOOL bDoFullCleanUp
VECTOR vInput

//INT temp_int
//#IF IS_DEBUG_BUILD INT START_TIME_STAG, TIMELIMITSTAG #ENDIF 
//	INT TIME_LEFT_STAG//, END_TIME_STAG, CURRENT_TIME_STAG
PED_INDEX pedStag
PED_INDEX pedBride
PED_INDEX pedBestman
PED_INDEX pedGuest
//PED_INDEX pedPlayerTemp

VEHICLE_INDEX vehWeddingCar
VEHICLE_INDEX vehParkedCar[4]
VEHICLE_INDEX vehImpound

OBJECT_INDEX objRope

//REL_GROUP_HASH stag_group
//PED_INDEX ped_farmer
VECTOR vCurrentDestination
VECTOR vPos1
VECTOR vPos2
FLOAT fWidth

VECTOR vTheChapel
VECTOR vTheChapel1
VECTOR vTheChapel2
FLOAT fTheChapel
VECTOR vChapelCenter

VECTOR vStagsHouse
VECTOR vStagsHouse1
VECTOR vStagsHouse2
VECTOR vStagsDoor
FLOAT fStagsHouse

VECTOR vBestman
VECTOR vBestman1
VECTOR vBestman2
FLOAT fBestman

//VEHICLE_INDEX veh_jetski
SEQUENCE_INDEX  seq
//SEQUENCE_INDEX drowning_victim_seq
BOOL bStagsBlipRemoved
BOOL bPlayerCloseToStag
BOOL bLeavingActivationArea
BOOL bPlayerInWeddingCar = TRUE
BOOL bStagMadeToGoOutAndPerformSeq
BOOL bWaitingForStagToGetInCar
BOOL bBestmanToldToGetInCar
BOOL bTimeToGetToChapelMentioned
BOOL bBestmansLocationRevealed
BOOL bBestmanApproached
BOOL bBrideApproached
BOOL bUnsuitableVehRemark
BOOL bSaidComeBackStag
BOOL bLeavingBestManBehind
BOOL bSaidSuit
BOOL bCameraActive
//BOOL bTimerQuicker
//BOOL in_car_conv_started
//BOOL in_car_conv_on_hold
//STRING sConvResumeLabel

//REL_GROUP_HASH farmergroup
//BOOL flag_attach_ped_to_jetski = FALSE
//VECTOR offset_vector
//BOOL PRINT_L1_HELP_TEXT = TRUE
INT conversation_index = 0, iCutsceneProgress

VEHICLE_INDEX veh_ped_is_in
VEHICLE_INDEX vehicleCandidate

CAMERA_INDEX handoverCam1
CAMERA_INDEX handoverCam2

structPedsForConversation cultresConversation

BOOL bStagIgnoredByPlayer
STRING s_time_to_get_to_chapel
INT f_stag_dialogue_warning_on_time = 0
//TIMEOFDAY sTimeOfDay
//	int end_hours, start_hours, current_hours
//INT iCutsceneTimer, iCurrentGameTimeInMilliseconds
INT iCurrMin, iCurrHour
INT iWeddingMin, iWeddingHour
INT iStoppingTimer = 6000
INT iCountdownAudioEvent

BOOL bSaidTimeOfWedding
BOOL bSetUpUntieScene
BOOL bUntieSceneEnding
BOOL bSetUpUntieSpeech
BOOL bCleanUpUntieScene
BOOL bCutsceneSkipped
BOOL bSoundPlayed
BOOL bPushInToFirstPerson

INT sceneId
//INT minutes
BLIP_INDEX blipWeddingCar
//handoverDestCam
BLIP_INDEX blipCurrentDestination
BLIP_INDEX blipPersonGettingPlayersAttention
BLIP_INDEX blipBestman//, blip_person_to_save
//DECISION_MAKER_INDEX coward_decision_maker
//#IF IS_DEBUG_BUILD
//	BOOL DISPLAY_TIMER = FALSE
//
//#ENDIF

///    //////////////////////////////////////////////////////////
///    							PROCEDURES
///    //////////////////////////////////////////////////////////
PROC missionCleanup()

	PRINTSTRING("\n @@@@@@@@@@@@@@@ missionCleanup @@@@@@@@@@@@@@@ \n")
	
	IF bDoFullCleanUp
		
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vChapelCenter - << 10, 10, 10 >>, vChapelCenter + << 10, 10, 10 >>, TRUE)
		REMOVE_SCENARIO_BLOCKING_AREAS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		RESET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID())
		
		IF NOT IS_ENTITY_DEAD(vehImpound)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehImpound)
			AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehImpound, << 50, 50, 50 >>)
				SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehImpound)
			ENDIF
		ENDIF
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vChapelCenter, << 100, 100, 100 >>)
			IF NOT IS_PED_INJURED(pedStag)
			AND NOT IS_PED_INJURED(pedBestman)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -330.36, 6154.03, 31.8 >>, PEDMOVE_WALK, -1)
					TASK_CHAT_TO_PED(NULL, pedBestman, CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedStag, seq)
				CLEAR_SEQUENCE_TASK(seq)
//				SET_PED_KEEP_TASK(pedStag, TRUE)
				
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -330.36, 6155.03, 31.8 >>, PEDMOVE_WALK, -1)
					TASK_CHAT_TO_PED(NULL, pedStag, CF_AUTO_CHAT, << 0, 0, 0 >>, 0, 0)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedBestman, seq)
				CLEAR_SEQUENCE_TASK(seq)
//				SET_PED_KEEP_TASK(pedBestman, TRUE)
			ENDIF
			
//			IF NOT IS_PED_INJURED(pedBride)
//				TASK_SMART_FLEE_COORD(pedBride, <<-330.36, 6154.03, 31.8 >>, 150, -1)
//			ENDIF
		ENDIF
		
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleCandidate)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehWeddingCar)
		
		IF NOT IS_PLAYER_DEAD(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(pedStag)
			SET_PED_CONFIG_FLAG(pedStag, PCF_CanSayFollowedByPlayerAudio, TRUE)
			IF NOT IS_ENTITY_ATTACHED(pedStag)
				FREEZE_ENTITY_POSITION(pedStag, FALSE)
			ENDIF
			REMOVE_PED_FROM_GROUP(pedStag)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedStag, RELGROUPHASH_CIVMALE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStag, FALSE)
		ELSE
			IF DOES_ENTITY_EXIST(objRope)
				DETACH_ENTITY(objRope)
			ENDIF
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedStag)
		
		IF NOT IS_PED_INJURED(pedBestman)
			SET_PED_CONFIG_FLAG(pedBestman, PCF_CanSayFollowedByPlayerAudio, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBestman, FALSE)
//			SET_PED_RELATIONSHIP_GROUP_HASH(pedBestman, RELGROUPHASH_CIVMALE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedBestman)
		
		IF NOT IS_PED_INJURED(pedBride)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBride, FALSE)
		ENDIF
		IF NOT IS_PED_INJURED(pedGuest)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuest, FALSE)
		ENDIF
		SET_PED_AS_NO_LONGER_NEEDED(pedBride)
	ENDIF
	
	RANDOM_EVENT_OVER()
	
//	#IF IS_DEBUG_BUILD
//		IF can_cleanup_script_for_debug
//			IF DOES_ENTITY_EXIST(pedStag)
//				DELETE_PED(pedStag)
//			ENDIF
//			
//		ENDIF
//		PRINTNL()
//		PRINTSTRING("stag do cleaned up")
//		PRINTNL()
//	#ENDIF
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC MISSION_PASSED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_PASSED @@@@@@@@@@@@@@@ \n")
//	WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//		WAIT(0)
//	ENDWHILE
	WHILE NOT LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
		WAIT(0)
	ENDWHILE
	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	missionCleanup()
ENDPROC

PROC MISSION_FAILED()
	PRINTSTRING("\n @@@@@@@@@@@@@@@ MISSION_FAILED @@@@@@@@@@@@@@@ \n")
	missionCleanup()
ENDPROC

PROC debug_stuff()

	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			bPlayerCloseToStag = TRUE
			IF NOT bStagsBlipRemoved
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), << -935.6071, 2766.6067, 24.2353 >>)
						SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), << -941.6176, 2763.7236, 24.2188 >>)
						SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
					ENDIF
				ELSE
					SET_ENTITY_COORDS(PLAYER_PED_ID(), << -935.6071, 2766.6067, 24.2353 >>)
				ENDIF
			ELSE
				bTimeToGetToChapelMentioned = TRUE
				IF NOT IS_ENTITY_DEAD(vehWeddingCar)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
						SET_ENTITY_COORDS(vehWeddingCar, vCurrentDestination)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehWeddingCar)
						IF NOT IS_ENTITY_DEAD(pedStag)
							IF NOT IS_PED_IN_ANY_VEHICLE(pedStag)
								SET_PED_INTO_VEHICLE(pedStag, vehWeddingCar, VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						IF NOT IS_ENTITY_DEAD(pedBestman)
							IF NOT IS_PED_IN_ANY_VEHICLE(pedBestman)
								SET_PED_INTO_VEHICLE(pedBestman, vehWeddingCar, VS_BACK_RIGHT)
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
							SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vCurrentDestination)
							SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							IF NOT IS_ENTITY_DEAD(pedStag)
								IF NOT IS_PED_IN_ANY_VEHICLE(pedStag)
									SET_PED_INTO_VEHICLE(pedStag, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedBestman)
								IF NOT IS_PED_IN_ANY_VEHICLE(pedBestman)
									SET_PED_INTO_VEHICLE(pedBestman, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			//"stagdau", "stagd_untied")
			//ENDIF
			ENDIF
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			IF bStagsBlipRemoved
			AND bTimeToGetToChapelMentioned
			
//					blip_person_to_save = CREATE_AMBIENT_BLIP_FOR_PED(pedStag)
				
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1004.1862, 2747.0303, 23.6826 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 292.2006)
				ELSE
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1004.1862, 2747.0303, 23.6826 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 292.2006)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedStag)
					SET_ENTITY_COORDS(pedStag, << -935.4650, 2767.6221, 24.2136 >>)
					SET_ENTITY_HEADING(pedStag, 143.8528)
					IF IS_PED_IN_GROUP(pedStag)
						REMOVE_PED_FROM_GROUP(pedStag)
					ENDIF
					OPEN_SEQUENCE_TASK(seq)
//							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 80000, SLF_DEFAULT)
						TASK_PLAY_ANIM(NULL, "re@stag_do@idle_a", "idle_a_ped", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedStag, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedStag, TRUE)
				ENDIF
				
				bStagsBlipRemoved = FALSE
				bPlayerCloseToStag = FALSE
				bLeavingActivationArea = FALSE
				bStagIgnoredByPlayer = FALSE
				bSaidTimeOfWedding = FALSE
				bSetUpUntieScene = FALSE
				bUntieSceneEnding = FALSE
				bSetUpUntieSpeech = FALSE
				bCleanUpUntieScene = FALSE
				
			ELIF bTimeToGetToChapelMentioned
			AND NOT bPlayerInWeddingCar
				
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE())
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1021.6111, 2738.6160, 22.9737 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 121.6179)
					IF NOT IS_PED_INJURED(pedStag)
						SET_PED_INTO_VEHICLE(pedStag, GET_PLAYERS_LAST_VEHICLE(), VS_FRONT_RIGHT)
					ENDIF
				ELSE
					SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1021.6111, 2738.6160, 22.9737 >>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 121.6179)
					IF NOT IS_PED_INJURED(pedStag)
						SET_PED_COORDS_KEEP_VEHICLE(pedStag, << -1021.6111, 2738.6160, 22.9737 >>)
						SET_ENTITY_HEADING(pedStag, 121.6179)
					ENDIF
				ENDIF
				
				bTimeToGetToChapelMentioned = FALSE
				bPlayerInWeddingCar = TRUE
				bWaitingForStagToGetInCar = FALSE
				bStagMadeToGoOutAndPerformSeq = FALSE
				
				conversation_index = 0
				f_stag_dialogue_warning_on_time = 0
				
			ELIF bBestmansLocationRevealed
			AND ambdestStage = ambpickupbestman
				
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -1993.5560, 481.7628, 103.2837 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 181.3286)
				IF NOT IS_PED_INJURED(pedStag)
					SET_PED_COORDS_KEEP_VEHICLE(pedStag, << -1993.5560, 481.7628, 103.2837 >>)
					SET_ENTITY_HEADING(pedStag, 181.3286)
				ENDIF
				
				bBestmansLocationRevealed = FALSE
				ambdestStage = ambgogetclothes
				
			ELIF ambdestStage = ambpickupbestman
			AND NOT bBestmanToldToGetInCar
				
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -2205.5095, 4299.7852, 47.2655 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 334.1069)
				IF NOT IS_PED_INJURED(pedStag)
					SET_PED_COORDS_KEEP_VEHICLE(pedStag, << -2205.5095, 4299.7852, 47.2655 >>)
					SET_ENTITY_HEADING(pedStag, 334.1069)
				ENDIF
				
				bBestmanToldToGetInCar = TRUE
				
			ENDIF
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)	
			MISSION_PASSED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()
		ENDIF
		
		//PRINTNL()
		//PRINTSTRING("stag do cleaned up")
		//PRINTNL()
	#ENDIF
	
ENDPROC

PROC DrawCountdown(INT iRaceTime, BOOL bFlash = FALSE)
	INT iR, iG, iB, iA
	INT iminutes, iseconds, itemp
	//PRINTSTRING("\n")
	///PRINTINT (iRaceTime)
	//PRINTSTRING("\n")
	iminutes = iRaceTime/60000
	
	
	
	iseconds = iRaceTime/1000
	itemp = iminutes *60
	iseconds -= itemp
	
	
	IF iseconds <=0
		iseconds +=60
	ENDIF
	
	IF iseconds = 60
		iseconds =00
	ENDIF
	
	
	IF iseconds < 10
		GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
		SET_TEXT_COLOUR(iR, iG, iB, iA)
	ENDIF
	
	IF bFlash
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		SET_TEXT_COLOUR(iR, iG, iB, iA)
	ENDIF
	
	
	DISPLAY_TEXT_WITH_2_NUMBERS(0.080, 0.680, "RACE_TIME", iminutes, iseconds) 
ENDPROC

FUNC BOOL IS_THIS_LABEL_PLAYING(STRING sRootLabel)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRootLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	    
	    IF ARE_STRINGS_EQUAL(sRootLabel, txtRootLabel)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			IF NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
			AND NOT IS_PED_IN_MODEL(PLAYER_PED_ID(), RHINO)
				IF GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) >= 1
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
			IF NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())))
			AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) != RHINO
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()), TRUE)
			IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_INTERFERING_WITH_EVENT()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_INJURED(pedStag)
			RETURN TRUE
		ELSE
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedStag, PLAYER_PED_ID())
			OR IS_ENTITY_IN_WATER(pedStag)
				RETURN TRUE
			ENDIF
			IF bLeavingActivationArea
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_police", CONV_PRIORITY_AMBIENT_HIGH)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(vehWeddingCar)
		IF NOT IS_VEHICLE_DRIVEABLE(vehWeddingCar)
		OR IS_VEHICLE_STUCK_ON_ROOF(vehWeddingCar)
		OR IS_ENTITY_IN_WATER(vehWeddingCar)
//		OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehWeddingCar, PLAYER_PED_ID())
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			WAIT(0)
			IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagdwrcar", CONV_PRIORITY_AMBIENT_HIGH)
				RETURN TRUE
			ENDIF
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK)
			OR IS_PED_IN_MODEL(PLAYER_PED_ID(), TOWTRUCK2)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehWeddingCar)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagdwrcar", CONV_PRIORITY_AMBIENT_HIGH)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		WAIT(0)
		IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagdwrcar", CONV_PRIORITY_AMBIENT_HIGH)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedStag)
		IF NOT IS_ENTITY_AT_ENTITY(pedStag, PLAYER_PED_ID(), << 150, 150, 150 >>)
			IF NOT IS_ENTITY_DEAD(vehWeddingCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
					SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
				ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF ambdestStage >= ambwaitforstag
		
		IF bBestmanApproached
			IF IS_PED_INJURED(pedBestman)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBestman, PLAYER_PED_ID())
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehWeddingCar)
			IF NOT IS_ENTITY_AT_ENTITY(vehWeddingCar, PLAYER_PED_ID(), << 20, 20, 20 >>)
				IF NOT bSaidComeBackStag
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_comeba", CONV_PRIORITY_AMBIENT_HIGH)
						bSaidComeBackStag = TRUE
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@@@ stagd_comeba @@@@@@@@@@@@@@@@@@@@@@@\n")
					ENDIF
				ENDIF
			ELSE
				bSaidComeBackStag = FALSE
			ENDIF
			IF NOT IS_ENTITY_AT_ENTITY(vehWeddingCar, PLAYER_PED_ID(), << 150, 150, 150 >>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF ambdestStage = ambgotoChapel
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_INJURED(pedBride)
			OR IS_PED_INJURED(pedGuest)
				RETURN TRUE
			ELSE
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBride, PLAYER_PED_ID())
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedGuest, PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedBestman)
			IF NOT IS_ENTITY_AT_ENTITY(pedBestman, PLAYER_PED_ID(), << 20, 20, 20 >>)
			AND bSaidTimeOfWedding
				IF NOT bLeavingBestManBehind
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_nobest", CONV_PRIORITY_AMBIENT_HIGH)
						bLeavingBestManBehind = TRUE
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@@@ stagd_nobest @@@@@@@@@@@@@@@@@@@@@@@\n")
					ENDIF
				ENDIF
			ELSE
				bLeavingBestManBehind = FALSE
			ENDIF
			IF NOT IS_ENTITY_AT_ENTITY(pedBestman, PLAYER_PED_ID(), << 150, 150, 150 >>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_LABLE_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	    TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	    
	    IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
	          RETURN TRUE
	    ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC create_Stag_and_assets()

	//random_int = GET_RANDOM_INT_IN_RANGE(0,2)
	
//	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInput, << 150, 150, 150 >>)
		
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
		SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, TRUE)
		
		REQUEST_MODEL(U_M_Y_StagGrm_01) //stag
		REQUEST_MODEL(superd) //car
		REQUEST_MODEL(Prop_Stag_Do_Rope) //rope
		
		WHILE NOT HAS_MODEL_LOADED(U_M_Y_StagGrm_01)
		//OR NOT HAS_MODEL_LOADED(U_M_M_BankMan)
		OR NOT HAS_MODEL_LOADED(superd)
		OR NOT HAS_MODEL_LOADED(Prop_Stag_Do_Rope)
			WAIT(0)
		ENDWHILE
		
		REQUEST_ANIM_DICT("re@stag_do@")
		REQUEST_ANIM_DICT("re@stag_do@idle_a")
//		REQUEST_ANIM_DICT("re@stag_do@arrive_church")
		REQUEST_SCRIPT_AUDIO_BANK("ROPE_CUT")
		WHILE NOT HAS_ANIM_DICT_LOADED("re@stag_do@")
		OR NOT HAS_ANIM_DICT_LOADED("re@stag_do@idle_a")
//		OR NOT HAS_ANIM_DICT_LOADED("re@stag_do@arrive_church")
		OR NOT REQUEST_SCRIPT_AUDIO_BANK("ROPE_CUT")
			WAIT(0)
		ENDWHILE
		
//		ADD_RELATIONSHIP_GROUP("staggroup", stag_group)
		//voffset_temp = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vInput,0.0, <<-5.0,-5.0,-14.0>>)
		pedStag = CREATE_PED(PEDTYPE_MISSION, U_M_Y_StagGrm_01 , << -935.5700, 2767.6160, 24.4480 >>, 140.500)
		SET_AMBIENT_VOICE_NAME(pedStag, "GROOM")
		//pedStag = CREATE_PED(PEDTYPE_DEALER,U_M_Y_StagGrm_01 , <<-1102.27, 2691.31, 18.5422 >>, 229.6918)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedStag, RELGROUPHASH_PLAYER)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStag, TRUE)
//		FREEZE_ENTITY_POSITION(pedStag, TRUE)
		SET_PED_MONEY(pedStag, 0)
		SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_HAIR, 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_HEAD, 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_TORSO, 0, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_LEG, 0, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_JBIB, 1, 0, 0) //(accs)
		TASK_SET_DECISION_MAKER(pedStag, DECISION_MAKER_EMPTY)
		SET_PED_COMBAT_MOVEMENT(pedStag, CM_WILLRETREAT)
		SET_PED_CONFIG_FLAG(pedStag, PCF_ForcedToUseSpecificGroupSeatIndex, TRUE)
		SET_PED_CONFIG_FLAG(pedStag, PCF_PreventPedFromReactingToBeingJacked, TRUE)
		
		objRope = CREATE_OBJECT(Prop_Stag_Do_Rope, GET_ENTITY_COORDS(pedStag))
		ATTACH_ENTITY_TO_ENTITY(objRope, pedStag, GET_PED_BONE_INDEX(pedStag, BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
		
//		blipPersonGettingPlayersAttention = CREATE_AMBIENT_BLIP_INITIAL_ENTITY(pedStag)
//		REMOVE_BLIP(blipPersonGettingPlayersAttention)
//		blipPersonGettingPlayersAttention = CREATE_AMBIENT_BLIP_FOR_PED(pedStag)
		
		//create crowd
		vehWeddingCar = CREATE_VEHICLE(superd, << -2009.0146, 455.3556, 101.6528 >>, 274.8103, FALSE, FALSE)
		//FREEZE_ENTITY_POSITION(vehWeddingCar, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehWeddingCar, TRUE)
//		SET_VEHICLE_COLOURS(vehWeddingCar, 134, 0)
		SET_VEHICLE_DIRT_LEVEL(vehWeddingCar, 0)
		SET_VEHICLE_DOORS_LOCKED(vehWeddingCar, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		SET_VEHICLE_MOD_KIT(vehWeddingCar, 0)
		SET_VEHICLE_MOD_COLOR_1(vehWeddingCar, MCT_METALLIC, 73, 0)
		SET_VEHICLE_MOD_COLOR_2(vehWeddingCar, MCT_PEARLESCENT, 48)
		SET_VEHICLE_MOD(vehWeddingCar, MOD_ENGINE, 1)
		SET_VEHICLE_MOD(vehWeddingCar, MOD_BRAKES, 1)
		REQUEST_VEHICLE_ASSET(SUPERD)
		SET_ENTITY_LOD_DIST(vehWeddingCar, 10000)
		SET_VEHICLE_LOD_MULTIPLIER(vehWeddingCar, 5)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(vehWeddingCar)
		
		vTheChapel = << -352.88, 6164.31, 30.5 >>
		vTheChapel1 = << -355.2565, 6164.9306, 30.2944 >>
		vTheChapel2 = << -330.5575, 6139.8515, 34.4886 >>
		fTheChapel = 25.3750
		vChapelCenter = << -343.5804, 6156.1982, 30.4890 >>
		
		vStagsHouse = << -1991.1265, 457.9910, 101.1808 >>
		vStagsHouse1 = << -2002.6903, 460.5390, 99.8280 >>
		vStagsHouse2 = << -1997.6046, 445.6651, 105.0519 >>
		vStagsDoor = << -2026.8170, 450.3329, 104.8771 >>
		fStagsHouse = 24.687500
		
		vBestman = << -2205.3506, 4298.7896, 47.2650 >>
		vBestman1 = << -2207.5253, 4301.6967, 47.1803 >>
		vBestman2 = << -2195.6367, 4295.4067, 51.1889 >>
		fBestman = 12.5625
		
		//vBestman = <<382.80, 2643.84, 46.76>>
		
		vCurrentDestination = vStagsHouse //first place to go
		vPos1 = vStagsHouse1
		vPos2 = vStagsHouse2
		fWidth = fStagsHouse
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(SUPERD, TRUE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vChapelCenter - << 10, 10, 10 >>, vChapelCenter + << 10, 10, 10 >>, FALSE)
		ADD_SCENARIO_BLOCKING_AREA(vChapelCenter - << 20, 20, 20 >>, vChapelCenter + << 20, 20, 20 >>)
		ADD_SCENARIO_BLOCKING_AREA(GET_ENTITY_COORDS(pedStag, FALSE) - << 10, 10, 10 >>, GET_ENTITY_COORDS(pedStag, FALSE) + << 10, 10, 10 >>)
		ADD_SCENARIO_BLOCKING_AREA(<< -2007.7510, 455.1994, 101.6406 >> - << 15, 15, 15 >>, << -2007.7510, 455.1994, 101.6406 >> + << 15, 15, 15 >>)
		ADD_SCENARIO_BLOCKING_AREA(<< -2202.7468, 4299.0078, 47.4293>> - << 25, 25, 15 >>, << -2202.7468, 4299.0078, 47.4293>> + << 25, 25, 15 >>)
		CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(pedStag), 10)
		
		IF NOT IS_ENTITY_DEAD(pedStag)
			OPEN_SEQUENCE_TASK(seq)
//				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 80000, SLF_WHILE_NOT_IN_FOV)
				TASK_PLAY_ANIM(NULL, "re@stag_do@idle_a", "idle_a_ped", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(pedStag, seq)
			CLEAR_SEQUENCE_TASK(seq)
			SET_PED_KEEP_TASK(pedStag, TRUE)
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(cultresConversation, 0, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		ADD_PED_FOR_DIALOGUE(cultresConversation, 3, pedStag, "GROOM")
//		ADD_PED_FOR_DIALOGUE(cultresConversation, 4, pedBestman, "BESTMAN")
		bAssetsLoaded = TRUE
//	ENDIF
	
ENDPROC

PROC chapel_cutscene()
	
//	IF iCutsceneProgress != 0
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
//			WHILE IS_SCREEN_FADING_OUT()
//				WAIT(0)
//			ENDWHILE
//			iCutsceneTimer = GET_GAME_TIMER()
////			STOP_CUTSCENE()
//			bCutsceneSkipped = TRUE
//			iCutsceneProgress = 4
//		ENDIF
//	ENDIF
	
	SWITCH iCutsceneProgress
		CASE 0
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
//			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_pass", CONV_PRIORITY_AMBIENT_HIGH)
//			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_FRONTEND_RADIO_ACTIVE(FALSE)
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			WAIT(500)
			bSaidTimeOfWedding = FALSE
//			CLEAR_HELP(TRUE)
//			DISPLAY_HUD(FALSE)
//			DISPLAY_RADAR(FALSE)
//			CLEAR_AREA_OF_PEDS(vChapelCenter, 25)
//			
//			handoverCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//			
//			//SET_CAM_COORD(handoverCam, << -333.2602, 6135.2603, 30.7778 >>)
//			//SET_CAM_ROT(handoverCam,<< 15.9618, 0.0000, 17.7652 >>)
//			//SET_CAM_FOV(handoverCam, 40)
//			
//			//	SET_CAM_COORD(handoverCam, << -361.2742, 6162.0151, 32.0547 >>)
//			//	SET_CAM_ROT(handoverCam,<< -0.6906, -0.0000, -106.5562 >>)
//			//	SET_CAM_FOV(handoverCam, 29.9)
//			
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				veh_ped_is_in = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				Set_Veh_Position(veh_ped_is_in, << -352.7711, 6159.4961, 31.4894 >>, 252.5146)
//			ENDIF
//			
//			SET_CAM_ACTIVE(handoverCam, TRUE)
//			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			
//			IF NOT IS_PED_INJURED(pedStag)
//			AND NOT IS_PED_INJURED(pedBestman)
//			AND NOT IS_ENTITY_DEAD(veh_ped_is_in)
//				sceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(veh_ped_is_in) - << 0, 0, 0.03 >>, GET_ENTITY_ROTATION(veh_ped_is_in))
//				handoverCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
////				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneId, veh_ped_is_in, 0)
//				
//				PLAY_SYNCHRONIZED_CAM_ANIM(handoverCam, sceneId, "sdrm_arrive_church_camera", "re@stag_do@arrive_church")
//				
//				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				CLEAR_PED_TASKS_IMMEDIATELY(pedStag)
//				
//				SET_ENTITY_COORDS(pedStag, GET_ENTITY_COORDS(veh_ped_is_in) + << 0, 0, 3 >>)
//				TASK_SYNCHRONIZED_SCENE(pedStag, sceneId, "re@stag_do@arrive_church", "sdrm_arrive_church_groom", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
//		//		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStag)
//				
//				SET_ENTITY_COORDS(pedBestman, GET_ENTITY_COORDS(veh_ped_is_in) + << 0, 0, 3 >>)
//				TASK_SYNCHRONIZED_SCENE(pedBestman, sceneId, "re@stag_do@arrive_church", "sdrm_arrive_church_bman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
//		//		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedBestman)
//				
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(veh_ped_is_in) + << 0, 0, 3 >>)
//				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "re@stag_do@arrive_church", "sdrm_arrive_church_two", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//		//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
//				
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(veh_ped_is_in, sceneId, "sdrm_arrive_church_car", "re@stag_do@arrive_church", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
//		//		PLAY_ENTITY_ANIM(veh_ped_is_in, "sdrm_arrive_church_car", "re@stag_do@arrive_church",  SLOW_BLEND_IN, FALSE, FALSE)
//				
//				SET_CAM_ACTIVE(handoverCam, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				bSetUpUntieScene = TRUE
//			ENDIF
//			
//		//	IF NOT IS_ENTITY_DEAD(pedStag)
//		//		OPEN_SEQUENCE_TASK(seq)
//		////			TASK_LEAVE_ANY_VEHICLE(NULL, 0)
//		//			TASK_GO_STRAIGHT_TO_COORD(NULL, <<-330.36, 6154.03, 31.8 >>, PEDMOVE_RUN, -1)
//		//			TASK_STAND_STILL(NULL, 100000000)
//		//		CLOSE_SEQUENCE_TASK(seq)
//		//		TASK_PERFORM_SEQUENCE(pedStag, seq)
//		//		SET_PED_KEEP_TASK(pedStag, TRUE)
//		//	ENDIF
//		//	
//		//	WAIT(500)
//		//	
//		//	IF NOT IS_PED_INJURED(pedBestman)
//		//		TASK_PERFORM_SEQUENCE(pedBestman, seq)
//		//		CLEAR_SEQUENCE_TASK(seq)
//		//		SET_PED_KEEP_TASK(pedBestman, TRUE)
//		//	ENDIF
//			
//				/*
//					CameraSet.StartCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
//					SET_CAM_COORD(CameraSet.StartCam,CameraSet.StartCamPos)
//					SET_CAM_ROT(CameraSet.StartCam, CameraSet.StartCamRot)
//					SET_CAM_FOV(CameraSet.StartCam, CameraSet.CamFov)
//					PLAY_SYNCHRONIZED_CAM_ANIM( CameraSet.StartCam, sceneId, AnimName, AnimDict) 
//					SET_CAM_ACTIVE(CameraSet.StartCam, true)*/
//					
//					
//				//SET_CAM_ACTIVE_WITH_INTERP(handoverDestCam, handoverCam, 5000)
//			
//			iCutsceneTimer = GET_GAME_TIMER() + 4000
			iCutsceneProgress ++
		BREAK
		
		CASE 1
//			IF iCutsceneTimer < GET_GAME_TIMER()
//				IF NOT IS_ENTITY_DEAD(pedBride)
//				AND NOT IS_ENTITY_DEAD(pedStag)
//					TASK_TURN_PED_TO_FACE_ENTITY(pedBride, pedStag)
//				ENDIF
//				
//			//	SET_CAM_COORD(handoverCam, << -329.5621, 6153.1973, 32.9069 >>)
//			//	SET_CAM_ROT(handoverCam,<< -6.8303, 0.0000, 70.9757 >>)
//			//	SET_CAM_FOV(handoverCam, 45.0)
//			
//				iCutsceneTimer = GET_GAME_TIMER() +10000 //2000
				iCutsceneProgress ++
//			ENDIF
		BREAK
		
		CASE 2
//			IF iCutsceneTimer < GET_GAME_TIMER()
				IF NOT IS_PED_INJURED(pedStag)
				AND NOT IS_PED_INJURED(pedBestman)
				AND NOT IS_PED_INJURED(pedBride)
				AND NOT IS_PED_INJURED(pedGuest)
				AND NOT IS_ENTITY_DEAD(veh_ped_is_in)
				
//					REQUEST_CUTSCENE("sdrm_mcs_2_p2")
					
//					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//	//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
//						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Groom", pedStag)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Groomsman", pedBestman)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Bride", pedBride)
//						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Father_of_the_bride", pedGuest)
//					ENDIF
					
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
//						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "TREVOR", CU_DONT_ANIMATE_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(veh_ped_is_in, "SDRM_Car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedStag, "Groom", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedBestman, "Groomsman", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedBride, "Bride", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedGuest, "Father_of_the_bride", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						START_CUTSCENE(CUTSCENE_DO_NOT_REPOSITION_PLAYER_TO_SCENE_ORIGIN)
						
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(veh_ped_is_in, FALSE) + << 5, 0, 0 >>, FALSE)
//						VEHICLE_INDEX vehTemp
//						vehTemp = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("SDRM_Car")
//						pedPlayerTemp = CREATE_PED_INSIDE_VEHICLE(veh_ped_is_in, PEDTYPE_MISSION, GET_PLAYER_MODEL())
//						PED_VARIATION_STRUCT pedVariationStruct
//						GET_PED_VARIATIONS(PLAYER_PED_ID(), pedVariationStruct)
//						SET_PED_VARIATIONS(pedPlayerTemp, pedVariationStruct)
						iCutsceneProgress ++
					ENDIF
				ENDIF
//			ENDIF
		BREAK
		
		CASE 3
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR")
//				IF DOES_ENTITY_EXIST(pedPlayerTemp)
//					DELETE_PED(pedPlayerTemp)
//				ENDIF
//			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//			IF HAS_CUTSCENE_FINISHED()
				PRINTSTRING("\n@@@@@@@@@@@@@@ IF HAS_CUTSCENE_FINISHED() @@@@@@@@@@@@@@@@@\n")
				IF NOT IS_ENTITY_DEAD(veh_ped_is_in)
//					Set_Veh_Position(veh_ped_is_in, << -352.7711, 6159.4961, 31.4894 >>, 252.5146)
					SET_VEHICLE_ON_GROUND_PROPERLY(veh_ped_is_in)
					SET_VEHICLE_DOORS_SHUT(veh_ped_is_in)
//					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), veh_ped_is_in)
//					STOP_SYNCHRONIZED_ENTITY_ANIM(veh_ped_is_in, NORMAL_BLEND_OUT, TRUE)
	//				WAIT(0)
					DISPLAY_HUD(TRUE)
					DISPLAY_RADAR(TRUE)
					PRINTLN("@@@@@@@@@ DISPLAY_HUD 1 @@@@@@@@@")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
				ENDIF
				iCutsceneProgress ++
			ENDIF
		BREAK
		
		CASE 4
//			IF iCutsceneTimer < GET_GAME_TIMER()
				
//				REMOVE_CUTSCENE()
//				KILL_ANY_CONVERSATION()
//				IF NOT IS_CUTSCENE_ACTIVE()
					DELETE_PED(pedStag)
					DELETE_PED(pedBestman)
					DELETE_PED(pedBride)
					DELETE_PED(pedGuest)
					
//					IF DOES_CAM_EXIST(handoverCam)
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//						SET_CAM_ACTIVE(handoverCam, FALSE)
//						DESTROY_CAM(handoverCam)
						
//					ENDIF
					
//					CLEAR_PED_TASKS(PLAYER_PED_ID())
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_FRONTEND_RADIO_ACTIVE(TRUE)
//					IF bCutsceneSkipped
//						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//						WHILE IS_SCREEN_FADING_IN()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
					
					MISSION_PASSED()
//				ENDIF
//			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC makeStagTalkInCar()
	IF DOES_ENTITY_EXIST(pedStag)
		IF NOT IS_ENTITY_DEAD(pedStag)
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			IF IS_THIS_LABEL_PLAYING("stagd_chape_1")
				IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
					blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
				ENDIF
				bBestmansLocationRevealed = TRUE
			ENDIF
			IF IS_THIS_LABEL_PLAYING("stagd_chape_3")
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(pedStag)
					IF IS_PED_IN_ANY_VEHICLE(pedStag)
						OPEN_SEQUENCE_TASK(seq)
							TASK_CLEAR_LOOK_AT(NULL)
							TASK_USE_MOBILE_PHONE_TIMED(NULL, 30600) //31800
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedStag, seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
			ENDIF
//			IF IS_THIS_LABEL_PLAYING("stagd_chape_9")
//				IF NOT IS_PED_INJURED(pedStag)
//					TASK_USE_MOBILE_PHONE(pedStag, FALSE)
//				ENDIF
//			ENDIF
			
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 13.5, 13.5, 13.5 >>)
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(pedStag, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					
					IF ambdestStage = ambgogetclothes
						IF TIMERB() > 5000
							IF conversation_index = 0
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//								LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()
									// CREATE_CONVERSATION(cultresConversation, "stagdau", s_time_to_get_to_chapel, CONV_PRIORITY_AMBIENT_HIGH)
									
									KILL_ANY_CONVERSATION()
//									WAIT(0)
									//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", s_time_to_get_to_chapel)
									
									IF CREATE_CONVERSATION(cultresConversation, "stagdau", s_time_to_get_to_chapel, CONV_PRIORITY_AMBIENT_HIGH)
										bSaidTimeOfWedding = TRUE
									ENDIF
									
									bTimeToGetToChapelMentioned = TRUE
									conversation_index++
									SETTIMERB(0)
								ENDIF
							ENDIF
						ENDIF
						
						IF TIMERB() > 5000
							IF conversation_index = 1
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()
									
									DISPLAY_HUD(TRUE)
									DISPLAY_RADAR(TRUE)
									PRINTLN("@@@@@@@@@ DISPLAY_HUD 2 @@@@@@@@@")
									//IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailor", CONV_PRIORITY_AMBIENT_HIGH)
									//ENDIF
									
									conversation_index++
									SETTIMERB(0)
	//								HANG_UP_AND_PUT_AWAY_PHONE()
								ENDIF
							ENDIF
						ENDIF
						
						IF TIMERB() > 2500
							IF conversation_index = 2
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()
									
									KILL_ANY_CONVERSATION()
//									WAIT(0)
									
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailM", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailF", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailT", CONV_PRIORITY_AMBIENT_HIGH)
									ENDIF
									
									conversation_index++
									SETTIMERB(0)
									
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
					
					IF NOT bBrideApproached
						IF ambdestStage = ambgotoChapel
							//IF TIMERB() > 5000
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF conversation_index = 0
								
	//								CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_bante1", CONV_PRIORITY_AMBIENT_HIGH)
									CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_enough", CONV_PRIORITY_AMBIENT_HIGH)
									
									conversation_index++
								ENDIF
							ENDIF
						ENDIF
						
						IF ambdestStage = ambgotoChapel
							IF TIMERB() > 5000
								IF conversation_index = 0
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_hairc")
										conversation_index++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						
						//time banter
						
						IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vChapelCenter, << 150, 150, 150 >>)
							IF f_stag_dialogue_warning_on_time = 0
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 11
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time10", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 1
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 2
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 10
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time9", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 2
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 3
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 9
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time8", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 3
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 4
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 8
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time7", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 4
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 5
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 7
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			//							CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time6", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 5
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 6
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 6
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time5", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 6
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 7
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 5
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time4", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 7
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 8
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 4
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time3", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 8
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 9
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 3
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time2", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 9
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 10
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 2
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time1", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 10
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 11
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 1
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_time0", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time = 11
										PRINTSTRING("\n TRIGGER TIME WARNING \N ")
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF f_stag_dialogue_warning_on_time < 6
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 6
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 7
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 5
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 8
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 4
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 9
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 3
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 10
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 2
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
							
							IF f_stag_dialogue_warning_on_time < 11
								IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 1
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_stress", CONV_PRIORITY_AMBIENT_HIGH)
										f_stag_dialogue_warning_on_time ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
	
PROC checkWeddingStillOnTime()

	IF GET_CLOCK_HOURS() > 2
	AND GET_CLOCK_HOURS() < 15
	AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "AMBIENT SCRIPT CAN RUN: Stag Man only runs between 6am and 3pm", 4000, 1)
	ELSE
		PRINTLN("Only runs between 3am and 3pm @@@@@@@@@@@@@@@@")
		
		IF NOT IS_PED_INJURED(pedStag)
			CLEAR_PED_TASKS(pedStag)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			WAIT(0)
			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_failda", CONV_PRIORITY_AMBIENT_HIGH)
			IF DOES_ENTITY_EXIST(objRope)
				DETACH_ENTITY(objRope)
			ENDIF
		ENDIF
		
		WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			WAIT(0)
		ENDWHILE
		
		MISSION_FAILED()
	ENDIF
	
ENDPROC

PROC countdownSound()
	INT iHours, iMinutes, iSeconds
	iHours = GET_CLOCK_HOURS()
	iMinutes = GET_CLOCK_MINUTES()
	iSeconds = GET_CLOCK_SECONDS()

	IF iCountdownAudioEvent = 0
		IF iMinutes = 55 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 1
		IF iMinutes = 56 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 2
		IF iMinutes = 56 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 3
		IF iMinutes = 57 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 4
		IF iMinutes = 57 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 5
		IF iMinutes = 58 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 6
		IF iMinutes = 58 AND iSeconds >= 15
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 7
		IF iMinutes = 58 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 8
		IF iMinutes = 58 AND iSeconds >= 45
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 9
		IF iMinutes = 59 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 10
		IF iMinutes = 59 AND iSeconds >= 15
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 11
		IF iMinutes = 59 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 12
		IF iMinutes = 59 AND iSeconds >= 45
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 13
		IF iMinutes >= 0 AND iHours = iWeddingHour
			PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ENDIF
	PRINTLN("@@@@@@@@@@@@@@@ iCountdownAudioEvent:", iCountdownAudioEvent, " iHours:", iHours, " iMinutes:", iMinutes, " iSeconds:", iSeconds, " @@@@@@@@@@@@@@@@@@@")

ENDPROC

PROC countdownToWedding()
	
	IF bSaidTimeOfWedding
	AND iWeddingHour != GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
	AND NOT DOES_CAM_EXIST(handoverCam1)
//		iCurrentGameTimeInMilliseconds = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000
//		iCurrentGameTimeInMilliseconds += GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60
		
//		IF iCurrentGameTimeInMilliseconds > 719999 // IF the time is more than 11:59(am)
//			IF iCurrentGameTimeInMilliseconds > 779999 // IF the time is more than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds - 720000, "", 0, TIMER_STYLE_CLOCKPM) // show evrything from 13:00 to 23:59 - 12:00 = as 01:00(pm) to 11:59(pm)
//			ELSE // IF the time is more than 11:59(am) but less than 12.59(pm)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "", 0, TIMER_STYLE_CLOCKPM) // show as 24-hour clock (normal)
//			ENDIF
//		ELSE // ELSE the time is less than 12:00(pm)
//			IF iCurrentGameTimeInMilliseconds < 60000 // IF the time is less than 01:00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds + 720000, "", 0, TIMER_STYLE_CLOCKAM) // show evrything from 00:00(am) to 00:59(am) + 12:00(am) = as 12:00(am) to 12.59
//			ELSE // IF the time is less than 12:00(pm) but more than 01.00(am)
//				DRAW_GENERIC_TIMER(iCurrentGameTimeInMilliseconds, "", 0, TIMER_STYLE_CLOCKAM) // show as 24-hour clock (normal)
//			ENDIF
//		ENDIF
		
		DRAW_GENERIC_TIMER((iWeddingHour *1000 *60 - GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())*1000 * 60) + (iWeddingMin *1000 - GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())*1000), "TIMER_TIME")//, 0, TIMER_STYLE_DONTUSEMILLISECONDS, 60000)
		
	//	PRINTNL()
	//	PRINTINT(iTimeLeftFlight)
	//	PRINTNL()
//		PRINTNL()
//		PRINTINT(GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())+1)PRINTSTRING(":")PRINTINT(GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())-30)
//		PRINTNL()
//		PRINTNL()
//		PRINTINT(iWeddingHour)PRINTSTRING(":")PRINTINT(iWeddingMin)
//		PRINTNL()
	ENDIF
	
//	IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 1
//	AND iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
//	AND NOT bToldToHurryOnce
//		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		IF CREATE_CONVERSATION(HitcherDialogueStruct, "REHH1AU", "REHH1_1HOUR", CONV_PRIORITY_AMBIENT_HIGH)
//			bToldToHurryOnce = TRUE
//		ENDIF
//	ENDIF
//	
//	IF iWeddingMin = 30
//		IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
//		AND iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 30
//		AND NOT bToldToHurryTwice
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iSoundCountdown = GET_SOUND_ID()
//			IF CREATE_CONVERSATION(HitcherDialogueStruct, "REHH1AU", "REHH1_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
//				PLAY_SOUND_FRONTEND(iSoundCountdown, "VARIABLE_COUNTDOWN_CLOCK_wp")
//				SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fadein", 35)
//				bToldToHurryTwice = TRUE
//			ENDIF
//		ENDIF
//		IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
//		AND (iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 5
//		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 4
//		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 3
//		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 2
//		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) + 1)
//			countdownSound()
//		ENDIF
//	ENDIF
	
	IF iWeddingMin = 0
//		IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 1
//		AND iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 30
//		AND NOT bToldToHurryTwice
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//			iSoundCountdown = GET_SOUND_ID()
//			IF CREATE_CONVERSATION(HitcherDialogueStruct, "REHH1AU", "REHH1_HHOUR", CONV_PRIORITY_AMBIENT_HIGH)
//				PLAY_SOUND_FRONTEND(iSoundCountdown, "VARIABLE_COUNTDOWN_CLOCK_wp")
//				SET_VARIABLE_ON_SOUND(iSoundCountdown, "countdown_fadein", 35)
//				bToldToHurryTwice = TRUE
//			ENDIF
//		ENDIF
		IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY()) + 1
		AND (iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 55
		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 56
		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 57
		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 58
		OR iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY()) - 59)
			countdownSound()
		ENDIF
	ENDIF
	
	IF iWeddingHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
//	AND iWeddingMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
		countdownSound()
		KILL_FACE_TO_FACE_CONVERSATION()
//		WAIT(0)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_failti", CONV_PRIORITY_AMBIENT_HIGH)
			//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Shit! Times up! We're too late. No!!", 4000, 1) //
			//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_failti")
			IF NOT IS_PED_INJURED(pedStag)
				TASK_USE_MOBILE_PHONE(pedStag, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedBride)
				TASK_SMART_FLEE_PED(pedBride, PLAYER_PED_ID(), 300, -1)
			ENDIF
			IF NOT IS_PED_INJURED(pedGuest)
				TASK_SMART_FLEE_PED(pedGuest, PLAYER_PED_ID(), 300, -1)
			ENDIF
			
			WAIT(8500)
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 3)
				ENDIF
			ENDIF
			
			WAIT(2000)
						
			IF NOT IS_ENTITY_DEAD(pedStag)
				CLEAR_PED_TASKS(pedStag)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, 5000, SLF_WHILE_NOT_IN_FOV)
				TASK_LOOK_AT_ENTITY(pedStag, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
				TASK_LEAVE_ANY_VEHICLE(pedStag, 0)
				REMOVE_PED_FROM_GROUP(pedStag)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedBestman)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, 5000, SLF_DEFAULT)
				TASK_LEAVE_ANY_VEHICLE(pedBestman, 500)
			ENDIF
			
			WAIT(1000)
			
			IF NOT IS_ENTITY_DEAD(vehWeddingCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedStag)
			AND NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
//				SET_PED_RELATIONSHIP_GROUP_HASH(pedStag, RELGROUPHASH_CIVMALE)
				OPEN_SEQUENCE_TASK(seq)
					IF GET_PLAYERS_LAST_VEHICLE() = vehWeddingCar
						//TASK_LEAVE_ANY_VEHICLE(NULL, 0)
						TASK_ENTER_VEHICLE(NULL, vehWeddingCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_JACK_ANYONE)
						//TASK_WANDER_STANDARD(NULL)
						TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehWeddingCar, PLAYER_PED_ID(), MISSION_FLEE, 15, DRIVINGMODE_AVOIDCARS, 10, 10)
					ELSE
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
					ENDIF
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedStag, seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_PED_KEEP_TASK(pedStag, TRUE)
				
				IF NOT IS_ENTITY_DEAD(pedBestman)
//					SET_PED_RELATIONSHIP_GROUP_HASH(pedBestman, RELGROUPHASH_CIVMALE)
					OPEN_SEQUENCE_TASK(seq)
						IF GET_PLAYERS_LAST_VEHICLE() = vehWeddingCar
							TASK_ENTER_VEHICLE(NULL, vehWeddingCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT)
							TASK_PAUSE(NULL, 100000)
						ELSE
							TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 300, -1)
						ENDIF
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedBestman, seq)
					CLEAR_SEQUENCE_TASK(seq)
					SET_PED_KEEP_TASK(pedBestman, TRUE)
//					SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(pedBestman, TRUE)
				ENDIF
			ENDIF
			
//			WAIT(3000)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			PRINTNL()PRINTINT(TIME_LEFT_STAG)
//			PRINTNL()PRINTINT(end_hours)
//			PRINTNL()PRINTINT(current_hours)
			
			PRINTSTRING("\n out of time no 1")
			
			WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				WAIT(0)
			ENDWHILE
			
			MISSION_FAILED()
		ENDIF
	ENDIF
ENDPROC
//
//PROC countdown_to_wedding()
//
//	CURRENT_TIME_STAG = GET_GAME_TIMER()
//	TIME_LEFT_STAG = END_TIME_STAG - CURRENT_TIME_STAG
//	#IF IS_DEBUG_BUILD
//		IF DISPLAY_TIMER = FALSE
//			IF IS_KEYBOARD_KEY_PRESSED(KEY_T)
//				DISPLAY_TIMER = TRUE
//			ENDIF
//		ENDIF
//		
//		
//		IF DISPLAY_TIMER = TRUE
//			DrawCountdown(TIME_LEFT_STAG, TRUE)
//			
//			IF IS_KEYBOARD_KEY_PRESSED(KEY_T)
//				DISPLAY_TIMER = FALSE
//			ENDIF
//		ENDIF
//		
//	#ENDIF
//	
//	current_hours = GET_CLOCK_HOURS()
//	
//	
//	
//	IF TIME_LEFT_STAG < 0
//	OR end_hours = current_hours
//		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Shit! Times up! We're too late. No!!", 4000, 1) //
//		//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_failti")
//		CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_failti", CONV_PRIORITY_AMBIENT_HIGH)
//		
//		
//		
//		WAIT(2000)
//		
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		WAIT(2000)
//		TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
//		
//		
//		
//		IF NOT IS_ENTITY_DEAD(pedStag)
//			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, 20000, SLF_DEFAULT)
//			TASK_LOOK_AT_ENTITY(pedStag, PLAYER_PED_ID(), 20000, SLF_DEFAULT)
//			TASK_LEAVE_ANY_VEHICLE(pedStag, 0)
//			REMOVE_PED_FROM_GROUP(pedStag)
//		ENDIF
//		
//		IF NOT IS_ENTITY_DEAD(pedBestman)
//			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, 20000, SLF_DEFAULT)
//			TASK_LEAVE_ANY_VEHICLE(pedBestman, 0)
//		ENDIF
//		
//		
//		WAIT(2000)
//		
//		
//		IF NOT IS_ENTITY_DEAD(pedStag)
//		AND NOT IS_ENTITY_DEAD(vehWeddingCar)
//			
//			OPEN_SEQUENCE_TASK(seq)
//				//TASK_LEAVE_ANY_VEHICLE(NULL, 0)
//				TASK_ENTER_VEHICLE(NULL, vehWeddingCar, -1, VS_DRIVER)
//				//TASK_WANDER_STANDARD(NULL)
//				TASK_VEHICLE_MISSION_PED_TARGET(NULL, vehWeddingCar, PLAYER_PED_ID(), MISSION_FLEE, 15.0, DRIVINGMODE_AVOIDCARS, 10.0, 10.0)
//			CLOSE_SEQUENCE_TASK(seq)
//			TASK_PERFORM_SEQUENCE(pedStag, seq)
//			CLEAR_SEQUENCE_TASK(seq)
//			SET_PED_KEEP_TASK(pedStag,TRUE)
//			
//			IF NOT IS_ENTITY_DEAD(pedBestman)
//				TASK_ENTER_VEHICLE(pedBestman,vehWeddingCar, -1, VS_FRONT_RIGHT)
//				
//				SET_PED_KEEP_TASK(pedBestman,TRUE)
//				SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(pedBestman, TRUE)
//			ENDIF
//		ENDIF
//		
//		WAIT(3000)
//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		PRINTNL()
//		PRINTINT(TIME_LEFT_STAG)
//		PRINTNL()
//		PRINTINT(end_hours)
//		PRINTNL()
//		PRINTINT(current_hours)
//		
//		PRINTSTRING("\n out of time no 1")
//		MISSION_FAILED()
//	ENDIF
//	
//	
//	
//ENDPROC

PROC makingStagReactToEvents()

	IF NOT IS_PED_INJURED(pedStag)
		IF NOT IS_ENTITY_DEAD(vehWeddingCar)
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_PED_IN_VEHICLE(pedStag, vehWeddingCar)
			AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehWeddingCar)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("\n Car has just been damaged. Guy should complain. \n")
					IF GET_RANDOM_BOOL()
						CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_crash", CONV_PRIORITY_AMBIENT_HIGH)
					ELSE
						CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_bump1", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
				ENDIF
			ELIF IS_PED_IN_VEHICLE(pedStag, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("\n Car has just been damaged. Guy should complain. \n")
					CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_bump1", CONV_PRIORITY_AMBIENT_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC carsHonkHorn()
	
	IF NOT IS_PED_INJURED(pedStag)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_ENTITY_A_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			ENDIF
		ENDIF
		vehicleCandidate = GET_RANDOM_VEHICLE_IN_SPHERE(GET_ENTITY_COORDS(pedStag, FALSE), 30, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
		
		IF NOT IS_ENTITY_DEAD(vehicleCandidate)
			IF NOT IS_VEHICLE_SEAT_FREE(vehicleCandidate)
				IF IS_ENTITY_AT_ENTITY(vehicleCandidate, pedStag, << 30, 30, 30 >>)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleCandidate)
					IF IS_VEHICLE_DRIVEABLE(vehicleCandidate)
						START_VEHICLE_HORN(vehicleCandidate, 3000)
						IF NOT IS_PED_INJURED(pedStag)
							PRINTSTRING("\n @@@@@@@@@@@@@@@@@ TASK_LOOK_AT_ENTITY @@@@@@@@@@@@@@@@@@@@\n")
//							TASK_LOOK_AT_ENTITY(pedStag, vehicleCandidate, 3000, SLF_WHILE_NOT_IN_FOV)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT bPlayerCloseToStag
								CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_greet2", CONV_PRIORITY_AMBIENT_HIGH)
							ENDIF
	//						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleCandidate)
						ENDIF
					ENDIF
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleCandidate)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

PROC checkIfPlayerIsCloseStag()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(pedStag)
		IF NOT bPlayerCloseToStag
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 130, 60, 50 >>)
			AND NOT IS_ENTITY_OCCLUDED(pedStag)
			OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 10, 10, 10 >>)
//				REMOVE_BLIP(blipPersonGettingPlayersAttention)
				IF NOT DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
					blipPersonGettingPlayersAttention = CREATE_BLIP_FOR_PED(pedStag)
					SHOW_HEIGHT_ON_BLIP(blipPersonGettingPlayersAttention, FALSE)
	//				blip_person_to_save = CREATE_AMBIENT_BLIP_FOR_PED(pedStag)
	//				SET_BLIP_AS_FRIENDLY(blip_person_to_save, TRUE)
					TASK_LOOK_AT_ENTITY(pedStag, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
				ENDIF
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 10, 10, 10 >>)
					IF DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
						SHOW_HEIGHT_ON_BLIP(blipPersonGettingPlayersAttention, TRUE)
					ENDIF
					SETTIMERB(0)
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "You have to help me. Im getting married! Untie me!", 4000, 1) //
					KILL_FACE_TO_FACE_CONVERSATION()
					WHILE IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						WAIT(0)
					ENDWHILE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_greetM", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_greetF", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_greetT", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
					bPlayerCloseToStag = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(pedStag)
				IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), << -1131.777, 2636.418, 15.589 >>, << -1040.995, 2717.077, 31.715 >>, 60.75)
				AND DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_greet2", CONV_PRIORITY_AMBIENT_HIGH)
					ENDIF
//					REMOVE_BLIP(blipPersonGettingPlayersAttention)
				//	SET_BLIP_SPRITE(blipPersonGettingPlayersAttention, RADAR)
					//SPRITE
//					blipPersonGettingPlayersAttention = CREATE_AMBIENT_BLIP_FOR_PED(pedStag)
//					SET_BLIP_AS_FRIENDLY(blipPersonGettingPlayersAttention, TRUE)
//					SET_BLIP_SCALE(blipPersonGettingPlayersAttention, 0.6)
					IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
						vehImpound = GET_PLAYERS_LAST_VEHICLE()
					ENDIF
					SET_RANDOM_EVENT_ACTIVE()
				ENDIF
			ENDIF
		ELSE
			IF NOT bStagIgnoredByPlayer
				IF NOT IS_PED_INJURED(pedStag)
					IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 25, 25, 25 >>, FALSE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						WAIT(0)
					//make guy shout if player ignores guy
					
					//	IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stag_leave", CONV_PRIORITY_AMBIENT_HIGH)
					//	ENDIF
					//	PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_leave")
						CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_leave", CONV_PRIORITY_AMBIENT_HIGH)
						
						bStagIgnoredByPlayer = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC checkIfPlayerUntiesStag()
	IF DOES_ENTITY_EXIST(pedStag)
		IF NOT IS_PED_INJURED(pedStag)
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedStag), 3)
				CLEAR_PED_TASKS_IMMEDIATELY(pedStag)
				SET_ENTITY_HEALTH(pedStag, 99)
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
				IF GET_ENTITY_SPEED(GET_PLAYERS_LAST_VEHICLE()) < 3
					FREEZE_ENTITY_POSITION(pedStag, TRUE)
				ELSE
					FREEZE_ENTITY_POSITION(pedStag, FALSE)
					IF IS_ENTITY_TOUCHING_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedStag)
						CLEAR_PED_TASKS_IMMEDIATELY(pedStag)
						SET_ENTITY_HEALTH(pedStag, 99)
					ENDIF
				ENDIF
			ENDIF
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 2, 2, 2 >>, FALSE)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF CAN_PLAYER_START_CUTSCENE()
						IF NOT bSetUpUntieScene
							SET_RANDOM_EVENT_ACTIVE()
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
							CLEAR_HELP(TRUE)
							DISPLAY_HUD(FALSE)
							DISPLAY_RADAR(FALSE)
							PRINTLN("@@@@@@@@@ DISPLAY_HUD 3 @@@@@@@@@")
	//						SET_ENTITY_COORDS(pedVictim, vCreateVictim)
	//						SET_ENTITY_COORDS(PLAYER_PED_ID(), vCreateVictim)
							
//							VECTOR scenePosition
//							FLOAT sceneHeading
							
							IF NOT IS_PED_INJURED(pedStag)
//								scenePosition = GET_ENTITY_COORDS(pedStag)
//								sceneHeading = GET_ENTITY_HEADING(pedStag)
								CLEAR_AREA(GET_ENTITY_COORDS(pedStag), 5, TRUE)
								REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5)
//								REMOVE_DECALS_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5)
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
								IF IS_ENTITY_AT_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedStag, << 5, 5, 5 >>)
									SET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE(), GET_ENTITY_COORDS(pedStag) - << 3, 4, 1.5 >>)
									SET_VEHICLE_ON_GROUND_PROPERLY(GET_PLAYERS_LAST_VEHICLE())
								ENDIF
							ENDIF
							
//							WAIT(0)
							
							IF NOT IS_PED_INJURED(pedStag)
								sceneId = CREATE_SYNCHRONIZED_SCENE(<< -935.5700, 2767.6160, 24.4480 >>, << 2.433, 0.152, 53.743 >>)//<< scenePosition.x +0.2, scenePosition.y, (scenePosition.z -0.97) >>, << 3, 0, 280 >>)
								handoverCam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
								SET_SYNCHRONIZED_SCENE_PHASE(sceneId, 0.158)
								
								PLAY_SYNCHRONIZED_CAM_ANIM(handoverCam1, sceneId, "untie_cam", "re@stag_do@")
								
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								CLEAR_PED_TASKS_IMMEDIATELY(pedStag)
								
								TASK_SYNCHRONIZED_SCENE(pedStag, sceneId, "re@stag_do@", "untie_ped", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStag)
								
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, "re@stag_do@", "untie_player", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								
//								DELETE_OBJECT(objRope)
								SET_CAM_ACTIVE(handoverCam1, TRUE)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								bSetUpUntieScene = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("\n  victum died1")
			MISSION_FAILED()
		ENDIF
	ENDIF
	
	IF bSetUpUntieScene
	AND NOT bSetUpUntieSpeech
	
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		WAIT(0)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_untieM", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_untieF", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_untieT", CONV_PRIORITY_AMBIENT_HIGH)
		ENDIF
		bSetUpUntieSpeech = TRUE
	ENDIF
	
	IF bSetUpUntieScene
	AND NOT bUntieSceneEnding
		IF NOT bSoundPlayed
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.2
			AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) < 0.3
				PLAY_SOUND_FROM_ENTITY(-1, "ROPE_CUT", PLAYER_PED_ID(), "ROPE_CUT_SOUNDSET")
				bSoundPlayed = TRUE
			ENDIF
		ENDIF
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.4647058823529412
		AND NOT bPushInToFirstPerson
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bPushInToFirstPerson = TRUE
			ENDIF
		ENDIF
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.5//0.851
			bUntieSceneEnding = TRUE
		ENDIF
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
//			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			WHILE IS_SCREEN_FADING_OUT()
				WAIT(0)
			ENDWHILE
			bUntieSceneEnding = TRUE
			bCutsceneSkipped = TRUE
		ENDIF
	ENDIF
	
	IF bUntieSceneEnding
	AND NOT bCleanUpUntieScene
		IF DOES_ENTITY_EXIST(objRope)
			DETACH_ENTITY(objRope)
		ENDIF
		
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -936.6537, 2767.4968, 24.4289 >>)//<< -936.3569, 2763.9961, 24.0906 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 208.4017)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
		
		SET_CAM_ACTIVE(handoverCam1, FALSE)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_CAM(handoverCam1)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		PRINTLN("@@@@@@@@@ DISPLAY_HUD 4 @@@@@@@@@")
		
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << 162.5284, 6839.6582, 18.8816 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 256.7518)
		IF NOT IS_PED_INJURED(pedStag)
			FREEZE_ENTITY_POSITION(pedStag, FALSE)
			CLEAR_PED_TASKS_IMMEDIATELY(pedStag)
			SET_PED_CAN_RAGDOLL(pedStag, TRUE)
			SET_ENTITY_COORDS(pedStag, << -935.6716, 2767.7717, 24.4289 >>) //<< -936.1550, 2766.0479, 24.2598 >>)
			SET_ENTITY_HEADING(pedStag, 136.9073)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStag)
//			SET_ENTITY_HEADING(pedStag, 79.2867)
		ENDIF
		
		IF bCutsceneSkipped
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			WHILE IS_SCREEN_FADING_IN()
				WAIT(0)
			ENDWHILE
			bCutsceneSkipped = FALSE
		ENDIF
		
		bPushInToFirstPerson = FALSE
		bCleanUpUntieScene = TRUE
	ENDIF
	
	IF bCleanUpUntieScene
	AND NOT IS_PED_INJURED(pedStag)
//		REMOVE_BLIP(blip_person_to_save)
//		REMOVE_BLIP(blipPersonGettingPlayersAttention)
		
		IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			IF IS_ENTITY_AT_ENTITY(pedStag, GET_PLAYERS_LAST_VEHICLE(), << 20, 20, 20 >>)
			AND GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_PLAYERS_LAST_VEHICLE()) >= 1
			AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
			AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
			AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
			AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
			AND GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) != RHINO
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 6500)
					TASK_ENTER_VEHICLE(NULL, GET_PLAYERS_LAST_VEHICLE(), DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedStag, seq)
				CLEAR_SEQUENCE_TASK(seq)
			ELIF NOT IS_PED_IN_GROUP(pedStag)
//				CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_nogood", CONV_PRIORITY_AMBIENT_HIGH)
				SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
				SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
			ENDIF
		ELIF NOT IS_PED_IN_GROUP(pedStag)
			SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
			SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
			SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
		ENDIF
		
		//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Help me! Im getting married and i should be at the chapel within 12 minutes. Can you take me there?!", 4000, 1) //
//		FREEZE_ENTITY_POSITION(pedStag, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStag, FALSE)
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
		bStagsBlipRemoved = TRUE
		bLeavingActivationArea = TRUE
		
		REMOVE_ANIM_DICT("re@stag_do@")
		REMOVE_ANIM_DICT("re@stag_do@idle_a")
		
		
//		iStartTimeFlight = GET_GAME_TIMER()
		iCurrHour = GET_TIMEOFDAY_HOUR(GET_CURRENT_TIMEOFDAY())
		iWeddingHour = (iCurrHour + 5)
		iCurrMin = GET_TIMEOFDAY_MINUTE(GET_CURRENT_TIMEOFDAY())
		IF iCurrMin >= 30
			// if gone half past the hour round up until the next hour
			iWeddingMin = 0
			iWeddingHour ++
//		ELSE
//			iWeddingMin = 30
		ENDIF
		
//		iTimeLimitFlight = (2*(120000/*4000*/)) // two game hours worth of milliseconds
//		IF iWeddingMin > 0
//			iTimeLimitFlight = (iTimeLimitFlight + 12000)
//		ENDIF
		
		IF iWeddingHour >24
			iWeddingHour -=24
		ENDIF
		
		IF iWeddingHour = 0
			s_time_to_get_to_chapel = "stagd_ptime0"
		ELIF iWeddingHour = 1
			s_time_to_get_to_chapel = "stagd_ptime1"
		ELIF iWeddingHour = 2
			s_time_to_get_to_chapel = "stagd_ptime2"
		ELIF iWeddingHour = 3
			s_time_to_get_to_chapel = "stagd_ptime3"
		ELIF iWeddingHour = 4
			s_time_to_get_to_chapel = "stagd_ptime4"			
		ELIF iWeddingHour = 5
			s_time_to_get_to_chapel = "stagd_ptime5"
		ELIF iWeddingHour = 6
			s_time_to_get_to_chapel = "stagd_ptime6"
		ELIF iWeddingHour = 7
			s_time_to_get_to_chapel = "stagd_ptime7"
		ELIF iWeddingHour = 8
			s_time_to_get_to_chapel = "stagd_ptime8"
		ELIF iWeddingHour = 9
			s_time_to_get_to_chapel = "stagd_ptime9"
		ELIF iWeddingHour = 10
			s_time_to_get_to_chapel = "stagd_ptim10"
		ELIF iWeddingHour = 11
			s_time_to_get_to_chapel = "stagd_ptim11"
		ELIF iWeddingHour = 12
			s_time_to_get_to_chapel = "stagd_ptim12"
		ELIF iWeddingHour = 13
			s_time_to_get_to_chapel = "stagd_ptim13"
		ELIF iWeddingHour = 14
			s_time_to_get_to_chapel = "stagd_ptim14"
		ELIF iWeddingHour = 15
			s_time_to_get_to_chapel = "stagd_ptim15"
		ELIF iWeddingHour = 16
			s_time_to_get_to_chapel = "stagd_ptim16"
		ELIF iWeddingHour = 17
			s_time_to_get_to_chapel = "stagd_ptim17"
		ELIF iWeddingHour = 18
			s_time_to_get_to_chapel = "stagd_ptim18"
		ELIF iWeddingHour = 19
			s_time_to_get_to_chapel = "stagd_ptim19"
		ELIF iWeddingHour = 20
			s_time_to_get_to_chapel = "stagd_ptim20"
		ELIF iWeddingHour = 21
			s_time_to_get_to_chapel = "stagd_ptim21"
		ELIF iWeddingHour = 22
			s_time_to_get_to_chapel = "stagd_ptim22"
		ELIF iWeddingHour = 23
			s_time_to_get_to_chapel = "stagd_ptim23"
		ENDIF
		
//		PRINTSTRING("\n TIME TO GET TO CHAPEL: ")PRINTINT(start_hours)PRINTNL()
		//PRINTINT(END_TIME_STAG)
		PRINTSTRING("\n STRING TO PRINT: ")PRINTSTRING(s_time_to_get_to_chapel)PRINTNL()
		//PRINTINT(END_TIME_STAG)
		
	ENDIF
	
ENDPROC

PROC getToDestinations()
	IF DOES_ENTITY_EXIST(pedStag)
		IF NOT IS_ENTITY_DEAD(pedStag)
			IF NOT IS_PED_INJURED(pedStag)
//				IF DOES_CAM_EXIST(handoverCam1)
					IF NOT bCameraActive
					AND bStagMadeToGoOutAndPerformSeq
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
						
						CAM_VIEW_MODE_CONTEXT activeViewModeContext
						CAM_VIEW_MODE activeViewMode
						
						activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
							
						IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT
							activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
							IF activeViewMode != CAM_VIEW_MODE_FIRST_PERSON
								//Disable look controls if not first person
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY)
							ENDIF
						ENDIF
						
						IF (IS_ENTITY_IN_ANGLED_AREA(pedStag, << -2002.0081, 455.5800, 101.0381 >>, << -2009.7613, 453.2027, 105.9135 >>, 12.4375) OR TIMERA() > 5000)
							bCameraActive = TRUE
							SETTIMERA(0)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							CLEAR_HELP(TRUE)
							DISPLAY_HUD(FALSE)
							DISPLAY_RADAR(FALSE)
							PRINTLN("@@@@@@@@@ DISPLAY_HUD 5 @@@@@@@@@")
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
							
						//	ENDIF
							REQUEST_VEHICLE_ASSET(SUPERD)
							WHILE NOT HAS_VEHICLE_ASSET_LOADED(SUPERD)
								WAIT(0)
							ENDWHILE
							
	//							WAIT(3000)
							
							handoverCam1 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -2014.228027,456.496124,103.552353 >>, << -6.509435,-0.037031,-95.987617 >>, 29.001434)
							handoverCam2 = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << -2014.333740,456.417999,103.315247 >>, << -6.198685,-0.037031,-100.573570 >>, 29.001434)
							SHAKE_CAM(handoverCam1, "HAND_SHAKE", 0.25)
							SHAKE_CAM(handoverCam2, "HAND_SHAKE", 0.25)
							
							SET_CAM_ACTIVE(handoverCam1, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							WAIT(1000)
							SET_CAM_ACTIVE_WITH_INTERP(handoverCam2, handoverCam1, 4000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_SIN_ACCEL_DECEL)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
	//							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//								IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	//									SET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-1991.8285, 462.2748, 101.5540>>)
	//									SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 193.4793)
	//									SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	//								ENDIF
	//							ENDIF
							
							CLEAR_AREA_OF_VEHICLES(vStagsDoor, 10)
							
							IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
								REQUEST_VEHICLE_HIGH_DETAIL_MODEL(GET_PLAYERS_LAST_VEHICLE())
								IF IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), vStagsDoor, << 8, 8, 8 >>)
									vehicleCandidate = GET_PLAYERS_LAST_VEHICLE()
									SET_ENTITY_AS_MISSION_ENTITY(vehicleCandidate)
									DELETE_VEHICLE(vehicleCandidate)
								ENDIF
							ENDIF
							
							//fade out of guy gettng changed
							
							
							//DO_SCREEN_FADE_OUT(1500)
							//WAIT(2500)
//							IF NOT IS_ENTITY_DEAD(pedStag)
//								voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedStag, << 0, 0, -1 >>)
								//DELETE_PED(pedStag)
//							ENDIF
							
							IF NOT IS_ENTITY_DEAD(vehWeddingCar)
								SET_VEHICLE_DOORS_LOCKED(vehWeddingCar, VEHICLELOCK_UNLOCKED)
								IF NOT DOES_BLIP_EXIST(blipWeddingCar)
									blipWeddingCar = CREATE_BLIP_FOR_VEHICLE(vehWeddingCar)
									SET_BLIP_AS_FRIENDLY(blipWeddingCar, TRUE)
								ENDIF
							ENDIF
							bPlayerInWeddingCar = FALSE
						ENDIF
					ENDIF
					
					IF bCameraActive
					AND DOES_CAM_EXIST(handoverCam1)
						IF TIMERA() > 6000 - 300
						AND NOT bPushInToFirstPerson
							IF (IS_PED_ON_FOOT(PLAYER_PED_ID()) AND GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
							OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
								ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
								PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
								bPushInToFirstPerson = TRUE
							ENDIF
						ENDIF
						IF TIMERA() > 6000
							PRINTLN("@@@@@@@@@@@@@@@@@@@@@ DOES_CAM_EXIST @@@@@@@@@@@@@@@@@@@@@@@@@@@")
	//						OPEN_SEQUENCE_TASK(seq)
	//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vStagsDoor, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP)
	//							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
	//							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
	//						CLOSE_SEQUENCE_TASK(seq)
	//						TASK_PERFORM_SEQUENCE(pedStag, seq)
	//						CLEAR_SEQUENCE_TASK(seq)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							ENDIF
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							DISPLAY_HUD(TRUE)
							DISPLAY_RADAR(TRUE)
							PRINTLN("@@@@@@@@@ DISPLAY_HUD 6 @@@@@@@@@")
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
							SET_CAM_ACTIVE(handoverCam1, FALSE)
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							DESTROY_CAM(handoverCam1)
	//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-7)
							STOP_GAMEPLAY_HINT()
						ENDIF
					ENDIF
//				ENDIF
				
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -2201.4172, 4296.0601, 47.5076 >>, << 100, 100, 100 >>)
					CLEAR_AREA_OF_VEHICLES(vChapelCenter, 5)
//					IF IS_ANY_VEHICLE_NEAR_POINT(vChapelCenter, 5)
//						vehicleCandidate = GET_RANDOM_VEHICLE_IN_SPHERE(vChapelCenter, 5, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
//						SET_ENTITY_AS_MISSION_ENTITY(vehicleCandidate)
//						DELETE_VEHICLE(vehicleCandidate)
//					ENDIF
				ENDIF
			ENDIF
			IF NOT bBestmanApproached
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBestman, << 100, 100, 100 >>)
					REQUEST_MODEL(IG_BESTMEN) // stag in a suit
					IF HAS_MODEL_LOADED(IG_BESTMEN)
						pedBestman = CREATE_PED(PEDTYPE_CIVMALE, IG_BESTMEN, << -2202.7468, 4299.0078, 47.4293>> /*<< -2192.5198, 4294.5957, 48.1771 >>*/, 73.0028)
						ADD_PED_FOR_DIALOGUE(cultresConversation, 4, pedBestman, "BESTMAN")
						SET_PED_COMPONENT_VARIATION(pedBestman, PED_COMP_HEAD, 0, 0, 0)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedBestman, RELGROUPHASH_PLAYER)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBestman, TRUE)
						SET_PED_CONFIG_FLAG(pedBestman, PCF_UseKinematicModeWhenStationary, TRUE)
						OPEN_SEQUENCE_TASK(seq)
	//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -2201.5225, 4295.4556, 47.5004 >>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
							TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedBestman, seq)
						CLEAR_SEQUENCE_TASK(seq)
						bBestmanApproached = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT bBrideApproached
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vTheChapel, << 20, 20, 20 >>)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//					WAIT(0)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_spot", CONV_PRIORITY_AMBIENT_HIGH)
						bBrideApproached = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vPos1, vPos2, fWidth)
			AND IS_ENTITY_IN_ANGLED_AREA(pedStag, vPos1, vPos2, fWidth)
			OR IS_ENTITY_AT_COORD(pedStag, vCurrentDestination, << 0, 0, LOCATE_SIZE_HEIGHT >>, TRUE)
			AND CAN_PLAYER_START_CUTSCENE()
			OR bStagMadeToGoOutAndPerformSeq
//			AND GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
			
				SWITCH ambdestStage
				
					CASE ambgogetclothes
					
						REMOVE_BLIP(blipCurrentDestination)
						IF NOT bStagMadeToGoOutAndPerformSeq
							vCurrentDestination = vBestman
								//IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									//IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_wait1", CONV_PRIORITY_AMBIENT_HIGH)
								//ENDIF
								//ENDIF
						//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							//	veh_ped_is_in = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
							//	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								//WAIT(1000)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
									BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 4)
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(vehWeddingCar)
							
								CAM_VIEW_MODE_CONTEXT activeViewModeContext
								CAM_VIEW_MODE activeViewMode
								activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
							
								IF activeViewModeContext != CAM_VIEW_MODE_CONTEXT_ON_FOOT
									activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
							  		IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON
										//First person don't do hint
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, 5000)
							    	ELSE
										TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), vehWeddingCar, 5000, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
										SET_GAMEPLAY_VEHICLE_HINT(vehWeddingCar, << 0, 0, 0 >>, TRUE, -1, DEFAULT_INTERP_TO_FROM_GAME)
							  		ENDIF
								ENDIF
							ENDIF
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//							WAIT(0)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_wait1", CONV_PRIORITY_AMBIENT_HIGH)
								
								IF NOT IS_PED_INJURED(pedStag)
									CLEAR_PED_TASKS(pedStag)
									OPEN_SEQUENCE_TASK(seq)
	//										TASK_LEAVE_ANY_VEHICLE(NULL, 0, ECF_USE_RIGHT_ENTRY)
	//										TASK_GO_STRAIGHT_TO_COORD(NULL, << -2005.0656, 456.3604, 101.6342 >>, PEDMOVEBLENDRATIO_RUN)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -2013.0945, 460.3515, 101.8024 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vStagsDoor, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedStag, seq)
									CLEAR_SEQUENCE_TASK(seq)
									SET_PED_KEEP_TASK(pedStag, TRUE)
								ENDIF
								bStagMadeToGoOutAndPerformSeq = TRUE
								SETTIMERA(0)
							ENDIF
						ENDIF
						//APPLY_FORCE_TO_ENTITY()
						
						IF NOT IS_PED_INJURED(pedStag)
						AND NOT IS_ENTITY_DEAD(vehWeddingCar)
							IF bStagMadeToGoOutAndPerformSeq
							AND IS_ENTITY_AT_COORD(pedStag, vStagsDoor, << 1, 1, 1 >>)
//							AND GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							AND NOT IS_PED_IN_ANY_VEHICLE(pedStag)
//							OR IS_ENTITY_AT_COORD(pedStag, << -2025.3313, 441.5908, 103.0756 >>, << 3, 10, 10 >>)
//							OR NOT IS_ENTITY_ON_SCREEN(pedStag)
//							OR IS_ENTITY_OCCLUDED(pedStag)
//							AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
								// swap character models for stag and put him in a suit
//								voffset_temp = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedStag, << 0, 0, -1 >>)
//								DELETE_PED(pedStag)
//								SET_MODEL_AS_NO_LONGER_NEEDED(U_M_Y_StagGrm_01)
//								pedStag = CREATE_PED(PEDTYPE_DEALER, U_M_M_BankMan, vStagsDoor, 200.0)
//								SET_PED_RELATIONSHIP_GROUP_HASH(pedStag, RELGROUPHASH_PLAYER)
								IF IS_PED_IN_GROUP(pedStag)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
											CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_reply", CONV_PRIORITY_AMBIENT_HIGH)
										ENDIF
									ENDIF
									REMOVE_PED_FROM_GROUP(pedStag)
								ENDIF
								IF IS_ENTITY_OCCLUDED(pedStag)
									IF NOT IS_PED_INJURED(pedStag)
										SET_ENTITY_VISIBLE(pedStag, FALSE)
										SET_ENTITY_COLLISION(pedStag, FALSE)
										SET_ENTITY_INVINCIBLE(pedStag, TRUE)
										FREEZE_ENTITY_POSITION(pedStag, TRUE)
									ENDIF
									IF TIMERA() > 15000//4000
										IF NOT IS_PED_INJURED(pedStag)
											SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_HAIR, 1, 0, 0) //(hair)
											SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_HEAD, 0, 1, 0) //(head)
											SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_TORSO, 1, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_LEG, 1, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(pedStag, PED_COMP_JBIB, 0, 0, 0) //(jbib)
											
											SET_ENTITY_VISIBLE(pedStag, TRUE)
											SET_ENTITY_COLLISION(pedStag, TRUE)
											SET_ENTITY_INVINCIBLE(pedStag, FALSE)
											FREEZE_ENTITY_POSITION(pedStag, FALSE)
											
											SET_ENTITY_COORDS(pedStag, vStagsDoor)
											SET_ENTITY_HEADING(pedStag, 106.5078)
											
											SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
											SET_PED_COMBAT_ATTRIBUTES(pedStag, CA_ALWAYS_FLEE, TRUE)
											SET_PED_STAY_IN_VEHICLE_WHEN_JACKED(pedStag, TRUE)
			//								TASK_SET_DECISION_MAKER(pedStag, DECISION_MAKER_EMPTY)
			//								SET_PED_COMBAT_MOVEMENT(pedStag, CM_WILLRETREAT)
			//								ADD_PED_FOR_DIALOGUE(cultresConversation, 3, pedStag, "GROOM")
											
											TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedStag, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
			//								TASK_GO_TO_ENTITY(pedStag, PLAYER_PED_ID(), DEFAULT_TIME_NEVER_WARP, 10, PEDMOVEBLENDRATIO_RUN)
											IF NOT IS_ENTITY_DEAD(vehWeddingCar)
												OPEN_SEQUENCE_TASK(seq)
		//											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -2008.2067, 453.7162, 101.6493 >>, PEDMOVEBLENDRATIO_WALK, 30000)
													TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV|SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
													TASK_ENTER_VEHICLE(NULL, vehWeddingCar, -1, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN)
												CLOSE_SEQUENCE_TASK(seq)
												TASK_PERFORM_SEQUENCE(pedStag, seq)
												CLEAR_SEQUENCE_TASK(seq)
											ENDIF
										ENDIF
										SETTIMERA(0)
										vCurrentDestination = vBestman
										vPos1 = vBestman1
										vPos2 = vBestman2
										fWidth = fBestman
										//blipCurrentDestination = ADD_BLIP_FOR_COORD(vCurrentDestination)
		//								SET_BLIP_ROUTE(blipCurrentDestination, TRUE)
										
										ambdestStage = ambwaitforstag
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE ambwaitforstag
						IF NOT IS_PED_INJURED(pedStag)
						AND NOT IS_ENTITY_DEAD(vehWeddingCar)
							IF IS_ENTITY_AT_ENTITY(pedStag, vehWeddingCar, << 5, 5, 5 >>)
							AND NOT bSaidSuit
								CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_suit", CONV_PRIORITY_AMBIENT_HIGH)
								bSaidSuit = TRUE
							ENDIF
							IF IS_PED_IN_ANY_VEHICLE(pedStag)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
//									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_chape")
										IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_chape", CONV_PRIORITY_AMBIENT_HIGH)
//											SETTIMERA(0)
//										ENDIF
//										IF TIMERA() > 2500 //1500
////										IF IS_THIS_LABEL_PLAYING("stagd_chape_2")
//											IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
//												blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
//												SETTIMERA(0)
//											ENDIF
//											bBestmansLocationRevealed = TRUE
//											IF TIMERA() > 2500
////											IF IS_THIS_LABEL_PLAYING("stagd_chape_3")
//												TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
//												IF NOT IS_PED_INJURED(pedStag)
//													IF IS_PED_IN_ANY_VEHICLE(pedStag)
//														OPEN_SEQUENCE_TASK(seq)
//															TASK_CLEAR_LOOK_AT(NULL)
//															TASK_USE_MOBILE_PHONE_TIMED(NULL, 25600) //31800
//														CLOSE_SEQUENCE_TASK(seq)
//														TASK_PERFORM_SEQUENCE(pedStag, seq)
//														CLEAR_SEQUENCE_TASK(seq)
//													ENDIF
//												ENDIF
												ambdestStage = ambpickupbestman //ambgotoChapel
												bStagMadeToGoOutAndPerformSeq = FALSE
												conversation_index = 0
											ENDIF
//										ENDIF
//									ENDIF
								ELSE
//									TASK_LOOK_AT_ENTITY(pedStag, PLAYER_PED_ID(), 5000, SLF_WHILE_NOT_IN_FOV)
									IF iStoppingTimer = 0
										iStoppingTimer = GET_GAME_TIMER()
									ENDIF
									IF (iStoppingTimer != 0 AND GET_GAME_TIMER() > iStoppingTimer + 6000)
										PRINTSTRING("\n@@@@@@@@@@@@@@@ bToldToGetIn @@@@@@@@@@@@@@@\n")
										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_getin", CONV_PRIORITY_AMBIENT_HIGH)
										iStoppingTimer = 0
									ENDIF
									SETTIMERA(0)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE ambpickupbestman
						
						IF NOT bStagMadeToGoOutAndPerformSeq
						//make sure car has stopped
							
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//							WAIT(0)
							IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_hairc", CONV_PRIORITY_AMBIENT_HIGH)
							
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
										veh_ped_is_in = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
										BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 2)
	//									WAIT(2000)
	//									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									ENDIF
								ENDIF
								
								IF NOT IS_PED_INJURED(pedBestman)
								AND NOT IS_PED_INJURED(pedStag)
									IF IS_PED_IN_ANY_VEHICLE(pedStag)
										veh_ped_is_in = GET_VEHICLE_PED_IS_IN(pedStag)
										OPEN_SEQUENCE_TASK(seq)
											TASK_ENTER_VEHICLE(NULL, veh_ped_is_in, -1, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK)
											TASK_CLEAR_LOOK_AT(NULL)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedBestman, seq)
										SET_PED_CONFIG_FLAG(pedBestman, PCF_UseKinematicModeWhenStationary, FALSE)
									ELSE
										TASK_GO_TO_ENTITY(pedBestman, pedStag, -1, 8.0)
									ENDIF
									
									IF NOT DOES_BLIP_EXIST(blipBestman)
										blipBestman = CREATE_BLIP_FOR_PED(pedBestman)
									ENDIF
									IF DOES_BLIP_EXIST(blipCurrentDestination)
										REMOVE_BLIP(blipCurrentDestination)
										PRINTSTRING("\n@@@@@@@@@@@@@@@ REMOVE_BLIP 3* @@@@@@@@@@@@@@\n")
									ENDIF
								ENDIF
								SETTIMERA(0)
								
								bBestmanToldToGetInCar = TRUE
								bStagMadeToGoOutAndPerformSeq = TRUE
							ENDIF
						ENDIF
						
						//APPLY_FORCE_TO_ENTITY()
						
						IF bStagMadeToGoOutAndPerformSeq
						AND NOT IS_PED_INJURED(pedStag)
							IF GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
							OR TIMERA() > 12000
								vCurrentDestination = vTheChapel
								vPos1 = vTheChapel1
								vPos2 = vTheChapel2
								fWidth = fTheChapel
								
//								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//								//	IF CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailor", CONV_PRIORITY_AMBIENT_HIGH)
//									//ENDIF
//									
//									//PLAY_AMBIENT_DIALOGUE_LINE(cultresConversation,pedStag, "stagdau", "stagd_tailo")
//									
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailM", CONV_PRIORITY_AMBIENT_HIGH)
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailF", CONV_PRIORITY_AMBIENT_HIGH)
//									ENDIF
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_tailT", CONV_PRIORITY_AMBIENT_HIGH)
//									ENDIF
//								ENDIF
								
								REQUEST_MODEL(IG_BRIDE)
								REQUEST_MODEL(WASHINGTON)
								REQUEST_MODEL(BALLER2)
								REQUEST_MODEL(PRIMO)
								IF HAS_MODEL_LOADED(IG_BRIDE)
								AND HAS_MODEL_LOADED(WASHINGTON)
								AND HAS_MODEL_LOADED(BALLER2)
								AND HAS_MODEL_LOADED(PRIMO)
				 				
									pedBride = CREATE_PED(PEDTYPE_CIVFEMALE, IG_BRIDE, << -330.36, 6154.03, 30.8 >>, 90.0)
	//								SET_PED_COMPONENT_VARIATION(pedBride, PED_COMP_HEAD, 0, 0, 0) //(head)
	//								SET_PED_COMPONENT_VARIATION(pedBride, PED_COMP_HAIR, 0, 0, 0) //(hair)
	//								SET_PED_COMPONENT_VARIATION(pedBride, PED_COMP_TORSO, 0, 1, 0) //(uppr)
	//								SET_PED_COMPONENT_VARIATION(pedBride, PED_COMP_LEG, 0, 0, 0) //(lowr)
	//								SET_PED_COMPONENT_VARIATION(pedBride, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBride, TRUE)
									OPEN_SEQUENCE_TASK(seq)
										//TASK_TURN_PED_TO_FACE_COORD( NULL,  voffset_temp )
						//				TASK_PLAY_ANIM(NULL, "amb@heart_attack", "f_attack", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(pedBride, seq)
									CLEAR_SEQUENCE_TASK(seq)
									
									pedGuest = CREATE_PED(PEDTYPE_CIVMALE, IG_BESTMEN, << -333.3692, 6157.6436, 30.4890>>, 83.2763)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGuest, TRUE)
									TASK_TURN_PED_TO_FACE_ENTITY(pedGuest, PLAYER_PED_ID(), -1)
									SET_PED_COMPONENT_VARIATION(pedGuest, PED_COMP_HEAD, 1, 0, 0) //(head)
									
									vehParkedCar[0] = CREATE_VEHICLE(WASHINGTON, << -342.1295, 6141.4043, 30.4839>>, 133.4604)
									vehParkedCar[1] = CREATE_VEHICLE(BALLER2, << -338.3777, 6137.4102, 30.4760>>, 135.6866)
									vehParkedCar[2] = CREATE_VEHICLE(PRIMO, << -329.4687, 6143.2471, 30.4893 >>, 313.8200)
									vehParkedCar[3] = CREATE_VEHICLE(WASHINGTON, << -322.3235, 6135.4272, 30.4924 >>, 316.5898)
									SET_VEHICLE_COLOURS(vehParkedCar[0], GET_RANDOM_INT_IN_RANGE(0, 127), 0) //121
									SET_VEHICLE_COLOURS(vehParkedCar[1], GET_RANDOM_INT_IN_RANGE(0, 127), 0) //18
									SET_VEHICLE_COLOURS(vehParkedCar[2], GET_RANDOM_INT_IN_RANGE(0, 127), 0)//126) //126
									SET_VEHICLE_COLOURS(vehParkedCar[3], GET_RANDOM_INT_IN_RANGE(0, 127), 0) //120
									
									ambdestStage = ambgotoChapel  //ambgogetclothes
									bStagMadeToGoOutAndPerformSeq = FALSE
									conversation_index = 0
								ENDIF
							ENDIF
						ENDIF
						PRINTSTRING("\n@@@@@@@@@@@@@@@ EVERY FRAME IN BESTMAN @@@@@@@@@@@@@@\n")
					BREAK
					
					CASE ambgotoChapel
						IF NOT bStagMadeToGoOutAndPerformSeq
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
									BRING_VEHICLE_TO_HALT(GET_PLAYERS_LAST_VEHICLE(), DEFAULT_VEH_STOPPING_DISTANCE, 2)
									bStagMadeToGoOutAndPerformSeq = TRUE
									WAIT(3000)
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									WAIT(0)
								ENDIF
							ENDIF
//							bStagMadeToGoOutAndPerformSeq = TRUE
						ENDIF
					BREAK
						//	vCurrentDestination = vStagsHouse
						//	vCurrentDestination = vBestman
				ENDSWITCH
				
				
			ENDIF
			
		ELSE
			IF IS_ENTITY_DEAD(pedStag)
				PRINTSTRING("\n  victum died2")
				missionCleanup()
			ENDIF
		ENDIF
	ENDIF

ENDPROC
//
//PROC MANAGE_CONVERSATION(STRING label, BOOL bCanTalk)
//	IF bCanTalk
//		IF NOT in_car_conv_started
//			IF CREATE_CONVERSATION(sSpeech, subGroupID, label, CONV_PRIORITY_HIGH)
//				in_car_conv_on_hold = FALSE
//				in_car_conv_started = TRUE
//			ENDIF
//		ENDIF
//		IF in_car_conv_on_hold
//			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, subGroupID, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
//				in_car_conv_on_hold = FALSE
//			ENDIF
//		ENDIF
//	ENDIF
//	IF NOT bCanTalk
//		IF NOT in_car_conv_on_hold
//			sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
//			KILL_FACE_TO_FACE_CONVERSATION()
//			in_car_conv_on_hold = TRUE
//		ENDIF
//	ENDIF
//ENDPROC

 PROC interruptionsToConversation()

	IF NOT IS_PED_INJURED(pedStag)
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedStag, FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(pedBestman)
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedBestman, FALSE)
	ENDIF
	
		IF IS_ENTITY_AT_ENTITY(pedStag, PLAYER_PED_ID(), << 4, 4, 4 >>)
//	IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
//		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE(), TRUE)
//		AND IS_PED_IN_VEHICLE(pedStag, GET_PLAYERS_LAST_VEHICLE(), TRUE)
		OR ambdestStage = ambwaitforstag
		OR bSaidComeBackStag
		OR bLeavingBestManBehind
		OR bUnsuitableVehRemark
		OR bStagMadeToGoOutAndPerformSeq
			IF ambdestStage >= ambpickupbestman
			AND bBestmanToldToGetInCar
			AND ambdestStage != ambwaitforstag
			AND NOT bSaidComeBackStag
			AND NOT bLeavingBestManBehind
			AND NOT bUnsuitableVehRemark
			AND NOT bStagMadeToGoOutAndPerformSeq // Should be OR NOT?
				IF IS_ENTITY_AT_ENTITY(pedBestman, PLAYER_PED_ID(), << 4, 4, 4 >>)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_PLAYERS_LAST_VEHICLE(), TRUE)
//				AND IS_PED_IN_VEHICLE(pedStag, GET_PLAYERS_LAST_VEHICLE(), TRUE)
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
				ELSE
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ELSE
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		ELSE
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
			ENDIF
		ENDIF
//	ENDIF
	
ENDPROC

PROC dontFollowPlayerInWrongVehicle()
	
	IF NOT IS_PED_INJURED(pedStag)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			IF NOT IS_PLAYER_IN_OR_ENTERING_A_SUITABLE_VEHICLE_TO_OFFER_LIFTS()
				PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@@@ VEHICLE DOESN'T FIT @@@@@@@@@@@@@@@@@@@@@@@@@@\n")
				IF IS_PED_IN_GROUP(pedStag)
					REMOVE_PED_FROM_GROUP(pedStag)
				ENDIF
				IF GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_GO_TO_ENTITY) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_GO_TO_ENTITY) != WAITING_TO_START_TASK
					TASK_GO_TO_ENTITY(pedStag, PLAYER_PED_ID(), -1, 6)
				ENDIF
				IF IS_PED_IN_ANY_VEHICLE(pedStag)
					TASK_LEAVE_ANY_VEHICLE(pedStag)
				ENDIF
				IF NOT bUnsuitableVehRemark
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bUnsuitableVehRemark = TRUE
						PRINTSTRING("\n@@@@@@@@@@@@@@@@@@@@@@@ stagd_nogood @@@@@@@@@@@@@@@@@@@@@@@@@@\n")
						IF IS_PLAYER_IN_OR_ENTERING_ANY_TYPE_OF_BIKE()
							PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "stagdau", "stagd_nogood", "stagd_nogood_1", CONV_PRIORITY_AMBIENT_HIGH)
						ELSE
							PLAY_SINGLE_LINE_FROM_CONVERSATION(cultresConversation, "stagdau", "stagd_nogood", "stagd_nogood_2", CONV_PRIORITY_AMBIENT_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bUnsuitableVehRemark = FALSE
				IF GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_GO_TO_ENTITY) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_GO_TO_ENTITY) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) = WAITING_TO_START_TASK
					CLEAR_PED_TASKS(pedStag)
					IF NOT IS_PED_IN_GROUP(pedStag)
						SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
						SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
		ELSE
//			IF IS_PED_IN_ANY_VEHICLE(pedStag, TRUE)
//				IF iStoppingTimer = 0
//					iStoppingTimer = GET_GAME_TIMER()
//				ENDIF
//				IF (iStoppingTimer != 0 AND GET_GAME_TIMER() > iStoppingTimer + 6000)
//					PRINTSTRING("\n@@@@@@@@@@@@@@@ bToldToGetIn @@@@@@@@@@@@@@@\n")
//					CREATE_CONVERSATION(cultresConversation, "stagdau", "stagd_getin", CONV_PRIORITY_AMBIENT_HIGH)
//					iStoppingTimer = 0
//				ENDIF
//			ENDIF
			IF NOT IS_PED_IN_GROUP(pedStag)
			AND NOT IS_PED_IN_ANY_VEHICLE(pedStag)
			AND GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(pedStag, SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
				SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
				SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
				SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC stagDistanceCheck()

	IF DOES_ENTITY_EXIST(pedStag)
		IF NOT IS_PED_INJURED(pedStag)
			IF IS_ENTITY_AT_ENTITY(pedStag, PLAYER_PED_ID(), << 4, 4, 4 >>, FALSE)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PED_IN_ANY_VEHICLE(pedStag)
				OR IS_PED_IN_ANY_TAXI(PLAYER_PED_ID())
					IF DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
						REMOVE_BLIP(blipPersonGettingPlayersAttention)
						IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
							blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
						ENDIF
					ENDIF
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND IS_PED_IN_ANY_VEHICLE(pedStag)
						// Both player and stag are in a vehicle and within 4 meters
						IF DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
							REMOVE_BLIP(blipPersonGettingPlayersAttention)
							IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
								blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
							ENDIF
						ENDIF
					ELSE
						// Only player or stag is in a vehicle and within 4 meters
						IF DOES_BLIP_EXIST(blipCurrentDestination)
							REMOVE_BLIP(blipCurrentDestination)
							IF NOT DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
								blipPersonGettingPlayersAttention = CREATE_BLIP_FOR_PED(pedStag)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
					//veh_jetski = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					
				//	ATTACH_ENTITY_TO_ENTITY(ped_farmer, veh_jetski, 0, <<0.0,-0.8,1.3>>, 0, 0)
				//	TASK_PLAY_ANIM(ped_farmer, "amb_sit_couch_f", "sit_down_idle_01", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
				//	flag_attach_ped_to_jetski = TRUE
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipCurrentDestination)
					REMOVE_BLIP(blipCurrentDestination)
					IF NOT DOES_BLIP_EXIST(blipPersonGettingPlayersAttention)
						blipPersonGettingPlayersAttention = CREATE_BLIP_FOR_PED(pedStag)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_IN_GROUP(pedStag)
				IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedStag, << 10, 10, 10 >>)
					CLEAR_PED_TASKS(pedStag)
					SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
					SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
					SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
				ENDIF
//				IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
//					AND IS_PED_IN_VEHICLE(pedVictim, GET_PLAYERS_LAST_VEHICLE())
						CLEAR_PED_TASKS(pedStag)
						SET_PED_AS_GROUP_MEMBER(pedStag, PLAYER_GROUP_ID())
						SET_PED_NEVER_LEAVES_GROUP(pedStag, TRUE)
						SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedStag, VS_FRONT_RIGHT)
					ENDIF
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF ambdestStage = ambgogetclothes
//		IF flag_make_stag_take_front_seat = FALSE
//		
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			AND NOT IS_PED_IN_ANY_VEHICLE(pedStag)
//				players_temp_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				CLEAR_PED_TASKS(pedStag)
//				TASK_ENTER_VEHICLE(pedStag, players_temp_car, -1, VS_FRONT_RIGHT)
//				SET_PED_KEEP_TASK(pedStag, TRUE)
//				flag_make_stag_take_front_seat = TRUE
//			ENDIF
//		ENDIF
//	ENDIF
	
	
ENDPROC

PROC checkIfPlayerIsInCar()
	
	IF NOT IS_ENTITY_DEAD(vehWeddingCar)
	AND NOT IS_ENTITY_DEAD(pedStag)
		IF NOT bPlayerInWeddingCar
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
			AND IS_PED_IN_VEHICLE(pedStag, vehWeddingCar)
				bPlayerInWeddingCar = TRUE
				IF DOES_BLIP_EXIST(blipWeddingCar)
					REMOVE_BLIP(blipWeddingCar)
				ENDIF
				IF bBestmansLocationRevealed
				AND NOT bStagMadeToGoOutAndPerformSeq
					IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
						blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Ok, lets get out of here", 4000, 1)
				ENDIF
			ENDIF
		ENDIF
		
		// check if groom is jacked
		IF NOT bPlayerInWeddingCar
		AND NOT bWaitingForStagToGetInCar
		
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
				IF NOT IS_PED_IN_VEHICLE(pedStag, vehWeddingCar)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Hey! Come back here", 4000, 1)
					ENDIF
					
					IF DOES_BLIP_EXIST(blipWeddingCar)
						REMOVE_BLIP(blipWeddingCar)
					ENDIF
					
					IF NOT DOES_BLIP_EXIST(blipWeddingCar)
						blipWeddingCar = CREATE_BLIP_FOR_PED(pedStag)
						SET_BLIP_AS_FRIENDLY(blipWeddingCar, TRUE)
					ENDIF
					
					bPlayerInWeddingCar = FALSE
					bWaitingForStagToGetInCar = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
		IF bPlayerInWeddingCar
		
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
			
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Hey! Come back here", 4000, 1)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipWeddingCar)
					blipWeddingCar = CREATE_BLIP_FOR_VEHICLE(vehWeddingCar)
					SET_BLIP_AS_FRIENDLY(blipWeddingCar, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blipCurrentDestination)
					REMOVE_BLIP(blipCurrentDestination)
				ENDIF
				
				bPlayerInWeddingCar = FALSE
			ENDIF
			
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehWeddingCar)
			AND NOT IS_PED_IN_VEHICLE(pedStag, vehWeddingCar)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					//PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Hey! Come back here", 4000, 1)
				ENDIF
				
				IF NOT DOES_BLIP_EXIST(blipWeddingCar)
					blipWeddingCar = CREATE_BLIP_FOR_VEHICLE(vehWeddingCar)
					SET_BLIP_AS_FRIENDLY(blipWeddingCar, TRUE)
				ENDIF
				IF DOES_BLIP_EXIST(blipCurrentDestination)
					REMOVE_BLIP(blipCurrentDestination)
				ENDIF
				
				bPlayerInWeddingCar = FALSE
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

///    //////////////////////////////////////////////////////////
///    							MAIN LOOP
///    //////////////////////////////////////////////////////////
///    
SCRIPT(coords_struct in_coords)

	vInput = in_coords.vec_coord[0]
	PRINTVECTOR(vInput)
	//SET_MISSION_FLAG(TRUE)
	PRINTNL()
	PRINTSTRING("pedStag")
	PRINTNL()
	PRINTSTRING("RUNNING...........")
	
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF NOT IS_PED_INJURED(pedStag)
			IF IS_PED_IN_GROUP(pedStag)
				REMOVE_PED_FROM_GROUP(pedStag)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedBestman)
			IF IS_PED_IN_GROUP(pedBestman)
				REMOVE_PED_FROM_GROUP(pedBestman)
			ENDIF
		ENDIF
		missionCleanup()
	ENDIF 
	// time check
	
	IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_STAG1)
	    TERMINATE_THIS_THREAD()
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	// set game time to 6am
//		SET_CLOCK_TIME(6, 00, 00)
//	#ENDIF

	//PD - All un checks are now in this one function, if you need to change any run checks give me a shout or bug me.
	IF CAN_RANDOM_EVENT_LAUNCH(vInput)
		IF GET_CLOCK_HOURS() > 2
		AND GET_CLOCK_HOURS() < 15
		AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			//RUN STAG DO
		ELSE
			PRINTLN("@@@@@@@@@@@@@@@@ Only runs between 3am and 3pm")
			TERMINATE_THIS_THREAD()
		ENDIF
		LAUNCH_RANDOM_EVENT()
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
		
	WHILE TRUE
		WAIT(0)
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR bLeavingActivationArea
//			IF IS_AMBIENT_SCRIPT_ALLOWED_TO_RUN(RUN_ON_MISSION_NEVER)

			IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
				IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
					missionCleanup()
				ENDIF
			ENDIF
			
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_SDRM")
			
				SWITCH ambStage
					CASE ambCanRun
						IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
							missionCleanup()
						ENDIF
						IF bAssetsLoaded
							bDoFullCleanUp = TRUE
							ambStage = (ambRunning)
						ELSE
							create_Stag_and_assets()
						ENDIF
					BREAK
					CASE ambRunning
						//DO STUFF
						HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
						
						IF NOT bStagsBlipRemoved
						//AND bPlayerCloseToStag = TRUE
							carsHonkHorn()
							checkIfPlayerUntiesStag()
							checkIfPlayerIsCloseStag()
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneId)
								checkWeddingStillOnTime()
							ENDIF
						ELSE
							//DO_TIMER(START_TIME_STAG,10000 )
							
							interruptionsToConversation()
							IF NOT bBrideApproached
								makingStagReactToEvents()
							ENDIF
							countdownToWedding()
							getToDestinations()
							makeStagTalkInCar()
							HANDLE_BUDDY_PERMANENT_HEAD_TRACK(pedStag, FALSE)
							
							IF NOT bTimeToGetToChapelMentioned
//								checkWeddingStillOnTime()
							ENDIF
							
							IF ambdestStage	= ambgogetclothes
							AND NOT bStagMadeToGoOutAndPerformSeq
								dontFollowPlayerInWrongVehicle()
								stagDistanceCheck()
							ELSE
								checkIfPlayerIsInCar()
							ENDIF
							
							IF ambdestStage	= ambgotoChapel
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vChapelCenter) < DEFAULT_CUTSCENE_LOAD_DIST
									IF NOT IS_CUTSCENE_ACTIVE()
										REQUEST_CUTSCENE("sdrm_mcs_2")
									ENDIF
									IF IS_CUTSCENE_ACTIVE()
										IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
											IF NOT IS_PED_INJURED(pedStag)
											AND NOT IS_PED_INJURED(pedBestman)
											AND NOT IS_PED_INJURED(pedBride)
											AND NOT IS_PED_INJURED(pedGuest)
											AND NOT IS_ENTITY_DEAD(veh_ped_is_in)
							//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
												SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Groom", pedStag)
												SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Groomsman", pedBestman)
												SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Bride", pedBride)
												SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Father_of_the_bride", pedGuest)
											ENDIF
										ENDIF
									ENDIF
								ELIF IS_CUTSCENE_ACTIVE()
									REMOVE_CUTSCENE()
								ENDIF
								
								IF bStagMadeToGoOutAndPerformSeq
									IF CAN_PLAYER_START_CUTSCENE()
										chapel_cutscene()
									ENDIF
								ELSE
									IF NOT IS_PED_INJURED(pedBestman)
										IF IS_PED_IN_ANY_VEHICLE(pedBestman)
											IF DOES_BLIP_EXIST(blipBestman)
												REMOVE_BLIP(blipBestman)
												IF DOES_BLIP_EXIST(blipCurrentDestination)
													REMOVE_BLIP(blipCurrentDestination)
													PRINTSTRING("\n@@@@@@@@@@@@@@@ REMOVE_BLIP 7* @@@@@@@@@@@@@@\n")
												ENDIF
												IF NOT DOES_BLIP_EXIST(blipCurrentDestination)
													blipCurrentDestination = CREATE_BLIP_FOR_COORD(vCurrentDestination, TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							IF IS_PLAYER_INTERFERING_WITH_EVENT()
								IF NOT IS_PED_INJURED(pedStag)
									TASK_SMART_FLEE_PED(pedStag, PLAYER_PED_ID(), 1000, -1)
								ENDIF
								IF NOT IS_PED_INJURED(pedBestman)
									TASK_SMART_FLEE_PED(pedBestman, PLAYER_PED_ID(), 1000, -1)
								ENDIF
								IF NOT IS_PED_INJURED(pedBride)
									TASK_SMART_FLEE_PED(pedBride, PLAYER_PED_ID(), 1000, -1)
								ENDIF
								IF NOT IS_PED_INJURED(pedGuest)
									TASK_SMART_FLEE_PED(pedGuest, PLAYER_PED_ID(), 1000, -1)
								ENDIF
								MISSION_FAILED()
							ENDIF
							
						ENDIF
						debug_stuff()
					BREAK
				ENDSWITCH
//			ELSE
//				missionCleanup()
//			ENDIF
		ELSE
			missionCleanup()
		ENDIF
	ENDWHILE


ENDSCRIPT
///    //////////////////////////////////////////////////////////
///    							DEBUG
///    //////////////////////////////////////////////////////////

