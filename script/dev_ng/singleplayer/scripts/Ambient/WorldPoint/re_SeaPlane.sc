// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Random Event - Sea Plane
//		DATE			: 	24/07/14
//		AUTHOR			:	Harry Turner
//		DESCRIPTION		:	'Sea Plane' random event. Player steals a sea plane and is then chased.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//================================================================================================================================================
//		INCLUDES
//================================================================================================================================================
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_event.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_interiors.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch"
USING "ambient_common.sch"
USING "cellphone_public.sch"
USING "chase_hint_cam.sch"
USING "cheat_controller_public.sch"
USING "clearMissionArea.sch"
USING "CompletionPercentage_public.sch"
USING "cutscene_public.sch"
USING "dialogue_public.sch"
USING "distributed_los.sch"
USING "flow_help_public.sch"
USING "flow_public_core_override.sch"      
USING "random_events_public.sch"
USING "rc_helper_functions.sch"
USING "script_blips.sch"
USING "script_buttons.sch"
USING "script_ped.sch"
USING "selector_public.sch"
USING "select_mission_stage.sch"   
USING "social_public.sch"

//================================================================================================================================================
//		ENUMS
//================================================================================================================================================
ENUM MISSION_STAGE_FLAGS
	MSF_STEAL_PLANE,
	MSF_MISSION_PASSED	// Passed
ENDENUM

ENUM MISSION_STAGE_SECTIONS
	MSS_SETUP,			// Stage is being setup
	MSS_ACTIVE,			// Stage is running
	MSS_CLEANUP		// Stage is being cleaned up
ENDENUM

ENUM MISSION_BIT_FLAGS
	MBF_SEQUENCE_OPEN,		// Used for sequence tasks
	MBF_PLANE_STOLEN,
	MBF_PLAYER_SPOTTED,
	MBF_STOP_WARNING,
	MBF_KICKOFF,
	MBF_ROUTE_DONE,
	MBF_BOATS_APPEAR,
	MBF_MISSION_SETUP
ENDENUM

ENUM MISSION_PED_FLAGS
	MPF_ENEMY_1,
	MPF_ENEMY_2,
	MPF_ENEMY_3,
	MPF_ENEMY_4,
	MPF_ENEMY_5,
	MPF_ENEMY_6,	// These two guys were stood outside
	MPF_ENEMY_7,	// the cave. They have been removed.
	MPF_ENEMY_8,
	MPF_ENEMY_9,
	MPF_ENEMY_10,
	MPF_ENEMY_11
ENDENUM

ENUM MISSION_PED_BIT_FLAGS
	MPBF_VEH_BLIP,			// Peds blip has been set to vehicle size
	MPBF_BLOCK_RAGDOLL,
	MPBF_DONT_CLEANUP,
	MPBF_SEEN_PLAYER,
	MPBF_STUNNED,
	MPBF_ANCHORED
ENDENUM

ENUM MISSION_VEHICLE_FLAGS
	MVF_PLANE,
	MVF_BOAT_1,
	MVF_BOAT_2,
	MVF_BOAT_3,
	MVF_BOAT_4
ENDENUM

ENUM SCENARIO_BLOCKING_AREAS
	SBA_CAVE
ENDENUM

//================================================================================================================================================
//		STRUCTS
//================================================================================================================================================
STRUCT MISSION_PED_STRUCT
	PED_INDEX			ped
	BLIP_INDEX			blip
	INT					iBitFlags
	INT					iEvent
	INT					iTimerA
	INT					iTimerB
ENDSTRUCT

STRUCT MISSION_VEHICLE_STRUCT
	VEHICLE_INDEX	veh
	BLIP_INDEX		blip
	INT				iEvent
	FLOAT			fTimer
ENDSTRUCT

//================================================================================================================================================
//		MISSION VARS
//================================================================================================================================================
MISSION_STAGE_FLAGS 	e_MissionStage 		= MSF_STEAL_PLANE		// Current stage
MISSION_STAGE_SECTIONS	e_StageSection		= MSS_SETUP			// Current section of the stage we're in

//================================================================================================================================================
//		PEDS
//================================================================================================================================================
MISSION_PED_STRUCT	a_Peds[COUNT_OF(MISSION_PED_FLAGS)]

//================================================================================================================================================
//		VEHICLES
//================================================================================================================================================
MISSION_VEHICLE_STRUCT	a_Vehicles[COUNT_OF(MISSION_VEHICLE_FLAGS)]

//================================================================================================================================================
//		OBJECTS
//================================================================================================================================================


//================================================================================================================================================
//		BLIPS
//================================================================================================================================================


//================================================================================================================================================
//		STRINGS
//================================================================================================================================================


//===============================================
//			ANIMATIONS
//===============================================


//===============================================
//			VEHICLE RECORDINGS
//===============================================


//===============================================
//			WAYPOINT RECORDINGS
//===============================================
STRING	str_WayRouteA	=	"RE_SeaPlane1"
STRING	str_WayRouteB	=	"RE_SeaPlane2"

//================================================================================================================================================
//		BOOLS
//================================================================================================================================================


//================================================================================================================================================
//		VECTORS
//================================================================================================================================================
VECTOR	V_ZERO		=	<<0,0,0>>
VECTOR	v_Input
VECTOR	v_PlaneStart	=	<<3097.46, 2181.55, 0.5>>

//================================================================================================================================================
//		INTEGERS
//================================================================================================================================================
INT			i_CurrentEvent		=	0
INT 		i_MissionBitFlags	=	0
INT			i_LOSCheckIndex		=	0

// For boat AI
INT			i_ClosestRouteANode	=	-1
INT			i_ClosestRouteBNode	=	-1

//================================================================================================================================================
//		MODELS
//================================================================================================================================================
MODEL_NAMES	e_ModelPlane	=	INT_TO_ENUM(MODEL_NAMES, HASH("DODO"))
MODEL_NAMES	e_ModelMexBoat	=	TROPIC
MODEL_NAMES	e_ModelMex		=	G_M_Y_MEXGOON_02
MODEL_NAMES	e_ModelArmBoat	=	SUNTRAP
MODEL_NAMES	e_ModelArm		=	G_M_Y_ARMGOON_02
SCRIPT_ENTITY_LOS_STRUCT	s_LOSChecks[2]

//================================================================================================================================================
//		MISC. VARIABLES
//================================================================================================================================================
REL_GROUP_HASH				relGroupEnemies
SCENARIO_BLOCKING_INDEX		a_ScenarioBlockingAreas[COUNT_OF(SCENARIO_BLOCKING_AREAS)]
structPedsForConversation	s_Convo
SEQUENCE_INDEX				seqMain

//================================================================================================================================================
//		DEBUG VARIABLES
//================================================================================================================================================
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID 			widGroup
#ENDIF

//================================================================================================================================================
//		FUNCTIONS/PROCEDURES
//================================================================================================================================================

//===============================================
//	Reset mission variables
//===============================================
PROC RESET_VARIABLES()
	e_MissionStage = MSF_STEAL_PLANE
	e_StageSection = MSS_SETUP
	i_CurrentEvent = 0
	i_MissionBitFlags = 0
ENDPROC

//===============================================
//	Remove all mission blips
//===============================================
PROC REMOVE_ALL_BLIPS()
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		SAFE_REMOVE_BLIP(a_Peds[i].blip)
	ENDREPEAT

	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		SAFE_REMOVE_BLIP(a_Vehicles[i].blip)
	ENDREPEAT
ENDPROC

//===============================================
//	Remove all anim dicts
//===============================================
PROC REMOVE_ALL_ANIM_DICTS()

ENDPROC

//===============================================
//	Remove all waypoint recordings
//===============================================
PROC REMOVE_ALL_WAYPOINTS()
	REMOVE_WAYPOINT_RECORDING( str_WayRouteA )
	REMOVE_WAYPOINT_RECORDING( str_WayRouteB )
ENDPROC

//===============================================
//	Remove all vehicle recordings
//===============================================
PROC REMOVE_ALL_VEH_RECS()

ENDPROC

//===============================================
//	Set mission bit flag
//===============================================
PROC SET_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag )
	SET_BIT( i_MissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Clear mission bit flag
//===============================================
PROC CLEAR_MISSION_BIT_FLAG( MISSION_BIT_FLAGS eFlag )
	CLEAR_BIT( i_MissionBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Checks if a mission bit flag has been set
//===============================================
FUNC BOOL IS_MISSION_BIT_FLAG_SET( MISSION_BIT_FLAGS eFlag )
	RETURN IS_BIT_SET( i_MissionBitFlags, ENUM_TO_INT( eFlag ) )
ENDFUNC

//===============================================
//	Custom conversation func
//===============================================
FUNC BOOL START_CONVERSATION( STRING strRoot )
	RETURN CREATE_CONVERSATION( s_Convo, "TEMPAU", strRoot, CONV_PRIORITY_HIGH )
ENDFUNC

//===============================================
//	Cleanup a mission ped
//===============================================
PROC CLEANUP_PED( MISSION_PED_FLAGS ePed, BOOL bForce )
	IF DOES_ENTITY_EXIST( a_Peds[ePed].ped )
		SAFE_REMOVE_BLIP( a_Peds[ePed].blip )
		
		IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		AND IS_ENTITY_ATTACHED( a_Peds[ePed].ped )
		AND NOT IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped, TRUE )
		AND NOT IS_PED_SITTING_IN_ANY_VEHICLE( a_Peds[ePed].ped )
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE( a_Peds[ePed].ped )
			DETACH_ENTITY( a_Peds[ePed].ped )
		ENDIF
		
		IF bForce
			DELETE_PED( a_Peds[ePed].ped )
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED( a_Peds[ePed].ped )
		ENDIF
		a_Peds[ePed].iEvent = 0
		a_Peds[ePed].iBitFlags = 0
	ENDIF
ENDPROC

//===============================================
//	Cleanup all mission peds
//===============================================
PROC CLEANUP_PEDS( BOOL bForce )
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		CLEANUP_PED( INT_TO_ENUM( MISSION_PED_FLAGS, i ), bForce )
	ENDREPEAT
ENDPROC

//===============================================
//	Check if it's safe to cleanup a specific vehicle
//===============================================
FUNC BOOL IS_SAFE_TO_CLEANUP_VEHICLE( VEHICLE_INDEX& vehIndex )
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehIndex)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

//===============================================
//	Cleanup mission vehicle
//===============================================
PROC CLEANUP_VEHICLE( MISSION_VEHICLE_FLAGS eVeh, BOOL bForce )
	IF DOES_ENTITY_EXIST( a_Vehicles[eVeh].veh )
		SAFE_REMOVE_BLIP( a_Vehicles[eVeh].blip )
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE( a_Vehicles[eVeh].veh )
			STOP_PLAYBACK_RECORDED_VEHICLE( a_Vehicles[eVeh].veh )
		ENDIF
		
		IF IS_SAFE_TO_CLEANUP_VEHICLE( a_Vehicles[eVeh].veh )
			IF bForce
				DELETE_VEHICLE( a_Vehicles[eVeh].veh )
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED( a_Vehicles[eVeh].veh )
			ENDIF
		ENDIF
		a_Vehicles[eVeh].veh = NULL
		a_Vehicles[eVeh].iEvent = 0
		a_Vehicles[eVeh].fTimer = 0
	ENDIF
ENDPROC

//===============================================
//	Cleanup all mission vehicles
//===============================================
PROC CLEANUP_VEHICLES( BOOL bForce )
	INT i
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		CLEANUP_VEHICLE( INT_TO_ENUM( MISSION_VEHICLE_FLAGS, i ), bForce )
	ENDREPEAT
ENDPROC

//===============================================
//	Custom function for opening a sequence
//===============================================
PROC SAFE_OPEN_SEQUENCE()
	IF NOT IS_MISSION_BIT_FLAG_SET(MBF_SEQUENCE_OPEN)
		CLEAR_SEQUENCE_TASK( seqMain )
		OPEN_SEQUENCE_TASK( seqMain )
		SET_MISSION_BIT_FLAG(MBF_SEQUENCE_OPEN)
	ENDIF
ENDPROC

//===============================================
//	Custom function for using a sequence
//===============================================
PROC SAFE_PERFORM_SEQUENCE_TASK( PED_INDEX ped )
	IF IS_MISSION_BIT_FLAG_SET(MBF_SEQUENCE_OPEN)
		CLOSE_SEQUENCE_TASK( seqMain )
		CLEAR_MISSION_BIT_FLAG(MBF_SEQUENCE_OPEN)
	ENDIF
	TASK_PERFORM_SEQUENCE( ped, seqMain )
	CLEAR_SEQUENCE_TASK( seqMain )
ENDPROC

//===============================================
//	Set ped bit flag
//===============================================
PROC SET_PED_BIT_FLAG( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	SET_BIT( a_Peds[ePed].iBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Clear ped bit flag
//===============================================
PROC CLEAR_PED_BIT_FLAG( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	CLEAR_BIT( a_Peds[ePed].iBitFlags, ENUM_TO_INT(eFlag) )
ENDPROC

//===============================================
//	Checks if a ped bit flag has been set
//===============================================
FUNC BOOL IS_PED_BIT_FLAG_SET( MISSION_PED_FLAGS ePed, MISSION_PED_BIT_FLAGS eFlag )
	RETURN IS_BIT_SET( a_Peds[ePed].iBitFlags, ENUM_TO_INT( eFlag ) )
ENDFUNC

//===============================================
//	Show blip if driver dead.
//===============================================
FUNC BOOL SHOULD_HIDE_BLIP_IN_VEHICLE( MISSION_PED_FLAGS ePed )
	IF ePed = MPF_ENEMY_2
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_1].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_ENEMY_4
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_3].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_ENEMY_5
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_3].ped )
		AND NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_4].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_ENEMY_9
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_8].ped )
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF ePed = MPF_ENEMY_11
		IF NOT IS_ENTITY_ALIVE( a_Peds[MPF_ENEMY_10].ped )
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

//===============================================
//	Manages blip size of peds. Removes blips
//	when they die.
//===============================================
PROC UPDATE_ALL_BLIPS()
	INT i
	REPEAT COUNT_OF(MISSION_PED_FLAGS) i
		IF DOES_ENTITY_EXIST(a_Peds[i].ped)
		AND DOES_BLIP_EXIST(a_Peds[i].blip)	
			IF IS_PED_IN_ANY_VEHICLE(a_Peds[i].ped)
				IF NOT IS_PED_BIT_FLAG_SET( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
					SET_BLIP_SCALE( a_Peds[i].blip, BLIP_SIZE_VEHICLE )
					SET_PED_BIT_FLAG( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				ENDIF
				
				IF GET_SEAT_PED_IS_IN( a_Peds[i].ped ) != VS_DRIVER
					IF SHOULD_HIDE_BLIP_IN_VEHICLE( INT_TO_ENUM(MISSION_PED_FLAGS, i) )
						IF GET_BLIP_ALPHA( a_Peds[i].blip ) > 0
							SET_BLIP_ALPHA( a_Peds[i].blip, 0 )
							SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, TRUE )
						ENDIF
					ELSE
						IF GET_BLIP_ALPHA( a_Peds[i].blip ) = 0
							SET_BLIP_ALPHA( a_Peds[i].blip, 255 )
							SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, FALSE )
						ENDIF
					ENDIF
				ENDIF

			ELIF IS_PED_BIT_FLAG_SET( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				SET_BLIP_SCALE( a_Peds[i].blip, BLIP_SIZE_PED )
				CLEAR_PED_BIT_FLAG( INT_TO_ENUM(MISSION_PED_FLAGS,i), MPBF_VEH_BLIP )
				
				IF GET_BLIP_ALPHA( a_Peds[i].blip ) = 0
					SET_BLIP_ALPHA( a_Peds[i].blip, 255 )
					SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[i].blip, FALSE )
				ENDIF
			ENDIF
			
			// Remove blips for any injured peds automatically.
			IF IS_PED_INJURED(a_Peds[i].ped)
				SAFE_REMOVE_BLIP(a_Peds[i].blip)
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(MISSION_VEHICLE_FLAGS) i
		IF DOES_ENTITY_EXIST(a_Vehicles[i].veh)
		AND DOES_BLIP_EXIST(a_Vehicles[i].blip)	
			// Remove blips for any destroyed cars
			IF NOT IS_VEHICLE_DRIVEABLE(a_Vehicles[i].veh)
				SAFE_REMOVE_BLIP(a_Vehicles[i].blip)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//===============================================
//	Configure specific vehicle colours
//===============================================
PROC SET_MISSION_VEHICLE_COLOURS( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		SWITCH eVeh
			CASE MVF_BOAT_1
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 111, 28 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 111, 156 )
			BREAK
			CASE MVF_BOAT_2
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 121, 20 )
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 0, 156 )
			BREAK
			CASE MVF_BOAT_3
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 121, 46)
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 0, 156 )
			BREAK
			CASE MVF_BOAT_4
				SET_VEHICLE_COLOURS( a_Vehicles[eVeh].veh, 111, 54)
				SET_VEHICLE_EXTRA_COLOURS( a_Vehicles[eVeh].veh, 112, 156 )
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//===============================================
//	Configure specific vehicle mods
//===============================================
PROC SET_MISSION_VEHICLE_MODS( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		SWITCH eVeh
			CASE MVF_BOAT_2
			CASE MVF_BOAT_3
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 1, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 2, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 3, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 4, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 5, FALSE )
			BREAK
			CASE MVF_BOAT_1
			CASE MVF_BOAT_4
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 1, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 2, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 3, TRUE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 4, FALSE )
				SET_VEHICLE_EXTRA( a_Vehicles[eVeh].veh, 5, TRUE )
			BREAK
		ENDSWITCH	
	ENDIF
ENDPROC

//===============================================
//	Configure specific ped variations
//===============================================
PROC SET_MISSION_PED_VARIATIONS( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			// Arm
			CASE MPF_ENEMY_1
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			BREAK
			// Arm
			CASE MPF_ENEMY_2
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			BREAK
			// Mex
			CASE MPF_ENEMY_3
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			BREAK
			// Mex
			CASE MPF_ENEMY_4
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			BREAK
			// Mex
			CASE MPF_ENEMY_5
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			BREAK
			// Mex
			CASE MPF_ENEMY_8
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
			BREAK
			// Mex
			CASE MPF_ENEMY_9
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			BREAK
			// Arm
			CASE MPF_ENEMY_10
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
			BREAK
			// Arm
			CASE MPF_ENEMY_11
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(a_Peds[ePed].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//===============================================
//	Setup mission peds for dialogue
//===============================================
PROC ADD_MISSION_PED_FOR_DIALOGUE( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			CASE MPF_ENEMY_1
				
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//===============================================
//	Creates a mission ped
//===============================================
FUNC BOOL SPAWN_MISSION_PED( MISSION_PED_FLAGS ePed, BOOL bBlip, VECTOR vPosOverride, FLOAT fHeadingOverride = -1.0 )
	
	IF NOT DOES_ENTITY_EXIST( a_Peds[ePed].ped )
	
		// Values used to create & configure the peds
		VECTOR vPos
		FLOAT fHeading
		MODEL_NAMES eModel
		WEAPON_TYPE	eWeapon = WEAPONTYPE_UNARMED
		VEHICLE_INDEX startVeh = NULL
		VEHICLE_SEAT eSeat = VS_DRIVER
		ENTITY_INDEX attachEntity
		VECTOR vAttachPosOffset = V_ZERO
		VECTOR vAttachRotOffset = V_ZERO
		WEAPONCOMPONENT_TYPE eWeaponComponent = WEAPONCOMPONENT_INVALID
		COMBAT_ABILITY_LEVEL eCombatLevel = CAL_AVERAGE
		INT iHealth = 0
		INT iArmour = 0
		INT iAccuracy = 1
		FLOAT fMinBlanks = 0.75
		FLOAT fMaxBlanks = 0.95
		FLOAT fSeeingRange = 60.0
		FLOAT fPeripheralSeeingRange = 5.0
        FLOAT fCentreAngle = 120.0
		FLOAT fMinAngle = -90.0 
		FLOAT fMaxAngle = 90.0
		FLOAT fHearingRange = 125
		REL_GROUP_HASH relGroup = relGroupEnemies
		BOOL bCanListen = TRUE
		BOOL bGenerateSound = TRUE
		BOOL bBlockEvents = FALSE
		BOOL bKeepTask = TRUE
		BOOL bDiesWhenInjured = TRUE
		BOOL bInfluenceWantedLevel = FALSE
		BOOL bAutoEquipWeapon = FALSE
		BOOL bScanDeadBodies = FALSE
		BOOL bLoadCollision = FALSE
		BOOL bLeaveVehicle = TRUE
		BOOL bVisible = TRUE
		BOOL bReact = TRUE
		BOOL bNoOffset = FALSE
		BOOL bCollision = TRUE
		BOOL bInvincible = FALSE
		BOOL bDoDriveby = TRUE
		BOOL bUseVehicle = TRUE
		BOOL bVehicleAttack = TRUE
		BOOL bSufferCritical = TRUE
		BOOL bTargetable = TRUE
		BOOL bRunFromExplosions = TRUE
		BOOL bFallOutVehicle = FALSE
		BOOL bBlindFire = FALSE
		BOOL bDisablePinDownOthers = FALSE
		BOOL bDisablePinnedDown = FALSE
		BOOL bMoveBeforeCover = FALSE
		BOOL bKinematicWhenStill = FALSE
		BOOL bDisableHurt = TRUE
		BOOL bLoadCover = FALSE
		BOOL bCantJackPlayer = FALSE
		BOOL bStopSpeaking = FALSE
		BOOL bDropsWeapon = TRUE
		BOOL bCleanupOnDeath = TRUE
		BOOL bOverrideMaxHealth = FALSE
		BOOL bAlwaysFlee = FALSE
		BOOL bDisablePainAudio = FALSE
		BOOL bStealthMode = FALSE
		BOOL bCompletelyBlockCollision = FALSE
		BOOL bFreeze = FALSE
		BOOL bNotAvoided = FALSE
		BOOL bAggresive = FALSE
		BOOL bDefensiveSphereSpawn = FALSE
		BOOL bStopFiringWhenDead = FALSE
		BOOL bAlwaysFight = FALSE
		BOOL bCanFightUnarmed = FALSE
		STRING strScenario
		#IF IS_DEBUG_BUILD TEXT_LABEL strDebugName	#ENDIF
		// Configure each ped appropriately
		SWITCH ePed
			CASE MPF_ENEMY_1
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 1"	#ENDIF
				eModel = e_ModelArm
				startVeh = a_Vehicles[MVF_BOAT_1].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_2
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 2"	#ENDIF
				eModel = e_ModelArm
				vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_BOAT_1].veh, <<0,-1.7,0.9>>)
				fHeading = GET_ENTITY_HEADING(a_Vehicles[MVF_BOAT_1].veh) + 90
				strScenario = "WORLD_HUMAN_HANG_OUT_STREET"
				eWeapon = WEAPONTYPE_MICROSMG
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
	
			CASE MPF_ENEMY_3
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 3"	#ENDIF
				eModel = e_ModelMex
				startVeh = a_Vehicles[MVF_BOAT_2].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_4
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 4"	#ENDIF
				eModel = e_ModelMex
				vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_BOAT_2].veh, <<0,-1.5,0.9>>)
				fHeading = GET_ENTITY_HEADING(a_Vehicles[MVF_BOAT_2].veh) + 90
				strScenario = "WORLD_HUMAN_HANG_OUT_STREET"
				eWeapon = WEAPONTYPE_MICROSMG
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_5
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 5"	#ENDIF
				eModel = e_ModelMex
				vPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(a_Vehicles[MVF_BOAT_2].veh, <<0,-2.3,0.9>>)
				fHeading = GET_ENTITY_HEADING(a_Vehicles[MVF_BOAT_2].veh) + 90
				strScenario = "WORLD_HUMAN_HANG_OUT_STREET"
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
//			CASE MPF_ENEMY_6
//				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 6"	#ENDIF
//				eModel = e_ModelMex
//				vPos = <<3072.9053, 2136.3877, 1.7442>>
//				fHeading = 183.4180
//				eWeapon = WEAPONTYPE_ASSAULTRIFLE
//				bAutoEquipWeapon = TRUE
//				bLoadCollision = TRUE
//				bLeaveVehicle = FALSE
//				bDefensiveSphereSpawn = TRUE
//				bStopFiringWhenDead = TRUE
//			BREAK
//			
//			CASE MPF_ENEMY_7
//				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 7"	#ENDIF
//				eModel = e_ModelMex
//				vPos = <<3074.63, 2135.04, 2.37>>
//				fHeading = 174.7321
//				eWeapon = WEAPONTYPE_ASSAULTRIFLE
//				bAutoEquipWeapon = TRUE
//				bLoadCollision = TRUE
//				bLeaveVehicle = FALSE
//				bDefensiveSphereSpawn = TRUE
//				bStopFiringWhenDead = TRUE
//			BREAK
			
			CASE MPF_ENEMY_8
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 8"	#ENDIF
				eModel = e_ModelMex
				startVeh = a_Vehicles[MVF_BOAT_3].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_9
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 9"	#ENDIF
				eModel = e_ModelMex
				startVeh = a_Vehicles[MVF_BOAT_3].veh
				eSeat = VS_FRONT_RIGHT
				eWeapon = WEAPONTYPE_MICROSMG
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_10
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 10"	#ENDIF
				eModel = e_ModelArm
				startVeh = a_Vehicles[MVF_BOAT_4].veh
				eWeapon = WEAPONTYPE_PISTOL
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			CASE MPF_ENEMY_11
				#IF IS_DEBUG_BUILD	strDebugName = "ENEMY 11"	#ENDIF
				eModel = e_ModelArm
				startVeh = a_Vehicles[MVF_BOAT_4].veh
				eSeat = VS_FRONT_RIGHT
				eWeapon = WEAPONTYPE_MICROSMG
				bLoadCollision = TRUE
				bLeaveVehicle = FALSE
				bRunFromExplosions = FALSE
				bAlwaysFight = TRUE
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("SPAWN_MISSION_PED() - Invalid ped flag.")
			BREAK
		ENDSWITCH
		
		// Exit function is the model hasn't already been loaded
		IF NOT HAS_MODEL_LOADED( eModel )
			RETURN FALSE
			PRINTLN("SPAWN_MISSION_PED() - Model not loaded.")
		ENDIF
		
		// Default position and header can be overridden
		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
			IF NOT DOES_ENTITY_EXIST( attachEntity )
				vPos = vPosOverride
			ELSE
				vAttachPosOffset = vPosOverride
			ENDIF
		ENDIF
		
		IF fHeadingOverride != -1.0
			fHeading = fHeadingOverride
		ENDIF
		
		// Create ped, setup blip and type
		IF IS_ENTITY_ALIVE( startVeh )
			a_Peds[ePed].ped = CREATE_PED_INSIDE_VEHICLE( startVeh, PEDTYPE_MISSION, eModel, eSeat )
		ELSE
			a_Peds[ePed].ped = CREATE_PED( PEDTYPE_MISSION, eModel, vPos, fHeading )
			SET_ENTITY_COLLISION( a_Peds[ePed].ped, bCollision )
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION( a_Peds[ePed].ped, !bCompletelyBlockCollision )
		ENDIF
		
		IF bNoOffset
			SET_ENTITY_COORDS_NO_OFFSET( a_Peds[ePed].ped, vPos )
		ENDIF
		
		IF bBlip
			IF relGroup = relGroupEnemies
				a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
			ELSE
				a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, FALSE )
			ENDIF
		ENDIF
		
		SET_PED_DROPS_WEAPONS_WHEN_DEAD( a_Peds[ePed].ped, bDropsWeapon )
		
		// Flags
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_ListensToSoundEvents, bCanListen )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_GeneratesSoundEvents, bGenerateSound )
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, bBlockEvents )
		SET_PED_KEEP_TASK(a_Peds[ePed].ped, bKeepTask )
		SET_PED_DIES_WHEN_INJURED(a_Peds[ePed].ped, bDiesWhenInjured )
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_DisableHurt, bDisableHurt)
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_DontInfluenceWantedLevel, !bInfluenceWantedLevel )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_WILL_SCAN_FOR_DEAD_PEDS, bScanDeadBodies )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_LEAVE_VEHICLES, bLeaveVehicle )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_NotAllowedToJackAnyPlayers, bCantJackPlayer )
		SET_ENTITY_LOAD_COLLISION_FLAG( a_Peds[ePed].ped, bLoadCollision )
		SET_PED_ARMOUR( a_Peds[ePed].ped, iArmour )
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_PLAY_REACTION_ANIMS, bReact )
		SET_ENTITY_VISIBLE( a_Peds[ePed].ped, bVisible )
		SET_ENTITY_INVINCIBLE( a_Peds[ePed].ped, bInvincible )
		GIVE_WEAPON_TO_PED( a_Peds[ePed].ped, eWeapon, INFINITE_AMMO, bAutoEquipWeapon, bAutoEquipWeapon )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_DO_DRIVEBYS, bDoDriveby )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, bUseVehicle )
		SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE_ATTACK, bVehicleAttack )
		SET_PED_COMBAT_ATTRIBUTES(  a_Peds[ePed].ped, CA_AGGRESSIVE, bAggresive )
		SET_PED_SUFFERS_CRITICAL_HITS( a_Peds[ePed].ped, bSufferCritical )
		SET_PED_CAN_BE_TARGETTED( a_Peds[ePed].ped, bTargetable )
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_RunFromFiresAndExplosions, bRunFromExplosions)  
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_FallsOutOfVehicleWhenKilled, bFallOutVehicle)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_BLIND_FIRE_IN_COVER, bBlindFire)                      
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_DISABLE_PIN_DOWN_OTHERS, bDisablePinDownOthers)                  
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_DISABLE_PINNED_DOWN, bDisablePinnedDown)                        
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, bMoveBeforeCover)                                    
		SET_PED_CONFIG_FLAG(a_Peds[ePed].ped, PCF_UseKinematicModeWhenStationary, bKinematicWhenStill)   
		SET_PED_TO_LOAD_COVER(a_Peds[ePed].ped, bLoadCover)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_ALWAYS_FLEE, bAlwaysFlee)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_ALWAYS_FIGHT, bAlwaysFight)
		SET_PED_COMBAT_ATTRIBUTES(a_Peds[ePed].ped, CA_CAN_FIGHT_ARMED_PEDS_WHEN_NOT_ARMED, bCanFightUnarmed)
		DISABLE_PED_PAIN_AUDIO(a_Peds[ePed].ped, bDisablePainAudio)
		STOP_PED_SPEAKING( a_Peds[ePed].ped, bStopSpeaking )
		SET_PED_VISUAL_FIELD_PROPERTIES( a_Peds[ePed].ped, fSeeingRange, fPeripheralSeeingRange, fCentreAngle, fMinAngle, fMaxAngle )
		FREEZE_ENTITY_POSITION( a_Peds[ePed].ped, bFreeze )
		SET_PED_CONFIG_FLAG( a_Peds[ePed].ped, PCF_Avoidance_Ignored_by_All, bNotAvoided )
		SET_PED_IS_AVOIDED_BY_OTHERS( a_Peds[ePed].ped, !bNotAvoided )

		IF NOT bCleanupOnDeath
			SET_PED_BIT_FLAG( ePed, MPBF_DONT_CLEANUP )
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(strScenario)
			TASK_START_SCENARIO_IN_PLACE( a_Peds[ePed].ped, strScenario, -1 )
		ENDIF
		
		IF iHealth > 0
			IF iHealth > GET_ENTITY_MAX_HEALTH( a_Peds[ePed].ped )
			OR bOverrideMaxHealth
				SET_ENTITY_MAX_HEALTH( a_Peds[ePed].ped, iHealth )
			ENDIF
			SET_ENTITY_HEALTH( a_Peds[ePed].ped, iHealth )
		ENDIF
		
		IF iAccuracy > 0
			SET_PED_ACCURACY( a_Peds[ePed].ped, iAccuracy )
		ENDIF
		
		IF fHearingRange >= 0
			SET_PED_HEARING_RANGE( a_Peds[ePed].ped, fHearingRange )
		ENDIF
		
		IF fMinBlanks > 0
		OR fMaxBlanks > 0
			SET_PED_CHANCE_OF_FIRING_BLANKS( a_Peds[ePed].ped, fMinBlanks, fMaxBlanks )
		ENDIF
		
		IF bDefensiveSphereSpawn
			SET_PED_SPHERE_DEFENSIVE_AREA( a_Peds[ePed].ped, vPos, 5.0 )
		ENDIF
		
		IF bStopFiringWhenDead
			STOP_PED_WEAPON_FIRING_WHEN_DROPPED( a_Peds[ePed].ped )
		ENDIF
		
		// Relationship group
		SET_PED_RELATIONSHIP_GROUP_HASH( a_Peds[ePed].ped, relGroup )
		
		#IF IS_DEBUG_BUILD 
			SET_PED_NAME_DEBUG( a_Peds[ePed].ped, strDebugName )
		#ENDIF
		
		// Attachment
		IF DOES_ENTITY_EXIST( attachEntity )
			ATTACH_ENTITY_TO_ENTITY( a_Peds[ePed].ped, attachEntity, -1, vAttachPosOffset, vAttachRotOffset, TRUE, TRUE, TRUE )
			SET_PED_CAN_RAGDOLL( a_Peds[ePed].ped, FALSE )
			SET_PED_BIT_FLAG( ePed, MPBF_BLOCK_RAGDOLL )
		ENDIF
		
		// Configure variations
		SET_MISSION_PED_VARIATIONS( ePed )
		
		ADD_MISSION_PED_FOR_DIALOGUE( ePed )
		
		IF eWeaponComponent != WEAPONCOMPONENT_INVALID
			GIVE_WEAPON_COMPONENT_TO_PED( a_Peds[ePed].ped, eWeapon, eWeaponComponent )
			SET_CURRENT_PED_WEAPON( a_Peds[ePed].ped, eWeapon, TRUE )
		ENDIF
		
		SET_PED_COMBAT_ABILITY( a_Peds[ePed].ped, eCombatLevel )
		
		IF bStealthMode
			SET_PED_STEALTH_MOVEMENT( a_Peds[ePed].ped, TRUE, "DEFAULT_ACTION" )
		ENDIF
		
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_CAVE()
	RETURN IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3150.507568,2179.343262,-3.388727>>, <<3064.607666,2223.615967,12.381769>>, 103 )
ENDFUNC

//FUNC BOOL IS_PLAYER_SHOOTING_NEAR_CAVE()
//	WEAPON_TYPE playerWeapon
//	GET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), playerWeapon )
//
//	RETURN ( IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3102.792236,2148.364014,-1.623682>>, <<3079.730469,2083.418457,12.910206>>, 85 )
//	AND IS_PED_SHOOTING( PLAYER_PED_ID() )
//	AND NOT IS_PED_CURRENT_WEAPON_SILENCED( PLAYER_PED_ID() )
//	AND playerWeapon != WEAPONTYPE_STUNGUN )
//ENDFUNC
//
//FUNC BOOL IS_EXPLOSION_NEAR_CAVE()
//	RETURN ( IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3102.792236,2148.364014,-1.623682>>, <<3079.730469,2083.418457,12.910206>>, 85 )
//	IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_DONTCARE, <<3102.792236,2148.364014,-1.623682>>, <<3079.730469,2083.418457,12.910206>>, 85 )
//	AND NOT IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_DONTCARE
//ENDFUNC

// Common code for when shit kicks off
PROC KICKOFF_PED( MISSION_PED_FLAGS ePed )
	SET_PED_BIT_FLAG( ePed, MPBF_SEEN_PLAYER )
							
	// Used to kick off the guards outside if the player sneaks in and alerts the cave enemies.
	IF NOT IS_MISSION_BIT_FLAG_SET( MBF_KICKOFF )
		SET_MISSION_BIT_FLAG( MBF_KICKOFF )
	ENDIF
	
	IF NOT IS_MISSION_BIT_FLAG_SET( MBF_PLAYER_SPOTTED )
		SET_MISSION_BIT_FLAG( MBF_PLAYER_SPOTTED )
	ENDIF
	
	a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
	
	// Have them continue to shoot even when player is far away
	SET_PED_SEEING_RANGE( a_Peds[ePed].ped, 200 )
	
	// Updates for vehicle beds
	IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
		IF GET_SEAT_PED_IS_IN(a_Peds[ePed].ped) != VS_DRIVER
			SET_BLIP_ALPHA( a_Peds[ePed].blip, 0 )
			SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[ePed].blip, TRUE )
		ELSE
			CLEAR_PED_TASKS( a_Peds[ePed].ped )
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Is the player in the target vehicle
//===============================================
FUNC BOOL IS_PLAYER_IN_SEA_PLANE()
	IF ( IS_ENTITY_ALIVE( a_Vehicles[MVF_PLANE].veh ) AND IS_PED_IN_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_PLANE].veh ) )
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BOAT_STUCK_ON_LAND( MISSION_PED_FLAGS ePed, INT iTime = 5000 )
	MISSION_VEHICLE_FLAGS eVeh
	
	SWITCH ePed
		CASE MPF_ENEMY_1
		CASE MPF_ENEMY_2
			eVeh = MVF_BOAT_1
		BREAK
		CASE MPF_ENEMY_3
		CASE MPF_ENEMY_4
		CASE MPF_ENEMY_5
			eVeh = MVF_BOAT_2
		BREAK
		CASE MPF_ENEMY_8
		CASE MPF_ENEMY_9
			eVeh = MVF_BOAT_3
		BREAK
		CASE MPF_ENEMY_10
		CASE MPF_ENEMY_11
			eVeh = MVF_BOAT_4
		BREAK
	ENDSWITCH
	
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
		IF NOT IS_ENTITY_IN_WATER( a_Vehicles[eVeh].veh )
			a_Vehicles[eVeh].fTimer += GET_FRAME_TIME()
		ELSE
			a_Vehicles[eVeh].fTimer = 0
		ENDIF
	ENDIF
	
	IF a_Vehicles[eVeh].fTimer >= iTime
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//===============================================
//	url:bugstar:2012005 - Enemies aren't alerted by an empty jetski shooting past
//===============================================
FUNC BOOL IS_PLAYER_LAST_VEHICLE_NEAR_BOATS()
	VEHICLE_INDEX vehLastPlayer
	vehLastPlayer = GET_PLAYERS_LAST_VEHICLE()
	
	IF IS_ENTITY_ALIVE(vehLastPlayer)
	AND IS_ENTITY_IN_ANGLED_AREA(vehLastPlayer, <<3083.230225,2195.515625,-3.223820>>, <<3096.518799,2195.455322,4.949678>>, 15)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//===============================================
//	Enemies don't react to explosions because they're dumb
//===============================================
FUNC BOOL IS_EXPLOSION_IN_CAVE()
	RETURN IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_DONTCARE, <<3150.507568,2179.343262,-30>>, <<3064.607666,2223.615967,12.381769>>, 103 )
ENDFUNC

//===============================================
//	Updates a ped
//===============================================
PROC UPDATE_PED( MISSION_PED_FLAGS ePed )
	IF IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		SWITCH ePed
			// Boat drivers
			CASE MPF_ENEMY_1
			CASE MPF_ENEMY_3
				IF a_Peds[ePed].iEvent >= 2
				AND a_Peds[ePed].iEvent < 5
					IF IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
					AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
					AND IS_ENTITY_ON_FIRE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
						SAFE_OPEN_SEQUENCE()
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, TRUE )
							TASK_LEAVE_ANY_VEHICLE( NULL, GET_RANDOM_INT_IN_RANGE(500, 1501), ECF_JUMP_OUT )
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
							TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
						SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
						SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
						a_Peds[ePed].iEvent = 5
					ENDIF
				ENDIF
			
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						// Make the two peds look at the next boat
						IF ePed = MPF_ENEMY_1
							IF IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_4].ped)
								SET_IK_TARGET(a_Peds[ePed].ped, IK_PART_HEAD, a_Peds[MPF_ENEMY_4].ped, ENUM_TO_INT(BONETAG_HEAD), V_ZERO, ITF_DEFAULT )
							ENDIF
						ELSE
							IF IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_2].ped)
								SET_IK_TARGET(a_Peds[ePed].ped, IK_PART_HEAD, a_Peds[MPF_ENEMY_2].ped, ENUM_TO_INT(BONETAG_HEAD), V_ZERO, ITF_DEFAULT )
							ENDIF
						ENDIF
						
						IF IS_MISSION_BIT_FLAG_SET( MBF_PLANE_STOLEN )
						OR IS_PED_IN_COMBAT( a_Peds[ePed].ped, PLAYER_PED_ID() )
						OR IS_PLAYER_LAST_VEHICLE_NEAR_BOATS()
						OR IS_EXPLOSION_IN_CAVE()
							KICKOFF_PED( ePed )
							TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 1
						BOOL bProceed
						
						IF ePed = MPF_ENEMY_1
							// Proceed when ped #2 is dead, or is in the boat
							IF NOT IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_2].ped)
							OR (IS_ENTITY_ALIVE(a_Vehicles[MVF_BOAT_1].veh) AND IS_PED_IN_VEHICLE(a_Peds[MPF_ENEMY_2].ped, a_Vehicles[MVF_BOAT_1].veh, TRUE))
								bProceed = TRUE
							ENDIF
						ELSE
							// Proceed when peds #4 & #5 are dead or in the boat
							IF ( (NOT IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_4].ped)) OR (IS_ENTITY_ALIVE(a_Vehicles[MVF_BOAT_2].veh) AND IS_PED_IN_VEHICLE(a_Peds[MPF_ENEMY_4].ped, a_Vehicles[MVF_BOAT_2].veh, TRUE)) )
							AND ( (NOT IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_5].ped)) OR (IS_ENTITY_ALIVE(a_Vehicles[MVF_BOAT_2].veh) AND IS_PED_IN_VEHICLE(a_Peds[MPF_ENEMY_5].ped, a_Vehicles[MVF_BOAT_2].veh, TRUE)) )
								bProceed = TRUE
							ENDIF
						ENDIF
						
						IF bProceed 
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 2
						// Waypoint control
						INT iClosest
						INT iClosestToBoat
						INT iTotal
						STRING strWaypoint
						EWAYPOINT_FOLLOW_FLAGS eFlags
						eFlags = EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_USE_TIGHTER_TURN_SETTINGS
						
						IF ePed = MPF_ENEMY_1
							strWaypoint = str_WayRouteA
						ELSE
							strWaypoint = str_WayRouteB
						ENDIF
					
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( strWaypoint, GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosest )
						WAYPOINT_RECORDING_GET_NUM_POINTS( strWaypoint, iTotal )
						WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( strWaypoint, GET_ENTITY_COORDS(a_Peds[ePed].ped), iClosestToBoat )
						
						IF iClosest > 0
						AND IS_PED_ON_FOOT(PLAYER_PED_ID())
							iClosest--
						ENDIF

						IF ePed = MPF_ENEMY_3
						AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<3108.675293,2194.120605,10.950867>>, <<3143.525391,2232.265137,-4.598444>>, 82.75 )
							iClosest = CLAMP_INT( iClosest, 22, iTotal )
						ENDIF
						
						// Exit cove when player far
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), v_PlaneStart ) >= 225
							iClosest = (iTotal - 1)
						ENDIF
						
						IF ( ( ePed = MPF_ENEMY_1 AND i_ClosestRouteANode != iClosest AND iClosest > iClosestToBoat ) OR ( ePed = MPF_ENEMY_3 AND i_ClosestRouteBNode != iClosest AND iClosest > iClosestToBoat ) )
						AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped) )
						AND NOT IS_PED_BEING_STUNNED(a_Peds[ePed].ped)
							IF ePed = MPF_ENEMY_1
								i_ClosestRouteANode = iClosest
							ELSE
								i_ClosestRouteBNode = iClosest
							ENDIF
							IF IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped) )
								SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), FALSE )
								CLEAR_PED_BIT_FLAG( ePed, MPBF_ANCHORED )
							ENDIF
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING( a_Peds[ePed].ped, GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), strWaypoint, DRIVINGMODE_AVOIDCARS, 0, eFlags, iClosest, -1, FALSE, 6 )							
						ENDIF
						
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING )
						AND GET_SCRIPT_TASK_STATUS( a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING ) = FINISHED_TASK
						AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_SHOOT_AT_ENTITY )
							TASK_VEHICLE_SHOOT_AT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							IF IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped) )
							AND CAN_ANCHOR_BOAT_HERE(GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped))
								SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), TRUE )
								SET_PED_BIT_FLAG( ePed, MPBF_ANCHORED )
							ENDIF
						ENDIF
						
						// Anchor boat if they're stunned
						IF NOT IS_PED_BIT_FLAG_SET( ePed, MPBF_STUNNED )
							IF IS_PED_BEING_STUNNED(a_Peds[ePed].ped)
								SET_PED_BIT_FLAG( ePed, MPBF_STUNNED )
								CLEAR_PED_TASKS( a_Peds[ePed].ped )
								IF NOT IS_PED_BIT_FLAG_SET( ePed, MPBF_ANCHORED )
								AND CAN_ANCHOR_BOAT_HERE(GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped))
									SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), TRUE )
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_BEING_STUNNED(a_Peds[ePed].ped)
								CLEAR_PED_BIT_FLAG( ePed, MPBF_STUNNED )
								IF NOT IS_PED_BIT_FLAG_SET( ePed, MPBF_ANCHORED )
									SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), FALSE )
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_SCRIPT_TASK_STATUS(a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING) = PERFORMING_TASK
						AND NOT IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped))
						AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK( a_Peds[ePed].ped )
							TASK_DRIVE_BY( a_Peds[ePed].ped, PLAYER_PED_ID(), NULL, V_ZERO, 500, 100, TRUE, FIRING_PATTERN_BURST_FIRE )
						ENDIF
						
						VECTOR vLastNode
						WAYPOINT_RECORDING_GET_COORD( strWaypoint, iTotal - 1, vLastNode )
						IF IS_ENTITY_AT_COORD( a_Peds[ePed].ped, vLastNode, <<8,8,8>> )
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							TASK_BOAT_MISSION( a_Peds[ePed].ped, GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), NULL, PLAYER_PED_ID(), V_ZERO, MISSION_FOLLOW, 20, DRIVINGMODE_AVOIDCARS, 5, BCF_DEFAULTSETTINGS )
							SET_MISSION_BIT_FLAG( MBF_ROUTE_DONE )
							REMOVE_WAYPOINT_RECORDING( strWaypoint )
							// Proceed when reached end of route
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 3
						IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
						AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
							IF IS_VEHICLE_STUCK_TIMER_UP( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped), VEH_STUCK_JAMMED, 6000 )
							OR IS_BOAT_STUCK_ON_LAND( ePed )
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
								SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
								a_Peds[ePed].iEvent++
							ELSE
								IF GET_SCRIPT_TASK_STATUS(a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
								AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK( a_Peds[ePed].ped )
									TASK_DRIVE_BY( a_Peds[ePed].ped, PLAYER_PED_ID(), NULL, V_ZERO, 500, 100, TRUE, FIRING_PATTERN_BURST_FIRE )
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 4
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_GET_OFF_BOAT )
							IF NOT IS_PED_ON_VEHICLE( a_Peds[ePed].ped )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
								TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							ELSE
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			// Stood up peds in boats
			CASE MPF_ENEMY_2
			CASE MPF_ENEMY_4
			CASE MPF_ENEMY_5
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						IF ePed = MPF_ENEMY_2 
						AND IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_START_SCENARIO_IN_PLACE )
						AND IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_4].ped)
							SET_IK_TARGET(a_Peds[ePed].ped, IK_PART_HEAD, a_Peds[MPF_ENEMY_4].ped, ENUM_TO_INT(BONETAG_HEAD), V_ZERO, ITF_DEFAULT )
						ENDIF
						
						IF ( ePed = MPF_ENEMY_4 OR ePed = MPF_ENEMY_5 )
						AND IS_ENTITY_ALIVE(a_Peds[MPF_ENEMY_2].ped)
						AND IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_START_SCENARIO_IN_PLACE )
							SET_IK_TARGET(a_Peds[ePed].ped, IK_PART_HEAD, a_Peds[MPF_ENEMY_2].ped, ENUM_TO_INT(BONETAG_HEAD), V_ZERO, ITF_DEFAULT )
						ENDIF
						
						IF IS_MISSION_BIT_FLAG_SET( MBF_PLANE_STOLEN )
						OR IS_PED_IN_COMBAT( a_Peds[ePed].ped, PLAYER_PED_ID() )
						OR IS_PLAYER_LAST_VEHICLE_NEAR_BOATS()
						OR IS_EXPLOSION_IN_CAVE()
							KICKOFF_PED(ePed)
							a_Peds[ePed].iTimerA = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(1250,1501)
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 1
						IF GET_GAME_TIMER() >= a_Peds[ePed].iTimerA
							IF ePed = MPF_ENEMY_2
							AND IS_ENTITY_ALIVE( a_Vehicles[MVF_BOAT_1].veh )
							AND IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT(a_Vehicles[MVF_BOAT_1].veh) )
								CLEAR_PED_TASKS( a_Peds[ePed].ped )
								TASK_ENTER_VEHICLE( a_Peds[ePed].ped, a_Vehicles[MVF_BOAT_1].veh, DEFAULT, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							ENDIF
							IF ePed = MPF_ENEMY_4
							AND IS_ENTITY_ALIVE( a_Vehicles[MVF_BOAT_2].veh )
							AND IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT(a_Vehicles[MVF_BOAT_2].veh) )
								CLEAR_PED_TASKS( a_Peds[ePed].ped )
								TASK_ENTER_VEHICLE( a_Peds[ePed].ped, a_Vehicles[MVF_BOAT_2].veh, DEFAULT, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							ENDIF
							
							IF ePed = MPF_ENEMY_5
							AND IS_ENTITY_ALIVE( a_Vehicles[MVF_BOAT_2].veh )
							AND IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT(a_Vehicles[MVF_BOAT_2].veh) )
								CLEAR_PED_TASKS( a_Peds[ePed].ped )
								TASK_ENTER_VEHICLE( a_Peds[ePed].ped, a_Vehicles[MVF_BOAT_2].veh, DEFAULT, VS_BACK_LEFT, PEDMOVEBLENDRATIO_SPRINT )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							ENDIF
							
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 2
						MISSION_VEHICLE_FLAGS eTargetVeh
						
						SWITCH ePed
							CASE MPF_ENEMY_2
								eTargetVeh = MVF_BOAT_1
							BREAK
							CASE MPF_ENEMY_4
								eTargetVeh = MVF_BOAT_2
							BREAK
							CASE MPF_ENEMY_5
								eTargetVeh = MVF_BOAT_2
							BREAK
						ENDSWITCH
					
						IF IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped, TRUE )
						OR NOT IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT(a_Vehicles[eTargetVeh].veh) )
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
							TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							a_Peds[ePed].iEvent++
						ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_ENTER_VEHICLE )
							IF ePed = MPF_ENEMY_2
							OR ePed = MPF_ENEMY_4
							AND IS_ENTITY_ALIVE( a_Vehicles[eTargetVeh].veh )
								TASK_ENTER_VEHICLE( a_Peds[ePed].ped, a_Vehicles[eTargetVeh].veh, DEFAULT, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT )
							ENDIF
							
							IF ePed = MPF_ENEMY_5
							AND IS_ENTITY_ALIVE( a_Vehicles[eTargetVeh].veh )
								TASK_ENTER_VEHICLE( a_Peds[ePed].ped, a_Vehicles[eTargetVeh].veh, DEFAULT, VS_BACK_LEFT, PEDMOVEBLENDRATIO_SPRINT )
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
						AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
							IF IS_ENTITY_ON_FIRE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
								SAFE_OPEN_SEQUENCE()
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, TRUE )
									TASK_LEAVE_ANY_VEHICLE( NULL, GET_RANDOM_INT_IN_RANGE(500, 1501), ECF_JUMP_OUT )
									TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
									TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
								SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
								SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
								a_Peds[ePed].iEvent++
							ENDIF
							
							IF a_Peds[ePed].iEvent = 3
								IF ( IS_VEHICLE_STUCK_TIMER_UP( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped), VEH_STUCK_JAMMED, 6000 )
								OR IS_BOAT_STUCK_ON_LAND( ePed ) )
								AND IS_MISSION_BIT_FLAG_SET( MBF_ROUTE_DONE )
									TASK_GET_OFF_BOAT(  a_Peds[ePed].ped )
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
									SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
									a_Peds[ePed].iEvent = 5
								ELIF NOT IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT( GET_VEHICLE_PED_IS_IN( a_Peds[ePed].ped ) ) )
									SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_LEAVE_VEHICLES, TRUE )
									TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
									a_Peds[ePed].iEvent++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 4
						// Ped is stood on boat shooting because driver was killed
						// or ped has left boat
					BREAK
					CASE 5
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_GET_OFF_BOAT )
							IF NOT IS_PED_ON_VEHICLE( a_Peds[ePed].ped )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
								TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							ELSE
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MPF_ENEMY_8
				IF a_Peds[ePed].iEvent >= 1
				AND a_Peds[ePed].iEvent < 3
					IF IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
					AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
					AND IS_ENTITY_ON_FIRE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
						SAFE_OPEN_SEQUENCE()
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, TRUE )
							TASK_LEAVE_ANY_VEHICLE( NULL, GET_RANDOM_INT_IN_RANGE(500, 1501), ECF_JUMP_OUT )
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
							TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
						SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
						SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
						a_Peds[ePed].iEvent = 3
					ENDIF
				ENDIF
			
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						IF ( IS_PLAYER_IN_SEA_PLANE() AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 125 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID()) AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 150 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_OCCLUDED(a_Peds[ePed].ped) )
						OR IS_MISSION_BIT_FLAG_SET( MBF_BOATS_APPEAR )
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_BOAT_3].veh )
								SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), FALSE )
								FLOAT fSpeed
								fSpeed = GET_ENTITY_SPEED( PLAYER_PED_ID() )
								fSpeed = CLAMP( fSpeed, 10, 40 )
								SET_VEHICLE_FORWARD_SPEED( a_Vehicles[MVF_BOAT_3].veh, fSpeed )
							ENDIF
							SET_CURRENT_PED_WEAPON( a_Peds[ePed].ped, WEAPONTYPE_PISTOL )
							TASK_BOAT_MISSION( a_Peds[ePed].ped, GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), NULL, PLAYER_PED_ID(), V_ZERO, MISSION_FOLLOW, 20, DRIVINGMODE_AVOIDCARS, 5, BCF_DEFAULTSETTINGS )
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
							SET_MISSION_BIT_FLAG( MBF_BOATS_APPEAR )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 1
						IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
						AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
							IF IS_VEHICLE_STUCK_TIMER_UP( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped), VEH_STUCK_JAMMED, 6000 )
							OR IS_BOAT_STUCK_ON_LAND( ePed )
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
								SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
								a_Peds[ePed].iEvent++
							ELSE
								IF GET_SCRIPT_TASK_STATUS(a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
								AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK( a_Peds[ePed].ped )
									TASK_DRIVE_BY( a_Peds[ePed].ped, PLAYER_PED_ID(), NULL, V_ZERO, 500, 100, TRUE, FIRING_PATTERN_BURST_FIRE )
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_GET_OFF_BOAT )
							IF NOT IS_PED_ON_VEHICLE( a_Peds[ePed].ped )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
								TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							ELSE
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MPF_ENEMY_10
				IF a_Peds[ePed].iEvent >= 1
				AND a_Peds[ePed].iEvent < 3
					IF IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
					AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
					AND IS_ENTITY_ON_FIRE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
						SAFE_OPEN_SEQUENCE()
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, TRUE )
							TASK_LEAVE_ANY_VEHICLE( NULL, GET_RANDOM_INT_IN_RANGE(500, 1501), ECF_JUMP_OUT )
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
							TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
						SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
						SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
						a_Peds[ePed].iEvent = 3
					ENDIF
				ENDIF
				
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						IF ( IS_PLAYER_IN_SEA_PLANE() AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 125 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID()) AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 150 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_OCCLUDED(a_Peds[ePed].ped) )
						OR IS_MISSION_BIT_FLAG_SET( MBF_BOATS_APPEAR )
							IF IS_ENTITY_ALIVE( a_Vehicles[MVF_BOAT_4].veh )
								SET_BOAT_ANCHOR( GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), FALSE )
								FLOAT fSpeed
								fSpeed = GET_ENTITY_SPEED( PLAYER_PED_ID() )
								fSpeed = CLAMP( fSpeed, 10, 40 )
								SET_VEHICLE_FORWARD_SPEED( a_Vehicles[MVF_BOAT_4].veh, fSpeed )
							ENDIF
							SET_CURRENT_PED_WEAPON( a_Peds[ePed].ped, WEAPONTYPE_PISTOL )
							TASK_BOAT_MISSION( a_Peds[ePed].ped, GET_VEHICLE_PED_IS_USING(a_Peds[ePed].ped), NULL, PLAYER_PED_ID(), V_ZERO, MISSION_FOLLOW, 20, DRIVINGMODE_AVOIDCARS, 5, BCF_DEFAULTSETTINGS )
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
							a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
							SET_MISSION_BIT_FLAG( MBF_BOATS_APPEAR )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 1
						IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
						AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
							IF IS_VEHICLE_STUCK_TIMER_UP( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped), VEH_STUCK_JAMMED, 6000 )
							OR IS_BOAT_STUCK_ON_LAND( ePed )
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
								SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
								a_Peds[ePed].iEvent++
							ELSE
								IF GET_SCRIPT_TASK_STATUS(a_Peds[ePed].ped, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
								AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK( a_Peds[ePed].ped )
									TASK_DRIVE_BY( a_Peds[ePed].ped, PLAYER_PED_ID(), NULL, V_ZERO, 500, 100, TRUE, FIRING_PATTERN_BURST_FIRE )
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_GET_OFF_BOAT )
							IF NOT IS_PED_ON_VEHICLE( a_Peds[ePed].ped )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
								TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							ELSE
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			
			CASE MPF_ENEMY_9
			CASE MPF_ENEMY_11
				IF a_Peds[ePed].iEvent >= 1
				AND a_Peds[ePed].iEvent < 3
					IF IS_PED_IN_ANY_VEHICLE( a_Peds[ePed].ped )
					AND IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
					AND IS_ENTITY_ON_FIRE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
						SAFE_OPEN_SEQUENCE()
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, TRUE )
							TASK_LEAVE_ANY_VEHICLE( NULL, GET_RANDOM_INT_IN_RANGE(500, 1501), ECF_JUMP_OUT )
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( NULL, FALSE )
							TASK_COMBAT_PED( NULL, PLAYER_PED_ID() )
						SAFE_PERFORM_SEQUENCE_TASK( a_Peds[ePed].ped )
						SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
						a_Peds[ePed].iEvent = 3
					ENDIF
				ENDIF
			
				SWITCH a_Peds[ePed].iEvent
					CASE 0
						IF ( IS_PLAYER_IN_SEA_PLANE() AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 125 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID()) AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Peds[ePed].ped ) <= 150 )
						OR ( IS_PED_ON_FOOT(PLAYER_PED_ID()) AND NOT IS_ENTITY_OCCLUDED(a_Peds[ePed].ped) )
							a_Peds[ePed].blip = CREATE_BLIP_FOR_PED( a_Peds[ePed].ped, TRUE )
							SET_BLIP_ALPHA( a_Peds[ePed].blip, 0 )
							SET_BLIP_HIDDEN_ON_LEGEND( a_Peds[ePed].blip, TRUE )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 1
						IF IS_PED_IN_ANY_VEHICLE(a_Peds[ePed].ped)
							IF IS_ENTITY_ALIVE( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped) )
								IF IS_VEHICLE_STUCK_TIMER_UP( GET_VEHICLE_PED_IS_IN(a_Peds[ePed].ped), VEH_STUCK_JAMMED, 6000 )
								OR IS_BOAT_STUCK_ON_LAND( ePed )
									TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, TRUE )
									SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_USE_VEHICLE, FALSE )
									a_Peds[ePed].iEvent++
								ELIF NOT IS_ENTITY_ALIVE( GET_PED_IN_VEHICLE_SEAT( GET_VEHICLE_PED_IS_IN( a_Peds[ePed].ped ) ) )
									SET_PED_COMBAT_ATTRIBUTES( a_Peds[ePed].ped, CA_LEAVE_VEHICLES, TRUE )
									TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
									a_Peds[ePed].iEvent = 3
								ENDIF
							ENDIF
						ELSE
							// Their AI has taken them out of their vehicle somehow
							TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							a_Peds[ePed].iEvent++
						ENDIF
					BREAK
					CASE 2
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING( a_Peds[ePed].ped, SCRIPT_TASK_GET_OFF_BOAT )
							IF NOT IS_PED_ON_VEHICLE( a_Peds[ePed].ped )
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( a_Peds[ePed].ped, FALSE )
								TASK_COMBAT_PED( a_Peds[ePed].ped, PLAYER_PED_ID() )
							ELSE
								TASK_GET_OFF_BOAT( a_Peds[ePed].ped, -1 )
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		IF IS_PED_BIT_FLAG_SET( ePed, MPBF_BLOCK_RAGDOLL )
			SET_PED_CAN_RAGDOLL( a_Peds[ePed].ped, TRUE )
			DETACH_ENTITY( a_Peds[ePed].ped, TRUE, FALSE )
			CLEAR_PED_BIT_FLAG( ePed, MPBF_BLOCK_RAGDOLL )
		ENDIF
		
		IF NOT IS_PED_BIT_FLAG_SET( ePed, MPBF_DONT_CLEANUP )
			IF IS_ENTITY_VISIBLE( a_Peds[ePed].ped )
				CLEANUP_PED( ePed, FALSE )
			ELSE
				CLEANUP_PED( ePed, TRUE )
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Updates a vehicle
//===============================================
PROC UPDATE_VEHICLE( MISSION_VEHICLE_FLAGS eVeh )
	IF IS_ENTITY_ALIVE( a_Vehicles[eVeh].veh )
	OR eVeh = MVF_PLANE
		SWITCH eVeh
			CASE MVF_BOAT_1
			CASE MVF_BOAT_2
			CASE MVF_BOAT_3
			CASE MVF_BOAT_4
				SWITCH a_Vehicles[eVeh].iEvent
					CASE 0
						IF NOT IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(a_Vehicles[eVeh].veh))
						AND NOT IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(a_Vehicles[eVeh].veh, VS_FRONT_RIGHT))
						AND NOT IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(a_Vehicles[eVeh].veh, VS_BACK_LEFT))
						AND NOT IS_ENTITY_ALIVE(GET_PED_IN_VEHICLE_SEAT(a_Vehicles[eVeh].veh, VS_BACK_RIGHT))
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(a_Vehicles[eVeh].veh, TRUE)
							SET_BOAT_ANCHOR(a_Vehicles[eVeh].veh, FALSE)
							a_Vehicles[eVeh].iEvent++
						ENDIF
					BREAK
					CASE 1
					
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ELSE
		IF IS_ENTITY_VISIBLE( a_Vehicles[eVeh].veh )
			CLEANUP_VEHICLE( eVeh, FALSE )
		ELSE
			CLEANUP_VEHICLE( eVeh, TRUE )
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Updates all mission peds
//===============================================
PROC UPDATE_ALL_PEDS()
	INT i
	REPEAT COUNT_OF( MISSION_PED_FLAGS ) i
		IF DOES_ENTITY_EXIST( a_Peds[i].ped )
			UPDATE_PED( INT_TO_ENUM( MISSION_PED_FLAGS, i ) )
		ENDIF
	ENDREPEAT
ENDPROC

//===============================================
//	Updates all mission vehs
//===============================================
PROC UPDATE_ALL_VEHICLES()
	INT i
	REPEAT COUNT_OF( MISSION_VEHICLE_FLAGS ) i
		IF DOES_ENTITY_EXIST( a_Vehicles[i].veh )
			UPDATE_VEHICLE( INT_TO_ENUM( MISSION_VEHICLE_FLAGS, i ) )
		ENDIF
	ENDREPEAT
ENDPROC

////===============================================
////	Updates all mission trains
////===============================================
//PROC UPDATE_ALL_TRAINS()
//	INT i
//	REPEAT COUNT_OF( MISSION_TRAIN_FLAGS ) i
//		IF DOES_ENTITY_EXIST( a_Trains[i].train )
//			UPDATE_TRAIN( INT_TO_ENUM( MISSION_TRAIN_FLAGS, i ) )
//		ENDIF
//	ENDREPEAT
//ENDPROC

//===============================================
//	Load scene with wait
//===============================================
PROC LOAD_SCENE_WITH_WAIT( VECTOR vPos, FLOAT fRadius = 20.0 )
	NEW_LOAD_SCENE_START_SPHERE( vPos, fRadius )
		
	WHILE IS_NEW_LOAD_SCENE_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_LOADED()
		WAIT(0)
	ENDWHILE

	NEW_LOAD_SCENE_STOP()
ENDPROC

//===============================================
//	Creates a mission vehicle
//===============================================
FUNC BOOL SPAWN_MISSION_VEHICLE( MISSION_VEHICLE_FLAGS eVeh, VECTOR vPosOverride, FLOAT fHeadingOverride = -1.0, BOOL bBlip = FALSE)
	
	IF NOT DOES_ENTITY_EXIST( a_Vehicles[eVeh].veh )
	
		// Values used to create & configure the vehicles
		VECTOR vPos
		FLOAT fHeading
		MODEL_NAMES eModel
		LOCK_STATE eLock = VEHICLELOCK_NONE
		BOOL bCollision = TRUE
		BOOL bVisible = TRUE
		BOOL bStrong = FALSE
		BOOL bFreeze = FALSE
		BOOL bTyresPop = TRUE
		BOOL bEngineOn = FALSE
		BOOL bHeliBlades = FALSE
		BOOL bEnemy = TRUE
		BOOL bInvincible = FALSE
		BOOL bLoadCollision = FALSE
		BOOL bTargetable = FALSE
		BOOL bAutoDamageEngine = FALSE
		BOOL bDisableTowing = FALSE
		BOOL bAutoAttach = TRUE
		BOOL bOnlyPlayerDamage = FALSE
		BOOL bAnchorBoat = FALSE
		BOOL bConsideredByPlayer = TRUE
		FLOAT fSpeed
		FLOAT fPlaneTurb = -1
		FLOAT fHeliTurb = -1
		ENTITY_INDEX attachEntity
		VECTOR vAttachPosOffset
		VECTOR vAttachRotOffset
		STRING strNumPlate
		#IF IS_DEBUG_BUILD STRING strName	#ENDIF

		// Configure each vehicle appropriately 
		SWITCH eVeh
			CASE MVF_PLANE
				#IF IS_DEBUG_BUILD	strName = "PLANE"	#ENDIF
				vPos = v_PlaneStart
				fHeading = 163.5528
				eModel = e_ModelPlane
				bEnemy = FALSE
				bDisableTowing = TRUE
				bAutoAttach = FALSE
				bAnchorBoat = TRUE
				bStrong = TRUE
			BREAK

			CASE MVF_BOAT_1
				#IF IS_DEBUG_BUILD	strName = "BOAT 1"	#ENDIF
				vPos = <<3087.114, 2195.558, 2>>
				fHeading = 157.680
				eModel = e_ModelArmBoat
				bOnlyPlayerDamage = TRUE
				bEngineOn = TRUE
				bAnchorBoat = TRUE
				bConsideredByPlayer = FALSE
				bDisableTowing = TRUE
		 		bAutoAttach = FALSE
			BREAK
			
			CASE MVF_BOAT_2
				#IF IS_DEBUG_BUILD	strName = "BOAT 2"	#ENDIF
				vPos = <<3092.788, 2197.243, 2>>
				fHeading = -22.320
				eModel = e_ModelMexBoat
				bOnlyPlayerDamage = TRUE
				bEngineOn = TRUE
				bAnchorBoat = TRUE
				bConsideredByPlayer = FALSE
				bDisableTowing = TRUE
		 		bAutoAttach = FALSE
			BREAK
			
			CASE MVF_BOAT_3
				#IF IS_DEBUG_BUILD	strName = "BOAT 3"	#ENDIF
				vPos = <<3170.4011, 1964.5874, 0.2889>>
				fHeading = 341.1273
				eModel = e_ModelMexBoat
				bOnlyPlayerDamage = TRUE
				bEngineOn = TRUE
				bAnchorBoat = TRUE
				bConsideredByPlayer = FALSE
				bDisableTowing = TRUE
		 		bAutoAttach = FALSE
			BREAK
			
			CASE MVF_BOAT_4
				#IF IS_DEBUG_BUILD	strName = "BOAT 4"	#ENDIF
				vPos = <<3227.12, 2074.91, 0.75>>
				fHeading = 175.16
				eModel = e_ModelArmBoat
				bOnlyPlayerDamage = TRUE
				bEngineOn = TRUE
				bAnchorBoat = TRUE
				bConsideredByPlayer = FALSE
				bDisableTowing = TRUE
		 		bAutoAttach = FALSE
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("SPAWN_MISSION_VEHICLE() - Invalid vehicle flag.")
			BREAK
		ENDSWITCH
		
		// Exit function is the model hasn't already been loaded
		IF NOT HAS_MODEL_LOADED( eModel )
			RETURN FALSE
			PRINTLN("SPAWN_MISSION_VEHICLE() - Model not loaded.")
		ENDIF
		
		// Default position and rotation can be overridden
		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
			vPos = vPosOverride
		ENDIF
		
		IF fHeadingOverride != -1.0
			fHeading = fHeadingOverride
		ENDIF
		
		// Create vehicle
		a_Vehicles[eVeh].veh = CREATE_VEHICLE( eModel, vPos, fHeading )
		SET_MISSION_VEHICLE_COLOURS( eVeh )
		SET_MISSION_VEHICLE_MODS( eVeh )
		SET_ENTITY_VISIBLE( a_Vehicles[eVeh].veh, bVisible )
		IF DOES_ENTITY_EXIST( attachEntity )
			ATTACH_ENTITY_TO_ENTITY( a_Vehicles[eVeh].veh, attachEntity, -1, vAttachPosOffset, vAttachRotOffset )
		ELSE
			SET_ENTITY_COLLISION( a_Vehicles[eVeh].veh, bCollision )
		ENDIF
		SET_VEHICLE_STRONG( a_Vehicles[eVeh].veh, bStrong )
		FREEZE_ENTITY_POSITION( a_Vehicles[eVeh].veh, bFreeze )
		SET_VEHICLE_TYRES_CAN_BURST( a_Vehicles[eVeh].veh, bTyresPop ) 
		IF bEngineOn
			SET_VEHICLE_ENGINE_ON( a_Vehicles[eVeh].veh, bEngineOn, TRUE )
		ENDIF
		SET_ENTITY_INVINCIBLE( a_Vehicles[eVeh].veh, bInvincible )
		SET_ENTITY_LOAD_COLLISION_FLAG( a_Vehicles[eVeh].veh, bLoadCollision )
		SET_VEHICLE_CAN_BE_TARGETTED( a_Vehicles[eVeh].veh, bTargetable )
		SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET( a_Vehicles[eVeh].veh, bAutoDamageEngine )
		SET_VEHICLE_AUTOMATICALLY_ATTACHES( a_Vehicles[eVeh].veh, bAutoAttach )
		SET_VEHICLE_DISABLE_TOWING( a_Vehicles[eVeh].veh, bDisableTowing )
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER( a_Vehicles[eVeh].veh, bOnlyPlayerDamage )
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER( a_Vehicles[eVeh].veh, bConsideredByPlayer )
		#IF IS_DEBUG_BUILD SET_VEHICLE_NAME_DEBUG(a_Vehicles[eVeh].veh, strName) #ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY( strNumPlate )
			SET_VEHICLE_NUMBER_PLATE_TEXT( a_Vehicles[eVeh].veh, strNumPlate )
		ENDIF
		
		IF fSpeed > 0
			SET_VEHICLE_FORWARD_SPEED( a_Vehicles[eVeh].veh, fSpeed )
		ENDIF
		
		IF bHeliBlades
			SET_HELI_BLADES_FULL_SPEED( a_Vehicles[eVeh].veh )
		ENDIF
		
		IF fPlaneTurb >= 0
			SET_PLANE_TURBULENCE_MULTIPLIER( a_Vehicles[eVeh].veh, fPlaneTurb )
		ENDIF
		
		IF fHeliTurb >= 0
			SET_HELI_TURBULENCE_SCALAR( a_Vehicles[eVeh].veh, fHeliTurb )
		ENDIF
		
		IF bAnchorBoat
			SET_BOAT_ANCHOR( a_Vehicles[eVeh].veh, TRUE )
		ENDIF
		
		IF eLock != VEHICLELOCK_NONE
			SET_VEHICLE_DOORS_LOCKED( a_Vehicles[eVeh].veh, eLock )
		ENDIF
		
		IF bBlip
			a_Vehicles[eVeh].blip = CREATE_BLIP_FOR_VEHICLE( a_Vehicles[eVeh].veh, bEnemy )
		ENDIF
		
		IF eVeh = MVF_PLANE
			CONTROL_LANDING_GEAR( a_Vehicles[eVeh].veh, LGC_RETRACT_INSTANT )
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

////===============================================
////	Creates a mission train
////===============================================
//FUNC BOOL SPAWN_MISSION_TRAIN( MISSION_TRAIN_FLAGS eTrain, VECTOR vPosOverride, BOOL bBlip = FALSE)
//	
//	IF NOT DOES_ENTITY_EXIST( a_Trains[eTrain].train )
//	
//		// Values used to create & configure the trains
//		VECTOR vPos
//		FLOAT fSpeed
//		STRING strTrain
//
//		// Configure each vehicle appropriately 
//		SWITCH eTrain
//			CASE MTF_EXAMPLE
//				vPos = V_ZERO
//				strTrain = ""
//				fSpeed = 0
//			BREAK
//
//			DEFAULT
//				SCRIPT_ASSERT("SPAWN_MISSION_TRAIN() - Invalid train flag.")
//			BREAK
//		ENDSWITCH
//		
//		// Default position and rotation can be overridden
//		IF NOT ARE_VECTORS_EQUAL( vPosOverride, V_ZERO )
//			vPos = vPosOverride
//		ENDIF
//		
//		// Create train
//		a_Trains[eTrain].train = CREATE_MISSION_TRAIN_BY_NAME( GET_HASH_KEY(strTrain), vPos, TRUE )
//		
//		SET_MISSION_TRAIN_SPEED( eTrain, fSpeed )
//		
//		IF bBlip
//			a_Trains[eTrain].blip = CREATE_BLIP_FOR_VEHICLE( a_Trains[eTrain].train, TRUE )
//		ENDIF
//	ENDIF
//		
//	RETURN TRUE
//ENDFUNC

//===============================================
//	Fade out and wait
//===============================================
PROC DO_FADE_OUT_WITH_WAIT( BOOL bHideHUDRadar = FALSE )
	IF NOT IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_OUT()		
			WAIT(0)
			IF bHideHUDRadar
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

//===============================================
//	Fade in and wait
//===============================================
PROC DO_FADE_IN_WITH_WAIT()
	IF NOT IS_SCREEN_FADED_IN()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		WHILE NOT IS_SCREEN_FADED_IN()		
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

////===============================================
////	Request all assets required for a specific stage
////===============================================
//PROC REQUEST_STAGE_ASSETS( MISSION_STAGE_FLAGS eStage, BOOL bLoadAll = TRUE )
//	bLoadAll = bLoadAll
//	SWITCH eStage
//		CASE MSF_STEAL_PLANE
//			
//		BREAK
//		
//		CASE MSF_MISSION_PASSED
//		
//		BREAK
//	ENDSWITCH
//ENDPROC
//
////===============================================
////	Check if all stage assets have been loaded
////===============================================
//FUNC BOOL HAVE_STAGE_ASSETS_LOADED( MISSION_STAGE_FLAGS eStage )
//	SWITCH eStage
//		CASE MSF_INTRO
//			RETURN TRUE
//		BREAK
//		
//		CASE MSF_MISSION_PASSED
//			RETURN TRUE
//		BREAK
//	ENDSWITCH
//	
//	RETURN FALSE
//ENDFUNC
//
////===============================================
////	Spawn all assets required for a specific stage
////===============================================
//PROC SPAWN_STAGE_ASSETS( MISSION_STAGE_FLAGS eStage )
//	SWITCH eStage
//		CASE MSF_INTRO
//			
//		BREAK
//
//		CASE MSF_MISSION_PASSED
//		
//		BREAK
//	ENDSWITCH
//ENDPROC
//
////===============================================
////	Load stage assets with wait. Used during
//// 	checkpoints & debug skips
////===============================================
//PROC CREATE_STAGE_ASSETS_WITH_WAIT( MISSION_STAGE_FLAGS eStage )
//	// Load assets
//	REQUEST_STAGE_ASSETS( eStage )
//		
//	// Wait until all have loaded
//	WHILE NOT HAVE_STAGE_ASSETS_LOADED( eStage )
//		WAIT(0)
//	ENDWHILE
//
//	// Spawn assets required
//	SPAWN_STAGE_ASSETS( eStage )
//ENDPROC

//===============================================
//	Remove mission text
//===============================================
PROC REMOVE_MISSION_TEXT( BOOL bClearSpeech = TRUE, BOOL bClearGodText = TRUE, BOOL bClearHelpText = TRUE, BOOL bKeepSubs = FALSE )
	IF bClearSpeech
		KILL_ANY_CONVERSATION()
	ENDIF
	
	IF bClearGodText
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		OR (bClearSpeech AND NOT bKeepSubs)
			CLEAR_PRINTS()
		ENDIF
		CLEAR_REMINDER_MESSAGE()
	ENDIF
	
	IF bClearHelpText
		CLEAR_HELP()
	ENDIF
ENDPROC

//===============================================
//	Add scenario blocking area
//===============================================
PROC ADD_MISSION_SCENARIO_BLOCKING_AREA( SCENARIO_BLOCKING_AREAS eArea )
	SWITCH eArea
		CASE SBA_CAVE
			a_ScenarioBlockingAreas[eArea] = ADD_SCENARIO_BLOCKING_AREA(<<3054.9910, 1995.8871, -20>>, <<3140.8171, 2225.6389, 10.4530>>)
		BREAK
	ENDSWITCH
ENDPROC

//===============================================
// 	Remove scenario blocking area
//===============================================
PROC REMOVE_MISSION_SCENARIO_BLOCKING_AREA( SCENARIO_BLOCKING_AREAS eArea )
	IF a_ScenarioBlockingAreas[eArea] != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(a_ScenarioBlockingAreas[eArea])
		a_ScenarioBlockingAreas[eArea] = NULL
	ENDIF
ENDPROC

//===============================================
// 	Remove all scenario blocking areas
//===============================================
PROC REMOVE_ALL_SCENARIO_BLOCKING_AREAS()
	INT i
	REPEAT COUNT_OF(SCENARIO_BLOCKING_AREAS) i
		REMOVE_MISSION_SCENARIO_BLOCKING_AREA(INT_TO_ENUM(SCENARIO_BLOCKING_AREAS,i))
	ENDREPEAT
ENDPROC


//===============================================
//	Suppress vehicle models
//===============================================
PROC SUPPRESS_MISSION_MODELS( BOOL bSuppress )
	bSuppress = bSuppress
ENDPROC

//===============================================
//	Toggle emergency services
//===============================================
PROC BLOCK_ALL_EMERGENCY_SERVICES( BOOL bBlock )
	IF bBlock
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
		SET_CREATE_RANDOM_COPS(FALSE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	ELSE
		SET_MAX_WANTED_LEVEL(5)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
		SET_CREATE_RANDOM_COPS(TRUE)
		ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE) 
		ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
		ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
    	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	ENDIF
ENDPROC

//===============================================
//	Go to the next stage
//===============================================
PROC ADVANCE_STAGE( MISSION_STAGE_FLAGS eNextStage )
	i_CurrentEvent = 0
	e_StageSection = MSS_SETUP
	e_MissionStage = eNextStage
ENDPROC

//===============================================
//	Disable the mod shops
//===============================================
PROC TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(BOOL bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_01_AP, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_05_ID2, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_06_BT1, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_07_CS1, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_08_CS6, bToggleFlag)
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(CARMOD_SHOP_SUPERMOD, bToggleFlag)
ENDPROC

//===============================================
//	Initialise mission
//===============================================
PROC SETUP_MISSION()
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF

	//REMOVE_MISSION_TEXT()
	SUPPRESS_MISSION_MODELS(TRUE)
	
	REGISTER_SCRIPT_WITH_AUDIO(TRUE)
	
//	// Load our text
//	REQUEST_ADDITIONAL_TEXT("CIAJET", DLC_TEXT_SLOT0)
	
	//BLOCK_ALL_EMERGENCY_SERVICES( TRUE )
	
	//SET_IGNORE_NO_GPS_FLAG( TRUE )
	
	//SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	//SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
	ADD_MISSION_SCENARIO_BLOCKING_AREA( SBA_CAVE )
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<3073.4102, 2161.6038, -20>>, <<3123.8157, 2216.8997, 9.0777>>, FALSE )
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA( <<3073.4102, 2161.6038, -20>>, <<3123.8157, 2216.8997, 9.0777>> )
	
	IF NOT IS_SPHERE_VISIBLE( <<3084.41, 2080.39, 0>>, 2.0 )
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<3077.5229, 2069.8306, -3.3524>>, <<3092.2957, 2092.0422, 2.7690>>, FALSE )
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA( <<3077.5229, 2069.8306, -3.3524>>, <<3092.2957, 2092.0422, 2.7690>> )
	ENDIF
	
	IF NOT IS_SPHERE_VISIBLE( <<3062.67, 2081.65, 0>>, 2.0 )
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<3054.3669, 2071.3608, -2.4173>>, <<3075.6523, 2091.9536, 2.7296>>, FALSE )
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA( <<3054.3669, 2071.3608, -2.4173>>, <<3075.6523, 2091.9536, 2.7296>> )
	ENDIF
	
	IF NOT IS_SPHERE_VISIBLE( <<3075.7163, 2125.5903, 0.8052>>, 2.0 )
		CLEAR_AREA_OF_PEDS( <<3075.7163, 2125.5903, 0.8052>>, 5.0 )
	ENDIF
	
	IF NOT IS_SPHERE_VISIBLE( <<3070.8816, 2103.9673, -2.0883>>, 2.0 )
		CLEAR_AREA_OF_VEHICLES( <<3070.8816, 2103.9673, -2.0883>>, 5.0 )
	ENDIF

	// Relationship groups
	ADD_RELATIONSHIP_GROUP("ENEMIES", relGroupEnemies)
//	ADD_RELATIONSHIP_GROUP("ALLIES", relGroupAllies)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, RELGROUPHASH_PLAYER)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, relGroupAllies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, relGroupEnemies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupEnemies, relGroupEnemies)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemies, relGroupAllies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relGroupEnemies, RELGROUPHASH_PLAYER)
	
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupAllies, relGroupAllies)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, relGroupAllies, RELGROUPHASH_PLAYER)
//	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupAllies, relGroupEnemies)

	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)
	
	// Conversation
	ADD_PED_FOR_DIALOGUE( s_Convo, 1, PLAYER_PED_ID(), "TREVOR" )
	
	SET_WANTED_LEVEL_MULTIPLIER( 0.0 )
	
	TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(TRUE)
	
	// Prevent random trains
//	DELETE_ALL_TRAINS()
//	SET_RANDOM_TRAINS(FALSE)
ENDPROC

//===============================================
//	Move ped out of vehicle
//===============================================
PROC SET_PLAYER_OUT_OF_ANY_VEHICLE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_ENTITY_COORDS( PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0,0,1>> )
	ENDIF
ENDPROC

//===============================================
//	Mission Cleanup
//===============================================
PROC MISSION_CLEANUP( BOOL bTerminate, BOOL bForce )

//	IF NOT HAS_CUTSCENE_FINISHED()
//		STOP_CUTSCENE()
//	ENDIF
	
	//SET_PLAYER_OUT_OF_ANY_VEHICLE()

	//REMOVE_CUTSCENE()

	RESET_VARIABLES()
	REMOVE_ALL_BLIPS()
	
	SUPPRESS_MISSION_MODELS( FALSE )
	REMOVE_MISSION_TEXT()
	
	BLOCK_ALL_EMERGENCY_SERVICES( FALSE )
	
	REMOVE_ALL_SCENARIO_BLOCKING_AREAS()
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<3054.9910, 1995.8871, -20>>, <<3140.8171, 2225.6389, 10.4530>>, TRUE )
	
	CLEAR_TIMECYCLE_MODIFIER()
	DISPLAY_RADAR( TRUE )
	
	SET_IGNORE_NO_GPS_FLAG( FALSE )
	
	REMOVE_ALL_ANIM_DICTS()
	REMOVE_ALL_WAYPOINTS()
	REMOVE_ALL_VEH_RECS()
	
	CLEANUP_PEDS( bForce )
	CLEANUP_VEHICLES( bForce )
//	CLEANUP_TRAINS( bForce )
	
	REMOVE_RELATIONSHIP_GROUP( relGroupEnemies )
	
	SET_WANTED_LEVEL_MULTIPLIER( 1.0 )
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_ANY_CONVERSATION()
	
	REMOVE_PED_FOR_DIALOGUE( s_Convo, 1 )
	REMOVE_PED_FOR_DIALOGUE( s_Convo, 3 )
	REMOVE_PED_FOR_DIALOGUE( s_Convo, 4 )
	REMOVE_PED_FOR_DIALOGUE( s_Convo, 5 )
	
	DESTROY_ALL_CAMS()
	RENDER_SCRIPT_CAMS( FALSE, FALSE )
	
	TOGGLE_CAR_MOD_SHOPS_UNAVAILABLE(FALSE)
	
	IF bTerminate
		#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(widGroup)
				DELETE_WIDGET_GROUP(widGroup)
			ENDIF
		#ENDIF
	
		RANDOM_EVENT_OVER()
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//===============================================
//	Mission Passed
//===============================================
PROC MISSION_PASSED()
	
	// Activate the reward vehicle gens.
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RE_SEAPLANE_CITY, TRUE)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_RE_SEAPLANE_COUNTRY, TRUE)

	RANDOM_EVENT_PASSED()
	RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
	MISSION_CLEANUP(TRUE, FALSE)
ENDPROC

//===============================================
//	Mission Failed
//===============================================
PROC MISSION_FAILED()
	IF IS_ENTITY_ALIVE(a_Vehicles[MVF_PLANE].veh)
		SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_PLANE].veh, VEHICLELOCK_LOCKED )
	ENDIF
	MISSION_CLEANUP(TRUE, FALSE)
ENDPROC

//===============================================
//	Warp the player to a specific point, optionally reset the camera
//===============================================
PROC WARP_PLAYER( VECTOR vPos, FLOAT fHeading, BOOL bSetCamera = TRUE, BOOL bRemoveWeapon = TRUE )
	SET_ENTITY_COORDS( PLAYER_PED_ID(), vPos )
	SET_ENTITY_HEADING( PLAYER_PED_ID(), fHeading )
	IF bSetCamera
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	ENDIF
	IF bRemoveWeapon
		SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), WEAPONTYPE_UNARMED )
	ENDIF
ENDPROC

//===============================================
//	Checks if vehicle has a driver
//===============================================
FUNC BOOL DOES_VEHICLE_HAVE_A_DRIVER( VEHICLE_INDEX veh )

	IF IS_ENTITY_ALIVE( veh )
	AND NOT IS_VEHICLE_SEAT_FREE( veh )
		PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT( veh )
		
		IF IS_ENTITY_ALIVE( pedDriver )
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//===============================================
// Checks if player has lost or killed a ped
//===============================================
FUNC BOOL HAS_PED_BEEN_LOST_OR_KILLED( MISSION_PED_FLAGS ePed, BOOL bIgnoreUnblipped = TRUE )
	IF NOT IS_ENTITY_ALIVE( a_Peds[ePed].ped )
		RETURN TRUE	// Ped killed
	ELSE
		IF GET_DISTANCE_BETWEEN_ENTITIES( a_Peds[ePed].ped, PLAYER_PED_ID() ) >= 400
			CLEANUP_PED( ePed, FALSE )	// Ped lost
			RETURN TRUE
		ENDIF
		
		IF bIgnoreUnblipped
			// Waiting peds haven't been triggered yet.
			INT iPed = ENUM_TO_INT( ePed )
			IF iPed >= ENUM_TO_INT(MPF_ENEMY_8)
			AND NOT DOES_BLIP_EXIST(a_Peds[ePed].blip)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//================================================================================================================================================
//		DEBUG FUNCTIONS/PROCEDURES
//================================================================================================================================================
#IF IS_DEBUG_BUILD	
	//===============================================
	//	Create widgets
	//===============================================
	PROC CREATE_DEBUG_WIDGETS()
		widGroup = START_WIDGET_GROUP("RE - Sea Plane")
			ADD_WIDGET_INT_READ_ONLY( "Current Event:", i_CurrentEvent )
		STOP_WIDGET_GROUP()
	ENDPROC
	
	//===============================================
	//	Debug update called every frame
	//===============================================
	PROC UPDATE_DEBUG()	
		// Debug pass/fail
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED( KEY_NUMPAD5 )
		AND IS_ENTITY_ALIVE( a_Vehicles[MVF_PLANE].veh )
		AND NOT IS_PED_SITTING_IN_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_PLANE].veh )
			SET_PED_INTO_VEHICLE( PLAYER_PED_ID(), a_Vehicles[MVF_PLANE].veh )
			SET_VEHICLE_ENGINE_ON( a_Vehicles[MVF_PLANE].veh, TRUE, TRUE )
		ENDIF
	ENDPROC
	
	//===============================================
	//	Initialise debug components
	//===============================================
	PROC SETUP_DEBUG()
		CREATE_DEBUG_WIDGETS()
	ENDPROC
#ENDIF

//===============================================
//	Steal plane
//===============================================
PROC DO_STAGE_STEAL_PLANE()
	SWITCH e_StageSection
		CASE MSS_SETUP
			// Mission setup
			SETUP_MISSION()
			
			// Debug setup
			#IF IS_DEBUG_BUILD
				SETUP_DEBUG()
			#ENDIF
			
			SET_MISSION_BIT_FLAG(MBF_MISSION_SETUP)
			
			REQUEST_MODEL( e_ModelMexBoat )
			
			// Progress to active state
			e_StageSection = MSS_ACTIVE
		BREAK
		
		CASE MSS_ACTIVE
			SWITCH i_CurrentEvent
				CASE 0
					IF HAS_MODEL_LOADED( e_ModelMexBoat )
						REQUEST_MODEL( e_ModelPlane )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 1
					IF HAS_MODEL_LOADED( e_ModelPlane )
						REQUEST_MODEL( e_ModelMex )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 2
					IF HAS_MODEL_LOADED( e_ModelMex )
						REQUEST_MODEL( e_ModelArmBoat )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 3
					IF HAS_MODEL_LOADED( e_ModelArmBoat )
						REQUEST_MODEL( e_ModelArm )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 4
					IF HAS_MODEL_LOADED( e_ModelArm )
						REQUEST_WAYPOINT_RECORDING( str_WayRouteA )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 5
					IF GET_IS_WAYPOINT_RECORDING_LOADED( str_WayRouteA )
						REQUEST_WAYPOINT_RECORDING( str_WayRouteB )
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 6
					IF HAS_MODEL_LOADED( e_ModelMexBoat )
					AND HAS_MODEL_LOADED( e_ModelPlane )
					AND HAS_MODEL_LOADED( e_ModelMex )
					AND HAS_MODEL_LOADED( e_ModelArm )
					AND HAS_MODEL_LOADED( e_ModelArmBoat )
					AND GET_IS_WAYPOINT_RECORDING_LOADED( str_WayRouteA )
					AND GET_IS_WAYPOINT_RECORDING_LOADED( str_WayRouteB )
						
						SPAWN_MISSION_VEHICLE( MVF_PLANE, V_ZERO, -1, TRUE )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelPlane )
						
						SPAWN_MISSION_VEHICLE( MVF_BOAT_1, V_ZERO )
						SPAWN_MISSION_VEHICLE( MVF_BOAT_2, V_ZERO )
						//SPAWN_MISSION_VEHICLE( MVF_BOAT_3, V_ZERO )
						
						INT i
						FOR i = ENUM_TO_INT(MPF_ENEMY_1) TO ENUM_TO_INT(MPF_ENEMY_5)
							SPAWN_MISSION_PED( INT_TO_ENUM(MISSION_PED_FLAGS, i), FALSE, V_ZERO )
						ENDFOR
						
						SET_RANDOM_EVENT_ACTIVE()
		
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 7
					IF IS_PLAYER_IN_SEA_PLANE()
						SAFE_REMOVE_BLIP( a_Vehicles[MVF_PLANE].blip )
					
						SET_MISSION_BIT_FLAG( MBF_PLANE_STOLEN )
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemies, RELGROUPHASH_PLAYER)
						
						SPAWN_MISSION_VEHICLE( MVF_BOAT_3, V_ZERO )
						SPAWN_MISSION_VEHICLE( MVF_BOAT_4, V_ZERO )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelMexBoat )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelArmBoat )
						
						SPAWN_MISSION_PED( MPF_ENEMY_8, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_ENEMY_9, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_ENEMY_10, FALSE, V_ZERO )
						SPAWN_MISSION_PED( MPF_ENEMY_11, FALSE, V_ZERO )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelMex )
						SET_MODEL_AS_NO_LONGER_NEEDED( e_ModelArm )
						
						i_CurrentEvent++
					ENDIF
					
					IF IS_PLAYER_IN_CAVE()
						IF NOT IS_PED_SWIMMING_UNDER_WATER( PLAYER_PED_ID() )
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupEnemies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupEnemies, RELGROUPHASH_PLAYER)
						ELSE
							IF NOT IS_MISSION_BIT_FLAG_SET( MBF_PLAYER_SPOTTED )
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, relGroupEnemies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, relGroupEnemies, RELGROUPHASH_PLAYER)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 8
					BOOL bProceed
					bProceed = TRUE
					
					INT i
					FOR i = ENUM_TO_INT(MPF_ENEMY_1) TO ENUM_TO_INT(MPF_ENEMY_11)
						IF NOT HAS_PED_BEEN_LOST_OR_KILLED( INT_TO_ENUM(MISSION_PED_FLAGS, i), FALSE )
							bProceed = FALSE
						ENDIF
					ENDFOR
					
					IF bProceed
						i_CurrentEvent++
					ENDIF
				BREAK
				CASE 9
					IF IS_ENTITY_ALIVE( a_Vehicles[MVF_PLANE].veh )
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), v_PlaneStart ) >= 200
						AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( a_Vehicles[MVF_PLANE].veh, v_PlaneStart ) >= 200
							e_StageSection = MSS_CLEANUP
						ENDIF
					ELSE
						MISSION_FAILED()
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MSS_CLEANUP
			// Move to next stage
			ADVANCE_STAGE( MSF_MISSION_PASSED )
		BREAK
	ENDSWITCH
ENDPROC

//===============================================
//	Mission passed stage
//===============================================
PROC DO_STAGE_MISSION_PASSED()
	SWITCH e_StageSection
		CASE MSS_SETUP
			// Progress to active state
			e_StageSection = MSS_ACTIVE
		BREAK
		
		CASE MSS_ACTIVE
			SWITCH i_CurrentEvent
				CASE 0
					e_StageSection = MSS_CLEANUP
				BREAK
			ENDSWITCH
		BREAK
		
		CASE MSS_CLEANUP
			// Passed
			MISSION_PASSED()
		BREAK
	ENDSWITCH
ENDPROC

//===============================================
//	Check for fail
//===============================================
PROC FAIL_CHECKS()
	BOOL bFail
	INT i
	// Abandoning the plane
	IF IS_MISSION_BIT_FLAG_SET( MBF_PLANE_STOLEN )
	OR IS_MISSION_BIT_FLAG_SET( MBF_KICKOFF )
		IF IS_ENTITY_ALIVE( a_Vehicles[MVF_PLANE].veh )
		AND GET_DISTANCE_BETWEEN_ENTITIES( PLAYER_PED_ID(), a_Vehicles[MVF_PLANE].veh ) >= 250
			bFail = TRUE
			FOR i = ENUM_TO_INT(MPF_ENEMY_1) TO ENUM_TO_INT(MPF_ENEMY_11)
				IF NOT HAS_PED_BEEN_LOST_OR_KILLED( INT_TO_ENUM(MISSION_PED_FLAGS, i) )
					bFail = FALSE
				ENDIF
			ENDFOR
			
			IF bFail
				MISSION_FAILED()
			ENDIF
		ENDIF
	ENDIF
	
	// Destroying the plane
	IF DOES_ENTITY_EXIST( a_Vehicles[MVF_PLANE].veh )
	AND NOT IS_ENTITY_ALIVE( a_Vehicles[MVF_PLANE].veh )
		bFail = TRUE
		FOR i = ENUM_TO_INT(MPF_ENEMY_1) TO ENUM_TO_INT(MPF_ENEMY_11)
			IF NOT HAS_PED_BEEN_LOST_OR_KILLED( INT_TO_ENUM(MISSION_PED_FLAGS, i) )
				bFail = FALSE
			ENDIF
		ENDFOR
		
		IF bFail
			MISSION_FAILED()
		ENDIF
	ENDIF
ENDPROC

//===============================================
//	Main update function called every frame
//===============================================
PROC UPDATE_MAIN()
	IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
		IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
			MISSION_CLEANUP( TRUE, FALSE )
		ENDIF
	ENDIF
	
	REPLAY_CHECK_FOR_EVENT_THIS_FRAME("RE_SEAPLANE")
	
	// Check for fail
	FAIL_CHECKS()

	// Update peds, vehicles, objects & blips
	UPDATE_ALL_PEDS()
	UPDATE_ALL_VEHICLES()
	UPDATE_ALL_BLIPS()
	
	// Distributed LOS checks
	UPDATE_SCRIPT_ENTITY_LOS_DISTRIBUTED_CHECKS( s_LOSChecks, i_LOSCheckIndex )
ENDPROC

//===============================================
//	As I bypass the height check of CAN_RANDOM_EVENT_LAUNCH(),
// 	a separate check is required here.
//	url:bugstar:2035010
//===============================================
FUNC BOOL IS_OK_TO_LAUNCH_RE_SEAPLANE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())

		VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID())
		FLOAT fGroundZ
		GET_GROUND_Z_FOR_3D_COORD(vPlayer, fGroundZ)
	
		// Would see stuff spawn
		IF vPlayer.Y < 2149.0417
		AND ( (IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())) OR (IS_ENTITY_IN_AIR(PLAYER_PED_ID()) AND (vPlayer.Z - fGroundZ) >= 7.5) )
		AND (vPlayer.Z - fGroundZ) >= 3 // Player is off ground in thei flying vehicle
			RETURN FALSE
		ENDIF
		
		// Too high, probably just passing by!
		IF vPlayer.Z >= 100
		AND IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//================================================================================================================================================
//		MAIN SCRIPT LOOP
//================================================================================================================================================
SCRIPT(coords_struct in_coords)
	v_Input = in_coords.vec_coord[0]
	
	// Handle force cleanup
	IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		IF IS_ENTITY_ALIVE(a_Vehicles[MVF_PLANE].veh)
			SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_PLANE].veh, VEHICLELOCK_LOCKED )
		ENDIF
		MISSION_CLEANUP(TRUE,FALSE)
	ENDIF
	
	IF IS_OK_TO_LAUNCH_RE_SEAPLANE()
	AND CAN_RANDOM_EVENT_LAUNCH(v_Input, RE_SEAPLANE, DEFAULT, DEFAULT, TRUE)
		LAUNCH_RANDOM_EVENT(RE_SEAPLANE)
	ELSE
		TERMINATE_THIS_THREAD()
	ENDIF
	
    WHILE TRUE	
	
		IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
		OR IS_MISSION_BIT_FLAG_SET( MBF_PLANE_STOLEN )
		OR IS_MISSION_BIT_FLAG_SET( MBF_KICKOFF )
			// Updates required regardless of stage
			IF IS_MISSION_BIT_FLAG_SET( MBF_MISSION_SETUP )
				UPDATE_MAIN()
			ENDIF

			// Update flow
			SWITCH e_MissionStage
				CASE MSF_STEAL_PLANE
					DO_STAGE_STEAL_PLANE()
				BREAK
				CASE MSF_MISSION_PASSED
					DO_STAGE_MISSION_PASSED()
				BREAK
			ENDSWITCH
		ELSE
			IF IS_ENTITY_ALIVE(a_Vehicles[MVF_PLANE].veh)
				SET_VEHICLE_DOORS_LOCKED( a_Vehicles[MVF_PLANE].veh, VEHICLELOCK_LOCKED )
			ENDIF
			MISSION_CLEANUP( TRUE, FALSE )
		ENDIF
	
		// Debug updates
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG()
		#ENDIF
		
		WAIT(0)
    ENDWHILE
ENDSCRIPT
