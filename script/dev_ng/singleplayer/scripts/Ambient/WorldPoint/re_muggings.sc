//                                      Random Events: Mugging
//                                              
                                                    // Vicki

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT     USE_TU_CHANGES  0 // Removed by Kenneth R.

// Includes -----------------------------------------------//
USING "rage_builtins.sch"
USING "commands_player.sch"
USING "globals.sch"
USING "commands_brains.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "cutscene_public.sch"
USING "commands_misc.sch"
USING "brains.sch"
USING "commands_camera.sch"
USING "script_player.sch"
USING "commands_object.sch"
USING "script_maths.sch"
USING "commands_debug.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "Ambient_Common.sch"
USING "completionpercentage_public.sch"
USING "commands_event.sch"
USING "locates_public.sch"
USING "randomChar_public.sch"
USING "random_events_public.sch"
USING "load_queue_public.sch"

// Variables ----------------------------------------------//
ENUM ambStageFlag
    ambCanRun,
    ambRunning
ENDENUM
ambStageFlag ambStage = ambCanRun

ENUM eventVariations
    TARGET_NONE,
    GIRL_SHOPPER,
    GUY_VICTIM,
    HOLD_UP_ANIM,
    TARGET_END
ENDENUM
eventVariations thisEvent = TARGET_NONE

ENUM MuggingActionStages
    SET_UP,
    HELDUPANIM_VARIATION,
    MUGGING_INTERACTION,
    EARLY_INTERVENTION,
    ABLE_TO_RETRIEVE_DROPPED_BAG,
    PLAYER_HAS_BAG,
    GIVEN_UP_ON_HELP_FROM_PLAYER,
    VIC_FROM_ANIM_THANKS,
    LOST_MUGGER,
    ENDING_EVENT,
    ATTACKED_BEFORE_MUGGING,
    AIMED_AT_VIC,
    GET_FOR_SELF,
    STAT_OUTCOME
ENDENUM
MuggingActionStages thisMuggingStage = SET_UP

ENUM MuggingSequenceStages
    INITIAL_MUGGING_SEQ,
    BEEN_MUGGED,
    VIC_CRY_OUT,
    MUGGER_THREATEN,
    ASK_PLAYER_FOR_HELP,
    WAITING_FOR_PLAYERS_HELP
ENDENUM
MuggingSequenceStages thisSequenceStage = INITIAL_MUGGING_SEQ

BOOL bVariablesInitialised = FALSE


BOOL bReactingToPlayer = FALSE, bVictimGotWallet
BOOL bVictimGoneToPlayer = FALSE
BOOL bCryOut = FALSE
BOOL bStartedMugging = FALSE
//BOOL bAttackPlayer = FALSE
BOOL bVictimFled = FALSE
BOOL bGirlMoving  = FALSE
BOOL bWarningIssued, bMuggerResponds, bPurseHasDropped, bPlayedVictimDialogue

PED_INDEX   pedVictim
PED_INDEX   pedAssailant

OBJECT_INDEX objIdStolenItem

BLIP_INDEX blipVictim
BLIP_INDEX blipMugger
BLIP_INDEX blipStolenItem
BLIP_INDEX blipLocation

VECTOR vCreateVictim
VECTOR vCreateAssailant
FLOAT fCreateVictim, fLocateSize, fStoppingDistance
FLOAT fCreateAssailant
FLOAT fRespot
Vector vRespot
    
MODEL_NAMES modelVictim
MODEL_NAMES modelAssailant
MODEL_NAMES modelBag

REL_GROUP_HASH relGroup_assailant

VECTOR vMuggingGoToPoint

INT iSelectedVariation

VECTOR vWorldPoint1 = << -131.0520, -1627.0002, 31.1755 >> 
VECTOR vWorldPoint2 = << 287.888, -284.603, 52.967 >>
VECTOR vWorldPoint3 = << -319.66, -832.28, 31.61 >>
VECTOR vWorldPoint4 = << 31, -1019, 28.5 >>


CONST_INT ONE_GUY_VICTIM        1
CONST_INT TWO_HOLD_UP_ANIM      2
CONST_INT THREE_GIRL_SHOPPER    3
CONST_INT FOUR_GIRL_SHOPPER     4

STRING sTextBlock
STRING sVicDrop, sShout, sAsk, sDrop, sPlayerReturn, sThx, sGiveUp
STRING sDemand, sWho, sGun, sFut
STRING sHandover, sWarn
STRING victimVoice, assailantVoice
STRING assailantAmbientVoice
STRING victimAmbientVoice = ""

SEQUENCE_INDEX seq
VECTOR vInput

VECTOR scenePosition
VECTOR sceneRotation
INT sceneId

CAMERA_INDEX CamIDPickingUpBag
BOOL bVictimDroppedBag = FALSE
BOOL bRobbedTimerSet = FALSE
//BOOL bRobberGivenPickupTask = FALSE
BOOL bDropYourBagSpeech = FALSE

BOOL bAskPlayerForHelpLine = FALSE

BOOL bFightingWithPlayer = FALSE

INT iRobbedTimer
INT iRobbedTimerCurrent

INT iHoldUpStages, iShockingEvent, iFlashTimestamp

INT iAttackType

INT iPlacementFlags = 0


VECTOR vInitialMarkerPos
BOOL bSavedVictim = FALSE

BOOL bMakingARunForIt = FALSE, bMuggerFleeing
INT iHandOver
BOOL bRunHandover = FALSE, bRunHint, bVictimGaveUp
BOOL bAskedPlayerForHelp = FALSE
BOOL bDoFullCleanUp = FALSE

BOOL bNEnd = FALSE
BOOL bSEnd = FALSE
BOOL bInAlley = FALSE
BOOL bStopppedAtPlayer = FALSE

BOOL bRemovedMuggerFromDialogue  = FALSE
BOOL bRemovedVictimFromDialogue  = FALSE
BOOL bFleeingPlayer = FALSE
INT iAnimSeqList

FLOAT fStoreBagCollectDist

VECTOR  vNEndOfAlley1 
VECTOR  vNEndOfAlley2 
VECTOR  vSEndOfAlley1 
VECTOR  vSEndOfAlley2 
VECTOR  vInAlley1
VECTOR  vInAlley2 
VECTOR vDroppedItemPlacement

INT iTimerStartMuggingAnim
INT iTimerCurrentMuggingAnim
INT iTimerStartAskLeaveAnim
BOOL bSetTimer

PICKUP_INDEX pickupStolenItem
INT iItemValue
INT iTurnStage
structPedsForConversation MuggingConversation
STRING sMuggingDict = "RANDOM@MUGGING3"
STRING sPickupDict = "pickup_object"
STRING sHandoverDict, sHandoverPlayer, sHandoverPed, sHandoverCam
STRING sStruggleThief, sStrugglePed, sFleeThiefBack, sFleePedBack, sFleeThiefFront, sFleePedFront   
VECTOR animScenePosition, animSceneRotation
STRING sReturnDict = "RANDOM@BICYCLE_THIEF@RETURN_FRONT"
STRING sClipSetOverride
INT iBlockObject = -1
CHASE_HINT_CAM_STRUCT       localChaseHintCamStruct

INT flashTimer

LoadQueueLarge sLoadQueue

//===============================================================================
// Functions ----------------------------------------------//
//===============================================================================
PROC getWorldPoint()
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        
        IF VDIST(vInput, vWorldPoint1) < 20
            iSelectedVariation = ONE_GUY_VICTIM
            thisEvent = GUY_VICTIM 
        ENDIF
        
        IF VDIST(vInput, vWorldPoint2) < 20 
            iSelectedVariation = TWO_HOLD_UP_ANIM
            thisEvent = HOLD_UP_ANIM
        ENDIF
        
        IF VDIST(vInput, vWorldPoint3) < 20 
            iSelectedVariation = THREE_GIRL_SHOPPER
            thisEvent = GIRL_SHOPPER
        ENDIF
        
        IF VDIST(vInput, vWorldPoint4) < 20
            iSelectedVariation = FOUR_GIRL_SHOPPER
            thisEvent = GIRL_SHOPPER
        ENDIF
    ENDIF
ENDPROC

PROC missionCleanup()

    #IF IS_DEBUG_BUILD

    #ENDIF
    
    IF bDoFullCleanUp
        IF bVariablesInitialised
            SET_MODEL_AS_NO_LONGER_NEEDED(modelVictim)
            SET_MODEL_AS_NO_LONGER_NEEDED(modelAssailant)
            SET_MODEL_AS_NO_LONGER_NEEDED(modelBag)
        ENDIF
        
        IF NOT IS_PED_INJURED(pedAssailant)
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 150, -1)
            ENDIF
            SET_PED_KEEP_TASK(pedAssailant, TRUE)
            IF NOT bPurseHasDropped
                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - SET_PED_MONEY(pedAssailant, iItemValue)")
                
                SET_PED_MONEY(pedAssailant, iItemValue)
                
            ENDIF
        ENDIF
        
        IF DOES_ENTITY_EXIST(pedVictim)
            IF NOT IS_PED_INJURED(pedVictim)
                SET_PED_CAN_BE_TARGETTED(pedVictim, TRUE)
                IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_WANDER_STANDARD) = FINISHED_TASK
                AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                    OPEN_SEQUENCE_TASK(seq)
                        IF IS_PED_DUCKING(pedVictim)
                            TASK_TOGGLE_DUCK(NULL, TOGGLE_DUCK_OFF)
                        ENDIF
                        TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 9000)
                        //TASK_PLAY_ANIM(NULL,"MOVE_M@GENERIC", "idle", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
                        TASK_SMART_FLEE_COORD(NULL, vInput, 250, -1)
                    CLOSE_SEQUENCE_TASK(seq)
                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                    CLEAR_SEQUENCE_TASK(seq)
                    SET_PED_KEEP_TASK(pedVictim, TRUE)
                    IF NOT bVictimGaveUp
                    AND NOT bSavedVictim
                                                                       
                        IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                            KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
                        ENDIF
                        
                    ENDIF
                ENDIF
            ENDIF
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 3")
        ENDIF
        
        WAIT(0)
        
        IF DOES_BLIP_EXIST(blipMugger)
            REMOVE_BLIP(blipMugger)
        ENDIF
        
        IF DOES_BLIP_EXIST(blipVictim)
            REMOVE_BLIP(blipVictim)
        ENDIF
        
        IF DOES_ENTITY_EXIST(objIdStolenItem)
            SET_OBJECT_AS_NO_LONGER_NEEDED(objIdStolenItem)
        ENDIF
        
        IF DOES_BLIP_EXIST(blipStolenItem)
            REMOVE_BLIP(blipStolenItem)
        ENDIF
    
    ENDIF
    
    IF iBlockObject != -1
        IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iBlockObject)
            REMOVE_NAVMESH_BLOCKING_OBJECT(iBlockObject)
        ENDIF
        iBlockObject = -1
    ENDIF
    
    IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
        SET_CREATE_RANDOM_COPS(TRUE)
        SET_WANTED_LEVEL_MULTIPLIER(1)
    ENDIF
    
    KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
    SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
    RANDOM_EVENT_OVER()
    
    CLEANUP_LOAD_QUEUE_LARGE(sLoadQueue)
    
    IF iShockingEvent <> 0
        REMOVE_SHOCKING_EVENT(iShockingEvent)
    ENDIF
    TERMINATE_THIS_THREAD()
ENDPROC

PROC REGISTER_AS_COMPLETE()
    SWITCH iSelectedVariation
        CASE ONE_GUY_VICTIM     
            REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG1)
        BREAK
        CASE TWO_HOLD_UP_ANIM 
            REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG2)
        BREAK
        CASE THREE_GIRL_SHOPPER 
            REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG3)
        BREAK
        CASE FOUR_GIRL_SHOPPER 
            REGISTER_SCRIPT_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG4)
        BREAK
    ENDSWITCH
ENDPROC

PROC missionPassed()
//  REGISTER_AS_COMPLETE()
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
    ENDIF
    
    RANDOM_EVENT_PASSED(RE_MUGGING, iSelectedVariation)
    LOAD_AND_PLAY_RANDOM_EVENT_PASS_MUSIC()
    RUN_AUTOSAVE_AFTER_RANDOM_EVENT_PASS()
    missionCleanup()
ENDPROC

PROC SET_FEMALE_ANIMS()
    sHandoverDict = "RANDOM@ATM_ROBBERY@RETURN_WALLET_FEMALE"
    SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
        CASE 0
            sHandoverPlayer = "Return_Wallet_Positive_A_Player"
            sHandoverPed = "Return_Wallet_Positive_A_Female"
            sHandoverCam = "Return_Wallet_Positive_A_Cam"
        BREAK
        CASE 1
            sHandoverPlayer = "Return_Wallet_Positive_B_Player"
            sHandoverPed = "Return_Wallet_Positive_B_Female"
            sHandoverCam = "Return_Wallet_Positive_B_Cam"
        BREAK
        CASE 2
            sHandoverPlayer = "Return_Wallet_Positive_C_Player"
            sHandoverPed = "Return_Wallet_Positive_C_Female"
            sHandoverCam = "Return_Wallet_Positive_C_Cam"
        BREAK
    ENDSWITCH
ENDPROC

PROC SET_MALE_ANIMS()
    sHandoverDict = "RANDOM@ATM_ROBBERY@RETURN_WALLET_MALE"

    SWITCH GET_RANDOM_INT_IN_RANGE(0,3)
        CASE 0
            sHandoverPlayer = "Return_Wallet_Positive_A_Player"
            sHandoverPed = "Return_Wallet_Positive_A_Male"
            sHandoverCam = "Return_Wallet_Positive_A_Cam"
        BREAK
        CASE 1
            sHandoverPlayer = "Return_Wallet_Positive_B_Player"
            sHandoverPed = "Return_Wallet_Positive_B_Male"
            sHandoverCam = "Return_Wallet_Positive_B_Cam"
        BREAK
        CASE 2
            sHandoverPlayer = "Return_Wallet_Positive_C_Player"
            sHandoverPed = "Return_Wallet_Positive_C_Male"
            sHandoverCam = "Return_Wallet_Positive_C_Cam"
        BREAK
    ENDSWITCH
ENDPROC

PROC INITIALISE_MUGGING_SCENE()
    VECTOR vAnimOffsetRot
    //set ped's position for anim
    SET_ENTITY_COORDS_NO_OFFSET(pedAssailant,  GET_ANIM_INITIAL_OFFSET_POSITION(sMuggingDict, sStruggleThief,  animScenePosition, animSceneRotation))
    //set ped's heading for anim
    vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sMuggingDict, sStruggleThief,  animScenePosition, animSceneRotation)
    IF vAnimOffsetRot.z < 0
        vAnimOffsetRot.z += 360
    ENDIF
    SET_ENTITY_HEADING(pedAssailant, vAnimOffsetRot.z)
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> SET_ENTITY_HEADING pedAssailant = ", vAnimOffsetRot.z)

    //set ped's position for anim
    SET_ENTITY_COORDS_NO_OFFSET(pedVictim,  GET_ANIM_INITIAL_OFFSET_POSITION(sMuggingDict, sStrugglePed,  animScenePosition, animSceneRotation))
    //set ped's heading for anim
    vAnimOffsetRot = GET_ANIM_INITIAL_OFFSET_ROTATION(sMuggingDict, sStrugglePed,  animScenePosition, animSceneRotation)
    IF vAnimOffsetRot.z < 0
        vAnimOffsetRot.z += 360
    ENDIF
    SET_ENTITY_HEADING(pedVictim, vAnimOffsetRot.z)
    CPRINTLN(DEBUG_AMBIENT,  GET_THIS_SCRIPT_NAME(), "> SET_ENTITY_HEADING pedVictim = ", vAnimOffsetRot.z)

    TASK_PLAY_ANIM(pedAssailant, sMuggingDict, sStruggleThief, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE )
    TASK_PLAY_ANIM(pedVictim, sMuggingDict, sStrugglePed, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE )
ENDPROC

PROC SET_MUGGER_TO_FLEE()
    PRINTSTRING("PAUL - SET_MUGGER_TO_FLEE") PRINTNL()
    
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> GET_ENTITY_HEADING(PLAYER_PED_ID()) = ", GET_ENTITY_HEADING(PLAYER_PED_ID()))
    ENDIF
    IF IS_PED_FACING_PED(pedAssailant, PLAYER_PED_ID(), 90)
        PRINTSTRING("PAUL - IS_PED_FACING_PED") PRINTNL()
        TASK_PLAY_ANIM(pedAssailant, sMuggingDict, sFleeThiefBack, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE )
        IF thisEvent = GUY_VICTIM
            OPEN_SEQUENCE_TASK(seq)
                TASK_PLAY_ANIM(NULL, sMuggingDict, sFleePedBack, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_A", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_B", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_C", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_A", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_B", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_C", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
            CLOSE_SEQUENCE_TASK(seq)
            TASK_PERFORM_SEQUENCE(pedVictim, seq)
            CLEAR_SEQUENCE_TASK(seq)
        ELSE
            IF iSelectedVariation = FOUR_GIRL_SHOPPER
                
                //Special case - add a navmesh blocking object in the fenced area.
                iBlockObject = ADD_NAVMESH_BLOCKING_OBJECT(<<39.7889, -1014.3917, 28.4847>>, <<4.000, 4.000, 4.000>>, 0, FALSE, BLOCKING_OBJECT_FLEEPATH)
                
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_backward_intro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_backward_loop_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
            ELSE
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, sFleePedBack, NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
            ENDIF  
        ENDIF
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> sFleeThiefBack")
    ELSE
        PRINTSTRING("PAUL - NOT IS_PED_FACING_PED") PRINTNL()
        TASK_PLAY_ANIM(pedAssailant, sMuggingDict, sFleeThiefFront, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
        IF thisEvent = GUY_VICTIM
            OPEN_SEQUENCE_TASK(seq)
                TASK_PLAY_ANIM(NULL, sMuggingDict, sFleePedFront, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE, 0.1)
                //TASK_LOOK_AT_COORD(NULL, <<-94.8543, -1582.6864, 30.2862>>, 3000)
                TASK_TURN_PED_TO_FACE_COORD(NULL, <<-94.8543, -1582.6864, 30.2862>>, 3000)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_B", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_C", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_A", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_B", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_C", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                TASK_PLAY_ANIM(NULL, "RANDOM@BICYCLE_THIEF@IDLE_A", "IDLE_A", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
            CLOSE_SEQUENCE_TASK(seq)
            TASK_PERFORM_SEQUENCE(pedVictim, seq)
            CLEAR_SEQUENCE_TASK(seq)
        ELSE
            IF iSelectedVariation = FOUR_GIRL_SHOPPER
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_forward_intro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_forward_loop_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
            ELSE
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, sFleePedFront, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
            ENDIF
        ENDIF
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> sFleeThiefFront")
    ENDIF
ENDPROC

PROC initialiseEventVariables()
    
    getWorldPoint()
    
    SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
    SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
    SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
    
    IF iSelectedVariation = ONE_GUY_VICTIM
        sMuggingDict = "RANDOM@MUGGING1"   
        
        sStruggleThief  = "STRUGGLE_Loop_A_Thief"
        sStrugglePed    = "STRUGGLE_Loop_A_Shopkeeper"
        sFleePedBack    = "FLEE_BACKWARD_Shopkeeper"//      FLEE_FORWARD_Shopkeeper
        sFleeThiefBack  = "FLEE_BACKWARD_Thief"     //      FLEE_FORWARD_Thief
        sFleePedFront   = "FLEE_FORWARD_Shopkeeper"
        sFleeThiefFront = "FLEE_FORWARD_Thief"

        SET_MALE_ANIMS()
        iItemValue = 200
        thisMuggingStage = MUGGING_INTERACTION
        vInitialMarkerPos = << -132.2607, -1628.3357, 31.2107 >> 
        modelVictim = A_M_Y_BUSINESS_02
        modelAssailant = G_M_Y_StrPunk_01

        vCreateVictim =   << -138.8140, -1635.9751, 31.3570 >> 
        fCreateVictim = 318.519
        
        vCreateAssailant = << -129.8504, -1629.6760, 31.2506 >>
        fCreateAssailant =    98  
        
        animScenePosition = vCreateVictim
        animSceneRotation = <<0,0,fCreateVictim>>
        
        vNEndOfAlley1 = <<-103.895126,-1593.239014,30.491978>>
        vNEndOfAlley2 = <<-89.296150,-1575.443604,32.309380>>
        vSEndOfAlley1 = <<-152.036728,-1654.379272,31.735992>>
        vSEndOfAlley2 = <<-169.818268,-1669.555542,33.941727>>
        vInAlley1 = <<-152.036728,-1654.379272,31.735992>>
        vInAlley2 = <<-103.895126,-1593.239014,30.491978>>
        
        fRespot = 340.0645
        vRespot = <<-151.0793, -1650.3220, 31.6504>>
        
        vMuggingGoToPoint = << -133.6872, -1630.2448, 31.2527 >> 
        modelBag = PROP_LD_WALLET_PICKUP //PROP_LD_CASE_01
        
        sTextBlock = "REMG1AU"
        sVicDrop = "REMG1_VIC" sShout = "REMG1_SHT" sAsk = "REMG1_ASK" sDrop = "REMG1_DRP" sPlayerReturn = "REMG1_OHY" sThx = "REMG1_THK" sGiveUp = "REMG1_GIV"
        sDemand = "REMG1_MUGA" sWho = "REMG1_WHO"

        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
            CASE CHAR_MICHAEL
                sHandover   = "REMG1_HOM"
                sWarn       = "REMG1_WAM"
            BREAK
            CASE CHAR_FRANKLIN
                sHandover   = "REMG1_HOF"
                sWarn       = "REMG1_WAF"
            BREAK
            CASE CHAR_TREVOR
                sHandover   = "REMG1_HOT"
                sWarn       = "REMG1_WAT"
            BREAK
        ENDSWITCH
        assailantAmbientVoice = "G_M_Y_StreetPunk_01_BLACK_MINI_03"
        victimAmbientVoice = "A_M_Y_Business_02_WHITE_FULL_01"
        victimVoice     = "MuggedMan" 
        assailantVoice  = "MuggerGang"
        bVariablesInitialised = TRUE
        
        sClipSetOverride = "move_m@hurry@b"
    ENDIF
            
    IF iSelectedVariation = TWO_HOLD_UP_ANIM
        sMuggingDict = "random@mugging2"
        sHandoverDict = "random@mugging2" //ortega_stand_loop_ort
        thisMuggingStage = HELDUPANIM_VARIATION
        vInitialMarkerPos = << 287.888, -284.603, 52.967 >> 
        modelVictim = A_F_Y_GENHOT_01
        modelAssailant = G_M_Y_StrPunk_01
        
        vCreateVictim =   << 288.6728, -282.4782, 52.9707 >> 
        fCreateVictim =  260.7569 
        
        vCreateAssailant = << 290.5373, -283.1980, 52.9799 >>
        fCreateAssailant = 45  
        
        fRespot = 251
        vRespot = << 297.4484, -261.2914, 53.0037 >>

        modelBag = PROP_LD_HANDBAG
        
        sTextBlock = "REMG2AU"
        sVicDrop = "REMG2_VIC" sShout = "REMG2_SHT" sAsk = "REMG2_ASK" sDrop = "REMG2_DRP" sPlayerReturn = "REMG2_OHY" sThx = "REMG2_THK" sGiveUp = "REMG2_GIV"
        sWho = "REMG2_WHO" sGun = "REMG2_GUN" sFut = "REMG2_FUT"

        SWITCH GET_CURRENT_PLAYER_PED_ENUM()
            CASE CHAR_MICHAEL
                sHandover   = "REMG2_HOM"
                sWarn       = "REMG2_WAM"
            BREAK
            CASE CHAR_FRANKLIN
                sHandover   = "REMG2_HOF"
                sWarn       = "REMG2_WAF"
            BREAK
            CASE CHAR_TREVOR
                sHandover   = "REMG2_HOT"
                sWarn       = "REMG2_WAT"
            BREAK
        ENDSWITCH
        
        assailantAmbientVoice = "G_M_Y_StreetPunk_01_BLACK_MINI_03"
        victimVoice     = "MuggedYMan" 
        assailantVoice  = "MuggerGang"
        bVariablesInitialised = TRUE
        
        sClipSetOverride = "move_m@hurry@b"
    ENDIF
    
    IF thisEvent = GIRL_SHOPPER
    
    
        SET_FEMALE_ANIMS()
        iItemValue = 2000
        thisMuggingStage = MUGGING_INTERACTION
        IF iSelectedVariation = THREE_GIRL_SHOPPER
            sMuggingDict = "RANDOM@MUGGING3"
                    
            
            vInitialMarkerPos = << -317.1964, -824.0751, 31.4284 >>
            modelVictim = A_F_Y_Hipster_01
            modelAssailant = A_M_O_Tramp_01

            vCreateVictim =   <<-310.9292, -833.8435, 30.6261>>
            fCreateVictim =   80.7161
            
            
            vCreateAssailant = << -322.5526, -827.1860, 30.5857 >>  
            fCreateAssailant =  336.5502
            
            fRespot = 81.8694
            vRespot = << -322.3941, -835.7213, 30.6001 >> 
                    
            vMuggingGoToPoint = <<-320.9667, -832.1209, 30.5979>>
            modelBag = Prop_LD_Purse_01 //prop_ld_handbag_s
            
            sTextBlock = "REMG3AU"
            sVicDrop = "REMG3_VIC" sShout = "REMG3_SHT" sAsk = "REMG3_ASK" sDrop = "REMG3_DRP" sPlayerReturn = "REMG3_OHY" sThx = "REMG3_THK" sGiveUp = "REMG3_GIV"
            sDemand = "REMG3_MUGB" sWho = "REMG3_WHO" sGun = "REMG3_GUN" sFut = "REMG3_FUT"

            SWITCH GET_CURRENT_PLAYER_PED_ENUM()
                CASE CHAR_MICHAEL
                    sHandover   = "REMG3_HOM"
                    sWarn       = "REMG3_WAM"
                BREAK
                CASE CHAR_FRANKLIN
                    sHandover   = "REMG3_HOF"
                    sWarn       = "REMG3_WAF"
                BREAK
                CASE CHAR_TREVOR
                    sHandover   = "REMG3_HOT"
                    sWarn       = "REMG3_WAT"
                BREAK
            ENDSWITCH
            
            assailantAmbientVoice = "G_M_Y_Korean_02_Korean_MINI_02"
            victimVoice     = "MUGGEDHIPSTER" 
            assailantVoice  = "MuggerTramp"
        
            bVariablesInitialised = TRUE
            
            sClipSetOverride = "move_f@hurry@a" 
        ENDIF
        
        IF iSelectedVariation = FOUR_GIRL_SHOPPER
            SET_FEMALE_ANIMS()
            
            sMuggingDict = "RANDOM@MUGGING4"   
        
            sStruggleThief  = "STRUGGLE_Loop_B_Thief"
            sStrugglePed    = "STRUGGLE_Loop_B_Shopkeeper"
            sFleePedBack    = "FLEE_BACKWARD_Shopkeeper"//      FLEE_FORWARD_Shopkeeper
            sFleeThiefBack  = "FLEE_BACKWARD_Thief"     //      FLEE_FORWARD_Thief
            sFleePedFront   = "FLEE_FORWARD_Shopkeeper"
            sFleeThiefFront = "FLEE_FORWARD_Thief"
            
            vInitialMarkerPos = << 32.8802, -1016.0609, 28.4527 >> 
            modelVictim = A_F_M_Tourist_01
            modelAssailant = G_M_Y_ArmGoon_02

            vCreateVictim =  <<32.2169, -1020.8641, 28.4560>>
            fCreateVictim =   159.1445
            
            animScenePosition = vCreateVictim
            animSceneRotation = <<0,0,fCreateVictim>>
            
            vCreateAssailant = << 38.1933, -1023.5788, 28.4889 >>
            fCreateAssailant =  60.1372 
            
            fRespot = 248
            vRespot = << 42.5323, -990.3530, 28.2480 >> 
            
            vNEndOfAlley1 = <<38.151863,-1001.485352,28.422756>>
            vNEndOfAlley2 =  <<40.136692,-995.271118,30.371975>>
            vSEndOfAlley1 = <<37.217583,-1040.353638,28.415060>>
            vSEndOfAlley2 = <<19.914917,-1036.813965,30.280445>>
            vInAlley1 = << 28.4511, -1035.7179, 28.3329 >>
            vInAlley2 = << 38.6536, -1001.0728, 28.4070 >>
            
            vMuggingGoToPoint = << 32.5360, -1020.2932, 28.4576 >>
            modelBag = PROP_LD_WALLET_PICKUP //prop_ld_handbag_s
            
            sTextBlock = "REMG4AU"
            sVicDrop = "REMG4_VIC" sShout = "REMG4_SHT" sAsk = "REMG4_ASK" sDrop = "REMG4_DRP" sPlayerReturn = "REMG4_OHY" sThx = "REMG4_THK" sGiveUp = "REMG4_GIV"
            sDemand = "REMG4_MUGB" sWho = "REMG4_WHO" sGun = "REMG4_GUN" sFut = "REMG4_FUT"

            SWITCH GET_CURRENT_PLAYER_PED_ENUM()
                CASE CHAR_MICHAEL
                    sHandover   = "REMG4_HOM"
                    sWarn       = "REMG4_WAM"
                BREAK
                CASE CHAR_FRANKLIN
                    sHandover   = "REMG4_HOF"
                    sWarn       = "REMG4_WAF"
                BREAK
                CASE CHAR_TREVOR
                    sHandover   = "REMG4_HOT"
                    sWarn       = "REMG4_WAT"
                BREAK
            ENDSWITCH
            assailantAmbientVoice = "G_M_Y_ArmGoon_02_White_Armenian_MINI_02"
            
            victimVoice     = "MuggedWoman" 
            assailantVoice  = "MuggerGang"
            
            bVariablesInitialised = TRUE
            
            sClipSetOverride = "move_f@hurry@a" 
        ENDIF
        
    ENDIF
                    
ENDPROC

PROC changeBlips()
    IF DOES_BLIP_EXIST(blipLocation)
        REMOVE_BLIP(blipLocation)
    ENDIF   
//  IF NOT DOES_BLIP_EXIST(blipVictim)
//      IF NOT IS_ENTITY_DEAD(pedVictim)
//          blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
//      ENDIF
//  ENDIF
    IF NOT DOES_BLIP_EXIST(blipMugger)
        IF NOT IS_ENTITY_DEAD(pedAssailant)
            blipMugger = CREATE_AMBIENT_BLIP_FOR_PED(pedAssailant, TRUE)
        ENDIF
    ENDIF
ENDPROC


PROC createScene()
    pedVictim = CREATE_PED(PEDTYPE_MISSION, modelVictim,  vCreateVictim,  fCreateVictim)
    SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, TRUE)
    SET_PED_CAN_BE_TARGETTED(pedVictim, FALSE)
    SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_PREFER_PAVEMENTS, TRUE)
    SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_DISABLE_HANDS_UP, TRUE)
    SET_PED_FLEE_ATTRIBUTES(pedVictim, FA_NEVER_FLEE, TRUE)
    STOP_PED_SPEAKING(pedVictim, TRUE) //Stop non script related speech. 
    SET_PED_INCREASED_AVOIDANCE_RADIUS(pedVictim)

    SET_PED_MONEY(pedVictim, 0)
    
    SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedVictim, TRUE)
    IF modelVictim = A_M_Y_GENSTREET_01
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 1, 0, 0) //(head)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TORSO, 0, 3, 0) //(uppr)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_LEG, 0, 2, 0) //(lowr)
    ENDIF
    
    IF modelVictim = A_M_Y_BUSINESS_02
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HEAD, 1, 0, 0) //(head)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_HAIR, 1, 0, 0) //(hair)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_TORSO, 1, 0, 0) //(uppr)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_LEG, 1, 0, 0) //(lowr)
        SET_PED_COMPONENT_VARIATION(pedVictim, PED_COMP_SPECIAL, 0, 0, 0) //(accs)
    ENDIF
    
    ADD_RELATIONSHIP_GROUP("theAssailant", relGroup_assailant)
    
    pedAssailant = CREATE_PED(PEDTYPE_MISSION, modelAssailant,  vCreateAssailant,  fCreateAssailant)
    SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, TRUE)
    SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedAssailant, TRUE)
    SET_ENTITY_IS_TARGET_PRIORITY(pedAssailant, TRUE)
    GIVE_WEAPON_TO_PED(pedAssailant, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
    SET_PED_COMBAT_ATTRIBUTES(pedAssailant, CA_AGGRESSIVE, FALSE)
    SET_PED_COMBAT_ATTRIBUTES(pedAssailant, CA_ALWAYS_FLEE, FALSE)
    SET_PED_DIES_WHEN_INJURED(pedAssailant, TRUE)
    SET_PED_CONFIG_FLAG(pedAssailant, PCF_DontInfluenceWantedLevel, TRUE)
    SET_PED_CONFIG_FLAG(pedAssailant, PCF_RemoveDeadExtraFarAway, TRUE)
    SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_DISABLE_HANDS_UP, TRUE)
    SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_USE_VEHICLE, FALSE)
    SET_PED_RELATIONSHIP_GROUP_HASH(pedAssailant, relGroup_assailant)
    //SET_PED_PATH_CAN_USE_LADDERS(pedAssailant, FALSE)
    STOP_PED_SPEAKING(pedAssailant, TRUE) //Stop non script related speech. 
    
    IF NOT IS_STRING_NULL(victimAmbientVoice)
        SET_AMBIENT_VOICE_NAME(pedVictim, victimAmbientVoice)
    ENDIF
    
    SET_AMBIENT_VOICE_NAME(pedAssailant, assailantAmbientVoice)
    
    
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_CIVMALE, relGroup_assailant)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_CIVFEMALE, relGroup_assailant)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroup_assailant,  RELGROUPHASH_CIVMALE)
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, relGroup_assailant,   RELGROUPHASH_CIVFEMALE) 
    SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroup_assailant, RELGROUPHASH_PLAYER)
    
    
    SWITCH GET_CURRENT_PLAYER_PED_ENUM()
        CASE CHAR_MICHAEL
            ADD_PED_FOR_DIALOGUE(MuggingConversation, 0, PLAYER_PED_ID(), "MICHAEL")
        BREAK
        CASE CHAR_FRANKLIN
            ADD_PED_FOR_DIALOGUE(MuggingConversation, 0, PLAYER_PED_ID(), "FRANKLIN")
        BREAK
        CASE CHAR_TREVOR
            ADD_PED_FOR_DIALOGUE(MuggingConversation, 0, PLAYER_PED_ID(), "TREVOR")
        BREAK
    ENDSWITCH
    ADD_PED_FOR_DIALOGUE(MuggingConversation, 1, pedAssailant, assailantVoice)
    ADD_PED_FOR_DIALOGUE(MuggingConversation, 2, pedVictim, victimVoice)
    
    IF thisEvent = GUY_VICTIM
        //SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_USE_VEHICLE, TRUE)
        SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_USE_COVER, FALSE)
    ENDIF
    
    IF iSelectedVariation = ONE_GUY_VICTIM
    OR iSelectedVariation = FOUR_GIRL_SHOPPER
        INITIALISE_MUGGING_SCENE()
//      objIdStolenItem = CREATE_OBJECT(modelBag, vCreateVictim)
//      ATTACH_ENTITY_TO_ENTITY(objIdStolenItem, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_L_HAND), << 0.200, -0.000, 0.000 >>, << -101.160, -176.760, 96.480 >>, TRUE)
    ENDIF
    
    IF iSelectedVariation = THREE_GIRL_SHOPPER
        scenePosition = <<-325.4189, -828.8596, 31.1>>
        sceneRotation = << 0.000, 0.000, 180.000 >>
        sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
        TASK_SYNCHRONIZED_SCENE(pedAssailant, sceneId, "AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@BASE", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT )
    ENDIF
    
    IF iSelectedVariation = FOUR_GIRL_SHOPPER
        DISABLE_NAVMESH_IN_AREA(<<37.1197, -1027.5654, 28.5315>>-<<1,1,1>>, <<37.1197, -1027.5654, 28.5315>>+<<1,1,1>>, TRUE)
    ENDIF
    
    PRINTSTRING("Paul - CREATE_AMBIENT_BLIP_INITIAL_COORD") PRINTNL()
ENDPROC

FUNC BOOL LOAD_AND_CREATE_ENTITIES()
    initialiseEventVariables()
//    sLoadQueue.iLastFrame = GET_FRAME_COUNT() //Wait till next frame before making more load requests with the queue.
    
    //These anim dictionaires have strings too long to fit in the load queue struct.
   
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue,"AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@BASE")
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue,"AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@EXIT")
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue,sReturnDict)
	LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue,sHandoverDict)
	
    LOAD_QUEUE_LARGE_ADD_MODEL(sLoadQueue, modelVictim)
    LOAD_QUEUE_LARGE_ADD_MODEL(sLoadQueue, modelAssailant)
    LOAD_QUEUE_LARGE_ADD_MODEL(sLoadQueue, modelBag)
    LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, sPickupDict)
    LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, sMuggingDict)
    LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, sClipSetOverride)
    LOAD_QUEUE_LARGE_ADD_ANIM_DICT(sLoadQueue, "RANDOM@BICYCLE_THIEF@IDLE_A")
    
//    IF HAS_ANIM_DICT_LOADED("AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@BASE")
//    AND HAS_ANIM_DICT_LOADED("AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@EXIT")
//    AND HAS_ANIM_DICT_LOADED(sReturnDict)
//    AND HAS_ANIM_DICT_LOADED(sHandoverDict)
    IF HAS_LOAD_QUEUE_LARGE_LOADED(sLoadQueue)
        createScene()
        RETURN TRUE
    ENDIF   
    RETURN FALSE
ENDFUNC



PROC blipForDroppedBag()
    IF DOES_BLIP_EXIST(blipMugger)
        REMOVE_BLIP(blipMugger)
    ENDIF
    IF DOES_BLIP_EXIST(blipVictim)
        REMOVE_BLIP(blipVictim)
    ENDIF
    IF NOT DOES_BLIP_EXIST(blipStolenItem)
        IF DOES_ENTITY_EXIST(objIdStolenItem)
            blipStolenItem = CREATE_AMBIENT_BLIP_FOR_OBJECT(objIdStolenItem)
        ENDIF
    ENDIF
ENDPROC

PROC pedChecks()
    IF DOES_ENTITY_EXIST(pedVictim)
        IF IS_PED_INJURED(pedVictim)
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF IS_PED_INJURED(pedAssailant)
                    CPRINTLN(DEBUG_AMBIENT, "re_muggings - missionCleanup() as pedVictim and pedAssailant are both injured.\n")
                    missionCleanup()
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    IF IS_PED_INJURED(pedVictim)
        IF DOES_BLIP_EXIST(blipVictim)
            REMOVE_BLIP(blipVictim)
        ENDIF
    ENDIF
    IF IS_PED_INJURED(pedAssailant)
        IF DOES_BLIP_EXIST(blipMugger)
            REMOVE_BLIP(blipMugger)
        ENDIF
    ENDIF
    
    IF NOT bRemovedMuggerFromDialogue
        IF IS_PED_INJURED(pedAssailant)
            REMOVE_PED_FOR_DIALOGUE(MuggingConversation, 1)
            bRemovedMuggerFromDialogue = TRUE
        ENDIF
    ENDIF
    IF NOT bRemovedVictimFromDialogue
        IF IS_PED_INJURED(pedVictim)
            REMOVE_PED_FOR_DIALOGUE(MuggingConversation, 2)
            bRemovedVictimFromDialogue = TRUE
        ENDIF
    ENDIF
ENDPROC

PROC SNAP_OUT_OF_LYING_DOWN_ANIM(BOOL bPlayAgitatedLoop = TRUE)
    //If the player has managed to get to this point without approching the vic then bring her out of the lying down anim - variation 4
    IF NOT IS_PED_INJURED(pedVictim)
    AND iSelectedVariation = FOUR_GIRL_SHOPPER
        IF IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_forward_loop_shopkeeper")
            OPEN_SEQUENCE_TASK(seq)
                TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_forward_outro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
                IF bPlayAgitatedLoop
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
                ENDIF
            CLOSE_SEQUENCE_TASK(seq)
            CLEAR_PED_TASKS(pedVictim)
            TASK_PERFORM_SEQUENCE(pedVictim, seq)
            CLEAR_SEQUENCE_TASK(seq)
        ENDIF
    ENDIF
ENDPROC

PROC blipForVictim()
    blipStolenItem = NULL
//  IF DOES_BLIP_EXIST(blipStolenItem)
//      REMOVE_BLIP(blipStolenItem)
//  ENDIF
    IF NOT DOES_BLIP_EXIST(blipVictim)
        IF NOT IS_ENTITY_DEAD(pedVictim)
            blipVictim = CREATE_AMBIENT_BLIP_FOR_PED(pedVictim)
        ENDIF
    ENDIF
ENDPROC

PROC continualTurnToFacePlayer(PED_INDEX targetPed, INT& stage) //, INT &turnTimer, INT timeGapTurn)
    FLOAT   fFacingAngle = 65   

    SWITCH stage
        CASE 0 // TURN TASK
            IF NOT IS_PED_INJURED(targetPed)
            //  CLEAR_PED_TASKS(targetPed)
                OPEN_SEQUENCE_TASK(Seq)
                    TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
                    TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
                CLOSE_SEQUENCE_TASK(Seq)
                TASK_PERFORM_SEQUENCE(targetPed, Seq)
                CLEAR_SEQUENCE_TASK(Seq)
                SETTIMERA(0)
                stage ++
            ENDIF
        BREAK
        
        CASE 1 // TURNING 
            IF NOT IS_PED_INJURED(targetPed)
                IF IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
                    // ACHIEVED TASK
                    stage ++
                ENDIF
            ENDIF   
        BREAK
        
        CASE 2 // WAITING TO TURN
            IF NOT IS_PED_INJURED(targetPed)
                IF NOT IS_PED_FACING_PED(targetPed, PLAYER_PED_ID(), fFacingAngle)
                    stage = 0
                ENDIF
            ENDIF
        BREAK
        
    ENDSWITCH

ENDPROC
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------OLD LADY VARIATION----------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_PLAYER_SHOOTING_NEAR_PEDS_IN_MUGGING_ANIM()
    IF NOT IS_ENTITY_DEAD(pedVictim)
        IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 5)
            IF IS_PED_SHOOTING(PLAYER_PED_ID())
                RETURN TRUE
            ENDIF   
        ENDIF   
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_MUGGER()
    IF NOT IS_PED_INJURED(pedAssailant)
        IF CAN_PED_SEE_HATED_PED(pedAssailant, PLAYER_PED_ID())
        AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<20,20,20>>)
            IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
                IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedAssailant)
                OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedAssailant)
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
        
        IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
            RETURN TRUE
        ENDIF
        IF  IS_PED_RAGDOLL(pedAssailant)
            RETURN TRUE
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_SHOOTING_AT_PEDS_IN_MUGGING_ANIM()
    IF NOT IS_ENTITY_DEAD(pedVictim)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20,20,20>>)
            IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
            OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
                IF IS_PED_SHOOTING(PLAYER_PED_ID())
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
    ELSE
        RETURN TRUE
    ENDIF
    IF NOT IS_ENTITY_DEAD(pedAssailant)
        IF IS_PLAYER_AIMING_AT_MUGGER()
            IF IS_PED_SHOOTING(PLAYER_PED_ID())
                RETURN TRUE
            ENDIF   
        ENDIF
    ELSE
        RETURN TRUE
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_VICTIM_IN_MUGGING_ANIM()
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<5.3,  5.3,  6>>, FALSE)
            IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedVictim, PLAYER_PED_ID())
                IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
                AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())
                    IF NOT IS_PED_INJURED(pedAssailant)
                        IF NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedAssailant)
                            IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
                            OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
                                SETTIMERB(0)
                                RETURN TRUE
                            ENDIF
                        ENDIF
                    ELSE
                        IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
                        OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
                            SETTIMERB(0)
                            RETURN TRUE
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_BUMPED_INTO_PEDS_IN_MUGGING_ANIM()
    IF NOT IS_PED_INJURED(pedVictim)
    AND NOT IS_PED_INJURED(pedAssailant)
        IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedAssailant)
        OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedVictim)
            RETURN TRUE
        ENDIF
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<2,2,2>>)
            //RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

PROC VICTIM_FLEES()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> - VICTIM_FLEES")
    IF NOT IS_PED_INJURED(pedVictim)
        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 150, -1)
        SET_PED_KEEP_TASK(pedVictim, TRUE)
        WAIT(0)
    ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_SHOOTING_AT_VICTIM()
    VECTOR vTempMax = <<3,3,3>>, vTempMin = <<-3,-3,-3>>
    IF IS_PED_INJURED(pedVictim)
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> IS_PLAYER_SHOOTING_AT_VICTIM() - IF IS_PED_INJURED()\n")
        RETURN TRUE
    ELSE
        IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> IS_PLAYER_SHOOTING_AT_VICTIM() - IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY()\n")
            RETURN TRUE
        ENDIF
        
        IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 5)
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> IS_PLAYER_SHOOTING_AT_VICTIM() - IF IS_BULLET_IN_AREA\n")
            RETURN TRUE
        ENDIF
    
        vTempMax = vTempMax+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
        vTempMin = vTempMin+GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>)
        IF IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_SMOKEGRENADE, TRUE)
        OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_GRENADE, TRUE)
        OR IS_PROJECTILE_TYPE_IN_AREA(vTempMin, vTempMax, WEAPONTYPE_STICKYBOMB, TRUE)
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> IS_PLAYER_SHOOTING_AT_VICTIM() - IF IS_PROJECTILE_TYPE_IN_AREA\n")
            RETURN TRUE
        ENDIF
        
        IF GET_IS_PETROL_DECAL_IN_RANGE(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 3)
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> IS_PLAYER_SHOOTING_AT_VICTIM() - IF GET_IS_PETROL_DECAL_IN_RANGE\n")
            RETURN TRUE
        ENDIF
        
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC thankPlayer()
    IF NOT IS_PED_INJURED(pedVictim)
        IF NOT IS_PED_RAGDOLL(pedVictim)
            IF IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
                IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                OR GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_COWER) = FINISHED_TASK
                    
                    IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                        KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
                    ENDIF
                    
                    IF NOT IS_PED_INJURED(pedAssailant)
                        IF NOT IS_PED_FLEEING(pedVictim) // If he's fleeing already, don't bother retasking
                            //SCRIPT_ASSERT("1") 
                            TASK_SMART_FLEE_PED(pedVictim, pedAssailant, 150, -1)
                        ENDIF
                    ELSE
                        IF thisEvent = HOLD_UP_ANIM
                            IF NOT IS_PED_FLEEING(pedVictim) // If he's fleeing already, don't bother retasking
                                //SCRIPT_ASSERT("2") 
                                TASK_SMART_FLEE_COORD(pedVictim, vInput, 150, -1)
                            ENDIF
                        ENDIF
                    ENDIF
                    SET_PED_KEEP_TASK(pedVictim, TRUE)
                    WAIT(0)
                    CPRINTLN(DEBUG_AMBIENT, "re_muggings thankPlayer() - Called thank you conversation")
                    CREATE_CONVERSATION(MuggingConversation, sTextBlock, sThx, CONV_PRIORITY_AMBIENT_HIGH)
                    CPRINTLN(DEBUG_AMBIENT, "re_muggings thankPlayer() - missionCleanup() has been called.\n")
                    WAIT(0)
                    IF iItemValue > 0
                        SET_PED_MONEY(PLAYER_PED_ID(), ((iItemValue/100)*90))
                        DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, (iItemValue))
                        WAIT(0)
                        CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, (iItemValue/10),FALSE,TRUE)
                    ENDIF
                    SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
                    missionPassed()
                ENDIF
            ELSE
                CPRINTLN(DEBUG_AMBIENT, "re_muggings thankPlayer() - missionCleanup() as random event not active\n")
                missionCleanup()
            ENDIF
        ELSE
            CPRINTLN(DEBUG_AMBIENT, "re_muggings thankPlayer() - missionCleanup() as pedVictim is RAGDOLL.\n")
            missionCleanup()
        ENDIF
    ELSE
        CPRINTLN(DEBUG_AMBIENT, "re_muggings thankPlayer() - missionCleanup() as pedVictim is injured.\n")
        missionCleanup()
    ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_HIT_BLIP_TRIGGER()
    BOOL bReturn = FALSE
    IF iSelectedVariation = ONE_GUY_VICTIM
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-179.103012,-1682.421143,53.693001>>, <<-83.532082,-1567.583618,25.187452>>, 40.750000, FALSE, FALSE)
            bReturn = TRUE
        ENDIF
    ELIF iSelectedVariation = TWO_HOLD_UP_ANIM
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<353.150635,-296.196167,62.765713>>, <<210.464584,-243.510620,46.137310>>, 123.750000)
            bReturn = TRUE
        ENDIF
    ELIF iSelectedVariation = THREE_GIRL_SHOPPER
        IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-325.7037, -829.3100, 30.5812>>) < 105.0)
            bReturn = TRUE
        ENDIF
    ENDIF
    RETURN bReturn
ENDFUNC

FUNC BOOL HAS_PLAYER_HIT_CUSTOM_TRIGGER()
    BOOL bReturn = FALSE
    IF iSelectedVariation = ONE_GUY_VICTIM
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-170.305145,-1670.361938,53.730835>>, <<-91.940430,-1578.828125,24.962210>>, 18.000000, FALSE, FALSE)
            bReturn = TRUE
        ENDIF
    ELIF iSelectedVariation = TWO_HOLD_UP_ANIM
        IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedAssailant)) < 30.0)
            bReturn = TRUE
        ENDIF
    ELIF iSelectedVariation = FOUR_GIRL_SHOPPER
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-41.326782,-959.644348,38.444405>>, <<119.920845,-1021.230835,21.857378>>, 123.750000)
            bReturn = TRUE
        ENDIF
    ELSE
        IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-325.7037, -829.3100, 30.5812>>) < 95.0)
            bReturn = TRUE
        ENDIF
    ENDIF
    RETURN bReturn
ENDFUNC

PROC runHoldUpAnimVariation()
    //PRINTSTRING("\nre_muggings iHoldUpStages = ")PRINTINT(iHoldUpStages)PRINTNL()
    SWITCH iHoldUpStages
        CASE 0 // set scene
            IF NOT IS_PED_INJURED(pedAssailant)
                TASK_PLAY_ANIM(pedAssailant, sMuggingDict, "IG_1_guy_stickup_loop", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
            ENDIF   
            IF NOT IS_PED_INJURED(pedVictim)
                TASK_PLAY_ANIM(pedVictim, sHandoverDict, "ortega_stand_loop_ort", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
            ENDIF   
            
            iHoldUpStages ++
        BREAK
        
        
        CASE 1 // player getting close
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF NOT IS_PED_INJURED(pedAssailant)
                    IF HAS_PLAYER_HIT_BLIP_TRIGGER()
                        IF NOT DOES_BLIP_EXIST(blipLocation)
                            IF NOT IS_ENTITY_DEAD(pedAssailant)
                                blipLocation = CREATE_AMBIENT_BLIP_FOR_PED(pedAssailant, TRUE)
                            ENDIF
                            IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
                                PRINTSTRING("Paul - SET_RANDOM_EVENT_ACTIVE") PRINTNL()
                                SET_RANDOM_EVENT_ACTIVE()
                            ENDIF
                        ENDIF
                    ENDIF
                
                    IF HAS_PLAYER_HIT_CUSTOM_TRIGGER()
                        //IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
                            //SET_RANDOM_EVENT_ACTIVE()
//                      ENDIF
//                  ENDIF
//                  IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<50, 50, 50>>)
//                      IF NOT IS_ENTITY_OCCLUDED(pedAssailant)
//                      OR NOT IS_ENTITY_OCCLUDED(pedVictim)
//                      OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<15, 15, 15>>)
                            CREATE_CONVERSATION(MuggingConversation, sTextBlock, "REMG2_ARG", CONV_PRIORITY_AMBIENT_HIGH)
                            changeBlips()
//                          IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
//                              SET_RANDOM_EVENT_ACTIVE()
//                          ENDIF
                            iTimerStartMuggingAnim = GET_GAME_TIMER()
                            SET_WANTED_LEVEL_MULTIPLIER(0)
                            bSetTimer = TRUE
                            iHoldUpStages ++
                        //ENDIF
                    ENDIF
                ELSE
                    IF NOT IS_PED_INJURED(pedVictim)
                        TASK_SMART_FLEE_COORD(pedVictim, vInitialMarkerPos, 250, -1)
                        SET_PED_KEEP_TASK(pedVictim, TRUE)
                        wait(0)
                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> pedAssailant dead before robbery.\n")
                        PRINTSTRING("PAUl - THANK PLAYER1") PRINTNL()
                        thankPlayer()
                    ENDIF
                ENDIF
            ENDIF
            
            IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                IF IS_PLAYER_SHOOTING_NEAR_PEDS_IN_MUGGING_ANIM()
                    
                    iHoldUpStages = 4
                ENDIF
                IF IS_PLAYER_AIMING_AT_MUGGER()
                    
                    iHoldUpStages = 9
                ENDIF
                IF IS_PLAYER_AIMING_AT_VICTIM_IN_MUGGING_ANIM()
                    
                    iHoldUpStages = 6
                ENDIF
                IF IS_PLAYER_SHOOTING_AT_PEDS_IN_MUGGING_ANIM()
                    iHoldUpStages = 4
                ENDIF
            ENDIF
            IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
                IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
                    missionCleanup()
                ENDIF
            ENDIF
        BREAK
        
        CASE 2 // player told to get away
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF NOT IS_PED_INJURED(pedAssailant)
                    IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<6, 6, 15>>)
                        IF CAN_PED_SEE_HATED_PED(pedAssailant, PLAYER_PED_ID()) OR IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<3, 3, 15>>) 
                            IF IS_PED_FACING_PED(PLAYER_PED_ID(), pedAssailant, 65)
                                iTimerStartAskLeaveAnim = GET_GAME_TIMER()
                                //"Fuck off!"
                                TASK_LOOK_AT_ENTITY(pedAssailant, PLAYER_PED_ID(), 7000)
                                CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWho, CONV_PRIORITY_AMBIENT_HIGH)
                                SETTIMERB(0)
                                iHoldUpStages ++
                            ELSE
                                iTimerStartAskLeaveAnim = GET_GAME_TIMER()
    //                          OPEN_SEQUENCE_TASK(seq)
    //                              TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
    //                              TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 7000)
    //                          CLOSE_SEQUENCE_TASK(seq)
    //                          TASK_PERFORM_SEQUENCE(pedAssailant, seq)
    //                          CLEAR_SEQUENCE_TASK(seq)
                                TASK_LOOK_AT_ENTITY(pedAssailant, PLAYER_PED_ID(), 7000)
                                CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWho, CONV_PRIORITY_AMBIENT_HIGH)
                                SETTIMERB(0)
                                iHoldUpStages ++
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF   
            IF IS_PLAYER_SHOOTING_NEAR_PEDS_IN_MUGGING_ANIM()
            OR IS_PLAYER_SHOOTING_AT_PEDS_IN_MUGGING_ANIM()
                iHoldUpStages = 4
            ENDIF
            IF IS_PLAYER_AIMING_AT_MUGGER()
                iHoldUpStages = 9
            ENDIF
            IF IS_PLAYER_AIMING_AT_VICTIM_IN_MUGGING_ANIM()
                iHoldUpStages = 6
            ENDIF
        BREAK
        
        CASE 3 
            IF (iTimerCurrentMuggingAnim - iTimerStartAskLeaveAnim) > 12500
                IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
                    iHoldUpStages = 4
                ENDIF
            ENDIF   
            // if player is still close victim flees and mugger fights
            IF (iTimerCurrentMuggingAnim - iTimerStartAskLeaveAnim) > 5500
                IF NOT bFightingWithPlayer
                    IF DOES_ENTITY_EXIST(pedAssailant)
                        IF NOT IS_PED_INJURED(pedAssailant)
                            CREATE_CONVERSATION(MuggingConversation, sTextBlock, sFut, CONV_PRIORITY_AMBIENT_HIGH)
                            TASK_COMBAT_PED(pedAssailant, PLAYER_PED_ID())
                            bFightingWithPlayer = TRUE
                            IF DOES_ENTITY_EXIST(pedVictim)
                                IF NOT IS_PED_INJURED(pedVictim)
                                    //CLEAR_PED_TASKS(pedVictim)
                                    TASK_SMART_FLEE_COORD(pedVictim, vInitialMarkerPos, 250, -1)
                                    SET_PED_KEEP_TASK(pedVictim, TRUE)
                                    WAIT(0)
                                ENDIF
                                IF NOT IS_PED_INJURED(pedVictim)
                                    //SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                                    //CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 4")
                                ENDIF
                                IF DOES_BLIP_EXIST(blipVictim)
                                    REMOVE_BLIP(blipVictim)
                                ENDIF
                            ENDIF   
                        ENDIF   
                    ENDIF
                ENDIF
            ENDIF
            IF bFightingWithPlayer
                PRINTSTRING("PAUL - bFightingWithPlayer") PRINTNL()
                iHoldUpStages = 10
            ELSE
                IF IS_PLAYER_SHOOTING_NEAR_PEDS_IN_MUGGING_ANIM()
                    PRINTSTRING("Paul case to 4 1") PRINTNL()
                    iHoldUpStages = 4
                ENDIF
                IF IS_PLAYER_AIMING_AT_MUGGER()
                    iHoldUpStages = 9
                ENDIF
                IF IS_PLAYER_AIMING_AT_VICTIM_IN_MUGGING_ANIM()
                    iHoldUpStages = 6
                ENDIF
                IF HAS_PLAYER_BUMPED_INTO_PEDS_IN_MUGGING_ANIM()
                    iHoldUpStages = 9
                ENDIF
                IF IS_PLAYER_SHOOTING_AT_PEDS_IN_MUGGING_ANIM()
                    PRINTSTRING("Paul case to 4 2") PRINTNL()
                    iHoldUpStages = 4
                ENDIF
            ENDIF
        BREAK
        
        CASE 10
            IF DOES_ENTITY_EXIST(pedAssailant)
                PRINTSTRING("PAUl - DOES_ENTITY_EXIST") PRINTNL()
                IF IS_PED_INJURED(pedAssailant) OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAssailant) > 100
                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," runHoldUpAnimVariation()- missionCleanup() as pedAssailant is injured.\n")
                    KILL_ANY_CONVERSATION()
                    PRINTSTRING("PAUl - THANK PLAYER2") PRINTNL()
                    thankPlayer()
                ENDIF
            ENDIF
        BREAK
        
        CASE 4 // player has shot near peds
            // if the player is shooting near the peds break the anim and flee
            PRINTSTRING("Paul case 4") PRINTNL()
            IF DOES_ENTITY_EXIST(pedVictim)
                IF NOT IS_PED_INJURED(pedVictim)
                    IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
                        CLEAR_PED_TASKS(pedVictim)
                            
                            
                        IF NOT IS_PED_INJURED(pedAssailant)
                            PRINTSTRING("PAUL - blending into idle!") PRINTNL()
                            OPEN_SEQUENCE_TASK(seq)
                                //TASK_PLAY_ANIM(NULL,"MOVE_M@GENERIC", "idle", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_DEFAULT)
                                TASK_SMART_FLEE_COORD(NULL, GET_ENTITY_COORDS(pedAssailant, FALSE), 150, -1)
                            CLOSE_SEQUENCE_TASK(seq)
                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                        ENDIF
                        bSavedVictim = TRUE
                    ELSE
                        CLEAR_PED_TASKS(pedVictim)
                        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                        SET_PED_KEEP_TASK(pedVictim, TRUE)
                        IF DOES_BLIP_EXIST(blipVictim)
                            REMOVE_BLIP(blipVictim)
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF NOT IS_PED_INJURED(pedAssailant)
                    CLEAR_PED_TASKS(pedAssailant)
                    TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 250, -1)
                    SET_PED_KEEP_TASK(pedAssailant, TRUE)
                    IF DOES_BLIP_EXIST(blipMugger)
                        REMOVE_BLIP(blipMugger)
                    ENDIF
                ENDIF
            ENDIF

            IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
            ENDIF

            WAIT(0)
            IF NOT bSavedVictim
                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," runHoldUpAnimVariation() - missionCleanup() CASE 4\n")
                IF NOT IS_PED_INJURED(pedAssailant)
                    CREATE_CONVERSATION(MuggingConversation, sTextBlock, "REMG2_FKIT", CONV_PRIORITY_AMBIENT_HIGH)
                ENDIF
                missionCleanup()
            ENDIF
        BREAK
        
        CASE 5 //player is aiming at mugger
        PRINTSTRING("Paul case 5") PRINTNL()
            IF TIMERB() > 250   
                IF DOES_ENTITY_EXIST(pedAssailant)
                    IF NOT IS_PED_INJURED(pedAssailant)
                        SET_PED_COMBAT_ATTRIBUTES(pedAssailant, CA_ALWAYS_FLEE, TRUE)
                        SET_PED_RESET_FLAG(pedAssailant, PRF_InstantBlendToAim, TRUE)
                        OPEN_SEQUENCE_TASK(seq)
                            //TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 2000)
                            //TASK_HANDS_UP(NULL, 2000)
                            TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
                        CLOSE_SEQUENCE_TASK(seq)    
                        TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                        CLEAR_SEQUENCE_TASK(seq)
                        SET_PED_KEEP_TASK(pedAssailant, TRUE)
                        WAIT(0)
                        SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, FALSE)
                        SET_PED_AS_NO_LONGER_NEEDED(pedAssailant)
                        
                        // "Alright, alright, no need to pull a fucking gun on me!"
                        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sGun, CONV_PRIORITY_AMBIENT_HIGH)
                    ENDIF
                ENDIF
                IF DOES_ENTITY_EXIST(pedVictim)
                    IF NOT IS_PED_INJURED(pedVictim)
                        IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
                            CLEAR_PED_TASKS(pedVictim)
                            //TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, (GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 5, 0>>)), PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 1.0)
                            bSavedVictim = TRUE
                        ENDIF
                    ENDIF
                ENDIF
                IF NOT bSavedVictim
                    WAIT(0)
                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " runHoldUpAnimVariation() - missionCleanup() CASE 5")
                    missionCleanup()
                ENDIF
                    
            ENDIF
        BREAK
        
        CASE 6 //player is aiming at victim
        PRINTSTRING("Paul case 6") PRINTNL()
            IF TIMERA() > 500
                IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                    IF DOES_ENTITY_EXIST(pedVictim)
                        IF NOT IS_PED_INJURED(pedVictim)
                            CLEAR_PED_TASKS(pedVictim)
                            OPEN_SEQUENCE_TASK(seq)
                                TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000)
                                //TASK_HANDS_UP(NULL, 2500)
                                TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
                            CLOSE_SEQUENCE_TASK(seq)    
                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                            SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 4")
                        ENDIF
                    ENDIF
                    //Temporary debug speech
                    //PRINT_STRING_WITH_LITERAL_STRING_NOW("STRING", "Fuck!", 4000, 1)
                    //------------------------------------
                    IF DOES_ENTITY_EXIST(pedAssailant)
                        IF NOT IS_PED_INJURED(pedAssailant)
                            CREATE_CONVERSATION(MuggingConversation, sTextBlock, sFut, CONV_PRIORITY_AMBIENT_HIGH)
                            CLEAR_PED_TASKS(pedAssailant)
                            OPEN_SEQUENCE_TASK(seq)
                                TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 10000)
                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
                                TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
                            CLOSE_SEQUENCE_TASK(seq)    
                            TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            SET_PED_KEEP_TASK(pedassailant, TRUE)
                            //SET_PED_AS_NO_LONGER_NEEDED(pedAssailant)
                        ENDIF
                    ENDIF
                ENDIF
                WAIT(0)
                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"runHoldUpAnimVariation() - missionCleanup() CASE 6")
                missionCleanup()
            ENDIF
        BREAK
        
        CASE 7 // set up save victim 'thank you'
            PRINTSTRING("Paul case 7") PRINTNL()
            SETTIMERA(0)

            iHoldUpStages ++
        BREAK
        
        CASE 8 // thank you
        PRINTSTRING("Paul case 8") PRINTNL()
            IF TIMERA() > 200
                PRINTSTRING("PAUl - THANK PLAYER3") PRINTNL()
                thankPlayer()
            ENDIF
        BREAK
        
        CASE 9 // bumped in to
        PRINTSTRING("Paul case 9") PRINTNL()
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF NOT IS_PED_INJURED(pedAssailant)
                    SET_PED_RESET_FLAG(pedAssailant, PRF_InstantBlendToAim, TRUE)
                    TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedAssailant, 50)
                        
                    IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                        KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
                    ENDIF

                    WAIT(0)
                    CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWho, CONV_PRIORITY_AMBIENT_HIGH)
                ENDIF
            ENDIF   
            IF DOES_ENTITY_EXIST(pedVictim)
                IF NOT IS_PED_INJURED(pedVictim)        
                    CLEAR_PED_TASKS(pedVictim)
                    TASK_COWER(pedVictim, 2000)
                    bSavedVictim = TRUE
                ENDIF   
            ENDIF
        BREAK
        
    ENDSWITCH
    
    IF iHoldUpStages <> 8                           
    AND bSavedVictim
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " holdupstage = ", iHoldUpStages)
        iHoldUpStages = 7
    ENDIF
                                                                        
    iTimerCurrentMuggingAnim =  GET_GAME_TIMER()
    
    // if player keeps back     
    IF bSetTimer AND NOT bFightingWithPlayer
        IF DOES_ENTITY_EXIST(pedAssailant)
        AND  DOES_ENTITY_EXIST(pedAssailant)
            IF NOT IS_PED_INJURED(pedVictim)
            AND NOT IS_PED_INJURED(pedAssailant)
                IF (iTimerCurrentMuggingAnim - iTimerStartMuggingAnim) > 12000
                AND NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<3, 3, 35>>)
                AND IS_ENTITY_ON_SCREEN(pedAssailant)
                AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                    IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, "REMG2_FKIT", CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING() OR IS_CELLPHONE_CONVERSATION_PLAYING()
                        SET_PED_RESET_FLAG(pedAssailant, PRF_InstantBlendToAim, TRUE)
                        SET_PED_SHOOTS_AT_COORD(pedAssailant, GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>))
                        CLEAR_PED_TASKS(pedVictim)
                        SET_ENTITY_HEALTH(pedVictim, 99)
                        TASK_SMART_FLEE_COORD(pedAssailant, vInitialMarkerPos, 250, -1)
                        SET_PED_KEEP_TASK(pedAssailant, TRUE)
                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," bSetTimer - missionCleanup() Player stands back")
                        missionCleanup()
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF

ENDPROC
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//-------------------------------------------------------------------------------------OLD LADY VARIATION END----------------------------------------------------------------------------------------------------
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
FUNC BOOL IS_PLAYER_SHOOTING_BEFORE_MUGGING_STARTED()
    IF DOES_ENTITY_EXIST(objIdStolenItem)
        IF IS_ENTITY_ATTACHED(objIdStolenItem)
            IF NOT IS_PED_INJURED(pedVictim)
                IF IS_ENTITY_ATTACHED_TO_ENTITY(objIdStolenItem, pedVictim) 
                    IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<18, 18, 18>>, FALSE)
                        IF IS_PED_SHOOTING(PLAYER_PED_ID())
                            RETURN TRUE
                        ENDIF
                    ENDIF   
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

PROC PlayerShootingNearPedsInitialStage()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - PlayerShootingNearPedsInitialStage()")
    // flee if the player is shooting near peds before and during the initial stage of the hold up 
//  // (not after the victim has dropped their bag)
    IF NOT IS_PED_INJURED(pedVictim)
        CLEAR_PED_TASKS(pedVictim)
        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
        SET_PED_KEEP_TASK(pedVictim, TRUE)
        SET_PED_AS_NO_LONGER_NEEDED(pedVictim)              
    ENDIF

    //ENDIF 
    //          if both fleeing both will have been marked as not needed so script will clean up
ENDPROC

PROC SWITCH_BLIPS_ON_EARLY_INTERVENTION()
    IF DOES_BLIP_EXIST(blipVictim)
        REMOVE_BLIP(blipVictim)
    ENDIF
    IF NOT DOES_BLIP_EXIST(blipStolenItem)
        IF DOES_PICKUP_EXIST(pickupStolenItem) 
            blipStolenItem = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupStolenItem)
        ENDIF
    ENDIF
    thisMuggingStage = ABLE_TO_RETRIEVE_DROPPED_BAG
ENDPROC

ENUM muggerState
    mugger1,
    mugger2,
    mugger3,
    mugger4,
    mugger5
ENDENUM

muggerState ms = mugger1
INT startPickupTimer = 0
BOOL purseStillinWorld = FALSE
VECTOR pickupCoords
PROC muggerApproach()
    
    
    IF NOT bStartedMugging
        FLOAT fGoToRadius
        IF iSelectedVariation = ONE_GUY_VICTIM
            fGoToRadius = 1.0
        ENDIF
        IF iSelectedVariation = TWO_HOLD_UP_ANIM
            fGoToRadius = 4.0
        ENDIF
        
        IF iSelectedVariation <> TWO_HOLD_UP_ANIM
            IF NOT IS_PED_INJURED(pedVictim)// MUGGING SEQUENCES
            AND NOT IS_PED_INJURED(pedAssailant)
                IF NOT IS_THIS_RANDOM_EVENT_SCRIPT_ACTIVE()
                    IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_ACTIVATION()
                        missionCleanup()
                    ENDIF
                ENDIF
                
                //IF iSelectedVariation = ONE_GUY_VICTIM
                    //Only do this for the first mugging.
                    //Mugging 3, there is too much dialogue for you to be able to hear it all and have the mugger run off before you get there.
                //  IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedAssailant)) < 80.0) AND (initialConvoSaid = FALSE)
                //      PRINTSTRING("initialConvoSaid CREATE_CONVERSATION") PRINTNL()
                //      CREATE_CONVERSATION(MuggingConversation, sTextBlock, sDemand, CONV_PRIORITY_AMBIENT_HIGH)
                //      initialConvoSaid = TRUE
                //  ENDIF
                //ENDIF
                
                IF HAS_PLAYER_HIT_BLIP_TRIGGER()
                    //create the blip
                    IF NOT DOES_BLIP_EXIST(blipLocation)
                        IF NOT IS_ENTITY_DEAD(pedAssailant)
                            blipLocation = CREATE_AMBIENT_BLIP_FOR_PED(pedAssailant, TRUE)
                        ENDIF
                        PRINTSTRING("Paul - SET_RANDOM_EVENT_ACTIVE") PRINTNL()
                        SET_RANDOM_EVENT_ACTIVE()
                    ENDIF
                ENDIF
    
                IF HAS_PLAYER_HIT_CUSTOM_TRIGGER()
                    changeBlips()
                    
                    IF iSelectedVariation = THREE_GIRL_SHOPPER
                        IF NOT bGirlMoving
                            OPEN_SEQUENCE_TASK(seq)
                                TASK_LOOK_AT_ENTITY(NULL, pedAssailant, 16000)
                                TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vMuggingGoToPoint, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, fGoToRadius, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
                            CLOSE_SEQUENCE_TASK(seq)
                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            bGirlMoving = TRUE
                        ENDIF
                        
                        OPEN_SEQUENCE_TASK(seq)
                            TASK_LOOK_AT_ENTITY(NULL, pedVictim, -1)
                            TASK_PLAY_ANIM(NULL,"AMB@PROP_HUMAN_SEAT_CHAIR@MALE@GENERIC@EXIT", "exit_forward", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_TURN_OFF_COLLISION)
                            TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vMuggingGoToPoint, pedVictim, PEDMOVE_WALK, FALSE, 1.5)
                            TASK_AIM_GUN_AT_ENTITY(NULL, pedVictim, -1, TRUE)
                        CLOSE_SEQUENCE_TASK(seq)
                        TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                        CLEAR_SEQUENCE_TASK(seq)
                        SETTIMERA(0)
                        
                        bStartedMugging = TRUE
                        SET_WANTED_LEVEL_MULTIPLIER(0)
                    ELSE
                        //todo - create this only once
                        //then test for the next mugger flee when there isn't a convo goigin.
                        IF iSelectedVariation = ONE_GUY_VICTIM
                            sDemand=sDemand
                            //CREATE_CONVERSATION(MuggingConversation, sTextBlock, sDemand, CONV_PRIORITY_AMBIENT_HIGH)
                        ENDIF
                        SET_MUGGER_TO_FLEE()
                        bStartedMugging = TRUE
                        bMakingARunForIt = TRUE
                        SETTIMERA(0)
                        
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    IF bStartedMugging 
        IF NOT bDropYourBagSpeech
            IF TIMERA() > 1000
                IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<60, 60, 50>>)
                    //CREATE_CONVERSATION(MuggingConversation, sTextBlock, sDemand, CONV_PRIORITY_AMBIENT_HIGH)
                    //"Hey you, gimme your purse!"
                    IF NOT IS_ENTITY_DEAD(pedVictim)
                        TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, pedAssailant)
                    ENDIF
                    bDropYourBagSpeech = TRUE
                ELSE
                    bDropYourBagSpeech = TRUE
                ENDIF
            ENDIF
        ENDIF
        
        IF bDropYourBagSpeech
        AND NOT bVictimDroppedBag
            IF NOT IS_PED_INJURED(pedVictim)    
            AND NOT IS_PED_INJURED(pedAssailant)
                IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sVicDrop, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
                    CLEAR_PED_TASKS(pedVictim)
                    OPEN_SEQUENCE_TASK(seq)
                        TASK_PLAY_ANIM(NULL, sMuggingDict, "handsup_standing_enter", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                        TASK_PLAY_ANIM(NULL, sMuggingDict, "handsup_standing_base", NORMAL_BLEND_IN, SLOW_BLEND_OUT, 7000)
                        TASK_PLAY_ANIM(NULL, sMuggingDict, "handsup_standing_exit", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                    CLOSE_SEQUENCE_TASK(seq)
                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                    CLEAR_SEQUENCE_TASK(seq)
                    changeBlips()
                    pickupStolenItem = CREATE_PICKUP(PICKUP_MONEY_PURSE, GET_SAFE_PICKUP_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0,2.5,0>>)), iPlacementFlags, iItemValue, TRUE, modelBag)
                    bVictimDroppedBag = TRUE
                ENDIF
            ENDIF
        ENDIF
        
        IF bVictimDroppedBag
            IF IS_PED_INJURED(pedAssailant) 
                SWITCH_BLIPS_ON_EARLY_INTERVENTION()
            ELSE
                SWITCH ms
                    CASE mugger1
                        PRINTSTRING("Paul mugger1") PRINTNL()
                        pickupCoords = GET_PICKUP_COORDS(pickupStolenItem)-<<0.75, 0, 0>>
                        IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                            CLEAR_PED_TASKS(pedAssailant)
                            OPEN_SEQUENCE_TASK(seq)
                                TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, pickupCoords, pedVictim, PEDMOVE_RUN, FALSE, 0.1)
                            CLOSE_SEQUENCE_TASK(seq)
                            TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            startPickupTimer = GET_GAME_TIMER()
                        ENDIF
                        ms = mugger2
                    BREAK
                    
                    CASE mugger2
                        PRINTSTRING("Paul mugger2") PRINTNL()
                        IF (GET_SCRIPT_TASK_STATUS(pedAssailant, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK)
                            pickupCoords = GET_PICKUP_COORDS(pickupStolenItem)-<<0.75, 0, 0>>
                            IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                                IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedAssailant), pickupCoords) > 1.0
                                    PRINTSTRING("Paul GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedAssailant), GET_PICKUP_COORDS(pickupStolenItem)) > 1.0") PRINTNL()
                                    purseStillinWorld = TRUE
                                    ms = mugger5
                                ELSE
                                    //Start picking it up.
                                    CLEAR_PED_TASKS(pedAssailant)
                                    OPEN_SEQUENCE_TASK(seq)
                                        TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_EXIT_AFTER_INTERRUPTED)
                                    CLOSE_SEQUENCE_TASK(seq)
                                    TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                                    CLEAR_SEQUENCE_TASK(seq)
                                    ms = mugger3
                                ENDIF
                            ELSE
                                //Already gone!
                                ms = mugger5
                            ENDIF
                        ENDIF
                        IF ((GET_GAME_TIMER() - startPickupTimer) > 15000)
                            purseStillinWorld = TRUE
                            ms = mugger5
                        ENDIF
                    BREAK
                    
                    CASE mugger3
                        PRINTSTRING("Paul mugger3") PRINTNL()
                        IF GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sPickupDict, "pickup_low") > 0.5  
                            IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                                REMOVE_PICKUP(pickupStolenItem)
                                ms = mugger4
                            ELSE
                                //Not there!
                                ms = mugger5
                            ENDIF
                        ENDIF
                    BREAK
                    
                    case mugger4
                        PRINTSTRING("Paul mugger4") PRINTNL()
                        IF (IS_ENTITY_PLAYING_ANIM(pedAssailant, sPickupDict, "pickup_low") AND GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sPickupDict, "pickup_low") > 0.58)
                        OR NOT IS_ENTITY_PLAYING_ANIM(pedAssailant, sPickupDict, "pickup_low")
                            ms = mugger5
                        ENDIF
                    BREAK
                    
                    case mugger5    
                        PRINTSTRING("Paul mugger5") PRINTNL()
                        SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, TRUE)
                        PRINTSTRING("PAUL -TASK_SMART_FLEE_PED pedAssailant")
                        TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 50, -1)
                        FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAssailant)
                        bMakingARunForIt = TRUE
                    BREAK
                ENDSWITCH
                
                /*IF DOES_PICKUP_EXIST(pickupStolenItem) 
                    IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                        IF NOT bRobberGivenPickupTask
                            CLEAR_PED_TASKS(pedAssailant)
                            OPEN_SEQUENCE_TASK(seq)
                                TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, GET_PICKUP_COORDS(pickupStolenItem), pedVictim, PEDMOVE_RUN, FALSE)
                                TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_SECONDARY|AF_EXIT_AFTER_INTERRUPTED)
                                //TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
                            CLOSE_SEQUENCE_TASK(seq)
                            TASK_PERFORM_SEQUENCE(pedAssailant, seq)
                            CLEAR_SEQUENCE_TASK(seq)
                            bRobberGivenPickupTask = TRUE 
                        ENDIF
                    ENDIF
                    IF IS_ENTITY_PLAYING_ANIM(pedAssailant, sPickupDict, "pickup_low")
                        IF GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sPickupDict, "pickup_low") > 0.5      
                            IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                                REMOVE_PICKUP(pickupStolenItem)
                                PRINTNL()
                                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " REMOVE_PICKUP(pickupStolenItem)")
                                PRINTNL()
                            ENDIF
                        ENDIF
                    ENDIF
                ELSE
                    IF (IS_ENTITY_PLAYING_ANIM(pedAssailant, sPickupDict, "pickup_low") AND GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sPickupDict, "pickup_low") > 0.58)
                    OR NOT IS_ENTITY_PLAYING_ANIM(pedAssailant, sPickupDict, "pickup_low")
                        //CLEAR_PED_TASKS(pedAssailant)
                        SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, TRUE)
                        PRINTSTRING("PAUL -TASK_SMART_FLEE_PED pedAssailant")
                        TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 50, -1)
                        FORCE_PED_AI_AND_ANIMATION_UPDATE(pedAssailant)
                        bMakingARunForIt = TRUE
                    ENDIF
                ENDIF*/
            ENDIF
            
        ENDIF   
    ENDIF

    
ENDPROC

PROC initialCryForHelp()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - initialCryForHelp()")
    // The mugged ped plays an anim where they are stood and shouts out for anyone to help.
    IF NOT IS_PED_INJURED(pedVictim)
    
        IF NOT bRobbedTimerSet
            iRobbedTimer = GET_GAME_TIMER()
            bRobbedTimerSet = TRUE
        ENDIF
        
        IF bRobbedTimerSet
            iRobbedTimerCurrent = GET_GAME_TIMER()
            IF NOT bCryOut
                IF (iRobbedTimerCurrent - iRobbedTimer) > 500
                    IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                        //"Somebody help! That guy just mugged me!"
                        IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sShout, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
                            IF thisEvent = GUY_VICTIM
                                IF NOT IS_PED_INJURED(pedVictim)
                                    IF NOT IS_PED_INJURED(pedAssailant) 

                                    ENDIF
                                ENDIF
                            ENDIF
                            IF thisEvent = GIRL_SHOPPER
                                IF NOT IS_PED_INJURED(pedVictim)

                                ENDIF
                            ENDIF
                            IF DOES_BLIP_EXIST(blipVictim)
                                REMOVE_BLIP(blipVictim)
                            ENDIF
                            bCryOut = TRUE
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF   

ENDPROC

FUNC BOOL HAS_PLAYER_APPROACHED_VICTIM()
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<12, 12, 5>>)   
            RETURN TRUE
        ENDIF   
    ENDIF
    RETURN FALSE
    
ENDFUNC

PROC approachingPlayer()
    // If the player approaches the mugged ped they will ask them for help specifically

    
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - approachingPlayer()")
    IF NOT IS_PED_INJURED(pedVictim)
        IF NOT bVictimGoneToPlayer
            IF IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_backward_loop_shopkeeper")
                PRINTSTRING("flee_backward_outro_shopkeeper") PRINTNL()
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_backward_outro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                    TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
                iAnimSeqList = 3
                bVictimGoneToPlayer = TRUE
            ELIF IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_forward_loop_shopkeeper")
                PRINTSTRING("flee_forward_loop_shopkeeper") PRINTNL()
                OPEN_SEQUENCE_TASK(seq)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_forward_outro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                    TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                    TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
                CLOSE_SEQUENCE_TASK(seq)
                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                CLEAR_SEQUENCE_TASK(seq)
                iAnimSeqList = 3
                bVictimGoneToPlayer = TRUE
            ELSE
                bVictimGoneToPlayer = TRUE
            ENDIF
        ENDIF
        
        IF bVictimGoneToPlayer
            IF NOT bAskPlayerForHelpLine
                IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<7, 7, 7>>)
                    IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
                        IF GET_SEQUENCE_PROGRESS(pedVictim) = iAnimSeqList
                            TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 9000)
                            //TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID(), 9000)
                            CREATE_CONVERSATION(MuggingConversation, sTextBlock, sAsk, CONV_PRIORITY_AMBIENT_HIGH)
                            bAskedPlayerForHelp = TRUE
                            bAskPlayerForHelpLine = TRUE
                        ENDIF
                    ELSE
                        STOP_PED_SPEAKING(pedVictim, TRUE) 
                        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sAsk, CONV_PRIORITY_AMBIENT_HIGH)
                        
                        OPEN_SEQUENCE_TASK(seq)
                            TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 7000)
                            TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 7000)
                        CLOSE_SEQUENCE_TASK(seq)
                        TASK_PERFORM_SEQUENCE(pedVictim, seq)
                        CLEAR_SEQUENCE_TASK(seq)
                        SETTIMERA(0)
                        bAskedPlayerForHelp = TRUE
                        bAskPlayerForHelpLine = TRUE
                    ENDIF
                ENDIF
            ENDIF   
        ENDIF
    
    ENDIF
                
ENDPROC
        
PROC waitingForPlayerIntervention()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - waitingForPlayerIntervention()")
    IF thisEvent = GUY_VICTIM
        //continualTurnToFacePlayer(pedVictim, iTurnStage)
    ENDIF
    
    IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
        IF TIMERA() > 10000
            IF NOT IS_PED_INJURED(pedVictim)
                IF NOT IS_ANY_SPEECH_PLAYING(pedVictim)
                    PLAY_PED_AMBIENT_SPEECH(pedVictim, "GENERIC_CURSE_MED")
                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "PLAY_PED_AMBIENT_SPEECH(pedVictim, GENERIC_CURSE_MED)")
                    SETTIMERA(0)
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    IF NOT bPurseHasDropped
        IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAssailant) > 200
            IF (NOT IS_ENTITY_ON_SCREEN(pedAssailant) OR IS_ENTITY_OCCLUDED(pedAssailant))
                thisMuggingStage = LOST_MUGGER
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC pickUpTheStolenItem()
// needs a code fix
    IF HAS_PICKUP_BEEN_COLLECTED(pickupStolenItem)
        //===========================
        // for later check: leave scene
        IF NOT IS_ENTITY_DEAD(pedVictim)
            fStoreBagCollectDist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim))
        ENDIF
        QUICK_INCREMENT_INT_STAT(RC_WALLETS_RECOVERED, 1)
        //===========================
        SET_CONTROL_SHAKE(PLAYER_CONTROL, 200, 250)
        IF DOES_ENTITY_EXIST(objIdStolenItem)
            DELETE_OBJECT(objIdStolenItem)
        ENDIF
        blipForVictim()
        START_RETURN_ITEM_BLIP_FLASHING(flashTimer)
        IF DOES_ENTITY_EXIST(pedVictim)
            IF NOT IS_PED_INJURED(pedVictim)
                SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, FALSE)
            ENDIF
        ENDIF
        
        SNAP_OUT_OF_LYING_DOWN_ANIM(FALSE)
        
        REMOVE_PICKUP(pickupStolenItem)
            
        IF NOT bVictimFled
            thisMuggingStage = PLAYER_HAS_BAG
            PRINT_LIMITED_RANDOM_EVENT_HELP(REHLP_RETURN_ITEM)
        ELSE
            thisMuggingStage = GET_FOR_SELF
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_TALKING_ON_FOOT()
    IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
            RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

PROC RETURN_BAG_SYNCED_SCENE()
    VEHICLE_INDEX tempCar
    VECTOR vTemp, vMin, vMax, vDistance
    FLOAT fAdjacent, fOpposite
    IF NOT IS_PED_INJURED(pedVictim)
        IF NOT bRunHandover
            IF bRunHint
                VECTOR player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
                BOOL height_check = TRUE
                IF iSelectedVariation = ONE_GUY_VICTIM
                    IF (player_pos.z > 33.5)
                        height_check = FALSE //If we are over a certain height and mugging1, don't run the scene. 
                    ENDIF
                ENDIF
                
                IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<5.0, 5.0, 5.0>>) AND height_check = TRUE
                    PRINTFLOAT(fLocateSize) PRINTNL()
                    IF IS_THIS_RANDOM_EVENT_HELP_BEING_DISPLAYED(REHLP_RETURN_ITEM)
                        CLEAR_HELP(TRUE)
                    ENDIF
                    IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
                        IF CAN_PLAYER_START_CUTSCENE()
                            IF NOT IS_PLAYER_TALKING_ON_FOOT()
                                IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
                                    SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
                                    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                                        WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), fStoppingDistance)
                                        OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                                        HIDE_HUD_AND_RADAR_THIS_FRAME()
                                        WAIT(0)
                                        ENDWHILE
                                        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                                            TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
                                        ENDIF
                                    ENDIF
                                    SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
                                    IF NOT IS_PED_INJURED(pedVictim)
                                        SET_PED_CAN_RAGDOLL(pedVictim, FALSE)
                                    ENDIF
                                    HIDE_HUD_AND_RADAR_THIS_FRAME()
                                    REMOVE_ALL_SHOCKING_EVENTS(FALSE)
                                    REMOVE_SHOCKING_EVENT(iShockingEvent)
                        
                                    IF IS_SCRIPTED_CONVERSATION_ONGOING()//Added by Steve T to fix audio issue documented in bug 1930539
                                        KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
                                    ENDIF

                                    bRunHandover = TRUE
                                    SETTIMERA(0)
                                    
                                    //PRINTSTRING("PAUL KILL") PRINTNL()
                                    //SET_ENTITY_HEALTH(pedVictim, 0)
                                    //DELETE_PED(pedVictim)
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ELSE
                    SCALE_CUTSCENE_TRIGGERS_BASED_ON_PLAYER_SPEED(GET_ENTITY_COORDS(pedVictim), fLocateSize, fStoppingDistance)
                ENDIF
                
            ELSE
                IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<15,15,15>>)
                //AND GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                    
                    OPEN_SEQUENCE_TASK(seq)
//                      TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO)
//                      TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, 
//                          (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim))-3), ENAV_KEEP_TO_PAVEMENTS)
                        TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
                        TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
                        IF thisEvent = GUY_VICTIM
                            PRINTSTRING("Paul IF thisEvent = GUY_VICTIM") PRINTNL()
                            TASK_PLAY_ANIM(NULL, sReturnDict, "RETURNING_FRONT_A", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, -1)
                        ENDIF
                    CLOSE_SEQUENCE_TASK(seq)
                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                    CLEAR_SEQUENCE_TASK(seq)
                    FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK)
                    bRunHint = TRUE
                ENDIF
            ENDIF
        ENDIF
        
        IF bRunHandover
            SWITCH iHandOver
                CASE 0
                    HIDE_HUD_AND_RADAR_THIS_FRAME()
                    //IF IS_PED_ON_FOOT(PLAYER_PED_ID())
                    IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
                        IF NOT IS_PED_INJURED(pedVictim)
                            //GET_POSITION_FOR_HANDOOVER_SCENE(pedVictim, scenePosition, sceneRotation.z)
                            GET_POSITION_FOR_WALLET_SCENE(pedVictim, scenePosition, sceneRotation, sHandoverDict, sHandoverPed)
                            
                            //If we are at an angle, limit this so we don't have the scene in the middle of the scenery. 
                            PRINTSTRING("Paul - angle") PRINTFLOAT(sceneRotation.z) PRINTNL()
                            IF iSelectedVariation = ONE_GUY_VICTIM
                                IF sceneRotation.z > 225 AND sceneRotation.z < 265 
                                    //245
                                    PRINTSTRING("PAUL - angle change! 245") PRINTNL()
                                    IF sceneRotation.z < 245
                                        sceneRotation.z = 225
                                    ELSE
                                        sceneRotation.z = 265
                                    ENDIF
                                ELIF sceneRotation.z > 45 AND sceneRotation.z < 85 
                                    //65
                                    PRINTSTRING("PAUL - angle change! 65") PRINTNL()
                                    IF sceneRotation.z < 65
                                        sceneRotation.z = 45
                                    ELSE
                                        sceneRotation.z = 85
                                    ENDIF
                                ENDIF
                            ENDIF
                                
                            
                            scenePosition.z +=2
                            GET_GROUND_Z_FOR_3D_COORD(scenePosition, scenePosition.z)
                            
                            vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 0)
                            vDistance = vTemp - scenePosition
                            vDistance.z = 0
                            fAdjacent = VMAG(vDistance)
                            GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
                            fOpposite = scenePosition.z - vTemp.z
                                                            
                            sceneRotation.x = ATAN2(fOpposite, fAdjacent)
                            IF sceneRotation.x > 30
                            OR sceneRotation.x < -30
                                sceneRotation.x = 0
                            ENDIF
                        ENDIF
                            
                        IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
                            IF NOT IS_VEHICLE_DRIVEABLE(tempCar)
                                tempCar = GET_PLAYERS_LAST_VEHICLE()
                                GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(tempCar), vMin, vMax)
                                IF IS_ENTITY_AT_COORD(tempCar, scenePosition, <<vMax.y+1.5, vMax.y+1.5, 3>>)
                                OR IS_ENTITY_AT_COORD(tempCar, GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation), <<vMax.y+1, vMax.y+1, 3>>)
                                
                                    PRINTSTRING("SCENER ROT RESPOTTING") PRINTFLOAT(sceneRotation.z) PRINTNL()
                                    IF iSelectedVariation = ONE_GUY_VICTIM
                                        IF (sceneRotation.z > 40) AND (sceneRotation.z < 220)
                                            vRespot = <<-148.3897, -1647.2146, 31.5867>>
                                            fRespot = 331.9427
                                        ELSE
                                            vRespot = <<-129.5632, -1622.2823, 31.1122>>
                                            fRespot = 148.9075
                                        ENDIF
                                    ELIF iSelectedVariation = THREE_GIRL_SHOPPER
                                        IF (sceneRotation.z > 160) AND (sceneRotation.z < 340)
                                            vRespot = <<-319.7090, -837.0066, 30.4490>>
                                            fRespot = 61.7986
                                        ELSE
                                            vRespot = <<-346.8735, -833.8024, 30.4135>>
                                            fRespot = 269.6216
                                        ENDIF
                                    ELIF iSelectedVariation = FOUR_GIRL_SHOPPER
                                        IF (sceneRotation.z > 240) OR (sceneRotation.z < 60)
                                            vRespot = <<37.2220, -1005.0401, 28.4648>>
                                            fRespot = 156.7986
                                        ELSE
                                            vRespot = <<29.9887, -1033.1838, 28.3794>>
                                            fRespot = 344.6216
                                        ENDIF
                                    ELSE
                                        fRespot = GET_ENTITY_HEADING(tempCar)
                                        vRespot = GET_UNOCCUPIED_POSITION_FOR_PLAYERS_CAR(scenePosition, GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation))
                                    ENDIF
                                    
                                    PRINTSTRING("PAUL vRespot") PRINTVECTOR(vRespot) PRINTNL()
                                    PRINTSTRING("PAUL fRespot") PRINTFLOAT(fRespot) PRINTNL()
                                    PRINTSTRING("PAUL scenePosition") PRINTVECTOR(scenePosition) PRINTNL()
                                    
                                    IF IS_VEHICLE_DRIVEABLE(tempCar)
                                        SET_ENTITY_COORDS(tempCar, vRespot)
                                        SET_ENTITY_HEADING(tempCar, fRespot)
                                        SET_VEHICLE_ON_GROUND_PROPERLY(tempCar)
                                        SET_VEHICLE_DOORS_SHUT(tempCar)
                                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - SET_ENTITY_COORDS(tempCar, vPlayersVeh)")
                                    ENDIF
                                ENDIF
                            ENDIF
                        ENDIF
                        CLEAR_AREA_OF_OBJECTS(scenePosition, 20, CLEAROBJ_FLAG_FORCE)
                        CLEAR_AREA_OF_PROJECTILES(scenePosition, 20)
                        CLEAR_AREA(scenePosition, 2, TRUE, TRUE)
                        
                        HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
                        objIdStolenItem = CREATE_OBJECT(modelBag, GET_ENTITY_COORDS(PLAYER_PED_ID()))
                        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                            ATTACH_ENTITY_TO_ENTITY(objIdStolenItem, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
                        ENDIF
                        sceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
                        SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sceneId, FALSE)
                        CamIDPickingUpBag = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
                        PLAY_SYNCHRONIZED_CAM_ANIM(CamIDPickingUpBag, sceneId, sHandoverCam, sHandoverDict)
                        CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
                        TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneId, sHandoverDict, sHandoverPlayer, INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
                        IF NOT IS_PED_INJURED(pedVictim)
                            CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
                            TASK_SYNCHRONIZED_SCENE(pedVictim, sceneId, sHandoverDict, sHandoverPed, INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
                        ENDIF
                        
                        SET_CAM_ACTIVE(CamIDPickingUpBag, TRUE)
                        RENDER_SCRIPT_CAMS(TRUE, FALSE)     
                        
                        //WAIT(0)
                        //recreate bag
                        
                        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sHandover, CONV_PRIORITY_AMBIENT_HIGH)
                        
                        SETTIMERA(0)
                        
                        iHandOver ++
                    ENDIF
                    HIDE_HUD_AND_RADAR_THIS_FRAME()
                BREAK
                
                CASE 1
                    //IF TIMERA() > 3500
                    IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(sceneId) > 0.9)
                    OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
                        SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
                        SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
                        SET_CAM_ACTIVE(CamIDPickingUpBag, FALSE)
                        DESTROY_CAM(CamIDPickingUpBag)  
                        SET_CAMERA_OVER_THE_SHOULDER_FOR_CATCH_UP()
                        IF NOT IS_PED_INJURED(pedVictim)
                            CLEAR_PED_TASKS_IMMEDIATELY(pedVictim)
                            SET_PED_MONEY(pedVictim, (iItemValue-(iItemValue/10)))
                            
                            vTemp = GET_ANIM_INITIAL_OFFSET_POSITION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 1)
                            GET_GROUND_Z_FOR_3D_COORD(vTemp, vTemp.z)
                            SET_ENTITY_COORDS(pedVictim, vTemp)
                            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedVictim, FALSE)
                            vTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(sHandoverDict, sHandoverPed, scenePosition, sceneRotation, 1)
                            SET_ENTITY_HEADING(pedVictim, vTemp.z)
                            IF iSelectedVariation = FOUR_GIRL_SHOPPER
                                IF IS_HEADING_ACCEPTABLE(vTemp.z, 0, 90)
                                    OPEN_SEQUENCE_TASK(seq)
                                        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<38.2412, -992.2134, 28.4317>>, PEDMOVEBLENDRATIO_WALK)
                                        TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
                                    CLOSE_SEQUENCE_TASK(seq)
                                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                    CLEAR_SEQUENCE_TASK(seq)
                                ELSE
                                    OPEN_SEQUENCE_TASK(seq)
                                        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<58.8030, -1067.7108, 28.4411>>, PEDMOVEBLENDRATIO_WALK)
                                        TASK_WANDER_STANDARD(NULL, GET_ENTITY_HEADING(pedVictim), EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
                                    CLOSE_SEQUENCE_TASK(seq)
                                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                    CLEAR_SEQUENCE_TASK(seq)
                                ENDIF
                            ELSE
                                OPEN_SEQUENCE_TASK(seq)
                                    //TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0,3,0>>),PEDMOVEBLENDRATIO_WALK)
                                   // TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedVictim, <<0, 2, 0>>), PEDMOVEBLENDRATIO_WALK, DEFAULT, DEFAULT, ENAV_NO_STOPPING|ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
                                    TASK_WANDER_STANDARD(NULL, vTemp.z, EWDR_KEEP_MOVING_WHILST_WAITING_FOR_FIRST_PATH)
                                CLOSE_SEQUENCE_TASK(seq)
                                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                CLEAR_SEQUENCE_TASK(seq)
                            ENDIF
                            SET_PED_MOVEMENT_CLIPSET(pedVictim, sClipSetOverride)
                            FORCE_PED_MOTION_STATE(pedVictim, MS_ON_FOOT_WALK, TRUE)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                        ENDIF
                        IF NOT IS_PED_INJURED(PLAYER_PED_ID())
                            CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
                            FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
                        ENDIF
                        IF DOES_ENTITY_EXIST(objIdStolenItem)
                            DELETE_OBJECT(objIdStolenItem)
                        ENDIF
                        IF NOT IS_PED_INJURED(pedVictim)
                            SET_PED_MONEY(pedVictim, iItemValue-(iItemValue/10))
                            SET_PED_CAN_RAGDOLL(pedVictim, TRUE)
                        ENDIF
                        INT debitCash
                        debitCash = iItemValue/100 
                        debitCash = debitCash*90 // take away 90% of the stolen cash, play keeps 10%
                        WAIT(0)
                        STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
                        PRINTSTRING("Paul debit account") PRINTNL()
                        DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, debitCash)
                        QUICK_INCREMENT_INT_STAT(RC_WALLETS_RETURNED, 1)
                        
                        SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
                        missionPassed()
                    ELSE
                        IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("Detach"))
                            IF NOT IS_PED_INJURED(pedVictim)
                            AND NOT IS_PED_INJURED(PLAYER_PED_ID())
                                IF IS_ENTITY_ATTACHED_TO_ENTITY(objIdStolenItem, PLAYER_PED_ID())
                                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Detach tag.")
                                    DETACH_ENTITY(objIdStolenItem, FALSE)
                                    
                                    IF IS_PED_MALE(pedVictim)
                                        ATTACH_ENTITY_TO_ENTITY(objIdStolenItem, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_PH_R_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
                                    ELSE
                                        ATTACH_ENTITY_TO_ENTITY(objIdStolenItem, pedVictim, GET_PED_BONE_INDEX(pedVictim, BONETAG_PH_L_HAND), << 0, 0, 0 >>, << 0, 0, 0 >>)
                                    ENDIF
                                    
                                    IF NOT bPlayedVictimDialogue
                                        IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sThx, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
                                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Victim dialogue tried to trigger.")
                                            bPlayedVictimDialogue = TRUE
                                        ENDIF
                                    ENDIF
                                ENDIF
                            ENDIF
                        ELSE
                            IF NOT bPlayedVictimDialogue
                                IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sThx, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
                                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Victim dialogue tried to trigger.")
                                    bPlayedVictimDialogue = TRUE
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                    HIDE_HUD_AND_RADAR_THIS_FRAME()
                BREAK
            ENDSWITCH
            
        ENDIF
    ELSE
        //Ped is dead, cleanup.
        PRINTSTRING("Paul - ped is dead while we are waiting to start the cutscene.") PRINTNL()
        missionCleanup()
    ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_HARMED_AIMED_AT_VICTIM()
    WEAPON_TYPE wPed
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE|WF_INCLUDE_MELEE)
            IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<20,20,20>>)
                IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
                OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
                    CPRINTLN(DEBUG_AMBIENT, "HAS_PLAYER_HARMED_AIMED_AT_VICTIM() TRUE 1")
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
        IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wPed)
            IF wPed = WEAPONTYPE_STUNGUN 
                IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedVictim, WEAPONTYPE_STUNGUN)
                    CPRINTLN(DEBUG_AMBIENT, "HAS_PLAYER_HARMED_AIMED_AT_VICTIM() TRUE 2")
                    RETURN TRUE
                ENDIF
            ENDIF
        ENDIF
        IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
            CPRINTLN(DEBUG_AMBIENT, "HAS_PLAYER_HARMED_AIMED_AT_VICTIM() TRUE 3")
            RETURN TRUE
        ENDIF
    ELSE
//      IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
            CPRINTLN(DEBUG_AMBIENT, "HAS_PLAYER_HARMED_AIMED_AT_VICTIM() TRUE 4")
            RETURN TRUE
//      ENDIF
    ENDIF
    
    IF IS_BULLET_IN_AREA(GET_PED_BONE_COORDS(pedVictim, BONETAG_HEAD, <<0,0,0>>), 3)
        return TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC AimingAtVictim()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - AimingAtVictim()")
    IF NOT IS_PED_INJURED(pedVictim)
        CLEAR_PED_TASKS(pedVictim)
        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
        SET_PED_KEEP_TASK(pedVictim, TRUE)
        IF DOES_ENTITY_EXIST(objIdStolenItem)
            DELETE_OBJECT(objIdStolenItem)
        ENDIF
        WAIT(0)
        IF DOES_BLIP_EXIST(blipVictim)
            REMOVE_BLIP(blipVictim)
        ENDIF
        
        SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 5")
    ENDIF   
//  IF NOT bAttackPlayer
//      IF NOT IS_PED_INJURED(pedAssailant)
//          CLEAR_PED_TASKS(pedAssailant)
//          OPEN_SEQUENCE_TASK(seq)
//              TASK_CLEAR_LOOK_AT(NULL)
//              TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
//          CLOSE_SEQUENCE_TASK(seq)
//          TASK_PERFORM_SEQUENCE(pedAssailant, seq)
//          CLEAR_SEQUENCE_TASK(seq)
//          
//          SET_PED_KEEP_TASK(pedAssailant, TRUE)
//          bAttackPlayer = TRUE
//      ENDIF   
//  ENDIF
    IF IS_PED_INJURED(pedAssailant)
    OR IS_PED_INJURED(pedVictim)
        IF DOES_BLIP_EXIST(blipMugger)
            REMOVE_BLIP(blipMugger)
        ENDIF
        IF DOES_BLIP_EXIST(blipVictim)
            REMOVE_BLIP(blipVictim)
        ENDIF
        IF DOES_PICKUP_EXIST(pickupStolenItem) 
            IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                bVictimFled = TRUE
                pickUpTheStolenItem()
            ELSE
                CPRINTLN(DEBUG_AMBIENT, "\nre_muggings AimingAtVictim() - missionCleanup()")
                
                SNAP_OUT_OF_LYING_DOWN_ANIM(FALSE)
                
                missionCleanup()
            ENDIF   
        ELSE
            CPRINTLN(DEBUG_AMBIENT, "\nre_muggings AimingAtVictim() - missionCleanup()")
            missionCleanup()
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL IS_MUGGER_WITH_BAG_DAMAGED_OR_DEAD()
    IF NOT IS_ENTITY_DEAD(pedAssailant)
        IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAssailant, PLAYER_PED_ID())
            IF bMakingARunForIt
                RETURN TRUE
            ENDIF
        ENDIF
    ELSE
        IF bMakingARunForIt
            RETURN TRUE
        ENDIF
    ENDIF   
    
    RETURN FALSE
ENDFUNC



FUNC BOOL HAS_PLAYER_ATTACKED_WHEN_BAG_IS_UNATTACHED()
    // the bag is not attached
    // after the victim has dropped their bag
    // before mugger has picked it up

    IF DOES_PICKUP_EXIST(pickupStolenItem)  
        IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
            // mugger aimed at
            IF DOES_ENTITY_EXIST(pedAssailant)
                IF NOT IS_ENTITY_DEAD(pedAssailant)
                    IF IS_PED_SHOOTING(PLAYER_PED_ID())
                        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedAssailant, <<18,  18,  18>>, FALSE)
                        OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAssailant, PLAYER_PED_ID())
                            SETTIMERB(0)
                            iAttackType = 1
                            RETURN TRUE
                        ENDIF
                    ENDIF
                ENDIF
                
                IF IS_PLAYER_AIMING_AT_MUGGER()
                    SETTIMERB(0)
                    iAttackType = 2
                    RETURN TRUE
                ENDIF
                
                // mugger killed
                IF IS_PED_INJURED(pedAssailant)
                    IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAssailant, PLAYER_PED_ID())
                        IF DOES_ENTITY_EXIST(pedVictim)
                            IF NOT IS_PED_INJURED(pedVictim)
                                CLEAR_PED_TASKS(pedVictim)
                                SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, FALSE)
                            ENDIF
                        ENDIF
                        SETTIMERB(0)
                        iAttackType = 2
                        RETURN TRUE
                    ENDIF
                ELSE
                    IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAssailant, PLAYER_PED_ID())
                        SETTIMERB(0)
                        iAttackType = 2
                        IF DOES_ENTITY_EXIST(pedVictim)
                            IF NOT IS_PED_INJURED(pedVictim)
                                CLEAR_PED_TASKS(pedVictim)
                                SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedVictim, FALSE)
                            ENDIF
                        ENDIF
                        IF NOT IS_PED_INJURED(pedAssailant)
                            CLEAR_PED_TASKS(pedAssailant)
                            TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 250, -1)
                            SET_PED_KEEP_TASK(pedassailant, TRUE)
                            SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, FALSE)
                            SET_PED_AS_NO_LONGER_NEEDED(pedassailant)
                        ENDIF
                        RETURN TRUE
                    ENDIF                           
                ENDIF   
            ENDIF
            
            // victim aimed at          
            IF DOES_ENTITY_EXIST(pedVictim)
                IF NOT IS_ENTITY_DEAD(pedVictim)
                    IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<5.3,  5.3,  6>>, FALSE)
                        IF  IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
                            IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedVictim)
                            OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedVictim)
                                SETTIMERB(0)
                                iAttackType = 3
                                RETURN TRUE
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF   
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC     

FUNC BOOL HAS_PLAYER_BUMPED_INTO_PEDS_DURING_MUGGING()
//  IF NOT IS_PED_INJURED(pedAssailant)
//      IF IS_PED_RAGDOLL(pedAssailant)
//          //IF IS_ENTITY_AT_ENTITY(pedAssailant, PLAYER_PED_ID(), <<1.0, 1.0, 1.0>>)
//              RETURN TRUE
//          //ENDIF
//      ENDIF
//  ENDIF
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_PED_RAGDOLL(pedVictim)
            //IF IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), <<1.0, 1.0, 1.0>>)
                RETURN TRUE
            //ENDIF
        ENDIF
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC bumpReaction()
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bumpReaction()")
    IF NOT IS_PED_INJURED(pedVictim)
        CLEAR_PED_TASKS(pedVictim)
        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
        SET_PED_KEEP_TASK(pedVictim, TRUE)
        WAIT(0)
        SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 6")
    ENDIF
    IF NOT IS_PED_INJURED(pedAssailant)
        // "Who the fuck are you?"
        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWho, CONV_PRIORITY_AMBIENT_HIGH)
        OPEN_SEQUENCE_TASK(seq)
            TASK_CLEAR_LOOK_AT(NULL)
            TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
        CLOSE_SEQUENCE_TASK(seq)
        TASK_PERFORM_SEQUENCE(pedAssailant, seq)
        CLEAR_SEQUENCE_TASK(seq)
        SET_PED_KEEP_TASK(pedassailant, TRUE)
        WAIT(0)
        SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, FALSE)
        SET_PED_AS_NO_LONGER_NEEDED(pedassailant)
    ENDIF   
ENDPROC

PROC bagUnattachedAttackReaction()
    BOOL bPickupIsGone
    
    pickUpTheStolenItem()
    
    IF DOES_PICKUP_EXIST(pickupStolenItem) 
        IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)")
        ENDIF
    ENDIF
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), iAttackType = ", iAttackType, " Reacting to player is ", bReactingToPlayer)
    IF iAttackType = 1 // shoot near
        IF NOT bReactingToPlayer
            IF NOT IS_PED_INJURED(pedAssailant)
                CLEAR_PED_TASKS(pedAssailant)
                TASK_COMBAT_PED(pedAssailant, PLAYER_PED_ID())
                SET_PED_KEEP_TASK(pedassailant, TRUE)
            ENDIF
            IF NOT IS_PED_INJURED(pedVictim)
                IF DOES_PICKUP_EXIST(pickupStolenItem) 
                    IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                        
                        PRINTSTRING("PAUL - TASK_FOLLOW_NAV_MESH_TO_COORD") PRINTNL()
                        CLEAR_PED_TASKS(pedVictim)
                        OPEN_SEQUENCE_TASK(seq)
                            TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PICKUP_COORDS(pickupStolenItem), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS)
                            
                        CLOSE_SEQUENCE_TASK(seq)
                        TASK_PERFORM_SEQUENCE(pedVictim, seq)
                        CLEAR_SEQUENCE_TASK(seq)
                    ENDIF
                ENDIF
            ENDIF
            bReactingToPlayer = TRUE
        ENDIF
    ENDIF
    
    IF iAttackType = 2 // targetted mugger
        IF NOT bReactingToPlayer
            IF NOT IS_PED_INJURED(pedAssailant)
                CLEAR_PED_TASKS(pedAssailant)
                TASK_COMBAT_PED(pedAssailant, PLAYER_PED_ID())
                SET_PED_KEEP_TASK(pedassailant, TRUE)
                SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, FALSE)
                SET_PED_AS_NO_LONGER_NEEDED(pedassailant)
            ENDIF
            IF NOT IS_PED_INJURED(pedVictim)
                IF DOES_PICKUP_EXIST(pickupStolenItem)
                    IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                        IF IS_ENTITY_AT_COORD(pedVictim, GET_PICKUP_COORDS(pickupStolenItem), <<10, 10, 10>>)
                            IF iSelectedVariation = THREE_GIRL_SHOPPER
                                IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_enter")
                                AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_base")
                                AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_exit")
                                    CLEAR_PED_TASKS(pedVictim)
                                    OPEN_SEQUENCE_TASK(seq)
                                        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PICKUP_COORDS(pickupStolenItem), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS)
                                        //TASK_STAND_STILL(NULL, 1000)
                                    CLOSE_SEQUENCE_TASK(seq)
                                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                    CLEAR_SEQUENCE_TASK(seq)
                                    bReactingToPlayer = TRUE
                                ENDIF
                            ELSE
                                IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sFleePedFront)
                                AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sStrugglePed)
                                AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sFleePedBack)
                                    CLEAR_PED_TASKS(pedVictim)
                                    OPEN_SEQUENCE_TASK(seq)
                                        TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_PICKUP_COORDS(pickupStolenItem), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS)
                                        //TASK_STAND_STILL(NULL, 1000)
                                    CLOSE_SEQUENCE_TASK(seq)
                                    TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                    CLEAR_SEQUENCE_TASK(seq)
                                    bReactingToPlayer = TRUE
                                ENDIF
                            ENDIF
                        ELSE
                            bPickupIsGone = TRUE
                        ENDIF
                    ELSE
                        bPickupIsGone = TRUE
                    ENDIF
                ELSE
                    bPickupIsGone = TRUE    
                ENDIF
                
                IF bPickupIsGone
                    IF iSelectedVariation = THREE_GIRL_SHOPPER
                        IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_enter")
                        AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_base")
                        AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "handsup_standing_exit")
                            CLEAR_PED_TASKS(pedVictim)
                            TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                            IF DOES_BLIP_EXIST(blipVictim)
                                REMOVE_BLIP(blipVictim)
                            ENDIF
                            SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 1")
                            bReactingToPlayer = TRUE
                            thisMuggingStage = ENDING_EVENT
                        ENDIF
                    ELSE
                        IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sFleePedFront)
                        AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sStrugglePed)
                        AND NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, sFleePedBack)
                            CLEAR_PED_TASKS(pedVictim)
                            TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                            IF DOES_BLIP_EXIST(blipVictim)
                                REMOVE_BLIP(blipVictim)
                            ENDIF
                            SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 1")
                            bReactingToPlayer = TRUE
                            thisMuggingStage = ENDING_EVENT
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
            
        ENDIF
    ENDIF

    IF iAttackType = 3 // targetted vic
        IF NOT bReactingToPlayer
            IF NOT IS_PED_INJURED(pedAssailant)
                CLEAR_PED_TASKS(pedAssailant)
                TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 250, -1)
                SET_PED_KEEP_TASK(pedassailant, TRUE)
                SET_ENTITY_LOAD_COLLISION_FLAG(pedAssailant, FALSE)
                SET_PED_AS_NO_LONGER_NEEDED(pedassailant)
            ENDIF
            IF NOT IS_PED_INJURED(pedVictim)
                CLEAR_PED_TASKS(pedVictim)
                TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                SET_PED_KEEP_TASK(pedVictim, TRUE)
                SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 7")
            ENDIF
            bVictimFled = TRUE
            bReactingToPlayer = TRUE
        ENDIF
    ENDIF
    
    IF iAttackType = 1 
    OR iAttackType = 2
        IF NOT IS_PED_INJURED(pedVictim)
            IF bReactingToPlayer
            
                IF DOES_PICKUP_EXIST(pickupStolenItem) 
                    IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
    //                  IF DOES_BLIP_EXIST(blipStolenItem)
    //                      REMOVE_BLIP(blipStolenItem)
    //                  ENDIF
    //                  
    //                  IF DOES_BLIP_EXIST(blipVictim)
    //                      REMOVE_BLIP(blipVictim)
    //                  ENDIF
                        
                        IF IS_ENTITY_PLAYING_ANIM(pedVictim, sPickupDict, "pickup_low")
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), IS_ENTITY_PLAYING_ANIM")
                            IF GET_ENTITY_ANIM_CURRENT_TIME(pedVictim, sPickupDict, "pickup_low") > 0.5
                                IF DOES_PICKUP_EXIST(pickupStolenItem) 
                                    IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem) 
                                        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sThx, CONV_PRIORITY_AMBIENT_HIGH)
                                        REMOVE_PICKUP(pickupStolenItem)
                                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), REMOVE_PICKUP(pickupStolenItem)")
                                        bVictimGotWallet = TRUE
                                    ENDIF
                                ENDIF
                            ENDIF
                        ELSE
                            IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                                CLEAR_PED_TASKS(pedVictim)
                                OPEN_SEQUENCE_TASK(seq)
                                    TASK_PLAY_ANIM(NULL, sPickupDict, "pickup_low")             
                                    TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 250, -1)
                                CLOSE_SEQUENCE_TASK(seq)
                                TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                CLEAR_SEQUENCE_TASK(seq)
                                SET_PED_KEEP_TASK(pedVictim, TRUE)
                                CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), REMOVE_PICKUP(pickupStolenItem)")
                            ENDIF
                        ENDIF
                    ELSE
                        IF bVictimGotWallet
							SET_PED_MONEY(pedVictim, iItemValue)
                            SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 8")
                            SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
                            missionPassed()
                        ELSE
                            //CLEAR_PED_TASKS(pedVictim)
                            TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                            IF DOES_BLIP_EXIST(blipVictim)
                                REMOVE_BLIP(blipVictim)
                            ENDIF
                            SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 1 GET_FOR_SELF")
                            bReactingToPlayer = TRUE
                            thisMuggingStage = GET_FOR_SELF
                        ENDIF
                    ENDIF
                ELSE
                    IF bVictimGotWallet
						SET_PED_MONEY(pedVictim, iItemValue)
                        SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 8")
                        SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_RETURN_ITEM)
                        missionPassed()
                    ELSE
                        //CLEAR_PED_TASKS(pedVictim)
                        TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                        SET_PED_KEEP_TASK(pedVictim, TRUE)
                        IF DOES_BLIP_EXIST(blipVictim)
                            REMOVE_BLIP(blipVictim)
                        ENDIF
                        SET_PED_AS_NO_LONGER_NEEDED(pedVictim)
                        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - bagUnattachedAttackReaction(), SET_PED_AS_NO_LONGER_NEEDED(pedVictim) 1 GET_FOR_SELF")
                        bReactingToPlayer = TRUE
                        thisMuggingStage = GET_FOR_SELF
                    ENDIF
                ENDIF
            ENDIF
        ELSE
            thisMuggingStage = ENDING_EVENT
        ENDIF
    ENDIF
    
    
    
ENDPROC

PROC leavingWithBag()
    IF NOT IS_ENTITY_DEAD(pedVictim)
        IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedVictim)) >    (fStoreBagCollectDist+50)
            PRINTNL()
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - Player has left with money.")
            PRINTNL()
            //SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(don't set leaving with wallet.)
            missionPassed()
        ELIF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < iItemValue
            PRINTNL()
            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," - Player has less money than picked up.")
            PRINTNL()
            //SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(don't set leaving with wallet.)
            missionPassed()
        ENDIF
    ENDIF

ENDPROC

PROC approachVictim()
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, g_vAnyMeansLocate)
        //  IF NOT IS_GAMEPLAY_HINT_ACTIVE()
                TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedVictim, 3000)
                TASK_LOOK_AT_ENTITY(pedVictim, PLAYER_PED_ID(), 3000)
                //SET_GAMEPLAY_PED_HINT(pedVictim, <<0, 0.3, 0.5>>, TRUE, 4000, 2000, 2000)
        //  ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC vicLookingOutForPlayer()

    FLOAT fAreaWidth
    
    IF iSelectedVariation = ONE_GUY_VICTIM
        fAreaWidth = 13.0
    ENDIF
    IF iSelectedVariation = FOUR_GIRL_SHOPPER
        fAreaWidth = 5.75
    ENDIF
    
    IF iSelectedVariation = ONE_GUY_VICTIM
    OR iSelectedVariation = FOUR_GIRL_SHOPPER
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vNEndOfAlley1, vNEndOfAlley2, fAreaWidth)
            IF NOT bNEnd
                IF NOT IS_PED_INJURED(pedVictim)
                    //CLEAR_PED_TASKS(pedVictim)
                    //TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, vNEndOfAlleyGoTo, PEDMOVEBLENDRATIO_RUN)
                    TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
                    bNEnd = TRUE
                    bSEnd = FALSE
                ENDIF
            ENDIF
        ENDIF
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vSEndOfAlley1, vSEndOfAlley2, fAreaWidth)
            IF NOT bSEnd
                IF NOT IS_PED_INJURED(pedVictim)
                    //CLEAR_PED_TASKS(pedVictim)
                    //TASK_FOLLOW_NAV_MESH_TO_COORD(pedVictim, vSEndOfAlleyGoTo, PEDMOVEBLENDRATIO_RUN)
                    TASK_TURN_PED_TO_FACE_ENTITY(pedVictim, PLAYER_PED_ID())
                    bSEnd = TRUE
                    bNEnd = FALSE
                ENDIF
            ENDIF
        ENDIF
        IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vInAlley1, vInAlley2, fAreaWidth)
            IF NOT bInAlley
                IF NOT IS_PED_INJURED(pedVictim)
                    //CLEAR_PED_TASKS(pedVictim)
                    bInAlley = TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDIF

    IF NOT bStopppedAtPlayer
        IF NOT IS_PED_INJURED(pedVictim)
            IF IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), <<25,25,5>>) AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(pedVictim, PLAYER_PED_ID())
                IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sPlayerReturn, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
                    IF NOT IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_forward_outro_shopkeeper")
                        //CLEAR_PED_TASKS(pedVictim)
                        OPEN_SEQUENCE_TASK(seq)
                            //TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), DEFAULT_TIME_BEFORE_WARP, 15)
                            TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
                            IF thisEvent = GUY_VICTIM
                                PRINTSTRING("Paul NULL, sReturnDict, ") PRINTNL()
                                TASK_PLAY_ANIM(NULL, sReturnDict, "RETURNING_FRONT_A", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1)
                            ENDIF
    //                      TASK_PLAY_ANIM(NULL, "amb@standwatch@shocked@male@idle_a", "idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1)
    //                      TASK_PLAY_ANIM(NULL, "amb@standwatch@shocked@male@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 2000, AF_LOOPING)
    //                      TASK_PLAY_ANIM(NULL, "amb@standwatch@shocked@male@idle_b", "idle_e", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1)
    //                      TASK_PLAY_ANIM(NULL, "amb@standwatch@shocked@male@base", "base", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 2000, AF_LOOPING)
                        CLOSE_SEQUENCE_TASK(seq)
                        TASK_PERFORM_SEQUENCE(pedVictim, seq)
                        CLEAR_SEQUENCE_TASK(seq)
                        bStopppedAtPlayer = TRUE
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC MUGGER_DROPS_ITEM()
    
    IF NOT DOES_PICKUP_EXIST(pickupStolenItem)
        IF IS_PED_INJURED(pedAssailant)
            vDroppedItemPlacement = GET_DEAD_PED_PICKUP_COORDS(pedAssailant)
        ELSE
            vDroppedItemPlacement = GET_SAFE_PICKUP_COORDS(GET_ENTITY_COORDS(pedAssailant))
        ENDIF
        
        pickupStolenItem = CREATE_PICKUP(PICKUP_MONEY_WALLET, vDroppedItemPlacement, iPlacementFlags, iItemValue, TRUE, modelBag)

        blipStolenItem = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupStolenItem)//ADD_BLIP_FOR_PICKUP(pickupStolenItem)
        KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
        bPurseHasDropped = TRUE
    ENDIF
ENDPROC

PROC detachBagFromMugger()
    
    IF DOES_BLIP_EXIST(blipMugger)
        REMOVE_BLIP(blipMugger)
    ENDIF
    IF DOES_ENTITY_EXIST(objIdStolenItem)
        IF IS_ENTITY_ATTACHED(objIdStolenItem)
            DETACH_ENTITY(objIdStolenItem)
            DELETE_OBJECT(objIdStolenItem)
            
            MUGGER_DROPS_ITEM()
        ENDIF
    ELSE
        MUGGER_DROPS_ITEM()
    ENDIF
    
    IF NOT IS_PED_INJURED(pedAssailant)
        IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sDrop, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING()
        //  had previously been told to get to fight you - now told to remove
        //  CLEAR_PED_TASKS(pedAssailant)
        //  OPEN_SEQUENCE_TASK(seq)
        //      TASK_CLEAR_LOOK_AT(NULL)
        //      TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
        //  CLOSE_SEQUENCE_TASK(seq)
        //  TASK_PERFORM_SEQUENCE(pedAssailant, seq)
        //  CLEAR_SEQUENCE_TASK(seq)

            IF thisEvent = GUY_VICTIM
                SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_DISABLE_HANDS_UP, TRUE)
                SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_USE_VEHICLE, TRUE)
                SET_PED_FLEE_ATTRIBUTES(pedAssailant, FA_USE_COVER, FALSE)
                TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 250, -1)
            ENDIF 
            
            SET_PED_KEEP_TASK(pedAssailant, TRUE)
        ENDIF
    ENDIF

ENDPROC

PROC victimReactionIfNearby()
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedVictim, <<8, 8, 18>>)
        OR IS_ENTITY_ON_SCREEN(pedVictim)
            CLEAR_PRINTS()
//          CLEAR_PED_TASKS(pedVictim)
        ENDIF
    ENDIF   
ENDPROC

PROC ISSUE_WARNING_TO_ROBBER()
    IF bWarningIssued
        IF NOT bMuggerResponds
            IF NOT IS_PED_INJURED(pedAssailant)
                IF IS_PED_ARMED(pedAssailant, WF_INCLUDE_GUN)
                    IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWho, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING() OR IS_CELLPHONE_CONVERSATION_PLAYING()
                        bMuggerResponds = TRUE
                    ENDIF
                ELSE
                    IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sDrop, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING() OR IS_CELLPHONE_CONVERSATION_PLAYING()
                        bMuggerResponds = TRUE
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ELSE
        IF IS_PLAYER_AIMING_AT_MUGGER()
            IF CREATE_CONVERSATION(MuggingConversation, sTextBlock, sWarn, CONV_PRIORITY_AMBIENT_HIGH) OR IS_CELLPHONE_CONVERSATION_PLAYING() OR IS_CELLPHONE_CONVERSATION_PLAYING()
                IF IS_PED_ARMED(pedAssailant, WF_INCLUDE_GUN)
                    TASK_COMBAT_PED(pedAssailant, PLAYER_PED_ID())
                ELSE
                    detachBagFromMugger()
                ENDIF
                bWarningIssued = TRUE
            ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC RUN_HINT_CAM()

    IF NOT IS_PED_INJURED(pedAssailant)
    OR NOT bVictimDroppedBag
    
        CONTROL_PED_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, pedAssailant)
    ELSE
        KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
    ENDIF
ENDPROC

PROC outcome()          
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," outcome() - missionCleanup()")
    missionCleanup()    
ENDPROC
            
PROC victimEndsScenario()
    IF NOT IS_PED_INJURED(pedVictim)
        IF IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_backward_loop_shopkeeper")
            OPEN_SEQUENCE_TASK(seq)
                TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_backward_outro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                TASK_WANDER_STANDARD(NULL)
            CLOSE_SEQUENCE_TASK(seq)
            TASK_PERFORM_SEQUENCE(pedVictim, seq)
            CLEAR_SEQUENCE_TASK(seq)

        ELIF IS_ENTITY_PLAYING_ANIM(pedVictim, sMuggingDict, "flee_forward_loop_shopkeeper")
            OPEN_SEQUENCE_TASK(seq)
                TASK_PLAY_ANIM(NULL, sMuggingDict, "flee_forward_outro_shopkeeper", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
                TASK_WANDER_STANDARD(NULL)
            CLOSE_SEQUENCE_TASK(seq)
            TASK_PERFORM_SEQUENCE(pedVictim, seq)
            CLEAR_SEQUENCE_TASK(seq)
        ENDIF

        CREATE_CONVERSATION(MuggingConversation, sTextBlock, sGiveUp, CONV_PRIORITY_AMBIENT_HIGH)
        bVictimGaveUp = TRUE
        SET_PED_KEEP_TASK(pedVictim, TRUE)
        WAIT(0)
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," victimEndsScenario() - missionCleanup()")
        missionCleanup()
    ENDIF
ENDPROC

FUNC BOOL IS_VARIATION_COMPLETE()
    BOOL bReturn = FALSE
    
    SWITCH iSelectedVariation
        CASE ONE_GUY_VICTIM 
            IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG1)
                bReturn = TRUE
            ENDIF
        BREAK
        CASE TWO_HOLD_UP_ANIM
            IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG2)
                bReturn = TRUE
            ENDIF
        BREAK
        CASE THREE_GIRL_SHOPPER 
            IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG3)
                bReturn = TRUE
            ENDIF
        BREAK
        CASE FOUR_GIRL_SHOPPER
            IF HAS_THIS_SCRIPT_BEEN_REGISTERED_IN_COMPLETION_PERCENTAGE_TOTAL(CP_RE_MUG4)
                bReturn = TRUE
            ENDIF
        BREAK
    ENDSWITCH
    RETURN bReturn
ENDFUNC

// Mission Script -----------------------------------------//
SCRIPT (coords_struct in_coords)

IF HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," - IF HAS_FORCE_CLEANUP_OCCURRED()\n")
    IF thisMuggingStage = PLAYER_HAS_BAG
    OR thisMuggingStage = GET_FOR_SELF
        //REGISTER_AS_COMPLETE()
        RANDOM_EVENT_PASSED(RE_MUGGING, iSelectedVariation)
        missionCleanup()
    ELSE
        missionCleanup()
    ENDIF
ENDIF

vInput = in_coords.vec_coord[0]
PRINTNL()
CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME()," launched")
PRINTVECTOR(vInput)
PRINTNL()

//Leave 3 frames between each load request for this script.
SET_LOAD_QUEUE_LARGE_FRAME_DELAY(sLoadQueue, 3) 

//BREAK_ON_NATIVE_COMMAND("CLEAR_PED_TASKS", FALSE)
//BREAK_ON_NATIVE_COMMAND("CLEAR_PED_TASKS_IMMEDIATELY", FALSE)

getWorldPoint()

IF iSelectedVariation = ONE_GUY_VICTIM
AND Is_This_Random_Character_Mission_Active(RC_RAMPAGE_4) 
AND NOT IS_THIS_RANDOM_CHARACTER_MISSION_COMPLETED(RC_RAMPAGE_4)
    CPRINTLN(DEBUG_RANDOM_EVENTS, GET_THIS_SCRIPT_NAME(), " - RC_RAMPAGE_4 is available, ONE_GUY_VICTIM cannot run, this script will terminate.")
    TERMINATE_THIS_THREAD()
ENDIF

IF CAN_RANDOM_EVENT_LAUNCH(vInput, RE_MUGGING, iSelectedVariation)
    LAUNCH_RANDOM_EVENT()
ELSE
    TERMINATE_THIS_THREAD()
ENDIF

BOOL victimHurt = FALSE
// Mission Loop -------------------------------------------//
WHILE TRUE
    WAIT(0)
    
    UPDATE_LOAD_QUEUE_LARGE(sLoadQueue)
    
    IF IS_PLAYER_PLAYING(PLAYER_ID())
        SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
    ENDIF
    
    FLASH_RANDOM_EVENT_RETURN_ITEM_BLIP(blipVictim, flashTimer)
    
    IF IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()
    OR bMakingARunForIt
        SWITCH ambStage
        
            CASE ambCanRun
                IF LOAD_AND_CREATE_ENTITIES()
                    SET_CREATE_RANDOM_COPS(FALSE)
                    CLEAR_AREA_OF_VEHICLES(<<-127.9025, -1574.0844, 36.4128>>, 10)
                    bDoFullCleanUp = TRUE
                    ambStage = (ambRunning)
                ELSE
                    IF SHOULD_THIS_RANDOM_EVENT_EXIT_BEFORE_LOADING_ASSETS()
                        missionCleanup()
                    ENDIF
                ENDIF   
            BREAK
            
            CASE ambRunning
            
                IF IS_PLAYER_PLAYING(PLAYER_ID())
                    IF iShockingEvent = 0
                        iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, vInput, 0)
                    ENDIF
                    
                    //Stop the peds being agreesive, so they don't re jack cars. 
                    SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
                    
                    SWITCH thisMuggingStage
                    
                        // for HOLD_UP_ANIM - Mugging2 in the debug menu
                        CASE HELDUPANIM_VARIATION       
                            runHoldUpAnimVariation()
                        BREAK
                        
                        CASE MUGGING_INTERACTION // drop bag and speech
                            DRAW_DEBUG_TEXT_2D("MUGGING_INTERACTION", <<0.02, 0.1, 0>>)
                            SWITCH  thisSequenceStage
                                CASE INITIAL_MUGGING_SEQ
                                    DRAW_DEBUG_TEXT_2D("INITIAL_MUGGING_SEQ", <<0.02, 0.1, 0>>)
                                    IF IS_PED_INJURED(pedAssailant) OR  HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedAssailant, PLAYER_PED_ID(), FALSE)
                                        victimHurt =  IS_PED_INJURED(pedVictim)
                                        //PRINTSTRING("Paul  - 1")  PRINTBOOL(victimHurt) PRINTNL()
                                        IF NOT DOES_PICKUP_EXIST(pickupStolenItem) 
                                            PRINTSTRING("Paul  - 2") PRINTNL()
                                            //AimingAtVictim() //Dont set ped as no longer needed as we need him to check if the mission is passed. 
                                        ENDIF
                                        
                                        IF (victimHurt = FALSE) AND iSelectedVariation = ONE_GUY_VICTIM
                                            //STOP_ANIM_PLAYBACK(pedVictim)
                                            SET_PED_TO_RAGDOLL(pedVictim, 500, 1000, TASK_RELAX, FALSE, FALSE)
                                            //SET_PED_RAGDOLL_FORCE_FALL(pedVictim) 
                                            TASK_SMART_FLEE_PED(pedVictim, PLAYER_PED_ID(), 250, -1)
                                            SET_PED_KEEP_TASK(pedVictim, TRUE)
                                            //WAIT(30000)
                                            PRINTSTRING("Paul  - 3") PRINTNL()
                                            SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT(RE_PASS_NORMAL)
                                            missionPassed()
                                        ENDIF
                                    ELSE
                                        muggerApproach()
                                    ENDIF
                                    
                                    IF purseStillinWorld = TRUE AND bMakingARunForIt = TRUE
                                        PRINTSTRING("Paul - HAS_PICKUP_BEEN_COLLECTED") PRINTNL()
                                        thisMuggingStage = ABLE_TO_RETRIEVE_DROPPED_BAG
                                    ELIF bMakingARunForIt
                                        thisSequenceStage = VIC_CRY_OUT
                                    ENDIF
                                BREAK
                                
                                CASE VIC_CRY_OUT
                                    DRAW_DEBUG_TEXT_2D("VIC_CRY_OUT", <<0.02, 0.1, 0>>)
                                    
                                    initialCryForHelp()
                                    IF bCryOut
                                    AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
                                        thisSequenceStage = ASK_PLAYER_FOR_HELP
                                    ENDIF
                                BREAK
                                
                                CASE ASK_PLAYER_FOR_HELP
                                    DRAW_DEBUG_TEXT_2D("ASK_PLAYER_FOR_HELP", <<0.02, 0.1, 0>>)
                                    
                                    IF HAS_PLAYER_APPROACHED_VICTIM()
                                        approachingPlayer()
                                    ENDIF
                                    IF bAskedPlayerForHelp
                                        thisSequenceStage = WAITING_FOR_PLAYERS_HELP
                                    ENDIF
                                    IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                                        PRINTSTRING("PAul - GET_SCRIPT_TASK_STATUS") PRINTNL()
                                        //Play the anims and look in the direction.
                                        OPEN_SEQUENCE_TASK(seq)
                                            
                                            /*IF NOT IS_PED_INJURED(pedAssailant) //Looks at 
                                                PRINTSTRING("PAul - TASK_LOOK_AT_ENTITY1") PRINTNL()
                                                TASK_LOOK_AT_ENTITY(NULL, pedAssailant, 5000)
                                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedAssailant, 5000)
                                            ELSE
                                                PRINTSTRING("PAul - TASK_LOOK_AT_ENTITY2") PRINTNL()
                                                TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), 5000)
                                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 5000)
                                            ENDIF*/
                                            //TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedAssailant)
                                            TASK_LOOK_AT_ENTITY(NULL, pedAssailant, 5000)
                                            TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 5000)
                                            TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", SLOW_BLEND_IN, SLOW_BLEND_OUT)
                                            TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                                            TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                                        CLOSE_SEQUENCE_TASK(seq)
                                        TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                        CLEAR_SEQUENCE_TASK(seq)
                                    ENDIF
                                BREAK
                                
                                CASE WAITING_FOR_PLAYERS_HELP
                                    waitingForPlayerIntervention()
                                    
                                    IF NOT IS_ENTITY_DEAD(pedVictim)
                                        //Play the anims and look in the direction.
                                        IF GET_SCRIPT_TASK_STATUS(pedVictim, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
                                            OPEN_SEQUENCE_TASK(seq)
                                                TASK_TURN_PED_TO_FACE_ENTITY(NULL, pedAssailant)
                                                TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_a", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                                                TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_b", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                                                TASK_PLAY_ANIM(NULL, sMuggingDict, "agitated_loop_c", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
                                            CLOSE_SEQUENCE_TASK(seq)
                                            TASK_PERFORM_SEQUENCE(pedVictim, seq)
                                            CLEAR_SEQUENCE_TASK(seq)
                                        ENDIF
                                    ENDIF
                                BREAK
                            ENDSWITCH
                            
                            IF HAS_PLAYER_HARMED_AIMED_AT_VICTIM() 
                                thisMuggingStage = AIMED_AT_VIC
                            ENDIF
                            
                            IF bMakingARunForIt
                                
                            ENDIF
                            
                            ISSUE_WARNING_TO_ROBBER()
                            
                            IF thisSequenceStage = INITIAL_MUGGING_SEQ
                                IF HAS_PLAYER_ATTACKED_WHEN_BAG_IS_UNATTACHED()
                                    IF iAttackType = 4
                                        IF DOES_BLIP_EXIST(blipMugger)
                                            REMOVE_BLIP(blipMugger)
                                        ENDIF
                                        IF DOES_BLIP_EXIST(blipVictim)
                                            REMOVE_BLIP(blipVictim)
                                        ENDIF
                                        IF NOT DOES_BLIP_EXIST(blipStolenItem)
                                            blipStolenItem = CREATE_AMBIENT_BLIP_FOR_PICKUP(pickupStolenItem)
                                        ENDIF
                                        thisMuggingStage = ABLE_TO_RETRIEVE_DROPPED_BAG
                                    ELSE
                                        thisMuggingStage = EARLY_INTERVENTION
                                    ENDIF
                                ENDIF   

                                IF HAS_PLAYER_BUMPED_INTO_PEDS_DURING_MUGGING()
                                    bumpReaction() // goes to cleanup from here
                                ENDIF
                                
                            ENDIF
                            
                            IF IS_MUGGER_WITH_BAG_DAMAGED_OR_DEAD()
                                detachBagFromMugger()
                                victimReactionIfNearby()
                                IF IS_ENTITY_AT_COORD(pedVictim, vDroppedItemPlacement, <<7,7,7>>)
                                    iAttackType = 2
                                    thisMuggingStage = EARLY_INTERVENTION
                                ELSE
                                    thisMuggingStage = ABLE_TO_RETRIEVE_DROPPED_BAG
                                ENDIF
                            ELSE
                                
                            ENDIF
                            
                            IF bMakingARunForIt
                                IF NOT IS_PED_IN_COMBAT(pedAssailant)
                                    FLASH_RANDOM_EVENT_DECISION_BLIP(blipMugger, iFlashTimestamp)
                                ELSE
                                    IF DOES_BLIP_EXIST(blipMugger)
                                        IF GET_BLIP_COLOUR(blipMugger) = BLIP_COLOUR_RED//ENUM_TO_INT(HUD_COLOUR_FRIENDLY) 
                                        ELSE
                                            SET_BLIP_AS_FRIENDLY(blipMugger, FALSE)
                                            SET_BLIP_COLOUR(blipMugger, BLIP_COLOUR_RED)
                                        ENDIF
                                    ENDIF   
                                ENDIF
                            ENDIF
                            
                            IF IS_PLAYER_SHOOTING_BEFORE_MUGGING_STARTED()
                                thisMuggingStage = ATTACKED_BEFORE_MUGGING
                            ENDIF
                            
                        BREAK
                                                    
                        CASE ABLE_TO_RETRIEVE_DROPPED_BAG
                            DRAW_DEBUG_TEXT_2D("ABLE_TO_RETRIEVE_DROPPED_BAG", <<0.02, 0.1, 0>>)
                            SNAP_OUT_OF_LYING_DOWN_ANIM()
                            IF HAS_PLAYER_APPROACHED_VICTIM()
                                approachingPlayer()
                                waitingForPlayerIntervention()
                            ENDIF
                            IF HAS_PLAYER_HARMED_AIMED_AT_VICTIM() 
                                thisMuggingStage = AIMED_AT_VIC 
                            ENDIF
                            pickUpTheStolenItem()
                        BREAK
                        
                        CASE PLAYER_HAS_BAG
                            //WAIT(100000)
                            DRAW_DEBUG_TEXT_2D("PLAYER_HAS_BAG", <<0.02, 0.1, 0>>)
                            SNAP_OUT_OF_LYING_DOWN_ANIM()
                            IF NOT bRunHandover
                                continualTurnToFacePlayer(pedVictim, iTurnStage)
                                iTurnStage = iTurnStage
                                vicLookingOutForPlayer()
                                approachVictim()
                                leavingWithBag() // player gets cash- go to cleanup
                                IF IS_PLAYER_SHOOTING_AT_VICTIM()
                                    VICTIM_FLEES()
                                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), "> PLAYER_HAS_BAG\n")
                                    //SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT() // don't set, keeps the bag.
                                    missionPassed()
                                ENDIF
                                IF GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) < iItemValue
                                    thisMuggingStage = GET_FOR_SELF
                                ENDIF
                            ENDIF
                            
                            RETURN_BAG_SYNCED_SCENE()
                        BREAK
                        
                        CASE ATTACKED_BEFORE_MUGGING
                            DRAW_DEBUG_TEXT_2D("ABLE_TO_RETRIEVE_DROPPED_BAG", <<0.02, 0.1, 0>>)
                            PlayerShootingNearPedsInitialStage() 
                        BREAK
                        
                        CASE EARLY_INTERVENTION
                            bagUnattachedAttackReaction()
                        BREAK
                        
                        CASE AIMED_AT_VIC
                            AimingAtVictim() 
                        BREAK
                        
                        CASE GIVEN_UP_ON_HELP_FROM_PLAYER
                            IF HAS_PLAYER_HARMED_AIMED_AT_VICTIM() 
                                thisMuggingStage = AIMED_AT_VIC 
                            ENDIF
                            victimEndsScenario() 
                        BREAK
                        
                        CASE LOST_MUGGER
                            IF NOT IS_PED_INJURED(pedVictim)
                                IF IS_ENTITY_AT_ENTITY(pedVictim, PLAYER_PED_ID(), <<20,20,20>>)
                                    thisMuggingStage = GIVEN_UP_ON_HELP_FROM_PLAYER
                                ELSE
                                    CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> LOST_MUGGER\n")
                                    missionCleanup()
                                ENDIF
                            ENDIF
                        BREAK
                                                
                        CASE ENDING_EVENT
                            DRAW_DEBUG_TEXT_2D("ABLE_TO_RETRIEVE_DROPPED_BAG", <<0.02, 0.1, 0>>)
                            IF DOES_ENTITY_EXIST(pedAssailant)  
                                IF IS_PED_INJURED(pedAssailant)
                                    IF DOES_BLIP_EXIST(blipMugger)
                                        REMOVE_BLIP(blipMugger)
                                    ENDIF
                                    IF NOT DOES_ENTITY_EXIST(pedVictim) 
                                        IF DOES_ENTITY_EXIST(objIdStolenItem)   
                                            DELETE_OBJECT(objIdStolenItem)
                                        ENDIF
                                        MUGGER_DROPS_ITEM()
                                        bVictimFled = TRUE
                                        pickUpTheStolenItem()
                                    ENDIF
                                ENDIF
                            ENDIF
                            
                        BREAK
                        
                        CASE GET_FOR_SELF
                            IF DOES_ENTITY_EXIST(objIdStolenItem)
                                DELETE_OBJECT(objIdStolenItem)
                            ENDIF
                            SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
                            CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(),"> GET_FOR_SELF\n")
                            //SET_PASS_TYPE_FOR_THIS_RANDOM_EVENT dont set, keeps the bag.
                            missionPassed()
                        BREAK
                            
                        CASE STAT_OUTCOME
                            outcome()   
                            // ______SCENARIO DONE_____
                        BREAK
                    
                    ENDSWITCH
                    
                ENDIF
                
                IF bPurseHasDropped
                    IF DOES_PICKUP_EXIST(pickupStolenItem) 
                        IF DOES_PICKUP_OBJECT_EXIST(pickupStolenItem)
                            IF VDIST(vDroppedItemPlacement, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 150
                                missionCleanup()
                            ENDIF
                        ENDIF
                    ENDIF
                ELSE
                    IF bMakingARunForIt
                        IF NOT IS_PED_INJURED(pedAssailant) 
                            IF thisMuggingStage != GIVEN_UP_ON_HELP_FROM_PLAYER
                                IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedAssailant) > 150
                                AND (NOT IS_ENTITY_ON_SCREEN(pedAssailant) OR IS_ENTITY_OCCLUDED(pedAssailant))
                                    KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
                                    thisMuggingStage = LOST_MUGGER
                                ELSE
                                    RUN_HINT_CAM()
                                    UPDATE_CHASE_BLIP(blipMugger, pedAssailant, 100)
                                ENDIF
                            ENDIF
                        ELSE
                            KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
                        ENDIF
                    ENDIF   
                ENDIF
                
                IF DOES_ENTITY_EXIST(pedVictim)
                    IF NOT IS_PED_INJURED(pedVictim)
                        IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedVictim, PLAYER_PED_ID())
                            IF DOES_ENTITY_EXIST(objIdStolenItem)
                                IF IS_ENTITY_ATTACHED_TO_ENTITY(objIdStolenItem, pedVictim)
                                    DETACH_ENTITY(objIdStolenItem)
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                
                IF bMuggerFleeing
                    //If we are far away from the victim but close to the player, flee from the player.
                    IF NOT bFleeingPlayer
                        IF NOT IS_ENTITY_DEAD(pedAssailant) AND NOT IS_ENTITY_DEAD(pedVictim)
                            IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedAssailant), GET_ENTITY_COORDS(pedVictim)) > 100.0)
                            AND (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedAssailant), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100.0)
                                PRINTSTRING("Paul - now fleeing player.") PRINTNL()
                                TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 200, -1)
                                bFleeingPlayer = TRUE
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                IF bMakingARunForIt
                AND NOT bMuggerFleeing
                    IF NOT IS_PED_INJURED(pedAssailant)
                        
                        IF iSelectedVariation = THREE_GIRL_SHOPPER
                            IF GET_SCRIPT_TASK_STATUS(pedAssailant, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
                                CLEAR_PED_SECONDARY_TASK(pedAssailant)
                                TASK_SMART_FLEE_PED(pedAssailant, pedVictim, 200, -1)
                                FORCE_PED_MOTION_STATE(pedAssailant, MS_ON_FOOT_RUN, TRUE)
                                bMuggerFleeing = TRUE
                            ENDIF
                        ELSE
                            IF GET_SCRIPT_TASK_STATUS(pedAssailant, SCRIPT_TASK_SMART_FLEE_PED) != PERFORMING_TASK
                                CLEAR_PED_SECONDARY_TASK(pedAssailant)
                                TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 200, -1)
                                FORCE_PED_MOTION_STATE(pedAssailant, MS_ON_FOOT_RUN, TRUE)
                                bMuggerFleeing = TRUE
                            ENDIF
                        ENDIF
                        
                        IF HAS_ANIM_EVENT_FIRED(pedAssailant, HASH("ENDS_IN_RUN"))
                            TASK_SMART_FLEE_PED(pedAssailant, pedVictim, 200, -1)
                            FORCE_PED_MOTION_STATE(pedAssailant, MS_ON_FOOT_RUN, TRUE)
                            bMuggerFleeing = TRUE
                        ENDIF
                        IF iSelectedVariation = ONE_GUY_VICTIM
                        OR iSelectedVariation = FOUR_GIRL_SHOPPER
                            IF IS_ENTITY_PLAYING_ANIM(pedAssailant, sMuggingDict, sFleeThiefFront)
                                IF GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sMuggingDict, sFleeThiefFront) >= 0.922
                                    TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 200, -1)
                                    FORCE_PED_MOTION_STATE(pedAssailant, MS_ON_FOOT_RUN, TRUE)
                                    bMuggerFleeing = TRUE
                                ENDIF
                            ENDIF
                            IF IS_ENTITY_PLAYING_ANIM(pedAssailant, sMuggingDict, sFleeThiefBack)
                                IF GET_ENTITY_ANIM_CURRENT_TIME(pedAssailant, sMuggingDict, sFleeThiefBack) >= 0.922
                                    TASK_SMART_FLEE_PED(pedAssailant, PLAYER_PED_ID(), 200, -1)
                                    FORCE_PED_MOTION_STATE(pedAssailant, MS_ON_FOOT_RUN, TRUE)
                                    bMuggerFleeing = TRUE
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
                
                pedChecks()
                
            BREAK
        ENDSWITCH
    ELSE
        CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " - IF NOT IS_WORLD_POINT_WITHIN_BRAIN_ACTIVATION_RANGE()")
        missionCleanup()
    ENDIF
    
    #IF IS_DEBUG_BUILD
    IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
        missionPassed()
    ENDIF
    IF  IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
        missionCleanup()
    ENDIF
    IF  IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
        IF NOT IS_PED_INJURED(pedAssailant)
            SET_PED_TO_RAGDOLL(pedAssailant, 3000, 3000, TASK_RELAX, FALSE, FALSE)
        ENDIF
    ENDIF
    #ENDIF
ENDWHILE

ENDSCRIPT

